package X;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ser.std.StdDelegatingSerializer;
import java.io.Serializable;

/* renamed from: X.1cF  reason: invalid class name and case insensitive filesystem */
public final class C26851cF extends C11630nW implements Serializable {
    public static final C26851cF instance = new C26851cF(null);
    private static final long serialVersionUID = 1;

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0068, code lost:
        if (r0 != false) goto L_0x006a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.CZZ _constructWriter(X.C11260mU r24, X.C29111ft r25, X.C28311eb r26, X.CXO r27, boolean r28, X.C183512m r29) {
        /*
            r23 = this;
            r14 = r25
            java.lang.String r8 = r14.getName()
            r3 = r24
            X.0kA r0 = r3.getConfig()
            boolean r0 = r0.canOverrideAccessModifiers()
            r12 = r29
            if (r0 == 0) goto L_0x001b
            java.lang.reflect.Member r0 = r12.getMember()
            X.C29081fq.checkAndFixAccess(r0)
        L_0x001b:
            r0 = r26
            X.0jR r9 = r12.getType(r0)
            X.CaI r7 = new X.CaI
            X.4Hw r10 = r14.getWrapperName()
            r1 = r27
            X.0ja r0 = r1._beanDesc
            X.0jY r11 = r0.getClassAnnotations()
            boolean r13 = r14.isRequired()
            r7.<init>(r8, r9, r10, r11, r12, r13)
            r5 = r23
            com.fasterxml.jackson.databind.JsonSerializer r2 = r5.findSerializerFromAnnotation(r3, r12)
            boolean r0 = r2 instanceof X.AnonymousClass2HD
            if (r0 == 0) goto L_0x0046
            r0 = r2
            X.2HD r0 = (X.AnonymousClass2HD) r0
            r0.resolve(r3)
        L_0x0046:
            boolean r0 = r2 instanceof X.C11690nn
            if (r0 == 0) goto L_0x0050
            X.0nn r2 = (X.C11690nn) r2
            com.fasterxml.jackson.databind.JsonSerializer r2 = r2.createContextual(r3, r7)
        L_0x0050:
            r4 = 0
            java.lang.Class r6 = r9._class
            boolean r0 = r6.isArray()
            if (r0 != 0) goto L_0x006a
            java.lang.Class<java.util.Collection> r0 = java.util.Collection.class
            boolean r0 = r0.isAssignableFrom(r6)
            if (r0 != 0) goto L_0x006a
            java.lang.Class<java.util.Map> r0 = java.util.Map.class
            boolean r0 = r0.isAssignableFrom(r6)
            r6 = 0
            if (r0 == 0) goto L_0x006b
        L_0x006a:
            r6 = 1
        L_0x006b:
            if (r6 == 0) goto L_0x0081
            X.0k8 r8 = r3._config
            X.0jR r7 = r9.getContentType()
            X.0jc r6 = r8.getAnnotationIntrospector()
            X.CXQ r4 = r6.findPropertyContentTypeResolver(r8, r12, r9)
            if (r4 != 0) goto L_0x019a
            X.CY1 r4 = r5.createTypeSerializer(r8, r7)
        L_0x0081:
            X.0k8 r6 = r3._config
            r0 = r5
            X.0jc r5 = r6.getAnnotationIntrospector()
            X.CXQ r3 = r5.findPropertyTypeResolver(r6, r12, r9)
            if (r3 != 0) goto L_0x018e
            X.CY1 r19 = r0.createTypeSerializer(r6, r9)
        L_0x0092:
            r17 = r9
            r3 = r9
            X.0jc r0 = r1._annotationIntrospector
            java.lang.Class r5 = r0.findSerializationType(r12)
            if (r5 == 0) goto L_0x00ab
            java.lang.Class r0 = r9._class
            boolean r7 = r5.isAssignableFrom(r0)
            if (r7 == 0) goto L_0x017c
            X.0jR r3 = r9.widenBy(r5)
        L_0x00a9:
            r28 = 1
        L_0x00ab:
            X.0k8 r0 = r1._config
            X.0jR r0 = X.C11630nW.modifySecondaryTypesByAnnotation(r0, r12, r3)
            if (r0 == r3) goto L_0x00b6
            r3 = r0
            r28 = 1
        L_0x00b6:
            if (r28 != 0) goto L_0x00c8
            X.0jc r0 = r1._annotationIntrospector
            X.290 r5 = r0.findSerializationTyping(r12)
            if (r5 == 0) goto L_0x00c8
            X.290 r0 = X.AnonymousClass290.STATIC
            r28 = 0
            if (r5 != r0) goto L_0x00c8
            r28 = 1
        L_0x00c8:
            if (r28 != 0) goto L_0x00cb
            r3 = 0
        L_0x00cb:
            if (r4 == 0) goto L_0x00dd
            if (r3 != 0) goto L_0x00d0
            r3 = r9
        L_0x00d0:
            X.0jR r0 = r3.getContentType()
            if (r0 == 0) goto L_0x01d9
            X.0jR r3 = r3.withContentTypeHandler(r4)
            r3.getContentType()
        L_0x00dd:
            r5 = 0
            r21 = 0
            X.0jc r4 = r1._annotationIntrospector
            X.0oC r0 = r1._outputProps
            X.0oC r0 = r4.findSerializationInclusion(r12, r0)
            if (r0 == 0) goto L_0x00f1
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x0113;
                case 1: goto L_0x0111;
                case 2: goto L_0x0126;
                case 3: goto L_0x0176;
                default: goto L_0x00f1;
            }
        L_0x00f1:
            X.CZZ r13 = new X.CZZ
            X.0ja r0 = r1._beanDesc
            X.0jY r16 = r0.getClassAnnotations()
            r15 = r12
            r20 = r3
            r22 = r5
            r18 = r2
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22)
            X.0jc r0 = r1._annotationIntrospector
            X.CaS r1 = r0.findUnwrappingNameTransformer(r12)
            if (r1 == 0) goto L_0x01a6
            X.CZc r0 = new X.CZc
            r0.<init>(r13, r1)
            return r0
        L_0x0111:
            r21 = 1
        L_0x0113:
            boolean r0 = r9.isContainerType()
            if (r0 == 0) goto L_0x00f1
            X.0k8 r4 = r1._config
            X.0kE r0 = X.C10480kE.WRITE_EMPTY_JSON_ARRAYS
            boolean r0 = r4.isEnabled(r0)
            if (r0 != 0) goto L_0x00f1
            java.lang.Object r5 = X.CZZ.MARKER_FOR_EMPTY
            goto L_0x00f1
        L_0x0126:
            java.lang.String r5 = r14.getName()
            java.lang.Object r0 = r1._defaultBean
            if (r0 != 0) goto L_0x0158
            X.0ja r4 = r1._beanDesc
            X.0k8 r0 = r1._config
            boolean r0 = r0.canOverrideAccessModifiers()
            java.lang.Object r0 = r4.instantiateBean(r0)
            r1._defaultBean = r0
            if (r0 != 0) goto L_0x0158
            X.0ja r0 = r1._beanDesc
            X.0jV r0 = r0.getClassInfo()
            java.lang.Class r0 = r0._class
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Class "
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = " has no default constructor; can not instantiate default bean value to support 'properties=JsonSerialize.Inclusion.NON_DEFAULT' annotation"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        L_0x0158:
            java.lang.Object r6 = r1._defaultBean
            java.lang.Object r5 = r12.getValue(r6)     // Catch:{ Exception -> 0x01a7 }
            if (r5 == 0) goto L_0x0178
            java.lang.Class r6 = r5.getClass()
            boolean r0 = r6.isArray()
            if (r0 == 0) goto L_0x00f1
            int r4 = java.lang.reflect.Array.getLength(r5)
            X.8oO r0 = new X.8oO
            r0.<init>(r6, r4, r5)
            r5 = r0
            goto L_0x00f1
        L_0x0176:
            java.lang.Object r5 = X.CZZ.MARKER_FOR_EMPTY
        L_0x0178:
            r21 = 1
            goto L_0x00f1
        L_0x017c:
            boolean r3 = r0.isAssignableFrom(r5)
            if (r3 == 0) goto L_0x020a
            X.0k8 r0 = r1._config
            X.0jr r0 = r0._base
            X.0js r0 = r0._typeFactory
            X.0jR r3 = r0.constructSpecializedType(r9, r5)
            goto L_0x00a9
        L_0x018e:
            X.0k6 r0 = r6._subtypeResolver
            java.util.Collection r0 = r0.collectAndResolveSubtypes(r12, r6, r5, r9)
            X.CY1 r19 = r3.buildTypeSerializer(r6, r9, r0)
            goto L_0x0092
        L_0x019a:
            X.0k6 r0 = r8._subtypeResolver
            java.util.Collection r0 = r0.collectAndResolveSubtypes(r12, r8, r6, r7)
            X.CY1 r4 = r4.buildTypeSerializer(r8, r7, r0)
            goto L_0x0081
        L_0x01a6:
            return r13
        L_0x01a7:
            r1 = move-exception
        L_0x01a8:
            java.lang.Throwable r0 = r1.getCause()
            if (r0 == 0) goto L_0x01b3
            java.lang.Throwable r1 = r1.getCause()
            goto L_0x01a8
        L_0x01b3:
            boolean r0 = r1 instanceof java.lang.Error
            if (r0 != 0) goto L_0x01d6
            boolean r0 = r1 instanceof java.lang.RuntimeException
            if (r0 == 0) goto L_0x01be
            java.lang.RuntimeException r1 = (java.lang.RuntimeException) r1
            throw r1
        L_0x01be:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Failed to get property '"
            java.lang.String r2 = "' of default "
            java.lang.Class r0 = r6.getClass()
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = " instance"
            java.lang.String r0 = X.AnonymousClass08S.A0T(r3, r5, r2, r1, r0)
            r4.<init>(r0)
            throw r4
        L_0x01d6:
            java.lang.Error r1 = (java.lang.Error) r1
            throw r1
        L_0x01d9:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "Problem trying to create BeanPropertyWriter for property '"
            r2.<init>(r0)
            java.lang.String r0 = r14.getName()
            r2.append(r0)
            java.lang.String r0 = "' (of type "
            r2.append(r0)
            X.0ja r0 = r1._beanDesc
            X.0jR r0 = r0._type
            r2.append(r0)
            java.lang.String r0 = "); serialization type "
            r2.append(r0)
            r2.append(r3)
            java.lang.String r0 = " has no content"
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r4.<init>(r0)
            throw r4
        L_0x020a:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Illegal concrete-type annotation for method '"
            java.lang.String r3 = r12.getName()
            java.lang.String r4 = "': class "
            java.lang.String r5 = r5.getName()
            java.lang.String r6 = " not a super-type of (declared) class "
            java.lang.String r7 = r0.getName()
            java.lang.String r0 = X.AnonymousClass08S.A0U(r2, r3, r4, r5, r6, r7)
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26851cF._constructWriter(X.0mU, X.1ft, X.1eb, X.CXO, boolean, X.12m):X.CZZ");
    }

    public JsonSerializer createSerializer(C11260mU r10, C10030jR r11) {
        boolean z;
        C10450k8 r5 = r10._config;
        C10120ja forSerialization = r5._base._classIntrospector.forSerialization(r5, r11, r5);
        JsonSerializer findSerializerFromAnnotation = findSerializerFromAnnotation(r10, forSerialization.getClassInfo());
        if (findSerializerFromAnnotation != null) {
            return findSerializerFromAnnotation;
        }
        C10070jV classInfo = forSerialization.getClassInfo();
        C10030jR r6 = r11;
        Class findSerializationType = r5.getAnnotationIntrospector().findSerializationType(classInfo);
        if (findSerializationType != null) {
            try {
                r6 = r11.widenBy(findSerializationType);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Failed to widen type " + r11 + " with concrete-type annotation (value " + findSerializationType.getName() + "), method '" + classInfo.getName() + "': " + e.getMessage());
            }
        }
        C10030jR modifySecondaryTypesByAnnotation = C11630nW.modifySecondaryTypesByAnnotation(r5, classInfo, r6);
        if (modifySecondaryTypesByAnnotation == r11) {
            z = false;
        } else {
            boolean z2 = false;
            if (modifySecondaryTypesByAnnotation._class == r11._class) {
                z2 = true;
            }
            if (!z2) {
                forSerialization = r5._base._classIntrospector.forSerialization(r5, modifySecondaryTypesByAnnotation, r5);
            }
            z = true;
        }
        C422428v findSerializationConverter = forSerialization.findSerializationConverter();
        if (findSerializationConverter == null) {
            return _createSerializer2(r10, modifySecondaryTypesByAnnotation, forSerialization, z);
        }
        C10030jR outputType = findSerializationConverter.getOutputType(r10.getTypeFactory());
        boolean z3 = false;
        if (outputType._class == modifySecondaryTypesByAnnotation._class) {
            z3 = true;
        }
        if (!z3) {
            forSerialization = r5._base._classIntrospector.forSerialization(r5, outputType, r5);
        }
        return new StdDelegatingSerializer(findSerializationConverter, outputType, _createSerializer2(r10, outputType, forSerialization, true));
    }

    public Iterable customSerializers() {
        return new C29161fy(this._factoryConfig._additionalSerializers);
    }

    public C26861cG withConfig(C11720nq r6) {
        if (this._factoryConfig == r6) {
            return this;
        }
        Class<?> cls = getClass();
        if (cls == C26851cF.class) {
            return new C26851cF(r6);
        }
        throw new IllegalStateException(AnonymousClass08S.A0S("Subtype of BeanSerializerFactory (", cls.getName(), ") has not properly overridden method 'withAdditionalSerializers': can not instantiate subtype with ", "additional serializer definitions"));
    }

    private C26851cF(C11720nq r1) {
        super(r1);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:3:0x0016 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:154:0x031b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:190:0x03b7 */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:164:0x0355 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:362:0x075d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:460:0x0363 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:168:0x0363 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:329:0x06e2 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:183:0x0392 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:426:0x031b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:425:0x031b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:83:0x01b8 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:423:0x02b7 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:131:0x02b7 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:424:0x01b8 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:139:0x02c9 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:422:0x02b7 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:421:0x01b8 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:416:0x01aa */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:78:0x01aa */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:104:0x0210 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:417:0x01aa */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:415:0x01b8 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:411:0x00a3 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:35:0x00a3 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:412:0x01b8 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:410:0x00a3 */
    /* JADX WARN: Type inference failed for: r5v66 */
    /* JADX WARN: Type inference failed for: r5v67 */
    /* JADX WARN: Type inference failed for: r5v68 */
    /* JADX WARN: Type inference failed for: r5v69 */
    /* JADX WARN: Type inference failed for: r5v71 */
    /* JADX WARN: Type inference failed for: r5v78, types: [com.fasterxml.jackson.databind.JsonSerializer] */
    /* JADX WARN: Type inference failed for: r7v145 */
    /* JADX WARN: Type inference failed for: r7v146 */
    /* JADX WARN: Type inference failed for: r7v147 */
    /* JADX WARN: Type inference failed for: r7v148 */
    /* JADX WARN: Type inference failed for: r7v156, types: [com.fasterxml.jackson.databind.JsonSerializer] */
    /* JADX WARN: Type inference failed for: r5v102 */
    /* JADX WARN: Type inference failed for: r7v183, types: [com.fasterxml.jackson.databind.ser.std.CollectionSerializer] */
    /* JADX WARN: Type inference failed for: r9v27, types: [com.fasterxml.jackson.databind.ser.impl.IndexedListSerializer] */
    /* JADX WARN: Type inference failed for: r5v103 */
    /* JADX WARN: Type inference failed for: r5v104 */
    /* JADX WARN: Type inference failed for: r7v186 */
    /* JADX WARN: Type inference failed for: r16v7, types: [com.fasterxml.jackson.databind.ser.std.EnumMapSerializer] */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0380, code lost:
        if (r1 != false) goto L_0x0382;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0384, code lost:
        if (r5 != false) goto L_0x0386;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x029f  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x030f  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0194  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.fasterxml.jackson.databind.JsonSerializer _createSerializer2(X.C11260mU r24, X.C10030jR r25, X.C10120ja r26, boolean r27) {
        /*
            r23 = this;
            r1 = r27
            r2 = r25
            java.lang.Class r3 = r2._class
            java.lang.Class<X.0jS> r0 = X.C10040jS.class
            boolean r5 = r0.isAssignableFrom(r3)
            r0 = r24
            r4 = r23
            r3 = r26
            if (r5 == 0) goto L_0x0111
            com.fasterxml.jackson.databind.ser.std.SerializableSerializer r7 = com.fasterxml.jackson.databind.ser.std.SerializableSerializer.instance
        L_0x0016:
            if (r7 != 0) goto L_0x0875
            X.0k8 r6 = r0._config
            boolean r5 = r2.isContainerType()
            if (r5 == 0) goto L_0x0301
            if (r27 != 0) goto L_0x0027
            r1 = 0
            boolean r1 = X.C11630nW.usesStaticTyping(r6, r3, r1)
        L_0x0027:
            X.0k8 r11 = r0._config
            if (r1 != 0) goto L_0x0040
            boolean r5 = r2._asStatic
            if (r5 == 0) goto L_0x0040
            boolean r5 = r2.isContainerType()
            if (r5 == 0) goto L_0x003f
            X.0jR r5 = r2.getContentType()
            java.lang.Class r7 = r5._class
            java.lang.Class<java.lang.Object> r5 = java.lang.Object.class
            if (r7 == r5) goto L_0x0040
        L_0x003f:
            r1 = 1
        L_0x0040:
            X.0jR r5 = r2.getContentType()
            X.CY1 r15 = r4.createTypeSerializer(r11, r5)
            if (r15 == 0) goto L_0x004b
            r1 = 0
        L_0x004b:
            X.0jV r7 = r3.getClassInfo()
            X.0jc r5 = r0.getAnnotationIntrospector()
            java.lang.Object r5 = r5.findContentSerializer(r7)
            if (r5 == 0) goto L_0x010d
            com.fasterxml.jackson.databind.JsonSerializer r16 = r0.serializerInstance(r7, r5)
        L_0x005d:
            boolean r5 = r2.isMapLikeType()
            r9 = 0
            if (r5 == 0) goto L_0x016d
            r12 = r2
            X.1Jm r12 = (X.C21991Jm) r12
            X.0jV r7 = r3.getClassInfo()
            X.0jc r5 = r0.getAnnotationIntrospector()
            java.lang.Object r5 = r5.findKeySerializer(r7)
            if (r5 == 0) goto L_0x010a
            com.fasterxml.jackson.databind.JsonSerializer r14 = r0.serializerInstance(r7, r5)
        L_0x0079:
            java.lang.Class<java.util.Map> r7 = java.util.Map.class
            java.lang.Class r5 = r12._class
            boolean r5 = r7.isAssignableFrom(r5)
            if (r5 == 0) goto L_0x0134
            X.10a r12 = (X.C180610a) r12
            java.lang.Iterable r5 = r4.customSerializers()
            java.util.Iterator r8 = r5.iterator()
            r19 = 0
            r7 = r9
        L_0x0090:
            boolean r5 = r8.hasNext()
            if (r5 == 0) goto L_0x00a3
            java.lang.Object r10 = r8.next()
            X.0nr r10 = (X.C11730nr) r10
            r13 = r3
            com.fasterxml.jackson.databind.JsonSerializer r7 = r10.findMapSerializer(r11, r12, r13, r14, r15, r16)
            if (r7 == 0) goto L_0x0090
        L_0x00a3:
            if (r7 != 0) goto L_0x00d6
            java.lang.Class<java.util.EnumMap> r7 = java.util.EnumMap.class
            java.lang.Class r5 = r12._class
            boolean r5 = r7.isAssignableFrom(r5)
            if (r5 == 0) goto L_0x00f3
            X.0jR r7 = r12.getKeyType()
            java.lang.Class r5 = r7._class
            boolean r5 = r5.isEnum()
            if (r5 == 0) goto L_0x00c5
            java.lang.Class r7 = r7._class
            X.0jc r5 = r11.getAnnotationIntrospector()
            X.BKG r19 = X.BKG.constructFromName(r7, r5)
        L_0x00c5:
            com.fasterxml.jackson.databind.ser.std.EnumMapSerializer r7 = new com.fasterxml.jackson.databind.ser.std.EnumMapSerializer
            X.0jR r17 = r12.getContentType()
            r18 = r1
            r20 = r15
            r21 = r16
            r16 = r7
            r16.<init>(r17, r18, r19, r20, r21)
        L_0x00d6:
            X.0nq r5 = r4._factoryConfig
            boolean r1 = r5.hasSerializerModifiers()
            if (r1 == 0) goto L_0x01b8
            X.0ns[] r5 = r5._modifiers
            X.1fy r1 = new X.1fy
            r1.<init>(r5)
            java.util.Iterator r5 = r1.iterator()
        L_0x00e9:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x01b8
            r5.next()
            goto L_0x00e9
        L_0x00f3:
            X.0jc r7 = r11.getAnnotationIntrospector()
            X.0jV r5 = r3.getClassInfo()
            java.lang.String[] r7 = r7.findPropertiesToIgnore(r5)
            r8 = r12
            r9 = r1
            r10 = r15
            r11 = r14
            r12 = r16
            com.fasterxml.jackson.databind.ser.std.MapSerializer r7 = com.fasterxml.jackson.databind.ser.std.MapSerializer.construct(r7, r8, r9, r10, r11, r12)
            goto L_0x00d6
        L_0x010a:
            r14 = 0
            goto L_0x0079
        L_0x010d:
            r16 = 0
            goto L_0x005d
        L_0x0111:
            X.1fw r7 = r3.findJsonValueMethod()
            if (r7 == 0) goto L_0x0131
            java.lang.reflect.Method r6 = r7._method
            X.0kA r5 = r0.getConfig()
            boolean r5 = r5.canOverrideAccessModifiers()
            if (r5 == 0) goto L_0x0126
            X.C29081fq.checkAndFixAccess(r6)
        L_0x0126:
            com.fasterxml.jackson.databind.JsonSerializer r5 = r4.findSerializerFromAnnotation(r0, r7)
            com.fasterxml.jackson.databind.ser.std.JsonValueSerializer r7 = new com.fasterxml.jackson.databind.ser.std.JsonValueSerializer
            r7.<init>(r6, r5)
            goto L_0x0016
        L_0x0131:
            r7 = 0
            goto L_0x0016
        L_0x0134:
            java.lang.Iterable r1 = r4.customSerializers()
            java.util.Iterator r5 = r1.iterator()
        L_0x013c:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x02fe
            java.lang.Object r1 = r5.next()
            X.0nr r1 = (X.C11730nr) r1
            r10 = r1
            r13 = r3
            com.fasterxml.jackson.databind.JsonSerializer r7 = r10.findMapLikeSerializer(r11, r12, r13, r14, r15, r16)
            if (r7 == 0) goto L_0x013c
            X.0nq r5 = r4._factoryConfig
            boolean r1 = r5.hasSerializerModifiers()
            if (r1 == 0) goto L_0x01b8
            X.0ns[] r5 = r5._modifiers
            X.1fy r1 = new X.1fy
            r1.<init>(r5)
            java.util.Iterator r5 = r1.iterator()
        L_0x0163:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x01b8
            r5.next()
            goto L_0x0163
        L_0x016d:
            boolean r5 = r2.isCollectionLikeType()
            if (r5 == 0) goto L_0x0285
            r8 = r2
            X.1CA r8 = (X.AnonymousClass1CA) r8
            java.lang.Class<java.util.Collection> r7 = java.util.Collection.class
            java.lang.Class r5 = r8._class
            boolean r5 = r7.isAssignableFrom(r5)
            if (r5 == 0) goto L_0x0242
            X.1ed r8 = (X.C28331ed) r8
            r22 = r16
            java.lang.Iterable r5 = r4.customSerializers()
            java.util.Iterator r10 = r5.iterator()
            r7 = 0
            r5 = r9
        L_0x018e:
            boolean r9 = r10.hasNext()
            if (r9 == 0) goto L_0x01aa
            java.lang.Object r5 = r10.next()
            X.0nr r5 = (X.C11730nr) r5
            r20 = r3
            r17 = r5
            r18 = r11
            r19 = r8
            r21 = r15
            com.fasterxml.jackson.databind.JsonSerializer r5 = r17.findCollectionSerializer(r18, r19, r20, r21, r22)
            if (r5 == 0) goto L_0x018e
        L_0x01aa:
            if (r5 != 0) goto L_0x01d7
            X.CX6 r9 = r3.findExpectedFormat(r7)
            if (r9 == 0) goto L_0x01bb
            X.CX7 r10 = r9.shape
            X.CX7 r9 = X.CX7.OBJECT
            if (r10 != r9) goto L_0x01bb
        L_0x01b8:
            if (r7 == 0) goto L_0x031b
            return r7
        L_0x01bb:
            java.lang.Class r11 = r8._class
            java.lang.Class<java.util.EnumSet> r9 = java.util.EnumSet.class
            boolean r9 = r9.isAssignableFrom(r11)
            if (r9 == 0) goto L_0x01f4
            X.0jR r8 = r8.getContentType()
            java.lang.Class r1 = r8._class
            boolean r1 = r1.isEnum()
            if (r1 != 0) goto L_0x01d2
            r8 = r7
        L_0x01d2:
            com.fasterxml.jackson.databind.ser.std.EnumSetSerializer r5 = new com.fasterxml.jackson.databind.ser.std.EnumSetSerializer
            r5.<init>(r8, r7)
        L_0x01d7:
            X.0nq r7 = r4._factoryConfig
            boolean r1 = r7.hasSerializerModifiers()
            if (r1 == 0) goto L_0x023f
            X.0ns[] r7 = r7._modifiers
            X.1fy r1 = new X.1fy
            r1.<init>(r7)
            java.util.Iterator r7 = r1.iterator()
        L_0x01ea:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L_0x023f
            r7.next()
            goto L_0x01ea
        L_0x01f4:
            X.0jR r7 = r8.getContentType()
            java.lang.Class r10 = r7._class
            java.lang.Class<java.util.RandomAccess> r7 = java.util.RandomAccess.class
            boolean r9 = r7.isAssignableFrom(r11)
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            if (r9 == 0) goto L_0x0232
            if (r10 != r7) goto L_0x0222
            if (r16 == 0) goto L_0x020e
            boolean r7 = X.C29081fq.isJacksonStdImpl(r22)
            if (r7 == 0) goto L_0x0210
        L_0x020e:
            com.fasterxml.jackson.databind.ser.impl.IndexedStringListSerializer r5 = com.fasterxml.jackson.databind.ser.impl.IndexedStringListSerializer.instance
        L_0x0210:
            if (r5 != 0) goto L_0x01d7
            X.0jR r8 = r8.getContentType()
            com.fasterxml.jackson.databind.ser.std.CollectionSerializer r5 = new com.fasterxml.jackson.databind.ser.std.CollectionSerializer
            r11 = 0
            r7 = r5
            r9 = r1
            r10 = r15
            r12 = r22
            r7.<init>(r8, r9, r10, r11, r12)
            goto L_0x01d7
        L_0x0222:
            X.0jR r10 = r8.getContentType()
            com.fasterxml.jackson.databind.ser.impl.IndexedListSerializer r5 = new com.fasterxml.jackson.databind.ser.impl.IndexedListSerializer
            r13 = 0
            r11 = r1
            r12 = r15
            r14 = r22
            r9 = r5
            r9.<init>(r10, r11, r12, r13, r14)
            goto L_0x0210
        L_0x0232:
            if (r10 != r7) goto L_0x0210
            if (r16 == 0) goto L_0x023c
            boolean r7 = X.C29081fq.isJacksonStdImpl(r22)
            if (r7 == 0) goto L_0x0210
        L_0x023c:
            com.fasterxml.jackson.databind.ser.impl.StringCollectionSerializer r5 = com.fasterxml.jackson.databind.ser.impl.StringCollectionSerializer.instance
            goto L_0x0210
        L_0x023f:
            r7 = r5
            goto L_0x01b8
        L_0x0242:
            java.lang.Iterable r1 = r4.customSerializers()
            java.util.Iterator r5 = r1.iterator()
        L_0x024a:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x02fe
            java.lang.Object r1 = r5.next()
            X.0nr r1 = (X.C11730nr) r1
            r17 = r1
            r18 = r11
            r19 = r8
            r20 = r3
            r21 = r15
            r22 = r16
            com.fasterxml.jackson.databind.JsonSerializer r7 = r17.findCollectionLikeSerializer(r18, r19, r20, r21, r22)
            if (r7 == 0) goto L_0x024a
            X.0nq r5 = r4._factoryConfig
            boolean r1 = r5.hasSerializerModifiers()
            if (r1 == 0) goto L_0x01b8
            X.0ns[] r5 = r5._modifiers
            X.1fy r1 = new X.1fy
            r1.<init>(r5)
            java.util.Iterator r5 = r1.iterator()
        L_0x027b:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x01b8
            r5.next()
            goto L_0x027b
        L_0x0285:
            boolean r5 = r2.isArrayType()
            if (r5 == 0) goto L_0x02fe
            r9 = r2
            X.BMA r9 = (X.BMA) r9
            r8 = r16
            java.lang.Iterable r5 = r4.customSerializers()
            java.util.Iterator r10 = r5.iterator()
            r7 = 0
        L_0x0299:
            boolean r5 = r10.hasNext()
            if (r5 == 0) goto L_0x02b7
            java.lang.Object r5 = r10.next()
            X.0nr r5 = (X.C11730nr) r5
            r17 = r5
            r18 = r11
            r19 = r9
            r20 = r3
            r21 = r15
            r22 = r8
            com.fasterxml.jackson.databind.JsonSerializer r7 = r17.findArraySerializer(r18, r19, r20, r21, r22)
            if (r7 == 0) goto L_0x0299
        L_0x02b7:
            if (r7 != 0) goto L_0x02d4
            java.lang.Class r10 = r9._class
            if (r16 == 0) goto L_0x02c3
            boolean r5 = X.C29081fq.isJacksonStdImpl(r8)
            if (r5 == 0) goto L_0x02c9
        L_0x02c3:
            java.lang.Class<java.lang.String[]> r5 = java.lang.String[].class
            if (r5 != r10) goto L_0x02f1
            com.fasterxml.jackson.databind.ser.impl.StringArraySerializer r7 = com.fasterxml.jackson.databind.ser.impl.StringArraySerializer.instance
        L_0x02c9:
            if (r7 != 0) goto L_0x02d4
            com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer r7 = new com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer
            X.0jR r5 = r9.getContentType()
            r7.<init>(r5, r1, r15, r8)
        L_0x02d4:
            X.0nq r5 = r4._factoryConfig
            boolean r1 = r5.hasSerializerModifiers()
            if (r1 == 0) goto L_0x01b8
            X.0ns[] r5 = r5._modifiers
            X.1fy r1 = new X.1fy
            r1.<init>(r5)
            java.util.Iterator r5 = r1.iterator()
        L_0x02e7:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x01b8
            r5.next()
            goto L_0x02e7
        L_0x02f1:
            java.util.HashMap r7 = X.C38451xS._arraySerializers
            java.lang.String r5 = r10.getName()
            java.lang.Object r7 = r7.get(r5)
            com.fasterxml.jackson.databind.JsonSerializer r7 = (com.fasterxml.jackson.databind.JsonSerializer) r7
            goto L_0x02c9
        L_0x02fe:
            r7 = r9
            goto L_0x01b8
        L_0x0301:
            java.lang.Iterable r1 = r4.customSerializers()
            java.util.Iterator r5 = r1.iterator()
        L_0x0309:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x031b
            java.lang.Object r1 = r5.next()
            X.0nr r1 = (X.C11730nr) r1
            com.fasterxml.jackson.databind.JsonSerializer r7 = r1.findSerializer(r6, r2, r3)
            if (r7 == 0) goto L_0x0309
        L_0x031b:
            if (r7 != 0) goto L_0x03b7
            java.lang.Class r1 = r2._class
            java.lang.String r5 = r1.getName()
            java.util.HashMap r1 = X.C11630nW._concrete
            java.lang.Object r7 = r1.get(r5)
            com.fasterxml.jackson.databind.JsonSerializer r7 = (com.fasterxml.jackson.databind.JsonSerializer) r7
            if (r7 != 0) goto L_0x0355
            java.util.HashMap r1 = X.C11630nW._concreteLazy
            java.lang.Object r1 = r1.get(r5)
            java.lang.Class r1 = (java.lang.Class) r1
            if (r1 == 0) goto L_0x0355
            java.lang.Object r7 = r1.newInstance()     // Catch:{ Exception -> 0x033e }
            com.fasterxml.jackson.databind.JsonSerializer r7 = (com.fasterxml.jackson.databind.JsonSerializer) r7     // Catch:{ Exception -> 0x033e }
            goto L_0x0355
        L_0x033e:
            r5 = move-exception
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r3 = "Failed to instantiate standard serializer (of type "
            java.lang.String r2 = r1.getName()
            java.lang.String r1 = "): "
            java.lang.String r0 = r5.getMessage()
            java.lang.String r0 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)
            r4.<init>(r0, r5)
            throw r4
        L_0x0355:
            if (r7 != 0) goto L_0x03b7
            java.lang.Class r5 = r2._class
            java.lang.Class<java.net.InetAddress> r1 = java.net.InetAddress.class
            boolean r1 = r1.isAssignableFrom(r5)
            if (r1 == 0) goto L_0x0701
            com.fasterxml.jackson.databind.ser.std.InetAddressSerializer r7 = com.fasterxml.jackson.databind.ser.std.InetAddressSerializer.instance
        L_0x0363:
            if (r7 != 0) goto L_0x03b7
            java.lang.Class r5 = r2._class
            java.lang.String r1 = X.C29081fq.canBeABeanType(r5)
            if (r1 != 0) goto L_0x0386
            java.lang.String r5 = r5.getName()
            java.lang.String r1 = "net.sf.cglib.proxy."
            boolean r1 = r5.startsWith(r1)
            if (r1 != 0) goto L_0x0382
            java.lang.String r1 = "org.hibernate.proxy."
            boolean r1 = r5.startsWith(r1)
            r5 = 0
            if (r1 == 0) goto L_0x0383
        L_0x0382:
            r5 = 1
        L_0x0383:
            r1 = 1
            if (r5 == 0) goto L_0x0387
        L_0x0386:
            r1 = 0
        L_0x0387:
            if (r1 != 0) goto L_0x0405
            java.lang.Class r1 = r2._class
            boolean r1 = r1.isEnum()
            if (r1 != 0) goto L_0x0405
            r7 = 0
        L_0x0392:
            if (r7 != 0) goto L_0x03b7
            java.lang.Class r1 = r2._class
            java.lang.Class<java.util.Iterator> r0 = java.util.Iterator.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x03d6
            r0 = 0
            X.0jR r5 = r2.containedType(r0)
            if (r5 != 0) goto L_0x03a9
            X.0jR r5 = X.C10300js.unknownType()
        L_0x03a9:
            X.CY1 r2 = r4.createTypeSerializer(r6, r5)
            boolean r1 = X.C11630nW.usesStaticTyping(r6, r3, r2)
            com.fasterxml.jackson.databind.ser.impl.IteratorSerializer r7 = new com.fasterxml.jackson.databind.ser.impl.IteratorSerializer
            r0 = 0
            r7.<init>(r5, r1, r2, r0)
        L_0x03b7:
            if (r7 == 0) goto L_0x0875
            X.0nq r1 = r4._factoryConfig
            boolean r0 = r1.hasSerializerModifiers()
            if (r0 == 0) goto L_0x0875
            X.0ns[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x03cc:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0875
            r1.next()
            goto L_0x03cc
        L_0x03d6:
            java.lang.Class<java.lang.Iterable> r0 = java.lang.Iterable.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x03f8
            r0 = 0
            X.0jR r5 = r2.containedType(r0)
            if (r5 != 0) goto L_0x03e9
            X.0jR r5 = X.C10300js.unknownType()
        L_0x03e9:
            X.CY1 r2 = r4.createTypeSerializer(r6, r5)
            boolean r1 = X.C11630nW.usesStaticTyping(r6, r3, r2)
            com.fasterxml.jackson.databind.ser.std.IterableSerializer r7 = new com.fasterxml.jackson.databind.ser.std.IterableSerializer
            r0 = 0
            r7.<init>(r5, r1, r2, r0)
            goto L_0x03b7
        L_0x03f8:
            java.lang.Class<java.lang.CharSequence> r0 = java.lang.CharSequence.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x0403
            com.fasterxml.jackson.databind.ser.std.ToStringSerializer r7 = com.fasterxml.jackson.databind.ser.std.ToStringSerializer.instance
            goto L_0x03b7
        L_0x0403:
            r7 = 0
            goto L_0x03b7
        L_0x0405:
            X.0jR r1 = r3._type
            java.lang.Class r5 = r1._class
            java.lang.Class<java.lang.Object> r1 = java.lang.Object.class
            if (r5 != r1) goto L_0x0410
            com.fasterxml.jackson.databind.JsonSerializer r7 = r0._unknownTypeSerializer
            goto L_0x0392
        L_0x0410:
            X.0k8 r5 = r0._config
            X.CZk r1 = new X.CZk
            r1.<init>(r3)
            r1._config = r5
            java.util.List r14 = r3.findProperties()
            X.0jc r10 = r5.getAnnotationIntrospector()
            java.util.HashMap r9 = new java.util.HashMap
            r9.<init>()
            java.util.Iterator r13 = r14.iterator()
        L_0x042a:
            boolean r7 = r13.hasNext()
            if (r7 == 0) goto L_0x0469
            java.lang.Object r7 = r13.next()
            X.1ft r7 = (X.C29111ft) r7
            X.12m r7 = r7.getAccessor()
            if (r7 == 0) goto L_0x0465
            java.lang.Class r11 = r7.getRawType()
            java.lang.Object r7 = r9.get(r11)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            if (r7 != 0) goto L_0x045f
            X.0jR r7 = r5.constructType(r11)
            X.0ja r7 = r5.introspectClassAnnotations(r7)
            X.0jV r7 = r7.getClassInfo()
            java.lang.Boolean r7 = r10.isIgnorableType(r7)
            if (r7 != 0) goto L_0x045c
            java.lang.Boolean r7 = java.lang.Boolean.FALSE
        L_0x045c:
            r9.put(r11, r7)
        L_0x045f:
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x042a
        L_0x0465:
            r13.remove()
            goto L_0x042a
        L_0x0469:
            X.1bz r7 = X.C26771bz.REQUIRE_SETTERS_FOR_GETTERS
            boolean r7 = r5.isEnabled(r7)
            if (r7 == 0) goto L_0x0495
            java.util.Iterator r11 = r14.iterator()
        L_0x0475:
            boolean r7 = r11.hasNext()
            if (r7 == 0) goto L_0x0495
            java.lang.Object r10 = r11.next()
            X.1ft r10 = (X.C29111ft) r10
            X.12m r9 = r10.getMutator()
            r7 = 0
            if (r9 == 0) goto L_0x0489
            r7 = 1
        L_0x0489:
            if (r7 != 0) goto L_0x0475
            boolean r7 = r10.isExplicitlyIncluded()
            if (r7 != 0) goto L_0x0475
            r11.remove()
            goto L_0x0475
        L_0x0495:
            boolean r7 = r14.isEmpty()
            r12 = 0
            if (r7 != 0) goto L_0x051d
            boolean r20 = X.C11630nW.usesStaticTyping(r5, r3, r12)
            X.CXO r11 = new X.CXO
            r11.<init>(r5, r3)
            java.util.ArrayList r12 = new java.util.ArrayList
            int r7 = r14.size()
            r12.<init>(r7)
            X.1eb r18 = r3.bindingsForBeanType()
            java.util.Iterator r14 = r14.iterator()
        L_0x04b6:
            boolean r7 = r14.hasNext()
            if (r7 == 0) goto L_0x051d
            java.lang.Object r10 = r14.next()
            X.1ft r10 = (X.C29111ft) r10
            X.12m r9 = r10.getAccessor()
            boolean r7 = r10.isTypeId()
            if (r7 == 0) goto L_0x04e2
            if (r9 == 0) goto L_0x04b6
            boolean r7 = r5.canOverrideAccessModifiers()
            if (r7 == 0) goto L_0x04db
            java.lang.reflect.Member r7 = r9.getMember()
            X.C29081fq.checkAndFixAccess(r7)
        L_0x04db:
            X.12m r7 = r1._typeId
            if (r7 != 0) goto L_0x0831
            r1._typeId = r9
            goto L_0x04b6
        L_0x04e2:
            X.46h r7 = r10.findReferenceType()
            if (r7 == 0) goto L_0x04f3
            X.46g r8 = r7._type
            X.46g r7 = X.AnonymousClass46g.BACK_REFERENCE
            r13 = 0
            if (r8 != r7) goto L_0x04f0
            r13 = 1
        L_0x04f0:
            if (r13 == 0) goto L_0x04f3
            goto L_0x04b6
        L_0x04f3:
            boolean r7 = r9 instanceof X.C29141fw
            if (r7 == 0) goto L_0x050a
            X.1fw r9 = (X.C29141fw) r9
            r15 = r4
            r16 = r0
            r17 = r10
            r19 = r11
            r21 = r9
            X.CZZ r7 = r15._constructWriter(r16, r17, r18, r19, r20, r21)
            r12.add(r7)
            goto L_0x04b6
        L_0x050a:
            X.1fr r9 = (X.C29091fr) r9
            r15 = r4
            r16 = r0
            r17 = r10
            r19 = r11
            r21 = r9
            X.CZZ r7 = r15._constructWriter(r16, r17, r18, r19, r20, r21)
            r12.add(r7)
            goto L_0x04b6
        L_0x051d:
            if (r12 != 0) goto L_0x0524
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
        L_0x0524:
            X.0nq r8 = r4._factoryConfig
            boolean r7 = r8.hasSerializerModifiers()
            if (r7 == 0) goto L_0x0547
            X.0ns[] r8 = r8._modifiers
            X.1fy r7 = new X.1fy
            r7.<init>(r8)
            java.util.Iterator r8 = r7.iterator()
        L_0x0537:
            boolean r7 = r8.hasNext()
            if (r7 == 0) goto L_0x0547
            java.lang.Object r7 = r8.next()
            X.0ns r7 = (X.C11740ns) r7
            r7.changeProperties(r5, r3, r12)
            goto L_0x0537
        L_0x0547:
            X.0jc r8 = r5.getAnnotationIntrospector()
            X.0jV r7 = r3.getClassInfo()
            java.lang.String[] r8 = r8.findPropertiesToIgnore(r7)
            if (r8 == 0) goto L_0x057c
            int r7 = r8.length
            if (r7 <= 0) goto L_0x057c
            java.util.HashSet r9 = X.C11830o1.arrayToSet(r8)
            java.util.Iterator r8 = r12.iterator()
        L_0x0560:
            boolean r7 = r8.hasNext()
            if (r7 == 0) goto L_0x057c
            java.lang.Object r7 = r8.next()
            X.CZZ r7 = (X.CZZ) r7
            X.0jl r7 = r7._name
            java.lang.String r7 = r7.getValue()
            boolean r7 = r9.contains(r7)
            if (r7 == 0) goto L_0x0560
            r8.remove()
            goto L_0x0560
        L_0x057c:
            X.0nq r8 = r4._factoryConfig
            boolean r7 = r8.hasSerializerModifiers()
            if (r7 == 0) goto L_0x0599
            X.0ns[] r8 = r8._modifiers
            X.1fy r7 = new X.1fy
            r7.<init>(r8)
            java.util.Iterator r8 = r7.iterator()
        L_0x058f:
            boolean r7 = r8.hasNext()
            if (r7 == 0) goto L_0x0599
            r8.next()
            goto L_0x058f
        L_0x0599:
            X.CZj r10 = r3.getObjectIdInfo()
            r8 = 0
            if (r10 == 0) goto L_0x05db
            java.lang.Class r9 = r10._generator
            java.lang.Class<X.CaR> r7 = X.C25132CaR.class
            r11 = 0
            if (r9 != r7) goto L_0x0674
            java.lang.String r7 = r10._propertyName
            int r14 = r12.size()
            r13 = 0
        L_0x05ae:
            if (r13 == r14) goto L_0x084d
            java.lang.Object r9 = r12.get(r13)
            X.CZZ r9 = (X.CZZ) r9
            X.0jl r0 = r9._name
            java.lang.String r0 = r0.getValue()
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x0670
            if (r13 <= 0) goto L_0x05ca
            r12.remove(r13)
            r12.add(r11, r9)
        L_0x05ca:
            X.0jR r11 = r9.getType()
            X.CZg r7 = new X.CZg
            java.lang.Class r0 = r10._scope
            r7.<init>(r0, r9)
            boolean r0 = r10._alwaysAsId
            X.CZl r8 = X.CZl.construct(r11, r8, r7, r0)
        L_0x05db:
            r1._objectIdWriter = r8
            r1._properties = r12
            X.0jc r7 = r5.getAnnotationIntrospector()
            X.0jV r0 = r3.getClassInfo()
            java.lang.Object r0 = r7.findFilterId(r0)
            r1._filterId = r0
            X.12m r0 = r3.findAnyGetter()
            if (r0 == 0) goto L_0x0636
            boolean r7 = r5.canOverrideAccessModifiers()
            if (r7 == 0) goto L_0x0600
            java.lang.reflect.Member r7 = r0.getMember()
            X.C29081fq.checkAndFixAccess(r7)
        L_0x0600:
            X.1eb r7 = r3.bindingsForBeanType()
            X.0jR r8 = r0.getType(r7)
            X.1bz r7 = X.C26771bz.USE_STATIC_TYPING
            boolean r9 = r5.isEnabled(r7)
            X.0jR r13 = r8.getContentType()
            X.CY1 r10 = r4.createTypeSerializer(r5, r13)
            r7 = 0
            r11 = 0
            r12 = 0
            com.fasterxml.jackson.databind.ser.std.MapSerializer r8 = com.fasterxml.jackson.databind.ser.std.MapSerializer.construct(r7, r8, r9, r10, r11, r12)
            X.CaI r11 = new X.CaI
            java.lang.String r12 = r0.getName()
            r14 = 0
            X.0jY r15 = r3.getClassAnnotations()
            r17 = 0
            r16 = r0
            r11.<init>(r12, r13, r14, r15, r16, r17)
            X.CZf r7 = new X.CZf
            r7.<init>(r11, r0, r8)
            r1._anyGetter = r7
        L_0x0636:
            java.util.List r10 = r1._properties
            X.1bz r0 = X.C26771bz.DEFAULT_VIEW_INCLUSION
            boolean r14 = r5.isEnabled(r0)
            int r11 = r10.size()
            X.CZZ[] r9 = new X.CZZ[r11]
            r8 = 0
            r13 = 0
        L_0x0646:
            if (r8 >= r11) goto L_0x069b
            java.lang.Object r7 = r10.get(r8)
            X.CZZ r7 = (X.CZZ) r7
            java.lang.Class[] r12 = r7._includeInViews
            if (r12 != 0) goto L_0x0659
            if (r14 == 0) goto L_0x0656
        L_0x0654:
            r9[r8] = r7
        L_0x0656:
            int r8 = r8 + 1
            goto L_0x0646
        L_0x0659:
            int r13 = r13 + 1
            int r5 = r12.length
            r0 = 1
            if (r5 != r0) goto L_0x0669
            X.CZi r5 = new X.CZi
            r0 = 0
            r0 = r12[r0]
            r5.<init>(r7, r0)
            r7 = r5
            goto L_0x0654
        L_0x0669:
            X.CZh r0 = new X.CZh
            r0.<init>(r7, r12)
            r7 = r0
            goto L_0x0654
        L_0x0670:
            int r13 = r13 + 1
            goto L_0x05ae
        L_0x0674:
            X.0js r8 = r0.getTypeFactory()
            r7 = 0
            X.0jR r9 = r8._constructType(r9, r7)
            X.0js r8 = r0.getTypeFactory()
            java.lang.Class<X.CaN> r7 = X.C25128CaN.class
            X.0jR[] r7 = r8.findTypeParameters(r9, r7)
            r9 = r7[r11]
            X.0jV r7 = r3.getClassInfo()
            X.CaN r8 = r0.objectIdGeneratorInstance(r7, r10)
            java.lang.String r7 = r10._propertyName
            boolean r0 = r10._alwaysAsId
            X.CZl r8 = X.CZl.construct(r9, r7, r8, r0)
            goto L_0x05db
        L_0x069b:
            if (r14 == 0) goto L_0x06bc
            if (r13 != 0) goto L_0x06bc
        L_0x069f:
            X.0nq r5 = r4._factoryConfig
            boolean r0 = r5.hasSerializerModifiers()
            if (r0 == 0) goto L_0x06bf
            X.0ns[] r5 = r5._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r5)
            java.util.Iterator r5 = r0.iterator()
        L_0x06b2:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x06bf
            r5.next()
            goto L_0x06b2
        L_0x06bc:
            r1._filteredProperties = r9
            goto L_0x069f
        L_0x06bf:
            java.util.List r0 = r1._properties
            if (r0 == 0) goto L_0x06f8
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x06f8
            java.util.List r5 = r1._properties
            int r0 = r5.size()
            X.CZZ[] r0 = new X.CZZ[r0]
            java.lang.Object[] r8 = r5.toArray(r0)
            X.CZZ[] r8 = (X.CZZ[]) r8
        L_0x06d7:
            com.fasterxml.jackson.databind.ser.BeanSerializer r7 = new com.fasterxml.jackson.databind.ser.BeanSerializer
            X.0ja r0 = r1._beanDesc
            X.0jR r5 = r0._type
            X.CZZ[] r0 = r1._filteredProperties
            r7.<init>(r5, r1, r8, r0)
        L_0x06e2:
            if (r7 != 0) goto L_0x0392
            boolean r0 = r3.hasKnownClassAnnotations()
            if (r0 == 0) goto L_0x0392
            X.0ja r0 = r1._beanDesc
            X.0jR r5 = r0._type
            com.fasterxml.jackson.databind.ser.BeanSerializer r7 = new com.fasterxml.jackson.databind.ser.BeanSerializer
            X.CZZ[] r1 = com.fasterxml.jackson.databind.ser.std.BeanSerializerBase.NO_PROPS
            r0 = 0
            r7.<init>(r5, r0, r1, r0)
            goto L_0x0392
        L_0x06f8:
            X.CZf r0 = r1._anyGetter
            if (r0 != 0) goto L_0x06fe
            r7 = 0
            goto L_0x06e2
        L_0x06fe:
            X.CZZ[] r8 = X.CZk.NO_PROPERTIES
            goto L_0x06d7
        L_0x0701:
            java.lang.Class<java.util.TimeZone> r1 = java.util.TimeZone.class
            boolean r1 = r1.isAssignableFrom(r5)
            if (r1 == 0) goto L_0x070d
            com.fasterxml.jackson.databind.ser.std.TimeZoneSerializer r7 = com.fasterxml.jackson.databind.ser.std.TimeZoneSerializer.instance
            goto L_0x0363
        L_0x070d:
            java.lang.Class<java.nio.charset.Charset> r1 = java.nio.charset.Charset.class
            boolean r1 = r1.isAssignableFrom(r5)
            if (r1 == 0) goto L_0x0719
            com.fasterxml.jackson.databind.ser.std.ToStringSerializer r7 = com.fasterxml.jackson.databind.ser.std.ToStringSerializer.instance
            goto L_0x0363
        L_0x0719:
            X.8WH r11 = X.AnonymousClass8WH.instance
            X.0k8 r10 = r0._config
            java.lang.Class r9 = r2._class
            java.lang.String r1 = r9.getName()
            java.lang.String r8 = "javax.xml."
            boolean r1 = r1.startsWith(r8)
            r7 = 0
            if (r1 != 0) goto L_0x0745
            boolean r1 = X.AnonymousClass8WH.hasSupertypeStartingWith(r11, r9, r8)
            if (r1 != 0) goto L_0x0745
            java.lang.String r1 = "org.w3c.dom.Node"
            boolean r1 = X.AnonymousClass8WH.doesImplement(r11, r9, r1)
            if (r1 == 0) goto L_0x075d
            java.lang.String r1 = "com.fasterxml.jackson.databind.ext.DOMSerializer"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Exception | LinkageError -> 0x075a }
            java.lang.Object r7 = r1.newInstance()     // Catch:{ Exception | LinkageError -> 0x075a }
            goto L_0x075b
        L_0x0745:
            java.lang.String r1 = "com.fasterxml.jackson.databind.ext.CoreXMLSerializers"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Exception | LinkageError -> 0x0750 }
            java.lang.Object r1 = r1.newInstance()     // Catch:{ Exception | LinkageError -> 0x0750 }
            goto L_0x0751
        L_0x0750:
            r1 = 0
        L_0x0751:
            if (r1 == 0) goto L_0x075d
            X.0nr r1 = (X.C11730nr) r1
            com.fasterxml.jackson.databind.JsonSerializer r7 = r1.findSerializer(r10, r2, r3)
            goto L_0x075d
        L_0x075a:
            r7 = 0
        L_0x075b:
            com.fasterxml.jackson.databind.JsonSerializer r7 = (com.fasterxml.jackson.databind.JsonSerializer) r7
        L_0x075d:
            if (r7 != 0) goto L_0x0363
            java.lang.Class<java.lang.Number> r1 = java.lang.Number.class
            boolean r1 = r1.isAssignableFrom(r5)
            if (r1 == 0) goto L_0x076b
            com.fasterxml.jackson.databind.ser.std.NumberSerializers$NumberSerializer r7 = com.fasterxml.jackson.databind.ser.std.NumberSerializers$NumberSerializer.instance
            goto L_0x0363
        L_0x076b:
            java.lang.Class<java.lang.Enum> r1 = java.lang.Enum.class
            boolean r1 = r1.isAssignableFrom(r5)
            if (r1 == 0) goto L_0x0816
            X.0k8 r8 = r0._config
            r7 = 0
            X.CX6 r12 = r3.findExpectedFormat(r7)
            if (r12 == 0) goto L_0x07a8
            X.CX7 r5 = r12.shape
            X.CX7 r1 = X.CX7.OBJECT
            if (r5 != r1) goto L_0x07a8
            r1 = r3
            X.0jZ r1 = (X.C10110jZ) r1
            java.lang.String r8 = "declaringClass"
            java.util.List r1 = r1._properties
            java.util.Iterator r5 = r1.iterator()
        L_0x078d:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x0363
            java.lang.Object r1 = r5.next()
            X.1ft r1 = (X.C29111ft) r1
            java.lang.String r1 = r1.getName()
            boolean r1 = r1.equals(r8)
            if (r1 == 0) goto L_0x078d
            r5.remove()
            goto L_0x0363
        L_0x07a8:
            java.lang.Class r11 = r2._class
            X.0jc r5 = r8.getAnnotationIntrospector()
            X.0kE r1 = X.C10480kE.WRITE_ENUMS_USING_TO_STRING
            boolean r1 = r8.isEnabled(r1)
            if (r1 == 0) goto L_0x07eb
            r7 = r11
            java.lang.Class r5 = r11.getSuperclass()
            java.lang.Class<java.lang.Enum> r1 = java.lang.Enum.class
            if (r5 == r1) goto L_0x07c3
            java.lang.Class r7 = r11.getSuperclass()
        L_0x07c3:
            java.lang.Object[] r13 = r7.getEnumConstants()
            java.lang.Enum[] r13 = (java.lang.Enum[]) r13
            if (r13 == 0) goto L_0x0865
            java.util.HashMap r10 = new java.util.HashMap
            r10.<init>()
            int r9 = r13.length
            r8 = 0
        L_0x07d2:
            if (r8 >= r9) goto L_0x07e5
            r7 = r13[r8]
            X.0jl r5 = new X.0jl
            java.lang.String r1 = r7.toString()
            r5.<init>(r1)
            r10.put(r7, r5)
            int r8 = r8 + 1
            goto L_0x07d2
        L_0x07e5:
            X.BKG r5 = new X.BKG
            r5.<init>(r11, r10)
            goto L_0x07ef
        L_0x07eb:
            X.BKG r5 = X.BKG.constructFromName(r11, r5)
        L_0x07ef:
            r1 = 1
            java.lang.Boolean r1 = com.fasterxml.jackson.databind.ser.std.EnumSerializer._isShapeWrittenUsingIndex(r11, r12, r1)
            com.fasterxml.jackson.databind.ser.std.EnumSerializer r7 = new com.fasterxml.jackson.databind.ser.std.EnumSerializer
            r7.<init>(r5, r1)
            X.0nq r5 = r4._factoryConfig
            boolean r1 = r5.hasSerializerModifiers()
            if (r1 == 0) goto L_0x0363
            X.0ns[] r5 = r5._modifiers
            X.1fy r1 = new X.1fy
            r1.<init>(r5)
            java.util.Iterator r5 = r1.iterator()
        L_0x080c:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x0363
            r5.next()
            goto L_0x080c
        L_0x0816:
            java.lang.Class<java.util.Calendar> r1 = java.util.Calendar.class
            boolean r1 = r1.isAssignableFrom(r5)
            if (r1 == 0) goto L_0x0822
            com.fasterxml.jackson.databind.ser.std.CalendarSerializer r7 = com.fasterxml.jackson.databind.ser.std.CalendarSerializer.instance
            goto L_0x0363
        L_0x0822:
            java.lang.Class<java.util.Date> r1 = java.util.Date.class
            boolean r1 = r1.isAssignableFrom(r5)
            if (r1 == 0) goto L_0x082e
            com.fasterxml.jackson.databind.ser.std.DateSerializer r7 = com.fasterxml.jackson.databind.ser.std.DateSerializer.instance
            goto L_0x0363
        L_0x082e:
            r7 = 0
            goto L_0x0363
        L_0x0831:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Multiple type ids specified with "
            r1.<init>(r0)
            r1.append(r7)
            java.lang.String r0 = " and "
            r1.append(r0)
            r1.append(r9)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x084d:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "Invalid Object Id definition for "
            X.0jR r0 = r3._type
            java.lang.Class r0 = r0._class
            java.lang.String r2 = r0.getName()
            java.lang.String r1 = ": can not find property with name '"
            java.lang.String r0 = "'"
            java.lang.String r0 = X.AnonymousClass08S.A0T(r4, r2, r1, r7, r0)
            r5.<init>(r0)
            throw r5
        L_0x0865:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Can not determine enum constants for Class "
            java.lang.String r0 = r11.getName()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x0875:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26851cF._createSerializer2(X.0mU, X.0jR, X.0ja, boolean):com.fasterxml.jackson.databind.JsonSerializer");
    }
}
