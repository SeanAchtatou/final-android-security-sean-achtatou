package X;

import com.facebook.rti.orca.MainService;

/* renamed from: X.0Re  reason: invalid class name */
public final class AnonymousClass0Re implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.orca.MainService$1";
    public final /* synthetic */ MainService A00;

    public AnonymousClass0Re(MainService mainService) {
        this.A00 = mainService;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0014, code lost:
        if (X.AnonymousClass03g.A03(r1) == false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r2 = this;
            com.facebook.rti.orca.MainService r0 = r2.A00
            X.C07620dr.A00(r0)
            com.facebook.rti.orca.MainService r0 = r2.A00
            X.03g r1 = r0.A00
            boolean r0 = X.AnonymousClass03g.A04(r1)
            if (r0 == 0) goto L_0x0016
            boolean r1 = X.AnonymousClass03g.A03(r1)
            r0 = 1
            if (r1 != 0) goto L_0x0017
        L_0x0016:
            r0 = 0
        L_0x0017:
            if (r0 != 0) goto L_0x001e
            com.facebook.rti.orca.MainService r0 = r2.A00
            r0.stopSelf()
        L_0x001e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Re.run():void");
    }
}
