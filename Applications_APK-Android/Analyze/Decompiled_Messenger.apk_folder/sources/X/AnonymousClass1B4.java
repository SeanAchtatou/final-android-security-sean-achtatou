package X;

/* renamed from: X.1B4  reason: invalid class name */
public final class AnonymousClass1B4 {
    public final C25051Yd A00;

    public static final AnonymousClass1B4 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1B4(r1);
    }

    public boolean A01() {
        return this.A00.Aem(284820756239370L);
    }

    public boolean A02(boolean z) {
        if (z) {
            return this.A00.Aem(284820756370444L);
        }
        return this.A00.Aer(284820756370444L, AnonymousClass0XE.A07);
    }

    public AnonymousClass1B4(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
