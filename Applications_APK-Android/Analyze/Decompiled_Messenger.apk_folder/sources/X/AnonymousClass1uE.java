package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.montagemetadata.MontageActorInfo;

/* renamed from: X.1uE  reason: invalid class name */
public final class AnonymousClass1uE implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new MontageActorInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new MontageActorInfo[i];
    }
}
