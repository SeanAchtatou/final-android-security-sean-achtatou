package X;

import com.google.common.collect.ImmutableMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0xR  reason: invalid class name and case insensitive filesystem */
public final class C16630xR implements C16650xU {
    private static final Map A00;
    private static volatile C16630xR A01;

    static {
        ImmutableMap.Builder builder = new ImmutableMap.Builder();
        builder.put(C16660xW.class, 4);
        builder.put(C16670xa.class, 3);
        builder.put(C16680xb.class, 2);
        builder.put(C16690xc.class, 0);
        A00 = builder.build();
    }

    public static final C16630xR A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C16630xR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C16630xR();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public int AeD(Class cls) {
        return ((Integer) A00.get(cls)).intValue();
    }
}
