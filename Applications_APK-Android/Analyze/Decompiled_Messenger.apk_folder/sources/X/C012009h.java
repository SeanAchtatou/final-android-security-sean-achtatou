package X;

/* renamed from: X.09h  reason: invalid class name and case insensitive filesystem */
public final class C012009h {
    public final long A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final long A05;
    public final long A06;

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00aa, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00ae */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C012009h A00() {
        /*
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ Exception -> 0x00af }
            java.lang.String r0 = "/proc/self/io"
            r1.<init>(r0)     // Catch:{ Exception -> 0x00af }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00af }
            r3.<init>(r1)     // Catch:{ Exception -> 0x00af }
            r4 = -1
            r6 = -1
            r8 = -1
            r10 = -1
            r12 = -1
            r14 = -1
            r16 = -1
        L_0x001a:
            java.lang.String r1 = r3.readLine()     // Catch:{ all -> 0x00a8 }
            if (r1 == 0) goto L_0x009f
            java.lang.String r0 = "rchar: "
            boolean r2 = r1.startsWith(r0)     // Catch:{ all -> 0x00a8 }
            r0 = 7
            if (r2 == 0) goto L_0x0032
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x00a8 }
            long r4 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x00a8 }
            goto L_0x001a
        L_0x0032:
            java.lang.String r2 = "wchar: "
            boolean r2 = r1.startsWith(r2)     // Catch:{ all -> 0x00a8 }
            if (r2 == 0) goto L_0x0043
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x00a8 }
            long r6 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x00a8 }
            goto L_0x001a
        L_0x0043:
            java.lang.String r2 = "syscr: "
            boolean r2 = r1.startsWith(r2)     // Catch:{ all -> 0x00a8 }
            if (r2 == 0) goto L_0x0054
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x00a8 }
            long r8 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x00a8 }
            goto L_0x001a
        L_0x0054:
            java.lang.String r2 = "syscw: "
            boolean r2 = r1.startsWith(r2)     // Catch:{ all -> 0x00a8 }
            if (r2 == 0) goto L_0x0065
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x00a8 }
            long r10 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x00a8 }
            goto L_0x001a
        L_0x0065:
            java.lang.String r0 = "read_bytes: "
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x00a8 }
            if (r0 == 0) goto L_0x0078
            r0 = 12
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x00a8 }
            long r12 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x00a8 }
            goto L_0x001a
        L_0x0078:
            java.lang.String r0 = "write_bytes: "
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x00a8 }
            if (r0 == 0) goto L_0x008b
            r0 = 13
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x00a8 }
            long r14 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x00a8 }
            goto L_0x001a
        L_0x008b:
            java.lang.String r0 = "cancelled_write_bytes: "
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x00a8 }
            if (r0 == 0) goto L_0x001a
            r0 = 23
            java.lang.String r0 = r1.substring(r0)     // Catch:{ all -> 0x00a8 }
            long r16 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x00a8 }
            goto L_0x001a
        L_0x009f:
            r3.close()     // Catch:{ Exception -> 0x00af }
            X.09h r3 = new X.09h     // Catch:{ Exception -> 0x00af }
            r3.<init>(r4, r6, r8, r10, r12, r14, r16)     // Catch:{ Exception -> 0x00af }
            return r3
        L_0x00a8:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00aa }
        L_0x00aa:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x00ae }
        L_0x00ae:
            throw r0     // Catch:{ Exception -> 0x00af }
        L_0x00af:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C012009h.A00():X.09h");
    }

    public C012009h A01(C012009h r26) {
        C012009h r6 = r26;
        long j = this.A02 - r6.A02;
        long j2 = this.A05 - r6.A05;
        long j3 = this.A03 - r6.A03;
        long j4 = this.A06 - r6.A06;
        long j5 = this.A01 - r6.A01;
        return new C012009h(j, j2, j3, j4, j5, this.A04 - r6.A04, this.A00 - r6.A00);
    }

    private C012009h(long j, long j2, long j3, long j4, long j5, long j6, long j7) {
        this.A02 = j;
        this.A05 = j2;
        this.A03 = j3;
        this.A06 = j4;
        this.A01 = j5;
        this.A04 = j6;
        this.A00 = j7;
    }
}
