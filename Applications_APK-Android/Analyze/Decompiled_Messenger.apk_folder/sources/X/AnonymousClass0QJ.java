package X;

import android.content.SharedPreferences;
import android.os.Build;

/* renamed from: X.0QJ  reason: invalid class name */
public final class AnonymousClass0QJ implements AnonymousClass0DD {
    public SharedPreferences.Editor A00;

    public AnonymousClass0DD ASR() {
        this.A00.clear();
        return this;
    }

    public AnonymousClass0DD Bz7(String str, int i) {
        this.A00.putInt(str, i);
        return this;
    }

    public AnonymousClass0DD BzB(String str, long j) {
        this.A00.putLong(str, j);
        return this;
    }

    public AnonymousClass0DD BzD(String str, String str2) {
        this.A00.putString(str, str2);
        return this;
    }

    public AnonymousClass0DD C1C(String str) {
        this.A00.remove(str);
        return this;
    }

    public void commit() {
        if (Build.VERSION.SDK_INT >= 9) {
            this.A00.apply();
        } else {
            this.A00.commit();
        }
    }

    public AnonymousClass0QJ(SharedPreferences.Editor editor) {
        this.A00 = editor;
    }
}
