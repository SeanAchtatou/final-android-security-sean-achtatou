package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.22D  reason: invalid class name */
public final class AnonymousClass22D {
    private static volatile AnonymousClass22D A01;
    public final C25051Yd A00;

    public static final AnonymousClass22D A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass22D.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass22D(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public AnonymousClass10O A01() {
        if (this.A00.Aem(282235187627258L)) {
            return AnonymousClass10O.RED_DOT;
        }
        return AnonymousClass10O.RED_WITH_TEXT;
    }

    private AnonymousClass22D(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
