package X;

import android.content.Context;
import android.content.ContextWrapper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.0ud  reason: invalid class name and case insensitive filesystem */
public final class C15040ud {
    public static C15030uc A00;
    private static final Object A01 = new Object();
    private static final Map A02 = new HashMap(4);
    private static final WeakHashMap A03 = new WeakHashMap();

    public static void A01(Context context) {
        synchronized (A01) {
            if (A02.containsKey(context)) {
                throw new IllegalStateException("The MountContentPools has a reference to an activity that has just been created");
            }
        }
    }

    public static void A02(Context context) {
        boolean z;
        synchronized (A01) {
            Map map = A02;
            map.remove(context);
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                Context context2 = (Context) ((Map.Entry) it.next()).getKey();
                while (true) {
                    if (!(context2 instanceof ContextWrapper)) {
                        z = false;
                        break;
                    }
                    context2 = ((ContextWrapper) context2).getBaseContext();
                    if (context2 == context) {
                        z = true;
                        break;
                    }
                }
                if (z) {
                    it.remove();
                }
            }
            A03.put(C21861Iz.A00(context), true);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005c, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C15690vh A00(android.content.Context r5, X.C17780zS r6) {
        /*
            int r0 = r6.A0M()
            r4 = 0
            if (r0 == 0) goto L_0x0068
            java.lang.Object r3 = X.C15040ud.A01
            monitor-enter(r3)
            java.util.Map r0 = X.C15040ud.A02     // Catch:{ all -> 0x0065 }
            java.lang.Object r2 = r0.get(r5)     // Catch:{ all -> 0x0065 }
            android.util.SparseArray r2 = (android.util.SparseArray) r2     // Catch:{ all -> 0x0065 }
            if (r2 != 0) goto L_0x0048
            android.content.Context r1 = X.C21861Iz.A00(r5)     // Catch:{ all -> 0x0065 }
            java.util.WeakHashMap r0 = X.C15040ud.A03     // Catch:{ all -> 0x0065 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x0065 }
            if (r0 == 0) goto L_0x0022
            monitor-exit(r3)     // Catch:{ all -> 0x0065 }
            return r4
        L_0x0022:
            X.0uc r0 = X.C15040ud.A00     // Catch:{ all -> 0x0065 }
            if (r0 != 0) goto L_0x003e
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0065 }
            r0 = 14
            if (r1 < r0) goto L_0x005d
            X.0uc r0 = new X.0uc     // Catch:{ all -> 0x0065 }
            r0.<init>()     // Catch:{ all -> 0x0065 }
            X.C15040ud.A00 = r0     // Catch:{ all -> 0x0065 }
            android.content.Context r1 = r5.getApplicationContext()     // Catch:{ all -> 0x0065 }
            android.app.Application r1 = (android.app.Application) r1     // Catch:{ all -> 0x0065 }
            X.0uc r0 = X.C15040ud.A00     // Catch:{ all -> 0x0065 }
            r1.registerActivityLifecycleCallbacks(r0)     // Catch:{ all -> 0x0065 }
        L_0x003e:
            android.util.SparseArray r2 = new android.util.SparseArray     // Catch:{ all -> 0x0065 }
            r2.<init>()     // Catch:{ all -> 0x0065 }
            java.util.Map r0 = X.C15040ud.A02     // Catch:{ all -> 0x0065 }
            r0.put(r5, r2)     // Catch:{ all -> 0x0065 }
        L_0x0048:
            int r0 = r6.A00     // Catch:{ all -> 0x0065 }
            java.lang.Object r1 = r2.get(r0)     // Catch:{ all -> 0x0065 }
            X.0vh r1 = (X.C15690vh) r1     // Catch:{ all -> 0x0065 }
            if (r1 != 0) goto L_0x005b
            X.0vh r1 = r6.A0R()     // Catch:{ all -> 0x0065 }
            int r0 = r6.A00     // Catch:{ all -> 0x0065 }
            r2.put(r0, r1)     // Catch:{ all -> 0x0065 }
        L_0x005b:
            monitor-exit(r3)     // Catch:{ all -> 0x0065 }
            return r1
        L_0x005d:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0065 }
            java.lang.String r0 = "Activity callbacks must be invoked manually below ICS (API level 14)"
            r1.<init>(r0)     // Catch:{ all -> 0x0065 }
            throw r1     // Catch:{ all -> 0x0065 }
        L_0x0065:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0065 }
            throw r0
        L_0x0068:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15040ud.A00(android.content.Context, X.0zS):X.0vh");
    }
}
