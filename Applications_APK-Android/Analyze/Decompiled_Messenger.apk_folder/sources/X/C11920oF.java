package X;

import android.graphics.RectF;
import android.net.Uri;
import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.spherical.photo.metadata.SphericalPhotoMetadata;
import com.facebook.ui.media.attachments.model.AnimatedImageTranscodingData;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.facebook.ui.media.attachments.model.MediaUploadConfig;
import com.facebook.ui.media.attachments.model.MediaUploadResult;
import com.facebook.ui.media.attachments.source.MediaResourceSendSource;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0oF  reason: invalid class name and case insensitive filesystem */
public final class C11920oF {
    public final AnonymousClass0jD A00;
    private final C11930oG A01;

    public MediaResource A01(JsonNode jsonNode) {
        C46652Qx r0;
        Uri uri;
        RectF rectF;
        Uri uri2;
        C64613Co r02;
        Uri uri3;
        C64553Ci r03;
        SphericalPhotoMetadata sphericalPhotoMetadata;
        MediaUploadConfig mediaUploadConfig;
        JsonNode jsonNode2 = jsonNode;
        if (jsonNode == null || jsonNode2.isNull()) {
            return null;
        }
        MediaUploadResult mediaUploadResult = new MediaUploadResult(JSONUtil.A0N(jsonNode2.get("fbid")));
        C46642Qw A002 = MediaResource.A00();
        A002.A0D = Uri.parse(JSONUtil.A0N(jsonNode2.get(TraceFieldType.Uri)));
        A002.A0L = C421828p.A00(JSONUtil.A0N(jsonNode2.get("type")));
        String A0N = JSONUtil.A0N(jsonNode2.get("source"));
        if (A0N == null) {
            r0 = C46652Qx.A0F;
        } else if (C46652Qx.A00.containsKey(A0N)) {
            r0 = (C46652Qx) C46652Qx.A00.get(A0N);
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0J("Unsupported Source: ", A0N));
        }
        A002.A0J = r0;
        String A0N2 = JSONUtil.A0N(jsonNode2.get("thumbnailUri"));
        if (A0N2 != null) {
            uri = Uri.parse(A0N2);
        } else {
            uri = null;
        }
        A002.A0C = uri;
        A002.A08 = JSONUtil.A06(jsonNode2.get("mediaItemId"));
        A002.A0M = A01(jsonNode2.get("originalMediaResource"));
        A002.A07 = JSONUtil.A06(jsonNode2.get("duration"));
        A002.A04 = JSONUtil.A04(jsonNode2.get("width"));
        A002.A00 = JSONUtil.A04(jsonNode2.get("height"));
        A002.A03 = JSONUtil.A04(jsonNode2.get("videoBitrate"));
        A002.A0E = C02310Dz.A00(JSONUtil.A05(jsonNode2.get("orientationHint"), 0));
        A002.A0a = JSONUtil.A0N(jsonNode2.get("offlineThreadingId"));
        A002.A0Z = JSONUtil.A0N(jsonNode2.get("mimeType"));
        A002.A06 = (long) JSONUtil.A04(jsonNode2.get("fileSize"));
        String A0N3 = JSONUtil.A0N(jsonNode2.get("cropArea"));
        if (Platform.stringIsNullOrEmpty(A0N3)) {
            rectF = MediaResource.A0m;
        } else {
            Splitter on = Splitter.on(",");
            Preconditions.checkNotNull(A0N3);
            Iterator it = new Iterable(A0N3) {
                public final /* synthetic */ CharSequence val$sequence;

                {
                    this.val$sequence = r2;
                }

                public Iterator iterator() {
                    Splitter splitter = Splitter.this;
                    return splitter.strategy.iterator(splitter, this.val$sequence);
                }

                public String toString() {
                    Joiner on = Joiner.on(", ");
                    StringBuilder sb = new StringBuilder("[");
                    try {
                        on.appendTo(sb, iterator());
                        sb.append(']');
                        return sb.toString();
                    } catch (IOException e) {
                        throw new AssertionError(e);
                    }
                }
            }.iterator();
            rectF = new RectF(Float.parseFloat((String) it.next()), Float.parseFloat((String) it.next()), Float.parseFloat((String) it.next()), Float.parseFloat((String) it.next()));
        }
        A002.A09 = rectF;
        A002.A0l = JSONUtil.A0S(jsonNode2.get("wantFirstFrameForThumbnail"));
        A002.A02 = JSONUtil.A05(jsonNode2.get("trimStartTimeMs"), -1);
        A002.A01 = JSONUtil.A05(jsonNode2.get("trimEndTimeMs"), -2);
        A002.A0O = mediaUploadResult;
        String A0N4 = JSONUtil.A0N(jsonNode2.get("externalContentUri"));
        if (A0N4 != null) {
            uri2 = Uri.parse(A0N4);
        } else {
            uri2 = null;
        }
        A002.A0A = uri2;
        A002.A0h = JSONUtil.A0S(jsonNode2.get("isTrustedExternalContentProvider"));
        A002.A0F = this.A01.A02(JSONUtil.A0N(jsonNode2.get("contentAppAttribution")));
        A002.A0j = JSONUtil.A0U(jsonNode2.get("renderAsSticker"), false);
        A002.A0i = JSONUtil.A0U(jsonNode2.get("isVoicemail"), false);
        A002.A0T = JSONUtil.A0Q(jsonNode2.get("callId"), BuildConfig.FLAVOR);
        A002.A0V = JSONUtil.A0N(jsonNode2.get("encryptionKeyBase64"));
        A002.A0G = ThreadKey.A06(JSONUtil.A0N(jsonNode2.get("threadKey")));
        A002.A0R = MediaResourceSendSource.A00(JSONUtil.A0Q(jsonNode2.get("sendSource"), null));
        String A0N5 = JSONUtil.A0N(jsonNode2.get("qualitySettingOption"));
        if (A0N5 == null) {
            r02 = C64613Co.A02;
        } else {
            r02 = (C64613Co) C64613Co.A00.get(A0N5);
            if (r02 == null) {
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Unsupported photo quality: ", A0N5));
            }
        }
        A002.A0K = r02;
        A002.A0g = JSONUtil.A0U(jsonNode2.get("isStreamingUpload"), false);
        String A0N6 = JSONUtil.A0N(jsonNode2.get("overlayImageUri"));
        if (A0N6 != null) {
            uri3 = Uri.parse(A0N6);
        } else {
            uri3 = null;
        }
        A002.A0B = uri3;
        String A0N7 = JSONUtil.A0N(jsonNode2.get("cameraType"));
        if (A0N7 == null) {
            r03 = C64553Ci.OTHER;
        } else {
            r03 = (C64553Ci) C64553Ci.A00.get(A0N7);
            if (r03 == null) {
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Unsupported Type: ", A0N7));
            }
        }
        A002.A0I = r03;
        String A0N8 = JSONUtil.A0N(jsonNode2.get("sphericalPhotoMetadata"));
        if (Platform.stringIsNullOrEmpty(A0N8)) {
            sphericalPhotoMetadata = null;
        } else {
            try {
                sphericalPhotoMetadata = (SphericalPhotoMetadata) this.A00.A00.readValue(A0N8, SphericalPhotoMetadata.class);
            } catch (IOException e) {
                throw new C21870AjW(e);
            }
        }
        A002.A0H = sphericalPhotoMetadata;
        A002.A05 = JSONUtil.A06(jsonNode2.get("dateTakenMs"));
        A002.A0f = JSONUtil.A0U(jsonNode2.get("isMuted"), false);
        A002.A0X = JSONUtil.A0N(jsonNode2.get("fileName"));
        A002.A0W = JSONUtil.A0N(jsonNode2.get("fileIconUri"));
        JsonNode jsonNode3 = jsonNode2.get("mediaUploadConfig");
        if (jsonNode3 == null || jsonNode3.isNull()) {
            mediaUploadConfig = null;
        } else {
            C25248Ccg ccg = new C25248Ccg();
            ccg.A06 = jsonNode3.get("animatedType").asText();
            ccg.A00 = Uri.parse(jsonNode3.get("inputLocalMediaUri").asText());
            ccg.A05 = Integer.valueOf(jsonNode3.get("outputWidth").asInt());
            ccg.A04 = Integer.valueOf(jsonNode3.get("outputHeight").asInt());
            if (jsonNode3.get("animatedImageTranscodingDataList") != null) {
                JsonNode jsonNode4 = jsonNode3.get("animatedImageTranscodingDataList");
                Preconditions.checkArgument(jsonNode4.isArray());
                ArrayList arrayList = new ArrayList();
                Iterator it2 = jsonNode4.iterator();
                while (it2.hasNext()) {
                    JsonNode jsonNode5 = (JsonNode) it2.next();
                    Uri parse = Uri.parse(jsonNode5.get("animatedImageUri").asText());
                    JsonNode jsonNode6 = jsonNode5.get("positionData");
                    Preconditions.checkArgument(jsonNode6.isArray());
                    float[] fArr = new float[jsonNode6.size()];
                    for (int i = 0; i < jsonNode6.size(); i++) {
                        fArr[i] = jsonNode6.get(i).floatValue();
                    }
                    arrayList.add(new AnimatedImageTranscodingData(parse, fArr, jsonNode5.get("rotationDegree").floatValue(), jsonNode5.get("rotationCenterX").floatValue(), jsonNode5.get("rotationCenterY").floatValue()));
                }
                ccg.A03 = ImmutableList.copyOf((Collection) arrayList);
            }
            if (jsonNode3.get("overlayBitmapWithoutAnimatedImageUri") != null) {
                ccg.A01 = Uri.parse(jsonNode3.get("overlayBitmapWithoutAnimatedImageUri").asText());
            }
            if (jsonNode3.get("shouldTranscodeFromImage") != null) {
                ccg.A08 = Boolean.valueOf(jsonNode3.get("shouldTranscodeFromImage").booleanValue()).booleanValue();
            }
            mediaUploadConfig = new MediaUploadConfig(ccg);
        }
        A002.A0N = mediaUploadConfig;
        return A002.A00();
    }

    public static final C11920oF A00(AnonymousClass1XY r4) {
        return new C11920oF(AnonymousClass0jD.A00(r4), new C11930oG(AnonymousClass0jD.A00(r4)));
    }

    public ObjectNode A02(MediaResource mediaResource) {
        String str;
        String str2;
        String sb;
        String str3;
        String str4;
        if (mediaResource == null) {
            return null;
        }
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        Uri uri = mediaResource.A0D;
        if (uri != null) {
            str = uri.toString();
        } else {
            str = null;
        }
        objectNode.put(TraceFieldType.Uri, str);
        objectNode.put("type", mediaResource.A0L.DBSerialValue);
        objectNode.put("source", mediaResource.A0J.DBSerialValue);
        Uri uri2 = mediaResource.A0C;
        if (uri2 != null) {
            str2 = uri2.toString();
        } else {
            str2 = null;
        }
        objectNode.put("thumbnailUri", str2);
        objectNode.put("mediaItemId", mediaResource.A08);
        objectNode.put("originalMediaResource", A02(mediaResource.A0M));
        objectNode.put("duration", mediaResource.A07);
        objectNode.put("width", mediaResource.A04);
        objectNode.put("height", mediaResource.A00);
        objectNode.put("videoBitrate", mediaResource.A03);
        objectNode.put("orientationHint", mediaResource.A0E.exifValue);
        objectNode.put("offlineThreadingId", mediaResource.A0b);
        objectNode.put("mimeType", mediaResource.A0a);
        objectNode.put("fileSize", mediaResource.A06);
        RectF rectF = mediaResource.A09;
        if (rectF == null) {
            sb = null;
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(rectF.left);
            sb2.append(',');
            sb2.append(rectF.top);
            sb2.append(',');
            sb2.append(rectF.right);
            sb2.append(',');
            sb2.append(rectF.bottom);
            sb = sb2.toString();
        }
        objectNode.put("cropArea", sb);
        objectNode.put("wantFirstFrameForThumbnail", mediaResource.A0l);
        objectNode.put("trimStartTimeMs", mediaResource.A02);
        objectNode.put("trimEndTimeMs", mediaResource.A01);
        objectNode.put("fbid", mediaResource.A03());
        Uri uri3 = mediaResource.A0A;
        if (uri3 != null) {
            str3 = uri3.toString();
        } else {
            str3 = null;
        }
        objectNode.put("externalContentUri", str3);
        objectNode.put("isTrustedExternalContentProvider", mediaResource.A0h);
        objectNode.put("contentAppAttribution", C11930oG.A01(mediaResource.A0F));
        objectNode.put("renderAsSticker", mediaResource.A0j);
        objectNode.put("isVoicemail", mediaResource.A0i);
        objectNode.put("callId", mediaResource.A0U);
        objectNode.put("encryptionKeyBase64", mediaResource.A0W);
        ThreadKey threadKey = mediaResource.A0G;
        if (threadKey != null) {
            objectNode.put("threadKey", threadKey.A0J());
        }
        MediaResourceSendSource mediaResourceSendSource = mediaResource.A0R;
        if (mediaResourceSendSource != null) {
            objectNode.put("sendSource", mediaResourceSendSource.toString());
        }
        objectNode.put("qualitySettingOption", mediaResource.A0K.DBSerialValue);
        objectNode.put("isStreamingUpload", mediaResource.A0g);
        Uri uri4 = mediaResource.A0B;
        if (uri4 != null) {
            str4 = uri4.toString();
        } else {
            str4 = null;
        }
        objectNode.put("overlayImageUri", str4);
        objectNode.put("cameraType", mediaResource.A0I.DBSerialValue);
        objectNode.put("dateTakenMs", mediaResource.A05);
        SphericalPhotoMetadata sphericalPhotoMetadata = mediaResource.A0H;
        if (sphericalPhotoMetadata != null) {
            try {
                objectNode.put("sphericalPhotoMetadata", this.A00.A00.writeValueAsString(sphericalPhotoMetadata));
            } catch (C37571vt e) {
                throw new C21870AjW(e);
            }
        }
        objectNode.put("isMuted", mediaResource.A0f);
        objectNode.put("fileName", mediaResource.A0Y);
        objectNode.put("fileIconUri", mediaResource.A0X);
        MediaUploadConfig mediaUploadConfig = mediaResource.A0N;
        if (mediaUploadConfig != null) {
            String str5 = mediaUploadConfig.A06;
            if (str5.equals("animated_sticker")) {
                ObjectNode objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                objectNode2.put("animatedType", str5);
                objectNode2.put("inputLocalMediaUri", mediaUploadConfig.A01.toString());
                objectNode2.put("outputWidth", mediaUploadConfig.A05);
                objectNode2.put("outputHeight", mediaUploadConfig.A04);
                ImmutableList immutableList = mediaUploadConfig.A03;
                if (immutableList != null) {
                    ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
                    C24971Xv it = immutableList.iterator();
                    while (it.hasNext()) {
                        AnimatedImageTranscodingData animatedImageTranscodingData = (AnimatedImageTranscodingData) it.next();
                        ObjectNode objectNode3 = new ObjectNode(JsonNodeFactory.instance);
                        objectNode3.put("animatedImageUri", animatedImageTranscodingData.A03.toString());
                        objectNode3.put("rotationDegree", animatedImageTranscodingData.A02);
                        objectNode3.put("rotationCenterX", animatedImageTranscodingData.A00);
                        objectNode3.put("rotationCenterY", animatedImageTranscodingData.A01);
                        float[] fArr = animatedImageTranscodingData.A04;
                        ArrayNode arrayNode2 = new ArrayNode(JsonNodeFactory.instance);
                        for (float r0 : fArr) {
                            arrayNode2._children.add(new C28941fc(r0));
                        }
                        objectNode3.put("positionData", arrayNode2);
                        arrayNode.add(objectNode3);
                    }
                    objectNode2.put("animatedImageTranscodingDataList", arrayNode);
                }
                Uri uri5 = mediaUploadConfig.A00;
                if (uri5 != null) {
                    objectNode2.put("overlayBitmapWithoutAnimatedImageUri", uri5.toString());
                }
                objectNode2.put("shouldTranscodeFromImage", mediaUploadConfig.A08);
                objectNode.put("mediaUploadConfig", objectNode2);
            }
        }
        return objectNode;
    }

    public String A03(List list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayNode.add(A02((MediaResource) it.next()));
        }
        return arrayNode.toString();
    }

    private C11920oF(AnonymousClass0jD r1, C11930oG r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public List A04(String str) {
        if (C06850cB.A0B(str)) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = this.A00.A02(str).iterator();
        while (it.hasNext()) {
            builder.add((Object) A01((JsonNode) it.next()));
        }
        return builder.build();
    }
}
