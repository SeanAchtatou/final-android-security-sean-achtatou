package X;

import android.os.DeadObjectException;
import com.google.common.base.Preconditions;

/* renamed from: X.0c4  reason: invalid class name and case insensitive filesystem */
public final class C06780c4 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.multiprocess.peer.PeerProcessManagerImpl$1";
    private int A00 = 0;
    public final /* synthetic */ C07930eP A01;

    public C06780c4(C07930eP r2) {
        this.A01 = r2;
    }

    public void run() {
        Preconditions.checkNotNull(this.A01.A00);
        Preconditions.checkNotNull(this.A01.A01);
        int i = this.A00 + 1;
        this.A00 = i;
        if (i < 5) {
            C07930eP.A02(this.A01);
            long j = ((long) (1 << this.A00)) * 1000;
            if (j > 60000) {
                j = 60000;
            }
            C07930eP r0 = this.A01;
            AnonymousClass00S.A05(r0.A01, r0.A08, j, 712724212);
            return;
        }
        try {
            C07930eP r02 = this.A01;
            r02.A05.C4x(r02.A00);
        } catch (Exception e) {
            if (e.getCause() instanceof DeadObjectException) {
                this.A01.A03.A03("multiprocess_broadcast_failed_dead_object");
                return;
            }
            throw e;
        }
    }
}
