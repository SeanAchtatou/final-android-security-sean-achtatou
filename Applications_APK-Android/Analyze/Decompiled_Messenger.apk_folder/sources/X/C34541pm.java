package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.WeakHashMap;

/* renamed from: X.1pm  reason: invalid class name and case insensitive filesystem */
public final class C34541pm implements AnonymousClass0B0 {
    public final FbSharedPreferences A00;
    public final AnonymousClass1Y7 A01;
    public final Object A02 = new Object();
    public WeakHashMap mListenerCache = new WeakHashMap();
    public Set mSubKeysForListener = new HashSet();

    public synchronized Map getAll() {
        HashMap hashMap;
        SortedMap AlW = this.A00.AlW(this.A01);
        hashMap = new HashMap(AlW.size());
        for (Map.Entry entry : AlW.entrySet()) {
            hashMap.put(((AnonymousClass1Y7) entry.getKey()).A06(this.A01), entry.getValue());
        }
        return hashMap;
    }

    public AnonymousClass0DD AY8() {
        return new AnonymousClass55Q(this, this.A00.edit());
    }

    public boolean contains(String str) {
        return this.A00.BBh((AnonymousClass1Y7) this.A01.A09(str));
    }

    public boolean getBoolean(String str, boolean z) {
        return this.A00.Aep((AnonymousClass1Y7) this.A01.A09(str), z);
    }

    public int getInt(String str, int i) {
        return this.A00.AqN((AnonymousClass1Y7) this.A01.A09(str), i);
    }

    public long getLong(String str, long j) {
        return this.A00.At2((AnonymousClass1Y7) this.A01.A09(str), j);
    }

    public String getString(String str, String str2) {
        return this.A00.B4F((AnonymousClass1Y7) this.A01.A09(str), str2);
    }

    public C34541pm(AnonymousClass1XY r3, String str) {
        this.A00 = FbSharedPreferencesModule.A00(r3);
        this.A01 = (AnonymousClass1Y7) C34551pn.A00.A09(AnonymousClass08S.A0J(str, "/"));
    }
}
