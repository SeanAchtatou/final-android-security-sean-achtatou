package X;

import android.text.TextUtils;

/* renamed from: X.0QW  reason: invalid class name */
public final class AnonymousClass0QW implements C01690Bg {
    public final /* synthetic */ String A00;

    public AnonymousClass0QW(String str) {
        this.A00 = str;
    }

    public Object get() {
        if (TextUtils.isEmpty(this.A00)) {
            return "unset";
        }
        return this.A00;
    }
}
