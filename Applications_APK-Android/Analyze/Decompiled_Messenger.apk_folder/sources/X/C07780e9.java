package X;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;

/* renamed from: X.0e9  reason: invalid class name and case insensitive filesystem */
public final class C07780e9 implements AnonymousClass06U {
    public final /* synthetic */ C09340h3 A00;

    public C07780e9(C09340h3 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(-1985459904);
        if (Build.VERSION.SDK_INT >= 21) {
            synchronized (this.A00.A0E) {
                try {
                    ((PowerManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8E, this.A00.A03)).isPowerSaveMode();
                } finally {
                    AnonymousClass09Y.A01(-980074610, A002);
                }
            }
        }
    }
}
