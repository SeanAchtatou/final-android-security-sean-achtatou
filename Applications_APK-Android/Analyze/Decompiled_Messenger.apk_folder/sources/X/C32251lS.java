package X;

/* renamed from: X.1lS  reason: invalid class name and case insensitive filesystem */
public final class C32251lS implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.mqttlite.whistle.ThreadSafeMqttClient$4";
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ C36181se A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ byte[] A04;

    public C32251lS(C36181se r1, String str, byte[] bArr, int i, int i2) {
        this.A02 = r1;
        this.A03 = str;
        this.A04 = bArr;
        this.A01 = i;
        this.A00 = i2;
    }

    public void run() {
        if (!this.A02.A02) {
            this.A02.A00.publish(this.A03, this.A04, this.A01, this.A00);
        } else {
            C010708t.A0J(ECX.$const$string(27), "publish ignored as client has been closed");
        }
    }
}
