package X;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1aN  reason: invalid class name and case insensitive filesystem */
public final class C25771aN extends C09260gp {
    private static C05540Zi A03;
    public AnonymousClass0UN A00;
    public volatile boolean A01;
    private volatile boolean A02;

    public static final C25771aN A00(AnonymousClass1XY r8) {
        C25771aN r0;
        synchronized (C25771aN.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r8)) {
                    AnonymousClass1XY r4 = (AnonymousClass1XY) A03.A01();
                    C05540Zi r02 = A03;
                    Context A003 = AnonymousClass1YA.A00(r4);
                    C04740Vz A012 = C04720Vx.A01(r4);
                    C09280gs A004 = C09280gs.A00(r4);
                    C11310mh A005 = C11310mh.A00(r4);
                    AnonymousClass0n4.A00(r4);
                    r02.A00 = new C25771aN(r4, A003, A012, A004, A005);
                }
                C05540Zi r1 = A03;
                r0 = (C25771aN) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C04310Tq A02(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.A91, r1);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C25771aN(X.AnonymousClass1XY r7, android.content.Context r8, X.C04740Vz r9, X.C09280gs r10, X.C11310mh r11) {
        /*
            r6 = this;
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r11)
            java.lang.String r5 = "threads_db2"
            r0 = r6
            r2 = r9
            r3 = r10
            r1 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            X.0UN r1 = new X.0UN
            r0 = 1
            r1.<init>(r0, r7)
            r6.A00 = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25771aN.<init>(X.1XY, android.content.Context, X.0Vz, X.0gs, X.0mh):void");
    }

    public void A07() {
        super.A09();
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass0D4.A01(new Exception("supplier-clearAllData"));
        }
    }

    public void A0B(SQLiteException sQLiteException) {
        super.A0B(sQLiteException);
        if (!this.A01) {
            this.A01 = true;
        }
    }
}
