package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.07Y  reason: invalid class name */
public class AnonymousClass07Y extends AnonymousClass07R {
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0030 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.content.Context r6) {
        /*
            r5 = this;
            java.io.File r4 = r5.A00
            X.C000300h.A01(r4)
            r2 = 2147483648(0x80000000, double:1.0609978955E-314)
            java.lang.String r1 = "AppUnpacker.fsync"
            r0 = -283075799(0xffffffffef209b29, float:-4.970518E28)
            X.AnonymousClass00C.A01(r2, r1, r0)
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0031 }
            java.lang.String r0 = "r"
            r1.<init>(r4, r0)     // Catch:{ all -> 0x0031 }
            java.io.FileDescriptor r0 = r1.getFD()     // Catch:{ all -> 0x002a }
            r0.sync()     // Catch:{ all -> 0x002a }
            r1.close()     // Catch:{ all -> 0x0031 }
            r0 = -1677413782(0xffffffff9c04b26a, float:-4.390573E-22)
            X.AnonymousClass00C.A00(r2, r0)
            return
        L_0x002a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002c }
        L_0x002c:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0030 }
        L_0x0030:
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r1 = move-exception
            r0 = -1948416264(0xffffffff8bdd86f8, float:-8.532918E-32)
            X.AnonymousClass00C.A00(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07Y.A01(android.content.Context):void");
    }

    public boolean A02(Context context, byte[] bArr) {
        File file = this.A00;
        C000300h.A01(file);
        return !file.exists();
    }

    public AnonymousClass07Y(String str, String str2) {
        super(str, str2);
    }
}
