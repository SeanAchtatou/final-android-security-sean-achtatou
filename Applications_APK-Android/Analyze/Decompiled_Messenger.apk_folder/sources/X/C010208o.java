package X;

import com.facebook.analytics.appstatelogger.AppStateLogFile;
import java.io.IOException;
import java.io.OutputStream;

/* renamed from: X.08o  reason: invalid class name and case insensitive filesystem */
public final class C010208o extends OutputStream {
    private boolean A00 = false;
    public final /* synthetic */ AppStateLogFile A01;

    public C010208o(AppStateLogFile appStateLogFile) {
        this.A01 = appStateLogFile;
    }

    private void A00() {
        if (this.A00) {
            throw new IOException("Stream is closed");
        }
    }

    public void close() {
        A00();
        try {
            flush();
            AppStateLogFile.ensureMappedByteBufferSizeRemaining(this.A01, 1);
            this.A01.mMappedByteBuffer.put((byte) 0);
            this.A00 = true;
            byte[] digest = this.A01.mDigest.digest();
            AppStateLogFile appStateLogFile = this.A01;
            synchronized (appStateLogFile.mPositionLock) {
                if (appStateLogFile.mIsEnabled) {
                    appStateLogFile.mMappedByteBuffer.position(5);
                }
                for (byte b : digest) {
                    byte b2 = b & 255;
                    byte[] bArr = AppStateLogFile.HEX_CHARACTERS;
                    byte b3 = bArr[b2 >>> 4];
                    byte b4 = bArr[b2 & 15];
                    appStateLogFile.mMappedByteBuffer.put(b3);
                    appStateLogFile.mMappedByteBuffer.put(b4);
                }
            }
            synchronized (this.A01.mPositionLock) {
                try {
                    this.A01.mIsContentOutputStreamOpen = false;
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
        } catch (Throwable th2) {
            synchronized (this.A01.mPositionLock) {
                try {
                    this.A01.mIsContentOutputStreamOpen = false;
                    throw th2;
                } catch (Throwable th3) {
                    while (true) {
                        th = th3;
                    }
                }
            }
        }
    }

    public void flush() {
        A00();
    }

    public void write(int i) {
        A00();
        AppStateLogFile.ensureMappedByteBufferSizeRemaining(this.A01, 1);
        this.A01.mMappedByteBuffer.put((byte) i);
    }

    public void write(byte[] bArr) {
        A00();
        AppStateLogFile.ensureMappedByteBufferSizeRemaining(this.A01, bArr.length);
        this.A01.mMappedByteBuffer.put(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        A00();
        AppStateLogFile.ensureMappedByteBufferSizeRemaining(this.A01, i2);
        this.A01.mMappedByteBuffer.put(bArr, i, i2);
    }
}
