package X;

import android.util.Log;
import com.facebook.common.util.TriState;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Wr  reason: invalid class name and case insensitive filesystem */
public final class C04880Wr implements AnonymousClass02v {
    private static volatile C04880Wr A05;
    private int A00;
    private AnonymousClass0UN A01;
    public final AnonymousClass00M A02;
    public final String A03 = "msgr";
    private final AnonymousClass09P A04;

    public void AWW(String str, String str2) {
        BIm(3, str, str2);
    }

    public void AY3(String str, String str2) {
        BIm(6, str, str2);
    }

    public void CLT(String str, String str2) {
        BIm(2, str, str2);
    }

    public void CMp(String str, String str2) {
        BIm(5, str, str2);
    }

    public void CNm(String str, String str2) {
        A01(str, str2, null);
        BIm(6, str, str2);
    }

    public static final C04880Wr A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C04880Wr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r4.getApplicationInjector();
                        C04910Wu.A00(applicationInjector);
                        A05 = new C04880Wr(applicationInjector);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private void A01(String str, String str2, Throwable th) {
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A01)).Ab9(AnonymousClass1Y3.A5y) != TriState.YES) {
            C97224kO r2 = new C97224kO(AnonymousClass08S.A0P(str, ": ", str2), th);
            AnonymousClass09P r1 = this.A04;
            AnonymousClass06G A022 = AnonymousClass06F.A02(str, str2);
            A022.A03 = r2;
            r1.CGQ(A022.A00());
        }
    }

    public void AWX(String str, String str2, Throwable th) {
        BIm(3, str, AnonymousClass08S.A07(str2, 10, AnonymousClass0D4.A01(th)));
    }

    public void AY4(String str, String str2, Throwable th) {
        BIm(6, str, AnonymousClass08S.A07(str2, 10, AnonymousClass0D4.A01(th)));
    }

    public int Aum() {
        return this.A00;
    }

    public boolean BFj(int i) {
        if (this.A00 <= i) {
            return true;
        }
        return false;
    }

    public void BIm(int i, String str, String str2) {
        String str3 = this.A03;
        if (str3 != null) {
            AnonymousClass00M r1 = this.A02;
            if (r1 == null || r1.A05()) {
                str = AnonymousClass08S.A0P(str3, ".", str);
            } else {
                str = AnonymousClass08S.A0T(str3, "(", r1.A03(), ").", str);
            }
        }
        Log.println(i, str, str2);
    }

    public void CLU(String str, String str2, Throwable th) {
        BIm(2, str, AnonymousClass08S.A07(str2, 10, AnonymousClass0D4.A01(th)));
    }

    public void CMq(String str, String str2, Throwable th) {
        BIm(5, str, AnonymousClass08S.A07(str2, 10, AnonymousClass0D4.A01(th)));
    }

    private C04880Wr(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(1, r3);
        this.A04 = C04750Wa.A01(r3);
        this.A02 = AnonymousClass00M.A00();
    }

    public void CNn(String str, String str2, Throwable th) {
        A01(str, str2, th);
        BIm(6, str, AnonymousClass08S.A07(str2, 10, AnonymousClass0D4.A01(th)));
    }

    public void C9Z(int i) {
        this.A00 = i;
    }
}
