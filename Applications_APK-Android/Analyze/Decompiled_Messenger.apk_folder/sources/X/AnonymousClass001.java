package X;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.facebook.acra.ACRA;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.001  reason: invalid class name */
public abstract class AnonymousClass001 extends AnonymousClass002 implements AnonymousClass004 {
    public static boolean A0h;
    public static final Object A0i = new Object();
    public static final Object A0j = new Object();
    public static volatile boolean A0k;
    public static volatile boolean A0l;
    private static volatile boolean A0m;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C = 0;
    public int A0D = 0;
    public int A0E;
    public int A0F = 0;
    public Bundle A0G;
    public Handler A0H;
    public Class A0I;
    public Class A0J;
    public Class A0K;
    public String A0L;
    public Field A0M;
    public Field A0N;
    public Field A0O;
    public Field A0P;
    public Method A0Q;
    public Method A0R;
    public Method A0S;
    public ArrayList A0T;
    public ArrayList A0U;
    public Random A0V;
    public boolean A0W = false;
    public boolean A0X;
    public boolean A0Y;
    private boolean A0Z;
    public final Object A0a = new Object();
    public final ArrayList A0b = new ArrayList();
    public final ArrayList A0c = new ArrayList();
    public final ArrayList A0d = new ArrayList();
    public volatile boolean A0e;
    public volatile boolean A0f = false;
    public volatile boolean A0g;

    private static void A03(AnonymousClass009 r1, Message message) {
        A0l = true;
        r1.A05();
    }

    public boolean A0E() {
        return false;
    }

    public boolean A0F() {
        return true;
    }

    public abstract Class A0G(Intent intent);

    public void A0H(int i) {
        switch (i) {
            case 4:
                this.A0C++;
                return;
            case 5:
                this.A0F++;
                return;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                this.A0D++;
                return;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                this.A0W = true;
                return;
            default:
                return;
        }
    }

    public void A0I(Intent intent) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        if (r4 != 2003) goto L_0x002b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(X.AnonymousClass001 r8, X.AnonymousClass00A r9, android.os.Handler r10, android.os.Message r11) {
        /*
            boolean r0 = X.AnonymousClass001.A0l
            r7 = 1
            if (r0 != 0) goto L_0x0145
            android.os.Handler r0 = r8.A0H
            r5 = 0
            if (r10 != r0) goto L_0x0144
            int r4 = r11.what
            java.lang.String r3 = "SplashScreenApplication"
            if (r4 == 0) goto L_0x0062
            r0 = 100
            if (r0 > r4) goto L_0x0018
            r0 = 161(0xa1, float:2.26E-43)
            if (r4 <= r0) goto L_0x0062
        L_0x0018:
            r0 = 246(0xf6, float:3.45E-43)
            java.lang.String r6 = "samsung"
            if (r4 == r0) goto L_0x005b
            r0 = 302(0x12e, float:4.23E-43)
            if (r4 == r0) goto L_0x0052
            r0 = 1002(0x3ea, float:1.404E-42)
            r2 = 0
            if (r4 == r0) goto L_0x0042
            r0 = 2003(0x7d3, float:2.807E-42)
            if (r4 == r0) goto L_0x005b
        L_0x002b:
            if (r2 != 0) goto L_0x0062
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)
            java.lang.Object[] r1 = new java.lang.Object[]{r0, r11}
            java.lang.String r0 = "stopping early-init message pump: unexpected message (what=%s) %s"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            android.util.Log.w(r3, r0)
            A03(r9, r11)
            return r7
        L_0x0042:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 < r0) goto L_0x002b
            java.lang.String r0 = android.os.Build.MANUFACTURER
            boolean r0 = r6.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x002b
            r2 = 1
            goto L_0x002b
        L_0x0052:
            java.lang.String r1 = android.os.Build.MANUFACTURER
            java.lang.String r0 = "vivo"
            boolean r2 = r0.equalsIgnoreCase(r1)
            goto L_0x002b
        L_0x005b:
            java.lang.String r0 = android.os.Build.MANUFACTURER
            boolean r2 = r6.equalsIgnoreCase(r0)
            goto L_0x002b
        L_0x0062:
            int r0 = r8.A05
            if (r4 != r0) goto L_0x0118
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            r0 = 28
            r4 = 3
            java.lang.String r2 = "null"
            if (r1 < r0) goto L_0x00cd
            java.lang.Class r1 = r8.A0J     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object r0 = r11.obj     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            boolean r0 = r1.isInstance(r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 != 0) goto L_0x008f
            java.lang.String r1 = "stopping early-init message pump: EXECUTE_TRANSACTION with unexpected CR %s %s"
            java.lang.Object r0 = r11.obj     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 == 0) goto L_0x0083
            java.lang.Class r2 = r0.getClass()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
        L_0x0083:
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r0}     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            android.util.Log.w(r3, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            goto L_0x00ec
        L_0x008f:
            java.lang.reflect.Method r2 = r8.A0S     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object r1 = r11.obj     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object r0 = r2.invoke(r1, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
        L_0x009f:
            boolean r0 = r2.hasNext()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 == 0) goto L_0x0118
            java.lang.Object r1 = r2.next()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Class r0 = r8.A0K     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            boolean r0 = r0.isInstance(r1)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 == 0) goto L_0x009f
            java.lang.reflect.Field r0 = r8.A0P     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            android.content.pm.ActivityInfo r0 = (android.content.pm.ActivityInfo) r0     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 == 0) goto L_0x009f
            int r0 = r0.launchMode     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 != r4) goto L_0x009f
            java.lang.String r1 = "stopping early-init message pump: EXECUTE_TRANSACTION with LaunchActivityItem for LAUNCH_SINGLE_INSTANCE: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r11}     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            android.util.Log.w(r3, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            goto L_0x00ee
        L_0x00cd:
            java.lang.Class r1 = r8.A0I     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object r0 = r11.obj     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            boolean r0 = r1.isInstance(r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 != 0) goto L_0x00f2
            java.lang.String r1 = "stopping early-init message pump: LAUNCH_ACTIVITY with unexpected ACR %s %s"
            java.lang.Object r0 = r11.obj     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 == 0) goto L_0x00e1
            java.lang.Class r2 = r0.getClass()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
        L_0x00e1:
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r0}     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            android.util.Log.w(r3, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
        L_0x00ec:
            r8.A0Y = r5     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
        L_0x00ee:
            A03(r9, r11)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            goto L_0x0110
        L_0x00f2:
            java.lang.reflect.Field r1 = r8.A0M     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object r0 = r11.obj     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            android.content.pm.ActivityInfo r0 = (android.content.pm.ActivityInfo) r0     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 == 0) goto L_0x0118
            int r0 = r0.launchMode     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            if (r0 != r4) goto L_0x0118
            java.lang.String r1 = "stopping early-init message pump: LAUNCH_ACTIVITY for LAUNCH_SINGLE_INSTANCE: %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r11}     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            android.util.Log.w(r3, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0111 }
            goto L_0x00ee
        L_0x0110:
            return r7
        L_0x0111:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r1)
            throw r0
        L_0x0118:
            int r1 = r11.what
            int r0 = r8.A00
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A01
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A02
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A03
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A04
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A06
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A08
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A09
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A0A
            if (r1 == r0) goto L_0x0145
            int r0 = r8.A0B
            if (r1 == r0) goto L_0x0145
            r7 = 0
            return r7
        L_0x0144:
            return r5
        L_0x0145:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass001.A04(X.001, X.00A, android.os.Handler, android.os.Message):boolean");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01fb, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:?, code lost:
        r0 = new java.lang.AssertionError(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0220, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0228, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:119:0x01f3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:84:0x0159 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0C() {
        /*
            r30 = this;
            boolean r0 = X.AnonymousClass001.A0m
            r9 = 1
            r10 = r30
            if (r0 != 0) goto L_0x0015
            r10.A07()
            com.facebook.base.app.ApplicationLike r0 = r10.A00
            r0.A02()
            r10.A0Z = r9
            r10.A02()
            return
        L_0x0015:
            X.00A r5 = new X.00A
            r5.<init>(r10)
            X.00R r4 = new X.00R
            java.lang.String r0 = "CallOnCreateImpl"
            r4.<init>(r10, r0, r5)
            r4.start()
            r1 = -452435011(0xffffffffe50863bd, float:-4.0255106E22)
            r2 = 32
            java.lang.String r0 = "Waiting for onCreate"
            X.AnonymousClass00C.A01(r2, r0, r1)
            r5.A04()     // Catch:{ all -> 0x0293 }
            r4.join()     // Catch:{ InterruptedException -> 0x028c }
            r0 = -1812034464(0xffffffff93fe8c60, float:-6.4257035E-27)
            X.AnonymousClass00C.A00(r2, r0)
            java.util.ArrayList r0 = r10.A0U
            if (r0 == 0) goto L_0x0055
            java.util.Iterator r1 = r0.iterator()
        L_0x0042:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0052
            java.lang.Object r0 = r1.next()
            android.os.Message r0 = (android.os.Message) r0
            X.AnonymousClass009.A01(r0)
            goto L_0x0042
        L_0x0052:
            r0 = 0
            r10.A0U = r0
        L_0x0055:
            java.lang.Object r1 = X.AnonymousClass001.A0j
            monitor-enter(r1)
            r1.notifyAll()     // Catch:{ all -> 0x0289 }
            r8 = 0
            X.AnonymousClass001.A0h = r8     // Catch:{ all -> 0x0289 }
            monitor-exit(r1)     // Catch:{ all -> 0x0289 }
            r10.A0Z = r9
            r10.A0Y = r8
            X.AnonymousClass001.A0k = r9
            java.lang.String r1 = "SplashScreenApplication.finishActivityCreations"
            r0 = -966182471(0xffffffffc66939b9, float:-14926.431)
            X.AnonymousClass00C.A01(r2, r1, r0)
            java.util.ArrayList r11 = r10.A0d     // Catch:{ all -> 0x027f }
        L_0x006f:
            boolean r0 = r11.isEmpty()     // Catch:{ all -> 0x027f }
            if (r0 != 0) goto L_0x0247
            java.lang.Object r7 = r11.get(r8)     // Catch:{ all -> 0x027f }
            com.facebook.base.app.SplashScreenApplication$RedirectHackActivity r7 = (com.facebook.base.app.SplashScreenApplication$RedirectHackActivity) r7     // Catch:{ all -> 0x027f }
            X.09j r0 = new X.09j     // Catch:{ all -> 0x027f }
            r0.<init>(r10)     // Catch:{ all -> 0x027f }
            r0.A04()     // Catch:{ all -> 0x027f }
            r5 = 0
            boolean r0 = r7.isFinishing()     // Catch:{ all -> 0x027f }
            if (r0 != 0) goto L_0x01c5
            boolean r0 = r7.A03     // Catch:{ all -> 0x027f }
            if (r0 != 0) goto L_0x01c5
            boolean r6 = r10.A0X     // Catch:{ all -> 0x027f }
            r10.A0X = r9     // Catch:{ all -> 0x0241 }
            int r0 = r10.A0E     // Catch:{ all -> 0x0241 }
            if (r0 != 0) goto L_0x0231
            java.util.ArrayList r0 = r10.A0c     // Catch:{ all -> 0x0241 }
            java.util.Iterator r14 = r0.iterator()     // Catch:{ all -> 0x0241 }
        L_0x009c:
            boolean r0 = r14.hasNext()     // Catch:{ all -> 0x0241 }
            if (r0 == 0) goto L_0x00b9
            java.lang.Object r13 = r14.next()     // Catch:{ all -> 0x0241 }
            com.facebook.base.app.SplashScreenActivity r13 = (com.facebook.base.app.SplashScreenActivity) r13     // Catch:{ all -> 0x0241 }
            long r4 = r13.A01     // Catch:{ all -> 0x0241 }
            long r0 = r7.A01     // Catch:{ all -> 0x0241 }
            int r12 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r12 != 0) goto L_0x009c
            int r1 = r13.A00     // Catch:{ all -> 0x0241 }
            int r0 = r10.A0E     // Catch:{ all -> 0x0241 }
            if (r1 <= r0) goto L_0x009c
            r10.A0E = r1     // Catch:{ all -> 0x0241 }
            goto L_0x009c
        L_0x00b9:
            boolean r0 = r7.A04     // Catch:{ all -> 0x0241 }
            r19 = r0
            int r13 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0241 }
            r5 = 0
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r8)     // Catch:{ all -> 0x0241 }
            r0 = 26
            if (r13 < r0) goto L_0x00f2
            r0 = 27
            if (r13 > r0) goto L_0x00f2
            java.lang.reflect.Method r4 = r10.A0R     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x01f3 }
            android.app.ActivityThread r0 = android.app.ActivityThread.currentActivityThread()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x01f3 }
            java.lang.reflect.Field r14 = r10.A0O     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x01f3 }
            java.lang.Object r21 = r14.get(r7)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x01f3 }
            r23 = r5
            java.lang.Integer r24 = java.lang.Integer.valueOf(r8)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x01f3 }
            r26 = r5
            r27 = r5
            r28 = r12
            r29 = r12
            r22 = r5
            r25 = r12
            java.lang.Object[] r14 = new java.lang.Object[]{r21, r22, r23, r24, r25, r26, r27, r28, r29}     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x01f3 }
            r4.invoke(r0, r14)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x01f3 }
            goto L_0x00f5
        L_0x00f2:
            r7.recreate()     // Catch:{ all -> 0x0241 }
        L_0x00f5:
            X.00F r14 = new X.00F     // Catch:{ all -> 0x0241 }
            r14.<init>(r10, r7)     // Catch:{ all -> 0x0241 }
            boolean r0 = X.AnonymousClass009.A08     // Catch:{ all -> 0x0241 }
            if (r0 == 0) goto L_0x0229
            android.os.MessageQueue r4 = android.os.Looper.myQueue()     // Catch:{ all -> 0x0241 }
            monitor-enter(r4)     // Catch:{ all -> 0x0241 }
            java.lang.reflect.Field r0 = X.AnonymousClass009.A03     // Catch:{ IllegalAccessException -> 0x0212 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ IllegalAccessException -> 0x0212 }
            android.os.Message r0 = (android.os.Message) r0     // Catch:{ IllegalAccessException -> 0x0212 }
            if (r0 == 0) goto L_0x0146
            r18 = 0
            r15 = r18
        L_0x0111:
            java.lang.reflect.Field r1 = X.AnonymousClass009.A05     // Catch:{ IllegalAccessException -> 0x0209 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ IllegalAccessException -> 0x0209 }
            android.os.Message r1 = (android.os.Message) r1     // Catch:{ IllegalAccessException -> 0x0209 }
            int r17 = r14.A00(r0)     // Catch:{ all -> 0x0220 }
            r16 = r17 & 2
            if (r16 == 0) goto L_0x013d
            X.AnonymousClass009.A02(r0)     // Catch:{ all -> 0x0220 }
            java.lang.reflect.Field r16 = X.AnonymousClass009.A05     // Catch:{ IllegalAccessException -> 0x0202 }
            r21 = r16
            r22 = r0
            r23 = r18
            r21.set(r22, r23)     // Catch:{ IllegalAccessException -> 0x0202 }
            if (r15 != 0) goto L_0x0137
            java.lang.reflect.Field r0 = X.AnonymousClass009.A03     // Catch:{ IllegalAccessException -> 0x0219 }
            r0.set(r4, r1)     // Catch:{ IllegalAccessException -> 0x0219 }
            goto L_0x013e
        L_0x0137:
            java.lang.reflect.Field r0 = X.AnonymousClass009.A05     // Catch:{ IllegalAccessException -> 0x01fb }
            r0.set(r15, r1)     // Catch:{ IllegalAccessException -> 0x01fb }
            goto L_0x013e
        L_0x013d:
            r15 = r0
        L_0x013e:
            r0 = r17 & 1
            if (r0 != 0) goto L_0x0146
            if (r1 == 0) goto L_0x0146
            r0 = r1
            goto L_0x0111
        L_0x0146:
            monitor-exit(r4)     // Catch:{ all -> 0x0220 }
            android.os.Message r0 = r14.A00     // Catch:{ all -> 0x0241 }
            if (r0 == 0) goto L_0x0239
            X.AnonymousClass009.A01(r0)     // Catch:{ all -> 0x0241 }
            android.app.ActivityThread r1 = android.app.ActivityThread.currentActivityThread()     // Catch:{ NullPointerException -> 0x0159 }
            android.os.IBinder r0 = r14.A01     // Catch:{ NullPointerException -> 0x0159 }
            android.app.Activity r5 = r1.getActivity(r0)     // Catch:{ NullPointerException -> 0x0159 }
            goto L_0x016e
        L_0x0159:
            long r0 = r7.A01     // Catch:{ all -> 0x0241 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0241 }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x0241 }
            java.lang.String r0 = "new activity not found?! rhaId:%x"
            java.lang.String r1 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0241 }
            java.lang.String r0 = "SplashScreenApplication"
            android.util.Log.w(r0, r1)     // Catch:{ all -> 0x0241 }
        L_0x016e:
            if (r19 == 0) goto L_0x019a
            if (r5 == 0) goto L_0x019a
            boolean r0 = r5.isFinishing()     // Catch:{ all -> 0x0241 }
            if (r0 != 0) goto L_0x019a
            r0 = 24
            if (r13 >= r0) goto L_0x0189
            java.lang.reflect.Method r4 = r10.A0Q     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
            android.app.ActivityThread r1 = android.app.ActivityThread.currentActivityThread()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
            android.os.IBinder r0 = r14.A01     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
            java.lang.Object[] r0 = new java.lang.Object[]{r0, r12}     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
            goto L_0x0197
        L_0x0189:
            java.lang.reflect.Method r4 = r10.A0Q     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
            android.app.ActivityThread r1 = android.app.ActivityThread.currentActivityThread()     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
            android.os.IBinder r13 = r14.A01     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
            java.lang.String r0 = "nonodex-recreateSafelyAndSynchronously"
            java.lang.Object[] r0 = new java.lang.Object[]{r13, r12, r0}     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
        L_0x0197:
            r4.invoke(r1, r0)     // Catch:{ IllegalAccessException | InvocationTargetException -> 0x0210 }
        L_0x019a:
            int r0 = r10.A0E     // Catch:{ all -> 0x0241 }
            if (r0 <= 0) goto L_0x01c1
            boolean r0 = r5 instanceof X.AnonymousClass00G     // Catch:{ all -> 0x0241 }
            if (r0 == 0) goto L_0x01c1
            boolean r0 = r5.isFinishing()     // Catch:{ all -> 0x0241 }
            if (r0 != 0) goto L_0x01c1
            r14 = r5
            X.00G r14 = (X.AnonymousClass00G) r14     // Catch:{ all -> 0x0241 }
            int r13 = r10.A0E     // Catch:{ all -> 0x0241 }
            int r12 = r10.A0C     // Catch:{ all -> 0x0241 }
            int r4 = r10.A0F     // Catch:{ all -> 0x0241 }
            int r1 = r10.A0D     // Catch:{ all -> 0x0241 }
            boolean r0 = r10.A0W     // Catch:{ all -> 0x0241 }
            r15 = r13
            r16 = r12
            r17 = r4
            r18 = r1
            r19 = r0
            r14.BpD(r15, r16, r17, r18, r19)     // Catch:{ all -> 0x0241 }
        L_0x01c1:
            r10.A0E = r8     // Catch:{ all -> 0x027f }
            r10.A0X = r6     // Catch:{ all -> 0x027f }
        L_0x01c5:
            r11.remove(r7)     // Catch:{ all -> 0x027f }
            if (r5 == 0) goto L_0x006f
            java.util.ArrayList r7 = r7.A02     // Catch:{ all -> 0x027f }
            if (r7 == 0) goto L_0x01d3
            int r6 = r7.size()     // Catch:{ all -> 0x027f }
            goto L_0x01d4
        L_0x01d3:
            r6 = 0
        L_0x01d4:
            if (r6 <= 0) goto L_0x006f
            android.app.ActivityThread r0 = android.app.ActivityThread.currentActivityThread()     // Catch:{ all -> 0x027f }
            android.app.Instrumentation r4 = r0.getInstrumentation()     // Catch:{ all -> 0x027f }
            r1 = 0
        L_0x01df:
            if (r1 >= r6) goto L_0x006f
            boolean r0 = r5.isFinishing()     // Catch:{ all -> 0x027f }
            if (r0 != 0) goto L_0x01f0
            java.lang.Object r0 = r7.get(r1)     // Catch:{ all -> 0x027f }
            android.content.Intent r0 = (android.content.Intent) r0     // Catch:{ all -> 0x027f }
            r4.callActivityOnNewIntent(r5, r0)     // Catch:{ all -> 0x027f }
        L_0x01f0:
            int r1 = r1 + 1
            goto L_0x01df
        L_0x01f3:
            java.lang.AssertionError r1 = new java.lang.AssertionError     // Catch:{ all -> 0x0241 }
            java.lang.String r0 = "should have bailed out of nonodex"
            r1.<init>(r0)     // Catch:{ all -> 0x0241 }
            goto L_0x0240
        L_0x01fb:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0220 }
            r0.<init>(r1)     // Catch:{ all -> 0x0220 }
            goto L_0x021f
        L_0x0202:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0220 }
            r0.<init>(r1)     // Catch:{ all -> 0x0220 }
            goto L_0x021f
        L_0x0209:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0220 }
            r0.<init>(r1)     // Catch:{ all -> 0x0220 }
            goto L_0x021f
        L_0x0210:
            r1 = move-exception
            goto L_0x0223
        L_0x0212:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0220 }
            r0.<init>(r1)     // Catch:{ all -> 0x0220 }
            goto L_0x021f
        L_0x0219:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0220 }
            r0.<init>(r1)     // Catch:{ all -> 0x0220 }
        L_0x021f:
            throw r0     // Catch:{ all -> 0x0220 }
        L_0x0220:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0220 }
            goto L_0x0228
        L_0x0223:
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0241 }
            r0.<init>(r1)     // Catch:{ all -> 0x0241 }
        L_0x0228:
            throw r0     // Catch:{ all -> 0x0241 }
        L_0x0229:
            java.lang.AssertionError r1 = new java.lang.AssertionError     // Catch:{ all -> 0x0241 }
            java.lang.String r0 = "init not called"
            r1.<init>(r0)     // Catch:{ all -> 0x0241 }
            goto L_0x0240
        L_0x0231:
            java.lang.AssertionError r1 = new java.lang.AssertionError     // Catch:{ all -> 0x0241 }
            java.lang.String r0 = "previous splash screen setup was not cleaned up"
            r1.<init>(r0)     // Catch:{ all -> 0x0241 }
            goto L_0x0240
        L_0x0239:
            java.lang.AssertionError r1 = new java.lang.AssertionError     // Catch:{ all -> 0x0241 }
            java.lang.String r0 = "should have found RELAUNCH_ACTIVITY message after Activity.recreate()"
            r1.<init>(r0)     // Catch:{ all -> 0x0241 }
        L_0x0240:
            throw r1     // Catch:{ all -> 0x0241 }
        L_0x0241:
            r0 = move-exception
            r10.A0E = r8     // Catch:{ all -> 0x027f }
            r10.A0X = r6     // Catch:{ all -> 0x027f }
            throw r0     // Catch:{ all -> 0x027f }
        L_0x0247:
            java.util.ArrayList r0 = r10.A0c     // Catch:{ all -> 0x027f }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x027f }
        L_0x024d:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x027f }
            if (r0 == 0) goto L_0x0263
            java.lang.Object r4 = r5.next()     // Catch:{ all -> 0x027f }
            com.facebook.base.app.SplashScreenActivity r4 = (com.facebook.base.app.SplashScreenActivity) r4     // Catch:{ all -> 0x027f }
            r4.finish()     // Catch:{ all -> 0x027f }
            r0 = 17432577(0x10a0001, float:2.53466E-38)
            r4.overridePendingTransition(r8, r0)     // Catch:{ all -> 0x027f }
            goto L_0x024d
        L_0x0263:
            r0 = -498844607(0xffffffffe2443c41, float:-9.049759E20)
            X.AnonymousClass00C.A00(r2, r0)
            X.AnonymousClass001.A0k = r8
            java.util.ArrayList r0 = r10.A0d
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0277
            r10.A02()
            return
        L_0x0277:
            java.lang.AssertionError r1 = new java.lang.AssertionError
            java.lang.String r0 = "should have recreated everything"
            r1.<init>(r0)
            throw r1
        L_0x027f:
            r1 = move-exception
            r0 = -1722741001(0xffffffff99510ef7, float:-1.0808069E-23)
            X.AnonymousClass00C.A00(r2, r0)
            X.AnonymousClass001.A0k = r8
            throw r1
        L_0x0289:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0289 }
            throw r0
        L_0x028c:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0293 }
            r0.<init>(r1)     // Catch:{ all -> 0x0293 }
            throw r0     // Catch:{ all -> 0x0293 }
        L_0x0293:
            r1 = move-exception
            r0 = 1779177062(0x6a0c1666, float:4.2338847E25)
            X.AnonymousClass00C.A00(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass001.A0C():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0402, code lost:
        if (r12.invoke(r5, r9) != null) goto L_0x0404;
     */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x045f  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0470  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0474  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x04a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D() {
        /*
            r28 = this;
            r10 = r28
            super.A0D()
            java.lang.Class<android.os.Message> r5 = android.os.Message.class
            java.lang.String r0 = "flags"
            java.lang.reflect.Field r0 = r5.getDeclaredField(r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            X.AnonymousClass009.A04 = r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r4 = 1
            r0.setAccessible(r4)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.String r0 = "target"
            java.lang.reflect.Field r0 = r5.getDeclaredField(r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            X.AnonymousClass009.A06 = r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r0.setAccessible(r4)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.String r3 = "next"
            java.lang.reflect.Field r0 = r5.getDeclaredField(r3)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            X.AnonymousClass009.A05 = r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r0.setAccessible(r4)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r2 = 0
            java.lang.Class[] r1 = new java.lang.Class[r2]     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.String r0 = "markInUse"
            java.lang.reflect.Method r0 = r5.getDeclaredMethod(r0, r1)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r0.setAccessible(r4)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.Class<android.os.MessageQueue> r1 = android.os.MessageQueue.class
            java.lang.Class[] r0 = new java.lang.Class[r2]     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r3, r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            X.AnonymousClass009.A07 = r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r0.setAccessible(r4)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.String r0 = "mMessages"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            X.AnonymousClass009.A03 = r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r0.setAccessible(r4)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            X.AnonymousClass009.A08 = r4     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            android.os.Handler r0 = X.AnonymousClass00P.A00     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            if (r0 != 0) goto L_0x0071
            android.app.ActivityThread r4 = android.app.ActivityThread.currentActivityThread()     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.Class<android.app.ActivityThread> r3 = android.app.ActivityThread.class
            java.lang.Class[] r1 = new java.lang.Class[r2]     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.String r0 = "getHandler"
            java.lang.reflect.Method r1 = r3.getDeclaredMethod(r0, r1)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            r0 = 1
            r1.setAccessible(r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.Object r0 = r1.invoke(r4, r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            android.os.Handler r0 = (android.os.Handler) r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            if (r0 == 0) goto L_0x0074
            X.AnonymousClass00P.A00 = r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
        L_0x0071:
            r10.A0H = r0     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            goto L_0x007c
        L_0x0074:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            java.lang.String r0 = "main handler unexpectedly null"
            r1.<init>(r0)     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
            throw r1     // Catch:{ IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x007c }
        L_0x007c:
            r10.getBaseContext()
            android.app.ActivityThread r7 = android.app.ActivityThread.currentActivityThread()
            android.app.Instrumentation r11 = r7.getInstrumentation()
            java.lang.String r6 = "SplashScreenApplication"
            if (r11 != 0) goto L_0x009e
            java.lang.String r0 = "not using instrumented startup: ActivityThread has no instrumentation"
            android.util.Log.w(r6, r0)
            r1 = 0
        L_0x0091:
            android.os.Handler r0 = r10.A0H
            if (r0 == 0) goto L_0x0469
            if (r1 == 0) goto L_0x0469
            r27 = r10
            java.lang.String r17 = "SplashScreenApplication"
            r0 = 0
            goto L_0x015e
        L_0x009e:
            java.lang.String r1 = r7.getProcessName()
            if (r1 == 0) goto L_0x00af
            r0 = 58
            int r1 = r1.indexOf(r0)
            r0 = -1
            if (r1 == r0) goto L_0x00af
        L_0x00ad:
            r1 = 0
            goto L_0x0091
        L_0x00af:
            java.lang.Class<android.app.ActivityThread> r9 = android.app.ActivityThread.class
            java.lang.String r1 = "mBoundApplication"
            java.lang.reflect.Field r2 = r9.getDeclaredField(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r0 = 1
            r2.setAccessible(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.Class r0 = r11.getClass()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.Class<android.app.Instrumentation> r3 = android.app.Instrumentation.class
            r8 = 1
            if (r0 == r3) goto L_0x010b
            java.lang.String r1 = r0.getName()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.String r0 = "io.selendroid.server.LightweightInstrumentation"
            boolean r0 = r1.equals(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            if (r0 == 0) goto L_0x0101
            java.lang.Object r2 = r2.get(r7)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.String r0 = "android.app.ActivityThread$AppBindData"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.String r0 = "instrumentationArgs"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r1.setAccessible(r8)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.Object r2 = r1.get(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            android.os.Bundle r2 = (android.os.Bundle) r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.String r1 = "yes"
            java.lang.String r0 = "want_splash"
            java.lang.String r0 = r2.getString(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            boolean r0 = r1.equals(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            if (r0 != 0) goto L_0x00fd
            java.lang.String r0 = "not using instrumented startup: selendroid test does not want splash screen"
            android.util.Log.w(r6, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            goto L_0x00ad
        L_0x00fd:
            r10.A0G = r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r2 = 1
            goto L_0x010c
        L_0x0101:
            java.lang.String r0 = "not using instrumented startup: custom unknown Instrumentation in place: "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            android.util.Log.w(r6, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            goto L_0x00ad
        L_0x010b:
            r2 = 0
        L_0x010c:
            java.lang.Class r1 = r3.getSuperclass()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.Class<java.lang.Object> r0 = java.lang.Object.class
            if (r1 == r0) goto L_0x0126
            java.lang.String r1 = "not using instrumented startup: Instrumentation has unknown superclass "
            java.lang.Class r0 = r3.getSuperclass()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.String r0 = r0.getName()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            android.util.Log.w(r6, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            goto L_0x00ad
        L_0x0126:
            r1 = 0
            if (r2 == 0) goto L_0x012a
            r1 = r11
        L_0x012a:
            X.00Q r5 = new X.00Q     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r5.<init>(r10, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.reflect.Field[] r4 = r3.getDeclaredFields()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            int r3 = r4.length     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r2 = 0
        L_0x0135:
            if (r2 >= r3) goto L_0x0146
            r1 = r4[r2]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r1.setAccessible(r8)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            java.lang.Object r0 = r1.get(r11)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r1.set(r5, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            int r2 = r2 + 1
            goto L_0x0135
        L_0x0146:
            java.lang.String r0 = "mInstrumentation"
            java.lang.reflect.Field r1 = r9.getDeclaredField(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r1.setAccessible(r8)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r1.set(r7, r5)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException -> 0x0155 }
            r1 = 1
            goto L_0x0091
        L_0x0155:
            r1 = move-exception
            java.lang.String r0 = "failed installing custom Instrumentation"
            android.util.Log.w(r6, r0, r1)
            r1 = 0
            goto L_0x0091
        L_0x015e:
            android.app.ActivityThread r9 = android.app.ActivityThread.currentActivityThread()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r3 = r9.getProcessName()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r2 = -1
            if (r3 == 0) goto L_0x0173
            r1 = 58
            int r1 = r3.indexOf(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            if (r1 == r2) goto L_0x0173
            goto L_0x0467
        L_0x0173:
            java.lang.String r1 = "android.app.ActivityThread$ActivityClientRecord"
            java.lang.Class r3 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0I = r3     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r1 = "token"
            java.lang.reflect.Field r2 = r3.getDeclaredField(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r1 = 1
            r2.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0N = r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r1 = "activityInfo"
            java.lang.reflect.Field r2 = r3.getDeclaredField(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r1 = 1
            r2.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0M = r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.Class<android.app.Activity> r2 = android.app.Activity.class
            java.lang.String r1 = "mToken"
            java.lang.reflect.Field r2 = r2.getDeclaredField(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r1 = 1
            r2.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0O = r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r1 = 24
            java.lang.String r5 = "performStopActivity"
            if (r2 >= r1) goto L_0x01be
            java.lang.Class<android.app.ActivityThread> r3 = android.app.ActivityThread.class
            java.lang.Class<android.os.IBinder> r2 = android.os.IBinder.class
            java.lang.Class r1 = java.lang.Boolean.TYPE     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.Class[] r1 = new java.lang.Class[]{r2, r1}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.reflect.Method r2 = r3.getDeclaredMethod(r5, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r1 = 1
            r2.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r10.A0Q = r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            goto L_0x01d4
        L_0x01be:
            java.lang.Class<android.app.ActivityThread> r4 = android.app.ActivityThread.class
            java.lang.Class<android.os.IBinder> r3 = android.os.IBinder.class
            java.lang.Class r2 = java.lang.Boolean.TYPE     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            java.lang.Class[] r1 = new java.lang.Class[]{r3, r2, r1}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.reflect.Method r2 = r4.getDeclaredMethod(r5, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r1 = 1
            r2.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0Q = r2     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
        L_0x01d4:
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r2 = 28
            r1 = 26
            if (r3 < r2) goto L_0x0207
            java.lang.String r2 = "android.app.servertransaction.LaunchActivityItem"
            java.lang.Class r3 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r10.A0K = r3     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.String r2 = "mInfo"
            java.lang.reflect.Field r3 = r3.getDeclaredField(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r2 = 1
            r3.setAccessible(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r10.A0P = r3     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.String r2 = "android.app.servertransaction.ClientTransaction"
            java.lang.Class r4 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r10.A0J = r4     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.String r3 = "getCallbacks"
            java.lang.Class[] r2 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.reflect.Method r3 = r4.getDeclaredMethod(r3, r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r2 = 1
            r3.setAccessible(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r10.A0S = r3     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            goto L_0x022d
        L_0x0207:
            if (r3 < r1) goto L_0x022d
            java.lang.Class<android.app.ActivityThread> r4 = android.app.ActivityThread.class
            java.lang.String r3 = "requestRelaunchActivity"
            java.lang.Class<android.os.IBinder> r18 = android.os.IBinder.class
            java.lang.Class<java.util.List> r19 = java.util.List.class
            r20 = r19
            java.lang.Class r21 = java.lang.Integer.TYPE     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.Class r25 = java.lang.Boolean.TYPE     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r22 = r25
            java.lang.Class<android.content.res.Configuration> r23 = android.content.res.Configuration.class
            r24 = r23
            r26 = r25
            java.lang.Class[] r2 = new java.lang.Class[]{r18, r19, r20, r21, r22, r23, r24, r25, r26}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.reflect.Method r3 = r4.getDeclaredMethod(r3, r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r2 = 1
            r3.setAccessible(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r10.A0R = r3     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
        L_0x022d:
            java.lang.Class<android.app.ActivityThread> r11 = android.app.ActivityThread.class
            java.lang.String r3 = "mBoundApplication"
            java.lang.reflect.Field r4 = r11.getDeclaredField(r3)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r2 = 1
            r4.setAccessible(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r2 = "mInitialApplication"
            java.lang.reflect.Field r16 = r11.getDeclaredField(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r3 = 1
            r2 = r16
            r2.setAccessible(r3)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r2 = "mAllApplications"
            java.lang.reflect.Field r3 = r11.getDeclaredField(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r2 = 1
            r3.setAccessible(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r2 = "android.app.ActivityThread$AppBindData"
            java.lang.Class r5 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r2 = "info"
            java.lang.reflect.Field r2 = r5.getDeclaredField(r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r5 = 1
            r2.setAccessible(r5)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r5 = "android.app.LoadedApk"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r6 = "mApplication"
            java.lang.reflect.Field r15 = r5.getDeclaredField(r6)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r6 = 1
            r15.setAccessible(r6)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r6 = "android.app.ContextImpl"
            java.lang.Class r8 = java.lang.Class.forName(r6)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r7 = "setOuterContext"
            java.lang.Class<android.content.Context> r6 = android.content.Context.class
            java.lang.Class[] r6 = new java.lang.Class[]{r6}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.reflect.Method r14 = r8.getDeclaredMethod(r7, r6)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r6 = 1
            r14.setAccessible(r6)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.Class<android.app.Application> r7 = android.app.Application.class
            java.lang.String r6 = "mLoadedApk"
            java.lang.reflect.Field r13 = r7.getDeclaredField(r6)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r6 = 1
            r13.setAccessible(r6)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r6 = 21
            r12 = 0
            if (r7 < r6) goto L_0x0303
            java.lang.String r6 = "getResources"
            if (r7 < r1) goto L_0x02a7
            java.lang.Class[] r1 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.reflect.Method r12 = r5.getDeclaredMethod(r6, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r1 = 1
            r12.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            goto L_0x02b3
        L_0x02a7:
            java.lang.Class[] r1 = new java.lang.Class[]{r11}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.reflect.Method r12 = r5.getDeclaredMethod(r6, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            r1 = 1
            r12.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
        L_0x02b3:
            java.lang.String r6 = "getAssets"
            r1 = 26
            if (r7 < r1) goto L_0x02c7
            java.lang.Class[] r1 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            java.lang.reflect.Method r11 = r5.getDeclaredMethod(r6, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            r1 = 1
            r11.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x02c4 }
            goto L_0x02d3
        L_0x02c4:
            r2 = move-exception
            goto L_0x045d
        L_0x02c7:
            java.lang.Class[] r1 = new java.lang.Class[]{r11}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.reflect.Method r11 = r5.getDeclaredMethod(r6, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            r1 = 1
            r11.setAccessible(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
        L_0x02d3:
            java.lang.String r1 = "getClassLoader"
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.reflect.Method r8 = r5.getDeclaredMethod(r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            r0 = 1
            r8.setAccessible(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.String r7 = "rewriteRValues"
            java.lang.Class<java.lang.ClassLoader> r6 = java.lang.ClassLoader.class
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.Class[] r0 = new java.lang.Class[]{r6, r1, r0}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.reflect.Method r7 = r5.getDeclaredMethod(r7, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            r0 = 1
            r7.setAccessible(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.Class<android.content.res.AssetManager> r5 = android.content.res.AssetManager.class
            java.lang.String r1 = "getAssignedPackageIdentifiers"
            r0 = 0
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            java.lang.reflect.Method r6 = r5.getDeclaredMethod(r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            r0 = 1
            r6.setAccessible(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x045b }
            goto L_0x0307
        L_0x0303:
            r11 = r12
            r6 = r12
            r7 = r12
            r8 = r12
        L_0x0307:
            java.lang.Object r0 = r4.get(r9)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.Object r5 = r2.get(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.Object r0 = r3.get(r9)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0T = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "android.app.ActivityThread$H"
            java.lang.Class r4 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "BIND_SERVICE"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A00 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "CREATE_SERVICE"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A01 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r1 = "DUMP_PROVIDER"
            r0 = -1
            int r0 = A01(r4, r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A02 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "EXIT_APPLICATION"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A03 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r1 = "INSTALL_PROVIDER"
            r0 = -1
            int r0 = A01(r4, r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A04 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r3 = "DISPATCH_PACKAGE_BROADCAST"
            java.lang.String r2 = "RELAUNCH_ACTIVITY"
            r0 = 28
            if (r1 < r0) goto L_0x0369
            java.lang.String r1 = "EXECUTE_TRANSACTION"
            r0 = 159(0x9f, float:2.23E-43)
            int r0 = A01(r4, r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A05 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r0 = 160(0xa0, float:2.24E-43)
            int r0 = A01(r4, r2, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A07 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r0 = 133(0x85, float:1.86E-43)
            A01(r4, r3, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            goto L_0x037a
        L_0x0369:
            java.lang.String r0 = "LAUNCH_ACTIVITY"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A05 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            int r0 = A00(r4, r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A07 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            A00(r4, r3)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
        L_0x037a:
            java.lang.String r0 = "RECEIVER"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A06 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "REMOVE_PROVIDER"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A08 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "SERVICE_ARGS"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A09 = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "STOP_SERVICE"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0A = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "UNBIND_SERVICE"
            int r0 = A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r10.A0B = r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            java.lang.String r0 = "GC_WHEN_IDLE"
            A00(r4, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            r13.set(r10, r5)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0458 }
            android.os.Bundle r1 = r10.A0G     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            if (r1 == 0) goto L_0x03d3
            java.lang.String r0 = "main_activity"
            java.lang.String r2 = r1.getString(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            android.os.Bundle r1 = r10.A0G     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r1.remove(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r1.<init>()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r1.setClassName(r10, r2)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r0 = 872546304(0x34020000, float:1.2107193E-7)
            r1.addFlags(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.String r0 = "android.intent.action.MAIN"
            r1.setAction(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.String r0 = "android.intent.category.LAUNCHER"
            r1.addCategory(r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r10.startActivity(r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
        L_0x03d3:
            android.content.Context r1 = r27.getBaseContext()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r2 = 0
            java.lang.Object[] r0 = new java.lang.Object[]{r10}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r14.invoke(r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.util.ArrayList r0 = r10.A0T     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r0.add(r10)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r15.set(r5, r10)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r0 = 21
            if (r1 < r0) goto L_0x044e
            r0 = 26
            if (r1 < r0) goto L_0x03fa
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object r0 = r12.invoke(r5, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            if (r0 == 0) goto L_0x044e
            goto L_0x0404
        L_0x03fa:
            java.lang.Object[] r0 = new java.lang.Object[]{r9}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object r0 = r12.invoke(r5, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            if (r0 == 0) goto L_0x044e
        L_0x0404:
            r0 = 26
            if (r1 < r0) goto L_0x0411
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object r1 = r11.invoke(r5, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            android.content.res.AssetManager r1 = (android.content.res.AssetManager) r1     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            goto L_0x041b
        L_0x0411:
            java.lang.Object[] r0 = new java.lang.Object[]{r9}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object r1 = r11.invoke(r5, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            android.content.res.AssetManager r1 = (android.content.res.AssetManager) r1     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
        L_0x041b:
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object r6 = r6.invoke(r1, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            android.util.SparseArray r6 = (android.util.SparseArray) r6     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            int r4 = r6.size()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r3 = 0
        L_0x0428:
            if (r3 >= r4) goto L_0x044e
            int r11 = r6.keyAt(r3)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r0 = 1
            if (r11 == r0) goto L_0x044b
            r0 = 127(0x7f, float:1.78E-43)
            if (r11 == r0) goto L_0x044b
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object r2 = r8.invoke(r5, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object r1 = r6.valueAt(r3)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r1, r0}     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r7.invoke(r5, r0)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
        L_0x044b:
            int r3 = r3 + 1
            goto L_0x0428
        L_0x044e:
            r0 = r16
            r0.set(r9, r10)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | NullPointerException | InvocationTargetException -> 0x0455 }
            r0 = 1
            goto L_0x0467
        L_0x0455:
            r2 = move-exception
            r0 = 1
            goto L_0x045d
        L_0x0458:
            r2 = move-exception
            r0 = 0
            goto L_0x045d
        L_0x045b:
            r2 = move-exception
            r0 = 0
        L_0x045d:
            if (r0 != 0) goto L_0x04a1
            java.lang.String r1 = "error initializing nonodex"
            r0 = r17
            android.util.Log.w(r0, r1, r2)
            r0 = 0
        L_0x0467:
            X.AnonymousClass001.A0m = r0
        L_0x0469:
            java.lang.String r2 = "EnsureDelegate"
            r1 = 1
            boolean r0 = X.AnonymousClass001.A0m
            if (r0 != 0) goto L_0x0474
            r10.A07()
            return
        L_0x0474:
            r10.A0Y = r1
            X.00A r1 = new X.00A
            r1.<init>(r10)
            X.00B r0 = new X.00B
            r0.<init>(r10, r2, r1)
            r0.start()
            r1.A04()
            r0.join()     // Catch:{ InterruptedException -> 0x049a }
            java.util.ArrayList r0 = r10.A0T
            boolean r0 = r0.remove(r10)
            if (r0 == 0) goto L_0x0492
            return
        L_0x0492:
            java.lang.AssertionError r1 = new java.lang.AssertionError
            java.lang.String r0 = "application was not found on mAllApplications"
            r1.<init>(r0)
            throw r1
        L_0x049a:
            r1 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r1)
            throw r0
        L_0x04a1:
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass001.A0D():void");
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.os.HandlerThread, java.util.ArrayList] */
    public void C50() {
        ? r3;
        synchronized (this.A0a) {
            r3 = 0;
            if (r3 != 0) {
                try {
                    r3.size();
                    Iterator it = r3.iterator();
                    while (it.hasNext()) {
                        Message message = (Message) it.next();
                        if (AnonymousClass009.A08) {
                            AnonymousClass009.A02(message);
                            AnonymousClass009.A00(message).sendMessage(message);
                        } else {
                            throw new AssertionError("init not called");
                        }
                    }
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            this.A0f = true;
        }
        Handler handler = this.A0H;
        if (handler != null) {
            AnonymousClass00S.A04(handler, new AnonymousClass0D5(), 1262855781);
        }
        if (r3 != 0) {
            r3.quit();
        }
        Object obj = A0i;
        synchronized (obj) {
            try {
                obj.notifyAll();
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public void onLowMemory() {
        if (this.A0Z) {
            super.onLowMemory();
        }
    }

    public void onTrimMemory(int i) {
        if (this.A0Z) {
            super.onTrimMemory(i);
        }
    }

    private static int A00(Class cls, String str) {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        return ((Integer) declaredField.get(null)).intValue();
    }

    private static int A01(Class cls, String str, int i) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return ((Integer) declaredField.get(null)).intValue();
        } catch (NoSuchFieldException unused) {
            return i;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0014, code lost:
        if (X.AnonymousClass001.A0l != false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A02() {
        /*
            r3 = this;
            r3.A07()
            com.facebook.base.app.ApplicationLike r2 = r3.A00
            boolean r0 = r2 instanceof X.AnonymousClass008
            if (r0 == 0) goto L_0x001e
            boolean r0 = X.AnonymousClass001.A0m
            if (r0 == 0) goto L_0x0016
            boolean r0 = r3.A0e
            if (r0 != 0) goto L_0x0016
            boolean r0 = X.AnonymousClass001.A0l
            r1 = 0
            if (r0 == 0) goto L_0x0017
        L_0x0016:
            r1 = 1
        L_0x0017:
            X.008 r2 = (X.AnonymousClass008) r2
            boolean r0 = r3.A0g
            r2.onSplashScreenDismissed(r1, r0)
        L_0x001e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass001.A02():void");
    }
}
