package X;

import android.net.Uri;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.13O  reason: invalid class name */
public final class AnonymousClass13O {
    private static volatile AnonymousClass13O A0E;
    public AnonymousClass13Q A00;
    public AnonymousClass13Q A01;
    public boolean A02 = false;
    public final C04460Ut A03;
    public final AnonymousClass0Ud A04;
    public final C31891kk A05;
    public final AnonymousClass1YI A06;
    public final AnonymousClass13Q A07;
    public final AnonymousClass0ZM A08;
    public final FbSharedPreferences A09;
    public final C31901kl A0A;
    public final C04310Tq A0B;
    public final C04310Tq A0C;
    public final C04310Tq A0D;

    public synchronized AnonymousClass13Q A01() {
        if (this.A00 == null) {
            if (!this.A02) {
                this.A02 = true;
                this.A09.C0i(C25951af.A0F, this.A08);
            }
            if (((Boolean) this.A0B.get()).booleanValue()) {
                this.A00 = new AnonymousClass13S(new C47732Xq(this.A0C), this.A04, this.A05.A02());
            } else {
                this.A00 = A02();
            }
        }
        return this.A00;
    }

    public synchronized AnonymousClass13Q A02() {
        if (this.A01 == null) {
            if (!this.A02) {
                this.A02 = true;
                this.A09.C0i(C25951af.A0F, this.A08);
            }
            if (((Boolean) this.A0D.get()).booleanValue()) {
                String B4F = this.A09.B4F(C25951af.A0K, "default");
                String B4F2 = this.A09.B4F(C25951af.A0I, BuildConfig.FLAVOR);
                String str = null;
                if ("intern".equals(B4F)) {
                    str = AnonymousClass08S.A0J("intern.", "facebook.com");
                } else if ("dev".equals(B4F)) {
                    str = AnonymousClass08S.A0J("dev.", "facebook.com");
                } else if (!"production".equals(B4F)) {
                    String B4F3 = this.A09.B4F(C25951af.A0J, null);
                    if (!C06850cB.A0B(B4F3)) {
                        try {
                            Uri.parse(B4F3);
                            str = B4F3;
                        } catch (Throwable th) {
                            C010708t.A0M("DefaultServerConfig", "Failed to parse web sandbox URL", th);
                        }
                    } else if (this.A06.AbO(AnonymousClass1Y3.A1W, false)) {
                        str = AnonymousClass08S.A0J("beta.", "facebook.com");
                    } else {
                        str = "facebook.com";
                    }
                }
                if (!C06850cB.A0B(str) || !C06850cB.A0B(B4F2)) {
                    this.A01 = new AnonymousClass13S(new AnonymousClass13V(str, this.A0C), this.A04, this.A05.A02());
                }
            }
            if (this.A01 == null) {
                this.A01 = this.A07;
            }
        }
        this.A01.AoB();
        return this.A01;
    }

    public static final AnonymousClass13O A00(AnonymousClass1XY r13) {
        if (A0E == null) {
            synchronized (AnonymousClass13O.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0E, r13);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r13.getApplicationInjector();
                        A0E = new AnonymousClass13O(FbSharedPreferencesModule.A00(applicationInjector), C04430Uq.A02(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.B0Y, applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.AYV, applicationInjector), AnonymousClass0Ud.A00(applicationInjector), new C31891kk(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.BU5, applicationInjector), AnonymousClass0WA.A00(applicationInjector), C31901kl.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0E;
    }

    private AnonymousClass13O(FbSharedPreferences fbSharedPreferences, C04460Ut r5, C04310Tq r6, C04310Tq r7, AnonymousClass0Ud r8, C31891kk r9, C04310Tq r10, AnonymousClass1YI r11, C31901kl r12) {
        this.A09 = fbSharedPreferences;
        this.A03 = r5;
        this.A0D = r6;
        this.A0B = r7;
        this.A04 = r8;
        this.A05 = r9;
        this.A0C = r10;
        this.A06 = r11;
        this.A0A = r12;
        this.A07 = new AnonymousClass13S(new AnonymousClass13V("facebook.com", r10), r8, r9.A02());
        this.A08 = new C31911km(this);
    }
}
