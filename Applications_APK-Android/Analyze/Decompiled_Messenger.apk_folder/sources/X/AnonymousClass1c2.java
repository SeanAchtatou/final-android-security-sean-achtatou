package X;

import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.Serializable;
import java.util.LinkedHashMap;

/* renamed from: X.1c2  reason: invalid class name */
public abstract class AnonymousClass1c2 extends C26791c3 implements Serializable {
    private static final long serialVersionUID = 1;
    public transient LinkedHashMap _objectIds;

    public abstract AnonymousClass1c2 createInstance(C10490kF r1, C28271eX r2, CYj cYj);

    public JsonDeserializer deserializerInstance(C10080jW r5, Object obj) {
        JsonDeserializer jsonDeserializer;
        JsonDeserializer jsonDeserializer2 = null;
        if (obj != null) {
            if (obj instanceof JsonDeserializer) {
                jsonDeserializer = (JsonDeserializer) obj;
            } else if (obj instanceof Class) {
                Class<C422228t> cls = (Class) obj;
                if (!(cls == JsonDeserializer.None.class || cls == C422228t.class)) {
                    if (JsonDeserializer.class.isAssignableFrom(cls)) {
                        C10490kF r1 = this._config;
                        CXG cxg = r1._base._handlerInstantiator;
                        if (cxg != null) {
                            jsonDeserializer2 = cxg.deserializerInstance(r1, r5, cls);
                        }
                        if (jsonDeserializer2 == null) {
                            jsonDeserializer = (JsonDeserializer) C29081fq.createInstance(cls, r1.canOverrideAccessModifiers());
                        } else {
                            jsonDeserializer = jsonDeserializer2;
                        }
                    } else {
                        throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned Class ", cls.getName(), "; expected Class<JsonDeserializer>"));
                    }
                }
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned deserializer definition of type ", obj.getClass().getName(), "; expected type JsonDeserializer or Class<JsonDeserializer> instead"));
            }
            if (jsonDeserializer instanceof C29181g0) {
                ((C29181g0) jsonDeserializer).resolve(this);
            }
            return jsonDeserializer;
        }
        return null;
    }

    public final C422628y keyDeserializerInstance(C10080jW r5, Object obj) {
        C422628y r6;
        C422628y r2 = null;
        if (obj != null) {
            if (obj instanceof C422628y) {
                r6 = (C422628y) obj;
            } else if (obj instanceof Class) {
                Class<C422228t> cls = (Class) obj;
                if (!(cls == C422528x.class || cls == C422228t.class)) {
                    if (C422628y.class.isAssignableFrom(cls)) {
                        C10490kF r1 = this._config;
                        CXG cxg = r1._base._handlerInstantiator;
                        if (cxg != null) {
                            r2 = cxg.keyDeserializerInstance(r1, r5, cls);
                        }
                        if (r2 == null) {
                            r6 = (C422628y) C29081fq.createInstance(cls, r1.canOverrideAccessModifiers());
                        } else {
                            r6 = r2;
                        }
                    } else {
                        throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned Class ", cls.getName(), "; expected Class<KeyDeserializer>"));
                    }
                }
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0P("AnnotationIntrospector returned key deserializer definition of type ", obj.getClass().getName(), "; expected type KeyDeserializer or Class<KeyDeserializer> instead"));
            }
            if (r6 instanceof C29181g0) {
                ((C29181g0) r6).resolve(this);
            }
            return r6;
        }
        return null;
    }

    public abstract AnonymousClass1c2 with(AnonymousClass1c6 r1);

    public C87454Ev findObjectId(Object obj, C25128CaN caN) {
        AnonymousClass4IX key = caN.key(obj);
        LinkedHashMap linkedHashMap = this._objectIds;
        if (linkedHashMap == null) {
            this._objectIds = new LinkedHashMap();
        } else {
            C87454Ev r0 = (C87454Ev) linkedHashMap.get(key);
            if (r0 != null) {
                return r0;
            }
        }
        C87454Ev r1 = new C87454Ev(obj);
        this._objectIds.put(key, r1);
        return r1;
    }

    public AnonymousClass1c2(AnonymousClass1c2 r1, C10490kF r2, C28271eX r3, CYj cYj) {
        super(r1, r2, r3, cYj);
    }

    public AnonymousClass1c2(AnonymousClass1c2 r1, AnonymousClass1c6 r2) {
        super(r1, r2);
    }

    public AnonymousClass1c2(AnonymousClass1c6 r1, C26841cE r2) {
        super(r1, r2);
    }
}
