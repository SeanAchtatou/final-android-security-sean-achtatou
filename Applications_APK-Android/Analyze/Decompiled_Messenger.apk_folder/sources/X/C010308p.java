package X;

import android.os.SystemClock;

/* renamed from: X.08p  reason: invalid class name and case insensitive filesystem */
public final class C010308p extends AnonymousClass02K {
    public int A00;
    public long A01;
    public long A02;
    public final /* synthetic */ AnonymousClass08W A03;

    public C010308p(AnonymousClass08W r1) {
        this.A03 = r1;
    }

    public void close() {
        AnonymousClass08W r2 = this.A03;
        synchronized (r2) {
            this.A02 = SystemClock.uptimeMillis();
            synchronized (r2) {
                AnonymousClass0KR r0 = r2.A00;
                if (r0 == null) {
                    r2.A01.add(this);
                } else {
                    AnonymousClass08W.A00(r0, this);
                }
            }
        }
    }
}
