package X;

import com.facebook.acra.LogCatCollector;
import com.facebook.crypto.keychain.KeyChain;
import com.facebook.crypto.module.LoggedInUserCrypto;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.187  reason: invalid class name */
public final class AnonymousClass187 {
    public static final Integer A04 = AnonymousClass07B.A01;
    private static final Class A05 = AnonymousClass187.class;
    private static volatile AnonymousClass187 A06;
    public final AnonymousClass18A A00;
    public final C04310Tq A01;
    private final C194418e A02 = C194418e.A00(BuildConfig.FLAVOR);
    private final AnonymousClass18M A03;

    private KeyChain A00(int i) {
        byte[] bArr;
        if (i < 0) {
            return (KeyChain) this.A01.get();
        }
        AnonymousClass18F r4 = (AnonymousClass18F) this.A01.get();
        synchronized (r4) {
            Map map = r4.A01;
            Integer valueOf = Integer.valueOf(i);
            bArr = (byte[]) map.get(valueOf);
            if (bArr == null) {
                AnonymousClass19A r5 = new AnonymousClass19A(AnonymousClass08S.A09("master_key_v", i));
                byte[] A042 = r4.A05.A04(r5);
                if (A042 != null) {
                    AnonymousClass18F.A03(r4);
                    AnonymousClass18F.A02(r4);
                    bArr = r4.A06(A042);
                } else {
                    bArr = AnonymousClass18F.A05(r4);
                    C194418e A002 = AnonymousClass18F.A00(r4);
                    try {
                        byte[] A062 = ((LoggedInUserCrypto) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQh, r4.A00)).A06(bArr, A002);
                        r4.A05.A03(AnonymousClass197.A01, A002.A00);
                        r4.A05.A03(r5, A062);
                    } catch (C37821wJ | C37921wZ | IOException e) {
                        C010708t.A0L(AnonymousClass18F.A0A, "Failed to encrypt versioned master key for local storage", e);
                        throw new C37911wY("Encryption failed", e);
                    }
                }
                r4.A01.put(valueOf, bArr);
            }
        }
        return new C50672eQ(this, bArr);
    }

    public static final AnonymousClass187 A01(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (AnonymousClass187.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new AnonymousClass187(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public String A02(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null) {
            return null;
        }
        try {
            return new String(A05(bArr, bArr2), LogCatCollector.UTF_8_ENCODING);
        } catch (C37911wY | C37921wZ | IOException e) {
            C010708t.A08(A05, "Failed to decrypt blob content", e);
            throw new RuntimeException(e);
        }
    }

    public byte[] A05(byte[] bArr, byte[] bArr2) {
        return new AnonymousClass192(new C50672eQ(this, bArr), this.A00.A00, AnonymousClass07B.A01).A00(bArr2, this.A02);
    }

    public byte[] A06(byte[] bArr, byte[] bArr2) {
        return new AnonymousClass192(new C50672eQ(this, bArr), this.A00.A00, AnonymousClass07B.A01).A01(bArr2, this.A02);
    }

    private AnonymousClass187(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass18A.A00(r2);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.AKP, r2);
        this.A03 = AnonymousClass18M.A00(r2);
    }

    public byte[] A03(byte[] bArr, int i) {
        return new AnonymousClass192(A00(i), this.A00.A00, AnonymousClass07B.A01).A01(bArr, this.A02);
    }

    public byte[] A04(byte[] bArr, String str, int i) {
        try {
            return new AnonymousClass192(A00(i), this.A00.A00, AnonymousClass07B.A01).A00(bArr, this.A02);
        } catch (C37911wY | C37921wZ | IOException e) {
            AnonymousClass18M r3 = this.A03;
            HashMap hashMap = new HashMap();
            hashMap.put("error", "FailedToDecryptWithVersionedMasterKey");
            hashMap.put("trigger_reason", str);
            hashMap.put("exception_type", e.getClass().getSimpleName());
            hashMap.put("error_info", e.getMessage());
            AnonymousClass18M.A03(r3, "tincan_db_error", hashMap);
            return null;
        }
    }
}
