package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0s7  reason: invalid class name and case insensitive filesystem */
public final class C13810s7 {
    private static volatile C13810s7 A01;
    public final AnonymousClass03g A00;

    public static final C13810s7 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C13810s7.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C13810s7(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C13810s7(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass03g.A00(r2);
    }
}
