package X;

/* renamed from: X.0GH  reason: invalid class name */
public final class AnonymousClass0GH extends AnonymousClass0FM {
    public AnonymousClass04b appWakeups = new AnonymousClass04b();

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r3) {
        this.appWakeups.clear();
        this.appWakeups.A0B(((AnonymousClass0GH) r3).appWakeups);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r12, AnonymousClass0FM r13) {
        AnonymousClass0GH r122 = (AnonymousClass0GH) r12;
        AnonymousClass0GH r132 = (AnonymousClass0GH) r13;
        if (r132 == null) {
            r132 = new AnonymousClass0GH();
        }
        if (r122 != null) {
            r132.appWakeups.clear();
            int i = 0;
            while (true) {
                AnonymousClass04b r3 = this.appWakeups;
                if (i >= r3.size()) {
                    break;
                }
                String str = (String) r3.A07(i);
                r132.appWakeups.put(str, new AnonymousClass0GI(((AnonymousClass0GI) r3.A09(i)).A02, 0, 0));
                AnonymousClass0GI r7 = (AnonymousClass0GI) this.appWakeups.A09(i);
                AnonymousClass0GI r6 = (AnonymousClass0GI) r122.appWakeups.get(str);
                AnonymousClass0GI r5 = (AnonymousClass0GI) r132.appWakeups.get(str);
                if (r6 == null) {
                    r5.A00(r7);
                } else {
                    if (r6.A02 != r7.A02) {
                        AnonymousClass0KZ.A00("AppWakeupMetrics", AnonymousClass08S.A0S("Diff only allowed for similar kind of wakeups: ", r7.toString(), ", ", r6.toString()), null);
                    }
                    r5.A02 = r7.A02;
                    r5.A00 = r7.A00 - r6.A00;
                    r5.A01 = r7.A01 - r6.A01;
                }
                i++;
            }
        } else {
            r132.appWakeups.clear();
            r132.appWakeups.A0B(this.appWakeups);
        }
        return r132;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r13, AnonymousClass0FM r14) {
        AnonymousClass0GH r132 = (AnonymousClass0GH) r13;
        AnonymousClass0GH r142 = (AnonymousClass0GH) r14;
        if (r142 == null) {
            r142 = new AnonymousClass0GH();
        }
        if (r132 != null) {
            r142.appWakeups.clear();
            int i = 0;
            int i2 = 0;
            while (true) {
                AnonymousClass04b r2 = this.appWakeups;
                if (i2 >= r2.size()) {
                    break;
                }
                String str = (String) r2.A07(i2);
                r142.appWakeups.put(str, new AnonymousClass0GI(((AnonymousClass0GI) r2.A09(i2)).A02, 0, 0));
                ((AnonymousClass0GI) this.appWakeups.A09(i2)).A01((AnonymousClass0GI) r132.appWakeups.get(str), (AnonymousClass0GI) r142.appWakeups.get(str));
                i2++;
            }
            while (true) {
                AnonymousClass04b r1 = r132.appWakeups;
                if (i >= r1.size()) {
                    break;
                }
                String str2 = (String) r1.A07(i);
                if (!r142.appWakeups.containsKey(str2)) {
                    r142.appWakeups.put(str2, r132.appWakeups.A09(i));
                }
                i++;
            }
        } else {
            r142.appWakeups.clear();
            r142.appWakeups.A0B(this.appWakeups);
        }
        return r142;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return C02740Gd.A02(this.appWakeups, ((AnonymousClass0GH) obj).appWakeups);
    }

    public int hashCode() {
        AnonymousClass04b r0 = this.appWakeups;
        if (r0 != null) {
            return r0.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            AnonymousClass04b r1 = this.appWakeups;
            if (i >= r1.size()) {
                return sb.toString();
            }
            sb.append((String) r1.A07(i));
            sb.append(": ");
            sb.append(r1.A09(i));
            sb.append(", ");
            i++;
        }
    }
}
