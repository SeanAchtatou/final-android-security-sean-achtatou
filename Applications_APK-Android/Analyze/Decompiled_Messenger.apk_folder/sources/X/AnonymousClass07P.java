package X;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.Executor;

/* renamed from: X.07P  reason: invalid class name */
public final class AnonymousClass07P {
    public static final Executor A06 = new AnonymousClass07Q();
    private static final C05860aS A07 = new C05860aS();
    private AnonymousClass01B A00 = null;
    public final Context A01;
    public final File A02;
    public final String A03;
    public final Executor A04;
    public final AnonymousClass07R[] A05;

    public static void A01(AnonymousClass07P r5) {
        for (AnonymousClass07R A012 : r5.A05) {
            A012.A01(r5.A01);
        }
        if (!new File(r5.A02, ".unpacked").createNewFile()) {
            throw new IOException("Could not create .unpacked file");
        }
    }

    public static void A02(AnonymousClass07P r2) {
        AnonymousClass01B r0 = r2.A00;
        C000300h.A01(r0);
        r0.close();
        r2.A00 = null;
        A07.A01(r2.A03);
    }

    public static byte[] A03(InputStream inputStream, byte[] bArr, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i2 = 0;
        while (i2 < i) {
            int read = inputStream.read(bArr, 0, Math.min(i - i2, bArr.length));
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(bArr, 0, read);
            i2 += read;
        }
        return byteArrayOutputStream.toByteArray();
    }

    public AnonymousClass07P(AnonymousClass07O r3) {
        Context context = r3.A00;
        C000300h.A01(context);
        this.A01 = context;
        File file = r3.A01;
        C000300h.A01(file);
        this.A02 = file;
        ArrayList arrayList = r3.A04;
        this.A05 = (AnonymousClass07R[]) arrayList.toArray(new AnonymousClass07R[arrayList.size()]);
        this.A04 = r3.A03;
        String str = r3.A02;
        C000300h.A01(str);
        this.A03 = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00bf, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002b, code lost:
        if (r0 == false) goto L_0x002d;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00c3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:66:0x00cc */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
            r15 = this;
            r15.A00()     // Catch:{ IOException | InterruptedException -> 0x00e7 }
            java.lang.String r1 = "AppUnpacker.unpack()"
            r0 = -580000483(0xffffffffdd6de51d, float:-1.07138372E18)
            r2 = 2147483648(0x80000000, double:1.0609978955E-314)
            X.AnonymousClass00C.A01(r2, r1, r0)     // Catch:{ IOException | InterruptedException -> 0x00e7 }
            r13 = 16384(0x4000, float:2.2959E-41)
            byte[] r12 = new byte[r13]     // Catch:{ all -> 0x00dc }
            java.io.File r4 = new java.io.File     // Catch:{ all -> 0x00dc }
            java.io.File r1 = r15.A02     // Catch:{ all -> 0x00dc }
            java.lang.String r0 = ".unpacked"
            r4.<init>(r1, r0)     // Catch:{ all -> 0x00dc }
            java.io.File r0 = r15.A02     // Catch:{ all -> 0x00dc }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x00dc }
            r5 = 0
            if (r0 == 0) goto L_0x002d
            boolean r0 = r4.exists()     // Catch:{ all -> 0x00dc }
            r4 = 0
            if (r0 != 0) goto L_0x002e
        L_0x002d:
            r4 = 1
        L_0x002e:
            X.07R[] r1 = r15.A05     // Catch:{ all -> 0x00dc }
            int r0 = r1.length     // Catch:{ all -> 0x00dc }
            if (r5 >= r0) goto L_0x0040
            if (r4 != 0) goto L_0x0040
            r1 = r1[r5]     // Catch:{ all -> 0x00dc }
            android.content.Context r0 = r15.A01     // Catch:{ all -> 0x00dc }
            boolean r4 = r1.A02(r0, r12)     // Catch:{ all -> 0x00dc }
            int r5 = r5 + 1
            goto L_0x002e
        L_0x0040:
            r14 = 0
            if (r4 != 0) goto L_0x0048
            r1 = 0
        L_0x0044:
            r0 = 718890688(0x2ad966c0, float:3.861824E-13)
            goto L_0x00ae
        L_0x0048:
            java.io.File r0 = r15.A02     // Catch:{ all -> 0x00d5 }
            X.C002401o.A00(r0)     // Catch:{ all -> 0x00d5 }
            java.io.File r0 = r15.A02     // Catch:{ all -> 0x00d5 }
            boolean r0 = r0.mkdirs()     // Catch:{ all -> 0x00d5 }
            if (r0 == 0) goto L_0x00cd
            X.07R[] r11 = r15.A05     // Catch:{ all -> 0x00d5 }
            int r10 = r11.length     // Catch:{ all -> 0x00d5 }
            r9 = 0
        L_0x0059:
            if (r9 >= r10) goto L_0x0090
            r1 = r11[r9]     // Catch:{ all -> 0x00d5 }
            android.content.Context r0 = r15.A01     // Catch:{ all -> 0x00d5 }
            java.io.InputStream r8 = r1.A00(r0)     // Catch:{ all -> 0x00d5 }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ all -> 0x00c4 }
            java.io.File r0 = r1.A00     // Catch:{ all -> 0x00c4 }
            X.C000300h.A01(r0)     // Catch:{ all -> 0x00c4 }
            r7.<init>(r0)     // Catch:{ all -> 0x00c4 }
            r6 = 2147483647(0x7fffffff, float:NaN)
            r4 = 0
        L_0x0071:
            if (r4 >= r6) goto L_0x0085
            int r0 = r6 - r4
            int r0 = java.lang.Math.min(r0, r13)     // Catch:{ all -> 0x00bd }
            int r1 = r8.read(r12, r14, r0)     // Catch:{ all -> 0x00bd }
            r0 = -1
            if (r1 == r0) goto L_0x0085
            r7.write(r12, r14, r1)     // Catch:{ all -> 0x00bd }
            int r4 = r4 + r1
            goto L_0x0071
        L_0x0085:
            r7.close()     // Catch:{ all -> 0x00c4 }
            if (r8 == 0) goto L_0x008d
            r8.close()     // Catch:{ all -> 0x00d5 }
        L_0x008d:
            int r9 = r9 + 1
            goto L_0x0059
        L_0x0090:
            java.util.concurrent.Executor r4 = r15.A04     // Catch:{ all -> 0x00d5 }
            if (r4 == 0) goto L_0x00a0
            X.07S r1 = new X.07S     // Catch:{ all -> 0x00d5 }
            r1.<init>(r15)     // Catch:{ all -> 0x00d5 }
            r0 = 191822118(0xb6ef926, float:4.602457E-32)
            X.AnonymousClass07A.A04(r4, r1, r0)     // Catch:{ all -> 0x00d5 }
            goto L_0x00a4
        L_0x00a0:
            A01(r15)     // Catch:{ all -> 0x00d5 }
            r14 = 2
        L_0x00a4:
            r1 = r14 | 1
            if (r1 != 0) goto L_0x0044
            java.io.File r0 = r15.A02     // Catch:{ all -> 0x00dc }
            X.C002401o.A00(r0)     // Catch:{ all -> 0x00dc }
            goto L_0x0044
        L_0x00ae:
            X.AnonymousClass00C.A00(r2, r0)     // Catch:{ IOException | InterruptedException -> 0x00e7 }
            r0 = r1 & 1
            if (r0 == 0) goto L_0x00b9
            r0 = r1 & 2
            if (r0 == 0) goto L_0x00bc
        L_0x00b9:
            A02(r15)     // Catch:{ IOException | InterruptedException -> 0x00e7 }
        L_0x00bc:
            return
        L_0x00bd:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00bf }
        L_0x00bf:
            r0 = move-exception
            r7.close()     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            throw r0     // Catch:{ all -> 0x00c4 }
        L_0x00c4:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00c6 }
        L_0x00c6:
            r0 = move-exception
            if (r8 == 0) goto L_0x00cc
            r8.close()     // Catch:{ all -> 0x00cc }
        L_0x00cc:
            throw r0     // Catch:{ all -> 0x00d5 }
        L_0x00cd:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = "Could not create the destination directory"
            r1.<init>(r0)     // Catch:{ all -> 0x00d5 }
            throw r1     // Catch:{ all -> 0x00d5 }
        L_0x00d5:
            r1 = move-exception
            java.io.File r0 = r15.A02     // Catch:{ all -> 0x00dc }
            X.C002401o.A00(r0)     // Catch:{ all -> 0x00dc }
            throw r1     // Catch:{ all -> 0x00dc }
        L_0x00dc:
            r1 = move-exception
            r0 = 1725735239(0x66dca147, float:5.2094783E23)
            X.AnonymousClass00C.A00(r2, r0)     // Catch:{ IOException | InterruptedException -> 0x00e7 }
            A02(r15)     // Catch:{ IOException | InterruptedException -> 0x00e7 }
            throw r1     // Catch:{ IOException | InterruptedException -> 0x00e7 }
        L_0x00e7:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07P.A04():void");
    }

    private void A00() {
        C05860aS r5;
        AnonymousClass00C.A01(2147483648L, "AppUnpacker.lock", -1244634176);
        try {
            r5 = A07;
            String str = this.A03;
            synchronized (r5) {
                try {
                    C05870aT r1 = (C05870aT) r5.A00.get(str);
                    if (r1 == null) {
                        C05870aT r12 = new C05870aT();
                        r12.A01.acquire();
                        r5.A00.put(str, r12);
                    } else {
                        r1.A00++;
                        try {
                            r1.A01.acquire();
                        } catch (InterruptedException | RuntimeException e) {
                            e = e;
                            C05860aS.A00(r5, str);
                            throw e;
                        }
                    }
                } catch (Throwable th) {
                    while (true) {
                        e = th;
                        break;
                    }
                }
            }
            boolean z = false;
            if (this.A00 == null) {
                z = true;
            }
            C000300h.A03(z);
            File filesDir = this.A01.getFilesDir();
            if (filesDir == null || !filesDir.exists()) {
                filesDir = new File("/data/local/tmp");
            }
            File file = new File(filesDir, AnonymousClass08S.A0J(this.A03, ".lock"));
            if (!file.exists()) {
                file.createNewFile();
            }
            this.A00 = new AnonymousClass01B(file);
            AnonymousClass00C.A00(2147483648L, -1211033660);
        } catch (Throwable th2) {
            AnonymousClass00C.A00(2147483648L, -702398667);
            throw th2;
        }
    }
}
