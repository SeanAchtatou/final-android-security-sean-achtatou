package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;

/* renamed from: X.1zO  reason: invalid class name and case insensitive filesystem */
public final class C39651zO implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4;
        float readFloat = parcel.readFloat();
        int readInt = parcel.readInt();
        int readInt2 = parcel.readInt();
        Integer num5 = null;
        if (parcel.readInt() == 0) {
            str = parcel.readString();
        } else {
            str = null;
        }
        int readInt3 = parcel.readInt();
        if (parcel.readInt() == 0) {
            str2 = parcel.readString();
        } else {
            str2 = null;
        }
        int readInt4 = parcel.readInt();
        if (parcel.readInt() == 0) {
            str3 = parcel.readString();
        } else {
            str3 = null;
        }
        int readInt5 = parcel.readInt();
        if (parcel.readInt() == 0) {
            str4 = parcel.readString();
        } else {
            str4 = null;
        }
        int readInt6 = parcel.readInt();
        if (parcel.readInt() == 0) {
            str5 = parcel.readString();
        } else {
            str5 = null;
        }
        int readInt7 = parcel.readInt();
        if (parcel.readInt() == 0) {
            str6 = parcel.readString();
        } else {
            str6 = null;
        }
        if (parcel.readInt() == 0) {
            num = Integer.valueOf(parcel.readInt());
        } else {
            num = null;
        }
        if (parcel.readInt() == 0) {
            num2 = Integer.valueOf(parcel.readInt());
        } else {
            num2 = null;
        }
        if (parcel.readInt() == 0) {
            num3 = Integer.valueOf(parcel.readInt());
        } else {
            num3 = null;
        }
        if (parcel.readInt() == 0) {
            num4 = Integer.valueOf(parcel.readInt());
        } else {
            num4 = null;
        }
        if (parcel.readInt() == 0) {
            num5 = Integer.valueOf(parcel.readInt());
        }
        float readFloat2 = parcel.readFloat();
        boolean z = false;
        if (parcel.readInt() == 1) {
            z = true;
        }
        long readLong = parcel.readLong();
        int[] createIntArray = parcel.createIntArray();
        float readFloat3 = parcel.readFloat();
        float readFloat4 = parcel.readFloat();
        boolean z2 = false;
        if (parcel.readInt() == 1) {
            z2 = true;
        }
        float readFloat5 = parcel.readFloat();
        float readFloat6 = parcel.readFloat();
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        float readFloat7 = parcel.readFloat();
        boolean z3 = false;
        if (parcel.readInt() == 1) {
            z3 = true;
        }
        boolean z4 = false;
        if (parcel.readInt() == 1) {
            z4 = true;
        }
        return new LocationComponentOptions(readFloat, readInt, readInt2, str, readInt3, str2, readInt4, str3, readInt5, str4, readInt6, str5, readInt7, str6, num, num2, num3, num4, num5, readFloat2, z, readLong, createIntArray, readFloat3, readFloat4, z2, readFloat5, readFloat6, readString, readString2, readFloat7, z3, z4);
    }

    public Object[] newArray(int i) {
        return new LocationComponentOptions[i];
    }
}
