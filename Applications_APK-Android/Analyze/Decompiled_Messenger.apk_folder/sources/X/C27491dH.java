package X;

/* renamed from: X.1dH  reason: invalid class name and case insensitive filesystem */
public final class C27491dH {
    public static volatile C27481dG A00;

    public static Object A00(Object obj, String str) {
        String A0J;
        if (A00 == null || obj == null || !C25231Yv.A01() || !(obj instanceof AnonymousClass1XV)) {
            return null;
        }
        AnonymousClass1XV r4 = (AnonymousClass1XV) obj;
        if (str == null) {
            A0J = r4.A06;
        } else {
            A0J = AnonymousClass08S.A0J("Fresco_", str);
        }
        return C27401d8.A00.A02(r4, A0J, 1, 6);
    }

    public static Object A01(String str) {
        if (A00 == null || str == null || !C25231Yv.A01()) {
            return null;
        }
        return C27401d8.A01(AnonymousClass08S.A0J("Fresco_", str), 6);
    }

    public static Runnable A02(Runnable runnable, String str) {
        if (A00 == null || runnable == null || str == null) {
            return runnable;
        }
        return C25231Yv.A00(AnonymousClass08S.A0J("Fresco_", str), runnable, 6);
    }

    public static void A03(Object obj) {
        if (A00 != null && obj != null && (obj instanceof AnonymousClass1XV)) {
            ((AnonymousClass1XV) obj).close();
        }
    }
}
