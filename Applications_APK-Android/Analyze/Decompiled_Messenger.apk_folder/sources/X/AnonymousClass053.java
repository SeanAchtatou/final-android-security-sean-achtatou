package X;

import android.net.Uri;
import io.card.payment.BuildConfig;

/* renamed from: X.053  reason: invalid class name */
public final class AnonymousClass053 implements AnonymousClass03a {
    private boolean A00;

    public void Bse() {
        AnonymousClass054 r16;
        C02320Ea A002;
        String[] A0C;
        boolean z;
        if (AnonymousClass08Z.A05(268435456) && (r16 = AnonymousClass054.A07) != null) {
            AnonymousClass00C.A01(268435456, "Starting Profilo", 1297394142);
            try {
                this.A00 = r16.A0A(AnonymousClass04i.A00, 1, C03730Pf.class, 0);
                if (z) {
                    if (A0C != null) {
                        A002.A02("URL", new Uri.Builder().scheme("https").authority("our.intern.facebook.com").path("intern/artillery2/waterfall").appendQueryParameter("id", A0C[0]).appendQueryParameter("pref_name", "Profilo").build().toString());
                    }
                }
            } finally {
                A002 = AnonymousClass0EY.A00(268435456, AnonymousClass0EY.A01, BuildConfig.FLAVOR);
                A002.A02("Success", Boolean.valueOf(this.A00));
                if (this.A00) {
                    A0C = r16.A0C();
                    if (A0C == null) {
                        A002.A02("URL", "No trace");
                    } else {
                        A002.A02("URL", new Uri.Builder().scheme("https").authority("our.intern.facebook.com").path("intern/artillery2/waterfall").appendQueryParameter("id", A0C[0]).appendQueryParameter("pref_name", "Profilo").build().toString());
                    }
                }
                A002.A03();
            }
        }
    }

    public void Bsf() {
        AnonymousClass054 r4;
        if (this.A00 && (r4 = AnonymousClass054.A07) != null) {
            r4.A0B(AnonymousClass04i.A00, C03730Pf.class, 0);
        }
    }
}
