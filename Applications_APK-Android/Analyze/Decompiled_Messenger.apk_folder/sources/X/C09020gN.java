package X;

import android.os.Build;
import android.view.Choreographer;
import com.google.common.base.Preconditions;

/* renamed from: X.0gN  reason: invalid class name and case insensitive filesystem */
public final class C09020gN {
    private static final boolean A06;
    public C29554EdC A00;
    public C08470fP A01 = null;
    private boolean A02;
    private final AnonymousClass09P A03;
    private final AnonymousClass1Y6 A04;
    private final boolean A05;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 16) {
            z = true;
        }
        A06 = z;
    }

    private void A00() {
        if (!this.A02) {
            this.A02 = true;
            if (this.A05) {
                EdA edA = new EdA(this, Choreographer.getInstance(), this.A03);
                C29554EdC edC = this.A00;
                if (edC != null) {
                    edC.AXI(this);
                }
                Preconditions.checkNotNull(edA);
                this.A00 = edA;
                return;
            }
            C29971Emk emk = new C29971Emk(this, Choreographer.getInstance());
            C29554EdC edC2 = this.A00;
            if (edC2 != null) {
                edC2.AXI(this);
            }
            Preconditions.checkNotNull(emk);
            this.A00 = emk;
        }
    }

    public void A01() {
        if (A06) {
            this.A04.AOz();
            A00();
            this.A00.AXI(this);
        }
    }

    public void A02() {
        if (A06) {
            this.A04.AOz();
            A00();
            this.A00.AYG(this);
        }
    }

    public C09020gN(Boolean bool, AnonymousClass1Y6 r3, AnonymousClass09P r4) {
        this.A05 = bool.booleanValue();
        this.A04 = r3;
        this.A03 = r4;
    }
}
