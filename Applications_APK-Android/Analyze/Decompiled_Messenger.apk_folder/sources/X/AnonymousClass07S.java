package X;

import java.io.IOException;

/* renamed from: X.07S  reason: invalid class name */
public final class AnonymousClass07S implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appunpacker.AppUnpacker$2";
    public final /* synthetic */ AnonymousClass07P A00;

    public AnonymousClass07S(AnonymousClass07P r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass00C.A01(2147483648L, "AppUnpacker.finishUnpackingOnBackgroundThread()", -7120075);
        try {
            AnonymousClass07P.A01(this.A00);
            AnonymousClass07P.A02(this.A00);
            AnonymousClass00C.A00(2147483648L, 1392940333);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Throwable th) {
            AnonymousClass00C.A00(2147483648L, 1721410292);
            throw th;
        }
    }
}
