package X;

import android.graphics.Rect;

/* renamed from: X.0yN  reason: invalid class name and case insensitive filesystem */
public abstract class C17130yN {
    public Rect A00(C14030sU r3) {
        if (!(this instanceof C29861h6)) {
            return ((C17140yO) this).A00;
        }
        C29861h6 r1 = (C29861h6) this;
        Rect rect = r1.A00;
        if (rect == null || rect.isEmpty()) {
            return null;
        }
        return r1.A00;
    }
}
