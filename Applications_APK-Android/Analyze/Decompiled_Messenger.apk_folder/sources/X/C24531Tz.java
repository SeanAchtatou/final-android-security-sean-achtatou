package X;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1Tz  reason: invalid class name and case insensitive filesystem */
public final class C24531Tz implements C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("additional_contacts", (byte) 15, 1);
    private static final C36231sj A01 = new C36231sj("AdditionalContacts");
    public final List additional_contacts;

    public boolean equals(Object obj) {
        C24531Tz r5;
        if (obj == null || !(obj instanceof C24531Tz) || (r5 = (C24531Tz) obj) == null) {
            return false;
        }
        if (this == r5) {
            return true;
        }
        List list = this.additional_contacts;
        boolean z = false;
        if (list != null) {
            z = true;
        }
        List list2 = r5.additional_contacts;
        boolean z2 = false;
        if (list2 != null) {
            z2 = true;
        }
        if (!z && !z2) {
            return true;
        }
        if (!z || !z2 || !B36.A0F(list, list2)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    public void CNX(C35781ro r4) {
        r4.A0i(A01);
        if (this.additional_contacts != null) {
            r4.A0e(A00);
            r4.A0f(new C36381sy((byte) 10, this.additional_contacts.size()));
            for (Long longValue : this.additional_contacts) {
                r4.A0d(longValue.longValue());
            }
            r4.A0U();
            r4.A0S();
        }
        r4.A0T();
        r4.A0X();
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{this.additional_contacts});
    }

    public C24531Tz(List list) {
        this.additional_contacts = list;
    }

    public String CJ9(int i, boolean z) {
        return B36.A06(this, i, z);
    }
}
