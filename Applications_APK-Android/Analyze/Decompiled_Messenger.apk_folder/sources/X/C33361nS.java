package X;

import android.content.res.Resources;
import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.drawee.fbpipeline.FbDraweeView;
import com.facebook.inject.ContextScoped;
import com.facebook.messaging.montage.model.art.ArtItem;
import com.facebook.ui.emoji.model.Emoji;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@ContextScoped
/* renamed from: X.1nS  reason: invalid class name and case insensitive filesystem */
public final class C33361nS extends C20831Dz implements CallerContextable {
    private static C04470Uu A0B = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.composer.art.bottomsheetpicker.BottomSheetRecentAdapter";
    public C26640D5g A00;
    public final Resources A01;
    public final AnonymousClass1MT A02;
    public final C46552Qm A03;
    public final D76 A04;
    public final C73473g8 A05;
    public final C22351Kz A06;
    public final ArrayList A07 = new ArrayList(20);
    public final Map A08 = new HashMap();
    private final C61832zZ A09;
    private final C624531u A0A;

    public static final C33361nS A00(AnonymousClass1XY r4) {
        C33361nS r0;
        synchronized (C33361nS.class) {
            C04470Uu A002 = C04470Uu.A00(A0B);
            A0B = A002;
            try {
                if (A002.A03(r4)) {
                    A0B.A00 = new C33361nS((AnonymousClass1XY) A0B.A01());
                }
                C04470Uu r1 = A0B;
                r0 = (C33361nS) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A0B.A02();
                throw th;
            }
        }
        return r0;
    }

    public static ArtItem A01(String str) {
        C26511Czx czx;
        if (C06850cB.A0C(str, "l")) {
            czx = C26511Czx.LOCATION;
        } else if (C06850cB.A0C(str, "t")) {
            czx = C26511Czx.TIME;
        } else if (C06850cB.A0C(str, "d")) {
            czx = C26511Czx.DATE;
        } else if (C06850cB.A0C(str, "b")) {
            czx = C26511Czx.BATTERY;
        } else {
            czx = C26511Czx.USER_PHOTO;
        }
        D7B d7b = new D7B();
        d7b.A06 = czx;
        d7b.A0E = czx.name;
        return d7b.A00();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static void A02(C33361nS r6, D2A d2a) {
        String str;
        String str2;
        String A0J;
        String str3;
        ArtItem artItem;
        ArtItem artItem2;
        String str4;
        if (d2a != null) {
            int indexOf = r6.A07.indexOf(d2a);
            if (indexOf != -1) {
                r6.A07.remove(indexOf);
            }
            r6.A07.add(0, d2a);
            if (r6.A07.size() > 20) {
                r6.A07.remove(20);
            }
            C624531u r2 = r6.A0A;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < r6.A07.size(); i++) {
                D2A d2a2 = (D2A) r6.A07.get(i);
                switch (d2a2.A00.ordinal()) {
                    case 0:
                        Emoji emoji = d2a2.A02;
                        if (emoji != null) {
                            str = "e:";
                            str2 = emoji.A04();
                            A0J = AnonymousClass08S.A0J(str, str2);
                            break;
                        }
                        A0J = BuildConfig.FLAVOR;
                        break;
                    case 1:
                        artItem = d2a2.A01;
                        if (artItem != null) {
                            str3 = "ms:";
                            A0J = AnonymousClass08S.A0J(str3, artItem.A08);
                            break;
                        }
                        A0J = BuildConfig.FLAVOR;
                        break;
                    case 2:
                        artItem2 = d2a2.A01;
                        if (artItem2 != null) {
                            str = "ss:";
                            str2 = D2A.A00(artItem2.A02);
                            A0J = AnonymousClass08S.A0J(str, str2);
                            break;
                        }
                        A0J = BuildConfig.FLAVOR;
                        break;
                    case 3:
                        artItem = d2a2.A01;
                        if (artItem != null) {
                            str3 = "s:";
                            A0J = AnonymousClass08S.A0J(str3, artItem.A08);
                            break;
                        }
                        A0J = BuildConfig.FLAVOR;
                        break;
                    case 4:
                        ArtItem artItem3 = d2a2.A01;
                        if (artItem3 != null) {
                            C26533D0t d0t = artItem3.A00;
                            if (d0t == C26533D0t.POLL) {
                                str4 = "p";
                            } else if (d0t == C26533D0t.A01) {
                                str4 = "m";
                            } else if (d0t == C26533D0t.SLIDER) {
                                str4 = "r";
                            } else {
                                str4 = BuildConfig.FLAVOR;
                            }
                            A0J = AnonymousClass08S.A0J("is:", str4);
                            break;
                        }
                        A0J = BuildConfig.FLAVOR;
                        break;
                    case 5:
                        artItem2 = d2a2.A01;
                        if (artItem2 != null) {
                            str = "ssi:";
                            str2 = D2A.A00(artItem2.A02);
                            A0J = AnonymousClass08S.A0J(str, str2);
                            break;
                        }
                        A0J = BuildConfig.FLAVOR;
                        break;
                    default:
                        A0J = BuildConfig.FLAVOR;
                        break;
                }
                sb.append(A0J);
                if (i < r6.A07.size() - 1) {
                    sb.append(',');
                }
            }
            r2.A0A(C22298Ase.$const$string(AnonymousClass1Y3.A26), sb.toString());
            r6.A04();
        }
    }

    public static void A03(C33361nS r1, C26593D3j d3j, C33781o8 r3) {
        Uri uri = d3j.A00;
        FbDraweeView fbDraweeView = (FbDraweeView) r3.A0H;
        AnonymousClass1Q0 A022 = C23521Pv.A00(uri).A02();
        C61832zZ r12 = r1.A09;
        r12.A0Q(CallerContext.A04(C33361nS.class));
        r12.A09(fbDraweeView.A05());
        r12.A0A(A022);
        fbDraweeView.A08(r12.A0F());
    }

    public int ArU() {
        return this.A07.size();
    }

    private C33361nS(AnonymousClass1XY r3) {
        this.A05 = C73473g8.A00(r3);
        this.A03 = C26564D2a.A00(r3);
        this.A06 = C22341Ky.A00(r3);
        this.A09 = C61832zZ.A00(r3);
        this.A0A = C624531u.A00(r3);
        this.A04 = new D76(r3);
        this.A01 = C04490Ux.A0L(r3);
        this.A02 = AnonymousClass1MT.A00(r3);
    }
}
