package X;

import com.google.common.collect.MapMakerInternalMap;

/* renamed from: X.1a2  reason: invalid class name and case insensitive filesystem */
public final class C25561a2 implements C25571a3 {
    public static final C25561a2 A00 = new C25561a2();

    public C07740e5 AUU(MapMakerInternalMap.Segment segment, C07740e5 r6, C07740e5 r7) {
        MapMakerInternalMap.WeakKeyStrongValueSegment weakKeyStrongValueSegment = (MapMakerInternalMap.WeakKeyStrongValueSegment) segment;
        C07750e6 r62 = (C07750e6) r6;
        C07750e6 r72 = (C07750e6) r7;
        if (r62.getKey() == null) {
            return null;
        }
        C07750e6 r2 = new C07750e6(weakKeyStrongValueSegment.queueForKeys, r62.getKey(), r62.A01, r72);
        r2.A00 = r62.A00;
        return r2;
    }

    public MapMakerInternalMap.Strength BHg() {
        return MapMakerInternalMap.Strength.WEAK;
    }

    public C07740e5 BLe(MapMakerInternalMap.Segment segment, Object obj, int i, C07740e5 r6) {
        return new C07750e6(((MapMakerInternalMap.WeakKeyStrongValueSegment) segment).queueForKeys, obj, i, (C07750e6) r6);
    }

    public MapMakerInternalMap.Segment BLh(MapMakerInternalMap mapMakerInternalMap, int i, int i2) {
        return new MapMakerInternalMap.WeakKeyStrongValueSegment(mapMakerInternalMap, i, i2);
    }

    public void CCs(MapMakerInternalMap.Segment segment, C07740e5 r2, Object obj) {
        ((C07750e6) r2).A00 = obj;
    }

    public MapMakerInternalMap.Strength CLa() {
        return MapMakerInternalMap.Strength.STRONG;
    }
}
