package X;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

/* renamed from: X.0z4  reason: invalid class name and case insensitive filesystem */
public final class C17540z4 {
    public final Resources A00;
    public final C17510z1 A01;
    private final Resources.Theme A02;

    public int A00(float f) {
        return AnonymousClass1K3.A00(f * this.A00.getDisplayMetrics().density);
    }

    public int A01(float f) {
        return AnonymousClass1K3.A00(f * this.A00.getDisplayMetrics().scaledDensity);
    }

    public int A02(int i) {
        if (i == 0) {
            return 0;
        }
        Integer num = (Integer) this.A01.A00(i);
        if (num != null) {
            return num.intValue();
        }
        int color = this.A00.getColor(i);
        this.A01.A01(i, Integer.valueOf(color));
        return color;
    }

    public int A03(int i) {
        if (i == 0) {
            return 0;
        }
        Integer num = (Integer) this.A01.A00(i);
        if (num != null) {
            return num.intValue();
        }
        int dimensionPixelSize = this.A00.getDimensionPixelSize(i);
        this.A01.A01(i, Integer.valueOf(dimensionPixelSize));
        return dimensionPixelSize;
    }

    public int A04(int i, int i2) {
        TypedArray obtainStyledAttributes = this.A02.obtainStyledAttributes(new int[]{i});
        try {
            return obtainStyledAttributes.getColor(0, A02(i2));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public int A05(int i, int i2) {
        TypedArray obtainStyledAttributes = this.A02.obtainStyledAttributes(new int[]{i});
        try {
            return obtainStyledAttributes.getDimensionPixelSize(0, A03(i2));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public final int A06(int i, int i2) {
        TypedArray obtainStyledAttributes = this.A02.obtainStyledAttributes(new int[]{i});
        try {
            return obtainStyledAttributes.getResourceId(0, i2);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public Drawable A07(int i) {
        if (i == 0) {
            return null;
        }
        return this.A00.getDrawable(i);
    }

    public Drawable A08(int i, int i2) {
        if (i == 0) {
            return null;
        }
        TypedArray obtainStyledAttributes = this.A02.obtainStyledAttributes(new int[]{i});
        try {
            return A07(obtainStyledAttributes.getResourceId(0, i2));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public String A09(int i) {
        if (i == 0) {
            return null;
        }
        String str = (String) this.A01.A00(i);
        if (str != null) {
            return str;
        }
        String string = this.A00.getString(i);
        this.A01.A01(i, string);
        return string;
    }

    public C17540z4(AnonymousClass0p4 r2) {
        this.A00 = r2.A09.getResources();
        this.A02 = r2.A09.getTheme();
        this.A01 = r2.A0A;
    }
}
