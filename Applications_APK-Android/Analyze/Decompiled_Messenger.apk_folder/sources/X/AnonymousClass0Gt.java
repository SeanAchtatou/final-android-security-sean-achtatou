package X;

/* renamed from: X.0Gt  reason: invalid class name */
public final class AnonymousClass0Gt implements C02780Gi {
    public void C2x(AnonymousClass0FM r8, C02910Gx r9) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9;
        boolean z10;
        boolean z11;
        boolean z12;
        boolean z13;
        boolean z14;
        C02600Fo r82 = (C02600Fo) r8;
        int i = r82.mqttFullPowerTimeS;
        if (i == 0) {
            z = false;
        } else {
            r9.AMU("mqtt_full_power_time_s", i);
            z = true;
        }
        boolean z15 = z | false;
        int i2 = r82.mqttLowPowerTimeS;
        if (i2 == 0) {
            z2 = false;
        } else {
            r9.AMU("mqtt_low_power_time_s", i2);
            z2 = true;
        }
        boolean z16 = z15 | z2;
        long j = r82.mqttTxBytes;
        if (j == 0) {
            z3 = false;
        } else {
            r9.AMV("mqtt_tx_bytes", j);
            z3 = true;
        }
        boolean z17 = z16 | z3;
        long j2 = r82.mqttRxBytes;
        if (j2 == 0) {
            z4 = false;
        } else {
            r9.AMV("mqtt_rx_bytes", j2);
            z4 = true;
        }
        boolean z18 = z17 | z4;
        int i3 = r82.mqttRequestCount;
        if (i3 == 0) {
            z5 = false;
        } else {
            r9.AMU("mqtt_request_count", i3);
            z5 = true;
        }
        boolean z19 = z18 | z5;
        int i4 = r82.mqttWakeupCount;
        if (i4 == 0) {
            z6 = false;
        } else {
            r9.AMU("mqtt_wakeup_count", i4);
            z6 = true;
        }
        boolean z20 = z19 | z6;
        int i5 = r82.ligerFullPowerTimeS;
        if (i5 == 0) {
            z7 = false;
        } else {
            r9.AMU("liger_full_power_time_s", i5);
            z7 = true;
        }
        boolean z21 = z20 | z7;
        int i6 = r82.ligerLowPowerTimeS;
        if (i6 == 0) {
            z8 = false;
        } else {
            r9.AMU("liger_low_power_time_s", i6);
            z8 = true;
        }
        boolean z22 = z21 | z8;
        long j3 = r82.ligerTxBytes;
        if (j3 == 0) {
            z9 = false;
        } else {
            r9.AMV("liger_tx_bytes", j3);
            z9 = true;
        }
        boolean z23 = z22 | z9;
        long j4 = r82.ligerRxBytes;
        if (j4 == 0) {
            z10 = false;
        } else {
            r9.AMV("liger_rx_bytes", j4);
            z10 = true;
        }
        boolean z24 = z23 | z10;
        int i7 = r82.ligerRequestCount;
        if (i7 == 0) {
            z11 = false;
        } else {
            r9.AMU("liger_request_count", i7);
            z11 = true;
        }
        boolean z25 = z24 | z11;
        int i8 = r82.ligerWakeupCount;
        if (i8 == 0) {
            z12 = false;
        } else {
            r9.AMU("liger_wakeup_count", i8);
            z12 = true;
        }
        boolean z26 = z25 | z12;
        int i9 = r82.proxygenActiveRadioTimeS;
        if (i9 == 0) {
            z13 = false;
        } else {
            r9.AMU("proxygen_active_radio_time_s", i9);
            z13 = true;
        }
        boolean z27 = z26 | z13;
        int i10 = r82.proxygenTailRadioTimeS;
        if (i10 == 0) {
            z14 = false;
        } else {
            r9.AMU("proxygen_tail_radio_time_s", i10);
            z14 = true;
        }
        if (z14 || z27) {
            r9.AMU("proxygen_meter_version", 1);
        }
    }
}
