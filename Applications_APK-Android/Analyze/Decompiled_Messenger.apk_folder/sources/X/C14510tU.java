package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0tU  reason: invalid class name and case insensitive filesystem */
public final class C14510tU implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0tR A00;

    public C14510tU(AnonymousClass0tR r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r11) {
        ImmutableList immutableList;
        int A002 = AnonymousClass09Y.A00(-885216178);
        AnonymousClass0tR r3 = this.A00;
        boolean A01 = AnonymousClass0tR.A01(r3);
        if (r3.A01 != A01) {
            r3.A01 = A01;
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, r3.A03.A00)).A01("inbox_ad_zero_rating_change"), AnonymousClass1Y3.A2G);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A08("is_ad_unit_hidden", Boolean.valueOf(A01));
                uSLEBaseShape0S0000000.A0O("messenger_inbox_ads");
                uSLEBaseShape0S0000000.A06();
            }
            C35211qr r7 = r3.A00;
            if (r7 != null) {
                C13880sE r0 = r7.A00.A0H;
                if (r0 == null) {
                    r0 = C13880sE.A07;
                }
                AnonymousClass101 r02 = r0.A03;
                boolean z = false;
                if (r02 != null && (immutableList = r02.A03) != null) {
                    int size = immutableList.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (((AnonymousClass12V) immutableList.get(i)).A00 == GraphQLMessengerInboxUnitType.A0F) {
                            z = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (z) {
                    AnonymousClass16R.A04(r7.A00, AnonymousClass07B.A0C, "Inbox ads zero rating change");
                }
            }
        }
        AnonymousClass09Y.A01(1723677519, A002);
    }
}
