package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1V7  reason: invalid class name */
public final class AnonymousClass1V7 extends Enum {
    private static final ImmutableMap A00;
    private static final /* synthetic */ AnonymousClass1V7[] A01;
    public static final AnonymousClass1V7 A02;
    public static final AnonymousClass1V7 A03;
    public static final AnonymousClass1V7 A04;
    public static final AnonymousClass1V7 A05;
    public static final AnonymousClass1V7 A06;
    public static final AnonymousClass1V7 A07;
    public static final AnonymousClass1V7 A08;
    public static final AnonymousClass1V7 A09;
    public static final AnonymousClass1V7 A0A;
    public static final AnonymousClass1V7 A0B;
    public static final AnonymousClass1V7 A0C;
    public static final AnonymousClass1V7 A0D;
    public static final AnonymousClass1V7 A0E;
    public static final AnonymousClass1V7 A0F;
    public static final AnonymousClass1V7 A0G;
    public static final AnonymousClass1V7 A0H;
    public static final AnonymousClass1V7 A0I;
    public static final AnonymousClass1V7 A0J;
    public static final AnonymousClass1V7 A0K;
    public static final AnonymousClass1V7 A0L;
    public static final AnonymousClass1V7 A0M;
    public static final AnonymousClass1V7 A0N;
    public static final AnonymousClass1V7 A0O;
    public static final AnonymousClass1V7 A0P;
    public static final AnonymousClass1V7 A0Q;
    public static final AnonymousClass1V7 A0R;
    public static final AnonymousClass1V7 A0S;
    public static final AnonymousClass1V7 A0T;
    public final int dbKeyValue;

    static {
        AnonymousClass1V7 r0 = new AnonymousClass1V7("BEFORE_FIRST_SENTINEL", 0, -1);
        A05 = r0;
        AnonymousClass1V7 r02 = new AnonymousClass1V7("REGULAR", 1, 0);
        A0K = r02;
        AnonymousClass1V7 r03 = new AnonymousClass1V7("ADD_MEMBERS", 2, 1);
        A03 = r03;
        AnonymousClass1V7 r04 = new AnonymousClass1V7("REMOVE_MEMBERS", 3, 2);
        A0M = r04;
        AnonymousClass1V7 r05 = new AnonymousClass1V7("SET_NAME", 4, 3);
        A0O = r05;
        AnonymousClass1V7 r06 = new AnonymousClass1V7("SET_IMAGE", 5, 4);
        A0N = r06;
        AnonymousClass1V7 r07 = new AnonymousClass1V7("VIDEO_CALL", 6, 5);
        A0T = r07;
        AnonymousClass1V7 r08 = new AnonymousClass1V7("MISSED_VIDEO_CALL", 7, 6);
        A0E = r08;
        AnonymousClass1V7 r09 = new AnonymousClass1V7("REMOVED_IMAGE", 8, 7);
        A0L = r09;
        AnonymousClass1V7 r13 = new AnonymousClass1V7("ADMIN", 9, 8);
        A04 = r13;
        AnonymousClass1V7 r12 = new AnonymousClass1V7("CALL_LOG", 10, 9);
        A07 = r12;
        AnonymousClass1V7 r010 = new AnonymousClass1V7(AnonymousClass24B.$const$string(44), 11, 50);
        A0G = r010;
        AnonymousClass1V7 r5 = new AnonymousClass1V7("P2P_PAYMENT_CANCELED", 12, 51);
        A0H = r5;
        AnonymousClass1V7 r52 = new AnonymousClass1V7("P2P_PAYMENT_GROUP", 13, 52);
        A0I = r52;
        AnonymousClass1V7 r53 = new AnonymousClass1V7(AnonymousClass80H.$const$string(AnonymousClass1Y3.A25), 14, 100);
        A0C = r53;
        AnonymousClass1V7 r54 = new AnonymousClass1V7("MISSED_CALL", 15, AnonymousClass1Y3.A0i);
        A0D = r54;
        AnonymousClass1V7 r14 = new AnonymousClass1V7("OUTGOING_CALL", 16, AnonymousClass1Y3.A0j);
        A0F = r14;
        AnonymousClass1V7 r11 = new AnonymousClass1V7(C22298Ase.$const$string(AnonymousClass1Y3.A0s), 17, AnonymousClass1Y3.A1B);
        A08 = r11;
        AnonymousClass1V7 r10 = new AnonymousClass1V7(C22298Ase.$const$string(AnonymousClass1Y3.A0t), 18, 151);
        A09 = r10;
        AnonymousClass1V7 r9 = new AnonymousClass1V7(C22298Ase.$const$string(AnonymousClass1Y3.A0o), 19, 152);
        A02 = r9;
        AnonymousClass1V7 r8 = new AnonymousClass1V7("TELEPHONE_CALL_LOG", 20, AnonymousClass1Y3.A1c);
        A0R = r8;
        AnonymousClass1V7 r7 = new AnonymousClass1V7("SMS_LOG", 21, AnonymousClass1Y3.A1d);
        A0P = r7;
        AnonymousClass1V7 r6 = new AnonymousClass1V7("SMS_MATCH", 22, 202);
        A0Q = r6;
        AnonymousClass1V7 r3 = new AnonymousClass1V7("GROUP_CALL_LOG", 23, AnonymousClass1Y3.A1e);
        A0B = r3;
        AnonymousClass1V7 r2 = new AnonymousClass1V7("BLOCKED", 24, 300);
        A06 = r2;
        AnonymousClass1V7 r1 = new AnonymousClass1V7("PENDING_SEND", 25, AnonymousClass1Y3.A7F);
        A0J = r1;
        AnonymousClass1V7 r32 = new AnonymousClass1V7("FAILED_SEND", 26, 901);
        A0A = r32;
        AnonymousClass1V7 r322 = new AnonymousClass1V7("UNKNOWN", 27, AnonymousClass1Y3.A87);
        A0S = r322;
        AnonymousClass1V7[] r55 = new AnonymousClass1V7[28];
        System.arraycopy(new AnonymousClass1V7[]{r0, r02, r03, r04, r05, r06, r07, r08, r09, r13, r12, r010, r5, r52, r53, r54, r14, r11, r10, r9, r8, r7, r6, r3, r2, r1, r32}, 0, r55, 0, 27);
        System.arraycopy(new AnonymousClass1V7[]{r322}, 0, r55, 27, 1);
        A01 = r55;
        ImmutableMap.Builder builder = ImmutableMap.builder();
        for (AnonymousClass1V7 r15 : values()) {
            builder.put(Integer.valueOf(r15.dbKeyValue), r15);
        }
        A00 = builder.build();
    }

    public static AnonymousClass1V7 A00(int i) {
        ImmutableMap immutableMap = A00;
        Integer valueOf = Integer.valueOf(i);
        Object obj = immutableMap.get(valueOf);
        if (obj != null) {
            return (AnonymousClass1V7) obj;
        }
        throw new NullPointerException(Preconditions.format("Unknown db key value %s", valueOf));
    }

    public static AnonymousClass1V7 valueOf(String str) {
        return (AnonymousClass1V7) Enum.valueOf(AnonymousClass1V7.class, str);
    }

    public static AnonymousClass1V7[] values() {
        return (AnonymousClass1V7[]) A01.clone();
    }

    private AnonymousClass1V7(String str, int i, int i2) {
        this.dbKeyValue = i2;
    }
}
