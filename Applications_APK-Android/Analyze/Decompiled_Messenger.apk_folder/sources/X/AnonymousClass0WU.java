package X;

import android.content.Context;
import com.facebook.gk.sessionless.GkSessionlessModule;
import com.facebook.inject.InjectorModule;
import com.facebook.mobileconfig.MobileConfigCoreParams;
import com.facebook.mobileconfig.MobileConfigManagerParamsHolder;
import java.util.Random;

@InjectorModule
/* renamed from: X.0WU  reason: invalid class name */
public final class AnonymousClass0WU extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static volatile C25051Yd A02;
    private static volatile C25051Yd A03;

    public static final Boolean A03() {
        return false;
    }

    public static final C25051Yd A01(AnonymousClass1XY r13) {
        if (A02 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r13);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r13.getApplicationInjector();
                        C25031Yb A012 = C25021Ya.A01(applicationInjector);
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        C04310Tq A022 = AnonymousClass0WY.A02(applicationInjector);
                        AnonymousClass0UQ A004 = AnonymousClass0UQ.A00(AnonymousClass1Y3.AcD, applicationInjector);
                        AnonymousClass0WZ A005 = AnonymousClass0WZ.A00(applicationInjector);
                        AnonymousClass0X5 r6 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A1m);
                        AnonymousClass0UQ A006 = AnonymousClass0UQ.A00(AnonymousClass1Y3.B6N, applicationInjector);
                        C04760Wb r1 = new C04760Wb(applicationInjector);
                        AnonymousClass0VB A007 = AnonymousClass0VB.A00(AnonymousClass1Y3.BTF, applicationInjector);
                        C04780Wd r2 = new C04780Wd(A022);
                        C25041Yc r3 = new C25041Yc(A012, r1, AnonymousClass08X.A07(A003, "useTranslationTablePerJavaManager"));
                        r3.A03 = A003.getApplicationContext().getFilesDir();
                        r3.A00 = A003.getAssets();
                        r3.A02 = r2;
                        r3.A04 = A004;
                        r3.A0K = new C04790We(A005);
                        boolean A04 = A04(A003);
                        r3.A0M = A006;
                        r3.A06 = A04;
                        if (!r6.isEmpty()) {
                            r3.A01 = (C25071Yf) r6.iterator().next();
                        }
                        r3.A05 = A007;
                        boolean z = false;
                        if (r3.A02 == null) {
                            z = true;
                        }
                        r3.A07 = z;
                        if (r3.A01 == null) {
                            r3.A01 = new C72913fC();
                        }
                        A02 = r3;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final C25051Yd A02(AnonymousClass1XY r11) {
        if (A03 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        C25031Yb A022 = C25021Ya.A02(applicationInjector);
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        AnonymousClass0WZ A004 = AnonymousClass0WZ.A00(applicationInjector);
                        AnonymousClass0X5 r5 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A1m);
                        AnonymousClass0UQ A005 = AnonymousClass0UQ.A00(AnonymousClass1Y3.B6N, applicationInjector);
                        AnonymousClass0VB A006 = AnonymousClass0VB.A00(AnonymousClass1Y3.BTF, applicationInjector);
                        C25041Yc r3 = new C25041Yc(A022, null, false);
                        r3.A03 = A003.getApplicationContext().getFilesDir();
                        r3.A00 = A003.getAssets();
                        r3.A0K = new C05990af(A004);
                        if (A04(A003)) {
                            r3.A0M = A005;
                            r3.A06 = true;
                        }
                        if (!r5.isEmpty()) {
                            r3.A01 = (C25071Yf) r5.iterator().next();
                        }
                        r3.A05 = A006;
                        boolean z = false;
                        if (r3.A02 == null) {
                            z = true;
                        }
                        r3.A07 = z;
                        if (r3.A01 == null) {
                            r3.A01 = new C72913fC();
                        }
                        A03 = r3;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private static boolean A04(Context context) {
        if (new Random().nextInt(60) != 0) {
            return false;
        }
        C46782Rs r8 = new C46782Rs(context);
        int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
        int A002 = r8.A00("api2", 0);
        if (A002 == 0) {
            r8.A01("api2", currentTimeMillis - ((int) (Math.random() * ((double) 5184000))));
            return false;
        } else if (A002 + 5184000 > currentTimeMillis) {
            return false;
        } else {
            r8.A01("api2", currentTimeMillis);
            return true;
        }
    }

    public static final MobileConfigManagerParamsHolder A00(AnonymousClass1XY r9) {
        AnonymousClass1YI A002 = GkSessionlessModule.A00(r9);
        Context A003 = AnonymousClass1YA.A00(r9);
        Boolean bool = false;
        boolean AbO = A002.AbO(123, false);
        boolean AbO2 = A002.AbO(AnonymousClass1Y3.A0s, false);
        long A004 = (long) AnonymousClass08X.A00(A003, "consistencyLoggingInterval", 2592000);
        boolean AbO3 = A002.AbO(AnonymousClass1Y3.A0w, false);
        MobileConfigManagerParamsHolder mobileConfigManagerParamsHolder = new MobileConfigManagerParamsHolder();
        mobileConfigManagerParamsHolder.setOmnistoreUpdaterExpected(AbO);
        mobileConfigManagerParamsHolder.setConsistencyLoggingEveryNSec(A004);
        mobileConfigManagerParamsHolder.setShouldUseOTAResource(AbO3);
        mobileConfigManagerParamsHolder.setUsePartialAndFullSyncFetch(true);
        new MobileConfigCoreParams().setIsCanaryEnabled(AbO2);
        if (bool.booleanValue()) {
            mobileConfigManagerParamsHolder.setQueryHashOptimization(false);
        }
        return mobileConfigManagerParamsHolder;
    }
}
