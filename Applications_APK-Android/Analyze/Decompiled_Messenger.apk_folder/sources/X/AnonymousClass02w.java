package X;

/* renamed from: X.02w  reason: invalid class name */
public final class AnonymousClass02w {
    public static AnonymousClass02v A00 = AnonymousClass02x.A01;

    public static void A09(Class cls, Throwable th, String str, Object... objArr) {
        if (A0G(5)) {
            A04(cls, String.format(null, str, objArr), th);
        }
    }

    public static void A00(Class cls, String str) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.AY3(cls.getSimpleName(), str);
        }
    }

    public static void A01(Class cls, String str) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(5)) {
            r1.CMp(cls.getSimpleName(), str);
        }
    }

    public static void A02(Class cls, String str) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.CNm(cls.getSimpleName(), str);
        }
    }

    public static void A03(Class cls, String str, Throwable th) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.AY4(cls.getSimpleName(), str, th);
        }
    }

    public static void A04(Class cls, String str, Throwable th) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(5)) {
            r1.CMq(cls.getSimpleName(), str, th);
        }
    }

    public static void A05(Class cls, String str, Throwable th) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.CNn(cls.getSimpleName(), str, th);
        }
    }

    public static void A06(Class cls, String str, Object... objArr) {
        AnonymousClass02v r2 = A00;
        if (r2.BFj(6)) {
            r2.AY3(cls.getSimpleName(), String.format(null, str, objArr));
        }
    }

    public static void A07(Class cls, String str, Object... objArr) {
        AnonymousClass02v r2 = A00;
        if (r2.BFj(5)) {
            r2.CMp(cls.getSimpleName(), String.format(null, str, objArr));
        }
    }

    public static void A08(Class cls, String str, Object... objArr) {
        AnonymousClass02v r2 = A00;
        if (r2.BFj(6)) {
            r2.CNm(cls.getSimpleName(), String.format(null, str, objArr));
        }
    }

    public static void A0A(String str, String str2) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.AY3(str, str2);
        }
    }

    public static void A0B(String str, String str2) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(5)) {
            r1.CMp(str, str2);
        }
    }

    public static void A0C(String str, String str2) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.CNm(str, str2);
        }
    }

    public static void A0D(String str, String str2, Throwable th) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.AY4(str, str2, th);
        }
    }

    public static void A0E(String str, String str2, Object... objArr) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(5)) {
            r1.CMp(str, String.format(null, str2, objArr));
        }
    }

    public static void A0F(String str, String str2, Object... objArr) {
        AnonymousClass02v r1 = A00;
        if (r1.BFj(6)) {
            r1.CNm(str, String.format(null, str2, objArr));
        }
    }

    public static boolean A0G(int i) {
        return A00.BFj(i);
    }
}
