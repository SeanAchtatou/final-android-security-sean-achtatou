package X;

import android.view.View;
import com.facebook.messaging.composer.triggers.mentions.MentionsSearchResultsView;
import com.facebook.messaging.threadview.scheme.interfaces.ThreadViewColorScheme;

/* renamed from: X.206  reason: invalid class name */
public final class AnonymousClass206 implements AnonymousClass3DM {
    public final /* synthetic */ C67563Pi A00;

    public AnonymousClass206(C67563Pi r1) {
        this.A00 = r1;
    }

    public /* bridge */ /* synthetic */ void Bb9(View view) {
        MentionsSearchResultsView mentionsSearchResultsView = (MentionsSearchResultsView) view;
        ThreadViewColorScheme threadViewColorScheme = this.A00.A07;
        C79983rd r0 = mentionsSearchResultsView.A00;
        r0.A02 = threadViewColorScheme;
        r0.A04();
        mentionsSearchResultsView.A00.A01 = new DRA(this);
    }
}
