package X;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ij  reason: invalid class name and case insensitive filesystem */
public final class C09810ij implements AnonymousClass1YQ {
    private static volatile C09810ij A02;
    public AnonymousClass0UN A00;
    public volatile List A01 = new ArrayList();

    public String getSimpleName() {
        return "DelayedLoggerImpl";
    }

    public static final C09810ij A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C09810ij.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C09810ij(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public void A01(String str, boolean z) {
        if (this.A01 != null) {
            synchronized (this) {
                if (this.A01 != null) {
                    this.A01.add(new C09900is(str, z, ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A00)).now()));
                    return;
                }
            }
        }
        int i = AnonymousClass1Y3.AQl;
        C08900gA r2 = C87224Dv.A00;
        ((C185414b) AnonymousClass1XX.A02(0, i, this.A00)).AOI(r2, str);
        if (z) {
            ((C185414b) AnonymousClass1XX.A02(0, i, this.A00)).AYS(r2);
        }
    }

    private C09810ij(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        r4 = 0;
        r3 = r5.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        if (r4 >= r3) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002c, code lost:
        r7 = (X.C09900is) r5.get(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0034, code lost:
        if (r7 != X.C09900is.A03) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        ((X.C185414b) X.AnonymousClass1XX.A02(0, X.AnonymousClass1Y3.AQl, r15.A00)).CH1(X.C87224Dv.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0046, code lost:
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        r1 = X.AnonymousClass1Y3.AQl;
        r9 = X.C87224Dv.A00;
        ((X.C185414b) X.AnonymousClass1XX.A02(0, r1, r15.A00)).AOP(r9, r7.A01, null, null, r7.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0061, code lost:
        if (r7.A02 == false) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0063, code lost:
        ((X.C185414b) X.AnonymousClass1XX.A02(0, r1, r15.A00)).AYS(r9);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void init() {
        /*
            r15 = this;
            r0 = 306603269(0x12466505, float:6.26023E-28)
            int r2 = X.C000700l.A03(r0)
        L_0x0007:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            monitor-enter(r15)
            java.util.List r0 = r15.A01     // Catch:{ all -> 0x0081 }
            if (r0 != 0) goto L_0x0013
            monitor-exit(r15)     // Catch:{ all -> 0x0081 }
            goto L_0x0073
        L_0x0013:
            java.util.List r0 = r15.A01     // Catch:{ all -> 0x0081 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0081 }
            if (r0 == 0) goto L_0x0020
            r0 = 0
            r15.A01 = r0     // Catch:{ all -> 0x0081 }
            monitor-exit(r15)     // Catch:{ all -> 0x0081 }
            goto L_0x007a
        L_0x0020:
            java.util.List r5 = r15.A01     // Catch:{ all -> 0x0081 }
            r15.A01 = r1     // Catch:{ all -> 0x0081 }
            monitor-exit(r15)     // Catch:{ all -> 0x0081 }
            r4 = 0
            int r3 = r5.size()
        L_0x002a:
            if (r4 >= r3) goto L_0x006f
            java.lang.Object r7 = r5.get(r4)
            X.0is r7 = (X.C09900is) r7
            X.0is r0 = X.C09900is.A03
            if (r7 != r0) goto L_0x0049
            int r6 = X.AnonymousClass1Y3.AQl
            X.0UN r1 = r15.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r6, r1)
            X.14b r1 = (X.C185414b) r1
            X.0gA r0 = X.C87224Dv.A00
            r1.CH1(r0)
        L_0x0046:
            int r4 = r4 + 1
            goto L_0x002a
        L_0x0049:
            int r1 = X.AnonymousClass1Y3.AQl
            X.0UN r0 = r15.A00
            r6 = 0
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.14b r8 = (X.C185414b) r8
            X.0gA r9 = X.C87224Dv.A00
            java.lang.String r10 = r7.A01
            long r13 = r7.A00
            r11 = 0
            r12 = 0
            r8.AOP(r9, r10, r11, r12, r13)
            boolean r0 = r7.A02
            if (r0 == 0) goto L_0x0046
            X.0UN r0 = r15.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.14b r1 = (X.C185414b) r1
            r1.AYS(r9)
            goto L_0x0046
        L_0x006f:
            r5.clear()
            goto L_0x0007
        L_0x0073:
            r0 = -203090530(0xfffffffff3e5159e, float:-3.629988E31)
            X.C000700l.A09(r0, r2)
            return
        L_0x007a:
            r0 = 1647819299(0x6237ba23, float:8.472917E20)
            X.C000700l.A09(r0, r2)
            return
        L_0x0081:
            r1 = move-exception
            monitor-exit(r15)     // Catch:{ all -> 0x0081 }
            r0 = 2067611637(0x7b3d3ff5, float:9.826413E35)
            X.C000700l.A09(r0, r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09810ij.init():void");
    }
}
