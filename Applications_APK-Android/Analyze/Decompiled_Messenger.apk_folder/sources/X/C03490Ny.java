package X;

import android.util.SparseIntArray;
import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0Ny  reason: invalid class name and case insensitive filesystem */
public final class C03490Ny extends C03160Kg {
    public long A00() {
        return -1864103899603750951L;
    }

    public void A01(AnonymousClass0FM r7, DataOutput dataOutput) {
        AnonymousClass0FX r72 = (AnonymousClass0FX) r7;
        dataOutput.writeInt(r5);
        for (SparseIntArray sparseIntArray : r72.timeInStateS) {
            int size = sparseIntArray.size();
            dataOutput.writeInt(size);
            for (int i = 0; i < size; i++) {
                dataOutput.writeInt(sparseIntArray.keyAt(i));
                dataOutput.writeInt(sparseIntArray.valueAt(i));
            }
        }
    }

    public boolean A03(AnonymousClass0FM r8, DataInput dataInput) {
        AnonymousClass0FX r82 = (AnonymousClass0FX) r8;
        int readInt = dataInput.readInt();
        if (r82.timeInStateS.length != readInt) {
            return false;
        }
        for (int i = 0; i < readInt; i++) {
            int readInt2 = dataInput.readInt();
            for (int i2 = 0; i2 < readInt2; i2++) {
                r82.timeInStateS[i].put(dataInput.readInt(), dataInput.readInt());
            }
        }
        return true;
    }
}
