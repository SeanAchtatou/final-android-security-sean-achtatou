package X;

import android.support.v4.app.INotificationSideChannel;

/* renamed from: X.0uz  reason: invalid class name and case insensitive filesystem */
public final class C15260uz implements C32461ln {
    public final int A00;
    public final String A01;
    public final String A02;

    public void C4w(INotificationSideChannel iNotificationSideChannel) {
        iNotificationSideChannel.ARc(this.A01, this.A00, this.A02);
    }

    public String toString() {
        return "CancelTask[" + "packageName:" + this.A01 + ", id:" + this.A00 + ", tag:" + this.A02 + ", all:" + false + "]";
    }

    public C15260uz(String str, int i, String str2) {
        this.A01 = str;
        this.A00 = i;
        this.A02 = str2;
    }
}
