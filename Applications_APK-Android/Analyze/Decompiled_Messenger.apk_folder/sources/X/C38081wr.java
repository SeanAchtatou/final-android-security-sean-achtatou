package X;

import android.net.Uri;

/* renamed from: X.1wr  reason: invalid class name and case insensitive filesystem */
public final class C38081wr {
    public Uri A00;
    public String A01;

    public boolean equals(Object obj) {
        if (!(obj instanceof C38081wr)) {
            return false;
        }
        return this.A00.equals(((C38081wr) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        return this.A01 + ", " + this.A00;
    }

    public C38081wr(String str, Uri uri) {
        this.A01 = str;
        this.A00 = uri;
    }
}
