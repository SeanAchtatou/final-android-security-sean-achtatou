package X;

import io.card.payment.BuildConfig;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0Sd  reason: invalid class name and case insensitive filesystem */
public final class C04140Sd {
    public final boolean A00;
    private final C04130Sb A01;
    private final C01850Bw A02;
    private final C01880Bz A03;
    private final AnonymousClass0Mo A04;
    private final C02050Cq A05;
    private final AnonymousClass08I A06;
    private final AnonymousClass0C2 A07;
    private final AnonymousClass0C1 A08;
    private final boolean A09;

    public final String toString() {
        try {
            return A00(this, false).toString();
        } catch (JSONException unused) {
            return BuildConfig.FLAVOR;
        }
    }

    public static JSONObject A00(C04140Sd r5, boolean z) {
        JSONObject jSONObject = new JSONObject();
        AnonymousClass0Mo r2 = r5.A04;
        if (r2 != null) {
            jSONObject.putOpt(r2.A00, r2.A01(z, r5.A09));
        }
        AnonymousClass08I r22 = r5.A06;
        if (r22 != null) {
            jSONObject.putOpt(r22.A00, r22.A01(z, r5.A09));
        }
        C01850Bw r23 = r5.A02;
        if (r23 != null) {
            jSONObject.putOpt(r23.A00, r23.A01(z, r5.A09));
        }
        C02050Cq r24 = r5.A05;
        if (r24 != null) {
            jSONObject.putOpt(r24.A00, r24.A01(z, r5.A09));
        }
        C04130Sb r4 = r5.A01;
        if (r4 != null) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.putOpt("ssr", r4.A03);
            jSONObject2.putOpt("mcd", Long.valueOf(r4.A00));
            jSONObject2.putOpt("mfcl", Long.valueOf(r4.A02));
            jSONObject2.putOpt("mcg", Long.valueOf(r4.A01));
            jSONObject.putOpt("ss", jSONObject2);
        }
        C01880Bz r25 = r5.A03;
        if (r25 != null) {
            jSONObject.putOpt(r25.A03, r25.A02(r5.A09));
        }
        AnonymousClass0C1 r26 = r5.A08;
        if (r26 != null) {
            jSONObject.putOpt(r26.A03, r26.A02(r5.A09));
        }
        AnonymousClass0C2 r27 = r5.A07;
        if (r27 != null) {
            jSONObject.putOpt(r27.A03, r27.A02(r5.A09));
        }
        return jSONObject;
    }

    public C04140Sd(AnonymousClass0Mo r1, AnonymousClass08I r2, C01850Bw r3, C02050Cq r4, C04130Sb r5, C01880Bz r6, AnonymousClass0C1 r7, AnonymousClass0C2 r8, boolean z, boolean z2) {
        this.A04 = r1;
        this.A06 = r2;
        this.A02 = r3;
        this.A05 = r4;
        this.A01 = r5;
        this.A03 = r6;
        this.A08 = r7;
        this.A07 = r8;
        this.A00 = z;
        this.A09 = z2;
    }
}
