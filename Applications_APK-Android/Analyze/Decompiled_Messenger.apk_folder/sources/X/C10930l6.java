package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.0l6  reason: invalid class name and case insensitive filesystem */
public final class C10930l6 implements C05700aB {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) A05.A09("config_server"));
    public static final AnonymousClass1Y7 A03 = ((AnonymousClass1Y7) A06.A09("mqtt_data_restriction_history"));
    public static final AnonymousClass1Y8 A04 = C04350Ue.A0B.A09("mqtt/").A09("mqtt_connect_hash");
    private static final AnonymousClass1Y7 A05 = ((AnonymousClass1Y7) C04350Ue.A00.A09("mqtt/"));
    private static final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) C04350Ue.A06.A09("mqtt/"));

    static {
        AnonymousClass1Y7 r1 = C04350Ue.A05;
        A01 = (AnonymousClass1Y7) ((AnonymousClass1Y7) r1.A09("fbns/")).A09("token");
        A00 = (AnonymousClass1Y7) ((AnonymousClass1Y7) r1.A09("fbns_lite/")).A09("token");
    }

    public static final C10930l6 A00() {
        return new C10930l6();
    }

    public ImmutableSet AzO() {
        return ImmutableSet.A04(A04);
    }
}
