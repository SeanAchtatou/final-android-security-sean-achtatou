package X;

import android.content.Context;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: X.05m  reason: invalid class name */
public final class AnonymousClass05m {
    public static final FilenameFilter A09 = new C005105s();
    public static final FilenameFilter A0A = new C005205t();
    public int A00 = 0;
    public long A01 = 0;
    public AnonymousClass05o A02 = new AnonymousClass05o();
    public File A03;
    public File A04;
    public File A05;
    public File A06;
    public boolean A07 = false;
    private Context A08;

    public static boolean A04(AnonymousClass05m r3, File file, File file2) {
        if (file2 != null) {
            if (file.renameTo(file2)) {
                return true;
            }
            r3.A02.A03++;
        }
        if (!file.exists() || file.delete()) {
            return false;
        }
        r3.A02.A02++;
        return false;
    }

    public static File A00(AnonymousClass05m r3) {
        File cacheDir = r3.A08.getCacheDir();
        File filesDir = r3.A08.getFilesDir();
        if (cacheDir == null || (!cacheDir.exists() && !cacheDir.mkdirs())) {
            return filesDir;
        }
        return cacheDir;
    }

    public Iterable A05() {
        List asList;
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(A01(this.A06, A0A));
        arrayList.addAll(A01(this.A06, A09));
        arrayList.addAll(A01(this.A03, A0A));
        arrayList.addAll(A01(this.A03, A09));
        File[] listFiles = this.A04.listFiles();
        if (listFiles == null) {
            asList = Collections.EMPTY_LIST;
        } else {
            asList = Arrays.asList(listFiles);
        }
        arrayList.addAll(asList);
        return arrayList;
    }

    public AnonymousClass05m(Context context, File file) {
        Context applicationContext = context.getApplicationContext();
        this.A08 = applicationContext;
        if (applicationContext == null) {
            this.A08 = context;
        }
        File A002 = A00(this);
        this.A03 = A002;
        file = file == null ? new File(A002, "profilo") : file;
        if (file.exists() || file.mkdirs()) {
            this.A03 = file;
        }
        this.A06 = new File(this.A03, "upload");
        this.A04 = new File(this.A03, "crash_dumps");
        this.A05 = new File(this.A03, "mmap_buffer");
    }

    public static List A01(File file, FilenameFilter filenameFilter) {
        File[] listFiles = file.listFiles(filenameFilter);
        if (listFiles == null) {
            return Collections.EMPTY_LIST;
        }
        return Arrays.asList(listFiles);
    }

    public static void A02(AnonymousClass05m r3, File file, int i) {
        if (file.exists() || file.isDirectory()) {
            List A012 = A01(file, A09);
            if (A012.size() > i) {
                Collections.sort(A012, new AnonymousClass0PL());
                for (File delete : A012.subList(0, A012.size() - i)) {
                    if (delete.delete()) {
                        r3.A02.A06++;
                    } else {
                        r3.A02.A04++;
                    }
                }
            }
        }
    }

    public static void A03(AnonymousClass05m r7, File file, File file2, long j) {
        if (file.exists() || file.isDirectory()) {
            long currentTimeMillis = System.currentTimeMillis() - j;
            for (File file3 : A01(file, A09)) {
                if (file3.lastModified() < currentTimeMillis) {
                    if (A04(r7, file3, new File(file2, file3.getName()))) {
                        r7.A02.A05++;
                    } else {
                        r7.A02.A04++;
                    }
                }
            }
        }
    }
}
