package X;

import android.os.Process;
import com.facebook.profilo.provider.perfevents.PerfEventsSession;

/* renamed from: X.0Ph  reason: invalid class name */
public final class AnonymousClass0Ph implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.profilo.provider.perfevents.PerfEventsSession$1";
    public final /* synthetic */ PerfEventsSession A00;

    public void run() {
        Process.setThreadPriority(5);
        PerfEventsSession.nativeRun(this.A00.mNativeHandle);
    }

    public AnonymousClass0Ph(PerfEventsSession perfEventsSession) {
        this.A00 = perfEventsSession;
    }
}
