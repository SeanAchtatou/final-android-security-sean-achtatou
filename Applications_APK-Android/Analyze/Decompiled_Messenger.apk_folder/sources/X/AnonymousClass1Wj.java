package X;

import com.google.firebase.iid.FirebaseInstanceId;

/* renamed from: X.1Wj  reason: invalid class name */
public final /* synthetic */ class AnonymousClass1Wj implements C24551Wa {
    public static final C24551Wa A00 = new AnonymousClass1Wj();

    private AnonymousClass1Wj() {
    }

    public final Object AUm(AnonymousClass1WU r9) {
        AnonymousClass1WN r2 = (AnonymousClass1WN) r9.A02(AnonymousClass1WN.class);
        AnonymousClass1WN.A02(r2);
        return new FirebaseInstanceId(r2, new C24701Wt(r2.A00), C24711Wu.A00(), C24711Wu.A00(), (AnonymousClass1Wh) r9.A02(AnonymousClass1Wh.class), (C24591We) r9.A02(C24591We.class));
    }
}
