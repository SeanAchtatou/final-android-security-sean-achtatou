package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.io.File;
import java.io.FileInputStream;

/* renamed from: X.1rd  reason: invalid class name and case insensitive filesystem */
public final class C35691rd extends FileInputStream {
    private int A00 = 0;
    private final int A01;
    public final /* synthetic */ C31111jA A02;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C35691rd(C31111jA r2, File file, int i) {
        super(file);
        this.A02 = r2;
        this.A01 = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void close() {
        String $const$string = TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1K);
        try {
            super.close();
        } finally {
            int i = this.A01;
            if (i != 0) {
                this.A02.A01.markerAnnotate(42991645, i, $const$string, this.A00);
                this.A02.A01.markerEnd(42991645, this.A01, (short) 2);
            }
        }
    }

    public int read() {
        int read = super.read();
        if (read != -1) {
            this.A00++;
        }
        return read;
    }

    public int read(byte[] bArr) {
        int read = super.read(bArr);
        if (read != -1) {
            this.A00 += read;
        }
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = super.read(bArr, i, i2);
        if (read != -1) {
            this.A00 += read;
        }
        return read;
    }
}
