package X;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0FQ  reason: invalid class name */
public final class AnonymousClass0FQ {
    public static final AnonymousClass0FR A00 = new AnonymousClass0FR();
    public static final List A01 = new AnonymousClass0FS(4);
    public static final ReadWriteLock A02 = new ReentrantReadWriteLock();
    public static volatile boolean A03;

    public static void A00(Object obj) {
        try {
            A02.readLock().lock();
            int size = A01.size();
            for (int i = 0; i < size; i++) {
                ((AnonymousClass0FU) A01.get(i)).BlQ(obj);
            }
        } finally {
            A02.readLock().unlock();
        }
    }

    public static void A01(Object obj) {
        try {
            A02.readLock().lock();
            int size = A01.size();
            for (int i = 0; i < size; i++) {
                ((AnonymousClass0FU) A01.get(i)).BXr(obj);
            }
        } finally {
            A02.readLock().unlock();
        }
    }

    public static void A02(Object obj) {
        try {
            A02.readLock().lock();
            int size = A01.size();
            for (int i = 0; i < size; i++) {
                ((AnonymousClass0FU) A01.get(i)).Bgz(obj);
            }
        } finally {
            A02.readLock().unlock();
        }
    }

    public static boolean A03() {
        if (!A03 || A01.isEmpty()) {
            return false;
        }
        return true;
    }
}
