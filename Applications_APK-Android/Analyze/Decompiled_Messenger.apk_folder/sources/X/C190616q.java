package X;

import com.facebook.inject.InjectorModule;
import java.util.concurrent.ExecutorService;

@InjectorModule
/* renamed from: X.16q  reason: invalid class name and case insensitive filesystem */
public final class C190616q extends AnonymousClass0UV {
    public static final C16730xg A01(AnonymousClass1XY r3) {
        return new C16730xg(AnonymousClass0UQ.A00(AnonymousClass1Y3.AaN, r3), AnonymousClass0UQ.A00(AnonymousClass1Y3.BLe, r3));
    }

    public static final C191016u A00(AnonymousClass1XY r1) {
        AnonymousClass0WT.A00(r1);
        return (C191016u) AnonymousClass0UQ.A00(AnonymousClass1Y3.BJK, r1).get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d8, code lost:
        if (r1.contains("NEWSPICE") != false) goto L_0x00da;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A02(X.AnonymousClass1XY r6) {
        /*
            X.00z r1 = X.AnonymousClass0UU.A05(r6)
            X.1YI r3 = X.AnonymousClass0WA.A00(r6)
            r0 = 145(0x91, float:2.03E-43)
            r2 = 0
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r2)
            boolean r0 = r3.AbO(r0, r2)
            if (r0 == 0) goto L_0x0044
            X.00z r0 = X.C001500z.A07
            if (r1 != r0) goto L_0x0044
            java.lang.String r4 = android.os.Build.MANUFACTURER
            if (r4 == 0) goto L_0x0044
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "samsung"
            boolean r0 = r1.contains(r0)
            r5 = 1
            if (r0 != 0) goto L_0x00e5
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "htc"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0045
            r0 = 147(0x93, float:2.06E-43)
        L_0x003c:
            boolean r0 = r3.AbO(r0, r2)
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r0)
        L_0x0044:
            return r6
        L_0x0045:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "sony"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0056
            r0 = 150(0x96, float:2.1E-43)
            goto L_0x003c
        L_0x0056:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "lg"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x00e5
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "asus"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0075
            r0 = 146(0x92, float:2.05E-43)
            goto L_0x003c
        L_0x0075:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "huawei"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0086
            r0 = 148(0x94, float:2.07E-43)
            goto L_0x003c
        L_0x0086:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "oppo"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x00e1
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "realme"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x00e1
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toLowerCase(r0)
            java.lang.String r0 = "vivo"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x00b3
            r0 = 152(0x98, float:2.13E-43)
            goto L_0x003c
        L_0x00b3:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r4.toUpperCase(r0)
            java.lang.String r0 = "INFINIX"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x00da
            java.lang.String r0 = "TECNO"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x00da
            java.lang.String r0 = "ITEL"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x00da
            java.lang.String r0 = "NEWSPICE"
            boolean r1 = r1.contains(r0)
            r0 = 0
            if (r1 == 0) goto L_0x00db
        L_0x00da:
            r0 = 1
        L_0x00db:
            if (r0 == 0) goto L_0x0044
            r0 = 151(0x97, float:2.12E-43)
            goto L_0x003c
        L_0x00e1:
            r0 = 149(0x95, float:2.09E-43)
            goto L_0x003c
        L_0x00e5:
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r5)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C190616q.A02(X.1XY):java.lang.Boolean");
    }

    public static final ExecutorService A03(AnonymousClass1XY r2) {
        return AnonymousClass0VM.A00(r2).A04(AnonymousClass0VS.URGENT, "orca_notification");
    }
}
