package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1cu  reason: invalid class name and case insensitive filesystem */
public final class C27261cu {
    private static volatile C27261cu A02;
    public C27341d2 A00;
    private AnonymousClass0UN A01;

    public static final C27261cu A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C27261cu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C27261cu(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C27341d2 A01() {
        if (this.A00 == null) {
            Context context = (Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A01);
            C27281cw r1 = null;
            if (0 == 0) {
                r1 = (C27281cw) C27271cv.A00("com_facebook_messenger_msys_plugins_interfaces_msysbootstrap_MessengerMsysBootstrapInterfaceSpec", "MsysBootstrap", context, new Object[0]);
            }
            this.A00 = new C27341d2(r1);
        }
        return this.A00;
    }

    private C27261cu(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(1, r3);
    }

    public static void A02(C27261cu r4) {
        C27291cx r3 = r4.A01().A00.A00;
        C27271cv.A02.getAndIncrement();
        r3.A03.A05("com.facebook.messenger.msys.plugins.interfaces.msysbootstrap.MessengerMsysBootstrapInterfaceSpec", "clearUserDataIfInitialized");
        try {
            C27291cx.A00(r3);
            if ("MESSENGER_MSYS_BOOTSTRAP_IMPL".equals("MESSENGER_MSYS_BOOTSTRAP_IMPL") && C27291cx.A01(r3)) {
                C27271cv.A02.getAndIncrement();
                r3.A03.A07("com.facebook.messenger.msys.plugins.implementations.messengermsys.msysbootstrap.MessengerMsysBootstrapImpl", "com.facebook.messenger.msys.plugins.interfaces.msysbootstrap.MessengerMsysBootstrapInterfaceSpec", "clearUserDataIfInitialized");
                try {
                    ((EJJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AwD, ((AnonymousClass999) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AbT, r3.A00)).A00)).clearUserData();
                    r3.A03.A00();
                } catch (Exception e) {
                    throw e;
                } catch (Throwable th) {
                    r3.A03.A00();
                    throw th;
                }
            }
        } finally {
            r3.A03.A01();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        X.C010708t.A0L(r8.getSimpleName(), "Exception during MSYS Bootstrap", r2);
        r5.A01(r4, "Unknown");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c9, code lost:
        X.C010708t.A0L(r8.getSimpleName(), "TimeoutException during MSYS Bootstrap", r2);
        r5.A01(r4, "Timeout");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d9, code lost:
        X.C010708t.A0L(r8.getSimpleName(), "ExecutionException during MSYS Bootstrap", r2);
        r5.A01(r4, "Execution Exception");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00e9, code lost:
        X.C010708t.A0L(r8.getSimpleName(), "InterruptedException during MSYS Bootstrap", r2);
        r5.A01(r4, "Interrupted");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.C27261cu r8) {
        /*
            X.1d2 r0 = r8.A01()
            java.lang.String r1 = "MESSENGER_MSYS_BOOTSTRAP_IMPL"
            X.1cw r0 = r0.A00
            X.1cx r3 = r0.A00
            java.util.concurrent.atomic.AtomicInteger r0 = X.C27271cv.A02
            r0.getAndIncrement()
            X.1d0 r0 = r3.A03
            java.lang.String r4 = "bootstrapMsys"
            java.lang.String r2 = "com.facebook.messenger.msys.plugins.interfaces.msysbootstrap.MessengerMsysBootstrapInterfaceSpec"
            r0.A05(r2, r4)
            X.C27291cx.A00(r3)     // Catch:{ all -> 0x010c }
            boolean r0 = r1.equals(r1)     // Catch:{ all -> 0x010c }
            if (r0 == 0) goto L_0x0106
            boolean r0 = X.C27291cx.A01(r3)     // Catch:{ all -> 0x010c }
            if (r0 == 0) goto L_0x0106
            java.util.concurrent.atomic.AtomicInteger r0 = X.C27271cv.A02     // Catch:{ all -> 0x010c }
            r0.getAndIncrement()     // Catch:{ all -> 0x010c }
            X.1d0 r1 = r3.A03     // Catch:{ all -> 0x010c }
            java.lang.String r0 = "com.facebook.messenger.msys.plugins.implementations.messengermsys.msysbootstrap.MessengerMsysBootstrapImpl"
            r1.A07(r0, r2, r4)     // Catch:{ all -> 0x010c }
            r1 = 0
            int r0 = X.AnonymousClass1Y3.AbT     // Catch:{ Exception -> 0x00fd }
            X.0UN r2 = r3.A00     // Catch:{ Exception -> 0x00fd }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r0, r2)     // Catch:{ Exception -> 0x00fd }
            X.999 r7 = (X.AnonymousClass999) r7     // Catch:{ Exception -> 0x00fd }
            r1 = 1
            int r0 = X.AnonymousClass1Y3.B1H     // Catch:{ Exception -> 0x00fd }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r0, r2)     // Catch:{ Exception -> 0x00fd }
            X.6NC r5 = (X.AnonymousClass6NC) r5     // Catch:{ Exception -> 0x00fd }
            r1 = 2
            int r0 = X.AnonymousClass1Y3.AYQ     // Catch:{ Exception -> 0x00fd }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r0, r2)     // Catch:{ Exception -> 0x00fd }
            X.1d4 r6 = (X.C27361d4) r6     // Catch:{ Exception -> 0x00fd }
            java.lang.Class<com.facebook.messenger.msys.plugins.implementations.messengermsys.msysbootstrap.MessengerMsysBootstrapImpl> r8 = com.facebook.messenger.msys.plugins.implementations.messengermsys.msysbootstrap.MessengerMsysBootstrapImpl.class
            java.util.UUID r0 = X.C188215g.A00()     // Catch:{ Exception -> 0x00fd }
            int r4 = r0.hashCode()     // Catch:{ Exception -> 0x00fd }
            int r2 = X.AnonymousClass1Y3.BBd     // Catch:{ Exception -> 0x00fd }
            X.0UN r1 = r5.A00     // Catch:{ Exception -> 0x00fd }
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ Exception -> 0x00fd }
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1     // Catch:{ Exception -> 0x00fd }
            r0 = 52822019(0x3260003, float:4.878303E-37)
            r1.markerStart(r0, r4)     // Catch:{ Exception -> 0x00fd }
            X.EJJ r7 = r7.A01()     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            int r2 = X.AnonymousClass1Y3.AOJ     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            X.0UN r1 = r6.A00     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            r0 = 564328638055171(0x2014100020303, double:2.78815393027439E-309)
            long r1 = r2.At0(r0)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            monitor-enter(r7)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            com.google.common.util.concurrent.ListenableFuture r6 = X.EJJ.A01(r7)     // Catch:{ all -> 0x00b5 }
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00b5 }
            java.lang.Object r0 = r6.get(r1, r0)     // Catch:{ all -> 0x00b5 }
            com.facebook.msys.mca.Mailbox r0 = (com.facebook.msys.mca.Mailbox) r0     // Catch:{ all -> 0x00b5 }
            monitor-exit(r7)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            if (r0 != 0) goto L_0x00a2
            java.lang.String r1 = r8.getSimpleName()     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            java.lang.String r0 = "null Mailbox from MSYS Bootstrap"
            X.C010708t.A0I(r1, r0)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            java.lang.String r0 = "Null Mailbox"
            r5.A01(r4, r0)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            goto L_0x00f7
        L_0x00a2:
            int r2 = X.AnonymousClass1Y3.BBd     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            X.0UN r1 = r5.A00     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            r1 = 52822019(0x3260003, float:4.878303E-37)
            r0 = 2
            r2.markerEnd(r1, r4, r0)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            goto L_0x00f7
        L_0x00b5:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
            throw r0     // Catch:{ InterruptedException -> 0x00e8, ExecutionException -> 0x00d8, TimeoutException -> 0x00c8, Exception -> 0x00b8 }
        L_0x00b8:
            r2 = move-exception
            java.lang.String r1 = r8.getSimpleName()     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "Exception during MSYS Bootstrap"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "Unknown"
            r5.A01(r4, r0)     // Catch:{ Exception -> 0x00fd }
            goto L_0x00f7
        L_0x00c8:
            r2 = move-exception
            java.lang.String r1 = r8.getSimpleName()     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "TimeoutException during MSYS Bootstrap"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "Timeout"
            r5.A01(r4, r0)     // Catch:{ Exception -> 0x00fd }
            goto L_0x00f7
        L_0x00d8:
            r2 = move-exception
            java.lang.String r1 = r8.getSimpleName()     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "ExecutionException during MSYS Bootstrap"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "Execution Exception"
            r5.A01(r4, r0)     // Catch:{ Exception -> 0x00fd }
            goto L_0x00f7
        L_0x00e8:
            r2 = move-exception
            java.lang.String r1 = r8.getSimpleName()     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "InterruptedException during MSYS Bootstrap"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ Exception -> 0x00fd }
            java.lang.String r0 = "Interrupted"
            r5.A01(r4, r0)     // Catch:{ Exception -> 0x00fd }
        L_0x00f7:
            X.1d0 r0 = r3.A03     // Catch:{ all -> 0x010c }
            r0.A00()     // Catch:{ all -> 0x010c }
            goto L_0x0106
        L_0x00fd:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ff }
        L_0x00ff:
            r1 = move-exception
            X.1d0 r0 = r3.A03     // Catch:{ all -> 0x010c }
            r0.A00()     // Catch:{ all -> 0x010c }
            throw r1     // Catch:{ all -> 0x010c }
        L_0x0106:
            X.1d0 r0 = r3.A03
            r0.A01()
            return
        L_0x010c:
            r1 = move-exception
            X.1d0 r0 = r3.A03
            r0.A01()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27261cu.A03(X.1cu):void");
    }
}
