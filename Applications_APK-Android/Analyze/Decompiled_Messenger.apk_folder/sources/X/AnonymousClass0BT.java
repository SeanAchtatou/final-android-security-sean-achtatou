package X;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0BT  reason: invalid class name */
public final class AnonymousClass0BT implements AnonymousClass0BU {
    public final Set A00 = new HashSet();

    public boolean CDi(Map map) {
        boolean z = true;
        for (AnonymousClass0BU CDi : this.A00) {
            z &= CDi.CDi(map);
        }
        return z;
    }
}
