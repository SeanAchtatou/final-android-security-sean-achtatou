package X;

import android.os.SystemClock;

/* renamed from: X.0Dd  reason: invalid class name and case insensitive filesystem */
public final class C02150Dd implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.protocol.MqttClient$7";
    public final /* synthetic */ AnonymousClass0C8 A00;

    public C02150Dd(AnonymousClass0C8 r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass0C8 r3 = this.A00;
        try {
            if (r3.A09()) {
                r3.A0C.C5M();
                AnonymousClass0CC r0 = r3.A0Y;
                if (r0 != null) {
                    AnonymousClass07g r02 = r0.A02.A06;
                }
                r3.A0V = SystemClock.elapsedRealtime();
            }
        } catch (Throwable th) {
            AnonymousClass0C8.A03(r3, AnonymousClass0CE.A01(th), AnonymousClass0CG.A03, th);
        }
    }
}
