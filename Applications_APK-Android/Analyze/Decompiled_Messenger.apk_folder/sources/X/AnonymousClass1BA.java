package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1BA  reason: invalid class name */
public final class AnonymousClass1BA {
    private static C05540Zi A01;
    public final C33671nx A00;

    public static final AnonymousClass1BA A00(AnonymousClass1XY r4) {
        AnonymousClass1BA r0;
        synchronized (AnonymousClass1BA.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new AnonymousClass1BA((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass1BA) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass1BA(AnonymousClass1XY r2) {
        this.A00 = C33671nx.A00(r2);
    }
}
