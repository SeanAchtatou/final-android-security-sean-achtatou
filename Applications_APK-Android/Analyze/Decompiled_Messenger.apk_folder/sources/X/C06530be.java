package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.0be  reason: invalid class name and case insensitive filesystem */
public final class C06530be implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass1U0 A00;

    public C06530be(AnonymousClass1U0 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(354112518);
        AnonymousClass1U0 r3 = this.A00;
        C24391Tl r2 = (C24391Tl) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B2Y, r3.A00);
        r2.A06.A04(r2.A02);
        r2.A00 = false;
        if (AnonymousClass1U0.A09(r3)) {
            AnonymousClass1U0.A06(r3, "onForceRebindBroadcast", true);
        }
        AnonymousClass09Y.A01(-991609866, A002);
    }
}
