package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0dl  reason: invalid class name and case insensitive filesystem */
public final class C07560dl extends AnonymousClass0UV {
    private static C05540Zi A00;

    public static final C05530Zh A01(AnonymousClass1XY r9) {
        C05530Zh r0;
        synchronized (C05530Zh.class) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r9)) {
                    A00.A00 = C25491Zv.A00((AnonymousClass1XY) A00.A01()).A01("MESSENGER_INBOX2", null, null, null, null, null);
                }
                C05540Zi r1 = A00;
                r0 = (C05530Zh) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final AnonymousClass17O A00(AnonymousClass1XY r1) {
        return AnonymousClass17N.A00(r1).A01("MESSENGER_INBOX2");
    }
}
