package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.auth.annotations.LoggedInUser;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.1Ah  reason: invalid class name and case insensitive filesystem */
public final class C19941Ah extends C19951Ai {
    public Map A00;
    public C04310Tq A01;

    public C19941Ah(RecyclerView recyclerView, ScheduledExecutorService scheduledExecutorService, @LoggedInUser C04310Tq r3) {
        super(recyclerView, scheduledExecutorService);
        this.A01 = r3;
    }
}
