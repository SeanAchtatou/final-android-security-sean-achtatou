package X;

/* renamed from: X.0CD  reason: invalid class name */
public final class AnonymousClass0CD implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$CallbackHandler$4";
    public final /* synthetic */ AnonymousClass0CC A00;

    public AnonymousClass0CD(AnonymousClass0CC r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass0C8 r2 = this.A00.A02.A0n;
        AnonymousClass0CC r1 = this.A00;
        if (r2 == r1.A00) {
            AnonymousClass0AH.A05(r1.A02, AnonymousClass0FG.CONNECTION_LOST, C01660Bc.A00);
            return;
        }
        AnonymousClass0C8 r22 = r1.A02.A0o;
        AnonymousClass0CC r12 = this.A00;
        if (r22 == r12.A00) {
            AnonymousClass0AH.A01(r12.A02);
        }
    }
}
