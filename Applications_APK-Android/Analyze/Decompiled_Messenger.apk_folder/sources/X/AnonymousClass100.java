package X;

/* renamed from: X.100  reason: invalid class name */
public final class AnonymousClass100 {
    public C09480hR A00;
    public final AnonymousClass101 A01;
    public final /* synthetic */ C25511Zx A02;

    public AnonymousClass100(C25511Zx r1, AnonymousClass101 r2) {
        this.A02 = r1;
        this.A01 = r2;
    }

    public void A00(C09480hR r3) {
        this.A00 = r3;
        if (r3 != null) {
            this.A02.A0E.put(this, true);
        } else {
            this.A02.A0E.remove(this);
        }
    }
}
