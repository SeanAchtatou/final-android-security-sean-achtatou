package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.callercontext.CallerContextable;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1te  reason: invalid class name and case insensitive filesystem */
public final class C36741te extends C36471tB implements CallerContextable {
    public static final Class A03 = C36741te.class;
    private static volatile C36741te A04 = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.tincan.outbound.PreKeyVerificationSP";
    public final AnonymousClass06A A00;
    public final AnonymousClass18N A01;
    public final C04310Tq A02;

    private C36741te(DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, AnonymousClass18N r3, C04310Tq r4, AnonymousClass06A r5) {
        super(204, deprecatedAnalyticsLogger);
        this.A01 = r3;
        this.A02 = r4;
        this.A00 = r5;
    }

    public static final C36741te A00(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (C36741te.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A04 = new C36741te(C06920cI.A00(applicationInjector), AnonymousClass18N.A00(applicationInjector), C10580kT.A04(applicationInjector), AnonymousClass067.A0A(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }
}
