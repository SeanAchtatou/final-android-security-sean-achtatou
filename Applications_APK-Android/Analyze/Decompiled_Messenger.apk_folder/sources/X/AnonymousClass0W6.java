package X;

import android.database.Cursor;
import com.google.common.base.Function;

/* renamed from: X.0W6  reason: invalid class name */
public final class AnonymousClass0W6 {
    public static final Function A02 = new AnonymousClass0W7();
    public static final Function A03 = new AnonymousClass0W8();
    public final String A00;
    public final String A01;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1.equals(r5.A00) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            if (r4 == r5) goto L_0x0026
            r2 = 0
            if (r5 == 0) goto L_0x001e
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r5.getClass()
            if (r1 != r0) goto L_0x001e
            X.0W6 r5 = (X.AnonymousClass0W6) r5
            java.lang.String r1 = r4.A00
            if (r1 == 0) goto L_0x001f
            java.lang.String r0 = r5.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0027
        L_0x001e:
            return r2
        L_0x001f:
            java.lang.String r0 = r5.A00
            if (r0 == 0) goto L_0x0027
            return r2
        L_0x0024:
            if (r0 != 0) goto L_0x0033
        L_0x0026:
            return r3
        L_0x0027:
            java.lang.String r1 = r4.A01
            java.lang.String r0 = r5.A01
            if (r1 == 0) goto L_0x0024
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0026
        L_0x0033:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0W6.equals(java.lang.Object):boolean");
    }

    public int A01(Cursor cursor) {
        return cursor.getColumnIndexOrThrow(this.A00);
    }

    public C06140av A03(String str) {
        return C06160ax.A03(this.A00, str);
    }

    public String A04() {
        return AnonymousClass08S.A0J(this.A00, " DESC");
    }

    public int hashCode() {
        int i;
        String str = this.A00;
        int i2 = 0;
        if (str != null) {
            i = str.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        String str2 = this.A01;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return i3 + i2;
    }

    public String toString() {
        return this.A00;
    }

    public AnonymousClass0W6(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    public int A00(Cursor cursor) {
        return cursor.getInt(A01(cursor));
    }

    public long A02(Cursor cursor) {
        return cursor.getLong(A01(cursor));
    }

    public String A05(Cursor cursor) {
        return cursor.getString(A01(cursor));
    }

    public boolean A06(Cursor cursor) {
        return cursor.isNull(A01(cursor));
    }

    public byte[] A07(Cursor cursor) {
        return cursor.getBlob(A01(cursor));
    }
}
