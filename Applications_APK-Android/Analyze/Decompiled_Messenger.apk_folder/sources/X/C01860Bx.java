package X;

import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0Bx  reason: invalid class name and case insensitive filesystem */
public abstract class C01860Bx implements C01870By {
    public final String A00;
    public final HashMap A01 = new HashMap();

    public synchronized Object A00(AnonymousClass0C4 r6) {
        try {
            if (!this.A01.containsKey(r6)) {
                this.A01.put(r6, r6.B8J().newInstance());
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("Incorrect usage for %s type %s", r6.Arl(), r6.B8J()), e);
        }
        return this.A01.get(r6);
    }

    public synchronized JSONObject A01(boolean z, boolean z2) {
        JSONObject jSONObject;
        jSONObject = new JSONObject();
        for (Map.Entry entry : this.A01.entrySet()) {
            jSONObject.putOpt(((AnonymousClass0C4) entry.getKey()).Arl(), entry.getValue());
        }
        return jSONObject;
    }

    public synchronized void A02(AnonymousClass0C4 r2, Object obj) {
        this.A01.put(r2, obj);
    }

    public String toString() {
        try {
            return A01(false, false).toString();
        } catch (JSONException unused) {
            return BuildConfig.FLAVOR;
        }
    }

    public C01860Bx(String str) {
        this.A00 = str;
    }
}
