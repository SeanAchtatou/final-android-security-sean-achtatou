package X;

/* renamed from: X.0L3  reason: invalid class name */
public final class AnonymousClass0L3 extends RuntimeException {
    public final int error;

    public AnonymousClass0L3(int i, String str) {
        super(str);
        this.error = i;
    }
}
