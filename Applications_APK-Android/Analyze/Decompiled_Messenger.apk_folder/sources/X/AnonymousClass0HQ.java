package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import com.google.common.base.Optional;

/* renamed from: X.0HQ  reason: invalid class name */
public final class AnonymousClass0HQ {
    public static int A02(Context context, int i, int i2) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics());
        }
        return i2;
    }

    public static int A03(Context context, int i, int i2) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return typedValue.data;
        }
        return i2;
    }

    public static Optional A08(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return Optional.of(Integer.valueOf(typedValue.data));
        }
        return Optional.absent();
    }

    public static Optional A09(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context == null || !context.getTheme().resolveAttribute(i, typedValue, true)) {
            return Optional.absent();
        }
        return Optional.of(Integer.valueOf(typedValue.resourceId));
    }

    private static Optional A0A(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return Optional.of(Float.valueOf(typedValue.getFloat()));
        }
        return Optional.absent();
    }

    public static float A00(Context context, int i, float f) {
        return ((Float) A0A(context, i).or(Float.valueOf(f))).floatValue();
    }

    public static int A01(Context context, int i, int i2) {
        Optional A08 = A08(context, i);
        if (A08.isPresent()) {
            return ((Integer) A08.get()).intValue();
        }
        return i2;
    }

    public static int A04(Context context, int i, int i2) {
        Optional A09 = A09(context, i);
        if (A09.isPresent()) {
            return ((Integer) A09.get()).intValue();
        }
        return i2;
    }

    public static Context A05(Context context, int i, int i2) {
        return new ContextThemeWrapper(context, A04(context, i, i2));
    }

    public static Drawable A06(Context context, int i, int i2) {
        Optional A07 = A07(context, i);
        if (A07.isPresent()) {
            return (Drawable) A07.get();
        }
        return context.getResources().getDrawable(i2);
    }

    public static Optional A07(Context context, int i) {
        Resources resources = context.getResources();
        Optional A09 = A09(context, i);
        if (A09.isPresent()) {
            return Optional.of(resources.getDrawable(((Integer) A09.get()).intValue()));
        }
        return Optional.absent();
    }
}
