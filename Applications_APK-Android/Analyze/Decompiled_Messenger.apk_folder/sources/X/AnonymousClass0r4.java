package X;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.facebook.messaging.accountswitch.SwitchAccountActivity;

/* renamed from: X.0r4  reason: invalid class name */
public final class AnonymousClass0r4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C13210qu A01;

    public AnonymousClass0r4(C13210qu r1, Context context) {
        this.A01 = r1;
        this.A00 = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        C417626w.A06(new Intent(this.A00, SwitchAccountActivity.class).putExtra(AnonymousClass24B.$const$string(235), "home").putExtra(AnonymousClass24B.$const$string(AnonymousClass1Y3.A1p), null), this.A00);
    }
}
