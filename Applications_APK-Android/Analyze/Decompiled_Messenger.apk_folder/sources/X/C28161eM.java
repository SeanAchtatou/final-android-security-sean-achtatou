package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import com.facebook.acra.ACRA;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.proxygen.TraceFieldType;
import com.google.common.base.Optional;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1eM  reason: invalid class name and case insensitive filesystem */
public abstract class C28161eM extends AnonymousClass1ES {
    public static final List A0P = Arrays.asList("com.facebook.katana.activity.media.ViewVideoActivity", "com.facebook.photos.albums.video.VideoAlbumLaunchPlayerActivity", "com.facebook.photos.taggablegallery.ProductionVideoGalleryActivity");
    public Optional A00 = Optional.absent();
    private AnonymousClass0US A01;
    private Set A02;
    private C14250su[] A03;
    public final Resources A04;
    public final AnonymousClass1ZE A05;
    public final AnonymousClass0XN A06;
    public final AnonymousClass0Z9 A07;
    public final AnonymousClass1ET A08;
    public final AnonymousClass0US A09;
    public final AnonymousClass0US A0A;
    public final AnonymousClass0US A0B;
    public final AnonymousClass0US A0C;
    public final AnonymousClass0US A0D;
    public final AnonymousClass0US A0E;
    public final AnonymousClass0US A0F;
    public final AnonymousClass0US A0G;
    public final AnonymousClass0US A0H;
    public final AnonymousClass0US A0I;
    public final AnonymousClass0US A0J;
    public final AnonymousClass0US A0K;
    public final AnonymousClass0US A0L;
    public final C25051Yd A0M;
    public final C28171eN A0N;
    public final C04310Tq A0O;

    private synchronized void A03() {
        int i = 0;
        while (true) {
            C14250su[] r1 = this.A03;
            if (i < r1.length) {
                r1[i] = null;
                i++;
            }
        }
    }

    public static synchronized void A05(C28161eM r4, boolean z) {
        synchronized (r4) {
            C14250su[] r3 = (C14250su[]) A02(r4).toArray(r4.A03);
            r4.A03 = r3;
            for (C14250su r0 : r3) {
                if (r0 != null) {
                    r0.onBeforeDialtoneStateChanged(z);
                }
            }
            r4.A03();
        }
    }

    public static boolean A06(C28161eM r5, Uri uri) {
        String str;
        if (uri != null) {
            try {
                str = uri.getQueryParameter(r5.A0M.B4E(844777117515827L, "oh"));
            } catch (UnsupportedOperationException unused) {
                str = null;
            }
            if (str != null) {
                return "z-m".equals(uri.getQueryParameter(r5.A0M.B4E(844777118302266L, "_nc_ad")));
            }
        }
        return true;
    }

    public int A0Z() {
        return C007106r.A03(((AnonymousClass50U) ((C14730tt) this).A01.get()).A00, 82.0f);
    }

    public Bitmap A0a(float f, float f2, AnonymousClass8ZY r7) {
        AnonymousClass50U r3 = (AnonymousClass50U) ((C14730tt) this).A01.get();
        int i = 2132214019;
        boolean z = false;
        if (r7.A00.equals(AnonymousClass07B.A0C)) {
            i = 2132214008;
            z = true;
        }
        return AnonymousClass50U.A00(r3, i, z);
    }

    public void A0b(Context context) {
    }

    public void A0d(String str, Context context) {
    }

    public synchronized void A0f(boolean z) {
        C14250su[] r2;
        this.A03 = (C14250su[]) A02(this).toArray(this.A03);
        int i = 0;
        while (true) {
            r2 = this.A03;
            if (i >= r1) {
                break;
            }
            C14250su r1 = r2[i];
            if (r1 != null && (r1 instanceof C28191eP)) {
                r1.onAfterDialtoneStateChanged(z);
                this.A03[i] = null;
            }
            i++;
        }
        for (C14250su r0 : r2) {
            if (r0 != null) {
                r0.onAfterDialtoneStateChanged(z);
            }
        }
        A03();
    }

    public static Set A02(C28161eM r2) {
        if (r2.A02 == null) {
            Set A002 = C008907u.A00();
            r2.A02 = A002;
            A002.addAll((Collection) r2.A01.get());
        }
        return r2.A02;
    }

    public static void A04(C28161eM r3, String str, Integer num, String str2, CallerContext callerContext, String str3, boolean z) {
        String str4;
        C11670nb r2 = new C11670nb(str);
        r2.A0D("pigeon_reserved_keyword_module", "dialtone");
        switch (num.intValue()) {
            case 1:
                str4 = "feature_tag";
                break;
            case 2:
                str4 = "faceweb";
                break;
            case 3:
                str4 = "image_size";
                break;
            case 4:
                str4 = "flex_plus";
                break;
            default:
                str4 = TraceFieldType.Uri;
                break;
        }
        r2.A0D("whitelist_type", str4);
        r2.A0D("whitelisted_element", str2);
        if (callerContext != null) {
            r2.A0D("whitelisted_callercontext", callerContext.A02);
        }
        r2.A0D("carrier_id", ((AnonymousClass155) r3.A0L.get()).A0A(AnonymousClass157.NORMAL));
        if (str3 != null) {
            r2.A0D("whitelisted_image_uri", str3);
            r2.A0E("dialtone_uri_can_whitelist", z);
        }
        ((DeprecatedAnalyticsLogger) r3.A09.get()).A09(r2);
    }

    public void A0e(String str, boolean z) {
        C11670nb r2 = new C11670nb("dialtone_upgrade_dialog_impression");
        r2.A0D("pigeon_reserved_keyword_module", "dialtone");
        r2.A0D(C22298Ase.$const$string(14), str);
        r2.A0E("is_photo", z);
        r2.A0D("carrier_id", ((AnonymousClass155) this.A0L.get()).A0A(AnonymousClass157.NORMAL));
        ((DeprecatedAnalyticsLogger) this.A09.get()).A09(r2);
    }

    public boolean A0g() {
        boolean Aep;
        if (!((FbSharedPreferences) this.A0D.get()).Aep(C05730aE.A0N, false) || !((C05720aD) this.A0K.get()).A07("dialtone_sticky_on_login")) {
            Aep = ((FbSharedPreferences) this.A0D.get()).Aep(C05730aE.A0M, false);
        } else {
            C30281hn edit = ((FbSharedPreferences) this.A0D.get()).edit();
            edit.putBoolean(C05730aE.A0N, false);
            edit.commit();
            Aep = true;
        }
        if (!Aep) {
            return false;
        }
        this.A07.A05();
        return true;
    }

    public C28161eM(Resources resources, AnonymousClass0Z9 r3, AnonymousClass1ET r4, AnonymousClass0US r5, AnonymousClass0US r6, AnonymousClass0US r7, C04310Tq r8, AnonymousClass0US r9, AnonymousClass0US r10, AnonymousClass0US r11, AnonymousClass0US r12, AnonymousClass0US r13, C25051Yd r14, AnonymousClass0US r15, AnonymousClass0US r16, AnonymousClass0US r17, AnonymousClass0US r18, AnonymousClass1ZE r19, AnonymousClass0US r20, AnonymousClass0US r21, AnonymousClass0XN r22, C28171eN r23) {
        this.A04 = resources;
        this.A0O = r8;
        this.A07 = r3;
        this.A08 = r4;
        this.A01 = r5;
        this.A09 = r6;
        this.A0B = r7;
        this.A0A = r9;
        this.A0F = r10;
        this.A0H = r11;
        this.A0D = r13;
        this.A0I = r12;
        this.A0M = r14;
        this.A0C = r15;
        this.A0J = r16;
        this.A0G = r17;
        this.A0K = r18;
        this.A05 = r19;
        this.A0E = r20;
        this.A0L = r21;
        this.A06 = r22;
        this.A0N = r23;
        this.A03 = new C14250su[10];
    }

    public String A0c(Integer num) {
        if (A0O()) {
            return "flex_plus";
        }
        switch (num.intValue()) {
            case 1:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "dialtone_video_interstitial";
            case 2:
            case 3:
            case 4:
            default:
                return "dialtone_photo_interstitial";
            case 5:
                if (((C05720aD) this.A0K.get()).A07("free_messenger_gif_interstitial")) {
                    return "free_messenger_gif_interstitial";
                }
                return "messenger_dialtone_gif_interstitial";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return "messenger_dialtone_my_day_interstitial";
            case 8:
                return "messenger_dialtone_location_interstitial";
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        if (r0 != false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0h(java.lang.Integer r6) {
        /*
            r5 = this;
            boolean r0 = X.AnonymousClass5VN.A00(r6)
            r2 = 1
            r4 = 0
            if (r0 != 0) goto L_0x0019
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 0
            if (r6 != r1) goto L_0x000e
            r0 = 1
        L_0x000e:
            if (r0 != 0) goto L_0x0019
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r0 = 0
            if (r6 != r1) goto L_0x0016
            r0 = 1
        L_0x0016:
            r3 = 0
            if (r0 == 0) goto L_0x001a
        L_0x0019:
            r3 = 1
        L_0x001a:
            X.0US r0 = r5.A0K
            java.lang.Object r1 = r0.get()
            X.0aD r1 = (X.C05720aD) r1
            java.lang.String r0 = "advanced_upsell_for_all_show_again"
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x002d
            if (r3 == 0) goto L_0x002d
            return r4
        L_0x002d:
            X.0US r0 = r5.A0G
            java.lang.Object r0 = r0.get()
            X.9AL r0 = (X.AnonymousClass9AL) r0
            int r1 = X.AnonymousClass1Y3.B6q
            X.0UN r0 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1
            X.1Y8 r0 = X.AnonymousClass158.A04
            boolean r0 = r1.Aep(r0, r4)
            if (r0 != 0) goto L_0x0075
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 0
            if (r6 != r1) goto L_0x004d
            r0 = 1
        L_0x004d:
            if (r0 == 0) goto L_0x005f
            X.0US r0 = r5.A0K
            java.lang.Object r1 = r0.get()
            X.0aD r1 = (X.C05720aD) r1
            java.lang.String r0 = "dialtone_photo_interstitial"
            boolean r0 = r1.A08(r0)
            if (r0 == 0) goto L_0x0075
        L_0x005f:
            boolean r0 = X.AnonymousClass5VN.A00(r6)
            if (r0 == 0) goto L_0x0076
            X.0US r0 = r5.A0K
            java.lang.Object r1 = r0.get()
            X.0aD r1 = (X.C05720aD) r1
            java.lang.String r0 = "dialtone_video_interstitial"
            boolean r0 = r1.A08(r0)
            if (r0 != 0) goto L_0x0076
        L_0x0075:
            return r2
        L_0x0076:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28161eM.A0h(java.lang.Integer):boolean");
    }
}
