package X;

import com.facebook.yoga.YogaNative;
import com.facebook.yoga.YogaNodeJNIBase;

/* renamed from: X.1K4  reason: invalid class name */
public final class AnonymousClass1K4 extends YogaNodeJNIBase {
    public void finalize() {
        int A03 = C000700l.A03(-1925225205);
        try {
            long j = this.mNativePointer;
            if (j != 0) {
                this.mNativePointer = 0;
                YogaNative.jni_YGNodeFreeJNI(j);
            }
        } finally {
            super.finalize();
            C000700l.A09(330946702, A03);
        }
    }

    public AnonymousClass1K4() {
    }

    public AnonymousClass1K4(C24001Ru r3) {
        super(YogaNative.jni_YGNodeNewWithConfigJNI(((AnonymousClass1KD) r3).A00));
    }
}
