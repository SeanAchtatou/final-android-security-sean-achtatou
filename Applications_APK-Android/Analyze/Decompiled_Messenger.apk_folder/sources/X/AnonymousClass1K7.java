package X;

import android.graphics.Path;
import android.graphics.RectF;

/* renamed from: X.1K7  reason: invalid class name */
public final class AnonymousClass1K7 {
    public static Path A00(float f, float f2, float f3, int i) {
        float f4 = (float) i;
        RectF rectF = new RectF(0.0f, 0.0f, f4, f4);
        Path path = new Path();
        float height = rectF.height() / 2.0f;
        float f5 = f2 * 2.0f * height;
        float f6 = f * 2.0f * height;
        path.reset();
        double d = (double) (height + f5);
        double d2 = (double) height;
        double abs = (double) Math.abs(f6);
        float A00 = C24271Sy.A00(d, d2, abs);
        int i2 = 0;
        if (f < 0.0f) {
            i2 = AnonymousClass1Y3.A1Q;
        }
        float f7 = (float) i2;
        path.addArc(rectF, A00 + f7 + f3, 360.0f - (A00 * 2.0f));
        double radians = Math.toRadians((double) f3);
        rectF.offset(((float) Math.cos(radians)) * f6, ((float) Math.sin(radians)) * f6);
        float f8 = -f5;
        rectF.inset(f8, f8);
        float A002 = C24271Sy.A00(d2, d, abs);
        path.arcTo(rectF, 180.0f + A002 + f7 + f3, A002 * -2.0f);
        path.close();
        return path;
    }

    public static Path A01(int i) {
        float f = (float) i;
        RectF rectF = new RectF(0.0f, 0.0f, f, f);
        Path path = new Path();
        path.addOval(rectF, Path.Direction.CW);
        path.close();
        return path;
    }
}
