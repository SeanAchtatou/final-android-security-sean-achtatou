package X;

/* renamed from: X.0lh  reason: invalid class name */
public final class AnonymousClass0lh implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.fbservice.service.BlueServiceQueue$3";
    public final /* synthetic */ C11000la A00;

    public AnonymousClass0lh(C11000la r1) {
        this.A00 = r1;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Region traversal failed: Recursive call in traverseIterativeStepInternal method
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Missing exception handler attribute for start block: B:105:0x016f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:156:0x0266 */
    public void run() {
        /*
            r22 = this;
            r0 = r22
            X.0la r5 = r0.A00
            X.1YI r1 = r5.A0C
            r0 = 30
            r6 = 0
            boolean r14 = r1.AbO(r0, r6)
        L_0x000d:
            X.0lc r0 = r5.A0J
            if (r0 != 0) goto L_0x0272
            monitor-enter(r5)
            java.util.LinkedList r0 = r5.A0E     // Catch:{ all -> 0x026f }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x026f }
            r1 = 1
            if (r0 == 0) goto L_0x0039
            boolean r0 = r5.A02     // Catch:{ all -> 0x026f }
            if (r0 != 0) goto L_0x025c
            r5.A02 = r1     // Catch:{ all -> 0x026f }
            java.util.Set r0 = r5.A0G     // Catch:{ all -> 0x026f }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x026f }
        L_0x0027:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x026f }
            if (r0 == 0) goto L_0x025c
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x026f }
            X.0lf r1 = (X.AnonymousClass0lf) r1     // Catch:{ all -> 0x026f }
            java.lang.Class r0 = r5.A0D     // Catch:{ all -> 0x026f }
            r1.Bki(r0)     // Catch:{ all -> 0x026f }
            goto L_0x0027
        L_0x0039:
            r5.A02 = r6     // Catch:{ all -> 0x026f }
            java.util.LinkedList r0 = r5.A0E     // Catch:{ all -> 0x026f }
            java.lang.Object r10 = r0.removeFirst()     // Catch:{ all -> 0x026f }
            X.0lb r10 = (X.C11010lb) r10     // Catch:{ all -> 0x026f }
            java.util.Map r1 = r5.A0F     // Catch:{ all -> 0x026f }
            java.lang.String r0 = r10.A03     // Catch:{ all -> 0x026f }
            java.lang.Object r7 = r1.get(r0)     // Catch:{ all -> 0x026f }
            X.0lc r7 = (X.C11020lc) r7     // Catch:{ all -> 0x026f }
            if (r7 != 0) goto L_0x0058
            java.lang.String r1 = "BlueServiceQueue"
            java.lang.String r0 = "No holder for queued operation!"
            X.C010708t.A0J(r1, r0)     // Catch:{ all -> 0x026f }
            monitor-exit(r5)     // Catch:{ all -> 0x026f }
            goto L_0x000d
        L_0x0058:
            X.069 r0 = r5.A0A     // Catch:{ all -> 0x026f }
            long r0 = r0.now()     // Catch:{ all -> 0x026f }
            r7.A01 = r0     // Catch:{ all -> 0x026f }
            r5.A00 = r7     // Catch:{ all -> 0x026f }
            boolean r0 = r10.A00     // Catch:{ all -> 0x026f }
            if (r0 != 0) goto L_0x006b
            X.0UW r0 = r5.A09     // Catch:{ all -> 0x026f }
            r0.A03()     // Catch:{ all -> 0x026f }
        L_0x006b:
            monitor-exit(r5)     // Catch:{ all -> 0x026f }
            r0 = 3
            boolean r0 = X.C010708t.A0X(r0)
            java.lang.String r2 = r10.A04
            r1 = 82372870(0x4e8e906, float:5.4756936E-36)
            java.lang.String r0 = "BlueService (%s)"
            X.C005505z.A05(r0, r2, r1)
            r2 = 0
            long r8 = r7.A01     // Catch:{ all -> 0x0184 }
            long r0 = r7.A08     // Catch:{ all -> 0x0184 }
            int r4 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            r1 = 0
            if (r4 < 0) goto L_0x0087
            r1 = 1
        L_0x0087:
            java.lang.String r0 = "Must set startTime before invoking getElapsedQueuedTime"
            com.google.common.base.Preconditions.checkState(r1, r0)     // Catch:{ all -> 0x0184 }
            java.util.concurrent.atomic.AtomicBoolean r0 = r5.A0H     // Catch:{ all -> 0x0184 }
            boolean r0 = r0.get()     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x017c
            android.os.Bundle r1 = r10.A01     // Catch:{ all -> 0x0184 }
            java.lang.String r0 = "overridden_viewer_context"
            android.os.Parcelable r1 = r1.getParcelable(r0)     // Catch:{ all -> 0x0184 }
            com.facebook.auth.viewercontext.ViewerContext r1 = (com.facebook.auth.viewercontext.ViewerContext) r1     // Catch:{ all -> 0x0184 }
            X.0aY r0 = r5.A04     // Catch:{ all -> 0x0170 }
            X.0il r13 = r0.Byt(r1)     // Catch:{ all -> 0x0170 }
            com.facebook.common.callercontext.CallerContext r12 = r10.A02     // Catch:{ all -> 0x0167 }
            X.0xw r4 = r7.A0A     // Catch:{ all -> 0x0167 }
            java.lang.ThreadLocal r0 = X.C11050ll.A00     // Catch:{ all -> 0x0167 }
            r0.set(r12)     // Catch:{ all -> 0x0167 }
            java.lang.ThreadLocal r0 = X.AnonymousClass0lm.A00     // Catch:{ all -> 0x0167 }
            r0.set(r4)     // Catch:{ all -> 0x0167 }
            X.0ln r8 = new X.0ln     // Catch:{ all -> 0x0167 }
            java.lang.String r11 = r10.A04     // Catch:{ all -> 0x0167 }
            android.os.Bundle r9 = r10.A01     // Catch:{ all -> 0x0167 }
            java.lang.String r4 = r10.A03     // Catch:{ all -> 0x0167 }
            X.0xw r1 = r7.A0A     // Catch:{ all -> 0x0167 }
            X.0ld r0 = r7.A02     // Catch:{ all -> 0x0167 }
            r15 = r8
            r16 = r11
            r17 = r9
            r18 = r4
            r19 = r1
            r20 = r12
            r21 = r0
            r15.<init>(r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x0167 }
            java.lang.String r1 = "BlueServiceHandlerProvider.get()"
            r0 = -833481591(0xffffffffce521489, float:-8.8114029E8)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x0167 }
            X.0Tq r0 = r5.A0I     // Catch:{ all -> 0x015f }
            java.lang.Object r4 = r0.get()     // Catch:{ all -> 0x015f }
            X.1cz r4 = (X.C27311cz) r4     // Catch:{ all -> 0x015f }
            r0 = -715825943(0xffffffffd5555ce9, float:-1.46621889E13)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0167 }
            monitor-enter(r5)     // Catch:{ all -> 0x0167 }
            boolean r0 = r7.A07     // Catch:{ all -> 0x015c }
            if (r0 == 0) goto L_0x00f1
            X.0uI r0 = X.C14880uI.A03     // Catch:{ all -> 0x015c }
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A00(r0)     // Catch:{ all -> 0x015c }
        L_0x00ef:
            monitor-exit(r5)     // Catch:{ all -> 0x015c }
            goto L_0x00f3
        L_0x00f1:
            r1 = 0
            goto L_0x00ef
        L_0x00f3:
            if (r1 != 0) goto L_0x0125
            java.lang.String r1 = "BlueServiceHandler.handleOperation"
            r0 = 1022722707(0x3cf58293, float:0.02996949)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x0167 }
            com.facebook.fbservice.service.OperationResult r1 = r4.BAz(r8)     // Catch:{ CancellationException -> 0x0105 }
            r0 = -1698558234(0xffffffff9ac20ee6, float:-8.026059E-23)
            goto L_0x0115
        L_0x0105:
            r1 = move-exception
            monitor-enter(r5)     // Catch:{ all -> 0x011d }
            boolean r0 = r7.A07     // Catch:{ all -> 0x011a }
            if (r0 == 0) goto L_0x0119
            X.0uI r0 = X.C14880uI.A03     // Catch:{ all -> 0x011a }
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A00(r0)     // Catch:{ all -> 0x011a }
            monitor-exit(r5)     // Catch:{ all -> 0x011a }
            r0 = 579159600(0x22854630, float:3.6124036E-18)
        L_0x0115:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0167 }
            goto L_0x0125
        L_0x0119:
            throw r1     // Catch:{ all -> 0x011a }
        L_0x011a:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x011a }
            throw r0     // Catch:{ all -> 0x011d }
        L_0x011d:
            r1 = move-exception
            r0 = -538098777(0xffffffffdfed43a7, float:-3.4193384E19)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0167 }
            goto L_0x0166
        L_0x0125:
            if (r14 == 0) goto L_0x0137
            boolean r0 = r1 instanceof com.facebook.fbservice.service.FutureOperationResult     // Catch:{ all -> 0x0167 }
            if (r0 == 0) goto L_0x0134
            r0 = r1
            com.facebook.fbservice.service.FutureOperationResult r0 = (com.facebook.fbservice.service.FutureOperationResult) r0     // Catch:{ all -> 0x0167 }
            com.google.common.util.concurrent.ListenableFuture r0 = r0.future     // Catch:{ all -> 0x0167 }
            X.C11000la.A02(r5, r7, r0)     // Catch:{ all -> 0x0167 }
            goto L_0x0137
        L_0x0134:
            X.C11000la.A01(r5, r7, r1)     // Catch:{ all -> 0x0167 }
        L_0x0137:
            if (r13 == 0) goto L_0x013c
            r13.close()     // Catch:{ all -> 0x0170 }
        L_0x013c:
            java.lang.ThreadLocal r0 = X.C11050ll.A00     // Catch:{ all -> 0x0184 }
            r0.remove()     // Catch:{ all -> 0x0184 }
            java.lang.ThreadLocal r0 = X.AnonymousClass0lm.A00     // Catch:{ all -> 0x0184 }
            r0.remove()     // Catch:{ all -> 0x0184 }
            if (r14 != 0) goto L_0x0157
            boolean r0 = r1 instanceof com.facebook.fbservice.service.FutureOperationResult     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x0154
            com.facebook.fbservice.service.FutureOperationResult r1 = (com.facebook.fbservice.service.FutureOperationResult) r1     // Catch:{ all -> 0x0184 }
            com.google.common.util.concurrent.ListenableFuture r0 = r1.future     // Catch:{ all -> 0x0184 }
            X.C11000la.A02(r5, r7, r0)     // Catch:{ all -> 0x0184 }
            goto L_0x0157
        L_0x0154:
            X.C11000la.A01(r5, r7, r1)     // Catch:{ all -> 0x0184 }
        L_0x0157:
            r0 = 375067921(0x165b1511, float:1.7697327E-25)
            goto L_0x0257
        L_0x015c:
            r1 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x015c }
            goto L_0x0166
        L_0x015f:
            r1 = move-exception
            r0 = 209398014(0xc7b28fe, float:1.9348672E-31)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x0167 }
        L_0x0166:
            throw r1     // Catch:{ all -> 0x0167 }
        L_0x0167:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0169 }
        L_0x0169:
            r0 = move-exception
            if (r13 == 0) goto L_0x016f
            r13.close()     // Catch:{ all -> 0x016f }
        L_0x016f:
            throw r0     // Catch:{ all -> 0x0170 }
        L_0x0170:
            r1 = move-exception
            java.lang.ThreadLocal r0 = X.C11050ll.A00     // Catch:{ all -> 0x0184 }
            r0.remove()     // Catch:{ all -> 0x0184 }
            java.lang.ThreadLocal r0 = X.AnonymousClass0lm.A00     // Catch:{ all -> 0x0184 }
            r0.remove()     // Catch:{ all -> 0x0184 }
            goto L_0x0183
        L_0x017c:
            java.util.concurrent.CancellationException r1 = new java.util.concurrent.CancellationException     // Catch:{ all -> 0x0184 }
            java.lang.String r0 = "Queue stopped"
            r1.<init>(r0)     // Catch:{ all -> 0x0184 }
        L_0x0183:
            throw r1     // Catch:{ all -> 0x0184 }
        L_0x0184:
            r8 = move-exception
            java.lang.String r1 = "BlueServiceQueue"
            java.lang.String r0 = "Exception during service"
            X.C010708t.A0M(r1, r0, r8)     // Catch:{ all -> 0x0267 }
            java.lang.String r11 = r10.A04     // Catch:{ all -> 0x0267 }
            com.facebook.common.callercontext.CallerContext r9 = r10.A02     // Catch:{ all -> 0x0267 }
            com.google.common.collect.ImmutableList r0 = X.AnonymousClass204.A00     // Catch:{ all -> 0x0267 }
            X.1Xv r12 = r0.iterator()     // Catch:{ all -> 0x0267 }
        L_0x0196:
            boolean r0 = r12.hasNext()     // Catch:{ all -> 0x0267 }
            if (r0 == 0) goto L_0x01bc
            java.lang.Object r4 = r12.next()     // Catch:{ all -> 0x0267 }
            java.lang.Class r4 = (java.lang.Class) r4     // Catch:{ all -> 0x0267 }
            java.util.List r0 = com.google.common.base.Throwables.getCausalChain(r8)     // Catch:{ all -> 0x0267 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x0267 }
            com.google.common.base.Preconditions.checkNotNull(r4)     // Catch:{ all -> 0x0267 }
            X.8mp r1 = new X.8mp     // Catch:{ all -> 0x0267 }
            r1.<init>(r0, r4)     // Catch:{ all -> 0x0267 }
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass0j4.A06(r1, r0)     // Catch:{ all -> 0x0267 }
            java.lang.Exception r0 = (java.lang.Exception) r0     // Catch:{ all -> 0x0267 }
            if (r0 == 0) goto L_0x0196
            r0 = 1
            goto L_0x01bd
        L_0x01bc:
            r0 = 0
        L_0x01bd:
            if (r0 == 0) goto L_0x01fb
            X.0bA r12 = r5.A03     // Catch:{ all -> 0x0267 }
            java.lang.Integer r4 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "orca_service_exception"
            X.0ZF r4 = r12.A05(r0, r6, r4, r6)     // Catch:{ all -> 0x0267 }
            boolean r0 = r4.A0G()     // Catch:{ all -> 0x0267 }
            if (r0 == 0) goto L_0x0220
            java.lang.Class r0 = r8.getClass()     // Catch:{ all -> 0x0267 }
            java.lang.String r1 = r0.getSimpleName()     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "type"
            r4.A0A(r0, r1)     // Catch:{ all -> 0x0267 }
            java.lang.String r1 = r8.getMessage()     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "msg"
            r4.A0A(r0, r1)     // Catch:{ all -> 0x0267 }
            if (r11 == 0) goto L_0x01ec
            java.lang.String r0 = "operation"
            r4.A0A(r0, r11)     // Catch:{ all -> 0x0267 }
        L_0x01ec:
            if (r9 == 0) goto L_0x01f7
            java.lang.String r1 = r9.toString()     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "caller_context"
            r4.A0A(r0, r1)     // Catch:{ all -> 0x0267 }
        L_0x01f7:
            r4.A0E()     // Catch:{ all -> 0x0267 }
            goto L_0x0220
        L_0x01fb:
            X.09P r4 = r5.A06     // Catch:{ all -> 0x0267 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0267 }
            r1.<init>()     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "Failed BlueService operation ["
            r1.append(r0)     // Catch:{ all -> 0x0267 }
            r1.append(r11)     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = ", "
            r1.append(r0)     // Catch:{ all -> 0x0267 }
            r1.append(r9)     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "]"
            r1.append(r0)     // Catch:{ all -> 0x0267 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "BlueServiceQueue"
            r4.softReport(r0, r1, r8)     // Catch:{ all -> 0x0267 }
        L_0x0220:
            boolean r0 = r10.A05     // Catch:{ all -> 0x0267 }
            if (r0 == 0) goto L_0x022a
            com.facebook.fbservice.service.OperationResult r4 = new com.facebook.fbservice.service.OperationResult     // Catch:{ all -> 0x0267 }
            r4.<init>(r8)     // Catch:{ all -> 0x0267 }
            goto L_0x0236
        L_0x022a:
            X.0uI r1 = X.AnonymousClass9HV.A01(r8)     // Catch:{ all -> 0x0267 }
            android.os.Bundle r0 = X.AnonymousClass9HV.A00(r8)     // Catch:{ all -> 0x0267 }
            com.facebook.fbservice.service.OperationResult r4 = com.facebook.fbservice.service.OperationResult.A01(r1, r0, r8)     // Catch:{ all -> 0x0267 }
        L_0x0236:
            if (r14 == 0) goto L_0x0251
            android.os.Bundle r1 = r10.A01     // Catch:{ all -> 0x0267 }
            java.lang.String r0 = "overridden_viewer_context"
            android.os.Parcelable r1 = r1.getParcelable(r0)     // Catch:{ all -> 0x0267 }
            com.facebook.auth.viewercontext.ViewerContext r1 = (com.facebook.auth.viewercontext.ViewerContext) r1     // Catch:{ all -> 0x0267 }
            X.0aY r0 = r5.A04     // Catch:{ all -> 0x0267 }
            X.0il r1 = r0.Byt(r1)     // Catch:{ all -> 0x0267 }
            X.C11000la.A01(r5, r7, r4)     // Catch:{ all -> 0x025e }
            if (r1 == 0) goto L_0x0254
            r1.close()     // Catch:{ all -> 0x0267 }
            goto L_0x0254
        L_0x0251:
            X.C11000la.A01(r5, r7, r4)     // Catch:{ all -> 0x0267 }
        L_0x0254:
            r0 = 1642073636(0x61e00e24, float:5.166362E20)
        L_0x0257:
            X.C005505z.A02(r2, r0)
            goto L_0x000d
        L_0x025c:
            monitor-exit(r5)     // Catch:{ all -> 0x026f }
            return
        L_0x025e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0260 }
        L_0x0260:
            r0 = move-exception
            if (r1 == 0) goto L_0x0266
            r1.close()     // Catch:{ all -> 0x0266 }
        L_0x0266:
            throw r0     // Catch:{ all -> 0x0267 }
        L_0x0267:
            r1 = move-exception
            r0 = 670332372(0x27f475d4, float:6.7851354E-15)
            X.C005505z.A02(r2, r0)
            throw r1
        L_0x026f:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x026f }
            throw r0
        L_0x0272:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0lh.run():void");
    }
}
