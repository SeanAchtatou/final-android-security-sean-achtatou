package X;

import java.io.FileInputStream;

/* renamed from: X.0Nm  reason: invalid class name and case insensitive filesystem */
public final class C03370Nm extends AnonymousClass0EM {
    private int A00;
    public final /* synthetic */ C03360Nl A01;

    public C03370Nm(C03360Nl r1) {
        this.A01 = r1;
    }

    public C03930Qq A00() {
        AnonymousClass0Nk[] r2 = this.A01.A00;
        int i = this.A00;
        this.A00 = i + 1;
        AnonymousClass0Nk r22 = r2[i];
        FileInputStream fileInputStream = new FileInputStream(r22.A00);
        try {
            return new C03930Qq(r22, fileInputStream);
        } catch (Throwable th) {
            fileInputStream.close();
            throw th;
        }
    }

    public boolean A01() {
        if (this.A00 < this.A01.A00.length) {
            return true;
        }
        return false;
    }
}
