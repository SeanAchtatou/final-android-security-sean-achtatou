package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0wV  reason: invalid class name and case insensitive filesystem */
public final class C16110wV implements C16120wW {
    public final /* synthetic */ RecyclerView A00;

    public void ByN(C33781o8 r3, C60862xx r4, C60862xx r5) {
        r3.A0A(false);
        RecyclerView recyclerView = this.A00;
        if (recyclerView.A0T) {
            if (!recyclerView.A0K.A0I(r3, r3, r4, r5)) {
                return;
            }
        } else if (!recyclerView.A0K.A0G(r3, r4, r5)) {
            return;
        }
        this.A00.A0f();
    }

    public C16110wV(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    public void ByG(C33781o8 r3, C60862xx r4, C60862xx r5) {
        RecyclerView recyclerView = this.A00;
        r3.A0A(false);
        if (recyclerView.A0K.A0E(r3, r4, r5)) {
            recyclerView.A0f();
        }
    }

    public void ByH(C33781o8 r3, C60862xx r4, C60862xx r5) {
        this.A00.A0w.A0A(r3);
        RecyclerView recyclerView = this.A00;
        RecyclerView.A0R(recyclerView, r3);
        r3.A0A(false);
        if (recyclerView.A0K.A0F(r3, r4, r5)) {
            recyclerView.A0f();
        }
    }

    public void CK5(C33781o8 r4) {
        RecyclerView recyclerView = this.A00;
        recyclerView.A0L.A1C(r4.A0H, recyclerView.A0w);
    }
}
