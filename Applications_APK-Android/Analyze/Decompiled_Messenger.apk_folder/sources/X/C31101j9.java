package X;

import com.facebook.stash.core.Stash;
import java.io.File;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1j9  reason: invalid class name and case insensitive filesystem */
public final class C31101j9 extends C31091j8 implements Stash {
    private final List A00;

    public C31101j9(Stash stash, List list) {
        super(stash);
        this.A00 = list;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ((C30981ix) it.next()).AP6(this);
        }
    }

    public File getResource(String str) {
        File resource = super.getResource(str);
        for (C30981ix BaB : this.A00) {
            BaB.BaB(str, resource);
        }
        return resource;
    }

    public File insert(String str) {
        File insert = super.insert(str);
        for (C30981ix BbN : this.A00) {
            BbN.BbN(str, insert);
        }
        return insert;
    }

    public boolean remove(String str) {
        boolean remove = super.remove(str);
        for (C30981ix Bla : this.A00) {
            Bla.Bla(str, 0);
        }
        return remove;
    }
}
