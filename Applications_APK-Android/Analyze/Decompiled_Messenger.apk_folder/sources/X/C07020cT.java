package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.LongSparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0cT  reason: invalid class name and case insensitive filesystem */
public final class C07020cT extends LongSparseArray {
    private static AtomicBoolean A02 = new AtomicBoolean(false);
    public static final CountDownLatch A03 = new CountDownLatch(1);
    public static volatile C25821aS A04;
    public static volatile Thread A05;
    public final Resources A00;
    public final C05170Xx A01;

    public Object get(long j) {
        Drawable A042;
        boolean z;
        boolean z2;
        int i;
        long j2 = j;
        Drawable.ConstantState constantState = (Drawable.ConstantState) super.get(j2);
        if (constantState != null) {
            return constantState;
        }
        Thread thread = A05;
        if (thread != null) {
            thread.setPriority(10);
            C005505z.A03("wait-for-init", 1201991624);
            try {
                A03.await();
                i = -1372366333;
            } catch (InterruptedException e) {
                AnonymousClass02w.A05(C07020cT.class, "Unexpected interrupt", e);
                i = 1736009748;
            } catch (Throwable th) {
                C005505z.A00(-924173601);
                throw th;
            }
            C005505z.A00(i);
        }
        C25821aS r7 = A04;
        Resources resources = this.A00;
        C05170Xx r5 = this.A01;
        boolean z3 = false;
        if (((int) (j >> 32)) == r7.A00) {
            z3 = true;
        }
        if (z3) {
            int i2 = (int) j2;
            if (r7.A03 != null) {
                int A002 = C04960Xb.A00(resources);
                synchronized (r7.A03) {
                    try {
                        z2 = r7.A03.A00.get(A002);
                    } catch (Throwable th2) {
                        while (true) {
                            th = th2;
                        }
                    }
                }
                if (!z2) {
                    TypedValue typedValue = new TypedValue();
                    SparseIntArray sparseIntArray = new SparseIntArray();
                    boolean z4 = true;
                    while (true) {
                        SparseIntArray sparseIntArray2 = r7.A02;
                        int size = sparseIntArray2.size();
                        for (int i3 = 0; i3 < size; i3++) {
                            sparseIntArray.append(sparseIntArray2.keyAt(i3), sparseIntArray2.valueAt(i3));
                        }
                        if (z4) {
                            for (int i4 : r7.A03.A01) {
                                if (C04960Xb.A03(typedValue, resources, i4)) {
                                    sparseIntArray.put(typedValue.data, i4);
                                }
                            }
                        }
                        synchronized (r7.A03) {
                            try {
                                if (sparseIntArray2 != r7.A02) {
                                    if (r7.A03.A00.get(A002)) {
                                        break;
                                    }
                                } else {
                                    r7.A02 = sparseIntArray;
                                    r7.A03.A00.put(A002, true);
                                    break;
                                }
                            } catch (Throwable th3) {
                                th = th3;
                                throw th;
                            }
                        }
                        z4 = false;
                    }
                }
            }
            int i5 = r7.A02.get(i2);
            if (i5 != 0) {
                A042 = r5.A05(i5, resources);
                z = true;
            } else {
                i5 = r7.A01.get(i2);
                if (i5 != 0) {
                    A042 = r5.A04(i5, resources);
                    z = false;
                }
            }
            return C04960Xb.A01(A042, i5, z);
        }
        return null;
    }

    public C07020cT(LongSparseArray longSparseArray, C05170Xx r8, Resources resources) {
        this.A01 = r8;
        this.A00 = resources;
        int size = longSparseArray.size();
        for (int i = 0; i < size; i++) {
            append(longSparseArray.keyAt(i), (Drawable.ConstantState) longSparseArray.valueAt(i));
        }
        if (A02.compareAndSet(false, true)) {
            Thread thread = new Thread(new C05510Zf(this), "CustomDrawablesCache-init");
            A05 = thread;
            thread.setPriority(1);
            thread.start();
        }
    }
}
