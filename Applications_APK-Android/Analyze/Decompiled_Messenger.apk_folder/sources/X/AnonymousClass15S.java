package X;

import android.content.Context;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.io.File;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.15S  reason: invalid class name */
public final class AnonymousClass15S extends C12040oR {
    private static volatile AnonymousClass15S A01;
    public AnonymousClass0UN A00;

    /* JADX INFO: finally extract failed */
    public File A02(C31071j6 r8) {
        short s = 3;
        try {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerStart(38469633, "feature", r8.A03);
            File A02 = super.A02(r8);
            int i = AnonymousClass1Y3.BBd;
            if (((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).isMarkerOn(38469633)) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerAnnotate(38469633, "exists", A02.exists());
            }
            QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00);
            if (A02 != null) {
                s = 2;
            }
            quickPerformanceLogger.markerEnd(38469633, s);
            return A02;
        } catch (Throwable th) {
            QuickPerformanceLogger quickPerformanceLogger2 = (QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00);
            if (0 != 0) {
                s = 2;
            }
            quickPerformanceLogger2.markerEnd(38469633, s);
            throw th;
        }
    }

    public File A03(C31071j6 r9) {
        short s = 3;
        try {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerStart(38469635, "feature", r9.A03);
            File A03 = super.A03(r9);
            if (A03 == null) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).CGS("FbAttributedPathProvider", AnonymousClass08S.A0J("getWithoutInit returned a null path for the config feature: ", r9.A03));
            }
            QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00);
            if (A03 != null) {
                s = 2;
            }
            quickPerformanceLogger.markerEnd(38469635, s);
            return A03;
        } catch (Throwable th) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(38469635, 3);
            throw th;
        }
    }

    public File A04(File file, C31071j6 r8) {
        try {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerStart(38469639, "feature", r8.A03);
            super.A04(file, r8);
            return file;
        } finally {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(38469639, 2);
        }
    }

    public void A05(Context context) {
        try {
            int i = AnonymousClass1Y3.BBd;
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerStart(38469638);
            super.A05(context);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerEnd(38469638, 2);
        } catch (Throwable th) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(38469638, 2);
            throw th;
        }
    }

    public static final AnonymousClass15S A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (AnonymousClass15S.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A01 = new AnonymousClass15S(applicationInjector, AnonymousClass1YA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass15S(AnonymousClass1XY r3, Context context) {
        this.A00 = new AnonymousClass0UN(7, r3);
        A05(context);
    }
}
