package X;

import java.io.IOException;

/* renamed from: X.0tY  reason: invalid class name */
public final class AnonymousClass0tY {
    public static final C182811d A00(C28271eX r1) {
        C182811d nextToken = r1.nextToken();
        if (nextToken != null) {
            return nextToken;
        }
        throw new IOException("Unexpected end of json input");
    }
}
