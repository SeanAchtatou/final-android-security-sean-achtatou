package X;

/* renamed from: X.01V  reason: invalid class name */
public final class AnonymousClass01V {
    public static final Integer A00 = AnonymousClass07B.A0Y;

    public static Integer A00(String str) {
        Integer num = null;
        for (Integer num2 : AnonymousClass07B.A00(5)) {
            if (str.matches(AnonymousClass08S.A0P("^.+", A01(num2).replace(".", "\\."), "(_[a-z]+)?$")) && (num == null || A01(num2).length() > A01(num).length())) {
                num = num2;
            }
        }
        return num;
    }

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return ".v2.txt";
            case 2:
                return ".v3.txt";
            case 3:
                return ".v4.txt";
            case 4:
                return ".v5.txt";
            default:
                return ".txt";
        }
    }
}
