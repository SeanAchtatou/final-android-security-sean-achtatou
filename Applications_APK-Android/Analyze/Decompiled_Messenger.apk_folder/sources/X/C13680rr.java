package X;

import android.database.Cursor;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0rr  reason: invalid class name and case insensitive filesystem */
public final class C13680rr {
    public Cursor A00;
    public ImmutableList A01;
    public final int A02;

    public ImmutableList A00() {
        if (this.A01 == null) {
            ImmutableList.Builder builder = ImmutableList.builder();
            int position = this.A00.getPosition();
            try {
                this.A00.moveToPosition(-1);
                while (this.A00.moveToNext()) {
                    String string = this.A00.getString(this.A02);
                    ThreadKey A06 = ThreadKey.A06(string);
                    if (A06 == null) {
                        C010708t.A0P("DefaultThreadResultsCursor", "Error parsing thread key from db cursor: %s", string);
                    } else {
                        builder.add((Object) A06);
                    }
                }
                this.A01 = builder.build();
            } finally {
                this.A00.moveToPosition(position);
            }
        }
        return this.A01;
    }

    public C13680rr(Cursor cursor, String str) {
        Preconditions.checkNotNull(cursor);
        this.A00 = cursor;
        this.A02 = cursor.getColumnIndexOrThrow(str);
    }
}
