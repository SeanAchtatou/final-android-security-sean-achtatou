package X;

import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.graphservice.factory.GraphQLServiceFactory;
import com.facebook.graphservice.interfaces.Tree;
import com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.graphservice.tree.TreeJNI;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.1ck  reason: invalid class name and case insensitive filesystem */
public final class C27161ck extends C12190on implements C12200oo {
    public static GSMBuilderShape0S0000000 A00(C27161ck r3, GraphQLServiceFactory graphQLServiceFactory) {
        if (r3 == null || !(r3 instanceof Tree) || !r3.isValid()) {
            return null;
        }
        return (GSMBuilderShape0S0000000) graphQLServiceFactory.newTreeBuilder("MessengerInboxUnit", GSMBuilderShape0S0000000.class, 738428414, r3);
    }

    public static C27161ck A01(C27161ck r2, GraphQLServiceFactory graphQLServiceFactory) {
        if ((r2 instanceof TreeJNI) && r2.isValid()) {
            return (C27161ck) r2.reinterpret(C27161ck.class, 738428414);
        }
        GSMBuilderShape0S0000000 A00 = A00(r2, graphQLServiceFactory);
        if (A00 == null) {
            return null;
        }
        return (C27161ck) A00.getResult(C27161ck.class, 738428414);
    }

    public static ImmutableList A02(ImmutableList immutableList, GraphQLServiceFactory graphQLServiceFactory) {
        if (immutableList == null || immutableList.isEmpty()) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            C27161ck A01 = A01((C27161ck) it.next(), graphQLServiceFactory);
            if (A01 != null) {
                builder.add((Object) A01);
            }
        }
        return builder.build();
    }

    public GraphQLMessengerInboxUnitType A0Q() {
        return (GraphQLMessengerInboxUnitType) A0O(-938051216, GraphQLMessengerInboxUnitType.A0S);
    }

    public GSTModelShape1S0000000 A0R() {
        return (GSTModelShape1S0000000) A0J(-20072456, GSTModelShape1S0000000.class, -1607226379);
    }

    public GSTModelShape1S0000000 A0S() {
        return (GSTModelShape1S0000000) A0J(984710882, GSTModelShape1S0000000.class, 281328026);
    }

    public ImmutableList A0T() {
        return A0M(974865482, GSTModelShape1S0000000.class, -123977991);
    }

    public String A0U() {
        return A0P(AnonymousClass1Y3.ARf);
    }

    private C27161ck(int i, int[] iArr) {
        super(i, iArr);
    }
}
