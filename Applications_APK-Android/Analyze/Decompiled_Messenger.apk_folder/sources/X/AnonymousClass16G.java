package X;

import android.content.Intent;

/* renamed from: X.16G  reason: invalid class name */
public final class AnonymousClass16G implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.chatheads.ipc.ChatHeadsBroadcaster$1";
    public final /* synthetic */ Intent A00;
    public final /* synthetic */ C18010zv A01;

    public AnonymousClass16G(C18010zv r1, Intent intent) {
        this.A01 = r1;
        this.A00 = intent;
    }

    public void run() {
        C18010zv.A02(this.A01, this.A00);
    }
}
