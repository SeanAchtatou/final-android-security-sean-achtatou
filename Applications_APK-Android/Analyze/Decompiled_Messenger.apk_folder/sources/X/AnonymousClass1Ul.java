package X;

/* renamed from: X.1Ul  reason: invalid class name */
public final class AnonymousClass1Ul {
    public static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("android_contacts_omnistore/")).A09("index_disabled"));
    public AnonymousClass0UN A00;
    public final boolean A01;

    public static final AnonymousClass1Ul A00(AnonymousClass1XY r2) {
        return new AnonymousClass1Ul(r2, AnonymousClass0UU.A02(r2));
    }

    public static final AnonymousClass1Ul A01(AnonymousClass1XY r2) {
        return new AnonymousClass1Ul(r2, AnonymousClass0UU.A02(r2));
    }

    public Integer A02() {
        if (this.A01) {
            return AnonymousClass07B.A01;
        }
        return AnonymousClass07B.A00;
    }

    private AnonymousClass1Ul(AnonymousClass1XY r9, C001300x r10) {
        this.A00 = new AnonymousClass0UN(2, r9);
        long parseLong = Long.parseLong(r10.A04);
        if (parseLong == 312713275593566L) {
            this.A01 = true;
        } else {
            this.A01 = C25011Xz.A05(256002347743983L, 181425161904154L, 105910932827969L, 1104941186305379L, 628551730674460L).contains(Long.valueOf(parseLong));
        }
    }
}
