package X;

/* renamed from: X.1QD  reason: invalid class name */
public class AnonymousClass1QD implements AnonymousClass1Q3 {
    public final C23311Pa A00;
    private final C22601Mc A01;
    private final AnonymousClass1Q3 A02;

    public String A01() {
        return C99084oO.$const$string(!(this instanceof AnonymousClass1QF) ? AnonymousClass1Y3.A0i : 100);
    }

    public C23581Qb A00(C23581Qb r2, C23601Qd r3, boolean z) {
        if (!(this instanceof AnonymousClass1QF)) {
            return new AnonymousClass1RM(this, r2, r3, z);
        }
        return r2;
    }

    public AnonymousClass1QD(C23311Pa r1, C22601Mc r2, AnonymousClass1Q3 r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00d6, code lost:
        if (r5 != false) goto L_0x00d8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ByS(X.C23581Qb r11, X.AnonymousClass1QK r12) {
        /*
            r10 = this;
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00e2 }
            if (r0 == 0) goto L_0x000b
            java.lang.String r0 = "BitmapMemoryCacheProducer#produceResults"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x00e2 }
        L_0x000b:
            X.1MN r4 = r12.A07     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r10.A01()     // Catch:{ all -> 0x00e2 }
            r4.Bk8(r12, r0)     // Catch:{ all -> 0x00e2 }
            X.1Q0 r2 = r12.A09     // Catch:{ all -> 0x00e2 }
            java.lang.Object r1 = r12.A0A     // Catch:{ all -> 0x00e2 }
            X.1Mc r0 = r10.A01     // Catch:{ all -> 0x00e2 }
            X.1Qd r8 = r0.A04(r2, r1)     // Catch:{ all -> 0x00e2 }
            X.1Pa r0 = r10.A00     // Catch:{ all -> 0x00e2 }
            X.1PS r9 = r0.Ab8(r8)     // Catch:{ all -> 0x00e2 }
            java.lang.String r7 = "memory_bitmap"
            java.lang.String r6 = "cached_value_found"
            r2 = 1
            r3 = 0
            if (r9 == 0) goto L_0x006b
            java.lang.Object r0 = r9.A0A()     // Catch:{ all -> 0x00e2 }
            X.1S9 r0 = (X.AnonymousClass1S9) r0     // Catch:{ all -> 0x00e2 }
            X.1nJ r0 = r0.Azo()     // Catch:{ all -> 0x00e2 }
            boolean r5 = r0.A01     // Catch:{ all -> 0x00e2 }
            if (r5 == 0) goto L_0x0062
            java.lang.String r1 = r10.A01()     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r10.A01()     // Catch:{ all -> 0x00e2 }
            boolean r0 = r4.C3Q(r12, r0)     // Catch:{ all -> 0x00e2 }
            if (r0 == 0) goto L_0x0069
            java.lang.String r0 = "true"
            java.util.Map r0 = X.C22665B6t.A01(r6, r0)     // Catch:{ all -> 0x00e2 }
        L_0x004e:
            r4.Bk6(r12, r1, r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r10.A01()     // Catch:{ all -> 0x00e2 }
            r4.Bt8(r12, r0, r2)     // Catch:{ all -> 0x00e2 }
            android.util.SparseArray r0 = r12.A05     // Catch:{ all -> 0x00e2 }
            r0.put(r2, r7)     // Catch:{ all -> 0x00e2 }
            r0 = 1065353216(0x3f800000, float:1.0)
            r11.BkJ(r0)     // Catch:{ all -> 0x00e2 }
        L_0x0062:
            r11.Bgg(r9, r5)     // Catch:{ all -> 0x00e2 }
            r9.close()     // Catch:{ all -> 0x00e2 }
            goto L_0x00d6
        L_0x0069:
            r0 = r3
            goto L_0x004e
        L_0x006b:
            X.1Pw r0 = r12.A08     // Catch:{ all -> 0x00e2 }
            int r1 = r0.mValue     // Catch:{ all -> 0x00e2 }
            X.1Pw r0 = X.C23531Pw.BITMAP_MEMORY_CACHE     // Catch:{ all -> 0x00e2 }
            int r0 = r0.mValue     // Catch:{ all -> 0x00e2 }
            java.lang.String r5 = "false"
            if (r1 < r0) goto L_0x009f
            java.lang.String r1 = r10.A01()     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r10.A01()     // Catch:{ all -> 0x00e2 }
            boolean r0 = r4.C3Q(r12, r0)     // Catch:{ all -> 0x00e2 }
            if (r0 == 0) goto L_0x009d
            java.util.Map r0 = X.C22665B6t.A01(r6, r5)     // Catch:{ all -> 0x00e2 }
        L_0x0089:
            r4.Bk6(r12, r1, r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r1 = r10.A01()     // Catch:{ all -> 0x00e2 }
            r0 = 0
            r4.Bt8(r12, r1, r0)     // Catch:{ all -> 0x00e2 }
            android.util.SparseArray r0 = r12.A05     // Catch:{ all -> 0x00e2 }
            r0.put(r2, r7)     // Catch:{ all -> 0x00e2 }
            r11.Bgg(r3, r2)     // Catch:{ all -> 0x00e2 }
            goto L_0x00d8
        L_0x009d:
            r0 = r3
            goto L_0x0089
        L_0x009f:
            X.1Q0 r0 = r12.A09     // Catch:{ all -> 0x00e2 }
            boolean r0 = r0.A0F     // Catch:{ all -> 0x00e2 }
            X.1Qb r2 = r10.A00(r11, r8, r0)     // Catch:{ all -> 0x00e2 }
            java.lang.String r1 = r10.A01()     // Catch:{ all -> 0x00e2 }
            java.lang.String r0 = r10.A01()     // Catch:{ all -> 0x00e2 }
            boolean r0 = r4.C3Q(r12, r0)     // Catch:{ all -> 0x00e2 }
            if (r0 == 0) goto L_0x00b9
            java.util.Map r3 = X.C22665B6t.A01(r6, r5)     // Catch:{ all -> 0x00e2 }
        L_0x00b9:
            r4.Bk6(r12, r1, r3)     // Catch:{ all -> 0x00e2 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00e2 }
            if (r0 == 0) goto L_0x00c7
            java.lang.String r0 = "mInputProducer.produceResult"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x00e2 }
        L_0x00c7:
            X.1Q3 r0 = r10.A02     // Catch:{ all -> 0x00e2 }
            r0.ByS(r2, r12)     // Catch:{ all -> 0x00e2 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x00e2 }
            if (r0 == 0) goto L_0x00d8
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x00e2 }
            goto L_0x00d8
        L_0x00d6:
            if (r5 == 0) goto L_0x006b
        L_0x00d8:
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x00e1
            X.AnonymousClass1NB.A01()
        L_0x00e1:
            return
        L_0x00e2:
            r1 = move-exception
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x00ec
            X.AnonymousClass1NB.A01()
        L_0x00ec:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QD.ByS(X.1Qb, X.1QK):void");
    }
}
