package X;

import com.facebook.proxygen.LigerSamplePolicy;

/* renamed from: X.0Uj  reason: invalid class name and case insensitive filesystem */
public final class C04360Uj implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appstate.AppStateManager$1";
    public final /* synthetic */ AnonymousClass0Ud A00;

    public C04360Uj(AnonymousClass0Ud r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass0Ud r1 = this.A00;
        r1.A06 = null;
        if (!r1.A0W) {
            AnonymousClass0Ud r3 = this.A00;
            r3.A0P = ((AnonymousClass06B) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AgK, r3.A02)).now() - LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT;
            AnonymousClass0Ud r32 = this.A00;
            r32.A0O = ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, r32.A02)).now() - LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT;
            AnonymousClass0Ud r33 = this.A00;
            if (r33.A03 == null) {
                r33.A03 = Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(17, AnonymousClass1Y3.AOJ, r33.A02)).Aem(285224483362318L));
            }
            boolean booleanValue = r33.A03.booleanValue();
            int i = AnonymousClass1Y3.AdX;
            AnonymousClass1YR r0 = (AnonymousClass1YR) AnonymousClass1XX.A02(15, i, r33.A02);
            if (booleanValue) {
                r0.A01("app_background_report_time_spent_only");
                ((AnonymousClass1YR) AnonymousClass1XX.A02(15, i, r33.A02)).A01("app_backgrounded");
            } else {
                r0.A01("app_backgrounded");
                ((AnonymousClass1YR) AnonymousClass1XX.A02(15, i, r33.A02)).A01("app_background_report_time_spent_only");
            }
            ((C04460Ut) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AKb, r33.A02)).C4y("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP");
            ((C33821oC) AnonymousClass1XX.A02(13, AnonymousClass1Y3.ATT, r33.A02)).A03(false);
        }
    }
}
