package X;

import android.content.Context;
import android.graphics.drawable.Drawable;

/* renamed from: X.1RA  reason: invalid class name */
public final class AnonymousClass1RA {
    private AnonymousClass0UN A00;

    public static final AnonymousClass1RA A00(AnonymousClass1XY r1) {
        return new AnonymousClass1RA(r1);
    }

    public void A01(Context context, AnonymousClass1RB r9, C21381Gs r10, AnonymousClass1KZ r11) {
        int i;
        int Ae4;
        int A002;
        Integer num;
        C37361vX r0;
        if (r10 == null) {
            r10 = C21381Gs.A0L;
        }
        AnonymousClass1MA r6 = (AnonymousClass1MA) AnonymousClass1XX.A03(AnonymousClass1Y3.A9Z, this.A00);
        Integer num2 = (Integer) r11.A07.get(r10);
        if (num2 != null) {
            i = num2.intValue();
        } else if (r10.shouldDrawBackgroundBehindBadge) {
            i = r11.A00;
        } else {
            i = 0;
        }
        boolean z = r10.canResizeBadgeIcon;
        Drawable drawable = null;
        if (r10 != null) {
            if (r10 != C21381Gs.A0Q || C06850cB.A0B(r11.A0B)) {
                r0 = (C37361vX) r11.A08.get(r10);
            } else {
                r0 = (C74023h9) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BOO, r6.A00);
            }
            if (r0 != null) {
                drawable = r0.AUv(context, r10, r11);
            }
        }
        C37361vX r02 = (C37361vX) r11.A08.get(r10);
        if (r02 == null) {
            Ae4 = r11.A01;
        } else {
            Ae4 = r02.Ae4();
        }
        C37361vX r03 = (C37361vX) r11.A08.get(r10);
        if (r03 == null) {
            A002 = 0;
        } else {
            A002 = C007106r.A00(context, r03.Ae5());
        }
        r9.A01(drawable);
        r9.A0A.setColor(i);
        r9.invalidateSelf();
        if (z) {
            num = r11.A0A;
        } else {
            num = AnonymousClass07B.A01;
        }
        r9.A06 = r11.A09;
        AnonymousClass1RB.A00(r9, r9.A05);
        r9.invalidateSelf();
        int i2 = r11.A03;
        int i3 = r11.A04;
        r9.A03 = i2;
        r9.A04 = i3;
        r9.invalidateSelf();
        r9.A07 = num;
        AnonymousClass1RB.A00(r9, r9.A05);
        r9.invalidateSelf();
        if (Ae4 == -1) {
            Ae4 = r9.A02;
        }
        if (Ae4 >= 0 && r9.A00 != Ae4) {
            r9.A00 = Ae4;
            r9.invalidateSelf();
        }
        if (A002 >= 0 && r9.A01 != A002) {
            r9.A01 = A002;
            r9.invalidateSelf();
        }
    }

    public AnonymousClass1RA(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
