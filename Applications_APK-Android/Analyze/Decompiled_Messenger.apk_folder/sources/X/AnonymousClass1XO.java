package X;

/* renamed from: X.1XO  reason: invalid class name */
public final class AnonymousClass1XO implements AnonymousClass1XP {
    public final /* synthetic */ AnonymousClass1XJ A00;

    public void BUd() {
    }

    public AnonymousClass1XO(AnonymousClass1XJ r1) {
        this.A00 = r1;
    }

    public void onResume() {
        AnonymousClass1XJ r2 = this.A00;
        int i = r2.A00 + 1;
        r2.A00 = i;
        if (i != 1) {
            return;
        }
        if (r2.A05) {
            r2.A07.A08(C14820uB.ON_RESUME);
            r2.A05 = false;
            return;
        }
        AnonymousClass00S.A02(r2.A02, r2.A04);
    }

    public void onStart() {
        AnonymousClass1XJ r2 = this.A00;
        int i = r2.A01 + 1;
        r2.A01 = i;
        if (i == 1 && r2.A06) {
            r2.A07.A08(C14820uB.ON_START);
            r2.A06 = false;
        }
    }
}
