package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.profilo.ipc.TraceContext;

/* renamed from: X.0HV  reason: invalid class name */
public final class AnonymousClass0HV implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new TraceContext(parcel);
    }

    public Object[] newArray(int i) {
        return new TraceContext[i];
    }
}
