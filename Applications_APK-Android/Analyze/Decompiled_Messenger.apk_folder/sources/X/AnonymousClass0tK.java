package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.HashMap;
import java.util.Map;

@UserScoped
/* renamed from: X.0tK  reason: invalid class name */
public final class AnonymousClass0tK {
    private static C05540Zi A01;
    public Map A00 = new HashMap();

    public static final AnonymousClass0tK A00(AnonymousClass1XY r3) {
        AnonymousClass0tK r0;
        synchronized (AnonymousClass0tK.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r3)) {
                    A01.A01();
                    A01.A00 = new AnonymousClass0tK();
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass0tK) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }
}
