package X;

import java.io.DataInputStream;

/* renamed from: X.0Ri  reason: invalid class name and case insensitive filesystem */
public final class C03990Ri {
    public DataInputStream A00;
    private final int A01;
    private final AnonymousClass0B6 A02;
    private final AnonymousClass0CA A03;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01f3, code lost:
        if (r4.A01.A01 == false) goto L_0x01f5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.C02020Cn A00() {
        /*
            r18 = this;
            r1 = r18
            monitor-enter(r1)
            java.io.DataInputStream r2 = r1.A00     // Catch:{ all -> 0x02e4 }
            r0 = 0
            if (r2 == 0) goto L_0x0009
            r0 = 1
        L_0x0009:
            X.AnonymousClass0A1.A01(r0)     // Catch:{ all -> 0x02e4 }
            java.io.DataInputStream r6 = r1.A00     // Catch:{ all -> 0x02e4 }
            int r3 = r6.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r0 = r3 >> 4
            java.util.Map r2 = X.AnonymousClass0CL.A00     // Catch:{ all -> 0x02e4 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x02e4 }
            java.lang.Object r7 = r2.get(r0)     // Catch:{ all -> 0x02e4 }
            X.0CL r7 = (X.AnonymousClass0CL) r7     // Catch:{ all -> 0x02e4 }
            r2 = r3 & 8
            r0 = 8
            r5 = 0
            r4 = 1
            r8 = 0
            if (r2 != r0) goto L_0x002a
            r8 = 1
        L_0x002a:
            r0 = r3 & 6
            int r9 = r0 >> 1
            r3 = r3 & r4
            r10 = 0
            if (r3 == 0) goto L_0x0033
            r10 = 1
        L_0x0033:
            r2 = 1
            r3 = 1
        L_0x0035:
            int r0 = r6.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            r11 = r0 & 127(0x7f, float:1.78E-43)
            int r11 = r11 * r2
            int r11 = r11 + r5
            int r2 = r2 << 7
            int r3 = r3 + r4
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 != 0) goto L_0x006c
            int r3 = r3 + r11
            X.0Ck r6 = new X.0Ck     // Catch:{ all -> 0x02e4 }
            r6.<init>(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x02e4 }
            android.util.Pair r2 = new android.util.Pair     // Catch:{ all -> 0x02e4 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x02e4 }
            r2.<init>(r6, r0)     // Catch:{ all -> 0x02e4 }
            java.lang.Object r0 = r2.first     // Catch:{ all -> 0x02e4 }
            X.0Ck r0 = (X.C01990Ck) r0     // Catch:{ all -> 0x02e4 }
            int r3 = r0.A00     // Catch:{ all -> 0x02e4 }
            X.0Op r6 = new X.0Op     // Catch:{ all -> 0x02e4 }
            r6.<init>(r0, r3)     // Catch:{ all -> 0x02e4 }
            java.io.DataInputStream r8 = r1.A00     // Catch:{ all -> 0x02e4 }
            X.0Ck r3 = r6.A01     // Catch:{ all -> 0x02e4 }
            X.0CL r3 = r3.A03     // Catch:{ all -> 0x02e4 }
            int r3 = r3.ordinal()     // Catch:{ all -> 0x02e4 }
            switch(r3) {
                case 0: goto L_0x0071;
                case 1: goto L_0x00d9;
                case 2: goto L_0x0103;
                case 3: goto L_0x00ec;
                case 4: goto L_0x006b;
                case 5: goto L_0x006b;
                case 6: goto L_0x006b;
                case 7: goto L_0x00ec;
                case 8: goto L_0x00ec;
                case 9: goto L_0x00ec;
                case 10: goto L_0x00ec;
                default: goto L_0x006b;
            }     // Catch:{ all -> 0x02e4 }
        L_0x006b:
            goto L_0x006e
        L_0x006c:
            r5 = r11
            goto L_0x0035
        L_0x006e:
            r9 = 0
            goto L_0x0125
        L_0x0071:
            java.lang.String r4 = r6.A00(r8)     // Catch:{ all -> 0x02e4 }
            java.lang.String r3 = "MQIsdp"
            boolean r3 = r3.equals(r4)     // Catch:{ all -> 0x02e4 }
            if (r3 == 0) goto L_0x00cd
            byte r10 = r8.readByte()     // Catch:{ all -> 0x02e4 }
            int r5 = r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r3 = r6.A00     // Catch:{ all -> 0x02e4 }
            r4 = 2
            int r3 = r3 - r4
            r6.A00 = r3     // Catch:{ all -> 0x02e4 }
            int r7 = r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r17 = r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r3 = r6.A00     // Catch:{ all -> 0x02e4 }
            int r3 = r3 + -2
            r6.A00 = r3     // Catch:{ all -> 0x02e4 }
            int r3 = r7 << 8
            r17 = r17 | r3
            X.0Rv r9 = new X.0Rv     // Catch:{ all -> 0x02e4 }
            r7 = r5 & 128(0x80, float:1.794E-43)
            r3 = 128(0x80, float:1.794E-43)
            r11 = 0
            if (r7 != r3) goto L_0x00a7
            r11 = 1
        L_0x00a7:
            r7 = r5 & 64
            r3 = 64
            r12 = 0
            if (r7 != r3) goto L_0x00af
            r12 = 1
        L_0x00af:
            r7 = r5 & 4
            r3 = 4
            r13 = 0
            if (r7 != r3) goto L_0x00b6
            r13 = 1
        L_0x00b6:
            r7 = r5 & 32
            r3 = 32
            r14 = 0
            if (r7 != r3) goto L_0x00be
            r14 = 1
        L_0x00be:
            r3 = r5 & 24
            int r15 = r3 >> 3
            r5 = r5 & r4
            r16 = 0
            if (r5 != r4) goto L_0x00c9
            r16 = 1
        L_0x00c9:
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ all -> 0x02e4 }
            goto L_0x0125
        L_0x00cd:
            r8.close()     // Catch:{ all -> 0x02e4 }
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x02e4 }
            java.lang.String r0 = "Invalid input - missing header"
            r2.<init>(r0)     // Catch:{ all -> 0x02e4 }
            goto L_0x02e3
        L_0x00d9:
            r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            byte r4 = r8.readByte()     // Catch:{ all -> 0x02e4 }
            int r3 = r6.A00     // Catch:{ all -> 0x02e4 }
            int r3 = r3 + -2
            r6.A00 = r3     // Catch:{ all -> 0x02e4 }
            X.0Rw r9 = new X.0Rw     // Catch:{ all -> 0x02e4 }
            r9.<init>(r4)     // Catch:{ all -> 0x02e4 }
            goto L_0x0125
        L_0x00ec:
            int r5 = r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r4 = r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r3 = r6.A00     // Catch:{ all -> 0x02e4 }
            int r3 = r3 + -2
            r6.A00 = r3     // Catch:{ all -> 0x02e4 }
            int r3 = r5 << 8
            r4 = r4 | r3
            X.0Cl r9 = new X.0Cl     // Catch:{ all -> 0x02e4 }
            r9.<init>(r4)     // Catch:{ all -> 0x02e4 }
            goto L_0x0125
        L_0x0103:
            java.lang.String r7 = r6.A00(r8)     // Catch:{ all -> 0x02e4 }
            X.0Ck r3 = r6.A01     // Catch:{ all -> 0x02e4 }
            int r3 = r3.A02     // Catch:{ all -> 0x02e4 }
            if (r3 <= 0) goto L_0x011f
            int r5 = r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r4 = r8.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r3 = r6.A00     // Catch:{ all -> 0x02e4 }
            int r3 = r3 + -2
            r6.A00 = r3     // Catch:{ all -> 0x02e4 }
            int r3 = r5 << 8
            r4 = r4 | r3
            goto L_0x0120
        L_0x011f:
            r4 = -1
        L_0x0120:
            X.0G8 r9 = new X.0G8     // Catch:{ all -> 0x02e4 }
            r9.<init>(r7, r4)     // Catch:{ all -> 0x02e4 }
        L_0x0125:
            int r5 = r6.A00     // Catch:{ all -> 0x02e4 }
            X.0Os r4 = new X.0Os     // Catch:{ all -> 0x02e4 }
            int r3 = r1.A01     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9, r5, r3)     // Catch:{ all -> 0x02e4 }
            java.io.DataInputStream r5 = r1.A00     // Catch:{ all -> 0x02e4 }
            X.0Ck r3 = r4.A01     // Catch:{ all -> 0x02e4 }
            X.0CL r3 = r3.A03     // Catch:{ all -> 0x02e4 }
            int r3 = r3.ordinal()     // Catch:{ all -> 0x02e4 }
            switch(r3) {
                case 0: goto L_0x013e;
                case 1: goto L_0x01fa;
                case 2: goto L_0x01dd;
                case 3: goto L_0x013b;
                case 4: goto L_0x013b;
                case 5: goto L_0x013b;
                case 6: goto L_0x013b;
                case 7: goto L_0x017d;
                case 8: goto L_0x01a3;
                case 9: goto L_0x01c6;
                default: goto L_0x013b;
            }     // Catch:{ all -> 0x02e4 }
        L_0x013b:
            r10 = 0
            goto L_0x0206
        L_0x013e:
            java.lang.Object r6 = r4.A01     // Catch:{ all -> 0x02e4 }
            X.0Rv r6 = (X.AnonymousClass0Rv) r6     // Catch:{ all -> 0x02e4 }
            java.lang.String r11 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
            boolean r3 = r6.A06     // Catch:{ all -> 0x02e4 }
            r15 = 0
            if (r3 == 0) goto L_0x017a
            java.lang.String r12 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
            java.lang.String r13 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
        L_0x0153:
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            if (r3 <= 0) goto L_0x0178
            boolean r3 = r6.A05     // Catch:{ all -> 0x02e4 }
            if (r3 == 0) goto L_0x0176
            java.lang.String r3 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
            X.0CJ r14 = X.AnonymousClass0CJ.A00(r3)     // Catch:{ all -> 0x02e4 }
        L_0x0163:
            boolean r3 = r6.A04     // Catch:{ all -> 0x02e4 }
            if (r3 == 0) goto L_0x016b
            java.lang.String r15 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
        L_0x016b:
            X.0CK r10 = new X.0CK     // Catch:{ all -> 0x02e4 }
            java.util.List r16 = java.util.Collections.emptyList()     // Catch:{ all -> 0x02e4 }
            r10.<init>(r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x02e4 }
            goto L_0x0206
        L_0x0176:
            r14 = r15
            goto L_0x0163
        L_0x0178:
            r14 = r15
            goto L_0x016b
        L_0x017a:
            r12 = r15
            r13 = r15
            goto L_0x0153
        L_0x017d:
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ all -> 0x02e4 }
            r8.<init>()     // Catch:{ all -> 0x02e4 }
        L_0x0182:
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            if (r3 <= 0) goto L_0x019d
            java.lang.String r7 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
            int r6 = r5.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            int r3 = r3 + -1
            r4.A00 = r3     // Catch:{ all -> 0x02e4 }
            com.facebook.rti.mqtt.protocol.messages.SubscribeTopic r3 = new com.facebook.rti.mqtt.protocol.messages.SubscribeTopic     // Catch:{ all -> 0x02e4 }
            r3.<init>(r7, r6)     // Catch:{ all -> 0x02e4 }
            r8.add(r3)     // Catch:{ all -> 0x02e4 }
            goto L_0x0182
        L_0x019d:
            X.0Rq r10 = new X.0Rq     // Catch:{ all -> 0x02e4 }
            r10.<init>(r8)     // Catch:{ all -> 0x02e4 }
            goto L_0x0206
        L_0x01a3:
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x02e4 }
            r7.<init>()     // Catch:{ all -> 0x02e4 }
        L_0x01a8:
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            if (r3 <= 0) goto L_0x01c0
            int r3 = r5.readUnsignedByte()     // Catch:{ all -> 0x02e4 }
            r6 = r3 & -4
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            int r3 = r3 + -1
            r4.A00 = r3     // Catch:{ all -> 0x02e4 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x02e4 }
            r7.add(r3)     // Catch:{ all -> 0x02e4 }
            goto L_0x01a8
        L_0x01c0:
            X.0Rr r10 = new X.0Rr     // Catch:{ all -> 0x02e4 }
            r10.<init>(r7)     // Catch:{ all -> 0x02e4 }
            goto L_0x0206
        L_0x01c6:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x02e4 }
            r6.<init>()     // Catch:{ all -> 0x02e4 }
        L_0x01cb:
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            if (r3 <= 0) goto L_0x01d7
            java.lang.String r3 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
            r6.add(r3)     // Catch:{ all -> 0x02e4 }
            goto L_0x01cb
        L_0x01d7:
            X.0Rp r10 = new X.0Rp     // Catch:{ all -> 0x02e4 }
            r10.<init>(r6)     // Catch:{ all -> 0x02e4 }
            goto L_0x0206
        L_0x01dd:
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            byte[] r10 = new byte[r3]     // Catch:{ all -> 0x02e4 }
            r5.readFully(r10)     // Catch:{ all -> 0x02e4 }
            r3 = 0
            r4.A00 = r3     // Catch:{ all -> 0x02e4 }
            int r5 = r4.A00     // Catch:{ all -> 0x02e4 }
            r3 = 1
            if (r3 == r5) goto L_0x01f5
            r3 = 2
            if (r3 != r5) goto L_0x0206
            X.0Ck r3 = r4.A01     // Catch:{ all -> 0x02e4 }
            boolean r3 = r3.A01     // Catch:{ all -> 0x02e4 }
            if (r3 != 0) goto L_0x0206
        L_0x01f5:
            byte[] r10 = X.AnonymousClass0Q0.A01(r10)     // Catch:{ all -> 0x02e4 }
            goto L_0x0206
        L_0x01fa:
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            if (r3 <= 0) goto L_0x0224
            java.lang.String r3 = r4.A00(r5)     // Catch:{ all -> 0x02e4 }
        L_0x0202:
            X.0CQ r10 = X.AnonymousClass0CQ.A00(r3)     // Catch:{ all -> 0x02e4 }
        L_0x0206:
            int r3 = r4.A00     // Catch:{ all -> 0x02e4 }
            if (r3 != 0) goto L_0x02bd
            X.0CL r5 = r0.A03     // Catch:{ all -> 0x02e4 }
            int r4 = r5.ordinal()     // Catch:{ all -> 0x02e4 }
            r3 = 0
            switch(r4) {
                case 0: goto L_0x027a;
                case 1: goto L_0x0270;
                case 2: goto L_0x0248;
                case 3: goto L_0x022e;
                case 4: goto L_0x0214;
                case 5: goto L_0x0214;
                case 6: goto L_0x0214;
                case 7: goto L_0x0266;
                case 8: goto L_0x025c;
                case 9: goto L_0x0252;
                case 10: goto L_0x0226;
                case 11: goto L_0x023c;
                case 12: goto L_0x0236;
                case 13: goto L_0x0242;
                default: goto L_0x0214;
            }     // Catch:{ all -> 0x02e4 }
        L_0x0214:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x02e4 }
            java.lang.String r2 = "Unknown message type: "
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x02e4 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r2, r0)     // Catch:{ all -> 0x02e4 }
            r3.<init>(r0)     // Catch:{ all -> 0x02e4 }
            throw r3     // Catch:{ all -> 0x02e4 }
        L_0x0224:
            r3 = 0
            goto L_0x0202
        L_0x0226:
            X.0P1 r4 = new X.0P1     // Catch:{ all -> 0x02e4 }
            X.0Cl r9 = (X.C02000Cl) r9     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x022e:
            X.0Cm r4 = new X.0Cm     // Catch:{ all -> 0x02e4 }
            X.0Cl r9 = (X.C02000Cl) r9     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x0236:
            X.0Cn r4 = new X.0Cn     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r3, r3)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x023c:
            X.0Cn r4 = new X.0Cn     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r3, r3)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x0242:
            X.0Cn r4 = new X.0Cn     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r3, r3)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x0248:
            X.0Cp r4 = new X.0Cp     // Catch:{ all -> 0x02e4 }
            X.0G8 r9 = (X.AnonymousClass0G8) r9     // Catch:{ all -> 0x02e4 }
            byte[] r10 = (byte[]) r10     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9, r10)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x0252:
            X.0Ou r4 = new X.0Ou     // Catch:{ all -> 0x02e4 }
            X.0Cl r9 = (X.C02000Cl) r9     // Catch:{ all -> 0x02e4 }
            X.0Rp r10 = (X.AnonymousClass0Rp) r10     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9, r10)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x025c:
            X.0P3 r4 = new X.0P3     // Catch:{ all -> 0x02e4 }
            X.0Cl r9 = (X.C02000Cl) r9     // Catch:{ all -> 0x02e4 }
            X.0Rr r10 = (X.C04060Rr) r10     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9, r10)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x0266:
            X.0P2 r4 = new X.0P2     // Catch:{ all -> 0x02e4 }
            X.0Cl r9 = (X.C02000Cl) r9     // Catch:{ all -> 0x02e4 }
            X.0Rq r10 = (X.C04050Rq) r10     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9, r10)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x0270:
            X.0P9 r4 = new X.0P9     // Catch:{ all -> 0x02e4 }
            X.0Rw r9 = (X.C04100Rw) r9     // Catch:{ all -> 0x02e4 }
            X.0CQ r10 = (X.AnonymousClass0CQ) r10     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9, r10)     // Catch:{ all -> 0x02e4 }
            goto L_0x0283
        L_0x027a:
            X.0P6 r4 = new X.0P6     // Catch:{ all -> 0x02e4 }
            X.0Rv r9 = (X.AnonymousClass0Rv) r9     // Catch:{ all -> 0x02e4 }
            X.0CK r10 = (X.AnonymousClass0CK) r10     // Catch:{ all -> 0x02e4 }
            r4.<init>(r0, r9, r10)     // Catch:{ all -> 0x02e4 }
        L_0x0283:
            X.0Ck r0 = r4.A00     // Catch:{ all -> 0x02e4 }
            X.0CL r0 = r0.A03     // Catch:{ all -> 0x02e4 }
            java.lang.String r6 = r0.name()     // Catch:{ all -> 0x02e4 }
            java.lang.String r5 = ""
            boolean r0 = r4 instanceof X.C02040Cp     // Catch:{ all -> 0x02e4 }
            if (r0 == 0) goto L_0x02a1
            r0 = r4
            X.0Cp r0 = (X.C02040Cp) r0     // Catch:{ all -> 0x02e4 }
            X.0G8 r0 = r0.A02()     // Catch:{ all -> 0x02e4 }
            java.lang.String r5 = r0.A01     // Catch:{ all -> 0x02e4 }
            java.lang.String r0 = X.AnonymousClass0AI.A01(r5)     // Catch:{ all -> 0x02e4 }
            if (r0 == 0) goto L_0x02a1
            r5 = r0
        L_0x02a1:
            X.0CA r3 = r1.A03     // Catch:{ all -> 0x02e4 }
            java.lang.Object r0 = r2.second     // Catch:{ all -> 0x02e4 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x02e4 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x02e4 }
            r3.A00(r0)     // Catch:{ all -> 0x02e4 }
            X.0CA r3 = r1.A03     // Catch:{ all -> 0x02e4 }
            java.lang.Object r0 = r2.second     // Catch:{ all -> 0x02e4 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x02e4 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x02e4 }
            r3.A03(r6, r5, r0)     // Catch:{ all -> 0x02e4 }
            monitor-exit(r1)
            return r4
        L_0x02bd:
            X.0B6 r5 = r1.A02     // Catch:{ all -> 0x02e4 }
            X.0CL r2 = r0.A03     // Catch:{ all -> 0x02e4 }
            java.lang.String r4 = r2.name()     // Catch:{ all -> 0x02e4 }
            int r0 = r0.A00     // Catch:{ all -> 0x02e4 }
            java.lang.String r3 = "message_type"
            java.lang.String r2 = "message_size"
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x02e4 }
            java.lang.String[] r0 = new java.lang.String[]{r3, r4, r2, r0}     // Catch:{ all -> 0x02e4 }
            java.util.Map r2 = X.C01740Bl.A00(r0)     // Catch:{ all -> 0x02e4 }
            java.lang.String r0 = "mqtt_invalid_message"
            r5.A07(r0, r2)     // Catch:{ all -> 0x02e4 }
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x02e4 }
            java.lang.String r0 = "Unexpected bytes remaining in payload"
            r2.<init>(r0)     // Catch:{ all -> 0x02e4 }
        L_0x02e3:
            throw r2     // Catch:{ all -> 0x02e4 }
        L_0x02e4:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03990Ri.A00():X.0Cn");
    }

    public C03990Ri(AnonymousClass0B6 r1, int i, AnonymousClass0CA r3) {
        this.A02 = r1;
        this.A01 = i;
        this.A03 = r3;
    }
}
