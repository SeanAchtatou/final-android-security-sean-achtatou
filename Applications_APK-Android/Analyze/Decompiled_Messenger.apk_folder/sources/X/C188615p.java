package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.montage.model.MontageThreadInfo;

/* renamed from: X.15p  reason: invalid class name and case insensitive filesystem */
public final class C188615p implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new MontageThreadInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new MontageThreadInfo[i];
    }
}
