package X;

import android.net.Uri;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.0eb  reason: invalid class name and case insensitive filesystem */
public final class C08050eb extends C08190ep implements C08210er {
    private static volatile C08050eb A04;
    public AnonymousClass0UN A00;
    public int[] A01;
    public final Map A02 = new C08140ek(this, 75);
    private final AnonymousClass0Ti A03;

    public String getName() {
        return "GraphServicesQPLLogsCollector";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    public void prepareDataForWriting() {
    }

    public boolean shouldSendAsync() {
        return false;
    }

    public static final C08050eb A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C08050eb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C08050eb(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static C25871aX A01(C08050eb r4, int i) {
        C25871aX r1;
        synchronized (r4.A02) {
            Map map = r4.A02;
            Integer valueOf = Integer.valueOf(i);
            r1 = (C25871aX) map.get(valueOf);
            if (r1 == null) {
                r1 = new C25871aX();
                r4.A02.put(valueOf, r1);
            }
        }
        return r1;
    }

    public AnonymousClass0Ti AsW() {
        return this.A03;
    }

    public Map getExtraFileFromWorkerThread(File file) {
        boolean z = false;
        if (this.A01 != null) {
            z = true;
        }
        if (z) {
            try {
                HashMap hashMap = new HashMap();
                File file2 = new File(file, "graphservices_qpl_logs.txt");
                JSONObject jSONObject = new JSONObject();
                synchronized (this.A02) {
                    for (Integer num : this.A02.keySet()) {
                        C25871aX r11 = (C25871aX) this.A02.get(num);
                        if (r11 != null) {
                            jSONObject.put(String.valueOf(num), new JSONObject().put("Query QPL Logs", ((AnonymousClass0jE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Axm, this.A00)).writeValueAsString(r11)));
                        }
                    }
                    this.A02.clear();
                }
                AnonymousClass0jE r3 = (AnonymousClass0jE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Axm, this.A00);
                AnonymousClass0jJ._configAndWriteValue(r3, r3._jsonFactory.createGenerator(file2, C48392aM.UTF8), jSONObject.toString());
                hashMap.put("graphservices_qpl_logs.txt", Uri.fromFile(file2).toString());
                return hashMap;
            } catch (IOException | JSONException e) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00)).softReport("GraphServicesQPLLogsCollector", e);
            }
        }
        return Collections.emptyMap();
    }

    private C08050eb(AnonymousClass1XY r4) {
        AnonymousClass0UN r1 = new AnonymousClass0UN(3, r4);
        this.A00 = r1;
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, r1)).AbO(644, false)) {
            int[] iArr = {3211329};
            this.A01 = iArr;
            this.A03 = AnonymousClass0Ti.A00(iArr);
            return;
        }
        this.A03 = AnonymousClass0Ti.A04;
    }
}
