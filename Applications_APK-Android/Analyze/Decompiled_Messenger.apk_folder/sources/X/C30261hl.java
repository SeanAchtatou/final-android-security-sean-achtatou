package X;

import android.net.http.AndroidHttpClient;
import com.facebook.common.dextricks.DexStore;
import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

@InjectorModule
/* renamed from: X.1hl  reason: invalid class name and case insensitive filesystem */
public final class C30261hl extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C06390bQ A01;

    public static final Integer A06() {
        return 3;
    }

    public static final C06390bQ A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = C06390bQ.A00();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C30367Eut A04() {
        return null;
    }

    public static final C04310Tq A07(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.B9i, r1);
    }

    public static final HttpClient A08(AnonymousClass1XY r1) {
        return AndroidHttpClient.newInstance((String) AnonymousClass0VG.A00(AnonymousClass1Y3.A43, r1).get());
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:20|(3:22|23|24)|25|26|27|28) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x00ad */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final org.apache.http.conn.scheme.SocketFactory A09(X.AnonymousClass1XY r6) {
        /*
            int r0 = X.AnonymousClass1Y3.A43
            X.0VG r3 = X.AnonymousClass0VG.A00(r0, r6)
            X.1jx r2 = X.C31601jx.A00(r6)
            android.content.Context r1 = X.AnonymousClass1YA.A00(r6)
            int r0 = X.AnonymousClass1Y3.AcD
            X.AnonymousClass0VG.A00(r0, r6)
            X.2Ih r5 = X.C44462Ih.A00(r6)
            r1.getApplicationContext()
            boolean r0 = r2.A03
            if (r0 != 0) goto L_0x0035
            r4 = 0
            java.lang.String r0 = java.security.KeyStore.getDefaultType()     // Catch:{ Exception -> 0x00df }
            java.security.KeyStore r0 = java.security.KeyStore.getInstance(r0)     // Catch:{ Exception -> 0x00df }
            r0.load(r4, r4)     // Catch:{ Exception -> 0x00df }
            X.8W8 r1 = new X.8W8     // Catch:{ Exception -> 0x00df }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00df }
            org.apache.http.conn.ssl.X509HostnameVerifier r0 = X.AnonymousClass8W8.A02     // Catch:{ Exception -> 0x00df }
            r1.setHostnameVerifier(r0)     // Catch:{ Exception -> 0x00df }
            return r1
        L_0x0035:
            java.lang.Object r0 = r3.get()
            java.lang.String r0 = (java.lang.String) r0
            android.net.http.AndroidHttpClient r2 = android.net.http.AndroidHttpClient.newInstance(r0)
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            org.apache.http.conn.scheme.SchemeRegistry r1 = r0.getSchemeRegistry()
            java.lang.String r0 = "https"
            org.apache.http.conn.scheme.Scheme r0 = r1.get(r0)
            org.apache.http.conn.scheme.SocketFactory r4 = r0.getSocketFactory()
            r2.close()
            int r1 = r5.A00
            r0 = 8
            if (r1 <= r0) goto L_0x00d3
            r0 = 16
            if (r1 > r0) goto L_0x00c2
            java.util.Set r0 = r5.A09     // Catch:{ 3cD -> 0x00bb }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ 3cD -> 0x00bb }
        L_0x0064:
            boolean r0 = r3.hasNext()     // Catch:{ 3cD -> 0x00bb }
            if (r0 == 0) goto L_0x008a
            java.lang.Object r1 = r3.next()     // Catch:{ 3cD -> 0x00bb }
            X.8Vw r1 = (X.C180248Vw) r1     // Catch:{ 3cD -> 0x00bb }
            java.lang.Class r0 = r1.getClass()     // Catch:{ 3cD -> 0x00bb }
            java.lang.String r2 = r0.getName()     // Catch:{ 3cD -> 0x00bb }
            boolean r0 = r1.ASA()     // Catch:{ 3cD -> 0x00bb }
            if (r0 != 0) goto L_0x0064
            java.lang.Class r1 = X.C44462Ih.A0B     // Catch:{ 3cD -> 0x00bb }
            java.lang.String r0 = "check fail "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r2)     // Catch:{ 3cD -> 0x00bb }
            X.C010708t.A06(r1, r0)     // Catch:{ 3cD -> 0x00bb }
            goto L_0x008c
        L_0x008a:
            r0 = 1
            goto L_0x008d
        L_0x008c:
            r0 = 0
        L_0x008d:
            if (r0 == 0) goto L_0x00c2
            X.8W5 r6 = new X.8W5     // Catch:{ 3cD -> 0x00bb }
            com.facebook.ssl.openssl.heartbleed.HeartbleedMitigation r0 = r5.A04     // Catch:{ 3cD -> 0x00bb }
            boolean r0 = r0.A03()     // Catch:{ 3cD -> 0x00bb }
            if (r0 == 0) goto L_0x00ad
            java.lang.String r0 = "TLS"
            javax.net.ssl.SSLContext r1 = javax.net.ssl.SSLContext.getInstance(r0)     // Catch:{ all -> 0x00ad }
            r0 = 0
            r1.init(r0, r0, r0)     // Catch:{ all -> 0x00ad }
            com.facebook.ssl.openssl.heartbleed.HeartbleedMitigation r0 = r5.A04     // Catch:{ all -> 0x00ad }
            r0.A02(r1)     // Catch:{ all -> 0x00ad }
            javax.net.ssl.SSLSocketFactory r3 = r1.getSocketFactory()     // Catch:{ all -> 0x00ad }
            goto L_0x00b1
        L_0x00ad:
            javax.net.ssl.SSLSocketFactory r3 = javax.net.ssl.HttpsURLConnection.getDefaultSSLSocketFactory()     // Catch:{ 3cD -> 0x00bb }
        L_0x00b1:
            X.4W0 r2 = r5.A08     // Catch:{ 3cD -> 0x00bb }
            int r1 = r5.A01     // Catch:{ 3cD -> 0x00bb }
            X.09P r0 = r5.A02     // Catch:{ 3cD -> 0x00bb }
            r6.<init>(r3, r2, r1, r0)     // Catch:{ 3cD -> 0x00bb }
            return r6
        L_0x00bb:
            java.lang.Class r1 = X.C44462Ih.A0B
            java.lang.String r0 = "exception occured while trying to create the socket factory"
            X.C010708t.A06(r1, r0)
        L_0x00c2:
            com.facebook.ssl.openssl.heartbleed.HeartbleedMitigation r0 = r5.A04
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x00d3
            X.09P r2 = r5.A02
            java.lang.String r1 = "heartbeat_not_applied"
            java.lang.String r0 = "Did not apply heartbleed fix"
            r2.CGS(r1, r0)
        L_0x00d3:
            r1 = r4
            boolean r0 = r4 instanceof org.apache.http.conn.ssl.SSLSocketFactory
            if (r0 == 0) goto L_0x00df
            org.apache.http.conn.ssl.SSLSocketFactory r1 = (org.apache.http.conn.ssl.SSLSocketFactory) r1
            org.apache.http.conn.ssl.X509HostnameVerifier r0 = r5.A0A
            r1.setHostnameVerifier(r0)
        L_0x00df:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30261hl.A09(X.1XY):org.apache.http.conn.scheme.SocketFactory");
    }

    public static final C30380EvE A01(AnonymousClass1XY r0) {
        AnonymousClass0WA.A00(r0);
        return new C30380EvE();
    }

    public static final C10740km A02(AnonymousClass1XY r0) {
        return C30271hm.A00(r0);
    }

    public static final C64393Bj A03(AnonymousClass1XY r0) {
        return C64393Bj.A00(r0);
    }

    public static final Boolean A05(AnonymousClass1XY r2) {
        return Boolean.valueOf(FbSharedPreferencesModule.A00(r2).Aep(C25951af.A00, false));
    }

    public static final HttpParams A0A(AnonymousClass1XY r5) {
        AnonymousClass0WA.A00(r5);
        AnonymousClass0VG A002 = AnonymousClass0VG.A00(AnonymousClass1Y3.A43, r5);
        C31601jx A003 = C31601jx.A00(r5);
        AnonymousClass0UX.A0a(r5);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 60000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 60000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED);
        HttpClientParams.setRedirecting(basicHttpParams, false);
        HttpProtocolParams.setUserAgent(basicHttpParams, (String) A002.get());
        HttpHost httpHost = A003.A01;
        if (httpHost != null) {
            basicHttpParams.setParameter("http.route.default-proxy", httpHost);
        }
        return basicHttpParams;
    }
}
