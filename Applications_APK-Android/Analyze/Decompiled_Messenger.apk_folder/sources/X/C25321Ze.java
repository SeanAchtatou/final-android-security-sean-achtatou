package X;

import java.util.EnumSet;

/* renamed from: X.1Ze  reason: invalid class name and case insensitive filesystem */
public final class C25321Ze implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.scheduler.JobsQueue$JobsQueueRunnable";
    public final AnonymousClass1ZX A00;
    public final /* synthetic */ AnonymousClass1YW A01;

    public C25321Ze(AnonymousClass1YW r1, AnonymousClass1ZX r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004e, code lost:
        if (r1 != null) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0053 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.C25321Ze r7) {
        /*
            X.1ZX r2 = r7.A00
            int r3 = r2.A02
            int r1 = X.AnonymousClass1Y3.BRs     // Catch:{ all -> 0x0103 }
            X.1YW r0 = r7.A01     // Catch:{ all -> 0x0103 }
            X.0UN r0 = r0.A00     // Catch:{ all -> 0x0103 }
            r6 = 2
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x0103 }
            X.1ZZ r5 = (X.AnonymousClass1ZZ) r5     // Catch:{ all -> 0x0103 }
            r0 = 0
            X.1Za r4 = X.AnonymousClass1ZZ.A00(r5, r2, r0)     // Catch:{ all -> 0x0103 }
            if (r4 == 0) goto L_0x0029
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x0103 }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x0103 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0103 }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x0103 }
            long r0 = r0.now()     // Catch:{ all -> 0x0103 }
            r4.A02 = r0     // Catch:{ all -> 0x0103 }
        L_0x0029:
            X.1ZX r0 = r7.A00     // Catch:{ all -> 0x0103 }
            X.1XV r5 = r0.A00     // Catch:{ all -> 0x0103 }
            boolean r0 = X.C25231Yv.A01()     // Catch:{ all -> 0x0103 }
            if (r0 == 0) goto L_0x0054
            if (r5 == 0) goto L_0x0054
            int r4 = r5.A03     // Catch:{ all -> 0x0103 }
            X.1d9 r2 = X.C27401d8.A00     // Catch:{ all -> 0x0103 }
            java.lang.String r1 = r5.A06     // Catch:{ all -> 0x0103 }
            r0 = 1
            X.1XV r1 = r2.A02(r5, r1, r0, r4)     // Catch:{ all -> 0x0103 }
            X.1ZX r0 = r7.A00     // Catch:{ all -> 0x004b }
            r0.run()     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ all -> 0x0103 }
            goto L_0x0059
        L_0x004b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004d }
        L_0x004d:
            r0 = move-exception
            if (r1 == 0) goto L_0x0053
            r1.close()     // Catch:{ all -> 0x0053 }
        L_0x0053:
            throw r0     // Catch:{ all -> 0x0103 }
        L_0x0054:
            X.1ZX r0 = r7.A00     // Catch:{ all -> 0x0103 }
            r0.run()     // Catch:{ all -> 0x0103 }
        L_0x0059:
            int r1 = X.AnonymousClass1Y3.BRs     // Catch:{ all -> 0x0103 }
            X.1YW r0 = r7.A01     // Catch:{ all -> 0x0103 }
            X.0UN r0 = r0.A00     // Catch:{ all -> 0x0103 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x0103 }
            X.1ZZ r5 = (X.AnonymousClass1ZZ) r5     // Catch:{ all -> 0x0103 }
            X.1ZX r1 = r7.A00     // Catch:{ all -> 0x0103 }
            r0 = 0
            X.1Za r4 = X.AnonymousClass1ZZ.A00(r5, r1, r0)     // Catch:{ all -> 0x0103 }
            if (r4 == 0) goto L_0x007f
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x0103 }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x0103 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0103 }
            X.069 r0 = (X.AnonymousClass069) r0     // Catch:{ all -> 0x0103 }
            long r0 = r0.now()     // Catch:{ all -> 0x0103 }
            r4.A00 = r0     // Catch:{ all -> 0x0103 }
        L_0x007f:
            int r1 = X.AnonymousClass1Y3.BRs     // Catch:{ all -> 0x0103 }
            X.1YW r0 = r7.A01     // Catch:{ all -> 0x0103 }
            X.0UN r0 = r0.A00     // Catch:{ all -> 0x0103 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x0103 }
            X.1ZZ r0 = (X.AnonymousClass1ZZ) r0     // Catch:{ all -> 0x0103 }
            X.1ZX r6 = r7.A00     // Catch:{ all -> 0x0103 }
            r2 = 0
            X.1Za r5 = X.AnonymousClass1ZZ.A00(r0, r6, r2)     // Catch:{ all -> 0x0103 }
            if (r5 == 0) goto L_0x00f9
            int r1 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x0103 }
            X.0UN r0 = r0.A00     // Catch:{ all -> 0x0103 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0103 }
            X.1ZE r1 = (X.AnonymousClass1ZE) r1     // Catch:{ all -> 0x0103 }
            java.lang.String r0 = "android_job_orchestrator"
            X.0bW r1 = r1.A01(r0)     // Catch:{ all -> 0x0103 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r4 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x0103 }
            r0 = 11
            r4.<init>(r1, r0)     // Catch:{ all -> 0x0103 }
            boolean r0 = r4.A0G()     // Catch:{ all -> 0x0103 }
            if (r0 == 0) goto L_0x00f9
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x0103 }
            r2.<init>()     // Catch:{ all -> 0x0103 }
            java.lang.String r1 = r6.A01     // Catch:{ all -> 0x0103 }
            java.lang.String r0 = "description"
            r2.put(r0, r1)     // Catch:{ all -> 0x0103 }
            java.util.EnumSet r0 = r6.A01     // Catch:{ all -> 0x0103 }
            java.lang.String r1 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0103 }
            java.lang.String r0 = "hints"
            r2.put(r0, r1)     // Catch:{ all -> 0x0103 }
            long r0 = r5.A01     // Catch:{ all -> 0x0103 }
            java.lang.String r1 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0103 }
            java.lang.String r0 = "scheduled_time"
            r2.put(r0, r1)     // Catch:{ all -> 0x0103 }
            long r0 = r5.A02     // Catch:{ all -> 0x0103 }
            java.lang.String r1 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0103 }
            java.lang.String r0 = "started_time"
            r2.put(r0, r1)     // Catch:{ all -> 0x0103 }
            long r0 = r5.A00     // Catch:{ all -> 0x0103 }
            java.lang.String r1 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0103 }
            r0 = 141(0x8d, float:1.98E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ all -> 0x0103 }
            r2.put(r0, r1)     // Catch:{ all -> 0x0103 }
            java.lang.String r0 = "job_info"
            r4.A0F(r0, r2)     // Catch:{ all -> 0x0103 }
            r4.A06()     // Catch:{ all -> 0x0103 }
            r0 = 3
            X.C010708t.A0X(r0)     // Catch:{ all -> 0x0103 }
        L_0x00f9:
            X.1YW r1 = r7.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            r1.A03(r0)
            return
        L_0x0103:
            r2 = move-exception
            X.1YW r1 = r7.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            r1.A03(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25321Ze.A00(X.1Ze):void");
    }

    public void run() {
        EnumSet enumSet;
        AnonymousClass1ZX r0 = this.A00;
        boolean z = false;
        if (!(r0 == null || (enumSet = r0.A01) == null || !enumSet.contains(AnonymousClass1ZW.A03))) {
            z = true;
        }
        if (z) {
            AnonymousClass07A.A04((C06480bZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BER, ((C25311Zd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ARL, this.A01.A00)).A00), new C83963yX(this), 1934157895);
        } else {
            A00(this);
        }
    }
}
