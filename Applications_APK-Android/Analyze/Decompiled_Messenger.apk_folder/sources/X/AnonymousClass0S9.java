package X;

import com.facebook.profilo.ipc.TraceConfigData;

/* renamed from: X.0S9  reason: invalid class name */
public final class AnonymousClass0S9 implements AnonymousClass05d {
    private final C004405h A00 = new AnonymousClass0SA();
    private final AnonymousClass0S7 A01;
    private final TraceConfigData A02;

    public long Ai2() {
        return this.A02.A02;
    }

    public AnonymousClass0S9(TraceConfigData traceConfigData) {
        this.A02 = traceConfigData;
        this.A01 = new AnonymousClass0S7(traceConfigData);
    }

    public AnonymousClass05i AiX() {
        return this.A01;
    }

    public C004405h B5B() {
        return this.A00;
    }
}
