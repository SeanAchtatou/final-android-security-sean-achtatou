package X;

import java.util.concurrent.ThreadFactory;

/* renamed from: X.15U  reason: invalid class name */
public final /* synthetic */ class AnonymousClass15U implements ThreadFactory {
    public static final ThreadFactory A00 = new AnonymousClass15U();

    private AnonymousClass15U() {
    }

    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, "firebase-iid-executor");
    }
}
