package X;

import android.os.Handler;
import android.telephony.CellInfo;
import com.facebook.tigon.nativeservice.common.NativePlatformContextHolder;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.125  reason: invalid class name */
public final class AnonymousClass125 {
    public static final ImmutableSet A0F = ImmutableSet.A05("gps", "network");
    private static volatile AnonymousClass125 A0G;
    public long A00 = Long.MIN_VALUE;
    public AnonymousClass0UN A01;
    public AtomicReference A02;
    public final ArrayList A03;
    public final Set A04;
    public volatile String A05;
    public volatile String A06;
    public volatile String A07;
    private volatile int A08;
    private volatile String A09;
    private volatile String A0A;
    private volatile String A0B;
    private volatile String A0C;
    private volatile String A0D;
    private volatile boolean A0E;

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0068 A[SYNTHETIC, Splitter:B:27:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0083 A[Catch:{ SecurityException -> 0x0096 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.AnonymousClass125 r5) {
        /*
            monitor-enter(r5)
            java.util.concurrent.atomic.AtomicReference r1 = r5.A02     // Catch:{ all -> 0x00c9 }
            r0 = 0
            r1.set(r0)     // Catch:{ all -> 0x00c9 }
            java.util.ArrayList r0 = r5.A03     // Catch:{ all -> 0x00c9 }
            r0.clear()     // Catch:{ all -> 0x00c9 }
            r2 = 0
            int r1 = X.AnonymousClass1Y3.BCt     // Catch:{ RuntimeException -> 0x003a }
            X.0UN r0 = r5.A01     // Catch:{ RuntimeException -> 0x003a }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ RuntimeException -> 0x003a }
            android.content.Context r1 = (android.content.Context) r1     // Catch:{ RuntimeException -> 0x003a }
            r0 = 10
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ RuntimeException -> 0x003a }
            int r0 = r1.checkCallingOrSelfPermission(r0)     // Catch:{ RuntimeException -> 0x003a }
            if (r0 == 0) goto L_0x0039
            int r1 = X.AnonymousClass1Y3.BCt     // Catch:{ RuntimeException -> 0x003a }
            X.0UN r0 = r5.A01     // Catch:{ RuntimeException -> 0x003a }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ RuntimeException -> 0x003a }
            android.content.Context r1 = (android.content.Context) r1     // Catch:{ RuntimeException -> 0x003a }
            r0 = 11
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ RuntimeException -> 0x003a }
            int r0 = r1.checkCallingOrSelfPermission(r0)     // Catch:{ RuntimeException -> 0x003a }
            if (r0 != 0) goto L_0x003a
        L_0x0039:
            r2 = 1
        L_0x003a:
            if (r2 == 0) goto L_0x0062
            com.google.common.collect.ImmutableSet r0 = X.AnonymousClass125.A0F     // Catch:{ SecurityException -> 0x0096 }
            X.1Xv r4 = r0.iterator()     // Catch:{ SecurityException -> 0x0096 }
        L_0x0042:
            boolean r0 = r4.hasNext()     // Catch:{ SecurityException -> 0x0096 }
            if (r0 == 0) goto L_0x0062
            java.lang.Object r3 = r4.next()     // Catch:{ SecurityException -> 0x0096 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ SecurityException -> 0x0096 }
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BCb     // Catch:{ SecurityException -> 0x0042, RuntimeException -> 0x0060 }
            X.0UN r0 = r5.A01     // Catch:{ SecurityException -> 0x0042, RuntimeException -> 0x0060 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ SecurityException -> 0x0042, RuntimeException -> 0x0060 }
            android.location.LocationManager r0 = (android.location.LocationManager) r0     // Catch:{ SecurityException -> 0x0042, RuntimeException -> 0x0060 }
            boolean r0 = r0.isProviderEnabled(r3)     // Catch:{ SecurityException -> 0x0042, RuntimeException -> 0x0060 }
            if (r0 == 0) goto L_0x0042
            goto L_0x0064
        L_0x0060:
            r0 = 0
            goto L_0x0065
        L_0x0062:
            r0 = 0
            goto L_0x0065
        L_0x0064:
            r0 = 1
        L_0x0065:
            r3 = 2
            if (r0 == 0) goto L_0x007d
            java.util.concurrent.atomic.AtomicReference r2 = r5.A02     // Catch:{ SecurityException -> 0x0096 }
            int r1 = X.AnonymousClass1Y3.ADo     // Catch:{ SecurityException -> 0x0096 }
            X.0UN r0 = r5.A01     // Catch:{ SecurityException -> 0x0096 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ SecurityException -> 0x0096 }
            X.1hB r1 = (X.C29911hB) r1     // Catch:{ SecurityException -> 0x0096 }
            java.lang.String r0 = "CarrierMonitor"
            android.telephony.CellLocation r0 = r1.A02(r0)     // Catch:{ SecurityException -> 0x0096 }
            r2.set(r0)     // Catch:{ SecurityException -> 0x0096 }
        L_0x007d:
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ SecurityException -> 0x0096 }
            r0 = 18
            if (r1 < r0) goto L_0x0096
            java.util.ArrayList r2 = r5.A03     // Catch:{ SecurityException -> 0x0096 }
            int r1 = X.AnonymousClass1Y3.ADo     // Catch:{ SecurityException -> 0x0096 }
            X.0UN r0 = r5.A01     // Catch:{ SecurityException -> 0x0096 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ SecurityException -> 0x0096 }
            X.1hB r0 = (X.C29911hB) r0     // Catch:{ SecurityException -> 0x0096 }
            java.util.List r0 = A01(r0)     // Catch:{ SecurityException -> 0x0096 }
            r2.addAll(r0)     // Catch:{ SecurityException -> 0x0096 }
        L_0x0096:
            monitor-exit(r5)
            java.util.concurrent.atomic.AtomicReference r0 = r5.A02
            java.lang.Object r1 = r0.get()
            android.telephony.CellLocation r1 = (android.telephony.CellLocation) r1
            if (r1 == 0) goto L_0x00bb
            boolean r0 = r1 instanceof android.telephony.cdma.CdmaCellLocation
            if (r0 == 0) goto L_0x00bc
            android.telephony.cdma.CdmaCellLocation r1 = (android.telephony.cdma.CdmaCellLocation) r1
            int r0 = r1.getSystemId()
            java.lang.String r0 = java.lang.Integer.toString(r0)
            java.lang.String r0 = com.google.common.base.Strings.nullToEmpty(r0)
            r5.A06 = r0
            r1.getNetworkId()
            r1.getBaseStationId()
        L_0x00bb:
            return
        L_0x00bc:
            boolean r0 = r1 instanceof android.telephony.gsm.GsmCellLocation
            if (r0 == 0) goto L_0x00bb
            android.telephony.gsm.GsmCellLocation r1 = (android.telephony.gsm.GsmCellLocation) r1
            r1.getLac()
            r1.getCid()
            return
        L_0x00c9:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass125.A03(X.125):void");
    }

    public static synchronized void A04(AnonymousClass125 r3) {
        synchronized (r3) {
            int i = AnonymousClass1Y3.ADo;
            r3.A05 = Strings.nullToEmpty(((C29911hB) AnonymousClass1XX.A02(2, i, r3.A01)).A00.getNetworkOperator());
            r3.A07 = Strings.nullToEmpty(((C29911hB) AnonymousClass1XX.A02(2, i, r3.A01)).A00.getSimOperator());
            r3.A06 = BuildConfig.FLAVOR;
            if (((C25051Yd) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AOJ, r3.A01)).Aem(283673999969684L)) {
                AnonymousClass00S.A04((Handler) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Abm, r3.A01), new C30130EqA(r3), 1491036963);
            } else {
                A03(r3);
            }
        }
    }

    public static final AnonymousClass125 A00(AnonymousClass1XY r4) {
        if (A0G == null) {
            synchronized (AnonymousClass125.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0G, r4);
                if (A002 != null) {
                    try {
                        A0G = new AnonymousClass125(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0G;
    }

    public static List A01(C29911hB r4) {
        ArrayList arrayList = new ArrayList();
        List<CellInfo> A032 = r4.A03("CarrierMonitor");
        if (A032 != null) {
            for (CellInfo cellInfo : A032) {
                if (cellInfo.isRegistered()) {
                    arrayList.add(cellInfo);
                }
            }
        }
        return arrayList;
    }

    public static void A02(AnonymousClass125 r3) {
        if (!r3.A0E) {
            synchronized (r3) {
                if (!r3.A0E) {
                    A04(r3);
                    AnonymousClass00S.A04((Handler) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Abm, r3.A01), new C32051l2(r3), 422406808);
                    r3.A0E = true;
                }
            }
        }
    }

    public static void A05(AnonymousClass125 r3, int i) {
        ((C29911hB) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ADo, r3.A01)).A04(new C36431t4(r3), i);
    }

    public void A06() {
        synchronized (this.A04) {
            for (NativePlatformContextHolder onCellLocationChanged : this.A04) {
                onCellLocationChanged.onCellLocationChanged();
            }
        }
    }

    private AnonymousClass125(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(6, r3);
        this.A0E = false;
        Collections.synchronizedMap(new AnonymousClass04a(1));
        this.A04 = new CopyOnWriteArraySet();
        this.A03 = new ArrayList();
        this.A02 = new AtomicReference();
    }
}
