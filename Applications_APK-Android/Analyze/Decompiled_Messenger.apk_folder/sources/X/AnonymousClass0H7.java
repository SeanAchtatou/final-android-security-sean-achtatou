package X;

import com.facebook.rti.orca.UpdateQeBroadcastReceiver;
import com.facebook.rti.push.service.idsharing.FbnsSharingStateReceiver;

/* renamed from: X.0H7  reason: invalid class name */
public final class AnonymousClass0H7 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.orca.FbnsLiteInitializer$3";
    public final /* synthetic */ AnonymousClass0GZ A00;
    public final /* synthetic */ boolean A01;

    public AnonymousClass0H7(AnonymousClass0GZ r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    public void run() {
        boolean z = this.A01;
        if (z) {
            AnonymousClass0H9.A00(this.A00.A03, FbnsSharingStateReceiver.class, z);
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
        }
        AnonymousClass0H9.A00(this.A00.A03, UpdateQeBroadcastReceiver.class, this.A01);
        Thread.currentThread().isInterrupted();
    }
}
