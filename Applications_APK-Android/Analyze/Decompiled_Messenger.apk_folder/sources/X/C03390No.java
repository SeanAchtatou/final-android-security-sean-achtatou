package X;

import android.text.SpannableStringBuilder;

/* renamed from: X.0No  reason: invalid class name and case insensitive filesystem */
public final class C03390No extends SpannableStringBuilder {
    public CharSequence A00;

    public void A00(CharSequence charSequence) {
        if (length() > 0) {
            append(this.A00);
        }
        length();
        append(charSequence);
    }

    public C03390No() {
        this("  •  ");
    }

    private C03390No(CharSequence charSequence) {
        this.A00 = charSequence;
    }
}
