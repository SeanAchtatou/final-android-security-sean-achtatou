package X;

import com.facebook.tigon.TigonErrorReporter;

/* renamed from: X.0ro  reason: invalid class name and case insensitive filesystem */
public final class C13650ro implements TigonErrorReporter {
    private final AnonymousClass09P A00;

    public C13650ro(AnonymousClass09P r1) {
        this.A00 = r1;
    }

    public void softReport(String str, String str2, Throwable th) {
        this.A00.CGU(str, str2, th, 1);
    }

    public void softReport(String str, Throwable th) {
        this.A00.CGV(str, th, 1);
    }
}
