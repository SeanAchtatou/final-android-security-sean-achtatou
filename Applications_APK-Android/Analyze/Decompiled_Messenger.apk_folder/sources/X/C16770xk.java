package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0xk  reason: invalid class name and case insensitive filesystem */
public final class C16770xk extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile Boolean A01;

    public static final Boolean A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        boolean z = false;
                        if (AnonymousClass0UU.A05(r5.getApplicationInjector()) == C001500z.A07) {
                            z = true;
                        }
                        A01 = Boolean.valueOf(z);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
