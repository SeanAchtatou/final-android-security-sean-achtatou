package X;

import io.card.payment.BuildConfig;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.1ro  reason: invalid class name and case insensitive filesystem */
public abstract class C35781ro {
    public C35761rm A00;

    public byte A09() {
        C35771rn r4 = (C35771rn) this;
        r4.A00.A01(r4.A04, 0, 1);
        return r4.A04[0];
    }

    public double A0A() {
        C35771rn r3 = (C35771rn) this;
        r3.A00.A01(r3.A04, 0, 8);
        byte[] bArr = r3.A04;
        return Double.longBitsToDouble(((((long) bArr[0]) & 255) << 56) | ((((long) bArr[1]) & 255) << 48) | ((((long) bArr[2]) & 255) << 40) | ((((long) bArr[3]) & 255) << 32) | ((((long) bArr[4]) & 255) << 24) | ((((long) bArr[5]) & 255) << 16) | ((((long) bArr[6]) & 255) << 8) | (255 & ((long) bArr[7])));
    }

    public float A0B() {
        C35771rn r4 = (C35771rn) this;
        r4.A00.A01(r4.A04, 0, 4);
        byte[] bArr = r4.A04;
        return Float.intBitsToFloat((bArr[3] & 255) | ((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8));
    }

    public int A0C() {
        int A01 = C35771rn.A01((C35771rn) this);
        return (-(A01 & 1)) ^ (A01 >>> 1);
    }

    public int A0D(byte b) {
        byte b2;
        if (!(!(this instanceof C35771rn) || (b2 = b & 15) == 2 || b2 == 3)) {
            if (b2 == 4) {
                return 8;
            }
            if (!(b2 == 6 || b2 == 8)) {
                if (b2 == 19) {
                    return 4;
                }
                switch (b2) {
                    case AnonymousClass1Y3.A01:
                    case AnonymousClass1Y3.A02:
                    case AnonymousClass1Y3.A03:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                        break;
                    default:
                        throw new C22334Aw1(1, AnonymousClass08S.A09("Unexpected data type ", (byte) b2));
                }
            }
        }
        return 1;
    }

    public long A0E() {
        C35771rn r7 = (C35771rn) this;
        int i = 0;
        long j = 0;
        while (true) {
            byte A09 = r7.A09();
            j |= ((long) (A09 & Byte.MAX_VALUE)) << i;
            if ((A09 & 128) != 128) {
                return (-(j & 1)) ^ (j >>> 1);
            }
            i += 7;
        }
    }

    public C36241sk A0F() {
        C35771rn r5 = (C35771rn) this;
        byte A09 = r5.A09();
        if (A09 == 0) {
            return C35771rn.A06;
        }
        short s = (short) ((A09 & 240) >> 4);
        byte b = A09 & 15;
        byte b2 = (byte) b;
        C36241sk r6 = new C36241sk(BuildConfig.FLAVOR, C35771rn.A00(b2), s == 0 ? r5.A0L() : (short) (r5.A03 + s));
        boolean z = true;
        if (!(b == 1 || b == 2)) {
            z = false;
        }
        if (z) {
            r5.A02 = b2 == 1 ? Boolean.TRUE : Boolean.FALSE;
        }
        r5.A03 = r6.A02;
        return r6;
    }

    public C36381sy A0G() {
        C35771rn r3 = (C35771rn) this;
        byte A09 = r3.A09();
        int i = (A09 >> 4) & 15;
        if (i == 15) {
            i = C35771rn.A01(r3);
        }
        byte A002 = C35771rn.A00(A09);
        r3.A0Y(A002);
        return new C36381sy(A002, i);
    }

    public C22332Avx A0H() {
        C35771rn r4 = (C35771rn) this;
        int A01 = C35771rn.A01(r4);
        byte A09 = A01 == 0 ? 0 : r4.A09();
        byte A002 = C35771rn.A00((byte) (A09 >> 4));
        byte A003 = C35771rn.A00((byte) (A09 & 15));
        if (A01 > 0) {
            r4.A0a(A002, A003);
        }
        return new C22332Avx(A002, A003, A01);
    }

    public C22331AuI A0I() {
        C36381sy A0G = ((C35771rn) this).A0G();
        return new C22331AuI(A0G.A00, A0G.A01);
    }

    public C36231sj A0J(Map map) {
        C35771rn r2 = (C35771rn) this;
        r2.A00.A00(r2.A03);
        r2.A03 = 0;
        return C35771rn.A07;
    }

    public String A0K() {
        byte[] bArr;
        C35771rn r5 = (C35771rn) this;
        int A01 = C35771rn.A01(r5);
        C35771rn.A04(r5, A01);
        if (A01 == 0) {
            return BuildConfig.FLAVOR;
        }
        if (-1 >= A01) {
            return new String((byte[]) null, 0, A01, C36371sx.A00);
        }
        if (A01 == 0) {
            bArr = new byte[0];
        } else {
            r5.A0Y((byte) 3);
            bArr = new byte[A01];
            r5.A00.A01(bArr, 0, A01);
        }
        return new String(bArr, C36371sx.A00);
    }

    public short A0L() {
        int A01 = C35771rn.A01((C35771rn) this);
        return (short) ((-(A01 & 1)) ^ (A01 >>> 1));
    }

    public void A0M() {
    }

    public void A0N() {
    }

    public void A0O() {
    }

    public void A0P() {
    }

    public void A0R() {
        C35771rn r4 = (C35771rn) this;
        C36361sw r3 = r4.A00;
        short[] sArr = r3.A01;
        int i = r3.A00;
        r3.A00 = i - 1;
        r4.A03 = sArr[i];
    }

    public void A0S() {
    }

    public void A0T() {
        C35771rn.A02((C35771rn) this, (byte) 0);
    }

    public void A0U() {
    }

    public void A0V() {
    }

    public void A0W() {
    }

    public void A0X() {
        C35771rn r4 = (C35771rn) this;
        C36361sw r3 = r4.A00;
        short[] sArr = r3.A01;
        int i = r3.A00;
        r3.A00 = i - 1;
        r4.A03 = sArr[i];
    }

    public void A0Z(byte b) {
        C35771rn.A02((C35771rn) this, b);
    }

    public void A0b(double d) {
        C35771rn r8 = (C35771rn) this;
        long doubleToLongBits = Double.doubleToLongBits(d);
        byte[] bArr = r8.A04;
        bArr[0] = (byte) ((int) ((doubleToLongBits >> 56) & 255));
        bArr[1] = (byte) ((int) ((doubleToLongBits >> 48) & 255));
        bArr[2] = (byte) ((int) ((doubleToLongBits >> 40) & 255));
        bArr[3] = (byte) ((int) ((doubleToLongBits >> 32) & 255));
        bArr[4] = (byte) ((int) ((doubleToLongBits >> 24) & 255));
        bArr[5] = (byte) ((int) ((doubleToLongBits >> 16) & 255));
        bArr[6] = (byte) ((int) ((doubleToLongBits >> 8) & 255));
        bArr[7] = (byte) ((int) (doubleToLongBits & 255));
        r8.A00.A02(bArr, 0, 8);
    }

    public void A0c(int i) {
        C35771rn.A05((C35771rn) this, (i >> 31) ^ (i << 1));
    }

    public void A0d(long j) {
        C35771rn r7 = (C35771rn) this;
        long j2 = (j >> 63) ^ (j << 1);
        int i = 0;
        while (true) {
            int i2 = ((-128 & j2) > 0 ? 1 : ((-128 & j2) == 0 ? 0 : -1));
            byte[] bArr = r7.A04;
            int i3 = i + 1;
            if (i2 == 0) {
                bArr[i] = (byte) ((int) j2);
                r7.A00.A02(bArr, 0, i3);
                return;
            }
            bArr[i] = (byte) ((int) ((127 & j2) | 128));
            j2 >>>= 7;
            i = i3;
        }
    }

    public void A0e(C36241sk r4) {
        C35771rn r2 = (C35771rn) this;
        if (r4.A00 == 2) {
            r2.A01 = r4;
        } else {
            C35771rn.A06(r2, r4, (byte) -1);
        }
    }

    public void A0f(C36381sy r4) {
        C35771rn.A03((C35771rn) this, r4.A00, r4.A01);
    }

    public void A0g(C22332Avx avx) {
        C35771rn r3 = (C35771rn) this;
        int i = avx.A02;
        if (i == 0) {
            C35771rn.A02(r3, (byte) 0);
            return;
        }
        C35771rn.A05(r3, i);
        byte b = avx.A00;
        byte[] bArr = C35771rn.A08;
        C35771rn.A02(r3, (byte) (bArr[avx.A01] | (bArr[b] << 4)));
    }

    public void A0h(C22331AuI auI) {
        C35771rn.A03((C35771rn) this, auI.A00, auI.A01);
    }

    public void A0i(C36231sj r4) {
        C35771rn r2 = (C35771rn) this;
        r2.A00.A00(r2.A03);
        r2.A03 = 0;
    }

    public void A0j(String str) {
        C35771rn r4 = (C35771rn) this;
        byte[] bytes = str.getBytes(C36371sx.A00);
        int length = bytes.length;
        C35771rn.A05(r4, length);
        r4.A00.A02(bytes, 0, length);
    }

    public void A0k(short s) {
        C35771rn.A05((C35771rn) this, (s >> 31) ^ (s << 1));
    }

    public void A0l(boolean z) {
        C35771rn r2 = (C35771rn) this;
        C36241sk r1 = r2.A01;
        byte b = 1;
        if (r1 != null) {
            if (!z) {
                b = 2;
            }
            C35771rn.A06(r2, r1, b);
            r2.A01 = null;
            return;
        }
        if (!z) {
            b = 2;
        }
        C35771rn.A02(r2, b);
    }

    public void A0m(byte[] bArr) {
        C35771rn r0 = (C35771rn) this;
        int length = bArr.length;
        C35771rn.A05(r0, length);
        r0.A00.A02(bArr, 0, length);
    }

    public boolean A0n() {
        C35771rn r2 = (C35771rn) this;
        Boolean bool = r2.A02;
        if (bool == null) {
            return r2.A09() == 1;
        }
        boolean booleanValue = bool.booleanValue();
        r2.A02 = null;
        return booleanValue;
    }

    public byte[] A0o() {
        C35771rn r4 = (C35771rn) this;
        int A01 = C35771rn.A01(r4);
        C35771rn.A04(r4, A01);
        if (A01 == 0) {
            return new byte[0];
        }
        r4.A0Y((byte) 3);
        byte[] bArr = new byte[A01];
        r4.A00.A01(bArr, 0, A01);
        return bArr;
    }

    public static boolean A07() {
        throw new C70863bP("Peeking into a list not supported, likely because it's sized");
    }

    public static boolean A08() {
        throw new C70863bP("Peeking into a map not supported, likely because it's sized");
    }

    public C35781ro(C35761rm r1) {
        this.A00 = r1;
    }

    public void A0Q() {
        A0J(Collections.emptyMap());
    }

    public void A0a(byte b, byte b2) {
        A0D(b);
        A0D(b2);
    }

    public void A0Y(byte b) {
        A0D(b);
    }
}
