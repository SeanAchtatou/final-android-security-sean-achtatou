package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0qu  reason: invalid class name and case insensitive filesystem */
public final class C13210qu {
    private static volatile C13210qu A01;
    public AnonymousClass0UN A00;

    public static final C13210qu A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C13210qu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C13210qu(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C13210qu(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
