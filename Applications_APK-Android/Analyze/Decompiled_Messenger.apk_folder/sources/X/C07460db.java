package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

/* renamed from: X.0db  reason: invalid class name and case insensitive filesystem */
public final class C07460db implements AnonymousClass06U {
    public final /* synthetic */ C07930eP A00;

    public C07460db(C07930eP r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r10) {
        String str;
        int A002 = AnonymousClass09Y.A00(244190063);
        C07930eP r6 = this.A00;
        if (r6.A09.equals(intent.getAction()) && (!r6.A0E || ((str = (String) r6.A0D.get()) != null && Objects.equal(str, intent.getStringExtra("__KEY_LOGGED_USER_ID__"))))) {
            Bundle bundleExtra = intent.getBundleExtra("peer_info");
            if (bundleExtra == null) {
                r6.A06.CGS("PeerProcessManagerImpl", AnonymousClass08S.A0J("Peer info bundle should be in the broadcast intent with action ", r6.A09));
            } else {
                try {
                    C06130au A003 = C06130au.A00(bundleExtra);
                    C06130au r4 = r6.A07;
                    int i = A003.A01;
                    if (i != r4.A01 && !r6.A0B.containsKey(Integer.valueOf(i))) {
                        Preconditions.checkNotNull(r4.A00, "The mMessenger member should have been set in init()");
                        Message obtain = Message.obtain((Handler) null, 0);
                        obtain.setData(r4.A01());
                        try {
                            A003.A00.send(obtain);
                            C07930eP.A04(r6, A003, AnonymousClass07B.A00);
                        } catch (RemoteException unused) {
                        }
                    }
                } catch (IllegalArgumentException unused2) {
                    r6.A06.CGS("PeerProcessManagerImpl", AnonymousClass08S.A0P("Peer info bundle in the broadcast intent with action ", r6.A09, " was malformed"));
                }
            }
        }
        AnonymousClass09Y.A01(-581193301, A002);
    }
}
