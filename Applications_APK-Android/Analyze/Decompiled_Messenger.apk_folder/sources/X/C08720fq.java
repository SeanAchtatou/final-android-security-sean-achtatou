package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.0fq  reason: invalid class name and case insensitive filesystem */
public final class C08720fq {
    public static AnonymousClass1Y7 A00;
    public static AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F;
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y7 A0H;
    public static final AnonymousClass1Y7 A0I;
    public static final AnonymousClass1Y7 A0J;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A06.A09("analytics");
        A0D = r1;
        A0E = (AnonymousClass1Y7) r1.A09("process_stat_interval");
        AnonymousClass1Y7 r12 = A0D;
        A02 = (AnonymousClass1Y7) r12.A09(TurboLoader.Locator.$const$string(143));
        A03 = (AnonymousClass1Y7) r12.A09("contacts_upload_interval");
        A04 = (AnonymousClass1Y7) r12.A09("device_info_interval");
        A05 = (AnonymousClass1Y7) r12.A09("metainf_fbmeta");
        A06 = (AnonymousClass1Y7) r12.A09("metainf_fbmeta_version_code");
        A09 = (AnonymousClass1Y7) r12.A09("device_stat_interval");
        r12.A09("user_logged_in");
        A0F = (AnonymousClass1Y7) r12.A09("sampling_config");
        A0G = (AnonymousClass1Y7) r12.A09("sampling_config_checksum");
        A0H = (AnonymousClass1Y7) r12.A09("sampling_config_owner_id");
        A0C = (AnonymousClass1Y7) r12.A09("periodic_events_last_sent");
        A07 = (AnonymousClass1Y7) r12.A09("device_info_need_upload_phone/");
        A08 = (AnonymousClass1Y7) r12.A09("device_info_oldest_known_installer");
        A0I = (AnonymousClass1Y7) r12.A09("apk_install_source_list");
        A0J = (AnonymousClass1Y7) r12.A09("previous_apk_install_source");
        AnonymousClass1Y7 r13 = C04350Ue.A06;
        A0B = (AnonymousClass1Y7) r13.A09("show_navigation_events");
        A0A = (AnonymousClass1Y7) r13.A09("show_endpoint_mapping");
        r13.A09("enable_notifications_debug_mode");
        AnonymousClass1Y7 r14 = A0D;
        r14.A09("build_flavor_last_report_time_stamp");
        r14.A09("build_flavor_last_report_build_id");
        A01 = (AnonymousClass1Y7) r14.A09("detection_front_camera_size");
        A00 = (AnonymousClass1Y7) r14.A09("detection_back_camera_size");
    }
}
