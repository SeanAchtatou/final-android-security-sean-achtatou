package X;

import android.os.Build;
import com.facebook.common.dextricks.DexErrorRecoveryInfo;
import com.facebook.common.dextricks.DexLibLoader;
import com.facebook.common.dextricks.DexStore;
import com.facebook.common.dextricks.MultiDexClassLoader;
import com.facebook.common.dextricks.MultiDexClassLoaderArtNative;
import com.facebook.common.dextricks.verifier.Verifier;
import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1bh  reason: invalid class name and case insensitive filesystem */
public final class C26591bh implements C08560fY {
    private static volatile C26591bh A00;

    public String AWk() {
        return "dex_info";
    }

    public static final C26591bh A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C26591bh.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C26591bh();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public long BL4() {
        return C08350fD.A04;
    }

    public void BjZ(PerformanceLoggingEvent performanceLoggingEvent) {
        boolean z;
        String str;
        boolean z2;
        if (DexLibLoader.deoptTaint) {
            performanceLoggingEvent.A0D("dex_unopt", true);
        }
        DexErrorRecoveryInfo mainDexStoreLoadInformation = DexLibLoader.getMainDexStoreLoadInformation();
        performanceLoggingEvent.A0B("odex_scheme_name", mainDexStoreLoadInformation.odexSchemeName);
        if ((mainDexStoreLoadInformation.loadResult & 128) != 0) {
            performanceLoggingEvent.A0D("oatmeal", true);
        }
        int i = mainDexStoreLoadInformation.loadResult;
        if ((i & DexStore.LOAD_RESULT_OATMEAL_QUICKENED) != 0) {
            performanceLoggingEvent.A0D("quick", true);
        } else if ((i & 512) != 0) {
            performanceLoggingEvent.A0D("dex2oat_quick", true);
        }
        if ((mainDexStoreLoadInformation.loadResult & 1024) != 0) {
            performanceLoggingEvent.A0D("mixed", true);
        }
        if ((mainDexStoreLoadInformation.loadResult & DexStore.LOAD_RESULT_PGO) != 0) {
            performanceLoggingEvent.A0D("pgo", true);
        }
        int i2 = mainDexStoreLoadInformation.loadResult;
        if ((i2 & 2048) != 0) {
            performanceLoggingEvent.A0D("quick_attempted", true);
        } else if ((i2 & DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED) != 0) {
            performanceLoggingEvent.A0D("dex2oat_quick_attempted", true);
        }
        if ((mainDexStoreLoadInformation.loadResult & DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED) != 0) {
            performanceLoggingEvent.A0D("mixed_attempted", true);
        }
        if ((mainDexStoreLoadInformation.loadResult & DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET) != 0) {
            performanceLoggingEvent.A0D("dex2oat_classpath", true);
        }
        if ((mainDexStoreLoadInformation.loadResult & DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP) != 0) {
            performanceLoggingEvent.A0D("periodic_pgo_profile_set", true);
        }
        if ((mainDexStoreLoadInformation.loadResult & DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED) != 0) {
            performanceLoggingEvent.A0D("periodic_pgo_profile_attempted", true);
        }
        if (mainDexStoreLoadInformation.dexoptDuringColdStart) {
            performanceLoggingEvent.A0D("dexopt_during_cold_start", true);
        }
        Class<Verifier> cls = Verifier.class;
        synchronized (cls) {
            z = Verifier.sTriedDisableRuntimeVerification;
        }
        if (z) {
            synchronized (cls) {
                z2 = Verifier.sDisabledRuntimeVerification;
            }
            if (z2) {
                performanceLoggingEvent.A0D("disabled_rt_verifier", true);
            } else {
                performanceLoggingEvent.A0D("failed_disable_rt_verifier", true);
            }
        }
        ClassLoader classLoader = MultiDexClassLoader.sInstalledClassLoader;
        if (classLoader != null) {
            str = classLoader.getClass().getSimpleName();
        } else {
            str = "mdcl_none";
        }
        if (str.isEmpty()) {
            str = "mdcl_anonymous";
        }
        performanceLoggingEvent.A0B("class_name", str);
        if (MultiDexClassLoader.sFancyLoaderFailure != null) {
            performanceLoggingEvent.A0D("mdcl_fancy_failure", true);
        }
        if ((classLoader instanceof MultiDexClassLoaderArtNative) && !MultiDexClassLoaderArtNative.isFastHooked()) {
            performanceLoggingEvent.A0D("slow_hooks", true);
        }
        if (Build.VERSION.SDK_INT >= 26) {
            performanceLoggingEvent.A06("odex_size", mainDexStoreLoadInformation.odexSize);
        }
        if (AnonymousClass096.A01()) {
            synchronized (AnonymousClass096.class) {
            }
            performanceLoggingEvent.A0B("ditto_patch", String.valueOf((Object) null));
        }
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A02;
    }
}
