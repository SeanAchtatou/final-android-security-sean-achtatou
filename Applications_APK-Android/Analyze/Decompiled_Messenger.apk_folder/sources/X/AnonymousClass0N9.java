package X;

/* renamed from: X.0N9  reason: invalid class name */
public final class AnonymousClass0N9 {
    public final boolean A00;
    public final boolean A01;
    public final boolean A02;

    public String toString() {
        return "[ " + getClass().getSimpleName() + " is default: " + this.A00 + " useNewWaitForGcDistractCoordinator: " + this.A02 + " useNewCollectGcDistractCoordinator: " + this.A01 + " ]";
    }

    public AnonymousClass0N9() {
        this(false, false, true);
    }

    private AnonymousClass0N9(boolean z, boolean z2, boolean z3) {
        this.A02 = z;
        this.A01 = z2;
        this.A00 = z3;
    }
}
