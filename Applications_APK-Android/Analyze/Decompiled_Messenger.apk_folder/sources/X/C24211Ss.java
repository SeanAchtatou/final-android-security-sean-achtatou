package X;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.callercontext.CallerContext;

/* renamed from: X.1Ss  reason: invalid class name and case insensitive filesystem */
public final class C24211Ss {
    public static void A00(AnonymousClass1ZE r5, CallerContext callerContext, Drawable drawable, Bitmap bitmap) {
        if (Math.random() <= 0.002d && r5 != null && callerContext != null && drawable != null && bitmap != null) {
            Rect bounds = drawable.getBounds();
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r5.A01("messenger_instrumented_drawable"), AnonymousClass1Y3.A37);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0D("analytics_tag", callerContext.A0F());
                uSLEBaseShape0S0000000.A0D("calling_class", callerContext.A02);
                uSLEBaseShape0S0000000.A0D("feature_tag", callerContext.A0G());
                uSLEBaseShape0S0000000.A0D("image_height", String.valueOf(bitmap.getHeight()));
                uSLEBaseShape0S0000000.A0D("image_width", String.valueOf(bitmap.getWidth()));
                uSLEBaseShape0S0000000.A0D("module_tag", callerContext.A0H());
                uSLEBaseShape0S0000000.A0D("view_height", String.valueOf(bounds.height()));
                uSLEBaseShape0S0000000.A0D("view_width", String.valueOf(bounds.width()));
                uSLEBaseShape0S0000000.A06();
            }
        }
    }
}
