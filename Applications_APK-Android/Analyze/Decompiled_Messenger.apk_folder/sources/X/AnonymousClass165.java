package X;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;

/* renamed from: X.165  reason: invalid class name */
public interface AnonymousClass165 {
    void BNK(Fragment fragment, int i, int i2, Intent intent);

    void BNh(Fragment fragment, Bundle bundle);

    void BNp(Fragment fragment, boolean z);

    void BNx(Fragment fragment, View view, Bundle bundle);

    void BNy(Fragment fragment);

    void BOY(Fragment fragment, Fragment fragment2);

    void BP9(C138056cU r1);

    void BPa(Fragment fragment, Bundle bundle);

    void BPf(Bundle bundle);

    void BTL(Fragment fragment, Configuration configuration);

    void BVy(Fragment fragment);

    void Bi0(Fragment fragment);

    void BmO(Fragment fragment);

    void Bmy(Fragment fragment, Bundle bundle);

    void BoL(Fragment fragment, boolean z);

    void BpQ(Fragment fragment);

    void Bq5(Fragment fragment);
}
