package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.16n  reason: invalid class name and case insensitive filesystem */
public final class C190316n extends AnonymousClass0W4 {
    private static final ImmutableList A00 = ImmutableList.of(C05910aX.A02, C05910aX.A03);

    public C190316n() {
        super("contacts_db_properties", A00);
    }
}
