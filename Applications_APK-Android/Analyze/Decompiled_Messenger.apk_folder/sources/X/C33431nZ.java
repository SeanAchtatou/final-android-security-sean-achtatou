package X;

import io.card.payment.BuildConfig;
import java.util.Map;

/* renamed from: X.1nZ  reason: invalid class name and case insensitive filesystem */
public final class C33431nZ {
    public Map adaptiveFetchClientParams = null;
    public Map additionalHttpHeaders = null;
    public String[] analyticTags = new String[0];
    public int cacheTtlSeconds = Integer.MAX_VALUE;
    public String clientQueryId = BuildConfig.FLAVOR;
    public String clientTraceId = BuildConfig.FLAVOR;
    public boolean doNotResumeLiveQuery = false;
    public boolean enableExperimentalGraphStoreCache = false;
    public boolean enableOfflineCaching = false;
    public boolean ensureCacheWrite = false;
    public String[] excludedCacheKeyParameters = new String[0];
    public int freshCacheTtlSeconds = 0;
    public String hackQueryContext = BuildConfig.FLAVOR;
    public int hackQueryType = 0;
    public String locale = BuildConfig.FLAVOR;
    public boolean markHttpRequestReplaySafe = false;
    public int networkTimeoutSeconds = -1;
    public boolean onlyCacheInitialNetworkResponse = false;
    public boolean parseOnClientExecutor = false;
    public boolean primed = false;
    public String primedClientQueryId = BuildConfig.FLAVOR;
    public int requestPurpose = 0;
    public boolean sendCacheAgeForAdaptiveFetch = false;
    public boolean terminateAfterFreshResponse = false;
}
