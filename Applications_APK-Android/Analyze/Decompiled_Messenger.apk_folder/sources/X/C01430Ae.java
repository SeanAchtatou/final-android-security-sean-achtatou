package X;

import com.facebook.acra.ACRA;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0Ae  reason: invalid class name and case insensitive filesystem */
public final class C01430Ae {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "FBNS";
            case 2:
                return "PreloadedFBNS";
            case 3:
                return "MqttDirect";
            case 4:
                return "MqttSimpleClient";
            case 5:
                return "MultiuserMqtt";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "TestMqttLite";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "TestFBNS";
            default:
                return "MqttLite";
        }
    }
}
