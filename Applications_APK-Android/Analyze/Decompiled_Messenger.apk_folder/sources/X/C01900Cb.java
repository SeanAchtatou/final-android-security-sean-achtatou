package X;

import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Cb  reason: invalid class name and case insensitive filesystem */
public class C01900Cb extends C01910Cc implements Runnable, C01940Cf {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.common.executors.WakingExecutorService$ListenableScheduledFutureImpl";
    public final AnonymousClass0CY A00;
    public final /* synthetic */ C01530Ap A01;

    public void addListener(Runnable runnable, Executor executor) {
        this.A00.addListener(runnable, executor);
    }

    public boolean cancel(boolean z) {
        C01970Ci r1;
        C01530Ap r3 = this.A01;
        synchronized (r3) {
            Iterator it = r3.A02.iterator();
            while (true) {
                if (!it.hasNext()) {
                    r1 = null;
                    break;
                }
                r1 = (C01970Ci) it.next();
                if (r1.A01 == this) {
                    break;
                }
            }
            if (r1 != null) {
                r3.A02.remove(r1);
                C01530Ap.A03(r3);
            }
        }
        return this.A00.cancel(z);
    }

    public int compareTo(Object obj) {
        throw new UnsupportedOperationException();
    }

    public long getDelay(TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void run() {
        this.A00.run();
    }

    public /* bridge */ /* synthetic */ Object A00() {
        return this.A00;
    }

    public /* bridge */ /* synthetic */ Future A01() {
        return this.A00;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C01900Cb(C01530Ap r2, Runnable runnable, Object obj) {
        super(r2.A00);
        this.A01 = r2;
        this.A00 = new AnonymousClass0CY(runnable, obj);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C01900Cb(C01530Ap r2, Callable callable) {
        super(r2.A00);
        this.A01 = r2;
        this.A00 = new AnonymousClass0CY(callable);
    }
}
