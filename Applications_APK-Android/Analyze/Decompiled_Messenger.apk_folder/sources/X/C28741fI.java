package X;

import android.database.Cursor;

/* renamed from: X.1fI  reason: invalid class name and case insensitive filesystem */
public final class C28741fI extends C28751fJ {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final int A0C;
    public final int A0D;
    public final int A0E;
    public final int A0F;
    public final int A0G;
    public final int A0H;

    public C28741fI(Cursor cursor) {
        super(cursor);
        this.A0F = cursor.getColumnIndexOrThrow("thread_key");
        this.A0H = cursor.getColumnIndexOrThrow("user_key");
        this.A0A = cursor.getColumnIndexOrThrow("name");
        this.A0B = cursor.getColumnIndexOrThrow("phone");
        this.A0E = cursor.getColumnIndexOrThrow("sms_participant_fbid");
        this.A04 = cursor.getColumnIndexOrThrow("is_commerce");
        cursor.getColumnIndexOrThrow("profile_type");
        this.A0G = cursor.getColumnIndexOrThrow("type");
        this.A03 = cursor.getColumnIndexOrThrow("is_admin");
        this.A00 = cursor.getColumnIndexOrThrow("admin_type");
        this.A07 = cursor.getColumnIndexOrThrow("last_read_receipt_time");
        this.A08 = cursor.getColumnIndexOrThrow("last_read_receipt_watermark_time");
        this.A06 = cursor.getColumnIndexOrThrow("last_delivered_receipt_time");
        this.A05 = cursor.getColumnIndexOrThrow("last_delivered_receipt_id");
        this.A0D = cursor.getColumnIndexOrThrow("request_timestamp_ms");
        this.A01 = cursor.getColumnIndexOrThrow("can_viewer_message");
        this.A02 = cursor.getColumnIndexOrThrow("inviter_user_key");
        this.A0C = cursor.getColumnIndexOrThrow("request_source");
        this.A09 = cursor.getColumnIndexOrThrow("messaging_actor_type");
    }
}
