package X;

import android.content.Intent;

/* renamed from: X.1w2  reason: invalid class name and case insensitive filesystem */
public class C37661w2 extends Exception {
    public final Intent mIntent;

    public C37661w2(String str, Intent intent) {
        super(str);
        this.mIntent = intent;
    }
}
