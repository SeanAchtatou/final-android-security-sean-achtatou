package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

@InjectorModule
/* renamed from: X.127  reason: invalid class name */
public final class AnonymousClass127 extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static final Object A01 = new Object();

    public static final AnonymousClass128 A00(AnonymousClass1XY r5) {
        AnonymousClass128 r0;
        synchronized (A01) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r5)) {
                    A00.A00 = new AnonymousClass128(FbSharedPreferencesModule.A00((AnonymousClass1XY) A00.A01()), AnonymousClass129.A0V);
                }
                C05540Zi r1 = A00;
                r0 = (AnonymousClass128) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }
}
