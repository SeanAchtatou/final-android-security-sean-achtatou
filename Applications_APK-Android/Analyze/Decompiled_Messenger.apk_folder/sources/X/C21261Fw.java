package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.1Fw  reason: invalid class name and case insensitive filesystem */
public final class C21261Fw implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.push.mqtt.external.ClientSubscriptionManager$2";
    public final /* synthetic */ C21171Fm A00;
    public final /* synthetic */ ImmutableList A01;
    public final /* synthetic */ ImmutableList A02;

    public C21261Fw(C21171Fm r1, ImmutableList immutableList, ImmutableList immutableList2) {
        this.A00 = r1;
        this.A01 = immutableList;
        this.A02 = immutableList2;
    }

    public void run() {
        C21171Fm.A01(this.A00, this.A01, this.A02);
        C21171Fm r0 = this.A00;
        boolean z = r0.A01;
        C33641nu r3 = r0.A00;
        ImmutableList immutableList = this.A01;
        ImmutableList immutableList2 = this.A02;
        if (r3 != null) {
            AnonymousClass0j4.A08(immutableList);
            AnonymousClass0j4.A08(immutableList2);
            if (!immutableList.isEmpty() || !immutableList2.isEmpty()) {
                r3.A00.A0D.A0P(z, immutableList, immutableList2);
            }
        } else if (!immutableList.isEmpty()) {
            AnonymousClass0j4.A08(immutableList);
        }
    }
}
