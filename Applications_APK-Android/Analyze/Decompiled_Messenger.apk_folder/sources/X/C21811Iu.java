package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Iu  reason: invalid class name and case insensitive filesystem */
public final class C21811Iu {
    private final Map A00 = new HashMap();

    public synchronized void A00() {
        Iterator it = this.A00.keySet().iterator();
        while (it.hasNext()) {
            AnonymousClass1KB r1 = (AnonymousClass1KB) this.A00.get(it.next());
            if (r1 == null || !r1.A00) {
                it.remove();
            } else {
                r1.A00 = false;
            }
        }
    }

    public synchronized void A01(AnonymousClass0p4 r6, C17810zV r7, String str) {
        if (str != null) {
            AnonymousClass1KB r1 = (AnonymousClass1KB) this.A00.get(str);
            if (r1 != null) {
                r1.A00 = true;
                C07870eJ r4 = r1.A01;
                int A01 = r4.A01();
                for (int i = 0; i < A01; i++) {
                    AnonymousClass10N r0 = (AnonymousClass10N) r4.A05(i);
                    r0.A00 = r7;
                    Object[] objArr = r0.A02;
                    if (objArr != null) {
                        objArr[0] = r6;
                    }
                }
            }
        }
    }

    public synchronized void A02(String str, AnonymousClass10N r4) {
        if (str != null) {
            AnonymousClass1KB r1 = (AnonymousClass1KB) this.A00.get(str);
            if (r1 == null) {
                r1 = new AnonymousClass1KB();
                this.A00.put(str, r1);
            }
            r1.A01.A09(r4.A01, r4);
        }
    }
}
