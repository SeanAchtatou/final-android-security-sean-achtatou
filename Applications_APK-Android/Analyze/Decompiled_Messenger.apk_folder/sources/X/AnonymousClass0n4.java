package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0n4  reason: invalid class name */
public final class AnonymousClass0n4 {
    private static C05540Zi A00;

    public static final AnonymousClass0n4 A00(AnonymousClass1XY r3) {
        AnonymousClass0n4 r0;
        synchronized (AnonymousClass0n4.class) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r3)) {
                    A00.A01();
                    A00.A00 = new AnonymousClass0n4();
                }
                C05540Zi r1 = A00;
                r0 = (AnonymousClass0n4) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }
}
