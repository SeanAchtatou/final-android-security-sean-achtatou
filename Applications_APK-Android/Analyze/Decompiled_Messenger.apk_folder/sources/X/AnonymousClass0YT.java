package X;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import java.util.List;
import java.util.WeakHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0YT  reason: invalid class name */
public final class AnonymousClass0YT {
    public static final Class A08 = AnonymousClass0YT.class;
    private static volatile AnonymousClass0YT A09;
    public AnonymousClass0UN A00;
    public Object A01 = new Object();
    public Object A02 = new Object();
    private Handler A03;
    private List A04;
    private boolean A05;
    public final WeakHashMap A06;
    public final WeakHashMap A07;

    public static final AnonymousClass0YT A00(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (AnonymousClass0YT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new AnonymousClass0YT(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static void A01(AnonymousClass0YT r5) {
        boolean A062;
        boolean z = false;
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, r5.A00)).AOz();
        synchronized (r5.A01) {
            try {
                A062 = r5.A06();
                if (A062 != r5.A05) {
                    z = true;
                    r5.A05 = A062;
                } else {
                    A062 = false;
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (z) {
            r5.A04.clear();
            synchronized (r5.A02) {
                try {
                    for (C05130Xt add : r5.A07.keySet()) {
                        r5.A04.add(add);
                    }
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
            for (C05130Xt Btw : r5.A04) {
                Btw.Btw(A062);
            }
            r5.A04.clear();
            return;
        }
        return;
        throw th;
    }

    public static void A02(AnonymousClass0YT r5) {
        synchronized (r5.A01) {
            if (r5.A06() != r5.A05) {
                r5.A03.sendEmptyMessageDelayed(0, 100);
            }
        }
    }

    public void A03(View view) {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, this.A00)).AOz();
        synchronized (this.A01) {
            this.A06.remove(view);
            this.A06.size();
        }
        A02(this);
    }

    public void A04(View view) {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, this.A00)).AOz();
        synchronized (this.A01) {
            this.A06.put(view, Long.valueOf(((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, this.A00)).now()));
            this.A06.size();
        }
        A01(this);
    }

    public void A05(C05130Xt r4) {
        synchronized (this.A02) {
            this.A07.put(r4, true);
        }
        r4.Btw(A06());
    }

    public boolean A06() {
        boolean z;
        synchronized (this.A01) {
            z = false;
            if (!this.A06.isEmpty()) {
                z = true;
            }
        }
        return z;
    }

    private AnonymousClass0YT(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(3, r4);
        this.A06 = new WeakHashMap();
        this.A07 = new WeakHashMap();
        this.A03 = new Handler(Looper.getMainLooper(), new AnonymousClass0YU(this));
        this.A04 = C04300To.A00();
    }
}
