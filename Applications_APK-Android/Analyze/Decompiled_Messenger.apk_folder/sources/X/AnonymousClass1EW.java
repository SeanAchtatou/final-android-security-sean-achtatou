package X;

import com.facebook.orca.threadview.DownloadAttachmentDialogFragment;
import java.util.concurrent.CancellationException;

/* renamed from: X.1EW  reason: invalid class name */
public final class AnonymousClass1EW extends AnonymousClass0lj {
    public final /* synthetic */ DownloadAttachmentDialogFragment A00;

    public AnonymousClass1EW(DownloadAttachmentDialogFragment downloadAttachmentDialogFragment) {
        this.A00 = downloadAttachmentDialogFragment;
    }

    public void A03(CancellationException cancellationException) {
        this.A00.A22();
    }
}
