package X;

import com.google.common.base.Platform;

/* renamed from: X.0a7  reason: invalid class name and case insensitive filesystem */
public final class C05660a7 {
    private final String A00;
    private final String A01;
    private final String A02;

    public static String A00(C05660a7 r3, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_");
        sb.append(r3.A01);
        String str2 = r3.A00;
        if (!Platform.stringIsNullOrEmpty(str2)) {
            sb.append("_sec:");
            sb.append(str2);
        }
        String str3 = r3.A02;
        if (!Platform.stringIsNullOrEmpty(str3)) {
            sb.append("_ter:");
            sb.append(str3);
        }
        return sb.toString();
    }

    public C11500nH A01() {
        return new C11500nH(A00(this, "is_db_invalidated"));
    }

    public C05660a7(String str, String str2, String str3) {
        this.A01 = str;
        this.A00 = str2;
        this.A02 = str3;
    }
}
