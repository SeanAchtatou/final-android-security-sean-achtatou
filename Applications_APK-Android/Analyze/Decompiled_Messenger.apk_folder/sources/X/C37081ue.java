package X;

import android.util.SparseIntArray;

/* renamed from: X.1ue  reason: invalid class name and case insensitive filesystem */
public final class C37081ue extends C09210gj {
    public final SparseIntArray A00;
    public final SparseIntArray A01;

    public C37081ue(int i) {
        this.A01 = new SparseIntArray(i);
        this.A00 = new SparseIntArray(i);
    }
}
