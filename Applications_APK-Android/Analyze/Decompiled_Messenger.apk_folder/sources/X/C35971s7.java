package X;

import com.facebook.graphql.enums.GraphQLMessengerCallToActionRenderStyle;
import com.facebook.graphql.enums.GraphQLMessengerCallToActionType;
import com.facebook.graphql.enums.GraphQLMessengerPIIDateSubtype;
import com.facebook.graphql.enums.GraphQLMessengerPIIType;
import com.facebook.graphql.enums.GraphQLMessengerPlatformWebviewStyleType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.browser.model.MessengerWebViewParams;
import com.facebook.messaging.business.common.calltoaction.model.CTABrandedCameraParams;
import com.facebook.messaging.business.common.calltoaction.model.CTAInformationIdentify;
import com.facebook.messaging.business.common.calltoaction.model.CTAPaymentInfo;
import com.facebook.messaging.business.common.calltoaction.model.CTAUserConfirmation;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.business.common.calltoaction.model.CallToActionShareTarget;
import com.facebook.messaging.business.common.calltoaction.model.CallToActionSimpleTarget;
import com.facebook.messaging.business.common.calltoaction.model.CallToActionTarget;
import com.facebook.messaging.business.informationidentify.model.PIIPrivacy;
import com.facebook.messaging.business.informationidentify.model.PIIQuestion;
import com.facebook.messaging.business.informationidentify.model.PIISinglePage;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1s7  reason: invalid class name and case insensitive filesystem */
public final class C35971s7 {
    public static CallToAction A00(C35961s6 r9) {
        GraphQLMessengerCallToActionType Abm;
        CallToActionTarget callToActionTarget;
        boolean z;
        CTAUserConfirmation cTAUserConfirmation;
        CTAPaymentInfo cTAPaymentInfo;
        MessengerWebViewParams A00;
        CTABrandedCameraParams cTABrandedCameraParams;
        CTAInformationIdentify cTAInformationIdentify;
        C30101hU A002;
        if (r9 == null || (Abm = r9.Abm()) == null) {
            return null;
        }
        C188515m r2 = new C188515m();
        r2.A0B = r9.getId();
        r2.A0E = r9.Abp();
        r2.A01(r9.Abq());
        r2.A02(r9.AvP());
        r2.A0G = r9.ArA();
        r2.A0H = r9.ArG();
        r2.A0F = r9.Aqt();
        GraphQLMessengerCallToActionRenderStyle Aiu = r9.Aiu();
        if (Aiu != null) {
            if (Aiu == null) {
                A002 = null;
            } else {
                A002 = C30101hU.A00(Aiu.name());
            }
            r2.A07 = A002;
        }
        String name = Abm.name();
        C16550xJ r4 = null;
        if (name != null) {
            try {
                r4 = C16550xJ.valueOf(name);
            } catch (IllegalArgumentException unused) {
            }
        }
        r2.A08 = r4;
        ImmutableList Abl = r9.Abl();
        if (Abl == null || Abl.isEmpty()) {
            callToActionTarget = null;
        } else {
            GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) Abl.get(0);
            if (r4.equals(C16550xJ.A03)) {
                C210799xo r1 = new C210799xo();
                r1.A02 = gSTModelShape1S0000000.A3m();
                r1.A07 = gSTModelShape1S0000000.A0P(706835233);
                r1.A05 = gSTModelShape1S0000000.A0P(-1086629201);
                r1.A00 = gSTModelShape1S0000000.A0P(1292959499);
                r1.A01 = gSTModelShape1S0000000.A0P(1419586366);
                r1.A03 = gSTModelShape1S0000000.A3n();
                r1.A04 = gSTModelShape1S0000000.A0P(486946241);
                r1.A06 = gSTModelShape1S0000000.A0P(-1921708012);
                callToActionTarget = new CallToActionShareTarget(r1);
            } else {
                C29392EYy eYy = new C29392EYy();
                eYy.A00 = gSTModelShape1S0000000.A3m();
                callToActionTarget = new CallToActionSimpleTarget(eYy);
            }
        }
        r2.A09 = callToActionTarget;
        GSTModelShape1S0000000 B84 = r9.B84();
        if (B84 == null || (Platform.stringIsNullOrEmpty(B84.A0P(861951438)) && Platform.stringIsNullOrEmpty(B84.A0P(1668820477)) && Platform.stringIsNullOrEmpty(B84.A0P(-1090478017)) && Platform.stringIsNullOrEmpty(B84.A0P(1961782604)))) {
            z = false;
        } else {
            z = true;
        }
        if (!z) {
            cTAUserConfirmation = null;
        } else {
            C30011hL r12 = new C30011hL();
            r12.A02 = B84.A0P(861951438);
            r12.A01 = B84.A0P(1668820477);
            r12.A03 = B84.A0P(-1090478017);
            r12.A00 = B84.A0P(1961782604);
            cTAUserConfirmation = new CTAUserConfirmation(r12);
        }
        r2.A06 = cTAUserConfirmation;
        GSTModelShape1S0000000 AxZ = r9.AxZ();
        if (AxZ == null) {
            cTAPaymentInfo = null;
        } else {
            C30071hR r13 = new C30071hR();
            r13.A01 = AxZ.A0P(-395678578);
            r13.A00 = AxZ.A0P(57346556);
            cTAPaymentInfo = new CTAPaymentInfo(r13);
        }
        r2.A05 = cTAPaymentInfo;
        r2.A0C = r9.Asu();
        GSTModelShape1S0000000 B9r = r9.B9r();
        if (B9r == null) {
            A00 = null;
        } else {
            C30041hO r3 = new C30041hO();
            r3.A00 = B9r.getDoubleValue(-1939109133);
            r3.A07 = B9r.getBooleanValue(-707284113);
            GraphQLMessengerPlatformWebviewStyleType graphQLMessengerPlatformWebviewStyleType = (GraphQLMessengerPlatformWebviewStyleType) B9r.A0O(-1929765397, GraphQLMessengerPlatformWebviewStyleType.A01);
            if (graphQLMessengerPlatformWebviewStyleType != null) {
                r3.A01 = C30051hP.A00(graphQLMessengerPlatformWebviewStyleType.name());
            }
            A00 = r3.A00();
        }
        r2.A02 = A00;
        r2.A0D = r9.Awz();
        GSTModelShape1S0000000 Ag2 = r9.Ag2();
        if (Ag2 == null) {
            cTABrandedCameraParams = null;
        } else {
            A0C a0c = new A0C();
            a0c.A00 = Ag2.A0P(264552097);
            a0c.A01 = Ag2.A0P(-2094369524);
            cTABrandedCameraParams = new CTABrandedCameraParams(a0c);
        }
        r2.A03 = cTABrandedCameraParams;
        GSTModelShape1S0000000 Ait = r9.Ait();
        if (Ait == null) {
            cTAInformationIdentify = null;
        } else {
            C04160So r42 = new C04160So();
            String A0P = Ait.A0P(474021748);
            if (A0P != null) {
                r42.A05 = A0P;
            }
            String A0P2 = Ait.A0P(-677446026);
            if (A0P2 != null) {
                r42.A03 = A0P2;
            }
            String A0P3 = Ait.A0P(582751986);
            if (A0P3 != null) {
                r42.A01 = A0P3;
            }
            r42.A04 = Integer.toString(Ait.getIntValue(-1810489645));
            r42.A02 = Integer.toString(Ait.getIntValue(445043936));
            Class<GSTModelShape1S0000000> cls = GSTModelShape1S0000000.class;
            GSTModelShape1S0000000 gSTModelShape1S00000002 = (GSTModelShape1S0000000) Ait.A0J(-1491681162, cls, -1470974065);
            if (gSTModelShape1S00000002 != null) {
                C187588nS r5 = new C187588nS();
                ImmutableList A0M = gSTModelShape1S00000002.A0M(-1782234803, cls, -932375598);
                if (A0M != null) {
                    ImmutableList.Builder builder = new ImmutableList.Builder();
                    C24971Xv it = A0M.iterator();
                    while (it.hasNext()) {
                        GSTModelShape1S0000000 gSTModelShape1S00000003 = (GSTModelShape1S0000000) it.next();
                        if (gSTModelShape1S00000003 != null) {
                            C187518nL r32 = new C187518nL();
                            String A3m = gSTModelShape1S00000003.A3m();
                            if (A3m != null) {
                                r32.A02 = A3m;
                            }
                            String A40 = gSTModelShape1S00000003.A40();
                            if (A40 != null) {
                                r32.A07 = A40;
                            }
                            GraphQLMessengerPIIType graphQLMessengerPIIType = (GraphQLMessengerPIIType) gSTModelShape1S00000003.A0O(3575610, GraphQLMessengerPIIType.A01);
                            if (graphQLMessengerPIIType != null) {
                                r32.A01 = C187818nr.A00(graphQLMessengerPIIType.name());
                            }
                            GraphQLMessengerPIIDateSubtype graphQLMessengerPIIDateSubtype = (GraphQLMessengerPIIDateSubtype) gSTModelShape1S00000003.A0O(-1268779017, GraphQLMessengerPIIDateSubtype.A01);
                            if (graphQLMessengerPIIDateSubtype != null) {
                                r32.A00 = C187598nT.A00(graphQLMessengerPIIDateSubtype.name());
                            }
                            r32.A05 = gSTModelShape1S00000003.A0P(598246771);
                            r32.A06 = gSTModelShape1S00000003.A3x();
                            r32.A03 = Integer.toString(gSTModelShape1S00000003.A0a());
                            r32.A04 = Boolean.toString(gSTModelShape1S00000003.getBooleanValue(3344108));
                            builder.add((Object) new PIIQuestion(r32));
                        }
                    }
                    r5.A01 = builder.build();
                }
                GSTModelShape1S0000000 gSTModelShape1S00000004 = (GSTModelShape1S0000000) gSTModelShape1S00000002.A0J(-1818637815, GSTModelShape1S0000000.class, 521144406);
                if (gSTModelShape1S00000004 != null) {
                    C187738nj r33 = new C187738nj();
                    r33.A00 = gSTModelShape1S00000004.A3y();
                    r33.A01 = gSTModelShape1S00000004.A42();
                    r5.A00 = new PIIPrivacy(r33);
                }
                String A0P4 = gSTModelShape1S00000002.A0P(-1335409467);
                if (A0P4 != null) {
                    r5.A02 = A0P4;
                }
                r42.A00 = new PIISinglePage(r5);
            }
            cTAInformationIdentify = new CTAInformationIdentify(r42);
        }
        r2.A04 = cTAInformationIdentify;
        return r2.A00();
    }

    public static ImmutableList A01(ImmutableList immutableList) {
        ImmutableList.Builder builder = ImmutableList.builder();
        if (immutableList != null) {
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                CallToAction A00 = A00((C35961s6) it.next());
                if (A00 != null) {
                    builder.add((Object) A00);
                }
            }
        }
        return builder.build();
    }
}
