package X;

import com.facebook.messaging.model.folders.FolderCounts;
import java.util.concurrent.ExecutorService;

/* renamed from: X.16v  reason: invalid class name and case insensitive filesystem */
public final class C191116v extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$16";
    public final /* synthetic */ FolderCounts A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C191116v(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, FolderCounts folderCounts) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = folderCounts;
    }
}
