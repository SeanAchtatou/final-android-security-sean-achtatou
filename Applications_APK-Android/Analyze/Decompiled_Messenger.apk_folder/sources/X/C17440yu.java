package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0yu  reason: invalid class name and case insensitive filesystem */
public final class C17440yu {
    private static volatile C17440yu A04;
    private final AnonymousClass0US A00;
    public volatile boolean A01;
    public volatile boolean A02;
    private volatile Boolean A03;

    public static final C17440yu A00(AnonymousClass1XY r5) {
        if (A04 == null) {
            synchronized (C17440yu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r5);
                if (A002 != null) {
                    try {
                        A04 = new C17440yu(AnonymousClass0UQ.A00(AnonymousClass1Y3.B3a, r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public boolean A01() {
        if (this.A03 == null) {
            this.A03 = Boolean.valueOf(ECX.$const$string(50).equals(((C001300x) this.A00.get()).A04));
        }
        if (this.A03.booleanValue()) {
            return true;
        }
        return false;
    }

    private C17440yu(AnonymousClass0US r1) {
        this.A00 = r1;
    }
}
