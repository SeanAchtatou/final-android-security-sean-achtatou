package X;

/* renamed from: X.1bK  reason: invalid class name and case insensitive filesystem */
public final class C26361bK {
    public static final AnonymousClass0f7 A06 = C08240eu.A00;
    private static final C26391bN A07 = new C26391bN("uninitialized");
    private static final C26381bM A08 = new C26371bL();
    public C26401bO A00 = A07;
    public C26381bM A01 = A08;
    public boolean A02 = false;
    public final AnonymousClass0f6 A03 = new C26411bP(this);
    public final AnonymousClass0f6 A04 = new C26421bQ(this);
    public volatile boolean A05 = false;

    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01bb, code lost:
        if (r0 == false) goto L_0x01ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        r12 = java.lang.Class.forName("com.android.internal.R$array").getDeclaredFields();
        r8 = r12.length;
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x021a, code lost:
        if (r7 >= r8) goto L_0x0249;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x021c, code lost:
        r1 = r12[r7];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0228, code lost:
        if (r1.getName().endsWith("boost_param_value") == false) goto L_0x0244;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x022a, code lost:
        r1 = r15.getResources().getIntArray(r1.getInt(null));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0237, code lost:
        if (r1 == null) goto L_0x0244;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x023a, code lost:
        if (r1.length == 0) goto L_0x0244;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x023c, code lost:
        r7 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0241, code lost:
        if (r1[0] < 1073741824) goto L_0x024a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0244, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0247, code lost:
        r7 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x0321, code lost:
        if (r14.A00 != r2) goto L_0x0323;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x017f A[Catch:{ Error | Exception -> 0x00ac, all -> 0x0329 }] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01cc A[Catch:{ Error | Exception -> 0x00ac, all -> 0x0329 }] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0277 A[SYNTHETIC, Splitter:B:172:0x0277] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x027d A[Catch:{ Error | Exception -> 0x00ac, all -> 0x0329 }] */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x02ed A[Catch:{ Error | Exception -> 0x00ac, all -> 0x0329 }] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x0302 A[SYNTHETIC, Splitter:B:210:0x0302] */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0175 A[Catch:{ Error | Exception -> 0x00ac, all -> 0x0329 }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x017e A[Catch:{ Error | Exception -> 0x00ac, all -> 0x0329 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A00(X.C26361bK r14, android.content.Context r15) {
        /*
            monitor-enter(r14)
            boolean r0 = r14.A05     // Catch:{ all -> 0x033e }
            if (r0 != 0) goto L_0x033c
            r4 = 0
            r3 = 1
            boolean r0 = X.C26281bC.A01()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x001f
            X.1bN r2 = X.C26361bK.A07     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "simulator"
            r2.A00 = r0     // Catch:{ all -> 0x0329 }
            X.1bM r1 = r14.A01     // Catch:{ all -> 0x033e }
            X.1bM r0 = X.C26361bK.A08     // Catch:{ all -> 0x033e }
            if (r1 == r0) goto L_0x0324
            X.1bO r0 = r14.A00     // Catch:{ all -> 0x033e }
            if (r0 == r2) goto L_0x0324
            goto L_0x0323
        L_0x001f:
            X.1bC r1 = X.C26281bC.A00()     // Catch:{ all -> 0x0329 }
            java.lang.String r5 = r1.A00     // Catch:{ all -> 0x0329 }
            X.1bF r0 = r1.A06     // Catch:{ all -> 0x0329 }
            boolean r0 = r0.A00()     // Catch:{ all -> 0x0329 }
            if (r0 != 0) goto L_0x0043
            X.1bN r2 = X.C26361bK.A07     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "invalid_cpu_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r5)     // Catch:{ all -> 0x0329 }
            r2.A00 = r0     // Catch:{ all -> 0x0329 }
            X.1bM r1 = r14.A01     // Catch:{ all -> 0x033e }
            X.1bM r0 = X.C26361bK.A08     // Catch:{ all -> 0x033e }
            if (r1 == r0) goto L_0x0324
            X.1bO r0 = r14.A00     // Catch:{ all -> 0x033e }
            if (r0 == r2) goto L_0x0324
            goto L_0x0323
        L_0x0043:
            boolean r0 = r1.A02     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x005d
            X.1bN r2 = X.C26361bK.A07     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "google_device_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r5)     // Catch:{ all -> 0x0329 }
            r2.A00 = r0     // Catch:{ all -> 0x0329 }
            X.1bM r1 = r14.A01     // Catch:{ all -> 0x033e }
            X.1bM r0 = X.C26361bK.A08     // Catch:{ all -> 0x033e }
            if (r1 == r0) goto L_0x0324
            X.1bO r0 = r14.A00     // Catch:{ all -> 0x033e }
            if (r0 == r2) goto L_0x0324
            goto L_0x0323
        L_0x005d:
            X.1bC r6 = X.C26281bC.A00()     // Catch:{ all -> 0x0329 }
            java.lang.String[] r9 = X.C26491bX.A00     // Catch:{ all -> 0x0329 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0329 }
            r0 = 28
            if (r1 < r0) goto L_0x00b4
            java.lang.Class<java.lang.Class> r8 = java.lang.Class.class
            java.lang.String r1 = "forName"
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            java.lang.Class[] r0 = new java.lang.Class[]{r7}     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.reflect.Method r2 = r8.getDeclaredMethod(r1, r0)     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.String r1 = "getDeclaredMethod"
            java.lang.Class<java.lang.Class[]> r0 = java.lang.Class[].class
            java.lang.Class[] r0 = new java.lang.Class[]{r7, r0}     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r0)     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.String r0 = "com.android.internal.os.ZygoteInit"
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ Error | Exception -> 0x00ac }
            r7 = 0
            java.lang.Object r2 = r2.invoke(r7, r0)     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.Class r2 = (java.lang.Class) r2     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.String r1 = "setApiBlacklistExemptions"
            java.lang.Class<java.lang.String[]> r0 = java.lang.String[].class
            java.lang.Class[] r0 = new java.lang.Class[]{r0}     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0}     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.Object r1 = r8.invoke(r2, r0)     // Catch:{ Error | Exception -> 0x00ac }
            java.lang.reflect.Method r1 = (java.lang.reflect.Method) r1     // Catch:{ Error | Exception -> 0x00ac }
            if (r1 == 0) goto L_0x00b4
            java.lang.Object[] r0 = new java.lang.Object[]{r9}     // Catch:{ Error | Exception -> 0x00ac }
            r1.invoke(r7, r0)     // Catch:{ Error | Exception -> 0x00ac }
            goto L_0x00b4
        L_0x00ac:
            r2 = move-exception
            java.lang.String r1 = "ApiBlacklistExemption"
            java.lang.String r0 = "Enable api exemption failed:"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ all -> 0x0329 }
        L_0x00b4:
            java.lang.String r1 = r6.A01     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "qualcomm"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x00fa
            java.lang.String r1 = android.os.Build.BRAND     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "lge"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x00d4
            android.content.Context r0 = r15.getApplicationContext()     // Catch:{ all -> 0x0329 }
            X.1bM r2 = X.C34641pw.A02(r0)     // Catch:{ all -> 0x0329 }
            if (r2 == 0) goto L_0x00d4
            goto L_0x015f
        L_0x00d4:
            boolean r0 = X.C34611pt.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x00e4
            X.1pt r2 = new X.1pt     // Catch:{ all -> 0x0329 }
            android.content.Context r0 = r15.getApplicationContext()     // Catch:{ all -> 0x0329 }
            r2.<init>(r0)     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x00e4:
            android.content.Context r0 = r15.getApplicationContext()     // Catch:{ all -> 0x0329 }
            X.1bM r2 = X.C34641pw.A02(r0)     // Catch:{ all -> 0x0329 }
            if (r2 != 0) goto L_0x015f
            boolean r0 = X.C34661py.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x015e
            X.1py r2 = new X.1py     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x00fa:
            java.lang.String r0 = "samsung"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x011a
            boolean r0 = X.C35891rz.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x010e
            X.1rz r2 = new X.1rz     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x010e:
            boolean r0 = X.C35921s2.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x015e
            X.1s2 r2 = new X.1s2     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x011a:
            java.lang.String r0 = "mediatek"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x013a
            boolean r0 = X.C46602Qr.A01()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x012e
            X.2Qr r2 = new X.2Qr     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x012e:
            boolean r0 = X.C55682oU.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x015e
            X.2oU r2 = new X.2oU     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x013a:
            java.lang.String r0 = "hisilicon"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0329 }
            if (r0 != 0) goto L_0x0146
            boolean r0 = r6.A03     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x015e
        L_0x0146:
            boolean r0 = X.C37711w7.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x0152
            X.1w7 r2 = new X.1w7     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x0152:
            boolean r0 = X.C52982k0.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x015e
            X.2k0 r2 = new X.2k0     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x015f
        L_0x015e:
            r2 = 0
        L_0x015f:
            if (r2 != 0) goto L_0x017a
            java.lang.String r1 = r6.A01     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "samsung"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x0329 }
            if (r0 != 0) goto L_0x017a
            boolean r0 = r6.A05     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x017a
            boolean r0 = X.C35891rz.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x017f
            X.1rz r2 = new X.1rz     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
        L_0x017a:
            boolean r0 = r6.A04     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x01ca
            goto L_0x018d
        L_0x017f:
            boolean r0 = X.C35921s2.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x018b
            X.1s2 r2 = new X.1s2     // Catch:{ all -> 0x0329 }
            r2.<init>()     // Catch:{ all -> 0x0329 }
            goto L_0x017a
        L_0x018b:
            r2 = 0
            goto L_0x017a
        L_0x018d:
            if (r2 == 0) goto L_0x01bd
            if (r0 == 0) goto L_0x01b8
            r6 = 7
            java.lang.String r7 = "msmnile"
            java.lang.String r8 = "sdm845"
            java.lang.String r9 = "msm8998"
            java.lang.String r10 = "msm8996e"
            java.lang.String r11 = "msm8996"
            java.lang.String r12 = "msm8994"
            java.lang.String r13 = "msm8992"
            java.lang.String[] r8 = new java.lang.String[]{r7, r8, r9, r10, r11, r12, r13}     // Catch:{ all -> 0x0329 }
            X.1bC r0 = X.C26281bC.A00()     // Catch:{ all -> 0x0329 }
            java.lang.String r7 = r0.A00     // Catch:{ all -> 0x0329 }
            r1 = 0
        L_0x01ab:
            if (r1 >= r6) goto L_0x01ba
            r0 = r8[r1]     // Catch:{ all -> 0x0329 }
            boolean r0 = r0.equals(r7)     // Catch:{ all -> 0x0329 }
            if (r0 != 0) goto L_0x01b8
            int r1 = r1 + 1
            goto L_0x01ab
        L_0x01b8:
            r0 = 0
            goto L_0x01bb
        L_0x01ba:
            r0 = 1
        L_0x01bb:
            if (r0 == 0) goto L_0x01ca
        L_0x01bd:
            boolean r0 = X.C49402cA.A00()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x01c9
            X.2cA r2 = new X.2cA     // Catch:{ all -> 0x0329 }
            r2.<init>(r15)     // Catch:{ all -> 0x0329 }
            goto L_0x01ca
        L_0x01c9:
            r2 = 0
        L_0x01ca:
            if (r2 == 0) goto L_0x030f
            int r0 = r2.AyN()     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x030f
            X.1bC r0 = X.C26281bC.A00()     // Catch:{ all -> 0x0329 }
            java.lang.String r10 = r0.A00     // Catch:{ all -> 0x0329 }
            X.1bF r6 = r0.A06     // Catch:{ all -> 0x0329 }
            int r1 = r2.AyO()     // Catch:{ all -> 0x0329 }
            r0 = 1
            if (r1 != r0) goto L_0x02b4
            java.lang.String[] r8 = X.C34671pz.A00     // Catch:{ all -> 0x0329 }
            int r7 = r8.length     // Catch:{ all -> 0x0329 }
            r1 = 0
        L_0x01e5:
            if (r1 >= r7) goto L_0x01f5
            r0 = r8[r1]     // Catch:{ all -> 0x0329 }
            boolean r0 = r10.startsWith(r0)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x01f0
            goto L_0x01f3
        L_0x01f0:
            int r1 = r1 + 1
            goto L_0x01e5
        L_0x01f3:
            r11 = 1
            goto L_0x01f6
        L_0x01f5:
            r11 = 0
        L_0x01f6:
            java.lang.String[] r8 = X.C34671pz.A03     // Catch:{ all -> 0x0329 }
            int r7 = r8.length     // Catch:{ all -> 0x0329 }
            r1 = 0
        L_0x01fa:
            if (r1 >= r7) goto L_0x020e
            r0 = r8[r1]     // Catch:{ all -> 0x0329 }
            boolean r0 = r10.startsWith(r0)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x020b
            X.1oc r7 = new X.1oc     // Catch:{ all -> 0x0329 }
            r7.<init>(r6, r4, r11)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x020b:
            int r1 = r1 + 1
            goto L_0x01fa
        L_0x020e:
            java.lang.String r0 = "com.android.internal.R$array"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0249 }
            java.lang.reflect.Field[] r12 = r0.getDeclaredFields()     // Catch:{ Exception -> 0x0249 }
            int r8 = r12.length     // Catch:{ Exception -> 0x0249 }
            r7 = 0
        L_0x021a:
            if (r7 >= r8) goto L_0x0249
            r1 = r12[r7]     // Catch:{ Exception -> 0x0249 }
            java.lang.String r13 = r1.getName()     // Catch:{ Exception -> 0x0249 }
            java.lang.String r0 = "boost_param_value"
            boolean r0 = r13.endsWith(r0)     // Catch:{ Exception -> 0x0249 }
            if (r0 == 0) goto L_0x0244
            r0 = 0
            int r1 = r1.getInt(r0)     // Catch:{ Exception -> 0x0249 }
            android.content.res.Resources r0 = r15.getResources()     // Catch:{ Exception -> 0x0249 }
            int[] r1 = r0.getIntArray(r1)     // Catch:{ Exception -> 0x0249 }
            if (r1 == 0) goto L_0x0244
            int r0 = r1.length     // Catch:{ Exception -> 0x0249 }
            if (r0 == 0) goto L_0x0244
            r1 = r1[r4]     // Catch:{ Exception -> 0x0249 }
            r0 = 1073741824(0x40000000, float:2.0)
            r7 = 2
            if (r1 < r0) goto L_0x024a
            goto L_0x0247
        L_0x0244:
            int r7 = r7 + 1
            goto L_0x021a
        L_0x0247:
            r7 = 3
            goto L_0x024a
        L_0x0249:
            r7 = -1
        L_0x024a:
            r0 = -1
            if (r7 != r0) goto L_0x0274
            java.lang.String r0 = "com.android.internal.R$integer"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x026f }
            java.lang.reflect.Field[] r12 = r0.getDeclaredFields()     // Catch:{ Exception -> 0x026f }
            int r8 = r12.length     // Catch:{ Exception -> 0x026f }
            r7 = 0
        L_0x0259:
            if (r7 >= r8) goto L_0x026f
            r0 = r12[r7]     // Catch:{ Exception -> 0x026f }
            java.lang.String r1 = r0.getName()     // Catch:{ Exception -> 0x026f }
            java.lang.String r0 = "boost_param"
            boolean r0 = r1.endsWith(r0)     // Catch:{ Exception -> 0x026f }
            if (r0 == 0) goto L_0x026a
            goto L_0x026d
        L_0x026a:
            int r7 = r7 + 1
            goto L_0x0259
        L_0x026d:
            r0 = 1
            goto L_0x0270
        L_0x026f:
            r0 = 0
        L_0x0270:
            r7 = -1
            if (r0 == 0) goto L_0x0274
            r7 = 2
        L_0x0274:
            r0 = 3
            if (r7 != r0) goto L_0x027d
            X.1oc r7 = new X.1oc     // Catch:{ all -> 0x0329 }
            r7.<init>(r6, r3, r11)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x027d:
            r0 = 2
            if (r7 != r0) goto L_0x0286
            X.1od r7 = new X.1od     // Catch:{ all -> 0x0329 }
            r7.<init>(r6, r3)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x0286:
            java.lang.String[] r8 = X.C34671pz.A02     // Catch:{ all -> 0x0329 }
            int r7 = r8.length     // Catch:{ all -> 0x0329 }
            r1 = 0
        L_0x028a:
            if (r1 >= r7) goto L_0x029d
            r0 = r8[r1]     // Catch:{ all -> 0x0329 }
            boolean r0 = r10.startsWith(r0)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x029a
            X.1oc r7 = new X.1oc     // Catch:{ all -> 0x0329 }
            r7.<init>(r6, r4, r11)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x029a:
            int r1 = r1 + 1
            goto L_0x028a
        L_0x029d:
            java.lang.String[] r8 = X.C34671pz.A01     // Catch:{ all -> 0x0329 }
            int r7 = r8.length     // Catch:{ all -> 0x0329 }
            r1 = 0
        L_0x02a1:
            if (r1 >= r7) goto L_0x02ea
            r0 = r8[r1]     // Catch:{ all -> 0x0329 }
            boolean r0 = r10.startsWith(r0)     // Catch:{ all -> 0x0329 }
            if (r0 == 0) goto L_0x02b1
            X.1od r7 = new X.1od     // Catch:{ all -> 0x0329 }
            r7.<init>(r6, r4)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x02b1:
            int r1 = r1 + 1
            goto L_0x02a1
        L_0x02b4:
            int r1 = r2.AyO()     // Catch:{ all -> 0x0329 }
            r0 = 2
            if (r1 != r0) goto L_0x02c1
            X.1vw r7 = new X.1vw     // Catch:{ all -> 0x0329 }
            r7.<init>(r6)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x02c1:
            int r1 = r2.AyO()     // Catch:{ all -> 0x0329 }
            r0 = 4
            if (r1 != r0) goto L_0x02ce
            X.1vx r7 = new X.1vx     // Catch:{ all -> 0x0329 }
            r7.<init>(r6)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x02ce:
            int r1 = r2.AyO()     // Catch:{ all -> 0x0329 }
            r0 = 8
            if (r1 != r0) goto L_0x02dc
            X.1vw r7 = new X.1vw     // Catch:{ all -> 0x0329 }
            r7.<init>(r6)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x02dc:
            int r1 = r2.AyO()     // Catch:{ all -> 0x0329 }
            r0 = 16
            if (r1 != r0) goto L_0x02ea
            X.1vw r7 = new X.1vw     // Catch:{ all -> 0x0329 }
            r7.<init>(r6)     // Catch:{ all -> 0x0329 }
            goto L_0x02eb
        L_0x02ea:
            r7 = 0
        L_0x02eb:
            if (r7 != 0) goto L_0x0302
            X.1bN r2 = X.C26361bK.A07     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "no_suitable_model_for_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r5)     // Catch:{ all -> 0x0329 }
            r2.A00 = r0     // Catch:{ all -> 0x0329 }
            X.1bM r1 = r14.A01     // Catch:{ all -> 0x033e }
            X.1bM r0 = X.C26361bK.A08     // Catch:{ all -> 0x033e }
            if (r1 == r0) goto L_0x0324
            X.1bO r0 = r14.A00     // Catch:{ all -> 0x033e }
            if (r0 == r2) goto L_0x0324
            goto L_0x0323
        L_0x0302:
            r14.A01 = r2     // Catch:{ all -> 0x0329 }
            r14.A00 = r7     // Catch:{ all -> 0x0329 }
            X.1bM r0 = X.C26361bK.A08     // Catch:{ all -> 0x033e }
            if (r2 == r0) goto L_0x0324
            X.1bN r0 = X.C26361bK.A07     // Catch:{ all -> 0x033e }
            if (r7 == r0) goto L_0x0324
            goto L_0x0323
        L_0x030f:
            X.1bN r2 = X.C26361bK.A07     // Catch:{ all -> 0x0329 }
            java.lang.String r0 = "unknown_platform_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r5)     // Catch:{ all -> 0x0329 }
            r2.A00 = r0     // Catch:{ all -> 0x0329 }
            X.1bM r1 = r14.A01     // Catch:{ all -> 0x033e }
            X.1bM r0 = X.C26361bK.A08     // Catch:{ all -> 0x033e }
            if (r1 == r0) goto L_0x0324
            X.1bO r0 = r14.A00     // Catch:{ all -> 0x033e }
            if (r0 == r2) goto L_0x0324
        L_0x0323:
            r4 = 1
        L_0x0324:
            r14.A02 = r4     // Catch:{ all -> 0x033e }
            r14.A05 = r3     // Catch:{ all -> 0x033e }
            goto L_0x033c
        L_0x0329:
            r2 = move-exception
            X.1bM r1 = r14.A01     // Catch:{ all -> 0x033e }
            X.1bM r0 = X.C26361bK.A08     // Catch:{ all -> 0x033e }
            if (r1 == r0) goto L_0x0337
            X.1bO r1 = r14.A00     // Catch:{ all -> 0x033e }
            X.1bN r0 = X.C26361bK.A07     // Catch:{ all -> 0x033e }
            if (r1 == r0) goto L_0x0337
            r4 = 1
        L_0x0337:
            r14.A02 = r4     // Catch:{ all -> 0x033e }
            r14.A05 = r3     // Catch:{ all -> 0x033e }
            throw r2     // Catch:{ all -> 0x033e }
        L_0x033c:
            monitor-exit(r14)
            return
        L_0x033e:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26361bK.A00(X.1bK, android.content.Context):void");
    }
}
