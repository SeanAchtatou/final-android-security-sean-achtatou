package X;

import android.app.Activity;
import java.lang.ref.WeakReference;
import java.util.Comparator;

/* renamed from: X.0tp  reason: invalid class name and case insensitive filesystem */
public final class C14690tp {
    public static final Comparator A02 = new C14530tW();
    public long A00 = Long.MIN_VALUE;
    private final WeakReference A01;

    public Activity A00() {
        return (Activity) this.A01.get();
    }

    public C14690tp(Activity activity) {
        this.A01 = new WeakReference(activity);
    }
}
