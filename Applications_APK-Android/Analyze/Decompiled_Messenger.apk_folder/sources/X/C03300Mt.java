package X;

import java.util.zip.ZipEntry;

/* renamed from: X.0Mt  reason: invalid class name and case insensitive filesystem */
public final class C03300Mt {
    public final String A00;
    public final ZipEntry A01;
    public final boolean A02;

    public C03300Mt(ZipEntry zipEntry, String str, boolean z) {
        this.A01 = zipEntry;
        this.A00 = str;
        this.A02 = z;
    }
}
