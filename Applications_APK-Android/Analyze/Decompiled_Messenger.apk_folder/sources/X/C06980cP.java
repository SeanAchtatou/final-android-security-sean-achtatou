package X;

import android.net.Uri;
import com.facebook.messaging.model.threadkey.ThreadKey;

/* renamed from: X.0cP  reason: invalid class name and case insensitive filesystem */
public final class C06980cP {
    public static final Uri A00;
    public static final Uri A01;
    public static final Uri A02;
    public static final Uri A03;
    public static final Uri A04;
    public static final Uri A05;
    public static final Uri A06;
    public static final Uri A07;
    public static final Uri A08;
    public static final Uri A09;

    static {
        Uri parse = Uri.parse("peer://msg_notification_chathead");
        A03 = parse;
        A02 = Uri.withAppendedPath(parse, "clear");
        Uri uri = A03;
        A01 = Uri.withAppendedPath(uri, "clear_active_threads");
        A00 = Uri.withAppendedPath(uri, "hidden");
        Uri parse2 = Uri.parse("peer://msg_notification_user_interaction");
        A09 = parse2;
        A08 = Uri.withAppendedPath(parse2, "is_user_in_app");
        Uri parse3 = Uri.parse("peer://msg_extension_status");
        A04 = parse3;
        A05 = Uri.withAppendedPath(parse3, C22298Ase.$const$string(76));
        Uri parse4 = Uri.parse("peer://msg_notification_unread_count");
        A07 = parse4;
        A06 = Uri.withAppendedPath(parse4, "clear_all");
    }

    public static final Uri A00(ThreadKey threadKey) {
        return Uri.parse(AnonymousClass08S.A0J("peer://msg_notification_chathead/active_threads/", Uri.encode(threadKey.toString())));
    }

    public static final Uri A01(ThreadKey threadKey) {
        return Uri.parse(AnonymousClass08S.A0J("peer://msg_notification_unread_count/thread/", Uri.encode(threadKey.toString())));
    }
}
