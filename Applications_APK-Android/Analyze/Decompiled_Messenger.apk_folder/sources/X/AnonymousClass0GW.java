package X;

import android.content.Context;

/* renamed from: X.0GW  reason: invalid class name */
public final class AnonymousClass0GW implements AnonymousClass1YQ, C06670bt {
    private final Context A00;
    private final C25051Yd A01;

    public String getSimpleName() {
        return "LyraFlagsController";
    }

    public static final AnonymousClass0GW A00(AnonymousClass1XY r1) {
        return new AnonymousClass0GW(r1);
    }

    public static final AnonymousClass0US A01(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.B0x, r1);
    }

    private void A02() {
        C25051Yd r2 = this.A01;
        AnonymousClass0XE r3 = AnonymousClass0XE.A05;
        AnonymousClass08X.A04(this.A00, "android_crash_lyra_hook_cxa_throw", r2.Aer(282179351348395L, r3) ? 1 : 0);
        AnonymousClass08X.A04(this.A00, "android_crash_lyra_enable_backtraces", this.A01.Aer(282179351413932L, r3) ? 1 : 0);
    }

    private AnonymousClass0GW(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0WT.A00(r2);
        this.A00 = AnonymousClass1YA.A00(r2);
    }

    public int Ai5() {
        return 164;
    }

    public void init() {
        int A03 = C000700l.A03(-1591719597);
        A02();
        C000700l.A09(546574402, A03);
    }

    public void BTE(int i) {
        A02();
    }
}
