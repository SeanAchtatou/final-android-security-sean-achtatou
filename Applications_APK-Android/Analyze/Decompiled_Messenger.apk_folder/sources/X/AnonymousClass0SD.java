package X;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.concurrent.Callable;

/* renamed from: X.0SD  reason: invalid class name */
public final class AnonymousClass0SD implements Callable {
    public final /* synthetic */ AnonymousClass0BE A00;
    public final /* synthetic */ String A01;

    public AnonymousClass0SD(AnonymousClass0BE r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    public Object call() {
        int i;
        String str = this.A01;
        try {
            AnonymousClass0Ry r7 = new AnonymousClass0Ry(str, Arrays.asList(InetAddress.getAllByName(str)), 0, 0);
            AnonymousClass0BE r6 = this.A00;
            synchronized (r6) {
                synchronized (r6) {
                    TreeSet A012 = r6.A00.A01();
                    if (A012.isEmpty()) {
                        i = 0;
                    } else {
                        i = ((AnonymousClass0Ry) A012.first()).A01 + 1;
                    }
                    AnonymousClass0Ry A002 = r6.A00.A00(r7);
                    if (A002 == null) {
                        r6.A00.A03(new AnonymousClass0Ry(r7.A02, r7.A00(), i, 0));
                    } else {
                        r6.A00.A04(A002, new AnonymousClass0Ry(r7.A02, r7.A00(), i, A002.A00));
                    }
                }
                r6.A00.A02();
            }
            return r7;
        } catch (UnknownHostException unused) {
            throw new AnonymousClass0SV(AnonymousClass0DA.A04);
        } catch (SecurityException unused2) {
            throw new AnonymousClass0SV(AnonymousClass0DA.A02);
        }
    }
}
