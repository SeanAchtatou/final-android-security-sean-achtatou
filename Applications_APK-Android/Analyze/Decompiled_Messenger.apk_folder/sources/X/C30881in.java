package X;

/* renamed from: X.1in  reason: invalid class name and case insensitive filesystem */
public final class C30881in implements C23311Pa {
    private final C23311Pa A00;
    private final C30871im A01;

    public AnonymousClass1PS AR1(Object obj, AnonymousClass1PS r3) {
        this.A01.BQT(obj);
        return this.A00.AR1(obj, r3);
    }

    public boolean AU9(AnonymousClass8SZ r2) {
        return this.A00.AU9(r2);
    }

    public AnonymousClass1PS Ab8(Object obj) {
        AnonymousClass1PS Ab8 = this.A00.Ab8(obj);
        C30871im r0 = this.A01;
        if (Ab8 == null) {
            r0.BQS(obj);
            return Ab8;
        }
        r0.BQR(obj);
        return Ab8;
    }

    public int C1M(AnonymousClass8SZ r2) {
        return this.A00.C1M(r2);
    }

    public boolean contains(Object obj) {
        return this.A00.contains(obj);
    }

    public int getCount() {
        return this.A00.getCount();
    }

    public int getSizeInBytes() {
        return this.A00.getSizeInBytes();
    }

    public C30881in(C23311Pa r1, C30871im r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
