package X;

import com.fasterxml.jackson.databind.JsonNode;

/* renamed from: X.0tg  reason: invalid class name and case insensitive filesystem */
public abstract class C14610tg extends C185113v {
    public abstract C182811d asToken();

    public JsonNode deepCopy() {
        return this;
    }

    public final JsonNode findValue(String str) {
        return null;
    }

    public final JsonNode get(int i) {
        return null;
    }

    public final JsonNode get(String str) {
        return null;
    }

    public final boolean has(String str) {
        return false;
    }

    public final boolean hasNonNull(String str) {
        return false;
    }

    public void serializeWithType(C11710np r1, C11260mU r2, CY1 cy1) {
        cy1.writeTypePrefixForScalar(this, r1);
        serialize(r1, r2);
        cy1.writeTypeSuffixForScalar(this, r1);
    }

    public String toString() {
        return asText();
    }

    public final JsonNode path(String str) {
        return C29651gl.instance;
    }
}
