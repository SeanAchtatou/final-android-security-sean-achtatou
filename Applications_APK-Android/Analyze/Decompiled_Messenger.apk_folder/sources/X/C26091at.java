package X;

/* renamed from: X.1at  reason: invalid class name and case insensitive filesystem */
public final class C26091at implements C26031an, C25981ai {
    private final int A00;
    private final int A01;
    private final int A02;
    private final C26031an A03;

    public int B8E(C04270Tg r3) {
        if (this.A03.B8E(r3) < this.A02) {
            return this.A01;
        }
        return this.A00;
    }

    public String getName() {
        return AnonymousClass08S.A0L(this.A03.getName(), ".if_less_than_", this.A02);
    }

    public C26091at(C26031an r1, int i, int i2, int i3) {
        this.A03 = r1;
        this.A02 = i;
        this.A01 = i2;
        this.A00 = i3;
    }

    public String AkW(C04270Tg r2) {
        return String.valueOf(B8E(r2));
    }
}
