package X;

import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Xm  reason: invalid class name */
public interface AnonymousClass1Xm {
    void AOu(C22916BKm bKm);

    void AOv(Class cls);

    C30187ErE APs(Class cls);

    C30183ErA APt(C22916BKm bKm);

    void AQ3(Class cls);

    C30360Eul AQ4(Class cls);

    C30187ErE AQ6(Class cls);

    C30183ErA AQ7(C22916BKm bKm);

    C38551xc AQA(C22916BKm bKm);

    C38551xc AQB(Class cls);

    C38551xc AQC(Class cls, Class cls2);

    void AQE(Class cls, C24891Xn r2);

    void AWd(C22916BKm bKm);

    void AWe(Class cls);

    void AWf(Class cls, Class cls2);

    List AeT();

    List Aho();

    AnonymousClass0UH Aq2();

    Set Av9();

    Map AvA();

    List B12();

    Map B1y();

    void C3N(Class cls);
}
