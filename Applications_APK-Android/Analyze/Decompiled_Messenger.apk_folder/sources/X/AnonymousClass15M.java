package X;

import java.util.Map;

/* renamed from: X.15M  reason: invalid class name */
public final class AnonymousClass15M {
    public final String A00;
    public final String A01;
    public final Map A02;
    public final /* synthetic */ AnonymousClass14J A03;

    public AnonymousClass15M(AnonymousClass14J r1, String str, String str2, Map map) {
        this.A03 = r1;
        this.A01 = str;
        this.A00 = str2;
        this.A02 = map;
    }
}
