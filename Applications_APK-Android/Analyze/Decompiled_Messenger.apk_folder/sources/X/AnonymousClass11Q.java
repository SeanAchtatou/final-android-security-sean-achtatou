package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.11Q  reason: invalid class name */
public final class AnonymousClass11Q extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C32591m0 A01;
    private static volatile C15390vD A02;

    public static final C32591m0 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C32591m0.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C32591m0(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C15390vD A02(AnonymousClass1XY r8) {
        if (A02 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        C33371nT r5 = new C33371nT(applicationInjector);
                        C32591m0 A003 = A00(applicationInjector);
                        new AnonymousClass11U(applicationInjector);
                        A02 = new AnonymousClass12O(new AnonymousClass12P(r5, new AnonymousClass12M(A003.A01()), C99084oO.$const$string(51)), AnonymousClass11W.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final C15390vD A01(AnonymousClass1XY r0) {
        return C100424qp.A00(r0);
    }
}
