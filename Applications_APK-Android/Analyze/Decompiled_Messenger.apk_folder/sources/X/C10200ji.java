package X;

import java.io.Serializable;

/* renamed from: X.0ji  reason: invalid class name and case insensitive filesystem */
public final class C10200ji implements C10210jj, C10220jk, Serializable {
    public static final C10230jl DEFAULT_ROOT_VALUE_SEPARATOR = new C10230jl(" ");
    private static final long serialVersionUID = -5512586643324525213L;
    public C10270jp _arrayIndenter;
    public transient int _nesting;
    public C10270jp _objectIndenter;
    public final C10240jm _rootSeparator;
    public boolean _spacesInObjectEntries;

    public void beforeArrayValues(C11710np r3) {
        this._arrayIndenter.writeIndentation(r3, this._nesting);
    }

    public void beforeObjectEntries(C11710np r3) {
        this._objectIndenter.writeIndentation(r3, this._nesting);
    }

    public /* bridge */ /* synthetic */ Object createInstance() {
        return new C10200ji(this, this._rootSeparator);
    }

    public void writeArrayValueSeparator(C11710np r3) {
        r3.writeRaw(',');
        this._arrayIndenter.writeIndentation(r3, this._nesting);
    }

    public void writeEndArray(C11710np r3, int i) {
        C10270jp r1 = this._arrayIndenter;
        if (!r1.isInline()) {
            this._nesting--;
        }
        if (i > 0) {
            r1.writeIndentation(r3, this._nesting);
        } else {
            r3.writeRaw(' ');
        }
        r3.writeRaw(']');
    }

    public void writeEndObject(C11710np r3, int i) {
        C10270jp r1 = this._objectIndenter;
        if (!r1.isInline()) {
            this._nesting--;
        }
        if (i > 0) {
            r1.writeIndentation(r3, this._nesting);
        } else {
            r3.writeRaw(' ');
        }
        r3.writeRaw('}');
    }

    public void writeObjectEntrySeparator(C11710np r3) {
        r3.writeRaw(',');
        this._objectIndenter.writeIndentation(r3, this._nesting);
    }

    public void writeObjectFieldValueSeparator(C11710np r2) {
        if (this._spacesInObjectEntries) {
            r2.writeRaw(" : ");
        } else {
            r2.writeRaw(':');
        }
    }

    public void writeRootValueSeparator(C11710np r2) {
        C10240jm r0 = this._rootSeparator;
        if (r0 != null) {
            r2.writeRaw(r0);
        }
    }

    public void writeStartArray(C11710np r2) {
        if (!this._arrayIndenter.isInline()) {
            this._nesting++;
        }
        r2.writeRaw('[');
    }

    public void writeStartObject(C11710np r2) {
        r2.writeRaw('{');
        if (!this._objectIndenter.isInline()) {
            this._nesting++;
        }
    }

    public C10200ji() {
        this(DEFAULT_ROOT_VALUE_SEPARATOR);
    }

    private C10200ji(C10200ji r2, C10240jm r3) {
        this._arrayIndenter = C10250jn.instance;
        this._objectIndenter = C10280jq.instance;
        this._spacesInObjectEntries = true;
        this._nesting = 0;
        this._arrayIndenter = r2._arrayIndenter;
        this._objectIndenter = r2._objectIndenter;
        this._spacesInObjectEntries = r2._spacesInObjectEntries;
        this._nesting = r2._nesting;
        this._rootSeparator = r3;
    }

    private C10200ji(C10240jm r2) {
        this._arrayIndenter = C10250jn.instance;
        this._objectIndenter = C10280jq.instance;
        this._spacesInObjectEntries = true;
        this._nesting = 0;
        this._rootSeparator = r2;
    }
}
