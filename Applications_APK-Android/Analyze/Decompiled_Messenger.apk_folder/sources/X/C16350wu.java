package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.0wu  reason: invalid class name and case insensitive filesystem */
public final class C16350wu extends Animation {
    public final /* synthetic */ C20871Ed A00;

    public C16350wu(C20871Ed r1) {
        this.A00 = r1;
    }

    public void applyTransformation(float f, Transformation transformation) {
        int i;
        C20871Ed r3 = this.A00;
        if (0 == 0) {
            i = r3.A01 - Math.abs(r3.A0R);
        } else {
            i = r3.A01;
        }
        int i2 = r3.A04;
        this.A00.A0A((i2 + ((int) (((float) (i - i2)) * f))) - r3.A0A.getTop());
        C15160up r32 = this.A00.A0B;
        float f2 = 1.0f - f;
        C15630vb r1 = r32.A05;
        if (f2 != r1.A00) {
            r1.A00 = f2;
        }
        r32.invalidateSelf();
    }
}
