package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.profilo.ipc.TraceContext;
import io.card.payment.BuildConfig;
import java.io.File;
import java.util.Arrays;

/* renamed from: X.06Q  reason: invalid class name */
public final class AnonymousClass06Q implements AnonymousClass0Z1 {
    private AnonymousClass0US A00;

    public String Amj() {
        return "profilo_state";
    }

    public static final AnonymousClass06Q A00(AnonymousClass1XY r2) {
        return new AnonymousClass06Q(AnonymousClass06R.A01(r2));
    }

    private AnonymousClass06Q(AnonymousClass0US r1) {
        this.A00 = r1;
    }

    public String getCustomData(Throwable th) {
        Iterable<File> A05;
        String[] strArr;
        String sb;
        boolean z;
        AnonymousClass054 r0;
        C004505k A002 = C004505k.A00();
        synchronized (A002) {
            A05 = A002.A03.A05();
        }
        int i = 0;
        for (File path : A05) {
            i += path.getPath().length() + 1;
        }
        StringBuilder sb2 = new StringBuilder(i);
        for (File path2 : A05) {
            sb2.append(path2.getPath());
            sb2.append(10);
        }
        String sb3 = sb2.toString();
        if (!C004004z.A00 || (r0 = AnonymousClass054.A07) == null) {
            strArr = null;
        } else {
            strArr = r0.A0C();
        }
        if (strArr == null) {
            sb = "<none>";
        } else {
            int length = strArr[0].length();
            StringBuilder sb4 = new StringBuilder(((length * r2) + r2) - 1);
            for (String str : strArr) {
                if (sb4.length() > 0) {
                    sb4.append(",");
                }
                sb4.append(str);
            }
            sb = sb4.toString();
        }
        AnonymousClass054 r6 = AnonymousClass054.A07;
        String str2 = BuildConfig.FLAVOR;
        if (r6 != null) {
            String[] strArr2 = null;
            if (r6.A02.get() != 0) {
                String[] strArr3 = new String[2];
                int i2 = 0;
                for (int i3 = 0; i3 < 2; i3++) {
                    TraceContext traceContext = (TraceContext) r6.A04.get(i3);
                    if (traceContext != null) {
                        strArr3[i2] = AnonymousClass08S.A0U("Context: ", traceContext.toString(), " ControllerObject: ", traceContext.A08.toString(), " LongContext: ", Long.toString(traceContext.A04));
                        i2++;
                    }
                }
                if (i2 != 0) {
                    strArr2 = (String[]) Arrays.copyOf(strArr3, i2);
                }
            }
            if (strArr2 != null) {
                int length2 = strArr2[0].length();
                StringBuilder sb5 = new StringBuilder((length2 * r2) + r2);
                for (String append : strArr2) {
                    sb5.append(append);
                    sb5.append("\n");
                }
                str2 = sb5.toString();
            }
        }
        AnonymousClass06R r1 = (AnonymousClass06R) this.A00.get();
        synchronized (r1) {
            z = r1.A01;
        }
        return StringFormatUtil.formatStrLocaleSafe("Trace IDs: %s\n%sNotification visible: %s\nTrace files: %s", sb, str2, Boolean.valueOf(z), sb3);
    }
}
