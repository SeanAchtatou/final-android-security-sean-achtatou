package X;

import android.os.PowerManager;
import com.facebook.messaging.notify.DirectMessageStorySeenNotification;
import com.facebook.messaging.notify.EventReminderNotification;
import com.facebook.messaging.notify.FailedToSendMessageNotification;
import com.facebook.messaging.notify.JoinRequestNotification;
import com.facebook.messaging.notify.MessageReactionNotification;
import com.facebook.messaging.notify.MessengerLivingRoomCreateNotification;
import com.facebook.messaging.notify.MontageMessageNotification;
import com.facebook.messaging.notify.MultipleAccountsNewMessagesNotification;
import com.facebook.messaging.notify.SimpleMessageNotification;
import com.facebook.messaging.notify.StaleNotification;
import com.facebook.messaging.notify.VideoChatLinkJoinAttemptNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.16x  reason: invalid class name and case insensitive filesystem */
public abstract class C191316x implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.NotificationWakefulExecutable";
    private final PowerManager.WakeLock A00;
    private final ExecutorService A01;

    public void A00() {
        if (this instanceof AnonymousClass171) {
            AnonymousClass171 r3 = (AnonymousClass171) this;
            AnonymousClass210 r4 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r3.A01.A00);
            FailedToSendMessageNotification failedToSendMessageNotification = r3.A00;
            AnonymousClass210.A02(r4, failedToSendMessageNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r4.A00)).AOx();
            AnonymousClass210.A01(r4, failedToSendMessageNotification);
        } else if (this instanceof AnonymousClass172) {
            AnonymousClass172 r32 = (AnonymousClass172) this;
            AnonymousClass210 r42 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r32.A01.A00);
            SimpleMessageNotification simpleMessageNotification = r32.A00;
            AnonymousClass210.A02(r42, simpleMessageNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r42.A00)).AOx();
            AnonymousClass210.A01(r42, simpleMessageNotification);
        } else if (this instanceof AnonymousClass173) {
            AnonymousClass173 r33 = (AnonymousClass173) this;
            AnonymousClass210 r43 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r33.A01.A00);
            MontageMessageNotification montageMessageNotification = r33.A00;
            AnonymousClass210.A02(r43, montageMessageNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r43.A00)).AOx();
            AnonymousClass210.A01(r43, montageMessageNotification);
        } else if (this instanceof AnonymousClass174) {
            AnonymousClass174 r34 = (AnonymousClass174) this;
            AnonymousClass210 r44 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r34.A01.A00);
            MontageMessageNotification montageMessageNotification2 = r34.A00;
            AnonymousClass210.A02(r44, montageMessageNotification2);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r44.A00)).AOx();
            AnonymousClass210.A01(r44, montageMessageNotification2);
        } else if (this instanceof AnonymousClass175) {
            AnonymousClass175 r35 = (AnonymousClass175) this;
            AnonymousClass210 r45 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r35.A01.A00);
            MontageMessageNotification montageMessageNotification3 = r35.A00;
            AnonymousClass210.A02(r45, montageMessageNotification3);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r45.A00)).AOx();
            AnonymousClass210.A01(r45, montageMessageNotification3);
        } else if (this instanceof AnonymousClass176) {
            AnonymousClass176 r36 = (AnonymousClass176) this;
            AnonymousClass210 r46 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r36.A01.A00);
            MontageMessageNotification montageMessageNotification4 = r36.A00;
            AnonymousClass210.A02(r46, montageMessageNotification4);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r46.A00)).AOx();
            AnonymousClass210.A01(r46, montageMessageNotification4);
        } else if (this instanceof AnonymousClass177) {
            AnonymousClass177 r37 = (AnonymousClass177) this;
            AnonymousClass210 r47 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r37.A01.A00);
            if (r47 != null) {
                MontageMessageNotification montageMessageNotification5 = r37.A00;
                AnonymousClass210.A02(r47, montageMessageNotification5);
                ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r47.A00)).AOx();
                AnonymousClass210.A01(r47, montageMessageNotification5);
            }
        } else if (this instanceof AnonymousClass17A) {
            AnonymousClass17A r38 = (AnonymousClass17A) this;
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r38.A00.A00)).A0D(r38.A01);
        } else if (this instanceof C13780s3) {
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, ((C13780s3) this).A00.A00)).A04();
        } else if (this instanceof AnonymousClass17B) {
            AnonymousClass17B r39 = (AnonymousClass17B) this;
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r39.A00.A00)).A0F(r39.A01);
        } else if (this instanceof AnonymousClass17C) {
            AnonymousClass17C r310 = (AnonymousClass17C) this;
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r310.A00.A00)).A0E(r310.A01);
        } else if (this instanceof AnonymousClass17E) {
            AnonymousClass17E r311 = (AnonymousClass17E) this;
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r311.A01.A00)).A06(r311.A00, r311.A02);
        } else if (this instanceof AnonymousClass17F) {
            AnonymousClass17F r312 = (AnonymousClass17F) this;
            AnonymousClass210 r48 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r312.A01.A00);
            VideoChatLinkJoinAttemptNotification videoChatLinkJoinAttemptNotification = r312.A00;
            AnonymousClass210.A02(r48, videoChatLinkJoinAttemptNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r48.A00)).AOx();
            AnonymousClass210.A01(r48, videoChatLinkJoinAttemptNotification);
        } else if (this instanceof AnonymousClass17G) {
            AnonymousClass17G r313 = (AnonymousClass17G) this;
            AnonymousClass210 r49 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r313.A01.A00);
            SimpleMessageNotification simpleMessageNotification2 = r313.A00;
            AnonymousClass210.A02(r49, simpleMessageNotification2);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r49.A00)).AOx();
            AnonymousClass210.A01(r49, simpleMessageNotification2);
        } else if (this instanceof AnonymousClass17H) {
            AnonymousClass17H r314 = (AnonymousClass17H) this;
            AnonymousClass210 r410 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r314.A01.A00);
            DirectMessageStorySeenNotification directMessageStorySeenNotification = r314.A00;
            AnonymousClass210.A02(r410, directMessageStorySeenNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r410.A00)).AOx();
            AnonymousClass210.A01(r410, directMessageStorySeenNotification);
        } else if (this instanceof AnonymousClass17I) {
            AnonymousClass17I r315 = (AnonymousClass17I) this;
            AnonymousClass210.A01((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r315.A01.A00), r315.A00);
        } else if (this instanceof C30331hs) {
            C30331hs r316 = (C30331hs) this;
            AnonymousClass210 r411 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r316.A01.A00);
            MessageReactionNotification messageReactionNotification = r316.A00;
            AnonymousClass210.A02(r411, messageReactionNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r411.A00)).AOx();
            AnonymousClass210.A01(r411, messageReactionNotification);
        } else if (this instanceof C30351hu) {
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, ((C30351hu) this).A00.A00)).A05();
        } else if (this instanceof C30361hv) {
            C30361hv r317 = (C30361hv) this;
            AnonymousClass210 r412 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r317.A01.A00);
            MessengerLivingRoomCreateNotification messengerLivingRoomCreateNotification = r317.A00;
            AnonymousClass210.A02(r412, messengerLivingRoomCreateNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r412.A00)).AOx();
            AnonymousClass210.A01(r412, messengerLivingRoomCreateNotification);
        } else if (this instanceof C30381hx) {
            C30381hx r318 = (C30381hx) this;
            AnonymousClass210 r413 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r318.A01.A00);
            EventReminderNotification eventReminderNotification = r318.A00;
            AnonymousClass210.A02(r413, eventReminderNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r413.A00)).AOx();
            AnonymousClass210.A01(r413, eventReminderNotification);
        } else if (this instanceof C30391hy) {
            C30391hy r319 = (C30391hy) this;
            AnonymousClass210 r414 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r319.A01.A00);
            JoinRequestNotification joinRequestNotification = r319.A00;
            AnonymousClass210.A02(r414, joinRequestNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r414.A00)).AOx();
            AnonymousClass210.A01(r414, joinRequestNotification);
        } else if (this instanceof C30411i0) {
            C30411i0 r320 = (C30411i0) this;
            AnonymousClass210 r415 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r320.A01.A00);
            MultipleAccountsNewMessagesNotification multipleAccountsNewMessagesNotification = r320.A00;
            AnonymousClass210.A02(r415, multipleAccountsNewMessagesNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r415.A00)).AOx();
            AnonymousClass210.A01(r415, multipleAccountsNewMessagesNotification);
        } else if (this instanceof C30421i1) {
            C30421i1 r321 = (C30421i1) this;
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r321.A01.A00)).A0C(r321.A00);
        } else if (this instanceof C13790s4) {
            C13790s4 r322 = (C13790s4) this;
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r322.A01.A00)).A09(r322.A00);
        } else if (this instanceof C14230sp) {
            C14230sp r323 = (C14230sp) this;
            AnonymousClass210 r416 = (AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r323.A01.A00);
            StaleNotification staleNotification = r323.A00;
            AnonymousClass210.A02(r416, staleNotification);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, r416.A00)).AOx();
            AnonymousClass210.A01(r416, staleNotification);
        } else if (this instanceof C13800s5) {
            C13800s5 r324 = (C13800s5) this;
            ((AnonymousClass210) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOd, r324.A01.A00)).A0B(r324.A00);
        }
    }

    public void A01() {
        C009007v.A02(this.A00, 60000);
        AnonymousClass07A.A04(this.A01, this, 439644858);
    }

    public C191316x(ExecutorService executorService, AnonymousClass0US r6, String str) {
        StringBuilder sb = new StringBuilder("orca_notification");
        if (str != null) {
            sb.append(AnonymousClass08S.A0J("_", str));
        }
        PowerManager.WakeLock A002 = C009007v.A00((PowerManager) r6.get(), 1, sb.toString());
        this.A00 = A002;
        C009007v.A03(A002, false);
        this.A01 = executorService;
    }

    public void run() {
        try {
            A00();
        } finally {
            C009007v.A01(this.A00);
        }
    }
}
