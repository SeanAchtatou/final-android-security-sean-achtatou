package X;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.google.common.base.Platform;

/* renamed from: X.1vG  reason: invalid class name */
public final class AnonymousClass1vG extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C118725jb A01;
    @Comparable(type = 13)
    public String A02;
    @Comparable(type = 13)
    public String A03;
    @Comparable(type = 3)
    public boolean A04;

    public AnonymousClass1vG(Context context) {
        super("QuicksilverNavBar");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }

    public static AnonymousClass11F A00(AnonymousClass0p4 r4, String str, C97444lH r6) {
        if (Platform.stringIsNullOrEmpty(str)) {
            return null;
        }
        ComponentBuilderCBuilderShape2_0S0300000 A002 = C78223oa.A00(r4);
        C61832zZ r1 = (C61832zZ) r6.A00.get();
        r1.A0Q(C97444lH.A01);
        r1.A0S(str);
        A002.A3U(r1.A0F());
        ((C78223oa) A002.A02).A0B = new ColorDrawable(1459617792);
        A002.A2G(2132148234);
        A002.A28(2132148234);
        A002.A3T(AnonymousClass36c.A00());
        A002.A2f(AnonymousClass10G.ALL, 2132148247);
        A002.A25(2131831162);
        A002.A2y(C17780zS.A0E(AnonymousClass1vG.class, r4, 1856422461, new Object[]{r4}));
        return A002;
    }

    public static void A01(E76 e76, AnonymousClass0p4 r5) {
        ComponentBuilderCBuilderShape2_0S0300000 A002 = C113425aY.A00(r5);
        A002.A3H(2132344975);
        A002.A2y(C17780zS.A0E(AnonymousClass1vG.class, r5, -513663253, new Object[]{r5}));
        A002.A25(2131825231);
        e76.A33(A002);
    }

    public static void A02(E76 e76, AnonymousClass0p4 r5) {
        ComponentBuilderCBuilderShape2_0S0300000 A002 = C113425aY.A00(r5);
        A002.A3H(2132345001);
        A002.A2y(C17780zS.A0E(AnonymousClass1vG.class, r5, -1613216430, new Object[]{r5}));
        A002.A25(2131831192);
        e76.A33(A002);
    }
}
