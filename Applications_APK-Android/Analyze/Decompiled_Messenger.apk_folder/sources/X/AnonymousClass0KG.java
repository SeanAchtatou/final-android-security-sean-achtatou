package X;

import android.content.Context;
import android.os.health.SystemHealthManager;

/* renamed from: X.0KG  reason: invalid class name */
public final class AnonymousClass0KG extends C007907e {
    private final SystemHealthManager A00;

    public AnonymousClass0FM A03() {
        return new AnonymousClass0KI();
    }

    public boolean A04(AnonymousClass0FM r4) {
        try {
            ((AnonymousClass0KI) r4).A0A(this.A00.takeMyUidSnapshot());
            return true;
        } catch (RuntimeException e) {
            AnonymousClass0KZ.A00("HealthStatsMetricsCollector", "Unable to snapshot healthstats", e);
            return false;
        }
    }

    public AnonymousClass0KG(Context context) {
        this.A00 = (SystemHealthManager) context.getSystemService("systemhealth");
    }
}
