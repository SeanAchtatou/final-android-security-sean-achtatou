package X;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1po  reason: invalid class name and case insensitive filesystem */
public final class C34561po {
    public final SparseArray A00 = new SparseArray();
    public final C01510An A01;
    public final AtomicInteger A02 = new AtomicInteger(1);
    private final AnonymousClass069 A03;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000c, code lost:
        if (r18 == 0) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C23467Bfk A00(java.lang.String r21, byte[] r22, java.lang.Integer r23, X.AnonymousClass0CZ r24, int r25, long r26, java.lang.String r28, java.lang.Long r29) {
        /*
            r20 = this;
            r5 = r20
            if (r29 == 0) goto L_0x000e
            long r18 = r29.longValue()
            r1 = 0
            int r0 = (r18 > r1 ? 1 : (r18 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0010
        L_0x000e:
            r18 = -1
        L_0x0010:
            X.Bfk r6 = new X.Bfk
            java.util.concurrent.atomic.AtomicInteger r0 = r5.A02
            int r14 = r0.getAndIncrement()
            X.069 r0 = r5.A03
            long r16 = r0.now()
            r12 = r26
            r8 = r22
            r15 = r28
            r7 = r21
            r9 = r23
            r10 = r24
            r11 = r25
            r6.<init>(r7, r8, r9, r10, r11, r12, r14, r15, r16, r18)
            android.util.SparseArray r2 = r5.A00
            monitor-enter(r2)
            android.util.SparseArray r1 = r5.A00     // Catch:{ all -> 0x0059 }
            int r0 = r6.A01     // Catch:{ all -> 0x0059 }
            r1.put(r0, r6)     // Catch:{ all -> 0x0059 }
            monitor-exit(r2)     // Catch:{ all -> 0x0059 }
            X.0An r4 = r5.A01
            X.Bfj r3 = new X.Bfj
            r3.<init>(r5, r6)
            int r0 = r6.A02
            long r1 = (long) r0
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            X.0Cf r2 = r4.C4X(r3, r1, r0)
            X.AnonymousClass0A1.A00(r2)
            X.0Cg r1 = r6.A0B
            r0 = 0
            if (r1 != 0) goto L_0x0053
            r0 = 1
        L_0x0053:
            X.AnonymousClass0A1.A02(r0)
            r6.A0B = r2
            return r6
        L_0x0059:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0059 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34561po.A00(java.lang.String, byte[], java.lang.Integer, X.0CZ, int, long, java.lang.String, java.lang.Long):X.Bfk");
    }

    public Collection A01() {
        ArrayList arrayList;
        synchronized (this.A00) {
            arrayList = new ArrayList(this.A00.size());
            for (int i = 0; i < this.A00.size(); i++) {
                C23467Bfk bfk = (C23467Bfk) this.A00.valueAt(i);
                long now = this.A03.now();
                long j = bfk.A00;
                boolean z = false;
                if (j >= 0 && bfk.A03 + j < now) {
                    z = true;
                }
                if (!z) {
                    arrayList.add(bfk);
                }
            }
        }
        return arrayList;
    }

    public void A02(int i) {
        C23467Bfk bfk;
        synchronized (this.A00) {
            bfk = (C23467Bfk) this.A00.get(i);
            if (bfk != null) {
                this.A00.remove(i);
            }
        }
        if (bfk != null && bfk.A0B != null) {
            bfk.A0B.cancel(false);
            bfk.A0B = null;
        }
    }

    public C34561po(C01510An r3, AnonymousClass069 r4) {
        this.A01 = r3;
        this.A03 = r4;
    }
}
