package X;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Executor;

/* renamed from: X.1PQ  reason: invalid class name */
public final class AnonymousClass1PQ implements AnonymousClass1PR {
    private final Deque A00 = new ArrayDeque();
    private final Executor A01;

    public synchronized void ANo(Runnable runnable) {
        AnonymousClass07A.A04(this.A01, runnable, 1984427032);
    }

    public synchronized void C1H(Runnable runnable) {
        this.A00.remove(runnable);
    }

    public AnonymousClass1PQ(Executor executor) {
        C05520Zg.A02(executor);
        this.A01 = executor;
    }
}
