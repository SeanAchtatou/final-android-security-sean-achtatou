package X;

import android.text.TextUtils;
import java.util.Map;

/* renamed from: X.1X1  reason: invalid class name */
public final class AnonymousClass1X1 {
    public int A00 = 0;
    public final C24721Wv A01;
    public final Map A02 = new AnonymousClass04a();

    public static final String A00(AnonymousClass1X1 r3) {
        String A022;
        synchronized (r3.A01) {
            A022 = r3.A01.A02();
        }
        if (TextUtils.isEmpty(A022)) {
            return null;
        }
        String[] split = A022.split(",");
        if (split.length <= 1 || TextUtils.isEmpty(split[1])) {
            return null;
        }
        return split[1];
    }

    public AnonymousClass1X1(C24721Wv r2) {
        this.A01 = r2;
    }
}
