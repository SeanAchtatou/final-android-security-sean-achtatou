package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

/* renamed from: X.0Bq  reason: invalid class name and case insensitive filesystem */
public final class C01790Bq extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0AR A00;

    public C01790Bq(AnonymousClass0AR r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(-1961278261);
        if (intent == null) {
            C000700l.A0D(intent, -1937770337, A01);
        } else if (!AnonymousClass0A2.A00(intent.getAction(), "com.facebook.rti.intent.ACTION_NOTIFICATION_ACK")) {
            C000700l.A0D(intent, 764955254, A01);
        } else if (!this.A00.A05.A04(intent)) {
            C000700l.A0D(intent, 1705175804, A01);
        } else {
            String stringExtra = intent.getStringExtra("extra_notification_id");
            if (!TextUtils.isEmpty(stringExtra)) {
                this.A00.A05(stringExtra, C012309k.A00(intent), intent.getBooleanExtra("extra_processor_completed", true));
            }
            C000700l.A0D(intent, 1435405643, A01);
        }
    }
}
