package X;

import android.database.DatabaseUtils;
import com.facebook.common.stringformat.StringFormatUtil;
import com.google.common.base.Functions$ToStringFunction;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.Collections;

/* renamed from: X.0av  reason: invalid class name and case insensitive filesystem */
public abstract class C06140av {
    public Iterable A01() {
        if (this instanceof C12900qB) {
            C12900qB r2 = (C12900qB) this;
            return AnonymousClass1CJ.A01(r2.A00.A01(), Collections.singleton(r2.A01));
        } else if (this instanceof C12910qC) {
            C12910qC r22 = (C12910qC) this;
            return AnonymousClass1CJ.A01(r22.A01.A01(), r22.A00.A01());
        } else if (!(this instanceof C12920qD)) {
            if (!(this instanceof C12930qE)) {
                if (this instanceof C12940qF) {
                    return ((C12940qF) this).A00.A01();
                }
                if (this instanceof AnonymousClass1CE) {
                    return AnonymousClass0j4.A00(((AnonymousClass1CE) this).A02, Functions$ToStringFunction.INSTANCE);
                }
                if (!(this instanceof AnonymousClass1CF) && !(this instanceof AnonymousClass1CG)) {
                    if (!(this instanceof C25601a6)) {
                        return Collections.singleton(((C25591a5) this).A02);
                    }
                    Iterable A00 = AnonymousClass0j4.A00(((C25601a6) this).A01, new AnonymousClass1CH());
                    Preconditions.checkNotNull(A00);
                    return new AnonymousClass1CD(A00);
                }
            }
            return Collections.emptyList();
        } else {
            Iterable A002 = AnonymousClass0j4.A00(((C12920qD) this).A00, new AnonymousClass7KU());
            Preconditions.checkNotNull(A002);
            return new AnonymousClass1CD(A002);
        }
    }

    public String A02() {
        String str;
        String A02;
        String str2;
        int i;
        String A0P;
        String str3;
        String str4;
        if (this instanceof C12900qB) {
            return StringFormatUtil.formatStrLocaleSafe("WHEN %1$s THEN ?", ((C12900qB) this).A00.A02());
        }
        if (this instanceof C12910qC) {
            C12910qC r1 = (C12910qC) this;
            return StringFormatUtil.formatStrLocaleSafe("UPDATE %1$s SET %2$s WHERE %3$s", r1.A02, r1.A01.A02(), r1.A00.A02());
        } else if (this instanceof C12920qD) {
            C12920qD r12 = (C12920qD) this;
            if (r12.A00.isEmpty()) {
                return BuildConfig.FLAVOR;
            }
            StringBuilder sb = new StringBuilder();
            boolean z = true;
            for (C06140av r13 : r12.A00) {
                if (!z) {
                    sb.append(",");
                }
                sb.append(r13.A02());
                z = false;
            }
            return sb.toString();
        } else if (this instanceof C12930qE) {
            return ((C12930qE) this).A00;
        } else {
            if (this instanceof C12940qF) {
                str = "NOT (";
                A02 = ((C12940qF) this).A00.A02();
                str2 = ")";
            } else if (!(this instanceof AnonymousClass1CE)) {
                if (this instanceof AnonymousClass1CF) {
                    str3 = ((AnonymousClass1CF) this).A00;
                    str4 = " IS NULL";
                } else if (this instanceof AnonymousClass1CG) {
                    str3 = ((AnonymousClass1CG) this).A00;
                    str4 = " NOT NULL";
                } else if (!(this instanceof C25601a6)) {
                    C25591a5 r0 = (C25591a5) this;
                    str = r0.A00;
                    A02 = r0.A01;
                    str2 = "?";
                } else {
                    C25601a6 r5 = (C25601a6) this;
                    if (r5.A01.isEmpty()) {
                        return BuildConfig.FLAVOR;
                    }
                    StringBuilder sb2 = new StringBuilder("(");
                    boolean z2 = true;
                    for (C06140av r2 : r5.A01) {
                        if (!z2) {
                            sb2.append(" ");
                            sb2.append(r5.A00);
                            sb2.append(" ");
                        }
                        sb2.append(r2.A02());
                        z2 = false;
                    }
                    sb2.append(")");
                    return sb2.toString();
                }
                return AnonymousClass08S.A0J(str3, str4);
            } else {
                AnonymousClass1CE r9 = (AnonymousClass1CE) this;
                if (r9.A02.isEmpty()) {
                    A0P = AnonymousClass1CE.A00(r9.A01);
                } else {
                    int size = r9.A02.size() - 1;
                    String str5 = ",?";
                    Preconditions.checkNotNull(str5);
                    boolean z3 = true;
                    if (size <= 1) {
                        if (size < 0) {
                            z3 = false;
                        }
                        Preconditions.checkArgument(z3, "invalid count: %s", size);
                        if (size == 0) {
                            str5 = BuildConfig.FLAVOR;
                        }
                    } else {
                        int length = str5.length();
                        long j = ((long) length) * ((long) size);
                        int i2 = (int) j;
                        if (((long) i2) == j) {
                            char[] cArr = new char[i2];
                            str5.getChars(0, length, cArr, 0);
                            while (true) {
                                i = i2 - length;
                                if (length >= i) {
                                    break;
                                }
                                System.arraycopy(cArr, 0, cArr, length, length);
                                length <<= 1;
                            }
                            System.arraycopy(cArr, 0, cArr, length, i);
                            str5 = new String(cArr);
                        } else {
                            throw new ArrayIndexOutOfBoundsException(AnonymousClass08S.A0G("Required array size too large: ", j));
                        }
                    }
                    A0P = AnonymousClass08S.A0P("(?", str5, ")");
                }
                return AnonymousClass08S.A0S(r9.A00, r9.A03 ? " NOT" : BuildConfig.FLAVOR, " IN ", A0P);
            }
            return AnonymousClass08S.A0P(str, A02, str2);
        }
    }

    public void A03(StringBuilder sb) {
        String sqlEscapeString;
        if (this instanceof C12900qB) {
            C12900qB r1 = (C12900qB) this;
            sb.append("WHEN ");
            r1.A00.A03(sb);
            sb.append(" THEN ");
            sqlEscapeString = DatabaseUtils.sqlEscapeString(r1.A01);
        } else if (this instanceof C12910qC) {
            C12910qC r12 = (C12910qC) this;
            sb.append(C22298Ase.$const$string(59));
            sb.append(r12.A02);
            sb.append(" SET ");
            r12.A01.A03(sb);
            sb.append(C99084oO.$const$string(93));
            r12.A00.A03(sb);
            return;
        } else if (this instanceof C12920qD) {
            C12920qD r2 = (C12920qD) this;
            if (!r2.A00.isEmpty()) {
                ((C06140av) r2.A00.get(0)).A03(sb);
                for (int i = 1; i < r2.A00.size(); i++) {
                    sb.append(",");
                    ((C06140av) r2.A00.get(i)).A03(sb);
                }
                return;
            }
            return;
        } else if (this instanceof C12930qE) {
            sqlEscapeString = ((C12930qE) this).A00;
        } else if (this instanceof C12940qF) {
            sb.append("NOT (");
            ((C12940qF) this).A00.A03(sb);
            sqlEscapeString = ")";
        } else if (this instanceof AnonymousClass1CE) {
            AnonymousClass1CE r13 = (AnonymousClass1CE) this;
            sb.append(r13.A00);
            sb.append(r13.A03 ? " NOT" : BuildConfig.FLAVOR);
            sb.append(" IN ");
            sqlEscapeString = AnonymousClass1CE.A00(r13.A02.isEmpty() ? r13.A01 : r13.A02);
        } else if (this instanceof AnonymousClass1CF) {
            sb.append(((AnonymousClass1CF) this).A00);
            sqlEscapeString = " IS NULL";
        } else if (this instanceof AnonymousClass1CG) {
            sb.append(((AnonymousClass1CG) this).A00);
            sqlEscapeString = " NOT NULL";
        } else if (!(this instanceof C25601a6)) {
            C25591a5 r14 = (C25591a5) this;
            sb.append(r14.A00);
            sb.append(r14.A01);
            sqlEscapeString = DatabaseUtils.sqlEscapeString(r14.A02);
        } else {
            C25601a6 r3 = (C25601a6) this;
            if (!r3.A01.isEmpty()) {
                sb.append("(");
                ((C06140av) r3.A01.get(0)).A03(sb);
                for (int i2 = 1; i2 < r3.A01.size(); i2++) {
                    sb.append(" ");
                    sb.append(r3.A00);
                    sb.append(" ");
                    ((C06140av) r3.A01.get(i2)).A03(sb);
                }
                sb.append(")");
                return;
            }
            return;
        }
        sb.append(sqlEscapeString);
    }

    public String[] A04() {
        if (this instanceof C12900qB) {
            return (String[]) AnonymousClass0j4.A0F(((C12900qB) this).A01(), String.class);
        }
        if (this instanceof C12910qC) {
            return (String[]) AnonymousClass0j4.A0F(((C12910qC) this).A01(), String.class);
        }
        if (this instanceof C12920qD) {
            return (String[]) AnonymousClass0j4.A0F(((C12920qD) this).A01(), String.class);
        }
        if (!(this instanceof C12930qE)) {
            if (this instanceof C12940qF) {
                return ((C12940qF) this).A00.A04();
            }
            if (this instanceof AnonymousClass1CE) {
                return (String[]) AnonymousClass0j4.A0F(((AnonymousClass1CE) this).A01(), String.class);
            }
            if (!(this instanceof AnonymousClass1CF) && !(this instanceof AnonymousClass1CG)) {
                return !(this instanceof C25601a6) ? new String[]{((C25591a5) this).A02} : (String[]) AnonymousClass0j4.A0F(((C25601a6) this).A01(), String.class);
            }
        }
        return new String[0];
    }
}
