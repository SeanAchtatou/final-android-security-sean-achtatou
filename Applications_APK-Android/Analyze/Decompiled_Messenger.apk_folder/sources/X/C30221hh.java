package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.messaging.model.messages.Message;
import com.facebook.user.model.UserKey;

/* renamed from: X.1hh  reason: invalid class name and case insensitive filesystem */
public final class C30221hh implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C30221hh(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(227900381);
        AnonymousClass0r6 r3 = this.A00;
        UserKey userKey = ((Message) intent.getParcelableExtra(AnonymousClass24B.$const$string(1014))).A0K.A01;
        C37291v0 r1 = (C37291v0) r3.A0N.get(userKey);
        if (r1 != null && r1.A0C) {
            r1.A0C = false;
            AnonymousClass0r6.A0A(r3, userKey);
        }
        AnonymousClass09Y.A01(1338300085, A002);
    }
}
