package X;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* renamed from: X.1eT  reason: invalid class name and case insensitive filesystem */
public abstract class C28231eT implements Comparable {
    public final Type _type;

    public C28231eT() {
        Type genericSuperclass = getClass().getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            this._type = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
            return;
        }
        throw new IllegalArgumentException("Internal error: TypeReference constructed without actual type information");
    }

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return 0;
    }
}
