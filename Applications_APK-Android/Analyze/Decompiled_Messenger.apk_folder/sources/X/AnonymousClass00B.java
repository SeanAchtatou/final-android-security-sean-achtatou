package X;

/* renamed from: X.00B  reason: invalid class name */
public final class AnonymousClass00B extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.base.app.SplashScreenApplication$2";
    public final /* synthetic */ AnonymousClass00A A00;
    public final /* synthetic */ AnonymousClass001 A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass00B(AnonymousClass001 r1, String str, AnonymousClass00A r3) {
        super(str);
        this.A01 = r1;
        this.A00 = r3;
    }

    public void run() {
        this.A01.A07();
        this.A00.A05();
    }
}
