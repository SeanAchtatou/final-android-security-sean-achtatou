package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0UG  reason: invalid class name */
public interface AnonymousClass0UG<E> extends Collection<E> {
    int AMN(Object obj, int i);

    int AUa(Object obj);

    Set AYC();

    int C18(Object obj, int i);

    int C7L(Object obj, int i);

    boolean C7M(Object obj, int i, int i2);

    boolean add(Object obj);

    boolean contains(Object obj);

    boolean containsAll(Collection collection);

    Set entrySet();

    boolean equals(Object obj);

    int hashCode();

    Iterator iterator();

    boolean remove(Object obj);

    int size();
}
