package X;

import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import org.apache.http.conn.ssl.X509HostnameVerifier;

/* renamed from: X.0Qh  reason: invalid class name and case insensitive filesystem */
public final class C03890Qh implements X509HostnameVerifier {
    private final C03910Qj A00;
    private final AnonymousClass0BB A01 = new AnonymousClass0BB();

    public C03890Qh(C03910Qj r2) {
        this.A00 = r2;
    }

    public void verify(String str, X509Certificate x509Certificate) {
        if (!this.A01.A04(str, x509Certificate).A00) {
            throw new SSLException(AnonymousClass08S.A0J("Failed to verify certificate for ", str));
        }
    }

    public void verify(String str, SSLSocket sSLSocket) {
        if (!verify(str, sSLSocket.getSession())) {
            throw new SSLException(AnonymousClass08S.A0J("Failed to verify socket for ", str));
        }
    }

    public void verify(String str, String[] strArr, String[] strArr2) {
        String str2;
        C03880Qg A002;
        C03910Qj r0 = this.A00;
        if (r0 != null) {
            r0.APJ(str, strArr, strArr2);
        }
        int length = strArr.length;
        if (length <= 1) {
            if (length == 0) {
                str2 = null;
            } else {
                str2 = strArr[0];
            }
            List asList = Arrays.asList(strArr2);
            if (AnonymousClass0BB.A00.matcher(str).matches()) {
                A002 = AnonymousClass0BB.A01(str, asList);
            } else {
                A002 = AnonymousClass0BB.A00(str, str2, asList);
            }
            if (!A002.A00) {
                throw new SSLException(AnonymousClass08S.A0J("Failed to verify cns and subjectAlts for ", str));
            }
            return;
        }
        throw new SSLException("Certificate has multiple common names");
    }

    public boolean verify(String str, SSLSession sSLSession) {
        return this.A01.verify(str, sSLSession);
    }
}
