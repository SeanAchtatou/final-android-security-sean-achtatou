package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.0yR  reason: invalid class name and case insensitive filesystem */
public final class C17170yR implements C05700aB {
    public static final AnonymousClass1Y8 A00;
    public static final AnonymousClass1Y8 A01 = A02.A09("time_ms_remote_user_verified");
    private static final AnonymousClass1Y8 A02;

    static {
        AnonymousClass1Y8 A0D = C04350Ue.A0B.A09("messages_sync_pref/");
        A02 = A0D;
        A00 = A0D.A09("is_logged_user_fetch_required");
    }

    public static final C17170yR A00() {
        return new C17170yR();
    }

    public ImmutableSet AzO() {
        return ImmutableSet.A04(A00);
    }
}
