package X;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1U6  reason: invalid class name */
public final class AnonymousClass1U6 implements AnonymousClass1U7 {
    private static volatile AnonymousClass1U6 A01;
    private final ImmutableMap A00;

    public static final AnonymousClass1U6 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1U6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1U6(AnonymousClass0UU.A08(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public ImmutableSet Ax9() {
        return this.A00.keySet();
    }

    public Object AxA(String str, C10880l0 r3) {
        return this.A00.get(str);
    }

    private AnonymousClass1U6(Boolean bool) {
        ImmutableMap.Builder builder = ImmutableMap.builder();
        builder.put(C99084oO.$const$string(AnonymousClass1Y3.A4d), bool);
        this.A00 = builder.build();
    }
}
