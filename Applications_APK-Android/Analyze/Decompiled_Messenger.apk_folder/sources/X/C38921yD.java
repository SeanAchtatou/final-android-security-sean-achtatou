package X;

import android.app.Application;
import android.content.Context;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1yD  reason: invalid class name and case insensitive filesystem */
public final class C38921yD {
    public final Context A00;
    public final C21513AHv A01;
    public final C21514AHw A02;
    public final ExecutorService A03;

    public C38921yD(C21514AHw aHw, ExecutorService executorService, C21513AHv aHv, Application application) {
        this.A02 = aHw;
        this.A03 = executorService;
        this.A01 = aHv;
        this.A00 = application.getApplicationContext();
    }
}
