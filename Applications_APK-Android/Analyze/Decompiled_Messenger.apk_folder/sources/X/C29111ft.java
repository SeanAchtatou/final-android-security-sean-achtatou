package X;

/* renamed from: X.1ft  reason: invalid class name and case insensitive filesystem */
public abstract class C29111ft implements C29121fu {
    public C860046h findReferenceType() {
        return null;
    }

    public Class[] findViews() {
        return null;
    }

    public abstract C183512m getAccessor();

    public abstract AnonymousClass137 getConstructorParameter();

    public abstract C29091fr getField();

    public abstract C29141fw getGetter();

    public abstract C183512m getMutator();

    public abstract String getName();

    public abstract C29141fw getSetter();

    public abstract C87974Hw getWrapperName();

    public abstract boolean hasConstructorParameter();

    public abstract boolean hasField();

    public abstract boolean hasGetter();

    public abstract boolean hasSetter();

    public abstract boolean isExplicitlyIncluded();

    public boolean isRequired() {
        return false;
    }

    public boolean isTypeId() {
        return false;
    }
}
