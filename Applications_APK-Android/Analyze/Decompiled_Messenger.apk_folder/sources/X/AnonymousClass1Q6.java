package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1Q6  reason: invalid class name */
public final class AnonymousClass1Q6 implements AnonymousClass1Q3 {
    public final AnonymousClass1Q3 A00;
    private final C30911iq A01;
    private final C30911iq A02;
    private final C22601Mc A03;

    public static Map A00(AnonymousClass1MN r4, AnonymousClass1QK r5, boolean z, int i) {
        if (!r4.C3Q(r5, "DiskCacheProducer")) {
            return null;
        }
        String valueOf = String.valueOf(z);
        if (!z) {
            return C22665B6t.A01("cached_value_found", valueOf);
        }
        String valueOf2 = String.valueOf(i);
        HashMap hashMap = new HashMap(2);
        hashMap.put("cached_value_found", valueOf);
        hashMap.put("encodedImageSize", valueOf2);
        return Collections.unmodifiableMap(hashMap);
    }

    public void ByS(C23581Qb r7, AnonymousClass1QK r8) {
        C30911iq r0;
        AnonymousClass1Q0 r2 = r8.A09;
        if (r2.A0E) {
            r8.A07.Bk8(r8, "DiskCacheProducer");
            C23601Qd A05 = this.A03.A05(r2, r8.A0A);
            boolean z = false;
            if (r2.A09 == AnonymousClass1OM.SMALL) {
                z = true;
            }
            if (z) {
                r0 = this.A02;
            } else {
                r0 = this.A01;
            }
            AtomicBoolean atomicBoolean = new AtomicBoolean(false);
            r0.A00(A05, atomicBoolean).A03(new C23971Rr(this, r8.A07, r8, r7));
            r8.A06(new C33241nG(atomicBoolean));
        } else if (r8.A08.mValue >= C23531Pw.DISK_CACHE.mValue) {
            r7.Bgg(null, 1);
        } else {
            this.A00.ByS(r7, r8);
        }
    }

    public AnonymousClass1Q6(C30911iq r1, C30911iq r2, C22601Mc r3, AnonymousClass1Q3 r4) {
        this.A01 = r1;
        this.A02 = r2;
        this.A03 = r3;
        this.A00 = r4;
    }
}
