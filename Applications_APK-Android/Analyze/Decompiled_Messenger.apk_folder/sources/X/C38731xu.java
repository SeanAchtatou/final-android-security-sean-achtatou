package X;

import io.card.payment.BuildConfig;

/* renamed from: X.1xu  reason: invalid class name and case insensitive filesystem */
public final class C38731xu {
    public String A00;
    public boolean A01;

    public String A00() {
        String str = this.A00;
        if (str == null) {
            return BuildConfig.FLAVOR;
        }
        return "success - " + this.A01 + ", message - " + str;
    }

    public C38731xu(boolean z, String str) {
        this.A01 = z;
        this.A00 = str;
    }
}
