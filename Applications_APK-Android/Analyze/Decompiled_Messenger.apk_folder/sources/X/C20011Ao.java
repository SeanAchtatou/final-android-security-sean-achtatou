package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import javax.inject.Singleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1Ao  reason: invalid class name and case insensitive filesystem */
public final class C20011Ao implements AnonymousClass0mA {
    private static volatile C20011Ao A03;
    public final AnonymousClass1YI A00;
    private final AnonymousClass06B A01;
    private final LinkedList A02 = new LinkedList();

    public String Aji() {
        return "data_loading_debug_events.txt";
    }

    public static final C20011Ao A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C20011Ao.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C20011Ao(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void A01(String str, String str2, Object obj, Object obj2) {
        String str3;
        if (this.A00.AbO(211, false)) {
            synchronized (this.A02) {
                while (this.A02.size() >= 250) {
                    this.A02.removeFirst();
                }
                LinkedList linkedList = this.A02;
                long now = this.A01.now();
                String str4 = null;
                if (obj == null) {
                    str3 = null;
                } else {
                    str3 = obj.toString();
                }
                if (obj2 != null) {
                    str4 = obj2.toString();
                }
                linkedList.add(new C71203bz(str, now, str2, str3, str4));
            }
        }
    }

    public String Ajh() {
        ArrayList arrayList;
        synchronized (this.A02) {
            arrayList = new ArrayList(this.A02);
        }
        Collections.reverse(arrayList);
        JSONArray jSONArray = new JSONArray();
        try {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C71203bz r4 = (C71203bz) it.next();
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("event", r4.A01);
                jSONObject.put("loader", r4.A03);
                jSONObject.put("timestamp", C71203bz.A05.format(Long.valueOf(r4.A00)) + " (" + r4.A00 + ")");
                String str = r4.A04;
                if (str != null) {
                    jSONObject.put("params", str);
                }
                String str2 = r4.A02;
                if (str2 != null) {
                    jSONObject.put("extra", str2);
                }
                jSONArray.put(jSONObject);
            }
        } catch (JSONException unused) {
        }
        return jSONArray.toString();
    }

    private C20011Ao(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WA.A00(r2);
        this.A01 = AnonymousClass067.A02();
    }
}
