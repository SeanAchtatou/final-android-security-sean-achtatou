package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import io.card.payment.BuildConfig;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0ct  reason: invalid class name and case insensitive filesystem */
public final class C07210ct {
    public static final HashSet A06 = new HashSet(Arrays.asList(1, 2));
    public AnonymousClass0f7 A00;
    public C08290f0 A01;
    public Set A02;
    public final int A03;
    public final int A04;
    public final C26501bY A05;

    public static String A00(int i) {
        switch (i) {
            case 1:
                return "cpuBoost";
            case 2:
                return "gpuBoost";
            case 3:
                return "delayedGC";
            case 4:
                return "layoutPreinflation";
            case 5:
                return "threadAffinity";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return "renderThreadBoost";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "classpreload";
            case 8:
                return "uiThreadBoost";
            case Process.SIGKILL:
                return "lithoLayoutThreadBoost";
            case AnonymousClass1Y3.A01 /*10*/:
                return "delayedAnalytics";
            case AnonymousClass1Y3.A02 /*11*/:
                return "deepDataCollection";
            case AnonymousClass1Y3.A03 /*12*/:
                return "memoryManagerTrim";
            case 13:
                return "softKeyboard";
            case 14:
                return "jitSampleOnly";
            case 15:
                return "blockIdleJob";
            case 16:
                return "activityThread";
            default:
                return BuildConfig.FLAVOR;
        }
    }

    public static int[] A01() {
        return new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
    }

    public C07210ct(int i, int i2, C26501bY r4) {
        this.A00 = null;
        this.A01 = null;
        this.A02 = null;
        this.A04 = i;
        this.A03 = i2;
        this.A05 = r4;
        if (r4 != null) {
            r4.A02 = i;
        }
    }

    public C07210ct(int i, int i2, C26501bY r3, Set set) {
        this(i, i2, r3);
        this.A02 = set;
    }
}
