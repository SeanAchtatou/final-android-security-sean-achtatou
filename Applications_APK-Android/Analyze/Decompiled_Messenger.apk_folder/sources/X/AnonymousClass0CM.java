package X;

import java.util.HashMap;

/* renamed from: X.0CM  reason: invalid class name */
public final class AnonymousClass0CM extends HashMap<Integer, AnonymousClass0CL> {
    public AnonymousClass0CM() {
        for (AnonymousClass0CL r1 : AnonymousClass0CL.values()) {
            put(Integer.valueOf(r1.mValue), r1);
        }
    }
}
