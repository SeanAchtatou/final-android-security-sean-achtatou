package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0sw  reason: invalid class name and case insensitive filesystem */
public final class C14270sw {
    private static C05540Zi A03;
    public int A00 = 0;
    public boolean A01 = false;
    private final C14340t9 A02;

    public static final C14270sw A00(AnonymousClass1XY r4) {
        C14270sw r0;
        synchronized (C14270sw.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C14270sw((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (C14270sw) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public boolean A01() {
        boolean z;
        boolean z2 = false;
        if (this.A00 == 0) {
            z2 = true;
        }
        if (z2) {
            C14340t9 r2 = this.A02;
            synchronized (r2) {
                z = r2.A01;
            }
            if (z) {
                return true;
            }
            return false;
        }
        return true;
    }

    private C14270sw(AnonymousClass1XY r2) {
        this.A02 = C14340t9.A00(r2);
    }
}
