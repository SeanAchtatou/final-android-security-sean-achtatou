package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.ipc.TraceContext;
import java.util.Arrays;
import java.util.TreeMap;

/* renamed from: X.04o  reason: invalid class name */
public final class AnonymousClass04o implements AnonymousClass04j {
    public static final int A00 = TriggerRegistry.A00.A02("video");

    public boolean AUJ(long j, Object obj, long j2, Object obj2) {
        return obj == obj2;
    }

    public boolean BE8() {
        return false;
    }

    public int AYs(long j, Object obj, AnonymousClass057 r8) {
        return ProvidersRegistry.A00.A00(Arrays.asList("qpl", "liger", "liger_http2", "transient_network_data"));
    }

    public TraceContext.TraceConfigExtras B6o(long j, Object obj, AnonymousClass057 r7) {
        TreeMap treeMap = new TreeMap();
        treeMap.put("provider.stack_trace.cpu_sampling_rate_ms", 11);
        return new TraceContext.TraceConfigExtras(treeMap, null, null);
    }
}
