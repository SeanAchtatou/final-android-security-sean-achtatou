package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0nc  reason: invalid class name and case insensitive filesystem */
public final class C11680nc {
    private static volatile C11680nc A01;
    public AnonymousClass0UN A00;

    public static final C11680nc A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C11680nc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C11680nc(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C11680nc(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
