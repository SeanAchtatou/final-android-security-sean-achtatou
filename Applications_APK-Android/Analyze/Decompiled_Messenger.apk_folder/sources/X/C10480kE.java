package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0kE  reason: invalid class name and case insensitive filesystem */
public final class C10480kE extends Enum implements C26781c0 {
    private static final /* synthetic */ C10480kE[] $VALUES;
    public static final C10480kE CLOSE_CLOSEABLE;
    public static final C10480kE EAGER_SERIALIZER_FETCH;
    public static final C10480kE FAIL_ON_EMPTY_BEANS;
    public static final C10480kE FLUSH_AFTER_WRITE_VALUE;
    public static final C10480kE INDENT_OUTPUT;
    public static final C10480kE ORDER_MAP_ENTRIES_BY_KEYS;
    public static final C10480kE WRAP_EXCEPTIONS;
    public static final C10480kE WRAP_ROOT_VALUE;
    public static final C10480kE WRITE_BIGDECIMAL_AS_PLAIN;
    public static final C10480kE WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS;
    public static final C10480kE WRITE_DATES_AS_TIMESTAMPS;
    public static final C10480kE WRITE_DATE_KEYS_AS_TIMESTAMPS;
    public static final C10480kE WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS;
    public static final C10480kE WRITE_EMPTY_JSON_ARRAYS;
    public static final C10480kE WRITE_ENUMS_USING_INDEX;
    public static final C10480kE WRITE_ENUMS_USING_TO_STRING;
    public static final C10480kE WRITE_NULL_MAP_VALUES;
    public static final C10480kE WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED;
    private final boolean _defaultState;

    static {
        C10480kE r14 = new C10480kE("WRAP_ROOT_VALUE", 0, false);
        WRAP_ROOT_VALUE = r14;
        C10480kE r13 = new C10480kE("INDENT_OUTPUT", 1, false);
        INDENT_OUTPUT = r13;
        C10480kE r0 = new C10480kE("FAIL_ON_EMPTY_BEANS", 2, true);
        FAIL_ON_EMPTY_BEANS = r0;
        C10480kE r15 = new C10480kE("WRAP_EXCEPTIONS", 3, true);
        WRAP_EXCEPTIONS = r15;
        C10480kE r11 = new C10480kE("CLOSE_CLOSEABLE", 4, false);
        CLOSE_CLOSEABLE = r11;
        C10480kE r10 = new C10480kE("FLUSH_AFTER_WRITE_VALUE", 5, true);
        FLUSH_AFTER_WRITE_VALUE = r10;
        C10480kE r9 = new C10480kE("WRITE_DATES_AS_TIMESTAMPS", 6, true);
        WRITE_DATES_AS_TIMESTAMPS = r9;
        C10480kE r8 = new C10480kE("WRITE_DATE_KEYS_AS_TIMESTAMPS", 7, false);
        WRITE_DATE_KEYS_AS_TIMESTAMPS = r8;
        C10480kE r7 = new C10480kE("WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS", 8, false);
        WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS = r7;
        C10480kE r6 = new C10480kE("WRITE_ENUMS_USING_TO_STRING", 9, false);
        WRITE_ENUMS_USING_TO_STRING = r6;
        C10480kE r5 = new C10480kE("WRITE_ENUMS_USING_INDEX", 10, false);
        WRITE_ENUMS_USING_INDEX = r5;
        C10480kE r4 = new C10480kE("WRITE_NULL_MAP_VALUES", 11, true);
        WRITE_NULL_MAP_VALUES = r4;
        C10480kE r3 = new C10480kE("WRITE_EMPTY_JSON_ARRAYS", 12, true);
        WRITE_EMPTY_JSON_ARRAYS = r3;
        C10480kE r21 = new C10480kE("WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED", 13, false);
        WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED = r21;
        C10480kE r212 = new C10480kE("WRITE_BIGDECIMAL_AS_PLAIN", 14, false);
        WRITE_BIGDECIMAL_AS_PLAIN = r212;
        C10480kE r213 = new C10480kE("WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS", 15, true);
        WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS = r213;
        C10480kE r214 = new C10480kE("ORDER_MAP_ENTRIES_BY_KEYS", 16, false);
        ORDER_MAP_ENTRIES_BY_KEYS = r214;
        C10480kE r215 = new C10480kE("EAGER_SERIALIZER_FETCH", 17, true);
        EAGER_SERIALIZER_FETCH = r215;
        C10480kE r27 = r21;
        C10480kE r28 = r212;
        C10480kE r29 = r213;
        C10480kE r30 = r214;
        C10480kE r31 = r215;
        C10480kE r16 = r0;
        C10480kE r17 = r15;
        C10480kE r18 = r11;
        C10480kE r19 = r10;
        C10480kE r20 = r9;
        C10480kE r152 = r13;
        $VALUES = new C10480kE[]{r14, r152, r16, r17, r18, r19, r20, r8, r7, r6, r5, r4, r3, r27, r28, r29, r30, r31};
    }

    public static C10480kE valueOf(String str) {
        return (C10480kE) Enum.valueOf(C10480kE.class, str);
    }

    public static C10480kE[] values() {
        return (C10480kE[]) $VALUES.clone();
    }

    public boolean enabledByDefault() {
        return this._defaultState;
    }

    private C10480kE(String str, int i, boolean z) {
        this._defaultState = z;
    }

    public int getMask() {
        return 1 << ordinal();
    }
}
