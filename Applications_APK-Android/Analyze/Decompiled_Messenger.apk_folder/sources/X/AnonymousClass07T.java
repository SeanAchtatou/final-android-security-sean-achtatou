package X;

/* renamed from: X.07T  reason: invalid class name */
public final class AnonymousClass07T extends AnonymousClass07R {
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0043, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0044, code lost:
        if (r2 == null) goto L_0x0049;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(android.content.Context r5, byte[] r6) {
        /*
            r4 = this;
            java.io.File r0 = r4.A00
            X.C000300h.A01(r0)
            boolean r0 = r0.exists()
            r3 = 1
            if (r0 != 0) goto L_0x000d
            return r3
        L_0x000d:
            android.content.res.AssetManager r2 = r5.getAssets()
            java.lang.String r1 = r4.A02
            r0 = 2
            java.io.InputStream r2 = r2.open(r1, r0)
            r0 = 2147483647(0x7fffffff, float:NaN)
            byte[] r1 = X.AnonymousClass07P.A03(r2, r6, r0)     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x0024
            r2.close()
        L_0x0024:
            java.io.FileInputStream r2 = new java.io.FileInputStream
            java.io.File r0 = r4.A00
            X.C000300h.A01(r0)
            r2.<init>(r0)
            int r0 = r1.length     // Catch:{ all -> 0x003d }
            int r0 = r0 + r3
            byte[] r0 = X.AnonymousClass07P.A03(r2, r6, r0)     // Catch:{ all -> 0x003d }
            r2.close()
            boolean r0 = java.util.Arrays.equals(r1, r0)
            r0 = r0 ^ r3
            return r0
        L_0x003d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003f }
        L_0x003f:
            r0 = move-exception
            goto L_0x0046
        L_0x0041:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r0 = move-exception
            if (r2 == 0) goto L_0x0049
        L_0x0046:
            r2.close()     // Catch:{ all -> 0x0049 }
        L_0x0049:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07T.A02(android.content.Context, byte[]):boolean");
    }

    public AnonymousClass07T(String str, String str2) {
        super(str, str2);
    }
}
