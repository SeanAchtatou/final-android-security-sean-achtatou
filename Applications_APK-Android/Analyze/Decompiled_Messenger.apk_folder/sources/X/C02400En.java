package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.0En  reason: invalid class name and case insensitive filesystem */
public abstract class C02400En {
    public Context A00;
    public final AnonymousClass0Ep A01;

    public void A01(String str) {
    }

    public void A02(String str, int i) {
    }

    public abstract void A03(String str, String str2, File file, File file2);

    public abstract String[] A04(String str, String str2, File file);

    public C02400En(Context context, AnonymousClass0Ep r2) {
        AnonymousClass0Eq.A00();
        this.A01 = r2;
        this.A00 = context;
    }
}
