package X;

import android.database.Cursor;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.0og  reason: invalid class name and case insensitive filesystem */
public final class C12170og implements C28021e8 {
    public static final String[] A04 = ((String[]) AnonymousClass0U8.A03(C28031e9.A17, C12180oj.A0t, String.class));
    private final Cursor A00;
    private final C28031e9 A01;
    private final C13680rr A02;
    private final C12180oj A03;

    public C29621gi BLn() {
        if (!this.A00.moveToNext()) {
            return null;
        }
        C17920zh A002 = ThreadSummary.A00();
        this.A01.A01(A002, this.A02);
        this.A03.A01(A002, this.A02);
        return new C29621gi(A002, this.A02.A00.getLong(this.A01.A0L));
    }

    public void close() {
        this.A00.close();
    }

    public C12170og(C13660rp r3, C13670rq r4, Cursor cursor, boolean z) {
        this.A00 = cursor;
        this.A01 = new C28031e9(r3, cursor, z);
        this.A03 = new C12180oj(r4, cursor);
        this.A02 = new C13680rr(cursor, "thread_key");
    }

    public C17920zh BLo() {
        C29621gi BLn = BLn();
        if (BLn != null) {
            return BLn.A01;
        }
        return null;
    }

    public ThreadSummary BLt() {
        C29621gi BLn = BLn();
        if (BLn != null) {
            return BLn.A01.A00();
        }
        return null;
    }
}
