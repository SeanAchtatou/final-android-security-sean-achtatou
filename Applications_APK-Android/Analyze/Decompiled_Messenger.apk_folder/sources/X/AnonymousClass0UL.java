package X;

import com.google.common.base.Preconditions;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.0UL  reason: invalid class name */
public abstract class AnonymousClass0UL implements Iterator {
    public Iterator A00 = C24941Xs.A02;
    private Iterator A01;
    public final Iterator A02;

    public Iterator A00(Object obj) {
        if (this instanceof AnonymousClass0UK) {
            return (Iterator) obj;
        }
        Iterator it = (Iterator) obj;
        if (it instanceof AnonymousClass0UK) {
            AnonymousClass0UK r1 = (AnonymousClass0UK) it;
            if (!r1.A00.hasNext()) {
                return new AnonymousClass0UM(r1.A02);
            }
        }
        return new AnonymousClass1Y1(it);
    }

    public boolean hasNext() {
        Preconditions.checkNotNull(this.A00);
        if (this.A00.hasNext()) {
            return true;
        }
        while (this.A02.hasNext()) {
            Iterator A002 = A00(this.A02.next());
            this.A00 = A002;
            Preconditions.checkNotNull(A002);
            if (this.A00.hasNext()) {
                return true;
            }
        }
        return false;
    }

    public void remove() {
        boolean z = false;
        if (this.A01 != null) {
            z = true;
        }
        C25001Xy.A04(z);
        this.A01.remove();
        this.A01 = null;
    }

    public AnonymousClass0UL(Iterator it) {
        Preconditions.checkNotNull(it);
        this.A02 = it;
    }

    public Object next() {
        if (hasNext()) {
            Iterator it = this.A00;
            this.A01 = it;
            return it.next();
        }
        throw new NoSuchElementException();
    }
}
