package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.Set;

@UserScoped
/* renamed from: X.1Ac  reason: invalid class name and case insensitive filesystem */
public final class C19891Ac {
    private static C05540Zi A03;
    public boolean A00 = false;
    public final Set A01 = new C13770s2();
    public final Set A02 = new C13770s2();

    public static final C19891Ac A00(AnonymousClass1XY r3) {
        C19891Ac r0;
        synchronized (C19891Ac.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r3)) {
                    A03.A01();
                    A03.A00 = new C19891Ac();
                }
                C05540Zi r1 = A03;
                r0 = (C19891Ac) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }
}
