package X;

import java.util.Arrays;

/* renamed from: X.08U  reason: invalid class name */
public final class AnonymousClass08U extends UnsatisfiedLinkError {
    public AnonymousClass08U(Throwable th, String str) {
        super(AnonymousClass08S.A0S("APK was built for a different platform. Supported ABIs: ", Arrays.toString(C002401o.A02()), " error: ", str));
        initCause(th);
    }
}
