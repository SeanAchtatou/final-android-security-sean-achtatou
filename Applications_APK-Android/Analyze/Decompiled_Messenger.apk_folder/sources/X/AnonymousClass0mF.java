package X;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000;
import com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000;
import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.graphql.query.GQSQStringShape3S0000000_I3;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.attachments.OtherAttachmentData;
import com.facebook.messaging.groups.create.model.CreateCustomizableGroupParams;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessageDraft;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.MontageThreadPreview;
import com.facebook.messaging.model.threads.NotificationSetting;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.model.threads.ThreadCustomization;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadUpdate;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.AddAdminsToGroupParams;
import com.facebook.messaging.service.model.AddAdminsToGroupResult;
import com.facebook.messaging.service.model.BlockUserParams;
import com.facebook.messaging.service.model.CreateLocalAdminMessageParams;
import com.facebook.messaging.service.model.DeleteAllTincanThreadsResult;
import com.facebook.messaging.service.model.EditDisplayNameParams;
import com.facebook.messaging.service.model.EditDisplayNameResult;
import com.facebook.messaging.service.model.EditPasswordParams;
import com.facebook.messaging.service.model.EditPasswordResult;
import com.facebook.messaging.service.model.EditUsernameParams;
import com.facebook.messaging.service.model.EditUsernameResult;
import com.facebook.messaging.service.model.FetchIdentityKeysParams;
import com.facebook.messaging.service.model.FetchIdentityKeysResult;
import com.facebook.messaging.service.model.FetchMessagesContextParams;
import com.facebook.messaging.service.model.FetchMessagesContextResult;
import com.facebook.messaging.service.model.FetchMoreMessagesParams;
import com.facebook.messaging.service.model.FetchMoreMessagesResult;
import com.facebook.messaging.service.model.FetchMoreThreadsParams;
import com.facebook.messaging.service.model.FetchThreadListParams;
import com.facebook.messaging.service.model.FetchThreadListResult;
import com.facebook.messaging.service.model.FetchThreadMetadataParams;
import com.facebook.messaging.service.model.FetchThreadMetadataResult;
import com.facebook.messaging.service.model.FetchThreadParams;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.facebook.messaging.service.model.IgnoreMessageRequestsParams;
import com.facebook.messaging.service.model.MarkFolderSeenResult;
import com.facebook.messaging.service.model.ModifyThreadParams;
import com.facebook.messaging.service.model.NewMessageResult;
import com.facebook.messaging.service.model.PostGameScoreParams;
import com.facebook.messaging.service.model.PostGameScoreResult;
import com.facebook.messaging.service.model.RemoveAdminsFromGroupParams;
import com.facebook.messaging.service.model.RemoveAdminsFromGroupResult;
import com.facebook.messaging.service.model.RemoveMemberParams;
import com.facebook.messaging.service.model.SaveDraftParams;
import com.facebook.messaging.service.model.SendMessageByRecipientsParams;
import com.facebook.messaging.service.model.SetSettingsParams;
import com.facebook.messaging.service.model.UpdateFolderCountsParams;
import com.facebook.messaging.service.model.UpdateFolderCountsResult;
import com.facebook.messaging.service.model.UpdateMontagePreviewBlockModeParams;
import com.facebook.messaging.service.model.UpdateMontagePreviewBlockModeResult;
import com.facebook.messaging.service.model.UpdateProfilePicUriWithFilePathParams;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.push.constants.PushProperty;
import com.facebook.user.model.Name;
import com.facebook.user.model.User;
import com.facebook.user.model.UserFbidIdentifier;
import com.facebook.user.model.UserKey;
import com.facebook.user.model.UserSmsIdentifier;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

/* renamed from: X.0mF  reason: invalid class name */
public abstract class AnonymousClass0mF implements AnonymousClass0mG {
    private final C12400pH A00;
    private final String A01;

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fb, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00fc, code lost:
        if (r2 != null) goto L_0x00fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x0101 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A08(X.C11060ln r11, X.C27311cz r12) {
        /*
            r10 = this;
            boolean r0 = r10 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0154
            boolean r0 = r10 instanceof X.C11070lo
            if (r0 != 0) goto L_0x0141
            boolean r0 = r10 instanceof X.C12380pF
            if (r0 != 0) goto L_0x0113
            r4 = r10
            X.0lz r4 = (X.C11150lz) r4
            boolean r0 = r4 instanceof X.C12370pE
            if (r0 != 0) goto L_0x0088
            boolean r0 = r4 instanceof X.C11130lx
            if (r0 != 0) goto L_0x001c
            com.facebook.fbservice.service.OperationResult r5 = r12.BAz(r11)
        L_0x001b:
            return r5
        L_0x001c:
            X.0lx r4 = (X.C11130lx) r4
            boolean r9 = X.C11130lx.A01(r4)
            r3 = 12
            com.facebook.fbservice.service.OperationResult r5 = r12.BAz(r11)     // Catch:{ all -> 0x0102 }
            boolean r0 = r5.success     // Catch:{ all -> 0x0102 }
            if (r0 != 0) goto L_0x002d
            goto L_0x0078
        L_0x002d:
            android.os.Bundle r1 = r11.A00     // Catch:{ all -> 0x0102 }
            java.lang.String r0 = "acceptMessageRequestParams"
            android.os.Parcelable r1 = r1.getParcelable(r0)     // Catch:{ all -> 0x0102 }
            com.facebook.messaging.service.model.AcceptMessageRequestParams r1 = (com.facebook.messaging.service.model.AcceptMessageRequestParams) r1     // Catch:{ all -> 0x0102 }
            com.google.common.base.Preconditions.checkNotNull(r1)     // Catch:{ all -> 0x0102 }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x0102 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0102 }
            X.0u9 r0 = (X.C14800u9) r0     // Catch:{ all -> 0x0102 }
            com.facebook.messaging.model.threadkey.ThreadKey r8 = r1.A00     // Catch:{ all -> 0x0102 }
            X.0l8 r7 = X.C10950l8.A0A     // Catch:{ all -> 0x0102 }
            X.0l8 r6 = X.C10950l8.A05     // Catch:{ all -> 0x0102 }
            X.0m6 r5 = r0.A01     // Catch:{ all -> 0x0102 }
            X.0mL r0 = r5.A0A     // Catch:{ all -> 0x0102 }
            X.0mM r2 = r0.A00()     // Catch:{ all -> 0x0102 }
            X.AnonymousClass0m6.A0A(r5, r8)     // Catch:{ all -> 0x00f9 }
            X.0mN r0 = r5.A08     // Catch:{ all -> 0x00f9 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r0.A01(r8)     // Catch:{ all -> 0x00f9 }
            if (r1 == 0) goto L_0x006e
            X.AnonymousClass0m6.A06(r5, r7, r8)     // Catch:{ all -> 0x00f9 }
            X.0zh r0 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x00f9 }
            r0.A02(r1)     // Catch:{ all -> 0x00f9 }
            r0.A0N = r6     // Catch:{ all -> 0x00f9 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A00()     // Catch:{ all -> 0x00f9 }
            r5.A0b(r0)     // Catch:{ all -> 0x00f9 }
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ all -> 0x0102 }
        L_0x0073:
            com.facebook.fbservice.service.OperationResult r5 = com.facebook.fbservice.service.OperationResult.A00     // Catch:{ all -> 0x0102 }
            if (r9 == 0) goto L_0x001b
            goto L_0x007a
        L_0x0078:
            if (r9 == 0) goto L_0x001b
        L_0x007a:
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            return r5
        L_0x0088:
            X.0pE r4 = (X.C12370pE) r4
            com.facebook.fbservice.service.OperationResult r5 = r12.BAz(r11)
            boolean r0 = r5.success
            if (r0 == 0) goto L_0x001b
            android.os.Bundle r1 = r11.A00
            java.lang.String r0 = "acceptMessageRequestParams"
            android.os.Parcelable r3 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.AcceptMessageRequestParams r3 = (com.facebook.messaging.service.model.AcceptMessageRequestParams) r3
            com.google.common.base.Preconditions.checkNotNull(r3)
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BJD
            X.0UN r0 = r4.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2ij r6 = (X.C52232ij) r6
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r3.A00
            X.0l8 r3 = X.C10950l8.A05
            X.0Tq r0 = r6.A0A
            java.lang.Object r0 = r0.get()
            X.1br r0 = (X.C26691br) r0
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A0D(r2)
            if (r0 == 0) goto L_0x001b
            X.0l8 r0 = r0.A0O
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x001b
            android.content.ContentValues r4 = new android.content.ContentValues
            r4.<init>()
            java.lang.String r1 = r2.A0J()
            java.lang.String r0 = "thread_key"
            r4.put(r0, r1)
            java.lang.String r1 = r3.dbName
            java.lang.String r0 = "folder"
            r4.put(r0, r1)
            X.0Tq r0 = r6.A0F
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r3 = r0.A06()
            java.lang.String r0 = r2.A0J()
            java.lang.String[] r2 = new java.lang.String[]{r0}
            java.lang.String r1 = "threads"
            r0 = 98
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r3.update(r1, r4, r0, r2)
            return r5
        L_0x00f9:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00fb }
        L_0x00fb:
            r0 = move-exception
            if (r2 == 0) goto L_0x0101
            r2.close()     // Catch:{ all -> 0x0101 }
        L_0x0101:
            throw r0     // Catch:{ all -> 0x0102 }
        L_0x0102:
            r2 = move-exception
            if (r9 == 0) goto L_0x0112
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x0112:
            throw r2
        L_0x0113:
            r5 = r10
            X.0pF r5 = (X.C12380pF) r5
            android.os.Bundle r1 = r11.A00
            java.lang.String r0 = "acceptMessageRequestParams"
            android.os.Parcelable r4 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.AcceptMessageRequestParams r4 = (com.facebook.messaging.service.model.AcceptMessageRequestParams) r4
            com.google.common.base.Preconditions.checkNotNull(r4)
            int r1 = X.AnonymousClass1Y3.BAS
            X.0UN r3 = r5.A00
            r0 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.3Bj r2 = (X.C64393Bj) r2
            int r1 = X.AnonymousClass1Y3.A3K
            r0 = 5
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.8fR r1 = (X.C183598fR) r1
            X.2Xl r0 = X.C12380pF.A00(r5)
            r2.A03(r1, r4, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A00
            return r0
        L_0x0141:
            r0 = r10
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r11)
            return r0
        L_0x0154:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A08(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A09(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (this instanceof C11150lz) {
                return r8.BAz(r7);
            }
            C12380pF r5 = (C12380pF) this;
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r3 = r5.A00;
            return OperationResult.A04((AddAdminsToGroupResult) ((C64393Bj) AnonymousClass1XX.A02(2, i, r3)).A03((C183648fW) AnonymousClass1XX.A02(21, AnonymousClass1Y3.BIC, r3), (AddAdminsToGroupParams) r7.A00.getParcelable("addAdminsToGroupParams"), C12380pF.A00(r5)));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x007c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0A(X.C11060ln r21, X.C27311cz r22) {
        /*
            r20 = this;
            r1 = r20
            boolean r0 = r1 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0161
            boolean r0 = r1 instanceof X.C11070lo
            r4 = r21
            if (r0 != 0) goto L_0x014e
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x001f
            X.0lz r1 = (X.C11150lz) r1
            boolean r0 = r1 instanceof X.C12370pE
            r2 = r22
            if (r0 != 0) goto L_0x001a
            boolean r0 = r1 instanceof X.C11130lx
        L_0x001a:
            com.facebook.fbservice.service.OperationResult r0 = r2.BAz(r4)
            return r0
        L_0x001f:
            r3 = r1
            X.0pF r3 = (X.C12380pF) r3
            int r1 = X.AnonymousClass1Y3.At1
            X.0UN r0 = r3.A00
            r7 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.711 r0 = (X.AnonymousClass711) r0
            X.14b r1 = r0.A00
            X.0gA r0 = X.C08870g7.A1S
            r1.CH1(r0)
            android.os.Bundle r1 = r4.A00
            java.lang.String r0 = "addMembersParams"
            android.os.Parcelable r1 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.AddMembersParams r1 = (com.facebook.messaging.service.model.AddMembersParams) r1
            int r4 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r3.A00
            r0 = 32
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r4, r2)
            X.1Yd r0 = (X.C25051Yd) r0
            r4 = 285739879241581(0x103e10000176d, double:1.411742579800915E-309)
            boolean r0 = r0.Aem(r4)
            r8 = 1
            if (r0 == 0) goto L_0x012f
            int r2 = X.AnonymousClass1Y3.B5Q
            X.0UN r0 = r3.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r8, r2, r0)
            X.B4e r6 = (X.C22623B4e) r6
            X.B5f r2 = r6.A07
            X.B5j r0 = r6.A06
            java.lang.Object r0 = r2.A01(r1, r0)
            com.facebook.messaging.service.model.FetchThreadResult r0 = (com.facebook.messaging.service.model.FetchThreadResult) r0
            if (r0 == 0) goto L_0x012f
            com.facebook.messaging.service.model.AddMembersResult r2 = new com.facebook.messaging.service.model.AddMembersResult
            int r0 = r6.A00
            long r4 = (long) r0
            com.google.common.collect.ImmutableList r0 = r6.A02
            r2.<init>(r4, r0)
            com.facebook.fbservice.service.OperationResult r2 = com.facebook.fbservice.service.OperationResult.A04(r2)
        L_0x007a:
            if (r2 != 0) goto L_0x013c
            int r2 = X.AnonymousClass1Y3.B5Q
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r2, r0)
            X.B4e r0 = (X.C22623B4e) r0
            X.2Xl r10 = X.C12380pF.A00(r3)
            java.lang.Class r2 = r3.getClass()
            com.facebook.common.callercontext.CallerContext r9 = com.facebook.common.callercontext.CallerContext.A04(r2)
            java.lang.String r6 = "add-members"
            r2 = 0
        L_0x0095:
            r4 = 3
            if (r2 >= r4) goto L_0x0133
            X.3Bj r4 = r0.A04     // Catch:{ TigonErrorException -> 0x0100 }
            X.3fs r5 = r4.A01()     // Catch:{ TigonErrorException -> 0x0100 }
            X.B4J r4 = r0.A08     // Catch:{ TigonErrorException -> 0x0100 }
            X.3fu r4 = X.C73323ft.A00(r4, r1)     // Catch:{ TigonErrorException -> 0x0100 }
            r4.A02 = r6     // Catch:{ TigonErrorException -> 0x0100 }
            X.3ft r4 = r4.A00()     // Catch:{ TigonErrorException -> 0x0100 }
            r5.A00(r4)     // Catch:{ TigonErrorException -> 0x0100 }
            java.lang.String r4 = "addMembers"
            r5.A02(r4, r9, r10)     // Catch:{ TigonErrorException -> 0x0100 }
            java.util.Map r4 = r5.A06     // Catch:{ TigonErrorException -> 0x0100 }
            java.lang.Object r4 = r4.get(r6)     // Catch:{ TigonErrorException -> 0x0100 }
            com.google.common.collect.ImmutableList r4 = (com.google.common.collect.ImmutableList) r4     // Catch:{ TigonErrorException -> 0x0100 }
            com.google.common.collect.ImmutableList r8 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ TigonErrorException -> 0x0100 }
            if (r4 == 0) goto L_0x00df
            com.google.common.collect.ImmutableList$Builder r11 = com.google.common.collect.ImmutableList.builder()     // Catch:{ TigonErrorException -> 0x0100 }
            X.1Xv r8 = r4.iterator()     // Catch:{ TigonErrorException -> 0x0100 }
        L_0x00c6:
            boolean r4 = r8.hasNext()     // Catch:{ TigonErrorException -> 0x0100 }
            if (r4 == 0) goto L_0x00db
            java.lang.Object r5 = r8.next()     // Catch:{ TigonErrorException -> 0x0100 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ TigonErrorException -> 0x0100 }
            com.facebook.user.model.UserFbidIdentifier r4 = new com.facebook.user.model.UserFbidIdentifier     // Catch:{ TigonErrorException -> 0x0100 }
            r4.<init>(r5)     // Catch:{ TigonErrorException -> 0x0100 }
            r11.add(r4)     // Catch:{ TigonErrorException -> 0x0100 }
            goto L_0x00c6
        L_0x00db:
            com.google.common.collect.ImmutableList r8 = r11.build()     // Catch:{ TigonErrorException -> 0x0100 }
        L_0x00df:
            X.711 r11 = r0.A05     // Catch:{ TigonErrorException -> 0x0100 }
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r1.A00     // Catch:{ TigonErrorException -> 0x0100 }
            long r14 = r4.A0G()     // Catch:{ TigonErrorException -> 0x0100 }
            r13 = r8
            r16 = r2
            java.lang.Integer r12 = X.AnonymousClass07B.A0C     // Catch:{ TigonErrorException -> 0x0100 }
            r18 = 0
            r19 = 0
            r17 = 0
            X.AnonymousClass711.A01(r11, r12, r13, r14, r16, r17, r18, r19)     // Catch:{ TigonErrorException -> 0x0100 }
            com.facebook.messaging.service.model.AddMembersResult r11 = new com.facebook.messaging.service.model.AddMembersResult     // Catch:{ TigonErrorException -> 0x0100 }
            long r4 = (long) r2     // Catch:{ TigonErrorException -> 0x0100 }
            r11.<init>(r4, r8)     // Catch:{ TigonErrorException -> 0x0100 }
            com.facebook.fbservice.service.OperationResult r2 = com.facebook.fbservice.service.OperationResult.A04(r11)     // Catch:{ TigonErrorException -> 0x0100 }
            goto L_0x013c
        L_0x0100:
            r5 = move-exception
            X.711 r11 = r0.A05
            com.google.common.collect.ImmutableList r13 = r1.A01
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r1.A00
            long r14 = r4.A0G()
            java.lang.Class r4 = r5.getClass()
            java.lang.String r18 = r4.getName()
            java.lang.String r19 = r5.getMessage()
            r16 = r2
            java.lang.Integer r12 = X.AnonymousClass07B.A00
            r17 = 0
            X.AnonymousClass711.A01(r11, r12, r13, r14, r16, r17, r18, r19)
            X.0h3 r4 = r0.A03
            boolean r4 = r4.A0Q()
            if (r4 == 0) goto L_0x0132
            r4 = 2
            if (r2 == r4) goto L_0x0132
            int r2 = r2 + 1
            goto L_0x0095
        L_0x012f:
            r2 = 0
            goto L_0x007a
        L_0x0132:
            throw r5
        L_0x0133:
            X.C000300h.A00()
            X.0uI r0 = X.C14880uI.A0J
            com.facebook.fbservice.service.OperationResult r2 = com.facebook.fbservice.service.OperationResult.A00(r0)
        L_0x013c:
            int r1 = X.AnonymousClass1Y3.At1
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.711 r0 = (X.AnonymousClass711) r0
            X.14b r1 = r0.A00
            X.0gA r0 = X.C08870g7.A1S
            r1.AYS(r0)
            return r2
        L_0x014e:
            r0 = r1
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r4)
            return r0
        L_0x0161:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0A(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0B(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            C010708t.A06(C10840kw.A03, "handleBlockUser not implemented yet, succeeding without effect");
        } else if (this instanceof C11070lo) {
            C11070lo r5 = (C11070lo) this;
            if (!((C12420pJ) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Abq, r5.A00)).A02()) {
                return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, r5.A00)).BAz(r7);
            }
            C11110lu r4 = new C11110lu();
            r4.A00(C11120lv.FACEBOOK, (C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, r5.A00), r7);
            r4.A00(C11120lv.TINCAN, (C27311cz) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B9l, r5.A00), r7);
            return C11070lo.A01(r4, C11070lo.A01);
        } else if (this instanceof C11150lz) {
            return r8.BAz(r7);
        } else {
            C12380pF r52 = (C12380pF) this;
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r3 = r52.A00;
            ((C64393Bj) AnonymousClass1XX.A02(2, i, r3)).A03((C184458hF) AnonymousClass1XX.A02(9, AnonymousClass1Y3.A0C, r3), (BlockUserParams) r7.A00.getParcelable("blockUserParams"), C12380pF.A00(r52));
        }
        return OperationResult.A00;
    }

    public OperationResult A0C(C11060ln r14, C27311cz r15) {
        MessagesCollection messagesCollection;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return C11070lo.A00((C11070lo) this, C11070lo.A03(((CreateLocalAdminMessageParams) AnonymousClass0ls.A00(r14, "createLocalAdminMessageParams")).A00.A0U)).BAz(r14);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r3 = (C11150lz) this;
                if (r3 instanceof C12440pM) {
                    C12440pM r32 = (C12440pM) r3;
                    Bundle bundle = r14.A00;
                    Message message = ((CreateLocalAdminMessageParams) bundle.getParcelable("createLocalAdminMessageParams")).A00;
                    long j = bundle.getLong("message_corrected_timestamp_key");
                    if (j > 0) {
                        AnonymousClass1TG A002 = Message.A00();
                        A002.A03(message);
                        A002.A04 = j;
                        message = A002.A00();
                    }
                    ((C52232ij) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BJD, r32.A01)).A0T(message);
                    return OperationResult.A00;
                } else if (r3 instanceof C12370pE) {
                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, ((C12370pE) r3).A00)).A0T(((CreateLocalAdminMessageParams) r14.A00.getParcelable("createLocalAdminMessageParams")).A00);
                    return OperationResult.A00;
                } else if (!(r3 instanceof C11130lx)) {
                    return r15.BAz(r14);
                } else {
                    C11130lx r33 = (C11130lx) r3;
                    Bundle bundle2 = r14.A00;
                    if (bundle2 == null) {
                        return new OperationResult(new NullPointerException("operationParams.getBundle() is null"));
                    }
                    CreateLocalAdminMessageParams createLocalAdminMessageParams = (CreateLocalAdminMessageParams) bundle2.getParcelable("createLocalAdminMessageParams");
                    if (createLocalAdminMessageParams == null || createLocalAdminMessageParams.A00 == null) {
                        return new OperationResult(new NullPointerException("adminMessage is null"));
                    }
                    Bundle bundle3 = r14.A00;
                    boolean z = false;
                    boolean z2 = false;
                    if (bundle3 != null) {
                        z2 = true;
                    }
                    Preconditions.checkArgument(z2);
                    CreateLocalAdminMessageParams createLocalAdminMessageParams2 = (CreateLocalAdminMessageParams) bundle3.getParcelable("createLocalAdminMessageParams");
                    if (!(createLocalAdminMessageParams2 == null || createLocalAdminMessageParams2.A00 == null)) {
                        z = true;
                    }
                    Preconditions.checkArgument(z);
                    ThreadKey threadKey = createLocalAdminMessageParams2.A00.A0U;
                    C60292wu r8 = new C60292wu();
                    r8.A03 = ThreadCriteria.A00(threadKey);
                    r8.A01 = C09510hU.STALE_DATA_OKAY;
                    r8.A00 = 20;
                    FetchThreadParams fetchThreadParams = new FetchThreadParams(r8);
                    Bundle bundle4 = new Bundle();
                    bundle4.putParcelable("fetchThreadParams", fetchThreadParams);
                    C207529qM r4 = new C207529qM();
                    r4.A01(r14);
                    r4.A05 = "fetch_thread";
                    r4.A00 = bundle4;
                    OperationResult A0V = r33.A0V(r4.A00(), r15);
                    if (A0V.success && createLocalAdminMessageParams2.A01 && ((FetchThreadResult) A0V.A07()).A05 == null) {
                        SendMessageByRecipientsParams sendMessageByRecipientsParams = new SendMessageByRecipientsParams(null, createLocalAdminMessageParams2.A00, Collections.singletonList(new UserFbidIdentifier(Long.toString(threadKey.A01))));
                        sendMessageByRecipientsParams.A00 = true;
                        Bundle bundle5 = new Bundle();
                        bundle5.putParcelable("createThreadParams", sendMessageByRecipientsParams);
                        C207529qM r1 = new C207529qM();
                        r1.A01(r14);
                        r1.A05 = "create_thread";
                        r1.A00 = bundle5;
                        A0V = r15.BAz(r1.A00());
                        ((C14800u9) r33.A01.get()).A0G((FetchThreadResult) A0V.A08());
                    }
                    if (!A0V.success) {
                        return A0V;
                    }
                    FetchThreadResult fetchThreadResult = (FetchThreadResult) A0V.A07();
                    ThreadSummary threadSummary = fetchThreadResult.A05;
                    if (threadSummary == null) {
                        return OperationResult.A02(C14880uI.A0E, "empty thread");
                    }
                    Message message2 = createLocalAdminMessageParams.A00;
                    if (!(!ThreadKey.A0E(threadSummary.A0S) || (messagesCollection = fetchThreadResult.A03) == null || messagesCollection.A05() == null)) {
                        bundle2.putLong("message_corrected_timestamp_key", fetchThreadResult.A03.A05().A03 + 1);
                    }
                    OperationResult BAz = r15.BAz(r14);
                    r33.A03.A0Y(message2, null, -1, C72153dk.A02, false);
                    ((C189216c) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B7s, r33.A00)).A0G(message2.A0U, "CacheServiceHandler.handleCreateLocalAdminMessage");
                    return BAz;
                }
            } else {
                throw new AnonymousClass7JR(r14);
            }
        }
    }

    public OperationResult A0D(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r4 = (C11150lz) this;
                if (r4 instanceof C12370pE) {
                    ThreadUpdate threadUpdate = (ThreadUpdate) r7.A00.getParcelable("threadUpdate");
                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, ((C12370pE) r4).A00)).A0b(ImmutableList.of(threadUpdate));
                    return OperationResult.A04(threadUpdate);
                } else if (!(r4 instanceof C11130lx)) {
                    return r8.BAz(r7);
                } else {
                    C11130lx r42 = (C11130lx) r4;
                    boolean A012 = C11130lx.A01(r42);
                    try {
                        ((C14800u9) r42.A01.get()).A02(0, (ThreadUpdate) r7.A00.getParcelable("threadUpdate"), false);
                        OperationResult BAz = r8.BAz(r7);
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r42.A00)).A03();
                        }
                    }
                }
            } else {
                throw new AnonymousClass7JR(r7);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0087, code lost:
        if (r1 <= 1) goto L_0x0089;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0E(X.C11060ln r30, X.C27311cz r31) {
        /*
            r29 = this;
            r1 = r29
            boolean r0 = r1 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0445
            boolean r0 = r1 instanceof X.C11070lo
            r4 = r30
            if (r0 != 0) goto L_0x0402
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x0257
            r7 = r1
            X.0lz r7 = (X.C11150lz) r7
            boolean r0 = r7 instanceof X.C12440pM
            if (r0 != 0) goto L_0x01dd
            boolean r0 = r7 instanceof X.C12370pE
            r3 = r31
            if (r0 != 0) goto L_0x0053
            boolean r0 = r7 instanceof X.C11130lx
            if (r0 != 0) goto L_0x0026
            com.facebook.fbservice.service.OperationResult r4 = r3.BAz(r4)
        L_0x0025:
            return r4
        L_0x0026:
            X.0lx r7 = (X.C11130lx) r7
            boolean r6 = X.C11130lx.A01(r7)
            r5 = 12
            com.facebook.fbservice.service.OperationResult r4 = r3.BAz(r4)     // Catch:{ 1w9 -> 0x0140 }
            java.lang.Object r1 = r4.A08()     // Catch:{ 1w9 -> 0x0140 }
            com.facebook.messaging.service.model.FetchThreadResult r1 = (com.facebook.messaging.service.model.FetchThreadResult) r1     // Catch:{ 1w9 -> 0x0140 }
            X.0US r0 = r7.A01     // Catch:{ 1w9 -> 0x0140 }
            java.lang.Object r0 = r0.get()     // Catch:{ 1w9 -> 0x0140 }
            X.0u9 r0 = (X.C14800u9) r0     // Catch:{ 1w9 -> 0x0140 }
            r0.A0G(r1)     // Catch:{ 1w9 -> 0x0140 }
            if (r6 == 0) goto L_0x0025
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            return r4
        L_0x0053:
            X.0pE r7 = (X.C12370pE) r7
            r6 = 0
            int r1 = X.AnonymousClass1Y3.B4i     // Catch:{ 1w9 -> 0x016a }
            X.0UN r0 = r7.A00     // Catch:{ 1w9 -> 0x016a }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ 1w9 -> 0x016a }
            X.2wn r0 = (X.C60222wn) r0     // Catch:{ 1w9 -> 0x016a }
            r0.A04()     // Catch:{ 1w9 -> 0x016a }
            android.os.Bundle r1 = r4.A00     // Catch:{ 1w9 -> 0x016a }
            java.lang.String r0 = "createThreadParams"
            android.os.Parcelable r0 = r1.getParcelable(r0)     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.service.model.SendMessageByRecipientsParams r0 = (com.facebook.messaging.service.model.SendMessageByRecipientsParams) r0     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.messages.Message r2 = r0.A01     // Catch:{ 1w9 -> 0x016a }
            boolean r0 = r0.A00     // Catch:{ 1w9 -> 0x016a }
            if (r0 == 0) goto L_0x00d1
            if (r2 == 0) goto L_0x00d1
            com.facebook.messaging.model.messages.Publicity r1 = r2.A0L     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.messages.Publicity r0 = com.facebook.messaging.model.messages.Publicity.A02     // Catch:{ 1w9 -> 0x016a }
            if (r1 != r0) goto L_0x00d1
            r3 = 1
            if (r2 == 0) goto L_0x0089
            com.google.common.collect.ImmutableList r0 = r2.A0W     // Catch:{ 1w9 -> 0x016a }
            if (r0 == 0) goto L_0x0089
            int r1 = r0.size()     // Catch:{ 1w9 -> 0x016a }
            r0 = 1
            if (r1 > r3) goto L_0x008a
        L_0x0089:
            r0 = 0
        L_0x008a:
            com.google.common.base.Preconditions.checkArgument(r0)     // Catch:{ 1w9 -> 0x016a }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ 1w9 -> 0x016a }
            r4.<init>()     // Catch:{ 1w9 -> 0x016a }
            com.google.common.collect.ImmutableList$Builder r5 = com.google.common.collect.ImmutableList.builder()     // Catch:{ 1w9 -> 0x016a }
            com.google.common.collect.ImmutableList r0 = r2.A0W     // Catch:{ 1w9 -> 0x016a }
            X.1Xv r9 = r0.iterator()     // Catch:{ 1w9 -> 0x016a }
        L_0x009c:
            boolean r0 = r9.hasNext()     // Catch:{ 1w9 -> 0x016a }
            if (r0 == 0) goto L_0x00d6
            java.lang.Object r8 = r9.next()     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.messages.ParticipantInfo r8 = (com.facebook.messaging.model.messages.ParticipantInfo) r8     // Catch:{ 1w9 -> 0x016a }
            X.1fR r1 = new X.1fR     // Catch:{ 1w9 -> 0x016a }
            r1.<init>()     // Catch:{ 1w9 -> 0x016a }
            r1.A04 = r8     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.threads.ThreadParticipant r0 = new com.facebook.messaging.model.threads.ThreadParticipant     // Catch:{ 1w9 -> 0x016a }
            r0.<init>(r1)     // Catch:{ 1w9 -> 0x016a }
            r4.add(r0)     // Catch:{ 1w9 -> 0x016a }
            X.0cv r3 = new X.0cv     // Catch:{ 1w9 -> 0x016a }
            r3.<init>()     // Catch:{ 1w9 -> 0x016a }
            com.facebook.user.model.UserKey r0 = r8.A01     // Catch:{ 1w9 -> 0x016a }
            X.1aB r1 = r0.type     // Catch:{ 1w9 -> 0x016a }
            java.lang.String r0 = r0.id     // Catch:{ 1w9 -> 0x016a }
            r3.A03(r1, r0)     // Catch:{ 1w9 -> 0x016a }
            java.lang.String r0 = r8.A03     // Catch:{ 1w9 -> 0x016a }
            r3.A0g = r0     // Catch:{ 1w9 -> 0x016a }
            com.facebook.user.model.User r0 = r3.A02()     // Catch:{ 1w9 -> 0x016a }
            r5.add(r0)     // Catch:{ 1w9 -> 0x016a }
            goto L_0x009c
        L_0x00d1:
            com.facebook.fbservice.service.OperationResult r4 = r3.BAz(r4)     // Catch:{ 1w9 -> 0x016a }
            goto L_0x0127
        L_0x00d6:
            X.0zh r1 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ 1w9 -> 0x016a }
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ 1w9 -> 0x016a }
            r1.A0N = r0     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A0U     // Catch:{ 1w9 -> 0x016a }
            r1.A0R = r0     // Catch:{ 1w9 -> 0x016a }
            r1.A03(r4)     // Catch:{ 1w9 -> 0x016a }
            r0 = 1
            r1.A0x = r0     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.threads.ThreadSummary r4 = r1.A00()     // Catch:{ 1w9 -> 0x016a }
            X.1oI r1 = new X.1oI     // Catch:{ 1w9 -> 0x016a }
            r1.<init>()     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A0U     // Catch:{ 1w9 -> 0x016a }
            r1.A00 = r0     // Catch:{ 1w9 -> 0x016a }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r2)     // Catch:{ 1w9 -> 0x016a }
            r1.A01(r0)     // Catch:{ 1w9 -> 0x016a }
            r0 = 1
            r1.A03 = r0     // Catch:{ 1w9 -> 0x016a }
            r1.A02 = r0     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.model.messages.MessagesCollection r3 = r1.A00()     // Catch:{ 1w9 -> 0x016a }
            X.2yX r2 = com.facebook.messaging.service.model.FetchThreadResult.A00()     // Catch:{ 1w9 -> 0x016a }
            com.facebook.fbservice.results.DataFetchDisposition r0 = com.facebook.fbservice.results.DataFetchDisposition.A0I     // Catch:{ 1w9 -> 0x016a }
            r2.A01 = r0     // Catch:{ 1w9 -> 0x016a }
            X.06B r0 = r7.A01     // Catch:{ 1w9 -> 0x016a }
            long r0 = r0.now()     // Catch:{ 1w9 -> 0x016a }
            r2.A00 = r0     // Catch:{ 1w9 -> 0x016a }
            r2.A04 = r4     // Catch:{ 1w9 -> 0x016a }
            r2.A02 = r3     // Catch:{ 1w9 -> 0x016a }
            com.google.common.collect.ImmutableList r0 = r5.build()     // Catch:{ 1w9 -> 0x016a }
            r2.A06 = r0     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.service.model.FetchThreadResult r0 = r2.A00()     // Catch:{ 1w9 -> 0x016a }
            com.facebook.fbservice.service.OperationResult r4 = com.facebook.fbservice.service.OperationResult.A04(r0)     // Catch:{ 1w9 -> 0x016a }
        L_0x0127:
            java.lang.Object r3 = r4.A08()     // Catch:{ 1w9 -> 0x016a }
            com.facebook.messaging.service.model.FetchThreadResult r3 = (com.facebook.messaging.service.model.FetchThreadResult) r3     // Catch:{ 1w9 -> 0x016a }
            if (r3 == 0) goto L_0x0025
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BJD     // Catch:{ 1w9 -> 0x016a }
            X.0UN r0 = r7.A00     // Catch:{ 1w9 -> 0x016a }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ 1w9 -> 0x016a }
            X.2ij r1 = (X.C52232ij) r1     // Catch:{ 1w9 -> 0x016a }
            java.lang.String r0 = "handleCreateThread"
            r1.A0Y(r3, r0)     // Catch:{ 1w9 -> 0x016a }
            goto L_0x0169
        L_0x0140:
            r3 = move-exception
            com.facebook.messaging.model.messages.Message r0 = r3.failedMessage     // Catch:{ all -> 0x0158 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0U     // Catch:{ all -> 0x0158 }
            if (r0 == 0) goto L_0x0157
            X.0US r0 = r7.A01     // Catch:{ all -> 0x0158 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0158 }
            X.0u9 r0 = (X.C14800u9) r0     // Catch:{ all -> 0x0158 }
            com.facebook.messaging.model.messages.Message r2 = r3.failedMessage     // Catch:{ all -> 0x0158 }
            X.0m6 r1 = r0.A01     // Catch:{ all -> 0x0158 }
            r0 = 1
            r1.A0Z(r2, r0)     // Catch:{ all -> 0x0158 }
        L_0x0157:
            throw r3     // Catch:{ all -> 0x0158 }
        L_0x0158:
            r2 = move-exception
            if (r6 == 0) goto L_0x0168
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x0168:
            throw r2
        L_0x0169:
            return r4
        L_0x016a:
            r5 = move-exception
            com.facebook.messaging.model.messages.Message r4 = r5.failedMessage
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r4.A0U
            if (r3 == 0) goto L_0x01dc
            X.1fF r1 = r3.A05
            X.1fF r0 = X.C28711fF.ONE_TO_ONE
            if (r1 != r0) goto L_0x01dc
            int r2 = X.AnonymousClass1Y3.AcJ
            X.0UN r0 = r7.A00
            r1 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r2, r0)
            X.1br r0 = (X.C26691br) r0
            com.facebook.messaging.service.model.FetchThreadResult r2 = r0.A0F(r3, r1)
            if (r2 == 0) goto L_0x01dc
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.A05
            if (r0 == 0) goto L_0x01dc
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            if (r0 == 0) goto L_0x01dc
            X.1TG r1 = com.facebook.messaging.model.messages.Message.A00()
            r1.A03(r4)
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.A05
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            r1.A0T = r0
            com.facebook.messaging.model.messages.Message r4 = r1.A00()
            com.facebook.messaging.model.send.SendError r0 = r4.A0S
            X.1u4 r0 = r0.A02
            boolean r0 = r0.shouldNotBeRetried
            java.lang.String r3 = "DbServiceHandler"
            r2 = 8
            if (r0 == 0) goto L_0x01ce
            int r1 = X.AnonymousClass1Y3.B7s
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.16c r0 = (X.C189216c) r0
            r0.A0A(r4, r3)
        L_0x01ba:
            int r1 = X.AnonymousClass1Y3.B4i
            X.0UN r0 = r7.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.2wn r1 = (X.C60222wn) r1
            r0 = 1
            r1.A05(r4, r0)
            X.1w9 r0 = new X.1w9
            r0.<init>(r5, r4)
            throw r0
        L_0x01ce:
            int r1 = X.AnonymousClass1Y3.B7s
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.16c r0 = (X.C189216c) r0
            r0.A0B(r4, r3)
            goto L_0x01ba
        L_0x01dc:
            throw r5
        L_0x01dd:
            X.0pM r7 = (X.C12440pM) r7
            java.lang.String r1 = "SmsServiceHandler.handleCreateThread"
            r0 = 1563958687(0x5d381d9f, float:8.2918343E17)
            X.C005505z.A03(r1, r0)
            android.os.Bundle r1 = r4.A00     // Catch:{ all -> 0x024f }
            java.lang.String r0 = "createThreadParams"
            android.os.Parcelable r4 = r1.getParcelable(r0)     // Catch:{ all -> 0x024f }
            com.facebook.messaging.service.model.SendMessageByRecipientsParams r4 = (com.facebook.messaging.service.model.SendMessageByRecipientsParams) r4     // Catch:{ all -> 0x024f }
            java.util.HashSet r2 = new java.util.HashSet     // Catch:{ all -> 0x024f }
            com.google.common.collect.ImmutableList r0 = r4.A02     // Catch:{ all -> 0x024f }
            int r0 = r0.size()     // Catch:{ all -> 0x024f }
            r2.<init>(r0)     // Catch:{ all -> 0x024f }
            com.google.common.collect.ImmutableList r0 = r4.A02     // Catch:{ all -> 0x024f }
            X.1Xv r1 = r0.iterator()     // Catch:{ all -> 0x024f }
        L_0x0202:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x024f }
            if (r0 == 0) goto L_0x0216
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x024f }
            com.facebook.user.model.UserIdentifier r0 = (com.facebook.user.model.UserIdentifier) r0     // Catch:{ all -> 0x024f }
            com.facebook.user.model.UserSmsIdentifier r0 = (com.facebook.user.model.UserSmsIdentifier) r0     // Catch:{ all -> 0x024f }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x024f }
            r2.add(r0)     // Catch:{ all -> 0x024f }
            goto L_0x0202
        L_0x0216:
            android.content.Context r0 = r7.A00     // Catch:{ all -> 0x024f }
            long r2 = X.AnonymousClass7I7.A00(r0, r2)     // Catch:{ all -> 0x024f }
            X.1TG r1 = com.facebook.messaging.model.messages.Message.A00()     // Catch:{ all -> 0x024f }
            com.facebook.messaging.model.messages.Message r0 = r4.A01     // Catch:{ all -> 0x024f }
            r1.A03(r0)     // Catch:{ all -> 0x024f }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A03(r2)     // Catch:{ all -> 0x024f }
            r1.A0T = r0     // Catch:{ all -> 0x024f }
            com.facebook.messaging.model.messages.Message r5 = r1.A00()     // Catch:{ all -> 0x024f }
            r4 = 3
            int r1 = X.AnonymousClass1Y3.BKF     // Catch:{ all -> 0x024f }
            X.0UN r0 = r7.A01     // Catch:{ all -> 0x024f }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x024f }
            X.9NT r1 = (X.AnonymousClass9NT) r1     // Catch:{ all -> 0x024f }
            r0 = 0
            r1.A01(r5, r0)     // Catch:{ all -> 0x024f }
            r0 = 20
            com.facebook.messaging.service.model.FetchThreadResult r0 = X.C12440pM.A01(r7, r2, r0)     // Catch:{ all -> 0x024f }
            com.facebook.fbservice.service.OperationResult r4 = com.facebook.fbservice.service.OperationResult.A04(r0)     // Catch:{ all -> 0x024f }
            r0 = -939391153(0xffffffffc802074f, float:-133149.23)
            X.C005505z.A00(r0)
            return r4
        L_0x024f:
            r1 = move-exception
            r0 = -525425338(0xffffffffe0aea546, float:-1.0067633E20)
            X.C005505z.A00(r0)
            throw r1
        L_0x0257:
            r9 = r1
            X.0pF r9 = (X.C12380pF) r9
            android.os.Bundle r1 = r4.A00
            java.lang.String r0 = "createThreadParams"
            android.os.Parcelable r5 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.SendMessageByRecipientsParams r5 = (com.facebook.messaging.service.model.SendMessageByRecipientsParams) r5
            r2 = 20
            int r1 = X.AnonymousClass1Y3.A3t     // Catch:{ all -> 0x03ea }
            X.0UN r0 = r9.A00     // Catch:{ all -> 0x03ea }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x03ea }
            X.B5r r0 = (X.B5r) r0     // Catch:{ all -> 0x03ea }
            com.facebook.messaging.model.messages.Message r1 = r5.A01     // Catch:{ all -> 0x03ea }
            X.1mR r2 = r0.A0G     // Catch:{ all -> 0x03ea }
            X.ErF r8 = r2.A02(r1)     // Catch:{ all -> 0x03ea }
            X.2rR r4 = r0.A0D     // Catch:{ all -> 0x0376 }
            java.lang.Integer r2 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0376 }
            r3 = 0
            java.lang.String r17 = "offline_threading_id"
            r10 = r17
            java.lang.String r11 = r1.A0w     // Catch:{ all -> 0x0376 }
            java.lang.String r25 = "current_time"
            r12 = r25
            java.lang.String r13 = X.C57112rR.A02(r4)     // Catch:{ all -> 0x0376 }
            java.lang.String r27 = "channel"
            r14 = r27
            java.lang.String r15 = X.C206839oz.A00(r2)     // Catch:{ all -> 0x0376 }
            java.lang.String[] r2 = new java.lang.String[]{r10, r11, r12, r13, r14, r15}     // Catch:{ all -> 0x0376 }
            java.util.Map r13 = X.C57172rY.A01(r2)     // Catch:{ all -> 0x0376 }
            X.C57112rR.A05(r13, r8, r3)     // Catch:{ all -> 0x0376 }
            X.2rY r2 = r4.A05     // Catch:{ all -> 0x0376 }
            r2.A03(r13)     // Catch:{ all -> 0x0376 }
            X.2qj r10 = r4.A03     // Catch:{ all -> 0x0376 }
            java.lang.String r11 = "msg_create_thread_attempted"
            r12 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r10.A08(r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x0376 }
            X.069 r2 = r0.A04     // Catch:{ all -> 0x0376 }
            long r14 = r2.now()     // Catch:{ all -> 0x0376 }
            X.0US r2 = r0.A0A     // Catch:{ all -> 0x0374 }
            java.lang.Object r7 = r2.get()     // Catch:{ all -> 0x0374 }
            X.B5X r7 = (X.B5X) r7     // Catch:{ all -> 0x0374 }
            X.3Bj r2 = r7.A01     // Catch:{ all -> 0x0374 }
            X.3fs r6 = r2.A01()     // Catch:{ all -> 0x0374 }
            X.B5V r2 = r7.A0A     // Catch:{ all -> 0x0374 }
            X.3fu r2 = X.C73323ft.A00(r2, r5)     // Catch:{ all -> 0x0374 }
            java.lang.String r10 = "create-thread"
            r2.A02 = r10     // Catch:{ all -> 0x0374 }
            X.3ft r2 = r2.A00()     // Catch:{ all -> 0x0374 }
            r6.A00(r2)     // Catch:{ all -> 0x0374 }
            X.2wu r4 = new X.2wu     // Catch:{ all -> 0x0374 }
            r4.<init>()     // Catch:{ all -> 0x0374 }
            X.0hU r2 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA     // Catch:{ all -> 0x0374 }
            r4.A01 = r2     // Catch:{ all -> 0x0374 }
            java.lang.String r11 = "{result=create-thread:$.thread_fbid}"
            com.facebook.messaging.model.threads.ThreadCriteria r3 = new com.facebook.messaging.model.threads.ThreadCriteria     // Catch:{ all -> 0x0374 }
            com.google.common.base.Preconditions.checkNotNull(r11)     // Catch:{ all -> 0x0374 }
            java.util.List r2 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0374 }
            r3.<init>(r11, r2)     // Catch:{ all -> 0x0374 }
            r4.A03 = r3     // Catch:{ all -> 0x0374 }
            X.0US r2 = r7.A03     // Catch:{ all -> 0x0374 }
            java.lang.Object r3 = r2.get()     // Catch:{ all -> 0x0374 }
            X.0kt r3 = (X.C10810kt) r3     // Catch:{ all -> 0x0374 }
            com.facebook.messaging.service.model.FetchThreadParams r2 = new com.facebook.messaging.service.model.FetchThreadParams     // Catch:{ all -> 0x0374 }
            r2.<init>(r4)     // Catch:{ all -> 0x0374 }
            X.3fu r2 = X.C73323ft.A00(r3, r2)     // Catch:{ all -> 0x0374 }
            java.lang.String r4 = "fetch-thread"
            r2.A02 = r4     // Catch:{ all -> 0x0374 }
            r2.A01 = r10     // Catch:{ all -> 0x0374 }
            X.3ft r2 = r2.A00()     // Catch:{ all -> 0x0374 }
            r6.A00(r2)     // Catch:{ all -> 0x0374 }
            java.lang.Class r2 = r7.getClass()     // Catch:{ all -> 0x0374 }
            com.facebook.common.callercontext.CallerContext r3 = com.facebook.common.callercontext.CallerContext.A04(r2)     // Catch:{ all -> 0x0374 }
            java.lang.String r2 = "createThread"
            r6.A01(r2, r3)     // Catch:{ all -> 0x0374 }
            java.util.Map r2 = r6.A06     // Catch:{ all -> 0x0374 }
            java.lang.Object r2 = r2.get(r4)     // Catch:{ all -> 0x0374 }
            com.facebook.messaging.service.model.FetchThreadResult r2 = (com.facebook.messaging.service.model.FetchThreadResult) r2     // Catch:{ all -> 0x0374 }
            X.2rR r4 = r0.A0D     // Catch:{ all -> 0x0374 }
            com.facebook.messaging.model.threads.ThreadSummary r3 = r2.A05     // Catch:{ all -> 0x0374 }
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r3.A0S     // Catch:{ all -> 0x0374 }
            X.069 r6 = r0.A04     // Catch:{ all -> 0x0374 }
            long r12 = r6.now()     // Catch:{ all -> 0x0374 }
            long r12 = r12 - r14
            java.lang.Integer r11 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0374 }
            java.lang.String r10 = r1.A0w     // Catch:{ all -> 0x0374 }
            java.lang.String r19 = "sent_timestamp_ms"
            long r6 = r1.A02     // Catch:{ all -> 0x0374 }
            java.lang.String r20 = java.lang.Long.toString(r6)     // Catch:{ all -> 0x0374 }
            java.lang.String r21 = "send_time_delta"
            java.lang.String r22 = java.lang.Long.toString(r12)     // Catch:{ all -> 0x0374 }
            java.lang.String r23 = "total_attachment_size"
            long r6 = r8.A08     // Catch:{ all -> 0x0374 }
            java.lang.String r24 = java.lang.Long.toString(r6)     // Catch:{ all -> 0x0374 }
            java.lang.String r26 = X.C57112rR.A02(r4)     // Catch:{ all -> 0x0374 }
            java.lang.String r28 = X.C206839oz.A00(r11)     // Catch:{ all -> 0x0374 }
            r18 = r10
            java.lang.String[] r6 = new java.lang.String[]{r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28}     // Catch:{ all -> 0x0374 }
            java.util.Map r6 = X.C57172rY.A01(r6)     // Catch:{ all -> 0x0374 }
            X.C57112rR.A06(r6, r3)     // Catch:{ all -> 0x0374 }
            X.2qj r3 = r4.A03     // Catch:{ all -> 0x0374 }
            java.lang.String r17 = "create_thread"
            java.lang.String r18 = "success"
            r20 = 0
            r21 = 0
            r22 = 0
            r19 = r6
            r16 = r3
            r16.A08(r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x0374 }
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r2)     // Catch:{ all -> 0x03ea }
            return r0
        L_0x0374:
            r6 = move-exception
            goto L_0x037c
        L_0x0376:
            r6 = move-exception
            r14 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x037c:
            X.0US r2 = r0.A09     // Catch:{ all -> 0x03ea }
            java.lang.Object r4 = r2.get()     // Catch:{ all -> 0x03ea }
            X.B5s r4 = (X.B5s) r4     // Catch:{ all -> 0x03ea }
            X.1R6 r3 = X.AnonymousClass1R6.UNKNOWN     // Catch:{ all -> 0x03ea }
            java.lang.String r2 = "create_thread_via_graph"
            X.1w9 r3 = r4.A01(r6, r2, r1, r3)     // Catch:{ all -> 0x03ea }
            X.2rR r2 = r0.A0D     // Catch:{ all -> 0x03ea }
            X.069 r0 = r0.A04     // Catch:{ all -> 0x03ea }
            long r10 = r0.now()     // Catch:{ all -> 0x03ea }
            long r10 = r10 - r14
            java.lang.Integer r6 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x03ea }
            X.2qj r4 = r2.A03     // Catch:{ all -> 0x03ea }
            java.lang.String r12 = "offline_threading_id"
            java.lang.String r13 = r1.A0w     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = "sent_timestamp_ms"
            long r0 = r1.A02     // Catch:{ all -> 0x03ea }
            java.lang.String r15 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x03ea }
            java.lang.String r16 = "send_time_delta"
            java.lang.String r17 = java.lang.Long.toString(r10)     // Catch:{ all -> 0x03ea }
            java.lang.String r18 = "error_type"
            com.facebook.messaging.model.messages.Message r0 = r3.failedMessage     // Catch:{ all -> 0x03ea }
            com.facebook.messaging.model.send.SendError r0 = r0.A0S     // Catch:{ all -> 0x03ea }
            X.1u4 r0 = r0.A02     // Catch:{ all -> 0x03ea }
            java.lang.String r19 = r0.toString()     // Catch:{ all -> 0x03ea }
            java.lang.String r20 = "error_message"
            java.lang.String r21 = r3.getMessage()     // Catch:{ all -> 0x03ea }
            java.lang.String r22 = "total_attachment_size"
            long r0 = r8.A08     // Catch:{ all -> 0x03ea }
            java.lang.String r23 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x03ea }
            java.lang.String r24 = "current_time"
            java.lang.String r25 = X.C57112rR.A02(r2)     // Catch:{ all -> 0x03ea }
            java.lang.String r26 = "channel"
            java.lang.String r27 = X.C206839oz.A00(r6)     // Catch:{ all -> 0x03ea }
            java.lang.String[] r0 = new java.lang.String[]{r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27}     // Catch:{ all -> 0x03ea }
            java.util.Map r12 = X.C57172rY.A01(r0)     // Catch:{ all -> 0x03ea }
            java.lang.String r10 = "create_thread"
            java.lang.String r11 = "failed"
            r13 = 0
            r14 = 0
            r15 = 0
            X.0nb r1 = X.C56722qj.A01(r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x03ea }
            com.facebook.analytics.DeprecatedAnalyticsLogger r0 = r4.A01     // Catch:{ all -> 0x03ea }
            r0.A07(r1)     // Catch:{ all -> 0x03ea }
            throw r3     // Catch:{ all -> 0x03ea }
        L_0x03ea:
            r4 = move-exception
            r2 = 27
            int r1 = X.AnonymousClass1Y3.AYO
            X.0UN r0 = r9.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.B5s r3 = (X.B5s) r3
            com.facebook.messaging.model.messages.Message r2 = r5.A01
            X.1R6 r1 = X.AnonymousClass1R6.UNKNOWN
            java.lang.String r0 = "create_thread"
            X.1w9 r0 = r3.A01(r4, r0, r2, r1)
            throw r0
        L_0x0402:
            r3 = r1
            X.0lo r3 = (X.C11070lo) r3
            java.lang.String r0 = "createThreadParams"
            android.os.Parcelable r0 = X.AnonymousClass0ls.A00(r4, r0)
            com.facebook.messaging.service.model.SendMessageByRecipientsParams r0 = (com.facebook.messaging.service.model.SendMessageByRecipientsParams) r0
            com.google.common.collect.ImmutableList r0 = r0.A02
            r1 = r0
            if (r0 == 0) goto L_0x042c
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x042c
            X.1Xv r1 = r1.iterator()
        L_0x041c:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0443
            java.lang.Object r0 = r1.next()
            com.facebook.user.model.UserIdentifier r0 = (com.facebook.user.model.UserIdentifier) r0
            boolean r0 = r0 instanceof com.facebook.user.model.UserSmsIdentifier
            if (r0 != 0) goto L_0x041c
        L_0x042c:
            r0 = 0
        L_0x042d:
            if (r0 == 0) goto L_0x043f
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BQB
        L_0x0432:
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r4)
            return r0
        L_0x043f:
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A49
            goto L_0x0432
        L_0x0443:
            r0 = 1
            goto L_0x042d
        L_0x0445:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0E(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0F(C11060ln r5, C27311cz r6) {
        if (this instanceof C10840kw) {
            C10840kw r3 = (C10840kw) this;
            ImmutableSet A06 = ((C193917y) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AI2, r3.A00)).A06();
            C10840kw.A01(r3, A06);
            return OperationResult.A04(new DeleteAllTincanThreadsResult(A06));
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B9l, ((C11070lo) this).A00)).BAz(r5);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r1 = (C11150lz) this;
                if (!(r1 instanceof C11130lx)) {
                    return r6.BAz(r5);
                }
                OperationResult BAz = r6.BAz(r5);
                ((C14800u9) ((C11130lx) r1).A01.get()).A08(C10950l8.A05, ImmutableList.copyOf((Collection) ((DeleteAllTincanThreadsResult) BAz.A07()).A00));
                return BAz;
            }
            throw new AnonymousClass7JR(r5);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x018b, code lost:
        if (r7.startsWith("smsid:") != false) goto L_0x018d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0238, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0239, code lost:
        X.C005505z.A00(288529750);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x023f, code lost:
        throw r1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0G(X.C11060ln r20, X.C27311cz r21) {
        /*
            r19 = this;
            r1 = r19
            boolean r0 = r1 instanceof X.C10840kw
            r8 = r20
            if (r0 != 0) goto L_0x0294
            boolean r0 = r1 instanceof X.C11070lo
            if (r0 != 0) goto L_0x026c
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x0240
            r0 = r1
            X.0lz r0 = (X.C11150lz) r0
            boolean r1 = r0 instanceof X.C12440pM
            if (r1 != 0) goto L_0x0124
            boolean r1 = r0 instanceof X.C12370pE
            r4 = r21
            if (r1 != 0) goto L_0x00ae
            boolean r1 = r0 instanceof X.C11130lx
            if (r1 != 0) goto L_0x0026
            com.facebook.fbservice.service.OperationResult r10 = r4.BAz(r8)
        L_0x0025:
            return r10
        L_0x0026:
            X.0lx r0 = (X.C11130lx) r0
            java.lang.String r6 = "CacheServiceHandler.handleDeleteMessages"
            boolean r11 = X.C11130lx.A01(r0)
            r3 = 12
            com.facebook.fbservice.service.OperationResult r10 = r4.BAz(r8)     // Catch:{ all -> 0x009d }
            java.lang.Object r9 = r10.A08()     // Catch:{ all -> 0x009d }
            com.facebook.messaging.service.model.DeleteMessagesResult r9 = (com.facebook.messaging.service.model.DeleteMessagesResult) r9     // Catch:{ all -> 0x009d }
            com.facebook.messaging.model.threadkey.ThreadKey r8 = r9.A00     // Catch:{ all -> 0x009d }
            if (r8 == 0) goto L_0x008d
            X.0m6 r1 = r0.A03     // Catch:{ all -> 0x009d }
            com.facebook.messaging.model.threads.ThreadSummary r7 = r1.A0R(r8)     // Catch:{ all -> 0x009d }
            if (r7 == 0) goto L_0x008d
            X.0US r1 = r0.A01     // Catch:{ all -> 0x009d }
            java.lang.Object r2 = r1.get()     // Catch:{ all -> 0x009d }
            X.0u9 r2 = (X.C14800u9) r2     // Catch:{ all -> 0x009d }
            X.0l8 r1 = r7.A0O     // Catch:{ all -> 0x009d }
            r2.A07(r1, r9)     // Catch:{ all -> 0x009d }
            int r2 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x009d }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x009d }
            r5 = 3
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ all -> 0x009d }
            X.16c r4 = (X.C189216c) r4     // Catch:{ all -> 0x009d }
            com.google.common.collect.ImmutableSet r2 = r9.A03     // Catch:{ all -> 0x009d }
            com.google.common.collect.ImmutableMap r1 = r9.A02     // Catch:{ all -> 0x009d }
            com.google.common.collect.ImmutableCollection r1 = r1.values()     // Catch:{ all -> 0x009d }
            r4.A0I(r8, r2, r1, r6)     // Catch:{ all -> 0x009d }
            boolean r1 = r9.A05     // Catch:{ all -> 0x009d }
            if (r1 != 0) goto L_0x007a
            int r2 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x009d }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x009d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ all -> 0x009d }
            X.16c r1 = (X.C189216c) r1     // Catch:{ all -> 0x009d }
            r1.A0G(r8, r6)     // Catch:{ all -> 0x009d }
        L_0x007a:
            X.0l8 r2 = r7.A0O     // Catch:{ all -> 0x009d }
            X.0l8 r1 = X.C10950l8.A06     // Catch:{ all -> 0x009d }
            if (r2 != r1) goto L_0x008d
            int r2 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x009d }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x009d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ all -> 0x009d }
            X.16c r1 = (X.C189216c) r1     // Catch:{ all -> 0x009d }
            r1.A09()     // Catch:{ all -> 0x009d }
        L_0x008d:
            if (r11 == 0) goto L_0x0025
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            return r10
        L_0x009d:
            r2 = move-exception
            if (r11 == 0) goto L_0x00ad
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x00ad:
            throw r2
        L_0x00ae:
            X.0pE r0 = (X.C12370pE) r0
            android.os.Bundle r1 = r8.A00
            java.lang.String r5 = "DeleteMessagesParams"
            android.os.Parcelable r7 = r1.getParcelable(r5)
            com.facebook.messaging.service.model.DeleteMessagesParams r7 = (com.facebook.messaging.service.model.DeleteMessagesParams) r7
            com.google.common.base.Preconditions.checkNotNull(r7)
            int r2 = X.AnonymousClass1Y3.BJD
            X.0UN r1 = r0.A00
            r0 = 2
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2ij r6 = (X.C52232ij) r6
            r8 = -1
            r10 = 0
            r11 = 0
            com.facebook.messaging.service.model.DeleteMessagesResult r6 = r6.A0O(r7, r8, r10, r11)
            X.0dQ r3 = com.google.common.collect.ImmutableSet.A01()
            java.lang.Integer r1 = r7.A03
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x00fa
            com.google.common.collect.ImmutableSet r0 = r6.A03
            if (r0 == 0) goto L_0x00fa
            X.1Xv r2 = r0.iterator()
        L_0x00e2:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00fa
            java.lang.Object r1 = r2.next()
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x00e2
            boolean r0 = X.AnonymousClass4JE.A03(r1)
            if (r0 != 0) goto L_0x00e2
            r3.A01(r1)
            goto L_0x00e2
        L_0x00fa:
            com.google.common.collect.ImmutableSet r2 = r3.build()
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x011f
            com.facebook.messaging.service.model.DeleteMessagesParams r3 = new com.facebook.messaging.service.model.DeleteMessagesParams
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A00
            r3.<init>(r2, r1, r0)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            r2.putParcelable(r5, r3)
            X.0ln r1 = new X.0ln
            java.lang.String r0 = "delete_messages"
            r1.<init>(r0, r2)
            r4.BAz(r1)
        L_0x011f:
            com.facebook.fbservice.service.OperationResult r10 = com.facebook.fbservice.service.OperationResult.A04(r6)
            return r10
        L_0x0124:
            X.0pM r0 = (X.C12440pM) r0
            java.lang.String r2 = "SmsServiceHandler.handleDeleteMessages"
            r1 = -564672340(0xffffffffde57c8ac, float:-3.88721671E18)
            X.C005505z.A03(r2, r1)
            android.os.Bundle r2 = r8.A00     // Catch:{ all -> 0x0238 }
            java.lang.String r1 = "DeleteMessagesParams"
            android.os.Parcelable r5 = r2.getParcelable(r1)     // Catch:{ all -> 0x0238 }
            com.facebook.messaging.service.model.DeleteMessagesParams r5 = (com.facebook.messaging.service.model.DeleteMessagesParams) r5     // Catch:{ all -> 0x0238 }
            com.google.common.collect.ImmutableSet r1 = r5.A01     // Catch:{ all -> 0x0238 }
            X.1Xv r8 = r1.iterator()     // Catch:{ all -> 0x0238 }
        L_0x013e:
            boolean r1 = r8.hasNext()     // Catch:{ all -> 0x0238 }
            r2 = 1
            if (r1 == 0) goto L_0x01b8
            java.lang.Object r7 = r8.next()     // Catch:{ all -> 0x0238 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x0238 }
            java.lang.String r1 = "sent."
            boolean r1 = r7.startsWith(r1)     // Catch:{ all -> 0x0238 }
            if (r1 == 0) goto L_0x0171
            r4 = 6
            int r3 = X.AnonymousClass1Y3.AHr     // Catch:{ all -> 0x0238 }
            X.0UN r1 = r0.A01     // Catch:{ all -> 0x0238 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r3, r1)     // Catch:{ all -> 0x0238 }
            X.4MK r4 = (X.AnonymousClass4MK) r4     // Catch:{ all -> 0x0238 }
            r1 = 5
            java.lang.String r3 = r7.substring(r1)     // Catch:{ all -> 0x0238 }
            monitor-enter(r4)     // Catch:{ all -> 0x0238 }
            X.1xX r1 = r4.A00     // Catch:{ all -> 0x01b5 }
            X.1xX r1 = r1.BDR()     // Catch:{ all -> 0x01b5 }
            java.lang.Object r7 = r1.get(r3)     // Catch:{ all -> 0x01b5 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x01b5 }
            monitor-exit(r4)     // Catch:{ all -> 0x0238 }
        L_0x0171:
            int r3 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x0238 }
            X.0UN r1 = r0.A01     // Catch:{ all -> 0x0238 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r3, r1)     // Catch:{ all -> 0x0238 }
            X.2e4 r6 = (X.C50462e4) r6     // Catch:{ all -> 0x0238 }
            java.lang.String r1 = "mmsid:"
            boolean r1 = r7.startsWith(r1)     // Catch:{ all -> 0x0238 }
            r4 = 0
            if (r1 != 0) goto L_0x018d
            java.lang.String r1 = "smsid:"
            boolean r2 = r7.startsWith(r1)     // Catch:{ all -> 0x0238 }
            r1 = 0
            if (r2 == 0) goto L_0x018e
        L_0x018d:
            r1 = 1
        L_0x018e:
            com.google.common.base.Preconditions.checkArgument(r1)     // Catch:{ all -> 0x0238 }
            java.lang.String r1 = "smsid:"
            boolean r1 = r7.startsWith(r1)     // Catch:{ all -> 0x0238 }
            if (r1 == 0) goto L_0x01b0
            android.net.Uri r3 = X.AnonymousClass9NS.A01(r7)     // Catch:{ all -> 0x0238 }
        L_0x019d:
            int r2 = X.AnonymousClass1Y3.BCt     // Catch:{ all -> 0x0238 }
            X.0UN r1 = r6.A00     // Catch:{ all -> 0x0238 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ all -> 0x0238 }
            android.content.Context r1 = (android.content.Context) r1     // Catch:{ all -> 0x0238 }
            android.content.ContentResolver r2 = r1.getContentResolver()     // Catch:{ all -> 0x0238 }
            r1 = 0
            r2.delete(r3, r1, r1)     // Catch:{ all -> 0x0238 }
            goto L_0x013e
        L_0x01b0:
            android.net.Uri r3 = X.AnonymousClass9NS.A00(r7)     // Catch:{ all -> 0x0238 }
            goto L_0x019d
        L_0x01b5:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0238 }
            throw r0     // Catch:{ all -> 0x0238 }
        L_0x01b8:
            com.facebook.messaging.model.threadkey.ThreadKey r7 = r5.A00     // Catch:{ all -> 0x0238 }
            r1 = 0
            r12 = 0
            if (r7 == 0) goto L_0x0202
            int r4 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x0238 }
            X.0UN r3 = r0.A01     // Catch:{ all -> 0x0238 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r4, r3)     // Catch:{ all -> 0x0238 }
            X.2e4 r6 = (X.C50462e4) r6     // Catch:{ all -> 0x0238 }
            long r3 = r7.A0G()     // Catch:{ all -> 0x0238 }
            com.facebook.messaging.model.threads.ThreadSummary r12 = r6.A0B(r3, r12)     // Catch:{ all -> 0x0238 }
            r6 = 2
            int r4 = X.AnonymousClass1Y3.AJF     // Catch:{ all -> 0x0238 }
            X.0UN r3 = r0.A01     // Catch:{ all -> 0x0238 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r6, r4, r3)     // Catch:{ all -> 0x0238 }
            X.9NU r6 = (X.AnonymousClass9NU) r6     // Catch:{ all -> 0x0238 }
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r5.A00     // Catch:{ all -> 0x0238 }
            long r7 = r3.A0G()     // Catch:{ all -> 0x0238 }
            r9 = 1
            r10 = -1
            com.google.common.collect.ImmutableList r3 = r6.A0A(r7, r9, r10)     // Catch:{ all -> 0x0238 }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x0238 }
            if (r3 == 0) goto L_0x01fc
            int r3 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x0238 }
            X.0UN r0 = r0.A01     // Catch:{ all -> 0x0238 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r3, r0)     // Catch:{ all -> 0x0238 }
            X.2e4 r0 = (X.C50462e4) r0     // Catch:{ all -> 0x0238 }
            r0.A0J(r2)     // Catch:{ all -> 0x0238 }
            goto L_0x01fd
        L_0x01fc:
            r2 = 0
        L_0x01fd:
            if (r12 != 0) goto L_0x0214
            if (r2 == 0) goto L_0x0214
            goto L_0x0204
        L_0x0202:
            r2 = 0
            goto L_0x0214
        L_0x0204:
            X.0zh r3 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x0238 }
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ all -> 0x0238 }
            r3.A0N = r0     // Catch:{ all -> 0x0238 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A00     // Catch:{ all -> 0x0238 }
            r3.A0R = r0     // Catch:{ all -> 0x0238 }
            com.facebook.messaging.model.threads.ThreadSummary r12 = r3.A00()     // Catch:{ all -> 0x0238 }
        L_0x0214:
            com.facebook.messaging.service.model.DeleteMessagesResult r11 = new com.facebook.messaging.service.model.DeleteMessagesResult     // Catch:{ all -> 0x0238 }
            com.facebook.messaging.model.threadkey.ThreadKey r13 = r5.A00     // Catch:{ all -> 0x0238 }
            com.google.common.collect.ImmutableSet r14 = r5.A01     // Catch:{ all -> 0x0238 }
            java.util.HashMap r15 = new java.util.HashMap     // Catch:{ all -> 0x0238 }
            r15.<init>(r1)     // Catch:{ all -> 0x0238 }
            java.util.HashSet r0 = new java.util.HashSet     // Catch:{ all -> 0x0238 }
            r0.<init>(r1)     // Catch:{ all -> 0x0238 }
            r18 = 0
            r17 = r2
            r16 = r0
            r11.<init>(r12, r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x0238 }
            com.facebook.fbservice.service.OperationResult r10 = com.facebook.fbservice.service.OperationResult.A04(r11)     // Catch:{ all -> 0x0238 }
            r0 = 1565724509(0x5d530f5d, float:9.505298E17)
            X.C005505z.A00(r0)
            return r10
        L_0x0238:
            r1 = move-exception
            r0 = 288529750(0x11329d56, float:1.4090207E-28)
            X.C005505z.A00(r0)
            throw r1
        L_0x0240:
            r5 = r1
            X.0pF r5 = (X.C12380pF) r5
            android.os.Bundle r1 = r8.A00
            java.lang.String r0 = "DeleteMessagesParams"
            android.os.Parcelable r4 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.DeleteMessagesParams r4 = (com.facebook.messaging.service.model.DeleteMessagesParams) r4
            int r1 = X.AnonymousClass1Y3.BAS
            X.0UN r3 = r5.A00
            r0 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.3Bj r2 = (X.C64393Bj) r2
            int r1 = X.AnonymousClass1Y3.Alf
            r0 = 11
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.4z6 r1 = (X.C104534z6) r1
            X.2Xl r0 = X.C12380pF.A00(r5)
            r2.A03(r1, r4, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A00
            return r0
        L_0x026c:
            r3 = r1
            X.0lo r3 = (X.C11070lo) r3
            java.lang.String r0 = "DeleteMessagesParams"
            android.os.Parcelable r0 = X.AnonymousClass0ls.A00(r8, r0)
            com.facebook.messaging.service.model.DeleteMessagesParams r0 = (com.facebook.messaging.service.model.DeleteMessagesParams) r0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A00
            if (r0 != 0) goto L_0x028b
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A49
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1cz r0 = (X.C27311cz) r0
        L_0x0286:
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r8)
            return r0
        L_0x028b:
            X.0lv r0 = X.C11070lo.A03(r0)
            X.1cz r0 = X.C11070lo.A00(r3, r0)
            goto L_0x0286
        L_0x0294:
            r6 = r1
            X.0kw r6 = (X.C10840kw) r6
            java.lang.String r0 = "DeleteMessagesParams"
            android.os.Parcelable r2 = X.AnonymousClass0ls.A00(r8, r0)
            com.facebook.messaging.service.model.DeleteMessagesParams r2 = (com.facebook.messaging.service.model.DeleteMessagesParams) r2
            com.facebook.messaging.model.threadkey.ThreadKey r13 = r2.A00
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r13)
            com.google.common.base.Preconditions.checkArgument(r0)
            com.google.common.collect.ImmutableMap$Builder r5 = com.google.common.collect.ImmutableMap.builder()
            X.0dQ r3 = com.google.common.collect.ImmutableSet.A01()
            int r4 = X.AnonymousClass1Y3.AXC
            X.0UN r1 = r6.A00
            r0 = 3
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r4, r1)
            X.1i3 r4 = (X.C30441i3) r4
            com.google.common.collect.ImmutableSet r1 = r2.A01
            X.0W6 r0 = X.C195818v.A05
            java.lang.String r0 = r0.A00
            X.0av r0 = X.C06160ax.A04(r0, r1)
            com.google.common.collect.ImmutableSet r7 = X.C30441i3.A06(r4, r0)
            X.1Xv r12 = r7.iterator()
        L_0x02cd:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x032f
            java.lang.Object r9 = r12.next()
            com.facebook.messaging.model.messages.Message r9 = (com.facebook.messaging.model.messages.Message) r9
            r4 = 8
            int r1 = X.AnonymousClass1Y3.AS0
            X.0UN r0 = r6.A00
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.Ao0 r11 = (X.C22080Ao0) r11
            com.google.common.collect.ImmutableList r0 = r9.A0X
            X.1Xv r10 = r0.iterator()
        L_0x02eb:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x0307
            java.lang.Object r0 = r10.next()
            com.facebook.messaging.model.attachment.Attachment r0 = (com.facebook.messaging.model.attachment.Attachment) r0
            java.lang.String r4 = r0.A07
            android.content.Context r1 = r11.A00
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r9.A0U
            java.io.File r0 = X.AnonymousClass51R.A02(r1, r0, r4)
            if (r0 == 0) goto L_0x02eb
            r0.delete()
            goto L_0x02eb
        L_0x0307:
            com.google.common.collect.ImmutableList r0 = r9.A0b
            X.1Xv r4 = r0.iterator()
        L_0x030d:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0327
            java.lang.Object r1 = r4.next()
            com.facebook.ui.media.attachments.model.MediaResource r1 = (com.facebook.ui.media.attachments.model.MediaResource) r1
            java.lang.String r0 = r1.A03()
            if (r0 == 0) goto L_0x030d
            java.lang.String r0 = r1.A03()
            r3.A01(r0)
            goto L_0x030d
        L_0x0327:
            java.lang.String r1 = r9.A0q
            java.lang.String r0 = r9.A0w
            r5.put(r1, r0)
            goto L_0x02cd
        L_0x032f:
            r4 = 11
            int r1 = X.AnonymousClass1Y3.B3A
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.29r r0 = (X.C423029r) r0
            r0.A02(r7)
            android.os.Bundle r1 = r8.A00
            r4 = 0
            java.lang.String r0 = "KEEP_IN_DB_AS_HIDDEN"
            boolean r0 = r1.getBoolean(r0, r4)
            if (r0 == 0) goto L_0x0408
            int r1 = X.AnonymousClass1Y3.AkG
            X.0UN r0 = r6.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1tG r7 = (X.C36521tG) r7
            com.google.common.collect.ImmutableSet r2 = r2.A01
            X.0W6 r0 = X.C195818v.A0E
            java.lang.String r1 = r0.A00
            java.lang.String r0 = r13.toString()
            X.0av r1 = X.C06160ax.A03(r1, r0)
            X.0W6 r0 = X.C195818v.A05
            java.lang.String r0 = r0.A00
            X.0av r0 = X.C06160ax.A04(r0, r2)
            r2 = 1
            X.0av[] r0 = new X.C06140av[]{r1, r0}
            X.1a6 r9 = X.C06160ax.A01(r0)
            X.0Tq r0 = r7.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r8 = r0.A01()
            android.content.ContentValues r7 = new android.content.ContentValues
            r7.<init>()
            X.0W6 r0 = X.C195818v.A03
            java.lang.String r1 = r0.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)
            r7.put(r1, r0)
            java.lang.String r2 = r9.A02()
            java.lang.String[] r1 = r9.A04()
            java.lang.String r0 = "messages"
            r8.update(r0, r7, r2, r1)
        L_0x039b:
            r2 = 12
            int r1 = X.AnonymousClass1Y3.Any
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1hr r0 = (X.C30321hr) r0
            X.1i4 r2 = r0.A01()
            r0 = -1
            r7 = 1
            com.facebook.messaging.model.messages.MessagesCollection r1 = r2.A0A(r13, r0, r7)
            if (r1 == 0) goto L_0x03f9
            boolean r0 = r1.A08()
            if (r0 != 0) goto L_0x03f9
            com.facebook.messaging.model.messages.Message r2 = r1.A05()
            com.google.common.base.Preconditions.checkNotNull(r2)
            int r1 = X.AnonymousClass1Y3.A9p
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.AnE r0 = (X.C22038AnE) r0
            r0.A01(r2)
        L_0x03ce:
            r2 = 7
            int r1 = X.AnonymousClass1Y3.AI2
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.17y r0 = (X.C193917y) r0
            com.facebook.messaging.service.model.FetchThreadResult r0 = r0.A01(r13, r4)
            com.google.common.collect.ImmutableMap r15 = r5.build()
            com.facebook.messaging.service.model.DeleteMessagesResult r11 = new com.facebook.messaging.service.model.DeleteMessagesResult
            com.facebook.messaging.model.threads.ThreadSummary r12 = r0.A05
            com.google.common.collect.ImmutableSet r14 = r15.keySet()
            com.google.common.collect.ImmutableSet r16 = r3.build()
            r17 = 0
            r18 = 0
            r11.<init>(r12, r13, r14, r15, r16, r17, r18)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r11)
            return r0
        L_0x03f9:
            int r1 = X.AnonymousClass1Y3.AkG
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1tG r1 = (X.C36521tG) r1
            r0 = 0
            r1.A0I(r13, r0, r0, r0)
            goto L_0x03ce
        L_0x0408:
            int r1 = X.AnonymousClass1Y3.AkG
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1tG r1 = (X.C36521tG) r1
            com.google.common.collect.ImmutableSet r0 = r2.A01
            r1.A0J(r13, r0)
            goto L_0x039b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0G(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01f8, code lost:
        if (r6 != null) goto L_0x01fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0271, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0273, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0274, code lost:
        if (r6 != null) goto L_0x0276;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0279, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:67:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:52:0x019f] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0273 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:54:0x01bf] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0276 A[SYNTHETIC, Splitter:B:75:0x0276] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0H(X.C11060ln r32, X.C27311cz r33) {
        /*
            r31 = this;
            r1 = r31
            boolean r0 = r1 instanceof X.C10840kw
            r2 = r32
            if (r0 != 0) goto L_0x03a4
            boolean r0 = r1 instanceof X.C11070lo
            if (r0 != 0) goto L_0x034b
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x02d5
            r4 = r1
            X.0lz r4 = (X.C11150lz) r4
            boolean r0 = r4 instanceof X.C27561dO
            r5 = r33
            if (r0 != 0) goto L_0x02bf
            boolean r0 = r4 instanceof X.C12440pM
            if (r0 != 0) goto L_0x00bd
            boolean r0 = r4 instanceof X.C12370pE
            if (r0 != 0) goto L_0x009c
            boolean r0 = r4 instanceof X.C11130lx
            if (r0 != 0) goto L_0x002a
            com.facebook.fbservice.service.OperationResult r5 = r5.BAz(r2)
        L_0x0029:
            return r5
        L_0x002a:
            X.0lx r4 = (X.C11130lx) r4
            android.os.Bundle r1 = r2.A00
            java.lang.String r0 = "deleteThreadsParams"
            android.os.Parcelable r6 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.DeleteThreadsParams r6 = (com.facebook.messaging.service.model.DeleteThreadsParams) r6
            boolean r7 = X.C11130lx.A01(r4)
            r3 = 12
            com.facebook.fbservice.service.OperationResult r5 = r5.BAz(r2)     // Catch:{ all -> 0x008b }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x008b }
            java.lang.Object r2 = r0.get()     // Catch:{ all -> 0x008b }
            X.0u9 r2 = (X.C14800u9) r2     // Catch:{ all -> 0x008b }
            X.0l8 r1 = X.C10950l8.A05     // Catch:{ all -> 0x008b }
            com.google.common.collect.ImmutableList r0 = r6.A00     // Catch:{ all -> 0x008b }
            r2.A08(r1, r0)     // Catch:{ all -> 0x008b }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x008b }
            java.lang.Object r2 = r0.get()     // Catch:{ all -> 0x008b }
            X.0u9 r2 = (X.C14800u9) r2     // Catch:{ all -> 0x008b }
            X.0l8 r1 = X.C10950l8.A0A     // Catch:{ all -> 0x008b }
            com.google.common.collect.ImmutableList r0 = r6.A00     // Catch:{ all -> 0x008b }
            r2.A08(r1, r0)     // Catch:{ all -> 0x008b }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x008b }
            java.lang.Object r2 = r0.get()     // Catch:{ all -> 0x008b }
            X.0u9 r2 = (X.C14800u9) r2     // Catch:{ all -> 0x008b }
            X.0l8 r1 = X.C10950l8.A0C     // Catch:{ all -> 0x008b }
            com.google.common.collect.ImmutableList r0 = r6.A00     // Catch:{ all -> 0x008b }
            r2.A08(r1, r0)     // Catch:{ all -> 0x008b }
            r2 = 3
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x008b }
            X.0UN r0 = r4.A00     // Catch:{ all -> 0x008b }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x008b }
            X.16c r0 = (X.C189216c) r0     // Catch:{ all -> 0x008b }
            r0.A08()     // Catch:{ all -> 0x008b }
            if (r7 == 0) goto L_0x0029
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            return r5
        L_0x008b:
            r2 = move-exception
            if (r7 == 0) goto L_0x009b
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x009b:
            throw r2
        L_0x009c:
            X.0pE r4 = (X.C12370pE) r4
            android.os.Bundle r1 = r2.A00
            java.lang.String r0 = "deleteThreadsParams"
            android.os.Parcelable r3 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.DeleteThreadsParams r3 = (com.facebook.messaging.service.model.DeleteThreadsParams) r3
            com.facebook.fbservice.service.OperationResult r5 = r5.BAz(r2)
            int r2 = X.AnonymousClass1Y3.BJD
            X.0UN r1 = r4.A00
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2ij r1 = (X.C52232ij) r1
            com.google.common.collect.ImmutableList r0 = r3.A00
            r1.A0a(r0)
            return r5
        L_0x00bd:
            X.0pM r4 = (X.C12440pM) r4
            java.lang.String r1 = "SmsServiceHandler.handleDeleteThreads"
            r0 = 1213976767(0x485bd0bf, float:225090.98)
            X.C005505z.A03(r1, r0)
            android.os.Bundle r1 = r2.A00     // Catch:{ all -> 0x02b7 }
            java.lang.String r0 = "deleteThreadsParams"
            android.os.Parcelable r1 = r1.getParcelable(r0)     // Catch:{ all -> 0x02b7 }
            com.facebook.messaging.service.model.DeleteThreadsParams r1 = (com.facebook.messaging.service.model.DeleteThreadsParams) r1     // Catch:{ all -> 0x02b7 }
            com.google.common.collect.ImmutableList r0 = r1.A00     // Catch:{ all -> 0x02b7 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x02b7 }
            if (r0 != 0) goto L_0x02ae
            com.google.common.collect.ImmutableList r1 = r1.A00     // Catch:{ all -> 0x02b7 }
            X.4AC r0 = new X.4AC     // Catch:{ all -> 0x02b7 }
            r0.<init>()     // Catch:{ all -> 0x02b7 }
            java.util.List r3 = X.C04300To.A07(r1, r0)     // Catch:{ all -> 0x02b7 }
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x02b7 }
            X.0UN r0 = r4.A01     // Catch:{ all -> 0x02b7 }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02b7 }
            X.2e4 r7 = (X.C50462e4) r7     // Catch:{ all -> 0x02b7 }
            java.util.HashSet r15 = new java.util.HashSet     // Catch:{ all -> 0x02b7 }
            r15.<init>(r3)     // Catch:{ all -> 0x02b7 }
            java.util.ArrayList r14 = new java.util.ArrayList     // Catch:{ all -> 0x02b7 }
            int r0 = r15.size()     // Catch:{ all -> 0x02b7 }
            r14.<init>(r0)     // Catch:{ all -> 0x02b7 }
            java.util.Iterator r22 = r15.iterator()     // Catch:{ all -> 0x02b7 }
        L_0x0101:
            boolean r0 = r22.hasNext()     // Catch:{ all -> 0x02b7 }
            r8 = 1
            r6 = 0
            if (r0 == 0) goto L_0x027a
            java.lang.Object r0 = r22.next()     // Catch:{ all -> 0x02b7 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x02b7 }
            if (r0 == 0) goto L_0x0101
            long r0 = r0.longValue()     // Catch:{ all -> 0x02b7 }
            r3 = 0
            int r2 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r2 >= 0) goto L_0x0170
            r4 = 9
            int r3 = X.AnonymousClass1Y3.A6D     // Catch:{ all -> 0x02b7 }
            X.0UN r2 = r7.A00     // Catch:{ all -> 0x02b7 }
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x02b7 }
            X.9NV r9 = (X.AnonymousClass9NV) r9     // Catch:{ all -> 0x02b7 }
            java.lang.String r6 = "SmsSpecialThreadManager"
            java.lang.String r3 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x02b7 }
            java.lang.String r2 = "thread_id"
            X.0av r8 = X.C06160ax.A03(r2, r3)     // Catch:{ all -> 0x02b7 }
            android.content.ContentResolver r5 = r9.A00     // Catch:{ Exception -> 0x0143 }
            android.net.Uri r4 = X.C196749Nh.A00     // Catch:{ Exception -> 0x0143 }
            java.lang.String r3 = r8.A02()     // Catch:{ Exception -> 0x0143 }
            java.lang.String[] r2 = r8.A04()     // Catch:{ Exception -> 0x0143 }
            r5.delete(r4, r3, r2)     // Catch:{ Exception -> 0x0143 }
            goto L_0x0151
        L_0x0143:
            r4 = move-exception
            java.lang.Long r2 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x02b7 }
            java.lang.Object[] r3 = new java.lang.Object[]{r2}     // Catch:{ all -> 0x02b7 }
            java.lang.String r2 = "Failed to delete SMS messages in thread %d"
            X.C010708t.A0U(r6, r4, r2, r3)     // Catch:{ all -> 0x02b7 }
        L_0x0151:
            android.content.ContentResolver r5 = r9.A00     // Catch:{ Exception -> 0x0161 }
            android.net.Uri r4 = X.C52462j8.A00     // Catch:{ Exception -> 0x0161 }
            java.lang.String r3 = r8.A02()     // Catch:{ Exception -> 0x0161 }
            java.lang.String[] r2 = r8.A04()     // Catch:{ Exception -> 0x0161 }
            r5.delete(r4, r3, r2)     // Catch:{ Exception -> 0x0161 }
            goto L_0x0101
        L_0x0161:
            r2 = move-exception
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x02b7 }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x02b7 }
            java.lang.String r0 = "Failed to delete MMS messages in thread %d"
            X.C010708t.A0U(r6, r2, r0, r1)     // Catch:{ all -> 0x02b7 }
            goto L_0x0101
        L_0x0170:
            java.util.List r5 = r7.A0D(r0)     // Catch:{ all -> 0x02b7 }
            int r2 = r5.size()     // Catch:{ all -> 0x02b7 }
            if (r2 != r8) goto L_0x021f
            r4 = 11
            int r3 = X.AnonymousClass1Y3.BQT     // Catch:{ all -> 0x02b7 }
            X.0UN r2 = r7.A00     // Catch:{ all -> 0x02b7 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x02b7 }
            X.7Rs r2 = (X.C157827Rs) r2     // Catch:{ all -> 0x02b7 }
            java.lang.Object r12 = r5.get(r6)     // Catch:{ all -> 0x02b7 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x02b7 }
            X.7RW r13 = r2.A02     // Catch:{ all -> 0x02b7 }
            X.06B r2 = r13.A00     // Catch:{ all -> 0x02b7 }
            long r20 = r2.now()     // Catch:{ all -> 0x02b7 }
            X.0W6 r2 = X.AnonymousClass2O4.A00     // Catch:{ all -> 0x02b7 }
            java.lang.String r2 = r2.A00     // Catch:{ all -> 0x02b7 }
            X.0av r3 = X.C06160ax.A03(r2, r12)     // Catch:{ all -> 0x02b7 }
            r16 = 0
            r6 = 0
            X.1ua r2 = r13.A01     // Catch:{ Exception -> 0x01f8, all -> 0x0271 }
            android.database.sqlite.SQLiteDatabase r23 = r2.A06()     // Catch:{ Exception -> 0x01f8, all -> 0x0271 }
            r2 = 52
            java.lang.String r24 = X.AnonymousClass24B.$const$string(r2)     // Catch:{ Exception -> 0x01f8, all -> 0x0271 }
            java.lang.String[] r25 = X.AnonymousClass7RW.A02     // Catch:{ Exception -> 0x01f8, all -> 0x0271 }
            java.lang.String r26 = r3.A02()     // Catch:{ Exception -> 0x01f8, all -> 0x0271 }
            java.lang.String[] r27 = r3.A04()     // Catch:{ Exception -> 0x01f8, all -> 0x0271 }
            r28 = 0
            r29 = 0
            r30 = 0
            android.database.Cursor r6 = r23.query(r24, r25, r26, r27, r28, r29, r30)     // Catch:{ Exception -> 0x01f8, all -> 0x0271 }
            boolean r2 = r6.moveToNext()     // Catch:{ Exception -> 0x01f8, all -> 0x0273 }
            if (r2 == 0) goto L_0x01fa
            X.0W6 r2 = X.AnonymousClass2O4.A01     // Catch:{ Exception -> 0x01f8, all -> 0x0273 }
            int r2 = r2.A01(r6)     // Catch:{ Exception -> 0x01f8, all -> 0x0273 }
            double r18 = r6.getDouble(r2)     // Catch:{ Exception -> 0x01f8, all -> 0x0273 }
            X.0W6 r2 = X.AnonymousClass2O4.A02     // Catch:{ Exception -> 0x01f6, all -> 0x0273 }
            long r2 = r2.A02(r6)     // Catch:{ Exception -> 0x01f6, all -> 0x0273 }
            r16 = r18
            r10 = 2592000000(0x9a7ec800, double:1.280618154E-314)
            long r8 = r20 - r2
            r3 = 0
            int r2 = (r18 > r3 ? 1 : (r18 == r3 ? 0 : -1))
            if (r2 == 0) goto L_0x01fa
            r3 = 0
            int r2 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x01fa
            r4 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r2 = (double) r8     // Catch:{ Exception -> 0x01f6, all -> 0x0273 }
            double r8 = (double) r10     // Catch:{ Exception -> 0x01f6, all -> 0x0273 }
            double r2 = r2 / r8
            double r2 = java.lang.Math.pow(r4, r2)     // Catch:{ Exception -> 0x01f6, all -> 0x0273 }
            double r16 = r18 * r2
            goto L_0x01fa
        L_0x01f6:
            r16 = r18
        L_0x01f8:
            if (r6 == 0) goto L_0x01fd
        L_0x01fa:
            r6.close()     // Catch:{ all -> 0x02b7 }
        L_0x01fd:
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ all -> 0x02b7 }
            r5.<init>()     // Catch:{ all -> 0x02b7 }
            X.0W6 r2 = X.AnonymousClass2O4.A01     // Catch:{ all -> 0x02b7 }
            java.lang.String r4 = r2.A00     // Catch:{ all -> 0x02b7 }
            r2 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r16 = r16 + r2
            java.lang.Double r2 = java.lang.Double.valueOf(r16)     // Catch:{ all -> 0x02b7 }
            r5.put(r4, r2)     // Catch:{ all -> 0x02b7 }
            X.0W6 r2 = X.AnonymousClass2O4.A02     // Catch:{ all -> 0x02b7 }
            java.lang.String r3 = r2.A00     // Catch:{ all -> 0x02b7 }
            java.lang.Long r2 = java.lang.Long.valueOf(r20)     // Catch:{ all -> 0x02b7 }
            r5.put(r3, r2)     // Catch:{ all -> 0x02b7 }
            X.AnonymousClass7RW.A01(r13, r12, r5)     // Catch:{ all -> 0x02b7 }
        L_0x021f:
            android.net.Uri r2 = X.C50472e5.A00     // Catch:{ all -> 0x02b7 }
            android.net.Uri r2 = android.content.ContentUris.withAppendedId(r2, r0)     // Catch:{ all -> 0x02b7 }
            android.content.ContentProviderOperation$Builder r2 = android.content.ContentProviderOperation.newDelete(r2)     // Catch:{ all -> 0x02b7 }
            android.content.ContentProviderOperation r2 = r2.build()     // Catch:{ all -> 0x02b7 }
            r14.add(r2)     // Catch:{ all -> 0x02b7 }
            r4 = 8
            int r3 = X.AnonymousClass1Y3.AxX     // Catch:{ all -> 0x02b7 }
            X.0UN r2 = r7.A00     // Catch:{ all -> 0x02b7 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x02b7 }
            X.2jI r4 = (X.C52552jI) r4     // Catch:{ all -> 0x02b7 }
            X.0W6 r2 = X.C52562jJ.A02     // Catch:{ all -> 0x02b7 }
            java.lang.String r3 = r2.A00     // Catch:{ all -> 0x02b7 }
            java.lang.String r2 = java.lang.Long.toString(r0)     // Catch:{ all -> 0x02b7 }
            X.0av r3 = X.C06160ax.A03(r3, r2)     // Catch:{ all -> 0x02b7 }
            X.2jK r2 = r4.A00     // Catch:{ all -> 0x02b7 }
            android.database.sqlite.SQLiteDatabase r5 = r2.A06()     // Catch:{ all -> 0x02b7 }
            java.lang.String r4 = r3.A02()     // Catch:{ all -> 0x02b7 }
            java.lang.String[] r3 = r3.A04()     // Catch:{ all -> 0x02b7 }
            java.lang.String r2 = "time_skew"
            r5.delete(r2, r4, r3)     // Catch:{ all -> 0x02b7 }
            r4 = 13
            int r3 = X.AnonymousClass1Y3.ASZ     // Catch:{ all -> 0x02b7 }
            X.0UN r2 = r7.A00     // Catch:{ all -> 0x02b7 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x02b7 }
            X.2eC r2 = (X.C50542eC) r2     // Catch:{ all -> 0x02b7 }
            X.C50542eC.A01(r2)     // Catch:{ all -> 0x02b7 }
            X.0zB r2 = r2.A01     // Catch:{ all -> 0x02b7 }
            r2.A0B(r0)     // Catch:{ all -> 0x02b7 }
            goto L_0x0101
        L_0x0271:
            r0 = move-exception
            goto L_0x0274
        L_0x0273:
            r0 = move-exception
        L_0x0274:
            if (r6 == 0) goto L_0x0279
            r6.close()     // Catch:{ all -> 0x02b7 }
        L_0x0279:
            throw r0     // Catch:{ all -> 0x02b7 }
        L_0x027a:
            int r1 = X.AnonymousClass1Y3.BCt     // Catch:{ OperationApplicationException | RemoteException -> 0x028e }
            X.0UN r0 = r7.A00     // Catch:{ OperationApplicationException | RemoteException -> 0x028e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ OperationApplicationException | RemoteException -> 0x028e }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ OperationApplicationException | RemoteException -> 0x028e }
            android.content.ContentResolver r1 = r0.getContentResolver()     // Catch:{ OperationApplicationException | RemoteException -> 0x028e }
            java.lang.String r0 = "mms-sms"
            r1.applyBatch(r0, r14)     // Catch:{ OperationApplicationException | RemoteException -> 0x028e }
            goto L_0x02ae
        L_0x028e:
            r3 = move-exception
            int r0 = r15.size()     // Catch:{ all -> 0x02b7 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x02b7 }
            java.lang.Object[] r2 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x02b7 }
            r0 = 48
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)     // Catch:{ all -> 0x02b7 }
            java.lang.String r0 = "sms/mms thread deletion failed. # threads = %d"
            X.C010708t.A0U(r1, r3, r0, r2)     // Catch:{ all -> 0x02b7 }
            X.9O1 r1 = new X.9O1     // Catch:{ all -> 0x02b7 }
            java.lang.String r0 = "Failed to delete sms thread."
            r1.<init>(r0)     // Catch:{ all -> 0x02b7 }
            throw r1     // Catch:{ all -> 0x02b7 }
        L_0x02ae:
            com.facebook.fbservice.service.OperationResult r5 = com.facebook.fbservice.service.OperationResult.A00     // Catch:{ all -> 0x02b7 }
            r0 = -438257179(0xffffffffe5e0b9e5, float:-1.32654905E23)
            X.C005505z.A00(r0)
            return r5
        L_0x02b7:
            r1 = move-exception
            r0 = -1140287436(0xffffffffbc089834, float:-0.008337069)
            X.C005505z.A00(r0)
            throw r1
        L_0x02bf:
            X.1dO r4 = (X.C27561dO) r4
            android.os.Bundle r1 = r2.A00
            java.lang.String r0 = "deleteThreadsParams"
            android.os.Parcelable r0 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.DeleteThreadsParams r0 = (com.facebook.messaging.service.model.DeleteThreadsParams) r0
            com.google.common.collect.ImmutableList r0 = r0.A00
            X.C27561dO.A05(r4, r0)
            com.facebook.fbservice.service.OperationResult r5 = r5.BAz(r2)
            return r5
        L_0x02d5:
            r4 = r1
            X.0pF r4 = (X.C12380pF) r4
            android.os.Bundle r1 = r2.A00
            java.lang.String r0 = "deleteThreadsParams"
            android.os.Parcelable r0 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.DeleteThreadsParams r0 = (com.facebook.messaging.service.model.DeleteThreadsParams) r0
            com.google.common.collect.ImmutableList r6 = r0.A00
            int r2 = X.AnonymousClass1Y3.BAS
            X.0UN r1 = r4.A00
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.3Bj r0 = (X.C64393Bj) r0
            X.3fs r3 = r0.A01()
            r5 = 0
            r1 = 0
        L_0x02f5:
            int r0 = r6.size()
            if (r5 >= r0) goto L_0x0335
            java.lang.Object r0 = r6.get(r5)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = (com.facebook.messaging.model.threadkey.ThreadKey) r0
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0C(r0)
            if (r0 != 0) goto L_0x0332
            r2 = 10
            int r1 = X.AnonymousClass1Y3.ABZ
            X.0UN r0 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.4z8 r2 = (X.C104554z8) r2
            com.facebook.messaging.service.model.DeleteThreadParams r1 = new com.facebook.messaging.service.model.DeleteThreadParams
            java.lang.Object r0 = r6.get(r5)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = (com.facebook.messaging.model.threadkey.ThreadKey) r0
            r1.<init>(r0)
            X.3fu r1 = X.C73323ft.A00(r2, r1)
            java.lang.String r0 = "thread-key-"
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r5)
            r1.A02 = r0
            X.3ft r0 = r1.A00()
            r3.A00(r0)
            r1 = 1
        L_0x0332:
            int r5 = r5 + 1
            goto L_0x02f5
        L_0x0335:
            if (r1 == 0) goto L_0x0348
            java.lang.Class r0 = r4.getClass()
            com.facebook.common.callercontext.CallerContext r2 = com.facebook.common.callercontext.CallerContext.A04(r0)
            X.2Xl r1 = X.C12380pF.A00(r4)
            java.lang.String r0 = "deleteThreads"
            r3.A02(r0, r2, r1)
        L_0x0348:
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A00
            return r0
        L_0x034b:
            r8 = r1
            X.0lo r8 = (X.C11070lo) r8
            java.lang.String r7 = "deleteThreadsParams"
            android.os.Parcelable r0 = X.AnonymousClass0ls.A00(r2, r7)
            com.facebook.messaging.service.model.DeleteThreadsParams r0 = (com.facebook.messaging.service.model.DeleteThreadsParams) r0
            com.google.common.collect.ImmutableList r0 = r0.A00
            java.util.HashMap r0 = X.C11070lo.A05(r0)
            X.0lu r6 = new X.0lu
            r6.<init>()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r5 = r0.iterator()
        L_0x0369:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x039d
            java.lang.Object r0 = r5.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r4 = r0.getKey()
            X.0lv r4 = (X.C11120lv) r4
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            com.facebook.messaging.service.model.DeleteThreadsParams r1 = new com.facebook.messaging.service.model.DeleteThreadsParams
            java.lang.Object r0 = r0.getValue()
            java.util.List r0 = (java.util.List) r0
            r1.<init>(r0)
            r3.putParcelable(r7, r1)
            X.1cz r2 = X.C11070lo.A00(r8, r4)
            X.0ln r1 = new X.0ln
            java.lang.String r0 = "delete_threads"
            r1.<init>(r0, r3)
            r6.A00(r4, r2, r1)
            goto L_0x0369
        L_0x039d:
            X.0lq r0 = X.C11070lo.A01
            com.facebook.fbservice.service.OperationResult r0 = X.C11070lo.A01(r6, r0)
            return r0
        L_0x03a4:
            X.0kw r1 = (X.C10840kw) r1
            java.lang.String r0 = "deleteThreadsParams"
            android.os.Parcelable r0 = X.AnonymousClass0ls.A00(r2, r0)
            com.facebook.messaging.service.model.DeleteThreadsParams r0 = (com.facebook.messaging.service.model.DeleteThreadsParams) r0
            com.google.common.collect.ImmutableList r0 = r0.A00
            X.C10840kw.A01(r1, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A00
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0H(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0I(C11060ln r10, C27311cz r11) {
        GSTModelShape1S0000000 gSTModelShape1S0000000;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r10);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r3 = (C11150lz) this;
                if (r3 instanceof C12370pE) {
                    C12370pE r32 = (C12370pE) r3;
                    OperationResult BAz = r11.BAz(r10);
                    EditDisplayNameResult editDisplayNameResult = (EditDisplayNameResult) BAz.A08();
                    if (editDisplayNameResult == null) {
                        return BAz;
                    }
                    C07220cv r4 = new C07220cv();
                    r4.A04((User) r32.A04.get());
                    r4.A0J = new Name(editDisplayNameResult.A00, editDisplayNameResult.A01, null);
                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r32.A00)).A05.A01(ImmutableList.of(r4.A02()));
                    return BAz;
                } else if (!(r3 instanceof C11130lx)) {
                    return r11.BAz(r10);
                } else {
                    C11130lx r33 = (C11130lx) r3;
                    boolean A012 = C11130lx.A01(r33);
                    try {
                        OperationResult BAz2 = r11.BAz(r10);
                        EditDisplayNameResult editDisplayNameResult2 = (EditDisplayNameResult) BAz2.A08();
                        if (editDisplayNameResult2 != null) {
                            C07220cv r6 = new C07220cv();
                            r6.A04((User) r33.A05.get());
                            r6.A0J = new Name(editDisplayNameResult2.A00, editDisplayNameResult2.A01, null);
                            User A02 = r6.A02();
                            ((AnonymousClass0XN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Aey, r33.A00)).A0E(A02);
                            ((C14800u9) r33.A01.get()).A02.A06(ImmutableList.of(A02));
                            ((C156167Jx) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BOj, r33.A00)).A01(A02.A0Q);
                        }
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r33.A00)).A03();
                        }
                    }
                }
            } else {
                Name name = ((EditDisplayNameParams) r10.A00.getParcelable("editDisplayNameParams")).A00;
                C849240y r34 = new C849240y();
                GQLCallInputCInputShape1S0000000 gQLCallInputCInputShape1S0000000 = new GQLCallInputCInputShape1S0000000(164);
                gQLCallInputCInputShape1S0000000.A09("first_name", name.firstName);
                gQLCallInputCInputShape1S0000000.A09("last_name", name.lastName);
                r34.A04("input", gQLCallInputCInputShape1S0000000);
                GSTModelShape1S0000000 gSTModelShape1S00000002 = (GSTModelShape1S0000000) C11170mD.A03(((CKn) AnonymousClass1XX.A02(29, AnonymousClass1Y3.As2, ((C12380pF) this).A00)).A00.A05(C26931cN.A01(r34))).get();
                if (gSTModelShape1S00000002 != null && (gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSTModelShape1S00000002.A0J(3599307, GSTModelShape1S0000000.class, -1398709648)) != null) {
                    return OperationResult.A04(new EditDisplayNameResult(gSTModelShape1S0000000.A0P(-160985414), gSTModelShape1S0000000.A0P(2013122196)));
                }
                throw new IllegalStateException();
            }
        }
    }

    public OperationResult A0J(C11060ln r9, C27311cz r10) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r9);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r4 = (C11150lz) this;
                if ((r4 instanceof C12370pE) || !(r4 instanceof C11130lx)) {
                    return r10.BAz(r9);
                }
                C11130lx r42 = (C11130lx) r4;
                OperationResult BAz = r10.BAz(r9);
                EditPasswordResult editPasswordResult = (EditPasswordResult) BAz.A08();
                if (editPasswordResult != null) {
                    C07220cv r2 = new C07220cv();
                    r2.A04((User) r42.A05.get());
                    r2.A1n = editPasswordResult.A00;
                    User A02 = r2.A02();
                    ((AnonymousClass0XN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Aey, r42.A00)).A0E(A02);
                    ((C14800u9) r42.A01.get()).A02.A06(ImmutableList.of(A02));
                    UserKey userKey = A02.A0Q;
                    Intent intent = new Intent("com.facebook.user.broadcast.ACTION_USERNAME_UPDATED");
                    intent.putExtra("updated_user", userKey);
                    ((C156167Jx) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BOj, r42.A00)).A00.C4x(intent);
                }
                return BAz;
            }
            EditPasswordParams editPasswordParams = (EditPasswordParams) r9.A00.getParcelable("editPasswordParams");
            AnonymousClass4YQ r43 = (AnonymousClass4YQ) AnonymousClass1XX.A02(26, AnonymousClass1Y3.AUy, ((C12380pF) this).A00);
            C849140x r5 = new C849140x();
            GQLCallInputCInputShape1S0000000 gQLCallInputCInputShape1S0000000 = new GQLCallInputCInputShape1S0000000(AnonymousClass1Y3.A1H);
            gQLCallInputCInputShape1S0000000.A0B(r43.A01);
            GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = new GQLCallInputCInputShape0S0000000(AnonymousClass1Y3.A0v);
            gQLCallInputCInputShape0S0000000.A09(AnonymousClass24B.$const$string(2), editPasswordParams.A00);
            gQLCallInputCInputShape1S0000000.A05("password_old", gQLCallInputCInputShape0S0000000);
            GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S00000002 = new GQLCallInputCInputShape0S0000000(AnonymousClass1Y3.A0v);
            gQLCallInputCInputShape0S00000002.A09(AnonymousClass24B.$const$string(2), editPasswordParams.A01);
            gQLCallInputCInputShape1S0000000.A05("password_new", gQLCallInputCInputShape0S00000002);
            GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S00000003 = new GQLCallInputCInputShape0S0000000(AnonymousClass1Y3.A0v);
            gQLCallInputCInputShape0S00000003.A09(AnonymousClass24B.$const$string(2), editPasswordParams.A02);
            gQLCallInputCInputShape1S0000000.A05("password_confirm", gQLCallInputCInputShape0S00000003);
            r5.A04("input", gQLCallInputCInputShape1S0000000);
            Preconditions.checkNotNull((GraphQLResult) r43.A00.A05(C26931cN.A01(r5)).get(), "Edit password result must not be null");
            return OperationResult.A04(new EditPasswordResult(true));
        }
    }

    public OperationResult A0K(C11060ln r10, C27311cz r11) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r10);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r4 = (C11150lz) this;
                if (r4 instanceof C12370pE) {
                    C12370pE r42 = (C12370pE) r4;
                    OperationResult BAz = r11.BAz(r10);
                    EditUsernameResult editUsernameResult = (EditUsernameResult) BAz.A08();
                    if (editUsernameResult == null) {
                        return BAz;
                    }
                    C07220cv r1 = new C07220cv();
                    r1.A04((User) r42.A04.get());
                    r1.A0x = editUsernameResult.A00;
                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r42.A00)).A05.A01(ImmutableList.of(r1.A02()));
                    return BAz;
                } else if (!(r4 instanceof C11130lx)) {
                    return r11.BAz(r10);
                } else {
                    C11130lx r43 = (C11130lx) r4;
                    boolean A012 = C11130lx.A01(r43);
                    try {
                        OperationResult BAz2 = r11.BAz(r10);
                        EditUsernameResult editUsernameResult2 = (EditUsernameResult) BAz2.A08();
                        if (editUsernameResult2 != null) {
                            C07220cv r12 = new C07220cv();
                            r12.A04((User) r43.A05.get());
                            r12.A0x = editUsernameResult2.A00;
                            User A02 = r12.A02();
                            ((AnonymousClass0XN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Aey, r43.A00)).A0E(A02);
                            ((C14800u9) r43.A01.get()).A02.A06(ImmutableList.of(A02));
                            UserKey userKey = A02.A0Q;
                            Intent intent = new Intent("com.facebook.user.broadcast.ACTION_USERNAME_UPDATED");
                            intent.putExtra("updated_user", userKey);
                            ((C156167Jx) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BOj, r43.A00)).A00.C4x(intent);
                        }
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r43.A00)).A03();
                        }
                    }
                }
            } else {
                AnonymousClass7O3 r13 = (AnonymousClass7O3) AnonymousClass1XX.A02(25, AnonymousClass1Y3.AuQ, ((C12380pF) this).A00);
                String str = ((EditUsernameParams) r10.A00.getParcelable("editUsernameParams")).A00;
                if (((GraphQLResult) r13.A00.A06(AnonymousClass7O3.A00(r13, str, true), C137796c1.A01).get()) != null) {
                    return OperationResult.A04(new EditUsernameResult(str));
                }
                throw new IllegalStateException();
            }
        }
    }

    public OperationResult A0L(C11060ln r6, C27311cz r7) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r6);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r1 = (C11150lz) this;
                if (!(r1 instanceof C12370pE)) {
                    boolean z = r1 instanceof C11130lx;
                }
                return r7.BAz(r6);
            }
            FetchThreadListParams fetchThreadListParams = (FetchThreadListParams) r6.A00.getParcelable("fetchThreadListParams");
            Preconditions.checkArgument(fetchThreadListParams.A03.equals(C10950l8.A09));
            OperationResult A04 = OperationResult.A04(((AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, ((C12380pF) this).A00)).A0G(fetchThreadListParams, r6.A01));
            C180878aL.A00(A04);
            return A04;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ed, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ee, code lost:
        if (r9 != null) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0189, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0190, code lost:
        throw r1;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:52:0x00f3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0M(X.C11060ln r14, X.C27311cz r15) {
        /*
            r13 = this;
            boolean r0 = r13 instanceof X.C10840kw
            if (r0 != 0) goto L_0x02df
            boolean r0 = r13 instanceof X.C11070lo
            if (r0 != 0) goto L_0x0292
            boolean r0 = r13 instanceof X.C12380pF
            if (r0 != 0) goto L_0x0254
            r3 = r13
            X.0lz r3 = (X.C11150lz) r3
            boolean r0 = r3 instanceof X.C12440pM
            if (r0 != 0) goto L_0x01d0
            boolean r0 = r3 instanceof X.C12370pE
            if (r0 != 0) goto L_0x0105
            boolean r0 = r3 instanceof X.C11130lx
            if (r0 != 0) goto L_0x0020
            com.facebook.fbservice.service.OperationResult r2 = r15.BAz(r14)
        L_0x001f:
            return r2
        L_0x0020:
            X.0lx r3 = (X.C11130lx) r3
            android.os.Bundle r1 = r14.A00
            java.lang.String r0 = "fetchPinnedThreadsParams"
            android.os.Parcelable r0 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchGroupThreadsParams r0 = (com.facebook.messaging.service.model.FetchGroupThreadsParams) r0
            X.0hU r4 = r0.A01
            X.0hU r0 = X.C09510hU.STALE_DATA_OKAY
            r2 = 0
            r1 = 0
            if (r4 != r0) goto L_0x0035
            r1 = 1
        L_0x0035:
            X.0m6 r0 = r3.A03
            boolean r0 = r0.A0e()
            if (r0 == 0) goto L_0x0048
            X.0m6 r0 = r3.A03
            boolean r0 = r0.A0f()
            if (r0 != 0) goto L_0x0047
            if (r1 == 0) goto L_0x0048
        L_0x0047:
            r2 = 1
        L_0x0048:
            if (r2 == 0) goto L_0x0066
            X.4zN r1 = new X.4zN
            r1.<init>()
            r0 = 1
            r1.A02 = r0
            X.0m6 r0 = r3.A03
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A0S()
            com.google.common.collect.ImmutableList r0 = r0.A00
            r1.A01 = r0
            com.facebook.messaging.service.model.FetchGroupThreadsResult r0 = new com.facebook.messaging.service.model.FetchGroupThreadsResult
            r0.<init>(r1)
            com.facebook.fbservice.service.OperationResult r2 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r2
        L_0x0066:
            boolean r10 = X.C11130lx.A01(r3)
            r4 = 12
            com.facebook.fbservice.service.OperationResult r0 = r15.BAz(r14)     // Catch:{ all -> 0x00f4 }
            java.lang.Object r7 = r0.A07()     // Catch:{ all -> 0x00f4 }
            com.facebook.messaging.service.model.FetchGroupThreadsResult r7 = (com.facebook.messaging.service.model.FetchGroupThreadsResult) r7     // Catch:{ all -> 0x00f4 }
            X.0US r0 = r3.A01     // Catch:{ all -> 0x00f4 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00f4 }
            X.0u9 r0 = (X.C14800u9) r0     // Catch:{ all -> 0x00f4 }
            X.0m6 r2 = r0.A01     // Catch:{ all -> 0x00f4 }
            com.google.common.collect.ImmutableList r8 = r7.A02     // Catch:{ all -> 0x00f4 }
            X.0mP r6 = r2.A04     // Catch:{ all -> 0x00f4 }
            X.0mL r0 = r2.A0A     // Catch:{ all -> 0x00f4 }
            X.0mM r9 = r0.A00()     // Catch:{ all -> 0x00f4 }
            java.util.Iterator r1 = r8.iterator()     // Catch:{ all -> 0x00eb }
        L_0x008e:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x00eb }
            if (r0 == 0) goto L_0x00a0
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x00eb }
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0     // Catch:{ all -> 0x00eb }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S     // Catch:{ all -> 0x00eb }
            X.AnonymousClass0m6.A0A(r2, r0)     // Catch:{ all -> 0x00eb }
            goto L_0x008e
        L_0x00a0:
            X.0mN r5 = r2.A08     // Catch:{ all -> 0x00eb }
            java.util.Iterator r2 = r8.iterator()     // Catch:{ all -> 0x00eb }
        L_0x00a6:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x00eb }
            if (r0 == 0) goto L_0x00b8
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x00eb }
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1     // Catch:{ all -> 0x00eb }
            java.lang.String r0 = "addGroupThreads"
            r5.A03(r1, r0)     // Catch:{ all -> 0x00eb }
            goto L_0x00a6
        L_0x00b8:
            com.google.common.base.Function r0 = X.C42782Bv.A01     // Catch:{ all -> 0x00eb }
            java.util.Collection r1 = X.AnonymousClass0TH.A00(r8, r0)     // Catch:{ all -> 0x00eb }
            X.0mL r0 = r6.A03     // Catch:{ all -> 0x00eb }
            r0.A01()     // Catch:{ all -> 0x00eb }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00eb }
            r0.<init>(r1)     // Catch:{ all -> 0x00eb }
            r6.A00 = r0     // Catch:{ all -> 0x00eb }
            r1 = 1
            X.0mL r0 = r6.A03     // Catch:{ all -> 0x00eb }
            r0.A01()     // Catch:{ all -> 0x00eb }
            r6.A01 = r1     // Catch:{ all -> 0x00eb }
            if (r9 == 0) goto L_0x00d7
            r9.close()     // Catch:{ all -> 0x00f4 }
        L_0x00d7:
            com.facebook.fbservice.service.OperationResult r2 = com.facebook.fbservice.service.OperationResult.A04(r7)     // Catch:{ all -> 0x00f4 }
            if (r10 == 0) goto L_0x001f
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            return r2
        L_0x00eb:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ed }
        L_0x00ed:
            r0 = move-exception
            if (r9 == 0) goto L_0x00f3
            r9.close()     // Catch:{ all -> 0x00f3 }
        L_0x00f3:
            throw r0     // Catch:{ all -> 0x00f4 }
        L_0x00f4:
            r2 = move-exception
            if (r10 == 0) goto L_0x0104
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x0104:
            throw r2
        L_0x0105:
            X.0pE r3 = (X.C12370pE) r3
            int r2 = X.AnonymousClass1Y3.Aeq
            X.0UN r1 = r3.A00
            r0 = 6
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0cb r4 = (X.C07080cb) r4
            X.1eA r2 = X.C12740pt.A02
            r0 = 0
            long r4 = r4.A01(r2, r0)
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            r0 = 0
            if (r2 <= 0) goto L_0x0120
            r0 = 1
        L_0x0120:
            if (r0 != 0) goto L_0x019f
            com.facebook.fbservice.service.OperationResult r0 = r15.BAz(r14)
            java.lang.Object r7 = r0.A07()
            com.facebook.messaging.service.model.FetchThreadListResult r7 = (com.facebook.messaging.service.model.FetchThreadListResult) r7
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BJD
            X.0UN r0 = r3.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2ij r6 = (X.C52232ij) r6
            X.0Tq r0 = r6.A0F
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r4 = r0.A06()
            r0 = 90020483(0x55d9a83, float:1.04197494E-35)
            X.C007406x.A01(r4, r0)
            X.1eA r5 = X.C12740pt.A02     // Catch:{ all -> 0x0191 }
            long r1 = r7.A00     // Catch:{ all -> 0x0191 }
            X.0Tq r0 = r6.A0B     // Catch:{ all -> 0x0191 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0191 }
            X.0cb r0 = (X.C07080cb) r0     // Catch:{ all -> 0x0191 }
            r0.A04(r5, r1)     // Catch:{ all -> 0x0191 }
            X.0Tq r0 = r6.A0F     // Catch:{ all -> 0x0191 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0191 }
            X.1aN r0 = (X.C25771aN) r0     // Catch:{ all -> 0x0191 }
            android.database.sqlite.SQLiteDatabase r5 = r0.A06()     // Catch:{ all -> 0x0191 }
            r0 = -1771314318(0xffffffff966be372, float:-1.9054918E-25)
            X.C007406x.A01(r5, r0)     // Catch:{ all -> 0x0191 }
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r7.A06     // Catch:{ all -> 0x0189 }
            com.google.common.collect.ImmutableList r2 = r0.A00     // Catch:{ all -> 0x0189 }
            long r0 = r7.A00     // Catch:{ all -> 0x0189 }
            X.C52232ij.A07(r6, r2, r0)     // Catch:{ all -> 0x0189 }
            com.google.common.collect.ImmutableList r1 = r7.A09     // Catch:{ all -> 0x0189 }
            if (r1 == 0) goto L_0x017c
            X.2sv r0 = r6.A05     // Catch:{ all -> 0x0189 }
            r0.A01(r1)     // Catch:{ all -> 0x0189 }
        L_0x017c:
            r5.setTransactionSuccessful()     // Catch:{ all -> 0x0189 }
            r0 = 1736697586(0x6783e6f2, float:1.2457804E24)
            X.C007406x.A02(r5, r0)     // Catch:{ all -> 0x0191 }
            r4.setTransactionSuccessful()     // Catch:{ all -> 0x0191 }
            goto L_0x0199
        L_0x0189:
            r1 = move-exception
            r0 = -1263071208(0xffffffffb4b71018, float:-3.4098116E-7)
            X.C007406x.A02(r5, r0)     // Catch:{ all -> 0x0191 }
            throw r1     // Catch:{ all -> 0x0191 }
        L_0x0191:
            r1 = move-exception
            r0 = -1585594171(0xffffffffa17dc0c5, float:-8.5974863E-19)
            X.C007406x.A02(r4, r0)
            throw r1
        L_0x0199:
            r0 = 2020719450(0x7871bb5a, float:1.9611617E34)
            X.C007406x.A02(r4, r0)
        L_0x019f:
            r2 = 5
            int r1 = X.AnonymousClass1Y3.AV7
            X.0UN r0 = r3.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0ph r5 = (X.C12620ph) r5
            X.0l8 r4 = X.C10950l8.A05
            r1 = -1
            r0 = 20
            com.google.common.collect.ImmutableList r4 = r5.A08(r4, r1, r0)
            X.4zN r2 = new X.4zN
            r2.<init>()
            r0 = 1
            r2.A02 = r0
            X.06B r0 = r3.A01
            long r0 = r0.now()
            r2.A00 = r0
            r2.A01 = r4
            com.facebook.messaging.service.model.FetchGroupThreadsResult r0 = new com.facebook.messaging.service.model.FetchGroupThreadsResult
            r0.<init>(r2)
            com.facebook.fbservice.service.OperationResult r2 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r2
        L_0x01d0:
            X.0pM r3 = (X.C12440pM) r3
            int r2 = X.AnonymousClass1Y3.AKa
            X.0UN r1 = r3.A01
            r0 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2e4 r4 = (X.C50462e4) r4
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0 = 90
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            java.lang.String r0 = "% %"
            X.2Bm r6 = new X.2Bm
            r6.<init>(r1, r0)
            int r5 = X.AnonymousClass1Y3.BCt
            X.0UN r1 = r4.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r5, r1)
            android.content.Context r0 = (android.content.Context) r0
            android.content.ContentResolver r7 = r0.getContentResolver()
            android.net.Uri r8 = X.C50462e4.A05
            java.lang.String[] r9 = X.C50462e4.A06
            java.lang.String r10 = X.C50462e4.A06(r4, r6)
            java.lang.String[] r11 = X.C50462e4.A09(r4, r6)
            java.lang.String r12 = "date DESC LIMIT 60"
            android.database.Cursor r1 = r7.query(r8, r9, r10, r11, r12)
            if (r1 == 0) goto L_0x022b
        L_0x0212:
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x0223 }
            if (r0 == 0) goto L_0x0228
            r0 = 0
            com.facebook.messaging.model.threads.ThreadSummary r0 = X.C50462e4.A04(r4, r1, r0)     // Catch:{ all -> 0x0223 }
            if (r0 == 0) goto L_0x0212
            r2.add(r0)     // Catch:{ all -> 0x0223 }
            goto L_0x0212
        L_0x0223:
            r0 = move-exception
            r1.close()
            throw r0
        L_0x0228:
            r1.close()
        L_0x022b:
            com.google.common.collect.ImmutableList r5 = com.google.common.collect.ImmutableList.copyOf(r2)
            X.4zN r4 = new X.4zN
            r4.<init>()
            r0 = 1
            r4.A02 = r0
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r3.A01
            r0 = 4
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.06B r0 = (X.AnonymousClass06B) r0
            long r0 = r0.now()
            r4.A00 = r0
            r4.A01 = r5
            com.facebook.messaging.service.model.FetchGroupThreadsResult r0 = new com.facebook.messaging.service.model.FetchGroupThreadsResult
            r0.<init>(r4)
            com.facebook.fbservice.service.OperationResult r2 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r2
        L_0x0254:
            r4 = r13
            X.0pF r4 = (X.C12380pF) r4
            android.os.Bundle r1 = r14.A00
            java.lang.String r0 = "fetchPinnedThreadsParams"
            android.os.Parcelable r0 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchGroupThreadsParams r0 = (com.facebook.messaging.service.model.FetchGroupThreadsParams) r0
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            X.0kg r1 = new X.0kg
            r1.<init>()
            X.0hU r0 = r0.A01
            r1.A02 = r0
            X.0l8 r0 = X.C10950l8.A05
            r1.A04 = r0
            r0 = 10
            r1.A00 = r0
            r1.A07 = r2
            com.facebook.messaging.service.model.FetchThreadListParams r3 = new com.facebook.messaging.service.model.FetchThreadListParams
            r3.<init>(r1)
            int r2 = X.AnonymousClass1Y3.ARS
            X.0UN r1 = r4.A00
            r0 = 24
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.7PV r1 = (X.AnonymousClass7PV) r1
            com.facebook.common.callercontext.CallerContext r0 = r14.A01
            com.facebook.messaging.service.model.FetchThreadListResult r0 = r1.A0G(r3, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r0
        L_0x0292:
            r5 = r13
            X.0lo r5 = (X.C11070lo) r5
            boolean r0 = X.C11070lo.A07(r5)
            if (r0 != 0) goto L_0x02ab
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A49
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r14)
            return r0
        L_0x02ab:
            java.lang.String r0 = "fetchPinnedThreadsParams"
            X.AnonymousClass0ls.A00(r14, r0)
            X.0lu r4 = new X.0lu
            r4.<init>()
            X.0lv r3 = X.C11120lv.SMS
            int r2 = X.AnonymousClass1Y3.BQB
            X.0UN r1 = r5.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            r4.A00(r3, r0, r14)
            X.0lv r3 = X.C11120lv.FACEBOOK
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r5.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            r4.A00(r3, r0, r14)
            X.4zL r0 = new X.4zL
            r0.<init>()
            com.facebook.fbservice.service.OperationResult r0 = X.C11070lo.A01(r4, r0)
            return r0
        L_0x02df:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0M(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0N(C11060ln r16, C27311cz r17) {
        Message message;
        if (!(this instanceof C10840kw)) {
            C11060ln r3 = r16;
            if (this instanceof C11070lo) {
                return C11070lo.A00((C11070lo) this, C11070lo.A03(((FetchMessagesContextParams) AnonymousClass0ls.A00(r3, "fetchMessagesContextParams")).A03)).BAz(r3);
            }
            if (!(this instanceof C12380pF)) {
                C11150lz r0 = (C11150lz) this;
                C27311cz r7 = r17;
                if (r0 instanceof C12370pE) {
                    C12370pE r02 = (C12370pE) r0;
                    FetchMessagesContextParams fetchMessagesContextParams = (FetchMessagesContextParams) AnonymousClass0ls.A00(r3, "fetchMessagesContextParams");
                    ((C12450pN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AwM, r02.A00)).A04(C12500pT.A05, "fetchMessagesContext (DSH). %s", fetchMessagesContextParams.A03);
                    if (!C06850cB.A0B(fetchMessagesContextParams.A04)) {
                        message = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r02.A00)).A08(fetchMessagesContextParams.A04);
                    } else {
                        message = null;
                        LinkedHashMap linkedHashMap = C26691br.A01((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r02.A00), C06160ax.A03("timestamp_ms", Long.toString(fetchMessagesContextParams.A02)), null, 1, false).A00;
                        if (!linkedHashMap.isEmpty()) {
                            message = (Message) linkedHashMap.values().iterator().next();
                        }
                    }
                    if (message != null) {
                        long j = (long) -1;
                        FetchMoreMessagesResult A0E = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r02.A00)).A0E(fetchMessagesContextParams.A03, message.A03, j, fetchMessagesContextParams.A01, " ASC");
                        MessagesCollection messagesCollection = A0E.A02;
                        Preconditions.checkNotNull(messagesCollection);
                        MessagesCollection messagesCollection2 = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r02.A00)).A0E(fetchMessagesContextParams.A03, j, message.A03, fetchMessagesContextParams.A00, " DESC").A02;
                        Preconditions.checkNotNull(messagesCollection2);
                        C33881oI A002 = MessagesCollection.A00(AnonymousClass0mB.A00((AnonymousClass0mB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BFT, r02.A00), messagesCollection, messagesCollection2, true));
                        A002.A04 = false;
                        MessagesCollection A003 = A002.A00();
                        AnonymousClass2R5 r2 = new AnonymousClass2R5();
                        Preconditions.checkNotNull(A003);
                        r2.A02 = A003;
                        DataFetchDisposition dataFetchDisposition = A0E.A01;
                        Preconditions.checkNotNull(dataFetchDisposition);
                        r2.A01 = dataFetchDisposition;
                        r2.A00 = A0E.A00;
                        Preconditions.checkNotNull(r2.A02);
                        Preconditions.checkNotNull(r2.A01);
                        return OperationResult.A04(new FetchMessagesContextResult(r2));
                    }
                } else if (r0 instanceof C11130lx) {
                    C11130lx r03 = (C11130lx) r0;
                    boolean A012 = C11130lx.A01(r03);
                    try {
                        OperationResult BAz = r7.BAz(r3);
                        AnonymousClass0m6 r4 = ((C14800u9) r03.A01.get()).A01;
                        AnonymousClass0m6.A09(r4, ((FetchMessagesContextResult) BAz.A07()).A02, r4.A07, true);
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r03.A00)).A03();
                        }
                    }
                }
                return r7.BAz(r3);
            }
            FetchMessagesContextParams fetchMessagesContextParams2 = (FetchMessagesContextParams) AnonymousClass0ls.A00(r3, "fetchMessagesContextParams");
            AnonymousClass7PV r42 = (AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, ((C12380pF) this).A00);
            CallerContext callerContext = r3.A01;
            ThreadKey threadKey = fetchMessagesContextParams2.A03;
            OperationResult A04 = OperationResult.A04((ThreadKey.A09(threadKey) || ThreadKey.A0B(threadKey)) ? (FetchMessagesContextResult) AnonymousClass7PV.A07(r42, ImmutableSet.A04(threadKey), new AnonymousClass2R8(r42, fetchMessagesContextParams2, callerContext), null) : r42.A0B(fetchMessagesContextParams2, callerContext));
            C180878aL.A00(A04);
            return A04;
        }
        throw C000300h.A00();
    }

    public OperationResult A0O(C11060ln r6, C27311cz r7) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r6);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r1 = (C11150lz) this;
                if (!(r1 instanceof C12370pE)) {
                    boolean z = r1 instanceof C11130lx;
                }
                return r7.BAz(r6);
            }
            FetchMoreThreadsParams fetchMoreThreadsParams = (FetchMoreThreadsParams) r6.A00.getParcelable("fetchMoreThreadsParams");
            Preconditions.checkArgument(fetchMoreThreadsParams.A04.equals(C10950l8.A09));
            OperationResult A04 = OperationResult.A04(((AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, ((C12380pF) this).A00)).A0F(fetchMoreThreadsParams, r6.A01));
            C180878aL.A00(A04);
            return A04;
        }
    }

    public OperationResult A0P(C11060ln r26, C27311cz r27) {
        OperationResult operationResult;
        int i;
        MessagesCollection messagesCollection;
        int i2;
        MessagesCollection messagesCollection2;
        C11060ln r0 = r26;
        if (this instanceof C10840kw) {
            C30311hq r3 = (C30311hq) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ah2, ((C10840kw) this).A00);
            FetchMoreMessagesParams fetchMoreMessagesParams = (FetchMoreMessagesParams) AnonymousClass0ls.A00(r0, "fetchMoreMessagesParams");
            return OperationResult.A04(((C12420pJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abq, r3.A00)).A02() ? ((C30321hr) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Any, r3.A00)).A01().A0B(fetchMoreMessagesParams.A02, fetchMoreMessagesParams.A01, fetchMoreMessagesParams.A00) : null);
        } else if (this instanceof C11070lo) {
            return C11070lo.A00((C11070lo) this, C11070lo.A03(((FetchMoreMessagesParams) AnonymousClass0ls.A00(r0, "fetchMoreMessagesParams")).A02)).BAz(r0);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r4 = (C11150lz) this;
                if (!(r4 instanceof C12440pM)) {
                    C27311cz r5 = r27;
                    if (r4 instanceof C12370pE) {
                        C12370pE r42 = (C12370pE) r4;
                        FetchMoreMessagesParams fetchMoreMessagesParams2 = (FetchMoreMessagesParams) r0.A00.getParcelable("fetchMoreMessagesParams");
                        ThreadKey threadKey = fetchMoreMessagesParams2.A02;
                        C12520pV r32 = C12500pT.A05;
                        ((C12450pN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AwM, r42.A00)).A03(r32, "fetchMoreMessages (DSH). " + threadKey);
                        long j = fetchMoreMessagesParams2.A01;
                        int i3 = fetchMoreMessagesParams2.A00;
                        boolean z = fetchMoreMessagesParams2.A03;
                        C005505z.A03("DbServiceHandler.handleFetchThread", -1262382082);
                        try {
                            FetchMoreMessagesResult A0E = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r42.A00)).A0E(threadKey, 0, j, i3, " DESC");
                            MessagesCollection messagesCollection3 = A0E.A02;
                            if (!z || !(A0E == FetchMoreMessagesResult.A04 || messagesCollection3 == null || (messagesCollection3.A01.size() != i3 && !messagesCollection3.A02))) {
                                operationResult = OperationResult.A04(A0E);
                                C12370pE.A03(operationResult);
                                i = 102853576;
                            } else {
                                FetchMoreMessagesParams fetchMoreMessagesParams3 = (FetchMoreMessagesParams) AnonymousClass0ls.A00(r0, "fetchMoreMessagesParams");
                                MessagesCollection messagesCollection4 = A0E.A02;
                                Message A06 = messagesCollection4 != null ? messagesCollection4.A06() : null;
                                if (A06 != null) {
                                    String str = r0.A04;
                                    String str2 = r0.A05;
                                    Bundle bundle = r0.A00;
                                    C16890xw r15 = r0.A03;
                                    CallerContext callerContext = r0.A01;
                                    C11030ld r7 = r0.A02;
                                    bundle.putParcelable("fetchMoreMessagesParams", new FetchMoreMessagesParams(fetchMoreMessagesParams3.A02, A06.A03, (fetchMoreMessagesParams3.A00 - messagesCollection4.A04()) + 1, fetchMoreMessagesParams3.A03, fetchMoreMessagesParams3.A04));
                                    r0 = new C11060ln(str2, bundle, str, r15, callerContext, r7);
                                }
                                OperationResult BAz = r5.BAz(r0);
                                FetchMoreMessagesResult fetchMoreMessagesResult = (FetchMoreMessagesResult) BAz.A08();
                                if (!fetchMoreMessagesParams3.A04) {
                                    C52232ij r72 = (C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r42.A00);
                                    if (!(fetchMoreMessagesResult == null || fetchMoreMessagesResult.A02 == null)) {
                                        SQLiteDatabase A062 = ((C25771aN) r72.A0F.get()).A06();
                                        C007406x.A01(A062, 1352141954);
                                        if (A0E != null) {
                                            try {
                                                if (A0E.A01.A08 && (messagesCollection2 = A0E.A02) != null && !r72.A07.A02(messagesCollection2, fetchMoreMessagesResult.A02)) {
                                                    C007406x.A02(A062, 1444338878);
                                                }
                                            } catch (SQLException e) {
                                                C52232ij.A09(e);
                                                i2 = -974523383;
                                            } catch (Throwable th) {
                                                C007406x.A02(A062, 1082567789);
                                                throw th;
                                            }
                                        }
                                        C52232ij.A06(r72, fetchMoreMessagesResult.A02, true);
                                        A062.setTransactionSuccessful();
                                        i2 = 1674023590;
                                        C007406x.A02(A062, i2);
                                    }
                                }
                                if (fetchMoreMessagesResult == null || (messagesCollection = fetchMoreMessagesResult.A02) == null || messagesCollection.A08()) {
                                    operationResult = OperationResult.A04(A0E);
                                    C12370pE.A03(operationResult);
                                } else if (messagesCollection4 == null || messagesCollection4.A08()) {
                                    operationResult = OperationResult.A04(fetchMoreMessagesResult);
                                    C12370pE.A04(operationResult, BAz);
                                } else {
                                    operationResult = OperationResult.A04(new FetchMoreMessagesResult(fetchMoreMessagesResult.A01, AnonymousClass0mB.A00((AnonymousClass0mB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BFT, r42.A00), messagesCollection4, fetchMoreMessagesResult.A02, false), fetchMoreMessagesResult.A00));
                                    C12370pE.A04(operationResult, BAz);
                                }
                                i = -1073231489;
                            }
                            C005505z.A00(i);
                            return operationResult;
                        } catch (Throwable th2) {
                            C005505z.A00(1371824390);
                            throw th2;
                        }
                    } else if (!(r4 instanceof C11130lx)) {
                        return r5.BAz(r0);
                    } else {
                        C11130lx r43 = (C11130lx) r4;
                        boolean A012 = C11130lx.A01(r43);
                        try {
                            OperationResult BAz2 = r5.BAz(r0);
                            FetchMoreMessagesResult fetchMoreMessagesResult2 = (FetchMoreMessagesResult) BAz2.A08();
                            if (fetchMoreMessagesResult2 != null && ((FetchMoreMessagesParams) AnonymousClass0ls.A00(r0, "fetchMoreMessagesParams")).A04) {
                                ((C14800u9) r43.A01.get()).A0A(fetchMoreMessagesResult2.A02);
                            }
                        } finally {
                            if (A012) {
                                ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r43.A00)).A03();
                            }
                        }
                    }
                } else {
                    C12440pM r44 = (C12440pM) r4;
                    C005505z.A03("SmsServiceHandler.handleFetchMoreMessages", -8187345);
                    try {
                        FetchMoreMessagesParams fetchMoreMessagesParams4 = (FetchMoreMessagesParams) r0.A00.getParcelable("fetchMoreMessagesParams");
                        return OperationResult.A04(new FetchMoreMessagesResult(DataFetchDisposition.A0F, C12440pM.A00(r44, fetchMoreMessagesParams4.A02.A03, fetchMoreMessagesParams4.A00, fetchMoreMessagesParams4.A01), ((AnonymousClass06B) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AgK, r44.A01)).now()));
                    } finally {
                        C005505z.A00(-1319413135);
                    }
                }
            } else {
                FetchMoreMessagesParams fetchMoreMessagesParams5 = (FetchMoreMessagesParams) r0.A00.getParcelable("fetchMoreMessagesParams");
                AnonymousClass7PV r45 = (AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, ((C12380pF) this).A00);
                CallerContext callerContext2 = r0.A01;
                ThreadKey threadKey2 = fetchMoreMessagesParams5.A02;
                OperationResult A04 = OperationResult.A04((ThreadKey.A09(threadKey2) || ThreadKey.A0B(threadKey2)) ? (FetchMoreMessagesResult) AnonymousClass7PV.A07(r45, ImmutableSet.A04(threadKey2), new AnonymousClass2R6(r45, fetchMoreMessagesParams5, callerContext2), "gql_thread_query_more_messages") : r45.A0C(fetchMoreMessagesParams5, callerContext2));
                C180878aL.A00(A04);
                return A04;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x015f A[Catch:{ all -> 0x0183 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0Q(X.C11060ln r17, X.C27311cz r18) {
        /*
            r16 = this;
            r1 = r16
            boolean r0 = r1 instanceof X.C10840kw
            if (r0 != 0) goto L_0x01c8
            boolean r0 = r1 instanceof X.C11070lo
            r4 = r17
            if (r0 != 0) goto L_0x01af
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x018b
            r0 = r1
            X.0lz r0 = (X.C11150lz) r0
            boolean r1 = r0 instanceof X.C12370pE
            r7 = r18
            if (r1 != 0) goto L_0x0070
            boolean r1 = r0 instanceof X.C11130lx
            if (r1 != 0) goto L_0x0022
            com.facebook.fbservice.service.OperationResult r6 = r7.BAz(r4)
        L_0x0021:
            return r6
        L_0x0022:
            X.0lx r0 = (X.C11130lx) r0
            boolean r8 = X.C11130lx.A01(r0)
            r5 = 12
            com.facebook.fbservice.service.OperationResult r6 = r7.BAz(r4)     // Catch:{ all -> 0x005f }
            java.lang.Object r3 = r6.A08()     // Catch:{ all -> 0x005f }
            com.facebook.messaging.service.model.FetchMoreRecentMessagesResult r3 = (com.facebook.messaging.service.model.FetchMoreRecentMessagesResult) r3     // Catch:{ all -> 0x005f }
            if (r3 == 0) goto L_0x004f
            java.lang.String r1 = "fetchMoreRecentMessagesParams"
            android.os.Parcelable r1 = X.AnonymousClass0ls.A00(r4, r1)     // Catch:{ all -> 0x005f }
            com.facebook.messaging.service.model.FetchMoreRecentMessagesParams r1 = (com.facebook.messaging.service.model.FetchMoreRecentMessagesParams) r1     // Catch:{ all -> 0x005f }
            boolean r1 = r1.A05     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x004f
            X.0US r1 = r0.A01     // Catch:{ all -> 0x005f }
            java.lang.Object r2 = r1.get()     // Catch:{ all -> 0x005f }
            X.0u9 r2 = (X.C14800u9) r2     // Catch:{ all -> 0x005f }
            com.facebook.messaging.model.messages.MessagesCollection r1 = r3.A02     // Catch:{ all -> 0x005f }
            r2.A0A(r1)     // Catch:{ all -> 0x005f }
        L_0x004f:
            if (r8 == 0) goto L_0x0021
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            return r6
        L_0x005f:
            r2 = move-exception
            if (r8 == 0) goto L_0x006f
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x006f:
            throw r2
        L_0x0070:
            X.0pE r0 = (X.C12370pE) r0
            java.lang.String r1 = "fetchMoreRecentMessagesParams"
            android.os.Parcelable r6 = X.AnonymousClass0ls.A00(r4, r1)
            com.facebook.messaging.service.model.FetchMoreRecentMessagesParams r6 = (com.facebook.messaging.service.model.FetchMoreRecentMessagesParams) r6
            com.facebook.messaging.model.threadkey.ThreadKey r9 = r6.A02
            int r3 = X.AnonymousClass1Y3.AwM
            X.0UN r2 = r0.A00
            r1 = 9
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0pN r8 = (X.C12450pN) r8
            X.0pV r5 = X.C12500pT.A05
            r1 = 1
            java.lang.Object[] r3 = new java.lang.Object[]{r9}
            java.lang.String r2 = "fetchMoreRecentMessages (DSH). %s"
            r8.A04(r5, r2, r3)
            java.lang.String r3 = "DbServiceHandler.handleFetchMoreRecentMessages"
            r2 = -1610912413(0xffffffff9ffb6d63, float:-1.064836E-19)
            X.C005505z.A03(r3, r2)
            int r3 = X.AnonymousClass1Y3.AcJ     // Catch:{ all -> 0x0183 }
            X.0UN r2 = r0.A00     // Catch:{ all -> 0x0183 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r3, r2)     // Catch:{ all -> 0x0183 }
            X.1br r5 = (X.C26691br) r5     // Catch:{ all -> 0x0183 }
            java.lang.String r3 = r6.A03     // Catch:{ all -> 0x0183 }
            java.lang.String r2 = r6.A04     // Catch:{ all -> 0x0183 }
            com.facebook.messaging.model.messages.Message r2 = r5.A0A(r3, r2)     // Catch:{ all -> 0x0183 }
            if (r2 != 0) goto L_0x00b9
            com.facebook.fbservice.service.OperationResult r6 = r7.BAz(r4)     // Catch:{ all -> 0x0183 }
            r0 = -1563049147(0xffffffffa2d5c345, float:-5.7940515E-18)
            goto L_0x017f
        L_0x00b9:
            int r3 = X.AnonymousClass1Y3.AcJ     // Catch:{ all -> 0x0183 }
            X.0UN r2 = r0.A00     // Catch:{ all -> 0x0183 }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r3, r2)     // Catch:{ all -> 0x0183 }
            X.1br r8 = (X.C26691br) r8     // Catch:{ all -> 0x0183 }
            long r10 = r6.A01     // Catch:{ all -> 0x0183 }
            r2 = -1
            long r12 = (long) r2     // Catch:{ all -> 0x0183 }
            int r14 = r6.A00     // Catch:{ all -> 0x0183 }
            java.lang.String r15 = " ASC"
            com.facebook.messaging.service.model.FetchMoreMessagesResult r7 = r8.A0E(r9, r10, r12, r14, r15)     // Catch:{ all -> 0x0183 }
            X.B77 r5 = com.facebook.messaging.service.model.FetchMoreRecentMessagesResult.A00(r7)     // Catch:{ all -> 0x0183 }
            com.facebook.messaging.model.messages.MessagesCollection r2 = r7.A02     // Catch:{ all -> 0x0183 }
            if (r2 == 0) goto L_0x0171
            int r3 = r2.A04()     // Catch:{ all -> 0x0183 }
            int r2 = r6.A00     // Catch:{ all -> 0x0183 }
            if (r3 != r2) goto L_0x0171
            int r2 = X.AnonymousClass1Y3.AcJ     // Catch:{ all -> 0x0183 }
            X.0UN r0 = r0.A00     // Catch:{ all -> 0x0183 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r2, r0)     // Catch:{ all -> 0x0183 }
            X.1br r6 = (X.C26691br) r6     // Catch:{ all -> 0x0183 }
            if (r7 == 0) goto L_0x015c
            com.facebook.messaging.model.messages.MessagesCollection r0 = r7.A02     // Catch:{ all -> 0x0183 }
            if (r0 == 0) goto L_0x015c
            boolean r0 = r0.A08()     // Catch:{ all -> 0x0183 }
            if (r0 != 0) goto L_0x015c
            com.facebook.messaging.model.messages.MessagesCollection r0 = r7.A02     // Catch:{ all -> 0x0183 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A00     // Catch:{ all -> 0x0183 }
            X.1a6 r4 = X.C06160ax.A00()     // Catch:{ all -> 0x0183 }
            java.lang.String r2 = r0.A0J()     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = "thread_key"
            X.0av r0 = X.C06160ax.A03(r0, r2)     // Catch:{ all -> 0x0183 }
            r4.A05(r0)     // Catch:{ all -> 0x0183 }
            java.lang.String r3 = "timestamp_ms DESC"
            r0 = 0
            X.2yW r2 = X.C26691br.A01(r6, r4, r3, r1, r0)     // Catch:{ all -> 0x0183 }
            java.util.LinkedHashMap r0 = r2.A00     // Catch:{ all -> 0x0183 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x0183 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0183 }
            if (r0 == 0) goto L_0x011f
            r6 = 0
            goto L_0x012f
        L_0x011f:
            java.util.LinkedHashMap r0 = r2.A00     // Catch:{ all -> 0x0183 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x0183 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0183 }
            java.lang.Object r6 = r0.next()     // Catch:{ all -> 0x0183 }
            com.facebook.messaging.model.messages.Message r6 = (com.facebook.messaging.model.messages.Message) r6     // Catch:{ all -> 0x0183 }
        L_0x012f:
            X.AnonymousClass0qX.A00(r6)     // Catch:{ all -> 0x0183 }
            com.facebook.messaging.model.messages.MessagesCollection r0 = r7.A02     // Catch:{ all -> 0x0183 }
            com.google.common.collect.ImmutableList r4 = r0.A01     // Catch:{ all -> 0x0183 }
            int r3 = r4.size()     // Catch:{ all -> 0x0183 }
            int r3 = r3 - r1
        L_0x013b:
            if (r3 < 0) goto L_0x015c
            java.lang.Object r2 = r4.get(r3)     // Catch:{ all -> 0x0183 }
            com.facebook.messaging.model.messages.Message r2 = (com.facebook.messaging.model.messages.Message) r2     // Catch:{ all -> 0x0183 }
            java.lang.String r1 = r2.A0q     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = r6.A0q     // Catch:{ all -> 0x0183 }
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)     // Catch:{ all -> 0x0183 }
            if (r0 != 0) goto L_0x015a
            java.lang.String r1 = r2.A0w     // Catch:{ all -> 0x0183 }
            java.lang.String r0 = r6.A0w     // Catch:{ all -> 0x0183 }
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)     // Catch:{ all -> 0x0183 }
            if (r0 != 0) goto L_0x015a
            int r3 = r3 + -1
            goto L_0x013b
        L_0x015a:
            r0 = 1
            goto L_0x015d
        L_0x015c:
            r0 = 0
        L_0x015d:
            if (r0 != 0) goto L_0x0171
            com.facebook.messaging.model.messages.MessagesCollection r0 = r7.A02     // Catch:{ all -> 0x0183 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x0183 }
            X.1oI r1 = com.facebook.messaging.model.messages.MessagesCollection.A00(r0)     // Catch:{ all -> 0x0183 }
            r0 = 0
            r1.A04 = r0     // Catch:{ all -> 0x0183 }
            com.facebook.messaging.model.messages.MessagesCollection r0 = r1.A00()     // Catch:{ all -> 0x0183 }
            r5.A02 = r0     // Catch:{ all -> 0x0183 }
        L_0x0171:
            com.facebook.messaging.service.model.FetchMoreRecentMessagesResult r0 = r5.A00()     // Catch:{ all -> 0x0183 }
            com.facebook.fbservice.service.OperationResult r6 = com.facebook.fbservice.service.OperationResult.A04(r0)     // Catch:{ all -> 0x0183 }
            X.C12370pE.A03(r6)     // Catch:{ all -> 0x0183 }
            r0 = -1213044302(0xffffffffb7b269b2, float:-2.1268472E-5)
        L_0x017f:
            X.C005505z.A00(r0)
            return r6
        L_0x0183:
            r1 = move-exception
            r0 = 1568974024(0x5d84a4c8, float:1.19474803E18)
            X.C005505z.A00(r0)
            throw r1
        L_0x018b:
            X.0pF r1 = (X.C12380pF) r1
            java.lang.String r0 = "fetchMoreRecentMessagesParams"
            android.os.Parcelable r3 = X.AnonymousClass0ls.A00(r4, r0)
            com.facebook.messaging.service.model.FetchMoreRecentMessagesParams r3 = (com.facebook.messaging.service.model.FetchMoreRecentMessagesParams) r3
            int r2 = X.AnonymousClass1Y3.ARS
            X.0UN r1 = r1.A00
            r0 = 24
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.7PV r1 = (X.AnonymousClass7PV) r1
            com.facebook.common.callercontext.CallerContext r0 = r4.A01
            com.facebook.messaging.service.model.FetchMoreRecentMessagesResult r0 = r1.A0E(r3, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r0)
            X.C180878aL.A00(r0)
            return r0
        L_0x01af:
            X.0lo r1 = (X.C11070lo) r1
            java.lang.String r0 = "fetchMoreRecentMessagesParams"
            android.os.Parcelable r0 = X.AnonymousClass0ls.A00(r4, r0)
            com.facebook.messaging.service.model.FetchMoreRecentMessagesParams r0 = (com.facebook.messaging.service.model.FetchMoreRecentMessagesParams) r0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A02
            X.0lv r0 = X.C11070lo.A03(r0)
            X.1cz r0 = X.C11070lo.A00(r1, r0)
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r4)
            return r0
        L_0x01c8:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0Q(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0277, code lost:
        if (r2 == X.C10950l8.A0C) goto L_0x0279;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ff, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0100, code lost:
        if (r6 != null) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0170, code lost:
        if (r6.A0A(X.C42762Bs.A01(r9.dbName)) == false) goto L_0x0172;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x0105 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0R(X.C11060ln r19, X.C27311cz r20) {
        /*
            r18 = this;
            r1 = r18
            boolean r0 = r1 instanceof X.C10840kw
            r3 = r19
            if (r0 != 0) goto L_0x03cb
            boolean r0 = r1 instanceof X.C11070lo
            if (r0 != 0) goto L_0x03af
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x0307
            r0 = r1
            X.0lz r0 = (X.C11150lz) r0
            boolean r1 = r0 instanceof X.C27561dO
            r5 = r20
            if (r1 != 0) goto L_0x00b2
            boolean r1 = r0 instanceof X.C12440pM
            if (r1 != 0) goto L_0x0250
            boolean r1 = r0 instanceof X.C12370pE
            if (r1 != 0) goto L_0x0117
            boolean r1 = r0 instanceof X.C11130lx
            if (r1 != 0) goto L_0x002a
            com.facebook.fbservice.service.OperationResult r13 = r5.BAz(r3)
        L_0x0029:
            return r13
        L_0x002a:
            X.0lx r0 = (X.C11130lx) r0
            int r4 = X.AnonymousClass1Y3.AwM
            X.0UN r2 = r0.A00
            r1 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.0pN r4 = (X.C12450pN) r4
            X.0pV r2 = X.C12500pT.A03
            java.lang.String r1 = "fetchMoreThreads (CSH)."
            r4.A03(r2, r1)
            boolean r9 = X.C11130lx.A01(r0)
            r4 = 12
            com.facebook.fbservice.service.OperationResult r13 = r5.BAz(r3)     // Catch:{ all -> 0x0106 }
            java.lang.Object r5 = r13.A08()     // Catch:{ all -> 0x0106 }
            com.facebook.messaging.service.model.FetchMoreThreadsResult r5 = (com.facebook.messaging.service.model.FetchMoreThreadsResult) r5     // Catch:{ all -> 0x0106 }
            X.0US r1 = r0.A01     // Catch:{ all -> 0x0106 }
            java.lang.Object r3 = r1.get()     // Catch:{ all -> 0x0106 }
            X.0u9 r3 = (X.C14800u9) r3     // Catch:{ all -> 0x0106 }
            X.0t0 r2 = r3.A02     // Catch:{ all -> 0x0106 }
            com.google.common.collect.ImmutableList r1 = r5.A06     // Catch:{ all -> 0x0106 }
            r2.A06(r1)     // Catch:{ all -> 0x0106 }
            X.0m6 r8 = r3.A01     // Catch:{ all -> 0x0106 }
            X.0l8 r2 = r5.A02     // Catch:{ all -> 0x0106 }
            com.facebook.messaging.model.threads.ThreadsCollection r7 = r5.A03     // Catch:{ all -> 0x0106 }
            X.0mL r1 = r8.A0A     // Catch:{ all -> 0x0106 }
            X.0mM r6 = r1.A00()     // Catch:{ all -> 0x0106 }
            X.0pa r5 = X.AnonymousClass0m6.A00(r8, r2)     // Catch:{ all -> 0x00fd }
            com.google.common.collect.ImmutableList r1 = r7.A00     // Catch:{ all -> 0x00fd }
            X.1Xv r3 = r1.iterator()     // Catch:{ all -> 0x00fd }
        L_0x0073:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x00fd }
            if (r1 == 0) goto L_0x008b
            java.lang.Object r2 = r3.next()     // Catch:{ all -> 0x00fd }
            com.facebook.messaging.model.threads.ThreadSummary r2 = (com.facebook.messaging.model.threads.ThreadSummary) r2     // Catch:{ all -> 0x00fd }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r2.A0S     // Catch:{ all -> 0x00fd }
            X.AnonymousClass0m6.A0A(r8, r1)     // Catch:{ all -> 0x00fd }
            r5.A02(r2)     // Catch:{ all -> 0x00fd }
            r8.A0b(r2)     // Catch:{ all -> 0x00fd }
            goto L_0x0073
        L_0x008b:
            boolean r2 = r7.A01     // Catch:{ all -> 0x00fd }
            X.0mL r1 = r5.A06     // Catch:{ all -> 0x00fd }
            r1.A01()     // Catch:{ all -> 0x00fd }
            r5.A02 = r2     // Catch:{ all -> 0x00fd }
            X.0m9 r3 = r8.A0B     // Catch:{ all -> 0x00fd }
            X.0l8 r2 = r5.A07     // Catch:{ all -> 0x00fd }
            java.lang.String r1 = "addMoreThreadsToThreadList"
            r3.A0D(r5, r2, r1)     // Catch:{ all -> 0x00fd }
            if (r6 == 0) goto L_0x00a2
            r6.close()     // Catch:{ all -> 0x0106 }
        L_0x00a2:
            if (r9 == 0) goto L_0x0029
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            return r13
        L_0x00b2:
            X.1dO r0 = (X.C27561dO) r0
            android.os.Bundle r2 = r3.A00
            java.lang.String r1 = "fetchMoreThreadsParams"
            android.os.Parcelable r11 = r2.getParcelable(r1)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r11 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r11
            com.google.common.collect.ImmutableSet r2 = r11.A07
            com.facebook.fbservice.service.OperationResult r13 = r5.BAz(r3)
            if (r2 == 0) goto L_0x0029
            boolean r1 = r2.isEmpty()
            if (r1 != 0) goto L_0x0029
            java.lang.Object r8 = r13.A07()
            com.facebook.messaging.service.model.FetchMoreThreadsResult r8 = (com.facebook.messaging.service.model.FetchMoreThreadsResult) r8
            com.facebook.messaging.model.threads.ThreadsCollection r10 = r8.A03
            if (r10 == 0) goto L_0x0303
            long r6 = r11.A02
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            X.1Xv r12 = r2.iterator()
        L_0x00e1:
            boolean r1 = r12.hasNext()
            if (r1 == 0) goto L_0x02d6
            java.lang.Object r1 = r12.next()
            X.2e3 r1 = (X.C50452e3) r1
            com.facebook.messaging.model.threads.ThreadSummary r4 = X.C27561dO.A01(r0, r1, r5)
            if (r4 == 0) goto L_0x00e1
            long r2 = r4.A0B
            int r1 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r1 > 0) goto L_0x00e1
            r9.add(r4)
            goto L_0x00e1
        L_0x00fd:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x00ff }
        L_0x00ff:
            r1 = move-exception
            if (r6 == 0) goto L_0x0105
            r6.close()     // Catch:{ all -> 0x0105 }
        L_0x0105:
            throw r1     // Catch:{ all -> 0x0106 }
        L_0x0106:
            r2 = move-exception
            if (r9 == 0) goto L_0x0116
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x0116:
            throw r2
        L_0x0117:
            X.0pE r0 = (X.C12370pE) r0
            int r4 = X.AnonymousClass1Y3.AwM
            X.0UN r2 = r0.A00
            r1 = 9
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.0pN r4 = (X.C12450pN) r4
            X.0pV r2 = X.C12500pT.A05
            java.lang.String r1 = "fetchMoreThreads (DSH)."
            r4.A03(r2, r1)
            android.os.Bundle r2 = r3.A00
            java.lang.String r1 = "fetchMoreThreadsParams"
            android.os.Parcelable r4 = r2.getParcelable(r1)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r4 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r4
            int r6 = X.AnonymousClass1Y3.AV7
            X.0UN r2 = r0.A00
            r1 = 5
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r6, r2)
            X.0ph r6 = (X.C12620ph) r6
            X.0l8 r9 = r4.A04
            X.0Tq r1 = r6.A05
            java.lang.Object r8 = r1.get()
            X.0cb r8 = (X.C07080cb) r8
            X.1eA r7 = X.C12740pt.A01(r9)
            r1 = -1
            long r13 = r8.A01(r7, r1)
            long r1 = r4.A01
            int r7 = r4.A00
            com.google.common.collect.ImmutableList r7 = r6.A07(r9, r1, r7)
            int r2 = r7.size()
            int r1 = r4.A00
            if (r2 >= r1) goto L_0x0172
            java.lang.String r1 = r9.dbName
            java.lang.String r1 = X.C42762Bs.A01(r1)
            boolean r1 = r6.A0A(r1)
            r2 = 1
            if (r1 != 0) goto L_0x0173
        L_0x0172:
            r2 = 0
        L_0x0173:
            int r1 = r4.A00
            com.google.common.collect.ImmutableList r1 = X.C12620ph.A02(r7, r1)
            com.facebook.messaging.model.threads.ThreadsCollection r10 = new com.facebook.messaging.model.threads.ThreadsCollection
            r10.<init>(r1, r2)
            java.util.Set r2 = X.C12620ph.A03(r7)
            X.1e7 r1 = r6.A00
            com.google.common.collect.ImmutableList r12 = r1.A04(r2)
            com.facebook.messaging.service.model.FetchMoreThreadsResult r7 = new com.facebook.messaging.service.model.FetchMoreThreadsResult
            com.facebook.fbservice.results.DataFetchDisposition r8 = com.facebook.fbservice.results.DataFetchDisposition.A0F
            r11 = 0
            r15 = 0
            r7.<init>(r8, r9, r10, r11, r12, r13, r15)
            com.facebook.messaging.model.threads.ThreadsCollection r2 = r7.A03
            boolean r1 = r2.A01
            if (r1 != 0) goto L_0x0248
            int r2 = r2.A02()
            int r1 = r4.A00
            if (r2 == r1) goto L_0x0248
            X.0hU r2 = r4.A03
            X.0hU r1 = X.C09510hU.DO_NOT_CHECK_SERVER
            if (r2 == r1) goto L_0x0248
            com.facebook.fbservice.service.OperationResult r13 = r5.BAz(r3)
            java.lang.Object r5 = r13.A08()
            com.facebook.messaging.service.model.FetchMoreThreadsResult r5 = (com.facebook.messaging.service.model.FetchMoreThreadsResult) r5
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BJD
            X.0UN r0 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2ij r4 = (X.C52232ij) r4
            X.0Tq r0 = r4.A0F
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r3 = r0.A06()
            r0 = -197303054(0xfffffffff43d64f2, float:-6.0021455E31)
            X.C007406x.A01(r3, r0)
            long r1 = r5.A00     // Catch:{ SQLException -> 0x0231 }
            com.facebook.messaging.model.threads.ThreadsCollection r6 = r5.A03     // Catch:{ SQLException -> 0x0231 }
            X.0l8 r0 = r5.A02     // Catch:{ SQLException -> 0x0231 }
            java.lang.String r0 = r0.dbName     // Catch:{ SQLException -> 0x0231 }
            X.C52232ij.A0E(r4, r0, r6)     // Catch:{ SQLException -> 0x0231 }
            com.google.common.collect.ImmutableList r0 = r6.A00     // Catch:{ SQLException -> 0x0231 }
            X.1Xv r7 = r0.iterator()     // Catch:{ SQLException -> 0x0231 }
        L_0x01dd:
            boolean r0 = r7.hasNext()     // Catch:{ SQLException -> 0x0231 }
            if (r0 == 0) goto L_0x01ed
            java.lang.Object r6 = r7.next()     // Catch:{ SQLException -> 0x0231 }
            com.facebook.messaging.model.threads.ThreadSummary r6 = (com.facebook.messaging.model.threads.ThreadSummary) r6     // Catch:{ SQLException -> 0x0231 }
            X.C52232ij.A05(r4, r6, r1, r11)     // Catch:{ SQLException -> 0x0231 }
            goto L_0x01dd
        L_0x01ed:
            r2 = 7
            int r1 = X.AnonymousClass1Y3.AkC     // Catch:{ SQLException -> 0x0231 }
            X.0UN r0 = r4.A00     // Catch:{ SQLException -> 0x0231 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ SQLException -> 0x0231 }
            X.9PD r1 = (X.AnonymousClass9PD) r1     // Catch:{ SQLException -> 0x0231 }
            com.google.common.collect.ImmutableList r0 = r5.A05     // Catch:{ SQLException -> 0x0231 }
            r1.A03(r3, r0)     // Catch:{ SQLException -> 0x0231 }
            com.google.common.collect.ImmutableList r1 = r5.A06     // Catch:{ SQLException -> 0x0231 }
            if (r1 == 0) goto L_0x020c
            boolean r0 = r1.isEmpty()     // Catch:{ SQLException -> 0x0231 }
            if (r0 != 0) goto L_0x020c
            X.2sv r0 = r4.A05     // Catch:{ SQLException -> 0x0231 }
            r0.A01(r1)     // Catch:{ SQLException -> 0x0231 }
        L_0x020c:
            X.2xv r1 = r4.A08     // Catch:{ SQLException -> 0x0231 }
            X.0l8 r0 = r5.A02     // Catch:{ SQLException -> 0x0231 }
            boolean r0 = r1.A02(r0)     // Catch:{ SQLException -> 0x0231 }
            if (r0 == 0) goto L_0x022d
            com.google.common.collect.ImmutableList r0 = r5.A04     // Catch:{ SQLException -> 0x0231 }
            X.1Xv r2 = r0.iterator()     // Catch:{ SQLException -> 0x0231 }
        L_0x021c:
            boolean r0 = r2.hasNext()     // Catch:{ SQLException -> 0x0231 }
            if (r0 == 0) goto L_0x022d
            java.lang.Object r1 = r2.next()     // Catch:{ SQLException -> 0x0231 }
            com.facebook.messaging.model.messages.MessagesCollection r1 = (com.facebook.messaging.model.messages.MessagesCollection) r1     // Catch:{ SQLException -> 0x0231 }
            r0 = 1
            X.C52232ij.A06(r4, r1, r0)     // Catch:{ SQLException -> 0x0231 }
            goto L_0x021c
        L_0x022d:
            r3.setTransactionSuccessful()     // Catch:{ SQLException -> 0x0231 }
            goto L_0x0239
        L_0x0231:
            r0 = move-exception
            X.C52232ij.A09(r0)     // Catch:{ all -> 0x0240 }
            r0 = 1987121971(0x76711333, float:1.22239546E33)
            goto L_0x023c
        L_0x0239:
            r0 = -1243451680(0xffffffffb5e26ee0, float:-1.6870581E-6)
        L_0x023c:
            X.C007406x.A02(r3, r0)
            return r13
        L_0x0240:
            r1 = move-exception
            r0 = 962126283(0x3958e1cb, float:2.068348E-4)
            X.C007406x.A02(r3, r0)
            throw r1
        L_0x0248:
            com.facebook.fbservice.service.OperationResult r13 = com.facebook.fbservice.service.OperationResult.A04(r7)
            X.C12370pE.A03(r13)
            return r13
        L_0x0250:
            X.0pM r0 = (X.C12440pM) r0
            java.lang.String r2 = "SmsServiceHandler.handleFetchMoreThreads"
            r1 = -1400805761(0xffffffffac81667f, float:-3.67778E-12)
            X.C005505z.A03(r2, r1)
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x02ce }
            X.0UN r0 = r0.A01     // Catch:{ all -> 0x02ce }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02ce }
            X.2e4 r5 = (X.C50462e4) r5     // Catch:{ all -> 0x02ce }
            java.lang.String r0 = "fetchMoreThreadsParams"
            android.os.Parcelable r3 = X.AnonymousClass0ls.A00(r3, r0)     // Catch:{ all -> 0x02ce }
            com.facebook.messaging.service.model.FetchMoreThreadsParams r3 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r3     // Catch:{ all -> 0x02ce }
            X.0l8 r2 = r3.A04     // Catch:{ all -> 0x02ce }
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ all -> 0x02ce }
            r4 = 0
            if (r2 == r0) goto L_0x0279
            X.0l8 r0 = X.C10950l8.A0C     // Catch:{ all -> 0x02ce }
            r1 = 0
            if (r2 != r0) goto L_0x027a
        L_0x0279:
            r1 = 1
        L_0x027a:
            java.lang.String r0 = "SMS Threads can only belong in the inbox or recent sms spam threads or sms business threads folder"
            com.google.common.base.Preconditions.checkArgument(r1, r0)     // Catch:{ all -> 0x02ce }
            java.util.HashMap r8 = new java.util.HashMap     // Catch:{ all -> 0x02ce }
            r8.<init>()     // Catch:{ all -> 0x02ce }
            X.0l8 r6 = r3.A04     // Catch:{ all -> 0x02ce }
            int r7 = r3.A00     // Catch:{ all -> 0x02ce }
            long r9 = r3.A02     // Catch:{ all -> 0x02ce }
            java.util.List r0 = r5.A0E(r6, r7, r8, r9)     // Catch:{ all -> 0x02ce }
            com.facebook.messaging.model.threads.ThreadsCollection r12 = new com.facebook.messaging.model.threads.ThreadsCollection     // Catch:{ all -> 0x02ce }
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x02ce }
            int r1 = r0.size()     // Catch:{ all -> 0x02ce }
            int r0 = r3.A00     // Catch:{ all -> 0x02ce }
            if (r1 == r0) goto L_0x029d
            r4 = 1
        L_0x029d:
            r12.<init>(r2, r4)     // Catch:{ all -> 0x02ce }
            com.facebook.messaging.service.model.FetchMoreThreadsResult r9 = new com.facebook.messaging.service.model.FetchMoreThreadsResult     // Catch:{ all -> 0x02ce }
            com.facebook.fbservice.results.DataFetchDisposition r10 = com.facebook.fbservice.results.DataFetchDisposition.A0H     // Catch:{ all -> 0x02ce }
            X.0l8 r11 = r3.A04     // Catch:{ all -> 0x02ce }
            r13 = 0
            java.util.Collection r0 = r8.values()     // Catch:{ all -> 0x02ce }
            com.google.common.collect.ImmutableList r14 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x02ce }
            r2 = 4
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x02ce }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x02ce }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x02ce }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x02ce }
            long r15 = r0.now()     // Catch:{ all -> 0x02ce }
            r17 = 0
            r9.<init>(r10, r11, r12, r13, r14, r15, r17)     // Catch:{ all -> 0x02ce }
            com.facebook.fbservice.service.OperationResult r13 = com.facebook.fbservice.service.OperationResult.A04(r9)     // Catch:{ all -> 0x02ce }
            r0 = 1951469240(0x74510eb8, float:6.6252965E31)
            X.C005505z.A00(r0)
            return r13
        L_0x02ce:
            r1 = move-exception
            r0 = -2114130474(0xffffffff81fcedd6, float:-9.291146E-38)
            X.C005505z.A00(r0)
            throw r1
        L_0x02d6:
            boolean r1 = r9.isEmpty()
            if (r1 != 0) goto L_0x0303
            int r1 = r11.A00
            com.facebook.messaging.model.threads.ThreadsCollection r6 = X.C27561dO.A02(r9, r10, r1)
            com.facebook.messaging.service.model.FetchMoreThreadsResult r3 = new com.facebook.messaging.service.model.FetchMoreThreadsResult
            com.facebook.fbservice.results.DataFetchDisposition r4 = com.facebook.fbservice.results.DataFetchDisposition.A0H
            X.0l8 r5 = r8.A02
            r7 = 0
            com.google.common.collect.ImmutableList r8 = r8.A06
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AnC
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1mI r0 = (X.C32761mI) r0
            long r9 = r0.now()
            r11 = 0
            r3.<init>(r4, r5, r6, r7, r8, r9, r11)
            com.facebook.fbservice.service.OperationResult r13 = com.facebook.fbservice.service.OperationResult.A04(r3)
            return r13
        L_0x0303:
            X.C27561dO.A04(r0)
            return r13
        L_0x0307:
            r4 = r1
            X.0pF r4 = (X.C12380pF) r4
            android.os.Bundle r1 = r3.A00
            java.lang.String r0 = "fetchMoreThreadsParams"
            android.os.Parcelable r8 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r8 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r8
            int r2 = X.AnonymousClass1Y3.ARS
            X.0UN r1 = r4.A00
            r0 = 24
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.7PV r7 = (X.AnonymousClass7PV) r7
            com.facebook.common.callercontext.CallerContext r9 = r3.A01
            X.0l8 r1 = r8.A04
            X.0l8 r0 = X.C10950l8.A05
            if (r1 == r0) goto L_0x0334
            com.facebook.messaging.service.model.FetchMoreThreadsResult r10 = r7.A0F(r8, r9)
        L_0x032c:
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r10)
            X.C180878aL.A00(r0)
            return r0
        L_0x0334:
            X.0Tq r0 = r7.A01
            java.lang.Object r6 = r0.get()
            X.3iP r6 = (X.C74743iP) r6
            r5 = 0
            r4 = 0
        L_0x033e:
            r0 = 3
            if (r4 >= r0) goto L_0x03a2
            monitor-enter(r6)
            java.util.Map r0 = r6.A01     // Catch:{ all -> 0x039f }
            com.google.common.collect.ImmutableMap r11 = com.google.common.collect.ImmutableMap.copyOf(r0)     // Catch:{ all -> 0x039f }
            monitor-exit(r6)
            java.lang.String r3 = "gql_thread_query_more_threads"
            X.AnonymousClass7PV.A09(r7, r6, r3)
            com.facebook.messaging.service.model.FetchMoreThreadsResult r10 = r7.A0F(r8, r9)
            X.AnonymousClass7PV.A09(r7, r6, r3)
            X.0dQ r2 = com.google.common.collect.ImmutableSet.A01()
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r10.A03
            com.google.common.collect.ImmutableList r0 = r0.A00
            X.1Xv r1 = r0.iterator()
        L_0x0361:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0373
            java.lang.Object r0 = r1.next()
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            r2.A01(r0)
            goto L_0x0361
        L_0x0373:
            com.google.common.collect.ImmutableSet r2 = r2.build()
            monitor-enter(r6)
            java.util.Map r0 = r6.A01     // Catch:{ all -> 0x039f }
            com.google.common.collect.ImmutableMap r0 = com.google.common.collect.ImmutableMap.copyOf(r0)     // Catch:{ all -> 0x039f }
            monitor-exit(r6)
            com.google.common.collect.ImmutableMap r1 = X.AnonymousClass7PV.A05(r11, r2)
            com.google.common.collect.ImmutableMap r0 = X.AnonymousClass7PV.A05(r0, r2)
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 != 0) goto L_0x032c
            r2 = 11
            int r1 = X.AnonymousClass1Y3.B8c
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7Pc r0 = (X.C157337Pc) r0
            r0.A04(r3)
            int r4 = r4 + 1
            goto L_0x033e
        L_0x039f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x03a2:
            X.1wA r2 = new X.1wA
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Different thread sequence ids for web fetch"
            r1.<init>(r0)
            r2.<init>(r1, r5)
            throw r2
        L_0x03af:
            X.0lo r1 = (X.C11070lo) r1
            java.lang.String r0 = "fetchMoreThreadsParams"
            android.os.Parcelable r4 = X.AnonymousClass0ls.A00(r3, r0)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r4 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r4
            X.0l8 r2 = r4.A04
            X.0ki r0 = r4.A05
            X.0lu r1 = X.C11070lo.A02(r1, r2, r0, r3)
            X.2Sq r0 = new X.2Sq
            r0.<init>(r4, r2)
            com.facebook.fbservice.service.OperationResult r0 = X.C11070lo.A01(r1, r0)
            return r0
        L_0x03cb:
            r4 = r1
            X.0kw r4 = (X.C10840kw) r4
            X.0Tq r0 = r4.A01
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x045b
            r2 = 5
            int r1 = X.AnonymousClass1Y3.Ah2
            X.0UN r0 = r4.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1hq r6 = (X.C30311hq) r6
            java.lang.String r0 = "fetchMoreThreadsParams"
            android.os.Parcelable r4 = X.AnonymousClass0ls.A00(r3, r0)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r4 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r4
            X.0l8 r5 = r4.A04
            X.0l8 r0 = X.C10950l8.A05
            r3 = 1
            r2 = 0
            r1 = 0
            if (r5 != r0) goto L_0x03f3
            r1 = 1
        L_0x03f3:
            java.lang.String r0 = "Tincan messages can only be in the INBOX"
            com.google.common.base.Preconditions.checkArgument(r1, r0)
            int r1 = X.AnonymousClass1Y3.Abq
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pJ r0 = (X.C12420pJ) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x0459
            int r1 = X.AnonymousClass1Y3.Any
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1hr r0 = (X.C30321hr) r0
            X.1i2 r5 = r0.A02()
            long r8 = r4.A01
            int r11 = r4.A00
            r10 = 1
            r6 = 0
            int r1 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x0423
            r0 = 1
        L_0x0423:
            com.google.common.base.Preconditions.checkArgument(r0)
            if (r11 > 0) goto L_0x0429
            r10 = 0
        L_0x0429:
            com.google.common.base.Preconditions.checkArgument(r10)
            r0 = 0
            android.util.Pair r0 = r5.A02(r0, r8, r11)
            com.facebook.messaging.service.model.FetchMoreThreadsResult r6 = new com.facebook.messaging.service.model.FetchMoreThreadsResult
            com.facebook.fbservice.results.DataFetchDisposition r7 = com.facebook.fbservice.results.DataFetchDisposition.A0E
            X.0l8 r8 = X.C10950l8.A05
            java.lang.Object r9 = r0.first
            com.facebook.messaging.model.threads.ThreadsCollection r9 = (com.facebook.messaging.model.threads.ThreadsCollection) r9
            r10 = 0
            java.lang.Object r11 = r0.second
            com.google.common.collect.ImmutableList r11 = (com.google.common.collect.ImmutableList) r11
            int r1 = X.AnonymousClass1Y3.AYB
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.06A r0 = (X.AnonymousClass06A) r0
            long r12 = r0.now()
            r14 = 0
            r6.<init>(r7, r8, r9, r10, r11, r12, r14)
        L_0x0452:
            if (r6 == 0) goto L_0x045b
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r6)
            return r0
        L_0x0459:
            r6 = 0
            goto L_0x0452
        L_0x045b:
            com.facebook.messaging.service.model.FetchMoreThreadsResult r0 = com.facebook.messaging.service.model.FetchMoreThreadsResult.A07
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0R(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (r3.A08 == null) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0S(X.C11060ln r6, X.C27311cz r7) {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0067
            boolean r0 = r5 instanceof X.C11070lo
            if (r0 != 0) goto L_0x0054
            boolean r0 = r5 instanceof X.C12380pF
            if (r0 != 0) goto L_0x001a
            r1 = r5
            X.0lz r1 = (X.C11150lz) r1
            boolean r0 = r1 instanceof X.C12370pE
            if (r0 != 0) goto L_0x0015
            boolean r0 = r1 instanceof X.C11130lx
        L_0x0015:
            com.facebook.fbservice.service.OperationResult r0 = r7.BAz(r6)
            return r0
        L_0x001a:
            r4 = r5
            X.0pF r4 = (X.C12380pF) r4
            android.os.Bundle r1 = r6.A00
            java.lang.String r0 = "fetchMoreThreadsParams"
            android.os.Parcelable r3 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r3 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r3
            if (r3 == 0) goto L_0x002e
            java.lang.String r1 = r3.A08
            r0 = 1
            if (r1 != 0) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            com.google.common.base.Preconditions.checkArgument(r0)
            r2 = 24
            int r1 = X.AnonymousClass1Y3.ARS
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7PV r1 = (X.AnonymousClass7PV) r1
            com.facebook.common.callercontext.CallerContext r0 = r6.A01
            com.facebook.messaging.service.model.FetchMoreThreadsResult r0 = r1.A0F(r3, r0)
            com.facebook.fbservice.service.OperationResult r3 = com.facebook.fbservice.service.OperationResult.A04(r0)
            android.os.Bundle r2 = r3.resultDataBundle
            if (r2 == 0) goto L_0x0053
            java.lang.String r1 = "source"
            java.lang.String r0 = "server"
            r2.putString(r1, r0)
        L_0x0053:
            return r3
        L_0x0054:
            r0 = r5
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r6)
            return r0
        L_0x0067:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0S(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0T(C11060ln r6, C27311cz r7) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r6);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r1 = (C11150lz) this;
                if (!(r1 instanceof C12370pE)) {
                    boolean z = r1 instanceof C11130lx;
                }
                return r7.BAz(r6);
            }
            C12380pF r4 = (C12380pF) this;
            FetchMoreThreadsParams fetchMoreThreadsParams = (FetchMoreThreadsParams) r6.A00.getParcelable("fetchMoreThreadsParams");
            boolean z2 = false;
            if (fetchMoreThreadsParams.A04 == C10950l8.A0E) {
                z2 = true;
            }
            Preconditions.checkArgument(z2);
            OperationResult A04 = OperationResult.A04(((AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, r4.A00)).A0F(fetchMoreThreadsParams, r6.A01));
            Bundle bundle = A04.resultDataBundle;
            if (bundle != null) {
                bundle.putString("source", "server");
            }
            return A04;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01ea, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x022b, code lost:
        if (r5.A0A(X.C42762Bs.A01(r10.dbName)) != false) goto L_0x022d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007a, code lost:
        if (r8.A0h() != false) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0111, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0112, code lost:
        if (r6 != null) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0142, code lost:
        if (r3 != null) goto L_0x0144;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01e4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01e5, code lost:
        if (r3 != null) goto L_0x01e7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0U(X.C11060ln r25, X.C27311cz r26) {
        /*
            r24 = this;
            r2 = r24
            boolean r0 = r2 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0304
            boolean r0 = r2 instanceof X.C11070lo
            r1 = r25
            if (r0 != 0) goto L_0x02f1
            boolean r0 = r2 instanceof X.C12380pF
            if (r0 != 0) goto L_0x02cd
            r0 = r2
            X.0lz r0 = (X.C11150lz) r0
            boolean r2 = r0 instanceof X.C12370pE
            r3 = r26
            if (r2 != 0) goto L_0x01eb
            boolean r2 = r0 instanceof X.C11130lx
            if (r2 != 0) goto L_0x0022
            com.facebook.fbservice.service.OperationResult r7 = r3.BAz(r1)
        L_0x0021:
            return r7
        L_0x0022:
            X.0lx r0 = (X.C11130lx) r0
            android.os.Bundle r4 = r1.A00
            java.lang.String r2 = "fetchMoreThreadsParams"
            android.os.Parcelable r5 = r4.getParcelable(r2)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r5 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r5
            long r6 = r5.A01
            r8 = 0
            int r2 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            r9 = 0
            if (r2 != 0) goto L_0x0038
            r9 = 1
        L_0x0038:
            if (r9 == 0) goto L_0x0090
            X.0m6 r8 = r0.A03
            X.2BV r7 = r5.A06
            X.0mL r2 = r8.A0A
            X.0mM r6 = r2.A00()
            int r2 = r7.ordinal()     // Catch:{ all -> 0x010f }
            r4 = 0
            switch(r2) {
                case 1: goto L_0x0070;
                case 2: goto L_0x0063;
                default: goto L_0x004c;
            }     // Catch:{ all -> 0x010f }
        L_0x004c:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x010f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x010f }
            r1.<init>()     // Catch:{ all -> 0x010f }
            java.lang.String r0 = "Invalid virtual folder specified: "
            r1.append(r0)     // Catch:{ all -> 0x010f }
            r1.append(r7)     // Catch:{ all -> 0x010f }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x010f }
            r2.<init>(r0)     // Catch:{ all -> 0x010f }
            throw r2     // Catch:{ all -> 0x010f }
        L_0x0063:
            boolean r2 = r8.A0e()     // Catch:{ all -> 0x010f }
            if (r2 == 0) goto L_0x007d
            boolean r2 = r8.A0f()     // Catch:{ all -> 0x010f }
            if (r2 == 0) goto L_0x007d
            goto L_0x007c
        L_0x0070:
            boolean r2 = r8.A0g()     // Catch:{ all -> 0x010f }
            if (r2 == 0) goto L_0x007d
            boolean r2 = r8.A0h()     // Catch:{ all -> 0x010f }
            if (r2 == 0) goto L_0x007d
        L_0x007c:
            r4 = 1
        L_0x007d:
            if (r6 == 0) goto L_0x0082
            r6.close()
        L_0x0082:
            if (r4 == 0) goto L_0x0090
            X.0m6 r2 = r0.A03
            X.2BV r4 = r5.A06
            X.0mL r1 = r2.A0A
            X.0mM r3 = r1.A00()
            goto L_0x0119
        L_0x0090:
            com.facebook.fbservice.service.OperationResult r7 = r3.BAz(r1)
            java.lang.Object r1 = r7.A07()
            com.facebook.messaging.service.model.FetchMoreThreadsResult r1 = (com.facebook.messaging.service.model.FetchMoreThreadsResult) r1
            X.0US r0 = r0.A01
            java.lang.Object r0 = r0.get()
            X.0u9 r0 = (X.C14800u9) r0
            X.2BV r4 = r5.A06
            X.0m6 r2 = r0.A01
            com.facebook.messaging.model.threads.ThreadsCollection r1 = r1.A03
            if (r9 == 0) goto L_0x00da
            X.0mL r0 = r2.A0A
            X.0mM r3 = r0.A00()
            int r0 = r4.ordinal()     // Catch:{ all -> 0x01e2 }
            switch(r0) {
                case 1: goto L_0x00d4;
                case 2: goto L_0x00ce;
                default: goto L_0x00b7;
            }     // Catch:{ all -> 0x01e2 }
        L_0x00b7:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x01e2 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01e2 }
            r1.<init>()     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = "Invalid virtual folder specified: "
            r1.append(r0)     // Catch:{ all -> 0x01e2 }
            r1.append(r4)     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x01e2 }
            r2.<init>(r0)     // Catch:{ all -> 0x01e2 }
            goto L_0x0136
        L_0x00ce:
            X.0mP r0 = r2.A04     // Catch:{ all -> 0x01e2 }
            X.AnonymousClass0m6.A0E(r2, r1, r0)     // Catch:{ all -> 0x01e2 }
            goto L_0x0109
        L_0x00d4:
            X.0mP r0 = r2.A05     // Catch:{ all -> 0x01e2 }
            X.AnonymousClass0m6.A0E(r2, r1, r0)     // Catch:{ all -> 0x01e2 }
            goto L_0x0109
        L_0x00da:
            X.0mL r0 = r2.A0A
            X.0mM r3 = r0.A00()
            int r0 = r4.ordinal()     // Catch:{ all -> 0x01e2 }
            switch(r0) {
                case 1: goto L_0x0104;
                case 2: goto L_0x00fe;
                default: goto L_0x00e7;
            }     // Catch:{ all -> 0x01e2 }
        L_0x00e7:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x01e2 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01e2 }
            r1.<init>()     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = "Invalid virtual folder specified: "
            r1.append(r0)     // Catch:{ all -> 0x01e2 }
            r1.append(r4)     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x01e2 }
            r2.<init>(r0)     // Catch:{ all -> 0x01e2 }
            goto L_0x0136
        L_0x00fe:
            X.0mP r0 = r2.A04     // Catch:{ all -> 0x01e2 }
            X.AnonymousClass0m6.A0D(r2, r1, r0)     // Catch:{ all -> 0x01e2 }
            goto L_0x0109
        L_0x0104:
            X.0mP r0 = r2.A05     // Catch:{ all -> 0x01e2 }
            X.AnonymousClass0m6.A0D(r2, r1, r0)     // Catch:{ all -> 0x01e2 }
        L_0x0109:
            if (r3 == 0) goto L_0x0021
            r3.close()
            return r7
        L_0x010f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0111 }
        L_0x0111:
            r0 = move-exception
            if (r6 == 0) goto L_0x01ea
            r6.close()     // Catch:{ all -> 0x01ea }
            goto L_0x01ea
        L_0x0119:
            int r1 = r4.ordinal()     // Catch:{ all -> 0x01e2 }
            switch(r1) {
                case 1: goto L_0x013e;
                case 2: goto L_0x0137;
                default: goto L_0x0120;
            }     // Catch:{ all -> 0x01e2 }
        L_0x0120:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x01e2 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01e2 }
            r1.<init>()     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = "Invalid virtual folder specified: "
            r1.append(r0)     // Catch:{ all -> 0x01e2 }
            r1.append(r4)     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x01e2 }
            r2.<init>(r0)     // Catch:{ all -> 0x01e2 }
        L_0x0136:
            throw r2     // Catch:{ all -> 0x01e2 }
        L_0x0137:
            com.facebook.messaging.model.threads.ThreadsCollection r10 = r2.A0S()     // Catch:{ all -> 0x01e2 }
            if (r3 == 0) goto L_0x0147
            goto L_0x0144
        L_0x013e:
            com.facebook.messaging.model.threads.ThreadsCollection r10 = r2.A0T()     // Catch:{ all -> 0x01e2 }
            if (r3 == 0) goto L_0x0147
        L_0x0144:
            r3.close()
        L_0x0147:
            com.facebook.messaging.service.model.FetchMoreThreadsResult r7 = new com.facebook.messaging.service.model.FetchMoreThreadsResult
            com.facebook.fbservice.results.DataFetchDisposition r8 = com.facebook.fbservice.results.DataFetchDisposition.A0D
            X.0l8 r9 = r5.A04
            java.util.HashSet r4 = new java.util.HashSet
            r4.<init>()
            com.google.common.collect.ImmutableList r1 = r10.A00
            X.1Xv r6 = r1.iterator()
        L_0x0158:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L_0x01a4
            java.lang.Object r5 = r6.next()
            com.facebook.messaging.model.threads.ThreadSummary r5 = (com.facebook.messaging.model.threads.ThreadSummary) r5
            com.google.common.collect.ImmutableList r1 = r5.A0m
            X.1Xv r3 = r1.iterator()
        L_0x016a:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x0184
            java.lang.Object r2 = r3.next()
            com.facebook.messaging.model.threads.ThreadParticipant r2 = (com.facebook.messaging.model.threads.ThreadParticipant) r2
            com.facebook.user.model.UserKey r1 = r2.A00()
            if (r1 == 0) goto L_0x016a
            com.facebook.user.model.UserKey r1 = r2.A00()
            r4.add(r1)
            goto L_0x016a
        L_0x0184:
            com.google.common.collect.ImmutableList r1 = r5.A0k
            X.1Xv r3 = r1.iterator()
        L_0x018a:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x0158
            java.lang.Object r2 = r3.next()
            com.facebook.messaging.model.threads.ThreadParticipant r2 = (com.facebook.messaging.model.threads.ThreadParticipant) r2
            com.facebook.user.model.UserKey r1 = r2.A00()
            if (r1 == 0) goto L_0x018a
            com.facebook.user.model.UserKey r1 = r2.A00()
            r4.add(r1)
            goto L_0x018a
        L_0x01a4:
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            java.util.Iterator r5 = r4.iterator()
        L_0x01ad:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x01ce
            java.lang.Object r4 = r5.next()
            com.facebook.user.model.UserKey r4 = (com.facebook.user.model.UserKey) r4
            r3 = 5
            int r2 = X.AnonymousClass1Y3.AxE
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0t0 r1 = (X.C14300t0) r1
            com.facebook.user.model.User r1 = r1.A03(r4)
            if (r1 == 0) goto L_0x01ad
            r6.add(r1)
            goto L_0x01ad
        L_0x01ce:
            com.google.common.collect.ImmutableList r12 = com.google.common.collect.ImmutableList.copyOf(r6)
            X.06A r0 = X.AnonymousClass06A.A00
            long r13 = r0.now()
            r11 = 0
            r15 = 0
            r7.<init>(r8, r9, r10, r11, r12, r13, r15)
            com.facebook.fbservice.service.OperationResult r7 = com.facebook.fbservice.service.OperationResult.A04(r7)
            return r7
        L_0x01e2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x01e4 }
        L_0x01e4:
            r0 = move-exception
            if (r3 == 0) goto L_0x01ea
            r3.close()     // Catch:{ all -> 0x01ea }
        L_0x01ea:
            throw r0
        L_0x01eb:
            X.0pE r0 = (X.C12370pE) r0
            android.os.Bundle r4 = r1.A00
            java.lang.String r2 = "fetchMoreThreadsParams"
            android.os.Parcelable r8 = r4.getParcelable(r2)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r8 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r8
            X.2BV r14 = r8.A06
            int r5 = X.AnonymousClass1Y3.AV7
            X.0UN r4 = r0.A00
            r2 = 5
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r5, r4)
            X.0ph r5 = (X.C12620ph) r5
            X.0l8 r10 = r8.A04
            long r11 = r8.A01
            int r13 = r8.A00
            r9 = r5
            com.google.common.collect.ImmutableList r9 = X.C12620ph.A01(r9, r10, r11, r13, r14)
            X.0l8 r2 = r8.A04
            java.lang.String r2 = X.C42762Bs.A00(r2, r14)
            java.lang.String r2 = X.C42762Bs.A01(r2)
            boolean r2 = r5.A0A(r2)
            r7 = 0
            if (r2 != 0) goto L_0x022d
            java.lang.String r2 = r10.dbName
            java.lang.String r2 = X.C42762Bs.A01(r2)
            boolean r2 = r5.A0A(r2)
            r6 = 0
            if (r2 == 0) goto L_0x022e
        L_0x022d:
            r6 = 1
        L_0x022e:
            int r4 = r9.size()
            int r2 = r8.A00
            if (r4 >= r2) goto L_0x0239
            if (r6 == 0) goto L_0x0239
            r7 = 1
        L_0x0239:
            com.facebook.messaging.model.threads.ThreadsCollection r4 = new com.facebook.messaging.model.threads.ThreadsCollection
            r4.<init>(r9, r7)
            com.facebook.messaging.service.model.FetchMoreThreadsResult r15 = new com.facebook.messaging.service.model.FetchMoreThreadsResult
            com.facebook.fbservice.results.DataFetchDisposition r16 = com.facebook.fbservice.results.DataFetchDisposition.A0F
            r19 = 0
            com.google.common.collect.ImmutableList r20 = com.google.common.collect.RegularImmutableList.A02
            X.0of r2 = r5.A01
            long r21 = r2.now()
            r23 = 0
            r17 = r10
            r18 = r4
            r15.<init>(r16, r17, r18, r19, r20, r21, r23)
            com.facebook.messaging.model.threads.ThreadsCollection r4 = r15.A03
            boolean r2 = r4.A01
            if (r2 != 0) goto L_0x02c8
            com.google.common.collect.ImmutableList r2 = r4.A00
            boolean r2 = r2.isEmpty()
            if (r2 == 0) goto L_0x02c8
            com.facebook.fbservice.service.OperationResult r7 = r3.BAz(r1)
            java.lang.Object r4 = r7.A08()
            com.facebook.messaging.service.model.FetchMoreThreadsResult r4 = (com.facebook.messaging.service.model.FetchMoreThreadsResult) r4
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BJD
            X.0UN r0 = r0.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2ij r6 = (X.C52232ij) r6
            X.0Tq r0 = r6.A0F
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r5 = r0.A06()
            r0 = 560133761(0x2162f681, float:7.6898024E-19)
            X.C007406x.A01(r5, r0)
            long r2 = r4.A00     // Catch:{ SQLException -> 0x02b5 }
            com.facebook.messaging.model.threads.ThreadsCollection r1 = r4.A03     // Catch:{ SQLException -> 0x02b5 }
            X.0l8 r0 = r4.A02     // Catch:{ SQLException -> 0x02b5 }
            java.lang.String r0 = X.C42762Bs.A00(r0, r14)     // Catch:{ SQLException -> 0x02b5 }
            X.C52232ij.A0E(r6, r0, r1)     // Catch:{ SQLException -> 0x02b5 }
            com.google.common.collect.ImmutableList r0 = r1.A00     // Catch:{ SQLException -> 0x02b5 }
            X.1Xv r4 = r0.iterator()     // Catch:{ SQLException -> 0x02b5 }
        L_0x029d:
            boolean r0 = r4.hasNext()     // Catch:{ SQLException -> 0x02b5 }
            if (r0 == 0) goto L_0x02ae
            java.lang.Object r1 = r4.next()     // Catch:{ SQLException -> 0x02b5 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1     // Catch:{ SQLException -> 0x02b5 }
            r0 = 0
            X.C52232ij.A05(r6, r1, r2, r0)     // Catch:{ SQLException -> 0x02b5 }
            goto L_0x029d
        L_0x02ae:
            r5.setTransactionSuccessful()     // Catch:{ SQLException -> 0x02b5 }
            r0 = 981758988(0x3a84740c, float:0.0010105385)
            goto L_0x02bc
        L_0x02b5:
            r0 = move-exception
            X.C52232ij.A09(r0)     // Catch:{ all -> 0x02c0 }
            r0 = -1052010262(0xffffffffc14b98ea, float:-12.724833)
        L_0x02bc:
            X.C007406x.A02(r5, r0)
            return r7
        L_0x02c0:
            r1 = move-exception
            r0 = 1138585178(0x43dd6e5a, float:442.86212)
            X.C007406x.A02(r5, r0)
            throw r1
        L_0x02c8:
            com.facebook.fbservice.service.OperationResult r7 = com.facebook.fbservice.service.OperationResult.A04(r15)
            return r7
        L_0x02cd:
            r5 = r2
            X.0pF r5 = (X.C12380pF) r5
            android.os.Bundle r2 = r1.A00
            java.lang.String r0 = "fetchMoreThreadsParams"
            android.os.Parcelable r4 = r2.getParcelable(r0)
            com.facebook.messaging.service.model.FetchMoreThreadsParams r4 = (com.facebook.messaging.service.model.FetchMoreThreadsParams) r4
            int r3 = X.AnonymousClass1Y3.ARS
            X.0UN r2 = r5.A00
            r0 = 24
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r3, r2)
            X.7PV r2 = (X.AnonymousClass7PV) r2
            com.facebook.common.callercontext.CallerContext r0 = r1.A01
            com.facebook.messaging.service.model.FetchMoreThreadsResult r0 = r2.A0F(r4, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r0
        L_0x02f1:
            r0 = r2
            X.0lo r0 = (X.C11070lo) r0
            int r3 = X.AnonymousClass1Y3.A49
            X.0UN r2 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r2)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r1)
            return r0
        L_0x0304:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0U(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0V(C11060ln r16, C27311cz r17) {
        C14880uI r0;
        FetchThreadResult A002;
        C11060ln r2 = r16;
        if (this instanceof C10840kw) {
            C10840kw r5 = (C10840kw) this;
            if (r5.A01.get() == null) {
                A002 = FetchThreadResult.A09;
            } else {
                FetchThreadParams fetchThreadParams = (FetchThreadParams) AnonymousClass0ls.A00(r2, "fetchThreadParams");
                C30311hq r6 = (C30311hq) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ah2, r5.A00);
                boolean z = false;
                if (fetchThreadParams.A04.A00.size() == 1) {
                    z = true;
                }
                Preconditions.checkArgument(z);
                FetchThreadResult A012 = ((C12420pJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abq, r6.A00)).A02() ? ((C30321hr) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Any, r6.A00)).A02().A01(fetchThreadParams.A04.A01(), fetchThreadParams.A01) : null;
                if (A012.A02()) {
                    return OperationResult.A04(A012);
                }
                boolean z2 = false;
                boolean z3 = false;
                if (fetchThreadParams.A04.A00.size() == 1) {
                    z3 = true;
                }
                Preconditions.checkArgument(z3);
                ThreadKey A013 = fetchThreadParams.A04.A01();
                Preconditions.checkNotNull(A013);
                UserKey A014 = UserKey.A01(String.valueOf(A013.A01));
                User A03 = ((C14300t0) AnonymousClass1XX.A02(10, AnonymousClass1Y3.AxE, r5.A00)).A03(A014);
                if (A03 == null) {
                    A03 = ((C28011e7) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amz, r5.A00)).A03(A014);
                }
                if (A03 == null) {
                    r0 = C14880uI.A0E;
                } else {
                    User user = (User) r5.A01.get();
                    Preconditions.checkNotNull(user);
                    if (A013.A05 == C28711fF.TINCAN) {
                        if (Long.parseLong(user.A0j) == A013.A04) {
                            z2 = true;
                        }
                        Preconditions.checkState(z2);
                    }
                    ImmutableList of = ImmutableList.of(A03, r5.A01.get());
                    C61222yX A003 = FetchThreadResult.A00();
                    A003.A05 = C52192if.TINCAN;
                    A003.A01 = DataFetchDisposition.A0E;
                    A003.A06 = of;
                    A003.A00 = AnonymousClass06A.A00.now();
                    A002 = A003.A00();
                }
            }
            return OperationResult.A04(A002);
        } else if (!(this instanceof C11070lo)) {
            C12380pF r8 = (C12380pF) this;
            FetchThreadParams fetchThreadParams2 = (FetchThreadParams) r2.A00.getParcelable("fetchThreadParams");
            C08770fv.A04((C08770fv) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B9x, r8.A00), fetchThreadParams2.A04.A01().A0J().hashCode(), "network_thread");
            if (((C25051Yd) AnonymousClass1XX.A02(32, AnonymousClass1Y3.AOJ, r8.A00)).Aem(282269545661712L)) {
                long max = (long) Math.max(3, 3);
                int i = 0;
                while (true) {
                    long j = (long) i;
                    if (j >= max) {
                        C000300h.A00();
                        r0 = C14880uI.A0J;
                        break;
                    }
                    try {
                        FetchThreadResult A0I = ((AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, r8.A00)).A0I(fetchThreadParams2, r2.A01);
                        ((AnonymousClass7ZV) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ALO, r8.A00)).A04(A0I);
                        OperationResult A04 = OperationResult.A04(A0I);
                        C180878aL.A00(A04);
                        return A04;
                    } catch (C37731wA e) {
                        if (!((C09340h3) AnonymousClass1XX.A02(33, AnonymousClass1Y3.AeN, r8.A00)).A0Q() || j == max - 1) {
                            throw e;
                        }
                        i++;
                    }
                }
            } else {
                FetchThreadResult A0I2 = ((AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, r8.A00)).A0I(fetchThreadParams2, r2.A01);
                ((AnonymousClass7ZV) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ALO, r8.A00)).A04(A0I2);
                OperationResult A042 = OperationResult.A04(A0I2);
                C180878aL.A00(A042);
                return A042;
            }
        } else {
            ThreadKey A015 = ((FetchThreadParams) AnonymousClass0ls.A00(r2, "fetchThreadParams")).A04.A01();
            Preconditions.checkNotNull(A015);
            return C11070lo.A00((C11070lo) this, C11070lo.A03(A015)).BAz(r2);
        }
        return OperationResult.A00(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x01b8, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x01b9, code lost:
        if (r2 != null) goto L_0x01bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01be, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0W(X.C11060ln r24, X.C27311cz r25) {
        /*
            r23 = this;
            r1 = r23
            boolean r0 = r1 instanceof X.C10840kw
            if (r0 != 0) goto L_0x02b6
            boolean r0 = r1 instanceof X.C11070lo
            r5 = r24
            if (r0 != 0) goto L_0x0277
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x0271
            r6 = r1
            X.0lz r6 = (X.C11150lz) r6
            boolean r0 = r6 instanceof X.C12440pM
            if (r0 != 0) goto L_0x01df
            boolean r0 = r6 instanceof X.C12370pE
            if (r0 != 0) goto L_0x006f
            boolean r0 = r6 instanceof X.C11130lx
            r3 = r25
            if (r0 == 0) goto L_0x006a
            X.0lx r6 = (X.C11130lx) r6
            X.0mQ r4 = r6.A04
            X.0l8 r2 = X.C10950l8.A05
            X.0hU r1 = X.C09510hU.DO_NOT_CHECK_SERVER
            r0 = 0
            boolean r0 = r4.A05(r2, r1, r0)
            if (r0 == 0) goto L_0x006a
            android.os.Bundle r1 = r5.A00
            java.lang.String r0 = "fetch_thread_with_participants_key"
            android.os.Parcelable r2 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams r2 = (com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams) r2
            X.0mQ r1 = r6.A04
            X.0l8 r0 = X.C10950l8.A05
            com.facebook.messaging.service.model.FetchThreadListResult r0 = r1.A04(r0)
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A06
            com.google.common.collect.ImmutableList r0 = r0.A00
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r0)
            java.util.Comparator r0 = com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams.A05
            java.util.Collections.sort(r1, r0)
            com.google.common.base.Predicate r0 = r2.A02
            java.lang.Iterable r0 = X.AnonymousClass0j4.A01(r1, r0)
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.copyOf(r0)
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x006a
            com.facebook.messaging.service.model.FetchThreadByParticipantsResult r0 = new com.facebook.messaging.service.model.FetchThreadByParticipantsResult
            r0.<init>(r1)
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r1
        L_0x006a:
            com.facebook.fbservice.service.OperationResult r1 = r3.BAz(r5)
            return r1
        L_0x006f:
            X.0pE r6 = (X.C12370pE) r6
            android.os.Bundle r1 = r5.A00
            java.lang.String r0 = "fetch_thread_with_participants_key"
            android.os.Parcelable r1 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams r1 = (com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams) r1
            int r3 = X.AnonymousClass1Y3.AV7
            X.0UN r2 = r6.A00
            r0 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r2)
            X.0ph r0 = (X.C12620ph) r0
            com.google.common.collect.ImmutableSet r5 = r1.A03
            com.google.common.collect.ImmutableList$Builder r4 = com.google.common.collect.ImmutableList.builder()
            boolean r2 = r5.isEmpty()
            if (r2 != 0) goto L_0x01c2
            java.lang.String r3 = "count(*) = "
            int r2 = r5.size()
            java.lang.String r12 = X.AnonymousClass08S.A09(r3, r2)
            java.lang.String r6 = "type = '"
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            java.lang.String r3 = X.C181710m.A01(r2)
            java.lang.String r2 = "' "
            java.lang.String r10 = X.AnonymousClass08S.A0P(r6, r3, r2)
            r7 = 0
            r3 = 0
            java.lang.String r2 = "thread_key"
            java.lang.String[] r9 = new java.lang.String[]{r2}
            r13 = 0
            r14 = 0
            java.lang.String r8 = "thread_participants"
            java.lang.String r11 = "thread_key"
            java.lang.String r6 = android.database.sqlite.SQLiteQueryBuilder.buildQueryString(r7, r8, r9, r10, r11, r12, r13, r14)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r10)
            r8 = 9
            java.lang.String r8 = X.C99084oO.$const$string(r8)
            r7.append(r8)
            java.lang.String r8 = "user_key"
            r7.append(r8)
            java.lang.String r8 = " IN ("
            r7.append(r8)
            X.1Xv r10 = r5.iterator()
        L_0x00db:
            boolean r5 = r10.hasNext()
            if (r5 == 0) goto L_0x00f9
            java.lang.Object r9 = r10.next()
            com.facebook.user.model.UserKey r9 = (com.facebook.user.model.UserKey) r9
            java.lang.String r5 = "'"
            r7.append(r5)
            java.lang.String r5 = r9.A06()
            r7.append(r5)
            java.lang.String r5 = "',"
            r7.append(r5)
            goto L_0x00db
        L_0x00f9:
            java.lang.String r5 = ","
            int r5 = r7.lastIndexOf(r5)
            r7.deleteCharAt(r5)
            java.lang.String r5 = ") AND "
            r7.append(r5)
            r7.append(r2)
            r7.append(r8)
            r7.append(r6)
            java.lang.String r6 = ")"
            r7.append(r6)
            r15 = 0
            java.lang.String[] r17 = new java.lang.String[]{r2}
            java.lang.String r18 = r7.toString()
            java.lang.String r16 = "thread_participants"
            r19 = r2
            r20 = r12
            r21 = r13
            r22 = r13
            java.lang.String r7 = android.database.sqlite.SQLiteQueryBuilder.buildQueryString(r15, r16, r17, r18, r19, r20, r21, r22)
            android.database.sqlite.SQLiteQueryBuilder r14 = new android.database.sqlite.SQLiteQueryBuilder
            r14.<init>()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r5 = 512(0x200, float:7.175E-43)
            r9.<init>(r5)
            java.lang.String r5 = "(("
            r9.append(r5)
            r9.append(r7)
            java.lang.String r5 = ") "
            r9.append(r5)
            java.lang.String r7 = "thread_participants"
            r9.append(r7)
            java.lang.String r5 = " JOIN "
            r9.append(r5)
            java.lang.String r8 = "threads"
            r9.append(r8)
            java.lang.String r5 = " ON "
            r9.append(r5)
            r9.append(r7)
            java.lang.String r7 = "."
            r9.append(r7)
            r9.append(r2)
            java.lang.String r5 = " = "
            r9.append(r5)
            r9.append(r8)
            r9.append(r7)
            r9.append(r2)
            r9.append(r6)
            java.lang.String r2 = r9.toString()
            r14.setTables(r2)
            X.0Tq r2 = r0.A04
            java.lang.Object r2 = r2.get()
            X.1aN r2 = (X.C25771aN) r2
            android.database.sqlite.SQLiteDatabase r15 = r2.A06()
            java.lang.String r6 = "optimistic_group_state"
            java.lang.String r5 = " != "
            X.105 r2 = X.AnonymousClass105.A02
            int r2 = r2.dbValue
            r16 = 0
            java.lang.String r17 = X.AnonymousClass08S.A0L(r6, r5, r2)
            r18 = 0
            r19 = 0
            java.lang.String r21 = "timestamp_ms DESC"
            r20 = r13
            android.database.Cursor r2 = r14.query(r15, r16, r17, r18, r19, r20, r21)
            X.0cX r0 = r0.A02
            X.0og r2 = r0.A00(r2, r3)
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.BLt()     // Catch:{ all -> 0x01b6 }
        L_0x01ac:
            if (r0 == 0) goto L_0x01bf
            r4.add(r0)     // Catch:{ all -> 0x01b6 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.BLt()     // Catch:{ all -> 0x01b6 }
            goto L_0x01ac
        L_0x01b6:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x01b8 }
        L_0x01b8:
            r0 = move-exception
            if (r2 == 0) goto L_0x01be
            r2.close()     // Catch:{ all -> 0x01be }
        L_0x01be:
            throw r0
        L_0x01bf:
            r2.close()
        L_0x01c2:
            com.google.common.collect.ImmutableList r0 = r4.build()
            com.facebook.messaging.service.model.FetchThreadByParticipantsResult r3 = new com.facebook.messaging.service.model.FetchThreadByParticipantsResult
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r0)
            com.google.common.base.Predicate r0 = r1.A01
            java.lang.Iterable r0 = X.AnonymousClass0j4.A01(r2, r0)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
            r3.<init>(r0)
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A04(r3)
            return r1
        L_0x01df:
            X.0pM r6 = (X.C12440pM) r6
            java.lang.String r1 = "SmsServiceHandler.handleFetchThreadKeyByParticipants"
            r0 = -1423373788(0xffffffffab290a24, float:-6.0054934E-13)
            X.C005505z.A03(r1, r0)
            android.os.Bundle r1 = r5.A00     // Catch:{ all -> 0x0269 }
            java.lang.String r0 = "fetch_thread_with_participants_key"
            android.os.Parcelable r1 = r1.getParcelable(r0)     // Catch:{ all -> 0x0269 }
            com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams r1 = (com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams) r1     // Catch:{ all -> 0x0269 }
            r5 = 1
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ all -> 0x0269 }
            com.google.common.collect.ImmutableSet r0 = r1.A03     // Catch:{ all -> 0x0269 }
            int r0 = r0.size()     // Catch:{ all -> 0x0269 }
            r4.<init>(r0)     // Catch:{ all -> 0x0269 }
            com.facebook.user.model.UserKey r3 = r1.A00     // Catch:{ all -> 0x0269 }
            com.google.common.collect.ImmutableSet r0 = r1.A03     // Catch:{ all -> 0x0269 }
            X.1Xv r2 = r0.iterator()     // Catch:{ all -> 0x0269 }
        L_0x0207:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0269 }
            if (r0 == 0) goto L_0x0229
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0269 }
            com.facebook.user.model.UserKey r1 = (com.facebook.user.model.UserKey) r1     // Catch:{ all -> 0x0269 }
            boolean r0 = r1.equals(r3)     // Catch:{ all -> 0x0269 }
            if (r0 != 0) goto L_0x0207
            boolean r0 = r1.A08()     // Catch:{ all -> 0x0269 }
            if (r0 != 0) goto L_0x0220
            goto L_0x0228
        L_0x0220:
            java.lang.String r0 = r1.A03()     // Catch:{ all -> 0x0269 }
            r4.add(r0)     // Catch:{ all -> 0x0269 }
            goto L_0x0207
        L_0x0228:
            r5 = 0
        L_0x0229:
            if (r5 == 0) goto L_0x025a
            boolean r0 = r4.isEmpty()     // Catch:{ all -> 0x0269 }
            if (r0 != 0) goto L_0x025a
            android.content.Context r0 = r6.A00     // Catch:{ all -> 0x0269 }
            long r2 = X.AnonymousClass7I7.A00(r0, r4)     // Catch:{ all -> 0x0269 }
            X.0zh r1 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x0269 }
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ all -> 0x0269 }
            r1.A0N = r0     // Catch:{ all -> 0x0269 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A03(r2)     // Catch:{ all -> 0x0269 }
            r1.A0R = r0     // Catch:{ all -> 0x0269 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A00()     // Catch:{ all -> 0x0269 }
            com.facebook.messaging.service.model.FetchThreadByParticipantsResult r1 = new com.facebook.messaging.service.model.FetchThreadByParticipantsResult     // Catch:{ all -> 0x0269 }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r0)     // Catch:{ all -> 0x0269 }
            r1.<init>(r0)     // Catch:{ all -> 0x0269 }
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A04(r1)     // Catch:{ all -> 0x0269 }
            r0 = 545022903(0x207c63b7, float:2.1378223E-19)
            goto L_0x0265
        L_0x025a:
            X.0uI r1 = X.C14880uI.A0E     // Catch:{ all -> 0x0269 }
            java.lang.String r0 = "Unable to get or create thread key"
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A02(r1, r0)     // Catch:{ all -> 0x0269 }
            r0 = -321964940(0xffffffffeccf3474, float:-2.0039628E27)
        L_0x0265:
            X.C005505z.A00(r0)
            return r1
        L_0x0269:
            r1 = move-exception
            r0 = -2074829807(0xffffffff84549c11, float:-2.4992142E-36)
            X.C005505z.A00(r0)
            throw r1
        L_0x0271:
            X.7JR r0 = new X.7JR
            r0.<init>(r5)
            throw r0
        L_0x0277:
            r4 = r1
            X.0lo r4 = (X.C11070lo) r4
            java.lang.String r0 = "fetch_thread_with_participants_key"
            android.os.Parcelable r0 = X.AnonymousClass0ls.A00(r5, r0)
            com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams r0 = (com.facebook.messaging.service.model.FetchThreadKeyByParticipantsParams) r0
            com.facebook.user.model.UserKey r3 = r0.A00
            com.google.common.collect.ImmutableSet r0 = r0.A03
            X.1Xv r2 = r0.iterator()
        L_0x028a:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x02b2
            java.lang.Object r1 = r2.next()
            com.facebook.user.model.UserKey r1 = (com.facebook.user.model.UserKey) r1
            boolean r0 = r1.equals(r3)
            if (r0 != 0) goto L_0x028a
            boolean r0 = r1.A08()
            if (r0 != 0) goto L_0x028a
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A49
        L_0x02a5:
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r5)
            return r0
        L_0x02b2:
            r2 = 1
            int r1 = X.AnonymousClass1Y3.BQB
            goto L_0x02a5
        L_0x02b6:
            java.lang.Class r1 = X.C10840kw.A03
            java.lang.String r0 = "handleFetchThreadKeyByParticipants not implemented yet, succeeding without effect"
            X.C010708t.A06(r1, r0)
            com.facebook.messaging.service.model.FetchThreadByParticipantsResult r1 = new com.facebook.messaging.service.model.FetchThreadByParticipantsResult
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            r1.<init>(r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0W(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0X(C11060ln r7, C27311cz r8) {
        if (!(this instanceof C10840kw)) {
            if (this instanceof C11070lo) {
                return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
            }
            if (!(this instanceof C12380pF)) {
                C11150lz r5 = (C11150lz) this;
                if (!(r5 instanceof C12440pM)) {
                    if (!(r5 instanceof C12370pE)) {
                        boolean z = r5 instanceof C11130lx;
                        return r8.BAz(r7);
                    }
                    C12370pE r52 = (C12370pE) r5;
                    FetchThreadMetadataParams fetchThreadMetadataParams = (FetchThreadMetadataParams) AnonymousClass0ls.A00(r7, "fetchThreadMetadataParams");
                    ((C12450pN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AwM, r52.A00)).A04(C12500pT.A05, "fetchThreadMetadata (DSH). %s", fetchThreadMetadataParams.A00);
                    return OperationResult.A04(new FetchThreadMetadataResult(((AnonymousClass9PD) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AkC, r52.A00)).A02(fetchThreadMetadataParams.A00, fetchThreadMetadataParams.A01)));
                }
            }
        }
        throw new UnsupportedOperationException("handleFetchThreadMetadata not implemented yet");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (r3.A07 == null) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0Y(X.C11060ln r6, X.C27311cz r7) {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0067
            boolean r0 = r5 instanceof X.C11070lo
            if (r0 != 0) goto L_0x0054
            boolean r0 = r5 instanceof X.C12380pF
            if (r0 != 0) goto L_0x001a
            r1 = r5
            X.0lz r1 = (X.C11150lz) r1
            boolean r0 = r1 instanceof X.C12370pE
            if (r0 != 0) goto L_0x0015
            boolean r0 = r1 instanceof X.C11130lx
        L_0x0015:
            com.facebook.fbservice.service.OperationResult r0 = r7.BAz(r6)
            return r0
        L_0x001a:
            r4 = r5
            X.0pF r4 = (X.C12380pF) r4
            android.os.Bundle r1 = r6.A00
            java.lang.String r0 = "fetchThreadListParams"
            android.os.Parcelable r3 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchThreadListParams r3 = (com.facebook.messaging.service.model.FetchThreadListParams) r3
            if (r3 == 0) goto L_0x002e
            java.lang.String r1 = r3.A07
            r0 = 1
            if (r1 != 0) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            com.google.common.base.Preconditions.checkArgument(r0)
            r2 = 24
            int r1 = X.AnonymousClass1Y3.ARS
            X.0UN r0 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7PV r1 = (X.AnonymousClass7PV) r1
            com.facebook.common.callercontext.CallerContext r0 = r6.A01
            com.facebook.messaging.service.model.FetchThreadListResult r0 = r1.A0G(r3, r0)
            com.facebook.fbservice.service.OperationResult r3 = com.facebook.fbservice.service.OperationResult.A04(r0)
            android.os.Bundle r2 = r3.resultDataBundle
            if (r2 == 0) goto L_0x0053
            java.lang.String r1 = "source"
            java.lang.String r0 = "server"
            r2.putString(r1, r0)
        L_0x0053:
            return r3
        L_0x0054:
            r0 = r5
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r6)
            return r0
        L_0x0067:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0Y(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0Z(C11060ln r9, C27311cz r10) {
        C14880uI r3;
        String A0J;
        if (this instanceof C10840kw) {
            C10840kw r7 = (C10840kw) this;
            ThreadKey threadKey = ((FetchIdentityKeysParams) AnonymousClass0ls.A00(r9, "fetchIdentityKeysParams")).A00;
            Preconditions.checkArgument(ThreadKey.A0F(threadKey));
            long j = threadKey.A01;
            String A08 = ((C193917y) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AI2, r7.A00)).A08(j);
            if (A08 == null) {
                r3 = C14880uI.A0E;
                A0J = AnonymousClass08S.A0G("Couldn't find device id for user ", j);
            } else {
                String str = j + "_" + A08;
                int i = AnonymousClass1Y3.BJ7;
                byte[] A012 = ((C48932bN) AnonymousClass1XX.A02(6, i, r7.A00)).A00.A01(str);
                if (A012 == null) {
                    r3 = C14880uI.A0E;
                    A0J = AnonymousClass08S.A0J("Couldn't find identity key for ", str);
                } else {
                    return OperationResult.A04(new FetchIdentityKeysResult(ImmutableMap.of(String.valueOf(j), C200359br.A00(A012), String.valueOf(threadKey.A04), C200359br.A00(((C48932bN) AnonymousClass1XX.A02(6, i, r7.A00)).ApI().A00.A00.A00()))));
                }
            }
            return OperationResult.A02(r3, A0J);
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B9l, ((C11070lo) this).A00)).BAz(r9);
        } else {
            if (this instanceof C11150lz) {
                return r10.BAz(r9);
            }
            throw new AnonymousClass7JR(r9);
        }
    }

    public OperationResult A0a(C11060ln r6, C27311cz r7) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r6);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r1 = (C11150lz) this;
                if (!(r1 instanceof C12370pE)) {
                    boolean z = r1 instanceof C11130lx;
                }
                return r7.BAz(r6);
            }
            C12380pF r4 = (C12380pF) this;
            FetchThreadListParams fetchThreadListParams = (FetchThreadListParams) r6.A00.getParcelable("fetchThreadListParams");
            boolean z2 = false;
            if (fetchThreadListParams.A03 == C10950l8.A0E) {
                z2 = true;
            }
            Preconditions.checkArgument(z2);
            OperationResult A04 = OperationResult.A04(((AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, r4.A00)).A0G(fetchThreadListParams, r6.A01));
            Bundle bundle = A04.resultDataBundle;
            if (bundle != null) {
                bundle.putString("source", "server");
            }
            return A04;
        }
    }

    /* JADX INFO: finally extract failed */
    public OperationResult A0b(C11060ln r11, C27311cz r12) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r11);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r4 = (C11150lz) this;
                if (r4 instanceof C12370pE) {
                    FetchThreadListParams fetchThreadListParams = (FetchThreadListParams) r11.A00.getParcelable("fetchThreadListParams");
                    C12620ph r42 = (C12620ph) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AV7, ((C12370pE) r4).A00);
                    AnonymousClass2BV r9 = AnonymousClass2BV.VIDEO_ROOM;
                    C005505z.A05("DbFetchThreadsHandler.fetchVirtualFolderThreadsList: ", r9, -382833757);
                    try {
                        C10950l8 r5 = fetchThreadListParams.A03;
                        ThreadsCollection threadsCollection = new ThreadsCollection(C12620ph.A01(r42, r5, -1, fetchThreadListParams.A01(), r9), r42.A0A(C42762Bs.A01(C42762Bs.A00(r5, r9))));
                        AnonymousClass161 r2 = new AnonymousClass161();
                        r2.A02 = DataFetchDisposition.A0F;
                        r2.A04 = r5;
                        r2.A06 = threadsCollection;
                        r2.A00 = r42.A01.now();
                        FetchThreadListResult fetchThreadListResult = new FetchThreadListResult(r2);
                        C005505z.A00(1352051646);
                        return OperationResult.A04(fetchThreadListResult);
                    } catch (Throwable th) {
                        C005505z.A00(1238864028);
                        throw th;
                    }
                } else if (!(r4 instanceof C11130lx)) {
                    return r12.BAz(r11);
                } else {
                    C11130lx r43 = (C11130lx) r4;
                    FetchThreadListParams fetchThreadListParams2 = (FetchThreadListParams) r11.A00.getParcelable("fetchThreadListParams");
                    C11230mQ r6 = r43.A04;
                    boolean z = true;
                    boolean z2 = false;
                    if (fetchThreadListParams2.A01 == C09510hU.STALE_DATA_OKAY) {
                        z2 = true;
                    }
                    if (!r6.A01.A0g() || (!r6.A01.A0h() && !z2)) {
                        z = false;
                    }
                    if (z) {
                        ThreadsCollection A0T = r43.A03.A0T();
                        DataFetchDisposition dataFetchDisposition = r43.A03.A0h() ? DataFetchDisposition.A0D : DataFetchDisposition.A0C;
                        AnonymousClass161 r1 = new AnonymousClass161();
                        r1.A02 = dataFetchDisposition;
                        r1.A04 = fetchThreadListParams2.A03;
                        r1.A06 = A0T;
                        return OperationResult.A04(new FetchThreadListResult(r1));
                    }
                    FetchThreadListResult fetchThreadListResult2 = (FetchThreadListResult) r12.BAz(r11).A07();
                    AnonymousClass0m6 r22 = ((C14800u9) r43.A01.get()).A01;
                    AnonymousClass0m6.A0E(r22, fetchThreadListResult2.A06, r22.A05);
                    return OperationResult.A04(fetchThreadListResult2);
                }
            } else {
                throw new AnonymousClass7JR(r11);
            }
        }
    }

    public OperationResult A0c(C11060ln r9, C27311cz r10) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r9);
        } else {
            if (this instanceof C11150lz) {
                return r10.BAz(r9);
            }
            Bundle bundle = r9.A00;
            ThreadKey threadKey = (ThreadKey) bundle.getParcelable("threadKey");
            OtherAttachmentData otherAttachmentData = (OtherAttachmentData) bundle.getParcelable("attachment");
            Preconditions.checkNotNull(threadKey);
            Preconditions.checkNotNull(otherAttachmentData);
            AnonymousClass7PV r5 = (AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, ((C12380pF) this).A00);
            String str = otherAttachmentData.A04;
            String str2 = otherAttachmentData.A03;
            GQSQStringShape3S0000000_I3 gQSQStringShape3S0000000_I3 = new GQSQStringShape3S0000000_I3(AnonymousClass1Y3.A0l);
            GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = new GQLCallInputCInputShape0S0000000(69);
            gQLCallInputCInputShape0S0000000.A09("thread_id", Long.toString(threadKey.A0G()));
            gQLCallInputCInputShape0S0000000.A09("message_id", str);
            gQSQStringShape3S0000000_I3.A04("thread_msg_id", gQLCallInputCInputShape0S0000000);
            C26931cN A002 = C26931cN.A00(gQSQStringShape3S0000000_I3);
            A002.A0A(((C05920aY) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BJQ, r5.A00)).Awn());
            ((AnonymousClass799) AnonymousClass1XX.A02(16, AnonymousClass1Y3.BQ4, r5.A00)).A02(A002);
            String str3 = (String) AnonymousClass169.A01(((C11170mD) AnonymousClass1XX.A02(8, AnonymousClass1Y3.ASp, r5.A00)).A04(A002), new AnonymousClass4AR(str2), C25141Ym.INSTANCE).get();
            return str3 == null ? OperationResult.A02(C14880uI.A01, "Received a null URI for attachment.") : OperationResult.A04(Uri.parse(str3));
        }
    }

    public OperationResult A0d(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (this instanceof C11150lz) {
                return r8.BAz(r7);
            }
            C12380pF r5 = (C12380pF) this;
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r3 = r5.A00;
            ((C64393Bj) AnonymousClass1XX.A02(2, i, r3)).A03((C183588fQ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.A5c, r3), (IgnoreMessageRequestsParams) r7.A00.getParcelable(C99084oO.$const$string(AnonymousClass1Y3.A4K)), C12380pF.A00(r5));
            return OperationResult.A00;
        }
    }

    public OperationResult A0e(C11060ln r14, C27311cz r15) {
        OperationResult operationResult;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r14);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r5 = (C11150lz) this;
                if (r5 instanceof C12370pE) {
                    C12370pE r52 = (C12370pE) r5;
                    C10950l8 A002 = C10950l8.A00(r14.A00.getString("folderName"));
                    OperationResult BAz = r15.BAz(r14);
                    if (BAz == null || !BAz.success) {
                        return BAz;
                    }
                    long now = r52.A01.now();
                    C52232ij r53 = (C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r52.A00);
                    FolderCounts A05 = r53.A04.A05(A002);
                    if (A05 != null) {
                        C52232ij.A0A(r53, A002, new FolderCounts(A05.A00, 0, now));
                    }
                    return OperationResult.A04(new MarkFolderSeenResult(A002, now));
                } else if (!(r5 instanceof C11130lx)) {
                    return r15.BAz(r14);
                } else {
                    C11130lx r54 = (C11130lx) r5;
                    boolean A012 = C11130lx.A01(r54);
                    try {
                        OperationResult BAz2 = r15.BAz(r14);
                        if (BAz2 != null && BAz2.success) {
                            MarkFolderSeenResult markFolderSeenResult = (MarkFolderSeenResult) BAz2.A08();
                            if (markFolderSeenResult == null) {
                            }
                            C14800u9 r9 = (C14800u9) r54.A01.get();
                            C10950l8 r8 = markFolderSeenResult.A01;
                            long j = markFolderSeenResult.A00;
                            FolderCounts A0N = r9.A01.A0N(r8);
                            if (A0N != null) {
                                r9.A05(r8, new FolderCounts(A0N.A00, 0, j));
                            }
                            if (!A012) {
                                return BAz2;
                            }
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r54.A00)).A03();
                            return BAz2;
                        } else if (!A012) {
                            return BAz2;
                        } else {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r54.A00)).A03();
                            return BAz2;
                        }
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r54.A00)).A03();
                        }
                    }
                }
            } else {
                C12380pF r4 = (C12380pF) this;
                C10950l8 A003 = C10950l8.A00(r14.A00.getString("folderName"));
                try {
                    C36991uR BvK = ((AnonymousClass58f) AnonymousClass1XX.A02(17, AnonymousClass1Y3.B5u, r4.A00)).A00.BvK();
                    try {
                        if (!C36991uR.A00(BvK).isConnected()) {
                            operationResult = OperationResult.A00(C14880uI.A04);
                        } else {
                            byte[] A004 = new C36281so(new C36291sp()).A00(new C22387Ax6(A003.dbName, true));
                            int length = A004.length;
                            byte[] bArr = new byte[(length + 1)];
                            System.arraycopy(A004, 0, bArr, 1, length);
                            operationResult = BvK.A08("/t_mf_as", bArr, 30000, 0) ? OperationResult.A00 : OperationResult.A00(C14880uI.A0A);
                        }
                    } catch (Exception e) {
                        operationResult = OperationResult.A03(C14880uI.A0A, e);
                    } catch (Throwable th) {
                        BvK.A06();
                        throw th;
                    }
                    BvK.A06();
                    if (operationResult.success) {
                        ((AnonymousClass20O) r4.A01.get()).A01(new C44642Iz(null, AnonymousClass07B.A00, null, true));
                        return operationResult;
                    }
                    ((AnonymousClass20O) r4.A01.get()).A01(new C44642Iz(null, AnonymousClass07B.A00, operationResult.errorDescription, true));
                    return operationResult;
                } catch (Exception e2) {
                    ((AnonymousClass20O) r4.A01.get()).A01(new C44642Iz(e2, AnonymousClass07B.A00, null, false));
                    throw e2;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v18, resolved type: X.1wB} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v19, resolved type: X.1wB} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v21, resolved type: com.facebook.tigon.TigonErrorException} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v62, resolved type: X.1wB} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v64, resolved type: X.1wB} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v65, resolved type: X.1wB} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v66, resolved type: X.1wB} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0f(X.C11060ln r48, X.C27311cz r49) {
        /*
            r47 = this;
            r2 = r47
            boolean r0 = r2 instanceof X.C10840kw
            r1 = r48
            if (r0 != 0) goto L_0x02dc
            boolean r0 = r2 instanceof X.C11070lo
            if (r0 != 0) goto L_0x0256
            r4 = r2
            X.0pF r4 = (X.C12380pF) r4
            android.os.Bundle r1 = r1.A00
            java.lang.String r0 = "markThreadsParams"
            android.os.Parcelable r2 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.MarkThreadsParams r2 = (com.facebook.messaging.service.model.MarkThreadsParams) r2
            int r3 = X.AnonymousClass1Y3.APw
            X.0UN r1 = r4.A00
            r0 = 28
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.7Yr r1 = (X.C159107Yr) r1
            boolean r0 = r2.A03
            if (r0 == 0) goto L_0x03e0
            com.google.common.collect.ImmutableList r0 = r2.A00
            int r4 = r0.size()
            r3 = 0
            r0 = 1
            if (r4 == r0) goto L_0x0034
            r0 = 0
        L_0x0034:
            if (r0 == 0) goto L_0x01f6
            long r17 = X.C50402dv.A00()
            com.google.common.collect.ImmutableList r0 = r2.A00
            java.lang.Object r0 = r0.get(r3)
            com.facebook.messaging.service.model.MarkThreadFields r0 = (com.facebook.messaging.service.model.MarkThreadFields) r0
            r3 = r17
            r0.A00 = r3
            java.lang.Integer r3 = r2.A02
            if (r3 == 0) goto L_0x00b2
            java.lang.String r3 = X.AnonymousClass79B.A01(r3)
            java.lang.String r25 = r3.toLowerCase()
        L_0x0052:
            int r4 = X.AnonymousClass1Y3.AZc
            X.0UN r3 = r1.A00
            r15 = 10
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r15, r4, r3)
            X.7Yq r8 = (X.C159097Yq) r8
            com.facebook.messaging.model.threadkey.ThreadKey r9 = r0.A06
            long r3 = r0.A02
            X.1tX r7 = r8.A00
            java.lang.String r5 = r9.A0J()
            java.lang.String r6 = X.C159097Yq.A01(r5, r3)
            X.1tb r5 = new X.1tb
            r5.<init>()
            r7.A06(r6, r5)
            X.0nb r7 = new X.0nb
            java.lang.String r5 = "message_read_initiate"
            r7.<init>(r5)
            long r5 = r9.A0G()
            java.lang.String r6 = java.lang.Long.toString(r5)
            java.lang.String r5 = "thread_key"
            r7.A0D(r5, r6)
            X.1fF r5 = r9.A05
            java.lang.String r6 = r5.toString()
            java.util.Locale r5 = java.util.Locale.US
            java.lang.String r6 = r6.toLowerCase(r5)
            java.lang.String r5 = "thread_type"
            r7.A0D(r5, r6)
            java.lang.String r4 = java.lang.Long.toString(r3)
            java.lang.String r3 = "sync_seq"
            r7.A0D(r3, r4)
            java.lang.String r3 = "mark"
            r6 = r25
            r7.A0D(r3, r6)
            X.1tX r4 = r8.A00
            r3 = 0
            r4.A05(r3, r7)
            r24 = 0
            goto L_0x00b5
        L_0x00b2:
            java.lang.String r25 = ""
            goto L_0x0052
        L_0x00b5:
            java.lang.Integer r5 = r2.A02     // Catch:{ Exception -> 0x03a8 }
            int r6 = X.AnonymousClass1Y3.AZp     // Catch:{ Exception -> 0x03a8 }
            X.0UN r4 = r1.A00     // Catch:{ Exception -> 0x03a8 }
            r3 = 7
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r3, r6, r4)     // Catch:{ Exception -> 0x03a8 }
            X.2rR r4 = (X.C57112rR) r4     // Catch:{ Exception -> 0x03a8 }
            java.lang.Integer r3 = X.AnonymousClass07B.A00     // Catch:{ Exception -> 0x03a8 }
            r4.A0D(r5, r0, r3)     // Catch:{ Exception -> 0x03a8 }
            int r6 = X.AnonymousClass1Y3.ADO     // Catch:{ Exception -> 0x03a8 }
            X.0UN r4 = r1.A00     // Catch:{ Exception -> 0x03a8 }
            r3 = 8
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r6, r4)     // Catch:{ Exception -> 0x03a8 }
            X.58e r3 = (X.AnonymousClass58e) r3     // Catch:{ Exception -> 0x03a8 }
            X.1gA r3 = r3.A00     // Catch:{ Exception -> 0x03a8 }
            X.1uR r40 = r3.BvK()     // Catch:{ Exception -> 0x03a8 }
            X.1so r7 = new X.1so     // Catch:{ all -> 0x01f1 }
            X.1sp r3 = new X.1sp     // Catch:{ all -> 0x01f1 }
            r3.<init>()     // Catch:{ all -> 0x01f1 }
            r7.<init>(r3)     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r23 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r22 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.facebook.messaging.model.threadkey.ThreadKey r6 = r0.A06     // Catch:{ all -> 0x01f1 }
            X.1fF r4 = r6.A05     // Catch:{ all -> 0x01f1 }
            X.1fF r3 = X.C28711fF.ONE_TO_ONE     // Catch:{ all -> 0x01f1 }
            if (r4 != r3) goto L_0x01ca
            long r3 = r6.A01     // Catch:{ all -> 0x01f1 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r22 = com.google.common.base.Optional.of(r3)     // Catch:{ all -> 0x01f1 }
        L_0x00fd:
            java.lang.String r27 = X.AnonymousClass79B.A00(r5)     // Catch:{ all -> 0x01f1 }
            boolean r3 = r0.A07     // Catch:{ all -> 0x01f1 }
            java.lang.Boolean r28 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r21 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r12 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r11 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r10 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r6 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r20 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r19 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r16 = com.google.common.base.Optional.absent()     // Catch:{ all -> 0x01f1 }
            long r3 = r0.A01     // Catch:{ all -> 0x01f1 }
            r8 = -1
            int r5 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r5 == 0) goto L_0x0137
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r6 = com.google.common.base.Optional.of(r3)     // Catch:{ all -> 0x01f1 }
        L_0x0137:
            long r3 = r0.A04     // Catch:{ all -> 0x01f1 }
            int r5 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r5 == 0) goto L_0x0145
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r21 = com.google.common.base.Optional.of(r3)     // Catch:{ all -> 0x01f1 }
        L_0x0145:
            X.Ay7 r14 = new X.Ay7     // Catch:{ all -> 0x01f1 }
            java.lang.Object r13 = r10.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x01f1 }
            java.lang.Object r12 = r12.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.Long r12 = (java.lang.Long) r12     // Catch:{ all -> 0x01f1 }
            java.lang.Object r11 = r11.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.Long r11 = (java.lang.Long) r11     // Catch:{ all -> 0x01f1 }
            java.lang.Object r10 = r23.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.Long r10 = (java.lang.Long) r10     // Catch:{ all -> 0x01f1 }
            java.lang.Object r9 = r22.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.Long r9 = (java.lang.Long) r9     // Catch:{ all -> 0x01f1 }
            java.lang.Object r8 = r6.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.Long r8 = (java.lang.Long) r8     // Catch:{ all -> 0x01f1 }
            java.lang.Object r6 = r21.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.Long r6 = (java.lang.Long) r6     // Catch:{ all -> 0x01f1 }
            java.lang.Object r5 = r20.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x01f1 }
            java.lang.Object r4 = r19.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ all -> 0x01f1 }
            java.lang.Object r3 = r16.orNull()     // Catch:{ all -> 0x01f1 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x01f1 }
            r19 = r17
            java.lang.Long r39 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x01f1 }
            r26 = r14
            r29 = r13
            r30 = r12
            r31 = r11
            r32 = r10
            r33 = r9
            r34 = r8
            r35 = r6
            r36 = r5
            r37 = r4
            r38 = r3
            r26.<init>(r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39)     // Catch:{ all -> 0x01f1 }
            X.1uS r4 = new X.1uS     // Catch:{ all -> 0x01f1 }
            r3 = 0
            r4.<init>(r3)     // Catch:{ all -> 0x01f1 }
            byte[] r4 = r7.A00(r4)     // Catch:{ all -> 0x01f1 }
            byte[] r7 = r7.A00(r14)     // Catch:{ all -> 0x01f1 }
            int r6 = r4.length     // Catch:{ all -> 0x01f1 }
            int r5 = r7.length     // Catch:{ all -> 0x01f1 }
            int r3 = r6 + r5
            byte[] r4 = java.util.Arrays.copyOf(r4, r3)     // Catch:{ all -> 0x01f1 }
            r3 = 0
            java.lang.System.arraycopy(r7, r3, r4, r6, r5)     // Catch:{ all -> 0x01f1 }
            r43 = 60000(0xea60, double:2.9644E-319)
            java.lang.String r41 = "/t_mt_req"
            r45 = 0
            r42 = r4
            boolean r3 = r40.A08(r41, r42, r43, r45)     // Catch:{ all -> 0x01f1 }
            goto L_0x01d6
        L_0x01ca:
            long r3 = r6.A03     // Catch:{ all -> 0x01f1 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x01f1 }
            com.google.common.base.Optional r23 = com.google.common.base.Optional.of(r3)     // Catch:{ all -> 0x01f1 }
            goto L_0x00fd
        L_0x01d6:
            r40.A06()     // Catch:{ Exception -> 0x03a8 }
            r24 = r3
            if (r3 != 0) goto L_0x03c3
            java.lang.Integer r4 = X.AnonymousClass07B.A00     // Catch:{ Exception -> 0x03a8 }
            java.lang.String r6 = ""
            java.lang.String r7 = ""
            r8 = 1
            r12 = -1
            r3 = r1
            r5 = r0
            r10 = r17
            r13 = r25
            X.C159107Yr.A00(r3, r4, r5, r6, r7, r8, r10, r12, r13)     // Catch:{ Exception -> 0x03a8 }
            goto L_0x03c3
        L_0x01f1:
            r3 = move-exception
            r40.A06()     // Catch:{ Exception -> 0x03a8 }
            throw r3     // Catch:{ Exception -> 0x03a8 }
        L_0x01f6:
            java.lang.Integer r8 = r2.A02
            int r3 = X.AnonymousClass1Y3.BAS
            X.0UN r0 = r1.A00
            r7 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r3, r0)
            X.3Bj r0 = (X.C64393Bj) r0
            X.3fs r6 = r0.A01()
        L_0x0207:
            com.google.common.collect.ImmutableList r0 = r2.A00
            int r0 = r0.size()
            if (r7 >= r0) goto L_0x0247
            com.google.common.collect.ImmutableList r0 = r2.A00
            java.lang.Object r5 = r0.get(r7)
            com.facebook.messaging.service.model.MarkThreadFields r5 = (com.facebook.messaging.service.model.MarkThreadFields) r5
            r4 = 7
            int r3 = X.AnonymousClass1Y3.AZp
            X.0UN r0 = r1.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r0)
            X.2rR r3 = (X.C57112rR) r3
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r3.A0D(r8, r5, r0)
            X.7Z0 r0 = X.C159107Yr.A01(r1, r8)
            X.3fu r5 = X.C73323ft.A00(r0, r5)
            java.lang.String r4 = "mark-thread-"
            java.lang.String r3 = "-"
            java.lang.String r0 = X.AnonymousClass79B.A00(r8)
            java.lang.String r0 = X.AnonymousClass08S.A0F(r4, r7, r3, r0)
            r5.A02 = r0
            X.3ft r0 = r5.A00()
            r6.A00(r0)
            int r7 = r7 + 1
            goto L_0x0207
        L_0x0247:
            java.lang.Class r0 = r1.getClass()
            com.facebook.common.callercontext.CallerContext r1 = com.facebook.common.callercontext.CallerContext.A04(r0)
            java.lang.String r0 = "markMultipleThreads"
            r6.A01(r0, r1)
            goto L_0x03e0
        L_0x0256:
            r8 = r2
            X.0lo r8 = (X.C11070lo) r8
            java.lang.String r7 = "markThreadsParams"
            android.os.Parcelable r6 = X.AnonymousClass0ls.A00(r1, r7)
            com.facebook.messaging.service.model.MarkThreadsParams r6 = (com.facebook.messaging.service.model.MarkThreadsParams) r6
            com.google.common.collect.ImmutableList r0 = r6.A01
            java.util.HashMap r0 = X.C11070lo.A05(r0)
            X.0lu r5 = new X.0lu
            r5.<init>()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r11 = r0.iterator()
        L_0x0274:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x02d5
            java.lang.Object r10 = r11.next()
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10
            X.3YF r9 = new X.3YF
            r9.<init>()
            java.lang.Integer r0 = r6.A02
            r9.A00 = r0
            boolean r0 = r6.A03
            r9.A01 = r0
            com.google.common.collect.ImmutableList r0 = r6.A00
            X.1Xv r3 = r0.iterator()
        L_0x0293:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x02b3
            java.lang.Object r2 = r3.next()
            com.facebook.messaging.service.model.MarkThreadFields r2 = (com.facebook.messaging.service.model.MarkThreadFields) r2
            java.lang.Object r1 = r10.getValue()
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r2.A06
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0293
            com.google.common.collect.ImmutableList$Builder r0 = r9.A02
            r0.add(r2)
            goto L_0x0293
        L_0x02b3:
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>()
            com.facebook.messaging.service.model.MarkThreadsParams r0 = new com.facebook.messaging.service.model.MarkThreadsParams
            r0.<init>(r9)
            r4.putParcelable(r7, r0)
            java.lang.Object r3 = r10.getKey()
            X.0lv r3 = (X.C11120lv) r3
            X.1cz r2 = X.C11070lo.A00(r8, r3)
            X.0ln r1 = new X.0ln
            java.lang.String r0 = "mark_threads"
            r1.<init>(r0, r4)
            r5.A00(r3, r2, r1)
            goto L_0x0274
        L_0x02d5:
            X.0lq r0 = X.C11070lo.A01
            com.facebook.fbservice.service.OperationResult r0 = X.C11070lo.A01(r5, r0)
            return r0
        L_0x02dc:
            r4 = r2
            X.0kw r4 = (X.C10840kw) r4
            java.lang.String r0 = "markThreadsParams"
            android.os.Parcelable r8 = X.AnonymousClass0ls.A00(r1, r0)
            com.facebook.messaging.service.model.MarkThreadsParams r8 = (com.facebook.messaging.service.model.MarkThreadsParams) r8
            java.lang.Integer r5 = r8.A02
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r3 = 1
            r2 = 0
            r0 = 0
            if (r5 != r1) goto L_0x02f1
            r0 = 1
        L_0x02f1:
            com.google.common.base.Preconditions.checkArgument(r0)
            int r1 = X.AnonymousClass1Y3.AkG
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1tG r0 = (X.C36521tG) r0
            com.google.common.collect.ImmutableList r1 = r8.A00
            X.0Tq r0 = r0.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r7 = r0.A01()
            r0 = 1987933346(0x767d74a2, float:1.28517256E33)
            X.C007406x.A01(r7, r0)
            X.1Xv r6 = r1.iterator()     // Catch:{ all -> 0x044d }
        L_0x0316:
            boolean r0 = r6.hasNext()     // Catch:{ all -> 0x044d }
            if (r0 == 0) goto L_0x033a
            java.lang.Object r5 = r6.next()     // Catch:{ all -> 0x044d }
            com.facebook.messaging.service.model.MarkThreadFields r5 = (com.facebook.messaging.service.model.MarkThreadFields) r5     // Catch:{ all -> 0x044d }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A06     // Catch:{ all -> 0x044d }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0)     // Catch:{ all -> 0x044d }
            com.google.common.base.Preconditions.checkArgument(r0)     // Catch:{ all -> 0x044d }
            boolean r0 = r5.A07     // Catch:{ all -> 0x044d }
            if (r0 == 0) goto L_0x0337
            long r1 = r5.A04     // Catch:{ all -> 0x044d }
        L_0x0331:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A06     // Catch:{ all -> 0x044d }
            X.C36521tG.A04(r7, r0, r1)     // Catch:{ all -> 0x044d }
            goto L_0x0316
        L_0x0337:
            r1 = 0
            goto L_0x0331
        L_0x033a:
            r7.setTransactionSuccessful()     // Catch:{ all -> 0x044d }
            r0 = 1912112168(0x71f88428, float:2.4611856E30)
            X.C007406x.A02(r7, r0)
            com.google.common.collect.ImmutableList r0 = r8.A00
            X.1Xv r9 = r0.iterator()
        L_0x0349:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x03e0
            java.lang.Object r5 = r9.next()
            com.facebook.messaging.service.model.MarkThreadFields r5 = (com.facebook.messaging.service.model.MarkThreadFields) r5
            boolean r0 = r5.A07
            if (r0 == 0) goto L_0x0349
            android.os.Bundle r8 = new android.os.Bundle
            r8.<init>()
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r5.A06
            java.lang.String r0 = "thread_key"
            r8.putParcelable(r0, r1)
            long r1 = r5.A04
            r6 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 * r6
            java.lang.String r0 = "timestamp_us"
            r8.putLong(r0, r1)
            r2 = 2
            int r1 = X.AnonymousClass1Y3.B1W
            X.0UN r0 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.fbservice.ops.BlueServiceOperationFactory r2 = (com.facebook.fbservice.ops.BlueServiceOperationFactory) r2
            java.lang.Class<X.0kw> r0 = X.C10840kw.class
            com.facebook.common.callercontext.CallerContext r1 = com.facebook.common.callercontext.CallerContext.A04(r0)
            java.lang.String r0 = "TincanSendReadReceipt"
            X.0lL r0 = r2.newInstance(r0, r8, r3, r1)
            r0.CGe()
            r2 = 9
            int r1 = X.AnonymousClass1Y3.BPS
            X.0UN r0 = r4.A00
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.7aN r10 = (X.C159577aN) r10
            monitor-enter(r10)
            com.facebook.messaging.model.threadkey.ThreadKey r11 = r5.A06     // Catch:{ all -> 0x03a5 }
            long r12 = r5.A04     // Catch:{ all -> 0x03a5 }
            X.06A r0 = X.AnonymousClass06A.A00     // Catch:{ all -> 0x03a5 }
            long r14 = r0.now()     // Catch:{ all -> 0x03a5 }
            r10.A06(r11, r12, r14)     // Catch:{ all -> 0x03a5 }
            monitor-exit(r10)
            goto L_0x0349
        L_0x03a5:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x03a8:
            r3 = move-exception
            java.lang.Integer r4 = X.AnonymousClass07B.A00
            java.lang.String r6 = r3.getMessage()
            java.lang.Class r3 = r3.getClass()
            java.lang.String r7 = r3.getSimpleName()
            r8 = 1
            r12 = -1
            r3 = r1
            r5 = r0
            r10 = r17
            r13 = r25
            X.C159107Yr.A00(r3, r4, r5, r6, r7, r8, r10, r12, r13)
        L_0x03c3:
            if (r24 == 0) goto L_0x03e3
            int r2 = X.AnonymousClass1Y3.AZc
            X.0UN r1 = r1.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r15, r2, r1)
            X.7Yq r1 = (X.C159097Yq) r1
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r0.A06
            long r2 = r0.A02
            X.1tX r1 = r1.A00
            java.lang.String r0 = r4.A0J()
            java.lang.String r0 = X.C159097Yq.A01(r0, r2)
            r1.A03(r0)
        L_0x03e0:
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A00
            return r0
        L_0x03e3:
            java.lang.Integer r5 = r2.A02
            r4 = 7
            int r3 = X.AnonymousClass1Y3.AZp     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.0UN r2 = r1.A00     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.2rR r3 = (X.C57112rR) r3     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            java.lang.Integer r2 = X.AnonymousClass07B.A01     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            r3.A0D(r5, r0, r2)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            r4 = 0
            int r3 = X.AnonymousClass1Y3.BAS     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.0UN r2 = r1.A00     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.3Bj r3 = (X.C64393Bj) r3     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.7Z0 r2 = X.C159107Yr.A01(r1, r5)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            r3.A02(r2, r0)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            int r3 = X.AnonymousClass1Y3.AZc     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.0UN r2 = r1.A00     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r15, r3, r2)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.7Yq r3 = (X.C159097Yq) r3     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r0.A06     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            long r4 = r0.A02     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            X.1tX r3 = r3.A00     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            java.lang.String r2 = r2.A0J()     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            java.lang.String r2 = X.C159097Yq.A01(r2, r4)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            r3.A03(r2)     // Catch:{ 1wB -> 0x0436, TigonErrorException -> 0x042e, Exception -> 0x0423 }
            goto L_0x03e0
        L_0x0423:
            r2 = move-exception
            r6 = -1
            r3 = r0
            r4 = r17
            r7 = r25
            X.C159107Yr.A03(r1, r2, r3, r4, r6, r7)
            throw r2
        L_0x042e:
            r3 = move-exception
            com.facebook.tigon.TigonError r2 = r3.tigonError
            if (r2 == 0) goto L_0x044b
            int r2 = r2.mCategory
            goto L_0x043f
        L_0x0436:
            r3 = move-exception
            com.facebook.http.protocol.ApiErrorResult r2 = r3.result
            if (r2 == 0) goto L_0x044b
            int r2 = r2.A02()
        L_0x043f:
            r4 = r1
            r5 = r3
            r6 = r0
            r7 = r17
            r9 = r2
            r10 = r25
            X.C159107Yr.A03(r4, r5, r6, r7, r9, r10)
            throw r3
        L_0x044b:
            r2 = -1
            goto L_0x043f
        L_0x044d:
            r1 = move-exception
            r0 = 1113953508(0x426594e4, float:57.3954)
            X.C007406x.A02(r7, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0f(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0g(C11060ln r15, C27311cz r16) {
        if (this instanceof C10840kw) {
            C10840kw r5 = (C10840kw) this;
            ModifyThreadParams modifyThreadParams = (ModifyThreadParams) AnonymousClass0ls.A00(r15, "modifyThreadParams");
            ThreadKey threadKey = modifyThreadParams.A03;
            if (modifyThreadParams.A0E) {
                String str = modifyThreadParams.A08;
                ContentValues contentValues = new ContentValues();
                contentValues.put(C193717w.A0G.A00, str);
                C36521tG.A08((C36521tG) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AkG, r5.A00), threadKey, contentValues);
            } else if (modifyThreadParams.A0D) {
                ((C36521tG) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AkG, r5.A00)).A0G(threadKey, Integer.valueOf(modifyThreadParams.A00));
            } else if (modifyThreadParams.A0B) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("thread_key", threadKey);
                bundle.putInt("message_lifetime_ms", modifyThreadParams.A00);
                ((BlueServiceOperationFactory) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B1W, r5.A00)).newInstance("ChangeThreadMessageLifetime", bundle, 1, CallerContext.A04(C10840kw.class)).CGe();
            } else {
                Preconditions.checkState(false, "Unsupported modify-threads operation.");
            }
            return OperationResult.A04(((C193917y) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AI2, r5.A00)).A01(threadKey, 0));
        } else if (this instanceof C11070lo) {
            return C11070lo.A00((C11070lo) this, C11070lo.A03(((ModifyThreadParams) AnonymousClass0ls.A00(r15, "modifyThreadParams")).A03)).BAz(r15);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r6 = (C11150lz) this;
                if (!(r6 instanceof C12440pM)) {
                    C27311cz r1 = r16;
                    if (r6 instanceof C12370pE) {
                        C12370pE r62 = (C12370pE) r6;
                        FetchThreadResult fetchThreadResult = (FetchThreadResult) r1.BAz(r15).A08();
                        if (fetchThreadResult != null) {
                            ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r62.A00)).A0Y(fetchThreadResult, "handleModifyThread");
                        }
                        return OperationResult.A04(fetchThreadResult);
                    } else if (!(r6 instanceof C11130lx)) {
                        return r1.BAz(r15);
                    } else {
                        C11130lx r63 = (C11130lx) r6;
                        boolean A012 = C11130lx.A01(r63);
                        try {
                            OperationResult BAz = r1.BAz(r15);
                            FetchThreadResult fetchThreadResult2 = (FetchThreadResult) BAz.A08();
                            if (!(fetchThreadResult2 == null || fetchThreadResult2.A05 == null)) {
                                ((C14800u9) r63.A01.get()).A0H(fetchThreadResult2);
                            }
                        } finally {
                            if (A012) {
                                ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r63.A00)).A03();
                            }
                        }
                    }
                } else {
                    C12440pM r64 = (C12440pM) r6;
                    ImmutableList immutableList = RegularImmutableList.A02;
                    ModifyThreadParams modifyThreadParams2 = (ModifyThreadParams) AnonymousClass0ls.A00(r15, "modifyThreadParams");
                    boolean z = modifyThreadParams2.A0E;
                    if (!z && !modifyThreadParams2.A0G && !modifyThreadParams2.A0C) {
                        return OperationResult.A00;
                    }
                    long j = modifyThreadParams2.A03.A03;
                    if (z) {
                        String str2 = modifyThreadParams2.A08;
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put(C50612eJ.A01.A00, str2);
                        C52602jN.A03((C52602jN) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ABY, ((C50462e4) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKa, r64.A01)).A00), j, contentValues2);
                    }
                    boolean z2 = modifyThreadParams2.A0G;
                    if (z2 || modifyThreadParams2.A0C) {
                        C50462e4 r11 = (C50462e4) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKa, r64.A01);
                        ThreadCustomization threadCustomization = modifyThreadParams2.A05;
                        if (z2) {
                            int i = modifyThreadParams2.A02;
                            ContentValues contentValues3 = new ContentValues();
                            contentValues3.put(C50612eJ.A02.A00, Integer.valueOf(i));
                            C52602jN.A03((C52602jN) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ABY, r11.A00), j, contentValues3);
                        }
                        if (modifyThreadParams2.A0C) {
                            String str3 = threadCustomization.A01;
                            ContentValues contentValues4 = new ContentValues();
                            contentValues4.put(C50612eJ.A00.A00, str3);
                            C52602jN.A03((C52602jN) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ABY, r11.A00), j, contentValues4);
                        }
                    }
                    ThreadSummary A0B = ((C50462e4) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKa, r64.A01)).A0B(j, null);
                    C61222yX A002 = FetchThreadResult.A00();
                    A002.A01 = DataFetchDisposition.A0F;
                    A002.A00 = ((AnonymousClass06B) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AgK, r64.A01)).now();
                    A002.A04 = A0B;
                    A002.A06 = immutableList;
                    return OperationResult.A04(A002.A00());
                }
            } else {
                C12380pF r52 = (C12380pF) this;
                ModifyThreadParams modifyThreadParams3 = (ModifyThreadParams) AnonymousClass0ls.A00(r15, "modifyThreadParams");
                int i2 = AnonymousClass1Y3.BAS;
                AnonymousClass0UN r3 = r52.A00;
                C73313fs A013 = ((C64393Bj) AnonymousClass1XX.A02(2, i2, r3)).A01();
                String str4 = null;
                if (modifyThreadParams3.A0E) {
                    C73333fu A003 = C73323ft.A00((AnonymousClass56M) AnonymousClass1XX.A02(13, AnonymousClass1Y3.ABc, r3), modifyThreadParams3);
                    A003.A02 = "setThreadName";
                    A003.A01 = null;
                    A013.A00(A003.A00());
                    str4 = "setThreadName";
                }
                if (modifyThreadParams3.A0H) {
                    C73333fu A004 = C73323ft.A00((C185438j5) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AsD, r52.A00), modifyThreadParams3);
                    A004.A02 = "setThreadImage";
                    A004.A01 = str4;
                    A013.A00(A004.A00());
                    str4 = "setThreadImage";
                }
                if (modifyThreadParams3.A0F) {
                    C73333fu A005 = C73323ft.A00((AnonymousClass56N) AnonymousClass1XX.A02(18, AnonymousClass1Y3.B1I, r52.A00), modifyThreadParams3);
                    A005.A02 = "muteThread";
                    A005.A01 = str4;
                    A013.A00(A005.A00());
                    str4 = "muteThread";
                }
                if (modifyThreadParams3.A0D) {
                    C73333fu A006 = C73323ft.A00((C185488jC) AnonymousClass1XX.A02(12, AnonymousClass1Y3.AG4, r52.A00), modifyThreadParams3);
                    A006.A02 = "setThreadEphemerality";
                    A006.A01 = str4;
                    A013.A00(A006.A00());
                    str4 = "setThreadEphemerality";
                }
                if (modifyThreadParams3.A0C) {
                    C73333fu A007 = C73323ft.A00((C185448j6) AnonymousClass1XX.A02(16, AnonymousClass1Y3.Afe, r52.A00), modifyThreadParams3);
                    A007.A02 = "setThreadTheme";
                    A007.A01 = str4;
                    A013.A00(A007.A00());
                    str4 = "setThreadTheme";
                }
                if (modifyThreadParams3.A06 != null) {
                    C73333fu A008 = C73323ft.A00((AnonymousClass8j7) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AKC, r52.A00), modifyThreadParams3);
                    A008.A02 = "setThreadParticipantNickname";
                    A008.A01 = str4;
                    A013.A00(A008.A00());
                }
                A013.A02("modifyThread", CallerContext.A04(r52.getClass()), C12380pF.A00(r52));
                return OperationResult.A00;
            }
        }
    }

    public OperationResult A0h(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (this instanceof C11150lz) {
                return r8.BAz(r7);
            }
            C12380pF r5 = (C12380pF) this;
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r3 = r5.A00;
            return OperationResult.A04((PostGameScoreResult) ((C64393Bj) AnonymousClass1XX.A02(2, i, r3)).A03((C185498jD) AnonymousClass1XX.A02(23, AnonymousClass1Y3.AfO, r3), (PostGameScoreParams) r7.A00.getParcelable("postGameScoreParams"), C12380pF.A00(r5)));
        }
    }

    public OperationResult A0i(C11060ln r13, C27311cz r14) {
        ParticipantInfo participantInfo;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r13);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r3 = (C11150lz) this;
                if (r3 instanceof C12370pE) {
                    C12370pE r32 = (C12370pE) r3;
                    Message message = (Message) r13.A00.getParcelable("message");
                    C10950l8 r4 = C10950l8.A05;
                    boolean z = true;
                    Message A08 = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r32.A00)).A08(message.A0q);
                    if (A08 != null && !A08.A14) {
                        z = false;
                    }
                    if (z) {
                        ((C07080cb) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Aeq, r32.A00)).A05(C12740pt.A00(r4), "1");
                    }
                    AnonymousClass1TG A002 = Message.A00();
                    A002.A03(message);
                    A002.A12 = true;
                    return OperationResult.A04(((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r32.A00)).A0Q(new NewMessageResult(AnonymousClass102.FROM_SERVER, ((C89774Qi) AnonymousClass1XX.A02(10, AnonymousClass1Y3.Ar9, r32.A00)).A01(A002.A00()), null, null, System.currentTimeMillis()), -1));
                } else if (!(r3 instanceof C11130lx)) {
                    return r14.BAz(r13);
                } else {
                    C11130lx r33 = (C11130lx) r3;
                    ((C1059154m) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AHK, r33.A00)).A00.A03("a_m_pm_start");
                    PushProperty pushProperty = (PushProperty) r13.A00.getParcelable("pushProperty");
                    boolean A012 = C11130lx.A01(r33);
                    try {
                        ThreadKey threadKey = ((Message) r13.A00.getParcelable("message")).A0U;
                        ThreadSummary A0R = r33.A03.A0R(threadKey);
                        if (A0R == null) {
                            C60292wu r5 = new C60292wu();
                            r5.A01 = C09510hU.DO_NOT_CHECK_SERVER;
                            r5.A03 = ThreadCriteria.A00(threadKey);
                            r5.A00 = 20;
                            FetchThreadParams fetchThreadParams = new FetchThreadParams(r5);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("fetchThreadParams", fetchThreadParams);
                            C207529qM r1 = new C207529qM();
                            r1.A01(r13);
                            r1.A05 = "fetch_thread";
                            r1.A00 = bundle;
                            A0R = ((FetchThreadResult) r33.A0V(r1.A00(), r14).A08()).A05;
                        }
                        OperationResult BAz = r14.BAz(r13);
                        NewMessageResult newMessageResult = (NewMessageResult) BAz.A08();
                        if (newMessageResult != null) {
                            Message A013 = ((C89774Qi) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Ar9, r33.A00)).A01(newMessageResult.A01);
                            ((C14800u9) r33.A01.get()).A0I(newMessageResult, -1);
                            ((C189216c) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B7s, r33.A00)).A0G(A013.A0U, "CacheServiceHandler.handlePushedMessage");
                            C206749oq r6 = (C206749oq) AnonymousClass1XX.A02(6, AnonymousClass1Y3.A21, r33.A00);
                            if (A0R != null && r6.A03 == C001500z.A07 && (participantInfo = A013.A0K) != null && participantInfo.A01() && !r6.A05.A1H(A013)) {
                                AnonymousClass31X r0 = pushProperty.A02;
                                r6.A04(A013, r0 == null ? "PUSH_NOTIFICATION" : AnonymousClass08S.A0J("PUSH_NOTIFICATION:", r0.toString()));
                            }
                            ((C1059154m) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AHK, r33.A00)).A00.A03("a_m_pm_finish");
                        } else {
                            ((C1059154m) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AHK, r33.A00)).A00.A03("a_m_pm_skip");
                        }
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r33.A00)).A03();
                        }
                    }
                }
            } else {
                throw new AnonymousClass7JR(r13);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x015e, code lost:
        if (r8 != null) goto L_0x0160;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0j(X.C11060ln r19, X.C27311cz r20) {
        /*
            r18 = this;
            r1 = r18
            boolean r0 = r1 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0237
            boolean r0 = r1 instanceof X.C11070lo
            r5 = r19
            if (r0 != 0) goto L_0x0224
            boolean r0 = r1 instanceof X.C12380pF
            if (r0 != 0) goto L_0x021e
            r3 = r1
            X.0lz r3 = (X.C11150lz) r3
            boolean r0 = r3 instanceof X.C27561dO
            r7 = r20
            if (r0 != 0) goto L_0x00f7
            boolean r0 = r3 instanceof X.C12440pM
            if (r0 != 0) goto L_0x0120
            boolean r0 = r3 instanceof X.C11130lx
            if (r0 != 0) goto L_0x0026
            com.facebook.fbservice.service.OperationResult r4 = r7.BAz(r5)
        L_0x0025:
            return r4
        L_0x0026:
            X.0lx r3 = (X.C11130lx) r3
            android.os.Bundle r2 = r5.A00
            java.lang.String r0 = "message"
            android.os.Parcelable r6 = r2.getParcelable(r0)
            com.facebook.messaging.model.messages.Message r6 = (com.facebook.messaging.model.messages.Message) r6
            com.facebook.fbservice.service.OperationResult r4 = r7.BAz(r5)
            java.lang.Object r8 = r4.A08()
            com.facebook.messaging.service.model.NewMessageResult r8 = (com.facebook.messaging.service.model.NewMessageResult) r8
            if (r8 == 0) goto L_0x0025
            X.0US r0 = r3.A01
            java.lang.Object r9 = r0.get()
            X.0u9 r9 = (X.C14800u9) r9
            com.facebook.messaging.model.messages.Message r1 = r8.A01
            X.0m6 r10 = r9.A01
            com.facebook.messaging.model.messages.MessagesCollection r12 = r8.A02
            java.lang.Boolean r0 = r8.A00
            r11 = r1
            X.3dk r15 = X.C72153dk.A02
            r13 = -1
            r16 = r0
            r10.A0Y(r11, r12, r13, r15, r16)
            X.0t0 r10 = r9.A02
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r1.A0K
            com.facebook.user.model.UserKey r8 = r0.A01
            long r0 = r1.A03
            r10.A05(r8, r0)
            X.16c r0 = r9.A00
            r0.A08()
            java.lang.String r0 = "delete_msg_id"
            java.lang.String r1 = r2.getString(r0)
            if (r1 == 0) goto L_0x00a4
            X.0m6 r0 = r3.A03
            com.facebook.messaging.model.messages.Message r0 = r0.A0O(r1)
            if (r0 == 0) goto L_0x00a4
            android.os.Bundle r10 = new android.os.Bundle
            r10.<init>()
            com.google.common.collect.ImmutableSet r9 = com.google.common.collect.ImmutableSet.A04(r1)
            com.facebook.messaging.service.model.DeleteMessagesParams r8 = new com.facebook.messaging.service.model.DeleteMessagesParams
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0U
            r8.<init>(r9, r1, r0)
            java.lang.String r0 = "DeleteMessagesParams"
            r10.putParcelable(r0, r8)
            X.9qM r1 = new X.9qM
            r1.<init>()
            r1.A01(r5)
            java.lang.String r0 = "delete_messages"
            r1.A05 = r0
            r1.A00 = r10
            X.0ln r0 = r1.A00()
            r3.A0G(r0, r7)
        L_0x00a4:
            r5 = 3
            int r1 = X.AnonymousClass1Y3.B7s
            X.0UN r0 = r3.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.16c r5 = (X.C189216c) r5
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r6.A0U
            java.lang.String r0 = "CacheServiceHandler.handleReceivedSms"
            r5.A0G(r1, r0)
            r1 = 1
            java.lang.String r0 = "should_show_notification"
            boolean r0 = r2.getBoolean(r0, r1)
            if (r0 == 0) goto L_0x0025
            r1 = 0
            java.lang.String r0 = "only_notify_from_chathead"
            boolean r0 = r2.getBoolean(r0, r1)
            if (r0 == 0) goto L_0x00ef
            com.facebook.push.constants.PushProperty r5 = new com.facebook.push.constants.PushProperty
            X.31X r0 = X.AnonymousClass31X.A0A
            r5.<init>(r0)
        L_0x00cf:
            r2 = 8
            int r1 = X.AnonymousClass1Y3.BIZ
            X.0UN r0 = r3.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2rg r2 = (X.C57252rg) r2
            com.facebook.messaging.model.threads.ThreadCustomization r1 = com.facebook.messaging.model.threads.ThreadCustomization.A02
            com.facebook.messaging.push.flags.ServerMessageAlertFlags r0 = com.facebook.messaging.push.flags.ServerMessageAlertFlags.A06
            com.facebook.messaging.notify.type.NewMessageNotification r1 = r2.A01(r6, r1, r5, r0)
            X.0Tq r0 = r3.A06
            java.lang.Object r0 = r0.get()
            X.16u r0 = (X.C191016u) r0
            r0.BMY(r1)
            return r4
        L_0x00ef:
            com.facebook.push.constants.PushProperty r5 = new com.facebook.push.constants.PushProperty
            X.31X r0 = X.AnonymousClass31X.A09
            r5.<init>(r0)
            goto L_0x00cf
        L_0x00f7:
            X.1dO r3 = (X.C27561dO) r3
            com.facebook.fbservice.service.OperationResult r4 = r7.BAz(r5)
            java.lang.Object r0 = r4.A08()
            com.facebook.messaging.service.model.NewMessageResult r0 = (com.facebook.messaging.service.model.NewMessageResult) r0
            if (r0 == 0) goto L_0x0025
            com.facebook.messaging.model.messages.Message r0 = r0.A01
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0U
            if (r0 == 0) goto L_0x0025
            X.2e3 r0 = X.C27561dO.A00(r3, r0)
            if (r0 == 0) goto L_0x0025
            r2 = 5
            int r1 = X.AnonymousClass1Y3.BEM
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2rr r0 = (X.C57362rr) r0
            r0.A02()
            return r4
        L_0x0120:
            X.0pM r3 = (X.C12440pM) r3
            java.lang.String r1 = "SmsServiceHandler.handleReceivedSms"
            r0 = 963929913(0x39746739, float:2.3308107E-4)
            X.C005505z.A03(r1, r0)
            android.os.Bundle r5 = r5.A00     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "message"
            android.os.Parcelable r13 = r5.getParcelable(r0)     // Catch:{ all -> 0x0216 }
            com.facebook.messaging.model.messages.Message r13 = (com.facebook.messaging.model.messages.Message) r13     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "is_readonly_mode"
            r4 = 0
            boolean r0 = r5.getBoolean(r0, r4)     // Catch:{ all -> 0x0216 }
            if (r0 != 0) goto L_0x01b5
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x0216 }
            X.0UN r0 = r3.A01     // Catch:{ all -> 0x0216 }
            r6 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x0216 }
            X.2e4 r2 = (X.C50462e4) r2     // Catch:{ all -> 0x0216 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r13.A0U     // Catch:{ all -> 0x0216 }
            long r0 = r0.A0G()     // Catch:{ all -> 0x0216 }
            java.util.List r0 = r2.A0D(r0)     // Catch:{ all -> 0x0216 }
            int r10 = r0.size()     // Catch:{ all -> 0x0216 }
            r9 = 5
            java.lang.String r8 = X.C21451Ha.A03(r13)     // Catch:{ all -> 0x0216 }
            if (r10 > r6) goto L_0x0160
            r7 = 0
            if (r8 == 0) goto L_0x0161
        L_0x0160:
            r7 = 1
        L_0x0161:
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r13.A0K     // Catch:{ all -> 0x0216 }
            r12 = 0
            com.facebook.user.model.UserKey r2 = r0.A01     // Catch:{ all -> 0x0216 }
            X.1aB r1 = r2.type     // Catch:{ all -> 0x0216 }
            X.1aB r0 = X.C25651aB.A02     // Catch:{ all -> 0x0216 }
            if (r1 != r0) goto L_0x01e7
            java.lang.String r6 = r2.A05()     // Catch:{ all -> 0x0216 }
        L_0x0170:
            int r1 = X.AnonymousClass1Y3.AFw     // Catch:{ all -> 0x0216 }
            X.0UN r0 = r3.A01     // Catch:{ all -> 0x0216 }
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r9, r1, r0)     // Catch:{ all -> 0x0216 }
            X.1Ha r11 = (X.C21451Ha) r11     // Catch:{ all -> 0x0216 }
            com.facebook.messaging.model.mms.MmsData r0 = r13.A0N     // Catch:{ all -> 0x0216 }
            boolean r1 = r0.A01()     // Catch:{ all -> 0x0216 }
            com.facebook.messaging.model.mms.MmsData r0 = r13.A0N     // Catch:{ all -> 0x0216 }
            com.google.common.collect.ImmutableList r0 = r0.A02     // Catch:{ all -> 0x0216 }
            int r9 = r0.size()     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "sms_takeover_message_received"
            X.0nb r2 = X.C21451Ha.A01(r0)     // Catch:{ all -> 0x0216 }
            if (r7 == 0) goto L_0x0195
            java.lang.String r0 = "is_pending_download"
            r2.A0E(r0, r1)     // Catch:{ all -> 0x0216 }
        L_0x0195:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r6)     // Catch:{ all -> 0x0216 }
            if (r0 != 0) goto L_0x01a4
            java.lang.String r1 = X.C50552eD.A01(r6)     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "sender_address"
            r2.A0D(r0, r1)     // Catch:{ all -> 0x0216 }
        L_0x01a4:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r12)     // Catch:{ all -> 0x0216 }
            if (r0 != 0) goto L_0x01af
            java.lang.String r0 = "phone_number_type"
            r2.A0D(r0, r12)     // Catch:{ all -> 0x0216 }
        L_0x01af:
            X.C21451Ha.A04(r2, r7, r8, r9, r10)     // Catch:{ all -> 0x0216 }
            X.C21451Ha.A05(r11, r2)     // Catch:{ all -> 0x0216 }
        L_0x01b5:
            com.facebook.messaging.service.model.NewMessageResult r11 = new com.facebook.messaging.service.model.NewMessageResult     // Catch:{ all -> 0x0216 }
            X.102 r12 = X.AnonymousClass102.FROM_CACHE_UP_TO_DATE     // Catch:{ all -> 0x0216 }
            r14 = 0
            r15 = 0
            r2 = 4
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x0216 }
            X.0UN r0 = r3.A01     // Catch:{ all -> 0x0216 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0216 }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x0216 }
            long r16 = r0.now()     // Catch:{ all -> 0x0216 }
            r11.<init>(r12, r13, r14, r15, r16)     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "is_class_zero"
            boolean r0 = r5.getBoolean(r0, r4)     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x01dc
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0216 }
            r11.A00 = r0     // Catch:{ all -> 0x0216 }
        L_0x01dc:
            com.facebook.fbservice.service.OperationResult r4 = com.facebook.fbservice.service.OperationResult.A04(r11)     // Catch:{ all -> 0x0216 }
            r0 = -1388577681(0xffffffffad3bfc6f, float:-1.0685771E-11)
            X.C005505z.A00(r0)
            return r4
        L_0x01e7:
            java.lang.String r6 = r2.A07()     // Catch:{ all -> 0x0216 }
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r6)     // Catch:{ all -> 0x0216 }
            if (r0 != 0) goto L_0x0170
            int r1 = X.AnonymousClass1Y3.ANW     // Catch:{ all -> 0x0216 }
            X.0UN r0 = r3.A01     // Catch:{ all -> 0x0216 }
            r11 = 7
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r11, r1, r0)     // Catch:{ all -> 0x0216 }
            X.2eF r0 = (X.C50572eF) r0     // Catch:{ all -> 0x0216 }
            com.facebook.phonenumbers.Phonenumber$PhoneNumber r2 = r0.A03(r6)     // Catch:{ all -> 0x0216 }
            if (r2 == 0) goto L_0x0170
            X.0UN r0 = r3.A01     // Catch:{ all -> 0x0216 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r11, r1, r0)     // Catch:{ all -> 0x0216 }
            X.2eF r0 = (X.C50572eF) r0     // Catch:{ all -> 0x0216 }
            com.facebook.phonenumbers.PhoneNumberUtil r0 = r0.A00     // Catch:{ all -> 0x0216 }
            com.facebook.phonenumbers.PhoneNumberUtil$PhoneNumberType r0 = r0.getNumberType(r2)     // Catch:{ all -> 0x0216 }
            java.lang.String r12 = r0.toString()     // Catch:{ all -> 0x0216 }
            goto L_0x0170
        L_0x0216:
            r1 = move-exception
            r0 = -221252501(0xfffffffff2cff46b, float:-8.2379367E30)
            X.C005505z.A00(r0)
            throw r1
        L_0x021e:
            X.7JR r0 = new X.7JR
            r0.<init>(r5)
            throw r0
        L_0x0224:
            r0 = r1
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.BQB
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r5)
            return r0
        L_0x0237:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0j(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0k(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (this instanceof C11150lz) {
                return r8.BAz(r7);
            }
            C12380pF r5 = (C12380pF) this;
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r3 = r5.A00;
            return OperationResult.A04((RemoveAdminsFromGroupResult) ((C64393Bj) AnonymousClass1XX.A02(2, i, r3)).A03((C183658fY) AnonymousClass1XX.A02(22, AnonymousClass1Y3.A4n, r3), (RemoveAdminsFromGroupParams) r7.A00.getParcelable("removeAdminsFromGroupParams"), C12380pF.A00(r5)));
        }
    }

    public OperationResult A0l(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (this instanceof C11150lz) {
                return r8.BAz(r7);
            }
            C12380pF r5 = (C12380pF) this;
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r3 = r5.A00;
            ((C64393Bj) AnonymousClass1XX.A02(2, i, r3)).A03((AnonymousClass2Q9) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Azy, r3), (RemoveMemberParams) r7.A00.getParcelable(C99084oO.$const$string(AnonymousClass1Y3.A5i)), C12380pF.A00(r5));
            return OperationResult.A00;
        }
    }

    public OperationResult A0m(C11060ln r10, C27311cz r11) {
        if (this instanceof C10840kw) {
            SaveDraftParams saveDraftParams = (SaveDraftParams) AnonymousClass0ls.A00(r10, "saveDraftParams");
            C36521tG r6 = (C36521tG) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AkG, ((C10840kw) this).A00);
            ThreadKey threadKey = saveDraftParams.A01;
            MessageDraft messageDraft = saveDraftParams.A00;
            ContentValues contentValues = new ContentValues();
            contentValues.put(C193717w.A02.A00, ((C13700rt) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B1R, r6.A01)).A02(messageDraft));
            C36521tG.A08(r6, threadKey, contentValues);
            return OperationResult.A00;
        } else if (this instanceof C11070lo) {
            return C11070lo.A00((C11070lo) this, C11070lo.A03(((SaveDraftParams) AnonymousClass0ls.A00(r10, "saveDraftParams")).A01)).BAz(r10);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r4 = (C11150lz) this;
                if (r4 instanceof C12440pM) {
                    return OperationResult.A00;
                }
                if (r4 instanceof C12370pE) {
                    SaveDraftParams saveDraftParams2 = (SaveDraftParams) r10.A00.getParcelable("saveDraftParams");
                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, ((C12370pE) r4).A00)).A0V(saveDraftParams2.A01, saveDraftParams2.A00);
                    return OperationResult.A00;
                } else if (!(r4 instanceof C11130lx)) {
                    return r11.BAz(r10);
                } else {
                    C11130lx r42 = (C11130lx) r4;
                    boolean A012 = C11130lx.A01(r42);
                    try {
                        OperationResult BAz = r11.BAz(r10);
                        SaveDraftParams saveDraftParams3 = (SaveDraftParams) r10.A00.getParcelable("saveDraftParams");
                        C14800u9 r62 = (C14800u9) r42.A01.get();
                        ThreadKey threadKey2 = saveDraftParams3.A01;
                        MessageDraft messageDraft2 = saveDraftParams3.A00;
                        ThreadSummary A0R = r62.A01.A0R(threadKey2);
                        if (A0R != null) {
                            AnonymousClass0m6 r1 = r62.A01;
                            C17920zh A002 = ThreadSummary.A00();
                            A002.A02(A0R);
                            A002.A0O = messageDraft2;
                            r1.A0b(A002.A00());
                        }
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r42.A00)).A03();
                        }
                    }
                }
            } else {
                throw new AnonymousClass7JR(r10);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b5, code lost:
        if (r11 != null) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ba, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0n(X.C11060ln r13, X.C27311cz r14) {
        /*
            r12 = this;
            boolean r0 = r12 instanceof X.C10840kw
            if (r0 != 0) goto L_0x0118
            boolean r0 = r12 instanceof X.C11070lo
            if (r0 != 0) goto L_0x0105
            boolean r0 = r12 instanceof X.C12380pF
            if (r0 != 0) goto L_0x00ff
            r5 = r12
            X.0lz r5 = (X.C11150lz) r5
            boolean r0 = r5 instanceof X.C12370pE
            if (r0 != 0) goto L_0x00bb
            boolean r0 = r5 instanceof X.C11130lx
            if (r0 != 0) goto L_0x001c
            com.facebook.fbservice.service.OperationResult r0 = r14.BAz(r13)
            return r0
        L_0x001c:
            X.0lx r5 = (X.C11130lx) r5
            android.os.Bundle r1 = r13.A00
            java.lang.String r0 = "updatedMessageSendErrorParams"
            android.os.Parcelable r6 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.UpdateMessageSendErrorParams r6 = (com.facebook.messaging.service.model.UpdateMessageSendErrorParams) r6
            com.facebook.fbservice.service.OperationResult r0 = r14.BAz(r13)
            X.0m6 r7 = r5.A03
            X.0mL r1 = r7.A0A
            X.0mM r11 = r1.A00()
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r6.A01     // Catch:{ all -> 0x00b2 }
            X.AnonymousClass0m6.A0A(r7, r2)     // Catch:{ all -> 0x00b2 }
            X.0mO r1 = r7.A06     // Catch:{ all -> 0x00b2 }
            com.facebook.messaging.model.messages.MessagesCollection r9 = r1.A02(r2)     // Catch:{ all -> 0x00b2 }
            com.google.common.collect.ImmutableList$Builder r8 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x00b2 }
            com.google.common.collect.ImmutableList r1 = r9.A01     // Catch:{ all -> 0x00b2 }
            X.1Xv r10 = r1.iterator()     // Catch:{ all -> 0x00b2 }
        L_0x0049:
            boolean r1 = r10.hasNext()     // Catch:{ all -> 0x00b2 }
            if (r1 == 0) goto L_0x007a
            java.lang.Object r4 = r10.next()     // Catch:{ all -> 0x00b2 }
            com.facebook.messaging.model.messages.Message r4 = (com.facebook.messaging.model.messages.Message) r4     // Catch:{ all -> 0x00b2 }
            java.lang.String r2 = r6.A02     // Catch:{ all -> 0x00b2 }
            java.lang.String r1 = r4.A0w     // Catch:{ all -> 0x00b2 }
            boolean r1 = r2.equals(r1)     // Catch:{ all -> 0x00b2 }
            if (r1 == 0) goto L_0x0076
            com.facebook.messaging.model.send.SendError r3 = r6.A00     // Catch:{ all -> 0x00b2 }
            X.1TG r2 = com.facebook.messaging.model.messages.Message.A00()     // Catch:{ all -> 0x00b2 }
            r2.A03(r4)     // Catch:{ all -> 0x00b2 }
            X.1V7 r1 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x00b2 }
            r2.A0C = r1     // Catch:{ all -> 0x00b2 }
            r2.A0R = r3     // Catch:{ all -> 0x00b2 }
            com.facebook.messaging.model.messages.Message r1 = r2.A00()     // Catch:{ all -> 0x00b2 }
            r8.add(r1)     // Catch:{ all -> 0x00b2 }
            goto L_0x0049
        L_0x0076:
            r8.add(r4)     // Catch:{ all -> 0x00b2 }
            goto L_0x0049
        L_0x007a:
            X.1oI r2 = com.facebook.messaging.model.messages.MessagesCollection.A00(r9)     // Catch:{ all -> 0x00b2 }
            com.google.common.collect.ImmutableList r1 = r8.build()     // Catch:{ all -> 0x00b2 }
            r2.A01(r1)     // Catch:{ all -> 0x00b2 }
            r1 = 1
            r2.A02 = r1     // Catch:{ all -> 0x00b2 }
            com.facebook.messaging.model.messages.MessagesCollection r4 = r2.A00()     // Catch:{ all -> 0x00b2 }
            X.0mO r3 = r7.A06     // Catch:{ all -> 0x00b2 }
            X.0Tq r1 = r7.A0E     // Catch:{ all -> 0x00b2 }
            java.lang.Object r2 = r1.get()     // Catch:{ all -> 0x00b2 }
            com.facebook.user.model.User r2 = (com.facebook.user.model.User) r2     // Catch:{ all -> 0x00b2 }
            r1 = 0
            r3.A04(r4, r2, r1, r1)     // Catch:{ all -> 0x00b2 }
            if (r11 == 0) goto L_0x009f
            r11.close()
        L_0x009f:
            int r3 = X.AnonymousClass1Y3.B7s
            X.0UN r2 = r5.A00
            r1 = 3
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.16c r3 = (X.C189216c) r3
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r6.A01
            java.lang.String r1 = "CacheServiceHandler.handleUpdateFailedMessage"
            r3.A0G(r2, r1)
            return r0
        L_0x00b2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00b4 }
        L_0x00b4:
            r0 = move-exception
            if (r11 == 0) goto L_0x00ba
            r11.close()     // Catch:{ all -> 0x00ba }
        L_0x00ba:
            throw r0
        L_0x00bb:
            X.0pE r5 = (X.C12370pE) r5
            android.os.Bundle r1 = r13.A00
            java.lang.String r0 = "updatedMessageSendErrorParams"
            android.os.Parcelable r4 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.UpdateMessageSendErrorParams r4 = (com.facebook.messaging.service.model.UpdateMessageSendErrorParams) r4
            int r2 = X.AnonymousClass1Y3.B4i
            X.0UN r1 = r5.A00
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2wn r3 = (X.C60222wn) r3
            java.lang.String r1 = "DbSendHandler.updateFailedMessageSendError"
            r0 = -1839763621(0xffffffff92576f5b, float:-6.7979294E-28)
            X.C005505z.A03(r1, r0)
            X.1a6 r2 = X.C06160ax.A00()     // Catch:{ all -> 0x00f7 }
            java.lang.String r1 = "offline_threading_id"
            java.lang.String r0 = r4.A02     // Catch:{ all -> 0x00f7 }
            X.0av r0 = X.C06160ax.A03(r1, r0)     // Catch:{ all -> 0x00f7 }
            r2.A05(r0)     // Catch:{ all -> 0x00f7 }
            com.facebook.messaging.model.send.SendError r0 = r4.A00     // Catch:{ all -> 0x00f7 }
            X.C60222wn.A01(r3, r2, r0)     // Catch:{ all -> 0x00f7 }
            r0 = 428396383(0x1988cf5f, float:1.4145827E-23)
            X.C005505z.A00(r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A00
            return r0
        L_0x00f7:
            r1 = move-exception
            r0 = 1513982042(0x5a3d885a, float:1.33371727E16)
            X.C005505z.A00(r0)
            throw r1
        L_0x00ff:
            X.7JR r0 = new X.7JR
            r0.<init>(r13)
            throw r0
        L_0x0105:
            r0 = r12
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r13)
            return r0
        L_0x0118:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0n(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0o(C11060ln r9, C27311cz r10) {
        UpdateFolderCountsResult updateFolderCountsResult;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r9);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r3 = (C11150lz) this;
                if (r3 instanceof C12370pE) {
                    UpdateFolderCountsParams updateFolderCountsParams = (UpdateFolderCountsParams) r9.A00.getParcelable("updateFolderCountsParams");
                    C52232ij r5 = (C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, ((C12370pE) r3).A00);
                    FolderCounts A05 = r5.A04.A05(updateFolderCountsParams.A02);
                    FolderCounts folderCounts = A05 == null ? new FolderCounts(updateFolderCountsParams.A00, updateFolderCountsParams.A01, 0) : new FolderCounts(updateFolderCountsParams.A00, updateFolderCountsParams.A01, A05.A02);
                    C52232ij.A0A(r5, updateFolderCountsParams.A02, folderCounts);
                    return OperationResult.A04(new UpdateFolderCountsResult(folderCounts));
                } else if (!(r3 instanceof C11130lx)) {
                    return r10.BAz(r9);
                } else {
                    C11130lx r32 = (C11130lx) r3;
                    boolean A012 = C11130lx.A01(r32);
                    try {
                        OperationResult BAz = r10.BAz(r9);
                        if (BAz == null || !BAz.success || (updateFolderCountsResult = (UpdateFolderCountsResult) BAz.A08()) == null) {
                            if (A012) {
                                ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r32.A00)).A03();
                            }
                            return null;
                        }
                        ((C14800u9) r32.A01.get()).A05(((UpdateFolderCountsParams) r9.A00.getParcelable("updateFolderCountsParams")).A02, updateFolderCountsResult.A00);
                    } finally {
                        if (A012) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r32.A00)).A03();
                        }
                    }
                }
            } else {
                throw new AnonymousClass7JR(r9);
            }
        }
    }

    public OperationResult A0p(C11060ln r7, C27311cz r8) {
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (this instanceof C11150lz) {
                return r8.BAz(r7);
            }
            C12380pF r5 = (C12380pF) this;
            String string = r7.A00.getString("montageAudienceMode");
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r3 = r5.A00;
            ((C64393Bj) AnonymousClass1XX.A02(2, i, r3)).A03((C183558fN) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Ach, r3), string, C12380pF.A00(r5));
            return OperationResult.A00;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0081, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0082, code lost:
        if (r7 != null) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0087, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0q(X.C11060ln r10, X.C27311cz r11) {
        /*
            r9 = this;
            boolean r0 = r9 instanceof X.C10840kw
            if (r0 != 0) goto L_0x00c1
            boolean r0 = r9 instanceof X.C11070lo
            if (r0 != 0) goto L_0x00ae
            boolean r0 = r9 instanceof X.C12380pF
            if (r0 != 0) goto L_0x00a8
            r5 = r9
            X.0lz r5 = (X.C11150lz) r5
            boolean r0 = r5 instanceof X.C12370pE
            if (r0 != 0) goto L_0x0088
            boolean r0 = r5 instanceof X.C11130lx
            if (r0 != 0) goto L_0x001c
            com.facebook.fbservice.service.OperationResult r0 = r11.BAz(r10)
        L_0x001b:
            return r0
        L_0x001c:
            X.0lx r5 = (X.C11130lx) r5
            com.facebook.fbservice.service.OperationResult r0 = r11.BAz(r10)
            java.lang.Object r6 = r0.A08()
            com.facebook.messaging.model.messages.Message r6 = (com.facebook.messaging.model.messages.Message) r6
            if (r6 == 0) goto L_0x001b
            X.0US r1 = r5.A01
            java.lang.Object r1 = r1.get()
            X.0u9 r1 = (X.C14800u9) r1
            X.0m6 r8 = r1.A01
            X.0mL r1 = r8.A0A
            X.0mM r7 = r1.A00()
            X.0mO r2 = r8.A06     // Catch:{ all -> 0x007f }
            java.lang.String r1 = r6.A0q     // Catch:{ all -> 0x007f }
            com.facebook.messaging.model.messages.Message r2 = r2.A01(r1)     // Catch:{ all -> 0x007f }
            if (r2 == 0) goto L_0x0067
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r2.A0U     // Catch:{ all -> 0x007f }
            com.facebook.messaging.model.messages.MessagesCollection r3 = r8.A0P(r1)     // Catch:{ all -> 0x007f }
            if (r3 == 0) goto L_0x0067
            com.google.common.collect.ImmutableList r1 = r3.A01     // Catch:{ all -> 0x007f }
            int r2 = r1.indexOf(r2)     // Catch:{ all -> 0x007f }
            r1 = -1
            if (r2 == r1) goto L_0x0067
            com.facebook.messaging.model.messages.MessagesCollection r4 = X.AnonymousClass0m6.A01(r3, r6, r2)     // Catch:{ all -> 0x007f }
            X.0mO r3 = r8.A06     // Catch:{ all -> 0x007f }
            X.0Tq r1 = r8.A0E     // Catch:{ all -> 0x007f }
            java.lang.Object r2 = r1.get()     // Catch:{ all -> 0x007f }
            com.facebook.user.model.User r2 = (com.facebook.user.model.User) r2     // Catch:{ all -> 0x007f }
            r1 = 0
            r3.A04(r4, r2, r1, r1)     // Catch:{ all -> 0x007f }
        L_0x0067:
            if (r7 == 0) goto L_0x006c
            r7.close()
        L_0x006c:
            r3 = 3
            int r2 = X.AnonymousClass1Y3.B7s
            X.0UN r1 = r5.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.16c r3 = (X.C189216c) r3
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r6.A0U
            java.lang.String r1 = "CacheServiceHandler.handleUpdateMontageDirectAdminTextMedia"
            r3.A0G(r2, r1)
            return r0
        L_0x007f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0081 }
        L_0x0081:
            r0 = move-exception
            if (r7 == 0) goto L_0x0087
            r7.close()     // Catch:{ all -> 0x0087 }
        L_0x0087:
            throw r0
        L_0x0088:
            X.0pE r5 = (X.C12370pE) r5
            android.os.Bundle r1 = r10.A00
            java.lang.String r0 = "Message"
            android.os.Parcelable r3 = r1.getParcelable(r0)
            com.facebook.messaging.model.messages.Message r3 = (com.facebook.messaging.model.messages.Message) r3
            int r2 = X.AnonymousClass1Y3.BJD
            X.0UN r1 = r5.A00
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2ij r1 = (X.C52232ij) r1
            r0 = 1
            X.C52232ij.A03(r1, r3, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r3)
            return r0
        L_0x00a8:
            X.7JR r0 = new X.7JR
            r0.<init>(r10)
            throw r0
        L_0x00ae:
            r0 = r9
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r10)
            return r0
        L_0x00c1:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0q(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009f, code lost:
        if (r8 != null) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a4, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0r(X.C11060ln r13, X.C27311cz r14) {
        /*
            r12 = this;
            boolean r0 = r12 instanceof X.C10840kw
            if (r0 != 0) goto L_0x01b1
            boolean r0 = r12 instanceof X.C11070lo
            if (r0 != 0) goto L_0x019e
            boolean r0 = r12 instanceof X.C12380pF
            if (r0 != 0) goto L_0x0198
            r5 = r12
            X.0lz r5 = (X.C11150lz) r5
            boolean r0 = r5 instanceof X.C12370pE
            if (r0 != 0) goto L_0x00a5
            boolean r0 = r5 instanceof X.C11130lx
            if (r0 != 0) goto L_0x001c
            com.facebook.fbservice.service.OperationResult r10 = r14.BAz(r13)
        L_0x001b:
            return r10
        L_0x001c:
            X.0lx r5 = (X.C11130lx) r5
            com.facebook.fbservice.service.OperationResult r10 = r14.BAz(r13)
            android.os.Bundle r1 = r13.A00
            java.lang.String r0 = "messageId"
            java.lang.String r4 = r1.getString(r0)
            java.lang.String r0 = "overlays"
            java.util.ArrayList r9 = r1.getParcelableArrayList(r0)
            X.0US r0 = r5.A01
            java.lang.Object r0 = r0.get()
            X.0u9 r0 = (X.C14800u9) r0
            X.0m6 r7 = r0.A01
            X.0mL r0 = r7.A0A
            X.0mM r8 = r0.A00()
            X.0mO r0 = r7.A06     // Catch:{ all -> 0x009c }
            com.facebook.messaging.model.messages.Message r6 = r0.A01(r4)     // Catch:{ all -> 0x009c }
            if (r6 == 0) goto L_0x007c
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r6.A0U     // Catch:{ all -> 0x009c }
            com.facebook.messaging.model.messages.MessagesCollection r3 = r7.A0P(r0)     // Catch:{ all -> 0x009c }
            if (r3 == 0) goto L_0x007c
            com.google.common.collect.ImmutableList r0 = r3.A01     // Catch:{ all -> 0x009c }
            int r2 = r0.indexOf(r6)     // Catch:{ all -> 0x009c }
            r0 = -1
            if (r2 == r0) goto L_0x007c
            X.1TG r1 = com.facebook.messaging.model.messages.Message.A00()     // Catch:{ all -> 0x009c }
            r1.A03(r6)     // Catch:{ all -> 0x009c }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r9)     // Catch:{ all -> 0x009c }
            r1.A0b = r0     // Catch:{ all -> 0x009c }
            com.facebook.messaging.model.messages.Message r0 = r1.A00()     // Catch:{ all -> 0x009c }
            com.facebook.messaging.model.messages.MessagesCollection r3 = X.AnonymousClass0m6.A01(r3, r0, r2)     // Catch:{ all -> 0x009c }
            X.0mO r2 = r7.A06     // Catch:{ all -> 0x009c }
            X.0Tq r0 = r7.A0E     // Catch:{ all -> 0x009c }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x009c }
            com.facebook.user.model.User r1 = (com.facebook.user.model.User) r1     // Catch:{ all -> 0x009c }
            r0 = 0
            r2.A04(r3, r1, r0, r0)     // Catch:{ all -> 0x009c }
        L_0x007c:
            if (r8 == 0) goto L_0x0081
            r8.close()
        L_0x0081:
            X.0m6 r0 = r5.A03
            com.facebook.messaging.model.messages.Message r3 = r0.A0O(r4)
            if (r3 == 0) goto L_0x001b
            r2 = 3
            int r1 = X.AnonymousClass1Y3.B7s
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.16c r2 = (X.C189216c) r2
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r3.A0U
            java.lang.String r0 = "CacheServiceHandler.handleUpdateMontageInteractiveFeedbackOverlays"
            r2.A0G(r1, r0)
            return r10
        L_0x009c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x009e }
        L_0x009e:
            r0 = move-exception
            if (r8 == 0) goto L_0x00a4
            r8.close()     // Catch:{ all -> 0x00a4 }
        L_0x00a4:
            throw r0
        L_0x00a5:
            X.0pE r5 = (X.C12370pE) r5
            android.os.Bundle r1 = r13.A00
            java.lang.String r0 = "messageId"
            java.lang.String r4 = r1.getString(r0)
            java.lang.String r0 = "overlays"
            java.util.ArrayList r7 = r1.getParcelableArrayList(r0)
            int r2 = X.AnonymousClass1Y3.Al9
            X.0UN r1 = r5.A00
            r0 = 11
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2bj r0 = (X.C49152bj) r0
            java.lang.String r6 = "montage_message_poll_options"
            java.lang.String r5 = "poll_id"
            X.0Tq r0 = r0.A01
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r3 = r0.A06()
            r0 = 304185852(0x122181fc, float:5.096286E-28)
            X.C007406x.A01(r3, r0)
            java.lang.String r0 = "msg_id"
            X.0av r4 = X.C06160ax.A03(r0, r4)     // Catch:{ all -> 0x0190 }
            java.util.Iterator r11 = r7.iterator()     // Catch:{ all -> 0x0190 }
        L_0x00e1:
            boolean r0 = r11.hasNext()     // Catch:{ all -> 0x0190 }
            if (r0 == 0) goto L_0x0184
            java.lang.Object r2 = r11.next()     // Catch:{ all -> 0x0190 }
            com.facebook.messaging.model.messages.MontageFeedbackOverlay r2 = (com.facebook.messaging.model.messages.MontageFeedbackOverlay) r2     // Catch:{ all -> 0x0190 }
            com.facebook.messaging.model.messages.MontageFeedbackPoll r0 = r2.A01     // Catch:{ all -> 0x0190 }
            if (r0 == 0) goto L_0x00e1
            java.lang.String r0 = r0.A04     // Catch:{ all -> 0x0190 }
            X.0av r8 = X.C06160ax.A03(r5, r0)     // Catch:{ all -> 0x0190 }
            X.0av[] r0 = new X.C06140av[]{r4, r8}     // Catch:{ all -> 0x0190 }
            X.1a6 r10 = X.C06160ax.A01(r0)     // Catch:{ all -> 0x0190 }
            android.content.ContentValues r9 = new android.content.ContentValues     // Catch:{ all -> 0x0190 }
            r9.<init>()     // Catch:{ all -> 0x0190 }
            java.lang.String r1 = "can_viewer_vote"
            com.facebook.messaging.model.messages.MontageFeedbackPoll r0 = r2.A01     // Catch:{ all -> 0x0190 }
            boolean r0 = r0.A08     // Catch:{ all -> 0x0190 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0190 }
            r9.put(r1, r0)     // Catch:{ all -> 0x0190 }
            java.lang.String r1 = "viewer_vote_index"
            com.facebook.messaging.model.messages.MontageFeedbackPoll r0 = r2.A01     // Catch:{ all -> 0x0190 }
            int r0 = r0.A00     // Catch:{ all -> 0x0190 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0190 }
            r9.put(r1, r0)     // Catch:{ all -> 0x0190 }
            java.lang.String r7 = "montage_message_poll"
            java.lang.String r1 = r10.A02()     // Catch:{ all -> 0x0190 }
            java.lang.String[] r0 = r10.A04()     // Catch:{ all -> 0x0190 }
            r3.update(r7, r9, r1, r0)     // Catch:{ all -> 0x0190 }
            java.lang.String r1 = r8.A02()     // Catch:{ all -> 0x0190 }
            java.lang.String[] r0 = r8.A04()     // Catch:{ all -> 0x0190 }
            r3.delete(r6, r1, r0)     // Catch:{ all -> 0x0190 }
            com.facebook.messaging.model.messages.MontageFeedbackPoll r0 = r2.A01     // Catch:{ all -> 0x0190 }
            com.google.common.collect.ImmutableList r0 = r0.A03     // Catch:{ all -> 0x0190 }
            X.1Xv r9 = r0.iterator()     // Catch:{ all -> 0x0190 }
        L_0x013e:
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x0190 }
            if (r0 == 0) goto L_0x00e1
            java.lang.Object r8 = r9.next()     // Catch:{ all -> 0x0190 }
            com.facebook.messaging.model.messages.MontageFeedbackPollOption r8 = (com.facebook.messaging.model.messages.MontageFeedbackPollOption) r8     // Catch:{ all -> 0x0190 }
            android.content.ContentValues r7 = new android.content.ContentValues     // Catch:{ all -> 0x0190 }
            r7.<init>()     // Catch:{ all -> 0x0190 }
            com.facebook.messaging.model.messages.MontageFeedbackPoll r0 = r2.A01     // Catch:{ all -> 0x0190 }
            java.lang.String r0 = r0.A04     // Catch:{ all -> 0x0190 }
            r7.put(r5, r0)     // Catch:{ all -> 0x0190 }
            java.lang.String r1 = "option_index"
            int r0 = r8.A01     // Catch:{ all -> 0x0190 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0190 }
            r7.put(r1, r0)     // Catch:{ all -> 0x0190 }
            java.lang.String r1 = "option_text"
            java.lang.String r0 = r8.A04     // Catch:{ all -> 0x0190 }
            r7.put(r1, r0)     // Catch:{ all -> 0x0190 }
            java.lang.String r1 = "vote_count"
            int r0 = r8.A03     // Catch:{ all -> 0x0190 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0190 }
            r7.put(r1, r0)     // Catch:{ all -> 0x0190 }
            r1 = 0
            r0 = -561136592(0xffffffffde8dbc30, float:-5.1065454E18)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x0190 }
            r3.insert(r6, r1, r7)     // Catch:{ all -> 0x0190 }
            r0 = 1922683611(0x7299d2db, float:6.093583E30)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x0190 }
            goto L_0x013e
        L_0x0184:
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x0190 }
            r0 = 1640322144(0x61c55460, float:4.5501106E20)
            X.C007406x.A02(r3, r0)
            com.facebook.fbservice.service.OperationResult r10 = com.facebook.fbservice.service.OperationResult.A00
            return r10
        L_0x0190:
            r1 = move-exception
            r0 = -708254878(0xffffffffd5c8e362, float:-2.76098659E13)
            X.C007406x.A02(r3, r0)
            throw r1
        L_0x0198:
            X.7JR r0 = new X.7JR
            r0.<init>(r13)
            throw r0
        L_0x019e:
            r0 = r12
            X.0lo r0 = (X.C11070lo) r0
            int r2 = X.AnonymousClass1Y3.A49
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1cz r0 = (X.C27311cz) r0
            com.facebook.fbservice.service.OperationResult r0 = r0.BAz(r13)
            return r0
        L_0x01b1:
            java.lang.AssertionError r0 = X.C000300h.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0r(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0s(C11060ln r8, C27311cz r9) {
        MontageThreadPreview montageThreadPreview;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r8);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r3 = (C11150lz) this;
                if (r3 instanceof C12370pE) {
                    C12370pE r32 = (C12370pE) r3;
                    UpdateMontagePreviewBlockModeParams updateMontagePreviewBlockModeParams = (UpdateMontagePreviewBlockModeParams) r8.A00.getParcelable("UpdateMontagePreviewBlockModeParams");
                    ThreadKey threadKey = updateMontagePreviewBlockModeParams.A00;
                    C42752Br r4 = updateMontagePreviewBlockModeParams.A01;
                    ThreadSummary threadSummary = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r32.A00)).A0F(threadKey, 0).A05;
                    if (threadSummary == null || (montageThreadPreview = threadSummary.A0X) == null) {
                        return OperationResult.A04(new UpdateMontagePreviewBlockModeResult(threadSummary));
                    }
                    AnonymousClass2VF r1 = new AnonymousClass2VF(montageThreadPreview);
                    r1.A02 = AnonymousClass2VF.A00(r4);
                    MontageThreadPreview montageThreadPreview2 = new MontageThreadPreview(r1);
                    long now = r32.A01.now();
                    C17920zh A002 = ThreadSummary.A00();
                    A002.A02(threadSummary);
                    A002.A0W = montageThreadPreview2;
                    ThreadSummary A003 = A002.A00();
                    C52232ij.A05((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r32.A00), A003, now, threadSummary);
                    return OperationResult.A04(new UpdateMontagePreviewBlockModeResult(A003));
                } else if (!(r3 instanceof C11130lx)) {
                    return r9.BAz(r8);
                } else {
                    C11130lx r33 = (C11130lx) r3;
                    OperationResult BAz = r9.BAz(r8);
                    UpdateMontagePreviewBlockModeResult updateMontagePreviewBlockModeResult = (UpdateMontagePreviewBlockModeResult) BAz.A08();
                    if (updateMontagePreviewBlockModeResult == null || updateMontagePreviewBlockModeResult.A00 == null) {
                        return BAz;
                    }
                    ((C14800u9) r33.A01.get()).A0E(updateMontagePreviewBlockModeResult.A00);
                    ((C189216c) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B7s, r33.A00)).A0G(updateMontagePreviewBlockModeResult.A00.A0S, "CacheServiceHandler.handleUpdateMontagePreviewBlockMode");
                    return BAz;
                }
            } else {
                throw new AnonymousClass7JR(r8);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public OperationResult A0t(C11060ln r10, C27311cz r11) {
        int i;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r10);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r5 = (C11150lz) this;
                if (r5 instanceof C12370pE) {
                    C12370pE r52 = (C12370pE) r5;
                    long j = r10.A00.getLong("offline_threading_id");
                    AnonymousClass105 r3 = (AnonymousClass105) r10.A00.getSerializable("state");
                    ThreadKey A02 = ThreadKey.A02(j);
                    ImmutableList copyOf = ImmutableList.copyOf((Collection) r10.A00.getParcelableArrayList("cant_message_users"));
                    SQLiteDatabase A06 = ((C25771aN) ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r52.A00)).A0F.get()).A06();
                    C007406x.A01(A06, 2124548983);
                    try {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("optimistic_group_state", Integer.valueOf(r3.dbValue));
                        if (r3 == AnonymousClass105.A02) {
                            contentValues.put("can_reply_to", (Boolean) false);
                        }
                        A06.update("threads", contentValues, AnonymousClass24B.$const$string(98), new String[]{A02.A0J()});
                        if (r3 == AnonymousClass105.A02 && C013509w.A01(copyOf)) {
                            ImmutableList.Builder builder = ImmutableList.builder();
                            C24971Xv it = copyOf.iterator();
                            while (it.hasNext()) {
                                builder.add((Object) ((User) it.next()).A0Q);
                            }
                            C25601a6 A012 = C06160ax.A01(C06160ax.A03("thread_key", A02.A0J()), C06160ax.A04("user_key", builder.build()));
                            ContentValues contentValues2 = new ContentValues();
                            contentValues2.put("can_viewer_message", (Boolean) false);
                            A06.update("thread_participants", contentValues2, A012.A02(), A012.A04());
                        }
                        A06.setTransactionSuccessful();
                        i = 1247203839;
                    } catch (SQLException e) {
                        C52232ij.A09(e);
                        i = 751498425;
                    } catch (Throwable th) {
                        C007406x.A02(A06, 1249287207);
                        throw th;
                    }
                    C007406x.A02(A06, i);
                    return OperationResult.A04(((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, r52.A00)).A0F(A02, 0));
                } else if (!(r5 instanceof C11130lx)) {
                    return r11.BAz(r10);
                } else {
                    C11130lx r53 = (C11130lx) r5;
                    boolean A013 = C11130lx.A01(r53);
                    try {
                        OperationResult BAz = r11.BAz(r10);
                        FetchThreadResult fetchThreadResult = (FetchThreadResult) BAz.A08();
                        if (!(fetchThreadResult == null || fetchThreadResult.A05 == null)) {
                            ((C14800u9) r53.A01.get()).A0E(fetchThreadResult.A05);
                            ((C189216c) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B7s, r53.A00)).A0G(fetchThreadResult.A05.A0S, "CacheServiceHandler.handleUpdateOptimisticGroupThreadState");
                        }
                    } finally {
                        if (A013) {
                            ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r53.A00)).A03();
                        }
                    }
                }
            } else {
                throw new AnonymousClass7JR(r10);
            }
        }
    }

    public OperationResult A0u(C11060ln r7, C27311cz r8) {
        User A03;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r5 = (C11150lz) this;
                if (!(r5 instanceof C12370pE)) {
                    boolean z = r5 instanceof C11130lx;
                    return r8.BAz(r7);
                }
                C12370pE r52 = (C12370pE) r5;
                UpdateProfilePicUriWithFilePathParams updateProfilePicUriWithFilePathParams = (UpdateProfilePicUriWithFilePathParams) r7.A00.getParcelable("updateProfilePicUriWithFilePathParams");
                if (!(updateProfilePicUriWithFilePathParams == null || (A03 = ((C14300t0) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AxE, r52.A00)).A03(updateProfilePicUriWithFilePathParams.A00)) == null)) {
                    C07220cv r1 = new C07220cv();
                    r1.A04(A03);
                    r1.A0Q = updateProfilePicUriWithFilePathParams.A01;
                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r52.A00)).A05.A01(ImmutableList.of(r1.A02()));
                }
                return OperationResult.A00;
            }
            throw new AnonymousClass7JR(r7);
        }
    }

    public OperationResult A0v(C11060ln r7, C27311cz r8) {
        NotificationSetting notificationSetting;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (this instanceof C11070lo) {
            return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, ((C11070lo) this).A00)).BAz(r7);
        } else {
            if (!(this instanceof C12380pF)) {
                C11150lz r3 = (C11150lz) this;
                if (!(r3 instanceof C12370pE)) {
                    return r8.BAz(r7);
                }
                C12370pE r32 = (C12370pE) r3;
                OperationResult BAz = r8.BAz(r7);
                if (!BAz.success || (notificationSetting = ((SetSettingsParams) r7.A00.getParcelable("setSettingsParams")).A00) == null) {
                    return BAz;
                }
                C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B6q, r32.A00)).edit();
                edit.BzA(C05690aA.A1b, notificationSetting.A01());
                edit.commit();
                return BAz;
            }
            C12380pF r5 = (C12380pF) this;
            int i = AnonymousClass1Y3.BAS;
            AnonymousClass0UN r33 = r5.A00;
            ((C64393Bj) AnonymousClass1XX.A02(2, i, r33)).A03((AnonymousClass56O) AnonymousClass1XX.A02(19, AnonymousClass1Y3.BFR, r33), (SetSettingsParams) r7.A00.getParcelable("setSettingsParams"), C12380pF.A00(r5));
            return OperationResult.A00;
        }
    }

    public OperationResult A0w(C11060ln r10, C27311cz r11) {
        boolean z;
        int i;
        int i2;
        ThreadSummary threadSummary;
        if (this instanceof C10840kw) {
            throw C000300h.A00();
        } else if (!(this instanceof C11070lo)) {
            C11150lz r3 = (C11150lz) this;
            if (r3 instanceof C12440pM) {
                C12440pM r32 = (C12440pM) r3;
                C005505z.A03("SmsServiceHandler.handleCreateGroup", -1962482707);
                try {
                    ImmutableList A05 = ((CreateCustomizableGroupParams) r10.A00.getParcelable("CreateCustomizableGroupParams")).A05();
                    HashSet hashSet = new HashSet(A05.size());
                    C24971Xv it = A05.iterator();
                    while (it.hasNext()) {
                        hashSet.add(((User) it.next()).A0P.getId());
                    }
                    return OperationResult.A04(C12440pM.A01(r32, AnonymousClass7I7.A00(r32.A00, hashSet), 20));
                } finally {
                    C005505z.A00(1959612629);
                }
            } else if (r3 instanceof C12370pE) {
                C12370pE r33 = (C12370pE) r3;
                ((C60222wn) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B4i, r33.A00)).A04();
                OperationResult BAz = r11.BAz(r10);
                ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r33.A00)).A0W((FetchThreadResult) BAz.A07(), ThreadKey.A02(((CreateCustomizableGroupParams) r10.A00.getParcelable("CreateCustomizableGroupParams")).A00));
                return BAz;
            } else if (!(r3 instanceof C11130lx)) {
                return r11.BAz(r10);
            } else {
                C11130lx r34 = (C11130lx) r3;
                boolean A012 = C11130lx.A01(r34);
                try {
                    OperationResult BAz2 = r11.BAz(r10);
                    FetchThreadResult fetchThreadResult = (FetchThreadResult) BAz2.A08();
                    if (!(fetchThreadResult == null || (threadSummary = fetchThreadResult.A05) == null || ThreadKey.A0E(threadSummary.A0S))) {
                        C14800u9 r5 = (C14800u9) r34.A01.get();
                        r5.A06(C10950l8.A05, ThreadKey.A02(((CreateCustomizableGroupParams) r10.A00.getParcelable("CreateCustomizableGroupParams")).A00));
                        r5.A0G(fetchThreadResult);
                    }
                } finally {
                    if (A012) {
                        ((C12400pH) AnonymousClass1XX.A02(12, AnonymousClass1Y3.BBF, r34.A00)).A03();
                    }
                }
            }
        } else {
            C11070lo r35 = (C11070lo) this;
            ImmutableList A052 = ((CreateCustomizableGroupParams) AnonymousClass0ls.A00(r10, "CreateCustomizableGroupParams")).A05();
            if (A052 != null && !A052.isEmpty()) {
                C24971Xv it2 = A052.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (!(((User) it2.next()).A0P instanceof UserSmsIdentifier)) {
                            break;
                        }
                    } else {
                        z = true;
                        break;
                    }
                }
            } else {
                z = false;
            }
            if (z) {
                i = 1;
                i2 = AnonymousClass1Y3.BQB;
            } else {
                i = 0;
                i2 = AnonymousClass1Y3.A49;
            }
            return ((C27311cz) AnonymousClass1XX.A02(i, i2, r35.A00)).BAz(r10);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01bf, code lost:
        if (r5 == X.C10950l8.A0C) goto L_0x01c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x024e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x024f, code lost:
        if (r3 != null) goto L_0x0251;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:74:0x0254 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0x(X.C11060ln r15, X.C27311cz r16) {
        /*
            r14 = this;
            boolean r0 = r14 instanceof X.C10840kw
            if (r0 != 0) goto L_0x02f5
            boolean r0 = r14 instanceof X.C11070lo
            if (r0 != 0) goto L_0x02c4
            boolean r0 = r14 instanceof X.C12380pF
            if (r0 != 0) goto L_0x029d
            r5 = r14
            X.0lz r5 = (X.C11150lz) r5
            boolean r0 = r5 instanceof X.C27561dO
            r7 = r16
            if (r0 != 0) goto L_0x0022
            boolean r0 = r5 instanceof X.C12440pM
            if (r0 != 0) goto L_0x017e
            boolean r0 = r5 instanceof X.C11130lx
            if (r0 != 0) goto L_0x0066
            com.facebook.fbservice.service.OperationResult r8 = r7.BAz(r15)
        L_0x0021:
            return r8
        L_0x0022:
            X.1dO r5 = (X.C27561dO) r5
            android.os.Bundle r1 = r15.A00
            java.lang.String r0 = "fetchThreadListParams"
            android.os.Parcelable r6 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchThreadListParams r6 = (com.facebook.messaging.service.model.FetchThreadListParams) r6
            com.google.common.collect.ImmutableSet r1 = r6.A05
            r1.toString()
            com.facebook.fbservice.service.OperationResult r8 = r7.BAz(r15)
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0021
            java.lang.Object r4 = r8.A07()
            com.facebook.messaging.service.model.FetchThreadListResult r4 = (com.facebook.messaging.service.model.FetchThreadListResult) r4
            com.facebook.messaging.model.threads.ThreadsCollection r3 = r4.A06
            if (r3 == 0) goto L_0x0299
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            X.1Xv r1 = r1.iterator()
        L_0x0050:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x025d
            java.lang.Object r0 = r1.next()
            X.2e3 r0 = (X.C50452e3) r0
            com.facebook.messaging.model.threads.ThreadSummary r0 = X.C27561dO.A01(r5, r0, r7)
            if (r0 == 0) goto L_0x0050
            r2.add(r0)
            goto L_0x0050
        L_0x0066:
            X.0lx r5 = (X.C11130lx) r5
            android.os.Bundle r10 = r15.A00
            java.lang.String r9 = "fetchThreadListParams"
            android.os.Parcelable r8 = r10.getParcelable(r9)
            com.facebook.messaging.service.model.FetchThreadListParams r8 = (com.facebook.messaging.service.model.FetchThreadListParams) r8
            X.0l8 r6 = r8.A03
            int r1 = X.AnonymousClass1Y3.AwM
            X.0UN r0 = r5.A00
            r4 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0pN r3 = (X.C12450pN) r3
            X.0pV r2 = X.C12500pT.A03
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "fetchThreadList (CSH) (folder="
            r1.<init>(r0)
            r1.append(r6)
            java.lang.String r0 = ")"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r3.A03(r2, r0)
            X.0hU r0 = r8.A01
            r0.toString()
            int r1 = X.AnonymousClass1Y3.B9x
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            X.0fv r2 = (X.C08770fv) r2
            java.lang.String r1 = "fetch_thread_list_operation_begin:"
            X.0m8 r0 = r5.A02
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.A0G(r0)
            X.0mQ r1 = r5.A04
            r0 = r8
            int r11 = X.AnonymousClass1Y3.BDk
            X.0UN r3 = r1.A00
            r1 = 1
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r1, r11, r3)
            X.0pX r11 = (X.C12540pX) r11
            X.0l8 r3 = r8.A03
            X.0hU r1 = r8.A01
            X.0pY r1 = r11.A01(r3, r1)
            X.0hU r3 = r1.A00
            X.0hU r1 = r8.A01
            if (r3 == r1) goto L_0x00e4
            X.0kg r1 = new X.0kg
            r1.<init>()
            r1.A00(r8)
            r1.A02 = r3
            com.facebook.http.interfaces.RequestPriority r0 = r8.A02
            r1.A03 = r0
            com.facebook.messaging.service.model.FetchThreadListParams r0 = new com.facebook.messaging.service.model.FetchThreadListParams
            r0.<init>(r1)
        L_0x00e4:
            if (r8 == r0) goto L_0x00ea
            r10.putParcelable(r9, r0)
            r8 = r0
        L_0x00ea:
            X.0hU r3 = r8.A01
            X.0mQ r1 = r5.A04
            int r0 = r8.A00
            boolean r0 = r1.A05(r6, r3, r0)
            if (r0 == 0) goto L_0x011b
            X.0mQ r0 = r5.A04
            com.facebook.messaging.service.model.FetchThreadListResult r0 = r0.A04(r6)
            com.facebook.fbservice.service.OperationResult r8 = com.facebook.fbservice.service.OperationResult.A04(r0)
            android.os.Bundle r3 = r8.resultDataBundle
            if (r3 == 0) goto L_0x010b
            java.lang.String r1 = "source"
            java.lang.String r0 = "cache"
            r3.putString(r1, r0)
        L_0x010b:
            java.lang.String r1 = "fetch_thread_list_operation_end:"
            X.0m8 r0 = r5.A02
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.A0G(r0)
            return r8
        L_0x011b:
            boolean r9 = X.C11130lx.A01(r5)
            r3 = 12
            com.facebook.fbservice.service.OperationResult r8 = r7.BAz(r15)     // Catch:{ all -> 0x016d }
            java.lang.Object r6 = r8.A08()     // Catch:{ all -> 0x016d }
            com.facebook.messaging.service.model.FetchThreadListResult r6 = (com.facebook.messaging.service.model.FetchThreadListResult) r6     // Catch:{ all -> 0x016d }
            com.facebook.fbservice.results.DataFetchDisposition r0 = r6.A02     // Catch:{ all -> 0x016d }
            com.facebook.common.util.TriState r0 = r0.A05     // Catch:{ all -> 0x016d }
            boolean r0 = r0.asBoolean(r4)     // Catch:{ all -> 0x016d }
            if (r0 != 0) goto L_0x015d
            X.0US r0 = r5.A01     // Catch:{ all -> 0x016d }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x016d }
            X.0u9 r0 = (X.C14800u9) r0     // Catch:{ all -> 0x016d }
            r0.A0F(r6)     // Catch:{ all -> 0x016d }
            r4 = 3
            int r1 = X.AnonymousClass1Y3.B7s     // Catch:{ all -> 0x016d }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x016d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x016d }
            X.16c r1 = (X.C189216c) r1     // Catch:{ all -> 0x016d }
            java.lang.String r0 = "CacheServiceHandler.handleFetchThreadList"
            r1.A0O(r0)     // Catch:{ all -> 0x016d }
            X.0Tq r0 = r5.A06     // Catch:{ all -> 0x016d }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x016d }
            X.16u r1 = (X.C191016u) r1     // Catch:{ all -> 0x016d }
            com.facebook.messaging.model.folders.FolderCounts r0 = r6.A03     // Catch:{ all -> 0x016d }
            r1.BMC(r0)     // Catch:{ all -> 0x016d }
        L_0x015d:
            if (r9 == 0) goto L_0x010b
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
            goto L_0x010b
        L_0x016d:
            r2 = move-exception
            if (r9 == 0) goto L_0x017d
            int r1 = X.AnonymousClass1Y3.BBF
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pH r0 = (X.C12400pH) r0
            r0.A03()
        L_0x017d:
            throw r2
        L_0x017e:
            X.0pM r5 = (X.C12440pM) r5
            java.lang.String r1 = "SmsServiceHandler.handleFetchThreadList"
            r0 = -1992304430(0xffffffff893fd8d2, float:-2.3092737E-33)
            X.C005505z.A03(r1, r0)
            android.os.Bundle r1 = r15.A00     // Catch:{ all -> 0x0255 }
            java.lang.String r0 = "fetchThreadListParams"
            android.os.Parcelable r4 = r1.getParcelable(r0)     // Catch:{ all -> 0x0255 }
            com.facebook.messaging.service.model.FetchThreadListParams r4 = (com.facebook.messaging.service.model.FetchThreadListParams) r4     // Catch:{ all -> 0x0255 }
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AKa     // Catch:{ all -> 0x0255 }
            X.0UN r0 = r5.A01     // Catch:{ all -> 0x0255 }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0255 }
            X.2e4 r8 = (X.C50462e4) r8     // Catch:{ all -> 0x0255 }
            int r2 = X.AnonymousClass1Y3.B8R     // Catch:{ all -> 0x0255 }
            X.0UN r1 = r8.A00     // Catch:{ all -> 0x0255 }
            r0 = 18
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x0255 }
            X.2e8 r1 = (X.C50502e8) r1     // Catch:{ all -> 0x0255 }
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r1.A02     // Catch:{ all -> 0x0255 }
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()     // Catch:{ all -> 0x0255 }
            r0.lock()     // Catch:{ all -> 0x0255 }
            X.2eA r3 = r1.A00     // Catch:{ all -> 0x0255 }
            X.0l8 r5 = r4.A03     // Catch:{ all -> 0x024c }
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ all -> 0x024c }
            r7 = 0
            r2 = 1
            if (r5 == r0) goto L_0x01c1
            X.0l8 r0 = X.C10950l8.A0C     // Catch:{ all -> 0x024c }
            r1 = 0
            if (r5 != r0) goto L_0x01c2
        L_0x01c1:
            r1 = 1
        L_0x01c2:
            java.lang.String r0 = "SMS Threads can only belong to inbox or business folder"
            com.google.common.base.Preconditions.checkArgument(r1, r0)     // Catch:{ all -> 0x024c }
            java.util.HashMap r11 = new java.util.HashMap     // Catch:{ all -> 0x024c }
            r11.<init>()     // Catch:{ all -> 0x024c }
            X.0l8 r9 = r4.A03     // Catch:{ all -> 0x024c }
            int r10 = r4.A01()     // Catch:{ all -> 0x024c }
            r12 = -1
            java.util.List r0 = r8.A0E(r9, r10, r11, r12)     // Catch:{ all -> 0x024c }
            com.facebook.messaging.model.threads.ThreadsCollection r6 = new com.facebook.messaging.model.threads.ThreadsCollection     // Catch:{ all -> 0x024c }
            com.google.common.collect.ImmutableList r5 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x024c }
            int r1 = r0.size()     // Catch:{ all -> 0x024c }
            int r0 = r4.A01()     // Catch:{ all -> 0x024c }
            if (r1 >= r0) goto L_0x01e9
            r7 = 1
        L_0x01e9:
            r6.<init>(r5, r7)     // Catch:{ all -> 0x024c }
            X.161 r5 = new X.161     // Catch:{ all -> 0x024c }
            r5.<init>()     // Catch:{ all -> 0x024c }
            r5.A06 = r6     // Catch:{ all -> 0x024c }
            com.facebook.fbservice.results.DataFetchDisposition r0 = com.facebook.fbservice.results.DataFetchDisposition.A0H     // Catch:{ all -> 0x024c }
            r5.A02 = r0     // Catch:{ all -> 0x024c }
            r6 = 4
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x024c }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x024c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)     // Catch:{ all -> 0x024c }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x024c }
            long r0 = r0.now()     // Catch:{ all -> 0x024c }
            r5.A00 = r0     // Catch:{ all -> 0x024c }
            X.0l8 r0 = r4.A03     // Catch:{ all -> 0x024c }
            r5.A04 = r0     // Catch:{ all -> 0x024c }
            java.util.Collection r0 = r11.values()     // Catch:{ all -> 0x024c }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x024c }
            r5.A09 = r0     // Catch:{ all -> 0x024c }
            com.facebook.messaging.service.model.FetchThreadListResult r4 = new com.facebook.messaging.service.model.FetchThreadListResult     // Catch:{ all -> 0x024c }
            r4.<init>(r5)     // Catch:{ all -> 0x024c }
            int r1 = X.AnonymousClass1Y3.BOk     // Catch:{ all -> 0x024c }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x024c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x024c }
            X.0l9 r0 = (X.C10960l9) r0     // Catch:{ all -> 0x024c }
            boolean r0 = r0.A08()     // Catch:{ all -> 0x024c }
            if (r0 == 0) goto L_0x023c
            r2 = 10
            int r1 = X.AnonymousClass1Y3.A9n     // Catch:{ all -> 0x024c }
            X.0UN r0 = r8.A00     // Catch:{ all -> 0x024c }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x024c }
            X.0ia r1 = (X.AnonymousClass0ia) r1     // Catch:{ all -> 0x024c }
            java.lang.Runnable r0 = r8.A02     // Catch:{ all -> 0x024c }
            r1.A02(r0)     // Catch:{ all -> 0x024c }
        L_0x023c:
            if (r3 == 0) goto L_0x0241
            r3.close()     // Catch:{ all -> 0x0255 }
        L_0x0241:
            com.facebook.fbservice.service.OperationResult r8 = com.facebook.fbservice.service.OperationResult.A04(r4)     // Catch:{ all -> 0x0255 }
            r0 = -946186412(0xffffffffc79a5754, float:-79022.66)
            X.C005505z.A00(r0)
            return r8
        L_0x024c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x024e }
        L_0x024e:
            r0 = move-exception
            if (r3 == 0) goto L_0x0254
            r3.close()     // Catch:{ all -> 0x0254 }
        L_0x0254:
            throw r0     // Catch:{ all -> 0x0255 }
        L_0x0255:
            r1 = move-exception
            r0 = 1050253808(0x3e9999f0, float:0.30000257)
            X.C005505z.A00(r0)
            throw r1
        L_0x025d:
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0299
            int r0 = r6.A01()
            com.facebook.messaging.model.threads.ThreadsCollection r0 = X.C27561dO.A02(r2, r3, r0)
            X.161 r3 = new X.161
            r3.<init>()
            r3.A06 = r0
            com.facebook.fbservice.results.DataFetchDisposition r0 = com.facebook.fbservice.results.DataFetchDisposition.A0H
            r3.A02 = r0
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AnC
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1mI r0 = (X.C32761mI) r0
            long r0 = r0.now()
            r3.A00 = r0
            X.0l8 r0 = r4.A04
            r3.A04 = r0
            com.google.common.collect.ImmutableList r0 = r4.A09
            r3.A09 = r0
            com.facebook.messaging.service.model.FetchThreadListResult r0 = new com.facebook.messaging.service.model.FetchThreadListResult
            r0.<init>(r3)
            com.facebook.fbservice.service.OperationResult r8 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r8
        L_0x0299:
            X.C27561dO.A04(r5)
            return r8
        L_0x029d:
            r4 = r14
            X.0pF r4 = (X.C12380pF) r4
            android.os.Bundle r1 = r15.A00
            java.lang.String r0 = "fetchThreadListParams"
            android.os.Parcelable r3 = r1.getParcelable(r0)
            com.facebook.messaging.service.model.FetchThreadListParams r3 = (com.facebook.messaging.service.model.FetchThreadListParams) r3
            int r2 = X.AnonymousClass1Y3.ARS
            X.0UN r1 = r4.A00
            r0 = 24
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.7PV r1 = (X.AnonymousClass7PV) r1
            com.facebook.common.callercontext.CallerContext r0 = r15.A01
            com.facebook.messaging.service.model.FetchThreadListResult r0 = r1.A0G(r3, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r0)
            X.C180878aL.A00(r0)
            return r0
        L_0x02c4:
            r5 = r14
            X.0lo r5 = (X.C11070lo) r5
            int r1 = X.AnonymousClass1Y3.B9x
            X.0UN r0 = r5.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r1, r0)
            X.0fv r4 = (X.C08770fv) r4
            java.lang.String r0 = "fetch_thread_list_operation_begin:multicache"
            r4.A0G(r0)
            java.lang.String r0 = "fetchThreadListParams"
            android.os.Parcelable r3 = X.AnonymousClass0ls.A00(r15, r0)
            com.facebook.messaging.service.model.FetchThreadListParams r3 = (com.facebook.messaging.service.model.FetchThreadListParams) r3
            X.0l8 r2 = r3.A03
            X.0ki r0 = r3.A04
            X.0lu r1 = X.C11070lo.A02(r5, r2, r0, r15)
            X.0pK r0 = new X.0pK
            r0.<init>(r3, r2)
            com.facebook.fbservice.service.OperationResult r1 = X.C11070lo.A01(r1, r0)
            java.lang.String r0 = "fetch_thread_list_operation_end:multicache"
            r4.A0G(r0)
            return r1
        L_0x02f5:
            r3 = r14
            X.0kw r3 = (X.C10840kw) r3
            X.0Tq r0 = r3.A01
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x037f
            r2 = 5
            int r1 = X.AnonymousClass1Y3.Ah2
            X.0UN r0 = r3.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1hq r5 = (X.C30311hq) r5
            java.lang.String r0 = "fetchThreadListParams"
            android.os.Parcelable r6 = X.AnonymousClass0ls.A00(r15, r0)
            com.facebook.messaging.service.model.FetchThreadListParams r6 = (com.facebook.messaging.service.model.FetchThreadListParams) r6
            X.0l8 r4 = r6.A03
            X.0l8 r0 = X.C10950l8.A05
            r3 = 1
            r2 = 0
            r1 = 0
            if (r4 != r0) goto L_0x031d
            r1 = 1
        L_0x031d:
            java.lang.String r0 = "Tincan messages can only be in the INBOX"
            com.google.common.base.Preconditions.checkArgument(r1, r0)
            int r1 = X.AnonymousClass1Y3.Abq
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pJ r0 = (X.C12420pJ) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x037f
            int r1 = X.AnonymousClass1Y3.Any
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1hr r0 = (X.C30321hr) r0
            X.1i2 r5 = r0.A02()
            int r3 = r6.A01()
            r2 = 0
            r0 = -1
            android.util.Pair r4 = r5.A02(r2, r0, r3)
            X.161 r3 = new X.161
            r3.<init>()
            com.facebook.fbservice.results.DataFetchDisposition r0 = com.facebook.fbservice.results.DataFetchDisposition.A0F
            r3.A02 = r0
            X.0l8 r0 = X.C10950l8.A05
            r3.A04 = r0
            java.lang.Object r0 = r4.first
            com.facebook.messaging.model.threads.ThreadsCollection r0 = (com.facebook.messaging.model.threads.ThreadsCollection) r0
            r3.A06 = r0
            int r2 = X.AnonymousClass1Y3.AYB
            X.0UN r1 = r5.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.06A r0 = (X.AnonymousClass06A) r0
            long r0 = r0.now()
            r3.A00 = r0
            java.lang.Object r0 = r4.second
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0
            r3.A09 = r0
            com.facebook.messaging.service.model.FetchThreadListResult r0 = new com.facebook.messaging.service.model.FetchThreadListResult
            r0.<init>(r3)
        L_0x037a:
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r0)
            return r0
        L_0x037f:
            X.0l8 r0 = X.C10950l8.A05
            com.facebook.messaging.service.model.FetchThreadListResult r0 = com.facebook.messaging.service.model.FetchThreadListResult.A00(r0)
            goto L_0x037a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.A0x(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0y(C11060ln r14, C27311cz r15) {
        ArrayList arrayList;
        if (this instanceof C10840kw) {
            C10840kw r4 = (C10840kw) this;
            if (r4.A01.get() == null) {
                arrayList = C04300To.A04(FetchThreadResult.A09);
            } else {
                FetchThreadParams fetchThreadParams = (FetchThreadParams) AnonymousClass0ls.A00(r14, "fetchThreadParams");
                ImmutableSet immutableSet = fetchThreadParams.A04.A00;
                Preconditions.checkNotNull(immutableSet);
                arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                C30311hq r6 = (C30311hq) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ah2, r4.A00);
                AnonymousClass04b r5 = new AnonymousClass04b();
                if (((C12420pJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Abq, r6.A00)).A02()) {
                    C24971Xv it = fetchThreadParams.A04.A00.iterator();
                    while (it.hasNext()) {
                        ThreadKey threadKey = (ThreadKey) it.next();
                        FetchThreadResult A012 = ((C30321hr) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Any, r6.A00)).A02().A01(threadKey, fetchThreadParams.A01);
                        if (A012 != FetchThreadResult.A09) {
                            r5.put(threadKey, A012);
                        }
                    }
                }
                C24971Xv it2 = immutableSet.iterator();
                while (it2.hasNext()) {
                    ThreadKey threadKey2 = (ThreadKey) it2.next();
                    FetchThreadResult fetchThreadResult = (FetchThreadResult) r5.get(threadKey2);
                    if (fetchThreadResult == null || !fetchThreadResult.A02()) {
                        arrayList2.add(threadKey2);
                    } else {
                        arrayList.add(fetchThreadResult);
                    }
                }
                Iterator it3 = arrayList2.iterator();
                while (it3.hasNext()) {
                    ThreadKey threadKey3 = (ThreadKey) it3.next();
                    UserKey A013 = UserKey.A01(String.valueOf(threadKey3.A01));
                    User A03 = ((C14300t0) AnonymousClass1XX.A02(10, AnonymousClass1Y3.AxE, r4.A00)).A03(A013);
                    if (A03 == null) {
                        A03 = ((C28011e7) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amz, r4.A00)).A03(A013);
                    }
                    if (A03 != null) {
                        User user = (User) r4.A01.get();
                        Preconditions.checkNotNull(user);
                        if (threadKey3.A05 == C28711fF.TINCAN) {
                            boolean z = false;
                            if (Long.parseLong(user.A0j) == threadKey3.A04) {
                                z = true;
                            }
                            Preconditions.checkState(z);
                        }
                        ImmutableList of = ImmutableList.of(A03, r4.A01.get());
                        C61222yX A002 = FetchThreadResult.A00();
                        A002.A05 = C52192if.TINCAN;
                        A002.A01 = DataFetchDisposition.A0E;
                        A002.A06 = of;
                        A002.A00 = AnonymousClass06A.A00.now();
                        arrayList.add(A002.A00());
                    }
                }
                if (arrayList.isEmpty()) {
                    return OperationResult.A00(C14880uI.A0E);
                }
            }
            return OperationResult.A05(arrayList);
        } else if (!(this instanceof C11070lo)) {
            C12380pF r42 = (C12380pF) this;
            FetchThreadParams fetchThreadParams2 = (FetchThreadParams) r14.A00.getParcelable("fetchThreadParams");
            ImmutableSet<ThreadKey> immutableSet2 = fetchThreadParams2.A04.A00;
            C08770fv r3 = (C08770fv) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B9x, r42.A00);
            for (ThreadKey hashCode : immutableSet2) {
                C08770fv.A04(r3, hashCode.hashCode(), "network_thread");
            }
            AnonymousClass7PV r32 = (AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, r42.A00);
            CallerContext callerContext = r14.A01;
            ImmutableSet immutableSet3 = fetchThreadParams2.A04.A00;
            ((C157337Pc) AnonymousClass1XX.A02(11, AnonymousClass1Y3.B8c, r32.A00)).A05("Received FetchThreadParams without thread keys.", immutableSet3);
            ThreadKey threadKey4 = immutableSet3.isEmpty() ? null : (ThreadKey) immutableSet3.iterator().next();
            ArrayList<FetchThreadResult> A0L = (threadKey4 == null || (!ThreadKey.A09(threadKey4) && !ThreadKey.A0B(threadKey4))) ? r32.A0L(fetchThreadParams2, callerContext) : (ArrayList) AnonymousClass7PV.A07(r32, immutableSet3, new AnonymousClass51B(r32, fetchThreadParams2, callerContext), C99084oO.$const$string(AnonymousClass1Y3.A4A));
            AnonymousClass7ZV r33 = (AnonymousClass7ZV) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ALO, r42.A00);
            for (FetchThreadResult A04 : A0L) {
                r33.A04(A04);
            }
            OperationResult A05 = OperationResult.A05(A0L);
            C180878aL.A00(A05);
            return A05;
        } else {
            C11070lo r8 = (C11070lo) this;
            FetchThreadParams fetchThreadParams3 = (FetchThreadParams) AnonymousClass0ls.A00(r14, "fetchThreadParams");
            ImmutableSet<ThreadKey> immutableSet4 = fetchThreadParams3.A04.A00;
            AnonymousClass04b r52 = new AnonymousClass04b();
            for (ThreadKey threadKey5 : immutableSet4) {
                C11120lv A032 = C11070lo.A03(threadKey5);
                Object obj = (Set) r52.get(A032);
                if (obj == null) {
                    obj = new HashSet();
                    r52.put(A032, obj);
                }
                obj.add(threadKey5);
            }
            ArrayList arrayList3 = new ArrayList();
            int size = r52.size();
            for (int i = 0; i < size; i++) {
                C207529qM r10 = new C207529qM();
                r10.A01(r14);
                C60292wu r11 = new C60292wu();
                r11.A00(fetchThreadParams3);
                r11.A03 = new ThreadCriteria(fetchThreadParams3.A04.A01, (Set) r52.A09(i));
                r10.A00.putParcelable("fetchThreadParams", new FetchThreadParams(r11));
                ArrayList A0A = C11070lo.A00(r8, (C11120lv) r52.A07(i)).BAz(r10.A00()).A0A();
                if (A0A != null) {
                    arrayList3.addAll(A0A);
                }
            }
            return OperationResult.A05(arrayList3);
        }
    }

    public abstract OperationResult A0z(C11060ln r1, C27311cz r2);

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x014a, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0160, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0176, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x018c, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x01a2, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x01b8, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x01ce, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x01e4, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x01fa, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0210, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0226, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x023c, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0252, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0268, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0042, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x027e, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0294, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x02aa, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x02c0, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x02d6, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x02ec, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x0302, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x0318, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x032e, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x0344, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0058, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x035a, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x0370, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x0386, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:292:0x039c, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x03b2, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x03c8, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x03de, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:320:0x03f4, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:327:0x040a, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:334:0x0420, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006e, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:341:0x0436, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x044c, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:355:0x0462, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:362:0x0477, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:0x048c, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:376:0x04a1, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:383:0x04b6, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:390:0x04cb, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:394:0x04d8, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0084, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x009a, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00b0, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c6, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00dc, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00f2, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0108, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x011e, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0134, code lost:
        if (r7 != false) goto L_0x04da;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult BB0(X.C11060ln r9, X.C27311cz r10) {
        /*
            r8 = this;
            java.lang.String r5 = "Web."
            java.lang.String r4 = r9.A05
            java.lang.String r2 = r8.A01
            java.lang.String r1 = "%s_handleOperation_%s"
            r0 = -788200506(0xffffffffd10503c6, float:-3.5705872E10)
            X.C005505z.A06(r1, r2, r4, r0)
            r1 = 10
            r7 = 0
            X.0pH r3 = r8.A00     // Catch:{ all -> 0x04ee }
            if (r3 == 0) goto L_0x0030
            boolean r0 = r8 instanceof X.C12380pF     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0030
            java.util.concurrent.locks.ReentrantLock r0 = r3.A00     // Catch:{ all -> 0x04ee }
            boolean r0 = r0.isHeldByCurrentThread()     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0030
            java.lang.String r6 = "MessagingLock"
            java.util.concurrent.locks.ReentrantLock r0 = r3.A00     // Catch:{ IllegalMonitorStateException -> 0x002a }
            r0.unlock()     // Catch:{ IllegalMonitorStateException -> 0x002a }
            r7 = 1
            goto L_0x0030
        L_0x002a:
            r3 = move-exception
            java.lang.String r0 = "Failed to unlock"
            X.C010708t.A0L(r6, r0, r3)     // Catch:{ all -> 0x04ee }
        L_0x0030:
            java.lang.String r0 = "fetch_thread_list"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0046
            com.facebook.fbservice.service.OperationResult r3 = r8.A0x(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 670391235(0x27f55bc3, float:6.810065E-15)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0046:
            java.lang.String r0 = "fetch_more_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x005c
            com.facebook.fbservice.service.OperationResult r3 = r8.A0R(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1606344992(0x5fbee120, float:2.750862E19)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x005c:
            java.lang.String r0 = "fetch_thread"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0072
            com.facebook.fbservice.service.OperationResult r3 = r8.A0V(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -125261649(0xfffffffff888a8af, float:-2.2174178E34)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0072:
            java.lang.String r0 = "fetch_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0088
            com.facebook.fbservice.service.OperationResult r3 = r8.A0y(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 525262151(0x1f4edd47, float:4.3805233E-20)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0088:
            java.lang.String r0 = "fetch_threads_metadata"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x009e
            com.facebook.fbservice.service.OperationResult r3 = r8.A0X(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -102759756(0xfffffffff9e002b4, float:-1.4539117E35)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x009e:
            java.lang.String r0 = "fetch_thread_by_participants"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x00b4
            com.facebook.fbservice.service.OperationResult r3 = r8.A0W(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1598095765(0x5f410195, float:1.3907561E19)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x00b4:
            java.lang.String r0 = "add_members"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x00ca
            com.facebook.fbservice.service.OperationResult r3 = r8.A0A(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1846013328(0xffffffff91f81270, float:-3.9138864E-28)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x00ca:
            java.lang.String r0 = "create_thread"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x00e0
            com.facebook.fbservice.service.OperationResult r3 = r8.A0E(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -206099852(0xfffffffff3b72a74, float:-2.9023785E31)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x00e0:
            java.lang.String r0 = "create_group"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x00f6
            com.facebook.fbservice.service.OperationResult r3 = r8.A0w(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -874417816(0xffffffffcbe17168, float:-2.9549264E7)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x00f6:
            java.lang.String r0 = "create_optimistic_group_thread"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x010c
            com.facebook.fbservice.service.OperationResult r3 = r8.A0D(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1529519622(0x5b2a9e06, float:4.8024495E16)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x010c:
            java.lang.String r0 = "update_optimistic_group_thread_state"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0122
            com.facebook.fbservice.service.OperationResult r3 = r8.A0t(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -2092010992(0xffffffff834e7210, float:-6.0668896E-37)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0122:
            java.lang.String r0 = "fetch_more_messages"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0138
            com.facebook.fbservice.service.OperationResult r3 = r8.A0P(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -2038193837(0xffffffff8683a153, float:-4.951377E-35)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0138:
            java.lang.String r0 = "fetch_more_recent_messages"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x014e
            com.facebook.fbservice.service.OperationResult r3 = r8.A0Q(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1529616166(0xffffffffa4d3e8da, float:-9.190113E-17)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x014e:
            java.lang.String r0 = "fetch_messages_context"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0164
            com.facebook.fbservice.service.OperationResult r3 = r8.A0N(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1736783666(0xffffffff987ac8ce, float:-3.2413123E-24)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0164:
            java.lang.String r0 = "remove_member"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x017a
            com.facebook.fbservice.service.OperationResult r3 = r8.A0l(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -20087907(0xfffffffffecd7b9d, float:-1.3656679E38)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x017a:
            java.lang.String r0 = "mark_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0190
            com.facebook.fbservice.service.OperationResult r3 = r8.A0f(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1318031027(0x4e8f8eb3, float:1.20424691E9)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0190:
            java.lang.String r0 = "block_user"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x01a6
            com.facebook.fbservice.service.OperationResult r3 = r8.A0B(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1109386699(0xffffffffbde01a35, float:-0.109424986)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x01a6:
            java.lang.String r0 = "delete_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x01bc
            com.facebook.fbservice.service.OperationResult r3 = r8.A0H(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1661880816(0xffffffff9cf1b610, float:-1.5995104E-21)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x01bc:
            java.lang.String r0 = "delete_all_tincan_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x01d2
            com.facebook.fbservice.service.OperationResult r3 = r8.A0F(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1345711513(0xffffffffafca1267, float:-3.6756662E-10)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x01d2:
            java.lang.String r0 = "delete_messages"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x01e8
            com.facebook.fbservice.service.OperationResult r3 = r8.A0G(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -297388690(0xffffffffee46356e, float:-1.5335656E28)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x01e8:
            java.lang.String r0 = "modify_thread"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x01fe
            com.facebook.fbservice.service.OperationResult r3 = r8.A0g(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 2049686872(0x7a2bbd58, float:2.2293078E35)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x01fe:
            java.lang.String r0 = "mark_folder_seen"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0214
            com.facebook.fbservice.service.OperationResult r3 = r8.A0e(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -4752056(0xffffffffffb77d48, float:NaN)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0214:
            java.lang.String r0 = "save_draft"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x022a
            com.facebook.fbservice.service.OperationResult r3 = r8.A0m(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1996324423(0xffffffff890281b9, float:-1.5709176E-33)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x022a:
            java.lang.String r0 = "pushed_message"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0240
            com.facebook.fbservice.service.OperationResult r3 = r8.A0i(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1880048145(0x700f4211, float:1.773449E29)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0240:
            java.lang.String r0 = "update_user_settings"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0256
            com.facebook.fbservice.service.OperationResult r3 = r8.A0v(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1929876468(0x730793f4, float:1.0741591E31)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0256:
            java.lang.String r0 = "get_authenticated_attachment_url"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x026c
            com.facebook.fbservice.service.OperationResult r3 = r8.A0c(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 280850020(0x10bd6e64, float:7.471744E-29)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x026c:
            java.lang.String r0 = "update_failed_message"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0282
            com.facebook.fbservice.service.OperationResult r3 = r8.A0n(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1108340939(0x420ff0cb, float:35.98515)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0282:
            java.lang.String r0 = "update_montage_audience_mode"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0298
            com.facebook.fbservice.service.OperationResult r3 = r8.A0p(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 302245552(0x1203e6b0, float:4.1620656E-28)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0298:
            java.lang.String r0 = "update_montage_preview_block_mode"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x02ae
            com.facebook.fbservice.service.OperationResult r3 = r8.A0s(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -160920139(0xfffffffff6688db5, float:-1.17918655E33)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x02ae:
            java.lang.String r0 = "update_montage_direct_admin_text_media"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x02c4
            com.facebook.fbservice.service.OperationResult r3 = r8.A0q(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1078364106(0xffffffffbfb97836, float:-1.448981)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x02c4:
            java.lang.String r0 = "update_montage_interactive_feedback_overlays"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x02da
            com.facebook.fbservice.service.OperationResult r3 = r8.A0r(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -886138277(0xffffffffcb2e9a5b, float:-1.1442779E7)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x02da:
            java.lang.String r0 = "message_accept_request"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x02f0
            com.facebook.fbservice.service.OperationResult r3 = r8.A08(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1490531997(0xffffffffa7284963, float:-2.3354467E-15)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x02f0:
            java.lang.String r0 = "message_ignore_requests"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0306
            com.facebook.fbservice.service.OperationResult r3 = r8.A0d(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 2048816751(0x7a1e766f, float:2.0569626E35)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0306:
            java.lang.String r0 = "create_local_admin_message"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x031c
            com.facebook.fbservice.service.OperationResult r3 = r8.A0C(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 103255229(0x6278cbd, float:3.1512607E-35)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x031c:
            java.lang.String r0 = "received_sms"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0332
            com.facebook.fbservice.service.OperationResult r3 = r8.A0j(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1071646335(0xffffffffc01ff981, float:-2.4996035)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0332:
            java.lang.String r0 = "update_folder_counts"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0348
            com.facebook.fbservice.service.OperationResult r3 = r8.A0o(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1320771702(0xffffffffb1469f8a, float:-2.890348E-9)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0348:
            java.lang.String r0 = "add_admins_to_group"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x035e
            com.facebook.fbservice.service.OperationResult r3 = r8.A09(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 479793745(0x1c991251, float:1.0129425E-21)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x035e:
            java.lang.String r0 = "remove_admins_from_group"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0374
            com.facebook.fbservice.service.OperationResult r3 = r8.A0k(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -2104507511(0xffffffff828fc389, float:-2.1124193E-37)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0374:
            java.lang.String r0 = "post_game_score"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x038a
            com.facebook.fbservice.service.OperationResult r3 = r8.A0h(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 922012728(0x36f4cc38, float:7.295541E-6)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x038a:
            java.lang.String r0 = "save_username"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x03a0
            com.facebook.fbservice.service.OperationResult r3 = r8.A0K(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1042190866(0x3e1e9212, float:0.15485409)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x03a0:
            java.lang.String r0 = "edit_password"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x03b6
            com.facebook.fbservice.service.OperationResult r3 = r8.A0J(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1099523964(0x4189677c, float:17.17553)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x03b6:
            java.lang.String r0 = "save_display_name"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x03cc
            com.facebook.fbservice.service.OperationResult r3 = r8.A0I(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -2026204075(0xffffffff873a9455, float:-1.4036676E-34)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x03cc:
            java.lang.String r0 = "fetch_tincan_identity_keys"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x03e2
            com.facebook.fbservice.service.OperationResult r3 = r8.A0Z(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -2081641117(0xffffffff83ecad63, float:-1.3910641E-36)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x03e2:
            java.lang.String r0 = "fetch_group_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x03f8
            com.facebook.fbservice.service.OperationResult r3 = r8.A0M(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 145722601(0x8af8ce9, float:1.0565557E-33)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x03f8:
            java.lang.String r0 = "fetch_video_room_threads_list"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x040e
            com.facebook.fbservice.service.OperationResult r3 = r8.A0b(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 1153088025(0x44baba19, float:1493.8156)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x040e:
            java.lang.String r0 = "fetch_more_virtual_folder_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0424
            com.facebook.fbservice.service.OperationResult r3 = r8.A0U(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1858279873(0xffffffff913ce63f, float:-1.4901535E-28)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0424:
            java.lang.String r0 = "fetch_unread_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x043a
            com.facebook.fbservice.service.OperationResult r3 = r8.A0a(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -2118171320(0xffffffff81bf4548, float:-7.026173E-38)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x043a:
            java.lang.String r0 = "fetch_more_unread_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0450
            com.facebook.fbservice.service.OperationResult r3 = r8.A0T(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 591583262(0x2342d81e, float:1.0562526E-17)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0450:
            java.lang.String r0 = "fetch_threads_with_page_assigned_admin_id"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x0465
            com.facebook.fbservice.service.OperationResult r3 = r8.A0Y(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = 2022942882(0x7893a8a2, float:2.3958997E34)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x0465:
            java.lang.String r0 = "fetch_more_threads_with_page_assigned_admin_id"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x047a
            com.facebook.fbservice.service.OperationResult r3 = r8.A0S(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -753849550(0xffffffffd3112b32, float:-6.2349496E11)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x047a:
            java.lang.String r0 = "update_profile_pic_uri_with_file_path"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x048f
            com.facebook.fbservice.service.OperationResult r3 = r8.A0u(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1839285533(0xffffffff925ebae3, float:-7.0281204E-28)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x048f:
            java.lang.String r0 = "fetch_follow_up_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x04a4
            com.facebook.fbservice.service.OperationResult r3 = r8.A0L(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1674652802(0xffffffff9c2ed37e, float:-5.784512E-22)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x04a4:
            java.lang.String r0 = "fetch_more_follow_up_threads"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x04b9
            com.facebook.fbservice.service.OperationResult r3 = r8.A0O(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -1912973245(0xffffffff8dfa5843, float:-1.5428688E-30)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x04b9:
            java.lang.String r0 = "fetch_message_after_timestamp"
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x04ee }
            if (r0 == 0) goto L_0x04ce
            com.facebook.fbservice.service.OperationResult r3 = r8.A0z(r9, r10)     // Catch:{ all -> 0x04ee }
            r0 = -839703512(0xffffffffcdf32428, float:-5.09904128E8)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
            goto L_0x04da
        L_0x04ce:
            com.facebook.fbservice.service.OperationResult r3 = r10.BAz(r9)     // Catch:{ all -> 0x04ee }
            r0 = 1662406435(0x63164f23, float:2.772714E21)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ed
        L_0x04da:
            X.0pH r1 = r8.A00
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r4)
            r0.toString()
            r1.A02()
        L_0x04ed:
            return r3
        L_0x04ee:
            r3 = move-exception
            r0 = -259139068(0xfffffffff08dda04, float:-3.512076E29)
            X.C005505z.A02(r1, r0)
            if (r7 == 0) goto L_0x04ff
            X.0pH r0 = r8.A00
            X.AnonymousClass08S.A0J(r5, r4)
            r0.A02()
        L_0x04ff:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0mF.BB0(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public AnonymousClass0mF(String str) {
        this.A01 = str;
        this.A00 = null;
    }

    public AnonymousClass0mF(String str, C12400pH r2) {
        this.A01 = str;
        this.A00 = r2;
    }
}
