package X;

import android.os.StrictMode;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Collection;

/* renamed from: X.01t  reason: invalid class name and case insensitive filesystem */
public class C002801t extends AnonymousClass025 {
    public final File A00;
    public final int A01;

    public static long A01(FileChannel fileChannel, ByteBuffer byteBuffer, long j) {
        A02(fileChannel, byteBuffer, 4, j);
        return ((long) byteBuffer.getInt()) & 4294967295L;
    }

    public static void A02(FileChannel fileChannel, ByteBuffer byteBuffer, int i, long j) {
        int read;
        byteBuffer.position(0);
        byteBuffer.limit(i);
        while (byteBuffer.remaining() > 0 && (read = fileChannel.read(byteBuffer, j)) != -1) {
            j += (long) read;
        }
        if (byteBuffer.remaining() <= 0) {
            byteBuffer.position(0);
            return;
        }
        throw new C03940Qt("ELF file truncated");
    }

    public static long A00(FileChannel fileChannel, ByteBuffer byteBuffer, long j) {
        A02(fileChannel, byteBuffer, 8, j);
        return byteBuffer.getLong();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01bc, code lost:
        if (r17 == 0) goto L_0x022c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01be, code lost:
        r4 = new java.lang.String[r3];
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01c1, code lost:
        r0 = r27 + 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01c3, code lost:
        if (r6 == false) goto L_0x01ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01c5, code lost:
        r15 = A01(r8, r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01ca, code lost:
        r15 = A00(r8, r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01d0, code lost:
        if (r15 != 1) goto L_0x020b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01d2, code lost:
        if (r6 == false) goto L_0x01d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01d5, code lost:
        r0 = A00(r8, r7, r27 + 8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01dc, code lost:
        r0 = A01(r8, r7, r27 + 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01e2, code lost:
        r0 = r0 + r17;
        r5 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01e9, code lost:
        r13 = 1 + r0;
        A02(r8, r7, 1, r0);
        r0 = (short) (r7.get() & 255);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01f6, code lost:
        if (r0 == 0) goto L_0x01fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01f8, code lost:
        r5.append((char) r0);
        r0 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x01fe, code lost:
        r4[r2] = r5.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0207, code lost:
        if (r2 == Integer.MAX_VALUE) goto L_0x0226;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0209, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x020b, code lost:
        if (r6 == false) goto L_0x0218;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x020d, code lost:
        r0 = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x020f, code lost:
        r27 = r27 + r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0213, code lost:
        if (r15 != 0) goto L_0x01c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0215, code lost:
        if (r2 != r3) goto L_0x0252;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0218, code lost:
        r0 = 16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0220, code lost:
        if (X.AnonymousClass01q.A07 == false) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0222, code lost:
        X.AnonymousClass02B.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0225, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:?, code lost:
        r1 = new X.C03940Qt("malformed DT_NEEDED section");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x022c, code lost:
        r1 = new X.C03940Qt("did not find file offset of DT_STRTAB table");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0234, code lost:
        r1 = new X.C03940Qt("Dynamic section string-table not found");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0252, code lost:
        r1 = new X.C03940Qt("malformed DT_NEEDED section");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0157, code lost:
        if (r21 == 0) goto L_0x0234;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0159, code lost:
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x015d, code lost:
        if (((long) r2) >= r33) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x015f, code lost:
        r0 = r4 + 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0161, code lost:
        if (r6 == false) goto L_0x0168;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0163, code lost:
        r13 = A01(r8, r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0168, code lost:
        r13 = A01(r8, r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x016e, code lost:
        if (r13 != 1) goto L_0x019f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0170, code lost:
        if (r6 == false) goto L_0x0179;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0172, code lost:
        r19 = A01(r8, r7, r4 + 8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0179, code lost:
        r19 = A00(r8, r7, r4 + 16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x017f, code lost:
        if (r6 == false) goto L_0x018a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0181, code lost:
        r13 = A01(r8, r7, r4 + 20);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x018a, code lost:
        r13 = A00(r8, r7, r4 + 40);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0194, code lost:
        if (r19 > r21) goto L_0x019f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x019a, code lost:
        if (r21 >= (r13 + r19)) goto L_0x019f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x019c, code lost:
        if (r6 == false) goto L_0x01ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x019f, code lost:
        r4 = r4 + ((long) r15);
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01a7, code lost:
        r17 = A01(r8, r7, r4 + 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01ad, code lost:
        r17 = A00(r8, r7, r4 + 8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01b3, code lost:
        r17 = r17 + (r21 - r19);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01b8, code lost:
        r17 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String[] A03(java.io.File r35) {
        /*
            boolean r0 = X.AnonymousClass01q.A07
            r3 = r35
            if (r0 == 0) goto L_0x0011
            java.lang.String r2 = r3.getName()
            java.lang.String r1 = "SoLoader.getElfDependencies["
            java.lang.String r0 = "]"
            X.AnonymousClass02B.A01(r1, r2, r0)
        L_0x0011:
            java.io.FileInputStream r35 = new java.io.FileInputStream     // Catch:{ all -> 0x025d }
            r0 = r35
            r0.<init>(r3)     // Catch:{ all -> 0x025d }
            java.nio.channels.FileChannel r8 = r35.getChannel()     // Catch:{ all -> 0x0258 }
            r0 = 8
            java.nio.ByteBuffer r7 = java.nio.ByteBuffer.allocate(r0)     // Catch:{ all -> 0x0258 }
            java.nio.ByteOrder r0 = java.nio.ByteOrder.LITTLE_ENDIAN     // Catch:{ all -> 0x0258 }
            r7.order(r0)     // Catch:{ all -> 0x0258 }
            r0 = 0
            long r3 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            r1 = 1179403647(0x464c457f, double:5.827028246E-315)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x024a
            r11 = 4
            r0 = 1
            A02(r8, r7, r0, r11)     // Catch:{ all -> 0x0258 }
            byte r0 = r7.get()     // Catch:{ all -> 0x0258 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            short r0 = (short) r0     // Catch:{ all -> 0x0258 }
            r6 = 1
            if (r0 == r6) goto L_0x0045
            r6 = 0
        L_0x0045:
            r13 = 5
            r0 = 1
            A02(r8, r7, r0, r13)     // Catch:{ all -> 0x0258 }
            byte r0 = r7.get()     // Catch:{ all -> 0x0258 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            short r1 = (short) r0     // Catch:{ all -> 0x0258 }
            r0 = 2
            if (r1 != r0) goto L_0x005a
            java.nio.ByteOrder r0 = java.nio.ByteOrder.BIG_ENDIAN     // Catch:{ all -> 0x0258 }
            r7.order(r0)     // Catch:{ all -> 0x0258 }
        L_0x005a:
            r9 = 28
            r2 = 32
            if (r6 == 0) goto L_0x0065
            long r4 = A01(r8, r7, r9)     // Catch:{ all -> 0x0258 }
            goto L_0x0069
        L_0x0065:
            long r4 = A00(r8, r7, r2)     // Catch:{ all -> 0x0258 }
        L_0x0069:
            if (r6 == 0) goto L_0x0085
            r15 = 44
            r0 = 2
            r17 = r8
            r18 = r7
            r19 = r0
            r20 = r15
            A02(r17, r18, r19, r20)     // Catch:{ all -> 0x0258 }
            short r1 = r7.getShort()     // Catch:{ all -> 0x0258 }
            r0 = 65535(0xffff, float:9.1834E-41)
            r1 = r1 & r0
            long r0 = (long) r1     // Catch:{ all -> 0x0258 }
            r33 = r0
            goto L_0x009e
        L_0x0085:
            r15 = 56
            r0 = 2
            r17 = r8
            r18 = r7
            r19 = r0
            r20 = r15
            A02(r17, r18, r19, r20)     // Catch:{ all -> 0x0258 }
            short r1 = r7.getShort()     // Catch:{ all -> 0x0258 }
            r0 = 65535(0xffff, float:9.1834E-41)
            r1 = r1 & r0
            long r0 = (long) r1     // Catch:{ all -> 0x0258 }
            r33 = r0
        L_0x009e:
            if (r6 == 0) goto L_0x00a3
            r15 = 42
        L_0x00a2:
            goto L_0x00a6
        L_0x00a3:
            r15 = 54
            goto L_0x00a2
        L_0x00a6:
            r20 = r15
            A02(r17, r18, r19, r20)     // Catch:{ all -> 0x0258 }
            short r15 = r7.getShort()     // Catch:{ all -> 0x0258 }
            r0 = 65535(0xffff, float:9.1834E-41)
            r15 = r15 & r0
            r17 = 65535(0xffff, double:3.23786E-319)
            r0 = 40
            int r16 = (r33 > r17 ? 1 : (r33 == r17 ? 0 : -1))
            if (r16 != 0) goto L_0x00d6
            if (r6 == 0) goto L_0x00c3
            long r0 = A01(r8, r7, r2)     // Catch:{ all -> 0x0258 }
            goto L_0x00c7
        L_0x00c3:
            long r0 = A00(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x00c7:
            if (r6 == 0) goto L_0x00cf
            long r0 = r0 + r9
            long r33 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x00d6
        L_0x00cf:
            r2 = 44
            long r0 = r0 + r2
            long r33 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x00d6:
            r2 = r4
            r18 = 0
        L_0x00d9:
            r31 = 1
            r29 = 8
            int r0 = (r18 > r33 ? 1 : (r18 == r33 ? 0 : -1))
            if (r0 >= 0) goto L_0x010b
            r9 = 0
            long r0 = r2 + r9
            if (r6 == 0) goto L_0x00ec
            long r16 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x00f0
        L_0x00ec:
            long r16 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x00f0:
            r9 = 2
            int r0 = (r16 > r9 ? 1 : (r16 == r9 ? 0 : -1))
            if (r0 != 0) goto L_0x00f9
            if (r6 == 0) goto L_0x0104
            goto L_0x00fe
        L_0x00f9:
            long r0 = (long) r15     // Catch:{ all -> 0x0258 }
            long r2 = r2 + r0
            long r18 = r18 + r31
            goto L_0x00d9
        L_0x00fe:
            long r2 = r2 + r11
            long r27 = A01(r8, r7, r2)     // Catch:{ all -> 0x0258 }
            goto L_0x010d
        L_0x0104:
            long r2 = r2 + r29
            long r27 = A00(r8, r7, r2)     // Catch:{ all -> 0x0258 }
            goto L_0x010d
        L_0x010b:
            r27 = 0
        L_0x010d:
            r25 = 0
            int r0 = (r27 > r25 ? 1 : (r27 == r25 ? 0 : -1))
            if (r0 == 0) goto L_0x0242
            r23 = r27
            r21 = 0
            r3 = 0
        L_0x0118:
            if (r6 == 0) goto L_0x0121
            long r0 = r23 + r25
            long r18 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0127
        L_0x0121:
            long r0 = r23 + r25
            long r18 = A00(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x0127:
            java.lang.String r9 = "malformed DT_NEEDED section"
            int r0 = (r18 > r31 ? 1 : (r18 == r31 ? 0 : -1))
            if (r0 != 0) goto L_0x012e
            goto L_0x0142
        L_0x012e:
            int r0 = (r18 > r13 ? 1 : (r18 == r13 ? 0 : -1))
            if (r0 != 0) goto L_0x0149
            if (r6 == 0) goto L_0x013b
            long r0 = r23 + r11
            long r21 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0149
        L_0x013b:
            long r0 = r23 + r29
            long r21 = A00(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0149
        L_0x0142:
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r3 == r0) goto L_0x023c
            int r3 = r3 + 1
        L_0x0149:
            r16 = 16
            if (r6 == 0) goto L_0x01a4
            r0 = 8
        L_0x014f:
            long r23 = r23 + r0
            int r0 = (r18 > r25 ? 1 : (r18 == r25 ? 0 : -1))
            if (r0 != 0) goto L_0x0118
            int r0 = (r21 > r25 ? 1 : (r21 == r25 ? 0 : -1))
            if (r0 == 0) goto L_0x0234
            r2 = 0
        L_0x015a:
            long r0 = (long) r2     // Catch:{ all -> 0x0258 }
            int r10 = (r0 > r33 ? 1 : (r0 == r33 ? 0 : -1))
            if (r10 >= 0) goto L_0x01b8
            long r0 = r4 + r25
            if (r6 == 0) goto L_0x0168
            long r13 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x016c
        L_0x0168:
            long r13 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x016c:
            int r0 = (r13 > r31 ? 1 : (r13 == r31 ? 0 : -1))
            if (r0 != 0) goto L_0x019f
            if (r6 == 0) goto L_0x0179
            long r0 = r4 + r29
            long r19 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x017f
        L_0x0179:
            long r0 = r4 + r16
            long r19 = A00(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x017f:
            if (r6 == 0) goto L_0x018a
            r13 = 20
            long r0 = r4 + r13
            long r13 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0192
        L_0x018a:
            r13 = 40
            long r0 = r4 + r13
            long r13 = A00(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x0192:
            int r0 = (r19 > r21 ? 1 : (r19 == r21 ? 0 : -1))
            if (r0 > 0) goto L_0x019f
            long r13 = r13 + r19
            int r0 = (r21 > r13 ? 1 : (r21 == r13 ? 0 : -1))
            if (r0 >= 0) goto L_0x019f
            if (r6 == 0) goto L_0x01ad
            goto L_0x01a7
        L_0x019f:
            long r0 = (long) r15     // Catch:{ all -> 0x0258 }
            long r4 = r4 + r0
            int r2 = r2 + 1
            goto L_0x015a
        L_0x01a4:
            r0 = 16
            goto L_0x014f
        L_0x01a7:
            long r4 = r4 + r11
            long r17 = A01(r8, r7, r4)     // Catch:{ all -> 0x0258 }
            goto L_0x01b3
        L_0x01ad:
            long r4 = r4 + r29
            long r17 = A00(r8, r7, r4)     // Catch:{ all -> 0x0258 }
        L_0x01b3:
            long r21 = r21 - r19
            long r17 = r17 + r21
            goto L_0x01ba
        L_0x01b8:
            r17 = 0
        L_0x01ba:
            int r0 = (r17 > r25 ? 1 : (r17 == r25 ? 0 : -1))
            if (r0 == 0) goto L_0x022c
            java.lang.String[] r4 = new java.lang.String[r3]     // Catch:{ all -> 0x0258 }
            r2 = 0
        L_0x01c1:
            long r0 = r27 + r25
            if (r6 == 0) goto L_0x01ca
            long r15 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x01ce
        L_0x01ca:
            long r15 = A00(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x01ce:
            int r0 = (r15 > r31 ? 1 : (r15 == r31 ? 0 : -1))
            if (r0 != 0) goto L_0x020b
            if (r6 == 0) goto L_0x01d5
            goto L_0x01dc
        L_0x01d5:
            long r0 = r27 + r29
            long r0 = A00(r8, r7, r0)     // Catch:{ all -> 0x0258 }
            goto L_0x01e2
        L_0x01dc:
            long r0 = r27 + r11
            long r0 = A01(r8, r7, r0)     // Catch:{ all -> 0x0258 }
        L_0x01e2:
            long r0 = r0 + r17
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0258 }
            r5.<init>()     // Catch:{ all -> 0x0258 }
        L_0x01e9:
            long r13 = r31 + r0
            r10 = 1
            A02(r8, r7, r10, r0)     // Catch:{ all -> 0x0258 }
            byte r0 = r7.get()     // Catch:{ all -> 0x0258 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            short r0 = (short) r0     // Catch:{ all -> 0x0258 }
            if (r0 == 0) goto L_0x01fe
            char r0 = (char) r0     // Catch:{ all -> 0x0258 }
            r5.append(r0)     // Catch:{ all -> 0x0258 }
            r0 = r13
            goto L_0x01e9
        L_0x01fe:
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x0258 }
            r4[r2] = r0     // Catch:{ all -> 0x0258 }
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r2 == r0) goto L_0x0226
            int r2 = r2 + 1
        L_0x020b:
            if (r6 == 0) goto L_0x0218
            r0 = 8
        L_0x020f:
            long r27 = r27 + r0
            int r0 = (r15 > r25 ? 1 : (r15 == r25 ? 0 : -1))
            if (r0 != 0) goto L_0x01c1
            if (r2 != r3) goto L_0x0252
            goto L_0x021b
        L_0x0218:
            r0 = 16
            goto L_0x020f
        L_0x021b:
            r35.close()     // Catch:{ all -> 0x025d }
            boolean r0 = X.AnonymousClass01q.A07
            if (r0 == 0) goto L_0x0225
            X.AnonymousClass02B.A00()
        L_0x0225:
            return r4
        L_0x0226:
            X.0Qt r1 = new X.0Qt     // Catch:{ all -> 0x0258 }
            r1.<init>(r9)     // Catch:{ all -> 0x0258 }
            goto L_0x0257
        L_0x022c:
            X.0Qt r1 = new X.0Qt     // Catch:{ all -> 0x0258 }
            java.lang.String r0 = "did not find file offset of DT_STRTAB table"
            r1.<init>(r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0257
        L_0x0234:
            X.0Qt r1 = new X.0Qt     // Catch:{ all -> 0x0258 }
            java.lang.String r0 = "Dynamic section string-table not found"
            r1.<init>(r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0257
        L_0x023c:
            X.0Qt r1 = new X.0Qt     // Catch:{ all -> 0x0258 }
            r1.<init>(r9)     // Catch:{ all -> 0x0258 }
            goto L_0x0257
        L_0x0242:
            X.0Qt r1 = new X.0Qt     // Catch:{ all -> 0x0258 }
            java.lang.String r0 = "ELF file does not contain dynamic linking information"
            r1.<init>(r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0257
        L_0x024a:
            X.0Qt r1 = new X.0Qt     // Catch:{ all -> 0x0258 }
            java.lang.String r0 = "file is not ELF"
            r1.<init>(r0)     // Catch:{ all -> 0x0258 }
            goto L_0x0257
        L_0x0252:
            X.0Qt r1 = new X.0Qt     // Catch:{ all -> 0x0258 }
            r1.<init>(r9)     // Catch:{ all -> 0x0258 }
        L_0x0257:
            throw r1     // Catch:{ all -> 0x0258 }
        L_0x0258:
            r0 = move-exception
            r35.close()     // Catch:{ all -> 0x025d }
            throw r0     // Catch:{ all -> 0x025d }
        L_0x025d:
            r1 = move-exception
            boolean r0 = X.AnonymousClass01q.A07
            if (r0 == 0) goto L_0x0265
            X.AnonymousClass02B.A00()
        L_0x0265:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C002801t.A03(java.io.File):java.lang.String[]");
    }

    public File A05(String str) {
        File file = new File(this.A00, str);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public String A06(String str) {
        File file = new File(this.A00, str);
        if (file.exists()) {
            return file.getCanonicalPath();
        }
        return null;
    }

    public void A07(Collection collection) {
        collection.add(this.A00.getAbsolutePath());
    }

    public String[] A08(String str) {
        File file = new File(this.A00, str);
        if (file.exists()) {
            return A03(file);
        }
        return null;
    }

    public int A09(String str, int i, StrictMode.ThreadPolicy threadPolicy) {
        return A0C(str, i, this.A00, threadPolicy);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00d4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:70:0x00d8 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A0C(java.lang.String r14, int r15, java.io.File r16, android.os.StrictMode.ThreadPolicy r17) {
        /*
            r13 = this;
            java.io.File r7 = new java.io.File
            r1 = r16
            r7.<init>(r1, r14)
            boolean r0 = r7.exists()
            if (r0 != 0) goto L_0x0012
            r1.getCanonicalPath()
            r0 = 0
            return r0
        L_0x0012:
            r1.getCanonicalPath()
            r0 = r15 & 1
            if (r0 == 0) goto L_0x0020
            int r0 = r13.A01
            r6 = 2
            r0 = r0 & r6
            if (r0 == 0) goto L_0x0020
            return r6
        L_0x0020:
            int r0 = r13.A01
            r6 = 1
            r0 = r0 & r6
            if (r0 == 0) goto L_0x0048
            java.lang.String[] r4 = A03(r7)
            java.util.Arrays.toString(r4)
            r3 = 0
        L_0x002e:
            int r0 = r4.length
            if (r3 >= r0) goto L_0x0048
            r2 = r4[r3]
            java.lang.String r0 = "/"
            boolean r0 = r2.startsWith(r0)
            if (r0 != 0) goto L_0x0045
            r0 = r15 | 1
            r1 = r0 & -3
            r0 = 0
            r5 = r17
            X.AnonymousClass01q.A0A(r2, r0, r0, r1, r5)
        L_0x0045:
            int r3 = r3 + 1
            goto L_0x002e
        L_0x0048:
            X.08k r5 = X.AnonymousClass01q.A02     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            java.lang.String r2 = r7.getAbsolutePath()     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            boolean r0 = r5.A04     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            if (r0 == 0) goto L_0x00f6
            r1 = 4
            r15 = r15 & r1
            r0 = 0
            if (r15 != r1) goto L_0x0058
            r0 = 1
        L_0x0058:
            if (r0 == 0) goto L_0x005d
            java.lang.String r12 = r5.A01     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            goto L_0x005f
        L_0x005d:
            java.lang.String r12 = r5.A02     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
        L_0x005f:
            r8 = 0
            java.lang.Runtime r4 = r5.A00     // Catch:{ IllegalAccessException | IllegalArgumentException | InvocationTargetException -> 0x0087 }
            monitor-enter(r4)     // Catch:{ IllegalAccessException | IllegalArgumentException | InvocationTargetException -> 0x0087 }
            java.lang.reflect.Method r3 = r5.A03     // Catch:{ all -> 0x0084 }
            java.lang.Runtime r1 = r5.A00     // Catch:{ all -> 0x0084 }
            java.lang.Class<X.01q> r0 = X.AnonymousClass01q.class
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ all -> 0x0084 }
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r0, r12}     // Catch:{ all -> 0x0084 }
            java.lang.Object r1 = r3.invoke(r1, r0)     // Catch:{ all -> 0x0084 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0084 }
            if (r1 != 0) goto L_0x007b
            monitor-exit(r4)     // Catch:{ all -> 0x0081 }
            return r6
        L_0x007b:
            java.lang.UnsatisfiedLinkError r0 = new java.lang.UnsatisfiedLinkError     // Catch:{ all -> 0x0081 }
            r0.<init>(r1)     // Catch:{ all -> 0x0081 }
            throw r0     // Catch:{ all -> 0x0081 }
        L_0x0081:
            r0 = move-exception
            r8 = r1
            goto L_0x0085
        L_0x0084:
            r0 = move-exception
        L_0x0085:
            monitor-exit(r4)     // Catch:{ all -> 0x0084 }
            throw r0     // Catch:{ IllegalAccessException | IllegalArgumentException | InvocationTargetException -> 0x0087 }
        L_0x0087:
            r1 = move-exception
            java.lang.String r0 = "Error: Cannot load "
            java.lang.String r8 = X.AnonymousClass08S.A0J(r0, r2)     // Catch:{ all -> 0x0094 }
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0094 }
            r0.<init>(r8, r1)     // Catch:{ all -> 0x0094 }
            throw r0     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r6 = move-exception
            if (r8 == 0) goto L_0x00f5
            java.lang.String r7 = "Error when loading lib: "
            java.lang.String r9 = " lib hash: "
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x00df, SecurityException -> 0x00d9, NoSuchAlgorithmException -> 0x00e5 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x00df, SecurityException -> 0x00d9, NoSuchAlgorithmException -> 0x00e5 }
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r5 = java.security.MessageDigest.getInstance(r0)     // Catch:{ IOException -> 0x00df, SecurityException -> 0x00d9, NoSuchAlgorithmException -> 0x00e5 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00df, SecurityException -> 0x00d9, NoSuchAlgorithmException -> 0x00e5 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x00df, SecurityException -> 0x00d9, NoSuchAlgorithmException -> 0x00e5 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x00d2 }
        L_0x00af:
            int r1 = r4.read(r2)     // Catch:{ all -> 0x00d2 }
            r0 = 0
            if (r1 <= 0) goto L_0x00ba
            r5.update(r2, r0, r1)     // Catch:{ all -> 0x00d2 }
            goto L_0x00af
        L_0x00ba:
            java.lang.String r3 = "%32x"
            r2 = 1
            java.math.BigInteger r1 = new java.math.BigInteger     // Catch:{ all -> 0x00d2 }
            byte[] r0 = r5.digest()     // Catch:{ all -> 0x00d2 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x00d2 }
            java.lang.Object[] r0 = new java.lang.Object[]{r1}     // Catch:{ all -> 0x00d2 }
            java.lang.String r10 = java.lang.String.format(r3, r0)     // Catch:{ all -> 0x00d2 }
            r4.close()     // Catch:{ IOException -> 0x00df, SecurityException -> 0x00d9, NoSuchAlgorithmException -> 0x00e5 }
            goto L_0x00ea
        L_0x00d2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00d4 }
        L_0x00d4:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x00d8 }
        L_0x00d8:
            throw r0     // Catch:{ IOException -> 0x00df, SecurityException -> 0x00d9, NoSuchAlgorithmException -> 0x00e5 }
        L_0x00d9:
            r0 = move-exception
            java.lang.String r10 = r0.toString()     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            goto L_0x00ea
        L_0x00df:
            r0 = move-exception
            java.lang.String r10 = r0.toString()     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            goto L_0x00ea
        L_0x00e5:
            r0 = move-exception
            java.lang.String r10 = r0.toString()     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
        L_0x00ea:
            java.lang.String r11 = " search path is "
            java.lang.String r1 = X.AnonymousClass08S.A0U(r7, r8, r9, r10, r11, r12)     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            java.lang.String r0 = "SoLoader"
            android.util.Log.e(r0, r1)     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
        L_0x00f5:
            throw r6     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
        L_0x00f6:
            java.lang.System.load(r2)     // Catch:{ UnsatisfiedLinkError -> 0x00fa }
            return r6
        L_0x00fa:
            r2 = move-exception
            java.lang.String r1 = r2.getMessage()
            java.lang.String r0 = "bad ELF magic"
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x0109
            r0 = 3
            return r0
        L_0x0109:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C002801t.A0C(java.lang.String, int, java.io.File, android.os.StrictMode$ThreadPolicy):int");
    }

    public String toString() {
        String str;
        try {
            str = String.valueOf(this.A00.getCanonicalPath());
        } catch (IOException unused) {
            str = this.A00.getName();
        }
        return getClass().getName() + "[root = " + str + " flags = " + this.A01 + ']';
    }

    public C002801t(File file, int i) {
        this.A00 = file;
        this.A01 = i;
    }
}
