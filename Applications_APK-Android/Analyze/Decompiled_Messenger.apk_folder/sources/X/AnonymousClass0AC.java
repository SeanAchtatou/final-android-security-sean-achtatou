package X;

import android.content.pm.PackageInfo;
import com.facebook.acra.ACRA;

/* renamed from: X.0AC  reason: invalid class name */
public final class AnonymousClass0AC {
    public final String A00;
    public volatile PackageInfo A01;
    public volatile Integer A02 = AnonymousClass07B.A00;

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("FbnsPackageInfo{mPackageName='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append(", mPackageStatus=");
        Integer num = this.A02;
        if (num != null) {
            switch (num.intValue()) {
                case 1:
                    str = "NOT_INSTALLED";
                    break;
                case 2:
                    str = "DISABLED";
                    break;
                case 3:
                    str = "UNSUPPORTED";
                    break;
                case 4:
                    str = "INSTALLED";
                    break;
                case 5:
                    str = "NOT_TRUSTED";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                    str = "TRUSTED";
                    break;
                default:
                    str = "UNKNOWN";
                    break;
            }
        } else {
            str = "null";
        }
        sb.append(str);
        sb.append(", mPackageInfo=");
        sb.append(this.A01);
        sb.append('}');
        return sb.toString();
    }

    public AnonymousClass0AC(String str) {
        this.A00 = str;
    }
}
