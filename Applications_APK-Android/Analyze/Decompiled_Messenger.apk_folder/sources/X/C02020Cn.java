package X;

/* renamed from: X.0Cn  reason: invalid class name and case insensitive filesystem */
public class C02020Cn {
    public final C01990Ck A00;
    private final Object A01;
    private final Object A02;

    public String toString() {
        String str;
        String r2 = this.A00.A03.toString();
        Integer A012 = AnonymousClass08G.A01(this.A00.A02);
        if (A012 != null) {
            switch (A012.intValue()) {
                case 1:
                    str = "ACKNOWLEDGED_DELIVERY";
                    break;
                case 2:
                    str = "ASSURED_DELIVERY";
                    break;
                default:
                    str = "FIRE_AND_FORGET";
                    break;
            }
        } else {
            str = "null";
        }
        return AnonymousClass08S.A07(r2, ' ', str);
    }

    public C02020Cn(C01990Ck r1, Object obj, Object obj2) {
        this.A00 = r1;
        this.A02 = obj;
        this.A01 = obj2;
    }

    public Object A00() {
        return this.A01;
    }

    public Object A01() {
        return this.A02;
    }
}
