package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Vy  reason: invalid class name and case insensitive filesystem */
public final class C04730Vy implements C04740Vz {
    private static volatile C04730Vy A01;
    public final AnonymousClass1Y6 A00;

    public static final C04730Vy A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C04730Vy.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C04730Vy(AnonymousClass0UX.A07(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void ASG() {
        this.A00.AOy("Database accessed from a UI Thread.");
    }

    private C04730Vy(AnonymousClass1Y6 r1) {
        this.A00 = r1;
    }
}
