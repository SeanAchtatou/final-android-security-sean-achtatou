package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0wZ  reason: invalid class name and case insensitive filesystem */
public final class C16150wZ implements C16160wa {
    public int A00 = 0;
    private C06380bP A01 = new AnonymousClass0ZH(30);
    public final C16200we A02;
    public final boolean A03;
    public final C16190wd A04;
    public final ArrayList A05 = new ArrayList();
    public final ArrayList A06 = new ArrayList();

    private int A00(int i, int i2) {
        for (int size = this.A06.size() - 1; size >= 0; size--) {
            C21731Im r7 = (C21731Im) this.A06.get(size);
            int i3 = r7.A00;
            if (i3 == 8) {
                int i4 = r7.A02;
                int i5 = r7.A01;
                int i6 = i5;
                int i7 = i4;
                if (i4 < i5) {
                    i6 = i4;
                    i7 = i5;
                }
                if (i < i6 || i > i7) {
                    if (i < i4) {
                        if (i2 == 1) {
                            r7.A02 = i4 + 1;
                            r7.A01 = i5 + 1;
                        } else if (i2 == 2) {
                            r7.A02 = i4 - 1;
                            r7.A01 = i5 - 1;
                        }
                    }
                } else if (i6 == i4) {
                    if (i2 == 1) {
                        r7.A01 = i5 + 1;
                    } else if (i2 == 2) {
                        r7.A01 = i5 - 1;
                    }
                    i++;
                } else {
                    if (i2 == 1) {
                        r7.A02 = i4 + 1;
                    } else if (i2 == 2) {
                        r7.A02 = i4 - 1;
                    }
                    i--;
                }
            } else {
                int i8 = r7.A02;
                if (i8 <= i) {
                    if (i3 == 1) {
                        i -= r7.A01;
                    } else if (i3 == 2) {
                        i += r7.A01;
                    }
                } else if (i2 == 1) {
                    r7.A02 = i8 + 1;
                } else if (i2 == 2) {
                    r7.A02 = i8 - 1;
                }
            }
        }
        for (int size2 = this.A06.size() - 1; size2 >= 0; size2--) {
            C21731Im r2 = (C21731Im) this.A06.get(size2);
            if (r2.A00 == 8) {
                int i9 = r2.A01;
                if (i9 != r2.A02 && i9 >= 0) {
                }
            } else if (r2.A01 > 0) {
            }
            this.A06.remove(size2);
            C0B(r2);
        }
        return i;
    }

    public static int A01(C16150wZ r5, int i, int i2) {
        int size = r5.A06.size();
        while (i2 < size) {
            C21731Im r3 = (C21731Im) r5.A06.get(i2);
            int i3 = r3.A00;
            if (i3 == 8) {
                int i4 = r3.A02;
                if (i4 == i) {
                    i = r3.A01;
                } else {
                    if (i4 < i) {
                        i--;
                    }
                    if (r3.A01 <= i) {
                        i++;
                    }
                }
            } else {
                int i5 = r3.A02;
                if (i5 > i) {
                    continue;
                } else if (i3 == 2) {
                    int i6 = r3.A01;
                    int i7 = i;
                    i -= i6;
                    if (i7 < i5 + i6) {
                        return -1;
                    }
                } else if (i3 == 1) {
                    i += r3.A01;
                }
            }
            i2++;
        }
        return i;
    }

    private void A02(C21731Im r11) {
        int i;
        boolean z;
        int i2 = r11.A00;
        if (i2 == 1 || i2 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int A002 = A00(r11.A02, i2);
        int i3 = r11.A02;
        int i4 = r11.A00;
        if (i4 == 2) {
            i = 0;
        } else if (i4 == 4) {
            i = 1;
        } else {
            throw new IllegalArgumentException("op should be remove or update." + r11);
        }
        int i5 = 1;
        for (int i6 = 1; i6 < r11.A01; i6++) {
            int A003 = A00(r11.A02 + (i * i6), r11.A00);
            int i7 = r11.A00;
            if (i7 == 2 ? A003 != A002 : !(i7 == 4 && A003 == A002 + 1)) {
                z = false;
            } else {
                z = true;
            }
            if (z) {
                i5++;
            } else {
                C21731Im BMn = BMn(i7, A002, i5, r11.A03);
                A04(BMn, i3);
                C0B(BMn);
                if (r11.A00 == 4) {
                    i3 += i5;
                }
                A002 = A003;
                i5 = 1;
            }
        }
        Object obj = r11.A03;
        C0B(r11);
        if (i5 > 0) {
            C21731Im BMn2 = BMn(r11.A00, A002, i5, obj);
            A04(BMn2, i3);
            C0B(BMn2);
        }
    }

    private void A03(C21731Im r5) {
        this.A06.add(r5);
        int i = r5.A00;
        if (i == 1) {
            this.A04.BMo(r5.A02, r5.A01);
        } else if (i == 2) {
            this.A04.BMr(r5.A02, r5.A01);
        } else if (i == 4) {
            this.A04.BK3(r5.A02, r5.A01, r5.A03);
        } else if (i == 8) {
            this.A04.BMp(r5.A02, r5.A01);
        } else {
            throw new IllegalArgumentException("Unknown update op type for " + r5);
        }
    }

    private void A04(C21731Im r4, int i) {
        this.A04.BWe(r4);
        int i2 = r4.A00;
        if (i2 == 2) {
            this.A04.BMq(i, r4.A01);
        } else if (i2 == 4) {
            this.A04.BK3(i, r4.A01, r4.A03);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    private boolean A06(int i) {
        int size = this.A06.size();
        int i2 = 0;
        while (i2 < size) {
            C21731Im r4 = (C21731Im) this.A06.get(i2);
            int i3 = r4.A00;
            if (i3 != 8) {
                if (i3 == 1) {
                    int i4 = r4.A02;
                    int i5 = i4 + r4.A01;
                    while (i4 < i5) {
                        if (A01(this, i4, i2 + 1) != i) {
                            i4++;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
                i2++;
            } else if (A01(this, r4.A01, i2 + 1) != i) {
                i2++;
            }
            return true;
        }
        return false;
    }

    public void A07() {
        int size = this.A06.size();
        for (int i = 0; i < size; i++) {
            this.A04.BWf((C21731Im) this.A06.get(i));
        }
        A05(this, this.A06);
        this.A00 = 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01c5, code lost:
        if (r1 != 8) goto L_0x01c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x0256, code lost:
        if (r1 == 0) goto L_0x0268;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0266, code lost:
        if (r11 == 0) goto L_0x0268;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a2, code lost:
        if (r5.A01 != (r2 - r3)) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00ff, code lost:
        if (r5.A01 != (r3 - r2)) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0103, code lost:
        r13 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x012e, code lost:
        if (r1 > r5.A02) goto L_0x0130;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0173, code lost:
        if (r1 >= r5.A02) goto L_0x0130;
     */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x01ca  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0004 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x01cd A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09() {
        /*
            r14 = this;
            X.0we r11 = r14.A02
            java.util.ArrayList r10 = r14.A05
        L_0x0004:
            int r8 = r10.size()
            r9 = 1
            int r8 = r8 - r9
            r2 = 0
        L_0x000b:
            if (r8 < 0) goto L_0x01a4
            java.lang.Object r0 = r10.get(r8)
            X.1Im r0 = (X.C21731Im) r0
            int r1 = r0.A00
            r0 = 8
            if (r1 != r0) goto L_0x019f
            if (r2 == 0) goto L_0x01a0
        L_0x001b:
            r0 = -1
            if (r8 == r0) goto L_0x01a7
            int r7 = r8 + 1
            java.lang.Object r6 = r10.get(r8)
            X.1Im r6 = (X.C21731Im) r6
            java.lang.Object r5 = r10.get(r7)
            X.1Im r5 = (X.C21731Im) r5
            int r1 = r5.A00
            r0 = 1
            if (r1 == r0) goto L_0x0176
            r0 = 2
            if (r1 == r0) goto L_0x0092
            r0 = 4
            if (r1 != r0) goto L_0x0004
            int r2 = r6.A01
            int r1 = r5.A02
            r3 = 4
            r4 = 0
            if (r2 >= r1) goto L_0x007f
            int r1 = r1 - r9
            r5.A02 = r1
        L_0x0042:
            r2 = r4
        L_0x0043:
            int r13 = r6.A02
            int r12 = r5.A02
            if (r13 > r12) goto L_0x006a
            int r12 = r12 + r9
            r5.A02 = r12
        L_0x004c:
            r10.set(r7, r6)
            int r0 = r5.A01
            if (r0 <= 0) goto L_0x0061
            r10.set(r8, r5)
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r10.add(r8, r2)
        L_0x005b:
            if (r4 == 0) goto L_0x0004
            r10.add(r8, r4)
            goto L_0x0004
        L_0x0061:
            r10.remove(r8)
            X.0wa r0 = r11.A00
            r0.C0B(r5)
            goto L_0x0056
        L_0x006a:
            int r0 = r5.A01
            int r12 = r12 + r0
            if (r13 >= r12) goto L_0x004c
            int r12 = r12 - r13
            X.0wa r1 = r11.A00
            int r13 = r13 + r9
            java.lang.Object r0 = r5.A03
            X.1Im r4 = r1.BMn(r3, r13, r12, r0)
            int r0 = r5.A01
            int r0 = r0 - r12
            r5.A01 = r0
            goto L_0x004c
        L_0x007f:
            int r0 = r5.A01
            int r1 = r1 + r0
            if (r2 >= r1) goto L_0x0042
            int r0 = r0 - r9
            r5.A01 = r0
            X.0wa r2 = r11.A00
            int r1 = r6.A02
            java.lang.Object r0 = r5.A03
            X.1Im r2 = r2.BMn(r3, r1, r9, r0)
            goto L_0x0043
        L_0x0092:
            int r3 = r6.A02
            int r2 = r6.A01
            r13 = 0
            if (r3 >= r2) goto L_0x00f5
            int r0 = r5.A02
            if (r0 != r3) goto L_0x00a4
            int r1 = r5.A01
            int r0 = r2 - r3
            r12 = 0
            if (r1 == r0) goto L_0x0103
        L_0x00a4:
            r12 = 0
        L_0x00a5:
            int r1 = r5.A02
            r4 = 2
            if (r2 >= r1) goto L_0x00db
            int r1 = r1 - r9
            r5.A02 = r1
        L_0x00ad:
            int r3 = r6.A02
            int r1 = r5.A02
            r2 = 0
            if (r3 > r1) goto L_0x00c6
            int r1 = r1 + r9
            r5.A02 = r1
        L_0x00b7:
            if (r13 == 0) goto L_0x0105
            r10.set(r8, r5)
            r10.remove(r7)
            X.0wa r0 = r11.A00
            r0.C0B(r6)
            goto L_0x0004
        L_0x00c6:
            int r0 = r5.A01
            int r1 = r1 + r0
            if (r3 >= r1) goto L_0x00b7
            int r1 = r1 - r3
            X.0wa r0 = r11.A00
            int r3 = r3 + r9
            X.1Im r2 = r0.BMn(r4, r3, r1, r2)
            int r1 = r6.A02
            int r0 = r5.A02
            int r1 = r1 - r0
            r5.A01 = r1
            goto L_0x00b7
        L_0x00db:
            int r0 = r5.A01
            int r1 = r1 + r0
            if (r2 >= r1) goto L_0x00ad
            int r0 = r0 - r9
            r5.A01 = r0
            r6.A00 = r4
            r6.A01 = r9
            int r0 = r5.A01
            if (r0 != 0) goto L_0x0004
            r10.remove(r7)
            X.0wa r0 = r11.A00
            r0.C0B(r5)
            goto L_0x0004
        L_0x00f5:
            int r1 = r5.A02
            int r0 = r2 + r9
            if (r1 != r0) goto L_0x0101
            int r0 = r5.A01
            int r3 = r3 - r2
            r12 = 1
            if (r0 == r3) goto L_0x0103
        L_0x0101:
            r12 = 1
            goto L_0x00a5
        L_0x0103:
            r13 = 1
            goto L_0x00a5
        L_0x0105:
            if (r12 == 0) goto L_0x014c
            if (r2 == 0) goto L_0x011f
            int r1 = r6.A02
            int r0 = r2.A02
            if (r1 <= r0) goto L_0x0114
            int r0 = r2.A01
            int r1 = r1 - r0
            r6.A02 = r1
        L_0x0114:
            int r1 = r6.A01
            int r0 = r2.A02
            if (r1 <= r0) goto L_0x011f
            int r0 = r2.A01
            int r1 = r1 - r0
            r6.A01 = r1
        L_0x011f:
            int r1 = r6.A02
            int r0 = r5.A02
            if (r1 <= r0) goto L_0x012a
            int r0 = r5.A01
            int r1 = r1 - r0
            r6.A02 = r1
        L_0x012a:
            int r1 = r6.A01
            int r0 = r5.A02
            if (r1 <= r0) goto L_0x0135
        L_0x0130:
            int r0 = r5.A01
            int r1 = r1 - r0
            r6.A01 = r1
        L_0x0135:
            r10.set(r8, r5)
            int r1 = r6.A02
            int r0 = r6.A01
            if (r1 == r0) goto L_0x0148
            r10.set(r7, r6)
        L_0x0141:
            if (r2 == 0) goto L_0x0004
            r10.add(r8, r2)
            goto L_0x0004
        L_0x0148:
            r10.remove(r7)
            goto L_0x0141
        L_0x014c:
            if (r2 == 0) goto L_0x0164
            int r1 = r6.A02
            int r0 = r2.A02
            if (r1 < r0) goto L_0x0159
            int r0 = r2.A01
            int r1 = r1 - r0
            r6.A02 = r1
        L_0x0159:
            int r1 = r6.A01
            int r0 = r2.A02
            if (r1 < r0) goto L_0x0164
            int r0 = r2.A01
            int r1 = r1 - r0
            r6.A01 = r1
        L_0x0164:
            int r1 = r6.A02
            int r0 = r5.A02
            if (r1 < r0) goto L_0x016f
            int r0 = r5.A01
            int r1 = r1 - r0
            r6.A02 = r1
        L_0x016f:
            int r1 = r6.A01
            int r0 = r5.A02
            if (r1 < r0) goto L_0x0135
            goto L_0x0130
        L_0x0176:
            int r4 = r6.A01
            int r3 = r5.A02
            r2 = 0
            if (r4 >= r3) goto L_0x017e
            r2 = -1
        L_0x017e:
            int r1 = r6.A02
            if (r1 >= r3) goto L_0x0184
            int r2 = r2 + 1
        L_0x0184:
            if (r3 > r1) goto L_0x018b
            int r0 = r5.A01
            int r1 = r1 + r0
            r6.A02 = r1
        L_0x018b:
            int r1 = r5.A02
            if (r1 > r4) goto L_0x0194
            int r0 = r5.A01
            int r4 = r4 + r0
            r6.A01 = r4
        L_0x0194:
            int r1 = r1 + r2
            r5.A02 = r1
            r10.set(r8, r5)
            r10.set(r7, r6)
            goto L_0x0004
        L_0x019f:
            r2 = 1
        L_0x01a0:
            int r8 = r8 + -1
            goto L_0x000b
        L_0x01a4:
            r8 = -1
            goto L_0x001b
        L_0x01a7:
            java.util.ArrayList r0 = r14.A05
            int r8 = r0.size()
            r7 = 0
        L_0x01ae:
            if (r7 >= r8) goto L_0x0272
            java.util.ArrayList r0 = r14.A05
            java.lang.Object r6 = r0.get(r7)
            X.1Im r6 = (X.C21731Im) r6
            int r1 = r6.A00
            r0 = 1
            if (r1 == r0) goto L_0x026d
            r0 = 2
            if (r1 == r0) goto L_0x020b
            r0 = 4
            if (r1 == r0) goto L_0x01d0
            r0 = 8
            if (r1 == r0) goto L_0x026d
        L_0x01c7:
            r0 = 0
            if (r0 == 0) goto L_0x01cd
            r0.run()
        L_0x01cd:
            int r7 = r7 + 1
            goto L_0x01ae
        L_0x01d0:
            int r10 = r6.A02
            int r0 = r6.A01
            int r5 = r10 + r0
            r3 = r10
            r2 = 0
            r11 = -1
        L_0x01d9:
            r1 = 4
            if (r10 >= r5) goto L_0x0259
            X.0wd r0 = r14.A04
            X.1o8 r0 = r0.AZt(r10)
            if (r0 != 0) goto L_0x01fc
            boolean r0 = r14.A06(r10)
            if (r0 != 0) goto L_0x01fc
            if (r11 != r9) goto L_0x01f7
            java.lang.Object r0 = r6.A03
            X.1Im r0 = r14.BMn(r1, r3, r2, r0)
            r14.A03(r0)
            r3 = r10
            r2 = 0
        L_0x01f7:
            r11 = 0
        L_0x01f8:
            int r2 = r2 + r9
            int r10 = r10 + 1
            goto L_0x01d9
        L_0x01fc:
            if (r11 != 0) goto L_0x0209
            java.lang.Object r0 = r6.A03
            X.1Im r0 = r14.BMn(r1, r3, r2, r0)
            r14.A02(r0)
            r3 = r10
            r2 = 0
        L_0x0209:
            r11 = 1
            goto L_0x01f8
        L_0x020b:
            int r4 = r6.A02
            int r0 = r6.A01
            int r10 = r4 + r0
            r3 = r4
            r2 = 0
            r1 = -1
        L_0x0214:
            r12 = 0
            r11 = 2
            if (r3 >= r10) goto L_0x024b
            X.0wd r0 = r14.A04
            X.1o8 r0 = r0.AZt(r3)
            if (r0 != 0) goto L_0x023d
            boolean r0 = r14.A06(r3)
            if (r0 != 0) goto L_0x023d
            if (r1 != r9) goto L_0x023b
            X.1Im r0 = r14.BMn(r11, r4, r2, r12)
            r14.A03(r0)
            r0 = 1
        L_0x0230:
            r1 = 0
        L_0x0231:
            if (r0 == 0) goto L_0x0238
            int r3 = r3 - r2
            int r10 = r10 - r2
            r2 = 1
        L_0x0236:
            int r3 = r3 + r9
            goto L_0x0214
        L_0x0238:
            int r2 = r2 + 1
            goto L_0x0236
        L_0x023b:
            r0 = 0
            goto L_0x0230
        L_0x023d:
            if (r1 != 0) goto L_0x0249
            X.1Im r0 = r14.BMn(r11, r4, r2, r12)
            r14.A02(r0)
            r0 = 1
        L_0x0247:
            r1 = 1
            goto L_0x0231
        L_0x0249:
            r0 = 0
            goto L_0x0247
        L_0x024b:
            int r0 = r6.A01
            if (r2 == r0) goto L_0x0256
            r14.C0B(r6)
            X.1Im r6 = r14.BMn(r11, r4, r2, r12)
        L_0x0256:
            if (r1 != 0) goto L_0x026d
            goto L_0x0268
        L_0x0259:
            int r0 = r6.A01
            if (r2 == r0) goto L_0x0266
            java.lang.Object r0 = r6.A03
            r14.C0B(r6)
            X.1Im r6 = r14.BMn(r1, r3, r2, r0)
        L_0x0266:
            if (r11 != 0) goto L_0x026d
        L_0x0268:
            r14.A02(r6)
            goto L_0x01c7
        L_0x026d:
            r14.A03(r6)
            goto L_0x01c7
        L_0x0272:
            java.util.ArrayList r0 = r14.A05
            r0.clear()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16150wZ.A09():void");
    }

    public C21731Im BMn(int i, int i2, int i3, Object obj) {
        C21731Im r0 = (C21731Im) this.A01.ALa();
        if (r0 == null) {
            return new C21731Im(i, i2, i3, obj);
        }
        r0.A00 = i;
        r0.A02 = i2;
        r0.A01 = i3;
        r0.A03 = obj;
        return r0;
    }

    public void C0B(C21731Im r2) {
        if (!this.A03) {
            r2.A03 = null;
            this.A01.C0w(r2);
        }
    }

    public C16150wZ(C16190wd r3, boolean z) {
        this.A04 = r3;
        this.A03 = z;
        this.A02 = new C16200we(this);
    }

    public static void A05(C16150wZ r3, List list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            r3.C0B((C21731Im) list.get(i));
        }
        list.clear();
    }

    public void A08() {
        A07();
        int size = this.A05.size();
        for (int i = 0; i < size; i++) {
            C21731Im r7 = (C21731Im) this.A05.get(i);
            int i2 = r7.A00;
            if (i2 == 1) {
                this.A04.BWf(r7);
                this.A04.BMo(r7.A02, r7.A01);
            } else if (i2 == 2) {
                this.A04.BWf(r7);
                this.A04.BMq(r7.A02, r7.A01);
            } else if (i2 == 4) {
                this.A04.BWf(r7);
                this.A04.BK3(r7.A02, r7.A01, r7.A03);
            } else if (i2 == 8) {
                this.A04.BWf(r7);
                this.A04.BMp(r7.A02, r7.A01);
            }
            Runnable runnable = null;
            if (runnable != null) {
                runnable.run();
            }
        }
        A05(this, this.A05);
        this.A00 = 0;
    }
}
