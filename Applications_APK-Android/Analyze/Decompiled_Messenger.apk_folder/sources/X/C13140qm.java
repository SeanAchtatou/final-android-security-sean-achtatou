package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0qm  reason: invalid class name and case insensitive filesystem */
public abstract class C13140qm {
    public final View A00;

    public C13140qm A00(int i, int i2) {
        A01(new ViewGroup.LayoutParams(i, i2));
        return this;
    }

    public C13140qm A01(ViewGroup.LayoutParams layoutParams) {
        this.A00.setLayoutParams(layoutParams);
        return this;
    }

    public final void A02(int i) {
        this.A00.setId(i);
    }

    public C13140qm(View view) {
        this.A00 = view;
    }
}
