package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.concurrent.locks.ReentrantLock;

@UserScoped
/* renamed from: X.0pH  reason: invalid class name and case insensitive filesystem */
public final class C12400pH {
    private static C05540Zi A01;
    public final ReentrantLock A00 = new ReentrantLock();

    public static final C12400pH A00(AnonymousClass1XY r3) {
        C12400pH r0;
        synchronized (C12400pH.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r3)) {
                    A01.A01();
                    A01.A00 = new C12400pH();
                }
                C05540Zi r1 = A01;
                r0 = (C12400pH) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public C158817Xl A01() {
        this.A00.lock();
        return new C176138Bc(this);
    }

    public void A02() {
        this.A00.lock();
    }

    public void A03() {
        try {
            this.A00.unlock();
        } catch (IllegalMonitorStateException e) {
            C010708t.A0L("MessagingLock", "Failed to unlock", e);
        }
    }
}
