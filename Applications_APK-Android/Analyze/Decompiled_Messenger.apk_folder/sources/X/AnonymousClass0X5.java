package X;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0X5  reason: invalid class name */
public final class AnonymousClass0X5<T> extends AbstractCollection<T> implements Set<T> {
    public static final Object A04 = new Object();
    public int A00 = 0;
    public final AnonymousClass1XY A01;
    public final int[] A02;
    public final Object[] A03;

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public Iterator iterator() {
        return new C05140Xu(this);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.A03.length;
    }

    public AnonymousClass0X5(AnonymousClass1XY r2, int[] iArr) {
        this.A01 = (AnonymousClass1XY) r2.getScopeAwareInjector();
        this.A02 = iArr;
        this.A03 = new Object[iArr.length];
    }

    public boolean contains(Object obj) {
        Iterator it = iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (obj == null) {
                if (next == null) {
                    return true;
                }
            } else if (obj.equals(next)) {
                return true;
            }
        }
        return false;
    }
}
