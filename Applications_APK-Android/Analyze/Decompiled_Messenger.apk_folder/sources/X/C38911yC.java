package X;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1yC  reason: invalid class name and case insensitive filesystem */
public final class C38911yC {
    public final AnonymousClass2SE A00;

    public C38911yC(ScheduledExecutorService scheduledExecutorService) {
        this.A00 = new AnonymousClass2SE(scheduledExecutorService, new AHq(this));
        if (C39211yg.A00()) {
            AnonymousClass2SE r1 = this.A00;
            TimeUnit timeUnit = TimeUnit.SECONDS;
            synchronized (r1) {
                long j = (long) 30;
                r1.A03 = r1.A07.scheduleAtFixedRate(r1.A06, j, j, timeUnit);
            }
        }
    }
}
