package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Bv  reason: invalid class name and case insensitive filesystem */
public final class C20291Bv extends C16070wR {
    @Comparable(type = 3)
    public int A00 = -1;
    @Comparable(type = 3)
    public int A01 = 5;
    @Comparable(type = 3)
    public int A02 = 10;
    @Comparable(type = 3)
    public long A03 = 600;
    @Comparable(type = 13)
    public C23610BiW A04;
    @Comparable(type = 13)
    public AmJ A05;
    @Comparable(type = 13)
    public C21960Alj A06;
    public AnonymousClass0UN A07;
    public AnonymousClass10N A08;
    @Comparable(type = 14)
    public C74913ik A09;
    public C74723iN A0A;
    @Comparable(type = 13)
    public AnonymousClass654 A0B;
    @Comparable(type = 13)
    public Integer A0C;
    @Comparable(type = 13)
    public Object A0D;
    @Comparable(type = 13)
    public String A0E;
    @Comparable(type = 3)
    public boolean A0F = false;

    public C20291Bv(Context context) {
        super("BaseGraphQLConnectionSection");
        this.A07 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
        this.A09 = new C74913ik();
    }

    public C16070wR A0T(boolean z) {
        C20291Bv r1 = (C20291Bv) super.A0T(z);
        if (!z) {
            r1.A09 = new C74913ik();
        }
        return r1;
    }
}
