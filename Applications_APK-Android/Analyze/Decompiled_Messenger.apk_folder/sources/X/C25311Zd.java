package X;

import com.google.common.util.concurrent.ListenableFuture;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Zd  reason: invalid class name and case insensitive filesystem */
public final class C25311Zd {
    private static volatile C25311Zd A01;
    public AnonymousClass0UN A00;

    public static final C25311Zd A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C25311Zd.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C25311Zd(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public ListenableFuture A01(Runnable runnable, boolean z) {
        if (z) {
            return ((AnonymousClass0VL) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AZx, this.A00)).CIC(runnable);
        }
        return ((C06480bZ) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ACz, this.A00)).CIC(runnable);
    }

    private C25311Zd(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
