package X;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.TextView;

/* renamed from: X.1qF  reason: invalid class name and case insensitive filesystem */
public final class C34831qF extends C13460rT implements C34841qG {
    public static final String __redex_internal_original_name = "com.facebook.rtc.fragments.statusbar.v1.LegacyCallStatusBarFragment";
    public ViewStub A00;
    public TextView A01;
    public AnonymousClass0UN A02;

    public void C2D(C35051qb r4) {
        C35041qa r42 = (C35041qa) r4;
        if (r42.A01) {
            if (this.A01 == null) {
                this.A00.inflate();
                TextView textView = (TextView) A2K(2131296956);
                this.A01 = textView;
                textView.setOnClickListener(new C162127es(this));
            }
            this.A0I.setVisibility(0);
            this.A01.setText(r42.A00);
            return;
        }
        View view = this.A0I;
        boolean z = false;
        if (!(view == null || this.A01 == null || view.getVisibility() != 0)) {
            z = true;
        }
        if (z) {
            this.A0I.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int A022 = C000700l.A02(-1148135633);
        View inflate = layoutInflater.inflate(2132412296, viewGroup, false);
        C000700l.A08(448848848, A022);
        return inflate;
    }

    public void A1q() {
        int A022 = C000700l.A02(-1818475625);
        super.A1q();
        ((C34901qM) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AaT, this.A02)).A0J(this);
        C000700l.A08(-1166676681, A022);
    }

    public void A1r() {
        int A022 = C000700l.A02(-1601630632);
        super.A1r();
        ((C34901qM) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AaT, this.A02)).A0I();
        C000700l.A08(-1988610260, A022);
    }

    public void A1v(View view, Bundle bundle) {
        super.A1v(view, bundle);
        this.A00 = (ViewStub) A2K(2131296953);
    }

    public void A2M(Bundle bundle) {
        super.A2M(bundle);
        this.A02 = new AnonymousClass0UN(2, AnonymousClass1XX.get(A1j()));
    }
}
