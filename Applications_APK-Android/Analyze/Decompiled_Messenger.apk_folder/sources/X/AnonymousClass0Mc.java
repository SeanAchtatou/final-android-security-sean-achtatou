package X;

import com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger;

/* renamed from: X.0Mc  reason: invalid class name */
public final class AnonymousClass0Mc implements C03460Nv {
    public void ASL(String str) {
        ClassTracingLogger.classNotFound();
    }

    public void ASM(String str) {
        ClassTracingLogger.beginClassLoad(str);
    }

    public void ASK(String str, Class cls) {
        ClassTracingLogger.classLoaded(cls);
    }
}
