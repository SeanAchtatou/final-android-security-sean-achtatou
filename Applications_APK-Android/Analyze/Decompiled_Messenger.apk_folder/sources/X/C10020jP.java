package X;

/* renamed from: X.0jP  reason: invalid class name and case insensitive filesystem */
public final class C10020jP extends C26701bs {
    private static final long serialVersionUID = -800374828948534376L;
    public final String[] _typeNames;
    public final C10030jR[] _typeParameters;

    public boolean equals(Object obj) {
        int length;
        if (obj == this) {
            return true;
        }
        if (obj != null && obj.getClass() == getClass()) {
            C10020jP r9 = (C10020jP) obj;
            if (r9._class == this._class) {
                C10030jR[] r5 = this._typeParameters;
                C10030jR[] r4 = r9._typeParameters;
                if (r5 == null) {
                    return r4 == null || r4.length == 0;
                }
                if (r4 != null && (length = r5.length) == r4.length) {
                    int i = 0;
                    while (i < length) {
                        if (r5[i].equals(r4[i])) {
                            i++;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isContainerType() {
        return false;
    }

    public static C10020jP constructUnsafe(Class cls) {
        return new C10020jP(cls, null, null, null, null, false);
    }

    public C10030jR _narrow(Class cls) {
        return new C10020jP(cls, this._typeNames, this._typeParameters, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public String buildCanonicalName() {
        StringBuilder sb = new StringBuilder();
        sb.append(this._class.getName());
        C10030jR[] r5 = this._typeParameters;
        if (r5 != null && (r4 = r5.length) > 0) {
            sb.append('<');
            boolean z = true;
            for (C10030jR r1 : r5) {
                if (z) {
                    z = false;
                } else {
                    sb.append(',');
                }
                sb.append(r1.toCanonical());
            }
            sb.append('>');
        }
        return sb.toString();
    }

    public C10030jR containedType(int i) {
        C10030jR[] r1;
        if (i < 0 || (r1 = this._typeParameters) == null || i >= r1.length) {
            return null;
        }
        return r1[i];
    }

    public int containedTypeCount() {
        C10030jR[] r0 = this._typeParameters;
        if (r0 == null) {
            return 0;
        }
        return r0.length;
    }

    public String containedTypeName(int i) {
        String[] strArr;
        if (i < 0 || (strArr = this._typeNames) == null || i >= strArr.length) {
            return null;
        }
        return strArr[i];
    }

    public C10030jR narrowContentsBy(Class cls) {
        throw new IllegalArgumentException("Internal error: SimpleType.narrowContentsBy() should never be called");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(40);
        sb.append("[simple type, class ");
        sb.append(buildCanonicalName());
        sb.append(']');
        return sb.toString();
    }

    public C10030jR widenContentsBy(Class cls) {
        throw new IllegalArgumentException("Internal error: SimpleType.widenContentsBy() should never be called");
    }

    public C10030jR withContentTypeHandler(Object obj) {
        throw new IllegalArgumentException("Simple types have no content types; can not call withContenTypeHandler()");
    }

    public /* bridge */ /* synthetic */ C10030jR withContentValueHandler(Object obj) {
        throw new IllegalArgumentException("Simple types have no content types; can not call withContenValueHandler()");
    }

    public /* bridge */ /* synthetic */ C10030jR withStaticTyping() {
        boolean z = this._asStatic;
        if (z) {
            return this;
        }
        return new C10020jP(this._class, this._typeNames, this._typeParameters, this._valueHandler, this._typeHandler, z);
    }

    public /* bridge */ /* synthetic */ C10030jR withTypeHandler(Object obj) {
        return new C10020jP(this._class, this._typeNames, this._typeParameters, this._valueHandler, obj, this._asStatic);
    }

    public /* bridge */ /* synthetic */ C10030jR withValueHandler(Object obj) {
        Object obj2 = obj;
        if (obj == this._valueHandler) {
            return this;
        }
        return new C10020jP(this._class, this._typeNames, this._typeParameters, obj2, this._typeHandler, this._asStatic);
    }

    public C10020jP(Class cls) {
        this(cls, null, null, null, null, false);
    }

    public C10020jP(Class cls, String[] strArr, C10030jR[] r9, Object obj, Object obj2, boolean z) {
        super(cls, 0, obj, obj2, z);
        if (strArr == null || strArr.length == 0) {
            this._typeNames = null;
            this._typeParameters = null;
            return;
        }
        this._typeNames = strArr;
        this._typeParameters = r9;
    }
}
