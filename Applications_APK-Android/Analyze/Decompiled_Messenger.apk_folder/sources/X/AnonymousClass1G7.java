package X;

/* renamed from: X.1G7  reason: invalid class name */
public final class AnonymousClass1G7 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.loader.AbstractListenableFutureFbLoader$1";
    public final /* synthetic */ AnonymousClass1EP A00;
    public final /* synthetic */ Object A01;
    public final /* synthetic */ Object A02;
    public final /* synthetic */ boolean A03;

    public AnonymousClass1G7(AnonymousClass1EP r1, Object obj, Object obj2, boolean z) {
        this.A00 = r1;
        this.A01 = obj;
        this.A02 = obj2;
        this.A03 = z;
    }

    public void run() {
        this.A00.A01.Bgh(this.A01, this.A02);
        if (this.A03) {
            this.A00.A01.BdJ(this.A01, this.A02);
        }
    }
}
