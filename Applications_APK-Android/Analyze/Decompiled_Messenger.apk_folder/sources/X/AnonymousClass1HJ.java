package X;

import com.facebook.common.util.TriState;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1HJ  reason: invalid class name */
public final class AnonymousClass1HJ {
    private static volatile AnonymousClass1HJ A02;
    public TriState A00;
    public TriState A01;

    public static final AnonymousClass1HJ A00(AnonymousClass1XY r3) {
        if (A02 == null) {
            synchronized (AnonymousClass1HJ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A02 = new AnonymousClass1HJ();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public AnonymousClass1HJ() {
        TriState triState = TriState.UNSET;
        this.A00 = triState;
        this.A01 = triState;
    }
}
