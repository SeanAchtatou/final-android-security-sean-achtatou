package X;

import android.content.Context;

/* renamed from: X.1qx  reason: invalid class name and case insensitive filesystem */
public final class C35271qx {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public C04310Tq A04;
    public boolean A05 = false;
    public final Context A06;
    public final C72463eK A07 = null;
    public final AnonymousClass0p4 A08;
    public final C27321d0 A09 = C27321d0.A00;
    public final AnonymousClass1BC A0A;
    public final AnonymousClass1BY A0B;

    public C35271qx(Context context, AnonymousClass0p4 r3, AnonymousClass1BC r4, AnonymousClass1BY r5) {
        this.A08 = r3;
        this.A0A = r4;
        this.A0B = r5;
        this.A06 = context;
    }
}
