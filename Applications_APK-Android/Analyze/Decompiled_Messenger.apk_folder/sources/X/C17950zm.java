package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.0zm  reason: invalid class name and case insensitive filesystem */
public final class C17950zm implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadSummary(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadSummary[i];
    }
}
