package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1es  reason: invalid class name and case insensitive filesystem */
public final class C28481es {
    private static volatile C28481es A01;
    public final AnonymousClass0jD A00;

    public static final C28481es A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C28481es.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C28481es(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C28481es(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0jD.A00(r2);
    }
}
