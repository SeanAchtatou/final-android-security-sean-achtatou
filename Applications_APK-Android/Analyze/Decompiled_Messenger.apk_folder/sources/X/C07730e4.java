package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0e4  reason: invalid class name and case insensitive filesystem */
public final class C07730e4 implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ C09340h3 A00;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public C07730e4(C09340h3 r1) {
        this.A00 = r1;
    }

    public void onActivityResumed(Activity activity) {
        AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BKH, this.A00.A03), new AnonymousClass8U0(this), 1703118411);
    }
}
