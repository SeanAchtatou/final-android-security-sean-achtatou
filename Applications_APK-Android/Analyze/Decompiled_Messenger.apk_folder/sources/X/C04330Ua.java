package X;

import com.google.common.base.Preconditions;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.0Ua  reason: invalid class name and case insensitive filesystem */
public final class C04330Ua {
    private static final Logger A02 = Logger.getLogger(C04330Ua.class.getName());
    private C25161Yo A00;
    private boolean A01;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r2 == null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        A00(r2.A01, r2.A02);
        r2 = r2.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        if (r1 == null) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        r0 = r1.A00;
        r1.A00 = r2;
        r2 = r1;
        r1 = r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.A01     // Catch:{ all -> 0x0026 }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r3)     // Catch:{ all -> 0x0026 }
            return
        L_0x0007:
            r0 = 1
            r3.A01 = r0     // Catch:{ all -> 0x0026 }
            X.1Yo r1 = r3.A00     // Catch:{ all -> 0x0026 }
            r2 = 0
            r3.A00 = r2     // Catch:{ all -> 0x0026 }
            monitor-exit(r3)     // Catch:{ all -> 0x0026 }
        L_0x0010:
            if (r1 == 0) goto L_0x0019
            X.1Yo r0 = r1.A00
            r1.A00 = r2
            r2 = r1
            r1 = r0
            goto L_0x0010
        L_0x0019:
            if (r2 == 0) goto L_0x0025
            java.lang.Runnable r1 = r2.A01
            java.util.concurrent.Executor r0 = r2.A02
            A00(r1, r0)
            X.1Yo r2 = r2.A00
            goto L_0x0019
        L_0x0025:
            return
        L_0x0026:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0026 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04330Ua.A01():void");
    }

    public void A02(Runnable runnable, Executor executor) {
        Preconditions.checkNotNull(runnable, "Runnable was null.");
        Preconditions.checkNotNull(executor, "Executor was null.");
        synchronized (this) {
            if (!this.A01) {
                this.A00 = new C25161Yo(runnable, executor, this.A00);
            } else {
                A00(runnable, executor);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.RuntimeException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static void A00(Runnable runnable, Executor executor) {
        try {
            AnonymousClass07A.A04(executor, runnable, -1933206678);
        } catch (RuntimeException e) {
            Logger logger = A02;
            Level level = Level.SEVERE;
            logger.log(level, "RuntimeException while executing runnable " + runnable + " with executor " + executor, (Throwable) e);
        }
    }
}
