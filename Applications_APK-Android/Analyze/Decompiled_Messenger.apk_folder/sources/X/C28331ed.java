package X;

/* renamed from: X.1ed  reason: invalid class name and case insensitive filesystem */
public final class C28331ed extends AnonymousClass1CA {
    private static final long serialVersionUID = -7834910259750909424L;

    public static C28331ed construct(Class cls, C10030jR r7) {
        return new C28331ed(cls, r7, null, null, false);
    }

    public C10030jR _narrow(Class cls) {
        return new C28331ed(cls, this._elementType, null, null, this._asStatic);
    }

    public C10030jR narrowContentsBy(Class cls) {
        C10030jR r1 = this._elementType;
        if (cls == r1._class) {
            return this;
        }
        return new C28331ed(this._class, r1.narrowBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public String toString() {
        return "[collection type; class " + this._class.getName() + ", contains " + this._elementType + "]";
    }

    public C10030jR widenContentsBy(Class cls) {
        C10030jR r1 = this._elementType;
        if (cls == r1._class) {
            return this;
        }
        return new C28331ed(this._class, r1.widenBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    private C28331ed(Class cls, C10030jR r2, Object obj, Object obj2, boolean z) {
        super(cls, r2, obj, obj2, z);
    }

    /* access modifiers changed from: private */
    public C28331ed withContentTypeHandler(Object obj) {
        return new C28331ed(this._class, this._elementType.withTypeHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* access modifiers changed from: private */
    public C28331ed withContentValueHandler(Object obj) {
        return new C28331ed(this._class, this._elementType.withValueHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* access modifiers changed from: private */
    public C28331ed withStaticTyping() {
        if (this._asStatic) {
            return this;
        }
        return new C28331ed(this._class, this._elementType.withStaticTyping(), this._valueHandler, this._typeHandler, true);
    }
}
