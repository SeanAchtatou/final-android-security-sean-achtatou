package X;

/* renamed from: X.08a  reason: invalid class name and case insensitive filesystem */
public final class C009308a {
    public static final C001300x A00;
    public static final C001300x A01;
    public static final C001300x A02;

    static {
        C001400y r5 = C001400y.DEVELOPMENT;
        C001500z r6 = C001500z.A07;
        A00 = new C001300x("orca-dev", "181425161904154", "29695f68d8dfa9d6a9cb4662735c9aff", "95a15d22a0e735b2983ecb9759dbaf91", r5, r6, AnonymousClass010.DEBUG);
        A01 = new C001300x("orca-in-house", "105910932827969", "3fd89d7c8cf293c5c6db88444077422f", "201f1a1fa4998b746f7b531e6434c224", C001400y.FACEBOOK, r6, AnonymousClass010.IN_HOUSE);
        A02 = new C001300x("orca-prod", "256002347743983", "256002347743983", "374e60f8b9bb6b8cbb30f78030438895", C001400y.PUBLIC, r6, AnonymousClass010.PROD);
    }
}
