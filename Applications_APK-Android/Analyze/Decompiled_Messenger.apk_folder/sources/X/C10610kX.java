package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.callercontext.CallerContext;

/* renamed from: X.0kX  reason: invalid class name and case insensitive filesystem */
public final class C10610kX implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new CallerContext(parcel);
    }

    public Object[] newArray(int i) {
        return new CallerContext[i];
    }
}
