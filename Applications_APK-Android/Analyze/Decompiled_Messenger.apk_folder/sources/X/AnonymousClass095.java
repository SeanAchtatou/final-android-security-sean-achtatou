package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/* renamed from: X.095  reason: invalid class name */
public final class AnonymousClass095 {
    private static AnonymousClass095 A05;
    public static final boolean A06;
    public static final boolean A07;
    public static final String[] A08;
    private static final Object A09 = new Object();
    private String A00 = null;
    private boolean A01 = false;
    public final Context A02;
    private final Object A03 = new Object();
    private volatile boolean A04 = false;

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0013, code lost:
        if (r5 > 23) goto L_0x0015;
     */
    static {
        /*
            int r5 = android.os.Build.VERSION.SDK_INT
            r4 = 0
            r3 = 1
            r0 = 24
            r2 = 0
            if (r5 < r0) goto L_0x000a
            r2 = 1
        L_0x000a:
            X.AnonymousClass095.A07 = r2
            r0 = 21
            if (r5 < r0) goto L_0x0015
            r0 = 23
            r1 = 1
            if (r5 <= r0) goto L_0x0016
        L_0x0015:
            r1 = 0
        L_0x0016:
            X.AnonymousClass095.A06 = r1
            r0 = 3
            if (r2 == 0) goto L_0x0037
            java.lang.String[] r1 = new java.lang.String[r0]
            X.AnonymousClass095.A08 = r1
            java.lang.String r0 = "zyte_v7.prof"
            r1[r4] = r0
        L_0x0023:
            java.lang.String[] r2 = X.AnonymousClass095.A08
            int r1 = r3 + 1
            java.lang.String r0 = "zyte.prof"
            r2[r3] = r0
            java.lang.String r0 = "art_pgo_input.txt"
            r2[r1] = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            X.AnonymousClass095.A09 = r0
            return
        L_0x0037:
            if (r1 == 0) goto L_0x0042
            java.lang.String[] r1 = new java.lang.String[r0]
            X.AnonymousClass095.A08 = r1
            java.lang.String r0 = "zyte_v5.prof"
            r1[r4] = r0
            goto L_0x0023
        L_0x0042:
            r0 = 2
            java.lang.String[] r0 = new java.lang.String[r0]
            X.AnonymousClass095.A08 = r0
            r3 = 0
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass095.<clinit>():void");
    }

    public static AnonymousClass095 A00(Context context) {
        AnonymousClass095 r0;
        AnonymousClass095 r02 = A05;
        if (r02 != null) {
            return r02;
        }
        synchronized (A09) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            r0 = new AnonymousClass095(context);
            A05 = r0;
        }
        return r0;
    }

    private static InputStream A01(Context context, String str, boolean z) {
        String A0P;
        if (str == null) {
            A0P = null;
        } else {
            A0P = AnonymousClass08S.A0P("secondary-program-dex-jars", File.separator, str);
        }
        if (A0P == null) {
            return null;
        }
        if (z) {
            A0P = AnonymousClass08S.A0J(A0P, ".xz");
        }
        try {
            return context.getAssets().open(A0P);
        } catch (IOException e) {
            e.getMessage();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String A02(AnonymousClass095 r4) {
        String str;
        String replace;
        String str2 = r4.A00;
        if (str2 != null) {
            return str2;
        }
        Context context = r4.A02;
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        try {
            str = packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            str = UUID.randomUUID().toString();
            Log.e("PGOProfileUtil", String.format("Could not find package name %s. Using UUID: %s", packageName, str), e);
        }
        if (str == null) {
            replace = null;
        } else {
            replace = str.replace('.', '_');
        }
        String format = String.format("%s_AV%d_%s%s", "art_pgo_profile", Integer.valueOf(Build.VERSION.SDK_INT), replace, ".prof");
        r4.A00 = format;
        return format;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:87|(2:89|90)|91|92) */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x009b, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00ca, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:77:0x00ce */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x00e0 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A03(X.AnonymousClass095 r14) {
        /*
            boolean r0 = r14.A04
            if (r0 != 0) goto L_0x00eb
            java.lang.Object r4 = r14.A03
            monitor-enter(r4)
            boolean r0 = r14.A04     // Catch:{ all -> 0x00e8 }
            if (r0 != 0) goto L_0x00e6
            java.lang.String r1 = A02(r14)     // Catch:{ all -> 0x00e8 }
            android.content.Context r0 = r14.A02     // Catch:{ all -> 0x00e8 }
            java.io.File r2 = r0.getFileStreamPath(r1)     // Catch:{ all -> 0x00e8 }
            boolean r0 = r2.exists()     // Catch:{ all -> 0x00e8 }
            if (r0 == 0) goto L_0x0021
            r2.getAbsolutePath()     // Catch:{ all -> 0x00e8 }
            r0 = 1
            goto L_0x00e1
        L_0x0021:
            android.content.Context r7 = r14.A02     // Catch:{ all -> 0x00e8 }
            r5 = 0
            r6 = 0
            java.util.zip.ZipFile r9 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x00cf }
            android.content.pm.ApplicationInfo r0 = r7.getApplicationInfo()     // Catch:{ IOException -> 0x00cf }
            java.lang.String r0 = r0.sourceDir     // Catch:{ IOException -> 0x00cf }
            r9.<init>(r0)     // Catch:{ IOException -> 0x00cf }
            java.lang.String[] r3 = X.AnonymousClass095.A08     // Catch:{ IOException -> 0x00cf }
            int r11 = r3.length     // Catch:{ IOException -> 0x00cf }
            r10 = 0
        L_0x0034:
            r13 = 1
            if (r10 >= r11) goto L_0x006e
            r8 = r3[r10]     // Catch:{ IOException -> 0x00cf }
            if (r8 == 0) goto L_0x006b
            r12 = r8
            if (r13 == 0) goto L_0x0044
            java.lang.String r0 = ".xz"
            java.lang.String r12 = X.AnonymousClass08S.A0J(r8, r0)     // Catch:{ IOException -> 0x00cf }
        L_0x0044:
            java.util.zip.ZipEntry r0 = r9.getEntry(r12)     // Catch:{ IOException -> 0x00cf }
            if (r0 == 0) goto L_0x0050
            X.0Mt r1 = new X.0Mt     // Catch:{ IOException -> 0x00cf }
            r1.<init>(r0, r12, r13)     // Catch:{ IOException -> 0x00cf }
            goto L_0x0051
        L_0x0050:
            r1 = r5
        L_0x0051:
            if (r1 != 0) goto L_0x0072
            if (r6 == 0) goto L_0x005b
            java.lang.String r0 = ".xz"
            java.lang.String r8 = X.AnonymousClass08S.A0J(r8, r0)     // Catch:{ IOException -> 0x00cf }
        L_0x005b:
            java.util.zip.ZipEntry r0 = r9.getEntry(r8)     // Catch:{ IOException -> 0x00cf }
            if (r0 == 0) goto L_0x0067
            X.0Mt r1 = new X.0Mt     // Catch:{ IOException -> 0x00cf }
            r1.<init>(r0, r8, r6)     // Catch:{ IOException -> 0x00cf }
            goto L_0x0068
        L_0x0067:
            r1 = r5
        L_0x0068:
            if (r1 == 0) goto L_0x006b
            goto L_0x0072
        L_0x006b:
            int r10 = r10 + 1
            goto L_0x0034
        L_0x006e:
            java.util.Arrays.toString(r3)     // Catch:{ IOException -> 0x00cf }
            r1 = r5
        L_0x0072:
            if (r1 == 0) goto L_0x007f
            java.util.zip.ZipEntry r0 = r1.A01     // Catch:{ IOException -> 0x00cf }
            if (r0 == 0) goto L_0x007f
            java.io.InputStream r5 = r9.getInputStream(r0)     // Catch:{ IOException -> 0x00cf }
            boolean r0 = r1.A02     // Catch:{ IOException -> 0x00cf }
            goto L_0x009f
        L_0x007f:
            r1 = 0
        L_0x0080:
            int r0 = r3.length     // Catch:{ IOException -> 0x00cf }
            if (r1 >= r0) goto L_0x009b
            r0 = r3[r1]     // Catch:{ IOException -> 0x00cf }
            if (r0 == 0) goto L_0x0097
            java.io.InputStream r0 = A01(r7, r0, r13)     // Catch:{ IOException -> 0x00cf }
            if (r0 == 0) goto L_0x008e
            goto L_0x009d
        L_0x008e:
            r0 = r3[r1]     // Catch:{ IOException -> 0x00cf }
            java.io.InputStream r0 = A01(r7, r0, r6)     // Catch:{ IOException -> 0x00cf }
            if (r0 == 0) goto L_0x0097
            goto L_0x009a
        L_0x0097:
            int r1 = r1 + 1
            goto L_0x0080
        L_0x009a:
            r5 = r0
        L_0x009b:
            r0 = 0
            goto L_0x009f
        L_0x009d:
            r5 = r0
            r0 = 1
        L_0x009f:
            if (r5 != 0) goto L_0x00a3
            r0 = 0
            goto L_0x00e1
        L_0x00a3:
            if (r0 == 0) goto L_0x00ab
            com.facebook.xzdecoder.XzInputStream r0 = new com.facebook.xzdecoder.XzInputStream     // Catch:{ IOException -> 0x00cf }
            r0.<init>(r5)     // Catch:{ IOException -> 0x00cf }
            r5 = r0
        L_0x00ab:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00cf }
            r3.<init>(r2)     // Catch:{ IOException -> 0x00cf }
            r0 = 512(0x200, float:7.175E-43)
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x00c8 }
        L_0x00b4:
            int r1 = r5.read(r2)     // Catch:{ all -> 0x00c8 }
            if (r1 <= 0) goto L_0x00be
            r3.write(r2, r6, r1)     // Catch:{ all -> 0x00c8 }
            goto L_0x00b4
        L_0x00be:
            r3.close()     // Catch:{ IOException -> 0x00cf }
            if (r5 == 0) goto L_0x00c6
            r5.close()     // Catch:{ IOException -> 0x00c6 }
        L_0x00c6:
            r0 = 1
            goto L_0x00e1
        L_0x00c8:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00ca }
        L_0x00ca:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x00ce }
        L_0x00ce:
            throw r0     // Catch:{ IOException -> 0x00cf }
        L_0x00cf:
            r0 = move-exception
            r0.getMessage()     // Catch:{ all -> 0x00da }
            if (r5 == 0) goto L_0x00d8
            r5.close()     // Catch:{ IOException -> 0x00d8 }
        L_0x00d8:
            r0 = 0
            goto L_0x00e1
        L_0x00da:
            r0 = move-exception
            if (r5 == 0) goto L_0x00e0
            r5.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x00e0:
            throw r0     // Catch:{ all -> 0x00e8 }
        L_0x00e1:
            r14.A01 = r0     // Catch:{ all -> 0x00e8 }
            r0 = 1
            r14.A04 = r0     // Catch:{ all -> 0x00e8 }
        L_0x00e6:
            monitor-exit(r4)     // Catch:{ all -> 0x00e8 }
            goto L_0x00eb
        L_0x00e8:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00e8 }
            throw r0
        L_0x00eb:
            boolean r0 = r14.A01
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass095.A03(X.095):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0061 A[SYNTHETIC, Splitter:B:37:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0066 A[Catch:{ all -> 0x006a }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File A05(java.io.File r8) {
        /*
            r7 = this;
            if (r8 == 0) goto L_0x0073
            java.io.File r6 = new java.io.File
            java.lang.String r0 = "art_pgo_ref_profile.prof"
            r6.<init>(r8, r0)
            r5 = 0
            if (r6 == 0) goto L_0x0072
            java.io.File r4 = r7.A04()
            if (r4 == 0) goto L_0x0072
            boolean r0 = r4.exists()
            if (r0 == 0) goto L_0x0072
            boolean r0 = r6.exists()
            if (r0 == 0) goto L_0x0027
            r6.getAbsolutePath()
            r4.getAbsolutePath()
            r6.delete()
        L_0x0027:
            r0 = 2147483647(0x7fffffff, float:NaN)
            r3 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x005b }
            r2.<init>(r4)     // Catch:{ all -> 0x005b }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x005e }
            r1.<init>(r6)     // Catch:{ all -> 0x005e }
            int r0 = X.AnonymousClass0Me.A09(r1, r2, r0)     // Catch:{ all -> 0x0058 }
            r2.close()     // Catch:{ all -> 0x006a }
            r1.close()     // Catch:{ all -> 0x006a }
            r1 = 0
            if (r0 <= 0) goto L_0x0043
            r1 = 1
        L_0x0043:
            r4.getAbsolutePath()     // Catch:{ all -> 0x0056 }
            r6.getAbsolutePath()     // Catch:{ all -> 0x0056 }
            if (r1 != 0) goto L_0x004f
            r6.delete()
            return r5
        L_0x004f:
            if (r1 != 0) goto L_0x0055
            r6.delete()
            return r6
        L_0x0055:
            return r6
        L_0x0056:
            r0 = move-exception
            goto L_0x006c
        L_0x0058:
            r0 = move-exception
            r3 = r1
            goto L_0x005f
        L_0x005b:
            r0 = move-exception
            r2 = r5
            goto L_0x005f
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            if (r2 == 0) goto L_0x0064
            r2.close()     // Catch:{ all -> 0x006a }
        L_0x0064:
            if (r3 == 0) goto L_0x0069
            r3.close()     // Catch:{ all -> 0x006a }
        L_0x0069:
            throw r0     // Catch:{ all -> 0x006a }
        L_0x006a:
            r0 = move-exception
            r1 = 0
        L_0x006c:
            if (r1 != 0) goto L_0x0071
            r6.delete()
        L_0x0071:
            throw r0
        L_0x0072:
            return r5
        L_0x0073:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass095.A05(java.io.File):java.io.File");
    }

    private AnonymousClass095(Context context) {
        this.A02 = context;
    }

    public File A04() {
        if (!A03(this)) {
            return null;
        }
        return this.A02.getFileStreamPath(A02(this));
    }
}
