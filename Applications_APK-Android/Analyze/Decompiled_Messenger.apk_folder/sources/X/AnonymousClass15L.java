package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.15L  reason: invalid class name */
public final class AnonymousClass15L {
    public final String A00;
    public final String A01;
    public final Map A02;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String str = this.A00;
        if (str != null && !str.isEmpty()) {
            sb.append(str);
        }
        String str2 = this.A01;
        if (str2 != null && !str2.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(":");
            }
            sb.append(str2);
        }
        return sb.toString();
    }

    public AnonymousClass15L(String str, String str2, Map map) {
        HashMap hashMap;
        this.A01 = str;
        this.A00 = str2;
        if (map == null) {
            this.A02 = hashMap;
            return;
        }
        hashMap = new HashMap(map);
        this.A02 = hashMap;
    }
}
