package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.SurfaceHolder;

/* renamed from: X.0L2  reason: invalid class name */
public final class AnonymousClass0L2 extends Handler {
    public final /* synthetic */ C009408b A00;

    public AnonymousClass0L2(C009408b r1) {
        this.A00 = r1;
    }

    public void handleMessage(Message message) {
        C009408b r3 = this.A00;
        synchronized (r3) {
            int i = message.what;
            if (i != 0) {
                if (i == 1) {
                    int i2 = r3.A02;
                    if (i2 == 1 && message.arg1 == 0) {
                        r3.A0J();
                        r3.A02 = message.arg1;
                    } else if (i2 == 0 && message.arg1 == 1) {
                        r3.A0P(r3.A05, r3.A04, r3.A03);
                        r3.A02 = message.arg1;
                        r3.A0N();
                    } else {
                        int i3 = message.arg1;
                        if (!(i3 == 0 || i3 == 1)) {
                            throw new AssertionError("unrecognized state");
                        }
                    }
                } else if (i == 2) {
                    SurfaceHolder surfaceHolder = r3.A05;
                    long nanoTime = System.nanoTime();
                    r3.A0M();
                    r3.A0Q(surfaceHolder, nanoTime);
                    if (r3.A01 > 0) {
                        r3.A00++;
                    }
                } else if (i == 3) {
                    r3.A0O(System.nanoTime());
                } else {
                    throw new AssertionError("unknown message " + message);
                }
                r3.notifyAll();
            } else {
                Looper.myLooper().quit();
            }
        }
    }
}
