package X;

import android.net.Uri;

/* renamed from: X.1Mc  reason: invalid class name and case insensitive filesystem */
public class C22601Mc {
    public static C22601Mc A00;

    public Uri A02(Uri uri) {
        return (!(this instanceof C22591Mb) || !C006206h.A06(uri)) ? uri : C006306i.A00(uri);
    }

    public C23601Qd A03(AnonymousClass1Q0 r3, Uri uri, Object obj) {
        if (!(this instanceof C22591Mb)) {
            return new C34031oX(A02(uri).toString());
        }
        return new C33171n8(C006306i.A00(uri).toString());
    }

    public C23601Qd A04(AnonymousClass1Q0 r9, Object obj) {
        if (!(this instanceof C22591Mb)) {
            return new C23591Qc(A02(r9.A02).toString(), r9.A06, r9.A07, r9.A04, null, null);
        }
        return C22591Mb.A00((C22591Mb) this, r9, obj, null, null);
    }

    public C23601Qd A05(AnonymousClass1Q0 r2, Object obj) {
        return A03(r2, r2.A02, obj);
    }

    public C23601Qd A06(AnonymousClass1Q0 r9, Object obj) {
        String str;
        if (!(this instanceof C22591Mb)) {
            AnonymousClass32I r0 = r9.A0B;
            C23601Qd r6 = null;
            if (r0 != null) {
                r6 = r0.Ayn();
                str = r0.getClass().getName();
            } else {
                str = null;
            }
            return new C23591Qc(A02(r9.A02).toString(), r9.A06, r9.A07, r9.A04, r6, str);
        }
        C22591Mb r2 = (C22591Mb) this;
        AnonymousClass32I r02 = r9.A0B;
        if (r02 != null) {
            return C22591Mb.A00(r2, r9, obj, r02.Ayn(), r02.getClass().getName());
        }
        return C22591Mb.A00(r2, r9, obj, null, null);
    }
}
