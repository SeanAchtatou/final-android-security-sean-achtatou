package X;

import android.content.Context;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1uc  reason: invalid class name and case insensitive filesystem */
public final class C37071uc extends AnonymousClass1YH {
    private static volatile C37071uc A00;

    public static final C37071uc A00(AnonymousClass1XY r7) {
        if (A00 == null) {
            synchronized (C37071uc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A00 = new C37071uc(AnonymousClass1YA.A00(applicationInjector), C04720Vx.A01(applicationInjector), C158657Vv.A00(applicationInjector), "inapp_browser_db");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private C37071uc(Context context, C04740Vz r3, C158657Vv r4, String str) {
        super(context, r3, ImmutableList.of(r4), str);
    }
}
