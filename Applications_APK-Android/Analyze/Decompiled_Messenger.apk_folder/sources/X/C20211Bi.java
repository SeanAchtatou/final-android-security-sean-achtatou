package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;

/* renamed from: X.1Bi  reason: invalid class name and case insensitive filesystem */
public final class C20211Bi {
    public static final Class A01 = C20211Bi.class;
    public final DeprecatedAnalyticsLogger A00;

    public static final C20211Bi A00(AnonymousClass1XY r2) {
        return new C20211Bi(C06920cI.A00(r2));
    }

    private C20211Bi(DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        this.A00 = deprecatedAnalyticsLogger;
    }
}
