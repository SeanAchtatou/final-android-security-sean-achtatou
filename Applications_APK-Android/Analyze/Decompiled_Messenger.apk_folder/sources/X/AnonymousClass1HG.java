package X;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1HG  reason: invalid class name */
public final class AnonymousClass1HG {
    public static final C08900gA A02 = C08870g7.A1h;
    public final AnonymousClass09P A00;
    public final C185414b A01;

    public static final AnonymousClass1HG A00(AnonymousClass1XY r1) {
        return new AnonymousClass1HG(r1);
    }

    public static void A01(AnonymousClass1HG r6, Map map) {
        JSONObject jSONObject;
        String str;
        if (map == null) {
            jSONObject = null;
        } else {
            jSONObject = new JSONObject();
            try {
                for (Map.Entry entry : map.entrySet()) {
                    jSONObject.put((String) entry.getKey(), entry.getValue());
                }
            } catch (JSONException e) {
                AnonymousClass09P r3 = r6.A00;
                AnonymousClass06G A022 = AnonymousClass06F.A02(AnonymousClass08S.A0J("messenger_games_", "json_gen_from_map"), "json exception while creating action tag");
                A022.A03 = e;
                r3.CGQ(A022.A00());
            }
        }
        C185414b r32 = r6.A01;
        C08900gA r2 = A02;
        String A002 = C855543x.A00(AnonymousClass07B.A0C);
        if (jSONObject != null) {
            str = jSONObject.toString();
        } else {
            str = null;
        }
        r32.AOM(r2, A002, str);
    }

    public AnonymousClass1HG(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass14R.A00(r2);
        this.A00 = C04750Wa.A01(r2);
    }
}
