package X;

import java.nio.ByteBuffer;

/* renamed from: X.1T9  reason: invalid class name */
public final class AnonymousClass1T9 extends C06220b5 {
    public float A06() {
        int A02 = A02(14);
        if (A02 != 0) {
            return this.A01.getFloat(A02 + this.A00);
        }
        return 0.0f;
    }

    public C36851ty A07() {
        C36851ty r2 = new C36851ty();
        int A02 = A02(10);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public String A08() {
        int A02 = A02(12);
        if (A02 != 0) {
            return A05(A02 + this.A00);
        }
        return null;
    }

    public String A09() {
        int A02 = A02(8);
        if (A02 != 0) {
            return A05(A02 + this.A00);
        }
        return null;
    }

    public boolean A0A() {
        int A02 = A02(18);
        if (A02 == 0 || this.A01.get(A02 + this.A00) == 0) {
            return false;
        }
        return true;
    }
}
