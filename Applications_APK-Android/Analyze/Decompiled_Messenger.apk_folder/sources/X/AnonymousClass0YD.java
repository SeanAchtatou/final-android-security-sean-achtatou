package X;

/* renamed from: X.0YD  reason: invalid class name */
public final class AnonymousClass0YD extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.RetryOutOfThreadsThread";

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0020, code lost:
        if (r1.endsWith("failed: Try again") == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void start() {
        /*
            r5 = this;
            monitor-enter(r5)
            r3 = 0
        L_0x0002:
            super.start()     // Catch:{ OutOfMemoryError -> 0x0006 }
            goto L_0x003d
        L_0x0006:
            r4 = move-exception
            boolean r0 = r5.isAlive()     // Catch:{ all -> 0x005e }
            if (r0 != 0) goto L_0x005d
            java.lang.String r1 = r4.getMessage()     // Catch:{ all -> 0x005e }
            java.lang.String r0 = "pthread_create"
            boolean r0 = r1.startsWith(r0)     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x0022
            java.lang.String r0 = "failed: Try again"
            boolean r1 = r1.endsWith(r0)     // Catch:{ all -> 0x005e }
            r0 = 1
            if (r1 != 0) goto L_0x0023
        L_0x0022:
            r0 = 0
        L_0x0023:
            if (r0 == 0) goto L_0x005d
            r0 = 3
            if (r3 >= r0) goto L_0x0046
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x005e }
            r1 = 24
            r0 = 0
            if (r2 < r1) goto L_0x0030
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x0046
            if (r3 <= 0) goto L_0x003a
            int r0 = r3 * 500
            long r0 = (long) r0     // Catch:{ all -> 0x005e }
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x003f }
        L_0x003a:
            int r3 = r3 + 1
            goto L_0x0002
        L_0x003d:
            monitor-exit(r5)
            return
        L_0x003f:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x005e }
            r0.<init>(r1)     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x005e }
        L_0x0046:
            java.lang.OutOfMemoryError r3 = new java.lang.OutOfMemoryError     // Catch:{ all -> 0x005e }
            java.lang.String r2 = r4.getMessage()     // Catch:{ all -> 0x005e }
            java.lang.String r1 = " JavaThreads:"
            int r0 = java.lang.Thread.activeCount()     // Catch:{ all -> 0x005e }
            java.lang.String r0 = X.AnonymousClass08S.A0L(r2, r1, r0)     // Catch:{ all -> 0x005e }
            r3.<init>(r0)     // Catch:{ all -> 0x005e }
            r3.initCause(r4)     // Catch:{ all -> 0x005e }
            throw r3     // Catch:{ all -> 0x005e }
        L_0x005d:
            throw r4     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0YD.start():void");
    }

    public AnonymousClass0YD(Runnable runnable, String str) {
        super(runnable, str);
    }
}
