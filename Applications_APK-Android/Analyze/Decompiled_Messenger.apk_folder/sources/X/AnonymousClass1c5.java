package X;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.ArrayBlockingQueueDeserializer;
import com.fasterxml.jackson.databind.deser.std.CollectionDeserializer;
import com.fasterxml.jackson.databind.deser.std.EnumDeserializer;
import com.fasterxml.jackson.databind.deser.std.EnumMapDeserializer;
import com.fasterxml.jackson.databind.deser.std.EnumSetDeserializer;
import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.fasterxml.jackson.databind.deser.std.MapDeserializer;
import com.fasterxml.jackson.databind.deser.std.ObjectArrayDeserializer;
import com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers;
import com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringCollectionDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.PrintStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/* renamed from: X.1c5  reason: invalid class name */
public abstract class AnonymousClass1c5 extends AnonymousClass1c6 implements Serializable {
    public static final Class CLASS_CHAR_BUFFER = CharSequence.class;
    public static final Class CLASS_ITERABLE = Iterable.class;
    public static final Class CLASS_OBJECT = Object.class;
    public static final Class CLASS_STRING = String.class;
    public static final HashMap _collectionFallbacks;
    public static final HashMap _mapFallbacks;
    public final AnonymousClass1c8 _factoryConfig;

    public JsonDeserializer createCollectionLikeDeserializer(C26791c3 r9, AnonymousClass1CA r10, C10120ja r11) {
        JsonDeserializer jsonDeserializer;
        AnonymousClass1CA r3 = r10;
        C10030jR contentType = r10.getContentType();
        JsonDeserializer jsonDeserializer2 = (JsonDeserializer) contentType.getValueHandler();
        C10490kF r4 = r9._config;
        C64433Bp r6 = (C64433Bp) contentType.getTypeHandler();
        if (r6 == null) {
            r6 = findTypeDeserializer(r4, contentType);
        }
        Iterator it = new C29161fy(this._factoryConfig._additionalDeserializers).iterator();
        while (true) {
            if (!it.hasNext()) {
                jsonDeserializer = null;
                break;
            }
            jsonDeserializer = ((AnonymousClass1c7) it.next()).findCollectionLikeDeserializer(r3, r4, r11, r6, jsonDeserializer2);
            if (jsonDeserializer != null) {
                break;
            }
        }
        if (jsonDeserializer != null) {
            AnonymousClass1c8 r1 = this._factoryConfig;
            if (r1.hasDeserializerModifiers()) {
                Iterator it2 = new C29161fy(r1._modifiers).iterator();
                while (it2.hasNext()) {
                    it2.next();
                }
            }
        }
        return jsonDeserializer;
    }

    public JsonDeserializer createMapLikeDeserializer(C26791c3 r10, C21991Jm r11, C10120ja r12) {
        JsonDeserializer jsonDeserializer;
        C21991Jm r3 = r11;
        C10030jR keyType = r11.getKeyType();
        C10030jR contentType = r11.getContentType();
        C10490kF r4 = r10._config;
        JsonDeserializer jsonDeserializer2 = (JsonDeserializer) contentType.getValueHandler();
        C422628y r6 = (C422628y) keyType.getValueHandler();
        C64433Bp r7 = (C64433Bp) contentType.getTypeHandler();
        if (r7 == null) {
            r7 = findTypeDeserializer(r4, contentType);
        }
        Iterator it = new C29161fy(this._factoryConfig._additionalDeserializers).iterator();
        while (true) {
            if (!it.hasNext()) {
                jsonDeserializer = null;
                break;
            }
            jsonDeserializer = ((AnonymousClass1c7) it.next()).findMapLikeDeserializer(r3, r4, r12, r6, r7, jsonDeserializer2);
            if (jsonDeserializer != null) {
                break;
            }
        }
        if (jsonDeserializer != null) {
            AnonymousClass1c8 r1 = this._factoryConfig;
            if (r1.hasDeserializerModifiers()) {
                Iterator it2 = new C29161fy(r1._modifiers).iterator();
                while (it2.hasNext()) {
                    it2.next();
                }
            }
        }
        return jsonDeserializer;
    }

    public abstract AnonymousClass1c6 withConfig(AnonymousClass1c8 r1);

    static {
        HashMap hashMap = new HashMap();
        _mapFallbacks = hashMap;
        hashMap.put(Map.class.getName(), LinkedHashMap.class);
        HashMap hashMap2 = _mapFallbacks;
        hashMap2.put(ConcurrentMap.class.getName(), ConcurrentHashMap.class);
        Class<TreeMap> cls = TreeMap.class;
        hashMap2.put(SortedMap.class.getName(), cls);
        hashMap2.put("java.util.NavigableMap", cls);
        try {
            _mapFallbacks.put(ConcurrentNavigableMap.class.getName(), ConcurrentSkipListMap.class);
        } catch (Throwable th) {
            PrintStream printStream = System.err;
            printStream.println("Problems with (optional) types: " + th);
        }
        HashMap hashMap3 = new HashMap();
        _collectionFallbacks = hashMap3;
        Class<ArrayList> cls2 = ArrayList.class;
        hashMap3.put(Collection.class.getName(), cls2);
        HashMap hashMap4 = _collectionFallbacks;
        hashMap4.put(List.class.getName(), cls2);
        hashMap4.put(Set.class.getName(), HashSet.class);
        Class<TreeSet> cls3 = TreeSet.class;
        hashMap4.put(SortedSet.class.getName(), cls3);
        Class<LinkedList> cls4 = LinkedList.class;
        hashMap4.put(Queue.class.getName(), cls4);
        hashMap4.put("java.util.Deque", cls4);
        hashMap4.put("java.util.NavigableSet", cls3);
    }

    private JsonDeserializer _findCustomEnumDeserializer(Class cls, C10490kF r4, C10120ja r5) {
        for (AnonymousClass1c7 findEnumDeserializer : new C29161fy(this._factoryConfig._additionalDeserializers)) {
            JsonDeserializer findEnumDeserializer2 = findEnumDeserializer.findEnumDeserializer(cls, r4, r5);
            if (findEnumDeserializer2 != null) {
                return findEnumDeserializer2;
            }
        }
        return null;
    }

    private Ca5 constructCreatorProperty(C26791c3 r20, C10120ja r21, String str, int i, AnonymousClass137 r24, Object obj) {
        Boolean hasRequiredMarker;
        boolean booleanValue;
        C26791c3 r6 = r20;
        C10490kF r5 = r6._config;
        C10140jc annotationIntrospector = r6.getAnnotationIntrospector();
        AnonymousClass137 r12 = r24;
        if (annotationIntrospector == null) {
            hasRequiredMarker = null;
        } else {
            hasRequiredMarker = annotationIntrospector.hasRequiredMarker(r12);
        }
        if (hasRequiredMarker == null) {
            booleanValue = false;
        } else {
            booleanValue = hasRequiredMarker.booleanValue();
        }
        C10120ja r4 = r21;
        C10030jR _constructType = r5._base._typeFactory._constructType(r12._type, r4.bindingsForBeanType());
        String str2 = str;
        C25124CaI caI = new C25124CaI(str2, _constructType, null, r4.getClassAnnotations(), r12, booleanValue);
        C10030jR resolveType = resolveType(r6, r4, _constructType, r12);
        if (resolveType != _constructType) {
            caI = caI.withType(resolveType);
        }
        JsonDeserializer findDeserializerFromAnnotation = findDeserializerFromAnnotation(r6, r12);
        C10030jR modifyTypeByAnnotation = modifyTypeByAnnotation(r6, r12, resolveType);
        C64433Bp r2 = (C64433Bp) modifyTypeByAnnotation.getTypeHandler();
        if (r2 == null) {
            r2 = findTypeDeserializer(r5, modifyTypeByAnnotation);
        }
        String str3 = str2;
        AnonymousClass137 r15 = r12;
        C64433Bp r13 = r2;
        C87974Hw r122 = caI._wrapperName;
        Ca5 ca5 = new Ca5(str3, modifyTypeByAnnotation, r122, r13, r4.getClassAnnotations(), r15, i, obj, caI._isRequired);
        if (findDeserializerFromAnnotation != null) {
            return new Ca5(ca5, findDeserializerFromAnnotation);
        }
        return ca5;
    }

    private static C29171fz constructEnumResolver(Class cls, C10490kF r7, C29141fw r8) {
        if (r8 != null) {
            Method method = r8._method;
            if (r7.canOverrideAccessModifiers()) {
                C29081fq.checkAndFixAccess(method);
            }
            Enum[] enumArr = (Enum[]) cls.getEnumConstants();
            HashMap hashMap = new HashMap();
            int length = enumArr.length;
            while (true) {
                length--;
                if (length < 0) {
                    return new C29171fz(cls, enumArr, hashMap);
                }
                Enum enumR = enumArr[length];
                try {
                    Object invoke = method.invoke(enumR, new Object[0]);
                    if (invoke != null) {
                        hashMap.put(invoke.toString(), enumR);
                    }
                } catch (Exception e) {
                    throw new IllegalArgumentException("Failed to access @JsonValue of Enum value " + enumR + ": " + e.getMessage());
                }
            }
        } else if (r7.isEnabled(AnonymousClass1c1.READ_ENUMS_USING_TO_STRING)) {
            Enum[] enumArr2 = (Enum[]) cls.getEnumConstants();
            HashMap hashMap2 = new HashMap();
            int length2 = enumArr2.length;
            while (true) {
                length2--;
                if (length2 < 0) {
                    return new C29171fz(cls, enumArr2, hashMap2);
                }
                Enum enumR2 = enumArr2[length2];
                hashMap2.put(enumR2.toString(), enumR2);
            }
        } else {
            r7.getAnnotationIntrospector();
            Enum[] enumArr3 = (Enum[]) cls.getEnumConstants();
            if (enumArr3 != null) {
                HashMap hashMap3 = new HashMap();
                for (Enum enumR3 : enumArr3) {
                    hashMap3.put(enumR3.name(), enumR3);
                }
                return new C29171fz(cls, enumArr3, hashMap3);
            }
            throw new IllegalArgumentException(AnonymousClass08S.A0J("No enum constants for class ", cls.getName()));
        }
    }

    public JsonDeserializer createArrayDeserializer(C26791c3 r12, BMA bma, C10120ja r14) {
        JsonDeserializer jsonDeserializer;
        C10490kF r7 = r12._config;
        BMA bma2 = bma;
        C10030jR contentType = bma.getContentType();
        JsonDeserializer jsonDeserializer2 = (JsonDeserializer) contentType.getValueHandler();
        C64433Bp r3 = (C64433Bp) contentType.getTypeHandler();
        if (r3 == null) {
            r3 = findTypeDeserializer(r7, contentType);
        }
        Iterator it = new C29161fy(this._factoryConfig._additionalDeserializers).iterator();
        while (true) {
            if (!it.hasNext()) {
                jsonDeserializer = null;
                break;
            }
            jsonDeserializer = ((AnonymousClass1c7) it.next()).findArrayDeserializer(bma2, r7, r14, r3, jsonDeserializer2);
            if (jsonDeserializer != null) {
                break;
            }
        }
        if (jsonDeserializer == null) {
            if (jsonDeserializer2 == null) {
                Class<String> cls = contentType._class;
                if (cls.isPrimitive()) {
                    if (cls == Integer.TYPE) {
                        return PrimitiveArrayDeserializers.IntDeser.instance;
                    }
                    if (cls == Long.TYPE) {
                        return PrimitiveArrayDeserializers.LongDeser.instance;
                    }
                    if (cls == Byte.TYPE) {
                        return new PrimitiveArrayDeserializers.ByteDeser();
                    }
                    if (cls == Short.TYPE) {
                        return new PrimitiveArrayDeserializers.ShortDeser();
                    }
                    if (cls == Float.TYPE) {
                        return new PrimitiveArrayDeserializers.FloatDeser();
                    }
                    if (cls == Double.TYPE) {
                        return new PrimitiveArrayDeserializers.DoubleDeser();
                    }
                    if (cls == Boolean.TYPE) {
                        return new PrimitiveArrayDeserializers.BooleanDeser();
                    }
                    if (cls == Character.TYPE) {
                        return new PrimitiveArrayDeserializers.CharDeser();
                    }
                    throw new IllegalStateException();
                } else if (cls == String.class) {
                    return StringArrayDeserializer.instance;
                }
            }
            jsonDeserializer = new ObjectArrayDeserializer(bma, jsonDeserializer2, r3);
        }
        AnonymousClass1c8 r1 = this._factoryConfig;
        if (r1.hasDeserializerModifiers()) {
            Iterator it2 = new C29161fy(r1._modifiers).iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
        return jsonDeserializer;
    }

    public JsonDeserializer createCollectionDeserializer(C26791c3 r18, C28331ed r19, C10120ja r20) {
        JsonDeserializer jsonDeserializer;
        C28331ed r12;
        C10120ja r8 = r20;
        C28331ed r6 = r19;
        C10030jR contentType = r6.getContentType();
        JsonDeserializer jsonDeserializer2 = (JsonDeserializer) contentType.getValueHandler();
        C26791c3 r4 = r18;
        C10490kF r7 = r4._config;
        C64433Bp r9 = (C64433Bp) contentType.getTypeHandler();
        if (r9 == null) {
            r9 = findTypeDeserializer(r7, contentType);
        }
        Iterator it = new C29161fy(this._factoryConfig._additionalDeserializers).iterator();
        while (true) {
            if (!it.hasNext()) {
                jsonDeserializer = null;
                break;
            }
            jsonDeserializer = ((AnonymousClass1c7) it.next()).findCollectionDeserializer(r6, r7, r8, r9, jsonDeserializer2);
            if (jsonDeserializer != null) {
                break;
            }
        }
        if (jsonDeserializer == null) {
            Class cls = r6._class;
            if (jsonDeserializer2 == null && EnumSet.class.isAssignableFrom(cls)) {
                jsonDeserializer = new EnumSetDeserializer(contentType, null);
            }
        }
        if (jsonDeserializer == null) {
            if (r6._class.isInterface() || r6.isAbstract()) {
                Class cls2 = (Class) _collectionFallbacks.get(r6._class.getName());
                if (cls2 == null) {
                    r12 = null;
                } else {
                    r12 = (C28331ed) r7._base._typeFactory.constructSpecializedType(r6, cls2);
                }
                if (r12 != null) {
                    r8 = r7._base._classIntrospector.forCreation(r7, r12, r7);
                } else {
                    throw new IllegalArgumentException("Can not find a deserializer for non-concrete Collection type " + r6);
                }
            } else {
                r12 = r6;
            }
            C64473Bx findValueInstantiator = findValueInstantiator(r4, r8);
            if (!findValueInstantiator.canCreateUsingDefault() && r12._class == ArrayBlockingQueue.class) {
                return new ArrayBlockingQueueDeserializer(r12, jsonDeserializer2, r9, findValueInstantiator, null);
            }
            if (contentType._class == String.class) {
                jsonDeserializer = new StringCollectionDeserializer(r12, findValueInstantiator, null, jsonDeserializer2);
            } else {
                jsonDeserializer = new CollectionDeserializer(r12, jsonDeserializer2, r9, findValueInstantiator, null);
            }
        }
        AnonymousClass1c8 r1 = this._factoryConfig;
        if (r1.hasDeserializerModifiers()) {
            Iterator it2 = new C29161fy(r1._modifiers).iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
        return jsonDeserializer;
    }

    public JsonDeserializer createEnumDeserializer(C26791c3 r7, C10030jR r8, C10120ja r9) {
        Class cls;
        C10490kF r5 = r7._config;
        Class cls2 = r8._class;
        JsonDeserializer _findCustomEnumDeserializer = _findCustomEnumDeserializer(cls2, r5, r9);
        if (_findCustomEnumDeserializer == null) {
            Iterator it = r9.getFactoryMethods().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C29141fw r3 = (C29141fw) it.next();
                if (r7.getAnnotationIntrospector().hasCreatorAnnotation(r3)) {
                    if (r3.getParameterCount() != 1 || !r3._method.getReturnType().isAssignableFrom(cls2)) {
                        throw new IllegalArgumentException("Unsuitable method (" + r3 + ") decorated with @JsonCreator (for Enum type " + cls2.getName() + ")");
                    }
                    Class<Long> rawParameterType = r3.getRawParameterType(0);
                    if (rawParameterType == String.class) {
                        cls = null;
                    } else if (rawParameterType == Integer.TYPE || rawParameterType == Integer.class) {
                        cls = Integer.class;
                    } else if (rawParameterType == Long.TYPE || rawParameterType == Long.class) {
                        cls = Long.class;
                    } else {
                        throw new IllegalArgumentException("Parameter #0 type for factory method (" + r3 + ") not suitable, must be java.lang.String or int/Integer/long/Long");
                    }
                    if (r5.canOverrideAccessModifiers()) {
                        C29081fq.checkAndFixAccess(r3._method);
                    }
                    _findCustomEnumDeserializer = new EnumDeserializer.FactoryBasedDeserializer(cls2, r3, cls);
                }
            }
            if (_findCustomEnumDeserializer == null) {
                _findCustomEnumDeserializer = new EnumDeserializer(constructEnumResolver(cls2, r5, r9.findJsonValueMethod()));
            }
        }
        AnonymousClass1c8 r1 = this._factoryConfig;
        if (r1.hasDeserializerModifiers()) {
            Iterator it2 = new C29161fy(r1._modifiers).iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
        return _findCustomEnumDeserializer;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C422628y createKeyDeserializer(X.C26791c3 r7, X.C10030jR r8) {
        /*
            r6 = this;
            X.0kF r4 = r7._config
            X.1c8 r0 = r6._factoryConfig
            X.1cC[] r0 = r0._additionalKeyDeserializers
            int r1 = r0.length
            r0 = 0
            if (r1 <= 0) goto L_0x000b
            r0 = 1
        L_0x000b:
            r2 = 0
            if (r0 == 0) goto L_0x0037
            java.lang.Class r0 = r8._class
            X.0jR r0 = r4.constructType(r0)
            X.0ja r3 = r4.introspectClassAnnotations(r0)
            X.1c8 r0 = r6._factoryConfig
            X.1cC[] r1 = r0._additionalKeyDeserializers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x0025:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0037
            java.lang.Object r0 = r1.next()
            X.1cC r0 = (X.C26821cC) r0
            X.28y r2 = r0.findKeyDeserializer(r8, r4, r3)
            if (r2 == 0) goto L_0x0025
        L_0x0037:
            if (r2 != 0) goto L_0x011f
            java.lang.Class r0 = r8._class
            boolean r0 = r0.isEnum()
            if (r0 == 0) goto L_0x00fd
            X.0kF r5 = r7._config
            X.0jr r0 = r5._base
            X.0jU r0 = r0._classIntrospector
            X.0ja r1 = r0.forDeserialization(r5, r8, r5)
            X.0jV r0 = r1.getClassInfo()
            com.fasterxml.jackson.databind.JsonDeserializer r2 = findDeserializerFromAnnotation(r7, r0)
            if (r2 != 0) goto L_0x00f5
            java.lang.Class r4 = r8._class
            com.fasterxml.jackson.databind.JsonDeserializer r0 = r6._findCustomEnumDeserializer(r4, r5, r1)
            if (r0 != 0) goto L_0x00f5
            X.1fw r0 = r1.findJsonValueMethod()
            X.1fz r2 = constructEnumResolver(r4, r5, r0)
            java.util.List r0 = r1.getFactoryMethods()
            java.util.Iterator r1 = r0.iterator()
        L_0x006d:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00ee
            java.lang.Object r3 = r1.next()
            X.1fw r3 = (X.C29141fw) r3
            X.0jc r0 = r5.getAnnotationIntrospector()
            boolean r0 = r0.hasCreatorAnnotation(r3)
            if (r0 == 0) goto L_0x006d
            int r1 = r3.getParameterCount()
            r0 = 1
            if (r1 != r0) goto L_0x00c9
            java.lang.reflect.Method r0 = r3._method
            java.lang.Class r0 = r0.getReturnType()
            boolean r0 = r0.isAssignableFrom(r4)
            if (r0 == 0) goto L_0x00c9
            r0 = 0
            java.lang.reflect.Type r1 = r3.getGenericParameterType(r0)
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r1 != r0) goto L_0x00b0
            boolean r0 = r5.canOverrideAccessModifiers()
            if (r0 == 0) goto L_0x00aa
            java.lang.reflect.Method r0 = r3._method
            X.C29081fq.checkAndFixAccess(r0)
        L_0x00aa:
            X.CWr r1 = new X.CWr
            r1.<init>(r2, r3)
            return r1
        L_0x00b0:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Parameter #0 type for factory method ("
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = ") not suitable, must be java.lang.String"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x00c9:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Unsuitable method ("
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = ") decorated with @JsonCreator (for Enum type "
            r1.append(r0)
            java.lang.String r0 = r4.getName()
            r1.append(r0)
            java.lang.String r0 = ")"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x00ee:
            X.CWr r1 = new X.CWr
            r0 = 0
            r1.<init>(r2, r0)
            return r1
        L_0x00f5:
            X.CWt r1 = new X.CWt
            java.lang.Class r0 = r8._class
            r1.<init>(r0, r2)
            return r1
        L_0x00fd:
            X.0jr r0 = r4._base
            X.0jU r0 = r0._classIntrospector
            X.0ja r3 = r0.forDeserialization(r4, r8, r4)
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            java.lang.Class[] r0 = new java.lang.Class[]{r2}
            java.lang.reflect.Constructor r1 = r3.findSingleArgConstructor(r0)
            if (r1 == 0) goto L_0x013e
            boolean r0 = r4.canOverrideAccessModifiers()
            if (r0 == 0) goto L_0x011a
            X.C29081fq.checkAndFixAccess(r1)
        L_0x011a:
            X.CUx r2 = new X.CUx
            r2.<init>(r1)
        L_0x011f:
            if (r2 == 0) goto L_0x0159
            X.1c8 r1 = r6._factoryConfig
            boolean r0 = r1.hasDeserializerModifiers()
            if (r0 == 0) goto L_0x0159
            X.1c9[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x0134:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0159
            r1.next()
            goto L_0x0134
        L_0x013e:
            java.lang.Class[] r0 = new java.lang.Class[]{r2}
            java.lang.reflect.Method r1 = r3.findFactoryMethod(r0)
            if (r1 == 0) goto L_0x0157
            boolean r0 = r4.canOverrideAccessModifiers()
            if (r0 == 0) goto L_0x0151
            X.C29081fq.checkAndFixAccess(r1)
        L_0x0151:
            X.CUp r2 = new X.CUp
            r2.<init>(r1)
            goto L_0x011f
        L_0x0157:
            r2 = 0
            goto L_0x011f
        L_0x0159:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1c5.createKeyDeserializer(X.1c3, X.0jR):X.28y");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:8:0x004b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:16:0x0067 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:43:0x004b */
    public JsonDeserializer createMapDeserializer(C26791c3 r21, C180610a r22, C10120ja r23) {
        EnumMapDeserializer enumMapDeserializer;
        HashSet hashSet;
        C180610a r15 = r22;
        C10120ja r10 = r23;
        C26791c3 r4 = r21;
        C10490kF r9 = r4._config;
        C10030jR keyType = r15.getKeyType();
        C10030jR contentType = r15.getContentType();
        JsonDeserializer jsonDeserializer = (JsonDeserializer) contentType.getValueHandler();
        C422628y r11 = (C422628y) keyType.getValueHandler();
        C64433Bp r3 = (C64433Bp) contentType.getTypeHandler();
        if (r3 == null) {
            r3 = findTypeDeserializer(r9, contentType);
        }
        Iterator it = new C29161fy(this._factoryConfig._additionalDeserializers).iterator();
        while (true) {
            if (!it.hasNext()) {
                enumMapDeserializer = null;
                break;
            }
            enumMapDeserializer = ((AnonymousClass1c7) it.next()).findMapDeserializer(r15, r9, r10, r11, r3, jsonDeserializer);
            if (enumMapDeserializer != null) {
                break;
            }
        }
        if (enumMapDeserializer == null) {
            Class cls = r15._class;
            if (EnumMap.class.isAssignableFrom(cls)) {
                Class cls2 = keyType._class;
                if (cls2 == null || !cls2.isEnum()) {
                    throw new IllegalArgumentException("Can not construct EnumMap; generic (key) type not available");
                }
                enumMapDeserializer = new EnumMapDeserializer(r15, null, jsonDeserializer, r3);
            }
            if (enumMapDeserializer == null) {
                if (r15._class.isInterface() || r15.isAbstract()) {
                    Class cls3 = (Class) _mapFallbacks.get(cls.getName());
                    if (cls3 != null) {
                        r15 = (C180610a) r9._base._typeFactory.constructSpecializedType(r15, cls3);
                        r10 = r9._base._classIntrospector.forCreation(r9, r15, r9);
                    } else {
                        throw new IllegalArgumentException("Can not find a deserializer for non-concrete Map type " + r15);
                    }
                }
                MapDeserializer mapDeserializer = new MapDeserializer(r15, findValueInstantiator(r4, r10), r11, jsonDeserializer, r3);
                String[] findPropertiesToIgnore = r9.getAnnotationIntrospector().findPropertiesToIgnore(r10.getClassInfo());
                if (findPropertiesToIgnore == null || findPropertiesToIgnore.length == 0) {
                    hashSet = null;
                } else {
                    hashSet = C11830o1.arrayToSet(findPropertiesToIgnore);
                }
                mapDeserializer._ignorableProperties = hashSet;
                enumMapDeserializer = mapDeserializer;
            }
        }
        AnonymousClass1c8 r1 = this._factoryConfig;
        if (r1.hasDeserializerModifiers()) {
            Iterator it2 = new C29161fy(r1._modifiers).iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
        return enumMapDeserializer;
    }

    public JsonDeserializer createTreeDeserializer(C10490kF r4, C10030jR r5, C10120ja r6) {
        JsonDeserializer jsonDeserializer;
        Class<ArrayNode> cls = r5._class;
        Iterator it = new C29161fy(this._factoryConfig._additionalDeserializers).iterator();
        while (true) {
            if (!it.hasNext()) {
                jsonDeserializer = null;
                break;
            }
            jsonDeserializer = ((AnonymousClass1c7) it.next()).findTreeNodeDeserializer(cls, r4, r6);
            if (jsonDeserializer != null) {
                break;
            }
        }
        if (jsonDeserializer != null) {
            return jsonDeserializer;
        }
        if (cls == ObjectNode.class) {
            return JsonNodeDeserializer.ObjectDeserializer._instance;
        }
        if (cls == ArrayNode.class) {
            return JsonNodeDeserializer.ArrayDeserializer._instance;
        }
        return JsonNodeDeserializer.instance;
    }

    public C64433Bp findTypeDeserializer(C10490kF r6, C10030jR r7) {
        C10030jR mapAbstractType;
        Class cls;
        C10070jV classInfo = r6.introspectClassAnnotations(r6.constructType(r7._class)).getClassInfo();
        C10140jc annotationIntrospector = r6.getAnnotationIntrospector();
        CXQ findTypeResolver = annotationIntrospector.findTypeResolver(r6, classInfo, r7);
        Collection collection = null;
        if (findTypeResolver == null) {
            findTypeResolver = r6._base._typeResolverBuilder;
            if (findTypeResolver == null) {
                return null;
            }
        } else {
            collection = r6._subtypeResolver.collectAndResolveSubtypes(classInfo, r6, annotationIntrospector);
        }
        if (findTypeResolver.getDefaultImpl() == null && r7.isAbstract() && (mapAbstractType = mapAbstractType(r6, r7)) != null && (cls = mapAbstractType._class) != r7._class) {
            findTypeResolver.defaultImpl(cls);
        }
        return findTypeResolver.buildTypeDeserializer(r6, r7, collection);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:244:0x03e0 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:325:0x0436 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:12:0x002c */
    /* JADX WARN: Type inference failed for: r3v7, types: [X.12y] */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0264, code lost:
        if (r5 == r12) goto L_0x0266;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C64473Bx findValueInstantiator(X.C26791c3 r31, X.C10120ja r32) {
        /*
            r30 = this;
            r15 = r31
            X.0kF r14 = r15._config
            r11 = r32
            X.0jV r1 = r11.getClassInfo()
            X.0jc r0 = r15.getAnnotationIntrospector()
            java.lang.Object r1 = r0.findValueInstantiator(r1)
            if (r1 == 0) goto L_0x01ba
            r4 = 0
            if (r1 == 0) goto L_0x001e
            boolean r0 = r1 instanceof X.C64473Bx
            if (r0 == 0) goto L_0x019c
            X.3Bx r1 = (X.C64473Bx) r1
            r4 = r1
        L_0x001e:
            r29 = r30
            if (r4 != 0) goto L_0x03d5
            X.0jR r0 = r11._type
            java.lang.Class r1 = r0._class
            java.lang.Class<X.3Bu> r0 = X.C64443Bu.class
            if (r1 != r0) goto L_0x0199
            X.CZX r4 = X.CZX.instance
        L_0x002c:
            if (r4 != 0) goto L_0x03d5
            X.0kA r0 = r15.getConfig()
            boolean r0 = r0.canOverrideAccessModifiers()
            X.3Bv r10 = new X.3Bv
            r10.<init>(r11, r0)
            X.0jc r9 = r15.getAnnotationIntrospector()
            X.0kF r8 = r15._config
            X.0je r1 = r8.getDefaultVisibilityChecker()
            X.0jV r0 = r11.getClassInfo()
            X.0je r7 = r9.findAutoDetectVisibility(r0, r1)
            java.util.List r0 = r11.getFactoryMethods()
            java.util.Iterator r19 = r0.iterator()
        L_0x0055:
            boolean r0 = r19.hasNext()
            if (r0 == 0) goto L_0x01e0
            java.lang.Object r3 = r19.next()
            X.1fw r3 = (X.C29141fw) r3
            boolean r18 = r9.hasCreatorAnnotation(r3)
            int r6 = r3.getParameterCount()
            if (r6 != 0) goto L_0x0071
            if (r18 == 0) goto L_0x0055
            r10.setDefaultCreator(r3)
            goto L_0x0055
        L_0x0071:
            r5 = 0
            r17 = 0
            r0 = 1
            if (r6 != r0) goto L_0x012c
            X.137 r1 = r3.getParameter(r5)
            if (r1 != 0) goto L_0x00af
            r0 = r17
        L_0x007f:
            if (r0 != 0) goto L_0x00ac
            r0 = r17
        L_0x0083:
            java.lang.Object r1 = r9.findInjectableValueId(r1)
            if (r1 != 0) goto L_0x0134
            if (r0 == 0) goto L_0x0091
            int r0 = r0.length()
            if (r0 != 0) goto L_0x0134
        L_0x0091:
            r0 = 0
            java.lang.Class r1 = r3.getRawParameterType(r0)
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r1 != r0) goto L_0x00b4
            if (r18 != 0) goto L_0x00a2
            boolean r0 = r7.isCreatorVisible(r3)
            if (r0 == 0) goto L_0x0055
        L_0x00a2:
            X.12y r1 = r10._stringCreator
            java.lang.String r0 = "String"
            X.C64453Bv.verifyNonDup(r10, r3, r1, r0)
            r10._stringCreator = r3
            goto L_0x0055
        L_0x00ac:
            java.lang.String r0 = r0._simpleName
            goto L_0x0083
        L_0x00af:
            X.4Hw r0 = r9.findNameForDeserialization(r1)
            goto L_0x007f
        L_0x00b4:
            java.lang.Class r0 = java.lang.Integer.TYPE
            if (r1 == r0) goto L_0x0119
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            if (r1 == r0) goto L_0x0119
            java.lang.Class r0 = java.lang.Long.TYPE
            if (r1 == r0) goto L_0x0106
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r1 == r0) goto L_0x0106
            java.lang.Class r0 = java.lang.Double.TYPE
            if (r1 == r0) goto L_0x00f3
            java.lang.Class<java.lang.Double> r0 = java.lang.Double.class
            if (r1 == r0) goto L_0x00f3
            java.lang.Class r0 = java.lang.Boolean.TYPE
            if (r1 == r0) goto L_0x00e0
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            if (r1 == r0) goto L_0x00e0
            boolean r0 = r9.hasCreatorAnnotation(r3)
            if (r0 == 0) goto L_0x0055
            r0 = 0
            r10.addDelegatingCreator(r3, r0)
            goto L_0x0055
        L_0x00e0:
            if (r18 != 0) goto L_0x00e8
            boolean r0 = r7.isCreatorVisible(r3)
            if (r0 == 0) goto L_0x0055
        L_0x00e8:
            X.12y r1 = r10._booleanCreator
            java.lang.String r0 = "boolean"
            X.C64453Bv.verifyNonDup(r10, r3, r1, r0)
            r10._booleanCreator = r3
            goto L_0x0055
        L_0x00f3:
            if (r18 != 0) goto L_0x00fb
            boolean r0 = r7.isCreatorVisible(r3)
            if (r0 == 0) goto L_0x0055
        L_0x00fb:
            X.12y r1 = r10._doubleCreator
            java.lang.String r0 = "double"
            X.C64453Bv.verifyNonDup(r10, r3, r1, r0)
            r10._doubleCreator = r3
            goto L_0x0055
        L_0x0106:
            if (r18 != 0) goto L_0x010e
            boolean r0 = r7.isCreatorVisible(r3)
            if (r0 == 0) goto L_0x0055
        L_0x010e:
            X.12y r1 = r10._longCreator
            java.lang.String r0 = "long"
            X.C64453Bv.verifyNonDup(r10, r3, r1, r0)
            r10._longCreator = r3
            goto L_0x0055
        L_0x0119:
            if (r18 != 0) goto L_0x0121
            boolean r0 = r7.isCreatorVisible(r3)
            if (r0 == 0) goto L_0x0055
        L_0x0121:
            X.12y r1 = r10._intCreator
            java.lang.String r0 = "int"
            X.C64453Bv.verifyNonDup(r10, r3, r1, r0)
            r10._intCreator = r3
            goto L_0x0055
        L_0x012c:
            boolean r0 = r9.hasCreatorAnnotation(r3)
            if (r0 != 0) goto L_0x0134
            goto L_0x0055
        L_0x0134:
            X.Ca5[] r2 = new X.Ca5[r6]
            r4 = r17
            r16 = 0
            r13 = 0
        L_0x013b:
            if (r5 >= r6) goto L_0x017f
            X.137 r1 = r3.getParameter(r5)
            if (r1 != 0) goto L_0x017a
            r0 = r17
        L_0x0145:
            if (r0 != 0) goto L_0x0177
            r0 = r17
        L_0x0149:
            java.lang.Object r26 = r9.findInjectableValueId(r1)
            if (r0 == 0) goto L_0x016c
            int r12 = r0.length()
            if (r12 <= 0) goto L_0x016c
            int r16 = r16 + 1
            r20 = r29
        L_0x0159:
            r21 = r15
            r22 = r11
            r23 = r0
            r24 = r5
            r25 = r1
            X.Ca5 r0 = r20.constructCreatorProperty(r21, r22, r23, r24, r25, r26)
            r2[r5] = r0
        L_0x0169:
            int r5 = r5 + 1
            goto L_0x013b
        L_0x016c:
            if (r26 == 0) goto L_0x0173
            int r13 = r13 + 1
            r20 = r29
            goto L_0x0159
        L_0x0173:
            if (r4 != 0) goto L_0x0169
            r4 = r1
            goto L_0x0169
        L_0x0177:
            java.lang.String r0 = r0._simpleName
            goto L_0x0149
        L_0x017a:
            X.4Hw r0 = r9.findNameForDeserialization(r1)
            goto L_0x0145
        L_0x017f:
            if (r18 != 0) goto L_0x0185
            if (r16 > 0) goto L_0x0185
            if (r13 <= 0) goto L_0x0055
        L_0x0185:
            int r0 = r16 + r13
            if (r0 != r6) goto L_0x018e
            r10.addPropertyCreator(r3, r2)
            goto L_0x0055
        L_0x018e:
            if (r16 != 0) goto L_0x01bd
            int r0 = r13 + 1
            if (r0 != r6) goto L_0x01bd
            r10.addDelegatingCreator(r3, r2)
            goto L_0x0055
        L_0x0199:
            r4 = 0
            goto L_0x002c
        L_0x019c:
            boolean r0 = r1 instanceof java.lang.Class
            if (r0 == 0) goto L_0x0478
            java.lang.Class r1 = (java.lang.Class) r1
            java.lang.Class<X.28t> r0 = X.C422228t.class
            if (r1 == r0) goto L_0x001e
            java.lang.Class<X.3Bx> r0 = X.C64473Bx.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x0466
            boolean r0 = r14.canOverrideAccessModifiers()
            java.lang.Object r4 = X.C29081fq.createInstance(r1, r0)
            X.3Bx r4 = (X.C64473Bx) r4
            goto L_0x001e
        L_0x01ba:
            r4 = 0
            goto L_0x001e
        L_0x01bd:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Argument #"
            r1.<init>(r0)
            int r0 = r4._index
            r1.append(r0)
            java.lang.String r0 = " of factory method "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " has no property name annotation; must have name when multiple-paramater constructor annotated as Creator"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x01e0:
            X.0jR r0 = r11._type
            boolean r0 = r0.isConcrete()
            if (r0 == 0) goto L_0x0399
            X.1fx r2 = r11.findDefaultConstructor()
            if (r2 == 0) goto L_0x01ff
            X.12y r1 = r10._defaultConstructor
            r0 = 0
            if (r1 == 0) goto L_0x01f4
            r0 = 1
        L_0x01f4:
            if (r0 == 0) goto L_0x01fc
            boolean r0 = r9.hasCreatorAnnotation(r2)
            if (r0 == 0) goto L_0x01ff
        L_0x01fc:
            r10.setDefaultCreator(r2)
        L_0x01ff:
            java.util.List r0 = r11.findProperties()
            java.util.Iterator r4 = r0.iterator()
            r21 = 0
            r12 = r21
            r6 = r12
        L_0x020c:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x023f
            java.lang.Object r2 = r4.next()
            X.1ft r2 = (X.C29111ft) r2
            X.137 r0 = r2.getConstructorParameter()
            if (r0 == 0) goto L_0x020c
            X.137 r1 = r2.getConstructorParameter()
            X.12y r3 = r1._owner
            boolean r0 = r3 instanceof X.C29151fx
            if (r0 == 0) goto L_0x020c
            if (r12 != 0) goto L_0x0236
            r12 = r3
            X.1fx r12 = (X.C29151fx) r12
            java.lang.reflect.Constructor r0 = r12._constructor
            java.lang.Class[] r0 = r0.getParameterTypes()
            int r0 = r0.length
            java.lang.String[] r6 = new java.lang.String[r0]
        L_0x0236:
            int r1 = r1._index
            java.lang.String r0 = r2.getName()
            r6[r1] = r0
            goto L_0x020c
        L_0x023f:
            java.util.List r0 = r11.getConstructors()
            java.util.Iterator r20 = r0.iterator()
        L_0x0247:
            boolean r0 = r20.hasNext()
            if (r0 == 0) goto L_0x0399
            java.lang.Object r5 = r20.next()
            X.1fx r5 = (X.C29151fx) r5
            java.lang.reflect.Constructor r0 = r5._constructor
            java.lang.Class[] r0 = r0.getParameterTypes()
            int r13 = r0.length
            boolean r1 = r9.hasCreatorAnnotation(r5)
            r4 = 0
            r0 = 1
            if (r1 != 0) goto L_0x0266
            r19 = 0
            if (r5 != r12) goto L_0x0268
        L_0x0266:
            r19 = 1
        L_0x0268:
            boolean r16 = r7.isCreatorVisible(r5)
            if (r13 != r0) goto L_0x031e
            if (r5 != r12) goto L_0x031a
            r0 = r6[r4]
        L_0x0272:
            X.137 r2 = r5.getParameter(r4)
            r1 = 0
            if (r0 != 0) goto L_0x027f
            if (r2 != 0) goto L_0x0314
            r0 = r1
        L_0x027c:
            if (r0 != 0) goto L_0x0310
            r0 = r1
        L_0x027f:
            java.lang.Object r28 = r9.findInjectableValueId(r2)
            if (r28 != 0) goto L_0x028d
            if (r0 == 0) goto L_0x02a5
            int r13 = r0.length()
            if (r13 <= 0) goto L_0x02a5
        L_0x028d:
            r26 = 0
            r22 = r29
            r23 = r15
            r24 = r11
            r25 = r0
            r27 = r2
            X.Ca5 r0 = r22.constructCreatorProperty(r23, r24, r25, r26, r27, r28)
            X.Ca5[] r0 = new X.Ca5[]{r0}
            r10.addPropertyCreator(r5, r0)
            goto L_0x0247
        L_0x02a5:
            java.lang.reflect.Constructor r0 = r5._constructor
            java.lang.Class[] r2 = r0.getParameterTypes()
            int r0 = r2.length
            if (r4 < r0) goto L_0x02c1
            r2 = 0
        L_0x02af:
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r2 != r0) goto L_0x02c4
            if (r19 != 0) goto L_0x02b7
            if (r16 == 0) goto L_0x0247
        L_0x02b7:
            X.12y r1 = r10._stringCreator
            java.lang.String r0 = "String"
            X.C64453Bv.verifyNonDup(r10, r5, r1, r0)
            r10._stringCreator = r5
            goto L_0x0247
        L_0x02c1:
            r2 = r2[r4]
            goto L_0x02af
        L_0x02c4:
            java.lang.Class r0 = java.lang.Integer.TYPE
            if (r2 == r0) goto L_0x0301
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            if (r2 == r0) goto L_0x0301
            java.lang.Class r0 = java.lang.Long.TYPE
            if (r2 == r0) goto L_0x02f2
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r2 == r0) goto L_0x02f2
            java.lang.Class r0 = java.lang.Double.TYPE
            if (r2 == r0) goto L_0x02e3
            java.lang.Class<java.lang.Double> r0 = java.lang.Double.class
            if (r2 == r0) goto L_0x02e3
            if (r19 == 0) goto L_0x0247
            r10.addDelegatingCreator(r5, r1)
            goto L_0x0247
        L_0x02e3:
            if (r19 != 0) goto L_0x02e7
            if (r16 == 0) goto L_0x0247
        L_0x02e7:
            X.12y r1 = r10._doubleCreator
            java.lang.String r0 = "double"
            X.C64453Bv.verifyNonDup(r10, r5, r1, r0)
            r10._doubleCreator = r5
            goto L_0x0247
        L_0x02f2:
            if (r19 != 0) goto L_0x02f6
            if (r16 == 0) goto L_0x0247
        L_0x02f6:
            X.12y r1 = r10._longCreator
            java.lang.String r0 = "long"
            X.C64453Bv.verifyNonDup(r10, r5, r1, r0)
            r10._longCreator = r5
            goto L_0x0247
        L_0x0301:
            if (r19 != 0) goto L_0x0305
            if (r16 == 0) goto L_0x0247
        L_0x0305:
            X.12y r1 = r10._intCreator
            java.lang.String r0 = "int"
            X.C64453Bv.verifyNonDup(r10, r5, r1, r0)
            r10._intCreator = r5
            goto L_0x0247
        L_0x0310:
            java.lang.String r0 = r0._simpleName
            goto L_0x027f
        L_0x0314:
            X.4Hw r0 = r9.findNameForDeserialization(r2)
            goto L_0x027c
        L_0x031a:
            r0 = r21
            goto L_0x0272
        L_0x031e:
            if (r19 != 0) goto L_0x0324
            if (r16 != 0) goto L_0x0324
            goto L_0x0247
        L_0x0324:
            X.Ca5[] r3 = new X.Ca5[r13]
            r2 = r21
            r18 = 0
            r17 = 0
        L_0x032c:
            if (r4 >= r13) goto L_0x0377
            X.137 r1 = r5.getParameter(r4)
            if (r5 != r12) goto L_0x0374
            r0 = r6[r4]
        L_0x0336:
            if (r0 != 0) goto L_0x0340
            if (r1 != 0) goto L_0x036f
            r0 = r21
        L_0x033c:
            if (r0 != 0) goto L_0x036c
            r0 = r21
        L_0x0340:
            java.lang.Object r28 = r9.findInjectableValueId(r1)
            if (r0 == 0) goto L_0x0363
            int r16 = r0.length()
            if (r16 <= 0) goto L_0x0363
            int r18 = r18 + 1
        L_0x034e:
            r22 = r29
            r23 = r15
            r24 = r11
            r25 = r0
            r26 = r4
            r27 = r1
            X.Ca5 r0 = r22.constructCreatorProperty(r23, r24, r25, r26, r27, r28)
            r3[r4] = r0
        L_0x0360:
            int r4 = r4 + 1
            goto L_0x032c
        L_0x0363:
            if (r28 == 0) goto L_0x0368
            int r17 = r17 + 1
            goto L_0x034e
        L_0x0368:
            if (r2 != 0) goto L_0x0360
            r2 = r1
            goto L_0x0360
        L_0x036c:
            java.lang.String r0 = r0._simpleName
            goto L_0x0340
        L_0x036f:
            X.4Hw r0 = r9.findNameForDeserialization(r1)
            goto L_0x033c
        L_0x0374:
            r0 = r21
            goto L_0x0336
        L_0x0377:
            if (r19 != 0) goto L_0x037d
            if (r18 > 0) goto L_0x037d
            if (r17 <= 0) goto L_0x0247
        L_0x037d:
            int r0 = r18 + r17
            if (r0 != r13) goto L_0x0386
            r10.addPropertyCreator(r5, r3)
            goto L_0x0247
        L_0x0386:
            if (r18 != 0) goto L_0x0391
            int r0 = r17 + 1
            if (r0 != r13) goto L_0x0391
            r10.addDelegatingCreator(r5, r3)
            goto L_0x0247
        L_0x0391:
            X.137 r0 = r10._incompleteParameter
            if (r0 != 0) goto L_0x0247
            r10._incompleteParameter = r2
            goto L_0x0247
        L_0x0399:
            X.3Bw r4 = new X.3Bw
            X.0ja r0 = r10._beanDesc
            X.0jR r0 = r0._type
            r4.<init>(r8, r0)
            X.12y r6 = r10._delegateCreator
            if (r6 != 0) goto L_0x0413
            r7 = 0
        L_0x03a7:
            X.12y r6 = r10._defaultConstructor
            X.12y r5 = r10._delegateCreator
            X.Ca5[] r3 = r10._delegateArgs
            X.12y r2 = r10._propertyBasedCreator
            X.Ca5[] r1 = r10._propertyBasedArgs
            r4._defaultCreator = r6
            r4._delegateCreator = r5
            r4._delegateType = r7
            r4._delegateArguments = r3
            r4._withArgsCreator = r2
            r4._constructorArguments = r1
            X.12y r0 = r10._stringCreator
            r4._fromStringCreator = r0
            X.12y r0 = r10._intCreator
            r4._fromIntCreator = r0
            X.12y r0 = r10._longCreator
            r4._fromLongCreator = r0
            X.12y r0 = r10._doubleCreator
            r4._fromDoubleCreator = r0
            X.12y r0 = r10._booleanCreator
            r4._fromBooleanCreator = r0
            X.137 r0 = r10._incompleteParameter
            r4._incompleteParameter = r0
        L_0x03d5:
            r0 = r29
            X.1c8 r0 = r0._factoryConfig
            X.1cB[] r2 = r0._valueInstantiators
            int r1 = r2.length
            r0 = 0
            if (r1 <= 0) goto L_0x03e0
            r0 = 1
        L_0x03e0:
            if (r0 == 0) goto L_0x0436
            X.1fy r0 = new X.1fy
            r0.<init>(r2)
            java.util.Iterator r1 = r0.iterator()
        L_0x03eb:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0436
            java.lang.Object r0 = r1.next()
            X.1cB r0 = (X.C26811cB) r0
            X.3Bx r4 = r0.findValueInstantiator(r14, r11, r4)
            if (r4 != 0) goto L_0x03eb
            X.1w6 r3 = new X.1w6
            java.lang.String r2 = "Broken registered ValueInstantiators (of type "
            java.lang.Class r0 = r0.getClass()
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = "): returned null ValueInstantiator"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        L_0x0413:
            X.Ca5[] r5 = r10._delegateArgs
            r3 = 0
            if (r5 == 0) goto L_0x0421
            int r2 = r5.length
            r1 = 0
        L_0x041a:
            if (r1 >= r2) goto L_0x0421
            r0 = r5[r1]
            if (r0 != 0) goto L_0x0433
            r3 = r1
        L_0x0421:
            X.0ja r0 = r10._beanDesc
            X.1eb r2 = r0.bindingsForBeanType()
            java.lang.reflect.Type r1 = r6.getGenericParameterType(r3)
            X.0js r0 = r2._typeFactory
            X.0jR r7 = r0._constructType(r1, r2)
            goto L_0x03a7
        L_0x0433:
            int r1 = r1 + 1
            goto L_0x041a
        L_0x0436:
            X.137 r0 = r4.getIncompleteParameter()
            if (r0 != 0) goto L_0x043d
            return r4
        L_0x043d:
            X.137 r4 = r4.getIncompleteParameter()
            X.12y r3 = r4._owner
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Argument #"
            r1.<init>(r0)
            int r0 = r4._index
            r1.append(r0)
            java.lang.String r0 = " of constructor "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " has no property name annotation; must have name when multiple-paramater constructor annotated as Creator"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0466:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = "AnnotationIntrospector returned Class "
            java.lang.String r1 = r1.getName()
            java.lang.String r0 = "; expected Class<ValueInstantiator>"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        L_0x0478:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = "AnnotationIntrospector returned key deserializer definition of type "
            java.lang.Class r0 = r1.getClass()
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = "; expected type KeyDeserializer or Class<KeyDeserializer> instead"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1c5.findValueInstantiator(X.1c3, X.0ja):X.3Bx");
    }

    public C10030jR mapAbstractType(C10490kF r4, C10030jR r5) {
        C26801cA[] r2 = this._factoryConfig._abstractTypeResolvers;
        boolean z = false;
        if (r2.length > 0) {
            z = true;
        }
        if (z) {
            Iterator it = new C29161fy(r2).iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        return r5;
    }

    public final AnonymousClass1c6 withAdditionalDeserializers(AnonymousClass1c7 r9) {
        AnonymousClass1c8 r1 = this._factoryConfig;
        if (r9 != null) {
            return withConfig(new AnonymousClass1c8((AnonymousClass1c7[]) C11830o1.insertInListNoDup(r1._additionalDeserializers, r9), r1._additionalKeyDeserializers, r1._modifiers, r1._abstractTypeResolvers, r1._valueInstantiators));
        }
        throw new IllegalArgumentException("Can not pass null Deserializers");
    }

    public AnonymousClass1c5(AnonymousClass1c8 r1) {
        this._factoryConfig = r1;
    }

    public static JsonDeserializer findDeserializerFromAnnotation(C26791c3 r1, C10080jW r2) {
        Object findDeserializer = r1.getAnnotationIntrospector().findDeserializer(r2);
        if (findDeserializer == null) {
            return null;
        }
        return r1.deserializerInstance(r2, findDeserializer);
    }

    public static C10030jR modifyTypeByAnnotation(C26791c3 r7, C10080jW r8, C10030jR r9) {
        JsonDeserializer deserializerInstance;
        C422628y keyDeserializerInstance;
        C10140jc annotationIntrospector = r7.getAnnotationIntrospector();
        Class findDeserializationType = annotationIntrospector.findDeserializationType(r8, r9);
        if (findDeserializationType != null) {
            try {
                r9 = r9.narrowBy(findDeserializationType);
            } catch (IllegalArgumentException e) {
                throw new C37701w6("Failed to narrow type " + r9 + " with concrete-type annotation (value " + findDeserializationType.getName() + "), method '" + r8.getName() + "': " + e.getMessage(), null, e);
            }
        }
        if (!r9.isContainerType()) {
            return r9;
        }
        Class findDeserializationKeyType = annotationIntrospector.findDeserializationKeyType(r8, r9.getKeyType());
        if (findDeserializationKeyType != null) {
            if (r9 instanceof C21991Jm) {
                try {
                    r9 = ((C21991Jm) r9).narrowKey(findDeserializationKeyType);
                } catch (IllegalArgumentException e2) {
                    throw new C37701w6("Failed to narrow key type " + r9 + " with key-type annotation (" + findDeserializationKeyType.getName() + "): " + e2.getMessage(), null, e2);
                }
            } else {
                throw new C37701w6("Illegal key-type annotation: type " + r9 + " is not a Map(-like) type");
            }
        }
        C10030jR keyType = r9.getKeyType();
        if (!(keyType == null || keyType.getValueHandler() != null || (keyDeserializerInstance = r7.keyDeserializerInstance(r8, annotationIntrospector.findKeyDeserializer(r8))) == null)) {
            r9 = ((C21991Jm) r9).withKeyValueHandler(keyDeserializerInstance);
            r9.getKeyType();
        }
        Class findDeserializationContentType = annotationIntrospector.findDeserializationContentType(r8, r9.getContentType());
        if (findDeserializationContentType != null) {
            try {
                r9 = r9.narrowContentsBy(findDeserializationContentType);
            } catch (IllegalArgumentException e3) {
                throw new C37701w6("Failed to narrow content type " + r9 + " with content-type annotation (" + findDeserializationContentType.getName() + "): " + e3.getMessage(), null, e3);
            }
        }
        if (r9.getContentType().getValueHandler() != null || (deserializerInstance = r7.deserializerInstance(r8, annotationIntrospector.findContentDeserializer(r8))) == null) {
            return r9;
        }
        return r9.withContentValueHandler(deserializerInstance);
    }

    public C10030jR resolveType(C26791c3 r6, C10120ja r7, C10030jR r8, C183512m r9) {
        C64433Bp findTypeDeserializer;
        C64433Bp buildTypeDeserializer;
        C422628y keyDeserializerInstance;
        if (r8.isContainerType()) {
            C10140jc annotationIntrospector = r6.getAnnotationIntrospector();
            if (!(r8.getKeyType() == null || (keyDeserializerInstance = r6.keyDeserializerInstance(r9, annotationIntrospector.findKeyDeserializer(r9))) == null)) {
                r8 = ((C21991Jm) r8).withKeyValueHandler(keyDeserializerInstance);
                r8.getKeyType();
            }
            JsonDeserializer deserializerInstance = r6.deserializerInstance(r9, annotationIntrospector.findContentDeserializer(r9));
            if (deserializerInstance != null) {
                r8 = r8.withContentValueHandler(deserializerInstance);
            }
            if (r9 instanceof C183512m) {
                C10490kF r4 = r6._config;
                C10140jc annotationIntrospector2 = r4.getAnnotationIntrospector();
                CXQ findPropertyContentTypeResolver = annotationIntrospector2.findPropertyContentTypeResolver(r4, r9, r8);
                C10030jR contentType = r8.getContentType();
                if (findPropertyContentTypeResolver == null) {
                    buildTypeDeserializer = findTypeDeserializer(r4, contentType);
                } else {
                    buildTypeDeserializer = findPropertyContentTypeResolver.buildTypeDeserializer(r4, contentType, r4._subtypeResolver.collectAndResolveSubtypes(r9, r4, annotationIntrospector2, contentType));
                }
                if (buildTypeDeserializer != null) {
                    r8 = r8.withContentTypeHandler(buildTypeDeserializer);
                }
            }
        }
        if (r9 instanceof C183512m) {
            C10490kF r3 = r6._config;
            C10140jc annotationIntrospector3 = r3.getAnnotationIntrospector();
            CXQ findPropertyTypeResolver = annotationIntrospector3.findPropertyTypeResolver(r3, r9, r8);
            if (findPropertyTypeResolver == null) {
                findTypeDeserializer = findTypeDeserializer(r3, r8);
            } else {
                findTypeDeserializer = findPropertyTypeResolver.buildTypeDeserializer(r3, r8, r3._subtypeResolver.collectAndResolveSubtypes(r9, r3, annotationIntrospector3, r8));
            }
        } else {
            findTypeDeserializer = findTypeDeserializer(r6._config, r8);
        }
        if (findTypeDeserializer != null) {
            return r8.withTypeHandler(findTypeDeserializer);
        }
        return r8;
    }
}
