package X;

/* renamed from: X.114  reason: invalid class name */
public enum AnonymousClass114 {
    NON_ADMIN(-1),
    REGULAR_ADMIN(0),
    GROUP_CREATOR(1),
    CHAT_SUPER_ADMIN(2);
    
    public final int dbValue;

    private AnonymousClass114(int i) {
        this.dbValue = i;
    }

    public static AnonymousClass114 A00(int i) {
        for (AnonymousClass114 r1 : values()) {
            if (r1.dbValue == i) {
                return r1;
            }
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown AdminType dbValue of ", i));
    }
}
