package X;

/* renamed from: X.1SI  reason: invalid class name */
public final class AnonymousClass1SI {
    public static C17180yS A00;
    public static final AnonymousClass1O3 A01 = new AnonymousClass1O3("BMP", "bmp");
    public static final AnonymousClass1O3 A02 = new AnonymousClass1O3("DNG", "dng");
    public static final AnonymousClass1O3 A03 = new AnonymousClass1O3("GIF", "gif");
    public static final AnonymousClass1O3 A04 = new AnonymousClass1O3("HEIF", "heif");
    public static final AnonymousClass1O3 A05 = new AnonymousClass1O3("ICO", "ico");
    public static final AnonymousClass1O3 A06 = new AnonymousClass1O3("JPEG", "jpeg");
    public static final AnonymousClass1O3 A07 = new AnonymousClass1O3("PNG", "png");
    public static final AnonymousClass1O3 A08 = new AnonymousClass1O3("WEBP_ANIMATED", "webp");
    public static final AnonymousClass1O3 A09 = new AnonymousClass1O3("WEBP_EXTENDED", "webp");
    public static final AnonymousClass1O3 A0A = new AnonymousClass1O3("WEBP_EXTENDED_WITH_ALPHA", "webp");
    public static final AnonymousClass1O3 A0B = new AnonymousClass1O3("WEBP_LOSSLESS", "webp");
    public static final AnonymousClass1O3 A0C = new AnonymousClass1O3("WEBP_SIMPLE", "webp");

    public static boolean A00(AnonymousClass1O3 r2) {
        if (r2 == A0C || r2 == A0B || r2 == A09 || r2 == A0A) {
            return true;
        }
        return false;
    }
}
