package X;

import android.content.pm.PackageManager;
import android.os.DeadObjectException;

/* renamed from: X.0NT  reason: invalid class name */
public final class AnonymousClass0NT {
    public static boolean A00(PackageManager packageManager, String str) {
        int i = 2;
        while (i >= 0) {
            try {
                packageManager.getPackageInfo(str, 128);
                return true;
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof DeadObjectException) {
                    return false;
                }
                if (i != 0) {
                    i--;
                } else {
                    throw e;
                }
            }
        }
        throw new IllegalStateException("should be unreachable");
    }
}
