package X;

/* renamed from: X.17w  reason: invalid class name and case insensitive filesystem */
public final class C193717w {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("admin_snippet", "BLOB");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6(AnonymousClass24B.$const$string(204), "INTEGER");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("draft", "TEXT");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("last_delivered_receipt_time_ms", "INTEGER");
    public static final AnonymousClass0W6 A04 = new AnonymousClass0W6("last_read_receipt_time_ms", "INTEGER");
    public static final AnonymousClass0W6 A05 = new AnonymousClass0W6("last_read_timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A06 = new AnonymousClass0W6("lookup_state", "INTEGER");
    public static final AnonymousClass0W6 A07 = new AnonymousClass0W6("block", "INTEGER");
    public static final AnonymousClass0W6 A08 = new AnonymousClass0W6("other_user_device_id", "TEXT");
    public static final AnonymousClass0W6 A09 = new AnonymousClass0W6(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1K), "INTEGER");
    public static final AnonymousClass0W6 A0A = new AnonymousClass0W6("outgoing_message_lifetime_ms", "INTEGER");
    public static final AnonymousClass0W6 A0B = new AnonymousClass0W6("session_state", "BLOB");
    public static final AnonymousClass0W6 A0C = new AnonymousClass0W6("snippet", "BLOB");
    public static final AnonymousClass0W6 A0D = new AnonymousClass0W6("snippet_sender_fbid", "INTEGER");
    public static final AnonymousClass0W6 A0E = new AnonymousClass0W6("thread_encryption_key_version", "INTEGER");
    public static final AnonymousClass0W6 A0F = new AnonymousClass0W6("thread_key", "TEXT");
    public static final AnonymousClass0W6 A0G = new AnonymousClass0W6("thread_name", "TEXT");
    public static final AnonymousClass0W6 A0H = new AnonymousClass0W6("timestamp_ms", "INTEGER");
}
