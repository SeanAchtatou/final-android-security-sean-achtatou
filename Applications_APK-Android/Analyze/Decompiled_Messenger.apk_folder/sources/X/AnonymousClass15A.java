package X;

/* renamed from: X.15A  reason: invalid class name */
public final class AnonymousClass15A {
    public final Class A00;
    public final Class A01;
    public final Class A02;
    public final Class A03;
    public final Class A04;
    public final Integer A05;
    public final String A06;

    public AnonymousClass15A(Class cls, Class cls2, Class cls3, Class cls4, Class cls5, Integer num, String str) {
        this.A03 = cls;
        this.A01 = cls2;
        this.A00 = cls3;
        this.A04 = cls4;
        this.A02 = cls5;
        this.A05 = num;
        this.A06 = str;
    }
}
