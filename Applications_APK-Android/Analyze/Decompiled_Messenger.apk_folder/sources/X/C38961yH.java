package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

/* renamed from: X.1yH  reason: invalid class name and case insensitive filesystem */
public final class C38961yH extends AnonymousClass5t1 {
    private final Handler A00 = new Handler(Looper.getMainLooper());
    private final Runnable A01 = new C124185t0(this);

    public void APp() {
        AnonymousClass00S.A05(this.A00, this.A01, 500, 1948623166);
    }

    public void CI0() {
        AnonymousClass00S.A02(this.A00, this.A01);
        super.CI0();
    }

    public C38961yH(Context context, int i) {
        super(context, i);
    }
}
