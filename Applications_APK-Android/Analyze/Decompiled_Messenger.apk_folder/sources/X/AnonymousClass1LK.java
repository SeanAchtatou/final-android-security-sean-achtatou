package X;

import java.io.File;
import java.util.concurrent.Callable;

/* renamed from: X.1LK  reason: invalid class name */
public final class AnonymousClass1LK implements Callable {
    public final /* synthetic */ C09800ii A00;
    public final /* synthetic */ AnonymousClass1LI A01;

    public AnonymousClass1LK(C09800ii r1, AnonymousClass1LI r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public Object call() {
        Class<C09800ii> cls;
        Object[] objArr;
        String str;
        this.A00.A00.A08();
        int A03 = this.A00.A00.A03(this.A01.A00);
        if (A03 == 0) {
            this.A00.A00.A0A(1);
            AnonymousClass1LJ.A01.remove(this.A01);
            return null;
        } else if (A03 != 1) {
            int i = 3;
            if (A03 == 2) {
                C09800ii.A01(this.A00, this.A01, false);
            } else if (A03 != 3) {
                throw new RuntimeException("Unknown cache state");
            }
            boolean z = false;
            if (A03 == 2) {
                z = true;
            }
            C09800ii r0 = this.A00;
            if (!z) {
                i = 4;
            }
            r0.A00.A0A(i);
            AnonymousClass1LJ.A01.remove(this.A01);
            File A04 = this.A00.A00.A04();
            String str2 = "OLD";
            if (A04 == null) {
                cls = C09800ii.class;
                if (!z) {
                    str2 = "LATEST";
                }
                objArr = new Object[]{str2};
                str = "Unable to get file path for %s file";
            } else if (!A04.isFile()) {
                cls = C09800ii.class;
                if (!z) {
                    str2 = "LATEST";
                }
                objArr = new Object[]{str2};
                str = "File wasn't a file for %s file";
            } else {
                C09800ii r02 = this.A00;
                int i2 = -2147483641;
                if (z) {
                    i2 = 6;
                }
                r02.A00.A0A(i2);
                AnonymousClass1LI r03 = this.A01;
                return new C22371Lb(r03.A01, r03.A00, A04, z);
            }
            C010708t.A0D(cls, str, objArr);
            return null;
        } else {
            this.A00.A00.A0A(2);
            C22371Lb A012 = C09800ii.A01(this.A00, this.A01, true);
            AnonymousClass1LJ.A01.remove(this.A01);
            return A012;
        }
    }
}
