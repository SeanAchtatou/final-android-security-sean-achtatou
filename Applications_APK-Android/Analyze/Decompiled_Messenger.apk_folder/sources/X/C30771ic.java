package X;

import java.util.Map;

/* renamed from: X.1ic  reason: invalid class name and case insensitive filesystem */
public abstract class C30771ic implements C30781id {
    private final int A00;
    private final long A01;
    private final C23121Oh A02;
    private final String A03;
    private final String A04;
    private final String A05;
    public final C23051Oa A06;
    private volatile Map A07;

    public int Acu() {
        return this.A00;
    }

    public String Ayb() {
        return this.A05;
    }

    public C23121Oh B00() {
        return this.A02;
    }

    public long B3V() {
        return this.A01;
    }

    public String B8L() {
        return this.A03;
    }

    public void C2s(String str, String str2) {
        this.A06.C08(this, AnonymousClass08S.A0S("PARAM ACCESS ERROR - ", str2, ": ", str), Acu());
    }

    public String getName() {
        return this.A04;
    }

    public C30771ic(C23121Oh r4, C23131Oi r5, int i, C23051Oa r7) {
        Integer num;
        int intValue;
        if (r5 == null || (num = r5.A02) == null) {
            throw new C37861wT("Bad config");
        } else if (num.intValue() == i) {
            Integer num2 = r5.A01;
            if (num2 == null || (intValue = num2.intValue()) <= 0) {
                this.A00 = 10;
            } else {
                this.A00 = intValue;
            }
            String str = r5.A05;
            if (str != null) {
                this.A05 = str;
            } else {
                this.A05 = "unknown";
            }
            String str2 = r5.A04;
            if (str2 != null) {
                this.A04 = str2;
            } else {
                this.A04 = "unknown";
            }
            this.A02 = r4;
            this.A03 = r5.A03 + ":" + num;
            this.A01 = r5.A00;
            this.A06 = r7;
        } else {
            throw new C37861wT("Unsupported config version");
        }
    }
}
