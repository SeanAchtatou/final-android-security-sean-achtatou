package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;

/* renamed from: X.1FU  reason: invalid class name */
public final class AnonymousClass1FU implements AnonymousClass1FI {
    public static final AnonymousClass1FU A00() {
        return new AnonymousClass1FU();
    }

    public ImmutableMap get() {
        return ImmutableMap.of(new SubscribeTopic(TurboLoader.Locator.$const$string(96), 0), AnonymousClass1FP.ALWAYS);
    }
}
