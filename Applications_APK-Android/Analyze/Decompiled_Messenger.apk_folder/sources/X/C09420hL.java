package X;

/* renamed from: X.0hL  reason: invalid class name and case insensitive filesystem */
public final class C09420hL {
    public final long A00;
    public final String A01;

    public String toString() {
        return "UniqueDeviceId{id=" + this.A01 + ", timestamp=" + this.A00 + "}";
    }

    public C09420hL(String str, long j) {
        this.A01 = str;
        this.A00 = j;
    }
}
