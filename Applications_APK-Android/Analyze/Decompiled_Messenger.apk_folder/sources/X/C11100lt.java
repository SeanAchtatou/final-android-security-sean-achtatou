package X;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0lt  reason: invalid class name and case insensitive filesystem */
public final class C11100lt {
    public int A00;
    public int A01;
    public ArrayList A02;
    public int[] A03 = new int[5];
    public long[] A04 = new long[5];
    public SparseArray[] A05 = new SparseArray[5];
    public AnonymousClass0lr[] A06 = new AnonymousClass0lr[5];
    public String[] A07 = new String[5];

    public void A01(C21921Jf r12) {
        for (int i = 0; i < this.A01; i++) {
            r12.CMO(TimeUnit.NANOSECONDS.toMillis(this.A04[i]), this.A04[i], this.A03[i], this.A07[i], this.A06[i], this.A05[i]);
        }
    }

    public void A00(long j, TimeUnit timeUnit, int i, String str, AnonymousClass0lr r10, SparseArray sparseArray) {
        int i2;
        int hashCode = str.hashCode();
        int i3 = this.A00;
        if ((hashCode & i3) == hashCode) {
            i2 = this.A01;
            int i4 = 0;
            while (true) {
                if (i4 < i2) {
                    if (this.A07[i4].equals(str)) {
                        break;
                    }
                    i4++;
                } else {
                    i4 = -1;
                    break;
                }
            }
            if (i4 >= 0) {
                if (this.A02 == null) {
                    this.A02 = new ArrayList();
                }
                this.A02.add(str);
                return;
            }
            this.A01 = i2 + 1;
        } else {
            i2 = this.A01;
            this.A01 = i2 + 1;
            this.A00 = hashCode | i3;
        }
        long[] jArr = this.A04;
        if (i2 == jArr.length) {
            int i5 = (i2 >> 1) + i2;
            this.A04 = Arrays.copyOf(jArr, i5);
            this.A07 = (String[]) Arrays.copyOf(this.A07, i5);
            this.A06 = (AnonymousClass0lr[]) Arrays.copyOf(this.A06, i5);
            this.A03 = Arrays.copyOf(this.A03, i5);
            this.A05 = (SparseArray[]) Arrays.copyOf(this.A05, i5);
        }
        if (r10 == null || r10.A03) {
            this.A04[i2] = timeUnit.toNanos(j);
            this.A07[i2] = str;
            this.A06[i2] = r10;
            this.A03[i2] = i;
            this.A05[i2] = sparseArray;
            return;
        }
        throw new IllegalStateException("PointData should be locked before passing to the storage");
    }
}
