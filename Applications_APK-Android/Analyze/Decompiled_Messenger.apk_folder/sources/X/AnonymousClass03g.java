package X;

import android.content.Context;
import android.location.LocationManager;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.03g  reason: invalid class name */
public final class AnonymousClass03g {
    private static volatile AnonymousClass03g A02;
    private AnonymousClass0UN A00;
    private final C04310Tq A01;

    public int A05(boolean z) {
        if (z) {
            return ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).AqL(564414537335638L, 1);
        }
        return 1;
    }

    public static final AnonymousClass03g A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass03g.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass03g(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static boolean A03(AnonymousClass03g r6) {
        Context context = (Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, r6.A00);
        boolean z = false;
        if (new C33561nm(context, (LocationManager) context.getSystemService("location"), false).A05() == AnonymousClass07B.A0N) {
            z = true;
        }
        boolean asBoolean = ((AnonymousClass0U9) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ACM, r6.A00)).A00.Aeq(C06540bf.A06).asBoolean(false);
        if (!z || !asBoolean) {
            return false;
        }
        return true;
    }

    public static boolean A04(AnonymousClass03g r3) {
        return ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, r3.A00)).Aem(281938833245043L);
    }

    public String A07() {
        String A002 = AnonymousClass0A9.A00((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00));
        if (A002 != null && A02()) {
            return A002;
        }
        if (A0E()) {
            return ((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00)).getPackageName();
        }
        return null;
    }

    public boolean A08() {
        C001500z r3 = C001500z.A07;
        C001500z r2 = (C001500z) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BH4, this.A00);
        if (r3 == r2 || C001500z.A03 == r2 || C001500z.A08 == r2) {
            return true;
        }
        return false;
    }

    public boolean A0A() {
        return ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aem(2306124948047070069L);
    }

    public boolean A0B() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, this.A00)).AbO(77, false);
    }

    private AnonymousClass03g(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A01 = AnonymousClass0XJ.A0K(r3);
    }

    public static final AnonymousClass03g A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    private boolean A02() {
        if (A0B() || !((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, this.A00)).AbO(78, true) || ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aem(285198713361897L)) {
            return false;
        }
        return true;
    }

    public Integer A06() {
        if (!A0C() || !"com.facebook.orca".equals(((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00)).getPackageName())) {
            return null;
        }
        return 300;
    }

    public boolean A09() {
        if (!A04(this) || !((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aem(281938833310580L)) {
            return false;
        }
        return true;
    }

    public boolean A0C() {
        if (A08() && this.A01.get() != null) {
            boolean z = false;
            if (AnonymousClass0A9.A00((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00)) != null) {
                z = true;
            }
            if (z) {
                return A02();
            }
        }
        return false;
    }

    public boolean A0D() {
        if (!A08() || this.A01.get() == null) {
            return false;
        }
        boolean z = false;
        if (AnonymousClass0A9.A00((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00)) != null) {
            z = true;
        }
        if (!z || !A02() || ((C25051Yd) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AOJ, this.A00)).Aem(285198713427434L)) {
            return false;
        }
        return true;
    }

    public boolean A0E() {
        if (!A08() || this.A01.get() == null) {
            return false;
        }
        return A04(this);
    }
}
