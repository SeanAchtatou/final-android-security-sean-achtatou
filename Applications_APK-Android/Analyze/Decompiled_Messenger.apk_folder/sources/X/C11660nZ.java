package X;

import android.content.res.Resources;
import android.os.Build;
import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0nZ  reason: invalid class name and case insensitive filesystem */
public final class C11660nZ {
    private static volatile C11660nZ A05;
    public boolean A00;
    private boolean A01;
    private boolean A02;
    private final AnonymousClass1YI A03;
    private final FbSharedPreferences A04;

    public static final C11660nZ A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C11660nZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C11660nZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public final void A01(Resources.Theme theme) {
        if (!this.A01 || !A02()) {
            theme.applyStyle(2132476127, true);
            return;
        }
        theme.applyStyle(2132476125, true);
        if (this.A02) {
            theme.applyStyle(2132476128, true);
        }
    }

    public final boolean A02() {
        if (Build.VERSION.SDK_INT <= 16 || !this.A03.AbO(617, false)) {
            return false;
        }
        return true;
    }

    public final boolean A03() {
        if (!this.A01 || !A02()) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0025, code lost:
        if (r3.A04.Aep(X.C27831dp.A04, false) == false) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C11660nZ(X.AnonymousClass1XY r4) {
        /*
            r3 = this;
            r3.<init>()
            com.facebook.prefs.shared.FbSharedPreferences r0 = com.facebook.prefs.shared.FbSharedPreferencesModule.A00(r4)
            r3.A04 = r0
            X.1YI r0 = X.AnonymousClass0WA.A00(r4)
            r3.A03 = r0
            com.facebook.prefs.shared.FbSharedPreferences r1 = r3.A04
            X.1Y7 r0 = X.C27831dp.A02
            r2 = 0
            boolean r0 = r1.Aep(r0, r2)
            r3.A01 = r0
            if (r0 == 0) goto L_0x0027
            com.facebook.prefs.shared.FbSharedPreferences r1 = r3.A04
            X.1Y7 r0 = X.C27831dp.A04
            boolean r1 = r1.Aep(r0, r2)
            r0 = 1
            if (r1 != 0) goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            r3.A02 = r0
            com.facebook.prefs.shared.FbSharedPreferences r1 = r3.A04
            X.1Y7 r0 = X.C27831dp.A03
            r1.Aep(r0, r2)
            com.facebook.prefs.shared.FbSharedPreferences r1 = r3.A04
            X.1Y7 r0 = X.C27831dp.A01
            boolean r0 = r1.Aep(r0, r2)
            r3.A00 = r0
            com.facebook.prefs.shared.FbSharedPreferences r1 = r3.A04
            X.1Y7 r0 = X.C27831dp.A00
            r1.Aep(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11660nZ.<init>(X.1XY):void");
    }
}
