package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eS  reason: invalid class name and case insensitive filesystem */
public final class C07960eS extends C07880eK {
    private static volatile C07960eS A04;
    public final C07970eT A00;
    private final AnonymousClass09P A01;
    private final C25051Yd A02;
    private final C07990eV A03;

    public static final C07960eS A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C07960eS.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C07960eS(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private C07960eS(AnonymousClass1XY r3) {
        this.A00 = C07970eT.A00(r3);
        this.A02 = AnonymousClass0WT.A00(r3);
        this.A01 = C04750Wa.A01(r3);
        this.A03 = C07990eV.A00(r3);
        A06(this.A02, this.A01);
    }
}
