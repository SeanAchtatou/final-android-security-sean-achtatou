package X;

import java.io.File;

/* renamed from: X.0Kb  reason: invalid class name and case insensitive filesystem */
public final class C03110Kb {
    public static final int A00;

    static {
        int A01 = (int) AnonymousClass0Gf.A01(-1);
        if (A01 < 0) {
            File file = new File("/sys/devices/system/cpu/");
            if (!file.exists() || !file.isDirectory()) {
                A01 = 0;
            } else {
                A01 = file.listFiles(new C03100Ka()).length;
            }
        }
        A00 = A01;
    }
}
