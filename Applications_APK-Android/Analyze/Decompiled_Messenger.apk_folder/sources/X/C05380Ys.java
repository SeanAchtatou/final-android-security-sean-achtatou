package X;

/* renamed from: X.0Ys  reason: invalid class name and case insensitive filesystem */
public final class C05380Ys extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.common.phantomdestructors.DestructorManagerImpl$1";
    public final /* synthetic */ C05300Yk A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C05380Ys(C05300Yk r1, String str) {
        super(str);
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (r3.A03.isEmpty() == false) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        r3.A00 = false;
        r3.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0046, code lost:
        if (r1 == false) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0030, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0030, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        monitor-enter(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            X.0Yk r3 = r5.A00
        L_0x0002:
            r0 = 0
        L_0x0003:
            if (r0 == 0) goto L_0x0011
            java.lang.ref.ReferenceQueue r4 = r3.A02     // Catch:{ InterruptedException -> 0x0003 }
            int r1 = r3.A01     // Catch:{ InterruptedException -> 0x0003 }
            long r1 = (long) r1     // Catch:{ InterruptedException -> 0x0003 }
            java.lang.ref.Reference r4 = r4.remove(r1)     // Catch:{ InterruptedException -> 0x0003 }
            X.0Yo r4 = (X.C05340Yo) r4     // Catch:{ InterruptedException -> 0x0003 }
            goto L_0x0019
        L_0x0011:
            java.lang.ref.ReferenceQueue r1 = r3.A02     // Catch:{ InterruptedException -> 0x0003 }
            java.lang.ref.Reference r4 = r1.remove()     // Catch:{ InterruptedException -> 0x0003 }
            X.0Yo r4 = (X.C05340Yo) r4     // Catch:{ InterruptedException -> 0x0003 }
        L_0x0019:
            if (r4 != 0) goto L_0x0033
            monitor-enter(r3)
            java.util.HashSet r0 = r3.A03     // Catch:{ all -> 0x006a }
            boolean r1 = r0.isEmpty()     // Catch:{ all -> 0x006a }
            r0 = 0
            if (r1 == 0) goto L_0x002b
            r3.A00 = r0     // Catch:{ all -> 0x006a }
            r3.notifyAll()     // Catch:{ all -> 0x006a }
            goto L_0x002e
        L_0x002b:
            monitor-exit(r3)
            r0 = 0
            goto L_0x0030
        L_0x002e:
            monitor-exit(r3)
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x0002
            return
        L_0x0033:
            X.0Xz r1 = r4.A00
            monitor-enter(r1)
            boolean r0 = r1.A01     // Catch:{ all -> 0x006d }
            if (r0 != 0) goto L_0x0051
            java.lang.Throwable r2 = r1.A00     // Catch:{ all -> 0x006d }
            if (r2 == 0) goto L_0x0051
            monitor-exit(r1)     // Catch:{ all -> 0x006d }
            X.00K r0 = X.AnonymousClass00J.A01
            if (r0 == 0) goto L_0x0048
            boolean r1 = r0.A1h
            r0 = 1
            if (r1 != 0) goto L_0x0049
        L_0x0048:
            r0 = 0
        L_0x0049:
            if (r0 == 0) goto L_0x0052
            X.4pP r0 = new X.4pP
            r0.<init>(r2)
            throw r0
        L_0x0051:
            monitor-exit(r1)     // Catch:{ all -> 0x006d }
        L_0x0052:
            monitor-enter(r3)
            java.util.HashSet r0 = r3.A03     // Catch:{ all -> 0x006a }
            boolean r0 = r0.remove(r4)     // Catch:{ all -> 0x006a }
            r1 = 0
            if (r0 == 0) goto L_0x0064
            java.util.HashSet r0 = r3.A03     // Catch:{ all -> 0x006a }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x006a }
            monitor-exit(r3)
            goto L_0x0003
        L_0x0064:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x006a }
            r0.<init>(r1)     // Catch:{ all -> 0x006a }
            throw r0     // Catch:{ all -> 0x006a }
        L_0x006a:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x006d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x006d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05380Ys.run():void");
    }
}
