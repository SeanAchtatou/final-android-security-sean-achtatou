package X;

import com.facebook.orca.threadlist.ThreadListFragment;
import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.16N  reason: invalid class name */
public final class AnonymousClass16N {
    public static final Class A09 = AnonymousClass16N.class;
    public int A00;
    public C13930sJ A01;
    public C13880sE A02;
    public ListenableFuture A03;
    public boolean A04;
    private Integer A05;
    private boolean A06;
    private final C06480bZ A07;
    private final Runnable A08 = new AnonymousClass10C(this);

    public static final AnonymousClass16N A00(AnonymousClass1XY r2) {
        return new AnonymousClass16N(C06460bX.A00(r2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r2.A04 != false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.AnonymousClass16N r2) {
        /*
            int r0 = r2.A00
            if (r0 > 0) goto L_0x0009
            boolean r1 = r2.A04
            r0 = 0
            if (r1 == 0) goto L_0x000a
        L_0x0009:
            r0 = 1
        L_0x000a:
            if (r0 != 0) goto L_0x001f
            boolean r0 = r2.A06
            if (r0 == 0) goto L_0x001f
            X.0sE r0 = r2.A02
            com.google.common.base.Preconditions.checkNotNull(r0)
            java.lang.Integer r1 = r2.A05
            X.0sE r0 = r2.A02
            r2.A03(r1, r0)
            r0 = 0
            r2.A06 = r0
        L_0x001f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass16N.A01(X.16N):void");
    }

    public static void A02(AnonymousClass16N r2, C13880sE r3) {
        ListenableFuture listenableFuture = r2.A03;
        if (listenableFuture != null) {
            listenableFuture.cancel(false);
            r2.A03 = null;
        }
        C13930sJ r0 = r2.A01;
        if (r0 != null) {
            ThreadListFragment.A0I(r0.A00, r3);
        }
    }

    public AnonymousClass16N(C06480bZ r2) {
        this.A07 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0022, code lost:
        if (r2.intValue() < r4.intValue()) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        if (r3.A04 != false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(java.lang.Integer r4, X.C13880sE r5) {
        /*
            r3 = this;
            com.google.common.base.Preconditions.checkNotNull(r4)
            int r0 = r3.A00
            if (r0 > 0) goto L_0x000c
            boolean r1 = r3.A04
            r0 = 0
            if (r1 == 0) goto L_0x000d
        L_0x000c:
            r0 = 1
        L_0x000d:
            if (r0 == 0) goto L_0x002e
            r0 = 1
            r3.A06 = r0
            r3.A02 = r5
            java.lang.Integer r2 = r3.A05
            if (r2 == 0) goto L_0x0028
            if (r4 == 0) goto L_0x0028
            int r1 = r2.intValue()
            int r0 = r4.intValue()
            if (r1 >= r0) goto L_0x0025
        L_0x0024:
            r4 = r2
        L_0x0025:
            r3.A05 = r4
        L_0x0027:
            return
        L_0x0028:
            if (r2 != 0) goto L_0x0024
            if (r4 != 0) goto L_0x0025
            r4 = 0
            goto L_0x0025
        L_0x002e:
            r0 = 0
            r3.A05 = r0
            int r1 = r4.intValue()
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            int r0 = r0.intValue()
            if (r1 >= r0) goto L_0x0041
            A02(r3, r5)
            return
        L_0x0041:
            r3.A02 = r5
            com.google.common.util.concurrent.ListenableFuture r0 = r3.A03
            if (r0 != 0) goto L_0x0027
            X.0bZ r1 = r3.A07
            java.lang.Runnable r0 = r3.A08
            com.google.common.util.concurrent.ListenableFuture r0 = r1.CIC(r0)
            r3.A03 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass16N.A03(java.lang.Integer, X.0sE):void");
    }
}
