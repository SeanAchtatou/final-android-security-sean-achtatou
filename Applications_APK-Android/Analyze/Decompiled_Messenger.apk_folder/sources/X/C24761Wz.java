package X;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.util.Log;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.google.firebase.iid.zzm;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.1Wz  reason: invalid class name and case insensitive filesystem */
public final class C24761Wz extends AnonymousClass1X0 {
    private final /* synthetic */ C24751Wy A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C24761Wz(C24751Wy r1, Looper looper) {
        super(looper);
        this.A00 = r1;
    }

    public final void handleMessage(Message message) {
        String stringExtra;
        String str;
        String str2;
        C24751Wy r4 = this.A00;
        if (message != null) {
            Object obj = message.obj;
            if (obj instanceof Intent) {
                Intent intent = (Intent) obj;
                intent.setExtrasClassLoader(new C185598jQ());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof zzm) {
                        r4.A01 = (zzm) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        r4.A00 = (Messenger) parcelableExtra;
                    }
                }
                Intent intent2 = (Intent) message.obj;
                String action = intent2.getAction();
                if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
                    stringExtra = intent2.getStringExtra("registration_id");
                    if (stringExtra == null) {
                        stringExtra = intent2.getStringExtra(TurboLoader.Locator.$const$string(26));
                    }
                    if (stringExtra == null) {
                        String stringExtra2 = intent2.getStringExtra("error");
                        if (stringExtra2 == null) {
                            String valueOf = String.valueOf(intent2.getExtras());
                            StringBuilder sb = new StringBuilder(valueOf.length() + 49);
                            sb.append("Unexpected response, no error or registration id ");
                            sb.append(valueOf);
                            Log.w("FirebaseInstanceId", sb.toString());
                            return;
                        }
                        if (Log.isLoggable("FirebaseInstanceId", 3) && stringExtra2.length() == 0) {
                            new String("Received InstanceID error ");
                        }
                        if (stringExtra2.startsWith("|")) {
                            String[] split = stringExtra2.split("\\|");
                            if (split.length <= 2 || !"ID".equals(split[1])) {
                                if (stringExtra2.length() != 0) {
                                    str2 = "Unexpected structured response ".concat(stringExtra2);
                                } else {
                                    str2 = new String("Unexpected structured response ");
                                }
                                Log.w("FirebaseInstanceId", str2);
                                return;
                            }
                            String str3 = split[2];
                            String str4 = split[3];
                            if (str4.startsWith(":")) {
                                str4 = str4.substring(1);
                            }
                            C24751Wy.A02(r4, str3, intent2.putExtra("error", str4).getExtras());
                            return;
                        }
                        synchronized (r4.A04) {
                            int i = 0;
                            while (true) {
                                AnonymousClass04b r1 = r4.A04;
                                if (i < r1.size()) {
                                    C24751Wy.A02(r4, (String) r1.A07(i), intent2.getExtras());
                                    i++;
                                }
                            }
                        }
                        return;
                    }
                    Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
                    if (matcher.matches()) {
                        String group = matcher.group(1);
                        String group2 = matcher.group(2);
                        Bundle extras = intent2.getExtras();
                        extras.putString("registration_id", group2);
                        C24751Wy.A02(r4, group, extras);
                        return;
                    } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        str = "Unexpected response string: ";
                    } else {
                        return;
                    }
                } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    str = "Unexpected response action: ";
                    stringExtra = String.valueOf(action);
                } else {
                    return;
                }
                if (stringExtra.length() == 0) {
                    new String(str);
                    return;
                }
                return;
            }
        }
        Log.w("FirebaseInstanceId", "Dropping invalid message");
    }
}
