package X;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

/* renamed from: X.0Vk  reason: invalid class name and case insensitive filesystem */
public final class C04610Vk extends AnonymousClass0VZ {
    public AnonymousClass1YF A00;
    public List A01;
    public PriorityQueue A02;
    public boolean A03 = true;
    public boolean A04;
    public boolean A05;
    public final int A06;
    public final AnonymousClass0VZ A07;
    public final C04540Vc A08;
    public final C04630Vm A09;
    public final AnonymousClass0VY A0A;
    public final String A0B;
    public volatile C04620Vl A0C = C04620Vl.A01;

    public static int A01(C04610Vk r2) {
        PriorityQueue priorityQueue = r2.A02;
        int i = 0;
        if (priorityQueue != null) {
            i = 0 + priorityQueue.size();
        }
        List list = r2.A01;
        if (list != null) {
            return i + list.size();
        }
        return i;
    }

    public static boolean A03(C04610Vk r3) {
        int i = r3.A08.A00 - r3.A09.A00;
        boolean z = false;
        if (i >= 0) {
            z = true;
        }
        Preconditions.checkState(z);
        if (i > 0 || r3.A05(new C42812By(r3)) != null) {
            return false;
        }
        return true;
    }

    public void A0H(C04560Ve r4) {
        C04620Vl r2 = this.A0C;
        C04620Vl r1 = C04620Vl.SHUTTING_DOWN;
        if (r2.compareTo((Enum) r1) >= 0) {
            r1 = r2;
        }
        this.A0C = r1;
        if (this.A00 == null) {
            this.A00 = new AnonymousClass1YF(r4);
        }
        if (A03(this)) {
            this.A0C = C04620Vl.TERMINATED;
            this.A00.A03();
        }
    }

    public C04610Vk(AnonymousClass0VZ r3, AnonymousClass0VY r4, int i, String str, int i2) {
        this.A0A = r4;
        this.A07 = r3;
        this.A08 = new C04540Vc(i);
        this.A0B = str;
        this.A09 = new C04630Vm(i);
        this.A06 = i2;
        if (i2 == 0 || i == Integer.MAX_VALUE) {
            this.A05 = true;
        }
    }

    public static void A02(ArrayList arrayList, Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            arrayList.add(((C07820eD) it.next()).C4G());
        }
    }

    public String toString() {
        Object valueOf;
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("mName:", this.A0B);
        int i = this.A08.A00 - this.A09.A00;
        boolean z = false;
        if (i >= 0) {
            z = true;
        }
        Preconditions.checkState(z);
        stringHelper.add("active", i);
        ArrayList<C04610Vk> arrayList = new ArrayList<>();
        arrayList.add(this);
        A0D(arrayList);
        int i2 = 0;
        for (C04610Vk A012 : arrayList) {
            i2 += A01(A012);
        }
        stringHelper.add("pending", i2 + this.A09.A00);
        Object obj = "(null)";
        if (this.A02 == null) {
            valueOf = obj;
        } else {
            valueOf = Integer.valueOf(A01(this));
        }
        stringHelper.add("exclusive", valueOf);
        List list = this.A01;
        if (list != null) {
            obj = Integer.valueOf(list.size());
        }
        stringHelper.add("delayed", obj);
        stringHelper.add("parentPend", this.A09.A00);
        return stringHelper.toString();
    }
}
