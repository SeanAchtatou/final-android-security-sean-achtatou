package X;

import com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000;
import com.google.common.collect.ImmutableSet;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1U9  reason: invalid class name */
public final class AnonymousClass1U9 implements AnonymousClass1U7 {
    public static final double A03 = Double.parseDouble(C26901cK.A03().toString());
    private static final ImmutableSet A04 = ImmutableSet.A04("nt_context");
    private static volatile AnonymousClass1U9 A05;
    private AnonymousClass0UN A00;
    public final String A01 = "0740e799e4c7e89308b0f7e9a5225f1b";
    private final C32211lO A02;

    public static final AnonymousClass1U9 A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (AnonymousClass1U9.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new AnonymousClass1U9(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public GQLCallInputCInputShape0S0000000 A01() {
        GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = new GQLCallInputCInputShape0S0000000(99);
        gQLCallInputCInputShape0S0000000.A09("styles_id", this.A01);
        gQLCallInputCInputShape0S0000000.A07(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1N), Double.valueOf(A03));
        if (this.A02.A02()) {
            gQLCallInputCInputShape0S0000000.A06("is_data_savings_mode_active", true);
        }
        if (C006006f.A01()) {
            gQLCallInputCInputShape0S0000000.A06("is_e2e_test", true);
        }
        if (((C180438Xh) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AK2, this.A00)).A01()) {
            gQLCallInputCInputShape0S0000000.A06("using_white_navbar", true);
        }
        return gQLCallInputCInputShape0S0000000;
    }

    public ImmutableSet Ax9() {
        return A04;
    }

    private AnonymousClass1U9(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        C33411nX.A00(r3);
        this.A02 = C32211lO.A00(r3);
    }

    public Object AxA(String str, C10880l0 r3) {
        return A01();
    }
}
