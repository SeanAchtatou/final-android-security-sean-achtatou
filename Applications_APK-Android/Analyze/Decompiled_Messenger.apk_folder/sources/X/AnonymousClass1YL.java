package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Executor;

/* renamed from: X.1YL  reason: invalid class name */
public abstract class AnonymousClass1YL {
    public final WeakHashMap A00 = new WeakHashMap();

    public synchronized void A01(Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        if (obj != null) {
            z2 = true;
        }
        AnonymousClass064.A03(z2);
        if (obj2 == null) {
            z = false;
        }
        AnonymousClass064.A03(z);
        Object obj3 = (Set) this.A00.get(obj2);
        if (obj3 == null) {
            obj3 = new HashSet(4);
        }
        obj3.add(obj);
        this.A00.put(obj2, obj3);
    }

    public synchronized void A02(Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        if (obj != null) {
            z2 = true;
        }
        AnonymousClass064.A03(z2);
        if (obj2 == null) {
            z = false;
        }
        AnonymousClass064.A03(z);
        Set set = (Set) this.A00.get(obj2);
        if (set != null) {
            set.remove(obj);
            if (set.isEmpty()) {
                this.A00.remove(obj2);
            }
        }
    }

    public void A03(Object obj, Object obj2, Object obj3) {
        Object obj4 = obj;
        if (this instanceof AnonymousClass1YN) {
            ((C26001ak) obj4).onDurableStorageSync((FbSharedPreferences) obj2, (AnonymousClass1Y7) obj3);
        } else if (this instanceof AnonymousClass1YK) {
            ((AnonymousClass0ZM) obj4).onSharedPreferenceChanged((FbSharedPreferences) obj2, (AnonymousClass1Y7) obj3);
        } else if (this instanceof AnonymousClass14N) {
            ((C30393EvU) obj4).onAttributionIdChanged();
        } else if (this instanceof C08160em) {
            C30368Ev1 ev1 = (C30368Ev1) obj4;
            C04210Sx r11 = (C04210Sx) obj2;
            synchronized (((C08160em) this)) {
                ev1.onURIChanged(r11.A00);
            }
        } else if (this instanceof C08170en) {
            C11620nT r112 = (C11620nT) obj2;
            ((C83933yU) obj4).onSessionFragmentsNavigationPathChanged$REDEX$mbMbsOrIKwl(r112.A02, r112.A00, r112.A01);
        } else if (!(this instanceof C07720e3)) {
            ((AnonymousClass0YJ) obj4).ARA(obj2, obj3);
        } else {
            C83913yS r113 = (C83913yS) obj2;
            ((C83903yR) obj4).onNavigationPathChanged$REDEX$t8OeH9J30dk(r113.A04, r113.A00, r113.A01, r113.A02, r113.A03);
        }
    }

    public synchronized boolean A05(Collection collection) {
        for (Object next : collection) {
            Iterator it = this.A00.entrySet().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (A06((Set) ((Map.Entry) it.next()).getValue(), next)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean A06(Set set, Object obj) {
        if (this instanceof AnonymousClass1YO) {
            AnonymousClass1Y7 r4 = (AnonymousClass1Y7) obj;
            Iterator it = set.iterator();
            while (it.hasNext()) {
                if (r4.A07((AnonymousClass1Y7) it.next())) {
                    return true;
                }
            }
            return false;
        } else if (!(this instanceof AnonymousClass1YM)) {
            return set.contains(obj);
        } else {
            AnonymousClass1Y7 r42 = (AnonymousClass1Y7) obj;
            Iterator it2 = set.iterator();
            while (it2.hasNext()) {
                if (r42.A07((AnonymousClass1Y7) it2.next())) {
                    return true;
                }
            }
            return false;
        }
    }

    public void A04(Collection collection, Object obj, Executor executor) {
        if (A05(collection)) {
            AnonymousClass07A.A04(executor, new C98924o5(this, collection, obj), -1947045230);
        }
    }
}
