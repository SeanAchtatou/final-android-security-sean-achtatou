package X;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.ContextChain;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1JE  reason: invalid class name */
public final class AnonymousClass1JE extends C17770zR {
    public static final Handler A0A = new Handler(Looper.getMainLooper());
    @Comparable(type = 0)
    public float A00 = 1.0f;
    @Comparable(type = 13)
    public Uri A01;
    @Comparable(type = 13)
    public CallerContext A02;
    @Comparable(type = 13)
    public ContextChain A03;
    @Comparable(type = 13)
    public C27482DdQ A04;
    public C27481DdP A05;
    @Comparable(type = 13)
    public C27495Ddd A06;
    @Comparable(type = 14)
    public C156827Mz A07;
    @Comparable(type = 13)
    public AnonymousClass3JC A08;
    public AnonymousClass0UN A09;

    public static ComponentBuilderCBuilderShape0_0S0100000 A00(AnonymousClass0p4 r4) {
        ComponentBuilderCBuilderShape0_0S0100000 componentBuilderCBuilderShape0_0S0100000 = new ComponentBuilderCBuilderShape0_0S0100000(1);
        ComponentBuilderCBuilderShape0_0S0100000.A01(componentBuilderCBuilderShape0_0S0100000, r4, 0, 0, new AnonymousClass1JE(r4.A09));
        return componentBuilderCBuilderShape0_0S0100000;
    }

    public static void A02(C27481DdP ddP) {
        Runnable runnable;
        synchronized (ddP) {
            runnable = ddP.A0G;
            ddP.A0G = null;
        }
        if (runnable != null) {
            AnonymousClass00S.A02(A0A, runnable);
        }
    }

    public int A0M() {
        return 3;
    }

    public AnonymousClass1JE(Context context) {
        super("FbFrescoVitoImage");
        this.A09 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
        this.A07 = new C156827Mz();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0015, code lost:
        if (r19 == null) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.AnonymousClass0p4 r10, X.C74093hG r11, X.AnonymousClass852 r12, com.facebook.common.callercontext.CallerContext r13, X.C27482DdQ r14, android.net.Uri r15, X.C22016Amm r16, X.AnonymousClass3JC r17, X.C27495Ddd r18, X.C27481DdP r19) {
        /*
            r7 = r17
            r8 = r13
            X.DdQ r0 = r12.A00(r13, r14)
            X.Amj r3 = r0.A00
            X.DdS r0 = r0.A04
            boolean r0 = r0.A0V()
            r1 = r18
            if (r0 != 0) goto L_0x0017
            r2 = r19
            if (r19 != 0) goto L_0x002b
        L_0x0017:
            if (r17 != 0) goto L_0x001b
            X.3JC r7 = X.AnonymousClass3JC.A0G
        L_0x001b:
            X.DdP r4 = r11.A00
            android.content.res.Resources r9 = r10.A03()
            r10 = 0
            r5 = r15
            r6 = r16
            X.DdP r2 = r3.BjP(r4, r5, r6, r7, r8, r9, r10)
            r11.A00 = r2
        L_0x002b:
            r2.A09 = r11
            r3.BOU(r2, r1)
            int r0 = r2.A02
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            int r0 = r2.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            X.C74253hW.A00(r13, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1JE.A03(X.0p4, X.3hG, X.852, com.facebook.common.callercontext.CallerContext, X.DdQ, android.net.Uri, X.Amm, X.3JC, X.Ddd, X.DdP):void");
    }

    public C15690vh A0R() {
        AnonymousClass852 r0 = (AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, this.A09);
        if (r0 == null) {
            return new C15720vn(3, true);
        }
        C27484DdS ddS = r0.A02.A04;
        ddS.A06();
        return new C15720vn(ddS.A0X(), ddS.A0J());
    }

    public static void A01(C74093hG r1, AnonymousClass852 r2, CallerContext callerContext, C27482DdQ ddQ, C27481DdP ddP) {
        C27482DdQ A002 = r2.A00(callerContext, ddQ);
        if (A002.A04.A0V() || ddP == null) {
            C27481DdP ddP2 = r1.A00;
            if (ddP2 != null) {
                A002.A00.BW6(ddP2);
                return;
            }
            return;
        }
        ddP.A09 = r1;
        A002.A00.BW6(ddP);
    }

    public C17770zR A16() {
        AnonymousClass1JE r1 = (AnonymousClass1JE) super.A16();
        r1.A05 = null;
        r1.A07 = new C156827Mz();
        return r1;
    }
}
