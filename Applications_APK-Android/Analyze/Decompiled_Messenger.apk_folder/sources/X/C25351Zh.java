package X;

import com.facebook.secure.intentswitchoff.FbReceiverSwitchOffDI;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Zh  reason: invalid class name and case insensitive filesystem */
public final class C25351Zh extends AnonymousClass1ZS {
    private static volatile C25351Zh A00;

    public static final C25351Zh A00(AnonymousClass1XY r6) {
        if (A00 == null) {
            synchronized (C25351Zh.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A00 = new C25351Zh(FbReceiverSwitchOffDI.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AjV, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private C25351Zh(FbReceiverSwitchOffDI fbReceiverSwitchOffDI, AnonymousClass0US r2) {
        super(fbReceiverSwitchOffDI, r2);
    }
}
