package X;

import android.view.View;

/* renamed from: X.0xm  reason: invalid class name and case insensitive filesystem */
public final class C16790xm implements C14360tB {
    public final /* synthetic */ C13450rS A00;

    public C16790xm(C13450rS r1) {
        this.A00 = r1;
    }

    public void BPR() {
        this.A00.A01 = null;
    }

    public void BPT(View view) {
        C13450rS r0 = this.A00;
        r0.A01 = view;
        if (view != null) {
            boolean z = r0.A08;
            int i = 2;
            if (z) {
                i = 0;
            }
            AnonymousClass1IA.A00(view, i);
        }
    }
}
