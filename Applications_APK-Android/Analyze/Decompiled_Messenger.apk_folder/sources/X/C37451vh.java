package X;

import android.content.Context;
import android.view.View;
import com.facebook.widget.text.BetterButton;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1vh  reason: invalid class name and case insensitive filesystem */
public final class C37451vh extends C20831Dz {
    public View.OnClickListener A00;
    public C24440Bzl A01;
    public ImmutableList A02;
    public Integer A03;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (r3 != r0.intValue()) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C37451vh r1, com.facebook.widget.text.BetterButton r2, int r3) {
        /*
            java.lang.Integer r0 = r1.A03
            if (r0 == 0) goto L_0x000b
            int r0 = r0.intValue()
            r1 = 1
            if (r3 == r0) goto L_0x000c
        L_0x000b:
            r1 = 0
        L_0x000c:
            r0 = 2132476476(0x7f1b023c, float:2.0604194E38)
            if (r1 == 0) goto L_0x0014
            r0 = 2132476475(0x7f1b023b, float:2.0604192E38)
        L_0x0014:
            X.C25873Cnn.A03(r2, r0)
            r2.setSelected(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37451vh.A01(X.1vh, com.facebook.widget.text.BetterButton, int):void");
    }

    public int ArU() {
        return this.A02.size();
    }

    public static void A00(C37451vh r4, BetterButton betterButton, int i) {
        int dimensionPixelSize;
        Context context = betterButton.getContext();
        AnonymousClass1R7 r3 = (AnonymousClass1R7) betterButton.getLayoutParams();
        int A012 = C24762CHc.A01(context);
        if (i == r4.ArU() - 1) {
            dimensionPixelSize = A012;
        } else {
            dimensionPixelSize = context.getResources().getDimensionPixelSize(2132148230);
        }
        if (i != 0) {
            A012 = 0;
        }
        r3.setMargins(A012, 0, dimensionPixelSize, 0);
        betterButton.setLayoutParams(r3);
    }
}
