package X;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.0UB  reason: invalid class name */
public abstract class AnonymousClass0UB<E> extends AbstractSet<E> {
    /* renamed from: A00 */
    public C24971Xv iterator() {
        Iterator it;
        Predicate predicate;
        if (this instanceof AnonymousClass0UA) {
            AnonymousClass0UA r2 = (AnonymousClass0UA) this;
            it = r2.A02.iterator();
            predicate = r2.A01;
        } else if (!(this instanceof C07650du)) {
            AnonymousClass0UF r1 = (AnonymousClass0UF) this;
            Iterator it2 = r1.A01.iterator();
            Iterator it3 = r1.A02.iterator();
            Preconditions.checkNotNull(it2);
            Preconditions.checkNotNull(it3);
            return C24931Xr.A04(new AnonymousClass0UK(new AnonymousClass0UJ(it2, it3)));
        } else {
            C07650du r22 = (C07650du) this;
            it = r22.A02.iterator();
            predicate = r22.A01;
        }
        return C24931Xr.A05(it, predicate);
    }

    public final boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }
}
