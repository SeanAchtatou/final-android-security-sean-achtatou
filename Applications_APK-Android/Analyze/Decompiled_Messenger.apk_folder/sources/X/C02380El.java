package X;

import android.content.Context;
import android.os.Build;
import java.io.File;

/* renamed from: X.0El  reason: invalid class name and case insensitive filesystem */
public final class C02380El {
    public static C02380El A03;
    public final Context A00;
    public final C02400En A01;
    private final AnonymousClass0Ep A02;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r2 == X.AnonymousClass07B.A0N) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File A00(java.lang.String r6) {
        /*
            r5 = this;
            r4 = 0
            android.content.Context r0 = r5.A00
            boolean r0 = X.AnonymousClass0GM.A00(r0)
            if (r0 == 0) goto L_0x003f
            java.lang.Integer r2 = X.C02420Er.A01(r6)
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            if (r2 == r0) goto L_0x0016
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            r0 = 0
            if (r2 != r1) goto L_0x0017
        L_0x0016:
            r0 = 1
        L_0x0017:
            if (r0 == 0) goto L_0x003f
            java.lang.String r3 = r5.A02(r6)
            if (r3 != 0) goto L_0x002b
            java.lang.Object[] r2 = new java.lang.Object[]{r6}
            java.lang.String r1 = "VoltronModuleManager"
            java.lang.String r0 = "Hash not found for module %s"
            X.C010708t.A0P(r1, r0, r2)
            return r4
        L_0x002b:
            X.0Ep r2 = r5.A02
            android.content.Context r1 = r5.A00
            boolean r0 = X.AnonymousClass0GS.A01(r6, r1)
            if (r0 == 0) goto L_0x003a
            java.io.File r0 = X.AnonymousClass0GS.A00(r6, r1)
            return r0
        L_0x003a:
            java.io.File r0 = r2.A02(r6, r3)
            return r0
        L_0x003f:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02380El.A00(java.lang.String):java.io.File");
    }

    public synchronized void A03(String str) {
        A04(str, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001e, code lost:
        if (r4 >= 10) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A04(java.lang.String r8, boolean r9) {
        /*
            r7 = this;
            monitor-enter(r7)
            int r4 = X.AnonymousClass0GL.A00(r8)     // Catch:{ all -> 0x011e }
            r0 = -3
            if (r4 != r0) goto L_0x0019
            java.lang.Object[] r3 = new java.lang.Object[]{r8}     // Catch:{ all -> 0x011e }
            java.lang.String r2 = "AppModuleIndexUtil"
            java.lang.String r1 = "Checking for valid module for %s"
            java.util.Locale r0 = java.util.Locale.US     // Catch:{ all -> 0x011e }
            java.lang.String r0 = java.lang.String.format(r0, r1, r3)     // Catch:{ all -> 0x011e }
            X.C010708t.A0K(r2, r0)     // Catch:{ all -> 0x011e }
        L_0x0019:
            if (r4 < 0) goto L_0x0020
            r1 = 10
            r0 = 1
            if (r4 < r1) goto L_0x0021
        L_0x0020:
            r0 = 0
        L_0x0021:
            if (r0 == 0) goto L_0x011c
            r3 = 0
            X.0En r0 = r7.A01     // Catch:{ all -> 0x0115 }
            r0.A01(r8)     // Catch:{ all -> 0x0115 }
            java.io.File r4 = r7.A00(r8)     // Catch:{ all -> 0x0115 }
            android.content.Context r0 = r7.A00     // Catch:{ all -> 0x0115 }
            boolean r0 = X.AnonymousClass0GM.A00(r0)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x0060
            if (r9 == 0) goto L_0x0041
            java.lang.Integer r0 = X.C02420Er.A01(r8)     // Catch:{ all -> 0x0115 }
            boolean r0 = X.AnonymousClass0GN.A01(r0)     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x0060
        L_0x0041:
            if (r4 == 0) goto L_0x0049
            boolean r0 = r4.exists()     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x0060
        L_0x0049:
            android.content.Context r0 = r7.A00     // Catch:{ all -> 0x0115 }
            boolean r0 = X.AnonymousClass0GS.A01(r8, r0)     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x0060
            X.0Q3 r1 = new X.0Q3     // Catch:{ all -> 0x0115 }
            if (r4 == 0) goto L_0x005d
            java.lang.String r0 = r4.getCanonicalPath()     // Catch:{ all -> 0x0115 }
        L_0x0059:
            r1.<init>(r8, r0)     // Catch:{ all -> 0x0115 }
            goto L_0x00b4
        L_0x005d:
            java.lang.String r0 = ""
            goto L_0x0059
        L_0x0060:
            java.util.ArrayDeque r5 = new java.util.ArrayDeque     // Catch:{ all -> 0x0115 }
            r5.<init>()     // Catch:{ all -> 0x0115 }
            r5.addLast(r8)     // Catch:{ all -> 0x0115 }
        L_0x0068:
            int r0 = r5.size()     // Catch:{ all -> 0x0115 }
            if (r0 <= 0) goto L_0x00ce
            java.lang.Object r2 = r5.removeFirst()     // Catch:{ all -> 0x0115 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0115 }
            X.0Eq r6 = X.AnonymousClass0Eq.A00()     // Catch:{ all -> 0x0115 }
            int r1 = X.AnonymousClass0GL.A00(r2)     // Catch:{ all -> 0x0115 }
            r0 = 1
            boolean r0 = X.AnonymousClass0Eq.A02(r6, r1, r0)     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x0068
            java.io.File r6 = r7.A00(r2)     // Catch:{ all -> 0x0115 }
            android.content.Context r0 = r7.A00     // Catch:{ all -> 0x0115 }
            boolean r0 = X.AnonymousClass0GM.A00(r0)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x00b8
            java.lang.Integer r0 = X.C02420Er.A01(r2)     // Catch:{ all -> 0x0115 }
            boolean r0 = X.AnonymousClass0GN.A01(r0)     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x00b8
            if (r6 == 0) goto L_0x00a1
            boolean r0 = r6.exists()     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x00b8
        L_0x00a1:
            android.content.Context r0 = r7.A00     // Catch:{ all -> 0x0115 }
            boolean r0 = X.AnonymousClass0GS.A01(r2, r0)     // Catch:{ all -> 0x0115 }
            if (r0 != 0) goto L_0x00b8
            X.0Q3 r1 = new X.0Q3     // Catch:{ all -> 0x0115 }
            if (r6 == 0) goto L_0x00b5
            java.lang.String r0 = r6.getCanonicalPath()     // Catch:{ all -> 0x0115 }
        L_0x00b1:
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0115 }
        L_0x00b4:
            throw r1     // Catch:{ all -> 0x0115 }
        L_0x00b5:
            java.lang.String r0 = ""
            goto L_0x00b1
        L_0x00b8:
            X.0En r1 = r7.A01     // Catch:{ all -> 0x0115 }
            java.lang.String r0 = r7.A02(r2)     // Catch:{ all -> 0x0115 }
            java.lang.String[] r2 = r1.A04(r2, r0, r6)     // Catch:{ all -> 0x0115 }
            r1 = 0
        L_0x00c3:
            int r0 = r2.length     // Catch:{ all -> 0x0115 }
            if (r1 >= r0) goto L_0x0068
            r0 = r2[r1]     // Catch:{ all -> 0x0115 }
            r5.addLast(r0)     // Catch:{ all -> 0x0115 }
            int r1 = r1 + 1
            goto L_0x00c3
        L_0x00ce:
            X.0Eq r2 = X.AnonymousClass0Eq.A00()     // Catch:{ all -> 0x0115 }
            int r1 = X.AnonymousClass0GL.A00(r8)     // Catch:{ all -> 0x0115 }
            r0 = 1
            boolean r0 = X.AnonymousClass0Eq.A02(r2, r1, r0)     // Catch:{ all -> 0x0115 }
            if (r0 == 0) goto L_0x00df
            r1 = 1
            goto L_0x00ed
        L_0x00df:
            X.0En r2 = r7.A01     // Catch:{ all -> 0x0115 }
            java.lang.String r1 = r7.A02(r8)     // Catch:{ all -> 0x0115 }
            java.io.File r0 = r7.A01(r8)     // Catch:{ all -> 0x0115 }
            r2.A03(r8, r1, r4, r0)     // Catch:{ all -> 0x0115 }
            r1 = 2
        L_0x00ed:
            X.0En r0 = r7.A01     // Catch:{ all -> 0x011e }
            r0.A02(r8, r1)     // Catch:{ all -> 0x011e }
            X.0Eq r2 = X.AnonymousClass0Eq.A00()     // Catch:{ all -> 0x011e }
            int r1 = X.AnonymousClass0GL.A00(r8)     // Catch:{ all -> 0x011e }
            monitor-enter(r2)     // Catch:{ all -> 0x011e }
            boolean r0 = X.AnonymousClass0GL.A02(r1)     // Catch:{ all -> 0x0112 }
            if (r0 == 0) goto L_0x0110
            java.util.BitSet r0 = r2.A01     // Catch:{ all -> 0x0112 }
            r0.set(r1)     // Catch:{ all -> 0x0112 }
            r0 = 3
            boolean r0 = X.C010708t.A0X(r0)     // Catch:{ all -> 0x0112 }
            if (r0 == 0) goto L_0x0110
            X.AnonymousClass0GL.A01(r1)     // Catch:{ all -> 0x0112 }
        L_0x0110:
            monitor-exit(r2)     // Catch:{ all -> 0x011e }
            goto L_0x011c
        L_0x0112:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x011e }
            throw r0     // Catch:{ all -> 0x011e }
        L_0x0115:
            r1 = move-exception
            X.0En r0 = r7.A01     // Catch:{ all -> 0x011e }
            r0.A02(r8, r3)     // Catch:{ all -> 0x011e }
            throw r1     // Catch:{ all -> 0x011e }
        L_0x011c:
            monitor-exit(r7)
            return
        L_0x011e:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02380El.A04(java.lang.String, boolean):void");
    }

    public C02380El(Context context, C02400En r2, AnonymousClass0Ep r3) {
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
    }

    public File A01(String str) {
        String str2;
        String A022 = A02(str);
        AnonymousClass0Ep r1 = this.A02;
        Context context = this.A00;
        File A002 = AnonymousClass0Ep.A00(r1, str, A022);
        boolean contains = context.getPackageName().contains("instagram");
        boolean z = false;
        if (contains) {
            z = true;
        }
        if (z) {
            str2 = "lib-zstd";
        } else {
            str2 = "lib-xzs";
        }
        return new File(A002, AnonymousClass08S.A0P(str2, File.separator, Build.CPU_ABI));
    }

    public String A02(String str) {
        if (!AnonymousClass0GO.A00(str)) {
            return null;
        }
        AnonymousClass0GP.A00(this.A00);
        return AnonymousClass0Eq.A00().A03(str);
    }
}
