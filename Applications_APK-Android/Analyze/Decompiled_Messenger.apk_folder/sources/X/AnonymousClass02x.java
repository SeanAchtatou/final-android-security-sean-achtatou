package X;

import android.util.Log;
import io.card.payment.BuildConfig;
import java.io.PrintWriter;
import java.io.StringWriter;

/* renamed from: X.02x  reason: invalid class name */
public final class AnonymousClass02x implements AnonymousClass02v {
    public static final AnonymousClass02x A01 = new AnonymousClass02x();
    private int A00 = 5;

    public void AWW(String str, String str2) {
        A00(3, str, str2);
    }

    public void AWX(String str, String str2, Throwable th) {
        A01(3, str, str2, th);
    }

    public void AY3(String str, String str2) {
        A00(6, str, str2);
    }

    public void AY4(String str, String str2, Throwable th) {
        A01(6, str, str2, th);
    }

    public void CLT(String str, String str2) {
        A00(2, str, str2);
    }

    public void CLU(String str, String str2, Throwable th) {
        A01(2, str, str2, th);
    }

    public void CMp(String str, String str2) {
        A00(5, str, str2);
    }

    public void CMq(String str, String str2, Throwable th) {
        A01(5, str, str2, th);
    }

    public void CNm(String str, String str2) {
        A00(6, str, str2);
    }

    public void CNn(String str, String str2, Throwable th) {
        A01(6, str, str2, th);
    }

    private void A00(int i, String str, String str2) {
        if ("unknown" != 0) {
            str = AnonymousClass08S.A0P("unknown", ":", str);
        }
        Log.println(i, str, str2);
    }

    private void A01(int i, String str, String str2, Throwable th) {
        String stringWriter;
        if ("unknown" != 0) {
            str = AnonymousClass08S.A0P("unknown", ":", str);
        }
        if (th == null) {
            stringWriter = BuildConfig.FLAVOR;
        } else {
            StringWriter stringWriter2 = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter2));
            stringWriter = stringWriter2.toString();
        }
        Log.println(i, str, AnonymousClass08S.A07(str2, 10, stringWriter));
    }

    public boolean BFj(int i) {
        if (this.A00 <= i) {
            return true;
        }
        return false;
    }

    private AnonymousClass02x() {
    }

    public int Aum() {
        return this.A00;
    }

    public void C9Z(int i) {
        this.A00 = i;
    }

    public void BIm(int i, String str, String str2) {
        A00(i, str, str2);
    }
}
