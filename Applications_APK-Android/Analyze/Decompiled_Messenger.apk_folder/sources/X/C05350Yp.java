package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

/* renamed from: X.0Yp  reason: invalid class name and case insensitive filesystem */
public final class C05350Yp extends C05360Yq {
    public static final AnonymousClass0YC A00 = new AnonymousClass0ZY();

    public static ListenableFuture A01(Iterable iterable) {
        return new C52112iW(ImmutableList.copyOf(iterable), true);
    }

    public static ListenableFuture A02(Iterable iterable) {
        return new C52112iW(ImmutableList.copyOf(iterable), false);
    }

    public static ListenableFuture A03(Object obj) {
        if (obj == null) {
            return C14010sS.A01;
        }
        return new C14010sS(obj);
    }

    public static ListenableFuture A05(ListenableFuture... listenableFutureArr) {
        return new C52112iW(ImmutableList.copyOf(listenableFutureArr), true);
    }

    public static void A07(ListenableFuture listenableFuture, C04810Wg r2) {
        A08(listenableFuture, r2, C25141Ym.INSTANCE);
    }

    public static ListenableFuture A00(ListenableFuture listenableFuture) {
        if (listenableFuture.isDone()) {
            return listenableFuture;
        }
        return new C25631a9(listenableFuture);
    }

    public static ListenableFuture A04(Throwable th) {
        Preconditions.checkNotNull(th);
        return new C1928391p(th);
    }

    public static Object A06(Future future) {
        Preconditions.checkState(future.isDone(), "Future was expected to be done: %s", future);
        return C07360dI.A00(future);
    }

    public static void A08(ListenableFuture listenableFuture, C04810Wg r2, Executor executor) {
        Preconditions.checkNotNull(r2);
        listenableFuture.addListener(new C25151Yn(listenableFuture, r2), executor);
    }
}
