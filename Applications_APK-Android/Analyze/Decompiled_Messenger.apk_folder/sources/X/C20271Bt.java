package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Bt  reason: invalid class name and case insensitive filesystem */
public final class C20271Bt extends C16070wR {
    @Comparable(type = 3)
    public long A00 = -1;
    @Comparable(type = 13)
    public C23610BiW A01;
    @Comparable(type = 13)
    public C09290gx A02;
    public AnonymousClass0UN A03;
    public AnonymousClass10N A04;
    @Comparable(type = 13)
    public AnonymousClass653 A05;
    @Comparable(type = 14)
    public AnonymousClass6RZ A06;
    public AnonymousClass7NE A07;
    public C04310Tq A08;
    public C04310Tq A09;

    public C20271Bt(Context context) {
        super("GraphQLRootQuerySection");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A03 = new AnonymousClass0UN(2, r2);
        this.A08 = AnonymousClass0VG.A00(AnonymousClass1Y3.BLy, r2);
        this.A09 = AnonymousClass0VG.A00(AnonymousClass1Y3.BKH, r2);
        this.A06 = new AnonymousClass6RZ();
    }

    public C16070wR A0T(boolean z) {
        C20271Bt r1 = (C20271Bt) super.A0T(z);
        if (!z) {
            r1.A06 = new AnonymousClass6RZ();
        }
        return r1;
    }
}
