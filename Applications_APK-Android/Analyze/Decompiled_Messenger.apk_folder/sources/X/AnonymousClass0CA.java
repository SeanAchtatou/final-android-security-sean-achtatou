package X;

import android.text.TextUtils;

/* renamed from: X.0CA  reason: invalid class name */
public final class AnonymousClass0CA {
    public final /* synthetic */ AnonymousClass0C8 A00;

    public AnonymousClass0CA(AnonymousClass0C8 r1) {
        this.A00 = r1;
    }

    public void A00(int i) {
        if (i >= 0) {
            ((C01880Bz) this.A00.A08.A07(C01880Bz.class)).A03((long) i, this.A00.A0b, "m", "r", "b");
        }
    }

    public void A01(int i) {
        if (i >= 0) {
            ((C01880Bz) this.A00.A08.A07(C01880Bz.class)).A03((long) i, this.A00.A0b, "m", "s", "b");
        }
    }

    public void A02(String str, int i) {
        ((C01880Bz) this.A00.A08.A07(C01880Bz.class)).A03(1, this.A00.A0b, "m", "s", "c");
        AnonymousClass0RC.A02.A00(i, true);
        AnonymousClass0CC r0 = this.A00.A0Y;
        if (r0 != null) {
            r0.A02.A0I.C2f(str, (long) i, true);
        }
    }

    public void A03(String str, String str2, int i) {
        ((C01880Bz) this.A00.A08.A07(C01880Bz.class)).A03(1, this.A00.A0b, "m", "r", "c");
        AnonymousClass0RC.A02.A00(i, false);
        AnonymousClass0C8 r0 = this.A00;
        r0.A08.A08(str, str2, r0.A0b, true);
        this.A00.A05.A00();
        AnonymousClass0CC r4 = this.A00.A0Y;
        if (r4 != null) {
            if (!TextUtils.isEmpty(str2)) {
                str = str2;
            }
            r4.A02.A0I.C2f(str, (long) i, false);
        }
    }
}
