package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.AbstractMapBasedMultimap;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

/* renamed from: X.11B  reason: invalid class name */
public class AnonymousClass11B extends AbstractCollection<V> {
    public Collection A00;
    public final Collection A01;
    public final AnonymousClass11B A02;
    public final Object A03;
    public final /* synthetic */ AbstractMapBasedMultimap A04;

    public AnonymousClass11B(AbstractMapBasedMultimap abstractMapBasedMultimap, Object obj, Collection collection, AnonymousClass11B r5) {
        Collection collection2;
        this.A04 = abstractMapBasedMultimap;
        this.A03 = obj;
        this.A00 = collection;
        this.A02 = r5;
        if (r5 == null) {
            collection2 = null;
        } else {
            collection2 = r5.A00;
        }
        this.A01 = collection2;
    }

    public void A01() {
        AnonymousClass11B r0 = this.A02;
        if (r0 != null) {
            r0.A01();
        } else {
            this.A04.A01.put(this.A03, this.A00);
        }
    }

    public void A02() {
        Collection collection;
        AnonymousClass11B r0 = this.A02;
        if (r0 != null) {
            r0.A02();
            if (this.A02.A00 != this.A01) {
                throw new ConcurrentModificationException();
            }
        } else if (this.A00.isEmpty() && (collection = (Collection) this.A04.A01.get(this.A03)) != null) {
            this.A00 = collection;
        }
    }

    public void A03() {
        AnonymousClass11B r0 = this.A02;
        if (r0 != null) {
            r0.A03();
        } else if (this.A00.isEmpty()) {
            this.A04.A01.remove(this.A03);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        A02();
        return this.A00.equals(obj);
    }

    public boolean add(Object obj) {
        A02();
        boolean isEmpty = this.A00.isEmpty();
        boolean add = this.A00.add(obj);
        if (add) {
            this.A04.A00++;
            if (isEmpty) {
                A01();
            }
        }
        return add;
    }

    public boolean addAll(Collection collection) {
        if (collection.isEmpty()) {
            return false;
        }
        int size = size();
        boolean addAll = this.A00.addAll(collection);
        if (addAll) {
            int size2 = this.A00.size();
            this.A04.A00 += size2 - size;
            if (size == 0) {
                A01();
            }
        }
        return addAll;
    }

    public void clear() {
        int size = size();
        if (size != 0) {
            this.A00.clear();
            this.A04.A00 -= size;
            A03();
        }
    }

    public boolean contains(Object obj) {
        A02();
        return this.A00.contains(obj);
    }

    public boolean containsAll(Collection collection) {
        A02();
        return this.A00.containsAll(collection);
    }

    public int hashCode() {
        A02();
        return this.A00.hashCode();
    }

    public Iterator iterator() {
        A02();
        return new AnonymousClass11E(this);
    }

    public boolean remove(Object obj) {
        A02();
        boolean remove = this.A00.remove(obj);
        if (remove) {
            AbstractMapBasedMultimap abstractMapBasedMultimap = this.A04;
            abstractMapBasedMultimap.A00--;
            A03();
        }
        return remove;
    }

    public boolean removeAll(Collection collection) {
        if (collection.isEmpty()) {
            return false;
        }
        int size = size();
        boolean removeAll = this.A00.removeAll(collection);
        if (removeAll) {
            int size2 = this.A00.size();
            this.A04.A00 += size2 - size;
            A03();
        }
        return removeAll;
    }

    public boolean retainAll(Collection collection) {
        Preconditions.checkNotNull(collection);
        int size = size();
        boolean retainAll = this.A00.retainAll(collection);
        if (retainAll) {
            int size2 = this.A00.size();
            this.A04.A00 += size2 - size;
            A03();
        }
        return retainAll;
    }

    public int size() {
        A02();
        return this.A00.size();
    }

    public String toString() {
        A02();
        return this.A00.toString();
    }
}
