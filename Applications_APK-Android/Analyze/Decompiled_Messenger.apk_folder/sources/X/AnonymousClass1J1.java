package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.1J1  reason: invalid class name */
public final class AnonymousClass1J1 extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A01;
    @Comparable(type = 13)
    public ThreadSummary A02;

    public AnonymousClass1J1(Context context) {
        super("M4ThreadItemAdsLoggingWrapperComponent");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    public C17770zR A16() {
        C17770zR r0;
        AnonymousClass1J1 r1 = (AnonymousClass1J1) super.A16();
        C17770zR r02 = r1.A01;
        if (r02 != null) {
            r0 = r02.A16();
        } else {
            r0 = null;
        }
        r1.A01 = r0;
        return r1;
    }
}
