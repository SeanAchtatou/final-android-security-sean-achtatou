package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.1Pb  reason: invalid class name and case insensitive filesystem */
public final class C23321Pb {
    private int A00 = 0;
    public final AnonymousClass1PY A01;
    public final LinkedHashMap A02 = new LinkedHashMap();

    public synchronized int A00() {
        return this.A02.size();
    }

    public synchronized int A01() {
        return this.A00;
    }

    public synchronized Object A02(Object obj) {
        Object remove;
        int i;
        remove = this.A02.remove(obj);
        int i2 = this.A00;
        if (remove == null) {
            i = 0;
        } else {
            i = this.A01.B3B(remove);
        }
        this.A00 = i2 - i;
        return remove;
    }

    public synchronized ArrayList A03(AnonymousClass8SZ r6) {
        ArrayList arrayList;
        int i;
        arrayList = new ArrayList();
        Iterator it = this.A02.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (r6 == null || r6.apply(entry.getKey())) {
                arrayList.add(entry.getValue());
                int i2 = this.A00;
                Object value = entry.getValue();
                if (value == null) {
                    i = 0;
                } else {
                    i = this.A01.B3B(value);
                }
                this.A00 = i2 - i;
                it.remove();
            }
        }
        return arrayList;
    }

    public synchronized void A04(Object obj, Object obj2) {
        int i;
        int i2;
        Object remove = this.A02.remove(obj);
        int i3 = this.A00;
        if (remove == null) {
            i = 0;
        } else {
            i = this.A01.B3B(remove);
        }
        this.A00 = i3 - i;
        this.A02.put(obj, obj2);
        int i4 = this.A00;
        if (obj2 == null) {
            i2 = 0;
        } else {
            i2 = this.A01.B3B(obj2);
        }
        this.A00 = i4 + i2;
    }

    public C23321Pb(AnonymousClass1PY r2) {
        this.A01 = r2;
    }
}
