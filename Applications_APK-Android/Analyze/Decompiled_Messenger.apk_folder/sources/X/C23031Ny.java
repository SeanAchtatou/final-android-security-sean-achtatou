package X;

/* renamed from: X.1Ny  reason: invalid class name and case insensitive filesystem */
public interface C23031Ny {
    boolean doesRenderSupportScaling();

    int getDuration();

    DJD getFrame(int i);

    int getFrameCount();

    int[] getFrameDurations();

    C80983tZ getFrameInfo(int i);

    int getHeight();

    int getLoopCount();

    int getSizeInBytes();

    int getWidth();
}
