package X;

import java.lang.ref.ReferenceQueue;

/* renamed from: X.0e6  reason: invalid class name and case insensitive filesystem */
public final class C07750e6 extends C07760e7 implements C07740e5 {
    public volatile Object A00 = null;

    public Object getValue() {
        return this.A00;
    }

    public C07750e6(ReferenceQueue referenceQueue, Object obj, int i, C07750e6 r5) {
        super(referenceQueue, obj, i, r5);
    }
}
