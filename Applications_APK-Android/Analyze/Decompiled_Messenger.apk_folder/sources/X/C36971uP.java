package X;

import com.facebook.user.model.UserKey;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/* renamed from: X.1uP  reason: invalid class name and case insensitive filesystem */
public final class C36971uP implements Callable {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C36971uP(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    public Object call() {
        int i;
        AnonymousClass0r6 r5 = this.A00;
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, r5.A02)).AOx();
        try {
            C005505z.A03("PresenceManager:sendAdditionalContactsInternal", -859931531);
            if (r5.A0G.A02() != 0 || r5.A0F.A02() != 0 || r5.A0E.A02() != 0 || r5.A0H.A02() != 0) {
                r5.A0W = false;
            } else if (r5.A0W) {
                i = -607906792;
                C005505z.A00(i);
                return null;
            } else {
                r5.A0W = true;
            }
            ArrayList arrayList = new ArrayList();
            synchronized (r5.A0D) {
                C05180Xy r8 = new C05180Xy();
                r5.A0D.clear();
                UserKey userKey = (UserKey) r5.A0O.get();
                for (UserKey userKey2 : r5.A0G.A06().keySet()) {
                    if (AnonymousClass0r6.A0K(userKey2, r8, userKey)) {
                        r5.A0D.add(userKey2);
                        r8.add(userKey2.id);
                        arrayList.add(Long.valueOf(Long.parseLong(userKey2.id)));
                    }
                }
                for (UserKey userKey3 : r5.A0F.A06().keySet()) {
                    if (AnonymousClass0r6.A0K(userKey3, r8, userKey)) {
                        r5.A0D.add(userKey3);
                        r8.add(userKey3.id);
                        arrayList.add(Long.valueOf(Long.parseLong(userKey3.id)));
                    }
                }
                for (UserKey userKey4 : r5.A0E.A06().keySet()) {
                    if (AnonymousClass0r6.A0H(r5, userKey4, r8, userKey)) {
                        r5.A0D.add(userKey4);
                        r8.add(userKey4.id);
                        arrayList.add(Long.valueOf(Long.parseLong(userKey4.id)));
                    }
                }
                for (UserKey userKey5 : r5.A0H.A06().keySet()) {
                    if (AnonymousClass0r6.A0H(r5, userKey5, r8, userKey)) {
                        r5.A0D.add(userKey5);
                        r8.add(userKey5.id);
                        arrayList.add(Long.valueOf(Long.parseLong(userKey5.id)));
                    }
                }
            }
            byte[] A002 = new C36281so(new C36291sp()).A00(new C24531Tz(arrayList));
            int length = A002.length;
            byte[] bArr = new byte[(length + 1)];
            System.arraycopy(A002, 0, bArr, 1, length);
            ((C191717b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Agr, r5.A02)).A02("/t_sac", bArr, AnonymousClass07B.A00, null);
            C005505z.A00(1546826478);
            return null;
        } catch (C70863bP e) {
            C010708t.A08(AnonymousClass0r6.A0X, "/t_sac serialization error", e);
            i = 907950023;
        } catch (Throwable th) {
            C005505z.A00(2114280601);
            throw th;
        }
    }
}
