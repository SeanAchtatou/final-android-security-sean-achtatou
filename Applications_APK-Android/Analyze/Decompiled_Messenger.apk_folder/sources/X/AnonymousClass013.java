package X;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

/* renamed from: X.013  reason: invalid class name */
public class AnonymousClass013 {
    private static final String[] A00 = {"crash_log", "insta_crash_log", "crash_lock", "remedy_log", "app_was_disabled", "ACRA-INSTALLATION", "is_employee", "app_state_logs"};

    public static C03670Oz A01(AnonymousClass013 r6, Context context, int i, int i2) {
        boolean z = true;
        if (i > i2) {
            if (C010408q.A00(context)) {
                try {
                    r6.A06(context, i, r6.A05());
                } catch (Throwable th) {
                    Log.w("CrashLoopRemedy", "non-fatal error showing notification", th);
                }
            }
            AnonymousClass014.A0B(context);
            if (i == 1) {
                File cacheDir = context.getCacheDir();
                if (cacheDir != null) {
                    A03(cacheDir, new String[0]);
                }
            } else if (i == 2) {
                r6.A07(context, new String[0]);
            } else if (i != 3) {
                Log.w("CrashLoopRemedy", AnonymousClass08S.A09("unknown remedy level ", i));
            } else {
                File file = new File(context.getApplicationInfo().dataDir, "app_was_disabled");
                try {
                    file.createNewFile();
                    if (file.exists()) {
                        try {
                            C03650Ox.A01(new C03650Ox(context), 2);
                        } catch (PackageManager.NameNotFoundException e) {
                            throw new RuntimeException(e);
                        } catch (RuntimeException e2) {
                            Log.e("CrashLoopRemedy", "unable to disable app entry points", e2);
                        }
                        return new C03670Oz(true, z);
                    }
                    throw new RuntimeException("could not disable crash loop: could not create signal file");
                } catch (IOException e3) {
                    throw new RuntimeException(e3);
                }
            }
            z = false;
            return new C03670Oz(true, z);
        }
        return new C03670Oz(false, false);
    }

    public String A05() {
        return "Default Crash Loop Remedy";
    }

    public static void A02(Notification.Builder builder, String str) {
        builder.setStyle(new Notification.BigTextStyle().bigText(str));
    }

    public void A06(Context context, int i, String str) {
        String str2;
        Locale locale = Locale.US;
        if (i == 1) {
            str2 = "cleared caches";
        } else if (i == 2) {
            str2 = "cleared data and logged out";
        } else if (i != 3) {
            str2 = String.format(locale, "??? %s", Integer.valueOf(i));
        } else {
            str2 = "disabled auto-start";
        }
        if (str == null) {
            str = "default";
        }
        String format = String.format(locale, "[employee only] %s using class %s in process %s.", str2, str, AnonymousClass00M.A00().A01);
        Notification.Builder smallIcon = new Notification.Builder(context).setWhen(System.currentTimeMillis()).setContentTitle("[fb] crash mitigation applied").setContentText(format).setSmallIcon(17301533);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 17) {
            smallIcon.setShowWhen(true);
        }
        if (i2 >= 16) {
            A02(smallIcon, format);
        }
        ((NotificationManager) context.getSystemService("notification")).notify(1, smallIcon.getNotification());
    }

    private static void A03(File file, String[] strArr) {
        String name = file.getName();
        int i = 0;
        while (i < strArr.length) {
            if (!name.matches(strArr[i])) {
                i++;
            } else {
                return;
            }
        }
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File A03 : listFiles) {
                    A03(A03, strArr);
                }
            } else {
                return;
            }
        }
        file.delete();
    }

    public C03670Oz A04(Context context, int i, int i2) {
        return A01(this, context, i, i2);
    }

    public void A07(Context context, String[] strArr) {
        File file = new File(context.getApplicationInfo().dataDir);
        String[] A08 = A08();
        int length = A08.length;
        int length2 = strArr.length;
        String[] strArr2 = (String[]) Arrays.copyOf(A08, length + length2);
        System.arraycopy(strArr, 0, strArr2, length, length2);
        A03(file, strArr2);
    }

    public String[] A08() {
        return A00;
    }
}
