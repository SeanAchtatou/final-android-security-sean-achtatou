package X;

import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1cg  reason: invalid class name and case insensitive filesystem */
public final class C27121cg extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static final Object A01 = new Object();

    public static final Boolean A01(AnonymousClass1XY r4) {
        Boolean bool;
        synchronized (A01) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r4)) {
                    C05540Zi r2 = A00;
                    boolean z = false;
                    if (AnonymousClass0XN.A00((AnonymousClass1XY) A00.A01()).A07() != null) {
                        z = true;
                    }
                    r2.A00 = Boolean.valueOf(z);
                }
                C05540Zi r1 = A00;
                bool = (Boolean) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return bool;
    }

    public static final ViewerContext A00(AnonymousClass1XY r0) {
        return AnonymousClass0XN.A00(r0).A07();
    }
}
