package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.shapes.OvalShape;

/* renamed from: X.0uo  reason: invalid class name and case insensitive filesystem */
public final class C15150uo extends OvalShape {
    private Paint A00 = new Paint();
    public final /* synthetic */ C15140un A01;

    public C15150uo(C15140un r2, int i) {
        this.A01 = r2;
        r2.A00 = i;
        A00((int) rect().width());
    }

    private void A00(int i) {
        float f = (float) (i / 2);
        this.A00.setShader(new RadialGradient(f, f, (float) this.A01.A00, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP));
    }

    public void draw(Canvas canvas, Paint paint) {
        int width = this.A01.getWidth() >> 1;
        float f = (float) width;
        float height = (float) (this.A01.getHeight() >> 1);
        canvas.drawCircle(f, height, f, this.A00);
        canvas.drawCircle(f, height, (float) (width - this.A01.A00), paint);
    }

    public void onResize(float f, float f2) {
        super.onResize(f, f2);
        A00((int) f);
    }
}
