package X;

import android.content.Context;
import java.io.File;
import java.util.ArrayList;

/* renamed from: X.1ie  reason: invalid class name and case insensitive filesystem */
public final class C30791ie implements AnonymousClass1PF {
    public AnonymousClass0UN A00;
    public C30921ir A01;
    public C30921ir A02;

    private C30921ir A00(String str, File file, C23251Ou r14) {
        long j = r14.A01;
        long j2 = r14.A02;
        long j3 = r14.A03;
        AnonymousClass1OL r9 = r14.A05;
        C30931is r7 = (C30931is) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auo, this.A00);
        C30941it r8 = new C30941it();
        r8.A03 = str;
        C30951iu r10 = new C30951iu();
        r10.A00 = j;
        r10.A01 = j2;
        r10.A02 = j3;
        r8.A00 = r10.A00();
        r8.A01 = new AnonymousClass0ZZ(((long) 60) * 86400);
        AnonymousClass1KG r1 = new AnonymousClass1KG(r9);
        if (r8.A04 == null) {
            r8.A04 = new ArrayList();
        }
        r8.A04.add(r1);
        return new C30921ir(r7.A01(file, r8.A00()), r14.A05);
    }

    public static final C30791ie A01(AnonymousClass1XY r1) {
        return new C30791ie(r1);
    }

    public C23361Pf Ab7(C23251Ou r8) {
        String str;
        File file;
        File file2 = (File) r8.A08.get();
        boolean equals = file2.equals(((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A00)).getFilesDir());
        if (((C001300x) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B3a, this.A00)).A02 != C001500z.A03) {
            file = new File(new File(file2, AnonymousClass08S.A0J(r8.A09, ".stash")), String.valueOf(r8.A00));
        } else {
            if (equals) {
                str = AnonymousClass08S.A0J(((File) r8.A08.get()).getName(), r8.A09);
            } else {
                str = r8.A09;
            }
            file = new File(C35651rZ.A00((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6S, this.A00), !equals, str, String.valueOf(r8.A00), "sessionless"));
        }
        if (equals) {
            if (this.A02 == null) {
                this.A02 = A00("fresco_small", file, r8);
            }
            return this.A02;
        }
        if (this.A01 == null) {
            this.A01 = A00("fresco", file, r8);
        }
        return this.A01;
    }

    public C30791ie(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
