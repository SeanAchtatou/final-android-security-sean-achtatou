package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.base.Platform;
import io.card.payment.BuildConfig;
import java.util.Arrays;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ca  reason: invalid class name and case insensitive filesystem */
public final class C27061ca {
    private static volatile C27061ca A01;
    public final FbSharedPreferences A00;

    public static final C27061ca A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C27061ca.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C27061ca(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C27061ca(AnonymousClass1XY r2) {
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }

    public boolean A01(String str) {
        if (!Platform.stringIsNullOrEmpty(str)) {
            FbSharedPreferences fbSharedPreferences = this.A00;
            AnonymousClass1Y7 r0 = C10990lD.A0W;
            String str2 = BuildConfig.FLAVOR;
            String[] split = fbSharedPreferences.B4F(r0, str2).split(" ");
            StringBuilder sb = new StringBuilder();
            for (String str3 : split) {
                if (!str3.equalsIgnoreCase(str) && !Platform.stringIsNullOrEmpty(str3)) {
                    sb.append(str2);
                    sb.append(str3);
                    str2 = " ";
                }
            }
            C30281hn edit = this.A00.edit();
            edit.BzC(C10990lD.A0W, sb.toString());
            edit.commit();
            if (r3 != 1 || !split[0].equalsIgnoreCase(str)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean A02(String str) {
        if (Platform.stringIsNullOrEmpty(str) || !Arrays.asList(this.A00.B4F(C10990lD.A0W, BuildConfig.FLAVOR).split(" ")).contains(str)) {
            return false;
        }
        return true;
    }
}
