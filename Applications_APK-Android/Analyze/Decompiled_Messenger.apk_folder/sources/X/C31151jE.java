package X;

/* renamed from: X.1jE  reason: invalid class name and case insensitive filesystem */
public final class C31151jE implements C22441Lj {
    private C12240os A00;
    public final /* synthetic */ C12240os A01;

    public C31151jE(C12240os r1) {
        this.A01 = r1;
    }

    public void CMg(String str, double d) {
        this.A00.A0J(str, Double.valueOf(d));
    }

    public void CMh(String str, int i) {
        this.A00.A0J(str, Integer.valueOf(i));
    }

    public void CMi(String str, long j) {
        this.A00.A0J(str, Long.valueOf(j));
    }

    public void CMj(String str, String str2) {
        this.A00.A0K(str, str2);
    }

    public void CMk(String str, boolean z) {
        C12240os.A01(this.A00, str, Boolean.valueOf(z));
    }

    public void CMl(String str, int[] iArr) {
        C16910xz A0E = this.A00.A0E(str);
        for (int valueOf : iArr) {
            C16910xz.A00(A0E, Integer.valueOf(valueOf));
        }
    }

    public void CMm(String str, String[] strArr) {
        C16910xz A0E = this.A00.A0E(str);
        for (String A002 : strArr) {
            C16910xz.A00(A0E, A002);
        }
    }

    public void CMo(String str) {
        this.A00 = this.A01.A0F(str);
    }
}
