package X;

import android.content.res.Resources;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import com.google.common.base.Preconditions;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.0A4  reason: invalid class name */
public final class AnonymousClass0A4 {
    private final Resources A00;
    private final SpannableStringBuilder A01;
    private final ArrayDeque A02;

    public SpannableString A00() {
        return new SpannableString(this.A01);
    }

    public void A01() {
        Preconditions.checkState(!this.A02.isEmpty());
        AnonymousClass0A5 r0 = (AnonymousClass0A5) this.A02.removeFirst();
        SpannableStringBuilder spannableStringBuilder = this.A01;
        spannableStringBuilder.setSpan(r0.A02, r0.A01, spannableStringBuilder.length(), r0.A00);
    }

    public void A02(int i) {
        this.A01.append((CharSequence) this.A00.getString(i));
    }

    public void A03(CharSequence charSequence) {
        this.A01.append(charSequence);
    }

    public void A04(Object obj, int i) {
        this.A02.addFirst(new AnonymousClass0A5(this.A01.length(), obj, i));
    }

    public void A05(String str, CharSequence charSequence) {
        A06(str, charSequence, 0, Collections.EMPTY_LIST);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, int, java.lang.CharSequence]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.replace(int, int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    public void A06(String str, CharSequence charSequence, int i, Object... objArr) {
        Preconditions.checkState(this.A02.isEmpty());
        Matcher matcher = Pattern.compile(Pattern.quote(str)).matcher(this.A01);
        if (matcher.find()) {
            int start = matcher.start();
            this.A01.replace(start, matcher.end(), charSequence);
            for (Object span : objArr) {
                this.A01.setSpan(span, start, charSequence.length() + start, i);
            }
        }
    }

    public void A07(String str, String str2, Object obj, int i) {
        A06(str, str2, i, obj);
    }

    public AnonymousClass0A4(Resources resources) {
        this(new SpannableStringBuilder(), resources);
    }

    public AnonymousClass0A4(SpannableStringBuilder spannableStringBuilder, Resources resources) {
        this.A02 = new ArrayDeque();
        this.A01 = spannableStringBuilder;
        this.A00 = resources;
    }
}
