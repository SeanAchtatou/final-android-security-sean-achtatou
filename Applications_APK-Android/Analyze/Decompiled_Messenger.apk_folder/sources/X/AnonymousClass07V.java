package X;

/* renamed from: X.07V  reason: invalid class name */
public final class AnonymousClass07V {
    public static String A00(Class cls) {
        try {
            return (String) cls.getDeclaredField("__redex_internal_original_name").get(cls);
        } catch (NoSuchFieldException unused) {
            return cls.getName();
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public static String A01(Class cls) {
        String A00 = A00(cls);
        int lastIndexOf = A00.lastIndexOf(46);
        if (lastIndexOf == -1) {
            return A00;
        }
        if (lastIndexOf != A00.length()) {
            try {
                return A00.substring(lastIndexOf + 1);
            } catch (Exception e) {
                throw new Error(e);
            }
        } else {
            throw new Error(AnonymousClass08S.A0P("Unexpected string ", A00, " in __redex_internal_original_name"));
        }
    }

    public static String A02(Object obj) {
        return A01(obj.getClass());
    }
}
