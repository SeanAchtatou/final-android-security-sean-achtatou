package X;

/* renamed from: X.1wz  reason: invalid class name and case insensitive filesystem */
public final class C38161wz implements AFE {
    private final long A00;
    private final C21393ACl A01;

    public boolean BGj() {
        return false;
    }

    public C21393ACl B2I(long j) {
        return this.A01;
    }

    public C38161wz(long j, long j2) {
        C21392ACk aCk;
        this.A00 = j;
        if (j2 == 0) {
            aCk = C21392ACk.A02;
        } else {
            aCk = new C21392ACk(0, j2);
        }
        this.A01 = new C21393ACl(aCk, aCk);
    }

    public long Akt() {
        return this.A00;
    }
}
