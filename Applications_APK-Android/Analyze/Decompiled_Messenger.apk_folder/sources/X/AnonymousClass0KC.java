package X;

import android.os.Handler;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0KC  reason: invalid class name */
public class AnonymousClass0KC extends C01910Cc implements Runnable, C01940Cf {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.common.executors.ListenableScheduledFutureImpl";
    public final AnonymousClass0CY A00;

    public void addListener(Runnable runnable, Executor executor) {
        this.A00.addListener(runnable, executor);
    }

    public int compareTo(Object obj) {
        throw new UnsupportedOperationException();
    }

    public long getDelay(TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void run() {
        this.A00.run();
    }

    public /* bridge */ /* synthetic */ Object A00() {
        return this.A00;
    }

    public /* bridge */ /* synthetic */ Future A01() {
        return this.A00;
    }

    public AnonymousClass0KC(Handler handler, Runnable runnable, Object obj) {
        super(handler);
        this.A00 = new AnonymousClass0CY(runnable, obj);
    }

    public AnonymousClass0KC(Handler handler, Callable callable) {
        super(handler);
        this.A00 = new AnonymousClass0CY(callable);
    }
}
