package X;

import android.content.Intent;

/* renamed from: X.0E6  reason: invalid class name */
public final class AnonymousClass0E6 implements AnonymousClass0E7 {
    public final int A00;
    public final Intent A01;
    public final /* synthetic */ AnonymousClass0E1 A02;

    public AnonymousClass0E6(AnonymousClass0E1 r1, Intent intent, int i) {
        this.A02 = r1;
        this.A01 = intent;
        this.A00 = i;
    }

    public void ATZ() {
        this.A02.stopSelf(this.A00);
    }

    public Intent getIntent() {
        return this.A01;
    }
}
