package X;

import androidx.fragment.app.Fragment;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1dz  reason: invalid class name and case insensitive filesystem */
public final class C27931dz extends C27941e0 {
    public static final C27961e2 A05 = new C27951e1();
    public boolean A00 = false;
    public final HashMap A01 = new HashMap();
    public final HashMap A02 = new HashMap();
    public final HashMap A03 = new HashMap();
    public final boolean A04;

    public void A02() {
        C13060qW.A0I(3);
        this.A00 = true;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C27931dz r5 = (C27931dz) obj;
            if (!this.A02.equals(r5.A02) || !this.A01.equals(r5.A01) || !this.A03.equals(r5.A03)) {
                return false;
            }
        }
        return true;
    }

    public void A03(C82933wm r6) {
        this.A02.clear();
        this.A01.clear();
        this.A03.clear();
        if (r6 != null) {
            Collection<Fragment> collection = r6.A00;
            if (collection != null) {
                for (Fragment fragment : collection) {
                    if (fragment != null) {
                        this.A02.put(fragment.A0V, fragment);
                    }
                }
            }
            Map map = r6.A01;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    C27931dz r2 = new C27931dz(this.A04);
                    r2.A03((C82933wm) entry.getValue());
                    this.A01.put(entry.getKey(), r2);
                }
            }
            Map map2 = r6.A02;
            if (map2 != null) {
                this.A03.putAll(map2);
            }
        }
    }

    public boolean A04(Fragment fragment) {
        if (!this.A02.containsKey(fragment.A0V)) {
            return true;
        }
        if (this.A04) {
            return this.A00;
        }
        return !false;
    }

    public int hashCode() {
        return (((this.A02.hashCode() * 31) + this.A01.hashCode()) * 31) + this.A03.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator it = this.A02.values().iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator it2 = this.A01.keySet().iterator();
        while (it2.hasNext()) {
            sb.append((String) it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator it3 = this.A03.keySet().iterator();
        while (it3.hasNext()) {
            sb.append((String) it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public C27931dz(boolean z) {
        this.A04 = z;
    }
}
