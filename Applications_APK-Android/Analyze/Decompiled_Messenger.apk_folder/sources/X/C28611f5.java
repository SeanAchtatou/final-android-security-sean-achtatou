package X;

/* renamed from: X.1f5  reason: invalid class name and case insensitive filesystem */
public final class C28611f5 {
    public static volatile C28671fB A01;
    public AnonymousClass0UN A00;

    public static final C28611f5 A00(AnonymousClass1XY r1) {
        return new C28611f5(r1);
    }

    public C28671fB A01() {
        if (A01 == null) {
            synchronized (this) {
                if (A01 == null) {
                    C28621f6 r2 = new C28621f6();
                    r2.A01 = new C04870Wq(this);
                    r2.A09 = new C28631f7(this);
                    r2.A02 = new C28641f8(this);
                    r2.A07 = new C28651f9(this);
                    r2.A04 = new AnonymousClass0YG(this);
                    r2.A05 = new C05080Xo(this);
                    r2.A0A = new C07100cd(this);
                    r2.A00 = new C07110ce(this);
                    r2.A03 = new C07120cf(this);
                    r2.A06 = new AnonymousClass0YH(this);
                    r2.A08 = new C28661fA(this);
                    A01 = new C28671fB(r2);
                    C28671fB.A0L = A01;
                }
            }
        }
        return A01;
    }

    private C28611f5(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(16, r3);
    }
}
