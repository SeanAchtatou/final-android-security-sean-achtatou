package X;

/* renamed from: X.0KF  reason: invalid class name */
public final class AnonymousClass0KF implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.common.analytics.defaultlogger.DefaultAnalyticsLogger$AnalyticsBackgroundWorker";
    public final /* synthetic */ AnonymousClass0QV A00;

    public AnonymousClass0KF(AnonymousClass0QV r1) {
        this.A00 = r1;
    }

    public void run() {
        this.A00.A0A.set(false);
        while (!this.A00.A09.isEmpty()) {
            ((Runnable) this.A00.A09.remove()).run();
        }
    }
}
