package X;

import android.content.Context;
import android.provider.Settings;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0ca  reason: invalid class name and case insensitive filesystem */
public final class C07070ca {
    public static void A01(C17760zQ r3, String str) {
        if (r3 instanceof C17680zI) {
            ((C17680zI) r3).A00 = str;
        } else if (r3 instanceof C17850za) {
            ArrayList arrayList = ((C17850za) r3).A00;
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                A01((C17760zQ) arrayList.get(size), str);
            }
        } else if (r3 instanceof C31441jh) {
            C31441jh r32 = (C31441jh) r3;
            r32.A00();
            ArrayList arrayList2 = r32.A05;
            for (int size2 = arrayList2.size() - 1; size2 >= 0; size2--) {
                ((C17680zI) arrayList2.get(size2)).A00 = str;
            }
        } else {
            throw new RuntimeException("Unhandled transition type: " + r3);
        }
    }

    public static void A02(C17760zQ r1, List list, String str) {
        if (r1 instanceof C31441jh) {
            C31441jh r12 = (C31441jh) r1;
            r12.A00();
            list.addAll(r12.A05);
        } else if (r1 != null) {
            list.add(r1);
        } else {
            throw new IllegalStateException(AnonymousClass08S.A0P("[", str, "] Adding null to transition list is not allowed."));
        }
    }

    public static boolean A03(Context context) {
        if (!AnonymousClass07c.ARE_TRANSITIONS_SUPPORTED || AnonymousClass07c.isAnimationDisabled) {
            return false;
        }
        if (!AnonymousClass07c.isEndToEndTestRun) {
            return true;
        }
        if (!AnonymousClass07c.CAN_CHECK_GLOBAL_ANIMATOR_SETTINGS || context == null) {
            return false;
        }
        float f = Settings.Global.getFloat(context.getContentResolver(), "animator_duration_scale", 1.0f);
        if (AnonymousClass07c.forceEnableTransitionsForInstrumentationTests || f != 0.0f) {
            return true;
        }
        return false;
    }

    public static C17750zP A00(C17470yx r5) {
        int i;
        String str;
        String str2;
        if (r5.BBv()) {
            str = r5.B73();
            Integer B74 = r5.B74();
            if (B74 == AnonymousClass07B.A00) {
                i = 1;
                str2 = null;
            } else if (B74 == AnonymousClass07B.A01) {
                str2 = r5.B75();
                i = 2;
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Unhandled transition key type ", C21819AiH.A00(B74)));
            }
        } else {
            C17770zR B5K = r5.B5K();
            i = 3;
            if (B5K != null) {
                str = B5K.A06;
            } else {
                str = null;
            }
            str2 = null;
        }
        if (str != null) {
            return new C17750zP(i, str, str2);
        }
        return null;
    }
}
