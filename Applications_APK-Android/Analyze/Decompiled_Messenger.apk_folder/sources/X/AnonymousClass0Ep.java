package X;

import java.io.File;
import java.io.IOException;

/* renamed from: X.0Ep  reason: invalid class name */
public final class AnonymousClass0Ep {
    public final File A00;

    public static File A00(AnonymousClass0Ep r3, String str, String str2) {
        File file = r3.A00;
        if (str2 == null) {
            str2 = "0";
        }
        return new File(file, AnonymousClass08S.A0P(str, "_", str2));
    }

    public File A02(String str, String str2) {
        return new File(A00(this, str, str2), "download.zip");
    }

    public AnonymousClass0Ep(String str, String str2) {
        this.A00 = new File(str, str2);
    }

    public static void A01(File file) {
        File[] listFiles;
        if (file.isDirectory() && (listFiles = file.listFiles()) != null) {
            for (File A01 : listFiles) {
                A01(A01);
            }
        }
        file.delete();
    }

    public void A03(String str, String str2) {
        File A002 = A00(this, str, str2);
        if (!A002.exists() && !A002.mkdirs() && !A002.isDirectory()) {
            throw new IOException(AnonymousClass08S.A0J("Error creating directory: '", A002.getCanonicalPath()));
        }
    }
}
