package X;

import android.net.Uri;
import com.facebook.proxygen.TraceFieldType;
import java.io.File;
import java.util.Arrays;

/* renamed from: X.1Q0  reason: invalid class name */
public class AnonymousClass1Q0 {
    private File A00;
    public final int A01;
    public final Uri A02;
    public final C196579Mo A03;
    public final C23541Px A04;
    public final C23561Pz A05;
    public final AnonymousClass36w A06;
    public final AnonymousClass1Q1 A07;
    public final C23411Pk A08;
    public final AnonymousClass1OM A09;
    public final C23531Pw A0A;
    public final AnonymousClass32I A0B;
    public final Boolean A0C;
    public final Boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;

    public synchronized File A02() {
        if (this.A00 == null) {
            this.A00 = new File(this.A02.getPath());
        }
        return this.A00;
    }

    public static AnonymousClass1Q0 A00(Uri uri) {
        if (uri == null) {
            return null;
        }
        return C23521Pv.A00(uri).A02();
    }

    public static AnonymousClass1Q0 A01(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        return A00(Uri.parse(str));
    }

    public boolean equals(Object obj) {
        C23601Qd r1;
        if (obj instanceof AnonymousClass1Q0) {
            AnonymousClass1Q0 r4 = (AnonymousClass1Q0) obj;
            if (this.A0G == r4.A0G && this.A0E == r4.A0E && this.A0F == r4.A0F && C14620th.A01(this.A02, r4.A02) && C14620th.A01(this.A09, r4.A09) && C14620th.A01(this.A00, r4.A00) && C14620th.A01(this.A03, r4.A03) && C14620th.A01(this.A04, r4.A04) && C14620th.A01(this.A06, r4.A06) && C14620th.A01(this.A05, r4.A05) && C14620th.A01(this.A0A, r4.A0A) && C14620th.A01(this.A0C, r4.A0C) && C14620th.A01(this.A0D, r4.A0D) && C14620th.A01(this.A07, r4.A07)) {
                AnonymousClass32I r0 = this.A0B;
                C23601Qd r2 = null;
                if (r0 != null) {
                    r1 = r0.Ayn();
                } else {
                    r1 = null;
                }
                AnonymousClass32I r02 = r4.A0B;
                if (r02 != null) {
                    r2 = r02.Ayn();
                }
                return C14620th.A01(r1, r2);
            }
        }
        return false;
    }

    public int hashCode() {
        C23601Qd r13;
        AnonymousClass32I r0 = this.A0B;
        if (r0 != null) {
            r13 = r0.Ayn();
        } else {
            r13 = null;
        }
        return Arrays.hashCode(new Object[]{this.A09, this.A02, Boolean.valueOf(this.A0G), this.A03, this.A05, this.A0A, Boolean.valueOf(this.A0E), Boolean.valueOf(this.A0F), this.A04, this.A0C, this.A06, this.A07, r13, this.A0D});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0045, code lost:
        if (X.C006206h.A06(r4.A00) == false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0076, code lost:
        if (r1.startsWith("video/") == false) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c3, code lost:
        if (r0 == false) goto L_0x00c5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1Q0(X.C23521Pv r4) {
        /*
            r3 = this;
            r3.<init>()
            X.1OM r0 = r4.A07
            r3.A09 = r0
            android.net.Uri r2 = r4.A00
            r3.A02 = r2
            if (r2 == 0) goto L_0x00c5
            boolean r0 = X.C006206h.A06(r2)
            if (r0 == 0) goto L_0x005f
            r1 = 0
        L_0x0014:
            r3.A01 = r1
            boolean r0 = r4.A0E
            r3.A0H = r0
            boolean r0 = r4.A0D
            r3.A0G = r0
            X.1Px r0 = r4.A02
            r3.A04 = r0
            X.36w r0 = r4.A04
            r3.A06 = r0
            X.1Q1 r0 = r4.A05
            if (r0 != 0) goto L_0x005e
            X.1Q1 r0 = X.AnonymousClass1Q1.A02
        L_0x002c:
            r3.A07 = r0
            X.9Mo r0 = r4.A01
            r3.A03 = r0
            X.1Pz r0 = r4.A03
            r3.A05 = r0
            X.1Pw r0 = r4.A08
            r3.A0A = r0
            boolean r0 = r4.A0C
            if (r0 == 0) goto L_0x0047
            android.net.Uri r0 = r4.A00
            boolean r1 = X.C006206h.A06(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0048
        L_0x0047:
            r0 = 0
        L_0x0048:
            r3.A0E = r0
            r0 = 1
            r3.A0F = r0
            java.lang.Boolean r0 = r4.A0A
            r3.A0C = r0
            X.32I r0 = r4.A09
            r3.A0B = r0
            X.1Pk r0 = r4.A06
            r3.A08 = r0
            java.lang.Boolean r0 = r4.A0B
            r3.A0D = r0
            return
        L_0x005e:
            goto L_0x002c
        L_0x005f:
            boolean r0 = X.C006206h.A04(r2)
            if (r0 == 0) goto L_0x007e
            java.lang.String r0 = r2.getPath()
            java.lang.String r1 = X.C80143rv.A00(r0)
            if (r1 == 0) goto L_0x0078
            java.lang.String r0 = "video/"
            boolean r1 = r1.startsWith(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0079
        L_0x0078:
            r0 = 0
        L_0x0079:
            r1 = 3
            if (r0 == 0) goto L_0x0014
            r1 = 2
            goto L_0x0014
        L_0x007e:
            boolean r0 = X.C006206h.A03(r2)
            if (r0 == 0) goto L_0x0086
            r1 = 4
            goto L_0x0014
        L_0x0086:
            if (r2 != 0) goto L_0x0093
            r1 = 0
        L_0x0089:
            java.lang.String r0 = "asset"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0098
            r1 = 5
            goto L_0x0014
        L_0x0093:
            java.lang.String r1 = r2.getScheme()
            goto L_0x0089
        L_0x0098:
            boolean r0 = X.C006206h.A05(r2)
            if (r0 == 0) goto L_0x00a1
            r1 = 6
            goto L_0x0014
        L_0x00a1:
            if (r2 != 0) goto L_0x00af
            r1 = 0
        L_0x00a4:
            java.lang.String r0 = "data"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00b4
            r1 = 7
            goto L_0x0014
        L_0x00af:
            java.lang.String r1 = r2.getScheme()
            goto L_0x00a4
        L_0x00b4:
            if (r2 != 0) goto L_0x00c8
            r1 = 0
        L_0x00b7:
            r0 = 808(0x328, float:1.132E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r0.equals(r1)
            r1 = 8
            if (r0 != 0) goto L_0x0014
        L_0x00c5:
            r1 = -1
            goto L_0x0014
        L_0x00c8:
            java.lang.String r1 = r2.getScheme()
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Q0.<init>(X.1Pv):void");
    }

    public String toString() {
        AnonymousClass217 A002 = C14620th.A00(this);
        AnonymousClass217.A00(A002, TraceFieldType.Uri, this.A02);
        AnonymousClass217.A00(A002, "cacheChoice", this.A09);
        AnonymousClass217.A00(A002, "decodeOptions", this.A04);
        AnonymousClass217.A00(A002, "postprocessor", this.A0B);
        AnonymousClass217.A00(A002, "priority", this.A05);
        AnonymousClass217.A00(A002, "resizeOptions", this.A06);
        AnonymousClass217.A00(A002, "rotationOptions", this.A07);
        AnonymousClass217.A00(A002, "bytesRange", this.A03);
        AnonymousClass217.A00(A002, "resizingAllowedOverride", this.A0D);
        A002.A01("progressiveRenderingEnabled", this.A0H);
        A002.A01("localThumbnailPreviewsEnabled", this.A0G);
        AnonymousClass217.A00(A002, "lowestPermittedRequestLevel", this.A0A);
        A002.A01("isDiskCacheEnabled", this.A0E);
        A002.A01("isMemoryCacheEnabled", this.A0F);
        AnonymousClass217.A00(A002, "decodePrefetches", this.A0C);
        return A002.toString();
    }
}
