package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.google.common.collect.ImmutableList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0bL  reason: invalid class name and case insensitive filesystem */
public final class C06340bL implements C07290dB {
    private AnonymousClass0US A00;

    public void BY7(int i) {
    }

    public static void A00(C182478dO r16, C182488dP r17, String str, long j, AnonymousClass0US r21, String str2, String str3, String str4, boolean z) {
        C182478dO r14 = r16;
        boolean z2 = z;
        if (r14.A02.A02(str4, z2).A01()) {
            C188518p6 r12 = (C188518p6) r21.get();
            long ApH = r12.ApH();
            long j2 = j;
            long j3 = j2;
            String str5 = str2;
            long A05 = r14.A03().A05(str5, -1);
            String str6 = str3;
            if (A05 != -1) {
                r14.A04(str6, ApH, z2);
                j3 = A05 + ApH;
            }
            C182488dP r6 = r17;
            if (j >= j3) {
                r14.A04(str6, ApH, z2);
                C11670nb A02 = r14.A02(r12, j2, str);
                if (A02 != null) {
                    if (r6.A02 == null) {
                        r6.A02 = ImmutableList.builder();
                    }
                    r6.A02.add((Object) A02);
                    r6.A00(str5, j2);
                    r6.A00 = Math.min(r6.A00, ApH + j);
                    return;
                }
                return;
            }
            r6.A00 = Math.min(r6.A00, j3);
        }
    }

    public static void A01(C182478dO r15, C182488dP r16, String str, long j, AnonymousClass0US r20, String str2, String str3, String str4, boolean z, long j2) {
        long j3 = j2;
        boolean z2 = z;
        if (r15.A02.A02(str4, z2).A01()) {
            long j4 = j;
            long j5 = j4;
            String str5 = str2;
            long j6 = j3;
            long A05 = r15.A03().A05(str5, -1);
            String str6 = str3;
            if (A05 != -1) {
                r15.A04(str6, j6, z2);
                j5 = A05 + j2;
            }
            C182488dP r8 = r16;
            if (j >= j5) {
                r15.A04(str6, j3, z2);
                C11670nb A02 = r15.A02((C422929f) r20.get(), j4, str);
                if (A02 != null) {
                    if (r8.A02 == null) {
                        r8.A02 = ImmutableList.builder();
                    }
                    r8.A02.add((Object) A02);
                    r8.A00(str5, j4);
                    r8.A00 = Math.min(r8.A00, j2 + j);
                    return;
                }
                return;
            }
            r8.A00 = Math.min(r8.A00, j5);
        }
    }

    /* JADX INFO: finally extract failed */
    public void BY5() {
        List<C11670nb> list;
        long j;
        AnonymousClass4MN r2 = (AnonymousClass4MN) this.A00.get();
        synchronized (r2) {
            AnonymousClass4MM r4 = r2.A01;
            C182478dO r1 = (C182478dO) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADW, r4.A00);
            long now = r4.A01.now();
            boolean AbO = r1.A04.AbO(184, false);
            if ((AbO || !r1.A0A) && (!AbO || r1.A05.A09())) {
                list = Collections.EMPTY_LIST;
            } else {
                String str = (String) r1.A07.get();
                synchronized (r1) {
                    j = r1.A00;
                }
                list = null;
                if (j <= now) {
                    C182488dP r8 = new C182488dP(r1);
                    try {
                        AnonymousClass4YC r0 = r1.A03;
                        C182478dO r7 = r1;
                        A01(r7, r8, str, now, r0.A00, "AppInstallationPeriodicReporter-app_installations", "AppInstallationPeriodicReporter", "app_installations", true, 86400000);
                        A01(r7, r8, str, now, r0.A0D, "PeriodicFeatureStatusReporter-features_status", "PeriodicFeatureStatusReporter", "features_status", true, 43200000);
                        A01(r7, r8, str, now, r0.A0E, "PistolFirePeriodicReporter-fbandroid_pistol_fire_crash", "PistolFirePeriodicReporter", "fbandroid_pistol_fire_crash", false, 86400000);
                        C182478dO r19 = r1;
                        C182488dP r20 = r8;
                        String str2 = str;
                        long j2 = now;
                        A00(r19, r20, str2, j2, r0.A0F, "ProcessStatusPeriodicReporter-process_status", "ProcessStatusPeriodicReporter", "process_status", false);
                        A01(r7, r8, str, now, r0.A03, "DBSizePeriodicReporter-db_size_info", "DBSizePeriodicReporter", "db_size_info", false, 86400000);
                        A00(r19, r20, str2, j2, r0.A04, "DeviceDetectionInfoPeriodicReporter-device_detection", "DeviceDetectionInfoPeriodicReporter", "device_detection", true);
                        A00(r7, r8, str, now, r0.A05, "DeviceInfoPeriodicReporter-device_info", AnonymousClass80H.$const$string(AnonymousClass1Y3.A1w), "device_info", true);
                        A00(r1, r8, str, now, r0.A06, "DeviceStatusPeriodicReporter-device_status", "DeviceStatusPeriodicReporter", "device_status", true);
                        A01(r7, r8, str, now, r0.A0C, "OptSvcEventPeriodicReporter-optsvc_event", "OptSvcEventPeriodicReporter", "optsvc_event", true, 14400000);
                        C182478dO r192 = r1;
                        C182488dP r202 = r8;
                        String str3 = str;
                        long j3 = now;
                        A01(r192, r202, str3, j3, r0.A02, "ContactsUploadPeriodicReporter-contacts_upload_state", "ContactsUploadPeriodicReporter", "contacts_upload_state", true, 86400000);
                        A01(r7, r8, str, now, r0.A08, "ImagePipelinePeriodicReporter-image_pipeline_counters", "ImagePipelinePeriodicReporter", "image_pipeline_counters", false, 14400000);
                        A01(r192, r202, str3, j3, r0.A09, "LocationStatePeriodicEventReporter-location_state_event", "LocationStatePeriodicEventReporter", "location_state_event", false, 28800000);
                        A01(r7, r8, str, now, r0.A0B, "MultiAccountsPeriodicReporter-mswitch_accounts_state", "MultiAccountsPeriodicReporter", "mswitch_accounts_state", true, 86400000);
                        A01(r192, r202, str3, j3, r0.A0H, "SwitchAccountsBadgingPeriodicLogger-badge_consistency_check", "SwitchAccountsBadgingPeriodicLogger", "badge_consistency_check", false, 172800000);
                        A01(r7, r8, str, now, r0.A01, "AudioCache-media_cache_size", "AudioCache", "media_cache_size", false, 3600000);
                        A01(r192, r202, str3, j3, r0.A0A, "MessageRequestBadgingPeriodicLogger-badge_consistency_check", "MessageRequestBadgingPeriodicLogger", "badge_consistency_check", false, 172800000);
                        A01(r7, r8, str, now, r0.A0G, "SmsTakeoverPeriodicReporter-sms_takeover_daily_status", "SmsTakeoverPeriodicReporter", "sms_takeover_daily_status", false, 86400000);
                        A01(r192, r202, str3, j3, r0.A07, "ImageFetchEfficiencyReporter-android_image_fetch_efficiency", "ImageFetchEfficiencyReporter", "android_image_fetch_efficiency", true, 86400000);
                        A01(r7, r8, str, now, r0.A0I, "VideoCachePeriodicReporter-video_cache_counters", "VideoCachePeriodicReporter", "video_cache_counters", false, 18000000);
                        A01(r192, r202, str3, j3, r0.A0J, "VideoPerformancePeriodicReporter-video_daily_data_usage", "VideoPerformancePeriodicReporter", "video_daily_data_usage", false, 86400000);
                        ImmutableList.Builder builder = r8.A02;
                        if (builder != null) {
                            list = builder.build();
                        } else {
                            list = null;
                        }
                        AnonymousClass16O r02 = r8.A01;
                        if (r02 != null) {
                            r02.A06();
                        }
                        long j4 = r8.A00;
                        if (j4 >= Long.MAX_VALUE) {
                            j4 = -1;
                        }
                        C182478dO.A01(r1, j4);
                    } catch (Throwable th) {
                        AnonymousClass16O r03 = r8.A01;
                        if (r03 != null) {
                            r03.A06();
                        }
                        long j5 = r8.A00;
                        if (j5 >= Long.MAX_VALUE) {
                            j5 = -1;
                        }
                        C182478dO.A01(r1, j5);
                        throw th;
                    }
                }
            }
            if (list != null && !list.isEmpty()) {
                DeprecatedAnalyticsLogger deprecatedAnalyticsLogger = (DeprecatedAnalyticsLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Avo, r2.A00);
                for (C11670nb A08 : list) {
                    deprecatedAnalyticsLogger.A08(A08);
                }
            }
        }
    }

    public C06340bL(AnonymousClass0US r1) {
        this.A00 = r1;
    }
}
