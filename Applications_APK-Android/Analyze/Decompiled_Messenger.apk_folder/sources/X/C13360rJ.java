package X;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import java.util.UUID;

/* renamed from: X.0rJ  reason: invalid class name and case insensitive filesystem */
public final class C13360rJ {
    public static final String A00 = String.format("Null metadata in caller identity, API=%d", Integer.valueOf(Build.VERSION.SDK_INT));
    public static final String A01 = AnonymousClass08S.A0J("com.facebook.class", UUID.randomUUID().toString().replace('-', '_'));

    /* JADX WARNING: Removed duplicated region for block: B:64:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0119 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C06620bo A00(android.content.Intent r13, X.AnonymousClass04e r14) {
        /*
            java.lang.String r4 = "v"
            java.lang.String r5 = "d"
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 17
            r0 = 0
            if (r2 < r1) goto L_0x000c
            r0 = 1
        L_0x000c:
            r6 = 0
            r11 = 0
            if (r0 == 0) goto L_0x0042
            r8 = 0
        L_0x0011:
            r9 = -1
            java.lang.String r1 = "_ci_"
            boolean r0 = r13.hasExtra(r1)
            if (r0 == 0) goto L_0x00fb
            android.os.Parcelable r3 = r13.getParcelableExtra(r1)
            android.app.PendingIntent r3 = (android.app.PendingIntent) r3
            if (r3 == 0) goto L_0x00f2
            r1 = 17
            r0 = 0
            if (r2 < r1) goto L_0x0028
            r0 = 1
        L_0x0028:
            if (r0 == 0) goto L_0x003d
            java.lang.String r7 = r3.getCreatorPackage()
        L_0x002e:
            r0 = 0
            if (r2 < r1) goto L_0x0032
            r0 = 1
        L_0x0032:
            if (r0 == 0) goto L_0x0038
            int r9 = r3.getCreatorUid()
        L_0x0038:
            r0 = 24
            if (r2 < r0) goto L_0x0064
            goto L_0x0049
        L_0x003d:
            java.lang.String r7 = r3.getTargetPackage()
            goto L_0x002e
        L_0x0042:
            java.lang.String r0 = "Skipping caller identity metadata check on API <= 17."
            A03(r14, r0, r11)
            r8 = 1
            goto L_0x0011
        L_0x0049:
            java.lang.Class<android.app.PendingIntent> r2 = android.app.PendingIntent.class
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            java.lang.Class[] r1 = new java.lang.Class[]{r0}     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.String r0 = "getTag"
            java.lang.reflect.Method r1 = r2.getMethod(r0, r1)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.String r0 = ""
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.Object r1 = r1.invoke(r3, r0)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            goto L_0x0093
        L_0x0064:
            r0 = 18
            if (r2 < r0) goto L_0x0092
            java.util.List r1 = java.util.Collections.emptyList()     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.Class[] r0 = new java.lang.Class[r6]     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.Object[] r2 = r1.toArray(r0)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.Class[] r2 = (java.lang.Class[]) r2     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.Class<android.app.PendingIntent> r1 = android.app.PendingIntent.class
            java.lang.String r0 = "getIntent"
            java.lang.reflect.Method r1 = r1.getMethod(r0, r2)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.Object[] r0 = new java.lang.Object[r6]     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.Object r0 = r1.invoke(r3, r0)     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            android.content.Intent r0 = (android.content.Intent) r0     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            java.lang.String r1 = r0.getAction()     // Catch:{ IllegalAccessException | NoSuchMethodException | InvocationTargetException -> 0x0089 }
            goto L_0x0093
        L_0x0089:
            r1 = move-exception
            java.lang.String r0 = "Error extracting metadata from caller identity."
            A03(r14, r0, r1)
            r1 = r11
            r8 = 0
            goto L_0x0093
        L_0x0092:
            r1 = 0
        L_0x0093:
            if (r1 == 0) goto L_0x00e9
            r0 = 11
            byte[] r2 = android.util.Base64.decode(r1, r0)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            java.lang.String r1 = new java.lang.String     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            java.lang.String r0 = "UTF-8"
            r1.<init>(r2, r0)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            r3.<init>(r1)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            java.lang.String r0 = "t"
            java.lang.String r0 = r3.getString(r0)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            long r1 = java.lang.Long.parseLong(r0)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            boolean r0 = r3.has(r5)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            if (r0 == 0) goto L_0x00bc
            java.lang.String r13 = r3.getString(r5)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e5 }
            goto L_0x00bd
        L_0x00bc:
            r13 = r11
        L_0x00bd:
            boolean r0 = r3.has(r4)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x0104 }
            if (r0 == 0) goto L_0x00c8
            java.lang.String r12 = r3.getString(r4)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x0104 }
            goto L_0x00c9
        L_0x00c8:
            r12 = r11
        L_0x00c9:
            r0 = 60000(0xea60, float:8.4078E-41)
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e3 }
            long r4 = r4 - r1
            long r2 = (long) r0     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e3 }
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            r0 = 0
            if (r1 >= 0) goto L_0x00d8
            r0 = 1
        L_0x00d8:
            if (r0 == 0) goto L_0x00dc
            r6 = 1
            goto L_0x010b
        L_0x00dc:
            java.lang.String r0 = "Caller identity has expired."
            A03(r14, r0, r11)     // Catch:{ UnsupportedEncodingException | IllegalArgumentException | JSONException -> 0x00e3 }
            r6 = r8
            goto L_0x010b
        L_0x00e3:
            r1 = move-exception
            goto L_0x0106
        L_0x00e5:
            r1 = move-exception
            r12 = r11
            r13 = r11
            goto L_0x0106
        L_0x00e9:
            java.lang.String r0 = X.C13360rJ.A00
            A03(r14, r0, r11)
            r6 = r8
            r12 = r11
            r13 = r11
            goto L_0x010b
        L_0x00f2:
            java.lang.String r0 = "Null caller identity intent extra."
            A03(r14, r0, r11)
            r12 = r11
            r13 = r11
            r7 = r11
            goto L_0x010c
        L_0x00fb:
            java.lang.String r0 = "Missing caller identity intent extra."
            A03(r14, r0, r11)
            r7 = r11
            r12 = r11
            r13 = r11
            goto L_0x010c
        L_0x0104:
            r1 = move-exception
            r12 = r11
        L_0x0106:
            java.lang.String r0 = "Error parsing metadata from caller identity."
            A03(r14, r0, r1)
        L_0x010b:
            r8 = r6
        L_0x010c:
            if (r8 == 0) goto L_0x0119
            X.0bo r0 = new X.0bo
            java.util.List r10 = java.util.Collections.singletonList(r7)
            r8 = r0
            r8.<init>(r9, r10, r11, r12, r13)
            return r0
        L_0x0119:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13360rJ.A00(android.content.Intent, X.04e):X.0bo");
    }

    private static void A03(AnonymousClass04e r1, String str, Throwable th) {
        if (r1 != null) {
            r1.C2T("CallerInfoHelper", str, th);
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x005d */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.content.Intent r9, android.content.Context r10, java.lang.String r11) {
        /*
            java.lang.ClassLoader r0 = r10.getClassLoader()     // Catch:{ Exception -> 0x007f }
            r9.setExtrasClassLoader(r0)     // Catch:{ Exception -> 0x007f }
            android.os.Bundle r5 = r9.getExtras()     // Catch:{ Exception -> 0x007f }
            if (r5 != 0) goto L_0x0012
            android.os.Bundle r5 = new android.os.Bundle     // Catch:{ Exception -> 0x007f }
            r5.<init>()     // Catch:{ Exception -> 0x007f }
        L_0x0012:
            java.lang.ClassLoader r0 = r10.getClassLoader()     // Catch:{ Exception -> 0x007f }
            r5.setClassLoader(r0)     // Catch:{ Exception -> 0x007f }
            java.lang.String r4 = "_ci_"
            android.content.pm.PackageManager r2 = r10.getPackageManager()     // Catch:{ NameNotFoundException | RuntimeException -> 0x002b }
            java.lang.String r1 = r10.getPackageName()     // Catch:{ NameNotFoundException | RuntimeException -> 0x002b }
            r0 = 0
            android.content.pm.PackageInfo r0 = r2.getPackageInfo(r1, r0)     // Catch:{ NameNotFoundException | RuntimeException -> 0x002b }
            java.lang.String r8 = r0.versionName     // Catch:{ NameNotFoundException | RuntimeException -> 0x002b }
            goto L_0x002c
        L_0x002b:
            r8 = 0
        L_0x002c:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x007f }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x007f }
            r3.<init>()     // Catch:{ Exception -> 0x007f }
            r0 = 0
            java.lang.String r2 = "t"
            java.lang.String r1 = java.lang.Long.toString(r6)     // Catch:{ JSONException -> 0x005d }
            r3.put(r2, r1)     // Catch:{ JSONException -> 0x005d }
            if (r11 == 0) goto L_0x0046
            java.lang.String r1 = "d"
            r3.put(r1, r11)     // Catch:{ JSONException -> 0x005d }
        L_0x0046:
            if (r8 == 0) goto L_0x004d
            java.lang.String r1 = "v"
            r3.put(r1, r8)     // Catch:{ JSONException -> 0x005d }
        L_0x004d:
            java.lang.String r2 = r3.toString()     // Catch:{  }
            java.lang.String r1 = "UTF-8"
            byte[] r2 = r2.getBytes(r1)     // Catch:{  }
            r1 = 11
            java.lang.String r0 = android.util.Base64.encodeToString(r2, r1)     // Catch:{  }
        L_0x005d:
            X.26I r3 = new X.26I     // Catch:{ Exception -> 0x007f }
            r3.<init>()     // Catch:{ Exception -> 0x007f }
            r3.A02 = r0     // Catch:{ Exception -> 0x007f }
            android.content.ComponentName r1 = new android.content.ComponentName     // Catch:{ Exception -> 0x007f }
            java.lang.String r0 = X.C13360rJ.A01     // Catch:{ Exception -> 0x007f }
            r1.<init>(r10, r0)     // Catch:{ Exception -> 0x007f }
            r3.A00 = r1     // Catch:{ Exception -> 0x007f }
            r2 = 0
            r1 = 1140850688(0x44000000, float:512.0)
            android.content.Intent r0 = X.AnonymousClass26I.A00(r3)     // Catch:{ Exception -> 0x007f }
            android.app.PendingIntent r0 = android.app.PendingIntent.getActivity(r10, r2, r0, r1)     // Catch:{ Exception -> 0x007f }
            r5.putParcelable(r4, r0)     // Catch:{ Exception -> 0x007f }
            r9.putExtras(r5)     // Catch:{ Exception -> 0x007f }
            return
        L_0x007f:
            r1 = move-exception
            X.3DY r0 = new X.3DY
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13360rJ.A01(android.content.Intent, android.content.Context, java.lang.String):void");
    }

    public static void A02(Intent intent, Context context, String str, AnonymousClass04e r3) {
        try {
            A01(intent, context, str);
        } catch (AnonymousClass3DY e) {
            r3.C2T("CallerInfoHelper", "Error attaching caller info to Intent.", e);
        }
    }
}
