package X;

import android.graphics.Bitmap;

/* renamed from: X.1S5  reason: invalid class name */
public final class AnonymousClass1S5 extends AnonymousClass1S6 {
    public AnonymousClass1PS A00;
    public final int A01;
    public final int A02;
    public final C33271nJ A03;
    public volatile Bitmap A04;

    public synchronized AnonymousClass1PS A05() {
        return AnonymousClass1PS.A00(this.A00);
    }

    public int getHeight() {
        int i;
        if (this.A02 % AnonymousClass1Y3.A1Q != 0 || (i = this.A01) == 5 || i == 7) {
            Bitmap bitmap = this.A04;
            if (bitmap != null) {
                return bitmap.getWidth();
            }
            return 0;
        }
        Bitmap bitmap2 = this.A04;
        if (bitmap2 != null) {
            return bitmap2.getHeight();
        }
        return 0;
    }

    public int getWidth() {
        int i;
        if (this.A02 % AnonymousClass1Y3.A1Q != 0 || (i = this.A01) == 5 || i == 7) {
            Bitmap bitmap = this.A04;
            if (bitmap != null) {
                return bitmap.getHeight();
            }
            return 0;
        }
        Bitmap bitmap2 = this.A04;
        if (bitmap2 != null) {
            return bitmap2.getWidth();
        }
        return 0;
    }

    public AnonymousClass1S5(AnonymousClass1PS r2, C33271nJ r3, int i, int i2) {
        AnonymousClass1PS A09 = r2.A09();
        C05520Zg.A02(A09);
        this.A00 = A09;
        this.A04 = (Bitmap) A09.A0A();
        this.A03 = r3;
        this.A02 = i;
        this.A01 = i2;
    }

    public AnonymousClass1S5(Bitmap bitmap, C22891Nf r3, C33271nJ r4, int i, int i2) {
        C05520Zg.A02(bitmap);
        this.A04 = bitmap;
        Bitmap bitmap2 = this.A04;
        C05520Zg.A02(r3);
        this.A00 = AnonymousClass1PS.A02(bitmap2, r3);
        this.A03 = r4;
        this.A02 = i;
        this.A01 = i2;
    }
}
