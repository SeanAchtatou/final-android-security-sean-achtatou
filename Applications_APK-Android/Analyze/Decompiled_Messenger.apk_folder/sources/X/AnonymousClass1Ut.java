package X;

import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.google.common.base.Preconditions;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Ut  reason: invalid class name */
public final class AnonymousClass1Ut implements OmnistoreComponent {
    public static final Class A0E = AnonymousClass1Ut.class;
    private static volatile AnonymousClass1Ut A0F;
    public Collection A00;
    public CollectionName A01;
    public Omnistore A02;
    private AnonymousClass1Uw A03;
    public final AnonymousClass1Uw A04 = new AnonymousClass1Ux(this);
    public final AnonymousClass0US A05;
    public final C25051Yd A06;
    public final C24341Tf A07;
    public final ExecutorService A08;
    public final C04310Tq A09;
    private final AnonymousClass0XN A0A;
    private final AnonymousClass18N A0B;
    private final C12420pJ A0C;
    private final AnonymousClass1Uw A0D = new AnonymousClass1Uv();

    public void Boz(int i) {
    }

    public String getCollectionLabel() {
        return "tincan_msg";
    }

    public synchronized void onCollectionAvailable(Collection collection) {
        this.A00 = collection;
        if (this.A0C.A02()) {
            this.A03 = this.A04;
        } else {
            this.A03 = this.A0D;
        }
    }

    public synchronized void onCollectionInvalidated() {
        this.A00 = null;
        this.A03 = null;
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static final AnonymousClass1Ut A00(AnonymousClass1XY r13) {
        if (A0F == null) {
            synchronized (AnonymousClass1Ut.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0F, r13);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r13.getApplicationInjector();
                        A0F = new AnonymousClass1Ut(AnonymousClass18N.A00(applicationInjector), AnonymousClass0XM.A00(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.APN, applicationInjector), C12420pJ.A00(applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AVl, applicationInjector), AnonymousClass1Uu.A00(applicationInjector), AnonymousClass0UX.A0Z(applicationInjector), C24341Tf.A00(applicationInjector), AnonymousClass0WT.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0F;
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    public void BVs(List list) {
        AnonymousClass1Uw r0 = this.A03;
        if (r0 != null) {
            r0.BAZ(list);
        }
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        long j;
        this.A02 = omnistore;
        if (this.A0C.A02() && this.A0A.A0I() && !this.A0A.A0J()) {
            boolean z = false;
            if (this.A0B.A02() == "deviceidinvalid") {
                z = true;
            }
            if (!z) {
                CollectionName.Builder createCollectionNameBuilder = omnistore.createCollectionNameBuilder("tincan_msg");
                createCollectionNameBuilder.addSegment(this.A0A.A09().A0j);
                createCollectionNameBuilder.addSegment(this.A0B.A02());
                this.A01 = createCollectionNameBuilder.build();
                C32151lI r2 = new C32151lI();
                r2.A05 = true;
                if (!this.A06.Aem(283326109190486L)) {
                    C60952y6 r1 = (C60952y6) this.A09.get();
                    r1.A00.AOx();
                    String A052 = ((C55592oK) r1.A03.get()).A05(C60952y6.A05);
                    if (A052 == null) {
                        j = 0;
                    } else {
                        try {
                            j = Long.parseLong(A052);
                        } catch (NumberFormatException unused) {
                            j = 0;
                        }
                    }
                    r2.A01 = j;
                }
                return C32171lK.A00(this.A01, new C32161lJ(r2));
            }
        }
        return C32171lK.A03;
    }

    private AnonymousClass1Ut(AnonymousClass18N r3, AnonymousClass0XN r4, C04310Tq r5, C12420pJ r6, AnonymousClass0US r7, AnonymousClass1Uu r8, ExecutorService executorService, C24341Tf r10, C25051Yd r11) {
        this.A0B = r3;
        this.A0A = r4;
        this.A09 = r5;
        this.A0C = r6;
        this.A05 = r7;
        this.A07 = r10;
        this.A08 = executorService;
        this.A06 = r11;
        synchronized (r8) {
            Preconditions.checkArgument(!r8.A03.contains(this));
            r8.A03.add(this);
        }
    }
}
