package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import java.util.concurrent.ExecutorService;

/* renamed from: X.17E  reason: invalid class name */
public final class AnonymousClass17E extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$31";
    public final /* synthetic */ ThreadKey A00;
    public final /* synthetic */ C190716r A01;
    public final /* synthetic */ String A02;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass17E(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, ThreadKey threadKey, String str2) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = threadKey;
        this.A02 = str2;
    }
}
