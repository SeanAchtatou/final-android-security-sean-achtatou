package X;

/* renamed from: X.0d0  reason: invalid class name */
public final class AnonymousClass0d0 extends AnonymousClass1RO {
    public AnonymousClass1PS A00 = null;
    public boolean A01 = false;

    public static boolean A00(AnonymousClass0d0 r2) {
        synchronized (r2) {
            if (r2.A01) {
                return false;
            }
            AnonymousClass1PS r1 = r2.A00;
            r2.A00 = null;
            r2.A01 = true;
            AnonymousClass1PS.A05(r1);
            return true;
        }
    }

    public AnonymousClass0d0(AnonymousClass0d1 r2, AnonymousClass373 r3, AnonymousClass1QK r4) {
        super(r2);
        r3.setCallback(this);
        r4.A06(new AnonymousClass2QT(this));
    }
}
