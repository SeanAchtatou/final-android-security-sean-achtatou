package X;

import java.util.List;
import java.util.Set;

/* renamed from: X.0UT  reason: invalid class name */
public class AnonymousClass0UT implements AnonymousClass1XY {
    public AnonymousClass1XY A00;

    public AnonymousClass1XY getApplicationInjector() {
        return this.A00.getApplicationInjector();
    }

    public C24811Xe getInjectorThreadStack() {
        return this.A00.getInjectorThreadStack();
    }

    public AnonymousClass0US getLazy(C22916BKm bKm) {
        return this.A00.getLazy(bKm);
    }

    public AnonymousClass0US getLazyList(C22916BKm bKm) {
        return this.A00.getLazyList(bKm);
    }

    public AnonymousClass0US getLazySet(C22916BKm bKm) {
        return this.A00.getLazySet(bKm);
    }

    public List getList(C22916BKm bKm) {
        return this.A00.getList(bKm);
    }

    public C04310Tq getListProvider(C22916BKm bKm) {
        return this.A00.getScopeAwareInjector().getListProvider(bKm);
    }

    public C04310Tq getProvider(C22916BKm bKm) {
        return this.A00.getScopeAwareInjector().getProvider(bKm);
    }

    public C24891Xn getScope(Class cls) {
        return this.A00.getScope(cls);
    }

    public C24851Xi getScopeAwareInjector() {
        return this.A00.getScopeAwareInjector();
    }

    public C24781Xb getScopeUnawareInjector() {
        return this.A00.getScopeUnawareInjector();
    }

    public Set getSet(C22916BKm bKm) {
        return this.A00.getSet(bKm);
    }

    public C04310Tq getSetProvider(C22916BKm bKm) {
        return this.A00.getScopeAwareInjector().getSetProvider(bKm);
    }

    public AnonymousClass0UT() {
    }

    public AnonymousClass0UT(AnonymousClass1XY r2) {
        this.A00 = (AnonymousClass1XY) r2.getScopeAwareInjector();
    }

    public Object getInstance(int i) {
        return this.A00.getInstance(i);
    }

    public Object getInstance(C22916BKm bKm) {
        return this.A00.getInstance(bKm);
    }

    public Object getInstance(Class cls) {
        return this.A00.getInstance(cls);
    }

    public Object getInstance(Class cls, Class cls2) {
        return this.A00.getInstance(cls, cls2);
    }
}
