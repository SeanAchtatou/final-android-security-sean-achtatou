package X;

import java.util.regex.Pattern;

/* renamed from: X.0NJ  reason: invalid class name */
public final class AnonymousClass0NJ {
    public static final int A00;
    public static final String A01 = Pattern.quote("|");
    public static final String A02 = Pattern.quote(",");

    static {
        int[] iArr = {3, 8, 8};
        int i = iArr[0];
        for (int i2 = 1; i2 < 3; i2++) {
            i = Math.min(i, iArr[i2]);
        }
        A00 = i;
    }
}
