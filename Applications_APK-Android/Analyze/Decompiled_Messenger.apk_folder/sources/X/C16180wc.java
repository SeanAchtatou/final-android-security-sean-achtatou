package X;

import android.os.Build;
import com.facebook.common.dextricks.DexStore;

/* renamed from: X.0wc  reason: invalid class name and case insensitive filesystem */
public final class C16180wc {
    public boolean A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07 = true;
    public boolean A08 = true;
    public boolean A09;

    public int A00() {
        int i = Build.VERSION.SDK_INT;
        int i2 = 0;
        if (i < 16) {
            return 0;
        }
        if (!this.A08) {
            i2 = 4;
        }
        if (this.A01) {
            i2 |= 1024;
        }
        if (!this.A07) {
            i2 |= 2;
        }
        if (this.A02) {
            i2 |= 512;
        }
        if (this.A03) {
            i2 |= DexStore.LOAD_RESULT_OATMEAL_QUICKENED;
        }
        if (this.A06) {
            i2 |= 1;
        }
        if (i < 19) {
            return i2;
        }
        if (this.A00) {
            i2 |= 2048;
        }
        if (this.A09) {
            i2 |= DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED;
        }
        if (i < 23) {
            return i2;
        }
        if (this.A05) {
            i2 |= DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED;
        }
        if (i < 26 || !this.A04) {
            return i2;
        }
        return i2 | 16;
    }
}
