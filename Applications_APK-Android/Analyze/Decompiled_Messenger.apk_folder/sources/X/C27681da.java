package X;

/* renamed from: X.1da  reason: invalid class name and case insensitive filesystem */
public final class C27681da {
    public C27661dY A00;
    public AnonymousClass1XL A01;

    public C27681da(C27671dZ r2, AnonymousClass1XL r3) {
        this.A00 = B3P.A02(r2);
        this.A01 = r3;
    }

    public void A00(C11410n6 r4, C14820uB r5) {
        AnonymousClass1XL A012 = C12990qL.A01(r5);
        AnonymousClass1XL r1 = this.A01;
        if (A012 != null && A012.compareTo((Enum) r1) < 0) {
            r1 = A012;
        }
        this.A01 = r1;
        this.A00.Bpu(r4, r5);
        this.A01 = A012;
    }
}
