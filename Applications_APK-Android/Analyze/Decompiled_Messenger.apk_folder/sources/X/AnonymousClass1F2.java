package X;

import android.os.Process;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1F2  reason: invalid class name */
public final class AnonymousClass1F2 {
    public final Map A00 = new HashMap(8);
    private final AnonymousClass1F3 A01 = new AnonymousClass1F3();
    private final Object A02 = new Object();

    public void A01() {
        if (this.A01.isHeldByCurrentThread()) {
            Thread currentThread = Thread.currentThread();
            synchronized (this.A02) {
                C33851oF r2 = (C33851oF) this.A00.get(currentThread);
                if (r2 != null) {
                    boolean z = true;
                    int i = r2.A01 - 1;
                    r2.A01 = i;
                    if (i != 1) {
                        z = false;
                    }
                    if (z) {
                        this.A00.remove(r2);
                    }
                    if (r2.A00 != Integer.MIN_VALUE && r2.A02 == Process.getThreadPriority(r2.A03)) {
                        Process.setThreadPriority(r2.A03, r2.A00);
                    }
                }
                this.A01.unlock();
            }
            return;
        }
        throw new IllegalStateException("Cannot unlock since this thread does not hold a lock currently");
    }

    public void A00() {
        boolean z;
        Thread currentThread = Thread.currentThread();
        int myTid = Process.myTid();
        synchronized (this.A02) {
            try {
                C33851oF r1 = (C33851oF) this.A00.get(currentThread);
                if (r1 != null) {
                    r1.A01++;
                } else {
                    this.A00.put(currentThread, new C33851oF(myTid));
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        while (!this.A01.tryLock()) {
            synchronized (this.A02) {
                try {
                    Thread owner = this.A01.getOwner();
                    if (owner != null) {
                        C33851oF r3 = (C33851oF) this.A00.get(owner);
                        if (r3 == null) {
                            z = false;
                        } else {
                            int threadPriority = Process.getThreadPriority(myTid);
                            int threadPriority2 = Process.getThreadPriority(r3.A03);
                            if (threadPriority2 > threadPriority) {
                                Process.setThreadPriority(r3.A03, threadPriority);
                                r3.A00 = threadPriority2;
                                r3.A02 = threadPriority;
                            }
                            z = true;
                        }
                        if (z) {
                            this.A01.lock();
                            return;
                        }
                    }
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
        }
        return;
        throw th;
    }
}
