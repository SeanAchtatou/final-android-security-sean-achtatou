package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.1bB  reason: invalid class name and case insensitive filesystem */
public final class C26271bB {
    public static final ImmutableSet A00 = ImmutableSet.A05("fb", "ro");
    public static final ImmutableSet A01;

    static {
        String[] strArr = new String[37];
        System.arraycopy(new String[]{"el", "en_GB", "es", "es_ES", "fi", "fil", "fr", "gu", "hi", "hr", "hu", "in", "it", "ja", "kn", "ko", "ml", "mr", "ms", "nb", "nl", "pa", "pl", "pt", "pt_PT", "ru", "sk"}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{"sv", "ta", "te", "th", "tl", "tr", "vi", "zh_CN", "zh_HK", "zh_TW"}, 0, strArr, 27, 10);
        A01 = ImmutableSet.A09("af", "ar", "bn", "cs", "da", "de", strArr);
    }
}
