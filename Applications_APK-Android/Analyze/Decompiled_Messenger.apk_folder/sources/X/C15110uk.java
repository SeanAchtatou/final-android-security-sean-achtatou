package X;

import java.io.Serializable;

/* renamed from: X.0uk  reason: invalid class name and case insensitive filesystem */
public final class C15110uk implements Serializable {
    private static final long serialVersionUID = 1;
    public String mId;
    public String mName;

    public C15110uk(String str, String str2) {
        this.mId = str;
        this.mName = str2;
    }
}
