package X;

import com.facebook.auth.viewercontext.ViewerContext;
import io.card.payment.BuildConfig;

/* renamed from: X.0Zl  reason: invalid class name and case insensitive filesystem */
public final class C05570Zl implements C05920aY {
    public static final ViewerContext A00;

    public ViewerContext Awn() {
        return null;
    }

    public ViewerContext B9S() {
        return null;
    }

    public void BxG() {
    }

    public void CA8(ViewerContext viewerContext) {
    }

    static {
        C05580Zo r1 = new C05580Zo();
        r1.A05 = "0";
        r1.A01 = BuildConfig.FLAVOR;
        A00 = new ViewerContext(r1);
    }

    public ViewerContext Asn() {
        return A00;
    }

    public ViewerContext Awc() {
        return A00;
    }

    public ViewerContext B9R() {
        return A00;
    }

    public C09830il Byt(ViewerContext viewerContext) {
        return C09830il.A00;
    }
}
