package X;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Set;

/* renamed from: X.1Ff  reason: invalid class name and case insensitive filesystem */
public abstract class C21101Ff<K, V> extends AbstractMap<K, V> {
    private transient Collection A00;
    private transient Set A01;
    private transient Set A02;

    /* JADX WARN: Type inference failed for: r0v1, types: [java.util.Set, X.B7R] */
    public Set A02() {
        if (this instanceof C21201Fq) {
            return new B7R((C21201Fq) this);
        }
        if (this instanceof C21121Fh) {
            return new B7g((C21121Fh) this);
        }
        if (!(this instanceof C21151Fk)) {
            return new C21211Fr((C21111Fg) this);
        }
        C21151Fk r2 = (C21151Fk) this;
        return C25011Xz.A09(r2.A01.entrySet(), r2.A00);
    }

    public Collection A01() {
        if (!(this instanceof C21091Fe)) {
            return new B7O(this);
        }
        C21091Fe r3 = (C21091Fe) this;
        return new B7Q(r3, r3.A01, r3.A00);
    }

    public Set A03() {
        if (this instanceof C21151Fk) {
            C21151Fk r2 = (C21151Fk) this;
            return C25011Xz.A09(r2.A01.keySet(), r2.A00);
        } else if (!(this instanceof C21111Fg)) {
            return new C33131n4(this);
        } else {
            return new C21161Fl((C21111Fg) this);
        }
    }

    public Set entrySet() {
        Set set = this.A01;
        if (set != null) {
            return set;
        }
        Set A022 = A02();
        this.A01 = A022;
        return A022;
    }

    public Set keySet() {
        Set set = this.A02;
        if (set != null) {
            return set;
        }
        Set A03 = A03();
        this.A02 = A03;
        return A03;
    }

    public Collection values() {
        Collection collection = this.A00;
        if (collection != null) {
            return collection;
        }
        Collection A012 = A01();
        this.A00 = A012;
        return A012;
    }
}
