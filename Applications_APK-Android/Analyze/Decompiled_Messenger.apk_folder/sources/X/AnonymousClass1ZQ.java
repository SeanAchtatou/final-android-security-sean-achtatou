package X;

import java.util.EnumSet;

/* renamed from: X.1ZQ  reason: invalid class name */
public abstract class AnonymousClass1ZQ {
    public EnumSet A00;
    public AnonymousClass1XV A01;
    public Integer A02;

    public void A00(AnonymousClass1ZW... r4) {
        if (r4 != null) {
            if (this.A00 == null) {
                this.A00 = EnumSet.noneOf(AnonymousClass1ZW.class);
            }
            for (AnonymousClass1ZW add : r4) {
                this.A00.add(add);
            }
        }
    }
}
