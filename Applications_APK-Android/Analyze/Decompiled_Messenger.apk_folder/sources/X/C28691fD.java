package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.JoinableInfo;

/* renamed from: X.1fD  reason: invalid class name and case insensitive filesystem */
public final class C28691fD implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new JoinableInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new JoinableInfo[i];
    }
}
