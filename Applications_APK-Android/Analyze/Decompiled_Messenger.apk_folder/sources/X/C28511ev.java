package X;

import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.model.threads.MarketplaceThreadUserData;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.card.payment.BuildConfig;

/* renamed from: X.1ev  reason: invalid class name and case insensitive filesystem */
public final class C28511ev {
    public AnonymousClass0UN A00;

    public static final C28511ev A00(AnonymousClass1XY r1) {
        return new C28511ev(r1);
    }

    public static MarketplaceThreadUserData A01(C28511ev r3, String str) {
        if (str == null || str.equals(BuildConfig.FLAVOR)) {
            return null;
        }
        JsonNode A02 = ((AnonymousClass0jD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A1l, r3.A00)).A02(str);
        C49472cH r2 = new C49472cH();
        r2.A08 = JSONUtil.A0N(A02.get("id"));
        r2.A03 = JSONUtil.A04(A02.get("good_ratings"));
        r2.A01 = JSONUtil.A04(A02.get("bad_ratings"));
        r2.A09 = JSONUtil.A0S(A02.get("are_ratings_private"));
        r2.A0A = JSONUtil.A0S(A02.get("are_seller_ratings_visible"));
        r2.A00 = JSONUtil.A02(A02.get("five_star_ratings_average"));
        r2.A02 = JSONUtil.A04(A02.get("five_star_total_rating_count_by_role"));
        r2.A04 = JSONUtil.A06(A02.get("join_time"));
        r2.A05 = JSONUtil.A0N(A02.get("commonality"));
        r2.A07 = JSONUtil.A0N(A02.get("first_name"));
        r2.A06 = JSONUtil.A0N(A02.get("current_city"));
        return new MarketplaceThreadUserData(r2);
    }

    public static String A02(MarketplaceThreadUserData marketplaceThreadUserData) {
        if (marketplaceThreadUserData == null) {
            return BuildConfig.FLAVOR;
        }
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        objectNode.put("id", marketplaceThreadUserData.A08);
        objectNode.put("good_ratings", marketplaceThreadUserData.A03);
        objectNode.put("bad_ratings", marketplaceThreadUserData.A01);
        objectNode.put("are_ratings_private", marketplaceThreadUserData.A09);
        objectNode.put("are_seller_ratings_visible", marketplaceThreadUserData.A0A);
        objectNode.put("five_star_ratings_average", marketplaceThreadUserData.A00);
        objectNode.put("five_star_total_rating_count_by_role", marketplaceThreadUserData.A02);
        objectNode.put("join_time", marketplaceThreadUserData.A04);
        objectNode.put("commonality", marketplaceThreadUserData.A05);
        objectNode.put("first_name", marketplaceThreadUserData.A07);
        objectNode.put("current_city", marketplaceThreadUserData.A06);
        return objectNode.toString();
    }

    public C28511ev(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
