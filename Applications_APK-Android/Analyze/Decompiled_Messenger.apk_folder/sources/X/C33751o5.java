package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1o5  reason: invalid class name and case insensitive filesystem */
public final class C33751o5 implements AnonymousClass0ZM {
    public final /* synthetic */ C33741o4 A00;

    public C33751o5(C33741o4 r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r7) {
        C193617v r4;
        ThreadKey A002 = C05690aA.A00(r7);
        if (A002 != null && (r4 = this.A00.A03) != null) {
            r4.BtY(null, AnonymousClass1FD.AUTOMATIC_REFRESH, ImmutableList.of(A002), "FbSharedPreferences");
        }
    }
}
