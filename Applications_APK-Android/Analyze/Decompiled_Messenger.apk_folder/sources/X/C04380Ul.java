package X;

import android.os.MessageQueue;

/* renamed from: X.0Ul  reason: invalid class name and case insensitive filesystem */
public abstract class C04380Ul implements MessageQueue.IdleHandler {
    private final String A00;

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0062, code lost:
        if (r4 > r1) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00f4, code lost:
        if (0 != 0) goto L_0x00f6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00() {
        /*
            r12 = this;
            boolean r0 = r12 instanceof X.C04370Uk
            if (r0 != 0) goto L_0x00de
            boolean r0 = r12 instanceof X.C05740aF
            if (r0 != 0) goto L_0x0099
            r0 = r12
            X.0Xv r0 = (X.C05150Xv) r0
            X.0YW r0 = r0.A00
            X.0Y6 r6 = r0.A00
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r6.A05
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.06B r0 = (X.AnonymousClass06B) r0
            long r4 = r0.now()
            int r0 = r6.A00
            r3 = 1
            r9 = 0
            if (r0 <= 0) goto L_0x004f
            java.util.concurrent.atomic.AtomicBoolean r0 = r6.A0K
            boolean r0 = r0.getAndSet(r9)
            if (r0 != 0) goto L_0x004f
            long r7 = r6.A04
            int r0 = r6.A00
            long r0 = (long) r0
            long r10 = r0 + r7
            int r2 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r2 > 0) goto L_0x004f
            int r2 = (r7 > r4 ? 1 : (r7 == r4 ? 0 : -1))
            if (r2 > 0) goto L_0x004f
            boolean r2 = r6.A0B
            if (r2 != 0) goto L_0x004e
            int r5 = X.AnonymousClass1Y3.Amu
            X.0UN r4 = r6.A05
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r3, r5, r4)
            android.os.Handler r4 = (android.os.Handler) r4
            r4.sendEmptyMessageDelayed(r9, r0)
            r6.A0B = r3
        L_0x004e:
            return r3
        L_0x004f:
            long r0 = r6.A03
            r7 = 0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x0064
            long r1 = r6.A04
            r7 = 500(0x1f4, double:2.47E-321)
            long r7 = r7 + r1
            int r0 = (r7 > r4 ? 1 : (r7 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x0064
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0066
        L_0x0064:
            r6.A03 = r4
        L_0x0066:
            r6.A04 = r4
            r6.A0B = r9
            java.util.concurrent.locks.ReentrantLock r0 = r6.A0O
            r0.lock()
            X.AnonymousClass0Y6.A07(r6)     // Catch:{ all -> 0x0092 }
            java.lang.Integer r1 = r6.A0S     // Catch:{ all -> 0x0092 }
            java.lang.Integer r0 = X.AnonymousClass07B.A0N     // Catch:{ all -> 0x0092 }
            if (r1 != r0) goto L_0x008c
            boolean r0 = r6.A0A     // Catch:{ all -> 0x0092 }
            if (r0 != 0) goto L_0x008c
            r6.A0A = r3     // Catch:{ all -> 0x0092 }
            boolean r0 = r6.A0C     // Catch:{ all -> 0x0092 }
            if (r0 == 0) goto L_0x0087
            java.util.concurrent.locks.Condition r0 = r6.A0N     // Catch:{ all -> 0x0092 }
            r0.signalAll()     // Catch:{ all -> 0x0092 }
        L_0x0087:
            java.util.concurrent.locks.Condition r0 = r6.A0M     // Catch:{ all -> 0x0092 }
            r0.signalAll()     // Catch:{ all -> 0x0092 }
        L_0x008c:
            java.util.concurrent.locks.ReentrantLock r0 = r6.A0O
            r0.unlock()
            return r3
        L_0x0092:
            r1 = move-exception
            java.util.concurrent.locks.ReentrantLock r0 = r6.A0O
            r0.unlock()
            throw r1
        L_0x0099:
            r4 = r12
            X.0aF r4 = (X.C05740aF) r4
            android.os.Message r1 = android.os.Message.obtain()
            r3 = 1
            r1.what = r3
            X.8sp r0 = r4.A00
            r0.A08(r1)
            X.8sp r2 = r4.A00
            boolean r1 = r2.A0H
            java.util.concurrent.atomic.AtomicInteger r0 = r2.A0F
            int r0 = r0.get()
            if (r1 == 0) goto L_0x00cd
            if (r0 > 0) goto L_0x00c6
            X.8sp r0 = r4.A00
            java.util.concurrent.Semaphore r0 = r0.A0D
            if (r0 == 0) goto L_0x00dc
            int r1 = r0.availablePermits()
            X.8sp r0 = r4.A00
            int r0 = r0.A09
            if (r1 >= r0) goto L_0x00dc
        L_0x00c6:
            r2.A02 = r3
            X.8sp r0 = r4.A00
            boolean r0 = r0.A02
            return r0
        L_0x00cd:
            if (r0 <= 0) goto L_0x00dc
            X.8sp r0 = r4.A00
            java.util.concurrent.Semaphore r0 = r0.A0D
            if (r0 == 0) goto L_0x00c6
            int r0 = r0.availablePermits()
            if (r0 <= 0) goto L_0x00dc
            goto L_0x00c6
        L_0x00dc:
            r3 = 0
            goto L_0x00c6
        L_0x00de:
            r6 = r12
            X.0Uk r6 = (X.C04370Uk) r6
            X.0Ud r7 = r6.A00
            int r2 = X.AnonymousClass1Y3.BT7
            X.0UN r1 = r7.A02
            r0 = 12
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0yu r0 = (X.C17440yu) r0
            boolean r0 = r0.A01
            if (r0 != 0) goto L_0x00f6
            r0 = 0
            if (r0 == 0) goto L_0x00f7
        L_0x00f6:
            r0 = 1
        L_0x00f7:
            r5 = 1
            if (r0 != 0) goto L_0x011b
            boolean r0 = r7.A0H()
            if (r0 == 0) goto L_0x011b
            r2 = 8
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r7.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            long r3 = r0.now()
            long r0 = r7.A0E
            long r3 = r3 - r0
            r1 = 60000(0xea60, double:2.9644E-319)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x011b
            r5 = 0
        L_0x011b:
            if (r5 == 0) goto L_0x01fd
            X.0Ud r3 = r6.A00
            java.lang.String r1 = "AppStateManager.announceAppEntry"
            r0 = -1195172801(0xffffffffb8c31c3f, float:-9.303586E-5)
            X.C005505z.A03(r1, r0)
            r1 = r3
            java.lang.String r0 = r3.A0S     // Catch:{ all -> 0x01f5 }
            if (r0 != 0) goto L_0x0140
            monitor-enter(r1)     // Catch:{ all -> 0x01f5 }
            java.lang.String r0 = r3.A0S     // Catch:{ all -> 0x013d }
            if (r0 != 0) goto L_0x013b
            java.util.UUID r0 = X.C188215g.A00()     // Catch:{ all -> 0x013d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x013d }
            r3.A0S = r0     // Catch:{ all -> 0x013d }
        L_0x013b:
            monitor-exit(r1)     // Catch:{ all -> 0x013d }
            goto L_0x0140
        L_0x013d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x013d }
            throw r0     // Catch:{ all -> 0x01f5 }
        L_0x0140:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f5 }
            r2.<init>()     // Catch:{ all -> 0x01f5 }
            java.lang.String r0 = r3.A0S     // Catch:{ all -> 0x01f5 }
            r2.append(r0)     // Catch:{ all -> 0x01f5 }
            r0 = 46
            r2.append(r0)     // Catch:{ all -> 0x01f5 }
            long r0 = r3.A08()     // Catch:{ all -> 0x01f5 }
            r2.append(r0)     // Catch:{ all -> 0x01f5 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x01f5 }
            r3.A05 = r0     // Catch:{ all -> 0x01f5 }
            r2 = 9
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ all -> 0x01f5 }
            long r0 = r0.now()     // Catch:{ all -> 0x01f5 }
            r3.A0K = r0     // Catch:{ all -> 0x01f5 }
            java.lang.Boolean r0 = r3.A04     // Catch:{ all -> 0x01f5 }
            if (r0 != 0) goto L_0x018d
            r2 = 17
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ all -> 0x01f5 }
            r0 = 285224483296781(0x103690002160d, double:1.4091961854976E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ all -> 0x01f5 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x01f5 }
            r3.A04 = r0     // Catch:{ all -> 0x01f5 }
        L_0x018d:
            java.lang.Boolean r0 = r3.A04     // Catch:{ all -> 0x01f5 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x01f5 }
            java.lang.String r5 = "app_foreground_report_time_spent_only"
            java.lang.String r4 = "app_foregrounded_immediate"
            r2 = 15
            if (r0 == 0) goto L_0x01d4
            int r1 = X.AnonymousClass1Y3.AdX     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.1YR r0 = (X.AnonymousClass1YR) r0     // Catch:{ all -> 0x01f5 }
            r0.A01(r5)     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.1YR r0 = (X.AnonymousClass1YR) r0     // Catch:{ all -> 0x01f5 }
            r0.A01(r4)     // Catch:{ all -> 0x01f5 }
        L_0x01b3:
            r2 = 4
            int r1 = X.AnonymousClass1Y3.AKb     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.0Ut r1 = (X.C04460Ut) r1     // Catch:{ all -> 0x01f5 }
            java.lang.String r0 = "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"
            r1.C4y(r0)     // Catch:{ all -> 0x01f5 }
            r2 = 13
            int r1 = X.AnonymousClass1Y3.ATT     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.1oC r1 = (X.C33821oC) r1     // Catch:{ all -> 0x01f5 }
            r0 = 1
            r1.A03(r0)     // Catch:{ all -> 0x01f5 }
            goto L_0x01ed
        L_0x01d4:
            int r1 = X.AnonymousClass1Y3.AdX     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.1YR r0 = (X.AnonymousClass1YR) r0     // Catch:{ all -> 0x01f5 }
            r0.A01(r4)     // Catch:{ all -> 0x01f5 }
            X.0UN r0 = r3.A02     // Catch:{ all -> 0x01f5 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x01f5 }
            X.1YR r0 = (X.AnonymousClass1YR) r0     // Catch:{ all -> 0x01f5 }
            r0.A01(r5)     // Catch:{ all -> 0x01f5 }
            goto L_0x01b3
        L_0x01ed:
            r0 = 823662428(0x3118175c, float:2.213219E-9)
            X.C005505z.A00(r0)
            r0 = 0
            return r0
        L_0x01f5:
            r1 = move-exception
            r0 = -1663604732(0xffffffff9cd76804, float:-1.4254394E-21)
            X.C005505z.A00(r0)
            throw r1
        L_0x01fd:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04380Ul.A00():boolean");
    }

    public C04380Ul(String str) {
        this.A00 = str;
    }

    public final boolean queueIdle() {
        try {
            return A00();
        } catch (Throwable th) {
            C010708t.A0T(this.A00, th, "Failure in IdleHandler");
            throw th;
        }
    }
}
