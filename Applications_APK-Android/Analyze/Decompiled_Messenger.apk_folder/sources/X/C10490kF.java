package X;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import java.io.Serializable;
import java.util.Map;

/* renamed from: X.0kF  reason: invalid class name and case insensitive filesystem */
public final class C10490kF extends C10460k9 implements Serializable {
    private static final long serialVersionUID = -4227480407273773599L;
    public final int _deserFeatures;
    public final JsonNodeFactory _nodeFactory;
    public final AnonymousClass2KN _problemHandlers;

    public C10140jc getAnnotationIntrospector() {
        if (isEnabled(C26771bz.USE_ANNOTATIONS)) {
            return super.getAnnotationIntrospector();
        }
        return CYm.instance;
    }

    public C10120ja introspectClassAnnotations(C10030jR r2) {
        return this._base._classIntrospector.forClassAnnotations(this, r2, this);
    }

    public final boolean isEnabled(AnonymousClass1c1 r3) {
        if ((r3.getMask() & this._deserFeatures) != 0) {
            return true;
        }
        return false;
    }

    public boolean useRootWrapping() {
        String str = this._rootName;
        if (str == null) {
            return isEnabled(AnonymousClass1c1.UNWRAP_ROOT_VALUE);
        }
        if (str.length() > 0) {
            return true;
        }
        return false;
    }

    public C10160je getDefaultVisibilityChecker() {
        C10160je defaultVisibilityChecker = super.getDefaultVisibilityChecker();
        if (!isEnabled(C26771bz.AUTO_DETECT_SETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withSetterVisibility(C10180jg.NONE);
        }
        if (!isEnabled(C26771bz.AUTO_DETECT_CREATORS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withCreatorVisibility(C10180jg.NONE);
        }
        if (!isEnabled(C26771bz.AUTO_DETECT_FIELDS)) {
            return defaultVisibilityChecker.withFieldVisibility(C10180jg.NONE);
        }
        return defaultVisibilityChecker;
    }

    public C10490kF(C10290jr r2, C10430k6 r3, Map map) {
        super(r2, r3, map);
        this._deserFeatures = C10470kA.collectFeatureDefaults(AnonymousClass1c1.class);
        this._nodeFactory = JsonNodeFactory.instance;
        this._problemHandlers = null;
    }

    public C10490kF(C10490kF r2, int i, int i2) {
        super(r2, i);
        this._deserFeatures = i2;
        this._nodeFactory = r2._nodeFactory;
        this._problemHandlers = r2._problemHandlers;
    }

    public C10490kF(C10490kF r2, C10290jr r3) {
        super(r2, r3);
        this._deserFeatures = r2._deserFeatures;
        this._nodeFactory = r2._nodeFactory;
        this._problemHandlers = r2._problemHandlers;
    }
}
