package X;

import java.util.List;

/* renamed from: X.1IW  reason: invalid class name */
public final class AnonymousClass1IW {
    public final int A00;
    public final int A01;
    public final int A02;
    public final List A03;
    public final List A04;

    public AnonymousClass1IW(int i, int i2, int i3, List list, List list2) {
        this.A02 = i;
        this.A00 = i2;
        this.A01 = i3;
        this.A03 = list;
        this.A04 = list2;
    }
}
