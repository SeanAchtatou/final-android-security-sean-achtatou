package X;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FJ  reason: invalid class name */
public final class AnonymousClass1FJ implements AnonymousClass1FI {
    private static volatile AnonymousClass1FJ A01;
    private final C32981me A00;

    public static final AnonymousClass1FJ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1FJ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1FJ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public ImmutableMap get() {
        HashMap hashMap = new HashMap();
        C24971Xv it = ImmutableSet.A0A(this.A00.A00).iterator();
        while (it.hasNext()) {
            AnonymousClass1F5 r2 = (AnonymousClass1F5) it.next();
            if (r2.A00) {
                hashMap.put(r2.A02, r2.A01);
            }
        }
        return ImmutableMap.copyOf(hashMap);
    }

    private AnonymousClass1FJ(AnonymousClass1XY r2) {
        this.A00 = C32981me.A00(r2);
    }
}
