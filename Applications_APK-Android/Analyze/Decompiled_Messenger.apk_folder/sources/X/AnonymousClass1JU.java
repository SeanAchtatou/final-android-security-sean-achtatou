package X;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.ipc.stories.model.StoryBucket;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.ipc.stories.model.ViewerInfo;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.montage.forked.viewer.overlays.slider.model.FbSliderVoteModel;
import com.facebook.resources.ui.FbFrameLayout;
import com.facebook.user.model.User;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.Map;
import java.util.concurrent.Executor;

/* renamed from: X.1JU  reason: invalid class name */
public final class AnonymousClass1JU extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public StoryBucket A02;
    @Comparable(type = 13)
    public StoryCard A03;
    @Comparable(type = 13)
    public C93514cr A04;
    @Comparable(type = 13)
    public AnonymousClass463 A05;
    @LoggedInUser
    public C04310Tq A06;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1JU(Context context) {
        super("FbSliderStickerComponent");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A01 = new AnonymousClass0UN(4, r2);
        this.A06 = AnonymousClass0WY.A01(r2);
    }

    public static FbSliderVoteModel A00(float f, User user) {
        C1292163w r3 = new C1292163w();
        r3.A00 = f;
        C1291363k r2 = new C1291363k();
        String str = user.A0j;
        r2.A02 = str;
        C28931fb.A06(str, "id");
        String A09 = user.A09();
        r2.A03 = A09;
        C28931fb.A06(A09, "name");
        r2.A04 = user.A0D();
        r3.A01 = new ViewerInfo(r2);
        return new FbSliderVoteModel(r3);
    }

    public void A0j(AnonymousClass0p4 r33, Object obj) {
        int i;
        int i2;
        boolean z;
        String str;
        int i3;
        DFl dFl;
        int parseColor;
        String A3y;
        FbFrameLayout fbFrameLayout = (FbFrameLayout) obj;
        StoryCard storyCard = this.A03;
        C93514cr r22 = this.A04;
        AnonymousClass463 r28 = this.A05;
        int i4 = this.A00;
        StoryBucket storyBucket = this.A02;
        int i5 = AnonymousClass1Y3.A1m;
        AnonymousClass0UN r3 = this.A01;
        DFl dFl2 = (DFl) AnonymousClass1XX.A02(1, i5, r3);
        C95644go r12 = (C95644go) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AM1, r3);
        C11170mD r9 = (C11170mD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ASp, r3);
        Executor executor = (Executor) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A6Z, r3);
        C04310Tq r21 = this.A06;
        ImageView imageView = (ImageView) fbFrameLayout.getChildAt(0);
        FbFrameLayout fbFrameLayout2 = (FbFrameLayout) fbFrameLayout.getChildAt(1);
        View childAt = fbFrameLayout.getChildAt(2);
        C73513gF r6 = (C73513gF) AnonymousClass0j4.A02(C847340c.A00(storyCard.A0T(), C99084oO.$const$string(127)));
        DFx A002 = DFx.A00();
        GSTModelShape1S0000000 B3G = r6.B3G();
        boolean z2 = true;
        boolean z3 = false;
        if (B3G != null) {
            z3 = true;
        }
        Preconditions.checkArgument(z3);
        Map map = A002.A01;
        String A3m = B3G.A3m();
        if (map.containsKey(A3m) && B3G.getIntValue(-1085479528) <= ((C26877DFs) A002.A01.get(A3m)).A04) {
            z2 = false;
        }
        if (z2) {
            DFx A003 = DFx.A00();
            GSTModelShape1S0000000 B3G2 = r6.B3G();
            String A3m2 = B3G2.A3m();
            A003.A00.put(A3m2, r6.B9h());
            Map map2 = A003.A01;
            DFt dFt = new DFt();
            String Adv = r6.Adv();
            int i6 = 0;
            if (Adv == null) {
                parseColor = 0;
            } else {
                parseColor = Color.parseColor(AnonymousClass08S.A0J("#", Adv));
            }
            dFt.A02 = parseColor;
            dFt.A08 = B3G2.getBooleanValue(-282985720);
            dFt.A05 = r6.Al8();
            dFt.A06 = A3m2;
            GSTModelShape1S0000000 Azs = r6.Azs();
            if (Azs == null) {
                A3y = null;
            } else {
                A3y = Azs.A3y();
            }
            dFt.A07 = A3y;
            String Azt = r6.Azt();
            if (Azt != null) {
                i6 = Color.parseColor(AnonymousClass08S.A0J("#", Azt));
            }
            dFt.A03 = i6;
            dFt.A00 = ((float) B3G2.getDoubleValue(1124422903)) / 100.0f;
            dFt.A01 = ((float) B3G2.getDoubleValue(-1058048922)) / 100.0f;
            dFt.A04 = B3G2.getIntValue(-1085479528);
            map2.put(A3m2, new C26877DFs(dFt));
        }
        GSTModelShape1S0000000 B3G3 = r6.B3G();
        Preconditions.checkNotNull(B3G3);
        String A3m3 = B3G3.A3m();
        Object obj2 = DFx.A00().A01.get(A3m3);
        Preconditions.checkNotNull(obj2);
        C26877DFs dFs = (C26877DFs) obj2;
        C26877DFs dFs2 = dFs;
        View rootView = fbFrameLayout.getRootView();
        Object obj3 = DFx.A00().A00.get(A3m3);
        Preconditions.checkNotNull(obj3);
        C95664gq A004 = C95244g5.A00(rootView, (GSTModelShape1S0000000) obj3, r12, storyCard);
        ImageView imageView2 = imageView;
        C95644go.A03(imageView2, A004);
        C95644go.A03(fbFrameLayout2, A004);
        DFp dFp = new DFp(imageView2.getContext());
        dFp.A04 = dFs.A05;
        User user = (User) r21.get();
        dFl2.A04 = dFs;
        if (dFs == null) {
            i = -1;
        } else {
            i = dFs.A02;
        }
        if (dFs == null) {
            i2 = C15320v6.MEASURED_STATE_MASK;
        } else {
            i2 = dFs.A03;
        }
        AnonymousClass6Lu A005 = C133326Lt.A00(i);
        int i7 = A005.A02;
        if (i7 == 0) {
            z = false;
        } else {
            dFl2.A09.A05 = i7;
            DisplayMetrics displayMetrics = dFl2.A02.getResources().getDisplayMetrics();
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{A005.A01, A005.A00});
            gradientDrawable.setCornerRadius((((float) displayMetrics.densityDpi) / 160.0f) * 10.0f);
            dFl2.A03 = gradientDrawable;
            z = true;
        }
        if (!z) {
            dFl2.A03.mutate().setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.SRC));
        }
        DFl dFl3 = dFl2;
        C25922Cor cor = dFl3.A0A;
        C26877DFs dFs3 = dFl3.A04;
        if (dFs3 == null || (str = dFs3.A07) == null) {
            str = BuildConfig.FLAVOR;
        }
        cor.A00.A01(str);
        cor.invalidateSelf();
        C25922Cor cor2 = dFl2.A0A;
        C25921Coq coq = cor2.A00;
        coq.A0A.setColor(i2);
        C25921Coq.A00(coq);
        coq.invalidateSelf();
        cor2.invalidateSelf();
        C26873DFm dFm = dFl2.A09;
        C26875DFq dFq = dFm.A0K;
        dFq.A01 = i;
        dFq.invalidateSelf();
        int i8 = dFm.A05;
        int i9 = dFm.A0D;
        if (i8 == i9) {
            if (i != -1) {
                if (((double) (((((((float) Color.red(i)) / 255.0f) * 299.0f) + ((((float) Color.green(i)) / 255.0f) * 587.0f)) + ((((float) Color.blue(i)) / 255.0f) * 114.0f)) / 1000.0f)) < 0.25d) {
                    i9 = 16777215 | (((int) (0.25f * 255.0f)) << 24);
                } else {
                    i9 = Color.rgb(Math.max(Color.red(i) - 30, 0), Math.max(Color.green(i) - 30, 0), Math.max(Color.blue(i) - 30, 0));
                }
            }
            dFm.A05 = i9;
        }
        dFm.A0G.setColor(dFm.A05);
        if (i2 == -1) {
            i3 = -1;
        } else {
            i3 = dFm.A0F;
        }
        dFm.A07 = i3;
        if (i2 != -1) {
            i2 = dFm.A0E;
        }
        dFm.A06 = i2;
        C26873DFm.A02(dFm, dFm.getBounds());
        dFm.invalidateSelf();
        DFl.A01(dFl2);
        boolean z4 = false;
        if (dFs2.A08) {
            String str2 = dFs2.A06;
            if (((FbSliderVoteModel) DFx.A00().A02.get(str2)) == null && i4 != 13) {
                z4 = true;
            }
            dFl2.A09.A09 = z4;
            dFl = dFl2;
            dFl.A05 = (FbSliderVoteModel) DFx.A00().A02.get(str2);
            DFl.A01(dFl);
        } else {
            dFl2.A09.A09 = false;
            FbSliderVoteModel A006 = A00(dFs2.A00, user);
            dFl = dFl2;
            dFl.A05 = A006;
            DFl.A01(dFl);
            DFx.A00().A02.remove(dFs2.A06);
        }
        int height = (int) A004.A01.height();
        dFl.A01 = 0;
        dFl.A00 = ((float) height) / ((float) dFl.getIntrinsicHeight());
        dFl.A01 = height;
        dFl.invalidateSelf();
        View childAt2 = fbFrameLayout2.getChildAt(0);
        dFl.A09.A08 = new C26874DFo(imageView, r22, storyBucket, storyCard.getId(), (User) r21.get(), r9, dFl, r28, executor, childAt2, dFp);
        ImageView imageView3 = imageView;
        imageView3.setOnTouchListener(new C26879DFv(dFl));
        imageView3.setImageDrawable(dFl);
        C15320v6.setBackground(childAt, dFp);
    }
}
