package X;

import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1en  reason: invalid class name and case insensitive filesystem */
public final class C28431en {
    public static final C28041eA A02 = new C28041eA("/threads_migration/");
    private static volatile C28431en A03;
    public boolean A00 = false;
    public final Set A01;

    public static final C28431en A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C28431en.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C28431en(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C28431en(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0X5(r3, AnonymousClass0X6.A1L);
        new AnonymousClass15y(r3);
    }
}
