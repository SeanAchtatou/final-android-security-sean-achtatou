package X;

import android.util.Log;
import com.facebook.acra.LogCatCollector;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/* renamed from: X.0HL  reason: invalid class name */
public final class AnonymousClass0HL {
    private static FileChannel A00;
    private static final String A01 = "TraceDirect";

    static {
        try {
            A00 = new FileOutputStream("/sys/kernel/debug/tracing/trace_marker").getChannel();
        } catch (FileNotFoundException e) {
            Log.e(A01, "Failed to open trace_marker file.", e);
            A00 = null;
        }
    }

    public static void A00(String str) {
        int write;
        if (A00 != null) {
            try {
                byte[] bytes = str.getBytes(Charset.forName(LogCatCollector.UTF_8_ENCODING));
                int length = bytes.length;
                if (length >= 1) {
                    do {
                        write = A00.write(ByteBuffer.wrap(bytes));
                    } while (write == 0);
                    if (write != length) {
                        Log.e(A01, "Partial write of systrace line.");
                    }
                }
            } catch (UnsupportedEncodingException e) {
                Log.e(A01, "Failed to encode raw systrace line to UTF-8.", e);
            } catch (IOException e2) {
                Log.e(A01, "Failed to write systrace line.", e2);
            }
        }
    }
}
