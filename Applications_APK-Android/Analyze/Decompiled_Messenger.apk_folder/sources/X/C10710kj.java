package X;

import com.facebook.common.connectionstatus.FbDataConnectionManager;
import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

@InjectorModule
/* renamed from: X.0kj  reason: invalid class name and case insensitive filesystem */
public final class C10710kj extends AnonymousClass0UV {
    public static final Class A00 = C10710kj.class;

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0183, code lost:
        if (r10.A02() != false) goto L_0x0185;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.facebook.proxygen.HTTPClient.Builder A01(X.AnonymousClass1XY r46) {
        /*
            r6 = r46
            X.1Yd r0 = X.AnonymousClass0WT.A00(r6)
            X.1WD r5 = X.C26251b9.A09(r6)
            X.C31581jv.A00(r6)
            android.content.Context r8 = X.AnonymousClass1YA.A00(r6)
            android.content.Context r3 = X.AnonymousClass1YA.A00(r6)
            android.content.Context r2 = X.AnonymousClass1YA.A00(r6)
            X.09t r1 = new X.09t
            r1.<init>(r2)
            X.1jw r4 = new X.1jw
            r4.<init>(r3, r1)
            X.1jx r3 = X.C31601jx.A00(r6)
            X.C06920cI.A00(r6)
            X.C31631k0.A00(r6)
            com.facebook.prefs.shared.FbSharedPreferencesModule.A00(r6)
            X.0zg r17 = X.C17900zf.A00(r6)
            X.09P r16 = X.C04750Wa.A01(r6)
            X.0iw r10 = new X.0iw
            r10.<init>(r6)
            com.facebook.proxygen.HTTPClient$Builder r9 = new com.facebook.proxygen.HTTPClient$Builder
            r9.<init>()
            java.io.File r6 = new java.io.File
            java.io.File r2 = r8.getFilesDir()
            java.lang.String r1 = "fbtlsx.store"
            r6.<init>(r2, r1)
            com.facebook.proxygen.PersistentSSLCacheSettings$Builder r2 = new com.facebook.proxygen.PersistentSSLCacheSettings$Builder
            java.lang.String r1 = r6.toString()
            r2.<init>(r1)
            r1 = 50
            r2.cacheCapacity = r1
            r1 = 150(0x96, float:2.1E-43)
            r2.syncInterval = r1
            r6 = 1
            r2.enableCrossDomainTickets = r6
            com.facebook.proxygen.PersistentSSLCacheSettings r1 = r2.build()
            r9.mPersistentSSLCacheSettings = r1
            boolean r2 = r3.A02
            r9.setIsSandbox(r2)
            r3 = 1
            r2 = r2 ^ r6
            r9.mIsHTTPSEnforced = r2
            r1 = 563353683689854(0x2005e0033017e, double:2.78333701569273E-309)
            long r1 = r0.At0(r1)
            int r6 = (int) r1
            r9.mMaxIdleHTTPSessions = r6
            r1 = 563353687425435(0x2005e006c019b, double:2.783337034148953E-309)
            long r1 = r0.At0(r1)
            int r6 = (int) r1
            r9.mMaxIdleHTTP2Sessions = r6
            r1 = 281878707372835(0x1005e00390323, double:1.392665856070555E-309)
            boolean r2 = r0.Aem(r1)
            r9.mIsPerDomainLimitEnabled = r2
            r1 = 563353684148611(0x2005e003a0183, double:2.78333701795929E-309)
            long r1 = r0.At0(r1)
            int r6 = (int) r1
            r9.mPerDomainMaxConns = r6
            java.io.File r6 = new java.io.File
            java.io.File r2 = r8.getFilesDir()
            java.lang.String r1 = "fbdns.store"
            r6.<init>(r2, r1)
            com.facebook.proxygen.PersistentSSLCacheSettings$Builder r2 = new com.facebook.proxygen.PersistentSSLCacheSettings$Builder
            java.lang.String r1 = r6.toString()
            r2.<init>(r1)
            r1 = 200(0xc8, float:2.8E-43)
            r2.cacheCapacity = r1
            r1 = 150(0x96, float:2.1E-43)
            r2.syncInterval = r1
            com.facebook.proxygen.PersistentSSLCacheSettings r2 = r2.build()
            r9.mPersistentDNSCacheSettings = r2
            r1 = 282046207362084(0x1008500000424, double:1.39349341597422E-309)
            boolean r2 = r0.Aeo(r1, r3)
            r9.mDnsCacheEnabled = r2
            r1 = 563521184727530(0x20085000a01ea, double:2.784164580776315E-309)
            int r2 = r0.AqO(r1, r3)
            r9.mDnsRequestsOutstanding = r2
            r1 = 282046208148521(0x10085000c0429, double:1.393493419859737E-309)
            boolean r1 = r0.Aem(r1)
            if (r1 == 0) goto L_0x0521
            r3 = 1546329600000(0x168086d3000, double:7.639883325074E-312)
        L_0x00e7:
            r1 = 282046208082984(0x10085000b0428, double:1.39349341953594E-309)
            r7 = 0
            boolean r6 = r0.Aeo(r1, r7)
            r1 = 282046207820838(0x1008500070426, double:1.39349341824077E-309)
            boolean r7 = r0.Aeo(r1, r7)
            r1 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r1
            com.facebook.proxygen.SSLVerificationSettings r2 = new com.facebook.proxygen.SSLVerificationSettings
            r2.<init>(r6, r7, r3)
            r9.mSSLVerificationSettings = r2
            r1 = 563353682903414(0x2005e00270176, double:2.7833370118072E-309)
            long r1 = r0.At0(r1)
            int r3 = (int) r1
            long r1 = (long) r3
            r9.mHappyEyeballsConnectionDelayMillis = r1
            java.io.File r3 = new java.io.File
            java.lang.String r2 = "preconnection"
            r1 = 0
            java.io.File r2 = r8.getDir(r2, r1)
            java.lang.String r1 = "preconnection_data"
            r3.<init>(r2, r1)
            java.lang.String r2 = r3.getPath()
            r9.mPreconnectFilePath = r2
            X.1Yd r3 = r5.A00
            r1 = 282342560171358(0x100ca0001055e, double:1.39495759339533E-309)
            boolean r2 = r3.Aem(r1)
            r9.mEnableLigerPreconnect = r2
            r1 = 0
            r9.mUseInjectedPreconnect = r1
            r1 = 281878709273396(0x1005e00560334, double:1.392665865460574E-309)
            boolean r2 = r0.Aem(r1)
            r9.mEnableHTTP2Weights = r2
            r1 = 563353683493244(0x2005e0030017c, double:2.78333701472135E-309)
            long r1 = r0.At0(r1)
            int r3 = (int) r1
            r9.mHTTP2WeightLowPri = r3
            r1 = 563353683034487(0x2005e00290177, double:2.783337012454787E-309)
            long r1 = r0.At0(r1)
            int r3 = (int) r1
            r9.mHTTP2WeightHighPri = r3
            r1 = 563353682182514(0x2005e001c0172, double:2.78333700824548E-309)
            long r1 = r0.At0(r1)
            int r3 = (int) r1
            r9.mEvbLoggingSamplingFreq = r3
            r1 = 563353683165561(0x2005e002b0179, double:2.78333701310238E-309)
            r3 = 4000(0xfa0, float:5.605E-42)
            int r2 = r0.AqL(r1, r3)
            r9.mHTTPSessionMaxReadBufferSize = r2
            r1 = 2306124887919690525(0x2001005e0024031d, double:1.5850311273281156E-154)
            r3 = 1
            boolean r1 = r0.Aeo(r1, r3)
            r7 = 0
            if (r1 == 0) goto L_0x0185
            boolean r1 = r10.A02()
            r6 = 1
            if (r1 == 0) goto L_0x0186
        L_0x0185:
            r6 = 0
        L_0x0186:
            com.facebook.proxygen.FizzSettings$Builder r5 = new com.facebook.proxygen.FizzSettings$Builder
            r5.<init>()
            r5.enabled = r6
            r5.sendEarlyData = r3
            r5.compatMode = r3
            r1 = 5
            r5.maxPskUses = r1
            r5.useJavaCrypto = r7
            r1 = 2306124887919493915(0x2001005e0021031b, double:1.585031127262995E-154)
            boolean r1 = r0.Aeo(r1, r7)
            r5.enableCertCompression = r1
            r1 = 844828659351614(0x3005e0023003e, double:4.174008172077496E-309)
            java.lang.String r3 = "zstd"
            java.lang.String r1 = r0.B4E(r1, r3)
            r5.preferredCompressionAlgorithm = r1
            r3 = 563353682772341(0x2005e00250175, double:2.783337011159614E-309)
            r1 = -1
            long r1 = r0.At1(r3, r1)
            int r3 = (int) r1
            r5.clientTestEnum = r3
            r1 = 281878710125368(0x1005e00630338, double:1.392665869669875E-309)
            boolean r1 = r0.Aeo(r1, r7)
            r5.enableDelegatedCredentials = r1
            if (r6 == 0) goto L_0x051d
            java.io.File r3 = new java.io.File
            java.io.File r2 = r8.getFilesDir()
            java.lang.String r1 = "fbfizz.store"
            r3.<init>(r2, r1)
            r1 = 1
            r5.persistentCacheEnabled = r1
            com.facebook.proxygen.PersistentSSLCacheSettings$Builder r2 = new com.facebook.proxygen.PersistentSSLCacheSettings$Builder
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            r1 = 30
            r2.cacheCapacity = r1
            r1 = 150(0x96, float:2.1E-43)
            r2.syncInterval = r1
            com.facebook.proxygen.PersistentSSLCacheSettings r2 = r2.build()
            r5.cacheSettings = r2
        L_0x01ee:
            com.facebook.proxygen.FizzSettings r2 = r5.build()
            r9.mFizzSettings = r2
            java.io.File r5 = new java.io.File
            java.io.File r2 = r8.getFilesDir()
            java.lang.String r1 = "fbquic-fizz.store"
            r5.<init>(r2, r1)
            com.facebook.proxygen.FizzSettings$Builder r3 = new com.facebook.proxygen.FizzSettings$Builder
            r3.<init>()
            r1 = 1
            r3.enabled = r1
            r1 = 281878707831592(0x1005e00400328, double:1.392665858337115E-309)
            boolean r1 = r0.Aeo(r1, r7)
            r3.sendEarlyData = r1
            r3.compatMode = r7
            r1 = 5
            r3.maxPskUses = r1
            r1 = 1
            r3.persistentCacheEnabled = r1
            com.facebook.proxygen.PersistentSSLCacheSettings$Builder r2 = new com.facebook.proxygen.PersistentSSLCacheSettings$Builder
            java.lang.String r1 = r5.toString()
            r2.<init>(r1)
            r1 = 30
            r2.cacheCapacity = r1
            r1 = 150(0x96, float:2.1E-43)
            r2.syncInterval = r1
            com.facebook.proxygen.PersistentSSLCacheSettings r1 = r2.build()
            r3.cacheSettings = r1
            com.facebook.proxygen.FizzSettings r2 = r3.build()
            r9.mQuicFizzSettings = r2
            r2 = 844828658499645(0x3005e0016003d, double:4.17400816786821E-309)
            java.lang.String r4 = ""
            java.lang.String r19 = r0.B4E(r2, r4)
            r2 = 281878705144596(0x1005e00170314, double:1.39266584506159E-309)
            boolean r20 = r0.Aeo(r2, r7)
            r2 = 281878705210133(0x1005e00180315, double:1.392665845385387E-309)
            boolean r22 = r0.Aeo(r2, r7)
            r2 = 281878705275670(0x1005e00190316, double:1.392665845709183E-309)
            boolean r23 = r0.Aeo(r2, r7)
            r2 = 563353683820928(0x2005e00350180, double:2.78333701634032E-309)
            long r2 = r0.At0(r2)
            int r5 = (int) r2
            r24 = r5
            r2 = 281878704161548(0x1005e0008030c, double:1.39266584020469E-309)
            boolean r25 = r0.Aeo(r2, r7)
            r5 = 563353683886465(0x2005e00360181, double:2.78333701666412E-309)
            r2 = 20
            long r2 = r0.At1(r5, r2)
            int r5 = (int) r2
            r26 = r5
            r2 = 2306124887922574127(0x2001005e0050032f, double:1.5850311282832132E-154)
            boolean r27 = r0.Aeo(r2, r7)
            r2 = 281878705341207(0x1005e001a0317, double:1.39266584603298E-309)
            r4 = 1
            boolean r28 = r0.Aeo(r2, r4)
            r4 = 563353685262731(0x2005e004b018b, double:2.783337023463775E-309)
            r2 = 0
            long r2 = r0.At1(r4, r2)
            int r4 = (int) r2
            r33 = r4
            r4 = 563353683624317(0x2005e0032017d, double:2.783337015368935E-309)
            r2 = 2000(0x7d0, double:9.88E-321)
            long r2 = r0.At1(r4, r2)
            int r4 = (int) r2
            r34 = r4
            r2 = 281878707307298(0x1005e00380322, double:1.39266585574676E-309)
            boolean r36 = r0.Aem(r2)
            r2 = 563353686049165(0x2005e0057018d, double:2.783337027349275E-309)
            long r2 = r0.At0(r2)
            int r15 = (int) r2
            r2 = 281878704227085(0x1005e0009030d, double:1.392665840528485E-309)
            boolean r38 = r0.Aem(r2)
            r2 = 281878709011248(0x1005e00520330, double:1.39266586416539E-309)
            boolean r39 = r0.Aem(r2)
            r2 = 563353686442385(0x2005e005d0191, double:2.78333702929204E-309)
            long r2 = r0.At0(r2)
            int r14 = (int) r2
            r2 = 563353686507922(0x2005e005e0192, double:2.783337029615836E-309)
            long r2 = r0.At0(r2)
            int r13 = (int) r2
            r2 = 563353686573459(0x2005e005f0193, double:2.78333702993963E-309)
            long r2 = r0.At0(r2)
            int r12 = (int) r2
            r2 = 563353686638996(0x2005e00600194, double:2.78333703026343E-309)
            long r2 = r0.At0(r2)
            int r11 = (int) r2
            r2 = 563353686704533(0x2005e00610195, double:2.783337030587224E-309)
            long r2 = r0.At0(r2)
            int r8 = (int) r2
            r2 = 281878711108413(0x1005e0072033d, double:1.39266587452676E-309)
            boolean r41 = r0.Aem(r2)
            r2 = 563353687884191(0x2005e0073019f, double:2.78333703641551E-309)
            long r2 = r0.At0(r2)
            int r7 = (int) r2
            r2 = 563353687687581(0x2005e0070019d, double:2.783337035444126E-309)
            long r2 = r0.At0(r2)
            int r6 = (int) r2
            r2 = 563353687753118(0x2005e0071019e, double:2.78333703576792E-309)
            long r2 = r0.At0(r2)
            int r5 = (int) r2
            r2 = 563353687622044(0x2005e006f019c, double:2.78333703512033E-309)
            long r2 = r0.At0(r2)
            int r4 = (int) r2
            r2 = 281878710846268(0x1005e006e033c, double:1.392665873231594E-309)
            boolean r46 = r0.Aem(r2)
            com.facebook.proxygen.QuicSettings r2 = new com.facebook.proxygen.QuicSettings
            r21 = 0
            r18 = r2
            r40 = 0
            r29 = r13
            r30 = r12
            r31 = r11
            r32 = r8
            r35 = r14
            r37 = r15
            r42 = r7
            r43 = r6
            r44 = r5
            r45 = r4
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46)
            r9.mQuicSettings = r2
            r2 = 2306124887918510864(0x2001005e00120310, double:1.585031126937392E-154)
            boolean r2 = r0.Aem(r2)
            r9.mEnableBackgroundQuicConnection = r2
            r2 = 281878707634981(0x1005e003d0325, double:1.39266585736573E-309)
            boolean r2 = r0.Aem(r2)
            r9.mEnablePreconnectWithQuic = r2
            r2 = 281878704423694(0x1005e000c030e, double:1.39266584149986E-309)
            boolean r2 = r0.Aem(r2)
            r9.mDisableTcpPreconnectWithQuic = r2
            r2 = 281878703768330(0x1005e0002030a, double:1.392665838261934E-309)
            boolean r2 = r0.Aem(r2)
            r9.mAttemptEarlyDataInQuicPreconnect = r2
            r2 = 563353684607364(0x2005e00410184, double:2.78333702022583E-309)
            long r5 = r0.At0(r2)
            r2 = 563353684672901(0x2005e00420185, double:2.78333702054963E-309)
            r4 = 0
            int r2 = r0.AqL(r2, r4)
            r9.mQuicTraceLoggingSampleSalt = r5
            r9.mQuicTraceLoggingSampleWeight = r2
            r2 = 281878704882449(0x1005e00130311, double:1.392665843766413E-309)
            boolean r2 = r0.Aem(r2)
            r9.mEnableHTTP3 = r2
            r2 = 563353684803975(0x2005e00440187, double:2.78333702119722E-309)
            long r2 = r0.At0(r2)
            int r5 = (int) r2
            r9.mIdleTimeoutForUnusedQuicSession = r5
            r2 = 1126303633965095(0x4005e00030027, double:5.564679323283076E-309)
            double r2 = r0.Aki(r2)
            r9.mBackgroundDNSSampleRate = r2
            r2 = 563353684869512(0x2005e00450188, double:2.783337021521015E-309)
            long r2 = r0.At0(r2)
            int r5 = (int) r2
            r9.mIdleTimeoutForUsedQuicSession = r5
            r2 = 281878706520863(0x1005e002c031f, double:1.392665851861254E-309)
            boolean r2 = r0.Aem(r2)
            r9.mInlinePersistenceLoading = r2
            r9.mSupportH2PubSub = r4
            java.lang.String r2 = ""
            r9.mH2PubSubHostnames = r2
            r2 = 2306124887924015929(0x2001005e00660339, double:1.5850311287607623E-154)
            boolean r5 = r0.Aem(r2)
            r2 = 563353686966679(0x2005e00650197, double:2.783337031882397E-309)
            long r2 = r0.At0(r2)
            int r7 = (int) r2
            r2 = 563353686901142(0x2005e00640196, double:2.7833370315586E-309)
            long r2 = r0.At0(r2)
            int r6 = (int) r2
            r9.setFlowControl(r5, r7, r6, r4)
            java.lang.String r2 = ""
            r9.mPreConnects = r2
            r2 = 281878705013523(0x1005e00150313, double:1.392665844414005E-309)
            boolean r2 = r0.Aem(r2)
            r9.mEnableLigerWorkerThread = r2
            r2 = 283764194282968(0x1021500000dd8, double:1.401981399150355E-309)
            boolean r2 = r0.Aem(r2)
            r9.mEnableZstd = r2
            r2 = 283764194348505(0x1021500010dd9, double:1.40198139947415E-309)
            boolean r2 = r0.Aem(r2)
            r9.mEnableLithiumZstd = r2
            r2 = 281878709470006(0x1005e00590336, double:1.392665866431956E-309)
            boolean r2 = r0.Aem(r2)
            r9.mEnableDzCompression = r2
            r2 = 563353687163288(0x2005e00680198, double:2.783337032853774E-309)
            long r2 = r0.At0(r2)
            int r5 = (int) r2
            r2 = 563353687228825(0x2005e00690199, double:2.78333703317757E-309)
            long r2 = r0.At0(r2)
            int r4 = (int) r2
            r2 = 563353687294362(0x2005e006a019a, double:2.783337033501366E-309)
            long r2 = r0.At0(r2)
            int r6 = (int) r2
            r2 = 844828663808066(0x3005e00670042, double:4.174008194095295E-309)
            java.lang.String r22 = r0.B4C(r2)
            r2 = 281878710649658(0x1005e006b033a, double:1.39266587226021E-309)
            boolean r23 = r0.Aem(r2)
            r18 = r9
            r19 = r5
            r20 = r4
            r21 = r6
            r18.configurePingTimeout(r19, r20, r21, r22, r23)
            r2 = 281878711239486(0x1005e0074033e, double:1.39266587517435E-309)
            r4 = 0
            boolean r5 = r0.Aeo(r2, r4)
            r2 = 281878711305023(0x1005e0075033f, double:1.392665875498145E-309)
            boolean r6 = r0.Aeo(r2, r4)
            r2 = 281878711370560(0x1005e00760340, double:1.39266587582194E-309)
            boolean r7 = r0.Aeo(r2, r4)
            r2 = 563353688146336(0x2005e007701a0, double:2.783337037710677E-309)
            r4 = -1
            int r3 = r0.AqL(r2, r4)
            com.facebook.proxygen.ConnQualityConfig r2 = new com.facebook.proxygen.ConnQualityConfig
            r2.<init>(r5, r6, r7, r3)
            r9.mConnQualityConfig = r2
            r2 = 281878703702793(0x1005e00010309, double:1.39266583793814E-309)
            boolean r2 = r0.Aem(r2)
            if (r2 == 0) goto L_0x04d8
            com.facebook.prefs.shared.FbSharedPreferences r2 = r10.A00
            boolean r2 = r2.BFQ()
            r5 = 0
            if (r2 == 0) goto L_0x04b7
            com.facebook.prefs.shared.FbSharedPreferences r3 = r10.A00
            X.1Y7 r2 = X.C25951af.A06
            java.lang.String r5 = r3.B4F(r2, r5)
        L_0x04b7:
            if (r5 == 0) goto L_0x04bf
            boolean r2 = r5.isEmpty()
            if (r2 == 0) goto L_0x04ed
        L_0x04bf:
            r2 = 281878708290345(0x1005e00470329, double:1.392665860603656E-309)
            boolean r2 = r0.Aem(r2)
            if (r2 == 0) goto L_0x04d3
            r2 = 844828661645377(0x3005e00460041, double:4.17400818341019E-309)
            java.lang.String r5 = r0.B4C(r2)
        L_0x04d3:
            if (r5 != 0) goto L_0x04ed
            r4 = 0
        L_0x04d6:
            r9.mCdnOverride = r4
        L_0x04d8:
            X.10g r4 = new X.10g
            r3 = r17
            r2 = r16
            r4.<init>(r3, r2)
            r9.mRootCACallbacks = r4
            boolean r2 = r10.A01()
            if (r2 == 0) goto L_0x057a
            java.lang.Class<X.0zg> r8 = X.C17910zg.class
            monitor-enter(r8)
            goto L_0x0529
        L_0x04ed:
            r4 = 0
            if (r5 == 0) goto L_0x04d6
            boolean r2 = r5.isEmpty()
            if (r2 != 0) goto L_0x04d6
            java.util.Locale r2 = java.util.Locale.ROOT
            java.lang.String r3 = r5.toLowerCase(r2)
            java.lang.String r2 = "^[a-z]{3}([0-9]+-[0-9]+)?$"
            boolean r2 = r3.matches(r2)
            if (r2 != 0) goto L_0x051b
            java.lang.String r2 = "^fna:f[a-z]{3}[0-9]+-[0-9]+$"
            boolean r2 = r3.matches(r2)
            if (r2 != 0) goto L_0x051b
            java.lang.String r2 = "^f[a-z]{3}[0-9]+-[0-9]+$"
            boolean r2 = r3.matches(r2)
            if (r2 == 0) goto L_0x04d6
            java.lang.String r2 = "fna:"
            java.lang.String r4 = X.AnonymousClass08S.A0J(r2, r3)
            goto L_0x04d6
        L_0x051b:
            r4 = r3
            goto L_0x04d6
        L_0x051d:
            r5.persistentCacheEnabled = r7
            goto L_0x01ee
        L_0x0521:
            X.1k6 r1 = r4.A00()
            long r3 = r1.A00
            goto L_0x00e7
        L_0x0529:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0570 }
            r5.<init>()     // Catch:{ all -> 0x0570 }
            java.lang.String r2 = "AndroidCAStore"
            java.security.KeyStore r7 = java.security.KeyStore.getInstance(r2)     // Catch:{ all -> 0x0567 }
            r2 = 0
            r7.load(r2, r2)     // Catch:{ all -> 0x0567 }
            java.util.Enumeration r6 = r7.aliases()     // Catch:{ all -> 0x0567 }
        L_0x053c:
            boolean r2 = r6.hasMoreElements()     // Catch:{ all -> 0x0567 }
            if (r2 == 0) goto L_0x0573
            java.lang.Object r3 = r6.nextElement()     // Catch:{ all -> 0x0567 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0567 }
            java.lang.String r2 = "user:"
            boolean r2 = r3.startsWith(r2)     // Catch:{ all -> 0x0567 }
            if (r2 == 0) goto L_0x053c
            java.security.cert.Certificate r3 = r7.getCertificate(r3)     // Catch:{ KeyStoreException -> 0x055e }
            boolean r2 = r3 instanceof java.security.cert.X509Certificate     // Catch:{ KeyStoreException -> 0x055e }
            if (r2 == 0) goto L_0x053c
            java.security.cert.X509Certificate r3 = (java.security.cert.X509Certificate) r3     // Catch:{ KeyStoreException -> 0x055e }
            r5.add(r3)     // Catch:{ KeyStoreException -> 0x055e }
            goto L_0x053c
        L_0x055e:
            r4 = move-exception
            java.lang.Class r3 = X.C17910zg.A01     // Catch:{ all -> 0x0567 }
            java.lang.String r2 = "Failed to get user Root CA"
            X.C010708t.A09(r3, r2, r4)     // Catch:{ all -> 0x0567 }
            goto L_0x053c
        L_0x0567:
            r4 = move-exception
            java.lang.Class r3 = X.C17910zg.A01     // Catch:{ all -> 0x0570 }
            java.lang.String r2 = "Failed to load AndroidCAStore"
            X.C010708t.A09(r3, r2, r4)     // Catch:{ all -> 0x0570 }
            goto L_0x0573
        L_0x0570:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0573:
            monitor-exit(r8)
            byte[][] r2 = X.C17910zg.A00(r5)
            r9.mUserInstalledCertificates = r2
        L_0x057a:
            r2 = 563357975314849(0x2005f000001a1, double:2.78335821913748E-309)
            long r2 = r0.At0(r2)
            r9.mNewConnTimeoutMillis = r2
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10710kj.A01(X.1XY):com.facebook.proxygen.HTTPClient$Builder");
    }

    public static final AnonymousClass1WC A00(AnonymousClass1XY r15) {
        C25051Yd A002 = AnonymousClass0WT.A00(r15);
        AnonymousClass1YA.A00(r15);
        FbSharedPreferences A003 = FbSharedPreferencesModule.A00(r15);
        FbDataConnectionManager A004 = FbDataConnectionManager.A00(r15);
        AnonymousClass1W0 r13 = new AnonymousClass1W0(r15);
        AnonymousClass0UQ A005 = AnonymousClass0UQ.A00(AnonymousClass1Y3.Ar4, r15);
        C06230bA A02 = AnonymousClass1ZF.A02(r15);
        AnonymousClass1W1 A06 = AnonymousClass1W1.A06(r15);
        AnonymousClass069 A03 = AnonymousClass067.A03(r15);
        AnonymousClass0Ud A006 = AnonymousClass0Ud.A00(r15);
        int AqL = A002.AqL(563353684738438L, 10000);
        int At0 = (int) A002.At0(563521184203237L);
        int At02 = (int) A002.At0(563353683100024L);
        AnonymousClass1W2 r7 = new AnonymousClass1W2();
        r7.A00 = A002.AqL(563353682248051L, 10);
        r7.A03 = new AnonymousClass1WA(AqL, At0, At02, A003);
        r7.A02 = new AnonymousClass1WB(A02, A004, A06, A03, r13, A005, A006);
        r7.A01 = new C21421Gx();
        return new AnonymousClass1WC(r7);
    }
}
