package X;

/* renamed from: X.0qM  reason: invalid class name */
public abstract class AnonymousClass0qM {
    public AnonymousClass1XL A05() {
        return ((C12990qL) this).A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        if (r3.A04 != false) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.C27671dZ r8) {
        /*
            r7 = this;
            r3 = r7
            X.0qL r3 = (X.C12990qL) r3
            X.1XL r1 = r3.A02
            X.1XL r0 = X.AnonymousClass1XL.DESTROYED
            if (r1 == r0) goto L_0x000b
            X.1XL r0 = X.AnonymousClass1XL.INITIALIZED
        L_0x000b:
            X.1da r4 = new X.1da
            r4.<init>(r8, r0)
            X.0qN r0 = r3.A01
            java.lang.Object r0 = r0.A03(r8, r4)
            X.1da r0 = (X.C27681da) r0
            if (r0 != 0) goto L_0x0074
            java.lang.ref.WeakReference r0 = r3.A06
            java.lang.Object r2 = r0.get()
            X.0n6 r2 = (X.C11410n6) r2
            if (r2 == 0) goto L_0x0074
            int r0 = r3.A00
            r6 = 1
            if (r0 != 0) goto L_0x002e
            boolean r0 = r3.A04
            r5 = 0
            if (r0 == 0) goto L_0x002f
        L_0x002e:
            r5 = 1
        L_0x002f:
            X.1XL r1 = X.C12990qL.A02(r3, r8)
            int r0 = r3.A00
            int r0 = r0 + r6
            r3.A00 = r0
        L_0x0038:
            X.1XL r0 = r4.A01
            int r0 = r0.compareTo(r1)
            if (r0 >= 0) goto L_0x006a
            X.0qN r0 = r3.A01
            java.util.HashMap r0 = r0.A00
            boolean r0 = r0.containsKey(r8)
            if (r0 == 0) goto L_0x006a
            X.1XL r1 = r4.A01
            java.util.ArrayList r0 = r3.A03
            r0.add(r1)
            X.1XL r0 = r4.A01
            X.0uB r0 = X.C12990qL.A00(r0)
            r4.A00(r2, r0)
            java.util.ArrayList r1 = r3.A03
            int r0 = r1.size()
            int r0 = r0 + -1
            r1.remove(r0)
            X.1XL r1 = X.C12990qL.A02(r3, r8)
            goto L_0x0038
        L_0x006a:
            if (r5 != 0) goto L_0x006f
            X.C12990qL.A03(r3)
        L_0x006f:
            int r0 = r3.A00
            int r0 = r0 - r6
            r3.A00 = r0
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0qM.A06(X.1dZ):void");
    }

    public void A07(C27671dZ r2) {
        ((C12990qL) this).A01.A02(r2);
    }
}
