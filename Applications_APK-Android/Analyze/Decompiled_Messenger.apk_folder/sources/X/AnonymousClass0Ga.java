package X;

import com.facebook.profilo.provider.network.NetworkTigonLigerProvider;
import com.facebook.tigon.tigonliger.TigonLigerService;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Ga  reason: invalid class name */
public final class AnonymousClass0Ga implements AnonymousClass1YQ {
    private static volatile AnonymousClass0Ga A01;
    private AnonymousClass0UN A00;

    public String getSimpleName() {
        return "AfterColdStartInitializer";
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BM1, r1);
    }

    public static final AnonymousClass0Ga A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0Ga.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0Ga(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass0Ga(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public void init() {
        int A03 = C000700l.A03(-577057243);
        C004505k A002 = C004505k.A00();
        if (!C03950Rc.A03((C03950Rc) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AIx, this.A00))) {
            A002.A08();
        }
        int i = AnonymousClass1Y3.Aj0;
        AnonymousClass0UN r6 = this.A00;
        NetworkTigonLigerProvider networkTigonLigerProvider = new NetworkTigonLigerProvider(null, (TigonLigerService) AnonymousClass1XX.A02(0, i, r6), (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Au1, r6));
        synchronized (A002) {
            if (networkTigonLigerProvider.A08()) {
                C000900o[] r1 = A002.A08;
                C000900o[] r12 = (C000900o[]) Arrays.copyOf(r1, r1.length + 1);
                r12[r12.length - 1] = networkTigonLigerProvider;
                A002.A08 = r12;
            } else {
                C000900o[] r13 = A002.A07;
                C000900o[] r14 = (C000900o[]) Arrays.copyOf(r13, r13.length + 1);
                r14[r14.length - 1] = networkTigonLigerProvider;
                A002.A07 = r14;
            }
        }
        C000700l.A09(-1166158919, A03);
    }
}
