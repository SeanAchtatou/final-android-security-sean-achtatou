package X;

/* renamed from: X.0Kk  reason: invalid class name */
public final class AnonymousClass0Kk extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.battery.xprocess.XProcessLogWriter$1";
    public final /* synthetic */ C03210Km A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0Kk(C03210Km r1, String str) {
        super(str);
        this.A00 = r1;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:39|(2:43|44)|45|46) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x010e */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b3 A[SYNTHETIC, Splitter:B:24:0x00b3] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010b A[SYNTHETIC, Splitter:B:43:0x010b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            X.0Kh r5 = new X.0Kh
            X.0Km r0 = r10.A00
            android.content.Context r0 = r0.A00
            r5.<init>(r0)
            X.C03170Kh.A00(r5)
            boolean r0 = r5.A04
            if (r0 != 0) goto L_0x0011
            return
        L_0x0011:
            X.0Km r4 = r10.A00
            X.C03170Kh.A00(r5)
            boolean r7 = r5.A03
            monitor-enter(r4)
            r0 = 1
            r4.A04 = r0     // Catch:{ all -> 0x010f }
            X.0FK r3 = new X.0FK     // Catch:{ all -> 0x010f }
            r3.<init>()     // Catch:{ all -> 0x010f }
            java.lang.Class<X.0FV> r2 = X.AnonymousClass0FV.class
            X.0FW r1 = new X.0FW     // Catch:{ all -> 0x010f }
            r1.<init>()     // Catch:{ all -> 0x010f }
            X.04b r0 = r3.A00     // Catch:{ all -> 0x010f }
            r0.put(r2, r1)     // Catch:{ all -> 0x010f }
            java.lang.Class<X.0Fl> r2 = X.C02570Fl.class
            X.0Fm r1 = new X.0Fm     // Catch:{ all -> 0x010f }
            r1.<init>()     // Catch:{ all -> 0x010f }
            X.04b r0 = r3.A00     // Catch:{ all -> 0x010f }
            r0.put(r2, r1)     // Catch:{ all -> 0x010f }
            java.lang.Class<X.0Fo> r2 = X.C02600Fo.class
            X.0Fq r1 = X.C02610Fp.A00     // Catch:{ all -> 0x010f }
            X.04b r0 = r3.A00     // Catch:{ all -> 0x010f }
            r0.put(r2, r1)     // Catch:{ all -> 0x010f }
            X.0G2 r6 = new X.0G2     // Catch:{ all -> 0x010f }
            r6.<init>(r3)     // Catch:{ all -> 0x010f }
            X.0Gg r3 = X.C03190Kj.A00()     // Catch:{ all -> 0x010f }
            X.04b r9 = r3.mMetricsMap     // Catch:{ all -> 0x010f }
            int r8 = r9.size()     // Catch:{ all -> 0x010f }
            r2 = 0
        L_0x0052:
            if (r2 >= r8) goto L_0x0061
            java.lang.Object r1 = r9.A07(r2)     // Catch:{ all -> 0x010f }
            java.lang.Class r1 = (java.lang.Class) r1     // Catch:{ all -> 0x010f }
            r0 = 1
            r3.A0D(r1, r0)     // Catch:{ all -> 0x010f }
            int r2 = r2 + 1
            goto L_0x0052
        L_0x0061:
            java.lang.Class<X.0Fl> r2 = X.C02570Fl.class
            X.04b r0 = r6.A00     // Catch:{ all -> 0x010f }
            java.lang.Object r1 = r0.get(r2)     // Catch:{ all -> 0x010f }
            X.07e r1 = (X.C007907e) r1     // Catch:{ all -> 0x010f }
            X.0FM r0 = r3.A09(r2)     // Catch:{ all -> 0x010f }
            r1.A04(r0)     // Catch:{ all -> 0x010f }
            X.0Gc r2 = new X.0Gc     // Catch:{ all -> 0x010f }
            X.0Gg r1 = r6.A03()     // Catch:{ all -> 0x010f }
            X.0Gg r0 = r6.A03()     // Catch:{ all -> 0x010f }
            r2.<init>(r6, r1, r3, r0)     // Catch:{ all -> 0x010f }
            r4.A01 = r2     // Catch:{ all -> 0x010f }
            X.0Kj r6 = new X.0Kj     // Catch:{ all -> 0x010f }
            android.content.Context r0 = r4.A00     // Catch:{ all -> 0x010f }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x010f }
            java.io.File r1 = r0.getFilesDir()     // Catch:{ all -> 0x010f }
            java.lang.String r0 = "batterymetrics"
            r3.<init>(r1, r0)     // Catch:{ all -> 0x010f }
            r9 = 0
            java.io.RandomAccessFile r8 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x00a7 }
            java.lang.String r1 = "/proc/self/cmdline"
            java.lang.String r0 = "r"
            r8.<init>(r1, r0)     // Catch:{ IOException -> 0x00a7 }
            java.lang.String r0 = r8.readLine()     // Catch:{ IOException -> 0x00a6, all -> 0x0107 }
            java.lang.String r2 = r0.trim()     // Catch:{ IOException -> 0x00a6, all -> 0x0107 }
            r8.close()     // Catch:{ IOException -> 0x00b6 }
            goto L_0x00b6
        L_0x00a6:
            r9 = r8
        L_0x00a7:
            java.lang.String r2 = "XProcessLog"
            java.lang.String r1 = "Unable to read process name"
            r0 = 0
            X.AnonymousClass0KZ.A00(r2, r1, r0)     // Catch:{ all -> 0x0105 }
            java.lang.String r2 = "unknown"
            if (r9 == 0) goto L_0x00b6
            r9.close()     // Catch:{ IOException -> 0x00b6 }
        L_0x00b6:
            java.lang.String r1 = ":"
            java.lang.String r0 = "_"
            java.lang.String r2 = r2.replace(r1, r0)     // Catch:{ all -> 0x010f }
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x010f }
            java.lang.String r0 = "metrics_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r2)     // Catch:{ all -> 0x010f }
            r1.<init>(r3, r0)     // Catch:{ all -> 0x010f }
            r6.<init>(r1)     // Catch:{ all -> 0x010f }
            r4.A02 = r6     // Catch:{ all -> 0x010f }
            if (r7 == 0) goto L_0x00e3
            android.content.Context r3 = r4.A00     // Catch:{ all -> 0x010f }
            if (r3 == 0) goto L_0x00e3
            X.0Kl r2 = new X.0Kl     // Catch:{ all -> 0x010f }
            r2.<init>(r4)     // Catch:{ all -> 0x010f }
            android.content.IntentFilter r1 = new android.content.IntentFilter     // Catch:{ all -> 0x010f }
            java.lang.String r0 = "ACTION_BATTERY_WRITE_XPROCESS_LOG"
            r1.<init>(r0)     // Catch:{ all -> 0x010f }
            r3.registerReceiver(r2, r1)     // Catch:{ all -> 0x010f }
        L_0x00e3:
            monitor-exit(r4)
            X.C03170Kh.A00(r5)
            long r3 = r5.A00
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00f7
            X.C03170Kh.A00(r5)
            long r0 = r5.A00
            android.os.SystemClock.sleep(r0)
        L_0x00f7:
            X.0Km r0 = r10.A00
            r0.A00()
            X.C03170Kh.A00(r5)
            long r0 = r5.A01
            android.os.SystemClock.sleep(r0)
            goto L_0x00f7
        L_0x0105:
            r0 = move-exception
            goto L_0x0109
        L_0x0107:
            r0 = move-exception
            r9 = r8
        L_0x0109:
            if (r9 == 0) goto L_0x010e
            r9.close()     // Catch:{ IOException -> 0x010e }
        L_0x010e:
            throw r0     // Catch:{ all -> 0x010f }
        L_0x010f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Kk.run():void");
    }
}
