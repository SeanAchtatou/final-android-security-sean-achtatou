package X;

import android.content.Context;
import android.os.RemoteException;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.push.mqtt.ipc.IMqttPushService;
import com.facebook.push.mqtt.service.MqttPushServiceClientImpl$MqttPublishListenerStub;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1uR  reason: invalid class name and case insensitive filesystem */
public final class C36991uR {
    public static final Class A0C = C36991uR.class;
    public IMqttPushService A00;
    public boolean A01;
    public final Context A02;
    public final AnonymousClass1Y6 A03;
    public final AnonymousClass06B A04;
    public final AnonymousClass069 A05;
    public final C29291gB A06;
    public final C32811mN A07;
    public final C24411Tn A08;
    public final AnonymousClass1U3 A09;
    public final Set A0A = C25011Xz.A03();
    private final ScheduledExecutorService A0B;

    public static synchronized IMqttPushService A00(C36991uR r1) {
        IMqttPushService iMqttPushService;
        synchronized (r1) {
            r1.A02();
            iMqttPushService = r1.A00;
            if (iMqttPushService == null) {
                throw new RemoteException();
            }
        }
        return iMqttPushService;
    }

    private synchronized void A02() {
        if (!this.A01) {
            throw new RemoteException();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r0 = X.AnonymousClass089.DISCONNECTED;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0015 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.AnonymousClass089 A05() {
        /*
            r1 = this;
            monitor-enter(r1)
            r1.A02()     // Catch:{ RemoteException -> 0x0015 }
            com.facebook.push.mqtt.ipc.IMqttPushService r0 = r1.A00     // Catch:{ RemoteException -> 0x0015 }
            if (r0 != 0) goto L_0x000b
            X.089 r0 = X.AnonymousClass089.DISCONNECTED     // Catch:{ RemoteException -> 0x0015 }
            goto L_0x0017
        L_0x000b:
            java.lang.String r0 = r0.AiF()     // Catch:{ RemoteException -> 0x0015 }
            X.089 r0 = X.AnonymousClass089.valueOf(r0)     // Catch:{ RemoteException -> 0x0015 }
            monitor-exit(r1)
            return r0
        L_0x0015:
            X.089 r0 = X.AnonymousClass089.DISCONNECTED     // Catch:{ all -> 0x0019 }
        L_0x0017:
            monitor-exit(r1)
            return r0
        L_0x0019:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36991uR.A05():X.089");
    }

    public synchronized void A06() {
        if (this.A01) {
            this.A0B.schedule(new C32261lT(this), 60, TimeUnit.SECONDS);
            this.A01 = false;
        }
    }

    public static C205949nG A01(C36991uR r7, String str, byte[] bArr, B6D b6d) {
        long now;
        C205949nG r0;
        if (!A00(r7).AU4(LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT)) {
            return new C205949nG(false, null, AnonymousClass07B.A00, null, r7.A04.now());
        }
        b6d.A01();
        try {
            now = r7.A04.now();
            boolean z = false;
            if (r7.A03(str, bArr, AnonymousClass07B.A00, null) != -1) {
                z = true;
            }
            if (!z) {
                r0 = new C205949nG(false, null, AnonymousClass07B.A01, null, now);
            } else if (!b6d.A05(3000)) {
                r0 = new C205949nG(false, null, AnonymousClass07B.A0C, null, now);
            } else {
                r0 = new C205949nG(true, b6d.A00, null, null, now);
            }
            return r0;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return C205949nG.A00(e, now);
        } finally {
            b6d.A02();
        }
    }

    public C36991uR(Context context, AnonymousClass1U3 r3, AnonymousClass06B r4, C24411Tn r5, AnonymousClass1Y6 r6, AnonymousClass069 r7, ScheduledExecutorService scheduledExecutorService, C29291gB r9) {
        this.A02 = context;
        this.A09 = r3;
        this.A04 = r4;
        this.A08 = r5;
        this.A03 = r6;
        this.A05 = r7;
        this.A06 = r9;
        this.A0B = scheduledExecutorService;
        this.A07 = new C32811mN(this);
    }

    public int A03(String str, byte[] bArr, Integer num, C57332ro r8) {
        MqttPushServiceClientImpl$MqttPublishListenerStub mqttPushServiceClientImpl$MqttPublishListenerStub;
        IMqttPushService A002 = A00(this);
        if (r8 != null) {
            mqttPushServiceClientImpl$MqttPublishListenerStub = new MqttPushServiceClientImpl$MqttPublishListenerStub(this, r8);
            synchronized (this) {
                this.A0A.add(r8);
            }
        } else {
            mqttPushServiceClientImpl$MqttPublishListenerStub = null;
        }
        return A002.Byg(str, bArr, AnonymousClass08G.A00(num), mqttPushServiceClientImpl$MqttPublishListenerStub);
    }

    public C205949nG A04(String str, byte[] bArr, B6D b6d) {
        try {
            return A01(this, str, bArr, b6d);
        } catch (RemoteException e) {
            return C205949nG.A00(e, this.A04.now());
        }
    }

    public boolean A07(String str, JsonNode jsonNode, long j) {
        return A08(str, C06850cB.A0G(jsonNode.toString()), j, 0);
    }

    public boolean A08(String str, byte[] bArr, long j, long j2) {
        return A00(this).Byj(str, bArr, j, null, j2);
    }

    public boolean A09(String str, byte[] bArr, long j, C57332ro r17, long j2, Integer num) {
        MqttPushServiceClientImpl$MqttPublishListenerStub mqttPushServiceClientImpl$MqttPublishListenerStub;
        IMqttPushService A002 = A00(this);
        String str2 = null;
        C57332ro r2 = r17;
        if (r17 != null) {
            mqttPushServiceClientImpl$MqttPublishListenerStub = new MqttPushServiceClientImpl$MqttPublishListenerStub(this, r2);
            synchronized (this) {
                this.A0A.add(r2);
            }
        } else {
            mqttPushServiceClientImpl$MqttPublishListenerStub = null;
        }
        if (num != null) {
            str2 = String.valueOf(num);
        }
        return A002.Byl(str, bArr, j, mqttPushServiceClientImpl$MqttPublishListenerStub, j2, str2);
    }
}
