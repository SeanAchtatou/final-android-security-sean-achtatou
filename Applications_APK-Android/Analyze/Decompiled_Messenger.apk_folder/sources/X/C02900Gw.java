package X;

/* renamed from: X.0Gw  reason: invalid class name and case insensitive filesystem */
public final class C02900Gw implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        AnonymousClass0G0 r62 = (AnonymousClass0G0) r6;
        long j = r62.javaHeapMaxSizeKb;
        if (j != 0) {
            r7.AMV("java_heap_max_size_kb", j);
        }
        long j2 = r62.javaHeapAllocatedKb;
        if (j2 != 0) {
            r7.AMV("java_heap_allocated_size_kb", j2);
        }
        long j3 = r62.nativeHeapSizeKb;
        if (j3 != 0) {
            r7.AMV("native_heap_size_kb", j3);
        }
        long j4 = r62.nativeHeapAllocatedKb;
        if (j4 != 0) {
            r7.AMV("native_heap_allocated_size_kb", j4);
        }
        long j5 = r62.vmSizeKb;
        if (j5 != 0) {
            r7.AMV("vm_size_kb", j5);
        }
        long j6 = r62.vmRssKb;
        if (j6 != 0) {
            r7.AMV("vm_rss_kb", j6);
        }
    }
}
