package X;

import com.facebook.litho.LithoView;
import java.lang.ref.WeakReference;

/* renamed from: X.0z7  reason: invalid class name and case insensitive filesystem */
public final class C17570z7 extends C17580z8 {
    private final WeakReference A00;

    public void onAccessibilityStateChanged(boolean z) {
        synchronized (C17560z6.class) {
            C17560z6.A01 = false;
        }
        LithoView lithoView = (LithoView) this.A00.get();
        if (lithoView != null) {
            lithoView.A0N(z);
            lithoView.A0C = true;
            lithoView.requestLayout();
        }
    }

    public C17570z7(LithoView lithoView) {
        this.A00 = new WeakReference(lithoView);
    }
}
