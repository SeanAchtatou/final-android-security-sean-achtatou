package X;

import android.os.Process;
import com.facebook.common.iopri.IoPriority;

/* renamed from: X.1qo  reason: invalid class name and case insensitive filesystem */
public final class C35181qo implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.appboost.cpu.periodicboost.PeriodicThreadBooster$4";
    public final /* synthetic */ C34731q5 A00;

    public C35181qo(C34731q5 r1) {
        this.A00 = r1;
    }

    public void run() {
        Integer num = this.A00.A07;
        if (num != null) {
            Process.setThreadPriority(num.intValue());
            this.A00.A07 = null;
        }
        C34761q8 r0 = this.A00.A06;
        if (r0 != null) {
            C73263fn r2 = r0.A01;
            int i = r0.A00;
            if (IoPriority.sLibLoaded && r2 != null) {
                IoPriority.nativeSetCurrentIoPriority(r2.mNativeEnumVal, i);
            }
            this.A00.A06 = null;
        }
        C34131oh.A00.set(0);
        C34131oh.A00();
    }
}
