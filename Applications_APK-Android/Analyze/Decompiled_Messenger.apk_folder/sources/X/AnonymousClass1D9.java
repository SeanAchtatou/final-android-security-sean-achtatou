package X;

import com.facebook.payments.p2p.model.PaymentCard;
import com.facebook.payments.paymentmethods.cardform.CardFormParams;

/* renamed from: X.1D9  reason: invalid class name */
public final class AnonymousClass1D9 extends C06020ai {
    public final /* synthetic */ C54862n9 A00;
    public final /* synthetic */ PaymentCard A01;
    public final /* synthetic */ C50732eW A02;
    public final /* synthetic */ CardFormParams A03;

    public AnonymousClass1D9(C54862n9 r1, CardFormParams cardFormParams, C50732eW r3, PaymentCard paymentCard) {
        this.A00 = r1;
        this.A03 = cardFormParams;
        this.A02 = r3;
        this.A01 = paymentCard;
    }
}
