package X;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;

/* renamed from: X.0Eq  reason: invalid class name */
public final class AnonymousClass0Eq {
    private static AnonymousClass0Eq A05;
    public final ArrayList A00;
    public final BitSet A01;
    public final String[] A02;
    private final BitSet A03;
    private final Integer[] A04;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
        if (r3 >= 10) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.Integer A01(X.AnonymousClass0Eq r2, int r3) {
        /*
            monitor-enter(r2)
            if (r3 < 0) goto L_0x0008
            r1 = 10
            r0 = 1
            if (r3 < r1) goto L_0x0009
        L_0x0008:
            r0 = 0
        L_0x0009:
            if (r0 == 0) goto L_0x0010
            java.lang.Integer[] r0 = r2.A04     // Catch:{ all -> 0x0014 }
            r0 = r0[r3]     // Catch:{ all -> 0x0014 }
            goto L_0x0012
        L_0x0010:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0014 }
        L_0x0012:
            monitor-exit(r2)
            return r0
        L_0x0014:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Eq.A01(X.0Eq, int):java.lang.Integer");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x002d, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A02(X.AnonymousClass0Eq r3, int r4, int r5) {
        /*
            monitor-enter(r3)
            r0 = -3
            r2 = 0
            if (r4 == r0) goto L_0x002c
            r0 = -2
            if (r4 == r0) goto L_0x002c
            r0 = -1
            r1 = 1
            if (r4 == r0) goto L_0x002a
            boolean r0 = X.AnonymousClass0GL.A02(r4)     // Catch:{ all -> 0x0027 }
            if (r0 == 0) goto L_0x002c
            if (r5 == r1) goto L_0x001f
            r0 = 2
            if (r5 != r0) goto L_0x002c
            java.util.BitSet r0 = r3.A03     // Catch:{ all -> 0x0027 }
            boolean r0 = r0.get(r4)     // Catch:{ all -> 0x0027 }
            monitor-exit(r3)
            return r0
        L_0x001f:
            java.util.BitSet r0 = r3.A01     // Catch:{ all -> 0x0027 }
            boolean r0 = r0.get(r4)     // Catch:{ all -> 0x0027 }
            monitor-exit(r3)
            return r0
        L_0x0027:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x002a:
            monitor-exit(r3)
            return r1
        L_0x002c:
            monitor-exit(r3)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Eq.A02(X.0Eq, int, int):boolean");
    }

    public synchronized void A04(int i, Integer num) {
        if (AnonymousClass0GL.A02(i)) {
            Integer[] numArr = this.A04;
            if (numArr[i] != num) {
                numArr[i] = num;
                Iterator it = this.A00.iterator();
                while (it.hasNext()) {
                    AnonymousClass0Q7 r3 = (AnonymousClass0Q7) it.next();
                    AnonymousClass07A.A04(r3.A01, new AnonymousClass0Q8(r3, C02420Er.A02(i), num), 852854951);
                }
                if (C010708t.A0X(3)) {
                    C02420Er.A02(i);
                }
            }
        }
    }

    public static synchronized AnonymousClass0Eq A00() {
        AnonymousClass0Eq r0;
        synchronized (AnonymousClass0Eq.class) {
            if (A05 == null) {
                A05 = new AnonymousClass0Eq();
            }
            r0 = A05;
        }
        return r0;
    }

    public String A03(String str) {
        String str2;
        int A002 = AnonymousClass0GL.A00(str);
        synchronized (this) {
            str2 = null;
            if (!(A002 == -3 || A002 == -2 || A002 == -1)) {
                if (AnonymousClass0GL.A02(A002)) {
                    str2 = this.A02[A002];
                }
            }
        }
        return str2;
    }

    public void A05(String str, Integer num) {
        A04(AnonymousClass0GL.A00(str), num);
    }

    private AnonymousClass0Eq() {
        this(10);
    }

    private AnonymousClass0Eq(int i) {
        Integer[] numArr = new Integer[i];
        this.A04 = numArr;
        this.A01 = new BitSet(i);
        this.A03 = new BitSet(i);
        this.A02 = new String[i];
        this.A00 = new ArrayList();
        for (int i2 = 0; i2 < i; i2++) {
            numArr[i2] = AnonymousClass07B.A00;
        }
    }
}
