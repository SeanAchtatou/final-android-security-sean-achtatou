package X;

import java.util.WeakHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0n7  reason: invalid class name and case insensitive filesystem */
public final class C11420n7 {
    private static volatile C11420n7 A01;
    public final WeakHashMap A00 = new WeakHashMap();

    public static final C11420n7 A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C11420n7.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C11420n7();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
