package X;

import com.facebook.common.util.TriState;

/* renamed from: X.0X4  reason: invalid class name */
public final class AnonymousClass0X4 {
    public final TriState[] A00;
    public final TriState[] A01;
    public final TriState[] A02;

    public synchronized TriState A01(int i) {
        return this.A00[i];
    }

    public synchronized void A02(int i) {
        this.A01[i] = TriState.UNSET;
        A00(i);
    }

    public synchronized void A03(int i) {
        this.A02[i] = TriState.UNSET;
        A00(i);
    }

    public synchronized void A04(int i, boolean z) {
        this.A02[i] = TriState.valueOf(z);
        A00(i);
    }

    public synchronized void A05(int i, boolean z) {
        this.A01[i] = TriState.valueOf(z);
        A00(i);
    }

    public synchronized boolean A06(int i) {
        return !TriState.UNSET.equals(this.A00[i]);
    }

    private void A00(int i) {
        TriState[] triStateArr = this.A00;
        TriState triState = this.A02[i];
        TriState triState2 = this.A01[i];
        if (TriState.UNSET.equals(triState2)) {
            triState2 = triState;
        }
        triStateArr[i] = triState2;
    }

    public AnonymousClass0X4(int i) {
        TriState[] triStateArr = new TriState[i];
        for (int i2 = 0; i2 < i; i2++) {
            triStateArr[i2] = TriState.UNSET;
        }
        this.A02 = triStateArr;
        TriState[] triStateArr2 = new TriState[i];
        for (int i3 = 0; i3 < i; i3++) {
            triStateArr2[i3] = TriState.UNSET;
        }
        this.A01 = triStateArr2;
        TriState[] triStateArr3 = new TriState[i];
        for (int i4 = 0; i4 < i; i4++) {
            triStateArr3[i4] = TriState.UNSET;
        }
        this.A00 = triStateArr3;
    }
}
