package X;

import android.content.Context;

/* renamed from: X.1Be  reason: invalid class name and case insensitive filesystem */
public final class C20171Be {
    public AnonymousClass0UN A00;
    public final Context A01;
    public final AnonymousClass1BC A02;

    public C20171Be(AnonymousClass1XY r3, Context context, AnonymousClass1BC r5) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = context;
        this.A02 = r5;
    }
}
