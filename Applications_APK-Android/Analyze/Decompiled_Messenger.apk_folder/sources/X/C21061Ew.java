package X;

import android.os.Process;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Ew  reason: invalid class name and case insensitive filesystem */
public abstract class C21061Ew {
    public C21061Ew A02(int i) {
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r0 = (AnonymousClass1F0) this;
            r0.A07 = i;
            return r0;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r02 = (C21021Es) this;
            r02.A07 = i;
            return r02;
        }
    }

    public C21061Ew A04(String str, double d) {
        String str2 = str;
        double d2 = d;
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r0 = (AnonymousClass1F0) this;
            r0.A04.markerAnnotate(r0.A08, r0.A06, str2, d2);
            return r0;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r7 = (C21021Es) this;
            C08520fU r8 = r7.A04;
            C04270Tg A01 = C21021Es.A01(r7);
            int i = r7.A06;
            C39851zi r1 = r8.A03;
            Ed3 A012 = r1 != null ? r1.A01(A01.A02) : null;
            if (!C08520fU.A09(A01.A02, r8.A07)) {
                C09580i5 r02 = r8.A04;
                int myTid = Process.myTid();
                C09160gd r2 = r8.A0G;
                synchronized (r02.A04) {
                    if (A01.A0b.get() == i) {
                        A01.A07 = myTid;
                        A01.A04(str, d);
                        r2.A00(A01);
                    }
                }
                C39851zi r03 = r8.A03;
                if (!(r03 == null || A012 == null)) {
                    r03.A03(A012);
                    return r7;
                }
            }
            return r7;
        }
    }

    public C21061Ew A05(String str, int i) {
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r3 = (AnonymousClass1F0) this;
            r3.A04.markerAnnotate(r3.A08, r3.A06, str, i);
            return r3;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r7 = (C21021Es) this;
            C08520fU r8 = r7.A04;
            C04270Tg A01 = C21021Es.A01(r7);
            int i2 = r7.A06;
            C39851zi r1 = r8.A03;
            Ed3 A012 = r1 != null ? r1.A01(A01.A02) : null;
            if (!C08520fU.A09(A01.A02, r8.A07)) {
                C09580i5 r0 = r8.A04;
                int myTid = Process.myTid();
                C09160gd r2 = r8.A0G;
                synchronized (r0.A04) {
                    if (A01.A0b.get() == i2) {
                        A01.A07 = myTid;
                        A01.A05(str, i);
                        r2.A00(A01);
                    }
                }
                C39851zi r02 = r8.A03;
                if (!(r02 == null || A012 == null)) {
                    r02.A03(A012);
                    return r7;
                }
            }
            return r7;
        }
    }

    public C21061Ew A06(String str, long j) {
        String str2 = str;
        long j2 = j;
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r0 = (AnonymousClass1F0) this;
            r0.A04.markerAnnotate(r0.A08, r0.A06, str2, j2);
            return r0;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r7 = (C21021Es) this;
            C08520fU r8 = r7.A04;
            C04270Tg A01 = C21021Es.A01(r7);
            int i = r7.A06;
            C39851zi r1 = r8.A03;
            Ed3 A012 = r1 != null ? r1.A01(A01.A02) : null;
            if (!C08520fU.A09(A01.A02, r8.A07)) {
                C09580i5 r02 = r8.A04;
                int myTid = Process.myTid();
                C09160gd r2 = r8.A0G;
                synchronized (r02.A04) {
                    if (A01.A0b.get() == i) {
                        A01.A07 = myTid;
                        A01.A06(str, j);
                        r2.A00(A01);
                    }
                }
                C39851zi r03 = r8.A03;
                if (!(r03 == null || A012 == null)) {
                    r03.A03(A012);
                    return r7;
                }
            }
            return r7;
        }
    }

    public C21061Ew A08(String str, String str2) {
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r3 = (AnonymousClass1F0) this;
            r3.A04.markerAnnotate(r3.A08, r3.A06, str, str2);
            return r3;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r7 = (C21021Es) this;
            C08520fU r8 = r7.A04;
            C04270Tg A01 = C21021Es.A01(r7);
            int i = r7.A06;
            C39851zi r1 = r8.A03;
            Ed3 A012 = r1 != null ? r1.A01(A01.A02) : null;
            if (!C08520fU.A09(A01.A02, r8.A07)) {
                C09580i5 r0 = r8.A04;
                int myTid = Process.myTid();
                C09160gd r2 = r8.A0G;
                synchronized (r0.A04) {
                    if (A01.A0b.get() == i) {
                        A01.A07 = myTid;
                        A01.A07(str, str2);
                        r2.A00(A01);
                    }
                }
                C39851zi r02 = r8.A03;
                if (!(r02 == null || A012 == null)) {
                    r02.A03(A012);
                    return r7;
                }
            }
            return r7;
        }
    }

    public C21061Ew A0A(String str, String str2, long j, int i) {
        AnonymousClass0lr r5;
        String str3 = str;
        String str4 = str2;
        int i2 = i;
        long j2 = j;
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r6 = (AnonymousClass1F0) this;
            r6.A04.markerPoint(r6.A08, r6.A06, str3, str4, j2, i2);
            return r6;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r3 = (C21021Es) this;
            C08520fU r2 = r3.A04;
            C04270Tg A01 = C21021Es.A01(r3);
            int i3 = r3.A06;
            int i4 = r3.A07;
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            int myTid = Process.myTid();
            C39851zi r62 = r2.A03;
            Ed3 A012 = r62 != null ? r62.A01(A01.A02) : null;
            boolean z = false;
            if (j == -1) {
                z = true;
            }
            if (!z || !r2.A0C) {
                long A02 = C08520fU.A02(r2, j2, timeUnit);
                C09580i5 r11 = r2.A04;
                TimeUnit timeUnit2 = TimeUnit.NANOSECONDS;
                boolean z2 = !z;
                C09160gd r7 = r2.A0G;
                Ed3 ed3 = A012;
                synchronized (r11.A04) {
                    if (str2 != null) {
                        if (C09580i5.A0A(A01, r7) && str2 != null) {
                            r5 = new AnonymousClass0lr();
                            r5.A00("__key", str4, 1);
                            r5.A03 = true;
                        }
                    }
                    r5 = null;
                }
                if (r11.A0J(A01, i3, i4, A02, timeUnit2, z2, str3, r5, i2, myTid, r7, ed3)) {
                    C08520fU.A08(r2, "markerPoint", A01.A02, str3, str4);
                }
                C39851zi r1 = r2.A03;
                if (!(r1 == null || A012 == null)) {
                    r1.A04(A012);
                }
                return r3;
            }
            throw new IllegalArgumentException("Autotime in lockless is illegal (time won't be valid when method is executed)");
        }
    }

    public C21061Ew A0B(String str, boolean z) {
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r3 = (AnonymousClass1F0) this;
            r3.A04.markerAnnotate(r3.A08, r3.A06, str, z);
            return r3;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r7 = (C21021Es) this;
            C08520fU r8 = r7.A04;
            C04270Tg A01 = C21021Es.A01(r7);
            int i = r7.A06;
            C39851zi r1 = r8.A03;
            Ed3 A012 = r1 != null ? r1.A01(A01.A02) : null;
            if (!C08520fU.A09(A01.A02, r8.A07)) {
                C09580i5 r0 = r8.A04;
                int myTid = Process.myTid();
                C09160gd r2 = r8.A0G;
                synchronized (r0.A04) {
                    if (A01.A0b.get() == i) {
                        A01.A07 = myTid;
                        A01.A08(str, z);
                        r2.A00(A01);
                    }
                }
                C39851zi r02 = r8.A03;
                if (!(r02 == null || A012 == null)) {
                    r02.A03(A012);
                    return r7;
                }
            }
            return r7;
        }
    }

    public C21061Ew A0C(String str, String[] strArr) {
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r3 = (AnonymousClass1F0) this;
            r3.A04.markerAnnotate(r3.A08, r3.A06, str, strArr);
            return r3;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r7 = (C21021Es) this;
            C08520fU r8 = r7.A04;
            C04270Tg A01 = C21021Es.A01(r7);
            int i = r7.A06;
            C39851zi r1 = r8.A03;
            Ed3 A012 = r1 != null ? r1.A01(A01.A02) : null;
            if (!C08520fU.A09(A01.A02, r8.A07)) {
                C09580i5 r0 = r8.A04;
                int myTid = Process.myTid();
                C09160gd r2 = r8.A0G;
                synchronized (r0.A04) {
                    if (A01.A0b.get() == i) {
                        A01.A07 = myTid;
                        A01.A09(str, strArr);
                        r2.A00(A01);
                    }
                }
                C39851zi r02 = r8.A03;
                if (!(r02 == null || A012 == null)) {
                    r02.A03(A012);
                    return r7;
                }
            }
            return r7;
        }
    }

    public C31011j0 A0D(String str) {
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r2 = (AnonymousClass1F0) this;
            r2.A03 = str;
            r2.A01 = -1;
            r2.A00 = 0;
            return r2;
        } else if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        } else {
            C21021Es r22 = (C21021Es) this;
            r22.A03 = str;
            r22.A01 = -1;
            r22.A00 = 0;
            return r22;
        }
    }

    public void BK9() {
        if (this instanceof AnonymousClass1F0) {
            AnonymousClass1F0 r1 = (AnonymousClass1F0) this;
            if (r1.A03 != null) {
                r1.BxA();
            }
            r1.A08 = 0;
            r1.A06 = 0;
            r1.A07 = 7;
            r1.A05.A00.set(r1);
        } else if (!(this instanceof AnonymousClass1F4)) {
            C21021Es r12 = (C21021Es) this;
            if (r12.A03 != null) {
                r12.BxA();
            }
            r12.A08 = null;
            r12.A06 = 0;
            r12.A07 = 7;
            r12.A05.A00.set(r12);
        }
    }

    public C21061Ew A03(String str) {
        if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        }
        A09(str, null);
        return this;
    }

    public C21061Ew A07(String str, long j) {
        if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        }
        A0A(str, null, j, 0);
        return this;
    }

    public C21061Ew A09(String str, String str2) {
        if (this instanceof AnonymousClass1F4) {
            return (AnonymousClass1F4) this;
        }
        A0A(str, str2, -1, 0);
        return this;
    }
}
