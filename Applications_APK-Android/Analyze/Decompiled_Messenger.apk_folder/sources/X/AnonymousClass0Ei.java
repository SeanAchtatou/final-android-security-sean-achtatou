package X;

import android.content.Context;
import com.facebook.common.dextricks.DalvikInternals;
import com.facebook.inject.InjectorModule;
import java.io.IOException;
import javax.inject.Singleton;

@InjectorModule
/* renamed from: X.0Ei  reason: invalid class name */
public final class AnonymousClass0Ei extends AnonymousClass0UV {
    private static volatile C02390Em A00;
    private static volatile AnonymousClass0Ep A01;
    private static volatile AnonymousClass0Ej A02;
    private static volatile C02370Ek A03;
    private static volatile C02400En A04;
    private static volatile C02380El A05;

    public static final C02390Em A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C02390Em.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        A00 = C02390Em.A00(AnonymousClass1YA.A00(r3.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final AnonymousClass0Ep A03(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass0Ep.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        A01 = C02410Eo.A00(AnonymousClass1YA.A00(r3.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    @Singleton
    private static AnonymousClass0Ej A04(Context context, AnonymousClass0XQ r2, AnonymousClass0Ep r3) {
        return new AnonymousClass0Ej(context, r2, r3);
    }

    public static final AnonymousClass0Ej A06(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (AnonymousClass0Ej.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = A04(AnonymousClass1YA.A00(applicationInjector), AnonymousClass0XP.A00(applicationInjector), A02(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final C02370Ek A07(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C02370Ek.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C02370Ek(A0A(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C02400En A09(AnonymousClass1XY r3) {
        if (A04 == null) {
            synchronized (C02400En.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r3);
                if (A002 != null) {
                    try {
                        A04 = A01(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r7.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0051, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C02380El A0B(X.AnonymousClass1XY r9) {
        /*
            X.0El r0 = X.AnonymousClass0Ei.A05
            if (r0 != 0) goto L_0x005a
            java.lang.Class<X.0El> r8 = X.C02380El.class
            monitor-enter(r8)
            X.0El r0 = X.AnonymousClass0Ei.A05     // Catch:{ all -> 0x0057 }
            X.0WD r7 = X.AnonymousClass0WD.A00(r0, r9)     // Catch:{ all -> 0x0057 }
            if (r7 == 0) goto L_0x0055
            X.1XY r0 = r9.getApplicationInjector()     // Catch:{ all -> 0x004d }
            android.content.Context r6 = X.AnonymousClass1YA.A02(r0)     // Catch:{ all -> 0x004d }
            X.0Em r5 = A01(r0)     // Catch:{ all -> 0x004d }
            com.facebook.quicklog.QuickPerformanceLogger r4 = X.AnonymousClass0ZD.A03(r0)     // Catch:{ all -> 0x004d }
            X.0Ep r3 = A02(r0)     // Catch:{ all -> 0x004d }
            java.lang.Class<X.0El> r2 = X.C02380El.class
            monitor-enter(r2)     // Catch:{ all -> 0x004d }
            X.0El r1 = X.C02380El.A03     // Catch:{ all -> 0x004a }
            if (r1 == 0) goto L_0x0036
            X.0En r0 = r1.A01     // Catch:{ all -> 0x004a }
            if (r0 == r5) goto L_0x0044
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x004a }
            java.lang.String r0 = "Different VoltronModuleLoaders detected!"
            r1.<init>(r0)     // Catch:{ all -> 0x004a }
            throw r1     // Catch:{ all -> 0x004a }
        L_0x0036:
            android.content.Context r0 = r6.getApplicationContext()     // Catch:{ all -> 0x004a }
            X.0El r1 = new X.0El     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x003f
            r6 = r0
        L_0x003f:
            r1.<init>(r6, r5, r3)     // Catch:{ all -> 0x004a }
            X.C02380El.A03 = r1     // Catch:{ all -> 0x004a }
        L_0x0044:
            monitor-exit(r2)     // Catch:{ all -> 0x004d }
            r5.A00 = r4     // Catch:{ all -> 0x004d }
            X.AnonymousClass0Ei.A05 = r1     // Catch:{ all -> 0x004d }
            goto L_0x0052
        L_0x004a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004d }
            throw r0     // Catch:{ all -> 0x004d }
        L_0x004d:
            r0 = move-exception
            r7.A01()     // Catch:{ all -> 0x0057 }
            throw r0     // Catch:{ all -> 0x0057 }
        L_0x0052:
            r7.A01()     // Catch:{ all -> 0x0057 }
        L_0x0055:
            monitor-exit(r8)     // Catch:{ all -> 0x0057 }
            goto L_0x005a
        L_0x0057:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0057 }
            throw r0
        L_0x005a:
            X.0El r0 = X.AnonymousClass0Ei.A05
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Ei.A0B(X.1XY):X.0El");
    }

    private static final C02390Em A01(AnonymousClass1XY r0) {
        return A00(r0);
    }

    public static final AnonymousClass0Ep A02(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public static final AnonymousClass0Ej A05(AnonymousClass1XY r0) {
        return A06(r0);
    }

    public static final C02400En A08(AnonymousClass1XY r0) {
        return A09(r0);
    }

    public static final C02380El A0A(AnonymousClass1XY r0) {
        return A0B(r0);
    }

    public static final String A0C(AnonymousClass1XY r1) {
        String str = AnonymousClass1YA.A02(r1).getApplicationInfo().dataDir;
        try {
            String realpath = DalvikInternals.realpath(str);
            if (realpath != null) {
                return realpath;
            }
            return str;
        } catch (IOException | UnsatisfiedLinkError unused) {
        }
    }
}
