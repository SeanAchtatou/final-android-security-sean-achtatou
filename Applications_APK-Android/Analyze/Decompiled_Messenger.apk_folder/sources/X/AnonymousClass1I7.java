package X;

/* renamed from: X.1I7  reason: invalid class name */
public enum AnonymousClass1I7 implements AnonymousClass1JL {
    HEADLINE(2132148409, 28),
    XXLARGE(2132148321, 24),
    XLARGE(2132148317, 18),
    LARGE(2132148267, 16),
    MEDIUM(2132148245, 14),
    SMALL_MEDIUM(2132148286, 13),
    SMALL(2132148266, 12);
    
    private final int textSizeResId;
    private final int textSizeSp;

    public int B5j() {
        return this.textSizeResId;
    }

    public int B5k() {
        return this.textSizeSp;
    }

    private AnonymousClass1I7(int i, int i2) {
        this.textSizeResId = i;
        this.textSizeSp = i2;
    }
}
