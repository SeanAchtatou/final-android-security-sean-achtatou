package X;

import java.util.Collections;
import java.util.Map;

/* renamed from: X.123  reason: invalid class name */
public final class AnonymousClass123 {
    private Map A00 = null;
    public final Boolean A01;
    public final Boolean A02;
    public final Boolean A03;
    public final Boolean A04;
    public final Double A05;
    public final Double A06;
    public final Double A07;
    public final Integer A08;
    public final Integer A09;
    public final Integer A0A;
    public final Integer A0B;
    public final Long A0C;
    public final Long A0D;
    public final Long A0E;
    public final Long A0F;
    public final Long A0G;
    public final Long A0H;
    public final Long A0I;
    public final Long A0J;
    public final String A0K;
    public final String A0L;
    public final String A0M;

    public synchronized Map A01() {
        Map map;
        map = this.A00;
        if (map == null) {
            this.A00 = Collections.synchronizedMap(new AnonymousClass04a());
            A00("network_info_rtt_avg", this.A0J);
            A00("network_info_rtt_stddev", this.A07);
            A00("network_info_network_changed", this.A03);
            A00("network_info_celltower_changed", this.A02);
            A00("network_info_opened_conn", this.A0G);
            A00("network_info_signal_level", this.A0B);
            A00("network_info_signal_dbm", this.A0A);
            A00("network_info_frequency_mhz", this.A08);
            A00("network_info_link_speed_mbps", this.A09);
            A00("network_info_app_backgrounded", this.A01);
            A00("network_info_ms_since_launch", this.A0F);
            A00("network_info_ms_since_init", this.A0E);
            A00("network_info_may_have_network", this.A04);
            A00("network_info_req_bw_ingress_avg", this.A0I);
            A00("network_info_req_bw_ingress_std_dev", this.A06);
            A00("network_info_req_bw_egress_avg", this.A0H);
            A00("network_info_req_bw_egress_std_dev", this.A05);
            A00("network_info_latency_quality", this.A0L);
            A00("network_info_upload_bw_quality", this.A0M);
            A00("network_info_download_bw_quality", this.A0K);
            A00("estimated_ttfb_ms", this.A0D);
            A00("estimated_bandwidth_bps", this.A0C);
            map = this.A00;
        }
        return map;
    }

    private void A00(String str, Object obj) {
        if (obj != null) {
            this.A00.put(str, String.valueOf(obj));
        }
    }

    public AnonymousClass123(Long l, Double d, Boolean bool, Boolean bool2, Long l2, Integer num, Integer num2, Integer num3, Integer num4, Boolean bool3, Long l3, Long l4, Boolean bool4, Long l5, Double d2, Long l6, Double d3, String str, String str2, String str3, Long l7, Long l8) {
        this.A0J = l;
        this.A07 = d;
        this.A03 = bool;
        this.A02 = bool2;
        this.A0G = l2;
        this.A0B = num;
        this.A0A = num2;
        this.A08 = num3;
        this.A09 = num4;
        this.A01 = bool3;
        this.A0F = l3;
        this.A0E = l4;
        this.A04 = bool4;
        this.A0I = l5;
        this.A06 = d2;
        this.A0H = l6;
        this.A05 = d3;
        this.A0L = str;
        this.A0M = str2;
        this.A0K = str3;
        this.A0D = l7;
        this.A0C = l8;
    }
}
