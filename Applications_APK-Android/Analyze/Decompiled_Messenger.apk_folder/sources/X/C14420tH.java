package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;

@UserScoped
/* renamed from: X.0tH  reason: invalid class name and case insensitive filesystem */
public final class C14420tH implements OmnistoreComponent {
    private static C05540Zi A06;
    private AnonymousClass0UN A00;
    public final AnonymousClass1Y6 A01;
    private final AnonymousClass0tK A02;
    private final C04310Tq A03;
    public volatile C193117q A04;
    public volatile C193117q A05;

    public void Boz(int i) {
    }

    public String getCollectionLabel() {
        return "messenger_bannertriggers";
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static final C14420tH A00(AnonymousClass1XY r7) {
        C14420tH r0;
        synchronized (C14420tH.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r7)) {
                    AnonymousClass1XY r5 = (AnonymousClass1XY) A06.A01();
                    A06.A00 = new C14420tH(r5, AnonymousClass0XJ.A0L(r5), AnonymousClass0UX.A07(r5), AnonymousClass0tK.A00(r5));
                }
                C05540Zi r1 = A06;
                r0 = (C14420tH) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0032, code lost:
        if (r4 != null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0037, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCollectionAvailable(com.facebook.omnistore.Collection r6) {
        /*
            r5 = this;
            java.lang.String r2 = ""
            r1 = -1
            r0 = 1
            com.facebook.omnistore.Cursor r4 = r6.query(r2, r1, r0)
        L_0x0008:
            boolean r0 = r4.step()     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x002b
            X.0tK r3 = r5.A02     // Catch:{ all -> 0x002f }
            java.lang.String r2 = r4.getPrimaryKey()     // Catch:{ all -> 0x002f }
            java.lang.String r0 = r4.getSortKey()     // Catch:{ all -> 0x002f }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x002f }
            java.lang.Long r1 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x002f }
            monitor-enter(r3)     // Catch:{ all -> 0x002f }
            java.util.Map r0 = r3.A00     // Catch:{ all -> 0x0028 }
            r0.put(r2, r1)     // Catch:{ all -> 0x0028 }
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
            goto L_0x0008
        L_0x0028:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
            throw r0     // Catch:{ all -> 0x002f }
        L_0x002b:
            r4.close()
            return
        L_0x002f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            if (r4 == 0) goto L_0x0037
            r4.close()     // Catch:{ all -> 0x0037 }
        L_0x0037:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14420tH.onCollectionAvailable(com.facebook.omnistore.Collection):void");
    }

    public void onCollectionInvalidated() {
        AnonymousClass0tK r1 = this.A02;
        synchronized (r1) {
            r1.A00.clear();
        }
    }

    public C32171lK provideSubscriptionInfo(Omnistore omnistore) {
        if (!((AnonymousClass1YI) AnonymousClass1XX.A03(AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A1D, false)) {
            return C32171lK.A03;
        }
        CollectionName.Builder createCollectionNameBuilder = omnistore.createCollectionNameBuilder(getCollectionLabel());
        createCollectionNameBuilder.addSegment((String) this.A03.get());
        return C32171lK.A00(createCollectionNameBuilder.build(), null);
    }

    private C14420tH(AnonymousClass1XY r3, C04310Tq r4, AnonymousClass1Y6 r5, AnonymousClass0tK r6) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A03 = r4;
        this.A01 = r5;
        this.A02 = r6;
    }

    public void BVs(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Delta delta = (Delta) it.next();
            if (delta.getType() != 2) {
                AnonymousClass0tK r3 = this.A02;
                String primaryKey = delta.getPrimaryKey();
                Long valueOf = Long.valueOf(Long.parseLong(delta.getSortKey()));
                synchronized (r3) {
                    r3.A00.put(primaryKey, valueOf);
                }
                if (this.A05 != null || this.A04 != null) {
                    this.A01.C4C(new C84413zQ(this));
                }
            }
        }
    }
}
