package X;

import com.facebook.profilo.multiprocess.ProfiloMultiProcessTraceListenerImpl;

/* renamed from: X.0Jr  reason: invalid class name */
public final class AnonymousClass0Jr extends C011609d {
    private final ProfiloMultiProcessTraceListenerImpl A00;
    private final AnonymousClass0Ra A01;

    public void BkP(int i) {
    }

    public void onTraceWriteAbort(long j, int i) {
        this.A00.onTraceWriteAbort(j, i);
    }

    public void onTraceWriteEnd(long j, int i) {
        this.A00.onTraceWriteEnd(j, i);
    }

    public void onTraceWriteStart(long j, int i, String str) {
        this.A00.onTraceWriteStart(j, i, str);
    }

    public AnonymousClass0Jr(String str) {
        ProfiloMultiProcessTraceListenerImpl profiloMultiProcessTraceListenerImpl = new ProfiloMultiProcessTraceListenerImpl();
        this.A00 = profiloMultiProcessTraceListenerImpl;
        this.A01 = new AnonymousClass0Ra(str, profiloMultiProcessTraceListenerImpl);
    }

    public AnonymousClass07D A00() {
        return this.A01;
    }
}
