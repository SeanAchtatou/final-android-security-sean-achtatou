package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1WQ  reason: invalid class name */
public final class AnonymousClass1WQ implements Collection<V> {
    public final /* synthetic */ C04180St A00;

    public AnonymousClass1WQ(C04180St r1) {
        this.A00 = r1;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.A00.A08();
    }

    public boolean contains(Object obj) {
        if (this.A00.A04(obj) >= 0) {
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        if (this.A00.A02() == 0) {
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new C47662Xi(this.A00, 1);
    }

    public boolean remove(Object obj) {
        int A04 = this.A00.A04(obj);
        if (A04 < 0) {
            return false;
        }
        this.A00.A09(A04);
        return true;
    }

    public boolean removeAll(Collection collection) {
        int A02 = this.A00.A02();
        int i = 0;
        boolean z = false;
        while (i < A02) {
            if (collection.contains(this.A00.A05(i, 1))) {
                this.A00.A09(i);
                i--;
                A02--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public boolean retainAll(Collection collection) {
        int A02 = this.A00.A02();
        int i = 0;
        boolean z = false;
        while (i < A02) {
            if (!collection.contains(this.A00.A05(i, 1))) {
                this.A00.A09(i);
                i--;
                A02--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public int size() {
        return this.A00.A02();
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public Object[] toArray() {
        C04180St r5 = this.A00;
        int A02 = r5.A02();
        Object[] objArr = new Object[A02];
        for (int i = 0; i < A02; i++) {
            objArr[i] = r5.A05(i, 1);
        }
        return objArr;
    }

    public Object[] toArray(Object[] objArr) {
        return this.A00.A0B(objArr, 1);
    }
}
