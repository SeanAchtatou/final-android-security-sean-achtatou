package X;

import io.card.payment.BuildConfig;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1YS  reason: invalid class name */
public final class AnonymousClass1YS {
    private AnonymousClass1YT A00;
    public final AnonymousClass1YZ A01;
    public final ConcurrentLinkedQueue A02 = new ConcurrentLinkedQueue();
    public final AtomicInteger A03 = new AtomicInteger();
    public final boolean A04;
    public volatile boolean A05;

    private static AnonymousClass1ZX A00(int i, C25261Yy r4) {
        AnonymousClass1ZW r0;
        AnonymousClass1ZP r1 = new AnonymousClass1ZP();
        r1.A01 = String.format(r4.A01(i), new Object[0]);
        r1.A00 = new AnonymousClass1ZV(r4, i);
        r1.A01 = r4.A03;
        r1.A02 = Integer.valueOf(i);
        r1.A00(null);
        if (r4.A02) {
            r0 = AnonymousClass1ZW.A02;
        } else {
            r0 = AnonymousClass1ZW.A01;
        }
        r1.A00(r0);
        if (r1.A00 != null) {
            String str = r1.A01;
            if (str != null && str.length() != 0) {
                return new AnonymousClass1ZX(r1);
            }
            throw new IllegalArgumentException("No description was supplied to JobOrchestrator");
        }
        throw new IllegalArgumentException("No runnable was supplied to JobOrchestrator");
    }

    private void A02(C25261Yy r9, int i, boolean z, int[] iArr) {
        if (z) {
            AnonymousClass1YT r7 = this.A00;
            AnonymousClass1ZX A002 = A00(i, r9);
            if (iArr != null) {
                for (int i2 : iArr) {
                    AnonymousClass1YY r2 = (AnonymousClass1YY) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTd, r7.A00);
                    int i3 = A002.A02;
                    AnonymousClass1YY.A01(i3, i2, r2.A01);
                    AnonymousClass1YY.A01(i2, i3, r2.A02);
                }
            }
            r7.A01(A002);
            return;
        }
        this.A00.A01(A00(i, r9));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        if (r9.A01.A00.Aem(285224484935202L) == false) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.AnonymousClass1YS r9) {
        /*
            java.util.concurrent.ConcurrentLinkedQueue r0 = r9.A02
            java.lang.Object r4 = r0.peek()
            X.1Z0 r4 = (X.AnonymousClass1Z0) r4
            r8 = 2
            if (r4 != 0) goto L_0x0010
            boolean r0 = X.C010708t.A0X(r8)
        L_0x000f:
            return
        L_0x0010:
            X.1Yy r6 = r4.A01
            boolean r0 = r6.A05()
            if (r0 != 0) goto L_0x0021
            java.util.concurrent.ConcurrentLinkedQueue r0 = r9.A02
            r0.poll()
            A03(r9)
            return
        L_0x0021:
            int[] r0 = r4.A03
            r5 = 0
            if (r0 == 0) goto L_0x007f
            int r1 = r0.length
            monitor-enter(r4)
            java.util.HashSet r0 = new java.util.HashSet     // Catch:{ all -> 0x00a4 }
            r0.<init>(r1)     // Catch:{ all -> 0x00a4 }
            r4.A00 = r0     // Catch:{ all -> 0x00a4 }
            monitor-exit(r4)
            boolean r0 = r6.A04()
            if (r0 == 0) goto L_0x0046
            X.1YZ r0 = r9.A01
            X.1Yd r2 = r0.A00
            r0 = 285224484935202(0x10369001b1622, double:1.409196193592475E-309)
            boolean r0 = r2.Aem(r0)
            r7 = 1
            if (r0 != 0) goto L_0x0047
        L_0x0046:
            r7 = 0
        L_0x0047:
            int[] r3 = r4.A03
            int r2 = r3.length
        L_0x004a:
            if (r5 >= r2) goto L_0x009d
            r1 = r3[r5]
            r0 = 27
            if (r1 == r0) goto L_0x0078
            switch(r1) {
                case 130: goto L_0x0063;
                case 131: goto L_0x006a;
                case 132: goto L_0x0071;
                default: goto L_0x0055;
            }
        L_0x0055:
            r0 = 0
        L_0x0056:
            if (r7 == 0) goto L_0x005a
            if (r0 != 0) goto L_0x005d
        L_0x005a:
            r4.A00(r1)
        L_0x005d:
            r9.A02(r6, r1, r7, r0)
            int r5 = r5 + 1
            goto L_0x004a
        L_0x0063:
            r0 = 129(0x81, float:1.81E-43)
            int[] r0 = new int[]{r0}
            goto L_0x0056
        L_0x006a:
            r0 = 130(0x82, float:1.82E-43)
            int[] r0 = new int[]{r0}
            goto L_0x0056
        L_0x0071:
            r0 = 131(0x83, float:1.84E-43)
            int[] r0 = new int[]{r0}
            goto L_0x0056
        L_0x0078:
            r0 = 39
            int[] r0 = new int[]{r0}
            goto L_0x0056
        L_0x007f:
            int r3 = r6.A00()
            monitor-enter(r4)
            java.util.HashSet r0 = new java.util.HashSet     // Catch:{ all -> 0x00a4 }
            r0.<init>(r3)     // Catch:{ all -> 0x00a4 }
            r4.A00 = r0     // Catch:{ all -> 0x00a4 }
            monitor-exit(r4)
            r2 = 0
        L_0x008d:
            if (r2 >= r3) goto L_0x009d
            r1 = 0
            int r0 = X.C94374eP.A00(r1)
            r4.A00(r0)
            r9.A02(r6, r0, r5, r1)
            int r2 = r2 + 1
            goto L_0x008d
        L_0x009d:
            boolean r0 = X.C010708t.A0X(r8)
            if (r0 == 0) goto L_0x000f
            return
        L_0x00a4:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YS.A03(X.1YS):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r0 != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(X.AnonymousClass1YS r0, java.lang.String r1) {
        /*
            boolean r0 = r0.A04
            if (r0 != 0) goto L_0x0019
            java.lang.String r0 = "app_foregrounded"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = "app_backgrounded"
            boolean r0 = r0.equals(r1)
            r1 = 0
            if (r0 == 0) goto L_0x0016
        L_0x0015:
            r1 = 1
        L_0x0016:
            r0 = 1
            if (r1 != 0) goto L_0x001a
        L_0x0019:
            r0 = 0
        L_0x001a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YS.A04(X.1YS, java.lang.String):boolean");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0071, code lost:
        if (r14.equals("app_foreground_report_time_spent_only") == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007b, code lost:
        if (r14.equals("device_locale_changed") == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0085, code lost:
        if (r14.equals("app_foregrounded_immediate") == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008f, code lost:
        if (r14.equals("app_background_report_time_spent_only") == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0099, code lost:
        if (r14.equals("app_backgrounded") == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a3, code lost:
        if (r14.equals("app_locale_changed") == false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00cb, code lost:
        if (r14.equals("action_network_connectivity_changed") == false) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d5, code lost:
        if (r14.equals("login_complete") == false) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00df, code lost:
        if (r14.equals("application_init") == false) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e9, code lost:
        if (r14.equals("app_backgrounded") == false) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f3, code lost:
        if (r14.equals("app_foregrounded") == false) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0138, code lost:
        if (r14.equals("application_init") == false) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018f, code lost:
        if (r14.equals("app_foregrounded") == false) goto L_0x0191;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.C25221Yu r27, java.lang.String r28, int r29, long r30, android.os.Bundle r32) {
        /*
            r26 = this;
            if (r29 == 0) goto L_0x01e1
            r2 = r26
            r14 = r28
            X.1XV r17 = A01(r14, r2)
            int r1 = X.AnonymousClass1Y3.AFH
            r5 = r27
            X.0UN r0 = r5.A00
            java.lang.Object r12 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Yw r12 = (X.C25241Yw) r12
            X.1Yx r11 = new X.1Yx
            r13 = r2
            r15 = r30
            r11.<init>(r12, r13, r14, r15, r17)
            boolean r0 = r2.A04
            r11.A02 = r0
            X.1Z0 r3 = new X.1Z0
            if (r0 == 0) goto L_0x00a6
            int r0 = r14.hashCode()
            r9 = 3
            r8 = 5
            r7 = 4
            r6 = 0
            r4 = 2
            r1 = 1
            switch(r0) {
                case -1814449310: goto L_0x006a;
                case -1561053480: goto L_0x0074;
                case -494931790: goto L_0x007e;
                case -197161193: goto L_0x0088;
                case 130218923: goto L_0x0092;
                case 757558189: goto L_0x009c;
                default: goto L_0x0033;
            }
        L_0x0033:
            r10 = -1
        L_0x0034:
            r0 = 6
            if (r10 == 0) goto L_0x0065
            if (r10 == r1) goto L_0x005f
            if (r10 == r4) goto L_0x0059
            if (r10 == r9) goto L_0x0054
            if (r10 == r7) goto L_0x004e
            if (r10 != r8) goto L_0x00f6
            int[] r0 = new int[r0]
            r0 = {2, 12, 15, 86, 106, 110} // fill-array
        L_0x0046:
            r3.<init>(r14, r11, r0)
            java.util.concurrent.ConcurrentLinkedQueue r4 = r2.A02
            monitor-enter(r4)
            goto L_0x011c
        L_0x004e:
            int[] r0 = new int[r4]
            r0 = {56, 138} // fill-array
            goto L_0x0046
        L_0x0054:
            int[] r0 = new int[]{r8}
            goto L_0x0046
        L_0x0059:
            int[] r0 = new int[r4]
            r0 = {56, 138} // fill-array
            goto L_0x0046
        L_0x005f:
            int[] r0 = new int[r7]
            r0 = {3, 13, 83, 88} // fill-array
            goto L_0x0046
        L_0x0065:
            int[] r0 = new int[]{r0}
            goto L_0x0046
        L_0x006a:
            java.lang.String r0 = "app_foreground_report_time_spent_only"
            boolean r0 = r14.equals(r0)
            r10 = 3
            if (r0 != 0) goto L_0x0034
            goto L_0x0033
        L_0x0074:
            java.lang.String r0 = "device_locale_changed"
            boolean r0 = r14.equals(r0)
            r10 = 4
            if (r0 != 0) goto L_0x0034
            goto L_0x0033
        L_0x007e:
            java.lang.String r0 = "app_foregrounded_immediate"
            boolean r0 = r14.equals(r0)
            r10 = 5
            if (r0 != 0) goto L_0x0034
            goto L_0x0033
        L_0x0088:
            java.lang.String r0 = "app_background_report_time_spent_only"
            boolean r0 = r14.equals(r0)
            r10 = 0
            if (r0 != 0) goto L_0x0034
            goto L_0x0033
        L_0x0092:
            java.lang.String r0 = "app_backgrounded"
            boolean r0 = r14.equals(r0)
            r10 = 1
            if (r0 != 0) goto L_0x0034
            goto L_0x0033
        L_0x009c:
            java.lang.String r0 = "app_locale_changed"
            boolean r0 = r14.equals(r0)
            r10 = 2
            if (r0 != 0) goto L_0x0034
            goto L_0x0033
        L_0x00a6:
            int r0 = r14.hashCode()
            r8 = 4
            r7 = 3
            r6 = 0
            r4 = 2
            r1 = 1
            switch(r0) {
                case -1466853626: goto L_0x00c4;
                case -1210009265: goto L_0x00ce;
                case 453279: goto L_0x00d8;
                case 130218923: goto L_0x00e2;
                case 566124160: goto L_0x00ec;
                default: goto L_0x00b2;
            }
        L_0x00b2:
            r9 = -1
        L_0x00b3:
            if (r9 == 0) goto L_0x0115
            if (r9 == r1) goto L_0x010c
            if (r9 == r4) goto L_0x0103
            if (r9 == r7) goto L_0x00fa
            if (r9 != r8) goto L_0x00f6
            r0 = 114(0x72, float:1.6E-43)
            int[] r0 = new int[]{r0}
            goto L_0x0046
        L_0x00c4:
            java.lang.String r0 = "action_network_connectivity_changed"
            boolean r0 = r14.equals(r0)
            r9 = 0
            if (r0 != 0) goto L_0x00b3
            goto L_0x00b2
        L_0x00ce:
            java.lang.String r0 = "login_complete"
            boolean r0 = r14.equals(r0)
            r9 = 4
            if (r0 != 0) goto L_0x00b3
            goto L_0x00b2
        L_0x00d8:
            java.lang.String r0 = "application_init"
            boolean r0 = r14.equals(r0)
            r9 = 2
            if (r0 != 0) goto L_0x00b3
            goto L_0x00b2
        L_0x00e2:
            java.lang.String r0 = "app_backgrounded"
            boolean r0 = r14.equals(r0)
            r9 = 1
            if (r0 != 0) goto L_0x00b3
            goto L_0x00b2
        L_0x00ec:
            java.lang.String r0 = "app_foregrounded"
            boolean r0 = r14.equals(r0)
            r9 = 3
            if (r0 != 0) goto L_0x00b3
            goto L_0x00b2
        L_0x00f6:
            int[] r0 = new int[r6]
            goto L_0x0046
        L_0x00fa:
            r0 = 58
            int[] r0 = new int[r0]
            r0 = {0, 8, 10, 14, 17, 19, 23, 25, 29, 30, 31, 33, 36, 37, 41, 42, 45, 46, 52, 54, 57, 59, 61, 63, 68, 69, 71, 75, 76, 77, 78, 81, 85, 87, 90, 91, 94, 95, 101, 103, 107, 109, 111, 115, 117, 119, 120, 122, 125, 127, 128, 134, 136, 140, 141, 142, 143, 145} // fill-array
            goto L_0x0046
        L_0x0103:
            r0 = 25
            int[] r0 = new int[r0]
            r0 = {4, 7, 9, 28, 39, 27, 44, 49, 50, 53, 67, 72, 74, 80, 82, 113, 124, 129, 130, 131, 132, 133, 137, 144, 147} // fill-array
            goto L_0x0046
        L_0x010c:
            r0 = 51
            int[] r0 = new int[r0]
            r0 = {1, 11, 14, 16, 18, 20, 21, 22, 24, 26, 31, 32, 34, 35, 36, 38, 40, 43, 47, 51, 55, 58, 60, 62, 64, 65, 66, 70, 73, 79, 84, 89, 92, 93, 96, 97, 98, 99, 100, 102, 104, 105, 108, 112, 116, 118, 121, 123, 126, 135, 146} // fill-array
            goto L_0x0046
        L_0x0115:
            int[] r0 = new int[r4]
            r0 = {48, 139} // fill-array
            goto L_0x0046
        L_0x011c:
            java.util.concurrent.ConcurrentLinkedQueue r0 = r2.A02     // Catch:{ all -> 0x01de }
            boolean r1 = r0.isEmpty()     // Catch:{ all -> 0x01de }
            java.util.concurrent.ConcurrentLinkedQueue r0 = r2.A02     // Catch:{ all -> 0x01de }
            r0.add(r3)     // Catch:{ all -> 0x01de }
            monitor-exit(r4)     // Catch:{ all -> 0x01de }
            if (r1 == 0) goto L_0x012d
            A03(r2)
        L_0x012d:
            boolean r0 = r2.A04
            if (r0 != 0) goto L_0x013a
            java.lang.String r0 = "application_init"
            boolean r1 = r14.equals(r0)
            r0 = 1
            if (r1 != 0) goto L_0x013b
        L_0x013a:
            r0 = 0
        L_0x013b:
            if (r0 == 0) goto L_0x0180
            X.1XV r23 = A01(r14, r2)
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r5.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A03(r1, r0)
            X.069 r6 = (X.AnonymousClass069) r6
            int r1 = X.AnonymousClass1Y3.BJm
            X.0UN r0 = r5.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Zi r4 = (X.C25361Zi) r4
            int r1 = X.AnonymousClass1Y3.B6J
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Zj r0 = (X.C25371Zj) r0
            X.1Z3 r3 = new X.1Z3
            r19 = r2
            r20 = r14
            r18 = r0
            r21 = r15
            r24 = r6
            r25 = r4
            r17 = r3
            r17.<init>(r18, r19, r20, r21, r23, r24, r25)
            r0 = 0
            r3.A02 = r0
            X.1Z0 r1 = new X.1Z0
            r0 = 0
            r1.<init>(r14, r3, r0)
            java.util.concurrent.ConcurrentLinkedQueue r0 = r2.A02
            r0.add(r1)
        L_0x0180:
            boolean r0 = r2.A04
            if (r0 != 0) goto L_0x0191
            boolean r0 = r2.A05
            if (r0 != 0) goto L_0x0191
            java.lang.String r0 = "app_foregrounded"
            boolean r1 = r14.equals(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0192
        L_0x0191:
            r0 = 0
        L_0x0192:
            if (r0 == 0) goto L_0x01d2
            r0 = 1
            r2.A05 = r0
            X.1XV r11 = A01(r14, r2)
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r5.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r1, r0)
            X.069 r4 = (X.AnonymousClass069) r4
            int r1 = X.AnonymousClass1Y3.BJm
            X.0UN r0 = r5.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Zi r3 = (X.C25361Zi) r3
            int r1 = X.AnonymousClass1Y3.Ams
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.1kM r0 = (X.C31771kM) r0
            X.1Z1 r5 = new X.1Z1
            r7 = r2
            r8 = r14
            r6 = r0
            r9 = r15
            r12 = r4
            r13 = r3
            r5.<init>(r6, r7, r8, r9, r11, r12, r13)
            r0 = 0
            r5.A02 = r0
            X.1Z0 r1 = new X.1Z0
            r0 = 0
            r1.<init>(r14, r5, r0)
            java.util.concurrent.ConcurrentLinkedQueue r0 = r2.A02
            r0.add(r1)
        L_0x01d2:
            boolean r0 = A04(r2, r14)
            if (r0 == 0) goto L_0x01e1
            java.util.concurrent.atomic.AtomicInteger r0 = r2.A03
            r0.incrementAndGet()
            return
        L_0x01de:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x01de }
            throw r0
        L_0x01e1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YS.A05(X.1Yu, java.lang.String, int, long, android.os.Bundle):void");
    }

    public AnonymousClass1YS(AnonymousClass1YT r2, boolean z, AnonymousClass1YZ r4) {
        this.A00 = r2;
        this.A04 = z;
        this.A01 = r4;
    }

    private static AnonymousClass1XV A01(String str, AnonymousClass1YS r3) {
        String str2;
        if (!C25231Yv.A01()) {
            return null;
        }
        if (r3.A04) {
            str2 = "_run_immediately";
        } else {
            str2 = BuildConfig.FLAVOR;
        }
        return C27401d8.A01(AnonymousClass08S.A0P("appjob_trigger_", str, str2), 4);
    }
}
