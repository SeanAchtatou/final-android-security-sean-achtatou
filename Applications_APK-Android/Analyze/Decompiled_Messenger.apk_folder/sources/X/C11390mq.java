package X;

import com.fasterxml.jackson.databind.JsonSerializer;
import java.util.HashMap;

/* renamed from: X.0mq  reason: invalid class name and case insensitive filesystem */
public final class C11390mq {
    public C44662Jb _readOnlyMap = null;
    public HashMap _sharedMap = new HashMap(64);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.4Gr.<init>(X.0jR, boolean):void
     arg types: [X.0jR, int]
     candidates:
      X.4Gr.<init>(java.lang.Class, boolean):void
      X.4Gr.<init>(X.0jR, boolean):void */
    public JsonSerializer untypedValueSerializer(C10030jR r4) {
        JsonSerializer jsonSerializer;
        synchronized (this) {
            jsonSerializer = (JsonSerializer) this._sharedMap.get(new C87814Gr(r4, false));
        }
        return jsonSerializer;
    }
}
