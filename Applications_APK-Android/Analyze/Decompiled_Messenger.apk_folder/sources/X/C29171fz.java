package X;

import java.io.Serializable;
import java.util.HashMap;

/* renamed from: X.1fz  reason: invalid class name and case insensitive filesystem */
public final class C29171fz implements Serializable {
    private static final long serialVersionUID = 1;
    public final Class _enumClass;
    public final Enum[] _enums;
    public final HashMap _enumsById;

    public C29171fz(Class cls, Enum[] enumArr, HashMap hashMap) {
        this._enumClass = cls;
        this._enums = enumArr;
        this._enumsById = hashMap;
    }
}
