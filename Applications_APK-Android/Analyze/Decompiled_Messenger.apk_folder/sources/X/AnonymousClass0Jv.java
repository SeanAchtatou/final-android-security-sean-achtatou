package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0Jv  reason: invalid class name */
public final class AnonymousClass0Jv implements FilenameFilter {
    public boolean accept(File file, String str) {
        return str.endsWith("_entity");
    }
}
