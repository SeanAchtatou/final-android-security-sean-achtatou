package X;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.14M  reason: invalid class name */
public final class AnonymousClass14M implements C05460Za {
    public static final ImmutableList A08 = ImmutableList.of("AttributionIdManagerListenerManager");
    private static volatile AnonymousClass14M A09;
    public AnonymousClass43N A00;
    public AnonymousClass2wE A01;
    public AnonymousClass0UN A02;
    public String A03;
    public Map A04 = new HashMap();
    public boolean A05;
    public final AnonymousClass1YL A06;
    public final LinkedList A07 = new LinkedList();

    public static final AnonymousClass14M A00(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (AnonymousClass14M.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new AnonymousClass14M(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static synchronized void A01(AnonymousClass14M r18) {
        AnonymousClass14M r1 = r18;
        synchronized (r1) {
            if (!r1.A05) {
                r1.A05 = true;
                r1.A07.clear();
                r1.A07.add(new AnonymousClass2wE(Long.toString(986244814899307L), null, 0, C12350pC.A04(AnonymousClass06A.A00.now()), "logout", "login", null, null, null, null, null, false, null, null));
                r1.A00 = null;
                r1.A01 = null;
                r1.A03 = ((C07790eA) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APk, r1.A02)).A07().toString();
            }
        }
    }

    public static void A02(AnonymousClass14M r25, String str) {
        AnonymousClass2wE r4;
        String str2 = str;
        if (str2.equals("from_other_app")) {
            r4 = new AnonymousClass2wE(null, null, 0, C12350pC.A04(AnonymousClass06A.A00.now()), str2, "external link (null)", null, null, null, null, null, false, null, null);
        } else if (str2.equals(AnonymousClass24B.$const$string(AnonymousClass1Y3.ABi))) {
            r4 = new AnonymousClass2wE(Long.toString(1603421209951282L), null, 0, C12350pC.A04(AnonymousClass06A.A00.now()), str2, "push notification", null, null, null, null, null, false, null, null);
        } else {
            r4 = null;
        }
        if (r4 != null) {
            AnonymousClass14M r3 = r25;
            r3.A07.clear();
            r3.A07.add(r4);
            r3.A00 = null;
            r3.A01 = null;
            r3.A03 = ((C07790eA) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APk, r3.A02)).A07().toString();
        }
    }

    public JsonNode A03() {
        LinkedList linkedList;
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, this.A02)).AbO(542, true)) {
            return null;
        }
        synchronized (this) {
            linkedList = new LinkedList(this.A07);
        }
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ObjectNode objectNode = new ObjectNode(jsonNodeFactory);
        for (int i = 0; i < linkedList.size(); i++) {
            AnonymousClass2wE r8 = (AnonymousClass2wE) linkedList.get(i);
            ObjectNode objectNode2 = new ObjectNode(jsonNodeFactory);
            objectNode2.put("bookmark_id", r8.A0A);
            objectNode2.put("session", r8.A08);
            objectNode2.put("subsession", r8.A00);
            objectNode2.put("timestamp", r8.A09);
            objectNode2.put("tap_point", r8.A07);
            objectNode2.put("bookmark_type_name", r8.A06);
            objectNode2.put(C99084oO.$const$string(169), r8.A0D);
            Integer num = r8.A03;
            if (!(num == null && r8.A05 == null)) {
                ObjectNode objectNode3 = new ObjectNode(jsonNodeFactory);
                if (num != null) {
                    objectNode3.put("badge_count", num);
                }
                String str = r8.A05;
                if (str != null) {
                    objectNode3.put("badge_type", str);
                }
                objectNode2.put("badging", objectNode3);
            }
            C97964mI r1 = r8.A01;
            if (!(r1 == null && r8.A02 == null && r8.A04 == null)) {
                ObjectNode objectNode4 = new ObjectNode(jsonNodeFactory);
                if (r1 != null) {
                    objectNode4.put("promo_source", r1.name);
                }
                C860146i r0 = r8.A02;
                if (r0 != null) {
                    objectNode4.put("promo_type", r0.name);
                }
                Long l = r8.A04;
                if (l != null) {
                    objectNode4.put("promo_id", l);
                }
                objectNode2.put("promotion", objectNode4);
            }
            if (!ImmutableMap.copyOf(r8.A0C).isEmpty()) {
                ObjectNode objectNode5 = new ObjectNode(jsonNodeFactory);
                for (Map.Entry entry : ImmutableMap.copyOf(r8.A0C).entrySet()) {
                    if (entry.getValue() instanceof String) {
                        objectNode5.put(((C859445x) entry.getKey()).toString(), (String) entry.getValue());
                    } else if (entry.getValue() instanceof Integer) {
                        objectNode5.put(((C859445x) entry.getKey()).toString(), (Integer) entry.getValue());
                    } else if (entry.getValue() instanceof Double) {
                        objectNode5.put(((C859445x) entry.getKey()).toString(), (Double) entry.getValue());
                    } else if (entry.getValue() instanceof Float) {
                        objectNode5.put(((C859445x) entry.getKey()).toString(), (Float) entry.getValue());
                    } else if (entry.getValue() instanceof Long) {
                        objectNode5.put(((C859445x) entry.getKey()).toString(), (Long) entry.getValue());
                    } else if (entry.getValue() instanceof Boolean) {
                        objectNode5.put(((C859445x) entry.getKey()).toString(), (Boolean) entry.getValue());
                    } else {
                        objectNode5.put(((C859445x) entry.getKey()).toString(), entry.getValue().toString());
                    }
                }
                objectNode2.put("extras", objectNode5);
            }
            objectNode.put(Integer.toString(i), objectNode2);
        }
        return objectNode;
    }

    private AnonymousClass14M(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(4, r3);
        this.A06 = new AnonymousClass14N();
    }

    public void clearUserData() {
        A01(this);
    }
}
