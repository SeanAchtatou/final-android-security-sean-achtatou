package X;

import android.view.MotionEvent;
import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1mK  reason: invalid class name and case insensitive filesystem */
public final class C32781mK {
    private static volatile C32781mK A06;
    public MotionEvent A00;
    public C192917o A01;
    public final AnonymousClass0Ud A02;
    public final C192917o A03 = new C192917o();
    public final C192917o A04 = new C192917o();
    public final QuickPerformanceLogger A05;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short, long):void
     arg types: [int, int, int, long]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long, java.util.concurrent.TimeUnit):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short, long):void */
    public void A01(MotionEvent motionEvent) {
        try {
            if (this.A00 == motionEvent) {
                C192917o r1 = this.A04;
                if (r1.A00 == 0) {
                    long j = r1.A03 - this.A02.A0E;
                    long j2 = this.A04.A03 - this.A02.A0I;
                    C192917o r10 = this.A04;
                    this.A05.markerStart(196636, 0, r10.A02 - (r10.A03 - r10.A01));
                    this.A05.markerAnnotate(196636, 0, "app_uptime_ms", String.valueOf(j));
                    this.A05.markerAnnotate(196636, 0, "activity_uptime_ms", String.valueOf(j2));
                    this.A05.markerEnd(196636, 0, (short) 2, this.A04.A02);
                }
            }
        } finally {
            this.A00 = null;
        }
    }

    public static final C32781mK A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (C32781mK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new C32781mK(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private C32781mK(AnonymousClass1XY r3) {
        this.A05 = AnonymousClass0ZD.A03(r3);
        this.A02 = AnonymousClass0Ud.A00(r3);
        C192917o r0 = this.A04;
        C192917o r1 = this.A03;
        r0.A04 = r1;
        r1.A00 = 0;
    }
}
