package X;

/* renamed from: X.21A  reason: invalid class name */
public final class AnonymousClass21A {
    public final Integer A00;
    public final Integer A01;
    public final Object A02;

    private AnonymousClass21A(Integer num, Integer num2, Object obj) {
        this.A00 = num;
        this.A01 = num2;
        this.A02 = obj;
    }

    public AnonymousClass21A(Integer num, Object obj) {
        this(num, AnonymousClass07B.A00, obj);
    }
}
