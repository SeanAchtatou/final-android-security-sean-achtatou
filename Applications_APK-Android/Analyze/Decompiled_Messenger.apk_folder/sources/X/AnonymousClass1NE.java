package X;

import android.view.View;

/* renamed from: X.1NE  reason: invalid class name */
public final class AnonymousClass1NE {
    public static int A00(int i) {
        int mode = View.MeasureSpec.getMode(i);
        int i2 = Integer.MIN_VALUE;
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                return View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), 0);
            }
            i2 = 1073741824;
            if (mode != 1073741824) {
                throw new IllegalStateException("Unexpected size spec mode");
            }
        }
        return View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), i2);
    }

    private static int A01(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            return Math.min(View.MeasureSpec.getSize(i), i2);
        }
        if (mode == 0) {
            return i2;
        }
        if (mode == 1073741824) {
            return View.MeasureSpec.getSize(i);
        }
        throw new IllegalStateException("Unexpected size spec mode");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0030, code lost:
        if (r3 > r4) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(int r8, int r9, float r10, X.AnonymousClass10L r11) {
        /*
            int r7 = android.view.View.MeasureSpec.getMode(r8)
            int r6 = android.view.View.MeasureSpec.getSize(r8)
            int r5 = android.view.View.MeasureSpec.getMode(r9)
            int r4 = android.view.View.MeasureSpec.getSize(r9)
            float r0 = (float) r6
            float r0 = r0 / r10
            double r0 = (double) r0
            double r0 = java.lang.Math.ceil(r0)
            int r3 = (int) r0
            float r0 = (float) r4
            float r0 = r0 * r10
            double r0 = (double) r0
            double r0 = java.lang.Math.ceil(r0)
            int r2 = (int) r0
            r0 = 0
            if (r7 != 0) goto L_0x002a
            if (r5 != 0) goto L_0x002a
            r11.A01 = r0
            r11.A00 = r0
            return
        L_0x002a:
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r7 != r1) goto L_0x0037
            if (r5 != r1) goto L_0x0037
            if (r3 <= r4) goto L_0x005a
        L_0x0032:
            r11.A01 = r2
            r11.A00 = r4
        L_0x0036:
            return
        L_0x0037:
            r0 = 1073741824(0x40000000, float:2.0)
            if (r7 != r0) goto L_0x0044
            r11.A01 = r6
            if (r5 == 0) goto L_0x0054
            if (r3 <= r4) goto L_0x0054
            r11.A00 = r4
            return
        L_0x0044:
            if (r5 != r0) goto L_0x004f
            r11.A00 = r4
            if (r7 == 0) goto L_0x0057
            if (r2 <= r6) goto L_0x0057
            r11.A01 = r6
            return
        L_0x004f:
            if (r7 == r1) goto L_0x005a
            if (r5 != r1) goto L_0x0036
            goto L_0x0032
        L_0x0054:
            r11.A00 = r3
            return
        L_0x0057:
            r11.A01 = r2
            return
        L_0x005a:
            r11.A01 = r6
            r11.A00 = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1NE.A02(int, int, float, X.10L):void");
    }

    public static void A03(int i, int i2, int i3, int i4, AnonymousClass10L r4) {
        r4.A01 = A01(i, i3);
        r4.A00 = A01(i2, i4);
    }

    public static void A04(int i, int i2, AnonymousClass10L r8) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode == 0 && mode2 == 0) {
            size = 0;
        } else {
            if (mode == 1073741824) {
                r8.A01 = size;
                if (mode2 == Integer.MIN_VALUE) {
                    r8.A00 = Math.min(size, size2);
                    return;
                } else if (mode2 == 0) {
                    r8.A00 = size;
                    return;
                } else if (mode2 == 1073741824) {
                    r8.A00 = size2;
                    return;
                }
            } else if (mode == Integer.MIN_VALUE) {
                if (mode2 == Integer.MIN_VALUE) {
                    size = Math.min(size, size2);
                } else if (mode2 != 0) {
                    if (mode2 == 1073741824) {
                        r8.A00 = size2;
                        r8.A01 = Math.min(size, size2);
                        return;
                    }
                }
            }
            r8.A00 = size2;
            r8.A01 = size2;
            return;
        }
        r8.A01 = size;
        r8.A00 = size;
    }
}
