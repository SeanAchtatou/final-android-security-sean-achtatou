package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1KE  reason: invalid class name */
public final class AnonymousClass1KE {
    private final Map A00 = Collections.synchronizedMap(new HashMap());

    public static AnonymousClass1KE A00(AnonymousClass1KE r4) {
        AnonymousClass1KE r3 = new AnonymousClass1KE();
        if (r4 == null) {
            return r3;
        }
        synchronized (r4.A00) {
            r3.A00.putAll(r4.A00);
        }
        return r3;
    }

    public Object A01(Class cls) {
        return this.A00.get(cls);
    }

    public void A02(Class cls, Object obj) {
        this.A00.put(cls, obj);
    }
}
