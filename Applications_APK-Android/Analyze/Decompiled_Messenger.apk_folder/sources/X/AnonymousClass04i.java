package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.ipc.TraceContext;
import java.util.TreeMap;

/* renamed from: X.04i  reason: invalid class name */
public final class AnonymousClass04i implements AnonymousClass04j {
    public static final int A00 = TriggerRegistry.A00.A02("manual");
    public static final AnonymousClass04i A01 = new AnonymousClass04i();

    public boolean AUJ(long j, Object obj, long j2, Object obj2) {
        return obj == obj2;
    }

    public boolean BE8() {
        return false;
    }

    public TraceContext.TraceConfigExtras B6o(long j, Object obj, AnonymousClass057 r7) {
        TreeMap treeMap = new TreeMap();
        treeMap.put("provider.stack_trace.cpu_sampling_rate_ms", 11);
        return new TraceContext.TraceConfigExtras(treeMap, null, null);
    }

    private AnonymousClass04i() {
    }

    public int AYs(long j, Object obj, AnonymousClass057 r6) {
        C005606a r0 = AnonymousClass06Z.A00().A00;
        if (r0 == null) {
            return 0;
        }
        return ProvidersRegistry.A00.A00(r0.A00);
    }
}
