package X;

import java.util.concurrent.Executor;

/* renamed from: X.1nC  reason: invalid class name and case insensitive filesystem */
public final class C33201nC implements C23981Rs {
    public final /* synthetic */ C30405Evg A00;
    public final /* synthetic */ C23981Rs A01;
    public final /* synthetic */ C23951Rp A02;
    public final /* synthetic */ Executor A03;

    public C33201nC(C23951Rp r1, C23981Rs r2, Executor executor, C30405Evg evg) {
        this.A02 = r1;
        this.A01 = r2;
        this.A03 = executor;
        this.A00 = evg;
    }

    public Object CIx(C23901Rk r6) {
        C23951Rp r4 = this.A02;
        C23981Rs r3 = this.A01;
        try {
            AnonymousClass07A.A04(this.A03, new C33221nE(this.A00, r4, r3, r6), 1766719051);
            return null;
        } catch (Exception e) {
            r4.A01(new C99634pO(e));
            return null;
        }
    }
}
