package X;

import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import com.facebook.common.dextricks.DexStore;
import java.util.ArrayDeque;

/* renamed from: X.1rL  reason: invalid class name and case insensitive filesystem */
public final class C35511rL {
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        if (r3 == null) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r0 = androidx.core.view.accessibility.AccessibilityNodeInfoCompat.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        X.C15320v6.onInitializeAccessibilityNodeInfo(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        if (r0 != null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r0.A08();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0049, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004a, code lost:
        if (r1 == null) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0050, code lost:
        if (X.AnonymousClass8NH.A01(r1, r3) == false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0058, code lost:
        if (r1.A02.getChildCount() <= 0) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0062, code lost:
        if (r1.A0b() == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0068, code lost:
        if (X.AnonymousClass8NH.A03(r1, r3) == false) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0070, code lost:
        if (r1.A02.getChildCount() <= 0) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0076, code lost:
        if (X.AnonymousClass8NH.A04(r1, r3) == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x007d, code lost:
        if (X.AnonymousClass8NH.A00(r1) == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0083, code lost:
        if (X.AnonymousClass8NH.A02(r1, r3) != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0085, code lost:
        r1.A08();
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x008a, code lost:
        r1.A08();
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0094, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0095, code lost:
        r1.A08();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0098, code lost:
        throw r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x008f A[LOOP:0: B:1:0x0008->B:52:0x008f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x005d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.view.View A00(android.view.View r4) {
        /*
            java.util.ArrayDeque r2 = new java.util.ArrayDeque
            r2.<init>()
            A05(r4, r2)
        L_0x0008:
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0099
            java.lang.Object r3 = r2.removeFirst()
            android.view.View r3 = (android.view.View) r3
            if (r3 == 0) goto L_0x005a
            int r1 = X.C15320v6.getImportantForAccessibility(r3)
            r0 = 2
            if (r1 == r0) goto L_0x0047
            r4 = 4
            if (r1 == r4) goto L_0x0047
            android.view.ViewParent r1 = r3.getParent()
        L_0x0024:
            boolean r0 = r1 instanceof android.view.View
            if (r0 == 0) goto L_0x0036
            r0 = r1
            android.view.View r0 = (android.view.View) r0
            int r0 = X.C15320v6.getImportantForAccessibility(r0)
            if (r0 == r4) goto L_0x005a
            android.view.ViewParent r1 = r1.getParent()
            goto L_0x0024
        L_0x0036:
            r1 = 0
            if (r3 == 0) goto L_0x004a
            androidx.core.view.accessibility.AccessibilityNodeInfoCompat r0 = androidx.core.view.accessibility.AccessibilityNodeInfoCompat.A01()     // Catch:{ IllegalArgumentException -> 0x0047 }
            X.C15320v6.onInitializeAccessibilityNodeInfo(r3, r0)     // Catch:{ NullPointerException -> 0x0041 }
            goto L_0x0049
        L_0x0041:
            if (r0 == 0) goto L_0x004a
            r0.A08()     // Catch:{ IllegalArgumentException -> 0x0047 }
            goto L_0x004a
        L_0x0047:
            r0 = 0
            goto L_0x005b
        L_0x0049:
            r1 = r0
        L_0x004a:
            if (r1 == 0) goto L_0x005a
            boolean r0 = X.AnonymousClass8NH.A01(r1, r3)
            if (r0 == 0) goto L_0x005e
            android.view.accessibility.AccessibilityNodeInfo r0 = r1.A02
            int r0 = r0.getChildCount()
            if (r0 <= 0) goto L_0x005e
        L_0x005a:
            r0 = 0
        L_0x005b:
            if (r0 == 0) goto L_0x008f
            return r3
        L_0x005e:
            boolean r0 = r1.A0b()     // Catch:{ all -> 0x0094 }
            if (r0 == 0) goto L_0x008a
            boolean r0 = X.AnonymousClass8NH.A03(r1, r3)     // Catch:{ all -> 0x0094 }
            if (r0 == 0) goto L_0x0079
            android.view.accessibility.AccessibilityNodeInfo r0 = r1.A02     // Catch:{ all -> 0x0094 }
            int r0 = r0.getChildCount()     // Catch:{ all -> 0x0094 }
            if (r0 <= 0) goto L_0x0085
            boolean r0 = X.AnonymousClass8NH.A04(r1, r3)     // Catch:{ all -> 0x0094 }
            if (r0 == 0) goto L_0x008a
            goto L_0x0085
        L_0x0079:
            boolean r0 = X.AnonymousClass8NH.A00(r1)     // Catch:{ all -> 0x0094 }
            if (r0 == 0) goto L_0x008a
            boolean r0 = X.AnonymousClass8NH.A02(r1, r3)     // Catch:{ all -> 0x0094 }
            if (r0 != 0) goto L_0x008a
        L_0x0085:
            r1.A08()
            r0 = 1
            goto L_0x005b
        L_0x008a:
            r1.A08()
            r0 = 0
            goto L_0x005b
        L_0x008f:
            A05(r3, r2)
            goto L_0x0008
        L_0x0094:
            r0 = move-exception
            r1.A08()
            throw r0
        L_0x0099:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35511rL.A00(android.view.View):android.view.View");
    }

    public static void A01(View view) {
        if (view != null && AnonymousClass3RG.A01(view.getContext())) {
            view.sendAccessibilityEvent(4194304);
            try {
                C15320v6.performAccessibilityAction(view, 64, null);
            } catch (NullPointerException unused) {
            }
        }
    }

    public static void A03(View view, long j) {
        if (view != null) {
            if (j < 500) {
                j = 500;
            }
            view.postDelayed(new C112955Zc(view), j);
        }
    }

    public static void A05(View view, ArrayDeque arrayDeque) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt != null) {
                    arrayDeque.addFirst(childAt);
                }
            }
        }
    }

    public static void A02(View view, int i) {
        if (!((AccessibilityManager) view.getContext().getApplicationContext().getSystemService("accessibility")).isEnabled()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            view.setImportantForAccessibility(i);
            return;
        }
        if (C15320v6.hasAccessibilityDelegate(view)) {
            view.setAccessibilityDelegate(null);
            if (view.getTag(2131299560) != null) {
                view.setClickable(((Boolean) view.getTag(2131299560)).booleanValue());
                view.setFocusable(((Boolean) view.getTag(2131299562)).booleanValue());
                view.setLongClickable(((Boolean) view.getTag(2131299563)).booleanValue());
                view.setContentDescription((String) view.getTag(2131299561));
            }
        }
        if (i == 1) {
            view.setFocusable(true);
        } else if (i == 2 || i == 4) {
            view.setAccessibilityDelegate(new C138486dF(i, view));
        }
    }

    public static void A04(View view, CharSequence charSequence) {
        ViewParent parent;
        if (((AccessibilityManager) view.getContext().getApplicationContext().getSystemService("accessibility")).isEnabled() && (parent = view.getParent()) != null) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain((int) DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET);
            view.onInitializeAccessibilityEvent(obtain);
            if (charSequence != null) {
                obtain.getText().add(charSequence);
                obtain.setContentDescription(null);
            }
            parent.requestSendAccessibilityEvent(view, obtain);
        }
    }

    public static void A06(StringBuilder sb, CharSequence charSequence, boolean z) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (z && !TextUtils.isEmpty(sb)) {
                sb.append(',');
                sb.append(' ');
            }
            sb.append(charSequence);
        }
    }
}
