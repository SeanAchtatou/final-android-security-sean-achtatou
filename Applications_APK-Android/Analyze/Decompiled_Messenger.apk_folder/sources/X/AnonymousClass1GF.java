package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1GF  reason: invalid class name */
public final class AnonymousClass1GF extends Enum {
    private static final /* synthetic */ AnonymousClass1GF[] A00;
    public static final AnonymousClass1GF A01;
    public static final AnonymousClass1GF A02;
    public static final AnonymousClass1GF A03;
    public static final AnonymousClass1GF A04;
    public static final AnonymousClass1GF A05;

    static {
        AnonymousClass1GF r2 = new AnonymousClass1GF("READY_WITH_THREADS_AND_UNITS", 0);
        A02 = r2;
        AnonymousClass1GF r3 = new AnonymousClass1GF("READY_WITH_THREADS", 1);
        A01 = r3;
        AnonymousClass1GF r4 = new AnonymousClass1GF("THREADS_AND_UNITS_LOADING", 2);
        A03 = r4;
        AnonymousClass1GF r5 = new AnonymousClass1GF("THREADS_LOADING", 3);
        A04 = r5;
        AnonymousClass1GF r6 = new AnonymousClass1GF("UNITS_LOADING", 4);
        A05 = r6;
        A00 = new AnonymousClass1GF[]{r2, r3, r4, r5, r6, new AnonymousClass1GF("OTHER_LOADING", 5)};
    }

    public static AnonymousClass1GF valueOf(String str) {
        return (AnonymousClass1GF) Enum.valueOf(AnonymousClass1GF.class, str);
    }

    public static AnonymousClass1GF[] values() {
        return (AnonymousClass1GF[]) A00.clone();
    }

    private AnonymousClass1GF(String str, int i) {
    }
}
