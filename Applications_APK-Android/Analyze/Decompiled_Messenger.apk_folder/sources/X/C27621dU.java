package X;

import android.os.Bundle;
import androidx.savedstate.Recreator;
import androidx.savedstate.SavedStateRegistry$1;
import java.util.Map;

/* renamed from: X.1dU  reason: invalid class name and case insensitive filesystem */
public final class C27621dU {
    public final C27631dV A00 = new C27631dV();
    private final C27521dK A01;

    public void A00(Bundle bundle) {
        AnonymousClass0qM AsK = this.A01.AsK();
        if (AsK.A05() == AnonymousClass1XL.INITIALIZED) {
            AsK.A06(new Recreator(this.A01));
            C27631dV r1 = this.A00;
            if (!r1.A03) {
                if (bundle != null) {
                    r1.A01 = bundle.getBundle("androidx.lifecycle.BundlableSavedStateRegistry.key");
                }
                AsK.A06(new SavedStateRegistry$1(r1));
                r1.A03 = true;
                return;
            }
            throw new IllegalStateException("SavedStateRegistry was already restored.");
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage");
    }

    public void A01(Bundle bundle) {
        C27631dV r1 = this.A00;
        Bundle bundle2 = new Bundle();
        Bundle bundle3 = r1.A01;
        if (bundle3 != null) {
            bundle2.putAll(bundle3);
        }
        AnonymousClass1XK r0 = r1.A02;
        C17390yp r2 = new C17390yp(r0);
        r0.A03.put(r2, false);
        while (r2.hasNext()) {
            Map.Entry entry = (Map.Entry) r2.next();
            bundle2.putBundle((String) entry.getKey(), ((C87044Dc) entry.getValue()).C4V());
        }
        bundle.putBundle("androidx.lifecycle.BundlableSavedStateRegistry.key", bundle2);
    }

    public C27621dU(C27521dK r2) {
        this.A01 = r2;
    }
}
