package X;

import android.app.Notification;
import android.support.v4.app.INotificationSideChannel;

/* renamed from: X.1FK  reason: invalid class name */
public final class AnonymousClass1FK implements C32461ln {
    public final int A00;
    public final Notification A01;
    public final String A02;
    public final String A03;

    public void C4w(INotificationSideChannel iNotificationSideChannel) {
        iNotificationSideChannel.BLx(this.A02, this.A00, this.A03, this.A01);
    }

    public String toString() {
        return "NotifyTask[" + "packageName:" + this.A02 + ", id:" + this.A00 + ", tag:" + this.A03 + "]";
    }

    public AnonymousClass1FK(String str, int i, String str2, Notification notification) {
        this.A02 = str;
        this.A00 = i;
        this.A03 = str2;
        this.A01 = notification;
    }
}
