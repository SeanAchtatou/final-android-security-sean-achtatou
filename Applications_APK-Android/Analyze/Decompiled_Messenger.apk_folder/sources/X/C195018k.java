package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.acra.LogCatCollector;
import com.facebook.messaging.model.threadkey.ThreadKey;
import java.io.IOException;

/* renamed from: X.18k  reason: invalid class name and case insensitive filesystem */
public final class C195018k {
    public static final Class A04 = C195018k.class;
    public final AnonymousClass187 A00;
    public final C04310Tq A01;
    public final C04310Tq A02;
    private final C194918j A03;

    public static byte[] A00(C195018k r8, ThreadKey threadKey, int i) {
        byte[] A022;
        try {
            AnonymousClass23J r3 = (AnonymousClass23J) r8.A01.get();
            synchronized (r3) {
                A022 = r3.A02(threadKey, i);
                if (A022 == null) {
                    AnonymousClass187 r1 = r8.A00;
                    A022 = new byte[AnonymousClass189.A03(AnonymousClass187.A04)];
                    r1.A00.A01.nextBytes(A022);
                    int A012 = r8.A03.A01();
                    byte[] A032 = r8.A00.A03(A022, A012);
                    String threadKey2 = threadKey.toString();
                    if (i > -1) {
                        threadKey2 = AnonymousClass08S.A0L(threadKey2, "_", i);
                    }
                    SQLiteDatabase A013 = ((AnonymousClass183) r8.A02.get()).A01();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(C30611iM.A00.A00, A032);
                    contentValues.put(C30611iM.A01.A00, Integer.valueOf(A012));
                    C06140av A033 = C06160ax.A03(C30611iM.A02.A00, threadKey2);
                    String A023 = A033.A02();
                    String[] A042 = A033.A04();
                    String $const$string = C99084oO.$const$string(26);
                    int update = A013.update($const$string, contentValues, A023, A042);
                    if (update == 0) {
                        contentValues.put(C30611iM.A02.A00, threadKey2);
                        C007406x.A00(827008253);
                        A013.insert($const$string, null, contentValues);
                        C007406x.A00(-1536649893);
                    } else if (update != 1) {
                        C010708t.A0B(A04, "Unable to store crypto key for %s (Updated %d threads)", threadKey2, Integer.valueOf(update));
                    }
                    r3.A01(threadKey, A022, i);
                }
            }
            return A022;
        } catch (C37911wY | C37921wZ | IOException e) {
            C010708t.A08(A04, "Failed to encrypt key for local storage", e);
            throw new RuntimeException(e);
        }
    }

    public byte[] A02(ThreadKey threadKey, byte[] bArr, int i) {
        if (bArr == null) {
            return null;
        }
        try {
            return this.A00.A06(A00(this, threadKey, i), bArr);
        } catch (C37911wY | C37921wZ | IOException e) {
            C010708t.A08(A04, "Failed to encrypt thread information for local storage", e);
            throw new RuntimeException(e);
        }
    }

    public C195018k(AnonymousClass187 r1, C04310Tq r2, C04310Tq r3, C194918j r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public void A01(String str, ThreadKey threadKey, String str2, int i, ContentValues contentValues) {
        byte[] A002 = A00(this, threadKey, i);
        byte[] bArr = null;
        if (str2 != null) {
            try {
                bArr = this.A00.A06(A002, str2.getBytes(LogCatCollector.UTF_8_ENCODING));
            } catch (C37911wY | C37921wZ | IOException e) {
                C010708t.A08(A04, "Failed to encrypt crypto session for local storage", e);
                throw new RuntimeException(e);
            }
        }
        contentValues.put(str, bArr);
    }
}
