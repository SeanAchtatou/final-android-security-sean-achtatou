package X;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import java.util.List;

/* renamed from: X.0sf  reason: invalid class name and case insensitive filesystem */
public final class C14130sf extends C28141eK {
    public static int A00() {
        return 16;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if ((r5 & r2) == r2) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        if (X.C28141eK.A04(r11) == X.AnonymousClass07B.A01) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A02(X.C14130sf r11, android.content.Intent r12, android.content.Context r13, android.content.pm.ComponentInfo r14, java.lang.String r15) {
        /*
            boolean r0 = r14.exported
            java.lang.String r1 = "AccessibleByAnyAppIntentScope"
            r7 = 2
            r4 = 0
            r6 = 1
            r10 = r15
            if (r0 == 0) goto L_0x0060
            if (r15 == 0) goto L_0x005f
            android.content.pm.PackageManager r0 = r13.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0032 }
            android.content.pm.PermissionInfo r0 = r0.getPermissionInfo(r15, r4)     // Catch:{ NameNotFoundException -> 0x0032 }
            int r5 = r0.protectionLevel     // Catch:{ NameNotFoundException -> 0x0032 }
            int r3 = android.os.Build.VERSION.SDK_INT
            r2 = 23
            r0 = 0
            if (r3 < r2) goto L_0x001e
            r0 = 1
        L_0x001e:
            if (r0 == 0) goto L_0x0030
            int r2 = A00()
        L_0x0024:
            r0 = r5 & 2
            if (r0 == r7) goto L_0x002c
            r5 = r5 & r2
            r0 = 0
            if (r5 != r2) goto L_0x002d
        L_0x002c:
            r0 = 1
        L_0x002d:
            if (r0 != 0) goto L_0x0060
            return r6
        L_0x0030:
            r2 = 3
            goto L_0x0024
        L_0x0032:
            r5 = move-exception
            boolean r0 = r11.A0H()
            if (r0 != 0) goto L_0x0042
            java.lang.Integer r3 = X.C28141eK.A04(r11)
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            r0 = 0
            if (r3 != r2) goto L_0x0043
        L_0x0042:
            r0 = 1
        L_0x0043:
            if (r0 == 0) goto L_0x00b5
            X.04e r4 = r11.A00
            java.lang.String r3 = r14.packageName
            java.lang.String r2 = r14.name
            boolean r0 = r14.exported
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.Object[] r2 = new java.lang.Object[]{r3, r2, r0, r15}
            java.lang.String r0 = "Error checking permission for %s/%s but fail-open: exported=%s, permission=%s."
            java.lang.String r0 = java.lang.String.format(r0, r2)
            r4.C2T(r1, r0, r5)
            return r6
        L_0x005f:
            return r6
        L_0x0060:
            boolean r0 = r11.A0H()
            if (r0 == 0) goto L_0x00b5
            r0 = 0
            X.0bp r3 = X.C49822cv.A00(r12, r0, r0)     // Catch:{ JSONException -> 0x006d }
            r12 = r0
            goto L_0x0073
        L_0x006d:
            r2 = move-exception
            java.lang.String r12 = r2.getMessage()
            r3 = r0
        L_0x0073:
            if (r3 != 0) goto L_0x0093
            X.04e r4 = r11.A00
            java.lang.String r7 = r14.packageName
            java.lang.String r8 = r14.name
            boolean r2 = r14.exported
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r2)
            java.lang.String r11 = r13.getPackageName()
            java.lang.Object[] r3 = new java.lang.Object[]{r7, r8, r9, r10, r11, r12}
            java.lang.String r2 = "Fail-open: allowing non-public component %s/%s: exported=%s, permission=%s for context package %s with error in intent parser %s"
        L_0x008b:
            java.lang.String r2 = java.lang.String.format(r2, r3)
            r4.C2T(r1, r2, r0)
            return r6
        L_0x0093:
            X.04e r4 = r11.A00
            java.lang.String r7 = r14.packageName
            java.lang.String r8 = r14.name
            boolean r2 = r14.exported
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r2)
            java.lang.String r11 = r13.getPackageName()
            org.json.JSONObject r2 = r3.A01
            if (r2 != 0) goto L_0x00b0
            java.lang.String r12 = ""
        L_0x00a9:
            java.lang.Object[] r3 = new java.lang.Object[]{r7, r8, r9, r10, r11, r12}
            java.lang.String r2 = "Fail-open: allowing non-public component %s/%s: exported=%s, permission=%s for context package %s from intent %s"
            goto L_0x008b
        L_0x00b0:
            java.lang.String r12 = r2.toString()
            goto L_0x00a9
        L_0x00b5:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14130sf.A02(X.0sf, android.content.Intent, android.content.Context, android.content.pm.ComponentInfo, java.lang.String):boolean");
    }

    public static Intent A01(C14130sf r3, Intent intent, List list, boolean z) {
        if (list.isEmpty()) {
            r3.A00.C2T("AccessibleByAnyAppIntentScope", "No matching public components.", null);
            return null;
        }
        if (z) {
            if (list.size() > 1) {
                C90684Up.A00(intent, r3.A00, r3.A0H());
                return C28141eK.A03(C28141eK.A08(list, intent));
            }
            ComponentInfo componentInfo = (ComponentInfo) list.get(0);
            intent.setComponent(new ComponentName(componentInfo.packageName, componentInfo.name));
        }
        C90684Up.A00(intent, r3.A00, r3.A0H());
        return intent;
    }

    public C14130sf(C008807t r2, AnonymousClass04e r3) {
        this(r2, r3, new C28151eL());
    }

    public C14130sf(C008807t r2, AnonymousClass04e r3, C28151eL r4) {
        super(r2, r3, false);
    }
}
