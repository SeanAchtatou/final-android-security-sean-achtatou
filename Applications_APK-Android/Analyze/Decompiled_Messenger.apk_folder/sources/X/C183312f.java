package X;

import com.facebook.zero.rewritenative.ZeroNativeRequestInterceptor;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.12f  reason: invalid class name and case insensitive filesystem */
public final class C183312f implements C31851kY {
    private static volatile C183312f A01;
    public ZeroNativeRequestInterceptor A00 = null;

    public static final C183312f A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C183312f.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C183312f();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public boolean Bmi(ImmutableList immutableList) {
        ZeroNativeRequestInterceptor zeroNativeRequestInterceptor = this.A00;
        if (zeroNativeRequestInterceptor == null) {
            return false;
        }
        zeroNativeRequestInterceptor.rulesChanged(immutableList);
        return false;
    }
}
