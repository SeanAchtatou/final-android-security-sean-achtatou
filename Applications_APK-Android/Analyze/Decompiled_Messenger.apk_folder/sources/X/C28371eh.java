package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.1eh  reason: invalid class name and case insensitive filesystem */
public final class C28371eh {
    public static void A03(SQLiteDatabase sQLiteDatabase, String str, BJR[] bjrArr) {
        sQLiteDatabase.delete("sqliteproc_schema", "table_name = ?", new String[]{str});
        ContentValues contentValues = new ContentValues();
        for (BJR bjr : bjrArr) {
            contentValues.put("table_name", str);
            contentValues.put("name", bjr.A05);
            contentValues.put("type_name", bjr.A09);
            contentValues.put(AnonymousClass80H.$const$string(60), bjr.A01);
            contentValues.put("is_nullable", Boolean.valueOf(bjr.A0D));
            contentValues.put("is_primary", Boolean.valueOf(bjr.A0E));
            contentValues.put(C22298Ase.$const$string(AnonymousClass1Y3.A1y), Boolean.valueOf(bjr.A0B));
            contentValues.put(C22298Ase.$const$string(82), Boolean.valueOf(bjr.A0C));
            contentValues.put("is_added", Boolean.valueOf(bjr.A0A));
            contentValues.put(C22298Ase.$const$string(235), bjr.A04);
            contentValues.put(C22298Ase.$const$string(234), bjr.A03);
            contentValues.put(C22298Ase.$const$string(283), bjr.A07);
            contentValues.put(C22298Ase.$const$string(AnonymousClass1Y3.A2D), bjr.A06);
            C007406x.A00(-497728300);
            sQLiteDatabase.insert("sqliteproc_schema", null, contentValues);
            C007406x.A00(1954988663);
        }
    }

    public static String A00(C13620rk r1, String str) {
        C28391ej r2 = new C28391ej(r1.A00(new AnonymousClass142(str)));
        try {
            if (r2.moveToFirst()) {
                return r2.A01.getString(1);
            }
            r2.close();
            return null;
        } finally {
            r2.close();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0034, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r2 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Set A01(X.C13620rk r4) {
        /*
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
            X.BK8 r0 = new X.BK8
            r0.<init>()
            android.database.Cursor r2 = r4.A00(r0)
            boolean r0 = r2.moveToFirst()     // Catch:{ all -> 0x0032 }
            if (r0 == 0) goto L_0x0029
        L_0x0014:
            X.BK9 r0 = new X.BK9     // Catch:{ all -> 0x0032 }
            r0.<init>(r2)     // Catch:{ all -> 0x0032 }
            android.database.Cursor r1 = r0.A01     // Catch:{ all -> 0x0032 }
            r0 = 1
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0032 }
            r3.add(r0)     // Catch:{ all -> 0x0032 }
            boolean r0 = r2.moveToNext()     // Catch:{ all -> 0x0032 }
            if (r0 != 0) goto L_0x0014
        L_0x0029:
            r2.close()
            java.lang.String r0 = "__database__"
            r3.remove(r0)
            return r3
        L_0x0032:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            if (r2 == 0) goto L_0x003a
            r2.close()     // Catch:{ all -> 0x003a }
        L_0x003a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28371eh.A01(X.0rk):java.util.Set");
    }

    public static void A02(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("table_name", str);
        contentValues.put("hash", str2);
        contentValues.put("index_hash", str3);
        C007406x.A00(318032543);
        sQLiteDatabase.insertWithOnConflict("sqliteproc_metadata", null, contentValues, 5);
        C007406x.A00(-1918534415);
    }
}
