package X;

import android.graphics.Matrix;

/* renamed from: X.1AN  reason: invalid class name */
public final class AnonymousClass1AN extends Matrix {
    public boolean A00;

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0081, code lost:
        if (r5 > r11) goto L_0x0083;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1AN A00(android.graphics.drawable.Drawable r8, android.widget.ImageView.ScaleType r9, int r10, int r11) {
        /*
            if (r9 != 0) goto L_0x0004
            android.widget.ImageView$ScaleType r9 = android.widget.ImageView.ScaleType.FIT_CENTER
        L_0x0004:
            r3 = 0
            if (r8 == 0) goto L_0x001f
            int r6 = r8.getIntrinsicWidth()
            int r5 = r8.getIntrinsicHeight()
            if (r6 <= 0) goto L_0x001f
            if (r5 <= 0) goto L_0x001f
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_XY
            if (r0 == r9) goto L_0x001f
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.MATRIX
            if (r0 == r9) goto L_0x001f
            if (r10 != r6) goto L_0x0020
            if (r11 != r5) goto L_0x0020
        L_0x001f:
            return r3
        L_0x0020:
            X.1AN r3 = new X.1AN
            r3.<init>()
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.CENTER
            r2 = 1
            r8 = 1056964608(0x3f000000, float:0.5)
            if (r0 != r9) goto L_0x0049
            int r0 = r10 - r6
            float r0 = (float) r0
            float r0 = r0 * r8
            int r0 = X.AnonymousClass1K3.A00(r0)
            float r1 = (float) r0
            int r0 = r11 - r5
            float r0 = (float) r0
            float r0 = r0 * r8
            int r0 = X.AnonymousClass1K3.A00(r0)
            float r0 = (float) r0
            r3.setTranslate(r1, r0)
            if (r6 > r10) goto L_0x0046
            if (r5 > r11) goto L_0x0046
            r2 = 0
        L_0x0046:
            r3.A00 = r2
            return r3
        L_0x0049:
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.CENTER_CROP
            r7 = 0
            if (r0 != r9) goto L_0x0079
            int r1 = r6 * r11
            int r0 = r10 * r5
            if (r1 <= r0) goto L_0x006f
            float r4 = (float) r11
            float r0 = (float) r5
            float r4 = r4 / r0
            float r1 = (float) r10
            float r0 = (float) r6
            float r0 = r0 * r4
            float r1 = r1 - r0
            float r1 = r1 * r8
        L_0x005c:
            r3.setScale(r4, r4)
            int r0 = X.AnonymousClass1K3.A00(r1)
            float r1 = (float) r0
            int r0 = X.AnonymousClass1K3.A00(r7)
            float r0 = (float) r0
            r3.postTranslate(r1, r0)
            r3.A00 = r2
            return r3
        L_0x006f:
            float r4 = (float) r10
            float r0 = (float) r6
            float r4 = r4 / r0
            float r7 = (float) r11
            float r0 = (float) r5
            float r0 = r0 * r4
            float r7 = r7 - r0
            float r7 = r7 * r8
            r1 = 0
            goto L_0x005c
        L_0x0079:
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.CENTER_INSIDE
            if (r0 != r9) goto L_0x00a8
            if (r6 > r10) goto L_0x0083
            r4 = 1065353216(0x3f800000, float:1.0)
            if (r5 <= r11) goto L_0x008d
        L_0x0083:
            float r2 = (float) r10
            float r0 = (float) r6
            float r2 = r2 / r0
            float r1 = (float) r11
            float r0 = (float) r5
            float r1 = r1 / r0
            float r4 = java.lang.Math.min(r2, r1)
        L_0x008d:
            float r1 = (float) r10
            float r0 = (float) r6
            float r0 = r0 * r4
            float r1 = r1 - r0
            float r1 = r1 * r8
            int r0 = X.AnonymousClass1K3.A00(r1)
            float r2 = (float) r0
            float r1 = (float) r11
            float r0 = (float) r5
            float r0 = r0 * r4
            float r1 = r1 - r0
            float r1 = r1 * r8
            int r0 = X.AnonymousClass1K3.A00(r1)
            float r0 = (float) r0
            r3.setScale(r4, r4)
            r3.postTranslate(r2, r0)
            return r3
        L_0x00a8:
            android.graphics.RectF r4 = new android.graphics.RectF
            r4.<init>()
            android.graphics.RectF r2 = new android.graphics.RectF
            r2.<init>()
            float r1 = (float) r6
            float r0 = (float) r5
            r4.set(r7, r7, r1, r0)
            float r1 = (float) r10
            float r0 = (float) r11
            r2.set(r7, r7, r1, r0)
            int[] r1 = X.AnonymousClass1M1.A00
            int r0 = r9.ordinal()
            r1 = r1[r0]
            r0 = 1
            if (r1 == r0) goto L_0x00dc
            r0 = 2
            if (r1 == r0) goto L_0x00d9
            r0 = 3
            if (r1 == r0) goto L_0x00d6
            r0 = 4
            if (r1 != r0) goto L_0x00df
            android.graphics.Matrix$ScaleToFit r0 = android.graphics.Matrix.ScaleToFit.END
        L_0x00d2:
            r3.setRectToRect(r4, r2, r0)
            return r3
        L_0x00d6:
            android.graphics.Matrix$ScaleToFit r0 = android.graphics.Matrix.ScaleToFit.CENTER
            goto L_0x00d2
        L_0x00d9:
            android.graphics.Matrix$ScaleToFit r0 = android.graphics.Matrix.ScaleToFit.START
            goto L_0x00d2
        L_0x00dc:
            android.graphics.Matrix$ScaleToFit r0 = android.graphics.Matrix.ScaleToFit.FILL
            goto L_0x00d2
        L_0x00df:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Only FIT_... values allowed"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AN.A00(android.graphics.drawable.Drawable, android.widget.ImageView$ScaleType, int, int):X.1AN");
    }

    private AnonymousClass1AN() {
    }
}
