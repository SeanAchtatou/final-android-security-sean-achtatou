package X;

/* renamed from: X.1rs  reason: invalid class name and case insensitive filesystem */
public final class C35821rs implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appchoreographer.BusySignalHandler$7";
    public final /* synthetic */ C28461eq A00;

    public C35821rs(C28461eq r1) {
        this.A00 = r1;
    }

    public void run() {
        synchronized (this.A00.A05) {
            for (C25101Yi BQF : this.A00.A07.keySet()) {
                BQF.BQF(true, false);
            }
        }
    }
}
