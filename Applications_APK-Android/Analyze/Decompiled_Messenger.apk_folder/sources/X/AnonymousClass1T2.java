package X;

import android.util.SparseIntArray;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1T2  reason: invalid class name */
public final class AnonymousClass1T2 {
    private static volatile AnonymousClass1T2 A09;
    public long A00;
    public boolean A01;
    public final SparseIntArray A02 = new SparseIntArray();
    public final Object A03 = new Object();
    public final Object A04 = new Object();
    public final Object A05 = new Object();
    public final Map A06 = new HashMap(15);
    public final Set A07 = Collections.synchronizedSet(new HashSet(15));
    public final Set A08 = new HashSet(15);

    public static int A00(AnonymousClass1T2 r6, C30442EwX ewX) {
        C859646c r5 = C859646c.A01;
        if (!r6.A07.contains(ewX)) {
            synchronized (r6.A03) {
                if (!r6.A06.containsKey(r5)) {
                    r6.A06.put(r5, ewX);
                    r6.A07.add(ewX);
                } else if (!r6.A07.contains(ewX)) {
                    throw new IllegalArgumentException(String.format("Marker %s cannot be added since marker with priority %s has already been seen", "gc", r5.name()));
                }
            }
        }
        return r5.ordinal();
    }

    public static final AnonymousClass1T2 A01(AnonymousClass1XY r3) {
        if (A09 == null) {
            synchronized (AnonymousClass1T2.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A09 = new AnonymousClass1T2();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public void A02() {
        synchronized (this.A05) {
            for (C30442EwX A002 : this.A08) {
                int A003 = A00(this, A002);
                synchronized (this.A04) {
                    int i = this.A02.get(A003) - 1;
                    if (i >= 0) {
                        this.A02.put(A003, i);
                        if (i == 0) {
                            this.A00 = (((long) (1 << A003)) ^ -1) & this.A00;
                        }
                    } else {
                        throw new IllegalStateException("Unset a marker which was not set.");
                    }
                }
            }
            this.A08.clear();
        }
    }
}
