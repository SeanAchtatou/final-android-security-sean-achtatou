package X;

import java.util.Map;

/* renamed from: X.21R  reason: invalid class name */
public final class AnonymousClass21R implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.NavigationLogger$2";
    public final /* synthetic */ AnonymousClass146 A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ Map A02;

    public AnonymousClass21R(AnonymousClass146 r1, String str, Map map) {
        this.A00 = r1;
        this.A01 = str;
        this.A02 = map;
    }

    public void run() {
        AnonymousClass146.A06(this.A00, this.A01, this.A02);
    }
}
