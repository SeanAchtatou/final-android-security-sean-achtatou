package X;

import android.os.SystemClock;
import com.facebook.mqttlite.MqttService;

/* renamed from: X.0sA  reason: invalid class name and case insensitive filesystem */
public final class C13840sA implements C01780Bp {
    public final /* synthetic */ MqttService A00;

    public C13840sA(MqttService mqttService) {
        this.A00 = mqttService;
    }

    public void BLy(boolean z) {
        C34261pE r7;
        AnonymousClass0C8 r6;
        long j;
        MqttService mqttService = this.A00;
        if (z && (r6 = (r7 = mqttService.A0D).A0n) != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            synchronized (r6) {
                j = r6.A0S;
            }
            if (elapsedRealtime - j > 300000) {
                r7.A0N("SCREEN_ON");
            }
        }
    }
}
