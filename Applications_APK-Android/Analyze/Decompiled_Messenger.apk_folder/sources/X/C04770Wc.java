package X;

import android.content.res.Resources;
import java.util.Map;

/* renamed from: X.0Wc  reason: invalid class name and case insensitive filesystem */
public interface C04770Wc {
    boolean Aem(long j);

    boolean Aeo(long j, boolean z);

    boolean Aer(long j, AnonymousClass0XE r3);

    boolean Aes(long j, boolean z, AnonymousClass0XE r4);

    double Aki(long j);

    double Akj(long j, double d);

    double Akl(long j, double d, AnonymousClass0XE r5);

    double Akm(long j, AnonymousClass0XE r3);

    Map Al7();

    int AqL(long j, int i);

    int AqO(long j, int i);

    long At0(long j);

    long At1(long j, long j2);

    long At3(long j, long j2, AnonymousClass0XE r5);

    long At4(long j, AnonymousClass0XE r3);

    String B4C(long j);

    String B4D(long j, int i, Resources resources);

    String B4E(long j, String str);

    String B4J(long j, AnonymousClass0XE r3);

    String B4K(long j, String str, AnonymousClass0XE r4);

    void BJC(long j);
}
