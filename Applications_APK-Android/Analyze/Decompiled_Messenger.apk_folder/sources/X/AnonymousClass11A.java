package X;

import com.google.common.collect.AbstractMapBasedMultimap;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/* renamed from: X.11A  reason: invalid class name */
public class AnonymousClass11A extends AbstractMapBasedMultimap<K, V>.WrappedCollection implements List<V> {
    public final /* synthetic */ AbstractMapBasedMultimap A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass11A(AbstractMapBasedMultimap abstractMapBasedMultimap, Object obj, List list, AnonymousClass11B r4) {
        super(abstractMapBasedMultimap, obj, list, r4);
        this.A00 = abstractMapBasedMultimap;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [X.11B, X.11A] */
    public void add(int i, Object obj) {
        A02();
        boolean isEmpty = this.A00.isEmpty();
        ((List) this.A00).add(i, obj);
        this.A00.A00++;
        if (isEmpty) {
            A01();
        }
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [X.11B, X.11A] */
    public boolean addAll(int i, Collection collection) {
        if (collection.isEmpty()) {
            return false;
        }
        int size = size();
        boolean addAll = ((List) this.A00).addAll(i, collection);
        if (addAll) {
            int size2 = this.A00.size();
            this.A00.A00 += size2 - size;
            if (size == 0) {
                A01();
            }
        }
        return addAll;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [X.11B, X.11A] */
    public Object get(int i) {
        A02();
        return ((List) this.A00).get(i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [X.11B, X.11A] */
    public int indexOf(Object obj) {
        A02();
        return ((List) this.A00).indexOf(obj);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [X.11B, X.11A] */
    public int lastIndexOf(Object obj) {
        A02();
        return ((List) this.A00).lastIndexOf(obj);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [X.11B, X.11A] */
    public Object remove(int i) {
        A02();
        Object remove = ((List) this.A00).remove(i);
        AbstractMapBasedMultimap abstractMapBasedMultimap = this.A00;
        abstractMapBasedMultimap.A00--;
        A03();
        return remove;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [X.11B, X.11A] */
    public Object set(int i, Object obj) {
        A02();
        return ((List) this.A00).set(i, obj);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: X.11B} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: X.11B} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: X.11B} */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.util.List, X.118] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List subList(int r6, int r7) {
        /*
            r5 = this;
            r5.A02()
            com.google.common.collect.AbstractMapBasedMultimap r4 = r5.A00
            java.lang.Object r3 = r5.A03
            java.util.Collection r0 = r5.A00
            java.util.List r0 = (java.util.List) r0
            java.util.List r2 = r0.subList(r6, r7)
            X.11B r1 = r5.A02
            if (r1 != 0) goto L_0x001e
            r1 = r5
        L_0x0014:
            boolean r0 = r2 instanceof java.util.RandomAccess
            if (r0 == 0) goto L_0x001f
            X.118 r0 = new X.118
            r0.<init>(r4, r3, r2, r1)
            return r0
        L_0x001e:
            goto L_0x0014
        L_0x001f:
            X.11A r0 = new X.11A
            r0.<init>(r4, r3, r2, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass11A.subList(int, int):java.util.List");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [X.11B, X.11A] */
    public ListIterator listIterator() {
        A02();
        return new B92(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [X.11B, X.11A] */
    public ListIterator listIterator(int i) {
        A02();
        return new B92(this, i);
    }
}
