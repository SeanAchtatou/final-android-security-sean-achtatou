package X;

import com.facebook.profilo.logger.Logger;

/* renamed from: X.0Hi  reason: invalid class name */
public final class AnonymousClass0Hi implements Runnable, AnonymousClass09S {
    public static final String __redex_internal_original_name = "com.facebook.tools.dextr.runtime.detour.RunnableWrapper";
    private int A00;
    private int A01;
    private Runnable A02;

    public void run() {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 14, 0, 0, this.A00, this.A01, 0);
        try {
            this.A02.run();
            Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 15, 0, 0, this.A00, writeStandardEntry, 0);
        } catch (Throwable th) {
            Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 15, 0, 0, this.A00, writeStandardEntry, 0);
            throw th;
        }
    }

    public String toString() {
        return "RunnableWrapper for " + this.A02;
    }

    public AnonymousClass0Hi(Runnable runnable, int i, int i2) {
        this.A02 = runnable;
        this.A01 = i;
        this.A00 = i2;
    }

    public Object getInnerRunnable() {
        return this.A02;
    }
}
