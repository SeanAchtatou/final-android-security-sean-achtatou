package X;

import com.facebook.profilo.core.ProvidersRegistry;
import java.util.List;
import java.util.TreeMap;

/* renamed from: X.056  reason: invalid class name */
public final class AnonymousClass056 implements AnonymousClass057 {
    public int A00;
    public TreeMap A01;
    public TreeMap A02;
    public TreeMap A03;
    public final int A04;
    public final int A05;

    public AnonymousClass056(int i, List list, int i2, TreeMap treeMap, TreeMap treeMap2, TreeMap treeMap3) {
        if (i > 0) {
            this.A04 = i;
            this.A05 = i2;
            this.A03 = treeMap;
            this.A01 = treeMap2;
            this.A02 = treeMap3;
            this.A00 = ProvidersRegistry.A00.A00(list);
            return;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0A("coinflip_sample_rate (", i, ") <= 0"));
    }
}
