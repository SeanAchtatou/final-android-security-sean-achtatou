package X;

/* renamed from: X.1Si  reason: invalid class name and case insensitive filesystem */
public final class C24111Si implements C22891Nf {
    public final /* synthetic */ C33251nH A00;
    public final /* synthetic */ AnonymousClass1PZ A01;

    public C24111Si(AnonymousClass1PZ r1, C33251nH r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void C0t(Object obj) {
        boolean z;
        AnonymousClass1PS A03;
        C179848St r2;
        AnonymousClass1PZ r3 = this.A01;
        C33251nH r5 = this.A00;
        C05520Zg.A02(r5);
        synchronized (r3) {
            synchronized (r3) {
                try {
                    C05520Zg.A02(r5);
                    int i = r5.A00;
                    boolean z2 = false;
                    if (i > 0) {
                        z2 = true;
                    }
                    C05520Zg.A05(z2);
                    r5.A00 = i - 1;
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
            synchronized (r3) {
                try {
                    if (r5.A01 || r5.A00 != 0) {
                        z = false;
                    } else {
                        r3.A03.A04(r5.A04, r5);
                        z = true;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
            A03 = AnonymousClass1PZ.A03(r3, r5);
        }
        AnonymousClass1PS.A05(A03);
        if (!z) {
            r5 = null;
        }
        if (!(r5 == null || (r2 = r5.A03) == null)) {
            r2.A00(r5.A04, true);
        }
        AnonymousClass1PZ.A07(r3);
        AnonymousClass1PZ.A06(r3);
    }
}
