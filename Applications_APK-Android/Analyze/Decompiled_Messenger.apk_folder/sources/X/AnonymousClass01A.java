package X;

import android.content.Context;
import java.nio.MappedByteBuffer;

/* renamed from: X.01A  reason: invalid class name */
public final class AnonymousClass01A implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.nobreak.CatchMeIfYouCan$1";
    public final /* synthetic */ Context A00;
    public final /* synthetic */ String A01;

    public AnonymousClass01A(String str, Context context) {
        this.A01 = str;
        this.A00 = context;
    }

    public void run() {
        AnonymousClass014.A02(this.A01);
        Context context = this.A00;
        AnonymousClass019 A012 = AnonymousClass014.A0E;
        MappedByteBuffer mappedByteBuffer = A012.A01;
        int i = A012.A00;
        for (int i2 = 0; i2 < i; i2++) {
            mappedByteBuffer.putLong(i2 << 3, 0);
        }
        AnonymousClass01C.A01(context, true);
    }
}
