package X;

import com.facebook.omnistore.module.OmnistoreComponent;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import java.util.HashMap;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1gP  reason: invalid class name and case insensitive filesystem */
public final class C29431gP {
    private static volatile C29431gP A04;
    public final HashMap A00;
    public final HashMap A01 = new HashMap(this.A03.size());
    public final Set A02;
    public final Set A03;

    public synchronized AnonymousClass1Uz A01(OmnistoreComponent omnistoreComponent) {
        Object obj;
        if (this.A02.contains(omnistoreComponent)) {
            obj = (AnonymousClass1V0) this.A00.get(omnistoreComponent);
            if (obj == null) {
                obj = new AnonymousClass1Uz(omnistoreComponent);
                this.A00.put(omnistoreComponent, obj);
            }
        } else {
            throw new RuntimeException("Tried to init an unregistered component");
        }
        return (AnonymousClass1Uz) obj;
    }

    public synchronized C36481tC A02(OmnistoreStoredProcedureComponent omnistoreStoredProcedureComponent) {
        Object obj;
        if (this.A03.contains(omnistoreStoredProcedureComponent)) {
            obj = (AnonymousClass1V0) this.A01.get(omnistoreStoredProcedureComponent);
            if (obj == null) {
                obj = new C36481tC(omnistoreStoredProcedureComponent);
                this.A01.put(omnistoreStoredProcedureComponent, obj);
            }
        } else {
            throw new RuntimeException("Tried to init an unregistered stored procedure component");
        }
        return (C36481tC) obj;
    }

    public static final C29431gP A00(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (C29431gP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A04 = new C29431gP(new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A1s), new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A1t));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private C29431gP(Set set, Set set2) {
        this.A02 = set;
        this.A03 = set2;
        this.A00 = new HashMap(set.size());
    }
}
