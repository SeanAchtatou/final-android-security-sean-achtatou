package X;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.198  reason: invalid class name */
public final class AnonymousClass198 implements AnonymousClass199 {
    public final /* synthetic */ C196118y A00;

    public AnonymousClass198(C196118y r1) {
        this.A00 = r1;
    }

    public void BlK() {
        C196118y r1 = this.A00;
        C194318c r3 = r1.A00;
        boolean z = r1.A01;
        r1.A01 = true;
        if (r3 != null) {
            if (z) {
                if (!r3.A00.A04.isMusicActive()) {
                    ThreadListFragment threadListFragment = r3.A00;
                    if (threadListFragment.A0J != C001500z.A07) {
                        try {
                            AssetFileDescriptor openRawResourceFd = threadListFragment.A12().openRawResourceFd(2131755092);
                            MediaPlayer mediaPlayer = new MediaPlayer();
                            if (threadListFragment.A1W) {
                                threadListFragment.mMediaPlayer = mediaPlayer;
                            }
                            mediaPlayer.setDataSource(openRawResourceFd.getFileDescriptor(), openRawResourceFd.getStartOffset(), openRawResourceFd.getLength());
                            openRawResourceFd.close();
                            mediaPlayer.setAudioStreamType(1);
                            mediaPlayer.setVolume(0.3f, 0.3f);
                            if (!threadListFragment.A1Z) {
                                mediaPlayer.setOnCompletionListener(new AnonymousClass23N(threadListFragment));
                            }
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                            if (threadListFragment.A1Z) {
                                mediaPlayer.setOnCompletionListener(new AnonymousClass23N(threadListFragment));
                            }
                        } catch (Throwable unused) {
                        }
                    } else {
                        threadListFragment.A0k.A0A("recents_pull_down");
                    }
                }
                ((C20921Ei) AnonymousClass1XX.A02(23, AnonymousClass1Y3.BMw, r3.A00.A0O)).A07(2, true);
                if (!((C16430x3) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AZ7, r3.A00.A0x.A01)).A07()) {
                    ThreadListFragment threadListFragment2 = r3.A00;
                    AnonymousClass10D r12 = threadListFragment2.A0x;
                    InterstitialTrigger interstitialTrigger = AnonymousClass3VY.A06;
                    ThreadListFragment.A0M(threadListFragment2);
                    r12.A02(interstitialTrigger);
                }
                r3.A00.A0D.A01("pull to refresh", AnonymousClass07B.A01);
            }
            r3.A00.A14.A0C(AnonymousClass1FD.EXPLICIT_USER_REFRESH, "user refresh", "ThreadListFragment");
        }
    }
}
