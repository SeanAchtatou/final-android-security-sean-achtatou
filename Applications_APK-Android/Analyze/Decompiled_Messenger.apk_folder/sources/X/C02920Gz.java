package X;

import android.content.Context;

/* renamed from: X.0Gz  reason: invalid class name and case insensitive filesystem */
public final class C02920Gz implements AnonymousClass0A0 {
    public static final C02920Gz A00 = new C02920Gz();

    public String getErrorMessage() {
        return null;
    }

    public boolean isPlatformSupported() {
        return false;
    }

    public void manualGcComplete() {
    }

    public void manualGcConcurrent() {
    }

    public void manualGcForAlloc() {
    }

    public void notAsBadTimeToDoGc() {
    }

    public void setUpHook(Context context, AnonymousClass0N9 r2) {
    }

    public void badTimeToDoGc(AnonymousClass084 r1) {
    }
}
