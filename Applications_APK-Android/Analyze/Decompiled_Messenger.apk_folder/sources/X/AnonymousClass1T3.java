package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1T3  reason: invalid class name */
public final class AnonymousClass1T3 {
    private static volatile AnonymousClass1T3 A07;
    public float A00 = 0.0f;
    public float A01 = 0.0f;
    public int A02 = 0;
    public int A03 = 0;
    private boolean A04 = false;
    public final AnonymousClass15Q A05;
    private final C008307n A06;

    public int A02() {
        A01(this);
        if (0 != 0) {
            return this.A02;
        }
        return this.A03;
    }

    public static final AnonymousClass1T3 A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (AnonymousClass1T3.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new AnonymousClass1T3(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static void A01(AnonymousClass1T3 r7) {
        int i;
        if (!r7.A04) {
            float A022 = r7.A06.A02();
            r7.A00 = A022;
            if (A022 <= 0.0f) {
                A022 = 60.0f;
            } else if (A022 < 30.0f) {
                A022 = 30.0f;
            } else if (A022 > 120.0f) {
                A022 = 120.0f;
            }
            r7.A01 = A022;
            AnonymousClass15Q r1 = r7.A05;
            AnonymousClass15Q.A02(r1);
            int i2 = 1;
            if (1 != 0) {
                i2 = r1.A00;
            }
            r7.A02 = (int) Math.ceil((double) (((float) (((long) i2) * 1000)) / A022));
            float f = r7.A01;
            AnonymousClass15Q r0 = r7.A05;
            AnonymousClass15Q.A02(r0);
            if (0 != 0) {
                i = r0.A00;
            } else {
                i = 1;
            }
            r7.A03 = (int) Math.ceil((double) (((float) (((long) i) * 1000)) / f));
            r7.A04 = true;
        }
    }

    private AnonymousClass1T3(AnonymousClass1XY r3) {
        this.A06 = C008307n.A00(r3);
        this.A05 = AnonymousClass15Q.A01(r3);
    }
}
