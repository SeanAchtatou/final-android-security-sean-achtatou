package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.tincan.messenger.TincanPreKeyManager;
import com.google.common.base.Preconditions;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1lh  reason: invalid class name and case insensitive filesystem */
public final class C32401lh extends C36471tB implements CallerContextable {
    public static final Class A08 = C32401lh.class;
    private static volatile C32401lh A09 = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.tincan.outbound.CheckThreadChecksumSP";
    public final BlueServiceOperationFactory A00;
    public final C36571tL A01;
    public final C36501tE A02;
    public final C36731td A03;
    public final TincanPreKeyManager A04;
    private final AnonymousClass06A A05;
    private final AnonymousClass18N A06;
    private final C04310Tq A07;

    public synchronized void A0D(ThreadKey threadKey, byte[] bArr) {
        A01(threadKey, "start");
        if (!A0C()) {
            C010708t.A05(A08, "Stored procedure sender not available to check thread checksum");
        } else if (this.A06.A03()) {
            C010708t.A05(A08, "Invalid device id");
        } else {
            C22338Aw9 aw9 = new C22338Aw9(Long.valueOf(Long.parseLong((String) this.A07.get())), this.A06.A02());
            C22036AnC anC = C22036AnC.A0V;
            Preconditions.checkNotNull(threadKey);
            A0B(C22030An2.A01(new C22337Aw7(Integer.valueOf(C22039AnF.A00), null, aw9, Long.valueOf(this.A05.now() * 1000), anC, null, C36471tB.A03(threadKey.A0J()).getBytes(C36471tB.A05), Long.valueOf(threadKey.A0G()), bArr)));
            A01(threadKey, "sent");
        }
    }

    private C32401lh(AnonymousClass18N r2, AnonymousClass06A r3, C04310Tq r4, C36501tE r5, C36571tL r6, TincanPreKeyManager tincanPreKeyManager, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, BlueServiceOperationFactory blueServiceOperationFactory, C36731td r10) {
        super(AnonymousClass1Y3.A1e, deprecatedAnalyticsLogger);
        this.A06 = r2;
        this.A05 = r3;
        this.A07 = r4;
        this.A02 = r5;
        this.A01 = r6;
        this.A04 = tincanPreKeyManager;
        this.A00 = blueServiceOperationFactory;
        this.A03 = r10;
    }

    public static final C32401lh A00(AnonymousClass1XY r12) {
        if (A09 == null) {
            synchronized (C32401lh.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r12);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r12.getApplicationInjector();
                        A09 = new C32401lh(AnonymousClass18N.A00(applicationInjector), AnonymousClass067.A0A(applicationInjector), C10580kT.A04(applicationInjector), C36501tE.A00(applicationInjector), C36571tL.A00(applicationInjector), C36591tN.A01(applicationInjector), C06920cI.A00(applicationInjector), C30111hV.A00(applicationInjector), new C36731td(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static void A01(ThreadKey threadKey, String str) {
        C11670nb r1 = new C11670nb("tincan_check_sum");
        r1.A0C("threadKey", threadKey);
        r1.A0D("action", str);
    }
}
