package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Ig  reason: invalid class name and case insensitive filesystem */
public abstract class C21671Ig {
    public Map A00;
    public Map A01;

    public Object A00(boolean z) {
        if (!(this instanceof C21331Gg)) {
            A01("is_full_span", Boolean.valueOf(z));
            return this;
        }
        throw new UnsupportedOperationException("ViewRenderInfo does not support isFullSpan.");
    }

    public void A01(String str, Object obj) {
        if (this.A00 == null) {
            this.A00 = Collections.synchronizedMap(new HashMap());
        }
        this.A00.put(str, obj);
    }

    public void A02(String str, Object obj) {
        if (this.A01 == null) {
            this.A01 = Collections.synchronizedMap(new HashMap());
        }
        this.A01.put(str, obj);
    }
}
