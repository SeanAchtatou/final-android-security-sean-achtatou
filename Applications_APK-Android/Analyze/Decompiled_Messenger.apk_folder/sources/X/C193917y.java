package X;

import android.content.Context;
import com.facebook.auth.annotations.LoggedInUser;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.17y  reason: invalid class name and case insensitive filesystem */
public final class C193917y extends C30431i2 {
    public static final String[] A0D;
    private static volatile C193917y A0E;
    public AnonymousClass0UN A00;
    public boolean A01 = false;
    public final C04460Ut A02;
    public final C13700rt A03;
    public final AnonymousClass187 A04;
    public final C195118l A05;
    public final C30441i3 A06;
    public final C04310Tq A07;
    @LoggedInUser
    public final C04310Tq A08;
    public final C04310Tq A09;
    public final C04310Tq A0A;
    private final C194718h A0B;
    private final C12420pJ A0C;

    static {
        String str = C193717w.A0F.A00;
        String str2 = C193717w.A0G.A00;
        String str3 = C193717w.A0H.A00;
        String str4 = C193717w.A05.A00;
        String str5 = C193717w.A03.A00;
        String str6 = C193717w.A04.A00;
        String str7 = C193717w.A0A.A00;
        String str8 = C193717w.A02.A00;
        String str9 = C193717w.A0C.A00;
        String str10 = C193717w.A0D.A00;
        String str11 = C193717w.A00.A00;
        String str12 = C193717w.A01.A00;
        String str13 = C193717w.A09.A00;
        String str14 = C193717w.A07.A00;
        String str15 = C193717w.A0E.A00;
        String str16 = str13;
        String str17 = str14;
        String str18 = str15;
        String str19 = str10;
        String str20 = str11;
        String str21 = str12;
        String str22 = str7;
        String str23 = str8;
        String str24 = str9;
        String str25 = str5;
        String str26 = str6;
        String str27 = str;
        String str28 = str2;
        String str29 = str3;
        A0D = new String[]{str27, str28, str29, str4, str25, str26, str22, str23, str24, str19, str20, str21, str16, str17, str18, AnonymousClass182.A00.A00, AnonymousClass182.A02.A00, AnonymousClass182.A03.A00, AnonymousClass182.A04.A00};
    }

    public static final C193917y A00(AnonymousClass1XY r5) {
        if (A0E == null) {
            synchronized (C193917y.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0E, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0E = new C193917y(applicationInjector, AnonymousClass1YA.A02(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0E;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0060, code lost:
        if (r2 != null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0065, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A04(long r13) {
        /*
            r12 = this;
            X.0W6 r0 = X.C193717w.A09
            java.lang.String r1 = r0.A00
            java.lang.String r0 = java.lang.String.valueOf(r13)
            X.0av r1 = X.C06160ax.A03(r1, r0)
            X.0Tq r0 = r12.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r4 = r0.A01()
            if (r4 != 0) goto L_0x001d
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            return r0
        L_0x001d:
            X.0W6 r3 = X.C193717w.A0F
            java.lang.String r0 = r3.A00
            java.lang.String[] r6 = new java.lang.String[]{r0}
            java.lang.String r7 = r1.A02()
            java.lang.String[] r8 = r1.A04()
            r9 = 0
            r10 = 0
            X.0W6 r0 = X.C193717w.A0H
            java.lang.String r11 = r0.A04()
            java.lang.String r5 = "threads"
            android.database.Cursor r2 = r4.query(r5, r6, r7, r8, r9, r10, r11)
            com.google.common.collect.ImmutableList$Builder r1 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x005d }
            r1.<init>()     // Catch:{ all -> 0x005d }
        L_0x0040:
            boolean r0 = r2.moveToNext()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0055
            java.lang.String r0 = r3.A05(r2)     // Catch:{ all -> 0x005d }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)     // Catch:{ all -> 0x005d }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x005d }
            r1.add(r0)     // Catch:{ all -> 0x005d }
            goto L_0x0040
        L_0x0055:
            com.google.common.collect.ImmutableList r0 = r1.build()     // Catch:{ all -> 0x005d }
            r2.close()
            return r0
        L_0x005d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005f }
        L_0x005f:
            r0 = move-exception
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ all -> 0x0065 }
        L_0x0065:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193917y.A04(long):com.google.common.collect.ImmutableList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00fa, code lost:
        if (r6 != null) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ff, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A05(java.lang.String r16) {
        /*
            r15 = this;
            X.0qA r5 = new X.0qA
            r5.<init>()
            java.lang.String r0 = " "
            r1 = r16
            java.lang.String[] r8 = r1.split(r0)
            int r7 = r8.length
            r6 = 0
        L_0x000f:
            if (r6 >= r7) goto L_0x0050
            r0 = r8[r6]
            java.lang.String r1 = r0.trim()
            boolean r0 = X.C06850cB.A0B(r1)
            if (r0 != 0) goto L_0x004d
            r0 = 37
            java.lang.String r4 = X.AnonymousClass08S.A06(r1, r0)
            java.lang.String r1 = "threads."
            X.0W6 r0 = X.C193717w.A0G
            java.lang.String r0 = r0.A00
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            X.2Bm r3 = new X.2Bm
            r3.<init>(r0, r4)
            X.0W6 r0 = X.AnonymousClass182.A00
            java.lang.String r0 = r0.A00
            X.2Bm r2 = new X.2Bm
            r2.<init>(r0, r4)
            X.0W6 r0 = X.AnonymousClass182.A02
            java.lang.String r1 = r0.A00
            X.2Bm r0 = new X.2Bm
            r0.<init>(r1, r4)
            r5.A05(r3)
            r5.A05(r2)
            r5.A05(r0)
        L_0x004d:
            int r6 = r6 + 1
            goto L_0x000f
        L_0x0050:
            com.google.common.collect.ImmutableList$Builder r2 = new com.google.common.collect.ImmutableList$Builder
            r2.<init>()
            X.0Tq r0 = r15.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r6 = r0.A01()
            if (r6 != 0) goto L_0x0066
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            return r0
        L_0x0066:
            java.lang.String r4 = "threads LEFT JOIN thread_participants ON "
            X.0W6 r0 = X.C193717w.A09
            java.lang.String r3 = r0.A00
            java.lang.String r1 = " = "
            X.0W6 r0 = X.AnonymousClass182.A05
            java.lang.String r0 = r0.A00
            java.lang.String r7 = X.AnonymousClass08S.A0S(r4, r3, r1, r0)
            r4 = 1
            X.0W6 r0 = X.C193717w.A0F
            java.lang.String r0 = r0.A00
            r3 = 0
            java.lang.String[] r8 = new java.lang.String[]{r0}
            java.lang.String r9 = r5.A02()
            java.lang.String[] r10 = r5.A04()
            r11 = 0
            r12 = 0
            X.0W6 r0 = X.C193717w.A0H
            java.lang.String r13 = r0.A04()
            java.lang.String r14 = "5"
            android.database.Cursor r6 = r6.query(r7, r8, r9, r10, r11, r12, r13, r14)
        L_0x0096:
            boolean r0 = r6.moveToNext()     // Catch:{ all -> 0x00f7 }
            if (r0 == 0) goto L_0x00b8
            X.0W6 r0 = X.C193717w.A0F     // Catch:{ all -> 0x00f7 }
            java.lang.String r5 = r0.A05(r6)     // Catch:{ all -> 0x00f7 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r5)     // Catch:{ all -> 0x00f7 }
            if (r0 != 0) goto L_0x00b4
            java.lang.String r1 = "TincanDbThreadsFetcher"
            java.lang.String r0 = "Found unparsable thread key "
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r5)     // Catch:{ all -> 0x00f7 }
            X.C010708t.A0J(r1, r0)     // Catch:{ all -> 0x00f7 }
            goto L_0x0096
        L_0x00b4:
            r2.add(r0)     // Catch:{ all -> 0x00f7 }
            goto L_0x0096
        L_0x00b8:
            r6.close()
            com.google.common.collect.ImmutableList r0 = r2.build()
            com.google.common.collect.ImmutableList$Builder r7 = new com.google.common.collect.ImmutableList$Builder
            r7.<init>()
            X.1Xv r6 = r0.iterator()
        L_0x00c8:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x00f2
            java.lang.Object r5 = r6.next()
            com.facebook.messaging.model.threadkey.ThreadKey r5 = (com.facebook.messaging.model.threadkey.ThreadKey) r5
            r1 = -1
            r0 = -1
            android.util.Pair r0 = r15.A02(r5, r1, r0)
            java.lang.Object r2 = r0.first
            com.facebook.messaging.model.threads.ThreadsCollection r2 = (com.facebook.messaging.model.threads.ThreadsCollection) r2
            int r1 = r2.A02()
            r0 = 0
            if (r1 != r4) goto L_0x00e7
            r0 = 1
        L_0x00e7:
            com.google.common.base.Preconditions.checkState(r0)
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.A03(r3)
            r7.add(r0)
            goto L_0x00c8
        L_0x00f2:
            com.google.common.collect.ImmutableList r0 = r7.build()
            return r0
        L_0x00f7:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00f9 }
        L_0x00f9:
            r0 = move-exception
            if (r6 == 0) goto L_0x00ff
            r6.close()     // Catch:{ all -> 0x00ff }
        L_0x00ff:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193917y.A05(java.lang.String):com.google.common.collect.ImmutableList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        if (r2 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004e, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableSet A06() {
        /*
            r12 = this;
            X.0Tq r0 = r12.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r4 = r0.A01()
            if (r4 != 0) goto L_0x0011
            com.google.common.collect.RegularImmutableSet r0 = com.google.common.collect.RegularImmutableSet.A05
            return r0
        L_0x0011:
            X.0W6 r3 = X.C193717w.A0F
            java.lang.String r0 = r3.A00
            java.lang.String[] r6 = new java.lang.String[]{r0}
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            java.lang.String r5 = "threads"
            android.database.Cursor r2 = r4.query(r5, r6, r7, r8, r9, r10, r11)
            X.0dQ r1 = new X.0dQ     // Catch:{ all -> 0x0046 }
            r1.<init>()     // Catch:{ all -> 0x0046 }
        L_0x0029:
            boolean r0 = r2.moveToNext()     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x003e
            java.lang.String r0 = r3.A05(r2)     // Catch:{ all -> 0x0046 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)     // Catch:{ all -> 0x0046 }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x0046 }
            r1.A01(r0)     // Catch:{ all -> 0x0046 }
            goto L_0x0029
        L_0x003e:
            com.google.common.collect.ImmutableSet r0 = r1.build()     // Catch:{ all -> 0x0046 }
            r2.close()
            return r0
        L_0x0046:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0048 }
        L_0x0048:
            r0 = move-exception
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ all -> 0x004e }
        L_0x004e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193917y.A06():com.google.common.collect.ImmutableSet");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0062, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0063, code lost:
        if (r6 != null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0068, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer A07(com.facebook.messaging.model.threadkey.ThreadKey r12) {
        /*
            r11 = this;
            X.0W6 r0 = X.C193717w.A0F
            java.lang.String r1 = r0.A00
            java.lang.String r0 = r12.toString()
            X.0av r2 = X.C06160ax.A03(r1, r0)
            X.0Tq r0 = r11.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r3 = r0.A01()
            if (r3 != 0) goto L_0x001d
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            return r0
        L_0x001d:
            X.0W6 r1 = X.C193717w.A06
            java.lang.String r0 = r1.A00
            java.lang.String[] r5 = new java.lang.String[]{r0}
            java.lang.String r6 = r2.A02()
            java.lang.String[] r7 = r2.A04()
            r8 = 0
            r9 = 0
            r10 = 0
            java.lang.String r4 = "threads"
            android.database.Cursor r6 = r3.query(r4, r5, r6, r7, r8, r9, r10)
            boolean r0 = r6.moveToNext()     // Catch:{ all -> 0x0060 }
            if (r0 == 0) goto L_0x005a
            int r5 = r1.A00(r6)     // Catch:{ all -> 0x0060 }
            r0 = 7
            java.lang.Integer[] r4 = X.AnonymousClass07B.A00(r0)     // Catch:{ all -> 0x0060 }
            int r3 = r4.length     // Catch:{ all -> 0x0060 }
            r2 = 0
        L_0x0047:
            if (r2 >= r3) goto L_0x0054
            r0 = r4[r2]     // Catch:{ all -> 0x0060 }
            int r1 = X.C22075Anv.A00(r0)     // Catch:{ all -> 0x0060 }
            if (r5 == r1) goto L_0x0056
            int r2 = r2 + 1
            goto L_0x0047
        L_0x0054:
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0060 }
        L_0x0056:
            r6.close()
            return r0
        L_0x005a:
            r6.close()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            return r0
        L_0x0060:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0062 }
        L_0x0062:
            r0 = move-exception
            if (r6 == 0) goto L_0x0068
            r6.close()     // Catch:{ all -> 0x0068 }
        L_0x0068:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193917y.A07(com.facebook.messaging.model.threadkey.ThreadKey):java.lang.Integer");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x012c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x012d, code lost:
        if (r5 != null) goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0132, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A08(long r14) {
        /*
            r13 = this;
            X.0pJ r0 = r13.A0C
            boolean r2 = r0.A02()
            r1 = 0
            r0 = 0
            if (r2 == 0) goto L_0x0092
            X.18h r4 = r13.A0B
            X.0Tq r2 = r13.A08
            java.lang.Object r2 = r2.get()
            com.facebook.user.model.User r2 = (com.facebook.user.model.User) r2
            java.lang.String r2 = r2.A0j
            long r2 = java.lang.Long.parseLong(r2)
            com.facebook.messaging.model.threadkey.ThreadKey r6 = com.facebook.messaging.model.threadkey.ThreadKey.A05(r14, r2)
            java.lang.String r5 = java.lang.Long.toString(r14)
            X.0W6 r2 = X.C194818i.A0A
            java.lang.String r3 = r2.A00
            java.lang.String r2 = r6.A0J()
            X.0av r3 = X.C06160ax.A03(r3, r2)
            X.0W6 r2 = X.C194818i.A01
            java.lang.String r2 = r2.A00
            X.0av r2 = X.C06160ax.A03(r2, r5)
            X.0av[] r2 = new X.C06140av[]{r3, r2}
            X.1a6 r3 = X.C06160ax.A01(r2)
            X.0Tq r2 = r4.A03
            java.lang.Object r2 = r2.get()
            X.183 r2 = (X.AnonymousClass183) r2
            android.database.sqlite.SQLiteDatabase r2 = r2.A01()
            java.lang.String[] r4 = X.C194718h.A06
            java.lang.String r5 = r3.A02()
            java.lang.String[] r6 = r3.A04()
            java.lang.String r3 = "thread_devices"
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r6 = r2.query(r3, r4, r5, r6, r7, r8, r9)
            com.google.common.collect.ImmutableList$Builder r5 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x008d }
            r5.<init>()     // Catch:{ all -> 0x008d }
        L_0x0062:
            boolean r2 = r6.moveToNext()     // Catch:{ all -> 0x008d }
            if (r2 == 0) goto L_0x007e
            X.Aw9 r4 = new X.Aw9     // Catch:{ all -> 0x008d }
            long r2 = r6.getLong(r0)     // Catch:{ all -> 0x008d }
            java.lang.Long r3 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x008d }
            r2 = 1
            java.lang.String r2 = r6.getString(r2)     // Catch:{ all -> 0x008d }
            r4.<init>(r3, r2)     // Catch:{ all -> 0x008d }
            r5.add(r4)     // Catch:{ all -> 0x008d }
            goto L_0x0062
        L_0x007e:
            com.google.common.collect.ImmutableList r3 = r5.build()     // Catch:{ all -> 0x008d }
            r6.close()
            boolean r2 = r3.isEmpty()
            if (r2 != 0) goto L_0x013b
            goto L_0x0133
        L_0x008d:
            r0 = move-exception
            r6.close()
            throw r0
        L_0x0092:
            X.0W6 r2 = X.C193717w.A09
            java.lang.String r3 = r2.A00
            java.lang.String r2 = java.lang.String.valueOf(r14)
            X.0av r3 = X.C06160ax.A03(r3, r2)
            X.0Tq r2 = r13.A0A
            java.lang.Object r2 = r2.get()
            X.183 r2 = (X.AnonymousClass183) r2
            android.database.sqlite.SQLiteDatabase r5 = r2.A01()
            if (r5 == 0) goto L_0x013b
            r4 = 1
            X.0W6 r2 = X.C193717w.A08
            java.lang.String r2 = r2.A00
            java.lang.String[] r7 = new java.lang.String[]{r2}
            java.lang.String r8 = r3.A02()
            java.lang.String[] r9 = r3.A04()
            r10 = 0
            r11 = 0
            r12 = 0
            java.lang.String r6 = "threads"
            android.database.Cursor r5 = r5.query(r6, r7, r8, r9, r10, r11, r12)
            X.0W6 r2 = X.C193717w.A08     // Catch:{ all -> 0x012a }
            java.lang.String r2 = r2.A00     // Catch:{ all -> 0x012a }
            int r3 = r5.getColumnIndex(r2)     // Catch:{ all -> 0x012a }
            boolean r2 = r5.moveToNext()     // Catch:{ all -> 0x012a }
            if (r2 == 0) goto L_0x00e2
            boolean r2 = r5.isNull(r3)     // Catch:{ all -> 0x012a }
            if (r2 != 0) goto L_0x00e2
            java.lang.String r0 = r5.getString(r3)     // Catch:{ all -> 0x012a }
            r5.close()
            return r0
        L_0x00e2:
            r5.close()
            X.0Tq r2 = r13.A08
            java.lang.Object r2 = r2.get()
            com.facebook.user.model.User r2 = (com.facebook.user.model.User) r2
            if (r2 == 0) goto L_0x013b
            java.lang.String r2 = r2.A0j
            long r2 = java.lang.Long.parseLong(r2)
            java.lang.Long r6 = java.lang.Long.valueOf(r2)
            long r2 = r6.longValue()
            com.facebook.messaging.model.threadkey.ThreadKey r3 = com.facebook.messaging.model.threadkey.ThreadKey.A05(r14, r2)
            X.18h r2 = r13.A0B
            com.google.common.collect.ImmutableList r5 = r2.A0A(r3)
            int r3 = r5.size()
            r2 = 2
            if (r3 != r2) goto L_0x013b
            java.lang.Object r1 = r5.get(r0)
            X.Aw9 r1 = (X.C22338Aw9) r1
            java.lang.Long r1 = r1.user_id
            boolean r1 = r1.equals(r6)
            if (r1 == 0) goto L_0x0125
            java.lang.Object r0 = r5.get(r4)
        L_0x0120:
            X.Aw9 r0 = (X.C22338Aw9) r0
            java.lang.String r0 = r0.instance_id
            return r0
        L_0x0125:
            java.lang.Object r0 = r5.get(r0)
            goto L_0x0120
        L_0x012a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x012c }
        L_0x012c:
            r0 = move-exception
            if (r5 == 0) goto L_0x0132
            r5.close()     // Catch:{ all -> 0x0132 }
        L_0x0132:
            throw r0
        L_0x0133:
            java.lang.Object r0 = r3.get(r0)
            X.Aw9 r0 = (X.C22338Aw9) r0
            java.lang.String r1 = r0.instance_id
        L_0x013b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193917y.A08(long):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0040, code lost:
        if (r1 != null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0045, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A09(com.facebook.messaging.model.threadkey.ThreadKey r13) {
        /*
            r12 = this;
            X.0W6 r2 = X.C193717w.A0F
            java.lang.String r1 = r2.A00
            java.lang.String r0 = r13.toString()
            X.0av r1 = X.C06160ax.A03(r1, r0)
            X.0Tq r0 = r12.A0A
            java.lang.Object r0 = r0.get()
            X.183 r0 = (X.AnonymousClass183) r0
            android.database.sqlite.SQLiteDatabase r3 = r0.A01()
            r0 = 0
            if (r3 != 0) goto L_0x001c
            return r0
        L_0x001c:
            java.lang.String r0 = r2.A00
            java.lang.String[] r5 = new java.lang.String[]{r0}
            java.lang.String r6 = r1.A02()
            java.lang.String[] r7 = r1.A04()
            r8 = 0
            r9 = 0
            r10 = 0
            java.lang.String r4 = "threads"
            java.lang.String r11 = "1"
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8, r9, r10, r11)
            boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x003d }
            r1.close()
            return r0
        L_0x003d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003f }
        L_0x003f:
            r0 = move-exception
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ all -> 0x0045 }
        L_0x0045:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C193917y.A09(com.facebook.messaging.model.threadkey.ThreadKey):boolean");
    }

    private C193917y(AnonymousClass1XY r3, Context context) {
        super(context);
        this.A00 = new AnonymousClass0UN(1, r3);
        AnonymousClass067.A0A(r3);
        this.A08 = AnonymousClass0WY.A01(r3);
        this.A0A = AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, r3);
        this.A03 = new C13700rt(r3);
        this.A06 = C30441i3.A04(r3);
        this.A0B = C194718h.A06(r3);
        this.A05 = C195118l.A00(r3);
        this.A04 = AnonymousClass187.A01(r3);
        this.A09 = AnonymousClass0VG.A00(AnonymousClass1Y3.B6n, r3);
        this.A0C = C12420pJ.A00(r3);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.AcJ, r3);
        AnonymousClass0WT.A00(r3);
        this.A02 = C04430Uq.A02(r3);
    }
}
