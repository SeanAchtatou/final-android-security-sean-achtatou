package X;

import android.content.Context;
import android.os.Handler;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;

/* renamed from: X.1BB  reason: invalid class name */
public final class AnonymousClass1BB extends AnonymousClass1BV implements C21791Is {
    public static final InterstitialTrigger A05 = new InterstitialTrigger(InterstitialTrigger.Action.A3p);
    public AnonymousClass0UN A00;
    public final Handler A01 = new Handler();
    public final C20331Bz A02;
    public final AnonymousClass1CK A03;
    public final AnonymousClass1C0 A04;

    public String Aqc() {
        return "7165";
    }

    public static final AnonymousClass1BB A00(AnonymousClass1XY r1) {
        return new AnonymousClass1BB(r1);
    }

    public C70753bE B3h(InterstitialTrigger interstitialTrigger) {
        if (C06850cB.A0A(this.A03.A02.B4K(1153767093478097058L, BuildConfig.FLAVOR, AnonymousClass0XE.A06))) {
            return C70753bE.INELIGIBLE;
        }
        return C70753bE.ELIGIBLE;
    }

    public ImmutableList B7C() {
        return ImmutableList.of(A05);
    }

    private AnonymousClass1BB(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = C20331Bz.A00(r3);
        this.A03 = AnonymousClass1CK.A00(r3);
        this.A04 = AnonymousClass1C0.A00(r3);
    }

    public void Bwu(Context context, InterstitialTrigger interstitialTrigger, Object obj) {
        Preconditions.checkNotNull(obj);
        ((C35731rh) obj).Bbj("7165");
    }
}
