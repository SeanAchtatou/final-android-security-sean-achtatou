package X;

/* renamed from: X.0Dy  reason: invalid class name and case insensitive filesystem */
public final class C02300Dy {
    private static volatile long A00 = -1;

    public static long A00() {
        long nanoTime = System.nanoTime();
        if (A00 != -1) {
            return A00;
        }
        return nanoTime;
    }
}
