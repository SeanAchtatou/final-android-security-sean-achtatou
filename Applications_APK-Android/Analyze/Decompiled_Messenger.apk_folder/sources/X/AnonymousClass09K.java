package X;

import com.facebook.profilo.config.v2.Config;
import java.util.TreeMap;

/* renamed from: X.09K  reason: invalid class name */
public final class AnonymousClass09K implements AnonymousClass05i {
    private int A00;
    private int A01;
    private AnonymousClass072 A02;
    private AnonymousClass056 A03;
    private AnonymousClass05Z A04;

    public static TreeMap A00(Config config, int i) {
        boolean optTraceConfigParamBool = config.optTraceConfigParamBool(i, "provider.atrace.single_lib_optimization", false);
        if (!optTraceConfigParamBool) {
            return null;
        }
        TreeMap treeMap = new TreeMap();
        treeMap.put("provider.atrace.single_lib_optimization", Boolean.valueOf(optTraceConfigParamBool));
        return treeMap;
    }

    public static TreeMap A01(Config config, int i) {
        int[] optTraceConfigParamIntList = config.optTraceConfigParamIntList(i, "provider.qpl.event_whitelist");
        if (optTraceConfigParamIntList == null || optTraceConfigParamIntList.length == 0) {
            return null;
        }
        TreeMap treeMap = new TreeMap();
        treeMap.put("provider.qpl.event_whitelist", optTraceConfigParamIntList);
        return treeMap;
    }

    public static TreeMap A02(Config config, int i) {
        int optTraceConfigParamInt = config.optTraceConfigParamInt(i, "provider.stack_trace.cpu_sampling_rate_ms", 0);
        int optTraceConfigParamInt2 = config.optTraceConfigParamInt(i, "provider.system_counters.sampling_rate_ms", 0);
        int optTraceConfigParamInt3 = config.optTraceConfigParamInt(i, "provider.high_freq_main_thread_counters.sampling_rate_ms", 0);
        int optTraceConfigParamInt4 = config.optTraceConfigParamInt(i, "provider.qpl.point_max_level", 0);
        if (optTraceConfigParamInt == 0 && optTraceConfigParamInt2 == 0 && optTraceConfigParamInt3 == 0 && optTraceConfigParamInt4 == 0) {
            return null;
        }
        TreeMap treeMap = new TreeMap();
        if (optTraceConfigParamInt > 0) {
            treeMap.put("provider.stack_trace.cpu_sampling_rate_ms", Integer.valueOf(optTraceConfigParamInt));
        }
        if (optTraceConfigParamInt2 > 0) {
            treeMap.put("provider.system_counters.sampling_rate_ms", Integer.valueOf(optTraceConfigParamInt2));
        }
        if (optTraceConfigParamInt3 > 0) {
            treeMap.put("provider.high_freq_main_thread_counters.sampling_rate_ms", Integer.valueOf(optTraceConfigParamInt3));
        }
        if (optTraceConfigParamInt4 > 0) {
            treeMap.put("provider.qpl.point_max_level", Integer.valueOf(optTraceConfigParamInt4));
        }
        int optTraceConfigParamInt5 = config.optTraceConfigParamInt(i, "trace_config.trace_timeout_ms", -1);
        if (optTraceConfigParamInt5 != -1) {
            treeMap.put("trace_config.trace_timeout_ms", Integer.valueOf(optTraceConfigParamInt5));
        }
        treeMap.put("trace_config.logger_priority", Integer.valueOf(config.optTraceConfigParamInt(i, "trace_config.logger_priority", 5)));
        treeMap.put("trace_config.post_trace_extension_ms", Integer.valueOf(config.optTraceConfigParamInt(i, "trace_config.post_trace_extension_ms", 0)));
        return treeMap;
    }

    public AnonymousClass057 Ai1(int i) {
        if (i == C003404k.A01 || i == C003404k.A02) {
            return this.A04;
        }
        if (i == C003604m.A01) {
            return this.A03;
        }
        if (i == C003704n.A01) {
            return this.A02;
        }
        return null;
    }

    public AnonymousClass09K(AnonymousClass05Z r1, AnonymousClass056 r2, AnonymousClass072 r3, int i, int i2) {
        this.A04 = r1;
        this.A03 = r2;
        this.A02 = r3;
        this.A01 = i;
        this.A00 = i2;
    }

    public int B6I() {
        return this.A00;
    }

    public int B6p() {
        return this.A01;
    }
}
