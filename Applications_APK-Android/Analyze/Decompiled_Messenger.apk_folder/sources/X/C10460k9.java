package X;

import java.io.Serializable;
import java.util.Map;

/* renamed from: X.0k9  reason: invalid class name and case insensitive filesystem */
public abstract class C10460k9 extends C10470kA implements Serializable {
    private static final int DEFAULT_MAPPER_FEATURES = C10470kA.collectFeatureDefaults(C26771bz.class);
    private static final long serialVersionUID = -8378230381628000111L;
    public final Map _mixInAnnotations;
    public final String _rootName;
    public final C10430k6 _subtypeResolver;
    public final Class _view;

    public final Class findMixInClassFor(Class cls) {
        Map map = this._mixInAnnotations;
        if (map == null) {
            return null;
        }
        return (Class) map.get(new C29041fm(cls));
    }

    public C10460k9(C10290jr r2, C10430k6 r3, Map map) {
        super(r2, DEFAULT_MAPPER_FEATURES);
        this._mixInAnnotations = map;
        this._subtypeResolver = r3;
        this._rootName = null;
        this._view = null;
    }

    public C10460k9(C10460k9 r2) {
        super(r2);
        this._mixInAnnotations = r2._mixInAnnotations;
        this._subtypeResolver = r2._subtypeResolver;
        this._rootName = r2._rootName;
        this._view = r2._view;
    }

    public C10460k9(C10460k9 r2, int i) {
        super(r2._base, i);
        this._mixInAnnotations = r2._mixInAnnotations;
        this._subtypeResolver = r2._subtypeResolver;
        this._rootName = r2._rootName;
        this._view = r2._view;
    }

    public C10460k9(C10460k9 r2, C10290jr r3) {
        super(r3, r2._mapperFeatures);
        this._mixInAnnotations = r2._mixInAnnotations;
        this._subtypeResolver = r2._subtypeResolver;
        this._rootName = r2._rootName;
        this._view = r2._view;
    }
}
