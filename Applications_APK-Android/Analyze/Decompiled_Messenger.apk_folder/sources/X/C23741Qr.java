package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.attachment.ImageData;

/* renamed from: X.1Qr  reason: invalid class name and case insensitive filesystem */
public final class C23741Qr implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ImageData(parcel);
    }

    public Object[] newArray(int i) {
        return new ImageData[i];
    }
}
