package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.1Qv  reason: invalid class name and case insensitive filesystem */
public final class C23781Qv extends C20831Dz implements C24161Sn {
    public C26751D9y A00;
    public ImmutableList A01;
    public Comparator A02 = new AnonymousClass23G();
    public final Context A03;
    public final Intent A04;
    public final AnonymousClass6Y6 A05;

    public static void A00(C23781Qv r7) {
        if (r7.A01 == null) {
            PackageManager packageManager = r7.A03.getPackageManager();
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(r7.A04, 65536);
            ArrayList arrayList = new ArrayList(queryIntentActivities.size());
            for (ResolveInfo next : queryIntentActivities) {
                arrayList.add(new C39161yb(next.loadIcon(packageManager), next.loadLabel(packageManager), next.activityInfo));
            }
            Collections.sort(arrayList, r7.A02);
            r7.A01 = ImmutableList.copyOf((Collection) arrayList);
        }
    }

    public C23781Qv(AnonymousClass1XY r2, Context context, Intent intent) {
        this.A05 = new AnonymousClass6Y6(r2);
        Preconditions.checkNotNull(context);
        this.A03 = context;
        Preconditions.checkNotNull(intent);
        this.A04 = intent;
    }

    public int ArU() {
        A00(this);
        return this.A01.size();
    }

    public Object getItem(int i) {
        A00(this);
        return this.A01.get(i);
    }
}
