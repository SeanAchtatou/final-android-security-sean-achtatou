package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.messages.Message;

/* renamed from: X.1nV  reason: invalid class name and case insensitive filesystem */
public final class C33391nV implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new Message(parcel);
    }

    public Object[] newArray(int i) {
        return new Message[i];
    }
}
