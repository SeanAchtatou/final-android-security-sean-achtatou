package X;

/* renamed from: X.0Gu  reason: invalid class name and case insensitive filesystem */
public final class C02880Gu implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        C02630Fr r62 = (C02630Fr) r6;
        int i = r62.acraActiveRadioTimeS;
        if (i != 0) {
            r7.AMU("acra_active_radio_time_s", i);
        }
        int i2 = r62.acraTailRadioTimeS;
        if (i2 != 0) {
            r7.AMU("acra_tail_radio_time_s", i2);
        }
        int i3 = r62.acraRadioWakeupCount;
        if (i3 != 0) {
            r7.AMU("acra_radio_wakeup_count", i3);
        }
        long j = r62.acraTxBytes;
        if (j != 0) {
            r7.AMV("acra_tx_bytes", j);
        }
    }
}
