package X;

/* renamed from: X.0aJ  reason: invalid class name and case insensitive filesystem */
public final class C05780aJ implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.crudolib.prefs.LightSharedPreferencesImpl$1";
    public final /* synthetic */ C05770aI A00;

    public C05780aJ(C05770aI r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x00c9 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            java.lang.String r1 = "LightSharedPreferences.tryLoadSharedPreference"
            r0 = -157432371(0xfffffffff69dc5cd, float:-1.6000049E33)
            X.AnonymousClass06K.A01(r1, r0)
            X.0aI r0 = r10.A00     // Catch:{ all -> 0x00fc }
            java.lang.Object r4 = r0.A02     // Catch:{ all -> 0x00fc }
            monitor-enter(r4)     // Catch:{ all -> 0x00fc }
            X.0aI r0 = r10.A00     // Catch:{ all -> 0x00f9 }
            X.0XS r5 = r0.A01     // Catch:{ all -> 0x00f9 }
            java.util.Map r6 = r0.A04     // Catch:{ all -> 0x00f9 }
            java.io.File r0 = r5.A00     // Catch:{ all -> 0x00f9 }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x00f9 }
            if (r0 == 0) goto L_0x00e5
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            java.io.File r0 = r5.A00     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            r1.<init>(r0)     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            r0 = 512(0x200, float:7.175E-43)
            r3.<init>(r1, r0)     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            r2.<init>(r3)     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            int r3 = r2.readUnsignedByte()     // Catch:{ all -> 0x00c3 }
            r0 = 1
            if (r3 != r0) goto L_0x00b7
            int r0 = r2.readInt()     // Catch:{ all -> 0x00c3 }
        L_0x0039:
            int r8 = r0 + -1
            if (r0 <= 0) goto L_0x00b3
            int r7 = r2.readUnsignedByte()     // Catch:{ all -> 0x00c3 }
            java.lang.String r3 = r2.readUTF()     // Catch:{ all -> 0x00c3 }
            switch(r7) {
                case 0: goto L_0x0054;
                case 1: goto L_0x0060;
                case 2: goto L_0x006c;
                case 3: goto L_0x0078;
                case 4: goto L_0x0084;
                case 5: goto L_0x0090;
                case 6: goto L_0x0098;
                default: goto L_0x0048;
            }     // Catch:{ all -> 0x00c3 }
        L_0x0048:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00c3 }
            java.lang.String r0 = "Unsupported type with ordinal: "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r7)     // Catch:{ all -> 0x00c3 }
            r1.<init>(r0)     // Catch:{ all -> 0x00c3 }
            throw r1     // Catch:{ all -> 0x00c3 }
        L_0x0054:
            boolean r0 = r2.readBoolean()     // Catch:{ all -> 0x00c3 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x00c3 }
            r6.put(r3, r0)     // Catch:{ all -> 0x00c3 }
            goto L_0x00b1
        L_0x0060:
            int r0 = r2.readInt()     // Catch:{ all -> 0x00c3 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00c3 }
            r6.put(r3, r0)     // Catch:{ all -> 0x00c3 }
            goto L_0x00b1
        L_0x006c:
            long r0 = r2.readLong()     // Catch:{ all -> 0x00c3 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x00c3 }
            r6.put(r3, r0)     // Catch:{ all -> 0x00c3 }
            goto L_0x00b1
        L_0x0078:
            float r0 = r2.readFloat()     // Catch:{ all -> 0x00c3 }
            java.lang.Float r0 = java.lang.Float.valueOf(r0)     // Catch:{ all -> 0x00c3 }
            r6.put(r3, r0)     // Catch:{ all -> 0x00c3 }
            goto L_0x00b1
        L_0x0084:
            double r0 = r2.readDouble()     // Catch:{ all -> 0x00c3 }
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ all -> 0x00c3 }
            r6.put(r3, r0)     // Catch:{ all -> 0x00c3 }
            goto L_0x00b1
        L_0x0090:
            java.lang.String r0 = r2.readUTF()     // Catch:{ all -> 0x00c3 }
            r6.put(r3, r0)     // Catch:{ all -> 0x00c3 }
            goto L_0x00b1
        L_0x0098:
            int r0 = r2.readInt()     // Catch:{ all -> 0x00c3 }
            java.util.HashSet r7 = new java.util.HashSet     // Catch:{ all -> 0x00c3 }
            r7.<init>(r0)     // Catch:{ all -> 0x00c3 }
        L_0x00a1:
            int r1 = r0 + -1
            if (r0 <= 0) goto L_0x00ae
            java.lang.String r0 = r2.readUTF()     // Catch:{ all -> 0x00c3 }
            r7.add(r0)     // Catch:{ all -> 0x00c3 }
            r0 = r1
            goto L_0x00a1
        L_0x00ae:
            r6.put(r3, r7)     // Catch:{ all -> 0x00c3 }
        L_0x00b1:
            r0 = r8
            goto L_0x0039
        L_0x00b3:
            r2.close()     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
            goto L_0x00e5
        L_0x00b7:
            X.1vz r1 = new X.1vz     // Catch:{ all -> 0x00c3 }
            java.lang.String r0 = "Expected version 1; got "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r3)     // Catch:{ all -> 0x00c3 }
            r1.<init>(r0)     // Catch:{ all -> 0x00c3 }
            throw r1     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00c5 }
        L_0x00c5:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x00c9 }
        L_0x00c9:
            throw r0     // Catch:{ 1vz | IOException | ArrayStoreException -> 0x00ca }
        L_0x00ca:
            r3 = move-exception
            java.lang.Class<X.0XS> r2 = X.AnonymousClass0XS.class
            java.io.File r0 = r5.A00     // Catch:{ all -> 0x00f9 }
            java.lang.String r1 = r0.getAbsolutePath()     // Catch:{ all -> 0x00f9 }
            java.lang.String r0 = r5.A02()     // Catch:{ all -> 0x00f9 }
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}     // Catch:{ all -> 0x00f9 }
            java.lang.String r0 = "Failed to read or parse SharedPreferences from: %s; Raw file: %s"
            X.C010708t.A0E(r2, r3, r0, r1)     // Catch:{ all -> 0x00f9 }
            java.io.File r0 = r5.A00     // Catch:{ all -> 0x00f9 }
            r0.delete()     // Catch:{ all -> 0x00f9 }
        L_0x00e5:
            monitor-exit(r4)     // Catch:{ all -> 0x00f9 }
            X.0aI r1 = r10.A00
            r0 = 1
            r1.A09 = r0
            X.0aI r0 = r10.A00
            java.util.concurrent.CountDownLatch r0 = r0.A05
            r0.countDown()
            r0 = -1903171987(0xffffffff8e8fe66d, float:-3.5474114E-30)
            X.AnonymousClass06K.A00(r0)
            return
        L_0x00f9:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00f9 }
            throw r0     // Catch:{ all -> 0x00fc }
        L_0x00fc:
            r2 = move-exception
            X.0aI r1 = r10.A00
            r0 = 1
            r1.A09 = r0
            X.0aI r0 = r10.A00
            java.util.concurrent.CountDownLatch r0 = r0.A05
            r0.countDown()
            r0 = 1959940098(0x74d25002, float:1.333014E32)
            X.AnonymousClass06K.A00(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05780aJ.run():void");
    }
}
