package X;

import java.util.Comparator;

/* renamed from: X.0BH  reason: invalid class name */
public final class AnonymousClass0BH implements Comparator {
    public int compare(Object obj, Object obj2) {
        return ((AnonymousClass0Ry) obj2).A01 - ((AnonymousClass0Ry) obj).A01;
    }
}
