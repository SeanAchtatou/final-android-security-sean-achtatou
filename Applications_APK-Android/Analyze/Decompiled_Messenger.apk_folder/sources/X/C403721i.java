package X;

import android.graphics.Bitmap;
import com.facebook.fresco.animation.factory.AnimatedFactoryV2Impl;

/* renamed from: X.21i  reason: invalid class name and case insensitive filesystem */
public final class C403721i implements C22761Ms {
    public final /* synthetic */ Bitmap.Config A00;
    public final /* synthetic */ AnimatedFactoryV2Impl A01;

    public C403721i(AnimatedFactoryV2Impl animatedFactoryV2Impl, Bitmap.Config config) {
        this.A01 = animatedFactoryV2Impl;
        this.A00 = config;
    }

    public AnonymousClass1S9 AWg(AnonymousClass1NY r3, int i, C33271nJ r5, C23541Px r6) {
        return AnimatedFactoryV2Impl.A00(this.A01).A03(r3, r6, this.A00);
    }
}
