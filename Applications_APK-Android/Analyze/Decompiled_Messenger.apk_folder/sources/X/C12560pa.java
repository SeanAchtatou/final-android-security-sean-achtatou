package X;

import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.0pa  reason: invalid class name and case insensitive filesystem */
public final class C12560pa {
    public long A00 = -1;
    public FolderCounts A01 = FolderCounts.A03;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public final AnonymousClass07X A05;
    public final AnonymousClass0mL A06;
    public final C10950l8 A07;

    public ThreadSummary A00(ThreadKey threadKey) {
        this.A06.A01();
        return (ThreadSummary) this.A05.remove(threadKey);
    }

    public void A01() {
        this.A06.A01();
        this.A05.clear();
        this.A03 = false;
        this.A00 = -1;
        this.A04 = false;
        this.A01 = FolderCounts.A03;
    }

    public void A02(ThreadSummary threadSummary) {
        C10950l8 r2;
        this.A06.A01();
        C10950l8 r3 = threadSummary.A0O;
        if (!((r3 == C10950l8.A0C && this.A07 == C10950l8.A05) || (r2 = this.A07) == C10950l8.A0B)) {
            Preconditions.checkArgument(r2.equals(r3), "cannot add thread in folder %s to cache folder %s", r3, r2);
        }
        this.A05.put(threadSummary.A0S, threadSummary);
    }

    public void A03(ThreadSummary threadSummary) {
        long j;
        this.A06.A01();
        C10950l8 r3 = this.A07;
        if (r3 != C10950l8.A0B || !threadSummary.A17) {
            C10950l8 r2 = threadSummary.A0O;
            Preconditions.checkArgument(r3.equals(r2), "cannot add thread in folder %s to cache folder %s", r2, r3);
        }
        ThreadKey threadKey = threadSummary.A0S;
        this.A06.A01();
        List list = this.A05.A01;
        if (list.isEmpty()) {
            j = -1;
        } else {
            j = ((ThreadSummary) list.get(list.size() - 1)).A0B;
        }
        long j2 = threadSummary.A0B;
        if (j2 <= j) {
            if (j2 == j) {
                if (A00(threadKey) != null) {
                    this.A05.put(threadKey, threadSummary);
                    return;
                }
                return;
            } else if (!this.A02) {
                A00(threadKey);
                if (this.A05.isEmpty()) {
                    A01();
                    return;
                }
                this.A02 = false;
                this.A04 = false;
                return;
            }
        }
        A02(threadSummary);
    }

    public C12560pa(C10950l8 r3, AnonymousClass0mL r4) {
        Comparator r0;
        this.A07 = r3;
        this.A06 = r4;
        if (r3 != C10950l8.A0B) {
            r0 = new C12610pg();
        } else {
            r0 = new C104784za();
        }
        this.A05 = new AnonymousClass07X(r0);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("folder", this.A07);
        stringHelper.add("includeFirst", this.A02);
        stringHelper.add("isLoaded", this.A03);
        stringHelper.add("upToDate", this.A04);
        stringHelper.add("lastFetchTimestamp", this.A00);
        stringHelper.add(C99084oO.$const$string(228), this.A05.keySet());
        return stringHelper.toString();
    }
}
