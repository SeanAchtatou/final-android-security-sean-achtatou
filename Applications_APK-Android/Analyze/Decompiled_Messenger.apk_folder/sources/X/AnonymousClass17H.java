package X;

import com.facebook.messaging.notify.DirectMessageStorySeenNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.17H  reason: invalid class name */
public final class AnonymousClass17H extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$29";
    public final /* synthetic */ DirectMessageStorySeenNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass17H(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, DirectMessageStorySeenNotification directMessageStorySeenNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = directMessageStorySeenNotification;
    }
}
