package X;

/* renamed from: X.0Go  reason: invalid class name and case insensitive filesystem */
public final class C02830Go implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        AnonymousClass0Fc r62 = (AnonymousClass0Fc) r6;
        long j = r62.rcharBytes;
        if (j != 0) {
            r7.AMV("rchar_bytes", j);
        }
        long j2 = r62.wcharBytes;
        if (j2 != 0) {
            r7.AMV("wchar_bytes", j2);
        }
        long j3 = r62.syscrCount;
        if (j3 != 0) {
            r7.AMV("syscr_count", j3);
        }
        long j4 = r62.syscwCount;
        if (j4 != 0) {
            r7.AMV("syscw_count", j4);
        }
        long j5 = r62.readBytes;
        if (j5 != 0) {
            r7.AMV("read_bytes", j5);
        }
        long j6 = r62.writeBytes;
        if (j6 != 0) {
            r7.AMV("write_bytes", j6);
        }
        long j7 = r62.cancelledWriteBytes;
        if (j7 != 0) {
            r7.AMV("cancelled_write_bytes", j7);
        }
        long j8 = r62.majorFaults;
        if (j8 != 0) {
            r7.AMV("major_faults_count", j8);
        }
        long j9 = r62.blkIoTicks;
        if (j9 != 0) {
            r7.AMV("blk_io_ticks", j9);
        }
    }
}
