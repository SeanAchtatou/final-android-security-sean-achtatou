package X;

import java.util.HashMap;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1ZT  reason: invalid class name */
public final class AnonymousClass1ZT {
    public final AnonymousClass8BA A00;
    public final AnonymousClass8BA A01;
    public final C06640bq A02;
    public final Pattern A03;

    public static AnonymousClass1ZT[] A00(String str) {
        AnonymousClass8BA r2;
        C06640bq r1;
        JSONObject jSONObject;
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (jSONArray.length() > 0) {
                AnonymousClass1ZT[] r7 = new AnonymousClass1ZT[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    String string = jSONObject2.getString("endpoint_name");
                    AnonymousClass8BA r3 = null;
                    if (jSONObject2.has("caller_info")) {
                        r2 = AnonymousClass8BA.A00(jSONObject2.getJSONObject("caller_info"));
                    } else {
                        r2 = null;
                    }
                    if (!jSONObject2.has("uri_component") || (jSONObject = jSONObject2.getJSONObject("uri_component")) == null) {
                        r1 = null;
                    } else {
                        try {
                            HashMap hashMap = new HashMap();
                            for (String str2 : C06640bq.A01) {
                                if (jSONObject.has(str2) && jSONObject.getString(str2) != null) {
                                    hashMap.put(str2, Pattern.compile(jSONObject.getString(str2), 32));
                                }
                            }
                            if (!hashMap.isEmpty()) {
                                r1 = new C06640bq(hashMap);
                            }
                        } catch (JSONException unused) {
                        }
                        r1 = null;
                    }
                    if (jSONObject2.has("intent_field")) {
                        r3 = AnonymousClass8BA.A00(jSONObject2.getJSONObject("intent_field"));
                    }
                    r7[i] = new AnonymousClass1ZT(string, r2, r1, r3);
                }
                return r7;
            }
        } catch (JSONException unused2) {
        }
        return new AnonymousClass1ZT[0];
    }

    private AnonymousClass1ZT(String str, AnonymousClass8BA r3, C06640bq r4, AnonymousClass8BA r5) {
        Pattern compile;
        if (str == null) {
            compile = null;
        } else {
            compile = Pattern.compile(str, 32);
        }
        this.A03 = compile;
        this.A01 = r5;
        this.A00 = r3;
        this.A02 = r4;
    }
}
