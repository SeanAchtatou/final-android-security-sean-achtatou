package X;

import com.facebook.acra.ACRA;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0lo  reason: invalid class name and case insensitive filesystem */
public final class C11070lo extends AnonymousClass0mF {
    public static final C11090lq A01 = new C11080lp();
    public AnonymousClass0UN A00;

    public C11070lo(AnonymousClass1XY r3) {
        super("MultiCacheServiceHandler");
        this.A00 = new AnonymousClass0UN(6, r3);
    }

    public static OperationResult A01(C11110lu r6, C11090lq r7) {
        HashMap hashMap = new HashMap(r6.A00.size());
        C07410dQ A012 = ImmutableSet.A01();
        Iterator it = r6.A00.iterator();
        while (it.hasNext()) {
            C12410pI r0 = (C12410pI) it.next();
            try {
                hashMap.put(r0.A02, r0.A00.BAz(r0.A01));
            } catch (Exception e) {
                A012.A01(e);
            }
        }
        ImmutableSet A04 = A012.build();
        OperationResult BL3 = r7.BL3(hashMap, A04);
        if (A04.isEmpty()) {
            return BL3;
        }
        throw new C97204kM(BL3, A04);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C11110lu A02(X.C11070lo r6, X.C10950l8 r7, X.C10700ki r8, X.C11060ln r9) {
        /*
            X.0lu r3 = new X.0lu
            r3.<init>()
            X.0ki r0 = X.C10700ki.SMS
            r4 = 0
            if (r8 == r0) goto L_0x0019
            X.0lv r2 = X.C11120lv.FACEBOOK
            int r1 = X.AnonymousClass1Y3.A49
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1cz r0 = (X.C27311cz) r0
            r3.A00(r2, r0, r9)
        L_0x0019:
            X.0l8 r0 = X.C10950l8.A05
            r4 = 2
            r5 = 1
            if (r7 != r0) goto L_0x0074
            X.0ki r0 = X.C10700ki.NON_SMS
            if (r8 == r0) goto L_0x0038
            boolean r0 = A07(r6)
            if (r0 == 0) goto L_0x0038
            X.0lv r2 = X.C11120lv.SMS
            int r1 = X.AnonymousClass1Y3.BQB
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.1cz r0 = (X.C27311cz) r0
            r3.A00(r2, r0, r9)
        L_0x0038:
            X.0ki r0 = X.C10700ki.SMS
            if (r8 == r0) goto L_0x005c
            r2 = 4
            int r1 = X.AnonymousClass1Y3.Abq
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pJ r0 = (X.C12420pJ) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x005c
            X.0lv r2 = X.C11120lv.TINCAN
            int r1 = X.AnonymousClass1Y3.B9l
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
        L_0x0057:
            X.1cz r0 = (X.C27311cz) r0
            r3.A00(r2, r0, r9)
        L_0x005c:
            java.util.ArrayList r0 = r3.A00
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0073
            java.lang.String r0 = r8.name()
            java.lang.Object[] r2 = new java.lang.Object[]{r7, r0, r9}
            java.lang.String r1 = "MultiCacheServiceHandler"
            java.lang.String r0 = "No available sub operation for FolderName: %s, ThreadTypeFilter: %s, OperationParams: %s."
            X.C010708t.A0P(r1, r0, r2)
        L_0x0073:
            return r3
        L_0x0074:
            X.0l8 r0 = X.C10950l8.A0C
            if (r7 != r0) goto L_0x005c
            X.0ki r0 = X.C10700ki.SMS
            if (r8 != r0) goto L_0x005c
            X.0lv r2 = X.C11120lv.SMS
            int r1 = X.AnonymousClass1Y3.BQB
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11070lo.A02(X.0lo, X.0l8, X.0ki, X.0ln):X.0lu");
    }

    public static C11120lv A03(ThreadKey threadKey) {
        C28711fF r3 = threadKey.A05;
        switch (r3.ordinal()) {
            case 0:
            case 1:
            case 4:
            case 5:
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
            case 8:
                return C11120lv.FACEBOOK;
            case 2:
            case 3:
                return C11120lv.TINCAN;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return C11120lv.SMS;
            default:
                throw new RuntimeException("Unexpected thread key type: " + r3);
        }
    }

    public static final C11070lo A04(AnonymousClass1XY r1) {
        return new C11070lo(r1);
    }

    public static HashMap A05(Iterable iterable) {
        HashMap hashMap = new HashMap();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            ThreadKey threadKey = (ThreadKey) it.next();
            C11120lv A03 = A03(threadKey);
            ArrayList arrayList = (ArrayList) hashMap.get(A03);
            if (arrayList == null) {
                arrayList = new ArrayList();
                hashMap.put(A03, arrayList);
            }
            arrayList.add(threadKey);
        }
        return hashMap;
    }

    public static boolean A07(C11070lo r3) {
        if (!((C10960l9) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BOk, r3.A00)).A0B() || !((C185814g) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AT9, r3.A00)).A02()) {
            return false;
        }
        return true;
    }

    public OperationResult A0z(C11060ln r4, C27311cz r5) {
        return ((C27311cz) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A49, this.A00)).BAz(r4);
    }

    public static C27311cz A00(C11070lo r3, C11120lv r4) {
        int i;
        int i2 = 1;
        switch (r4.ordinal()) {
            case 0:
                i2 = 0;
                i = AnonymousClass1Y3.A49;
                break;
            case 1:
                i = AnonymousClass1Y3.BQB;
                break;
            case 2:
                i2 = 2;
                i = AnonymousClass1Y3.B9l;
                break;
            default:
                throw new RuntimeException("Unexpected PermalinkCacheType: " + r4);
        }
        return (C27311cz) AnonymousClass1XX.A02(i2, i, r3.A00);
    }

    public static void A06(HashMap hashMap, ImmutableList immutableList) {
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            User user = (User) it.next();
            User user2 = (User) hashMap.get(user.A0Q);
            if (user2 == null || user.A00 > user2.A00) {
                hashMap.put(user.A0Q, user);
            }
        }
    }
}
