package X;

import com.facebook.forker.Process;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1CW  reason: invalid class name */
public final class AnonymousClass1CW {
    public int A00;
    public C72923fD A01;
    public C16070wR A02;
    public final List A03 = new ArrayList();

    public static AnonymousClass1CW A01(AnonymousClass1CW r24, AnonymousClass1CW r25) {
        int i;
        C72923fD r6;
        int i2 = 0;
        C72923fD r5 = null;
        AnonymousClass1CW A002 = A00(0, null, false);
        AnonymousClass1CW r0 = r24;
        if (r24 != null) {
            i = r0.A00;
        } else {
            i = 0;
        }
        AnonymousClass1CW r15 = r25;
        if (r25 != null) {
            i2 = r15.A00;
        }
        List list = A002.A03;
        if (r24 != null) {
            r6 = r0.A01;
        } else {
            r6 = null;
        }
        if (r25 != null) {
            r5 = r15.A01;
        }
        if (r24 != null) {
            for (AnonymousClass1IH r02 : r0.A03) {
                int i3 = r02.A03;
                int i4 = r02.A01;
                int i5 = r02.A02;
                int i6 = r02.A00;
                C21681Ih r4 = r02.A04;
                C21681Ih r22 = r4;
                int i7 = i6;
                int i8 = i5;
                int i9 = i4;
                int i10 = i3;
                list.add(new AnonymousClass1IH(i10, i9, i8, i7, r22, r02.A07, r02.A06, r02.A05));
            }
        }
        if (r15 != null) {
            for (AnonymousClass1IH r11 : r15.A03) {
                int i11 = r11.A02;
                int i12 = -1;
                if (i11 >= 0) {
                    i12 = i11 + i;
                }
                int i13 = r11.A00;
                C21681Ih r3 = r11.A04;
                C21681Ih r18 = r3;
                int i14 = i13;
                list.add(new AnonymousClass1IH(r11.A03, r11.A01 + i, i12, i14, r18, r11.A07, r11.A06, r11.A05));
            }
        }
        A002.A00 = i + i2;
        if (r6 != null) {
            if (r5 == null) {
                r5 = r6;
            } else {
                r5 = r6.A00(r5);
            }
        }
        A002.A01 = r5;
        return A002;
    }

    public static AnonymousClass1CW A00(int i, C16070wR r3, boolean z) {
        C72923fD r0;
        AnonymousClass1CW r1 = new AnonymousClass1CW();
        r1.A00 = i;
        r1.A02 = r3;
        if (z) {
            r0 = new C72923fD();
        } else {
            r0 = null;
        }
        r1.A01 = r0;
        return r1;
    }

    public static List A02(List list, AnonymousClass1KE r5) {
        if (r5 == null) {
            return list;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (int i = 0; i < list.size(); i++) {
            arrayList.add(new C21741In((C21681Ih) list.get(i), r5));
        }
        return arrayList;
    }

    public void A03() {
        for (AnonymousClass1IH r1 : this.A03) {
            r1.A04 = null;
            r1.A07 = null;
            r1.A06 = null;
            r1.A05 = null;
        }
        this.A03.clear();
        this.A01 = null;
        this.A00 = 0;
    }

    public void A04(AnonymousClass1IH r12) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        this.A03.add(r12);
        int i8 = r12.A03;
        int i9 = 1;
        if (i8 == -3) {
            i9 = -r12.A00;
        } else if (i8 == -1) {
            i9 = r12.A00;
        } else if (i8 != 1) {
            i9 = -1;
            if (i8 != 3) {
                i9 = 0;
            }
        }
        this.A00 += i9;
        C72923fD r1 = this.A01;
        if (r1 != null) {
            switch (i8) {
                case Process.SD_BLACK_HOLE:
                    i4 = r12.A00 + 0;
                    i = 0;
                    i2 = 0;
                    i3 = 0;
                    i5 = 0;
                    i7 = 0;
                    i6 = 0;
                    break;
                case -2:
                    i7 = r12.A00 + 0;
                    i = 0;
                    i2 = 0;
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    i6 = 0;
                    break;
                case -1:
                    i2 = r12.A00 + 0;
                    i = 0;
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    i7 = 0;
                    i6 = 0;
                    break;
                case 0:
                    i6 = r12.A00 + 0;
                    i = 0;
                    i2 = 0;
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    i7 = 0;
                    break;
                case 1:
                    i = 1;
                    i2 = 0;
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    i7 = 0;
                    i6 = 0;
                    break;
                case 2:
                    i = 0;
                    i2 = 0;
                    i3 = 0;
                    i4 = 0;
                    i5 = 1;
                    i7 = 0;
                    i6 = 0;
                    break;
                case 3:
                    i = 0;
                    i2 = 0;
                    i3 = 1;
                    i4 = 0;
                    i5 = 0;
                    i7 = 0;
                    i6 = 0;
                    break;
                default:
                    i = 0;
                    i2 = 0;
                    i3 = 0;
                    i4 = 0;
                    i5 = 0;
                    i7 = 0;
                    i6 = 0;
                    break;
            }
            this.A01 = r1.A00(new C72923fD(i9, i, i2, i3, i4, i5, i7, i6));
        }
    }

    private AnonymousClass1CW() {
    }
}
