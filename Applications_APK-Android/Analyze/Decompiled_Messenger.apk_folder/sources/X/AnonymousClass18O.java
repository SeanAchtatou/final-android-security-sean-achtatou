package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.18O  reason: invalid class name */
public final class AnonymousClass18O implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18O(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r9) {
        int A002 = AnonymousClass09Y.A00(-1829341985);
        C33741o4 r4 = this.A00;
        C193617v r0 = r4.A03;
        if (r0 != null) {
            r0.Btb(true, false, "my montage");
            r4.A03.BtU("my montage");
        }
        AnonymousClass09Y.A01(-1015356866, A002);
    }
}
