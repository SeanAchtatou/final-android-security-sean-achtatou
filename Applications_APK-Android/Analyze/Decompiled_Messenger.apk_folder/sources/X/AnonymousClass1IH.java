package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1IH  reason: invalid class name */
public final class AnonymousClass1IH {
    public static final List A08 = new ArrayList();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public C21681Ih A04;
    public List A05;
    public List A06;
    public List A07;

    public static AnonymousClass1IH A00(int i, int i2, C21681Ih r11, Object obj, Object obj2) {
        List list;
        List list2 = null;
        if (obj != null) {
            list = Collections.singletonList(obj);
        } else {
            list = null;
        }
        if (obj2 != null) {
            list2 = Collections.singletonList(obj2);
        }
        return new AnonymousClass1IH(i, i2, -1, 1, r11, null, list, list2);
    }

    public static AnonymousClass1IH A01(int i, int i2, Object obj) {
        List list;
        if (obj != null) {
            list = Collections.singletonList(obj);
        } else {
            list = null;
        }
        return new AnonymousClass1IH(0, i, i2, 1, null, null, list, list);
    }

    public AnonymousClass1IH(int i, int i2, int i3, int i4, C21681Ih r9, List list, List list2, List list3) {
        this.A03 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A00 = i4;
        this.A04 = r9 == null ? C21621Ib.A01() : r9;
        if (list == null) {
            this.A07 = A08;
        } else {
            int size = list.size();
            this.A07 = new ArrayList(size);
            for (int i5 = 0; i5 < size; i5++) {
                C21681Ih r1 = (C21681Ih) list.get(i5);
                List list4 = this.A07;
                if (r1 == null) {
                    r1 = C21621Ib.A01();
                }
                list4.add(r1);
            }
        }
        if (list2 != null) {
            this.A06 = Collections.unmodifiableList(list2);
        }
        if (list3 != null) {
            this.A05 = Collections.unmodifiableList(list3);
        }
    }
}
