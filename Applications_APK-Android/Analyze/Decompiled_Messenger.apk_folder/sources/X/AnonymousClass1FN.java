package X;

import android.os.Handler;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FN  reason: invalid class name */
public final class AnonymousClass1FN implements AnonymousClass1YQ, C13270r7 {
    private static volatile AnonymousClass1FN A06;
    public AnonymousClass0UN A00;
    public boolean A01;
    public final AnonymousClass0US A02;
    public final Set A03 = new C05180Xy();
    private final Handler A04;
    private final C04460Ut A05;

    private synchronized void A01(Boolean bool) {
        AnonymousClass1FP r1;
        C05180Xy r6 = new C05180Xy();
        ImmutableMap.Builder builder = ImmutableMap.builder();
        for (AnonymousClass1FI r0 : (Set) this.A02.get()) {
            ImmutableMap immutableMap = r0.get();
            C24971Xv it = immutableMap.keySet().iterator();
            while (true) {
                if (it.hasNext()) {
                    SubscribeTopic subscribeTopic = (SubscribeTopic) it.next();
                    if (r6.add(subscribeTopic.A01)) {
                        builder.put(subscribeTopic, immutableMap.get(subscribeTopic));
                    } else {
                        throw new IllegalStateException(AnonymousClass08S.A0J("Duplicate topics not allowed at this time: ", subscribeTopic.A01));
                    }
                }
            }
        }
        ImmutableMap build = builder.build();
        synchronized (this) {
            if (this.A01) {
                r1 = AnonymousClass1FP.APP_USE;
            } else {
                r1 = AnonymousClass1FP.ALWAYS;
            }
        }
        Set keySet = AnonymousClass0TG.A06(build, new C33001mg(r1)).keySet();
        Collection A022 = C25011Xz.A02(keySet, this.A03);
        Collection A023 = C25011Xz.A02(this.A03, keySet);
        if (bool != null) {
            C21171Fm r5 = (C21171Fm) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ai2, this.A00);
            boolean booleanValue = bool.booleanValue();
            if (A022 == null) {
                A022 = new ArrayList();
            }
            ImmutableList copyOf = ImmutableList.copyOf(A022);
            if (A023 == null) {
                A023 = new ArrayList();
            }
            AnonymousClass07A.A04(r5.A02, new EY1(r5, booleanValue, copyOf, ImmutableList.copyOf(A023)), 2087318792);
        } else {
            ((C21171Fm) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ai2, this.A00)).A03(A022, A023);
        }
        this.A03.clear();
        this.A03.addAll(keySet);
    }

    public synchronized void A02() {
        A01(null);
    }

    public String getSimpleName() {
        return "ClientSubscriptionAutoSubscriber";
    }

    public synchronized void onAppActive() {
        this.A01 = true;
        A01(true);
    }

    public synchronized void onAppPaused() {
    }

    public synchronized void onAppStopped() {
        this.A01 = false;
        A01(false);
    }

    public synchronized void onDeviceActive() {
        A02();
    }

    public synchronized void onDeviceStopped() {
        A02();
    }

    public static final AnonymousClass1FN A00(AnonymousClass1XY r5) {
        if (A06 == null) {
            synchronized (AnonymousClass1FN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        AnonymousClass1FG A003 = AnonymousClass1FG.A00(applicationInjector);
                        AnonymousClass0WT.A00(applicationInjector);
                        A06 = new AnonymousClass1FN(applicationInjector, A003);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private AnonymousClass1FN(AnonymousClass1XY r3, AnonymousClass1FG r4) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A05 = C04430Uq.A02(r3);
        this.A04 = C29321gE.A00(r3);
        this.A02 = r4.A00;
    }

    public void init() {
        int A032 = C000700l.A03(1805594197);
        AnonymousClass87G r2 = new AnonymousClass87G(this);
        C06600bl BMm = this.A05.BMm();
        BMm.A02("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE", r2);
        BMm.A01(this.A04);
        BMm.A00().A00();
        C000700l.A09(1824658963, A032);
    }
}
