package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1G3  reason: invalid class name */
public final class AnonymousClass1G3 {
    public final /* synthetic */ AnonymousClass16R A00;

    public AnonymousClass1G3(AnonymousClass16R r1) {
        this.A00 = r1;
    }

    public void A00(boolean z) {
        if (z) {
            ScheduledFuture scheduledFuture = this.A00.A0M;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(true);
                this.A00.A0M = null;
            }
            AnonymousClass16R r3 = this.A00;
            Integer num = AnonymousClass07B.A0N;
            if (!AnonymousClass16R.A08(r3, num, "active_now")) {
                AnonymousClass16R.A04(r3, num, "active_now");
            }
            AnonymousClass16R r2 = this.A00;
            r2.A00 = r2.A06.now();
            return;
        }
        C50352dq r7 = new C50352dq(this);
        C20921Ei r32 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, this.A00.A08);
        if (C20921Ei.A03(r32)) {
            synchronized (r32.A06) {
                r32.A01 = true;
            }
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r32.A00)).markerPoint(5505198, "active_now_loaded");
        }
        AnonymousClass16R r1 = this.A00;
        if (r1.A0M == null) {
            long now = r1.A06.now();
            AnonymousClass16R r8 = this.A00;
            if (now - r8.A00 > 4000) {
                Integer num2 = AnonymousClass07B.A0N;
                if (!AnonymousClass16R.A08(r8, num2, "active_now")) {
                    AnonymousClass16R.A04(r8, num2, "active_now");
                }
                this.A00.A00 = now;
                return;
            }
            r8.A0M = r8.A0L.schedule(r7, 4000, TimeUnit.MILLISECONDS);
        }
    }
}
