package X;

import android.content.Context;
import android.net.Uri;
import com.facebook.sosource.bsod.BSODActivity;
import java.util.Arrays;

/* renamed from: X.012  reason: invalid class name */
public final class AnonymousClass012 extends AnonymousClass013 {
    public static final Uri A00 = Uri.parse("https://www.facebook.com/help/messenger-app/522894144496549");
    private static final String[] A01 = {"user_storage_device_key", "tincan_db.*"};

    public String A05() {
        return "Messenger Crash Loop Remedy";
    }

    private static void A00(Context context, long j) {
        context.getSharedPreferences("crash_loop_critical_data", 0).edit().putLong("backoff_until", Long.valueOf(System.currentTimeMillis() + j).longValue()).commit();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x002f, code lost:
        if (java.lang.Long.valueOf(r11.getSharedPreferences("crash_loop_critical_data", 0).getLong("backoff_until", r4.longValue())).longValue() <= java.lang.System.currentTimeMillis()) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0172, code lost:
        if (r7.equals("launch_url") == false) goto L_0x0174;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C03670Oz A04(android.content.Context r11, int r12, int r13) {
        /*
            r10 = this;
            super.A04(r11, r12, r13)
            boolean r0 = X.C010408q.A00(r11)
            if (r0 != 0) goto L_0x0031
            r0 = 0
            java.lang.Long r4 = java.lang.Long.valueOf(r0)
            java.lang.String r3 = "backoff_until"
            java.lang.String r1 = "crash_loop_critical_data"
            r0 = 0
            android.content.SharedPreferences r2 = r11.getSharedPreferences(r1, r0)
            long r0 = r4.longValue()
            long r0 = r2.getLong(r3, r0)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            long r3 = r0.longValue()
            long r1 = java.lang.System.currentTimeMillis()
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r1 = 1
            if (r0 > 0) goto L_0x0032
        L_0x0031:
            r1 = 0
        L_0x0032:
            if (r1 != 0) goto L_0x01fd
            boolean r0 = X.C010408q.A00(r11)
            if (r0 == 0) goto L_0x004a
            java.lang.String r0 = r10.A05()     // Catch:{ all -> 0x0042 }
            r10.A06(r11, r12, r0)     // Catch:{ all -> 0x0042 }
            goto L_0x004a
        L_0x0042:
            r2 = move-exception
            java.lang.String r1 = "MessengerCrashLoopRemedyHandler"
            java.lang.String r0 = "unable to show notification"
            android.util.Log.w(r1, r0, r2)
        L_0x004a:
            java.lang.String r3 = "RemediationCodeFetcher"
            java.lang.String r0 = "https://graph.facebook.com/messenger_recovery"
            java.net.URL r7 = new java.net.URL     // Catch:{ MalformedURLException -> 0x019f }
            r7.<init>(r0)     // Catch:{ MalformedURLException -> 0x019f }
            android.os.StrictMode$ThreadPolicy r9 = android.os.StrictMode.getThreadPolicy()
            android.os.StrictMode$ThreadPolicy$Builder r0 = new android.os.StrictMode$ThreadPolicy$Builder
            r0.<init>()
            android.os.StrictMode$ThreadPolicy$Builder r0 = r0.permitAll()
            android.os.StrictMode$ThreadPolicy r0 = r0.build()
            android.os.StrictMode.setThreadPolicy(r0)
            r2 = 0
            java.lang.String r5 = "auth_token"
            java.lang.String r4 = ""
            java.lang.String r0 = "crash_loop_critical_data"
            r6 = 0
            android.content.SharedPreferences r0 = r11.getSharedPreferences(r0, r6)     // Catch:{ IOException -> 0x0190 }
            java.lang.String r1 = r0.getString(r5, r4)     // Catch:{ IOException -> 0x0190 }
            boolean r0 = r1.isEmpty()     // Catch:{ IOException -> 0x0190 }
            if (r0 == 0) goto L_0x0089
            X.00x r5 = X.C009308a.A02     // Catch:{ IOException -> 0x0190 }
            java.lang.String r4 = r5.A04     // Catch:{ IOException -> 0x0190 }
            java.lang.String r1 = "|"
            java.lang.String r0 = r5.A05     // Catch:{ IOException -> 0x0190 }
            java.lang.String r1 = X.AnonymousClass08S.A0P(r4, r1, r0)     // Catch:{ IOException -> 0x0190 }
        L_0x0089:
            java.lang.String r0 = "access_token="
            java.lang.String r5 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ IOException -> 0x0190 }
            android.content.pm.PackageManager r4 = r11.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00a8 }
            java.lang.String r1 = r11.getPackageName()     // Catch:{ NameNotFoundException -> 0x00a8 }
            android.content.pm.PackageInfo r0 = r4.getPackageInfo(r1, r6)     // Catch:{ NameNotFoundException -> 0x00a8 }
            java.lang.String r0 = r0.versionName     // Catch:{ IOException -> 0x0190 }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ IOException -> 0x0190 }
            java.lang.String r0 = "[FBAN/Orca-Android;FBAV/%s;]"
            java.lang.String r1 = java.lang.String.format(r0, r1)     // Catch:{ IOException -> 0x0190 }
            goto L_0x00aa
        L_0x00a8:
            java.lang.String r1 = ""
        L_0x00aa:
            java.net.URLConnection r4 = r7.openConnection()     // Catch:{ IOException -> 0x0190 }
            java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ IOException -> 0x0190 }
            r0 = 1
            r4.setDoOutput(r0)     // Catch:{ IOException -> 0x0190 }
            java.lang.String r0 = "POST"
            r4.setRequestMethod(r0)     // Catch:{ IOException -> 0x0190 }
            java.lang.String r0 = "User-Agent"
            r4.setRequestProperty(r0, r1)     // Catch:{ IOException -> 0x0190 }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0190 }
            r0 = -13633493(0xffffffffff2ff82b, float:-2.3390346E38)
            java.io.OutputStream r0 = X.AnonymousClass0EN.A01(r4, r0)     // Catch:{ IOException -> 0x0190 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0190 }
            byte[] r0 = r5.getBytes()     // Catch:{ IOException -> 0x0190 }
            r1.write(r0)     // Catch:{ IOException -> 0x0190 }
            int r1 = r4.getResponseCode()     // Catch:{ IOException -> 0x0190 }
            r0 = 200(0xc8, float:2.8E-43)
            if (r1 != r0) goto L_0x00e1
            r0 = 1824919863(0x6cc61137, float:1.9155889E27)
            java.io.InputStream r4 = X.AnonymousClass0EN.A00(r4, r0)     // Catch:{ IOException -> 0x0190 }
            goto L_0x00e2
        L_0x00e1:
            r4 = 0
        L_0x00e2:
            if (r4 == 0) goto L_0x0196
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0190 }
            java.lang.String r0 = "UTF-8"
            r1.<init>(r4, r0)     // Catch:{ IOException -> 0x0190 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0189 }
            r5.<init>(r1)     // Catch:{ IOException -> 0x0189 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0189 }
            r1.<init>()     // Catch:{ IOException -> 0x0189 }
        L_0x00f5:
            java.lang.String r0 = r5.readLine()     // Catch:{ IOException -> 0x0189 }
            if (r0 == 0) goto L_0x00ff
            r1.append(r0)     // Catch:{ IOException -> 0x0189 }
            goto L_0x00f5
        L_0x00ff:
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0189 }
            boolean r0 = r1.isEmpty()     // Catch:{ IOException -> 0x0189 }
            if (r0 != 0) goto L_0x0196
            java.lang.String r0 = "\""
            java.lang.String[] r7 = r1.split(r0)     // Catch:{ IOException -> 0x0189 }
            int r1 = r7.length     // Catch:{ IOException -> 0x0189 }
            r0 = 4
            if (r1 < r0) goto L_0x0196
            r8 = 0
            r1 = r7[r6]     // Catch:{ IOException -> 0x0189 }
            java.lang.String r0 = "{"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0189 }
            if (r0 == 0) goto L_0x0196
            r6 = 1
            r1 = r7[r6]     // Catch:{ IOException -> 0x0189 }
            java.lang.String r0 = "instruction"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0189 }
            if (r0 == 0) goto L_0x0196
            r5 = 2
            r1 = r7[r5]     // Catch:{ IOException -> 0x0189 }
            java.lang.String r0 = ":"
            boolean r0 = r1.equals(r0)     // Catch:{ IOException -> 0x0189 }
            if (r0 == 0) goto L_0x0196
            r1 = 3
            r7 = r7[r1]     // Catch:{ IOException -> 0x0189 }
            boolean r0 = r7.isEmpty()     // Catch:{ IOException -> 0x0189 }
            if (r0 == 0) goto L_0x0140
            java.lang.Integer r2 = X.AnonymousClass07B.A00     // Catch:{ IOException -> 0x0189 }
            goto L_0x0196
        L_0x0140:
            java.util.Locale r0 = java.util.Locale.US     // Catch:{ IOException -> 0x0189 }
            java.lang.String r7 = r7.toLowerCase(r0)     // Catch:{ IOException -> 0x0189 }
            int r0 = r7.hashCode()     // Catch:{ IOException -> 0x0189 }
            switch(r0) {
                case -497859813: goto L_0x014e;
                case 104988959: goto L_0x0158;
                case 212308314: goto L_0x0161;
                case 546768611: goto L_0x016b;
                default: goto L_0x014d;
            }     // Catch:{ IOException -> 0x0189 }
        L_0x014d:
            goto L_0x0174
        L_0x014e:
            java.lang.String r0 = "clean_state"
            boolean r0 = r7.equals(r0)     // Catch:{ IOException -> 0x0189 }
            r8 = 3
            if (r0 != 0) goto L_0x0175
            goto L_0x0174
        L_0x0158:
            java.lang.String r0 = "no_op"
            boolean r0 = r7.equals(r0)     // Catch:{ IOException -> 0x0189 }
            if (r0 == 0) goto L_0x0174
            goto L_0x0175
        L_0x0161:
            java.lang.String r0 = "clean_recoverable_state"
            boolean r0 = r7.equals(r0)     // Catch:{ IOException -> 0x0189 }
            r8 = 2
            if (r0 != 0) goto L_0x0175
            goto L_0x0174
        L_0x016b:
            java.lang.String r0 = "launch_url"
            boolean r0 = r7.equals(r0)     // Catch:{ IOException -> 0x0189 }
            r8 = 1
            if (r0 != 0) goto L_0x0175
        L_0x0174:
            r8 = -1
        L_0x0175:
            if (r8 == 0) goto L_0x0186
            if (r8 == r6) goto L_0x0183
            if (r8 == r5) goto L_0x0180
            if (r8 != r1) goto L_0x0196
            java.lang.Integer r2 = X.AnonymousClass07B.A0N     // Catch:{ IOException -> 0x0189 }
            goto L_0x0196
        L_0x0180:
            java.lang.Integer r2 = X.AnonymousClass07B.A0C     // Catch:{ IOException -> 0x0189 }
            goto L_0x0196
        L_0x0183:
            java.lang.Integer r2 = X.AnonymousClass07B.A01     // Catch:{ IOException -> 0x0189 }
            goto L_0x0196
        L_0x0186:
            java.lang.Integer r2 = X.AnonymousClass07B.A00     // Catch:{ IOException -> 0x0189 }
            goto L_0x0196
        L_0x0189:
            r1 = move-exception
            java.lang.String r0 = "Unable to parse server response"
            android.util.Log.e(r3, r0, r1)     // Catch:{ IOException -> 0x0190 }
            goto L_0x0196
        L_0x0190:
            r1 = move-exception
            java.lang.String r0 = "Unable to get remediation code from server"
            android.util.Log.e(r3, r0, r1)     // Catch:{ all -> 0x019a }
        L_0x0196:
            android.os.StrictMode.setThreadPolicy(r9)
            goto L_0x01a6
        L_0x019a:
            r0 = move-exception
            android.os.StrictMode.setThreadPolicy(r9)
            throw r0
        L_0x019f:
            java.lang.String r0 = "Got a malformed url https://graph.facebook.com/messenger_recovery"
            android.util.Log.w(r3, r0)
            java.lang.Integer r2 = X.AnonymousClass07B.A00
        L_0x01a6:
            if (r2 == 0) goto L_0x01ac
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r2 != r0) goto L_0x01b4
        L_0x01ac:
            boolean r0 = X.C010408q.A00(r11)
            if (r0 == 0) goto L_0x01b4
            java.lang.Integer r2 = X.AnonymousClass07B.A0C
        L_0x01b4:
            if (r2 == 0) goto L_0x01fd
            int[] r1 = X.C03540Ol.A00
            int r0 = r2.intValue()
            r4 = r1[r0]
            r0 = 1
            if (r4 == r0) goto L_0x01f7
            r0 = 2
            r3 = 0
            r1 = 10800000(0xa4cb80, double:5.335909E-317)
            if (r4 == r0) goto L_0x01e1
            r0 = 3
            if (r4 == r0) goto L_0x01de
            r0 = 4
            if (r4 != r0) goto L_0x01d6
            java.lang.String[] r0 = new java.lang.String[r3]
        L_0x01d0:
            r10.A07(r11, r0)
            A00(r11, r1)
        L_0x01d6:
            r2 = 0
            X.0Oz r1 = new X.0Oz
            r0 = 1
            r1.<init>(r0, r2)
            return r1
        L_0x01de:
            java.lang.String[] r0 = X.AnonymousClass012.A01
            goto L_0x01d0
        L_0x01e1:
            android.net.Uri r4 = X.AnonymousClass012.A00
            android.content.Intent r3 = new android.content.Intent
            java.lang.String r0 = "android.intent.action.VIEW"
            r3.<init>(r0, r4)
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            android.content.Intent r0 = r3.addFlags(r0)
            r11.startActivity(r0)
            A00(r11, r1)
            goto L_0x01d6
        L_0x01f7:
            r0 = 1800000(0x1b7740, double:8.89318E-318)
            A00(r11, r0)
        L_0x01fd:
            X.0Oz r1 = new X.0Oz
            r0 = 0
            r1.<init>(r0, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass012.A04(android.content.Context, int, int):X.0Oz");
    }

    public void A06(Context context, int i, String str) {
        BSODActivity.A00(context, 2131231430, 2131820613, 2131820612, 2131820611, 2131821487);
    }

    public String[] A08() {
        String[] A08 = super.A08();
        int length = A08.length;
        String[] strArr = (String[]) Arrays.copyOf(A08, length + 2);
        System.arraycopy(new String[]{"instacrash_threshold", "instacrash_interval"}, 0, strArr, length, 2);
        return strArr;
    }

    public AnonymousClass012() {
        this(new AnonymousClass0IH());
    }

    private AnonymousClass012(AnonymousClass0IH r1) {
    }
}
