package X;

import com.facebook.acra.info.ExternalProcessInfo;

/* renamed from: X.09P  reason: invalid class name */
public interface AnonymousClass09P {
    void Bz1(String str);

    void Bz2(String str, String str2);

    void Bz9(String str, AnonymousClass0Z1 r2);

    void C1g(String str);

    void C2k(Throwable th);

    void C2l(String str);

    void CGQ(AnonymousClass06F r1);

    void CGR(AnonymousClass06F r1, ExternalProcessInfo externalProcessInfo);

    void CGS(String str, String str2);

    void CGT(String str, String str2, int i);

    void CGU(String str, String str2, Throwable th, int i);

    void CGV(String str, Throwable th, int i);

    void CGW(AnonymousClass06F r1);

    void CGY(String str, String str2);

    void CGZ(String str, String str2, Throwable th);

    void CGa(String str, Throwable th);

    void removeCustomData(String str);

    void softReport(String str, String str2, Throwable th);

    void softReport(String str, Throwable th);
}
