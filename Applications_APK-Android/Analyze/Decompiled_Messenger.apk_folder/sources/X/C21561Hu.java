package X;

import com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import java.util.concurrent.Callable;

/* renamed from: X.1Hu  reason: invalid class name and case insensitive filesystem */
public final class C21561Hu implements Callable {
    public final /* synthetic */ AnonymousClass17S A00;
    public final /* synthetic */ C33461nc A01;

    public C21561Hu(AnonymousClass17S r1, C33461nc r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX INFO: finally extract failed */
    public Object call() {
        AnonymousClass101 r2;
        int i;
        Integer num;
        C21581Hw r3 = (C21581Hw) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BKZ, this.A00.A00);
        C33461nc r4 = this.A01;
        C005505z.A05("InboxAdsFetcherWithUnitStore.fetchInternal(%s)", r4.A00, -407768020);
        try {
            if (r4.A00 == C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
                r2 = C21581Hw.A02(r3);
                i = -125785580;
            } else {
                C26931cN A002 = C21581Hw.A00(r3);
                if (!Objects.equal(A002.A00.Azr(A002, (AnonymousClass0m4) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AZ6, r3.A00)), ((C06080ao) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ab0, r3.A00)).A03(new C11500nH(C05660a7.A00(r3.A01, "last_successful_cache_key"))))) {
                    num = AnonymousClass07B.A00;
                } else if (((C06080ao) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ab0, r3.A00)).A02(new C11500nH(C05660a7.A00(r3.A01, "last_successful_fetch_ms")), 0) == 0) {
                    num = AnonymousClass07B.A00;
                } else if (((C06080ao) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ab0, r3.A00)).A08(r3.A01.A01(), false)) {
                    num = AnonymousClass07B.A00;
                } else {
                    num = AnonymousClass07B.A01;
                }
                if (num == AnonymousClass07B.A00) {
                    r2 = C21581Hw.A02(r3);
                    i = -1184215986;
                } else {
                    C005505z.A03("doDatabaseFetch", -1384730511);
                    ImmutableList A003 = r3.A02.A00();
                    Class<GSMBuilderShape0S0000000> cls = GSMBuilderShape0S0000000.class;
                    GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("Viewer", cls, 1649765866);
                    GSMBuilderShape0S0000000 gSMBuilderShape0S00000002 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MessengerInboxUnitsConnection", cls, 921501124);
                    gSMBuilderShape0S00000002.setTreeList("nodes", C27161ck.A02(A003, C05850aR.A02()));
                    gSMBuilderShape0S0000000.setTree("messenger_inbox_units", (GSTModelShape1S0000000) gSMBuilderShape0S00000002.getResult(GSTModelShape1S0000000.class, 921501124));
                    GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSMBuilderShape0S0000000.getResult(GSTModelShape1S0000000.class, 1649765866);
                    C005505z.A00(653897847);
                    r2 = C21581Hw.A03(r3, AnonymousClass102.FROM_CACHE_UP_TO_DATE, gSTModelShape1S0000000);
                    i = 705437687;
                }
            }
            C005505z.A00(i);
            synchronized (this.A00) {
                this.A00.A02 = r2;
            }
            return r2;
        } catch (Throwable th) {
            C005505z.A00(-1965528752);
            throw th;
        }
    }
}
