package X;

import android.content.Context;
import com.facebook.ipc.stories.model.AudienceControlData;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1v4  reason: invalid class name and case insensitive filesystem */
public final class C37311v4 extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C73513gF A01;
    @Comparable(type = 13)
    public AudienceControlData A02;
    @Comparable(type = 13)
    public StoryCard A03;
    @Comparable(type = 13)
    public C88334Jw A04;
    @Comparable(type = 14)
    public AnonymousClass5CN A05 = new AnonymousClass5CN();
    @Comparable(type = 13)
    public C90204Sj A06;

    public C37311v4(Context context) {
        super("QuestionStickerOverlayContainerComponent");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }

    public static void A00(AnonymousClass0p4 r3, C860646n r4) {
        if (r3.A04 != null) {
            r3.A0F(new C61322yh(0, r4), "updateState:QuestionStickerOverlayContainerComponent.updateOverlayState");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0062, code lost:
        if (r1 != com.facebook.graphql.enums.GraphQLStoryOverlayQuestionStickerStyle.A01) goto L_0x0064;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C17770zR A0O(X.AnonymousClass0p4 r25) {
        /*
            r24 = this;
            r3 = r24
            com.facebook.ipc.stories.model.StoryCard r0 = r3.A03
            r23 = r0
            com.facebook.ipc.stories.model.AudienceControlData r0 = r3.A02
            r22 = r0
            X.3gF r7 = r3.A01
            X.4Jw r13 = r3.A04
            int r2 = X.AnonymousClass1Y3.AM1
            X.0UN r1 = r3.A00
            r0 = 2
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4go r12 = (X.C95644go) r12
            X.5CN r0 = r3.A05
            X.46n r11 = r0.selectedOverlay
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 796370404(0x2f77a5e4, float:2.2523455E-10)
            r0 = -320690508(0xffffffffece2a6b4, float:-2.1920357E27)
            com.facebook.graphservice.tree.TreeJNI r6 = r7.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r6
            r9 = 0
            if (r6 == 0) goto L_0x019c
            r0 = -1030340122(0xffffffffc29641e6, float:-75.12871)
            java.lang.String r8 = r6.A0P(r0)
            r0 = -1648472118(0xffffffff9dbe4fca, float:-5.037508E-21)
            java.lang.String r5 = r6.A0P(r0)
            r0 = 2036780306(0x7966cd12, float:7.4899225E34)
            java.lang.String r4 = r6.A0P(r0)
            r1 = -1457567128(0xffffffffa91f4a68, float:-3.536963E-14)
            r0 = 1722139962(0x66a5c53a, float:3.9141433E23)
            com.facebook.graphservice.tree.TreeJNI r0 = r7.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 != 0) goto L_0x019d
            r10 = r9
        L_0x0052:
            com.facebook.graphql.enums.GraphQLStoryOverlayQuestionStickerStyle r1 = com.facebook.graphql.enums.GraphQLStoryOverlayQuestionStickerStyle.A02
            r0 = -1541657706(0xffffffffa41c2b96, float:-3.3864027E-17)
            java.lang.Enum r1 = r7.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLStoryOverlayQuestionStickerStyle r1 = (com.facebook.graphql.enums.GraphQLStoryOverlayQuestionStickerStyle) r1
            if (r1 == 0) goto L_0x0064
            com.facebook.graphql.enums.GraphQLStoryOverlayQuestionStickerStyle r0 = com.facebook.graphql.enums.GraphQLStoryOverlayQuestionStickerStyle.A01
            r3 = 1
            if (r1 == r0) goto L_0x0065
        L_0x0064:
            r3 = 0
        L_0x0065:
            if (r8 == 0) goto L_0x019c
            if (r5 == 0) goto L_0x019c
            if (r4 == 0) goto L_0x019c
            java.lang.String r14 = r23.getId()
            java.lang.String r2 = r23.getId()
            java.lang.String r1 = r6.A3m()
            java.lang.Class<X.1v4> r9 = X.C37311v4.class
            r0 = r25
            java.lang.Object[] r6 = new java.lang.Object[]{r0, r2, r1}
            r2 = -1741503230(0xffffffff9832c502, float:-2.3105424E-24)
            X.10N r9 = X.C17780zS.A0E(r9, r0, r2, r6)
            java.lang.String r2 = r23.getId()
            java.lang.Class<X.1v4> r15 = X.C37311v4.class
            java.lang.Object[] r6 = new java.lang.Object[]{r0, r2, r1}
            r2 = 829436257(0x31703161, float:3.4952665E-9)
            X.10N r6 = X.C17780zS.A0E(r15, r0, r2, r6)
            java.lang.String r2 = "QuestionStickerOverlayContainerComponentSpec"
            r13.CIQ(r14, r2, r9, r6)
            X.4Q5 r9 = new X.4Q5
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = r23.A0J()
            int r6 = X.C95184fz.A01(r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = r23.A0J()
            int r2 = X.C95184fz.A00(r2)
            r9.<init>(r6, r2)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r7 = r7.B3v()
            android.content.res.Resources r6 = r0.A03()
            r2 = 2132148265(0x7f160029, float:1.9938503E38)
            int r2 = r6.getDimensionPixelSize(r2)
            int r2 = r2 << 1
            X.4gq r7 = r12.A07(r9, r7, r2)
            android.graphics.RectF r2 = r7.A01
            float r2 = r2.width()
            int r2 = (int) r2
            float r6 = (float) r2
            r9 = 1133248512(0x438c0000, float:280.0)
            android.content.res.Resources r2 = r0.A03()
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()
            float r2 = r2.density
            float r2 = r2 * r9
            float r6 = r6 / r2
            X.46n r2 = X.C860646n.RESPONSE_EDITOR_OVERLAY
            if (r11 != r2) goto L_0x01a3
            r6 = 11
            java.lang.String r11 = "backButtonPressEventHandler"
            java.lang.String r12 = "isIgQuestion"
            java.lang.String r13 = "isStickerTapped"
            java.lang.String r14 = "owner"
            r7 = 21
            java.lang.String r15 = X.C99084oO.$const$string(r7)
            java.lang.String r16 = "questionBackgroundColor"
            java.lang.String r17 = "questionString"
            java.lang.String r18 = "questionTextColor"
            java.lang.String r19 = "scalingFactor"
            java.lang.String r20 = "sendButtonClickHandler"
            java.lang.String r21 = "storyCard"
            java.lang.String[] r11 = new java.lang.String[]{r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r6)
            X.5yL r9 = new X.5yL
            r9.<init>()
            X.0zR r6 = r0.A04
            if (r6 == 0) goto L_0x0112
            java.lang.String r12 = r6.A06
            r9.A07 = r12
        L_0x0112:
            r7.clear()
            r6 = r22
            r9.A01 = r6
            r6 = 3
            r7.set(r6)
            r6 = 10
            r7.set(r6)
            r9.A06 = r4
            r4 = 5
            r7.set(r4)
            r9.A07 = r8
            r4 = 6
            r7.set(r4)
            r9.A08 = r5
            r4 = 7
            r7.set(r4)
            r9.A05 = r10
            r4 = 4
            r7.set(r4)
            r9.A09 = r3
            r3 = 1
            r7.set(r3)
            r3 = 1065353216(0x3f800000, float:1.0)
            r9.A00 = r3
            r3 = 8
            r7.set(r3)
            r3 = 1
            r9.A0A = r3
            r3 = 2
            r7.set(r3)
            java.lang.String r3 = r23.getId()
            java.lang.Class<X.1v4> r5 = X.C37311v4.class
            java.lang.Object[] r4 = new java.lang.Object[]{r0, r3, r1}
            r3 = -888481032(0xffffffffcb0adaf8, float:-9100024.0)
            X.10N r3 = X.C17780zS.A0E(r5, r0, r3, r4)
            r9.A03 = r3
            r3 = 9
            r7.set(r3)
            java.lang.String r3 = r23.getId()
            java.lang.Object[] r4 = new java.lang.Object[]{r0, r3, r1}
            r3 = 1991993034(0x76bb66ca, float:1.9004772E33)
            X.10N r3 = X.C17780zS.A0E(r5, r0, r3, r4)
            r9.A02 = r3
            r3 = 0
            r7.set(r3)
            X.46n r5 = X.C860646n.STICKER_OVERLAY
            java.lang.String r4 = r23.getId()
            java.lang.Class<X.1v4> r3 = X.C37311v4.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0, r2, r5, r4, r1}
            r1 = -1336101728(0xffffffffb05cb4a0, float:-8.029222E-10)
            X.10N r1 = X.C17780zS.A0E(r3, r0, r1, r2)
            X.11G r0 = r9.A14()
            r0.A0L(r1)
            r0 = 11
            X.AnonymousClass11F.A0C(r0, r7, r11)
        L_0x019c:
            return r9
        L_0x019d:
            java.lang.String r10 = r0.A41()
            goto L_0x0052
        L_0x01a3:
            r9 = 8
            java.lang.String r12 = "isIgQuestion"
            java.lang.String r13 = "isStickerTapped"
            r11 = 21
            java.lang.String r14 = X.C99084oO.$const$string(r11)
            java.lang.String r15 = "questionBackgroundColor"
            java.lang.String r16 = "questionString"
            java.lang.String r17 = "questionTextColor"
            java.lang.String r18 = "scalingFactor"
            java.lang.String r19 = "sendButtonClickHandler"
            java.lang.String[] r12 = new java.lang.String[]{r12, r13, r14, r15, r16, r17, r18, r19}
            java.util.BitSet r11 = new java.util.BitSet
            r11.<init>(r9)
            X.5yK r9 = new X.5yK
            r9.<init>()
            X.0zR r13 = r0.A04
            if (r13 == 0) goto L_0x01cf
            java.lang.String r14 = r13.A06
            r9.A07 = r14
        L_0x01cf:
            r11.clear()
            r9.A06 = r4
            r4 = 3
            r11.set(r4)
            r9.A07 = r8
            r4 = 4
            r11.set(r4)
            r9.A08 = r5
            r4 = 5
            r11.set(r4)
            r9.A05 = r10
            r4 = 2
            r11.set(r4)
            r9.A09 = r3
            r3 = 0
            r11.set(r3)
            r9.A00 = r6
            r3 = 6
            r11.set(r3)
            r3 = 0
            r9.A0A = r3
            r3 = 1
            r11.set(r3)
            android.graphics.RectF r3 = r7.A01
            float r3 = r3.width()
            int r4 = (int) r3
            X.11G r3 = r9.A14()
            r3.CNK(r4)
            android.graphics.RectF r3 = r7.A01
            float r3 = r3.height()
            int r4 = (int) r3
            X.11G r3 = r9.A14()
            r3.BL8(r4)
            double r3 = r7.A00
            float r5 = (float) r3
            X.11G r3 = r9.A14()
            r3.A05(r5)
            X.10G r5 = X.AnonymousClass10G.TOP
            android.graphics.RectF r3 = r7.A01
            float r3 = r3.top
            int r4 = (int) r3
            X.11G r3 = r9.A14()
            r3.BxI(r5, r4)
            X.10G r5 = X.AnonymousClass10G.START
            android.graphics.RectF r3 = r7.A01
            float r3 = r3.left
            int r4 = (int) r3
            X.11G r3 = r9.A14()
            r3.BxI(r5, r4)
            java.lang.String r3 = r23.getId()
            java.lang.Class<X.1v4> r5 = X.C37311v4.class
            java.lang.Object[] r4 = new java.lang.Object[]{r0, r3, r1}
            r3 = -888481032(0xffffffffcb0adaf8, float:-9100024.0)
            X.10N r3 = X.C17780zS.A0E(r5, r0, r3, r4)
            r9.A03 = r3
            r3 = 7
            r11.set(r3)
            X.46n r5 = X.C860646n.STICKER_OVERLAY
            java.lang.String r4 = r23.getId()
            java.lang.Class<X.1v4> r3 = X.C37311v4.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0, r5, r2, r4, r1}
            r1 = -1336101728(0xffffffffb05cb4a0, float:-8.029222E-10)
            X.10N r1 = X.C17780zS.A0E(r3, r0, r1, r2)
            X.11G r0 = r9.A14()
            r0.A0L(r1)
            r0 = 8
            X.AnonymousClass11F.A0C(r0, r11, r12)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37311v4.A0O(X.0p4):X.0zR");
    }

    public C17770zR A16() {
        C37311v4 r1 = (C37311v4) super.A16();
        r1.A05 = new AnonymousClass5CN();
        return r1;
    }
}
