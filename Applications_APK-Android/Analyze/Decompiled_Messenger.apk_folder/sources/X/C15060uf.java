package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.0uf  reason: invalid class name and case insensitive filesystem */
public final class C15060uf extends C17770zR {
    public int A00;
    public int A01;
    public Drawable A02;

    public C15060uf(Drawable drawable) {
        super("DrawableComponent");
        this.A02 = drawable;
    }
}
