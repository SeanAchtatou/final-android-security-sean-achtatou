package X;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1Kk  reason: invalid class name and case insensitive filesystem */
public final class C22231Kk {
    public static final AtomicBoolean A00 = new AtomicBoolean();
    public static final AtomicReference A01 = new AtomicReference();

    public static Typeface A00(Context context) {
        boolean compareAndSet;
        Typeface typeface = (Typeface) A01.get();
        if (typeface != null) {
            return typeface;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                A01.compareAndSet(null, Typeface.create(C99084oO.$const$string(82), 0));
            } catch (Exception e) {
                C010708t.A0A(C22231Kk.class, "Unable to load roboto medium", e);
            }
        } else if (!A00.get()) {
            AtomicBoolean atomicBoolean = A00;
            synchronized (atomicBoolean) {
                compareAndSet = atomicBoolean.compareAndSet(false, true);
            }
            if (compareAndSet) {
                File file = new File(context.getDir("asset_infra", 0), "Roboto-Medium.ttf");
                if (file.isFile()) {
                    A01.compareAndSet(null, Typeface.createFromFile(file));
                }
            }
        }
        return (Typeface) A01.get();
    }

    private C22231Kk() {
    }
}
