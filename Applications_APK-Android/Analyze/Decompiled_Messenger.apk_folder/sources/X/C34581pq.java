package X;

import com.google.common.base.MoreObjects;

/* renamed from: X.1pq  reason: invalid class name and case insensitive filesystem */
public final class C34581pq {
    public final long A00;
    public final long A01;
    public final long A02;
    public final AnonymousClass089 A03;
    public final boolean A04;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C34581pq r8 = (C34581pq) obj;
                if (!(this.A03 == r8.A03 && this.A00 == r8.A00 && this.A01 == r8.A01 && this.A04 == r8.A04)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public C34581pq(AnonymousClass089 r1, long j, long j2, long j3, boolean z) {
        this.A03 = r1;
        this.A02 = j;
        this.A00 = j2;
        this.A01 = j3;
        this.A04 = z;
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("mConnectionState", this.A03);
        stringHelper.add("mServiceGeneratedMs", this.A02);
        stringHelper.add("mLastConnectionMs", this.A00);
        stringHelper.add("mLastDisconnectMs", this.A01);
        stringHelper.add("mClockSkewDetected", this.A04);
        return stringHelper.toString();
    }
}
