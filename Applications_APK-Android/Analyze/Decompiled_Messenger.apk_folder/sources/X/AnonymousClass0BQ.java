package X;

import android.content.BroadcastReceiver;

/* renamed from: X.0BQ  reason: invalid class name */
public final class AnonymousClass0BQ extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0BO A00;

    public AnonymousClass0BQ(AnonymousClass0BO r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0068, code lost:
        r11.A00.A0L.run();
        X.C000700l.A0D(r13, 1068786995, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0075, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r12, android.content.Intent r13) {
        /*
            r11 = this;
            r0 = 1497928290(0x59489262, float:3.52849656E15)
            int r2 = X.C000700l.A01(r0)
            java.lang.String r1 = r13.getAction()
            X.0BO r0 = r11.A00
            java.lang.String r0 = r0.A0G
            boolean r0 = X.AnonymousClass0A2.A00(r1, r0)
            if (r0 != 0) goto L_0x001c
            r0 = -668639138(0xffffffffd825605e, float:-7.2733325E14)
            X.C000700l.A0D(r13, r0, r2)
            return
        L_0x001c:
            X.0BO r3 = r11.A00
            monitor-enter(r3)
            r13.getAction()     // Catch:{ all -> 0x0076 }
            android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0076 }
            X.0BO r4 = r11.A00     // Catch:{ all -> 0x0076 }
            long r0 = r4.A00     // Catch:{ all -> 0x0076 }
            r6 = 900000(0xdbba0, double:4.44659E-318)
            int r5 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x0038
            monitor-exit(r3)     // Catch:{ all -> 0x0076 }
            r0 = -1282971778(0xffffffffb387677e, float:-6.3052525E-8)
            X.C000700l.A0D(r13, r0, r2)
            return
        L_0x0038:
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0076 }
            long r8 = r8 + r0
            r4.A01 = r8     // Catch:{ all -> 0x0076 }
            boolean r0 = r4.A03     // Catch:{ all -> 0x0076 }
            if (r0 == 0) goto L_0x0057
            int r1 = r4.A04     // Catch:{ all -> 0x0076 }
            r0 = 23
            if (r1 < r0) goto L_0x0059
            boolean r0 = r4.A0I     // Catch:{ all -> 0x0076 }
            if (r0 == 0) goto L_0x0059
            X.07y r5 = r4.A0E     // Catch:{ all -> 0x0076 }
            android.app.AlarmManager r6 = r4.A05     // Catch:{ all -> 0x0076 }
            r7 = 2
            android.app.PendingIntent r10 = r4.A07     // Catch:{ all -> 0x0076 }
            r5.A04(r6, r7, r8, r10)     // Catch:{ all -> 0x0076 }
        L_0x0057:
            monitor-exit(r3)     // Catch:{ all -> 0x0076 }
            goto L_0x0068
        L_0x0059:
            r0 = 19
            if (r1 < r0) goto L_0x0057
            X.07y r5 = r4.A0E     // Catch:{ all -> 0x0076 }
            android.app.AlarmManager r6 = r4.A05     // Catch:{ all -> 0x0076 }
            r7 = 2
            android.app.PendingIntent r10 = r4.A07     // Catch:{ all -> 0x0076 }
            r5.A02(r6, r7, r8, r10)     // Catch:{ all -> 0x0076 }
            goto L_0x0057
        L_0x0068:
            X.0BO r0 = r11.A00
            java.lang.Runnable r0 = r0.A0L
            r0.run()
            r0 = 1068786995(0x3fb46533, float:1.4093384)
            X.C000700l.A0D(r13, r0, r2)
            return
        L_0x0076:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0076 }
            r0 = 208040442(0xc6671fa, float:1.7752854E-31)
            X.C000700l.A0D(r13, r0, r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BQ.onReceive(android.content.Context, android.content.Intent):void");
    }
}
