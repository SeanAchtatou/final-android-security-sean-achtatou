package X;

import com.facebook.storage.trash.FbTrashManager;
import java.io.File;
import java.util.UUID;

/* renamed from: X.1j4  reason: invalid class name and case insensitive filesystem */
public class C31051j4 {
    public final File A00;

    public boolean A01() {
        if (!(this instanceof FbTrashManager)) {
            return false;
        }
        return ((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, ((FbTrashManager) this).A00)).A0G();
    }

    public C31051j4(File file) {
        this.A00 = file;
    }

    public boolean A02(File file) {
        if (A01()) {
            return AnonymousClass9AZ.A02(file);
        }
        if (file.renameTo(new File(this.A00, UUID.randomUUID().toString())) || AnonymousClass9AZ.A02(file)) {
            return true;
        }
        return false;
    }
}
