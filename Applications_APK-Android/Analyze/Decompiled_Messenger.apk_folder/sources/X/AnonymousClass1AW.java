package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1AW  reason: invalid class name */
public class AnonymousClass1AW extends AnonymousClass1AX implements C20891Ef {
    public AnonymousClass3TZ A00;
    public Field A01;
    public Field A02;
    private Integer A03;
    private Integer A04;
    private Integer A05;
    private Integer A06;
    private Integer A07;
    public final Rect A08 = new Rect();
    private final List A09 = new ArrayList();

    public void A1q(RecyclerView recyclerView, C15930wD r4, int i) {
        C621830i createSmoothScroller;
        if (i != -1) {
            C3b();
            C127145y5 r1 = null;
            if (r1 == null) {
                createSmoothScroller = null;
            } else {
                createSmoothScroller = r1.createSmoothScroller(recyclerView.getContext(), this);
            }
            if (createSmoothScroller == null) {
                super.A1q(recyclerView, r4, i);
                return;
            }
            createSmoothScroller.A00 = i;
            A1I(createSmoothScroller);
        }
    }

    public void C3b() {
        this.A06 = null;
        this.A07 = null;
        this.A03 = null;
        this.A05 = null;
        this.A04 = null;
    }

    public void A14(int i, C15740vp r4) {
        C005505z.A03("BetterLinearLayoutManager.removeAndRecycleViewAt", -978182258);
        if (0 != 0) {
            try {
                A17(A0u(i));
                this.A09.add(new C30421Evw());
            } catch (Throwable th) {
                C005505z.A00(-225784203);
                throw th;
            }
        } else {
            super.A14(i, r4);
        }
        C005505z.A00(1015420813);
    }

    public void A18(View view, int i) {
        C005505z.A03("BetterLinearLayoutManager.addView", 259265234);
        try {
            super.A18(view, i);
        } finally {
            C005505z.A00(-662339497);
        }
    }

    public void A19(View view, int i, int i2) {
        C005505z.A03("BetterLinearLayoutManager.measureChildWithMargins", 240356205);
        try {
            super.A19(view, i, i2);
        } finally {
            C005505z.A00(1426560024);
        }
    }

    public void A1C(View view, C15740vp r4) {
        C005505z.A03("BetterLinearLayoutManager.removeAndRecycleView", -693411756);
        if (0 != 0) {
            try {
                A17(view);
                this.A09.add(new C30421Evw());
            } catch (Throwable th) {
                C005505z.A00(735302963);
                throw th;
            }
        } else {
            super.A1C(view, r4);
        }
        C005505z.A00(-914094184);
    }

    public int A1c(int i, C15740vp r6, C15930wD r7) {
        C005505z.A03("BetterLinearLayoutManager.scrollVerticallyBy", 1986522334);
        try {
            C3b();
            int A1c = super.A1c(i, r6, r7);
            C005505z.A00(-151016156);
            return A1c;
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("Adapter count: " + A0d() + " Scroll amount: " + i + " " + r7, e);
        } catch (Throwable th) {
            C005505z.A00(-365984667);
            throw th;
        }
    }

    public int A1t() {
        if (this.A03 == null) {
            this.A03 = Integer.valueOf(super.A1t());
        }
        return this.A03.intValue();
    }

    public int A1u() {
        if (this.A05 == null) {
            this.A05 = Integer.valueOf(super.A1u());
        }
        return this.A05.intValue();
    }

    public int A1v() {
        if (this.A06 == null) {
            this.A06 = Integer.valueOf(super.A1v());
        }
        return this.A06.intValue();
    }

    public int AZp() {
        if (this.A04 == null) {
            if (this.A00 == null) {
                this.A00 = new AnonymousClass3TZ(this);
            }
            this.A04 = Integer.valueOf(this.A00.A00());
        }
        return this.A04.intValue();
    }

    public int AZs() {
        if (this.A07 == null) {
            this.A07 = Integer.valueOf(super.AZs());
        }
        return this.A07.intValue();
    }

    public int A0e() {
        if (C191216w.A00()) {
            return super.A0e();
        }
        return this.A08.bottom;
    }

    public int A0f() {
        if (C191216w.A00()) {
            return super.A0f();
        }
        return this.A08.left;
    }

    public int A0g() {
        if (C191216w.A00()) {
            return super.A0g();
        }
        return this.A08.right;
    }

    public int A0h() {
        if (C191216w.A00()) {
            return super.A0h();
        }
        return this.A08.top;
    }

    public void A1j(int i) {
        C3b();
        super.A1j(i);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1m(X.C15740vp r5, X.C15930wD r6) {
        /*
            r4 = this;
            r4.C3b()
            super.A1m(r5, r6)
            r0 = 0
            if (r0 == 0) goto L_0x003c
            r3 = 0
            java.lang.reflect.Field r0 = r4.A01
            if (r0 != 0) goto L_0x0027
            java.lang.Class<X.19S> r2 = X.AnonymousClass19S.class
            java.lang.String r0 = "mLastStackFromEnd"
            java.lang.reflect.Field r0 = r2.getDeclaredField(r0)     // Catch:{ NoSuchFieldException -> 0x0036 }
            r4.A01 = r0     // Catch:{ NoSuchFieldException -> 0x0036 }
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ NoSuchFieldException -> 0x0036 }
            java.lang.String r0 = "mStackFromEnd"
            java.lang.reflect.Field r0 = r2.getDeclaredField(r0)     // Catch:{ NoSuchFieldException -> 0x0036 }
            r4.A02 = r0     // Catch:{ NoSuchFieldException -> 0x0036 }
            r0.setAccessible(r1)     // Catch:{ NoSuchFieldException -> 0x0036 }
        L_0x0027:
            java.lang.reflect.Field r0 = r4.A02     // Catch:{  }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r3)     // Catch:{  }
            r0.set(r4, r1)     // Catch:{  }
            java.lang.reflect.Field r0 = r4.A01     // Catch:{  }
            r0.set(r4, r1)     // Catch:{  }
            return
        L_0x0036:
            r0 = move-exception
            java.lang.RuntimeException r0 = X.AnonymousClass95E.A00(r0)
            throw r0
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AW.A1m(X.0vp, X.0wD):void");
    }

    public void A1z(int i) {
        super.A1z(i);
        if (this.A00 == null) {
            this.A00 = new AnonymousClass3TZ(this);
        }
        AnonymousClass3TZ r1 = this.A00;
        r1.A00 = C197219k.A00(r1.A01, i);
    }

    public void C4c(int i, int i2) {
        C3b();
        super.C4c(i, i2);
    }

    public AnonymousClass1AW() {
    }

    public AnonymousClass1AW(int i, boolean z) {
        super(i, z);
    }
}
