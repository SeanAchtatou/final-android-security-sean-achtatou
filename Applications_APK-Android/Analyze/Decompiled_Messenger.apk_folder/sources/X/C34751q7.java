package X;

/* renamed from: X.1q7  reason: invalid class name and case insensitive filesystem */
public final class C34751q7 {
    public final /* synthetic */ C32291lW A00;
    public final /* synthetic */ C13100qf A01;

    public C34751q7(C13100qf r1, C32291lW r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0033, code lost:
        if (r1.A0b != false) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(java.lang.Integer r5, android.os.Bundle r6) {
        /*
            r4 = this;
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0012
            X.1lW r1 = r4.A00
            X.0qf r0 = r4.A01
            android.app.Activity r0 = r0.A00
            r1.Bw0(r0)
            return
        L_0x0012:
            X.0qf r0 = r4.A01
            X.1lX r3 = X.C13100qf.A00(r0)
            int[] r1 = X.AnonymousClass2GN.A00
            int r0 = r5.intValue()
            r0 = r1[r0]
            switch(r0) {
                case 1: goto L_0x004d;
                case 2: goto L_0x004d;
                case 3: goto L_0x004d;
                case 4: goto L_0x004d;
                case 5: goto L_0x0050;
                case 6: goto L_0x0050;
                case 7: goto L_0x0050;
                default: goto L_0x0023;
            }
        L_0x0023:
            X.0wO r2 = X.C16040wO.INBOX
        L_0x0025:
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r3.A01
            androidx.fragment.app.Fragment r1 = r0.A0O(r1)
            if (r1 == 0) goto L_0x0035
            boolean r1 = r1.A0b
            r0 = 1
            if (r1 == 0) goto L_0x0036
        L_0x0035:
            r0 = 0
        L_0x0036:
            if (r0 != 0) goto L_0x003b
            r3.A05()
        L_0x003b:
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r3.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            com.google.common.base.Preconditions.checkNotNull(r0)
            X.0rS r0 = (X.C13450rS) r0
            r0.A2T(r2, r6)
            return
        L_0x004d:
            X.0wO r2 = X.C16040wO.CONTACTS
            goto L_0x0025
        L_0x0050:
            X.0wO r2 = X.C16040wO.DISCOVER
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34751q7.A00(java.lang.Integer, android.os.Bundle):void");
    }
}
