package X;

import android.app.Activity;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

/* renamed from: X.1er  reason: invalid class name and case insensitive filesystem */
public final class C28471er {
    private final ExecutorService A00;

    public static final C28471er A00(AnonymousClass1XY r1) {
        return new C28471er(r1);
    }

    public void A01(Activity activity, AnonymousClass0US r5) {
        if (Build.VERSION.SDK_INT >= 21 && activity != null && r5.get() != null && ((C001300x) r5.get()).A02 == C001500z.A03) {
            try {
                AnonymousClass07A.A04(this.A00, new C14630ti(new WeakReference(activity)), 480995207);
            } catch (RejectedExecutionException unused) {
            }
        }
    }

    private C28471er(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0UX.A0Z(r2);
    }
}
