package X;

/* renamed from: X.0FW  reason: invalid class name */
public final class AnonymousClass0FW extends C007907e {
    private final ThreadLocal A00 = new ThreadLocal();
    private final ThreadLocal A01 = new ThreadLocal();

    public /* bridge */ /* synthetic */ AnonymousClass0FM A03() {
        return new AnonymousClass0FV();
    }

    public boolean A04(AnonymousClass0FM r8) {
        AnonymousClass0FV r82 = (AnonymousClass0FV) r8;
        C02740Gd.A00(r82, "Null value passed to getSnapshot!");
        try {
            C02560Fk r2 = (C02560Fk) this.A01.get();
            if (r2 == null) {
                r2 = new C02560Fk("/proc/self/stat", 512);
                this.A01.set(r2);
            }
            r2.A04();
            if (!r2.A01) {
                return false;
            }
            for (int i = 0; i < 13; i++) {
                r2.A06();
            }
            r82.userTimeS = A00(r2);
            r82.systemTimeS = A00(r2);
            r82.childUserTimeS = A00(r2);
            r82.childSystemTimeS = A00(r2);
            if (this.A00.get() == null) {
                this.A00.set(new AnonymousClass0FV());
            }
            AnonymousClass0FV r4 = (AnonymousClass0FV) this.A00.get();
            if (Double.compare(r82.userTimeS, r4.userTimeS) < 0 || Double.compare(r82.systemTimeS, r4.systemTimeS) < 0 || Double.compare(r82.childUserTimeS, r4.childUserTimeS) < 0 || Double.compare(r82.childSystemTimeS, r4.childSystemTimeS) < 0) {
                AnonymousClass0KZ.A00("CpuMetricsCollector", AnonymousClass08S.A0S("Cpu Time Decreased from ", r4.toString(), " to ", r82.toString()), null);
                return false;
            }
            r4.A0B(r82);
            return true;
        } catch (AnonymousClass0KX e) {
            AnonymousClass0KZ.A00("CpuMetricsCollector", "Unable to parse CPU time field", e);
            return false;
        }
    }

    private static double A00(C02560Fk r6) {
        double A03 = (((double) r6.A03()) * 1.0d) / ((double) C02750Ge.A00);
        r6.A06();
        return A03;
    }
}
