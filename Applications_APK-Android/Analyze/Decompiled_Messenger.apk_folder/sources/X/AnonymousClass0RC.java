package X;

import android.os.SystemClock;

/* renamed from: X.0RC  reason: invalid class name */
public final class AnonymousClass0RC {
    public static final AnonymousClass0RC A02 = new AnonymousClass0RC();
    private long A00 = -1;
    private AnonymousClass0RD A01 = new AnonymousClass0RD();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public synchronized void A00(int i, boolean z) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long j = this.A00;
        long j2 = elapsedRealtime - j;
        AnonymousClass0RD r11 = this.A01;
        r11.A00++;
        long j3 = 14000;
        if (j < 0 || j2 > 14000) {
            r11.A03++;
        }
        if (z) {
            r11.A02 += i;
        } else {
            r11.A01 += i;
        }
        long j4 = r11.A04;
        if (j >= 0) {
            j3 = Math.min(14000L, j2);
        }
        r11.A04 = j4 + j3;
        this.A00 = elapsedRealtime;
    }
}
