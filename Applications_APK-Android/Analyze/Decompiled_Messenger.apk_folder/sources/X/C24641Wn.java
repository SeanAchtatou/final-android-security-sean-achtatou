package X;

/* renamed from: X.1Wn  reason: invalid class name and case insensitive filesystem */
public final class C24641Wn {
    public final boolean A00;
    private final Class A01;

    public boolean equals(Object obj) {
        if (!(obj instanceof C24641Wn)) {
            return false;
        }
        C24641Wn r4 = (C24641Wn) obj;
        if (!r4.A01.equals(this.A01) || r4.A00 != this.A00) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.A01.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.A00).hashCode();
    }

    public C24641Wn(Class cls, boolean z) {
        this.A01 = cls;
        this.A00 = z;
    }
}
