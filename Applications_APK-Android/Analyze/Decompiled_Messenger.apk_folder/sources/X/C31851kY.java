package X;

import com.facebook.zero.common.ZeroUrlRewriteRule;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.1kY  reason: invalid class name and case insensitive filesystem */
public interface C31851kY {
    public static final ImmutableList A00 = ImmutableList.of(new ZeroUrlRewriteRule("^(https?)://(api\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5}))($|\\?.*$|\\/.*$)", "$1://b-$2$5"), new ZeroUrlRewriteRule("^(https?)://(graph\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5}))($|\\?.*$|\\/.*$)", "$1://b-$2$5"), new ZeroUrlRewriteRule("^(https?)://(free|m|mobile|d|b-m)\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5})($|\\?.*$|\\/.*$)", "$1://m.$3facebook.com$4$5"), new ZeroUrlRewriteRule("^(https?)://(www|web|z-m-www)\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5})($|\\?.*$|\\/.*$)", "$1://www.$3facebook.com$4$5"));
    public static final ImmutableList A01 = RegularImmutableList.A02;

    boolean Bmi(ImmutableList immutableList);
}
