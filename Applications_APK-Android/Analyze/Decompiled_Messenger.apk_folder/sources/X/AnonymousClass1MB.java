package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.user.model.UserKey;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1MB  reason: invalid class name */
public final class AnonymousClass1MB implements CallerContextable {
    private static volatile AnonymousClass1MB A04 = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.media.profileimagefetcher.messenger.MessengerProfileImageFetcher";
    public C30661iR A00;
    public AnonymousClass0UN A01;
    private Boolean A02;
    private Boolean A03;

    public synchronized AnonymousClass1QO A01(AnonymousClass1Q0 r6, UserKey userKey, CallerContext callerContext) {
        if (this.A02 == null) {
            this.A02 = false;
        }
        if (this.A03 == null) {
            this.A03 = false;
        }
        if (this.A02.booleanValue() && ((C08790fx) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AsC, this.A01)).A0A(5505027)) {
            return C33521ni.A00(((AnonymousClass0VL) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AZx, this.A01)).CIF(new C191748xP(this, r6, userKey, callerContext)));
        } else if (this.A03.booleanValue()) {
            return C33521ni.A00(((AnonymousClass0WP) AnonymousClass1XX.A02(7, AnonymousClass1Y3.A8a, this.A01)).CIH("ThreadTile delayed load Until Idle", new C112485Xb(this, r6, callerContext), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY, AnonymousClass07B.A01));
        } else {
            return ((AnonymousClass1MZ) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AF0, this.A01)).A02(r6, callerContext);
        }
    }

    public static final AnonymousClass1MB A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass1MB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass1MB(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private AnonymousClass1MB(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(12, r3);
    }
}
