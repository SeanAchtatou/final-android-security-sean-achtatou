package X;

import android.os.HandlerThread;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eZ  reason: invalid class name and case insensitive filesystem */
public final class C08030eZ {
    private static volatile C08030eZ A03;
    private AnonymousClass0UN A00;
    public final HandlerThread A01;
    public final C26151az A02 = new C26151az();

    public static final C08030eZ A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C08030eZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C08030eZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public C26191b3 A01() {
        AnonymousClass09P r2 = (AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A00);
        if (C26191b3.A04 == null) {
            C26191b3.A04 = new C26191b3(r2);
        }
        C26191b3 r1 = C26191b3.A04;
        if (!r1.A03) {
            r1.A00 = r2;
            r1.A03 = true;
        }
        return r1;
    }

    private C08030eZ(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(3, r4);
        this.A00 = r2;
        HandlerThread A022 = ((AnonymousClass0V4) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Awb, r2)).A02("DependencyManagerDI", AnonymousClass0V7.BACKGROUND);
        this.A01 = A022;
        A022.start();
    }
}
