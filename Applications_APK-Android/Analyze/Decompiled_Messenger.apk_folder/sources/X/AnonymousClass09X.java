package X;

import com.facebook.acra.ActionId;
import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.io.File;
import java.util.TreeMap;

/* renamed from: X.09X  reason: invalid class name */
public final class AnonymousClass09X extends AnonymousClass04v {
    private final QuickPerformanceLogger A00;

    public void Bsb(int i, int i2, int i3, int i4) {
        while (i > 0) {
            this.A00.markerGenerate(8126471, 3, 0);
            i--;
        }
        while (i2 > 0) {
            this.A00.markerGenerate(8126472, 2, 0);
            i2--;
        }
        while (i3 > 0) {
            this.A00.markerGenerate(8126473, 2, 0);
            i3--;
        }
        while (i4 > 0) {
            this.A00.markerGenerate(8126474, 2, 0);
            i4--;
        }
    }

    private static void A00(TraceContext traceContext, TreeMap treeMap) {
        for (String put : TriggerRegistry.A00.A03(traceContext.A01)) {
            treeMap.put("controller", put);
        }
        int i = traceContext.A01;
        if (i == C003404k.A01 || i == C003704n.A01) {
            treeMap.put("markerid", Long.toString(traceContext.A04));
        }
    }

    public void BTJ() {
        this.A00.updateListenerMarkers();
    }

    public void Bsa(File file, long j) {
        TreeMap treeMap = new TreeMap();
        treeMap.put("trace_id", AnonymousClass0HU.A02(j));
        this.A00.markerGenerateWithAnnotations(8126465, 2, 0, treeMap);
    }

    public void Btg(File file) {
        this.A00.markerGenerate(8126469, 3, 0);
    }

    public void Bth(File file) {
        this.A00.markerGenerate(8126469, 2, 0);
    }

    public void onTraceAbort(TraceContext traceContext) {
        TreeMap treeMap = new TreeMap();
        A00(traceContext, treeMap);
        int i = traceContext.A00;
        int i2 = i & Integer.MAX_VALUE;
        boolean z = false;
        if ((i & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            z = true;
        }
        short s = 2;
        if (z) {
            s = 509;
            treeMap.put("abort_reason", AnonymousClass0PI.A00(i2));
        } else if (i2 == 1) {
            s = 51;
        } else if (i2 == 2) {
            return;
        } else {
            if (i2 == 3) {
                s = ActionId.MISSED_EVENT;
            } else if (i2 == 4) {
                s = ActionId.TIMEOUT;
            } else if (i2 == 5) {
                s = ActionId.NEW_START_FOUND;
            }
        }
        treeMap.put("trace_id", traceContext.A09);
        this.A00.markerGenerateWithAnnotations(8126466, s, 0, treeMap);
    }

    public void onTraceStart(TraceContext traceContext) {
        TreeMap treeMap = new TreeMap();
        A00(traceContext, treeMap);
        treeMap.put("trace_id", traceContext.A09);
        this.A00.markerGenerateWithAnnotations(8126512, 2, 0, treeMap);
    }

    public void onTraceStop(TraceContext traceContext) {
        TreeMap treeMap = new TreeMap();
        A00(traceContext, treeMap);
        treeMap.put("trace_id", traceContext.A09);
        this.A00.markerGenerateWithAnnotations(8126516, 2, 0, treeMap);
    }

    public AnonymousClass09X(QuickPerformanceLogger quickPerformanceLogger) {
        this.A00 = quickPerformanceLogger;
    }
}
