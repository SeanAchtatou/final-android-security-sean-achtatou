package X;

import android.content.Context;
import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0ik  reason: invalid class name and case insensitive filesystem */
public abstract class C09820ik extends AnonymousClass0iW {
    public final Object A00 = new Object();
    public final AtomicInteger A01 = new AtomicInteger();
    public volatile AnonymousClass1LI A02;

    public void A0A(int i) {
        String str;
        AnonymousClass0iU r4 = (AnonymousClass0iU) this;
        if (i == 0) {
            C09810ij r3 = (C09810ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BRo, r4.A00);
            if (r3.A01 != null) {
                synchronized (r3) {
                    if (r3.A01 != null) {
                        r3.A01.add(C09900is.A03);
                        return;
                    }
                }
            }
            ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r3.A00)).CH1(C87224Dv.A00);
        } else if (i == -2147483638) {
            AnonymousClass1Y7 A04 = r4.A01.A04("download_count");
            FbSharedPreferences fbSharedPreferences = (FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r4.A01.A00);
            int AqN = fbSharedPreferences.AqN(A04, 0);
            ((C09810ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BRo, r4.A00)).A01(AnonymousClass08S.A0G("SUCCESSFUL_DOWNLOAD_", Long.highestOneBit((long) AqN)), true);
            AnonymousClass1Y7 A042 = r4.A01.A04("download_blocked_time");
            C30281hn edit = fbSharedPreferences.edit();
            edit.C1B(A042);
            edit.Bz6(A04, AqN + 1);
            edit.commit();
        } else {
            switch (i) {
                case -2147483641:
                    str = "SUCCESSFULLY_LOADED_LATEST_FILE_FROM_CACHE";
                    break;
                case -2147483640:
                    str = "DOWNLOAD_BLOCKED_BY_DEVICE_CONDITION";
                    break;
                case -2147483639:
                    str = "DOWNLOAD_SUCCEEDED_BUT_SAVE_FAILED";
                    break;
                case -2147483638:
                    str = "SUCCESSFULLY_DOWNLOADED_FROM_NETWORK";
                    break;
                default:
                    switch (i) {
                        case 0:
                            str = "EXECUTOR_INITIALIZED";
                            break;
                        case 1:
                            str = "NO_DISK_CACHE";
                            break;
                        case 2:
                            str = TurboLoader.Locator.$const$string(118);
                            break;
                        case 3:
                            str = "OLD_FILE";
                            break;
                        case 4:
                            str = "LATEST_FILE";
                            break;
                        case 5:
                            str = "SUCCESSFULLY_LOADED_COLD_START_FILE";
                            break;
                        case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                            str = "SUCCESSFULLY_LOADED_OLD_FILE_FROM_CACHE";
                            break;
                        default:
                            str = "UNKNOWN_EVENT";
                            break;
                    }
            }
            boolean z = false;
            if ((i & Integer.MIN_VALUE) != 0) {
                z = true;
            }
            ((C09810ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BRo, r4.A00)).A01(str, z);
        }
    }

    public Context A0B() {
        return (Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, ((AnonymousClass0iU) this).A00);
    }

    public AnonymousClass1LI A0C() {
        int A002 = C35521rM.A00();
        return (A002 > 480 ? AnonymousClass1LD.A00 : A002 > 320 ? AnonymousClass1LD.A04 : A002 > 240 ? AnonymousClass1LD.A03 : A002 > 160 ? AnonymousClass1LD.A02 : AnonymousClass1LD.A01).A00();
    }

    public C09950ix A0D() {
        return C26631bl.A00;
    }

    public AnonymousClass1LS A0E() {
        return (AnonymousClass1LS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgU, ((AnonymousClass0iU) this).A00);
    }

    public C09880iq A0F() {
        return ((AnonymousClass0iU) this).A01;
    }

    public String A0G() {
        return "FacebookEmoji.ttf";
    }

    static {
        TimeUnit.DAYS.toSeconds(180);
    }

    public static File A02(Context context, String str, String str2) {
        String path = context.getApplicationContext().getFilesDir().getPath();
        return new File(new File(new File(path, str + '.' + "default" + '.' + "latest"), "storage"), str2);
    }
}
