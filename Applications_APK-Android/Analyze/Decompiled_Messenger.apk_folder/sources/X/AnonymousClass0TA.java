package X;

import com.facebook.messenger.app.MessengerLoggedInUserProvider;

/* renamed from: X.0TA  reason: invalid class name */
public abstract class AnonymousClass0TA extends AnonymousClass0TB {
    public C49362c5 A00;
    public AnonymousClass0jJ A01;
    public C04310Tq A02;

    public String A0I() {
        return AnonymousClass2G5.A00;
    }

    public boolean A0J() {
        if (!(this instanceof MessengerLoggedInUserProvider)) {
            return false;
        }
        return ((Boolean) ((MessengerLoggedInUserProvider) this).A00.get()).booleanValue();
    }

    public void A0G() {
        AnonymousClass1XX r1 = AnonymousClass1XX.get(getContext());
        this.A01 = C05040Xk.A04();
        this.A02 = AnonymousClass0XJ.A0K(r1);
        this.A00 = new C49362c5(r1);
    }
}
