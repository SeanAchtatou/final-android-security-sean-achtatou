package X;

import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1CK  reason: invalid class name */
public final class AnonymousClass1CK {
    private static volatile AnonymousClass1CK A03;
    public final C001500z A00;
    public final AnonymousClass1CL A01;
    public final C25051Yd A02;

    public static final AnonymousClass1CK A01(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass1CK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass1CK(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public float A02() {
        return (float) this.A02.Aki(1127063847764044L);
    }

    public int A03() {
        return (int) this.A02.At0(564113892246219L);
    }

    public String A04() {
        return this.A02.B4K(1153767093476917409L, BuildConfig.FLAVOR, AnonymousClass0XE.A06);
    }

    public String A05() {
        return this.A02.B4K(1153767093478555812L, BuildConfig.FLAVOR, AnonymousClass0XE.A06);
    }

    public boolean A06() {
        return this.A02.Aem(282226595988704L);
    }

    public boolean A07() {
        return this.A01.A01.Aen(282638915602160L, "android_messenger_search_h1_2019.show_search_tabs");
    }

    public boolean A08() {
        return this.A02.Aem(282638918878993L);
    }

    public boolean A09() {
        return this.A02.Aem(282638918813456L);
    }

    public boolean A0A() {
        return this.A02.Aem(282638918747919L);
    }

    private AnonymousClass1CK(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass0WT.A00(r2);
        this.A01 = AnonymousClass1CL.A00(r2);
        this.A00 = AnonymousClass0UU.A05(r2);
    }

    public static final AnonymousClass1CK A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
