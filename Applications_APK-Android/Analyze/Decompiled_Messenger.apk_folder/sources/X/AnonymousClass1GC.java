package X;

import com.facebook.acra.ACRA;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1GC  reason: invalid class name */
public final class AnonymousClass1GC {
    public AnonymousClass0UN A00;
    private final AnonymousClass1HF A01;
    private final AnonymousClass1HG A02;

    public static String A01(Integer num) {
        int i;
        switch (num.intValue()) {
            case 1:
                return "thread_type";
            case 2:
                return "initial_thread_id";
            case 3:
                return "thread_size";
            case 4:
                i = 165;
                break;
            case 5:
                return "is_nux";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                i = AnonymousClass1Y3.A47;
                break;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "metadata";
            default:
                return "source";
        }
        return C99084oO.$const$string(i);
    }

    public static final AnonymousClass1GC A00(AnonymousClass1XY r1) {
        return new AnonymousClass1GC(r1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x012c, code lost:
        if (com.google.common.base.Platform.stringIsNullOrEmpty(r13.A08) == false) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0234, code lost:
        if (com.google.common.base.Platform.stringIsNullOrEmpty(r13.A08) == false) goto L_0x0236;
     */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0209  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0212  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x022d  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x023e  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0285  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0292  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02d3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(android.content.Context r12, com.facebook.messaging.games.interfaces.GamesStartConfig r13) {
        /*
            r11 = this;
            java.lang.String r0 = r13.A09
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            r4 = 1
            r0 = r0 ^ r4
            com.google.common.base.Preconditions.checkArgument(r0)
            java.lang.String r1 = r13.A09
            java.lang.Integer r0 = X.AnonymousClass07B.A0S
            java.lang.String r0 = X.AnonymousClass2SW.A00(r0)
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x003d
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AwN
            X.0UN r0 = r11.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6oF r5 = (X.C144626oF) r5
            java.lang.String r1 = "Unknown games entry point"
            java.lang.String r0 = "messenger_games"
            X.06G r0 = X.AnonymousClass06F.A02(r0, r1)
            X.06F r3 = r0.A00()
            int r2 = X.AnonymousClass1Y3.Amr
            X.0UN r1 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.09P r0 = (X.AnonymousClass09P) r0
            r0.CGQ(r3)
        L_0x003d:
            boolean r0 = r13.A0H
            if (r0 == 0) goto L_0x011d
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r13.A02
            r3 = 0
            if (r0 == 0) goto L_0x0066
            int r1 = X.AnonymousClass1Y3.BOE
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1rH r0 = (X.C35471rH) r0
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 284919543829696(0x10322003314c0, double:1.40768958435012E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0066
            r3 = 1
        L_0x0066:
            if (r3 == 0) goto L_0x00de
            android.content.Intent r3 = new android.content.Intent
            java.lang.String r0 = "open_game_list_extension"
            r3.<init>(r0)
            X.6p8 r4 = new X.6p8
            r4.<init>()
            X.CUG r0 = X.CUG.A06
            r4.A04 = r0
            r0 = 2131825206(0x7f111236, float:1.9283262E38)
            r4.A01 = r0
            r0 = 2131231368(0x7f080288, float:1.8078815E38)
            r4.A00 = r0
            int r2 = X.AnonymousClass1Y3.BOE
            X.0UN r1 = r11.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1rH r0 = (X.C35471rH) r0
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 284919544681676(0x10322004014cc, double:1.40768958855946E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x00db
            X.6pD r0 = X.C145136pD.FULL_SCREEN_WITH_TOP_MARGIN
        L_0x00a5:
            r4.A03 = r0
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r13.A02
            r4.A05 = r1
            X.CUP r2 = new X.CUP
            r2.<init>()
            r2.A00 = r1
            java.lang.String r1 = r13.A09
            r2.A01 = r1
            java.lang.String r0 = "entryPoint"
            X.C28931fb.A06(r1, r0)
            com.facebook.messaging.games.chatextension.list.model.GamesListExtensionParams r0 = new com.facebook.messaging.games.chatextension.list.model.GamesListExtensionParams
            r0.<init>(r2)
            r4.A02 = r0
            com.facebook.messaging.extensions.common.ExtensionParams r1 = new com.facebook.messaging.extensions.common.ExtensionParams
            r1.<init>(r4)
            java.lang.String r0 = "extension_params"
            r3.putExtra(r0, r1)
            int r2 = X.AnonymousClass1Y3.AKb
            X.0UN r1 = r11.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0Ut r0 = (X.C04460Ut) r0
            r0.C4x(r3)
            return
        L_0x00db:
            X.6pD r0 = X.C145136pD.A01
            goto L_0x00a5
        L_0x00de:
            java.lang.Class<com.facebook.messaging.games.GamesSelectionActivity> r0 = com.facebook.messaging.games.GamesSelectionActivity.class
            android.content.Intent r3 = new android.content.Intent
            r3.<init>(r12, r0)
            java.lang.ClassLoader r0 = r0.getClassLoader()
            r3.setExtrasClassLoader(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r13.A02
            if (r1 == 0) goto L_0x00f5
            java.lang.String r0 = "thread_key"
            r3.putExtra(r0, r1)
        L_0x00f5:
            java.lang.String r1 = r13.A0C
            if (r1 == 0) goto L_0x00fe
            java.lang.String r0 = "section_type"
            r3.putExtra(r0, r1)
        L_0x00fe:
            java.lang.String r1 = r13.A0B
            if (r1 == 0) goto L_0x010b
            r0 = 187(0xbb, float:2.62E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r3.putExtra(r0, r1)
        L_0x010b:
            java.lang.String r1 = r13.A09
            java.lang.String r0 = "entry_point"
            r3.putExtra(r0, r1)
            java.lang.String r1 = r13.A0A
            if (r1 == 0) goto L_0x02bd
            java.lang.String r0 = "m_action_id"
            r3.putExtra(r0, r1)
            goto L_0x02bd
        L_0x011d:
            java.lang.String r0 = r13.A07
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 == 0) goto L_0x012e
            java.lang.String r0 = r13.A08
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            r0 = 0
            if (r1 != 0) goto L_0x012f
        L_0x012e:
            r0 = 1
        L_0x012f:
            com.google.common.base.Preconditions.checkArgument(r0)
            boolean r0 = r13.A0F
            if (r0 != 0) goto L_0x01e3
            X.1HG r6 = r11.A02
            java.lang.String r10 = r13.A09
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r13.A02
            r8 = 0
            java.lang.String r7 = r13.A05
            java.lang.String r5 = r13.A0D
            X.14b r0 = r6.A01
            X.0gA r9 = X.AnonymousClass1HG.A02
            r0.CH1(r9)
            X.14b r3 = r6.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            java.lang.String r2 = A01(r0)
            java.lang.String r0 = ": "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r0, r10)
            r3.AMv(r9, r0)
            if (r1 == 0) goto L_0x0198
            X.14b r9 = r6.A01
            X.0gA r2 = X.AnonymousClass1HG.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            java.lang.String r10 = A01(r0)
            boolean r0 = r1.A0L()
            if (r0 == 0) goto L_0x02f8
            java.lang.Integer r0 = X.AnonymousClass07B.A01
        L_0x016d:
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x02f4;
                case 2: goto L_0x02f0;
                case 3: goto L_0x02ec;
                default: goto L_0x0174;
            }
        L_0x0174:
            java.lang.String r3 = "one_to_one_thread"
        L_0x0176:
            java.lang.String r0 = ": "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r10, r0, r3)
            r9.AMv(r2, r0)
            X.14b r9 = r6.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            java.lang.String r3 = A01(r0)
            long r0 = r1.A0G()
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = ": "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r3, r0, r1)
            r9.AMv(r2, r0)
        L_0x0198:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r8)
            if (r0 != 0) goto L_0x01b1
            X.14b r3 = r6.A01
            X.0gA r2 = X.AnonymousClass1HG.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            java.lang.String r1 = A01(r0)
            java.lang.String r0 = ": "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r8)
            r3.AMv(r2, r0)
        L_0x01b1:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r7)
            if (r0 != 0) goto L_0x01ca
            X.14b r3 = r6.A01
            X.0gA r2 = X.AnonymousClass1HG.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            java.lang.String r1 = A01(r0)
            java.lang.String r0 = ": "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r7)
            r3.AMv(r2, r0)
        L_0x01ca:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 != 0) goto L_0x01e3
            X.14b r3 = r6.A01
            X.0gA r2 = X.AnonymousClass1HG.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            java.lang.String r1 = A01(r0)
            java.lang.String r0 = ": "
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r0, r5)
            r3.AMv(r2, r0)
        L_0x01e3:
            java.lang.String r3 = r13.A07
            if (r3 == 0) goto L_0x02d6
            X.1HG r2 = r11.A02
            com.google.common.collect.ImmutableMap r0 = r13.A04
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            if (r0 == 0) goto L_0x01f5
            r1.putAll(r0)
        L_0x01f5:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
        L_0x01f7:
            java.lang.String r0 = X.C144266nb.A00(r0)
            r1.put(r0, r3)
            X.AnonymousClass1HG.A01(r2, r1)
        L_0x0201:
            java.lang.String r1 = r13.A0A
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x020e
            X.1HF r0 = r11.A01
            r0.A01(r1, r4)
        L_0x020e:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r13.A02
            if (r0 == 0) goto L_0x02d3
            long r0 = r0.A0G()
            java.lang.String r4 = java.lang.String.valueOf(r0)
            com.google.common.base.Preconditions.checkNotNull(r4)
        L_0x021d:
            android.content.Intent r3 = new android.content.Intent
            java.lang.Class<com.facebook.quicksilver.QuicksilverActivity> r0 = com.facebook.quicksilver.QuicksilverActivity.class
            r3.<init>(r12, r0)
            java.lang.String r0 = r13.A07
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            r2 = 1
            if (r0 == 0) goto L_0x0236
            java.lang.String r0 = r13.A08
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            r0 = 0
            if (r1 != 0) goto L_0x0237
        L_0x0236:
            r0 = 1
        L_0x0237:
            com.google.common.base.Preconditions.checkArgument(r0)
            java.lang.String r1 = r13.A07
            if (r1 == 0) goto L_0x0243
            java.lang.String r0 = "app_id"
            r3.putExtra(r0, r1)
        L_0x0243:
            java.lang.String r1 = r13.A09
            java.lang.String r0 = "source"
            r3.putExtra(r0, r1)
            com.facebook.graphql.enums.GraphQLInstantGameContextType r1 = com.facebook.graphql.enums.GraphQLInstantGameContextType.A06
            r0 = 43
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r3.putExtra(r0, r1)
            r0 = 322(0x142, float:4.51E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r3.putExtra(r0, r4)
            com.facebook.messaging.model.messages.Message r0 = r13.A01
            if (r0 == 0) goto L_0x026d
            java.lang.String r1 = r0.A0q
            r0 = 247(0xf7, float:3.46E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r3.putExtra(r0, r1)
        L_0x026d:
            X.0gA r0 = X.AnonymousClass1HG.A02
            java.lang.String r1 = r0.A0D
            java.lang.String r0 = "funnel_definition"
            r3.putExtra(r0, r1)
            boolean r1 = r13.A0E
            r0 = 325(0x145, float:4.55E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r3.putExtra(r0, r1)
            java.lang.String r1 = r13.A06
            if (r1 == 0) goto L_0x028e
            r0 = 32
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r3.putExtra(r0, r1)
        L_0x028e:
            com.google.common.collect.ImmutableList r0 = r13.A03
            if (r0 == 0) goto L_0x029c
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r0)
            java.lang.String r0 = "required_permission_list"
            r3.putStringArrayListExtra(r0, r1)
        L_0x029c:
            java.lang.String r1 = r13.A08
            if (r1 == 0) goto L_0x02a9
            r0 = 78
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r3.putExtra(r0, r1)
        L_0x02a9:
            boolean r0 = r13.A0G
            if (r0 == 0) goto L_0x02b2
            java.lang.String r0 = "should_skip_tos"
            r3.putExtra(r0, r2)
        L_0x02b2:
            android.content.Context r0 = r12.getApplicationContext()
            if (r12 != r0) goto L_0x02bd
            r0 = 805306368(0x30000000, float:4.656613E-10)
            r3.setFlags(r0)
        L_0x02bd:
            X.A1g r0 = r13.A00
            if (r0 == 0) goto L_0x0310
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x0310
            X.A1g r0 = r13.A00
            java.lang.Object r0 = r0.get()
            X.EpY r0 = (X.C30099EpY) r0
            r0.BHm(r3)
            return
        L_0x02d3:
            r4 = 0
            goto L_0x021d
        L_0x02d6:
            java.lang.String r3 = r13.A08
            if (r3 == 0) goto L_0x0201
            X.1HG r2 = r11.A02
            com.google.common.collect.ImmutableMap r0 = r13.A04
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            if (r0 == 0) goto L_0x02e8
            r1.putAll(r0)
        L_0x02e8:
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            goto L_0x01f7
        L_0x02ec:
            java.lang.String r3 = "unknown"
            goto L_0x0176
        L_0x02f0:
            java.lang.String r3 = "self_thread"
            goto L_0x0176
        L_0x02f4:
            java.lang.String r3 = "group_thread"
            goto L_0x0176
        L_0x02f8:
            boolean r0 = r1.A0Q()
            if (r0 == 0) goto L_0x0302
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            goto L_0x016d
        L_0x0302:
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0B(r1)
            if (r0 == 0) goto L_0x030c
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            goto L_0x016d
        L_0x030c:
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            goto L_0x016d
        L_0x0310:
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AT5
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pR r0 = (X.C12480pR) r0
            X.1eJ r0 = r0.A03
            r0.A0B(r3, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1GC.A02(android.content.Context, com.facebook.messaging.games.interfaces.GamesStartConfig):void");
    }

    public AnonymousClass1GC(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A02 = new AnonymousClass1HG(r3);
        this.A01 = AnonymousClass1HF.A00(r3);
    }
}
