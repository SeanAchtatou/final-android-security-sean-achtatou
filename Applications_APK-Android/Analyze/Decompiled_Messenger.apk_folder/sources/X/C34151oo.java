package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.storage.trash.FbTrashManager;
import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1oo  reason: invalid class name and case insensitive filesystem */
public final class C34151oo extends C34161op implements C05460Za, C34181or, C34171oq {
    private static volatile C34151oo A03;
    public AnonymousClass0UN A00;
    private AnonymousClass150 A01;
    private final AnonymousClass15V A02;

    public void A04(C31071j6 r7, AnonymousClass13z r8, File file) {
        try {
            int i = AnonymousClass1Y3.BBd;
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(3, i, this.A00)).markerStart(38469637);
            if (((QuickPerformanceLogger) AnonymousClass1XX.A02(3, i, this.A00)).isMarkerOn(38469637)) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(3, i, this.A00)).markerAnnotate(38469637, "feature", r7.A03);
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(38469637, "plugin", r8.A02());
            }
            super.A04(r7, r8, file);
        } finally {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BBd, this.A00)).markerEnd(38469637, 2);
        }
    }

    public synchronized AnonymousClass15T B43() {
        if (this.A01 == null) {
            this.A01 = new AnonymousClass150(this.A02, "user_scope");
        }
        return this.A01;
    }

    public static final C34151oo A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C34151oo.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C34151oo(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public boolean AWt(File file) {
        return ((FbTrashManager) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AN9, this.A00)).A02(file);
    }

    public Executor Ady() {
        return (ExecutorService) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Aoc, this.A00);
    }

    private C34151oo(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A02 = new AnonymousClass15V(r3);
    }

    public void clearUserData() {
        A02();
        ((FbTrashManager) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AN9, this.A00)).A03();
    }
}
