package X;

/* renamed from: X.20L  reason: invalid class name */
public final class AnonymousClass20L {
    public final int A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    public AnonymousClass20L(String str, String str2, String str3, int i, String str4, String str5) {
        this.A04 = str;
        this.A05 = str2;
        this.A01 = str3;
        this.A00 = i;
        this.A03 = str4;
        this.A02 = str5;
    }
}
