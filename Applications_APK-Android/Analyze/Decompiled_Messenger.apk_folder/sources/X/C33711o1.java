package X;

import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.util.TriState;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

@UserScoped
/* renamed from: X.1o1  reason: invalid class name and case insensitive filesystem */
public final class C33711o1 {
    private static C05540Zi A04;
    public final Resources A00;
    public final C33701o0 A01;
    public final C25051Yd A02;
    private final C04310Tq A03;

    public static int A00(MigColorScheme migColorScheme) {
        if (migColorScheme != null) {
            return migColorScheme.B9m();
        }
        return C17190yT.A00().B9m();
    }

    public static int A01(MigColorScheme migColorScheme) {
        if (migColorScheme == null) {
            migColorScheme = C17190yT.A00();
        }
        return migColorScheme.B2C().AhV();
    }

    public static final C33711o1 A02(AnonymousClass1XY r6) {
        C33711o1 r0;
        synchronized (C33711o1.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r6)) {
                    AnonymousClass1XY r4 = (AnonymousClass1XY) A04.A01();
                    A04.A00 = new C33711o1(r4, AnonymousClass0VB.A00(AnonymousClass1Y3.ALK, r4), AnonymousClass0VB.A00(AnonymousClass1Y3.B8Y, r4));
                }
                C05540Zi r1 = A04;
                r0 = (C33711o1) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A03(C33711o1 r4, C46862Se r5, MigColorScheme migColorScheme) {
        Resources resources;
        int i;
        int AhV;
        if (!r4.A02.Aeo(285117108983168L, false)) {
            r5.A01(null);
            return;
        }
        if (!((TriState) r4.A03.get()).asBoolean(false)) {
            resources = r4.A00;
            i = 2131823146;
        } else {
            resources = r4.A00;
            i = 2131823144;
        }
        r5.A01(resources.getString(i));
        if (migColorScheme != null) {
            AhV = migColorScheme.AbV();
        } else {
            AhV = C114945d3.A01.AhV();
        }
        r5.A01 = AhV;
    }

    public AnonymousClass3AN A04(MigColorScheme migColorScheme) {
        String string = this.A00.getString(2131821428);
        ColorDrawable colorDrawable = new ColorDrawable(A00(migColorScheme));
        C46862Se r2 = new C46862Se();
        r2.A07 = string;
        if (migColorScheme == null) {
            migColorScheme = C17190yT.A00();
        }
        r2.A02 = migColorScheme.B0J().AhV();
        r2.A00 = this.A00.getDimension(AnonymousClass1I7.SMALL.B5j());
        r2.A03 = colorDrawable;
        r2.A08 = AnonymousClass07B.A01;
        r2.A09 = false;
        r2.A01(null);
        return r2.A00();
    }

    private C33711o1(AnonymousClass1XY r4, C04310Tq r5, C04310Tq r6) {
        Object obj;
        this.A02 = AnonymousClass0WT.A00(r4);
        this.A00 = C04490Ux.A0L(r4);
        C29001fi.A00(r4);
        this.A03 = C04750Wa.A05(r4);
        if (this.A02.Aem(282780646770545L)) {
            obj = r5.get();
        } else {
            obj = r6.get();
        }
        this.A01 = (C33701o0) obj;
    }
}
