package X;

/* renamed from: X.0wA  reason: invalid class name and case insensitive filesystem */
public final class C15900wA {
    public final AnonymousClass04a A00 = new AnonymousClass04a();
    public final C17610zB A01 = new C17610zB();

    public static C60862xx A00(C15900wA r4, C33781o8 r5, int i) {
        C60862xx r1;
        int A06 = r4.A00.A06(r5);
        if (A06 >= 0) {
            AnonymousClass04a r42 = r4.A00;
            C37491vl r3 = (C37491vl) r42.A09(A06);
            if (r3 != null) {
                int i2 = r3.A00;
                if ((i2 & i) != 0) {
                    int i3 = i2 & (i ^ -1);
                    r3.A00 = i3;
                    if (i == 4) {
                        r1 = r3.A02;
                    } else if (i == 8) {
                        r1 = r3.A01;
                    } else {
                        throw new IllegalArgumentException("Must provide flag PRE or POST");
                    }
                    if ((i3 & 12) == 0) {
                        r42.A08(A06);
                        r3.A00 = 0;
                        r3.A02 = null;
                        r3.A01 = null;
                        C37491vl.A03.C0w(r3);
                    }
                    return r1;
                }
            }
        }
        return null;
    }

    public void A01(C33781o8 r3) {
        C37491vl r1 = (C37491vl) this.A00.get(r3);
        if (r1 == null) {
            r1 = (C37491vl) C37491vl.A03.ALa();
            if (r1 == null) {
                r1 = new C37491vl();
            }
            this.A00.put(r3, r1);
        }
        r1.A00 |= 1;
    }

    public void A02(C33781o8 r3) {
        C37491vl r1 = (C37491vl) this.A00.get(r3);
        if (r1 != null) {
            r1.A00 &= -2;
        }
    }

    public void A03(C33781o8 r4) {
        C17610zB r2 = this.A01;
        int A012 = r2.A01() - 1;
        while (true) {
            if (A012 < 0) {
                break;
            } else if (r4 == r2.A06(A012)) {
                r2.A0A(A012);
                break;
            } else {
                A012--;
            }
        }
        C37491vl r1 = (C37491vl) this.A00.remove(r4);
        if (r1 != null) {
            r1.A00 = 0;
            r1.A02 = null;
            r1.A01 = null;
            C37491vl.A03.C0w(r1);
        }
    }

    public void A04(C33781o8 r3, C60862xx r4) {
        C37491vl r1 = (C37491vl) this.A00.get(r3);
        if (r1 == null) {
            r1 = (C37491vl) C37491vl.A03.ALa();
            if (r1 == null) {
                r1 = new C37491vl();
            }
            this.A00.put(r3, r1);
        }
        r1.A02 = r4;
        r1.A00 |= 4;
    }

    public void A05(C33781o8 r3, C60862xx r4) {
        C37491vl r1 = (C37491vl) this.A00.get(r3);
        if (r1 == null) {
            r1 = (C37491vl) C37491vl.A03.ALa();
            if (r1 == null) {
                r1 = new C37491vl();
            }
            this.A00.put(r3, r1);
        }
        r1.A01 = r4;
        r1.A00 |= 8;
    }
}
