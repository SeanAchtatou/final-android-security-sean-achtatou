package X;

import com.facebook.common.dextricks.DexStore;
import io.card.payment.BuildConfig;
import java.util.ArrayList;

/* renamed from: X.1eY  reason: invalid class name and case insensitive filesystem */
public final class C28281eY {
    public static final char[] NO_CHARS = new char[0];
    private final AnonymousClass11L _allocator;
    public char[] _currentSegment;
    public int _currentSize;
    public boolean _hasSegments = false;
    public char[] _inputBuffer;
    public int _inputLen;
    public int _inputStart;
    public char[] _resultArray;
    public String _resultString;
    public int _segmentSize;
    public ArrayList _segments;

    public static void clearSegments(C28281eY r2) {
        r2._hasSegments = false;
        r2._segments.clear();
        r2._segmentSize = 0;
        r2._currentSize = 0;
    }

    private void resetWithEmpty() {
        this._inputStart = -1;
        this._currentSize = 0;
        this._inputLen = 0;
        this._inputBuffer = null;
        this._resultString = null;
        this._resultArray = null;
        if (this._hasSegments) {
            clearSegments(this);
        }
    }

    public char[] emptyAndGetCurrentSegment() {
        this._inputStart = -1;
        this._currentSize = 0;
        this._inputLen = 0;
        this._inputBuffer = null;
        this._resultString = null;
        this._resultArray = null;
        if (this._hasSegments) {
            clearSegments(this);
        }
        char[] cArr = this._currentSegment;
        if (cArr != null) {
            return cArr;
        }
        char[] findBuffer = findBuffer(this, 0);
        this._currentSegment = findBuffer;
        return findBuffer;
    }

    public void resetWithShared(char[] cArr, int i, int i2) {
        this._resultString = null;
        this._resultArray = null;
        this._inputBuffer = cArr;
        this._inputStart = i;
        this._inputLen = i2;
        if (this._hasSegments) {
            clearSegments(this);
        }
    }

    public static void expand(C28281eY r2, int i) {
        if (r2._segments == null) {
            r2._segments = new ArrayList();
        }
        char[] cArr = r2._currentSegment;
        r2._hasSegments = true;
        r2._segments.add(cArr);
        int i2 = r2._segmentSize;
        int length = cArr.length;
        r2._segmentSize = i2 + length;
        int i3 = length >> 1;
        if (i3 >= i) {
            i = i3;
        }
        r2._currentSize = 0;
        r2._currentSegment = new char[Math.min((int) DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED, length + i)];
    }

    public static char[] findBuffer(C28281eY r1, int i) {
        AnonymousClass11L r12 = r1._allocator;
        if (r12 != null) {
            return r12.allocCharBuffer(AnonymousClass11P.TEXT_BUFFER, i);
        }
        return new char[Math.max(i, (int) AnonymousClass1Y3.A87)];
    }

    public static void unshare(C28281eY r5, int i) {
        int i2 = r5._inputLen;
        r5._inputLen = 0;
        char[] cArr = r5._inputBuffer;
        r5._inputBuffer = null;
        int i3 = r5._inputStart;
        r5._inputStart = -1;
        int i4 = i + i2;
        char[] cArr2 = r5._currentSegment;
        if (cArr2 == null || i4 > cArr2.length) {
            r5._currentSegment = findBuffer(r5, i4);
        }
        if (i2 > 0) {
            System.arraycopy(cArr, i3, r5._currentSegment, 0, i2);
        }
        r5._segmentSize = 0;
        r5._currentSize = i2;
    }

    public char[] contentsAsArray() {
        int i;
        char[] cArr = this._resultArray;
        if (cArr == null) {
            String str = this._resultString;
            if (str != null) {
                cArr = str.toCharArray();
            } else {
                int i2 = this._inputStart;
                if (i2 >= 0) {
                    int i3 = this._inputLen;
                    if (i3 >= 1) {
                        cArr = new char[i3];
                        System.arraycopy(this._inputBuffer, i2, cArr, 0, i3);
                    }
                } else {
                    int size = size();
                    if (size >= 1) {
                        cArr = new char[size];
                        ArrayList arrayList = this._segments;
                        if (arrayList != null) {
                            int size2 = arrayList.size();
                            i = 0;
                            for (int i4 = 0; i4 < size2; i4++) {
                                char[] cArr2 = (char[]) this._segments.get(i4);
                                int length = cArr2.length;
                                System.arraycopy(cArr2, 0, cArr, i, length);
                                i += length;
                            }
                        } else {
                            i = 0;
                        }
                        System.arraycopy(this._currentSegment, 0, cArr, i, this._currentSize);
                    }
                }
                cArr = NO_CHARS;
            }
            this._resultArray = cArr;
        }
        return cArr;
    }

    public String contentsAsString() {
        if (this._resultString == null) {
            char[] cArr = this._resultArray;
            if (cArr != null) {
                this._resultString = new String(cArr);
            } else {
                int i = this._inputStart;
                String str = BuildConfig.FLAVOR;
                if (i >= 0) {
                    int i2 = this._inputLen;
                    if (i2 < 1) {
                        this._resultString = str;
                        return str;
                    }
                    this._resultString = new String(this._inputBuffer, i, i2);
                } else {
                    int i3 = this._segmentSize;
                    int i4 = this._currentSize;
                    if (i3 == 0) {
                        if (i4 != 0) {
                            str = new String(this._currentSegment, 0, i4);
                        }
                        this._resultString = str;
                    } else {
                        StringBuilder sb = new StringBuilder(i3 + i4);
                        ArrayList arrayList = this._segments;
                        if (arrayList != null) {
                            int size = arrayList.size();
                            for (int i5 = 0; i5 < size; i5++) {
                                char[] cArr2 = (char[]) this._segments.get(i5);
                                sb.append(cArr2, 0, cArr2.length);
                            }
                        }
                        sb.append(this._currentSegment, 0, this._currentSize);
                        this._resultString = sb.toString();
                    }
                }
            }
        }
        return this._resultString;
    }

    public char[] expandCurrentSegment() {
        int min;
        char[] cArr = this._currentSegment;
        int length = cArr.length;
        if (length == 262144) {
            min = 262145;
        } else {
            min = Math.min((int) DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED, (length >> 1) + length);
        }
        char[] cArr2 = new char[min];
        this._currentSegment = cArr2;
        System.arraycopy(cArr, 0, cArr2, 0, length);
        return this._currentSegment;
    }

    public char[] finishCurrentSegment() {
        if (this._segments == null) {
            this._segments = new ArrayList();
        }
        this._hasSegments = true;
        this._segments.add(this._currentSegment);
        int length = this._currentSegment.length;
        this._segmentSize += length;
        char[] cArr = new char[Math.min(length + (length >> 1), (int) DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED)];
        this._currentSize = 0;
        this._currentSegment = cArr;
        return cArr;
    }

    public char[] getCurrentSegment() {
        if (this._inputStart >= 0) {
            unshare(this, 1);
        } else {
            char[] cArr = this._currentSegment;
            if (cArr == null) {
                this._currentSegment = findBuffer(this, 0);
            } else if (this._currentSize >= cArr.length) {
                expand(this, 1);
            }
        }
        return this._currentSegment;
    }

    public char[] getTextBuffer() {
        if (this._inputStart >= 0) {
            return this._inputBuffer;
        }
        char[] cArr = this._resultArray;
        if (cArr != null) {
            return cArr;
        }
        String str = this._resultString;
        if (str != null) {
            char[] charArray = str.toCharArray();
            this._resultArray = charArray;
            return charArray;
        } else if (!this._hasSegments) {
            return this._currentSegment;
        } else {
            return contentsAsArray();
        }
    }

    public void releaseBuffers() {
        if (this._allocator == null) {
            resetWithEmpty();
        } else if (this._currentSegment != null) {
            resetWithEmpty();
            char[] cArr = this._currentSegment;
            this._currentSegment = null;
            this._allocator._charBuffers[AnonymousClass11P.TEXT_BUFFER.ordinal()] = cArr;
        }
    }

    public int size() {
        if (this._inputStart >= 0) {
            return this._inputLen;
        }
        char[] cArr = this._resultArray;
        if (cArr != null) {
            return cArr.length;
        }
        String str = this._resultString;
        if (str != null) {
            return str.length();
        }
        return this._segmentSize + this._currentSize;
    }

    public C28281eY(AnonymousClass11L r2) {
        this._allocator = r2;
    }

    public String toString() {
        return contentsAsString();
    }

    public void append(String str, int i, int i2) {
        if (this._inputStart >= 0) {
            unshare(this, i2);
        }
        this._resultString = null;
        this._resultArray = null;
        char[] cArr = this._currentSegment;
        int length = cArr.length;
        int i3 = this._currentSize;
        int i4 = length - i3;
        if (i4 >= i2) {
            str.getChars(i, i + i2, cArr, i3);
            this._currentSize += i2;
            return;
        }
        if (i4 > 0) {
            int i5 = i + i4;
            str.getChars(i, i5, cArr, i3);
            i2 -= i4;
            i = i5;
        }
        while (true) {
            expand(this, i2);
            char[] cArr2 = this._currentSegment;
            int min = Math.min(cArr2.length, i2);
            int i6 = i + min;
            str.getChars(i, i6, cArr2, 0);
            this._currentSize += min;
            i2 -= min;
            if (i2 > 0) {
                i = i6;
            } else {
                return;
            }
        }
    }

    public void append(char[] cArr, int i, int i2) {
        if (this._inputStart >= 0) {
            unshare(this, i2);
        }
        this._resultString = null;
        this._resultArray = null;
        char[] cArr2 = this._currentSegment;
        int length = cArr2.length;
        int i3 = this._currentSize;
        int i4 = length - i3;
        if (i4 >= i2) {
            System.arraycopy(cArr, i, cArr2, i3, i2);
            this._currentSize += i2;
            return;
        }
        if (i4 > 0) {
            System.arraycopy(cArr, i, cArr2, i3, i4);
            i += i4;
            i2 -= i4;
        }
        do {
            expand(this, i2);
            char[] cArr3 = this._currentSegment;
            int min = Math.min(cArr3.length, i2);
            System.arraycopy(cArr, i, cArr3, 0, min);
            this._currentSize += min;
            i += min;
            i2 -= min;
        } while (i2 > 0);
    }
}
