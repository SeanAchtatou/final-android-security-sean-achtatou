package X;

import androidx.fragment.app.Fragment;

/* renamed from: X.0w3  reason: invalid class name and case insensitive filesystem */
public final class C15830w3 {
    public static final C15830w3 A00() {
        return new C15830w3();
    }

    public static void A01(Fragment fragment) {
        if (fragment instanceof C16010wL) {
            C73493gD r1 = (C73493gD) AnonymousClass1XX.A03(AnonymousClass1Y3.Arm, ((C16010wL) fragment).A00);
            if (r1.A08()) {
                r1.A03(C66123Iw.A01);
            }
        }
    }
}
