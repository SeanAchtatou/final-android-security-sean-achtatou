package X;

/* renamed from: X.0bN  reason: invalid class name and case insensitive filesystem */
public final class C06360bN {
    public final long A00;
    public final long A01;
    public final long A02;
    public final long A03;

    public String toString() {
        return "{single=(" + this.A03 + "," + this.A01 + ")" + ", batch=(" + this.A02 + "," + this.A00 + ")}";
    }

    public C06360bN(long j, long j2, long j3, long j4) {
        this.A03 = j;
        this.A01 = j2;
        this.A02 = j3;
        this.A00 = j4;
    }
}
