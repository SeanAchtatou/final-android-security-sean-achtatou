package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.os.DeadObjectException;
import android.os.Handler;

/* renamed from: X.07y  reason: invalid class name and case insensitive filesystem */
public final class C009207y {
    public static final C009207y A01 = new C009207y(null);
    public final AnonymousClass09P A00;

    public ComponentName A00(Context context, Intent intent) {
        try {
            return context.startService(intent);
        } catch (SecurityException e) {
            C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to startService");
            AnonymousClass09P r1 = this.A00;
            if (r1 != null) {
                r1.softReport("RtiGracefulSystemMethodHelper", "startService SecurityException", e);
                return null;
            }
            return null;
        } catch (IllegalStateException e2) {
            e = e2;
            if (Build.VERSION.SDK_INT >= 26) {
                return null;
            }
            throw e;
        } catch (RuntimeException e3) {
            e = e3;
            if (e.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r12 = this.A00;
                if (r12 != null) {
                    r12.softReport("RtiGracefulSystemMethodHelper", "startService DeadObjectException", e);
                }
                return null;
            }
            throw e;
        }
    }

    public void A02(AlarmManager alarmManager, int i, long j, PendingIntent pendingIntent) {
        try {
            alarmManager.setExact(i, j, pendingIntent);
        } catch (SecurityException e) {
            C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to setExact");
            AnonymousClass09P r0 = this.A00;
            if (r0 != null) {
                r0.softReport("RtiGracefulSystemMethodHelper", e);
            }
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r1 = this.A00;
                if (r1 != null) {
                    r1.softReport("RtiGracefulSystemMethodHelper", "setExact DeadObjectException", e2);
                    return;
                }
                return;
            }
            throw e2;
        }
    }

    public void A03(AlarmManager alarmManager, int i, long j, PendingIntent pendingIntent) {
        try {
            alarmManager.setAndAllowWhileIdle(i, j, pendingIntent);
        } catch (SecurityException e) {
            C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to setAndAllowWhileIdle");
            AnonymousClass09P r0 = this.A00;
            if (r0 != null) {
                r0.softReport("RtiGracefulSystemMethodHelper", e);
            }
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r1 = this.A00;
                if (r1 != null) {
                    r1.softReport("RtiGracefulSystemMethodHelper", "setAndAllowWhileIdle DeadObjectException", e2);
                    return;
                }
                return;
            }
            throw e2;
        }
    }

    public void A04(AlarmManager alarmManager, int i, long j, PendingIntent pendingIntent) {
        try {
            alarmManager.setExactAndAllowWhileIdle(i, j, pendingIntent);
        } catch (SecurityException e) {
            C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to setExactAndAllowWhileIdle");
            AnonymousClass09P r0 = this.A00;
            if (r0 != null) {
                r0.softReport("RtiGracefulSystemMethodHelper", e);
            }
        } catch (NullPointerException e2) {
            AnonymousClass09P r1 = this.A00;
            if (r1 != null) {
                r1.softReport("RtiGracefulSystemMethodHelper", "setExactAndAllowWhileIdle NullPointerException", e2);
            }
        } catch (RuntimeException e3) {
            if (e3.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r12 = this.A00;
                if (r12 != null) {
                    r12.softReport("RtiGracefulSystemMethodHelper", "setExactAndAllowWhileIdle DeadObjectException", e3);
                    return;
                }
                return;
            }
            throw e3;
        }
    }

    public void A05(AlarmManager alarmManager, PendingIntent pendingIntent) {
        if (pendingIntent != null) {
            try {
                alarmManager.cancel(pendingIntent);
            } catch (SecurityException e) {
                AnonymousClass09P r1 = this.A00;
                if (r1 != null) {
                    r1.softReport("RtiGracefulSystemMethodHelper", "cancelAlarm", e);
                }
            } catch (RuntimeException e2) {
                if (e2.getCause() instanceof DeadObjectException) {
                    AnonymousClass09P r12 = this.A00;
                    if (r12 != null) {
                        r12.softReport("RtiGracefulSystemMethodHelper", "cancelAlarm DeadObjectException", e2);
                        return;
                    }
                    return;
                }
                throw e2;
            }
        }
    }

    public void A06(Context context, ComponentName componentName) {
        boolean z;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(componentName.getPackageName(), (int) AnonymousClass1Y3.A4J);
            int i = 0;
            while (packageInfo != null) {
                ServiceInfo[] serviceInfoArr = packageInfo.services;
                if (serviceInfoArr == null || i >= serviceInfoArr.length) {
                    break;
                }
                String className = componentName.getClassName();
                String str = packageInfo.services[i].name;
                if (className == null) {
                    z = false;
                    if (str == null) {
                        z = true;
                    }
                } else {
                    z = className.equals(str);
                }
                if (!z) {
                    i++;
                } else {
                    return;
                }
            }
            AnonymousClass09P r1 = this.A00;
            if (r1 != null) {
                r1.CGS("RtiGracefulSystemMethodHelper", "verifyServiceExistsInManifest service not found");
            }
        } catch (PackageManager.NameNotFoundException unused) {
        } catch (RuntimeException e) {
            if (e.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r12 = this.A00;
                if (r12 != null) {
                    r12.softReport("RtiGracefulSystemMethodHelper", "verifyServiceExistsInManifest DeadObjectException", e);
                    return;
                }
                return;
            }
            throw e;
        }
    }

    public boolean A07(Context context, BroadcastReceiver broadcastReceiver) {
        try {
            context.unregisterReceiver(broadcastReceiver);
            return true;
        } catch (IllegalArgumentException | SecurityException e) {
            C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to unregisterReceiver");
            AnonymousClass09P r1 = this.A00;
            if (r1 == null) {
                return false;
            }
            r1.softReport("RtiGracefulSystemMethodHelper", "unregisterReceiver", e);
            return false;
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r12 = this.A00;
                if (r12 == null) {
                    return false;
                }
                r12.softReport("RtiGracefulSystemMethodHelper", "unregisterReceiver DeadObjectException", e2);
                return false;
            }
            throw e2;
        }
    }

    public boolean A08(Context context, BroadcastReceiver broadcastReceiver, IntentFilter intentFilter, String str, Handler handler) {
        try {
            context.registerReceiver(broadcastReceiver, intentFilter, str, handler);
            return true;
        } catch (IllegalArgumentException | SecurityException e) {
            C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to registerReceiver");
            AnonymousClass09P r1 = this.A00;
            if (r1 == null) {
                return false;
            }
            r1.softReport("RtiGracefulSystemMethodHelper", "registerReceiver", e);
            return false;
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof DeadObjectException) {
                AnonymousClass09P r12 = this.A00;
                if (r12 == null) {
                    return false;
                }
                r12.softReport("RtiGracefulSystemMethodHelper", "registerReceiver DeadObjectException", e2);
                return false;
            }
            throw e2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A09(android.content.Context r6, android.content.Intent r7) {
        /*
            r5 = this;
            java.lang.String r4 = "RtiGracefulSystemMethodHelper"
            r3 = 0
            r6.sendBroadcast(r7)     // Catch:{ SecurityException -> 0x0007, RuntimeException -> 0x0017 }
            goto L_0x002b
        L_0x0007:
            r2 = move-exception
            java.lang.String r0 = "Failed to sendBroadcast"
            X.C010708t.A0R(r4, r2, r0)
            X.09P r1 = r5.A00
            if (r1 == 0) goto L_0x0029
            java.lang.String r0 = "sendBroadcast"
            r1.softReport(r4, r0, r2)
            return r3
        L_0x0017:
            r2 = move-exception
            java.lang.Throwable r0 = r2.getCause()
            boolean r0 = r0 instanceof android.os.DeadObjectException
            if (r0 == 0) goto L_0x002a
            X.09P r1 = r5.A00
            if (r1 == 0) goto L_0x0029
            java.lang.String r0 = "sendBroadcast DeadObjectException"
            r1.softReport(r4, r0, r2)
        L_0x0029:
            return r3
        L_0x002a:
            throw r2
        L_0x002b:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C009207y.A09(android.content.Context, android.content.Intent):boolean");
    }

    public C009207y(AnonymousClass09P r1) {
        this.A00 = r1;
    }

    public Object A01(Context context, String str, Class cls) {
        try {
            Object systemService = context.getSystemService(str);
            if (systemService == null || !cls.isInstance(systemService)) {
                return null;
            }
            return systemService;
        } catch (Exception e) {
            AnonymousClass09P r2 = this.A00;
            if (r2 == null) {
                return null;
            }
            r2.softReport("RtiGracefulSystemMethodHelper", AnonymousClass08S.A0J("exception in getting system service ", cls.getName()), e);
            return null;
        }
    }
}
