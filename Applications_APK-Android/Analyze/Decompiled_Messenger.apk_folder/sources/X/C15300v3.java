package X;

/* renamed from: X.0v3  reason: invalid class name and case insensitive filesystem */
public class C15300v3 {
    public int A00 = 0;
    public final int A01;
    private final C06380bP A02;
    private final boolean A03;

    public Object A00() {
        Object ALa;
        if (this.A03) {
            synchronized (this) {
                ALa = this.A02.ALa();
                this.A00 = Math.max(0, this.A00 - 1);
            }
            return ALa;
        }
        Object ALa2 = this.A02.ALa();
        this.A00 = Math.max(0, this.A00 - 1);
        return ALa2;
    }

    public void C0t(Object obj) {
        if (this.A03) {
            synchronized (this) {
                this.A02.C0w(obj);
                this.A00 = Math.min(this.A01, this.A00 + 1);
            }
            return;
        }
        this.A02.C0w(obj);
        this.A00 = Math.min(this.A01, this.A00 + 1);
    }

    public C15300v3(int i, boolean z) {
        C06380bP r0;
        this.A03 = z;
        this.A01 = i;
        if (z) {
            r0 = new AnonymousClass0ZG(i);
        } else {
            r0 = new AnonymousClass0ZH(i);
        }
        this.A02 = r0;
    }
}
