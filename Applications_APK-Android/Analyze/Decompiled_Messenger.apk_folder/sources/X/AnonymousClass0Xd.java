package X;

import com.facebook.auth.annotations.LoggedInUser;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Xd  reason: invalid class name */
public final class AnonymousClass0Xd extends C04980Xe {
    private static volatile AnonymousClass0Xd A00;

    private AnonymousClass0Xd(@LoggedInUser C04310Tq r2, AnonymousClass1YI r3, C05000Xg r4, C05030Xj r5) {
        super(r3, r4, AnonymousClass1Y3.A5a, r5);
    }

    public static final AnonymousClass0Xd A00(AnonymousClass1XY r7) {
        if (A00 == null) {
            synchronized (AnonymousClass0Xd.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A00 = new AnonymousClass0Xd(AnonymousClass0WY.A01(applicationInjector), AnonymousClass0WA.A00(applicationInjector), C04990Xf.A00(applicationInjector), C05020Xi.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
