package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;

/* renamed from: X.0oE  reason: invalid class name */
public final class AnonymousClass0oE {
    public final DeprecatedAnalyticsLogger A00;

    public static final AnonymousClass0oE A00(AnonymousClass1XY r1) {
        return new AnonymousClass0oE(r1);
    }

    public void A01(String str, String str2) {
        C11670nb r1 = new C11670nb("missing_sender_name");
        r1.A0D("path", str);
        r1.A0D("id", str2);
        this.A00.A09(r1);
    }

    public AnonymousClass0oE(AnonymousClass1XY r2) {
        this.A00 = C06920cI.A00(r2);
    }
}
