package X;

import com.facebook.fbtrace.FbTraceNode;
import com.google.common.base.Preconditions;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1pT  reason: invalid class name and case insensitive filesystem */
public final class C34371pT {
    private static volatile C34371pT A02;
    public final C34381pU A00;
    private final AnonymousClass09P A01;

    public static FbTraceNode A00(FbTraceNode fbTraceNode) {
        FbTraceNode fbTraceNode2 = FbTraceNode.A03;
        if (fbTraceNode == fbTraceNode2) {
            return fbTraceNode2;
        }
        return FbTraceNode.A00(fbTraceNode);
    }

    public static final C34371pT A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C34371pT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C34371pT(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        if (r6.A01.nextInt(r1) == 0) goto L_0x0054;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbtrace.FbTraceNode A02() {
        /*
            r7 = this;
            java.lang.String r1 = "sampling_rate"
            com.google.common.base.Preconditions.checkNotNull(r1)
            X.1pU r6 = r7.A00
            X.0Tq r0 = r6.A02
            java.lang.Object r0 = r0.get()
            com.facebook.common.util.TriState r0 = (com.facebook.common.util.TriState) r0
            r5 = 0
            boolean r0 = r0.asBoolean(r5)
            if (r0 != 0) goto L_0x0054
            boolean r0 = r1.equals(r1)
            if (r0 == 0) goto L_0x0033
            r3 = 564238443610869(0x2012c000002f5, double:2.78770831051064E-309)
        L_0x0021:
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0044
            r5 = 0
        L_0x0028:
            if (r5 == 0) goto L_0x0056
            java.lang.String r0 = X.AnonymousClass0HU.A01()
            com.facebook.fbtrace.FbTraceNode r0 = com.facebook.fbtrace.FbTraceNode.A01(r0)
            return r0
        L_0x0033:
            java.lang.String r0 = "voip_sampling_rate"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0041
            r3 = 564238443676406(0x2012c000102f6, double:2.787708310834433E-309)
            goto L_0x0021
        L_0x0041:
            r3 = 0
            goto L_0x0021
        L_0x0044:
            X.1Yd r0 = r6.A00
            int r1 = r0.AqL(r3, r5)
            if (r1 <= 0) goto L_0x0028
            java.util.Random r0 = r6.A01
            int r0 = r0.nextInt(r1)
            if (r0 != 0) goto L_0x0028
        L_0x0054:
            r5 = 1
            goto L_0x0028
        L_0x0056:
            com.facebook.fbtrace.FbTraceNode r0 = com.facebook.fbtrace.FbTraceNode.A03
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34371pT.A02():com.facebook.fbtrace.FbTraceNode");
    }

    private C34371pT(AnonymousClass1XY r2) {
        this.A01 = C04750Wa.A01(r2);
        this.A00 = new C34381pU(r2);
    }

    public FbTraceNode A03(String str) {
        FbTraceNode fbTraceNode;
        Preconditions.checkNotNull(str);
        if (str != null) {
            try {
                if (str.length() == 22) {
                    long[] jArr = {AnonymousClass0HU.A00(str.substring(0, 11)), AnonymousClass0HU.A00(str.substring(11, 22))};
                    fbTraceNode = new FbTraceNode(AnonymousClass0HU.A02(jArr[0]), AnonymousClass0HU.A02(jArr[1]), null);
                    if (fbTraceNode == FbTraceNode.A03) {
                        this.A01.CGS("invalid_fbtrace_metadata", AnonymousClass08S.A0J("invalide fbtrace metadata: ", str));
                    }
                    return fbTraceNode;
                }
            } catch (IllegalArgumentException | IndexOutOfBoundsException e) {
                C010708t.A0F(FbTraceNode.A04, e, "invalid FbTrace metadata: %s", str);
                fbTraceNode = FbTraceNode.A03;
            }
        }
        throw new IllegalArgumentException("Invalid Metadata");
    }
}
