package X;

import android.content.res.Resources;

/* renamed from: X.1ZM  reason: invalid class name */
public abstract class AnonymousClass1ZM implements C04770Wc {
    public volatile C25851aV A00;

    public abstract double A03(long j, double d, boolean z);

    public abstract long A04(long j, long j2, boolean z);

    public abstract Integer A05(long j);

    public abstract String A06(long j);

    public abstract String A07(long j, String str, boolean z);

    public abstract void A08(long j, AnonymousClass10F r3);

    public boolean A09() {
        return ((AnonymousClass1ZL) this).A03 != null;
    }

    public boolean A0A(long j) {
        AnonymousClass1ZL r1 = (AnonymousClass1ZL) this;
        if (!r1.A09()) {
            return false;
        }
        boolean z = false;
        if ((AnonymousClass1ZL.A00(r1, j) & 16) != 0) {
            z = true;
        }
        return !z;
    }

    public boolean A0B(long j) {
        AnonymousClass1ZL r1 = (AnonymousClass1ZL) this;
        if (!r1.A09()) {
            return false;
        }
        boolean z = true;
        if ((AnonymousClass1ZL.A00(r1, j) & 1) == 0) {
            z = false;
        }
        return !z;
    }

    public abstract boolean A0C(long j, boolean z, boolean z2);

    public double Akm(long j, AnonymousClass0XE r9) {
        return Akl(j, C05480Zc.A00(j), r9);
    }

    public int AqL(long j, int i) {
        long At3 = At3(j, (long) i, AnonymousClass0XE.A06);
        int i2 = (int) At3;
        if (((long) i2) == At3) {
            return i2;
        }
        return i;
    }

    public int AqO(long j, int i) {
        long At3 = At3(j, (long) i, AnonymousClass0XE.A07);
        int i2 = (int) At3;
        if (((long) i2) == At3) {
            return i2;
        }
        return i;
    }

    public long At4(long j, AnonymousClass0XE r9) {
        return At3(j, C05480Zc.A01(j), r9);
    }

    public boolean Aem(long j) {
        return Aer(j, AnonymousClass0XE.A06);
    }

    public boolean Aeo(long j, boolean z) {
        return Aes(j, z, AnonymousClass0XE.A06);
    }

    public boolean Aes(long j, boolean z, AnonymousClass0XE r6) {
        if (this.A00 == null || !this.A00.hasBoolOverrideForParam(j)) {
            if (r6.A02) {
                AnonymousClass0XF A02 = A02(j);
                AnonymousClass0XE.A01(r6).A00 = A02;
                if (!(A02 == AnonymousClass0XF.A07 || A02 == AnonymousClass0XF.A05)) {
                    return z;
                }
            }
            return A0C(j, z, r6.A03);
        }
        if (r6.A02) {
            AnonymousClass0XE.A01(r6).A00 = AnonymousClass0XF.A06;
        }
        return this.A00.boolOverrideForParam(j, z);
    }

    public double Aki(long j) {
        return Akm(j, AnonymousClass0XE.A06);
    }

    public double Akj(long j, double d) {
        return Akl(j, d, AnonymousClass0XE.A06);
    }

    public double Akl(long j, double d, AnonymousClass0XE r12) {
        long j2 = j;
        double d2 = d;
        if (this.A00 == null || !this.A00.hasDoubleOverrideForParam(j)) {
            if (r12.A02) {
                AnonymousClass0XF A02 = A02(j);
                AnonymousClass0XE.A01(r12).A00 = A02;
                if (!(A02 == AnonymousClass0XF.A07 || A02 == AnonymousClass0XF.A05)) {
                    return d;
                }
            }
            return A03(j2, d2, r12.A03);
        }
        if (r12.A02) {
            AnonymousClass0XE.A01(r12).A00 = AnonymousClass0XF.A06;
        }
        return this.A00.doubleOverrideForParam(j, d);
    }

    public long At0(long j) {
        return At4(j, AnonymousClass0XE.A06);
    }

    public long At1(long j, long j2) {
        return At3(j, j2, AnonymousClass0XE.A06);
    }

    public long At3(long j, long j2, AnonymousClass0XE r12) {
        long j3 = j;
        long j4 = j2;
        if (this.A00 == null || !this.A00.hasIntOverrideForParam(j)) {
            if (r12.A02) {
                AnonymousClass0XF A02 = A02(j);
                AnonymousClass0XE.A01(r12).A00 = A02;
                if (!(A02 == AnonymousClass0XF.A07 || A02 == AnonymousClass0XF.A05)) {
                    return j2;
                }
            }
            return A04(j3, j4, r12.A03);
        }
        if (r12.A02) {
            AnonymousClass0XE.A01(r12).A00 = AnonymousClass0XF.A06;
        }
        return this.A00.intOverrideForParam(j, j2);
    }

    public String B4C(long j) {
        return B4J(j, AnonymousClass0XE.A06);
    }

    public String B4D(long j, int i, Resources resources) {
        String B4K = B4K(j, null, AnonymousClass0XE.A06);
        if (B4K == null) {
            return resources.getString(i);
        }
        return B4K;
    }

    public String B4E(long j, String str) {
        return B4K(j, str, AnonymousClass0XE.A06);
    }

    public String B4K(long j, String str, AnonymousClass0XE r6) {
        if (this.A00 == null || !this.A00.hasStringOverrideForParam(j)) {
            if (r6.A02) {
                AnonymousClass0XF A02 = A02(j);
                AnonymousClass0XE.A01(r6).A00 = A02;
                if (!(A02 == AnonymousClass0XF.A07 || A02 == AnonymousClass0XF.A05)) {
                    return str;
                }
            }
            return A07(j, str, r6.A03);
        }
        if (r6.A02) {
            AnonymousClass0XE.A01(r6).A00 = AnonymousClass0XF.A06;
        }
        String stringOverrideForParam = this.A00.stringOverrideForParam(j, str);
        if ("__fbt_null__".equals(stringOverrideForParam)) {
            return str;
        }
        return stringOverrideForParam;
    }

    public void BJC(long j) {
        A08(j, AnonymousClass10F.MANUAL_EXPOSURE);
    }

    public AnonymousClass1ZM(C25851aV r1) {
        this.A00 = r1;
    }

    private AnonymousClass0XF A02(long j) {
        if (!A09()) {
            return AnonymousClass0XF.A04;
        }
        if (A0B(j)) {
            return AnonymousClass0XF.A07;
        }
        if (A0A(j)) {
            return AnonymousClass0XF.A05;
        }
        return AnonymousClass0XF.A03;
    }

    public boolean Aer(long j, AnonymousClass0XE r4) {
        return Aes(j, AnonymousClass0XG.A03(j), r4);
    }

    public String B4J(long j, AnonymousClass0XE r4) {
        return B4K(j, C05480Zc.A02(j), r4);
    }
}
