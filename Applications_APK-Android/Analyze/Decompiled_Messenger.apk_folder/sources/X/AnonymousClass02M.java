package X;

import com.facebook.acra.anr.ANRDetectorConfig;
import com.facebook.acra.anr.IANRDetector;
import com.facebook.acra.anr.multisignal.MultiSignalANRDetector;

/* renamed from: X.02M  reason: invalid class name */
public final class AnonymousClass02M extends AnonymousClass02O {
    public IANRDetector createANRDetector(int i, ANRDetectorConfig aNRDetectorConfig, int i2) {
        if (i == 6) {
            return MultiSignalANRDetector.getInstance(aNRDetectorConfig);
        }
        return super.createANRDetector(i, aNRDetectorConfig, i2);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass02M(int r25, java.lang.String r26, android.app.Application r27) {
        /*
            r24 = this;
            java.lang.String r0 = "acraconfig_use_multipart_enabled"
            r8 = r27
            boolean r12 = X.AnonymousClass08X.A07(r8, r0)
            java.lang.String r0 = "acraconfig_logcat_native_crash_enabled_enabled"
            boolean r13 = X.AnonymousClass08X.A07(r8, r0)
            java.lang.String r0 = "acraconfig_use_zstd_enabled"
            boolean r14 = X.AnonymousClass08X.A07(r8, r0)
            java.lang.String r0 = "acraconfig_zero_crashlog_blocked"
            boolean r15 = X.AnonymousClass08X.A07(r8, r0)
            com.facebook.acra.config.StartupBlockingConfig r1 = new com.facebook.acra.config.StartupBlockingConfig
            boolean r0 = X.C010408q.A00(r8)
            r3 = 10
            if (r0 == 0) goto L_0x0026
            r3 = r25
        L_0x0026:
            r4 = 10000(0x2710, double:4.9407E-320)
            r6 = 2131824308(0x7f110eb4, float:1.928144E38)
            r7 = 2131824307(0x7f110eb3, float:1.9281438E38)
            r2 = 1
            r1.<init>(r2, r3, r4, r6, r7)
            java.lang.String r17 = X.AnonymousClass01P.A05()
            java.lang.String r0 = "acraconfig_use_upload_service"
            boolean r22 = X.AnonymousClass08X.A07(r8, r0)
            r7 = r24
            r10 = 0
            r11 = 1
            r18 = 200(0xc8, float:2.8E-43)
            r19 = 0
            r20 = 0
            r21 = 0
            r23 = 0
            r9 = r26
            r16 = r1
            r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02M.<init>(int, java.lang.String, android.app.Application):void");
    }
}
