package X;

/* renamed from: X.0Rv  reason: invalid class name */
public final class AnonymousClass0Rv {
    public final int A00;
    public final int A01;
    public final int A02;
    public final boolean A03;
    public final boolean A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;

    public AnonymousClass0Rv(int i, boolean z, boolean z2, boolean z3, int i2) {
        this.A01 = i;
        this.A05 = z;
        this.A04 = z2;
        this.A06 = false;
        this.A07 = false;
        this.A02 = 0;
        this.A03 = z3;
        this.A00 = i2;
    }

    public AnonymousClass0Rv(int i, boolean z, boolean z2, boolean z3, boolean z4, int i2, boolean z5, int i3) {
        this.A01 = i;
        this.A05 = z;
        this.A04 = z2;
        this.A06 = z3;
        this.A07 = z4;
        this.A02 = i2;
        this.A03 = z5;
        this.A00 = i3;
    }
}
