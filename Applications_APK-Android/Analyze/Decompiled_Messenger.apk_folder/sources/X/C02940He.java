package X;

import android.os.Process;
import android.util.Log;
import com.facebook.profilo.provider.stacktrace.CPUProfiler;

/* renamed from: X.0He  reason: invalid class name and case insensitive filesystem */
public final class C02940He implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.profilo.provider.stacktrace.StackFrameThread$1";

    public void run() {
        Process.setThreadPriority(0);
        try {
            if (CPUProfiler.sInitialized) {
                CPUProfiler.nativeLoggerLoop();
            }
        } catch (Exception e) {
            Log.e("StackFrameThread", e.getMessage(), e);
        }
    }
}
