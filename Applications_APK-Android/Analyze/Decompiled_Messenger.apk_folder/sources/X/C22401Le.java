package X;

import com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem;
import com.facebook.messaging.contacts.ranking.logging.ScoreLoggingItem;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/* renamed from: X.1Le  reason: invalid class name and case insensitive filesystem */
public final class C22401Le {
    public static void A00(RankingLoggingItem rankingLoggingItem, ImmutableMap.Builder builder) {
        String str = rankingLoggingItem.A03;
        if (!C06850cB.A0B(str)) {
            builder.put("md", str);
        }
        builder.put(C99084oO.$const$string(AnonymousClass1Y3.A3z), String.valueOf(rankingLoggingItem.A00));
        ImmutableList immutableList = rankingLoggingItem.A01;
        if (immutableList != null) {
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                ScoreLoggingItem scoreLoggingItem = (ScoreLoggingItem) it.next();
                String str2 = scoreLoggingItem.A02;
                builder.put(AnonymousClass08S.A0J(str2, "_score"), String.valueOf(scoreLoggingItem.A00));
                builder.put(AnonymousClass08S.A0J(str2, "_index"), String.valueOf(scoreLoggingItem.A01));
            }
        }
    }
}
