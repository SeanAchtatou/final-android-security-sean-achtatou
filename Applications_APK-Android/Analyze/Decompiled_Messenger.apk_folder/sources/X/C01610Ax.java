package X;

import android.content.Context;

/* renamed from: X.0Ax  reason: invalid class name and case insensitive filesystem */
public final class C01610Ax extends C01620Ay {
    public String A00() {
        return "ZeroRatingConnectionConfigOverrides";
    }

    public String A01() {
        return "com.facebook.rti.mqtt.ACTION_ZR_SWITCH";
    }

    public void A04(String str, String str2) {
        AnonymousClass0DD AY8 = this.A05.AbP(AnonymousClass07B.A07).AY8();
        AY8.BzD("zero_rating_last_host", str);
        AY8.BzB("zero_rating_last_host_timestamp", System.currentTimeMillis());
        AY8.commit();
    }

    public C01610Ax(Context context, C008007h r8, Integer num, AnonymousClass09P r10, AnonymousClass0AW r11) {
        super(context, r8, num, r10, r11);
        AnonymousClass0B0 AbP = this.A05.AbP(AnonymousClass07B.A07);
        if (System.currentTimeMillis() - AbP.getLong("zero_rating_last_host_timestamp", 0) < 86400000) {
            this.A07 = AbP.getString("zero_rating_last_host", null);
        }
    }
}
