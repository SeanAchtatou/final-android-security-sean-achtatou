package X;

import android.os.Build;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/* renamed from: X.0Jk  reason: invalid class name */
public final class AnonymousClass0Jk extends IOException {
    private int mChildExceptions;
    private final ArrayList mSub19Causes;

    public void A00(IOException iOException) {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 19) {
            z = true;
        }
        if (!z) {
            if (getCause() == null) {
                initCause(iOException);
            } else {
                this.mSub19Causes.add(iOException);
            }
        }
        this.mChildExceptions++;
    }

    public AnonymousClass0Jk(String str) {
        super(str);
        if (Build.VERSION.SDK_INT >= 19) {
            this.mSub19Causes = null;
        } else {
            this.mSub19Causes = new ArrayList();
        }
    }

    public String toString() {
        String message = getMessage();
        if (message == null) {
            message = "NO MESSAGE";
        }
        return String.format("AggregateIOException (%d): %s", Integer.valueOf(this.mChildExceptions), message);
    }

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 19) {
            z = true;
        }
        if (!z) {
            int i = 0;
            while (i < this.mSub19Causes.size()) {
                int i2 = i + 1;
                printStream.println(AnonymousClass08S.A09("Exception ", i2));
                ((IOException) this.mSub19Causes.get(i)).printStackTrace(printStream);
                i = i2;
            }
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 19) {
            z = true;
        }
        if (!z) {
            int i = 0;
            while (i < this.mSub19Causes.size()) {
                int i2 = i + 1;
                printWriter.println(AnonymousClass08S.A09("Exception ", i2));
                ((IOException) this.mSub19Causes.get(i)).printStackTrace(printWriter);
                i = i2;
            }
        }
    }
}
