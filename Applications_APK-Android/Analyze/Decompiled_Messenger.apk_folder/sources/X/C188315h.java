package X;

import android.os.Looper;

/* renamed from: X.15h  reason: invalid class name and case insensitive filesystem */
public abstract class C188315h {
    public void A01(Runnable runnable) {
        ((C31671k4) this).A00.A01(runnable);
    }

    public boolean A02() {
        return !(this instanceof C31681k5) ? ((C31671k4) this).A00.A02() : Looper.getMainLooper().getThread() == Thread.currentThread();
    }
}
