package X;

import com.facebook.auth.annotations.LoggedInUser;

/* renamed from: X.0qw  reason: invalid class name and case insensitive filesystem */
public final class C13230qw {
    @LoggedInUser
    public final C04310Tq A00;

    public static final C13230qw A00(AnonymousClass1XY r1) {
        return new C13230qw(r1);
    }

    private C13230qw(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WY.A01(r2);
    }
}
