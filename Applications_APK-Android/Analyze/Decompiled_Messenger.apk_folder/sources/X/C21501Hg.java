package X;

/* renamed from: X.1Hg  reason: invalid class name and case insensitive filesystem */
public final class C21501Hg {
    public static final AnonymousClass1Y8 A00;
    public static final AnonymousClass1Y8 A01 = A00.A09("generic_read_only_block");
    public static final AnonymousClass1Y8 A02;

    static {
        AnonymousClass1Y8 A0D = C05690aA.A1i.A09("featurelimits/");
        A00 = A0D;
        A02 = A0D.A09("read_only");
    }
}
