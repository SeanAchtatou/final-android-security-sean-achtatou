package X;

/* renamed from: X.134  reason: invalid class name */
public final class AnonymousClass134 {
    public static String okNameForIsGetter(C29141fw r3, String str) {
        Class<Boolean> rawType;
        if (!str.startsWith("is") || ((rawType = r3.getRawType()) != Boolean.class && rawType != Boolean.TYPE)) {
            return null;
        }
        return manglePropertyName(str.substring(2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
        if (r1.startsWith(r0) != false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        if (r0 != false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String okNameForRegularGetter(X.C29141fw r3, java.lang.String r4) {
        /*
            java.lang.String r0 = "get"
            boolean r0 = r4.startsWith(r0)
            r2 = 0
            if (r0 == 0) goto L_0x003e
            java.lang.String r0 = "getCallbacks"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x003f
            java.lang.Class r1 = r3.getRawType()
            if (r1 == 0) goto L_0x0060
            boolean r0 = r1.isArray()
            if (r0 == 0) goto L_0x0060
            java.lang.Class r0 = r1.getComponentType()
            java.lang.Package r0 = r0.getPackage()
            if (r0 == 0) goto L_0x0060
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = "net.sf.cglib"
            boolean r0 = r1.startsWith(r0)
            if (r0 != 0) goto L_0x003b
            java.lang.String r0 = "org.hibernate.repackage.cglib"
        L_0x0035:
            boolean r0 = r1.startsWith(r0)
            if (r0 == 0) goto L_0x0060
        L_0x003b:
            r0 = 1
        L_0x003c:
            if (r0 == 0) goto L_0x0062
        L_0x003e:
            return r2
        L_0x003f:
            java.lang.String r0 = "getMetaClass"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x0062
            java.lang.Class r1 = r3.getRawType()
            if (r1 == 0) goto L_0x0060
            boolean r0 = r1.isArray()
            if (r0 != 0) goto L_0x0060
            java.lang.Package r0 = r1.getPackage()
            if (r0 == 0) goto L_0x0060
            java.lang.String r1 = r0.getName()
            java.lang.String r0 = "groovy.lang"
            goto L_0x0035
        L_0x0060:
            r0 = 0
            goto L_0x003c
        L_0x0062:
            r0 = 3
            java.lang.String r0 = r4.substring(r0)
            java.lang.String r0 = manglePropertyName(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass134.okNameForRegularGetter(X.1fw, java.lang.String):java.lang.String");
    }

    public static String manglePropertyName(String str) {
        int length = str.length();
        StringBuilder sb = null;
        if (length == 0) {
            return null;
        }
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            char lowerCase = Character.toLowerCase(charAt);
            if (charAt == lowerCase) {
                break;
            }
            if (sb == null) {
                sb = new StringBuilder(str);
            }
            sb.setCharAt(i, lowerCase);
        }
        if (sb != null) {
            return sb.toString();
        }
        return str;
    }
}
