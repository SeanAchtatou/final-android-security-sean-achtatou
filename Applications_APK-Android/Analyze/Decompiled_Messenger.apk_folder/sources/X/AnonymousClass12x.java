package X;

import android.view.ViewGroup;
import com.facebook.litho.LithoView;

/* renamed from: X.12x  reason: invalid class name */
public final class AnonymousClass12x {
    public final int A00;
    public final ViewGroup A01;
    public final LithoView A02;
    public final LithoView A03;

    public AnonymousClass12x(ViewGroup viewGroup, LithoView lithoView, int i, LithoView lithoView2) {
        this.A01 = viewGroup;
        this.A03 = lithoView;
        this.A00 = i;
        this.A02 = lithoView2;
    }
}
