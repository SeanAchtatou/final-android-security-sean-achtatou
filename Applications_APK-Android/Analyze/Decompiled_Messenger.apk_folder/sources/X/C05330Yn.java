package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Yn  reason: invalid class name and case insensitive filesystem */
public final class C05330Yn extends C09260gp {
    private static volatile C05330Yn A00;

    public static final C05330Yn A00(AnonymousClass1XY r9) {
        if (A00 == null) {
            synchronized (C05330Yn.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r9);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r9.getApplicationInjector();
                        A00 = new C05330Yn(AnonymousClass1YA.A00(applicationInjector), C04720Vx.A01(applicationInjector), C09280gs.A00(applicationInjector), C190216m.A00(applicationInjector), AnonymousClass17L.A00(applicationInjector), AnonymousClass17M.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C05330Yn(android.content.Context r7, X.C04740Vz r8, X.C09280gs r9, X.C190216m r10, X.AnonymousClass17L r11, X.AnonymousClass17M r12) {
        /*
            r6 = this;
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r10, r11, r12)
            java.lang.String r5 = "contacts_db2"
            r0 = r6
            r2 = r8
            r3 = r9
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05330Yn.<init>(android.content.Context, X.0Vz, X.0gs, X.16m, X.17L, X.17M):void");
    }

    public void A07() {
        super.A09();
    }
}
