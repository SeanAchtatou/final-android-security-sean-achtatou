package X;

/* renamed from: X.0Ar  reason: invalid class name and case insensitive filesystem */
public final class C01550Ar extends C01540Aq {
    private static final long serialVersionUID = 0;
    private final Object reference;

    public boolean A02() {
        return true;
    }

    public boolean equals(Object obj) {
        if (obj instanceof C01550Ar) {
            return this.reference.equals(((C01550Ar) obj).reference);
        }
        return false;
    }

    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    public String toString() {
        return "Optional.of(" + this.reference + ")";
    }

    public C01550Ar(Object obj) {
        this.reference = obj;
    }

    public Object A01() {
        return this.reference;
    }
}
