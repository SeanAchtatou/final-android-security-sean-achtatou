package X;

import android.view.View;

/* renamed from: X.0ze  reason: invalid class name and case insensitive filesystem */
public final class C17890ze implements View.OnLongClickListener {
    public AnonymousClass10N A00;

    public boolean onLongClick(View view) {
        AnonymousClass10N r2 = this.A00;
        if (r2 == null) {
            return false;
        }
        if (C33181nA.A04 == null) {
            C33181nA.A04 = new AnonymousClass5O1();
        }
        AnonymousClass5O1 r1 = C33181nA.A04;
        r1.A00 = view;
        Object AXa = r2.A00.Alq().AXa(r2, r1);
        C33181nA.A04.A00 = null;
        if (AXa == null || !((Boolean) AXa).booleanValue()) {
            return false;
        }
        return true;
    }
}
