package X;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.BackStackState;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManagerState;
import androidx.fragment.app.FragmentState;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0qW  reason: invalid class name and case insensitive filesystem */
public abstract class C13060qW {
    public boolean A00;
    public int A01 = -1;
    public C27641dW A02;
    public Fragment A03;
    public Fragment A04;
    public C27711dd A05;
    public C13020qR A06;
    public C27931dz A07;
    public Runnable A08 = new C27811dn(this);
    public ArrayList A09;
    public ArrayList A0A;
    public ArrayList A0B;
    public ConcurrentHashMap A0C = new ConcurrentHashMap();
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    private C27801dm A0I = new C27791dl(this);
    private ArrayList A0J;
    private ArrayList A0K;
    private ArrayList A0L;
    private ArrayList A0M;
    public final C27751dh A0N = new C27741dg(this, false);
    public final C27731df A0O = new C27731df(this);
    public final C27781dk A0P = new C27781dk(this);
    public final C27721de A0Q = new C27721de();
    public final ArrayList A0R = new ArrayList();
    public final AtomicInteger A0S = new AtomicInteger();
    private final C27771dj A0T = new C27761di(this);

    private void A02() {
        this.A00 = false;
        this.A0L.clear();
        this.A0M.clear();
    }

    /* JADX INFO: finally extract failed */
    public static void A0A(C13060qW r6, int i) {
        try {
            r6.A00 = true;
            C27721de r5 = r6.A0Q;
            Iterator it = r5.A00.iterator();
            while (it.hasNext()) {
                C012909q r0 = (C012909q) r5.A01.get(((Fragment) it.next()).A0V);
                if (r0 != null) {
                    r0.A00 = i;
                }
            }
            for (C012909q r02 : r5.A01.values()) {
                if (r02 != null) {
                    r02.A00 = i;
                }
            }
            r6.A0b(i, false);
            r6.A00 = false;
            r6.A12(true);
        } catch (Throwable th) {
            r6.A00 = false;
            throw th;
        }
    }

    public static boolean A0K(C13060qW r5, String str, int i, int i2) {
        C13060qW r3 = r5;
        r5.A12(false);
        r5.A0H(true);
        Fragment fragment = r5.A04;
        if (fragment != null && i < 0 && str == null && fragment.A17().A14()) {
            return true;
        }
        boolean A1A = r3.A1A(r5.A0M, r5.A0L, str, i, i2);
        if (A1A) {
            r3.A00 = true;
            try {
                r3.A0F(r3.A0M, r3.A0L);
            } finally {
                r3.A02();
            }
        }
        A09(r3);
        if (r3.A0E) {
            r3.A0E = false;
            r3.A05();
        }
        r3.A0Q.A01.values().removeAll(Collections.singleton(null));
        return A1A;
    }

    public void A0W() {
        this.A0D = true;
        A12(true);
        A03();
        A0A(this, -1);
        this.A06 = null;
        this.A05 = null;
        this.A03 = null;
        if (this.A02 != null) {
            Iterator it = this.A0N.A00.iterator();
            while (it.hasNext()) {
                ((C27901dw) it.next()).cancel();
            }
            this.A02 = null;
        }
    }

    public void A0Y() {
        A12(true);
        A04();
    }

    public void A0Z() {
        this.A0G = false;
        this.A0H = false;
        for (Fragment fragment : this.A0Q.A01()) {
            if (fragment != null) {
                fragment.A0O.A0Z();
            }
        }
    }

    public void A0h(Fragment fragment) {
        A0I(2);
        A0m(fragment);
        if (!fragment.A0Y) {
            this.A0Q.A04(fragment);
            fragment.A0f = false;
            if (fragment.A0I == null) {
                fragment.A0c = false;
            }
            if (A0J(fragment)) {
                this.A0F = true;
            }
        }
    }

    public void A0j(Fragment fragment) {
        A0I(2);
        if (fragment.A0Y) {
            fragment.A0Y = false;
            if (!fragment.A0W) {
                this.A0Q.A04(fragment);
                A0I(2);
                if (A0J(fragment)) {
                    this.A0F = true;
                }
            }
        }
    }

    public void A0k(Fragment fragment) {
        A0I(2);
        if (!fragment.A0Y) {
            fragment.A0Y = true;
            if (fragment.A0W) {
                A0I(2);
                this.A0Q.A03(fragment);
                if (A0J(fragment)) {
                    this.A0F = true;
                }
                A08(fragment);
            }
        }
    }

    public void A0l(Fragment fragment) {
        A0I(2);
        if (!fragment.A0b) {
            fragment.A0b = true;
            fragment.A0c = true ^ fragment.A0c;
            A08(fragment);
        }
    }

    public void A0p(Fragment fragment) {
        A0I(2);
        boolean z = false;
        if (fragment.A0C > 0) {
            z = true;
        }
        boolean z2 = !z;
        if (!fragment.A0Y || z2) {
            this.A0Q.A03(fragment);
            if (A0J(fragment)) {
                this.A0F = true;
            }
            fragment.A0f = true;
            A08(fragment);
        }
    }

    public boolean A14() {
        return A0K(this, null, -1, 0);
    }

    public boolean A19(Fragment fragment) {
        if (fragment != null) {
            C13060qW r1 = fragment.A0P;
            if (!fragment.equals(r1.A04) || !A19(r1.A03)) {
                return false;
            }
        }
        return true;
    }

    private ViewGroup A00(Fragment fragment) {
        if (fragment.A0D > 0 && this.A05.A01()) {
            View A002 = this.A05.A00(fragment.A0D);
            if (A002 instanceof ViewGroup) {
                return (ViewGroup) A002;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0069, code lost:
        if (r2 != null) goto L_0x006b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass3AU A01(android.content.Context r7, X.C27711dd r8, androidx.fragment.app.Fragment r9, boolean r10) {
        /*
            X.2NQ r1 = r9.A0K
            if (r1 != 0) goto L_0x00ab
            r0 = 0
        L_0x0005:
            if (r1 != 0) goto L_0x00a7
            r1 = 0
        L_0x0008:
            r6 = 0
            r9.A1J(r6)
            int r2 = r9.A0D
            android.view.View r3 = r8.A00(r2)
            r5 = 0
            if (r3 == 0) goto L_0x0024
            r2 = 2131301370(0x7f0913fa, float:1.8220796E38)
            java.lang.Object r2 = r3.getTag(r2)
            if (r2 == 0) goto L_0x0024
            r2 = 2131301370(0x7f0913fa, float:1.8220796E38)
            r3.setTag(r2, r5)
        L_0x0024:
            android.view.ViewGroup r2 = r9.A0J
            if (r2 == 0) goto L_0x002f
            android.animation.LayoutTransition r2 = r2.getLayoutTransition()
            if (r2 == 0) goto L_0x002f
        L_0x002e:
            return r5
        L_0x002f:
            android.view.animation.Animation r2 = r9.A1i(r0, r10, r1)
            if (r2 != 0) goto L_0x006b
            if (r1 == 0) goto L_0x0071
            android.content.res.Resources r2 = r7.getResources()
            java.lang.String r3 = r2.getResourceTypeName(r1)
            java.lang.String r2 = "anim"
            boolean r4 = r2.equals(r3)
            if (r4 == 0) goto L_0x0054
            android.view.animation.Animation r3 = android.view.animation.AnimationUtils.loadAnimation(r7, r1)     // Catch:{ NotFoundException -> 0x00b0, RuntimeException -> 0x0054 }
            if (r3 == 0) goto L_0x0053
            X.3AU r2 = new X.3AU     // Catch:{ NotFoundException -> 0x00b0, RuntimeException -> 0x0054 }
            r2.<init>(r3)     // Catch:{ NotFoundException -> 0x00b0, RuntimeException -> 0x0054 }
            goto L_0x00af
        L_0x0053:
            r6 = 1
        L_0x0054:
            if (r6 != 0) goto L_0x0071
            android.animation.Animator r3 = android.animation.AnimatorInflater.loadAnimator(r7, r1)     // Catch:{ RuntimeException -> 0x0062 }
            if (r3 == 0) goto L_0x0071
            X.3AU r2 = new X.3AU     // Catch:{ RuntimeException -> 0x0062 }
            r2.<init>(r3)     // Catch:{ RuntimeException -> 0x0062 }
            goto L_0x00b2
        L_0x0062:
            r2 = move-exception
            if (r4 != 0) goto L_0x00b3
            android.view.animation.Animation r2 = android.view.animation.AnimationUtils.loadAnimation(r7, r1)
            if (r2 == 0) goto L_0x0071
        L_0x006b:
            X.3AU r0 = new X.3AU
            r0.<init>(r2)
            return r0
        L_0x0071:
            if (r0 == 0) goto L_0x002e
            r1 = 4097(0x1001, float:5.741E-42)
            if (r0 == r1) goto L_0x009e
            r1 = 4099(0x1003, float:5.744E-42)
            if (r0 == r1) goto L_0x0095
            r1 = 8194(0x2002, float:1.1482E-41)
            if (r0 == r1) goto L_0x008c
            r0 = -1
        L_0x0080:
            if (r0 < 0) goto L_0x002e
            X.3AU r1 = new X.3AU
            android.view.animation.Animation r0 = android.view.animation.AnimationUtils.loadAnimation(r7, r0)
            r1.<init>(r0)
            return r1
        L_0x008c:
            r0 = 2130772053(0x7f010055, float:1.7147214E38)
            if (r10 == 0) goto L_0x0080
            r0 = 2130772052(0x7f010054, float:1.7147212E38)
            goto L_0x0080
        L_0x0095:
            r0 = 2130772055(0x7f010057, float:1.7147218E38)
            if (r10 == 0) goto L_0x0080
            r0 = 2130772054(0x7f010056, float:1.7147216E38)
            goto L_0x0080
        L_0x009e:
            r0 = 2130772058(0x7f01005a, float:1.7147224E38)
            if (r10 == 0) goto L_0x0080
            r0 = 2130772057(0x7f010059, float:1.7147222E38)
            goto L_0x0080
        L_0x00a7:
            int r1 = r1.A01
            goto L_0x0008
        L_0x00ab:
            int r0 = r1.A02
            goto L_0x0005
        L_0x00af:
            return r2
        L_0x00b0:
            r0 = move-exception
            throw r0
        L_0x00b2:
            return r2
        L_0x00b3:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13060qW.A01(android.content.Context, X.1dd, androidx.fragment.app.Fragment, boolean):X.3AU");
    }

    private void A03() {
        int i;
        if (!this.A0C.isEmpty()) {
            for (Fragment fragment : this.A0C.keySet()) {
                A07(fragment);
                AnonymousClass2NQ r0 = fragment.A0K;
                if (r0 == null) {
                    i = 0;
                } else {
                    i = r0.A03;
                }
                A0s(fragment, i);
            }
        }
    }

    private void A04() {
        if (this.A0B != null) {
            while (!this.A0B.isEmpty()) {
                ((C24933CPk) this.A0B.remove(0)).A00();
            }
        }
    }

    private void A05() {
        for (Fragment fragment : this.A0Q.A02()) {
            if (fragment != null) {
                A0o(fragment);
            }
        }
    }

    private void A06(C21321Gd r5) {
        int i = this.A01;
        if (i >= 1) {
            int min = Math.min(i, 3);
            for (Fragment fragment : this.A0Q.A01()) {
                if (fragment.A0F < min) {
                    A0s(fragment, min);
                    if (fragment.A0I != null && !fragment.A0b && fragment.A08) {
                        r5.add(fragment);
                    }
                }
            }
        }
    }

    private void A07(Fragment fragment) {
        HashSet hashSet = (HashSet) this.A0C.get(fragment);
        if (hashSet != null) {
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                ((C29702EgP) it.next()).A00();
            }
            hashSet.clear();
            A0B(this, fragment);
            this.A0C.remove(fragment);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r3.A0L() <= 0) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        if (r3.A19(r3.A03) == false) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        r1.A01 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        r1 = r3.A0N;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A09(X.C13060qW r3) {
        /*
            java.util.ArrayList r1 = r3.A0R
            monitor-enter(r1)
            java.util.ArrayList r0 = r3.A0R     // Catch:{ all -> 0x0028 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0028 }
            r2 = 1
            if (r0 != 0) goto L_0x0012
            X.1dh r0 = r3.A0N     // Catch:{ all -> 0x0028 }
            r0.A01 = r2     // Catch:{ all -> 0x0028 }
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            return
        L_0x0012:
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            X.1dh r1 = r3.A0N
            int r0 = r3.A0L()
            if (r0 <= 0) goto L_0x0026
            androidx.fragment.app.Fragment r0 = r3.A03
            boolean r0 = r3.A19(r0)
            if (r0 == 0) goto L_0x0026
        L_0x0023:
            r1.A01 = r2
            return
        L_0x0026:
            r2 = 0
            goto L_0x0023
        L_0x0028:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13060qW.A09(X.0qW):void");
    }

    public static void A0C(C13060qW r2, Fragment fragment) {
        if (fragment != null) {
            if (fragment.equals(r2.A0Q.A00(fragment.A0V))) {
                boolean A19 = fragment.A0P.A19(fragment);
                Boolean bool = fragment.A0S;
                if (bool == null || bool.booleanValue() != A19) {
                    fragment.A0S = Boolean.valueOf(A19);
                    C13060qW r1 = fragment.A0O;
                    A09(r1);
                    A0C(r1, r1.A04);
                }
            }
        }
    }

    private void A0E(ArrayList arrayList, ArrayList arrayList2) {
        int size;
        int indexOf;
        int indexOf2;
        ArrayList arrayList3 = this.A0B;
        if (arrayList3 == null) {
            size = 0;
        } else {
            size = arrayList3.size();
        }
        int i = 0;
        while (i < size) {
            C24933CPk cPk = (C24933CPk) this.A0B.get(i);
            if (arrayList == null || cPk.A02 || (indexOf2 = arrayList.indexOf(cPk.A01)) == -1 || arrayList2 == null || !((Boolean) arrayList2.get(indexOf2)).booleanValue()) {
                boolean z = false;
                if (cPk.A00 == 0) {
                    z = true;
                }
                if (z || (arrayList != null && cPk.A01.A0R(arrayList, 0, arrayList.size()))) {
                    this.A0B.remove(i);
                    i--;
                    size--;
                    if (arrayList == null || cPk.A02 || (indexOf = arrayList.indexOf(cPk.A01)) == -1 || arrayList2 == null || !((Boolean) arrayList2.get(indexOf)).booleanValue()) {
                        cPk.A00();
                    } else {
                        C16280wn r3 = cPk.A01;
                        r3.A02.A0g(r3, cPk.A02, false, false);
                    }
                }
            } else {
                this.A0B.remove(i);
                i--;
                size--;
                C16280wn r32 = cPk.A01;
                r32.A02.A0g(r32, cPk.A02, false, false);
            }
            i++;
        }
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [boolean] */
    /* JADX WARN: Type inference failed for: r8v1 */
    /* JADX WARN: Type inference failed for: r8v2 */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0208, code lost:
        if (r5.A0R(r13, r6 + 1, r7) != false) goto L_0x020a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0G(java.util.ArrayList r30, java.util.ArrayList r31, int r32, int r33) {
        /*
            r29 = this;
            r11 = r33
            r12 = r32
            r10 = r29
            r13 = r30
            java.lang.Object r0 = r13.get(r12)
            X.0wn r0 = (X.C16280wn) r0
            boolean r9 = r0.A0F
            java.util.ArrayList r0 = r10.A0K
            if (r0 != 0) goto L_0x016d
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r10.A0K = r0
        L_0x001b:
            java.util.ArrayList r1 = r10.A0K
            X.1de r0 = r10.A0Q
            java.util.List r0 = r0.A01()
            r1.addAll(r0)
            androidx.fragment.app.Fragment r0 = r10.A04
            r21 = r0
            r7 = r12
            r20 = 0
        L_0x002d:
            r6 = 1
            r14 = r31
            if (r7 >= r11) goto L_0x0172
            java.lang.Object r5 = r13.get(r7)
            X.0wn r5 = (X.C16280wn) r5
            java.lang.Object r0 = r14.get(r7)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0126
            java.util.ArrayList r0 = r10.A0K
            r8 = r0
            r4 = 0
        L_0x0048:
            java.util.ArrayList r0 = r5.A0C
            int r0 = r0.size()
            if (r4 >= r0) goto L_0x015f
            java.util.ArrayList r0 = r5.A0C
            java.lang.Object r14 = r0.get(r4)
            X.1gT r14 = (X.C29471gT) r14
            int r1 = r14.A00
            r19 = 0
            if (r1 == r6) goto L_0x011f
            r0 = 2
            r18 = 3
            r17 = 9
            if (r1 == r0) goto L_0x00a6
            r0 = r18
            if (r1 == r0) goto L_0x008a
            r0 = 6
            if (r1 == r0) goto L_0x008a
            r0 = 7
            if (r1 == r0) goto L_0x011f
            r0 = 8
            if (r1 != r0) goto L_0x0088
            java.util.ArrayList r1 = r5.A0C
            X.1gT r0 = new X.1gT
            r15 = r0
            r16 = r17
            r17 = r21
            r15.<init>(r16, r17)
            r1.add(r4, r0)
            int r4 = r4 + 1
            androidx.fragment.app.Fragment r0 = r14.A05
            r21 = r0
        L_0x0088:
            int r4 = r4 + r6
            goto L_0x0048
        L_0x008a:
            androidx.fragment.app.Fragment r0 = r14.A05
            r8.remove(r0)
            androidx.fragment.app.Fragment r3 = r14.A05
            r0 = r21
            if (r3 != r0) goto L_0x0088
            java.util.ArrayList r2 = r5.A0C
            X.1gT r1 = new X.1gT
            r0 = r17
            r1.<init>(r0, r3)
            r2.add(r4, r1)
            int r4 = r4 + 1
            r21 = r19
            goto L_0x0088
        L_0x00a6:
            androidx.fragment.app.Fragment r3 = r14.A05
            int r0 = r3.A0D
            r25 = r0
            int r2 = r8.size()
            int r2 = r2 - r6
            r16 = 0
        L_0x00b3:
            if (r2 < 0) goto L_0x010d
            java.lang.Object r1 = r8.get(r2)
            androidx.fragment.app.Fragment r1 = (androidx.fragment.app.Fragment) r1
            int r15 = r1.A0D
            r0 = r25
            if (r15 != r0) goto L_0x00c5
            if (r1 != r3) goto L_0x00c8
            r16 = 1
        L_0x00c5:
            int r2 = r2 + -1
            goto L_0x00b3
        L_0x00c8:
            r0 = r21
            if (r1 != r0) goto L_0x00e2
            java.util.ArrayList r0 = r5.A0C
            r21 = r0
            X.1gT r15 = new X.1gT
            r0 = r17
            r15.<init>(r0, r1)
            r22 = r4
            r23 = r15
            r21.add(r22, r23)
            int r4 = r4 + 1
            r21 = r19
        L_0x00e2:
            X.1gT r15 = new X.1gT
            r22 = r15
            r23 = r18
            r24 = r1
            r22.<init>(r23, r24)
            int r0 = r14.A01
            r15.A01 = r0
            int r0 = r14.A03
            r15.A03 = r0
            int r0 = r14.A02
            r15.A02 = r0
            int r0 = r14.A04
            r15.A04 = r0
            java.util.ArrayList r0 = r5.A0C
            r22 = r0
            r23 = r4
            r24 = r15
            r22.add(r23, r24)
            r8.remove(r1)
            int r4 = r4 + r6
            goto L_0x00c5
        L_0x010d:
            if (r16 == 0) goto L_0x0118
            java.util.ArrayList r0 = r5.A0C
            r0.remove(r4)
            int r4 = r4 + -1
            goto L_0x0088
        L_0x0118:
            r14.A00 = r6
            r8.add(r3)
            goto L_0x0088
        L_0x011f:
            androidx.fragment.app.Fragment r0 = r14.A05
            r8.add(r0)
            goto L_0x0088
        L_0x0126:
            java.util.ArrayList r3 = r10.A0K
            java.util.ArrayList r0 = r5.A0C
            int r2 = r0.size()
            int r2 = r2 - r6
        L_0x012f:
            if (r2 < 0) goto L_0x015f
            java.util.ArrayList r0 = r5.A0C
            java.lang.Object r4 = r0.get(r2)
            X.1gT r4 = (X.C29471gT) r4
            int r1 = r4.A00
            if (r1 == r6) goto L_0x0159
            r0 = 3
            if (r1 == r0) goto L_0x0153
            switch(r1) {
                case 6: goto L_0x0153;
                case 7: goto L_0x0159;
                case 8: goto L_0x0150;
                case 9: goto L_0x014b;
                case 10: goto L_0x0146;
                default: goto L_0x0143;
            }
        L_0x0143:
            int r2 = r2 + -1
            goto L_0x012f
        L_0x0146:
            X.1XL r0 = r4.A07
            r4.A06 = r0
            goto L_0x0143
        L_0x014b:
            androidx.fragment.app.Fragment r0 = r4.A05
            r21 = r0
            goto L_0x0143
        L_0x0150:
            r21 = 0
            goto L_0x0143
        L_0x0153:
            androidx.fragment.app.Fragment r0 = r4.A05
            r3.add(r0)
            goto L_0x0143
        L_0x0159:
            androidx.fragment.app.Fragment r0 = r4.A05
            r3.remove(r0)
            goto L_0x0143
        L_0x015f:
            if (r20 != 0) goto L_0x0167
            boolean r0 = r5.A05
            r20 = 0
            if (r0 == 0) goto L_0x0169
        L_0x0167:
            r20 = 1
        L_0x0169:
            int r7 = r7 + 1
            goto L_0x002d
        L_0x016d:
            r0.clear()
            goto L_0x001b
        L_0x0172:
            java.util.ArrayList r0 = r10.A0K
            r0.clear()
            if (r9 != 0) goto L_0x0198
            int r0 = r10.A01
            if (r0 < r6) goto L_0x0198
            X.0qR r0 = r10.A06
            android.content.Context r2 = r0.A01
            X.1dd r1 = r10.A05
            r27 = 0
            X.1dj r0 = r10.A0T
            r25 = r12
            r26 = r11
            r21 = r2
            r22 = r1
            r23 = r13
            r24 = r14
            r28 = r0
            X.C16320wr.A07(r21, r22, r23, r24, r25, r26, r27, r28)
        L_0x0198:
            r3 = r12
        L_0x0199:
            if (r3 >= r11) goto L_0x01c4
            java.lang.Object r2 = r13.get(r3)
            X.0wn r2 = (X.C16280wn) r2
            java.lang.Object r0 = r14.get(r3)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r1 = 1
            if (r0 == 0) goto L_0x01bd
            r0 = -1
            r2.A0N(r0)
            int r0 = r33 + -1
            if (r3 == r0) goto L_0x01b7
            r1 = 0
        L_0x01b7:
            r2.A0P(r1)
        L_0x01ba:
            int r3 = r3 + 1
            goto L_0x0199
        L_0x01bd:
            r2.A0N(r6)
            r2.A0M()
            goto L_0x01ba
        L_0x01c4:
            if (r9 == 0) goto L_0x0282
            X.1Gd r15 = new X.1Gd
            r15.<init>()
            r10.A06(r15)
            r8 = 1
            r7 = r11
            r24 = r14
            int r6 = r33 + -1
        L_0x01d4:
            if (r6 < r12) goto L_0x0260
            java.lang.Object r5 = r13.get(r6)
            X.0wn r5 = (X.C16280wn) r5
            java.lang.Object r0 = r14.get(r6)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r4 = r0.booleanValue()
            r1 = 0
        L_0x01e7:
            java.util.ArrayList r0 = r5.A0C
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x025e
            java.util.ArrayList r0 = r5.A0C
            java.lang.Object r0 = r0.get(r1)
            X.1gT r0 = (X.C29471gT) r0
            boolean r0 = X.C16280wn.A01(r0)
            if (r0 == 0) goto L_0x025b
            r0 = 1
        L_0x01fe:
            r3 = 0
            if (r0 == 0) goto L_0x020a
            int r0 = r6 + 1
            boolean r1 = r5.A0R(r13, r0, r7)
            r0 = 1
            if (r1 == 0) goto L_0x020b
        L_0x020a:
            r0 = 0
        L_0x020b:
            if (r0 == 0) goto L_0x0253
            java.util.ArrayList r0 = r10.A0B
            if (r0 != 0) goto L_0x0218
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r10.A0B = r0
        L_0x0218:
            X.CPk r2 = new X.CPk
            r2.<init>(r5, r4)
            java.util.ArrayList r0 = r10.A0B
            r0.add(r2)
            r1 = 0
        L_0x0223:
            java.util.ArrayList r0 = r5.A0C
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0241
            java.util.ArrayList r0 = r5.A0C
            java.lang.Object r0 = r0.get(r1)
            X.1gT r0 = (X.C29471gT) r0
            boolean r16 = X.C16280wn.A01(r0)
            if (r16 == 0) goto L_0x023e
            androidx.fragment.app.Fragment r0 = r0.A05
            r0.A1R(r2)
        L_0x023e:
            int r1 = r1 + 1
            goto L_0x0223
        L_0x0241:
            if (r4 == 0) goto L_0x0257
            r5.A0M()
        L_0x0246:
            int r11 = r11 + -1
            if (r6 == r11) goto L_0x0250
            r13.remove(r6)
            r13.add(r11, r5)
        L_0x0250:
            r10.A06(r15)
        L_0x0253:
            int r6 = r6 + -1
            goto L_0x01d4
        L_0x0257:
            r5.A0P(r3)
            goto L_0x0246
        L_0x025b:
            int r1 = r1 + 1
            goto L_0x01e7
        L_0x025e:
            r0 = 0
            goto L_0x01fe
        L_0x0260:
            int r4 = r15.size()
            r3 = 0
        L_0x0265:
            if (r3 >= r4) goto L_0x0287
            java.lang.Object[] r0 = r15.A03
            r2 = r0[r3]
            androidx.fragment.app.Fragment r2 = (androidx.fragment.app.Fragment) r2
            boolean r0 = r2.A0W
            if (r0 != 0) goto L_0x027f
            android.view.View r1 = r2.A14()
            float r0 = r1.getAlpha()
            r2.A0B = r0
            r0 = 0
            r1.setAlpha(r0)
        L_0x027f:
            int r3 = r3 + 1
            goto L_0x0265
        L_0x0282:
            r9 = 0
            r7 = r11
            r24 = r14
            r8 = 1
        L_0x0287:
            if (r11 == r12) goto L_0x02ad
            if (r9 == 0) goto L_0x02ad
            int r0 = r10.A01
            if (r0 < r8) goto L_0x02a8
            X.0qR r0 = r10.A06
            android.content.Context r4 = r0.A01
            X.1dd r3 = r10.A05
            X.1dj r0 = r10.A0T
            r27 = 1
            r21 = r4
            r22 = r3
            r23 = r13
            r25 = r12
            r26 = r11
            r28 = r0
            X.C16320wr.A07(r21, r22, r23, r24, r25, r26, r27, r28)
        L_0x02a8:
            int r0 = r10.A01
            r10.A0b(r0, r8)
        L_0x02ad:
            if (r12 >= r7) goto L_0x02e3
            java.lang.Object r1 = r13.get(r12)
            X.0wn r1 = (X.C16280wn) r1
            java.lang.Object r0 = r14.get(r12)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x02c8
            int r0 = r1.A00
            if (r0 < 0) goto L_0x02c8
            r0 = -1
            r1.A00 = r0
        L_0x02c8:
            r0 = 0
            if (r0 == 0) goto L_0x02e0
            r1 = 0
        L_0x02cc:
            r0 = 0
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x02e0
            r0 = 0
            java.lang.Object r0 = r0.get(r1)
            java.lang.Runnable r0 = (java.lang.Runnable) r0
            r0.run()
            int r1 = r1 + 1
            goto L_0x02cc
        L_0x02e0:
            int r12 = r12 + 1
            goto L_0x02ad
        L_0x02e3:
            if (r20 == 0) goto L_0x0300
            java.util.ArrayList r0 = r10.A0A
            if (r0 == 0) goto L_0x0300
            r1 = 0
        L_0x02ea:
            java.util.ArrayList r0 = r10.A0A
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0300
            java.util.ArrayList r0 = r10.A0A
            java.lang.Object r0 = r0.get(r1)
            X.9jq r0 = (X.C204149jq) r0
            r0.onBackStackChanged()
            int r1 = r1 + 1
            goto L_0x02ea
        L_0x0300:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13060qW.A0G(java.util.ArrayList, java.util.ArrayList, int, int):void");
    }

    private void A0H(boolean z) {
        if (this.A00) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (this.A06 == null) {
            if (this.A0D) {
                throw new IllegalStateException("FragmentManager has been destroyed");
            }
            throw new IllegalStateException("FragmentManager has not been attached to a host.");
        } else if (Looper.myLooper() != this.A06.A02.getLooper()) {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        } else if (z || !A13()) {
            if (this.A0M == null) {
                this.A0M = new ArrayList();
                this.A0L = new ArrayList();
            }
            this.A00 = true;
            try {
                A0E(null, null);
            } finally {
                this.A00 = false;
            }
        } else {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    public static boolean A0I(int i) {
        if (Log.isLoggable("FragmentManager", i)) {
            return true;
        }
        return false;
    }

    private static boolean A0J(Fragment fragment) {
        boolean z;
        if (!fragment.A0a || !fragment.A0e) {
            Iterator it = fragment.A0O.A0Q.A02().iterator();
            boolean z2 = false;
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                Fragment fragment2 = (Fragment) it.next();
                if (fragment2 != null) {
                    z2 = A0J(fragment2);
                    continue;
                }
                if (z2) {
                    z = true;
                    break;
                }
            }
            if (z) {
                return true;
            }
            return false;
        }
        return true;
    }

    public int A0L() {
        ArrayList arrayList = this.A09;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    public Fragment.SavedState A0N(Fragment fragment) {
        C27721de r0 = this.A0Q;
        C012909q r3 = (C012909q) r0.A01.get(fragment.A0V);
        if (r3 == null || !r3.A03().equals(fragment)) {
            A0D(this, new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        return r3.A02();
    }

    public Fragment A0O(int i) {
        C27721de r3 = this.A0Q;
        for (int size = r3.A00.size() - 1; size >= 0; size--) {
            Fragment fragment = (Fragment) r3.A00.get(size);
            if (fragment != null && fragment.A0E == i) {
                return fragment;
            }
        }
        for (C012909q r0 : r3.A01.values()) {
            if (r0 != null) {
                Fragment A032 = r0.A03();
                if (A032.A0E == i) {
                    return A032;
                }
            }
        }
        return null;
    }

    public Fragment A0Q(String str) {
        C27721de r3 = this.A0Q;
        if (str != null) {
            for (int size = r3.A00.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) r3.A00.get(size);
                if (fragment != null && str.equals(fragment.A0T)) {
                    return fragment;
                }
            }
        }
        if (str == null) {
            return null;
        }
        for (C012909q r0 : r3.A01.values()) {
            if (r0 != null) {
                Fragment A032 = r0.A03();
                if (str.equals(A032.A0T)) {
                    return A032;
                }
            }
        }
        return null;
    }

    public Fragment A0R(String str) {
        for (C012909q r0 : this.A0Q.A01.values()) {
            if (r0 != null) {
                Fragment A032 = r0.A03();
                if (!str.equals(A032.A0V)) {
                    A032 = A032.A0O.A0R(str);
                }
                if (A032 != null) {
                    return A032;
                }
            }
        }
        return null;
    }

    public C27801dm A0S() {
        Fragment fragment = this.A03;
        if (fragment != null) {
            return fragment.A0P.A0S();
        }
        return this.A0I;
    }

    public C16290wo A0T() {
        return new C16280wn(this);
    }

    public List A0U() {
        return this.A0Q.A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (r0.isEmpty() != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0V() {
        /*
            r5 = this;
            java.util.ArrayList r4 = r5.A0R
            monitor-enter(r4)
            java.util.ArrayList r0 = r5.A0B     // Catch:{ all -> 0x0038 }
            r3 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0010
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0038 }
            r1 = 1
            if (r0 == 0) goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            java.util.ArrayList r0 = r5.A0R     // Catch:{ all -> 0x0038 }
            int r0 = r0.size()     // Catch:{ all -> 0x0038 }
            if (r0 != r2) goto L_0x001a
            r3 = 1
        L_0x001a:
            if (r1 != 0) goto L_0x001e
            if (r3 == 0) goto L_0x0036
        L_0x001e:
            X.0qR r0 = r5.A06     // Catch:{ all -> 0x0038 }
            android.os.Handler r1 = r0.A02     // Catch:{ all -> 0x0038 }
            java.lang.Runnable r0 = r5.A08     // Catch:{ all -> 0x0038 }
            X.AnonymousClass00S.A02(r1, r0)     // Catch:{ all -> 0x0038 }
            X.0qR r0 = r5.A06     // Catch:{ all -> 0x0038 }
            android.os.Handler r2 = r0.A02     // Catch:{ all -> 0x0038 }
            java.lang.Runnable r1 = r5.A08     // Catch:{ all -> 0x0038 }
            r0 = 888021414(0x34ee21a6, float:4.4355437E-7)
            X.AnonymousClass00S.A04(r2, r1, r0)     // Catch:{ all -> 0x0038 }
            A09(r5)     // Catch:{ all -> 0x0038 }
        L_0x0036:
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            return
        L_0x0038:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13060qW.A0V():void");
    }

    public void A0X() {
        for (Fragment fragment : this.A0Q.A01()) {
            if (fragment != null) {
                fragment.onLowMemory();
                fragment.A0O.A0X();
            }
        }
    }

    public void A0a() {
        A0w(new AnonymousClass6ZC(this, null, -1, 0), false);
    }

    public void A0b(int i, boolean z) {
        C13020qR r2;
        if (this.A06 == null && i != -1) {
            throw new IllegalStateException("No activity");
        } else if (z || i != this.A01) {
            this.A01 = i;
            for (Fragment A0n : this.A0Q.A01()) {
                A0n(A0n);
            }
            for (Fragment fragment : this.A0Q.A02()) {
                if (fragment != null && !fragment.A08) {
                    A0n(fragment);
                }
            }
            A05();
            if (this.A0F && (r2 = this.A06) != null && this.A01 == 4) {
                r2.A04();
                this.A0F = false;
            }
        }
    }

    public void A0c(Configuration configuration) {
        for (Fragment fragment : this.A0Q.A01()) {
            if (fragment != null) {
                fragment.onConfigurationChanged(configuration);
                fragment.A0O.A0c(configuration);
            }
        }
    }

    public void A0d(Bundle bundle, String str, Fragment fragment) {
        if (fragment.A0P != this) {
            A0D(this, new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putString(str, fragment.A0V);
    }

    public void A0e(Parcelable parcelable) {
        C012909q r3;
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.A02 != null) {
                this.A0Q.A01.clear();
                Iterator it = fragmentManagerState.A02.iterator();
                while (it.hasNext()) {
                    FragmentState fragmentState = (FragmentState) it.next();
                    if (fragmentState != null) {
                        C27931dz r0 = this.A07;
                        Fragment fragment = (Fragment) r0.A02.get(fragmentState.A07);
                        if (fragment != null) {
                            A0I(2);
                            r3 = new C012909q(this.A0P, fragment, fragmentState);
                        } else {
                            r3 = new C012909q(this.A0P, this.A06.A01.getClassLoader(), A0S(), fragmentState);
                        }
                        r3.A03().A0P = this;
                        A0I(2);
                        r3.A0I(this.A06.A01.getClassLoader());
                        this.A0Q.A01.put(r3.A03().A0V, r3);
                        r3.A00 = this.A01;
                    }
                }
                for (Fragment fragment2 : this.A07.A02.values()) {
                    C27721de r02 = this.A0Q;
                    if (!r02.A01.containsKey(fragment2.A0V)) {
                        A0I(2);
                        A0s(fragment2, 1);
                        fragment2.A0f = true;
                        A0s(fragment2, -1);
                    }
                }
                C27721de r4 = this.A0Q;
                ArrayList<String> arrayList = fragmentManagerState.A03;
                r4.A00.clear();
                if (arrayList != null) {
                    for (String str : arrayList) {
                        Fragment A002 = r4.A00(str);
                        if (A002 != null) {
                            A0I(2);
                            r4.A04(A002);
                        } else {
                            throw new IllegalStateException(AnonymousClass08S.A0P("No instantiated fragment for (", str, ")"));
                        }
                    }
                }
                BackStackState[] backStackStateArr = fragmentManagerState.A04;
                if (backStackStateArr != null) {
                    this.A09 = new ArrayList(backStackStateArr.length);
                    int i = 0;
                    while (true) {
                        BackStackState[] backStackStateArr2 = fragmentManagerState.A04;
                        if (i >= backStackStateArr2.length) {
                            break;
                        }
                        C16280wn A003 = backStackStateArr2[i].A00(this);
                        if (A0I(2)) {
                            PrintWriter printWriter = new PrintWriter(new AnonymousClass8Q2());
                            A003.A0O("  ", printWriter, false);
                            printWriter.close();
                        }
                        this.A09.add(A003);
                        i++;
                    }
                } else {
                    this.A09 = null;
                }
                this.A0S.set(fragmentManagerState.A00);
                String str2 = fragmentManagerState.A01;
                if (str2 != null) {
                    Fragment A004 = this.A0Q.A00(str2);
                    this.A04 = A004;
                    A0C(this, A004);
                }
            }
        }
    }

    public void A0f(Menu menu) {
        if (this.A01 >= 1) {
            for (Fragment fragment : this.A0Q.A01()) {
                if (fragment != null && !fragment.A0b) {
                    fragment.A0O.A0f(menu);
                }
            }
        }
    }

    public void A0g(C16280wn r11, boolean z, boolean z2, boolean z3) {
        if (z) {
            r11.A0P(z3);
        } else {
            r11.A0M();
        }
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(r11);
        arrayList2.add(Boolean.valueOf(z));
        if (z2 && this.A01 >= 1) {
            C16320wr.A07(this.A06.A01, this.A05, arrayList, arrayList2, 0, 1, true, this.A0T);
        }
        if (z3) {
            A0b(this.A01, true);
        }
        for (Fragment fragment : this.A0Q.A02()) {
            if (fragment != null && fragment.A0I != null && fragment.A08 && r11.A0Q(fragment.A0D)) {
                float f = fragment.A0B;
                if (f > 0.0f) {
                    fragment.A0I.setAlpha(f);
                }
                if (z3) {
                    fragment.A0B = 0.0f;
                } else {
                    fragment.A0B = -1.0f;
                    fragment.A08 = false;
                }
            }
        }
    }

    public void A0m(Fragment fragment) {
        C27721de r0 = this.A0Q;
        if (!r0.A01.containsKey(fragment.A0V)) {
            C012909q r2 = new C012909q(this.A0P, fragment);
            r2.A0I(this.A06.A01.getClassLoader());
            this.A0Q.A01.put(r2.A03().A0V, r2);
            if (fragment.A0A) {
                if (fragment.A0h) {
                    A0i(fragment);
                } else {
                    A0q(fragment);
                }
                fragment.A0A = false;
            }
            r2.A00 = this.A01;
            A0I(2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0107, code lost:
        if (r0 != false) goto L_0x0109;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0n(androidx.fragment.app.Fragment r8) {
        /*
            r7 = this;
            X.1de r0 = r7.A0Q
            java.lang.String r1 = r8.A0V
            java.util.HashMap r0 = r0.A01
            boolean r0 = r0.containsKey(r1)
            if (r0 != 0) goto L_0x0011
            r0 = 3
            A0I(r0)
            return
        L_0x0011:
            int r0 = r7.A01
            r7.A0s(r8, r0)
            android.view.View r0 = r8.A0I
            if (r0 == 0) goto L_0x0087
            X.1de r5 = r7.A0Q
            android.view.ViewGroup r4 = r8.A0J
            r3 = 0
            if (r4 == 0) goto L_0x003e
            if (r0 == 0) goto L_0x003e
            java.util.ArrayList r0 = r5.A00
            int r0 = r0.indexOf(r8)
            int r2 = r0 + -1
        L_0x002b:
            if (r2 < 0) goto L_0x003e
            java.util.ArrayList r0 = r5.A00
            java.lang.Object r1 = r0.get(r2)
            androidx.fragment.app.Fragment r1 = (androidx.fragment.app.Fragment) r1
            android.view.ViewGroup r0 = r1.A0J
            if (r0 != r4) goto L_0x0131
            android.view.View r0 = r1.A0I
            if (r0 == 0) goto L_0x0131
            r3 = r1
        L_0x003e:
            if (r3 == 0) goto L_0x0058
            android.view.View r0 = r3.A0I
            android.view.ViewGroup r2 = r8.A0J
            int r1 = r2.indexOfChild(r0)
            android.view.View r0 = r8.A0I
            int r0 = r2.indexOfChild(r0)
            if (r0 >= r1) goto L_0x0058
            r2.removeViewAt(r0)
            android.view.View r0 = r8.A0I
            r2.addView(r0, r1)
        L_0x0058:
            boolean r0 = r8.A08
            if (r0 == 0) goto L_0x0087
            android.view.ViewGroup r0 = r8.A0J
            if (r0 == 0) goto L_0x0087
            float r2 = r8.A0B
            r1 = 0
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x006c
            android.view.View r0 = r8.A0I
            r0.setAlpha(r2)
        L_0x006c:
            r8.A0B = r1
            r0 = 0
            r8.A08 = r0
            X.0qR r0 = r7.A06
            android.content.Context r2 = r0.A01
            X.1dd r1 = r7.A05
            r0 = 1
            X.3AU r2 = A01(r2, r1, r8, r0)
            if (r2 == 0) goto L_0x0087
            android.view.animation.Animation r1 = r2.A01
            if (r1 == 0) goto L_0x0123
            android.view.View r0 = r8.A0I
            r0.startAnimation(r1)
        L_0x0087:
            boolean r0 = r8.A0c
            if (r0 == 0) goto L_0x00d2
            android.view.View r0 = r8.A0I
            r3 = 1
            r2 = 0
            if (r0 == 0) goto L_0x00bf
            X.0qR r0 = r7.A06
            android.content.Context r4 = r0.A01
            X.1dd r1 = r7.A05
            boolean r0 = r8.A0b
            r0 = r0 ^ r3
            X.3AU r6 = A01(r4, r1, r8, r0)
            if (r6 == 0) goto L_0x00ee
            android.animation.Animator r1 = r6.A00
            if (r1 == 0) goto L_0x00ee
            android.view.View r0 = r8.A0I
            r1.setTarget(r0)
            boolean r0 = r8.A0b
            if (r0 == 0) goto L_0x00e8
            X.2NQ r0 = r8.A0K
            if (r0 != 0) goto L_0x00e5
            r0 = 0
        L_0x00b2:
            if (r0 == 0) goto L_0x00d3
            X.2NQ r0 = androidx.fragment.app.Fragment.A01(r8)
            r0.A09 = r2
        L_0x00ba:
            android.animation.Animator r0 = r6.A00
            r0.start()
        L_0x00bf:
            boolean r0 = r8.A0W
            if (r0 == 0) goto L_0x00cb
            boolean r0 = A0J(r8)
            if (r0 == 0) goto L_0x00cb
            r7.A0F = r3
        L_0x00cb:
            r8.A0c = r2
            boolean r0 = r8.A0b
            r8.A1x(r0)
        L_0x00d2:
            return
        L_0x00d3:
            android.view.ViewGroup r5 = r8.A0J
            android.view.View r4 = r8.A0I
            r5.startViewTransition(r4)
            android.animation.Animator r1 = r6.A00
            X.4TD r0 = new X.4TD
            r0.<init>(r5, r4, r8)
            r1.addListener(r0)
            goto L_0x00ba
        L_0x00e5:
            boolean r0 = r0.A09
            goto L_0x00b2
        L_0x00e8:
            android.view.View r0 = r8.A0I
            r0.setVisibility(r2)
            goto L_0x00ba
        L_0x00ee:
            if (r6 == 0) goto L_0x00fc
            android.view.View r1 = r8.A0I
            android.view.animation.Animation r0 = r6.A01
            r1.startAnimation(r0)
            android.view.animation.Animation r0 = r6.A01
            r0.start()
        L_0x00fc:
            boolean r0 = r8.A0b
            if (r0 == 0) goto L_0x0109
            X.2NQ r0 = r8.A0K
            if (r0 != 0) goto L_0x0120
            r0 = 0
        L_0x0105:
            r1 = 8
            if (r0 == 0) goto L_0x010a
        L_0x0109:
            r1 = 0
        L_0x010a:
            android.view.View r0 = r8.A0I
            r0.setVisibility(r1)
            X.2NQ r0 = r8.A0K
            if (r0 != 0) goto L_0x011d
            r0 = 0
        L_0x0114:
            if (r0 == 0) goto L_0x00bf
            X.2NQ r0 = androidx.fragment.app.Fragment.A01(r8)
            r0.A09 = r2
            goto L_0x00bf
        L_0x011d:
            boolean r0 = r0.A09
            goto L_0x0114
        L_0x0120:
            boolean r0 = r0.A09
            goto L_0x0105
        L_0x0123:
            android.animation.Animator r1 = r2.A00
            android.view.View r0 = r8.A0I
            r1.setTarget(r0)
            android.animation.Animator r0 = r2.A00
            r0.start()
            goto L_0x0087
        L_0x0131:
            int r2 = r2 + -1
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13060qW.A0n(androidx.fragment.app.Fragment):void");
    }

    public void A0o(Fragment fragment) {
        if (!fragment.A06) {
            return;
        }
        if (this.A00) {
            this.A0E = true;
            return;
        }
        fragment.A06 = false;
        A0s(fragment, this.A01);
    }

    public void A0r(Fragment fragment) {
        if (fragment != null) {
            if (!fragment.equals(this.A0Q.A00(fragment.A0V)) || !(fragment.A0N == null || fragment.A0P == this)) {
                throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
            }
        }
        Fragment fragment2 = this.A04;
        this.A04 = fragment;
        A0C(this, fragment2);
        A0C(this, this.A04);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0045, code lost:
        if (r0 != 3) goto L_0x0047;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x0148 */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0148 A[LOOP:0: B:91:0x0148->B:161:0x0148, LOOP_START, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0s(androidx.fragment.app.Fragment r18, int r19) {
        /*
            r17 = this;
            r5 = r17
            X.1de r0 = r5.A0Q
            r14 = r18
            java.lang.String r1 = r14.A0V
            java.util.HashMap r0 = r0.A01
            java.lang.Object r4 = r0.get(r1)
            X.09q r4 = (X.C012909q) r4
            r7 = 1
            if (r4 != 0) goto L_0x001c
            X.09q r4 = new X.09q
            X.1dk r0 = r5.A0P
            r4.<init>(r0, r14)
            r4.A00 = r7
        L_0x001c:
            int r0 = r4.A01()
            r1 = r19
            int r3 = java.lang.Math.min(r1, r0)
            int r10 = r14.A0F
            r1 = 0
            r9 = -1
            r8 = 2
            r2 = 3
            if (r10 > r3) goto L_0x00bb
            if (r10 >= r3) goto L_0x003b
            java.util.concurrent.ConcurrentHashMap r0 = r5.A0C
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x003b
            r5.A07(r14)
        L_0x003b:
            int r0 = r14.A0F
            if (r0 == r9) goto L_0x0051
            if (r0 == 0) goto L_0x0099
            if (r0 == r7) goto L_0x009e
            if (r0 == r8) goto L_0x00b0
            if (r0 == r2) goto L_0x00b5
        L_0x0047:
            int r0 = r14.A0F
            if (r0 == r3) goto L_0x0050
            A0I(r2)
            r14.A0F = r3
        L_0x0050:
            return
        L_0x0051:
            if (r3 <= r9) goto L_0x0099
            A0I(r2)
            androidx.fragment.app.Fragment r13 = r14.A0M
            java.lang.String r6 = " that does not belong to this FragmentManager!"
            java.lang.String r10 = " declared target fragment "
            java.lang.String r0 = "Fragment "
            if (r13 == 0) goto L_0x007f
            java.lang.String r12 = r13.A0V
            X.1de r11 = r5.A0Q
            androidx.fragment.app.Fragment r11 = r11.A00(r12)
            boolean r11 = r13.equals(r11)
            if (r11 == 0) goto L_0x027e
            androidx.fragment.app.Fragment r12 = r14.A0M
            int r11 = r12.A0F
            if (r11 >= r7) goto L_0x0077
            r5.A0s(r12, r7)
        L_0x0077:
            androidx.fragment.app.Fragment r11 = r14.A0M
            java.lang.String r11 = r11.A0V
            r14.A0U = r11
            r14.A0M = r1
        L_0x007f:
            java.lang.String r11 = r14.A0U
            if (r11 == 0) goto L_0x0092
            X.1de r1 = r5.A0Q
            androidx.fragment.app.Fragment r1 = r1.A00(r11)
            if (r1 == 0) goto L_0x025e
            int r0 = r1.A0F
            if (r0 >= r7) goto L_0x0092
            r5.A0s(r1, r7)
        L_0x0092:
            X.0qR r1 = r5.A06
            androidx.fragment.app.Fragment r0 = r5.A03
            r4.A0F(r1, r5, r0)
        L_0x0099:
            if (r3 <= 0) goto L_0x009e
            r4.A06()
        L_0x009e:
            if (r3 <= r9) goto L_0x00a3
            r4.A07()
        L_0x00a3:
            if (r3 <= r7) goto L_0x00b0
            X.1dd r0 = r5.A05
            r4.A0E(r0)
            r4.A05()
            r4.A09()
        L_0x00b0:
            if (r3 <= r8) goto L_0x00b5
            r4.A0C()
        L_0x00b5:
            if (r3 <= r2) goto L_0x0047
            r4.A0A()
            goto L_0x0047
        L_0x00bb:
            if (r10 <= r3) goto L_0x0047
            if (r10 == 0) goto L_0x01ce
            r6 = 0
            if (r10 == r7) goto L_0x0194
            if (r10 == r8) goto L_0x00d3
            if (r10 == r2) goto L_0x00ce
            r0 = 4
            if (r10 != r0) goto L_0x0047
            if (r3 >= r0) goto L_0x00ce
            r4.A08()
        L_0x00ce:
            if (r3 >= r2) goto L_0x00d3
            r4.A0D()
        L_0x00d3:
            if (r3 >= r8) goto L_0x0194
            A0I(r2)
            android.view.View r0 = r14.A0I
            if (r0 == 0) goto L_0x00eb
            X.0qR r0 = r5.A06
            boolean r0 = r0.A08(r14)
            if (r0 == 0) goto L_0x00eb
            android.util.SparseArray r0 = r14.A0H
            if (r0 != 0) goto L_0x00eb
            r4.A0B()
        L_0x00eb:
            android.view.View r8 = r14.A0I
            if (r8 == 0) goto L_0x0189
            android.view.ViewGroup r0 = r14.A0J
            if (r0 == 0) goto L_0x0189
            r0.endViewTransition(r8)
            android.view.View r0 = r14.A0I
            r0.clearAnimation()
            androidx.fragment.app.Fragment r8 = r14.A0L
            if (r8 == 0) goto L_0x0146
            boolean r0 = r8.A0f
            if (r0 != 0) goto L_0x0109
            boolean r0 = r8.A1Y()
            if (r0 == 0) goto L_0x0146
        L_0x0109:
            r0 = 1
        L_0x010a:
            if (r0 != 0) goto L_0x0189
            int r0 = r5.A01
            r8 = 0
            if (r0 <= r9) goto L_0x012d
            boolean r0 = r5.A0D
            if (r0 != 0) goto L_0x012d
            android.view.View r0 = r14.A0I
            int r0 = r0.getVisibility()
            if (r0 != 0) goto L_0x012d
            float r0 = r14.A0B
            int r0 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r0 < 0) goto L_0x012d
            X.0qR r0 = r5.A06
            android.content.Context r1 = r0.A01
            X.1dd r0 = r5.A05
            X.3AU r1 = A01(r1, r0, r14, r6)
        L_0x012d:
            r14.A0B = r8
            if (r1 == 0) goto L_0x0182
            X.1dj r15 = r5.A0T
            android.view.View r13 = r14.A0I
            android.view.ViewGroup r12 = r14.A0J
            r12.startViewTransition(r13)
            X.EgP r8 = new X.EgP
            r8.<init>()
            X.EgM r9 = new X.EgM
            r9.<init>(r14)
            monitor-enter(r8)
            goto L_0x0148
        L_0x0146:
            r0 = 0
            goto L_0x010a
        L_0x0148:
            boolean r0 = r8.A01     // Catch:{ all -> 0x029e }
            if (r0 == 0) goto L_0x0150
            r8.wait()     // Catch:{ InterruptedException -> 0x0148 }
            goto L_0x0148
        L_0x0150:
            X.EgQ r0 = r8.A00     // Catch:{ all -> 0x029e }
            if (r0 == r9) goto L_0x015c
            r8.A00 = r9     // Catch:{ all -> 0x029e }
            boolean r0 = r8.A02     // Catch:{ all -> 0x029e }
            if (r0 == 0) goto L_0x015c
            monitor-exit(r8)     // Catch:{ all -> 0x029e }
            goto L_0x015e
        L_0x015c:
            monitor-exit(r8)     // Catch:{ all -> 0x029e }
            goto L_0x0161
        L_0x015e:
            r9.onCancel()
        L_0x0161:
            r15.BpR(r14, r8)
            android.view.animation.Animation r0 = r1.A01
            if (r0 == 0) goto L_0x0242
            X.9o4 r9 = new X.9o4
            r9.<init>(r0, r12, r13)
            android.view.View r1 = r14.A0I
            X.2NQ r0 = androidx.fragment.app.Fragment.A01(r14)
            r0.A05 = r1
            X.EgK r0 = new X.EgK
            r0.<init>(r12, r14, r15, r8)
            r9.setAnimationListener(r0)
            android.view.View r0 = r14.A0I
            r0.startAnimation(r9)
        L_0x0182:
            android.view.ViewGroup r1 = r14.A0J
            android.view.View r0 = r14.A0I
            r1.removeView(r0)
        L_0x0189:
            java.util.concurrent.ConcurrentHashMap r0 = r5.A0C
            java.lang.Object r0 = r0.get(r14)
            if (r0 != 0) goto L_0x023a
            A0B(r5, r14)
        L_0x0194:
            if (r3 >= r7) goto L_0x01ce
            boolean r0 = r14.A0f
            if (r0 == 0) goto L_0x01a3
            int r1 = r14.A0C
            r0 = 0
            if (r1 <= 0) goto L_0x01a0
            r0 = 1
        L_0x01a0:
            if (r0 != 0) goto L_0x01a3
            r6 = 1
        L_0x01a3:
            if (r6 != 0) goto L_0x01df
            X.1dz r0 = r5.A07
            boolean r0 = r0.A04(r14)
            if (r0 != 0) goto L_0x01df
            java.lang.String r1 = r14.A0U
            if (r1 == 0) goto L_0x01bf
            X.1de r0 = r5.A0Q
            androidx.fragment.app.Fragment r1 = r0.A00(r1)
            if (r1 == 0) goto L_0x01bf
            boolean r0 = r1.A0h
            if (r0 == 0) goto L_0x01bf
            r14.A0M = r1
        L_0x01bf:
            java.util.concurrent.ConcurrentHashMap r0 = r5.A0C
            java.lang.Object r0 = r0.get(r14)
            if (r0 == 0) goto L_0x01d7
            X.2NQ r0 = androidx.fragment.app.Fragment.A01(r14)
            r0.A03 = r3
            r3 = 1
        L_0x01ce:
            if (r3 >= 0) goto L_0x0047
            X.1dz r0 = r5.A07
            r4.A0H(r0)
            goto L_0x0047
        L_0x01d7:
            X.0qR r1 = r5.A06
            X.1dz r0 = r5.A07
            r4.A0G(r1, r0)
            goto L_0x01ce
        L_0x01df:
            androidx.fragment.app.Fragment r9 = r4.A03()
            X.1de r0 = r5.A0Q
            java.lang.String r1 = r9.A0V
            java.util.HashMap r0 = r0.A01
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x01bf
            r0 = 2
            A0I(r0)
            X.1de r8 = r5.A0Q
            androidx.fragment.app.Fragment r7 = r4.A03()
            java.util.HashMap r0 = r8.A01
            java.util.Collection r0 = r0.values()
            java.util.Iterator r11 = r0.iterator()
        L_0x0203:
            boolean r0 = r11.hasNext()
            r10 = 0
            if (r0 == 0) goto L_0x0225
            java.lang.Object r0 = r11.next()
            X.09q r0 = (X.C012909q) r0
            if (r0 == 0) goto L_0x0203
            androidx.fragment.app.Fragment r6 = r0.A03()
            java.lang.String r1 = r7.A0V
            java.lang.String r0 = r6.A0U
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0203
            r6.A0M = r7
            r6.A0U = r10
            goto L_0x0203
        L_0x0225:
            java.util.HashMap r1 = r8.A01
            java.lang.String r0 = r7.A0V
            r1.put(r0, r10)
            java.lang.String r0 = r7.A0U
            if (r0 == 0) goto L_0x0236
            androidx.fragment.app.Fragment r0 = r8.A00(r0)
            r7.A0M = r0
        L_0x0236:
            r5.A0q(r9)
            goto L_0x01bf
        L_0x023a:
            X.2NQ r0 = androidx.fragment.app.Fragment.A01(r14)
            r0.A03 = r3
            goto L_0x0194
        L_0x0242:
            android.animation.Animator r1 = r1.A00
            X.2NQ r0 = androidx.fragment.app.Fragment.A01(r14)
            r0.A04 = r1
            X.EgJ r11 = new X.EgJ
            r16 = r8
            r11.<init>(r12, r13, r14, r15, r16)
            r1.addListener(r11)
            android.view.View r0 = r14.A0I
            r1.setTarget(r0)
            r1.start()
            goto L_0x0182
        L_0x025e:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r14)
            r1.append(r10)
            java.lang.String r0 = r14.A0U
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x027e:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r14)
            r1.append(r10)
            androidx.fragment.app.Fragment r0 = r14.A0M
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x029e:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x029e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13060qW.A0s(androidx.fragment.app.Fragment, int):void");
    }

    public void A0t(Fragment fragment, AnonymousClass1XL r5) {
        if (!fragment.equals(this.A0Q.A00(fragment.A0V)) || !(fragment.A0N == null || fragment.A0P == this)) {
            throw new IllegalArgumentException("Fragment " + fragment + " is not an active fragment of FragmentManager " + this);
        }
        fragment.A02 = r5;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: X.1dL} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: androidx.fragment.app.Fragment} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: androidx.fragment.app.Fragment} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0v(X.C13020qR r6, X.C27711dd r7, androidx.fragment.app.Fragment r8) {
        /*
            r5 = this;
            X.0qR r0 = r5.A06
            if (r0 != 0) goto L_0x007f
            r5.A06 = r6
            r5.A05 = r7
            r5.A03 = r8
            if (r8 == 0) goto L_0x000f
            A09(r5)
        L_0x000f:
            boolean r0 = r6 instanceof X.C27531dL
            if (r0 == 0) goto L_0x0037
            r0 = r6
            X.1dL r0 = (X.C27531dL) r0
            X.1dW r4 = r0.AwJ()
            r5.A02 = r4
            if (r8 == 0) goto L_0x001f
            r0 = r8
        L_0x001f:
            X.1dh r3 = r5.A0N
            X.0qM r2 = r0.AsK()
            X.1XL r1 = r2.A05()
            X.1XL r0 = X.AnonymousClass1XL.DESTROYED
            if (r1 == r0) goto L_0x0037
            androidx.activity.OnBackPressedDispatcher$LifecycleOnBackPressedCancellable r1 = new androidx.activity.OnBackPressedDispatcher$LifecycleOnBackPressedCancellable
            r1.<init>(r4, r2, r3)
            java.util.concurrent.CopyOnWriteArrayList r0 = r3.A00
            r0.add(r1)
        L_0x0037:
            if (r8 == 0) goto L_0x005a
            X.0qW r0 = r8.A0P
            X.1dz r3 = r0.A07
            java.util.HashMap r1 = r3.A01
            java.lang.String r0 = r8.A0V
            java.lang.Object r2 = r1.get(r0)
            X.1dz r2 = (X.C27931dz) r2
            if (r2 != 0) goto L_0x0057
            X.1dz r2 = new X.1dz
            boolean r0 = r3.A04
            r2.<init>(r0)
            java.util.HashMap r1 = r3.A01
            java.lang.String r0 = r8.A0V
            r1.put(r0, r2)
        L_0x0057:
            r5.A07 = r2
            return
        L_0x005a:
            boolean r0 = r6 instanceof X.AnonymousClass0n8
            if (r0 == 0) goto L_0x0076
            X.0n8 r6 = (X.AnonymousClass0n8) r6
            X.1dx r2 = r6.B9I()
            X.1dy r1 = new X.1dy
            X.1e2 r0 = X.C27931dz.A05
            r1.<init>(r2, r0)
            java.lang.Class<X.1dz> r0 = X.C27931dz.class
            X.1e0 r0 = r1.A00(r0)
            X.1dz r0 = (X.C27931dz) r0
            r5.A07 = r0
            return
        L_0x0076:
            X.1dz r1 = new X.1dz
            r0 = 0
            r1.<init>(r0)
            r5.A07 = r1
            return
        L_0x007f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Already attached"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13060qW.A0v(X.0qR, X.1dd, androidx.fragment.app.Fragment):void");
    }

    public void A0w(C29461gS r4, boolean z) {
        if (!z) {
            if (this.A06 == null) {
                if (this.A0D) {
                    throw new IllegalStateException("FragmentManager has been destroyed");
                }
                throw new IllegalStateException("FragmentManager has not been attached to a host.");
            } else if (A13()) {
                throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
            }
        }
        synchronized (this.A0R) {
            if (this.A06 != null) {
                this.A0R.add(r4);
                A0V();
            } else if (!z) {
                throw new IllegalStateException("Activity has been destroyed");
            }
        }
    }

    public void A0x(C29461gS r3, boolean z) {
        if (!z || (this.A06 != null && !this.A0D)) {
            A0H(z);
            if (r3.Aap(this.A0M, this.A0L)) {
                this.A00 = true;
                try {
                    A0F(this.A0M, this.A0L);
                } finally {
                    A02();
                }
            }
            A09(this);
            if (this.A0E) {
                this.A0E = false;
                A05();
            }
            this.A0Q.A01.values().removeAll(Collections.singleton(null));
        }
    }

    public void A0y(String str, int i) {
        A0w(new AnonymousClass6ZC(this, str, -1, i), false);
    }

    public void A0z(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        String A0J2 = AnonymousClass08S.A0J(str, "    ");
        C27721de r4 = this.A0Q;
        String A0J3 = AnonymousClass08S.A0J(str, "    ");
        if (!r4.A01.isEmpty()) {
            printWriter.print(str);
            printWriter.print("Active Fragments:");
            for (C012909q r0 : r4.A01.values()) {
                printWriter.print(str);
                if (r0 != null) {
                    Fragment A032 = r0.A03();
                    printWriter.println(A032);
                    A032.A1c(A0J3, fileDescriptor, printWriter, strArr);
                } else {
                    printWriter.println("null");
                }
            }
        }
        int size3 = r4.A00.size();
        if (size3 > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i = 0; i < size3; i++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(((Fragment) r4.A00.get(i)).toString());
            }
        }
        ArrayList arrayList = this.A0J;
        if (arrayList != null && (size2 = arrayList.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i2 = 0; i2 < size2; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.A0J.get(i2)).toString());
            }
        }
        ArrayList arrayList2 = this.A09;
        if (arrayList2 != null && (size = arrayList2.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i3 = 0; i3 < size; i3++) {
                C16280wn r1 = (C16280wn) this.A09.get(i3);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(r1.toString());
                r1.A0O(A0J2, printWriter, true);
            }
        }
        printWriter.print(str);
        printWriter.println(AnonymousClass08S.A09("Back Stack Index: ", this.A0S.get()));
        synchronized (this.A0R) {
            int size4 = this.A0R.size();
            if (size4 > 0) {
                printWriter.print(str);
                printWriter.println("Pending Actions:");
                for (int i4 = 0; i4 < size4; i4++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i4);
                    printWriter.print(": ");
                    printWriter.println((C29461gS) this.A0R.get(i4));
                }
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.A06);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.A05);
        if (this.A03 != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.A03);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.A01);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.A0G);
        printWriter.print(" mStopped=");
        printWriter.print(this.A0H);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.A0D);
        if (this.A0F) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.A0F);
        }
    }

    public void A10(boolean z) {
        for (Fragment fragment : this.A0Q.A01()) {
            if (fragment != null) {
                fragment.A0O.A10(z);
            }
        }
    }

    public void A11(boolean z) {
        for (Fragment fragment : this.A0Q.A01()) {
            if (fragment != null) {
                fragment.A0O.A11(z);
            }
        }
    }

    public boolean A13() {
        if (this.A0G || this.A0H) {
            return true;
        }
        return false;
    }

    public boolean A15(Menu menu) {
        boolean z = false;
        if (this.A01 >= 1) {
            for (Fragment fragment : this.A0Q.A01()) {
                if (fragment != null) {
                    boolean z2 = false;
                    if (!fragment.A0b) {
                        if (fragment.A0a && fragment.A0e) {
                            z2 = true;
                        }
                        z2 |= fragment.A0O.A15(menu);
                    }
                    if (z2) {
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    public boolean A16(Menu menu, MenuInflater menuInflater) {
        if (this.A01 < 1) {
            return false;
        }
        ArrayList arrayList = null;
        boolean z = false;
        for (Fragment fragment : this.A0Q.A01()) {
            if (fragment != null && fragment.A1b(menu, menuInflater)) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(fragment);
                z = true;
            }
        }
        if (this.A0J != null) {
            for (int i = 0; i < this.A0J.size(); i++) {
                Fragment fragment2 = (Fragment) this.A0J.get(i);
                if (arrayList != null) {
                    arrayList.contains(fragment2);
                }
            }
        }
        this.A0J = arrayList;
        return z;
    }

    public boolean A17(MenuItem menuItem) {
        boolean z;
        if (this.A01 >= 1) {
            for (Fragment fragment : this.A0Q.A01()) {
                if (fragment != null) {
                    if (fragment.A0b || (!fragment.A1g(menuItem) && !fragment.A0O.A17(menuItem))) {
                        z = false;
                    } else {
                        z = true;
                    }
                    if (z) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean A18(MenuItem menuItem) {
        boolean z;
        if (this.A01 >= 1) {
            for (Fragment fragment : this.A0Q.A01()) {
                if (fragment != null) {
                    if (fragment.A0b || ((!fragment.A0a || !fragment.A0e || !fragment.A1f(menuItem)) && !fragment.A0O.A18(menuItem))) {
                        z = false;
                    } else {
                        z = true;
                    }
                    if (z) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean A1A(ArrayList arrayList, ArrayList arrayList2, String str, int i, int i2) {
        int size;
        ArrayList arrayList3 = this.A09;
        if (arrayList3 != null) {
            if (str == null && i < 0 && (i2 & 1) == 0) {
                int size2 = arrayList3.size() - 1;
                if (size2 >= 0) {
                    arrayList.add(this.A09.remove(size2));
                    arrayList2.add(true);
                }
            } else {
                if (str != null || i >= 0) {
                    size = arrayList3.size() - 1;
                    while (size >= 0) {
                        C16280wn r1 = (C16280wn) this.A09.get(size);
                        if ((str != null && str.equals(r1.getName())) || (i >= 0 && i == r1.A00)) {
                            break;
                        }
                        size--;
                    }
                    if (size >= 0) {
                        if ((i2 & 1) != 0) {
                            while (true) {
                                size--;
                                if (size < 0) {
                                    break;
                                }
                                C16280wn r12 = (C16280wn) this.A09.get(size);
                                if ((str == null || !str.equals(r12.getName())) && (i < 0 || i != r12.A00)) {
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    size = -1;
                }
                if (size != this.A09.size() - 1) {
                    for (int size3 = this.A09.size() - 1; size3 > size; size3--) {
                        arrayList.add(this.A09.remove(size3));
                        arrayList2.add(true);
                    }
                }
            }
            return true;
        }
        return false;
    }

    public String toString() {
        int identityHashCode;
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        Fragment fragment = this.A03;
        if (fragment != null) {
            sb.append(fragment.getClass().getSimpleName());
            sb.append("{");
            identityHashCode = System.identityHashCode(fragment);
        } else {
            C13020qR r1 = this.A06;
            sb.append(r1.getClass().getSimpleName());
            sb.append("{");
            identityHashCode = System.identityHashCode(r1);
        }
        sb.append(Integer.toHexString(identityHashCode));
        sb.append("}");
        sb.append("}}");
        return sb.toString();
    }

    private void A08(Fragment fragment) {
        int i;
        ViewGroup A002 = A00(fragment);
        if (A002 != null) {
            if (A002.getTag(2131301370) == null) {
                A002.setTag(2131301370, fragment);
            }
            Fragment fragment2 = (Fragment) A002.getTag(2131301370);
            AnonymousClass2NQ r0 = fragment.A0K;
            if (r0 == null) {
                i = 0;
            } else {
                i = r0.A01;
            }
            fragment2.A1J(i);
        }
    }

    public static void A0B(C13060qW r2, Fragment fragment) {
        fragment.A1D();
        r2.A0P.A0D(fragment, false);
        fragment.A0J = null;
        fragment.A0I = null;
        fragment.A0Q = null;
        fragment.A04.A09(null);
        fragment.A0d = false;
    }

    public static void A0D(C13060qW r7, RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new AnonymousClass8Q2());
        C13020qR r4 = r7.A06;
        if (r4 != null) {
            try {
                r4.A07("  ", null, printWriter, new String[0]);
            } catch (Exception e) {
                Log.e("FragmentManager", "Failed dumping state", e);
            }
        } else {
            r7.A0z("  ", null, printWriter, new String[0]);
        }
        throw runtimeException;
    }

    private void A0F(ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList.isEmpty()) {
            return;
        }
        if (arrayList.size() == arrayList2.size()) {
            A0E(arrayList, arrayList2);
            int size = arrayList.size();
            int i = 0;
            int i2 = 0;
            while (i < size) {
                if (!((C16280wn) arrayList.get(i)).A0F) {
                    if (i2 != i) {
                        A0G(arrayList, arrayList2, i2, i);
                    }
                    i2 = i + 1;
                    if (((Boolean) arrayList2.get(i)).booleanValue()) {
                        while (i2 < size && ((Boolean) arrayList2.get(i2)).booleanValue() && !((C16280wn) arrayList.get(i2)).A0F) {
                            i2++;
                        }
                    }
                    A0G(arrayList, arrayList2, i, i2);
                    i = i2 - 1;
                }
                i++;
            }
            if (i2 != size) {
                A0G(arrayList, arrayList2, i2, size);
                return;
            }
            return;
        }
        throw new IllegalStateException("Internal error with the back stack records");
    }

    public Parcelable A0M() {
        ArrayList arrayList;
        int size;
        A04();
        A03();
        A12(true);
        this.A0G = true;
        C27721de r1 = this.A0Q;
        ArrayList arrayList2 = new ArrayList(r1.A01.size());
        for (C012909q r0 : r1.A01.values()) {
            if (r0 != null) {
                arrayList2.add(r0.A04());
                A0I(2);
            }
        }
        BackStackState[] backStackStateArr = null;
        if (arrayList2.isEmpty()) {
            A0I(2);
            return null;
        }
        C27721de r12 = this.A0Q;
        synchronized (r12.A00) {
            if (r12.A00.isEmpty()) {
                arrayList = null;
            } else {
                arrayList = new ArrayList(r12.A00.size());
                Iterator it = r12.A00.iterator();
                while (it.hasNext()) {
                    arrayList.add(((Fragment) it.next()).A0V);
                    A0I(2);
                }
            }
        }
        ArrayList arrayList3 = this.A09;
        if (arrayList3 != null && (size = arrayList3.size()) > 0) {
            backStackStateArr = new BackStackState[size];
            for (int i = 0; i < size; i++) {
                backStackStateArr[i] = new BackStackState((C16280wn) this.A09.get(i));
                if (A0I(2)) {
                    this.A09.get(i);
                }
            }
        }
        FragmentManagerState fragmentManagerState = new FragmentManagerState();
        fragmentManagerState.A02 = arrayList2;
        fragmentManagerState.A03 = arrayList;
        fragmentManagerState.A04 = backStackStateArr;
        fragmentManagerState.A00 = this.A0S.get();
        Fragment fragment = this.A04;
        if (fragment != null) {
            fragmentManagerState.A01 = fragment.A0V;
        }
        return fragmentManagerState;
    }

    public Fragment A0P(Bundle bundle, String str) {
        String string = bundle.getString(str);
        if (string == null) {
            return null;
        }
        Fragment A002 = this.A0Q.A00(string);
        if (A002 == null) {
            A0D(this, new IllegalStateException(AnonymousClass08S.A0S("Fragment no longer exists for key ", str, ": unique id ", string)));
        }
        return A002;
    }

    public void A0i(Fragment fragment) {
        boolean z;
        if (A13()) {
            A0I(2);
            return;
        }
        C27931dz r2 = this.A07;
        if (r2.A02.containsKey(fragment.A0V)) {
            z = false;
        } else {
            r2.A02.put(fragment.A0V, fragment);
            z = true;
        }
        if (z) {
            A0I(2);
        }
    }

    public void A0q(Fragment fragment) {
        if (A13()) {
            A0I(2);
            return;
        }
        boolean z = false;
        if (this.A07.A02.remove(fragment.A0V) != null) {
            z = true;
        }
        if (z) {
            A0I(2);
        }
    }

    public void A0u(Fragment fragment, boolean z) {
        ViewGroup A002 = A00(fragment);
        if (A002 != null && (A002 instanceof C29031fl)) {
            ((C29031fl) A002).A00 = !z;
        }
    }

    public void A12(boolean z) {
        boolean z2;
        A0H(z);
        while (true) {
            ArrayList arrayList = this.A0M;
            ArrayList arrayList2 = this.A0L;
            synchronized (this.A0R) {
                if (this.A0R.isEmpty()) {
                    z2 = false;
                } else {
                    int size = this.A0R.size();
                    z2 = false;
                    for (int i = 0; i < size; i++) {
                        z2 |= ((C29461gS) this.A0R.get(i)).Aap(arrayList, arrayList2);
                    }
                    this.A0R.clear();
                    AnonymousClass00S.A02(this.A06.A02, this.A08);
                }
            }
            if (z2) {
                this.A00 = true;
                try {
                    A0F(this.A0M, this.A0L);
                } finally {
                    A02();
                }
            } else {
                A09(this);
                if (this.A0E) {
                    this.A0E = false;
                    A05();
                }
                this.A0Q.A01.values().removeAll(Collections.singleton(null));
                return;
            }
        }
        while (true) {
        }
    }
}
