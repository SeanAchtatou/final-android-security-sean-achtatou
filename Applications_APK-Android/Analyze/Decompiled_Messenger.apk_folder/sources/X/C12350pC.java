package X;

import com.facebook.common.util.StringLocaleUtil;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0pC  reason: invalid class name and case insensitive filesystem */
public abstract class C12350pC {
    public static final Set A00 = new HashSet(Collections.singletonList("FbChromeFragment"));

    public int A05() {
        return ((C12330pA) this).A00;
    }

    public C97964mI A06() {
        return ((C12330pA) this).A01;
    }

    public C860146i A07() {
        return ((C12330pA) this).A02;
    }

    public Class A08() {
        return ((C12330pA) this).A0F;
    }

    public Integer A09() {
        return ((C12330pA) this).A04;
    }

    public Long A0A() {
        return ((C12330pA) this).A05;
    }

    public String A0B() {
        return ((C12330pA) this).A07;
    }

    public String A0C() {
        return ((C12330pA) this).A08;
    }

    public String A0D() {
        return ((C12330pA) this).A09;
    }

    public String A0E() {
        return ((C12330pA) this).A0G;
    }

    public String A0F() {
        return ((C12330pA) this).A0A;
    }

    public String A0G() {
        return ((C12330pA) this).A0H;
    }

    public String A0H() {
        return ((C12330pA) this).A0B;
    }

    public String A0I() {
        return ((C12330pA) this).A0C;
    }

    public Map A0J() {
        return ((C12330pA) this).A0D;
    }

    public static String A04(long j) {
        return StringLocaleUtil.A00("%s.%s", Long.valueOf(j / 1000), Long.valueOf(j % 1000));
    }
}
