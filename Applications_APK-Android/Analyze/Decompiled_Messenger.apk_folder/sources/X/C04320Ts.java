package X;

import java.util.Map;

/* renamed from: X.0Ts  reason: invalid class name and case insensitive filesystem */
public final class C04320Ts extends C06750c1 {
    public final C06110ar A00;
    public final Map A01 = AnonymousClass0TG.A04();

    public C04320Ts(int i) {
        super(C06980cP.A07, i);
        C06110ar r2 = new C06110ar();
        this.A00 = r2;
        r2.A03("peer://msg_notification_unread_count/clear_thread/{thread_id}", 0);
        this.A00.A03("peer://msg_notification_unread_count/thread/{thread_id}", 1);
    }
}
