package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.zero.common.ZeroToken;
import com.facebook.zero.common.util.CarrierAndSimMccMnc;
import com.facebook.zero.protocol.params.FetchZeroTokenRequestParams;
import com.google.common.util.concurrent.ListenableFuture;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1eR  reason: invalid class name and case insensitive filesystem */
public final class C28211eR {
    private static volatile C28211eR A03;
    public AnonymousClass0UN A00;
    public final Map A01 = new HashMap();
    public final Set A02 = new HashSet();

    public void A04(ZeroToken zeroToken, AnonymousClass157 r6) {
        HashSet hashSet;
        synchronized (this) {
            hashSet = new HashSet(this.A02);
        }
        Iterator it = this.A02.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            C28201eQ r1 = (C28201eQ) it.next();
            if (r1 instanceof C28191eP) {
                r1.BYw(zeroToken, r6);
                hashSet.remove(r1);
                break;
            }
        }
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            ((C28201eQ) it2.next()).BYw(zeroToken, r6);
        }
    }

    public synchronized void A05(C28201eQ r2) {
        this.A02.add(r2);
    }

    public void A07(Throwable th, AnonymousClass157 r4) {
        HashSet hashSet;
        synchronized (this) {
            hashSet = new HashSet(this.A02);
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            ((C28201eQ) it.next()).BYv(th, r4);
        }
    }

    public static final C28211eR A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C28211eR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C28211eR(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static void A01(C28211eR r6, String str, AnonymousClass157 r8, String str2) {
        boolean z = true;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, r6.A00)).A01("iorg_campaign_api_fetch"), AnonymousClass1Y3.A2M);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("carrier_id", ((AnonymousClass155) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AC0, r6.A00)).A0A(r8));
            USLEBaseShape0S0000000 A0K = uSLEBaseShape0S0000000.A0K(str);
            A0K.A0D("fast_hash", ((AnonymousClass155) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AC0, r6.A00)).A0C(r8));
            if (r8 != AnonymousClass157.DIALTONE) {
                z = false;
            }
            A0K.A08("is_dialtone", Boolean.valueOf(z));
            A0K.A08("is_dual_token", Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AOJ, r6.A00)).Aem(286714836884574L)));
            A0K.A08("is_graphql", Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AOJ, r6.A00)).Aem(286714836819037L)));
            A0K.A0D("reason", str2);
            A0K.A0D("token_hash", ((AnonymousClass155) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AC0, r6.A00)).A0D(r8));
            A0K.A0D("eligibility_hash", ((AnonymousClass155) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AC0, r6.A00)).A0B(r8));
            A0K.A06();
        }
    }

    public FetchZeroTokenRequestParams A02(AnonymousClass157 r13, String str) {
        AnonymousClass1Y8 r3;
        String A0D = ((AnonymousClass155) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AC0, this.A00)).A0D(r13);
        String str2 = str;
        boolean equals = str.equals(AnonymousClass24B.$const$string(AnonymousClass1Y3.A7p));
        String str3 = BuildConfig.FLAVOR;
        if (equals) {
            if (r13 == AnonymousClass157.DIALTONE) {
                r3 = C05730aE.A0Q;
            } else {
                r3 = C05730aE.A0R;
            }
            A0D = ((FbSharedPreferences) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B6q, this.A00)).B4F(r3, str3);
        }
        if (((C05720aD) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Az1, this.A00)).A07("auto_new_res_eligible")) {
            str3 = ((FbSharedPreferences) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B6q, this.A00)).B4F(C05730aE.A0f, str3);
        }
        int i = AnonymousClass1Y3.AHR;
        CarrierAndSimMccMnc A022 = ((C47852Yi) AnonymousClass1XX.A02(2, i, this.A00)).A02();
        String A032 = ((C47852Yi) AnonymousClass1XX.A02(2, i, this.A00)).A03();
        boolean z = false;
        if (r13 == AnonymousClass157.DIALTONE) {
            z = true;
        }
        return new FetchZeroTokenRequestParams(A022, A032, z, !((AnonymousClass155) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AC0, this.A00)).A00.BBh(AnonymousClass155.A00(r13).A09("backup_rewrite_rules")), A0D, str2, str3);
    }

    public void A03() {
        for (ListenableFuture listenableFuture : this.A01.values()) {
            if (listenableFuture != null) {
                listenableFuture.cancel(true);
            }
        }
        this.A01.clear();
    }

    public final void A06(AnonymousClass157 r5, String str) {
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AcD, this.A00)).Ab9(582) == TriState.YES && !this.A01.containsKey(r5)) {
            A01(this, "fetch_attempt", r5, str);
            ((AnonymousClass155) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AC0, this.A00)).A0K(r5);
            this.A01.put(r5, ((C48262a4) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BEv, this.A00)).A03(A02(r5, str), new C48252a3(this, r5, str)));
        }
    }

    private C28211eR(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(10, r3);
    }
}
