package X;

import com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0rn  reason: invalid class name and case insensitive filesystem */
public final class C13640rn extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static volatile AndroidAsyncExecutorFactory A02;
    private static volatile AndroidAsyncExecutorFactory A03;

    public static final AndroidAsyncExecutorFactory A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AndroidAsyncExecutorFactory(AnonymousClass0UX.A0f(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final AndroidAsyncExecutorFactory A01(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AndroidAsyncExecutorFactory(AnonymousClass0UX.A0j(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }
}
