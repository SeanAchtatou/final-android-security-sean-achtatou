package X;

import android.os.StrictMode;
import java.util.UUID;

/* renamed from: X.15g  reason: invalid class name and case insensitive filesystem */
public final class C188215g {
    public static UUID A00() {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return UUID.randomUUID();
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
}
