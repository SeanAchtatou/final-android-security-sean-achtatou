package X;

import android.os.Process;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eR  reason: invalid class name and case insensitive filesystem */
public final class C07950eR extends C07880eK {
    private static volatile C07950eR A03;
    public int A00 = -1;
    public AnonymousClass0UN A01;
    public Integer A02 = null;

    public static final C07950eR A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C07950eR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C07950eR(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static void A01(C07950eR r5) {
        if (r5.A00 == -1) {
            r5.A00 = C70903bT.A00("(ComponentLayout)");
        }
        try {
            Process.getThreadPriority(r5.A00);
        } catch (IllegalArgumentException e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r5.A01)).softReport("LithoLayoutThreadBoost", "Layout thread unexpectedly died", e);
            r5.A00 = -1;
        }
    }

    private C07950eR(AnonymousClass1XY r5) {
        AnonymousClass0UN r3 = new AnonymousClass0UN(2, r5);
        this.A01 = r3;
        A06((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r3), (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r3));
    }
}
