package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import java.util.List;

/* renamed from: X.20W  reason: invalid class name */
public final class AnonymousClass20W implements AnonymousClass4JN {
    public final /* synthetic */ C26374Cww A00;

    public AnonymousClass20W(C26374Cww cww) {
        this.A00 = cww;
    }

    public void BZb(List list) {
        int i;
        int i2;
        if (((C73473g8) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASj, this.A00.A00)).A04()) {
            i = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A00.A00)).AqN(AnonymousClass129.A05, 0) % list.size();
            i2 = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A00.A00)).AqN(AnonymousClass129.A04, 0);
        } else {
            i = 0;
            i2 = 0;
        }
        ((Cy0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A7b, this.A00.A00)).A02(((C26534D0u) list.get(i)).A05, i2);
        this.A00.A2S();
    }
}
