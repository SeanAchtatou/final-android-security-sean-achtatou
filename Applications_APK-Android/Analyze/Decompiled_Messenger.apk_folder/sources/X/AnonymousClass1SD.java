package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1SD  reason: invalid class name */
public final class AnonymousClass1SD extends C23441Pn implements AnonymousClass1OQ {
    private static volatile AnonymousClass1SD A02;
    public final AnonymousClass0Ud A00;
    public final QuickPerformanceLogger A01;

    public static final AnonymousClass1SD A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass1SD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass1SD(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private AnonymousClass1SD(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0Ud.A00(r2);
        this.A01 = AnonymousClass0ZD.A03(r2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void BgO(AnonymousClass1Q0 r6, CallerContext callerContext, int i, boolean z, boolean z2) {
        if (AnonymousClass1QP.A01()) {
            int identityHashCode = System.identityHashCode(r6);
            this.A01.markerStart(1179651, identityHashCode);
            if (this.A01.isMarkerOn(1179651, identityHashCode)) {
                String str = this.A00.A05;
                if (str == null) {
                    str = "UNKNOWN";
                }
                C21061Ew withMarker = this.A01.withMarker(1179651, identityHashCode);
                AnonymousClass55L.A00(withMarker, str, r6, callerContext, z);
                withMarker.A08("content_length", Integer.toString(i));
                withMarker.A08("is_cancellation_requested", Boolean.toString(z2));
                withMarker.BK9();
            }
            this.A01.markerEnd(1179651, identityHashCode, (short) 2);
        }
    }
}
