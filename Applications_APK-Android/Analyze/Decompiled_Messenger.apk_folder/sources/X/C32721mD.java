package X;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import java.util.HashMap;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1mD  reason: invalid class name and case insensitive filesystem */
public final class C32721mD implements AnonymousClass1YQ {
    private static volatile C32721mD A03;
    public AnonymousClass0UN A00;
    public HashMap A01 = null;
    private final C001500z A02;

    /* JADX INFO: finally extract failed */
    public void A06(String str, C138436dA r15) {
        String str2 = str;
        if (!Platform.stringIsNullOrEmpty(str)) {
            String A05 = ((C50552eD) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ADL, this.A00)).A05(str);
            long now = ((C32761mI) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AnC, this.A00)).now();
            AnonymousClass2OA r4 = (AnonymousClass2OA) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1e, this.A00);
            ContentValues contentValues = new ContentValues();
            contentValues.put(AnonymousClass2O6.A02.A00, Long.valueOf(now));
            contentValues.put(AnonymousClass2O6.A01.A00, str);
            C06140av A032 = C06160ax.A03(AnonymousClass2O6.A00.A00, A05);
            C007406x.A01(r4.A00.A06(), -1584403271);
            try {
                if (r4.A00.A06().update("block_table", contentValues, A032.A02(), A032.A04()) == 0) {
                    contentValues.put(AnonymousClass2O6.A00.A00, A05);
                    SQLiteDatabase A06 = r4.A00.A06();
                    C007406x.A00(2076668679);
                    A06.insert("block_table", null, contentValues);
                    C007406x.A00(-2086875434);
                }
                r4.A00.A06().setTransactionSuccessful();
                C007406x.A02(r4.A00.A06(), -1507859642);
                A02(now, str2, A05, true);
                int i = AnonymousClass1Y3.BEM;
                ((C57362rr) AnonymousClass1XX.A02(0, i, this.A00)).A02();
                ((C57362rr) AnonymousClass1XX.A02(0, i, this.A00)).A03();
                ((C21451Ha) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AFw, this.A00)).A0D(r15, true);
            } catch (Throwable th) {
                C007406x.A02(r4.A00.A06(), -1807387150);
                throw th;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void A07(String str, C138436dA r11) {
        String str2 = str;
        if (!Platform.stringIsNullOrEmpty(str)) {
            String A05 = ((C50552eD) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ADL, this.A00)).A05(str);
            AnonymousClass2OA r4 = (AnonymousClass2OA) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1e, this.A00);
            C06140av A032 = C06160ax.A03(AnonymousClass2O6.A00.A00, A05);
            C007406x.A01(r4.A00.A06(), -342791400);
            try {
                r4.A00.A06().delete("block_table", A032.A02(), A032.A04());
                r4.A00.A06().setTransactionSuccessful();
                C007406x.A02(r4.A00.A06(), -113822221);
                A02(0, str2, A05, false);
                int i = AnonymousClass1Y3.BEM;
                ((C57362rr) AnonymousClass1XX.A02(0, i, this.A00)).A03();
                String $const$string = AnonymousClass24B.$const$string(AnonymousClass1Y3.A4z);
                Intent intent = new Intent();
                intent.setAction(C06680bu.A11);
                intent.putExtra("calling_class", $const$string);
                C189216c.A04((C189216c) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B7s, ((C57362rr) AnonymousClass1XX.A02(0, i, this.A00)).A00), intent);
                ((C21451Ha) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AFw, this.A00)).A0D(r11, false);
            } catch (Throwable th) {
                C007406x.A02(r4.A00.A06(), -1886280339);
                throw th;
            }
        }
    }

    public String getSimpleName() {
        return "SmsBlockThreadManager";
    }

    public static final C32721mD A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C32721mD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C32721mD(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private void A01() {
        if (this.A01 == null && C001500z.A07.equals(this.A02)) {
            List<C187188mg> A05 = A05();
            this.A01 = new HashMap();
            for (C187188mg A032 : A05) {
                A03(A032);
            }
        }
    }

    private void A03(C187188mg r3) {
        Preconditions.checkNotNull(this.A01);
        this.A01.put(r3.A03, r3);
        String str = r3.A03;
        String str2 = r3.A02;
        if (!str.equals(str2)) {
            this.A01.put(str2, r3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005f, code lost:
        if (r3 != null) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0064, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A05() {
        /*
            r11 = this;
            int r2 = X.AnonymousClass1Y3.A1e
            X.0UN r1 = r11.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2OA r0 = (X.AnonymousClass2OA) r0
            X.1ua r0 = r0.A00
            android.database.sqlite.SQLiteDatabase r0 = r0.A06()
            java.lang.String r1 = "block_table"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r3 = r0.query(r1, r2, r3, r4, r5, r6, r7)
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x005c }
            r4.<init>()     // Catch:{ all -> 0x005c }
        L_0x0022:
            boolean r0 = r3.moveToNext()     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0058
            X.0W6 r0 = X.AnonymousClass2O6.A00     // Catch:{ all -> 0x005c }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x005c }
            java.lang.String r8 = X.C52412j3.A02(r3, r0)     // Catch:{ all -> 0x005c }
            X.0W6 r0 = X.AnonymousClass2O6.A01     // Catch:{ all -> 0x005c }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x005c }
            java.lang.String r7 = X.C52412j3.A02(r3, r0)     // Catch:{ all -> 0x005c }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AaZ     // Catch:{ all -> 0x005c }
            X.0UN r0 = r11.A00     // Catch:{ all -> 0x005c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x005c }
            X.2eE r0 = (X.C50562eE) r0     // Catch:{ all -> 0x005c }
            java.lang.String r6 = r0.A09(r8)     // Catch:{ all -> 0x005c }
            X.0W6 r0 = X.AnonymousClass2O6.A02     // Catch:{ all -> 0x005c }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x005c }
            long r9 = X.C52412j3.A01(r3, r0)     // Catch:{ all -> 0x005c }
            X.8mg r5 = new X.8mg     // Catch:{ all -> 0x005c }
            r5.<init>(r6, r7, r8, r9)     // Catch:{ all -> 0x005c }
            r4.add(r5)     // Catch:{ all -> 0x005c }
            goto L_0x0022
        L_0x0058:
            r3.close()
            return r4
        L_0x005c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            if (r3 == 0) goto L_0x0064
            r3.close()     // Catch:{ all -> 0x0064 }
        L_0x0064:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32721mD.A05():java.util.List");
    }

    public boolean A08(String str, boolean z) {
        if (!z) {
            A01();
        }
        if (this.A01 == null) {
            return false;
        }
        return this.A01.containsKey(((C50552eD) AnonymousClass1XX.A02(4, AnonymousClass1Y3.ADL, this.A00)).A05(str));
    }

    private C32721mD(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(7, r3);
        this.A02 = AnonymousClass0UU.A05(r3);
    }

    private void A02(long j, String str, String str2, boolean z) {
        A01();
        HashMap hashMap = this.A01;
        if (hashMap != null) {
            String str3 = str;
            String str4 = str2;
            if (z) {
                C187188mg r0 = (C187188mg) hashMap.get(str2);
                long j2 = j;
                if (r0 != null) {
                    A03(new C187188mg(r0.A01, r0.A03, r0.A02, j2));
                } else {
                    A03(new C187188mg(((C50562eE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AaZ, this.A00)).A09(str2), str3, str4, j2));
                }
            } else {
                Preconditions.checkNotNull(hashMap);
                this.A01.remove(str);
                if (!str.equals(str2)) {
                    this.A01.remove(str2);
                }
            }
        }
    }

    public ImmutableSet A04() {
        A01();
        HashMap hashMap = this.A01;
        if (hashMap == null) {
            return RegularImmutableSet.A05;
        }
        Preconditions.checkNotNull(hashMap);
        return ((C50542eC) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ASZ, this.A00)).A02(this.A01.keySet());
    }

    public void init() {
        int A032 = C000700l.A03(-895642334);
        A01();
        C000700l.A09(144292950, A032);
    }
}
