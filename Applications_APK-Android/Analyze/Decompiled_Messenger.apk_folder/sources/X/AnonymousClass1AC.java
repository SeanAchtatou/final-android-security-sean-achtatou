package X;

import android.os.HandlerThread;
import android.os.Looper;
import com.facebook.forker.Process;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1AC  reason: invalid class name */
public final class AnonymousClass1AC {
    public static volatile Looper A0R;
    public int A00;
    public C58142tE A01;
    public C16070wR A02;
    public C16070wR A03;
    public C16070wR A04;
    public AnonymousClass1AI A05;
    public List A06;
    public boolean A07;
    private C16070wR A08;
    public final C21811Iu A09 = new C21811Iu();
    public final AnonymousClass1RE A0A = new AnonymousClass1RE();
    public final C31551js A0B = C21851Iy.A00(new AnonymousClass11N(Looper.getMainLooper()));
    public final AnonymousClass1AG A0C;
    public final C157037Nu A0D;
    public final AnonymousClass1AF A0E;
    public final AnonymousClass1GA A0F;
    public final String A0G;
    public final Map A0H = new HashMap();
    public final AtomicBoolean A0I;
    public final boolean A0J;
    private final AnonymousClass1AE A0K = new AnonymousClass1AE(null);
    private final AnonymousClass16K A0L;
    private final AnonymousClass16K A0M;
    private final boolean A0N;
    private final boolean A0O;
    private final boolean A0P;
    public volatile boolean A0Q = false;

    private C402720y A01(C16070wR r8, String str, int i) {
        if (r8 != null) {
            if (str.equals(r8.A04)) {
                return new C402720y(r8, i);
            }
            List list = r8.A06;
            if (list != null && !list.isEmpty()) {
                int size = list.size();
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    C16070wR r1 = (C16070wR) list.get(i3);
                    C402720y A012 = A01(r1, str, i + i2);
                    if (A012 != null) {
                        return A012;
                    }
                    i2 += r1.A00;
                }
            }
        }
        return null;
    }

    public static String A03(int i) {
        if (i == -1) {
            return "none";
        }
        if (i == 0) {
            return "setRoot";
        }
        if (i == 1) {
            return "setRootAsync";
        }
        if (i == 2) {
            return "updateState";
        }
        if (i == 3) {
            return "updateStateAsync";
        }
        throw new IllegalStateException("Unknown source");
    }

    public static String A04(AnonymousClass1AC r3) {
        Integer num;
        String str;
        Integer num2;
        synchronized (r3) {
            if (r3.A0Q) {
                return "[Released Tree]";
            }
            StringBuilder sb = new StringBuilder();
            sb.append("tag: ");
            sb.append(r3.A0G);
            sb.append(", currentSection.size: ");
            C16070wR r1 = r3.A03;
            String str2 = null;
            if (r1 != null) {
                num = Integer.valueOf(r1.A00);
            } else {
                num = null;
            }
            sb.append(num);
            sb.append(", currentSection.name: ");
            C16070wR r12 = r3.A03;
            if (r12 != null) {
                str = r12.A0A;
            } else {
                str = null;
            }
            sb.append(str);
            sb.append(", nextSection.size: ");
            C16070wR r13 = r3.A04;
            if (r13 != null) {
                num2 = Integer.valueOf(r13.A00);
            } else {
                num2 = null;
            }
            sb.append(num2);
            sb.append(", nextSection.name: ");
            C16070wR r14 = r3.A04;
            if (r14 != null) {
                str2 = r14.A0A;
            }
            sb.append(str2);
            sb.append(", pendingChangeSets.size: ");
            sb.append(r3.A06.size());
            sb.append(", pendingStateUpdates.size: ");
            sb.append(r3.A05.A00.size());
            sb.append(", pendingNonLazyStateUpdates.size: ");
            sb.append(r3.A05.A01.size());
            sb.append("\n");
            String sb2 = sb.toString();
            return sb2;
        }
    }

    private synchronized void A09(C16070wR r5) {
        List list = r5.A06;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                A09((C16070wR) list.get(i));
            }
        }
    }

    public static synchronized void A0J(AnonymousClass1AC r3, String str, C61322yh r5, boolean z) {
        C16070wR r0;
        synchronized (r3) {
            if (!r3.A0Q) {
                if (r3.A03 == null && r3.A04 == null) {
                    throw new IllegalStateException("State set with no attached Section");
                }
                AnonymousClass1AI r2 = r3.A05;
                Map map = r2.A00;
                Object obj = (List) map.get(str);
                if (obj == null) {
                    obj = new ArrayList();
                    map.put(str, obj);
                }
                obj.add(r5);
                if (!z) {
                    Map map2 = r2.A01;
                    Object obj2 = (List) map2.get(str);
                    if (obj2 == null) {
                        obj2 = new ArrayList();
                        map2.put(str, obj2);
                    }
                    obj2.add(r5);
                }
                if (!z) {
                    if (r3.A07) {
                        int i = r3.A00 + 1;
                        r3.A00 = i;
                        if (i == 50) {
                            C09070gU.A01(AnonymousClass07B.A0C, "SectionTree:StateUpdatesFromInsideChangeSetCalculateExceedsThreshold", "Large number of state updates detected which indicates an infinite loop leading to unresponsive apps");
                        }
                    }
                    C16070wR r1 = r3.A04;
                    if (r1 == null) {
                        C16070wR r12 = r3.A03;
                        if (r12 != null) {
                            r0 = r12.A0T(false);
                        }
                        r0 = null;
                    } else {
                        if (r1 != null) {
                            r0 = r1.A0T(false);
                        }
                        r0 = null;
                    }
                    r3.A04 = r0;
                }
            }
        }
    }

    public void A0L() {
        C16070wR r0;
        synchronized (this) {
            if (!this.A0Q) {
                r0 = this.A03;
            } else {
                throw new IllegalStateException("Calling refresh on a released tree");
            }
        }
        if (r0 != null) {
            A07(r0);
        }
    }

    public void A0M(int i, int i2, int i3, int i4, int i5) {
        C16070wR r1;
        synchronized (this) {
            r1 = this.A03;
        }
        if (r1 != null) {
            A0A(r1, i, i2, i3, i4, i5);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0034, code lost:
        if (r5.A0N == false) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0036, code lost:
        if (r1 != false) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x003a, code lost:
        if (r5.A0D != null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x003c, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x003d, code lost:
        r5.A0M.A02(1, null, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0042, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0043, code lost:
        java.lang.Thread.currentThread().getStackTrace();
        r1 = new X.C157047Nv();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0052, code lost:
        if (r5.A0D != null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0054, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0055, code lost:
        A0E(r5, 0, null, null, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0058, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0059, code lost:
        java.lang.Thread.currentThread().getStackTrace();
        r1 = new X.C157047Nv();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0N(X.C16070wR r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = r5.A0Q     // Catch:{ all -> 0x006e }
            if (r0 != 0) goto L_0x0066
            X.0wR r0 = r5.A03     // Catch:{ all -> 0x006e }
            if (r0 == 0) goto L_0x0011
            int r1 = r0.A09     // Catch:{ all -> 0x006e }
            int r0 = r6.A09     // Catch:{ all -> 0x006e }
            if (r1 != r0) goto L_0x0011
        L_0x000f:
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            goto L_0x001c
        L_0x0011:
            X.0wR r0 = r5.A04     // Catch:{ all -> 0x006e }
            if (r0 == 0) goto L_0x001d
            int r1 = r0.A09     // Catch:{ all -> 0x006e }
            int r0 = r6.A09     // Catch:{ all -> 0x006e }
            if (r1 != r0) goto L_0x001d
            goto L_0x000f
        L_0x001c:
            return
        L_0x001d:
            r4 = 0
            if (r6 == 0) goto L_0x0021
            goto L_0x0023
        L_0x0021:
            r0 = 0
            goto L_0x0027
        L_0x0023:
            X.0wR r0 = r6.A0T(r4)     // Catch:{ all -> 0x006e }
        L_0x0027:
            r5.A04 = r0     // Catch:{ all -> 0x006e }
            X.0wR r0 = r5.A03     // Catch:{ all -> 0x006e }
            r3 = 1
            r1 = 0
            if (r0 != 0) goto L_0x0030
            r1 = 1
        L_0x0030:
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            boolean r0 = r5.A0N
            r2 = 0
            if (r0 == 0) goto L_0x0050
            if (r1 != 0) goto L_0x0050
            X.7Nu r0 = r5.A0D
            if (r0 != 0) goto L_0x0043
            r1 = r2
        L_0x003d:
            X.16K r0 = r5.A0M
            r0.A02(r3, r2, r1)
            return
        L_0x0043:
            X.7Nv r1 = new X.7Nv
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.getStackTrace()
            r1.<init>()
            goto L_0x003d
        L_0x0050:
            X.7Nu r0 = r5.A0D
            if (r0 != 0) goto L_0x0059
            r1 = r2
        L_0x0055:
            A0E(r5, r4, r2, r2, r1)
            return
        L_0x0059:
            X.7Nv r1 = new X.7Nv
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.getStackTrace()
            r1.<init>()
            goto L_0x0055
        L_0x0066:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x006e }
            java.lang.String r0 = "Setting root on a released tree"
            r1.<init>(r0)     // Catch:{ all -> 0x006e }
            throw r1     // Catch:{ all -> 0x006e }
        L_0x006e:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AC.A0N(X.0wR):void");
    }

    public synchronized void A0P(String str, C61322yh r5, String str2) {
        C157047Nv r1;
        if (this.A0O) {
            A0Q(str, r5, str2);
        } else {
            this.A0L.A01();
            A0J(this, str, r5, false);
            if (this.A0D == null) {
                r1 = null;
            } else {
                Thread.currentThread().getStackTrace();
                r1 = new C157047Nv();
            }
            this.A0L.A02(2, str2, r1);
            C32121lB.A0A.addAndGet(1);
        }
    }

    public synchronized void A0Q(String str, C61322yh r5, String str2) {
        C157047Nv r1;
        if (this.A0P) {
            A0P(str, r5, str2);
        } else {
            this.A0M.A01();
            A0J(this, str, r5, false);
            if (this.A0D == null) {
                r1 = null;
            } else {
                Thread.currentThread().getStackTrace();
                r1 = new C157047Nv();
            }
            this.A0M.A02(3, str2, r1);
            C32121lB.A09.addAndGet(1);
        }
    }

    public static C402720y A02(AnonymousClass1AC r2, String str) {
        C16070wR r1 = r2.A02;
        if (r1 != null) {
            C402720y A012 = r2.A01(r1, str, 0);
            if (A012 != null) {
                return A012;
            }
            throw new C119325kf(AnonymousClass08S.A0P("Did not find section with key '", str, "'!"));
        }
        throw new IllegalStateException("You cannot call requestFocus methods before dataBound() is called!");
    }

    private void A06(C16070wR r5) {
        r5.A0C(r5.A03);
        this.A09.A01(r5.A03, r5, r5.A04);
        if (!r5.A0H()) {
            List list = r5.A06;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                A06((C16070wR) list.get(i));
            }
        }
    }

    private void A07(C16070wR r5) {
        r5.A0O(r5.A03);
        if (!r5.A0H()) {
            List list = r5.A06;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                A07((C16070wR) list.get(i));
            }
        }
    }

    private void A08(C16070wR r5) {
        r5.A0E(r5.A03);
        if (!r5.A0H()) {
            List list = r5.A06;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                A08((C16070wR) list.get(i));
            }
        }
    }

    private void A0A(C16070wR r25, int i, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        C16070wR r2 = r25;
        C24181Sp r4 = (C24181Sp) this.A0H.get(r2.A04);
        int i9 = r2.A00;
        int i10 = i;
        int i11 = i2;
        int i12 = i3;
        int i13 = i5;
        int i14 = i4;
        if (r4 == null) {
            r4 = new C24181Sp();
            this.A0H.put(r2.A04, r4);
        } else if (r4.A01 == i10 && r4.A03 == i11 && r4.A00 == i12 && r4.A02 == i14 && r4.A04 == i9 && i13 != 1) {
            return;
        }
        r4.A03 = i11;
        r4.A01 = i10;
        r4.A00 = i12;
        r4.A02 = i14;
        r4.A04 = i9;
        C16070wR r16 = r2;
        AnonymousClass1GA r17 = r2.A03;
        r16.A0J(r17, i10, i11, i9, i12, i14);
        if (!r2.A0H()) {
            List list = r2.A06;
            int size = list.size();
            int i15 = 0;
            for (int i16 = 0; i16 < size; i16++) {
                C16070wR r3 = (C16070wR) list.get(i16);
                int i17 = i - i15;
                int i18 = i2 - i15;
                int i19 = i3 - i15;
                int i20 = i4 - i15;
                int i21 = r3.A00;
                int i22 = -1;
                if (i17 >= i21 || i18 < 0) {
                    i6 = -1;
                    i7 = -1;
                } else {
                    i6 = Math.max(i17, 0);
                    i7 = Math.min(i18, i21 - 1);
                }
                if (i19 >= i21 || i20 < 0) {
                    i8 = -1;
                } else {
                    i8 = Math.max(i19, 0);
                    i22 = Math.min(i20, i21 - 1);
                }
                i15 += i21;
                A0A(r3, i6, i7, i8, i22, i13);
            }
        }
    }

    private void A0B(C16070wR r18, List list, C157047Nv r20) {
        boolean A022 = C27041cY.A02();
        if (A022) {
            C27041cY.A01("applyChangeSetToTarget");
        }
        ArrayList arrayList = new ArrayList();
        List list2 = list;
        try {
            int size = list2.size();
            boolean z = false;
            for (int i = 0; i < size; i++) {
                AnonymousClass1CW r8 = (AnonymousClass1CW) list2.get(i);
                if (r8.A03.size() > 0) {
                    int size2 = r8.A03.size();
                    for (int i2 = 0; i2 < size2; i2++) {
                        AnonymousClass1IH r0 = (AnonymousClass1IH) r8.A03.get(i2);
                        switch (r0.A03) {
                            case Process.SD_BLACK_HOLE:
                                this.A0C.AWu(r0.A01, r0.A00);
                                z = true;
                                break;
                            case -2:
                                this.A0C.CKm(r0.A01, r0.A00, r0.A07);
                                z = true;
                                break;
                            case -1:
                                this.A0C.BDC(r0.A01, r0.A00, r0.A07);
                                z = true;
                                break;
                            case 0:
                                this.A0C.BLF(r0.A01, r0.A02);
                                z = true;
                                break;
                            case 1:
                                this.A0C.BD9(r0.A01, r0.A04);
                                z = true;
                                break;
                            case 2:
                                this.A0C.CK6(r0.A01, r0.A04);
                                z = true;
                                break;
                            case 3:
                                this.A0C.AWp(r0.A01);
                                z = true;
                                break;
                        }
                    }
                    this.A0C.A03();
                }
                arrayList.addAll(r8.A03);
            }
            AnonymousClass1CX r13 = new AnonymousClass1CX(arrayList);
            C157047Nv r12 = r20;
            if (r20 != null) {
                synchronized (this) {
                    this.A08 = this.A03;
                }
            }
            this.A0C.BM3(z, new AnonymousClass1CZ(this, r12, r13, z, A022, r18));
            if (A022) {
                C27041cY.A00();
            }
        } catch (Throwable th) {
            if (A022) {
                C27041cY.A00();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        if (r3.getClass().equals(r12.getClass()) == false) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A0C(X.AnonymousClass1GA r17, X.C16070wR r18, X.C16070wR r19, java.util.Map r20, X.AnonymousClass1AE r21, java.lang.String r22) {
        /*
            r12 = r19
            if (r19 == 0) goto L_0x0206
            boolean r16 = X.C27041cY.A02()
            if (r16 == 0) goto L_0x0015
            java.lang.String r1 = "createChildren:"
            java.lang.String r0 = r12.A0A
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            X.C27041cY.A01(r0)
        L_0x0015:
            r11 = r17
            X.1GA r1 = new X.1GA     // Catch:{ all -> 0x01ff }
            r1.<init>(r11)     // Catch:{ all -> 0x01ff }
            X.1AC r0 = r11.A02     // Catch:{ all -> 0x01ff }
            r1.A02 = r0     // Catch:{ all -> 0x01ff }
            X.10N r0 = r11.A00     // Catch:{ all -> 0x01ff }
            r1.A00 = r0     // Catch:{ all -> 0x01ff }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x01ff }
            r0.<init>(r12)     // Catch:{ all -> 0x01ff }
            r1.A03 = r0     // Catch:{ all -> 0x01ff }
            r12.A03 = r1     // Catch:{ all -> 0x01ff }
            r3 = r18
            if (r18 == 0) goto L_0x0035
            int r0 = r3.A00     // Catch:{ all -> 0x01ff }
            r12.A00 = r0     // Catch:{ all -> 0x01ff }
        L_0x0035:
            boolean r6 = r12.A0H()     // Catch:{ all -> 0x01ff }
            if (r6 != 0) goto L_0x0040
            X.1KE r0 = r11.A07     // Catch:{ all -> 0x01ff }
            r12.A0I(r0)     // Catch:{ all -> 0x01ff }
        L_0x0040:
            if (r18 == 0) goto L_0x0051
            java.lang.Class r1 = r3.getClass()     // Catch:{ all -> 0x01ff }
            java.lang.Class r0 = r12.getClass()     // Catch:{ all -> 0x01ff }
            boolean r1 = r1.equals(r0)     // Catch:{ all -> 0x01ff }
            r0 = 1
            if (r1 != 0) goto L_0x0052
        L_0x0051:
            r0 = 0
        L_0x0052:
            if (r18 == 0) goto L_0x0057
            if (r0 == 0) goto L_0x0057
            goto L_0x0062
        L_0x0057:
            X.1GA r0 = r12.A03     // Catch:{ all -> 0x01ff }
            r12.A0N(r0)     // Catch:{ all -> 0x01ff }
            X.1GA r0 = r12.A03     // Catch:{ all -> 0x01ff }
            r12.A0D(r0)     // Catch:{ all -> 0x01ff }
            goto L_0x00a3
        L_0x0062:
            java.lang.Object r0 = r3.A0B(r3)     // Catch:{ all -> 0x01ff }
            if (r0 != 0) goto L_0x00b9
            X.1GA r0 = r12.A03     // Catch:{ all -> 0x01ff }
            r12.A0D(r0)     // Catch:{ all -> 0x01ff }
            java.lang.Object r0 = r12.A0B(r12)     // Catch:{ all -> 0x01ff }
            if (r0 == 0) goto L_0x0098
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01ff }
            r1.<init>()     // Catch:{ all -> 0x01ff }
            java.lang.String r0 = "We were about to transfer a null service from "
            r1.append(r0)     // Catch:{ all -> 0x01ff }
            r1.append(r3)     // Catch:{ all -> 0x01ff }
            java.lang.String r0 = " to "
            r1.append(r0)     // Catch:{ all -> 0x01ff }
            r1.append(r12)     // Catch:{ all -> 0x01ff }
            java.lang.String r0 = " while the later created a non-null service"
            r1.append(r0)     // Catch:{ all -> 0x01ff }
            java.lang.String r2 = r1.toString()     // Catch:{ all -> 0x01ff }
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x01ff }
            java.lang.String r0 = "SectionTree:NullToNonNullServiceTransfer"
            X.C09070gU.A01(r1, r0, r2)     // Catch:{ all -> 0x01ff }
        L_0x0098:
            X.11I r1 = r3.A0S()     // Catch:{ all -> 0x01ff }
            X.11I r0 = r12.A0S()     // Catch:{ all -> 0x01ff }
            r12.A0M(r1, r0)     // Catch:{ all -> 0x01ff }
        L_0x00a3:
            java.lang.String r0 = r12.A04     // Catch:{ all -> 0x01ff }
            r10 = r20
            java.lang.Object r5 = r10.get(r0)     // Catch:{ all -> 0x01ff }
            java.util.List r5 = (java.util.List) r5     // Catch:{ all -> 0x01ff }
            if (r5 == 0) goto L_0x00fa
            X.11I r4 = r12.A0S()     // Catch:{ all -> 0x01ff }
            int r2 = r5.size()     // Catch:{ all -> 0x01ff }
            r1 = 0
            goto L_0x00bf
        L_0x00b9:
            X.1GA r0 = r12.A03     // Catch:{ all -> 0x01ff }
            r12.A0G(r0, r3, r12)     // Catch:{ all -> 0x01ff }
            goto L_0x0098
        L_0x00bf:
            if (r1 >= r2) goto L_0x00cd
            java.lang.Object r0 = r5.get(r1)     // Catch:{ all -> 0x01ff }
            X.2yh r0 = (X.C61322yh) r0     // Catch:{ all -> 0x01ff }
            r4.applyStateUpdate(r0)     // Catch:{ all -> 0x01ff }
            int r1 = r1 + 1
            goto L_0x00bf
        L_0x00cd:
            int r0 = r5.size()     // Catch:{ all -> 0x01ff }
            long r0 = (long) r0     // Catch:{ all -> 0x01ff }
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A06     // Catch:{ all -> 0x01ff }
            r2.addAndGet(r0)     // Catch:{ all -> 0x01ff }
            r1 = 0
            if (r19 == 0) goto L_0x00de
            boolean r0 = r12.A08     // Catch:{ all -> 0x01ff }
            r0 = r0 | r1
            goto L_0x00df
        L_0x00de:
            r0 = 0
        L_0x00df:
            if (r0 != 0) goto L_0x00e7
            boolean r0 = r12.A0K(r3, r12)     // Catch:{ all -> 0x01ff }
            if (r0 == 0) goto L_0x00e8
        L_0x00e7:
            r1 = 1
        L_0x00e8:
            if (r1 == 0) goto L_0x00fa
            r0 = 1
            r12.A08 = r0     // Catch:{ all -> 0x01ff }
            X.0wR r1 = r12.A02     // Catch:{ all -> 0x01ff }
            if (r1 == 0) goto L_0x00fa
            r1.A08 = r0     // Catch:{ all -> 0x01ff }
            X.0wR r0 = r1.A02     // Catch:{ all -> 0x01ff }
            if (r0 == 0) goto L_0x00fa
            X.C16070wR.A01(r0)     // Catch:{ all -> 0x01ff }
        L_0x00fa:
            if (r6 != 0) goto L_0x01f9
            r9 = 0
            if (r18 == 0) goto L_0x0100
            goto L_0x0102
        L_0x0100:
            r8 = r9
            goto L_0x010c
        L_0x0102:
            boolean r0 = r3.A0H()     // Catch:{ all -> 0x01ff }
            if (r0 != 0) goto L_0x0100
            java.util.Map r8 = X.C16070wR.A00(r3)     // Catch:{ all -> 0x01ff }
        L_0x010c:
            X.1KE r7 = r11.A07     // Catch:{ all -> 0x01ff }
            X.1KE r0 = r12.A0L(r11, r7)     // Catch:{ all -> 0x01ff }
            r11.A07 = r0     // Catch:{ all -> 0x01ff }
            X.38i r2 = r11.A05()     // Catch:{ all -> 0x01ff }
            r0 = 14
            X.3eb r1 = A00(r11, r0, r9, r12)     // Catch:{ all -> 0x01ff }
            X.1GA r0 = r12.A03     // Catch:{ all -> 0x01ff }
            X.1Bw r0 = r12.A0R(r0)     // Catch:{ all -> 0x01ff }
            if (r0 != 0) goto L_0x012e
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x01ff }
            r0.<init>()     // Catch:{ all -> 0x01ff }
        L_0x012b:
            r12.A06 = r0     // Catch:{ all -> 0x01ff }
            goto L_0x0131
        L_0x012e:
            java.util.List r0 = r0.A00     // Catch:{ all -> 0x01ff }
            goto L_0x012b
        L_0x0131:
            if (r2 == 0) goto L_0x0138
            if (r1 == 0) goto L_0x0138
            r2.BJI(r1)     // Catch:{ all -> 0x01ff }
        L_0x0138:
            java.util.List r6 = r12.A06     // Catch:{ all -> 0x01ff }
            int r5 = r6.size()     // Catch:{ all -> 0x01ff }
            r4 = 0
        L_0x013f:
            if (r4 >= r5) goto L_0x01f3
            java.lang.Object r3 = r6.get(r4)     // Catch:{ all -> 0x01ff }
            X.0wR r3 = (X.C16070wR) r3     // Catch:{ all -> 0x01ff }
            r3.A02 = r12     // Catch:{ all -> 0x01ff }
            java.lang.String r1 = r3.A05     // Catch:{ all -> 0x01ff }
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x01ff }
            if (r0 != 0) goto L_0x01dd
            java.lang.String r0 = r12.A04     // Catch:{ all -> 0x01ff }
            java.lang.String r15 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ all -> 0x01ff }
            X.1GA r14 = r12.A03     // Catch:{ all -> 0x01ff }
            X.0wR r1 = r14.A0K()     // Catch:{ all -> 0x01ff }
            if (r1 == 0) goto L_0x019b
            X.1GA r0 = r1.A03     // Catch:{ all -> 0x01ff }
            X.17T r0 = r0.A01     // Catch:{ all -> 0x01ff }
            java.util.Set r0 = r0.A00     // Catch:{ all -> 0x01ff }
            boolean r0 = r0.contains(r15)     // Catch:{ all -> 0x01ff }
            if (r0 == 0) goto L_0x019b
            java.lang.String r13 = r3.A0A     // Catch:{ all -> 0x01ff }
            java.util.Map r0 = r1.A07     // Catch:{ all -> 0x01ff }
            if (r0 != 0) goto L_0x0178
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x01ff }
            r0.<init>()     // Catch:{ all -> 0x01ff }
            r1.A07 = r0     // Catch:{ all -> 0x01ff }
        L_0x0178:
            java.util.Map r0 = r1.A07     // Catch:{ all -> 0x01ff }
            boolean r0 = r0.containsKey(r13)     // Catch:{ all -> 0x01ff }
            if (r0 == 0) goto L_0x01bb
            java.util.Map r0 = r1.A07     // Catch:{ all -> 0x01ff }
            java.lang.Object r0 = r0.get(r13)     // Catch:{ all -> 0x01ff }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x01ff }
            int r2 = r0.intValue()     // Catch:{ all -> 0x01ff }
        L_0x018c:
            java.util.Map r1 = r1.A07     // Catch:{ all -> 0x01ff }
            int r0 = r2 + 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x01ff }
            r1.put(r13, r0)     // Catch:{ all -> 0x01ff }
            java.lang.String r15 = X.AnonymousClass08S.A09(r15, r2)     // Catch:{ all -> 0x01ff }
        L_0x019b:
            r3.A04 = r15     // Catch:{ all -> 0x01ff }
            X.17T r0 = r14.A01     // Catch:{ all -> 0x01ff }
            java.util.Set r0 = r0.A00     // Catch:{ all -> 0x01ff }
            r0.add(r15)     // Catch:{ all -> 0x01ff }
            X.1GA r1 = new X.1GA     // Catch:{ all -> 0x01ff }
            r1.<init>(r11)     // Catch:{ all -> 0x01ff }
            X.1AC r0 = r11.A02     // Catch:{ all -> 0x01ff }
            r1.A02 = r0     // Catch:{ all -> 0x01ff }
            X.10N r0 = r11.A00     // Catch:{ all -> 0x01ff }
            r1.A00 = r0     // Catch:{ all -> 0x01ff }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x01ff }
            r0.<init>(r3)     // Catch:{ all -> 0x01ff }
            r1.A03 = r0     // Catch:{ all -> 0x01ff }
            r3.A03 = r1     // Catch:{ all -> 0x01ff }
            goto L_0x01bd
        L_0x01bb:
            r2 = 0
            goto L_0x018c
        L_0x01bd:
            if (r8 != 0) goto L_0x01c3
            r0 = r9
        L_0x01c0:
            if (r0 == 0) goto L_0x01cc
            goto L_0x01ce
        L_0x01c3:
            java.lang.String r0 = r3.A04     // Catch:{ all -> 0x01ff }
            java.lang.Object r0 = r8.get(r0)     // Catch:{ all -> 0x01ff }
            X.0p2 r0 = (X.C12300p2) r0     // Catch:{ all -> 0x01ff }
            goto L_0x01c0
        L_0x01cc:
            r0 = r9
            goto L_0x01d2
        L_0x01ce:
            java.lang.Object r0 = r0.A00     // Catch:{ all -> 0x01ff }
            X.0wR r0 = (X.C16070wR) r0     // Catch:{ all -> 0x01ff }
        L_0x01d2:
            r18 = r0
            r19 = r3
            A0C(r17, r18, r19, r20, r21, r22)     // Catch:{ all -> 0x01ff }
            int r4 = r4 + 1
            goto L_0x013f
        L_0x01dd:
            java.lang.String r2 = "Your Section "
            java.lang.Class r0 = r3.getClass()     // Catch:{ all -> 0x01ff }
            java.lang.String r1 = r0.getSimpleName()     // Catch:{ all -> 0x01ff }
            java.lang.String r0 = " has an empty key. Please specify a key."
            java.lang.String r1 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x01ff }
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x01ff }
            r0.<init>(r1)     // Catch:{ all -> 0x01ff }
            throw r0     // Catch:{ all -> 0x01ff }
        L_0x01f3:
            X.1KE r0 = r11.A07     // Catch:{ all -> 0x01ff }
            if (r0 == r7) goto L_0x01f9
            r11.A07 = r7     // Catch:{ all -> 0x01ff }
        L_0x01f9:
            if (r16 == 0) goto L_0x01fe
            X.C27041cY.A00()
        L_0x01fe:
            return
        L_0x01ff:
            r0 = move-exception
            if (r16 == 0) goto L_0x0205
            X.C27041cY.A00()
        L_0x0205:
            throw r0
        L_0x0206:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Can't generate a subtree with a null root"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AC.A0C(X.1GA, X.0wR, X.0wR, java.util.Map, X.1AE, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r2 == X.AnonymousClass07B.A0C) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0D(X.AnonymousClass1AC r3) {
        /*
            X.1AF r3 = r3.A0E
            java.lang.Integer r2 = r3.A01
            if (r2 == 0) goto L_0x000f
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            if (r2 == r0) goto L_0x000f
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r0 = 0
            if (r2 != r1) goto L_0x0010
        L_0x000f:
            r0 = 1
        L_0x0010:
            if (r0 == 0) goto L_0x0018
            r0 = 0
            r3.A02 = r0
            r3.A01()
        L_0x0018:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AC.A0D(X.1AC):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0200, code lost:
        if (r10 != r2) goto L_0x0202;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0214, code lost:
        if (r1 == false) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:?, code lost:
        r5 = new java.lang.StringBuilder();
        r5.append("Changet count is below 0! ");
        r5.append("Current section: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x03a8, code lost:
        if (r1 != null) goto L_0x03b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x03aa, code lost:
        r5.append("null; ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x03ad, code lost:
        r5.append("Next section: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x03b3, code lost:
        r5.append(r1.A0A + " , key=" + r1.A04 + ", count=" + r1.A00 + ", childrenSize=" + r1.A06.size() + "; ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x03e4, code lost:
        if (r0 != null) goto L_0x03e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x03e7, code lost:
        r5.append(r0.A0A + " , key=" + r0.A04 + ", count=" + r0.A00 + ", childrenSize=" + r0.A06.size() + "; ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x0418, code lost:
        r5.append("null; ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x041b, code lost:
        r5.append("Changes: [");
        r7 = r3.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x0424, code lost:
        if (r2 >= r7.A00) goto L_0x0459;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x0426, code lost:
        r4 = (X.AnonymousClass1IH) r7.A03.get(r2);
        r5.append(r4.A03 + " " + r4.A01 + " " + r4.A02);
        r5.append(", ");
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:268:0x0459, code lost:
        r5.append("]");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:0x0467, code lost:
        throw new java.lang.IllegalStateException(r5.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:?, code lost:
        r1 = r7.A0F.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x0470, code lost:
        if (r1 == null) goto L_0x047a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x0472, code lost:
        r8 = (X.C35301r0) r1.A01(X.C35301r0.class);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x047e, code lost:
        if (X.C35301r0.A01(r8) == false) goto L_0x0489;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x0480, code lost:
        r8.A03("_changeset", "_end", r8.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x0489, code lost:
        if (r9 == null) goto L_0x0490;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x048b, code lost:
        if (r4 == null) goto L_0x0490;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x048d, code lost:
        r9.BJI(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0089, code lost:
        if (r9.BHH(r4) == false) goto L_0x008b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01d1 A[Catch:{ all -> 0x0468 }] */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x038f A[EDGE_INSN: B:254:0x038f->B:255:? ?: BREAK  , SYNTHETIC, Splitter:B:254:0x038f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0E(X.AnonymousClass1AC r26, int r27, java.lang.String r28, X.AnonymousClass16J r29, X.C157047Nv r30) {
        /*
            r7 = r26
            r6 = r28
            if (r28 != 0) goto L_0x0008
            java.lang.String r6 = r7.A0G
        L_0x0008:
            boolean r18 = X.C27041cY.A02()
            if (r18 == 0) goto L_0x0036
            if (r6 == 0) goto L_0x0019
            java.lang.String r0 = "extra:"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r6)
            X.C27041cY.A01(r0)
        L_0x0019:
            monitor-enter(r7)
            X.0wR r0 = r7.A04     // Catch:{ all -> 0x0025 }
            if (r0 == 0) goto L_0x0022
            java.lang.String r2 = r0.A0A     // Catch:{ all -> 0x0025 }
        L_0x0020:
            monitor-exit(r7)     // Catch:{ all -> 0x0025 }
            goto L_0x0029
        L_0x0022:
            java.lang.String r2 = "<null>"
            goto L_0x0020
        L_0x0025:
            r3 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0025 }
            goto L_0x04d5
        L_0x0029:
            java.lang.String r1 = "_applyNewChangeSet_"
            java.lang.String r0 = A03(r27)
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            X.C27041cY.A01(r0)
        L_0x0036:
            boolean r0 = X.AnonymousClass1AJ.A00
            if (r0 == 0) goto L_0x0049
            monitor-enter(r7)
            X.0wR r0 = r7.A04     // Catch:{ all -> 0x0045 }
            monitor-exit(r7)     // Catch:{ all -> 0x0045 }
            A03(r27)
            r7.hashCode()
            goto L_0x0049
        L_0x0045:
            r3 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0045 }
            goto L_0x04d5
        L_0x0049:
            monitor-enter(r7)     // Catch:{ all -> 0x04b6 }
            boolean r0 = r7.A0Q     // Catch:{ all -> 0x04af }
            if (r0 == 0) goto L_0x0051
            monitor-exit(r7)     // Catch:{ all -> 0x04af }
            goto L_0x0490
        L_0x0051:
            X.0wR r1 = r7.A03     // Catch:{ all -> 0x04af }
            r12 = 1
            if (r1 == 0) goto L_0x0075
            X.0wR r1 = r1.A0T(r12)     // Catch:{ all -> 0x04af }
        L_0x005a:
            X.0wR r2 = r7.A04     // Catch:{ all -> 0x04af }
            r0 = 0
            if (r2 == 0) goto L_0x0073
            X.0wR r0 = r2.A0T(r0)     // Catch:{ all -> 0x04af }
        L_0x0063:
            X.1GA r2 = r7.A0F     // Catch:{ all -> 0x04af }
            X.38i r9 = r2.A05()     // Catch:{ all -> 0x04af }
            X.1AI r2 = r7.A05     // Catch:{ all -> 0x04af }
            X.1AI r5 = X.AnonymousClass1AI.A00(r2)     // Catch:{ all -> 0x04af }
            r7.A07 = r12     // Catch:{ all -> 0x04af }
            monitor-exit(r7)     // Catch:{ all -> 0x04af }
            goto L_0x0077
        L_0x0073:
            r0 = 0
            goto L_0x0063
        L_0x0075:
            r1 = 0
            goto L_0x005a
        L_0x0077:
            X.1GA r3 = r7.A0F     // Catch:{ all -> 0x04b6 }
            r2 = 15
            X.3eb r4 = A00(r3, r2, r1, r0)     // Catch:{ all -> 0x04b6 }
            if (r9 == 0) goto L_0x008b
            if (r4 == 0) goto L_0x008b
            boolean r2 = r9.BHH(r4)     // Catch:{ all -> 0x04b6 }
            r28 = 1
            if (r2 != 0) goto L_0x008d
        L_0x008b:
            r28 = 0
        L_0x008d:
            if (r4 == 0) goto L_0x00aa
            java.lang.String r2 = "attribution"
            r4.A02(r2, r6)     // Catch:{ all -> 0x04b6 }
            java.lang.String r3 = "section_set_root_source"
            java.lang.String r2 = A03(r27)     // Catch:{ all -> 0x04b6 }
            r4.A02(r3, r2)     // Catch:{ all -> 0x04b6 }
            java.lang.String r8 = "sections_set_root_bg_thread"
            boolean r3 = X.C191216w.A00()     // Catch:{ all -> 0x04b6 }
            r2 = 0
            if (r3 != 0) goto L_0x00a7
            r2 = 1
        L_0x00a7:
            r4.A03(r8, r2)     // Catch:{ all -> 0x04b6 }
        L_0x00aa:
            X.1RE r2 = r7.A0A     // Catch:{ all -> 0x04b6 }
            r2.A00()     // Catch:{ all -> 0x04b6 }
        L_0x00af:
            r8 = 0
            if (r0 == 0) goto L_0x046c
            if (r18 == 0) goto L_0x00b9
            java.lang.String r2 = "calculateNewChangeSet"
            X.C27041cY.A01(r2)     // Catch:{ all -> 0x04b6 }
        L_0x00b9:
            X.1GA r11 = r7.A0F     // Catch:{ all -> 0x04b6 }
            java.util.Map r10 = r5.A00     // Catch:{ all -> 0x04b6 }
            X.1AE r2 = r7.A0K     // Catch:{ all -> 0x04b6 }
            r23 = r2
            java.lang.String r15 = r7.A0G     // Catch:{ all -> 0x04b6 }
            java.lang.String r2 = r0.A05     // Catch:{ all -> 0x04b6 }
            r0.A04 = r2     // Catch:{ all -> 0x04b6 }
            X.38i r13 = r11.A05()     // Catch:{ all -> 0x04b6 }
            r2 = 11
            X.3eb r3 = A00(r11, r2, r1, r0)     // Catch:{ all -> 0x04b6 }
            boolean r17 = X.C27041cY.A02()     // Catch:{ all -> 0x04b6 }
            if (r17 == 0) goto L_0x00dc
            java.lang.String r2 = "createTree"
            X.C27041cY.A01(r2)     // Catch:{ all -> 0x04b6 }
        L_0x00dc:
            r22 = r10
            r24 = r15
            r19 = r11
            r20 = r1
            r21 = r0
            A0C(r19, r20, r21, r22, r23, r24)     // Catch:{ all -> 0x0468 }
            if (r17 == 0) goto L_0x00ee
            X.C27041cY.A00()     // Catch:{ all -> 0x04b6 }
        L_0x00ee:
            if (r13 == 0) goto L_0x00f5
            if (r3 == 0) goto L_0x00f5
            r13.BJI(r3)     // Catch:{ all -> 0x04b6 }
        L_0x00f5:
            if (r17 == 0) goto L_0x00fc
            java.lang.String r2 = "ChangeSetState.generateChangeSet"
            X.C27041cY.A01(r2)     // Catch:{ all -> 0x04b6 }
        L_0x00fc:
            java.lang.String r25 = ""
            java.lang.String r26 = ""
            X.1By r3 = new X.1By     // Catch:{ all -> 0x0468 }
            r3.<init>()     // Catch:{ all -> 0x0468 }
            X.38i r10 = r11.A05()     // Catch:{ all -> 0x0468 }
            r2 = 13
            X.3eb r2 = A00(r11, r2, r1, r0)     // Catch:{ all -> 0x0468 }
            if (r1 == 0) goto L_0x0146
            if (r0 == 0) goto L_0x0146
            java.lang.String r14 = r1.A0A     // Catch:{ all -> 0x0468 }
            java.lang.String r13 = r0.A0A     // Catch:{ all -> 0x0468 }
            boolean r13 = r14.equals(r13)     // Catch:{ all -> 0x0468 }
            if (r13 != 0) goto L_0x0146
            r21 = 0
            java.util.List r13 = r3.A01     // Catch:{ all -> 0x0468 }
            java.lang.Thread r16 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0468 }
            java.lang.String r27 = r16.getName()     // Catch:{ all -> 0x0468 }
            r22 = r13
            X.1CW r13 = X.C20321By.A01(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28)     // Catch:{ all -> 0x0468 }
            r20 = 0
            java.util.List r14 = r3.A01     // Catch:{ all -> 0x0468 }
            java.lang.String r27 = r16.getName()     // Catch:{ all -> 0x0468 }
            r21 = r0
            r22 = r14
            X.1CW r11 = X.C20321By.A01(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28)     // Catch:{ all -> 0x0468 }
            X.1CW r11 = X.AnonymousClass1CW.A01(r13, r11)     // Catch:{ all -> 0x0468 }
            r3.A00 = r11     // Catch:{ all -> 0x0468 }
            goto L_0x0158
        L_0x0146:
            java.util.List r13 = r3.A01     // Catch:{ all -> 0x0468 }
            java.lang.Thread r14 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0468 }
            java.lang.String r27 = r14.getName()     // Catch:{ all -> 0x0468 }
            r22 = r13
            X.1CW r11 = X.C20321By.A01(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28)     // Catch:{ all -> 0x0468 }
            r3.A00 = r11     // Catch:{ all -> 0x0468 }
        L_0x0158:
            if (r10 == 0) goto L_0x01be
            if (r2 == 0) goto L_0x01be
            if (r1 != 0) goto L_0x0160
            r13 = -1
            goto L_0x0162
        L_0x0160:
            int r13 = r1.A00     // Catch:{ all -> 0x0468 }
        L_0x0162:
            java.lang.String r11 = "current_root_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            X.1CW r11 = r3.A00     // Catch:{ all -> 0x0468 }
            java.util.List r11 = r11.A03     // Catch:{ all -> 0x0468 }
            int r13 = r11.size()     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "change_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            X.1CW r11 = r3.A00     // Catch:{ all -> 0x0468 }
            int r13 = r11.A00     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "final_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            X.1CW r11 = r3.A00     // Catch:{ all -> 0x0468 }
            X.3fD r14 = r11.A01     // Catch:{ all -> 0x0468 }
            if (r14 == 0) goto L_0x01bb
            int r13 = r14.A02     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_effective_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            int r13 = r14.A04     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_insert_single_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            int r13 = r14.A03     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_insert_range_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            int r13 = r14.A01     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_delete_single_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            int r13 = r14.A00     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_delete_range_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            int r13 = r14.A07     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_update_single_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            int r13 = r14.A06     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_update_range_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
            int r13 = r14.A05     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "changeset_move_count"
            r2.A01(r11, r13)     // Catch:{ all -> 0x0468 }
        L_0x01bb:
            r10.BJI(r2)     // Catch:{ all -> 0x0468 }
        L_0x01be:
            r2 = 0
            if (r1 == 0) goto L_0x01c5
            int r10 = r1.A00     // Catch:{ all -> 0x0468 }
            if (r10 < 0) goto L_0x01ce
        L_0x01c5:
            if (r0 == 0) goto L_0x01cc
            int r10 = r0.A00     // Catch:{ all -> 0x0468 }
            if (r10 >= 0) goto L_0x01cc
            goto L_0x01ce
        L_0x01cc:
            r10 = 0
            goto L_0x01cf
        L_0x01ce:
            r10 = 1
        L_0x01cf:
            if (r10 != 0) goto L_0x038f
            if (r17 == 0) goto L_0x01d6
            X.C27041cY.A00()     // Catch:{ all -> 0x04b6 }
        L_0x01d6:
            if (r18 == 0) goto L_0x01db
            X.C27041cY.A00()     // Catch:{ all -> 0x04b6 }
        L_0x01db:
            monitor-enter(r7)     // Catch:{ all -> 0x04b6 }
            r13 = 0
            if (r1 == 0) goto L_0x01e0
            r13 = 1
        L_0x01e0:
            X.0wR r11 = r7.A03     // Catch:{ all -> 0x038b }
            r10 = 0
            if (r11 == 0) goto L_0x01e6
            r10 = 1
        L_0x01e6:
            if (r13 == 0) goto L_0x01f0
            if (r10 == 0) goto L_0x01f0
            int r2 = r1.A09     // Catch:{ all -> 0x038b }
            int r1 = r11.A09     // Catch:{ all -> 0x038b }
            if (r2 == r1) goto L_0x01f4
        L_0x01f0:
            if (r13 != 0) goto L_0x01f6
            if (r10 != 0) goto L_0x01f6
        L_0x01f4:
            r11 = 1
            goto L_0x01f7
        L_0x01f6:
            r11 = 0
        L_0x01f7:
            X.0wR r1 = r7.A04     // Catch:{ all -> 0x038b }
            if (r1 == 0) goto L_0x0202
            int r10 = r0.A09     // Catch:{ all -> 0x038b }
            int r2 = r1.A09     // Catch:{ all -> 0x038b }
            r1 = 1
            if (r10 == r2) goto L_0x0203
        L_0x0202:
            r1 = 0
        L_0x0203:
            if (r11 == 0) goto L_0x0216
            if (r1 == 0) goto L_0x0216
            monitor-enter(r7)     // Catch:{ all -> 0x038b }
            java.util.Map r2 = r5.A01     // Catch:{ all -> 0x035f }
            X.1AI r1 = r7.A05     // Catch:{ all -> 0x035f }
            java.util.Map r1 = r1.A01     // Catch:{ all -> 0x035f }
            boolean r1 = r2.equals(r1)     // Catch:{ all -> 0x035f }
            monitor-exit(r7)     // Catch:{ all -> 0x038b }
            r14 = 1
            if (r1 != 0) goto L_0x0217
        L_0x0216:
            r14 = 0
        L_0x0217:
            if (r14 == 0) goto L_0x0265
            X.0wR r10 = r7.A03     // Catch:{ all -> 0x038b }
            r7.A03 = r0     // Catch:{ all -> 0x038b }
            r7.A04 = r8     // Catch:{ all -> 0x038b }
            r1 = 0
            r7.A07 = r1     // Catch:{ all -> 0x038b }
            r7.A00 = r1     // Catch:{ all -> 0x038b }
            X.1AI r11 = r7.A05     // Catch:{ all -> 0x038b }
            java.util.Map r1 = r5.A00     // Catch:{ all -> 0x038b }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x038b }
            if (r1 != 0) goto L_0x025b
            java.util.Map r1 = r5.A00     // Catch:{ all -> 0x038b }
            java.util.Set r1 = r1.keySet()     // Catch:{ all -> 0x038b }
            java.util.Iterator r13 = r1.iterator()     // Catch:{ all -> 0x038b }
        L_0x0238:
            boolean r1 = r13.hasNext()     // Catch:{ all -> 0x038b }
            if (r1 == 0) goto L_0x025b
            java.lang.Object r8 = r13.next()     // Catch:{ all -> 0x038b }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x038b }
            java.util.Map r1 = r11.A00     // Catch:{ all -> 0x038b }
            boolean r1 = r1.containsKey(r8)     // Catch:{ all -> 0x038b }
            if (r1 == 0) goto L_0x025b
            java.util.Map r2 = r11.A00     // Catch:{ all -> 0x038b }
            java.util.Map r1 = r5.A00     // Catch:{ all -> 0x038b }
            X.AnonymousClass1AI.A01(r2, r1, r8)     // Catch:{ all -> 0x038b }
            java.util.Map r2 = r11.A01     // Catch:{ all -> 0x038b }
            java.util.Map r1 = r5.A01     // Catch:{ all -> 0x038b }
            X.AnonymousClass1AI.A01(r2, r1, r8)     // Catch:{ all -> 0x038b }
            goto L_0x0238
        L_0x025b:
            java.util.List r2 = r7.A06     // Catch:{ all -> 0x038b }
            X.1CW r1 = r3.A00     // Catch:{ all -> 0x038b }
            r2.add(r1)     // Catch:{ all -> 0x038b }
            if (r10 == 0) goto L_0x026a
            goto L_0x0267
        L_0x0265:
            r0 = r8
            goto L_0x026d
        L_0x0267:
            r7.A08(r10)     // Catch:{ all -> 0x038b }
        L_0x026a:
            r7.A09(r0)     // Catch:{ all -> 0x038b }
        L_0x026d:
            monitor-exit(r7)     // Catch:{ all -> 0x038b }
            if (r14 == 0) goto L_0x0315
            if (r0 == 0) goto L_0x0275
            r7.A06(r0)     // Catch:{ all -> 0x04b6 }
        L_0x0275:
            java.util.List r8 = r3.A01     // Catch:{ all -> 0x04b6 }
            int r3 = r8.size()     // Catch:{ all -> 0x04b6 }
            r2 = 0
        L_0x027c:
            if (r2 >= r3) goto L_0x028e
            java.lang.Object r0 = r8.get(r2)     // Catch:{ all -> 0x04b6 }
            X.0wR r0 = (X.C16070wR) r0     // Catch:{ all -> 0x04b6 }
            java.util.Map r1 = r7.A0H     // Catch:{ all -> 0x04b6 }
            java.lang.String r0 = r0.A04     // Catch:{ all -> 0x04b6 }
            r1.remove(r0)     // Catch:{ all -> 0x04b6 }
            int r2 = r2 + 1
            goto L_0x027c
        L_0x028e:
            X.1Iu r0 = r7.A09     // Catch:{ all -> 0x04b6 }
            r0.A00()     // Catch:{ all -> 0x04b6 }
            r10 = r29
            r2 = r30
            boolean r0 = r7.A0J     // Catch:{ all -> 0x04b6 }
            if (r0 == 0) goto L_0x02e8
            if (r0 == 0) goto L_0x036c
            boolean r8 = X.C27041cY.A02()     // Catch:{ all -> 0x04b6 }
            if (r8 == 0) goto L_0x02a8
            java.lang.String r0 = "applyChangeSetsToTargetBackgroundAllowed"
            X.C27041cY.A01(r0)     // Catch:{ all -> 0x04b6 }
        L_0x02a8:
            monitor-enter(r7)     // Catch:{ all -> 0x0365 }
            boolean r0 = r7.A0Q     // Catch:{ all -> 0x0362 }
            if (r0 == 0) goto L_0x02af
            monitor-exit(r7)     // Catch:{ all -> 0x0362 }
            goto L_0x02e2
        L_0x02af:
            X.0wR r1 = r7.A03     // Catch:{ all -> 0x0362 }
            java.util.List r0 = r7.A06     // Catch:{ all -> 0x0362 }
            r7.A0B(r1, r0, r2)     // Catch:{ all -> 0x0362 }
            java.util.List r0 = r7.A06     // Catch:{ all -> 0x0362 }
            r0.clear()     // Catch:{ all -> 0x0362 }
            monitor-exit(r7)     // Catch:{ all -> 0x0362 }
            boolean r0 = X.C191216w.A00()     // Catch:{ all -> 0x0365 }
            if (r0 == 0) goto L_0x02c6
            A0D(r7)     // Catch:{ all -> 0x0365 }
            goto L_0x02e2
        L_0x02c6:
            java.lang.String r2 = ""
            X.1js r0 = r7.A0B     // Catch:{ all -> 0x0365 }
            boolean r0 = r0.BHG()     // Catch:{ all -> 0x0365 }
            if (r0 == 0) goto L_0x02d8
            java.lang.String r1 = "SectionTree.applyChangeSetsToTargetBackgroundAllowed - "
            java.lang.String r0 = r7.A0G     // Catch:{ all -> 0x0365 }
            java.lang.String r2 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0365 }
        L_0x02d8:
            X.1js r1 = r7.A0B     // Catch:{ all -> 0x0365 }
            X.Evy r0 = new X.Evy     // Catch:{ all -> 0x0365 }
            r0.<init>(r7, r10)     // Catch:{ all -> 0x0365 }
            r1.BxL(r0, r2)     // Catch:{ all -> 0x0365 }
        L_0x02e2:
            if (r8 == 0) goto L_0x0315
            X.C27041cY.A00()     // Catch:{ all -> 0x04b6 }
            goto L_0x0315
        L_0x02e8:
            boolean r0 = X.C191216w.A00()     // Catch:{ all -> 0x04b6 }
            if (r0 == 0) goto L_0x02f2
            A0F(r7, r2)     // Catch:{ IndexOutOfBoundsException -> 0x0374 }
            goto L_0x0315
        L_0x02f2:
            X.1js r0 = r7.A0B     // Catch:{ all -> 0x04b6 }
            boolean r0 = r0.BHG()     // Catch:{ all -> 0x04b6 }
            if (r0 == 0) goto L_0x031d
            java.lang.String r1 = "SectionTree.postNewChangeSets - "
            java.lang.String r0 = r7.A0G     // Catch:{ all -> 0x04b6 }
            java.lang.String r8 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x04b6 }
        L_0x0302:
            X.16L r3 = new X.16L     // Catch:{ all -> 0x04b6 }
            r3.<init>(r7, r10, r2)     // Catch:{ all -> 0x04b6 }
            java.util.concurrent.atomic.AtomicBoolean r2 = r7.A0I     // Catch:{ all -> 0x04b6 }
            r0 = 0
            boolean r0 = r2.compareAndSet(r12, r0)     // Catch:{ all -> 0x04b6 }
            if (r0 == 0) goto L_0x0317
            X.1js r0 = r7.A0B     // Catch:{ all -> 0x04b6 }
            r0.BxM(r3, r8)     // Catch:{ all -> 0x04b6 }
        L_0x0315:
            monitor-enter(r7)     // Catch:{ all -> 0x04b6 }
            goto L_0x0320
        L_0x0317:
            X.1js r0 = r7.A0B     // Catch:{ all -> 0x04b6 }
            r0.BxL(r3, r8)     // Catch:{ all -> 0x04b6 }
            goto L_0x0315
        L_0x031d:
            java.lang.String r8 = ""
            goto L_0x0302
        L_0x0320:
            java.util.Map r0 = r5.A00     // Catch:{ all -> 0x0387 }
            r0.clear()     // Catch:{ all -> 0x0387 }
            java.util.Map r0 = r5.A01     // Catch:{ all -> 0x0387 }
            r0.clear()     // Catch:{ all -> 0x0387 }
            X.0v3 r0 = X.AnonymousClass1AH.A00     // Catch:{ all -> 0x0387 }
            r0.C0t(r5)     // Catch:{ all -> 0x0387 }
            boolean r0 = r7.A0Q     // Catch:{ all -> 0x0387 }
            if (r0 == 0) goto L_0x0336
            monitor-exit(r7)     // Catch:{ all -> 0x0387 }
            goto L_0x0490
        L_0x0336:
            X.0wR r1 = r7.A03     // Catch:{ all -> 0x0387 }
            if (r1 == 0) goto L_0x0348
            X.0wR r1 = r1.A0T(r12)     // Catch:{ all -> 0x0387 }
        L_0x033e:
            X.0wR r2 = r7.A04     // Catch:{ all -> 0x0387 }
            r0 = 0
            if (r2 == 0) goto L_0x034a
            X.0wR r0 = r2.A0T(r0)     // Catch:{ all -> 0x0387 }
            goto L_0x034b
        L_0x0348:
            r1 = 0
            goto L_0x033e
        L_0x034a:
            r0 = 0
        L_0x034b:
            if (r0 == 0) goto L_0x034e
            goto L_0x0354
        L_0x034e:
            r2 = 0
            r7.A07 = r2     // Catch:{ all -> 0x0387 }
            r7.A00 = r2     // Catch:{ all -> 0x0387 }
            goto L_0x035c
        L_0x0354:
            X.1AI r2 = r7.A05     // Catch:{ all -> 0x0387 }
            X.1AI r5 = X.AnonymousClass1AI.A00(r2)     // Catch:{ all -> 0x0387 }
            r7.A07 = r12     // Catch:{ all -> 0x0387 }
        L_0x035c:
            monitor-exit(r7)     // Catch:{ all -> 0x0387 }
            goto L_0x00af
        L_0x035f:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x038b }
            throw r0     // Catch:{ all -> 0x038b }
        L_0x0362:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0362 }
            throw r0     // Catch:{ all -> 0x0365 }
        L_0x0365:
            r1 = move-exception
            if (r8 == 0) goto L_0x0373
            X.C27041cY.A00()     // Catch:{ all -> 0x04b6 }
            goto L_0x0373
        L_0x036c:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x04b6 }
            java.lang.String r0 = "Must use UIThread-only variant when background change sets are not enabled."
            r1.<init>(r0)     // Catch:{ all -> 0x04b6 }
        L_0x0373:
            throw r1     // Catch:{ all -> 0x04b6 }
        L_0x0374:
            r3 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x04b6 }
            java.lang.String r1 = A04(r7)     // Catch:{ all -> 0x04b6 }
            java.lang.String r0 = r3.getMessage()     // Catch:{ all -> 0x04b6 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x04b6 }
            r2.<init>(r0, r3)     // Catch:{ all -> 0x04b6 }
            throw r2     // Catch:{ all -> 0x04b6 }
        L_0x0387:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0387 }
            goto L_0x04b5
        L_0x038b:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x038b }
            goto L_0x04b5
        L_0x038f:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0468 }
            r5.<init>()     // Catch:{ all -> 0x0468 }
            java.lang.String r4 = "Changet count is below 0! "
            r5.append(r4)     // Catch:{ all -> 0x0468 }
            java.lang.String r4 = "Current section: "
            r5.append(r4)     // Catch:{ all -> 0x0468 }
            java.lang.String r11 = "null; "
            java.lang.String r7 = "; "
            java.lang.String r8 = ", childrenSize="
            java.lang.String r9 = ", count="
            java.lang.String r10 = " , key="
            if (r1 != 0) goto L_0x03b3
            r5.append(r11)     // Catch:{ all -> 0x0468 }
        L_0x03ad:
            java.lang.String r1 = "Next section: "
            r5.append(r1)     // Catch:{ all -> 0x0468 }
            goto L_0x03e4
        L_0x03b3:
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0468 }
            r12.<init>()     // Catch:{ all -> 0x0468 }
            java.lang.String r4 = r1.A0A     // Catch:{ all -> 0x0468 }
            r12.append(r4)     // Catch:{ all -> 0x0468 }
            r12.append(r10)     // Catch:{ all -> 0x0468 }
            java.lang.String r4 = r1.A04     // Catch:{ all -> 0x0468 }
            r12.append(r4)     // Catch:{ all -> 0x0468 }
            r12.append(r9)     // Catch:{ all -> 0x0468 }
            int r4 = r1.A00     // Catch:{ all -> 0x0468 }
            r12.append(r4)     // Catch:{ all -> 0x0468 }
            r12.append(r8)     // Catch:{ all -> 0x0468 }
            java.util.List r1 = r1.A06     // Catch:{ all -> 0x0468 }
            int r1 = r1.size()     // Catch:{ all -> 0x0468 }
            r12.append(r1)     // Catch:{ all -> 0x0468 }
            r12.append(r7)     // Catch:{ all -> 0x0468 }
            java.lang.String r1 = r12.toString()     // Catch:{ all -> 0x0468 }
            r5.append(r1)     // Catch:{ all -> 0x0468 }
            goto L_0x03ad
        L_0x03e4:
            if (r0 != 0) goto L_0x03e7
            goto L_0x0418
        L_0x03e7:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0468 }
            r4.<init>()     // Catch:{ all -> 0x0468 }
            java.lang.String r1 = r0.A0A     // Catch:{ all -> 0x0468 }
            r4.append(r1)     // Catch:{ all -> 0x0468 }
            r4.append(r10)     // Catch:{ all -> 0x0468 }
            java.lang.String r1 = r0.A04     // Catch:{ all -> 0x0468 }
            r4.append(r1)     // Catch:{ all -> 0x0468 }
            r4.append(r9)     // Catch:{ all -> 0x0468 }
            int r1 = r0.A00     // Catch:{ all -> 0x0468 }
            r4.append(r1)     // Catch:{ all -> 0x0468 }
            r4.append(r8)     // Catch:{ all -> 0x0468 }
            java.util.List r0 = r0.A06     // Catch:{ all -> 0x0468 }
            int r0 = r0.size()     // Catch:{ all -> 0x0468 }
            r4.append(r0)     // Catch:{ all -> 0x0468 }
            r4.append(r7)     // Catch:{ all -> 0x0468 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x0468 }
            r5.append(r0)     // Catch:{ all -> 0x0468 }
            goto L_0x041b
        L_0x0418:
            r5.append(r11)     // Catch:{ all -> 0x0468 }
        L_0x041b:
            java.lang.String r0 = "Changes: ["
            r5.append(r0)     // Catch:{ all -> 0x0468 }
            X.1CW r7 = r3.A00     // Catch:{ all -> 0x0468 }
        L_0x0422:
            int r0 = r7.A00     // Catch:{ all -> 0x0468 }
            if (r2 >= r0) goto L_0x0459
            java.util.List r0 = r7.A03     // Catch:{ all -> 0x0468 }
            java.lang.Object r4 = r0.get(r2)     // Catch:{ all -> 0x0468 }
            X.1IH r4 = (X.AnonymousClass1IH) r4     // Catch:{ all -> 0x0468 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0468 }
            r3.<init>()     // Catch:{ all -> 0x0468 }
            int r0 = r4.A03     // Catch:{ all -> 0x0468 }
            r3.append(r0)     // Catch:{ all -> 0x0468 }
            java.lang.String r1 = " "
            r3.append(r1)     // Catch:{ all -> 0x0468 }
            int r0 = r4.A01     // Catch:{ all -> 0x0468 }
            r3.append(r0)     // Catch:{ all -> 0x0468 }
            r3.append(r1)     // Catch:{ all -> 0x0468 }
            int r0 = r4.A02     // Catch:{ all -> 0x0468 }
            r3.append(r0)     // Catch:{ all -> 0x0468 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0468 }
            r5.append(r0)     // Catch:{ all -> 0x0468 }
            java.lang.String r0 = ", "
            r5.append(r0)     // Catch:{ all -> 0x0468 }
            int r2 = r2 + 1
            goto L_0x0422
        L_0x0459:
            java.lang.String r0 = "]"
            r5.append(r0)     // Catch:{ all -> 0x0468 }
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0468 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x0468 }
            r1.<init>(r0)     // Catch:{ all -> 0x0468 }
            throw r1     // Catch:{ all -> 0x0468 }
        L_0x0468:
            r0 = move-exception
            if (r17 == 0) goto L_0x04b5
            goto L_0x04b2
        L_0x046c:
            X.1GA r0 = r7.A0F     // Catch:{ all -> 0x04b6 }
            X.1KE r1 = r0.A07     // Catch:{ all -> 0x04b6 }
            if (r1 == 0) goto L_0x047a
            java.lang.Class<X.1r0> r0 = X.C35301r0.class
            java.lang.Object r8 = r1.A01(r0)     // Catch:{ all -> 0x04b6 }
            X.1r0 r8 = (X.C35301r0) r8     // Catch:{ all -> 0x04b6 }
        L_0x047a:
            boolean r0 = X.C35301r0.A01(r8)     // Catch:{ all -> 0x04b6 }
            if (r0 == 0) goto L_0x0489
            java.lang.String r2 = "_changeset"
            java.lang.String r1 = "_end"
            java.lang.String r0 = r8.A00     // Catch:{ all -> 0x04b6 }
            r8.A03(r2, r1, r0)     // Catch:{ all -> 0x04b6 }
        L_0x0489:
            if (r9 == 0) goto L_0x0490
            if (r4 == 0) goto L_0x0490
            r9.BJI(r4)     // Catch:{ all -> 0x04b6 }
        L_0x0490:
            if (r18 == 0) goto L_0x049a
            X.C27041cY.A00()
            if (r6 == 0) goto L_0x049a
            X.C27041cY.A00()
        L_0x049a:
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A07
            r0 = 1
            r2.addAndGet(r0)
            boolean r0 = X.C191216w.A00()
            if (r0 == 0) goto L_0x04ae
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A08
            r0 = 1
            r2.addAndGet(r0)
        L_0x04ae:
            return
        L_0x04af:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x04af }
            goto L_0x04b5
        L_0x04b2:
            X.C27041cY.A00()     // Catch:{ all -> 0x04b6 }
        L_0x04b5:
            throw r0     // Catch:{ all -> 0x04b6 }
        L_0x04b6:
            r3 = move-exception
            if (r18 == 0) goto L_0x04c1
            X.C27041cY.A00()
            if (r6 == 0) goto L_0x04c1
            X.C27041cY.A00()
        L_0x04c1:
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A07
            r0 = 1
            r2.addAndGet(r0)
            boolean r0 = X.C191216w.A00()
            if (r0 == 0) goto L_0x04d5
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A08
            r0 = 1
            r2.addAndGet(r0)
        L_0x04d5:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AC.A0E(X.1AC, int, java.lang.String, X.16J, X.7Nv):void");
    }

    public static void A0F(AnonymousClass1AC r3, C157047Nv r4) {
        if (!r3.A0J) {
            boolean A022 = C27041cY.A02();
            if (A022) {
                C27041cY.A01("applyChangeSetsToTargetUIThreadOnly");
            }
            try {
                synchronized (r3) {
                    if (!r3.A0Q) {
                        ArrayList arrayList = new ArrayList(r3.A06);
                        r3.A06.clear();
                        C16070wR r0 = r3.A03;
                        r3.A0B(r0, arrayList, r4);
                        A0D(r3);
                    }
                }
                if (A022) {
                    C27041cY.A00();
                }
            } catch (Throwable th) {
                if (A022) {
                    C27041cY.A00();
                }
                throw th;
            }
        } else {
            throw new IllegalStateException("Cannot use UIThread-only variant when background change sets are enabled.");
        }
    }

    public static void A0G(AnonymousClass1AC r4, C16070wR r5) {
        r5.A0Q(r5.A03);
        if (!r5.A0H()) {
            List list = r5.A06;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                A0G(r4, (C16070wR) list.get(i));
            }
        }
    }

    public static void A0H(AnonymousClass1AC r12, C16070wR r13, boolean z, boolean z2, long j, AnonymousClass1CX r18, int i) {
        int i2;
        int i3;
        int i4 = i;
        C16070wR r2 = r13;
        if (!r13.A0H()) {
            C24181Sp r0 = (C24181Sp) r12.A0H.get(r13.A04);
            if (r0 != null) {
                i2 = r0.A01;
                i3 = r0.A03;
            } else {
                i2 = -1;
                i3 = -1;
            }
            r2.A0P(r13.A03, z, z2, j, i2, i3, r18, i4);
            List list = r13.A06;
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                C16070wR r132 = (C16070wR) list.get(i5);
                A0H(r12, r132, z, z2, j, r18, i4);
                i4 += r132.A00;
            }
        }
    }

    public static void A0I(AnonymousClass1AC r2, Integer num) {
        if (num == AnonymousClass07B.A00 || num == AnonymousClass07B.A01) {
            r2.A0E.A02 = true;
        }
        if (num == AnonymousClass07B.A0N) {
            r2.A0E.A02 = false;
        }
        AnonymousClass1AF r0 = r2.A0E;
        r0.A01 = num;
        r0.A01();
    }

    public static /* synthetic */ boolean A0K(C402720y r2, int i) {
        int i2 = r2.A01.A00;
        if (i < i2 && i >= 0) {
            return true;
        }
        C09070gU.A01(AnonymousClass07B.A01, "SectionTree:OutOfBoundsRequestFocus", AnonymousClass08S.A0B("You are trying to request focus with offset on an index that is out of bounds: requested ", i, " , total ", i2));
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002e, code lost:
        if (r4.A0D != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0030, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0031, code lost:
        r4.A0M.A02(1, null, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0036, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0037, code lost:
        java.lang.Thread.currentThread().getStackTrace();
        r1 = new X.C157047Nv();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0O(X.C16070wR r5) {
        /*
            r4 = this;
            boolean r0 = r4.A0Q
            if (r0 != 0) goto L_0x0047
            monitor-enter(r4)
            X.0wR r0 = r4.A03     // Catch:{ all -> 0x0044 }
            if (r0 == 0) goto L_0x0011
            int r1 = r0.A09     // Catch:{ all -> 0x0044 }
            int r0 = r5.A09     // Catch:{ all -> 0x0044 }
            if (r1 != r0) goto L_0x0011
        L_0x000f:
            monitor-exit(r4)     // Catch:{ all -> 0x0044 }
            goto L_0x001c
        L_0x0011:
            X.0wR r0 = r4.A04     // Catch:{ all -> 0x0044 }
            if (r0 == 0) goto L_0x001d
            int r1 = r0.A09     // Catch:{ all -> 0x0044 }
            int r0 = r5.A09     // Catch:{ all -> 0x0044 }
            if (r1 != r0) goto L_0x001d
            goto L_0x000f
        L_0x001c:
            return
        L_0x001d:
            r0 = 0
            if (r5 == 0) goto L_0x0021
            goto L_0x0023
        L_0x0021:
            r0 = 0
            goto L_0x0027
        L_0x0023:
            X.0wR r0 = r5.A0T(r0)     // Catch:{ all -> 0x0044 }
        L_0x0027:
            r4.A04 = r0     // Catch:{ all -> 0x0044 }
            monitor-exit(r4)     // Catch:{ all -> 0x0044 }
            X.7Nu r0 = r4.A0D
            r3 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0037
            r1 = r3
        L_0x0031:
            X.16K r0 = r4.A0M
            r0.A02(r2, r3, r1)
            return
        L_0x0037:
            X.7Nv r1 = new X.7Nv
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.getStackTrace()
            r1.<init>()
            goto L_0x0031
        L_0x0044:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0044 }
            throw r0
        L_0x0047:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Setting root on a released tree"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AC.A0O(X.0wR):void");
    }

    public AnonymousClass1AC(AnonymousClass1AD r6) {
        Looper looper;
        boolean z = r6.A02;
        this.A0O = z;
        boolean z2 = r6.A03;
        this.A0P = z2;
        if (!z || !z2) {
            this.A0N = false;
            String str = r6.A01;
            this.A0G = str;
            AnonymousClass1AG r2 = new AnonymousClass1AG(r6.A06, this.A0K, str);
            this.A0C = r2;
            this.A0J = r2.CIZ();
            this.A0E = new AnonymousClass1AF(r2);
            AnonymousClass1GA r1 = new AnonymousClass1GA(r6.A05);
            r1.A02 = this;
            r1.A00 = new AnonymousClass1TB(this);
            this.A0F = r1;
            this.A06 = new ArrayList();
            AnonymousClass1AI r0 = (AnonymousClass1AI) AnonymousClass1AH.A00.A00();
            this.A05 = r0 == null ? new AnonymousClass1AI() : r0;
            C31551js r4 = r6.A00;
            if (r4 == null) {
                synchronized (AnonymousClass1AC.class) {
                    if (A0R == null) {
                        HandlerThread handlerThread = new HandlerThread("SectionChangeSetThread", 0);
                        handlerThread.start();
                        A0R = handlerThread.getLooper();
                    }
                    looper = A0R;
                }
                r4 = new AnonymousClass11N(looper);
            }
            this.A0M = new AnonymousClass16K(this, C21851Iy.A00(r4));
            this.A0L = new AnonymousClass16K(this, this.A0B);
            this.A0D = null;
            this.A0I = new AtomicBoolean(r6.A04);
            return;
        }
        throw new RuntimeException("Cannot force both sync and async state updates at the same time");
    }

    public static C72593eb A00(AnonymousClass0p4 r2, int i, C16070wR r4, C16070wR r5) {
        String str;
        C637038i A052 = r2.A05();
        if (A052 == null) {
            return null;
        }
        C72593eb A002 = C637138j.A00(r2, A052, A052.BLg(r2, i));
        if (A002 != null) {
            String str2 = "null";
            if (r4 == null) {
                str = str2;
            } else {
                str = r4.A0A;
            }
            A002.A02("section_current", str);
            if (r5 != null) {
                str2 = r5.A0A;
            }
            A002.A02("section_next", str2);
        }
        return A002;
    }

    public static void A05(C31551js r1, Runnable runnable) {
        String str;
        if (C191216w.A00()) {
            runnable.run();
            return;
        }
        if (r1.BHG()) {
            str = "SectionTree.focusRequestOnUiThread";
        } else {
            str = BuildConfig.FLAVOR;
        }
        r1.BxL(runnable, str);
    }
}
