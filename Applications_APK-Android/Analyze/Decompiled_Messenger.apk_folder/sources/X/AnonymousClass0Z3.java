package X;

import java.util.Comparator;

/* renamed from: X.0Z3  reason: invalid class name */
public final class AnonymousClass0Z3 implements Comparator {
    public int compare(Object obj, Object obj2) {
        return Long.valueOf(((C13330rF) obj).getStartTime()).compareTo(Long.valueOf(((C13330rF) obj2).getStartTime()));
    }
}
