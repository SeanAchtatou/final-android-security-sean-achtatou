package X;

import android.database.sqlite.SQLiteDatabase;
import java.util.WeakHashMap;

/* renamed from: X.1zR  reason: invalid class name and case insensitive filesystem */
public final class C39681zR {
    public final String A00;
    public final String[] A01;
    public final String[] A02;
    private final WeakHashMap A03 = new WeakHashMap(1);

    public static C23303Bbu A00(C39681zR r2, SQLiteDatabase sQLiteDatabase) {
        C23303Bbu bbu = (C23303Bbu) r2.A03.get(sQLiteDatabase);
        if (bbu != null) {
            return bbu;
        }
        C23303Bbu bbu2 = new C23303Bbu();
        r2.A03.put(sQLiteDatabase, bbu2);
        return bbu2;
    }

    public C39681zR(String str, String[] strArr, String[] strArr2) {
        this.A00 = str;
        this.A02 = strArr;
        this.A01 = strArr2;
    }
}
