package X;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1gz  reason: invalid class name and case insensitive filesystem */
public final class C29791gz implements C29801h0 {
    private static volatile C29791gz A05;
    public AnonymousClass0UN A00;
    public final Runnable A01 = new C29811h1(this);
    public final Runnable A02 = new C29841h4(this);
    public final Runnable A03 = new C29851h5(this);
    private final C29831h3 A04 = new C29821h2(this);

    public void BNH(int i, int i2, Intent intent) {
    }

    public void BWF(C12570pc r1) {
    }

    public void BsQ(MotionEvent motionEvent) {
    }

    public static final C29791gz A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C29791gz.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C29791gz(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A01(C29791gz r4, int i) {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBd, r4.A00)).markerAnnotate(i, ErrorReportingConstants.ENDPOINT, AnonymousClass01P.A02());
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBd, r4.A00)).markerEnd(i, 2);
    }

    public static void A02(C29791gz r3, View view, Runnable runnable) {
        C005505z.A03("FirstFrameTracker.setLayoutListener", 172338208);
        try {
            View rootView = view.getRootView();
            rootView.getViewTreeObserver().addOnGlobalLayoutListener(new C37181up(r3, rootView, runnable));
        } finally {
            C005505z.A00(-1820079950);
        }
    }

    public void BWH(C12570pc r5) {
        boolean z;
        View view;
        int i = AnonymousClass1Y3.BBd;
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(2, i, this.A00)).markerStart(44826636);
        if (((QuickPerformanceLogger) AnonymousClass1XX.A02(2, i, this.A00)).isMarkerOn(44826636)) {
            if (!(r5 instanceof C12570pc) || (view = r5.A0I) == null) {
                z = false;
            } else {
                A02(this, view, this.A02);
                z = true;
            }
            if (!z) {
                ((AnonymousClass3E1) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AsL, this.A00)).BxO(this.A04);
            }
        }
    }

    private C29791gz(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
