package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.1K1  reason: invalid class name */
public final class AnonymousClass1K1 {
    public final float A00;
    public final Integer A01;

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass1K1)) {
            return false;
        }
        AnonymousClass1K1 r4 = (AnonymousClass1K1) obj;
        Integer num = this.A01;
        if (num != r4.A01) {
            return false;
        }
        if (num == AnonymousClass07B.A00 || num == AnonymousClass07B.A0N || Float.compare(this.A00, r4.A00) == 0) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int floatToIntBits = Float.floatToIntBits(this.A00);
        switch (this.A01.intValue()) {
            case 1:
                i = 1;
                break;
            case 2:
                i = 2;
                break;
            case 3:
                i = 3;
                break;
            default:
                i = 0;
                break;
        }
        return floatToIntBits + i;
    }

    public String toString() {
        switch (this.A01.intValue()) {
            case 0:
                return TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1N);
            case 1:
                return Float.toString(this.A00);
            case 2:
                return this.A00 + "%";
            case 3:
                return "auto";
            default:
                throw new IllegalStateException();
        }
    }

    public AnonymousClass1K1(float f, Integer num) {
        this.A00 = f;
        this.A01 = num;
    }
}
