package X;

/* renamed from: X.0S8  reason: invalid class name */
public final class AnonymousClass0S8 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.protocol.DefaultMqttClientCore$1";
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass0PK A02;
    public final /* synthetic */ AnonymousClass0CK A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ boolean A05;
    public final /* synthetic */ boolean A06;

    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "DEFAULT";
            case 2:
                return "SEQ_PREFERRED";
            case 3:
                return "SEQ_NONPREFERRED";
            case 4:
                return "HE_PREFERRED";
            case 5:
                return "HE_NONPREFERRED";
            default:
                return "UNKNOWN";
        }
    }

    public AnonymousClass0S8(AnonymousClass0PK r1, String str, int i, boolean z, AnonymousClass0CK r5, int i2, boolean z2) {
        this.A02 = r1;
        this.A04 = str;
        this.A01 = i;
        this.A05 = z;
        this.A03 = r5;
        this.A00 = i2;
        this.A06 = z2;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:(2:145|146)|147|148) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:187|(2:189|190)|191|192|242) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:179|180|181|(2:183|184)|185|186) */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x0496, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x04a9, code lost:
        r6 = X.C01660Bc.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x04cd, code lost:
        r6 = new X.C01550Ar(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x04d5, code lost:
        r0.A03 = r7.getLocalAddress();
        r0.A04 = r7.getInetAddress();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x05b9, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:?, code lost:
        r5.A0D.A01(X.AnonymousClass0CE.A00(r4), X.AnonymousClass0CG.A02, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x05c6, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:0x05c8, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:?, code lost:
        r5.A0D.A01(X.AnonymousClass0CE.A00(r4), X.AnonymousClass0CG.A02, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x05d5, code lost:
        r5.A0D.A01(X.AnonymousClass0CE.A00(r4), X.AnonymousClass0CG.A02, r4);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:147:0x03fa */
    /* JADX WARNING: Missing exception handler attribute for start block: B:185:0x0471 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:191:0x047e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:206:0x0498 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:243:0x0541 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:46:0x0177 */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0492 A[SYNTHETIC, Splitter:B:203:0x0492] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0496 A[ExcHandler: all (th java.lang.Throwable), PHI: r7 r18 
      PHI: (r7v2 java.net.Socket) = (r7v3 java.net.Socket), (r7v3 java.net.Socket), (r7v14 java.net.Socket), (r7v14 java.net.Socket), (r7v17 java.net.Socket), (r7v17 java.net.Socket), (r7v18 java.net.Socket), (r7v21 java.net.Socket) binds: [B:203:0x0492, B:204:?, B:58:0x01c0, B:59:?, B:44:0x016e, B:46:0x0177, B:35:0x013c, B:24:0x00ff] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r18v5 java.lang.Integer) = (r18v6 java.lang.Integer), (r18v6 java.lang.Integer), (r18v2 java.lang.Integer), (r18v2 java.lang.Integer), (r18v9 java.lang.Integer), (r18v9 java.lang.Integer), (r18v2 java.lang.Integer), (r18v2 java.lang.Integer) binds: [B:203:0x0492, B:204:?, B:58:0x01c0, B:59:?, B:44:0x016e, B:46:0x0177, B:35:0x013c, B:24:0x00ff] A[DONT_GENERATE, DONT_INLINE], Splitter:B:24:0x00ff] */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x04a9 A[Catch:{ IOException -> 0x04e2, all -> 0x0531 }] */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x04cd A[Catch:{ IOException -> 0x04e2, all -> 0x0531 }] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x04d5 A[Catch:{ IOException -> 0x04e2, all -> 0x0531 }] */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x0513 A[Catch:{ IOException -> 0x04e2, all -> 0x0531 }] */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x051b A[Catch:{ IOException -> 0x04e2, all -> 0x0531 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:185:0x0471=Splitter:B:185:0x0471, B:238:0x0522=Splitter:B:238:0x0522, B:147:0x03fa=Splitter:B:147:0x03fa, B:171:0x045e=Splitter:B:171:0x045e, B:191:0x047e=Splitter:B:191:0x047e} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:60:0x01d9=Splitter:B:60:0x01d9, B:210:0x049b=Splitter:B:210:0x049b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r35 = this;
            r1 = r35
            X.0PK r0 = r1.A02     // Catch:{ all -> 0x05f3 }
            java.lang.String r5 = r1.A04     // Catch:{ all -> 0x05f3 }
            int r11 = r1.A01     // Catch:{ all -> 0x05f3 }
            boolean r10 = r1.A05     // Catch:{ all -> 0x05f3 }
            X.0CK r2 = r1.A03     // Catch:{ all -> 0x05f3 }
            r34 = r2
            int r15 = r1.A00     // Catch:{ all -> 0x05f3 }
            boolean r7 = r1.A06     // Catch:{ all -> 0x05f3 }
            r22 = 0
            r9 = 1
            long r16 = android.os.SystemClock.elapsedRealtime()     // Catch:{ 0SV -> 0x0569 }
            X.0BE r4 = r0.A08     // Catch:{ 0SV -> 0x0569 }
            X.0C6 r2 = r0.A09     // Catch:{ 0SV -> 0x0569 }
            int r2 = r2.A05     // Catch:{ 0SV -> 0x0569 }
            long r2 = (long) r2     // Catch:{ 0SV -> 0x0569 }
            r12 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r12
            java.util.concurrent.Future r6 = r4.A00(r5)     // Catch:{ InterruptedException -> 0x0559, ExecutionException -> 0x0549, TimeoutException -> 0x0541 }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0559, ExecutionException -> 0x0549, TimeoutException -> 0x0541 }
            java.lang.Object r2 = r6.get(r2, r4)     // Catch:{ InterruptedException -> 0x0559, ExecutionException -> 0x0549, TimeoutException -> 0x0541 }
            X.0Ry r2 = (X.AnonymousClass0Ry) r2     // Catch:{ InterruptedException -> 0x0559, ExecutionException -> 0x0549, TimeoutException -> 0x0541 }
            X.0B6 r13 = r0.A06     // Catch:{ 0SV -> 0x0569 }
            long r18 = android.os.SystemClock.elapsedRealtime()     // Catch:{ 0SV -> 0x0569 }
            long r18 = r18 - r16
            X.0CB r3 = r0.A0D     // Catch:{ 0SV -> 0x0569 }
            X.0C8 r3 = r3.A00     // Catch:{ 0SV -> 0x0569 }
            long r3 = r3.A0W     // Catch:{ 0SV -> 0x0569 }
            X.0CB r6 = r0.A0D     // Catch:{ 0SV -> 0x0569 }
            X.0C8 r6 = r6.A00     // Catch:{ 0SV -> 0x0569 }
            X.0At r6 = r6.A09     // Catch:{ 0SV -> 0x0569 }
            long r16 = r6.A02()     // Catch:{ 0SV -> 0x0569 }
            X.0CB r6 = r0.A0D     // Catch:{ 0SV -> 0x0569 }
            X.0C8 r6 = r6.A00     // Catch:{ 0SV -> 0x0569 }
            android.net.NetworkInfo r12 = r6.A0X     // Catch:{ 0SV -> 0x0569 }
            java.lang.String r8 = "mqtt_dns_lookup_duration"
            java.lang.String r21 = "timespan_ms"
            java.lang.String r14 = java.lang.String.valueOf(r18)     // Catch:{ 0SV -> 0x0569 }
            r6 = r21
            java.lang.String[] r6 = new java.lang.String[]{r6, r14}     // Catch:{ 0SV -> 0x0569 }
            java.util.Map r6 = X.C01740Bl.A00(r6)     // Catch:{ 0SV -> 0x0569 }
            java.lang.String r4 = java.lang.Long.toString(r3)     // Catch:{ 0SV -> 0x0569 }
            java.lang.String r3 = "mqtt_session_id"
            r6.put(r3, r4)     // Catch:{ 0SV -> 0x0569 }
            java.lang.String r4 = java.lang.Long.toString(r16)     // Catch:{ 0SV -> 0x0569 }
            java.lang.String r3 = "network_session_id"
            r6.put(r3, r4)     // Catch:{ 0SV -> 0x0569 }
            X.AnonymousClass0B6.A00(r13, r6, r12)     // Catch:{ 0SV -> 0x0569 }
            r13.A07(r8, r6)     // Catch:{ 0SV -> 0x0569 }
            long r19 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x04e2 }
            java.lang.Integer r18 = X.AnonymousClass07B.A00     // Catch:{ IOException -> 0x04e2 }
            r8 = 0
            java.util.List r4 = r2.A00()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r3 = 0
            java.lang.Object r6 = r4.get(r3)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.net.InetAddress r6 = (java.net.InetAddress) r6     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.util.List r4 = r2.A00()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.lang.Object r3 = r4.get(r3)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.net.InetAddress r3 = (java.net.InetAddress) r3     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.lang.Class r12 = r3.getClass()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r4 = 1
        L_0x0098:
            java.util.List r3 = r2.A00()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            int r3 = r3.size()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            if (r4 >= r3) goto L_0x00c4
            java.util.List r3 = r2.A00()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.lang.Object r3 = r3.get(r4)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.net.InetAddress r3 = (java.net.InetAddress) r3     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.lang.Class r3 = r3.getClass()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            boolean r3 = r3.equals(r12)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            if (r3 != 0) goto L_0x00c1
            java.util.List r3 = r2.A00()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.lang.Object r4 = r3.get(r4)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.net.InetAddress r4 = (java.net.InetAddress) r4     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            goto L_0x00c5
        L_0x00c1:
            int r4 = r4 + 1
            goto L_0x0098
        L_0x00c4:
            r4 = 0
        L_0x00c5:
            if (r7 == 0) goto L_0x01bb
            long r16 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            X.0BD r3 = r0.A07     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            X.0Pp r14 = new X.0Pp     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.util.concurrent.ExecutorService r12 = r3.A01     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            javax.net.SocketFactory r7 = javax.net.ssl.SSLSocketFactory.getDefault()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            javax.net.ssl.SSLSocketFactory r7 = (javax.net.ssl.SSLSocketFactory) r7     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            X.0BC r3 = r3.A00     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r14.<init>(r12, r7, r3)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            if (r4 == 0) goto L_0x011f
            X.0C6 r3 = r0.A09     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            int r7 = r3.A09     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            int r12 = r7 * 1000
            r24 = r6
            r25 = r4
            r28 = r14
            X.0S5 r23 = new X.0S5     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.util.concurrent.ScheduledExecutorService r7 = r0.A0B     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            int r3 = r3.A06     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r26 = r11
            r27 = r12
            r29 = r7
            r30 = r3
            r23.<init>(r24, r25, r26, r27, r28, r29, r30)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.net.Socket r7 = r23.A01()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            boolean r3 = r7.isConnected()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            if (r3 == 0) goto L_0x0144
            java.net.InetAddress r3 = r7.getInetAddress()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            boolean r3 = r6.equals(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            if (r3 == 0) goto L_0x0112
            java.lang.Integer r18 = X.AnonymousClass07B.A0Y     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            goto L_0x0144
        L_0x0112:
            java.net.InetAddress r3 = r7.getInetAddress()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            boolean r3 = r4.equals(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            if (r3 == 0) goto L_0x0144
            java.lang.Integer r18 = X.AnonymousClass07B.A0i     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            goto L_0x0144
        L_0x011f:
            X.0C6 r3 = r0.A09     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            int r3 = r3.A09     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            int r4 = r3 * 1000
            java.net.Socket r7 = new java.net.Socket     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r7.<init>()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r7.setTcpNoDelay(r9)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r3 = 0
            r7.setSoTimeout(r3)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r7.setKeepAlive(r3)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            java.net.InetSocketAddress r3 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r3.<init>(r6, r11)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r7.connect(r3, r4)     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            boolean r3 = r7.isConnected()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            if (r3 == 0) goto L_0x0144
            java.lang.Integer r18 = X.AnonymousClass07B.A01     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
        L_0x0144:
            X.0C6 r3 = r0.A09     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            int r3 = r3.A09     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            int r3 = r3 * 1000
            long r3 = (long) r3     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            long r12 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            long r12 = r12 - r16
            long r3 = r3 - r12
            r12 = 0
            int r6 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r6 <= 0) goto L_0x01ab
            java.lang.String r13 = "handshakeAndVerifySocket failed because of "
            r16 = 0
            int r6 = (r3 > r16 ? 1 : (r3 == r16 ? 0 : -1))
            if (r6 <= 0) goto L_0x01b3
            java.util.concurrent.ExecutorService r12 = r14.A00     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            X.0ST r6 = new X.0ST     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r6.<init>(r14, r7, r5, r11)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r14 = -1283761990(0xffffffffb37b58ba, float:-5.8521188E-8)
            java.util.concurrent.Future r12 = X.AnonymousClass07A.A03(r12, r6, r14)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0195, ExecutionException -> 0x017f, TimeoutException -> 0x0177 }
            java.lang.Object r6 = r12.get(r3, r6)     // Catch:{ InterruptedException -> 0x0195, ExecutionException -> 0x017f, TimeoutException -> 0x0177 }
            java.net.Socket r6 = (java.net.Socket) r6     // Catch:{ InterruptedException -> 0x0195, ExecutionException -> 0x017f, TimeoutException -> 0x0177 }
            goto L_0x01d9
        L_0x0177:
            java.net.SocketTimeoutException r4 = new java.net.SocketTimeoutException     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.lang.String r3 = "handshakeAndVerifySocket timeout"
            r4.<init>(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            goto L_0x01ba
        L_0x017f:
            r4 = move-exception
            java.io.IOException r5 = new java.io.IOException     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r3.<init>()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r3.append(r13)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r3.append(r4)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r5.<init>(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            goto L_0x01aa
        L_0x0195:
            r4 = move-exception
            java.io.IOException r5 = new java.io.IOException     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r3.<init>()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r3.append(r13)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r3.append(r4)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r5.<init>(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
        L_0x01aa:
            throw r5     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
        L_0x01ab:
            java.net.SocketTimeoutException r4 = new java.net.SocketTimeoutException     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.lang.String r3 = "connectSocket already timeout"
            r4.<init>(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            goto L_0x01ba
        L_0x01b3:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.lang.String r3 = "non-positive timeout value"
            r4.<init>(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
        L_0x01ba:
            throw r4     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
        L_0x01bb:
            java.net.Socket r7 = new java.net.Socket     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r7.<init>()     // Catch:{ IOException -> 0x048c, all -> 0x0489 }
            r7.setTcpNoDelay(r9)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r3 = 0
            r7.setSoTimeout(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r7.setKeepAlive(r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r4.<init>(r6, r11)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            X.0C6 r3 = r0.A09     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            int r3 = r3.A09     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            int r3 = r3 * 1000
            r7.connect(r4, r3)     // Catch:{ IOException -> 0x048f, all -> 0x0496 }
            r6 = r7
        L_0x01d9:
            X.0B6 r12 = r0.A06     // Catch:{ IOException -> 0x04e2 }
            long r24 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x04e2 }
            long r24 = r24 - r19
            java.lang.String r27 = A00(r18)     // Catch:{ IOException -> 0x04e2 }
            if (r8 != 0) goto L_0x020d
            X.0Bc r7 = X.C01660Bc.A00     // Catch:{ IOException -> 0x04e2 }
        L_0x01e9:
            X.0CB r3 = r0.A0D     // Catch:{ IOException -> 0x04e2 }
            X.0C8 r3 = r3.A00     // Catch:{ IOException -> 0x04e2 }
            long r3 = r3.A0W     // Catch:{ IOException -> 0x04e2 }
            X.0CB r8 = r0.A0D     // Catch:{ IOException -> 0x04e2 }
            X.0C8 r8 = r8.A00     // Catch:{ IOException -> 0x04e2 }
            X.0At r8 = r8.A09     // Catch:{ IOException -> 0x04e2 }
            long r31 = r8.A02()     // Catch:{ IOException -> 0x04e2 }
            X.0CB r8 = r0.A0D     // Catch:{ IOException -> 0x04e2 }
            X.0C8 r8 = r8.A00     // Catch:{ IOException -> 0x04e2 }
            android.net.NetworkInfo r8 = r8.A0X     // Catch:{ IOException -> 0x04e2 }
            r23 = r12
            r26 = r11
            r28 = r7
            r29 = r3
            r33 = r8
            r23.A01(r24, r26, r27, r28, r29, r31, r33)     // Catch:{ IOException -> 0x04e2 }
            goto L_0x0213
        L_0x020d:
            X.0Ar r7 = new X.0Ar     // Catch:{ IOException -> 0x04e2 }
            r7.<init>(r8)     // Catch:{ IOException -> 0x04e2 }
            goto L_0x01e9
        L_0x0213:
            if (r6 == 0) goto L_0x0221
            java.net.InetAddress r3 = r6.getLocalAddress()     // Catch:{ IOException -> 0x04e2 }
            r0.A03 = r3     // Catch:{ IOException -> 0x04e2 }
            java.net.InetAddress r3 = r6.getInetAddress()     // Catch:{ IOException -> 0x04e2 }
            r0.A04 = r3     // Catch:{ IOException -> 0x04e2 }
        L_0x0221:
            if (r6 != 0) goto L_0x0228
            X.0BE r3 = r0.A08     // Catch:{ all -> 0x05f3 }
            r3.A01(r2)     // Catch:{ all -> 0x05f3 }
        L_0x0228:
            X.0CB r7 = r0.A0D     // Catch:{ all -> 0x05f3 }
            if (r6 == 0) goto L_0x0238
            java.lang.String r4 = "SSL"
        L_0x022e:
            java.lang.String r3 = ""
            r7.A06(r4, r3)     // Catch:{ all -> 0x05f3 }
            X.AnonymousClass0A1.A00(r6)     // Catch:{ all -> 0x05f3 }
            monitor-enter(r0)     // Catch:{ all -> 0x05f3 }
            goto L_0x023b
        L_0x0238:
            java.lang.String r4 = "SSL-failed"
            goto L_0x022e
        L_0x023b:
            boolean r3 = r0.A0E     // Catch:{ all -> 0x0485 }
            if (r3 == 0) goto L_0x0250
            java.lang.String r3 = "DefaultMqttClientCore"
            java.lang.String r2 = "connection/connecting/aborted before sending connect"
            X.C010708t.A0J(r3, r2)     // Catch:{ all -> 0x0485 }
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x0485 }
            X.0SB r2 = X.AnonymousClass0SB.A0G     // Catch:{ all -> 0x0485 }
            r4.<init>(r2)     // Catch:{ all -> 0x0485 }
            monitor-exit(r0)     // Catch:{ all -> 0x0485 }
            goto L_0x0589
        L_0x0250:
            r0.A0E = r9     // Catch:{ all -> 0x0485 }
            monitor-exit(r0)     // Catch:{ all -> 0x0485 }
            X.0Ri r8 = new X.0Ri     // Catch:{ all -> 0x0478 }
            X.0Rg r3 = new X.0Rg     // Catch:{ all -> 0x0478 }
            r3.<init>()     // Catch:{ all -> 0x0478 }
            X.0B6 r7 = r0.A06     // Catch:{ all -> 0x0478 }
            X.0C6 r3 = r0.A09     // Catch:{ all -> 0x0478 }
            int r4 = r3.A08     // Catch:{ all -> 0x0478 }
            X.0CA r3 = r0.A0C     // Catch:{ all -> 0x0478 }
            r8.<init>(r7, r4, r3)     // Catch:{ all -> 0x0478 }
            X.0Rh r7 = new X.0Rh     // Catch:{ all -> 0x0478 }
            X.0C6 r3 = r0.A09     // Catch:{ all -> 0x0478 }
            int r9 = r3.A08     // Catch:{ all -> 0x0478 }
            X.0AY r4 = r0.A0A     // Catch:{ all -> 0x0478 }
            X.0CA r3 = r0.A0C     // Catch:{ all -> 0x0478 }
            r7.<init>(r9, r4, r3)     // Catch:{ all -> 0x0478 }
            java.io.DataInputStream r4 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0464 }
            java.io.InputStream r3 = r6.getInputStream()     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0464 }
            r8.A00 = r4     // Catch:{ IOException -> 0x0464 }
            java.io.DataOutputStream r9 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0464 }
            java.io.BufferedOutputStream r4 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0464 }
            java.io.OutputStream r3 = r6.getOutputStream()     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0464 }
            r9.<init>(r4)     // Catch:{ IOException -> 0x0464 }
            r7.A00 = r9     // Catch:{ IOException -> 0x0464 }
            X.0C6 r3 = r0.A09     // Catch:{ IOException -> 0x0464 }
            int r3 = r3.A03     // Catch:{ IOException -> 0x0464 }
            int r3 = r3 * 1000
            r6.setSoTimeout(r3)     // Catch:{ IOException -> 0x0464 }
            java.lang.String r11 = "DefaultMqttClientCore"
            long r16 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x0464 }
            X.0Ck r4 = new X.0Ck     // Catch:{ IOException -> 0x03d6 }
            X.0CL r3 = X.AnonymousClass0CL.A03     // Catch:{ IOException -> 0x03d6 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x03d6 }
            X.0Rv r3 = new X.0Rv     // Catch:{ IOException -> 0x03d6 }
            r26 = r10 ^ 1
            r27 = 1
            r24 = 3
            r25 = 1
            r23 = r3
            r28 = r15
            r23.<init>(r24, r25, r26, r27, r28)     // Catch:{ IOException -> 0x03d6 }
            X.0P6 r10 = new X.0P6     // Catch:{ IOException -> 0x03d6 }
            r9 = r34
            r10.<init>(r4, r3, r9)     // Catch:{ IOException -> 0x03d6 }
            X.AnonymousClass0PK.A00(r0, r7, r10)     // Catch:{ IOException -> 0x03d6 }
            X.0Cn r14 = r8.A00()     // Catch:{ InterruptedIOException -> 0x03cd, IOException -> 0x03c4, JSONException -> 0x03bb, DataFormatException -> 0x03df }
            X.0Ck r3 = r14.A00     // Catch:{ IOException -> 0x0464 }
            X.0CL r4 = r3.A03     // Catch:{ IOException -> 0x0464 }
            X.0CL r3 = X.AnonymousClass0CL.A02     // Catch:{ IOException -> 0x0464 }
            if (r4 == r3) goto L_0x02dc
            java.lang.Object[] r4 = new java.lang.Object[]{r4}     // Catch:{ IOException -> 0x0464 }
            java.lang.String r3 = "receive/unexpected; type=%s"
            X.C010708t.A0P(r11, r3, r4)     // Catch:{ IOException -> 0x0464 }
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A0B     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x02dc:
            X.0B6 r13 = r0.A06     // Catch:{ IOException -> 0x0464 }
            X.0CL r3 = X.AnonymousClass0CL.A03     // Catch:{ IOException -> 0x0464 }
            java.lang.String r12 = r3.toString()     // Catch:{ IOException -> 0x0464 }
            long r19 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x0464 }
            long r19 = r19 - r16
            X.0CB r3 = r0.A0D     // Catch:{ IOException -> 0x0464 }
            X.0C8 r3 = r3.A00     // Catch:{ IOException -> 0x0464 }
            long r15 = r3.A0W     // Catch:{ IOException -> 0x0464 }
            X.0CB r3 = r0.A0D     // Catch:{ IOException -> 0x0464 }
            X.0C8 r3 = r3.A00     // Catch:{ IOException -> 0x0464 }
            X.0At r3 = r3.A09     // Catch:{ IOException -> 0x0464 }
            long r17 = r3.A02()     // Catch:{ IOException -> 0x0464 }
            X.0CB r3 = r0.A0D     // Catch:{ IOException -> 0x0464 }
            X.0C8 r3 = r3.A00     // Catch:{ IOException -> 0x0464 }
            android.net.NetworkInfo r10 = r3.A0X     // Catch:{ IOException -> 0x0464 }
            java.lang.String r9 = "operation"
            java.lang.String r4 = java.lang.String.valueOf(r19)     // Catch:{ IOException -> 0x0464 }
            r3 = r21
            java.lang.String[] r3 = new java.lang.String[]{r9, r12, r3, r4}     // Catch:{ IOException -> 0x0464 }
            java.util.Map r4 = X.C01740Bl.A00(r3)     // Catch:{ IOException -> 0x0464 }
            java.lang.String r9 = java.lang.Long.toString(r15)     // Catch:{ IOException -> 0x0464 }
            java.lang.String r3 = "mqtt_session_id"
            r4.put(r3, r9)     // Catch:{ IOException -> 0x0464 }
            java.lang.String r9 = java.lang.Long.toString(r17)     // Catch:{ IOException -> 0x0464 }
            java.lang.String r3 = "network_session_id"
            r4.put(r3, r9)     // Catch:{ IOException -> 0x0464 }
            X.AnonymousClass0B6.A00(r13, r4, r10)     // Catch:{ IOException -> 0x0464 }
            java.lang.String r3 = "mqtt_response_time"
            r13.A07(r3, r4)     // Catch:{ IOException -> 0x0464 }
            X.0P9 r14 = (X.AnonymousClass0P9) r14     // Catch:{ IOException -> 0x0464 }
            X.0Rw r3 = r14.A03()     // Catch:{ IOException -> 0x0464 }
            byte r9 = r3.A00     // Catch:{ IOException -> 0x0464 }
            if (r9 == 0) goto L_0x037a
            java.lang.Byte r3 = java.lang.Byte.valueOf(r9)     // Catch:{ IOException -> 0x0464 }
            java.lang.Object[] r4 = new java.lang.Object[]{r3}     // Catch:{ IOException -> 0x0464 }
            java.lang.String r3 = "connection/refused; rc=%s"
            X.C010708t.A0P(r11, r3, r4)     // Catch:{ IOException -> 0x0464 }
            r3 = 17
            if (r9 != r3) goto L_0x034e
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A05     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x034e:
            r3 = 5
            if (r9 != r3) goto L_0x035a
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A04     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x035a:
            r3 = 4
            if (r9 != r3) goto L_0x0366
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A03     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x0366:
            r3 = 19
            if (r9 != r3) goto L_0x0372
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A06     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x0372:
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A02     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x037a:
            X.0CQ r12 = r14.A02()     // Catch:{ IOException -> 0x0464 }
            java.lang.String r3 = r12.A03     // Catch:{ IOException -> 0x0464 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ IOException -> 0x0464 }
            if (r3 != 0) goto L_0x03b8
            java.lang.String r3 = r12.A04     // Catch:{ IOException -> 0x0464 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ IOException -> 0x0464 }
            if (r3 != 0) goto L_0x03b8
            X.0C5 r11 = new X.0C5     // Catch:{ IOException -> 0x0464 }
            java.lang.String r10 = r12.A03     // Catch:{ IOException -> 0x0464 }
            java.lang.String r9 = r12.A04     // Catch:{ IOException -> 0x0464 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0464 }
            r11.<init>(r10, r9, r3)     // Catch:{ IOException -> 0x0464 }
        L_0x039b:
            X.0CB r4 = r0.A0D     // Catch:{ IOException -> 0x0464 }
            java.lang.String r3 = r12.A05     // Catch:{ IOException -> 0x0464 }
            r4.A05(r3)     // Catch:{ IOException -> 0x0464 }
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            java.lang.String r9 = r12.A01     // Catch:{ IOException -> 0x0464 }
            if (r9 != 0) goto L_0x03aa
            java.lang.String r9 = ""
        L_0x03aa:
            java.lang.String r3 = r12.A02     // Catch:{ IOException -> 0x0464 }
            if (r3 != 0) goto L_0x03b0
            java.lang.String r3 = ""
        L_0x03b0:
            X.0Bu r3 = X.C01830Bu.A00(r9, r3)     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r11)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x03b8:
            X.0C5 r11 = X.AnonymousClass0C5.A01     // Catch:{ IOException -> 0x0464 }
            goto L_0x039b
        L_0x03bb:
            r9 = move-exception
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A01     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x03c4:
            r9 = move-exception
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A01     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x03cd:
            r9 = move-exception
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A0C     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x03d6:
            r9 = move-exception
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A07     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
            goto L_0x03ec
        L_0x03df:
            r9 = move-exception
            java.lang.String r3 = "exception/compression_error"
            X.C010708t.A0T(r11, r9, r3)     // Catch:{ IOException -> 0x0464 }
            X.0CR r4 = new X.0CR     // Catch:{ IOException -> 0x0464 }
            X.0SB r3 = X.AnonymousClass0SB.A01     // Catch:{ IOException -> 0x0464 }
            r4.<init>(r3, r9)     // Catch:{ IOException -> 0x0464 }
        L_0x03ec:
            r3 = r22
            r6.setSoTimeout(r3)     // Catch:{ IOException -> 0x0464 }
            boolean r3 = r4.A05     // Catch:{ all -> 0x0478 }
            if (r3 != 0) goto L_0x0401
            if (r6 == 0) goto L_0x03fa
            r6.close()     // Catch:{ IOException -> 0x03fa }
        L_0x03fa:
            X.0BE r0 = r0.A08     // Catch:{ all -> 0x05f3 }
            r0.A01(r2)     // Catch:{ all -> 0x05f3 }
            goto L_0x0589
        L_0x0401:
            monitor-enter(r0)     // Catch:{ all -> 0x0478 }
            X.0CB r3 = r0.A0D     // Catch:{ all -> 0x0461 }
            X.089 r9 = X.AnonymousClass089.DISCONNECTED     // Catch:{ all -> 0x0461 }
            X.0C8 r3 = r3.A00     // Catch:{ all -> 0x0461 }
            X.089 r3 = r3.A0Z     // Catch:{ all -> 0x0461 }
            boolean r3 = r3.equals(r9)     // Catch:{ all -> 0x0461 }
            if (r3 == 0) goto L_0x0426
            java.lang.String r4 = "DefaultMqttClientCore"
            java.lang.String r3 = "connection/connecting/unexpected_disconnect"
            X.C010708t.A0J(r4, r3)     // Catch:{ all -> 0x0461 }
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x0461 }
            X.0SB r3 = X.AnonymousClass0SB.A0H     // Catch:{ all -> 0x0461 }
            r4.<init>(r3)     // Catch:{ all -> 0x0461 }
            monitor-exit(r0)     // Catch:{ all -> 0x0461 }
            if (r6 == 0) goto L_0x0584
            r6.close()     // Catch:{ IOException -> 0x0584 }
            goto L_0x0584
        L_0x0426:
            r0.A02 = r5     // Catch:{ all -> 0x0461 }
            r0.A05 = r6     // Catch:{ all -> 0x0461 }
            r0.A01 = r7     // Catch:{ all -> 0x0461 }
            r0.A00 = r8     // Catch:{ all -> 0x0461 }
            X.0CB r5 = r0.A0D     // Catch:{ all -> 0x0461 }
            X.089 r3 = X.AnonymousClass089.CONNECTED     // Catch:{ all -> 0x0461 }
            r5.A03(r3)     // Catch:{ all -> 0x0461 }
            monitor-exit(r0)     // Catch:{ all -> 0x0461 }
            X.0CB r3 = r0.A0D     // Catch:{ all -> 0x0478 }
            r3.A00()     // Catch:{ all -> 0x0478 }
            X.0BE r9 = r0.A08     // Catch:{ all -> 0x05f3 }
            monitor-enter(r9)     // Catch:{ all -> 0x05f3 }
            X.0BG r0 = r9.A00     // Catch:{ all -> 0x05f0 }
            X.0Ry r8 = r0.A00(r2)     // Catch:{ all -> 0x05f0 }
            if (r8 == 0) goto L_0x045e
            X.0BG r7 = r9.A00     // Catch:{ all -> 0x05f0 }
            X.0Ry r6 = new X.0Ry     // Catch:{ all -> 0x05f0 }
            java.lang.String r5 = r8.A02     // Catch:{ all -> 0x05f0 }
            java.util.List r3 = r8.A00()     // Catch:{ all -> 0x05f0 }
            int r2 = r8.A01     // Catch:{ all -> 0x05f0 }
            r0 = 0
            r6.<init>(r5, r3, r2, r0)     // Catch:{ all -> 0x05f0 }
            r7.A04(r8, r6)     // Catch:{ all -> 0x05f0 }
            X.0BG r0 = r9.A00     // Catch:{ all -> 0x05f0 }
            r0.A02()     // Catch:{ all -> 0x05f0 }
        L_0x045e:
            monitor-exit(r9)     // Catch:{ all -> 0x05f3 }
            goto L_0x0589
        L_0x0461:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0461 }
            throw r3     // Catch:{ all -> 0x0478 }
        L_0x0464:
            r5 = move-exception
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x0478 }
            X.0SB r3 = X.AnonymousClass0SB.A08     // Catch:{ all -> 0x0478 }
            r4.<init>(r3, r5)     // Catch:{ all -> 0x0478 }
            if (r6 == 0) goto L_0x0471
            r6.close()     // Catch:{ IOException -> 0x0471 }
        L_0x0471:
            X.0BE r0 = r0.A08     // Catch:{ all -> 0x05f3 }
            r0.A01(r2)     // Catch:{ all -> 0x05f3 }
            goto L_0x0589
        L_0x0478:
            r4 = move-exception
            if (r6 == 0) goto L_0x047e
            r6.close()     // Catch:{ IOException -> 0x047e }
        L_0x047e:
            X.0BE r0 = r0.A08     // Catch:{ all -> 0x05f3 }
            r0.A01(r2)     // Catch:{ all -> 0x05f3 }
            goto L_0x0540
        L_0x0485:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0485 }
            goto L_0x0540
        L_0x0489:
            r10 = move-exception
            r7 = r8
            goto L_0x049b
        L_0x048c:
            r3 = move-exception
            r7 = r8
            goto L_0x0490
        L_0x048f:
            r3 = move-exception
        L_0x0490:
            if (r7 == 0) goto L_0x0498
            r7.close()     // Catch:{ IOException -> 0x0498, all -> 0x0496 }
            goto L_0x0498
        L_0x0496:
            r10 = move-exception
            goto L_0x049b
        L_0x0498:
            throw r3     // Catch:{ all -> 0x0499 }
        L_0x0499:
            r10 = move-exception
            r8 = r3
        L_0x049b:
            X.0B6 r9 = r0.A06     // Catch:{ IOException -> 0x04e2 }
            long r13 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x04e2 }
            long r13 = r13 - r19
            java.lang.String r16 = A00(r18)     // Catch:{ IOException -> 0x04e2 }
            if (r8 != 0) goto L_0x04cd
            X.0Bc r6 = X.C01660Bc.A00     // Catch:{ IOException -> 0x04e2 }
        L_0x04ab:
            X.0CB r3 = r0.A0D     // Catch:{ IOException -> 0x04e2 }
            X.0C8 r3 = r3.A00     // Catch:{ IOException -> 0x04e2 }
            long r3 = r3.A0W     // Catch:{ IOException -> 0x04e2 }
            X.0CB r5 = r0.A0D     // Catch:{ IOException -> 0x04e2 }
            X.0C8 r5 = r5.A00     // Catch:{ IOException -> 0x04e2 }
            X.0At r5 = r5.A09     // Catch:{ IOException -> 0x04e2 }
            long r20 = r5.A02()     // Catch:{ IOException -> 0x04e2 }
            X.0CB r5 = r0.A0D     // Catch:{ IOException -> 0x04e2 }
            X.0C8 r5 = r5.A00     // Catch:{ IOException -> 0x04e2 }
            android.net.NetworkInfo r5 = r5.A0X     // Catch:{ IOException -> 0x04e2 }
            r12 = r9
            r15 = r11
            r17 = r6
            r18 = r3
            r22 = r5
            r12.A01(r13, r15, r16, r17, r18, r20, r22)     // Catch:{ IOException -> 0x04e2 }
            goto L_0x04d3
        L_0x04cd:
            X.0Ar r6 = new X.0Ar     // Catch:{ IOException -> 0x04e2 }
            r6.<init>(r8)     // Catch:{ IOException -> 0x04e2 }
            goto L_0x04ab
        L_0x04d3:
            if (r7 == 0) goto L_0x04e1
            java.net.InetAddress r3 = r7.getLocalAddress()     // Catch:{ IOException -> 0x04e2 }
            r0.A03 = r3     // Catch:{ IOException -> 0x04e2 }
            java.net.InetAddress r3 = r7.getInetAddress()     // Catch:{ IOException -> 0x04e2 }
            r0.A04 = r3     // Catch:{ IOException -> 0x04e2 }
        L_0x04e1:
            throw r10     // Catch:{ IOException -> 0x04e2 }
        L_0x04e2:
            r5 = move-exception
            boolean r3 = r5 instanceof java.net.SocketTimeoutException     // Catch:{ all -> 0x0531 }
            if (r3 == 0) goto L_0x04ef
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x0531 }
            X.0SB r3 = X.AnonymousClass0SB.A0F     // Catch:{ all -> 0x0531 }
            r4.<init>(r3, r5)     // Catch:{ all -> 0x0531 }
            goto L_0x0522
        L_0x04ef:
            java.lang.String r4 = r5.getMessage()     // Catch:{ all -> 0x0531 }
            if (r4 == 0) goto L_0x050e
            java.lang.String r3 = "Could not validate certificate: current time"
            boolean r3 = r4.contains(r3)     // Catch:{ all -> 0x0531 }
            if (r3 == 0) goto L_0x050e
            java.lang.String r3 = "validation time: Thu Aug 28"
            boolean r3 = r4.contains(r3)     // Catch:{ all -> 0x0531 }
            if (r3 != 0) goto L_0x0510
            java.lang.String r3 = "validation time: Wed Aug 27"
            boolean r3 = r4.contains(r3)     // Catch:{ all -> 0x0531 }
            if (r3 == 0) goto L_0x050e
            goto L_0x0510
        L_0x050e:
            r3 = 0
            goto L_0x0511
        L_0x0510:
            r3 = 1
        L_0x0511:
            if (r3 == 0) goto L_0x051b
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x0531 }
            X.0SB r3 = X.AnonymousClass0SB.A0E     // Catch:{ all -> 0x0531 }
            r4.<init>(r3, r5)     // Catch:{ all -> 0x0531 }
            goto L_0x0522
        L_0x051b:
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x0531 }
            X.0SB r3 = X.AnonymousClass0SB.A0D     // Catch:{ all -> 0x0531 }
            r4.<init>(r3, r5)     // Catch:{ all -> 0x0531 }
        L_0x0522:
            X.0BE r3 = r0.A08     // Catch:{ all -> 0x05f3 }
            r3.A01(r2)     // Catch:{ all -> 0x05f3 }
            X.0CB r3 = r0.A0D     // Catch:{ all -> 0x05f3 }
            java.lang.String r2 = "SSL-failed"
            java.lang.String r0 = ""
            r3.A06(r2, r0)     // Catch:{ all -> 0x05f3 }
            goto L_0x0589
        L_0x0531:
            r4 = move-exception
            X.0BE r3 = r0.A08     // Catch:{ all -> 0x05f3 }
            r3.A01(r2)     // Catch:{ all -> 0x05f3 }
            X.0CB r3 = r0.A0D     // Catch:{ all -> 0x05f3 }
            java.lang.String r2 = "SSL-failed"
            java.lang.String r0 = ""
            r3.A06(r2, r0)     // Catch:{ all -> 0x05f3 }
        L_0x0540:
            throw r4     // Catch:{ all -> 0x05f3 }
        L_0x0541:
            X.0SV r2 = new X.0SV     // Catch:{ 0SV -> 0x0569 }
            X.0DA r0 = X.AnonymousClass0DA.A03     // Catch:{ 0SV -> 0x0569 }
            r2.<init>(r0)     // Catch:{ 0SV -> 0x0569 }
            goto L_0x0568
        L_0x0549:
            r2 = move-exception
            java.lang.Throwable r0 = r2.getCause()     // Catch:{ 0SV -> 0x0569 }
            boolean r0 = r0 instanceof X.AnonymousClass0SV     // Catch:{ 0SV -> 0x0569 }
            if (r0 == 0) goto L_0x0561
            java.lang.Throwable r0 = r2.getCause()     // Catch:{ 0SV -> 0x0569 }
            X.0SV r0 = (X.AnonymousClass0SV) r0     // Catch:{ 0SV -> 0x0569 }
            throw r0     // Catch:{ 0SV -> 0x0569 }
        L_0x0559:
            X.0SV r2 = new X.0SV     // Catch:{ 0SV -> 0x0569 }
            X.0DA r0 = X.AnonymousClass0DA.A01     // Catch:{ 0SV -> 0x0569 }
            r2.<init>(r0)     // Catch:{ 0SV -> 0x0569 }
            goto L_0x0568
        L_0x0561:
            X.0SV r2 = new X.0SV     // Catch:{ 0SV -> 0x0569 }
            X.0DA r0 = X.AnonymousClass0DA.A01     // Catch:{ 0SV -> 0x0569 }
            r2.<init>(r0)     // Catch:{ 0SV -> 0x0569 }
        L_0x0568:
            throw r2     // Catch:{ 0SV -> 0x0569 }
        L_0x0569:
            r3 = move-exception
            X.0DA r2 = X.AnonymousClass0DA.A03     // Catch:{ all -> 0x05f3 }
            X.0DA r0 = r3.mDNSResolveStatus     // Catch:{ all -> 0x05f3 }
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x05f3 }
            if (r0 == 0) goto L_0x057c
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x05f3 }
            X.0SB r0 = X.AnonymousClass0SB.A09     // Catch:{ all -> 0x05f3 }
            r4.<init>(r0, r3)     // Catch:{ all -> 0x05f3 }
            goto L_0x0589
        L_0x057c:
            X.0CR r4 = new X.0CR     // Catch:{ all -> 0x05f3 }
            X.0SB r0 = X.AnonymousClass0SB.A0A     // Catch:{ all -> 0x05f3 }
            r4.<init>(r0, r3)     // Catch:{ all -> 0x05f3 }
            goto L_0x0589
        L_0x0584:
            X.0BE r0 = r0.A08     // Catch:{ all -> 0x05f3 }
            r0.A01(r2)     // Catch:{ all -> 0x05f3 }
        L_0x0589:
            boolean r0 = r4.A05     // Catch:{ all -> 0x05f3 }
            if (r0 != 0) goto L_0x0592
            X.0PK r0 = r1.A02     // Catch:{ all -> 0x05f3 }
            r0.ASP()     // Catch:{ all -> 0x05f3 }
        L_0x0592:
            X.0PK r0 = r1.A02     // Catch:{ all -> 0x05f3 }
            X.0CB r0 = r0.A0D     // Catch:{ all -> 0x05f3 }
            r0.A02(r4)     // Catch:{ all -> 0x05f3 }
            X.0PK r5 = r1.A02     // Catch:{ all -> 0x05f3 }
        L_0x059b:
            monitor-enter(r5)     // Catch:{ all -> 0x05f3 }
            X.0CB r0 = r5.A0D     // Catch:{ all -> 0x05ed }
            X.089 r2 = X.AnonymousClass089.CONNECTED     // Catch:{ all -> 0x05ed }
            X.0C8 r0 = r0.A00     // Catch:{ all -> 0x05ed }
            X.089 r0 = r0.A0Z     // Catch:{ all -> 0x05ed }
            boolean r0 = r0.equals(r2)     // Catch:{ all -> 0x05ed }
            if (r0 != 0) goto L_0x05ac
            monitor-exit(r5)     // Catch:{ all -> 0x05ed }
            goto L_0x05e0
        L_0x05ac:
            X.0Ri r0 = r5.A00     // Catch:{ all -> 0x05ed }
            monitor-exit(r5)     // Catch:{ all -> 0x05ed }
            X.0Cn r2 = r0.A00()     // Catch:{ IOException -> 0x05c8, DataFormatException -> 0x05c6, JSONException -> 0x05b9 }
            X.0CB r0 = r5.A0D     // Catch:{ all -> 0x05f3 }
            r0.A04(r2)     // Catch:{ all -> 0x05f3 }
            goto L_0x059b
        L_0x05b9:
            r4 = move-exception
            X.0CB r3 = r5.A0D     // Catch:{ all -> 0x05f3 }
            X.0CE r2 = X.AnonymousClass0CE.A00(r4)     // Catch:{ all -> 0x05f3 }
            X.0CG r0 = X.AnonymousClass0CG.A02     // Catch:{ all -> 0x05f3 }
            r3.A01(r2, r0, r4)     // Catch:{ all -> 0x05f3 }
            goto L_0x05e0
        L_0x05c6:
            r4 = move-exception
            goto L_0x05d5
        L_0x05c8:
            r4 = move-exception
            X.0CB r3 = r5.A0D     // Catch:{ all -> 0x05f3 }
            X.0CE r2 = X.AnonymousClass0CE.A00(r4)     // Catch:{ all -> 0x05f3 }
            X.0CG r0 = X.AnonymousClass0CG.A02     // Catch:{ all -> 0x05f3 }
            r3.A01(r2, r0, r4)     // Catch:{ all -> 0x05f3 }
            goto L_0x05e0
        L_0x05d5:
            X.0CB r3 = r5.A0D     // Catch:{ all -> 0x05f3 }
            X.0CE r2 = X.AnonymousClass0CE.A00(r4)     // Catch:{ all -> 0x05f3 }
            X.0CG r0 = X.AnonymousClass0CG.A02     // Catch:{ all -> 0x05f3 }
            r3.A01(r2, r0, r4)     // Catch:{ all -> 0x05f3 }
        L_0x05e0:
            X.0CB r2 = r5.A0D     // Catch:{ all -> 0x05f3 }
            X.089 r0 = X.AnonymousClass089.DISCONNECTED     // Catch:{ all -> 0x05f3 }
            r2.A03(r0)     // Catch:{ all -> 0x05f3 }
            X.0CB r0 = r5.A0D     // Catch:{ all -> 0x05f3 }
            r0.A00()     // Catch:{ all -> 0x05f3 }
            return
        L_0x05ed:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x05ed }
            goto L_0x05f2
        L_0x05f0:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x05f3 }
        L_0x05f2:
            throw r0     // Catch:{ all -> 0x05f3 }
        L_0x05f3:
            r4 = move-exception
            java.lang.String r2 = "DefaultMqttClientCore"
            java.lang.String r0 = "exception/networkThreadLoop"
            X.C010708t.A0R(r2, r4, r0)
            X.0PK r0 = r1.A02
            X.0CB r0 = r0.A0D
            X.0C8 r0 = r0.A00
            X.0CC r3 = r0.A0Y
            if (r3 == 0) goto L_0x0614
            X.0AH r0 = r3.A02
            X.0C8 r2 = r0.A0n
            X.0C8 r0 = r3.A00
            if (r2 != r0) goto L_0x0614
            X.0AH r0 = r3.A02
            X.0AG r0 = r0.A0I
            r0.BSZ(r4)
        L_0x0614:
            X.0PK r0 = r1.A02
            X.0CB r3 = r0.A0D
            X.0CE r2 = X.AnonymousClass0CE.A0M
            X.0CG r0 = X.AnonymousClass0CG.A02
            r3.A01(r2, r0, r4)
            X.0PK r0 = r1.A02
            X.0CB r2 = r0.A0D
            java.lang.String r1 = "Mqtt Unhandled Exception"
            java.lang.String r0 = "Unhandled exception in Mqtt networkThreadLoop"
            r2.A07(r4, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0S8.run():void");
    }
}
