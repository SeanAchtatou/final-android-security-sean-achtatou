package X;

import java.io.IOException;

/* renamed from: X.0KQ  reason: invalid class name */
public final class AnonymousClass0KQ extends IOException {
    public AnonymousClass0KQ(String str) {
        super(str);
    }

    public AnonymousClass0KQ(String str, Throwable th) {
        super(str, th);
    }
}
