package X;

import java.io.File;
import java.util.Map;

/* renamed from: X.0er  reason: invalid class name and case insensitive filesystem */
public interface C08210er {
    Map getExtraFileFromWorkerThread(File file);

    String getName();

    boolean isMemoryIntensive();

    void prepareDataForWriting();

    boolean shouldSendAsync();
}
