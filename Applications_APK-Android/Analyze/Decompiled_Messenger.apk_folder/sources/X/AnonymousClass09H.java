package X;

import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.writer.NativeTraceWriterCallbacks;
import java.io.File;

/* renamed from: X.09H  reason: invalid class name */
public interface AnonymousClass09H extends C003804w, NativeTraceWriterCallbacks, C003904y {
    void BTJ();

    void BgU();

    void BkO();

    void BkP(int i);

    void Bsa(File file, long j);

    void Bsb(int i, int i2, int i3, int i4);

    void onTraceAbort(TraceContext traceContext);

    void onTraceStart(TraceContext traceContext);

    void onTraceStop(TraceContext traceContext);
}
