package X;

/* renamed from: X.0Gg  reason: invalid class name and case insensitive filesystem */
public final class C02760Gg extends AnonymousClass0FM {
    public final AnonymousClass04b mMetricsMap = new AnonymousClass04b();
    private final AnonymousClass04b mMetricsValid = new AnonymousClass04b();

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C02760Gg r5 = (C02760Gg) obj;
            if (!C02740Gd.A02(this.mMetricsValid, r5.mMetricsValid) || !C02740Gd.A02(this.mMetricsMap, r5.mMetricsMap)) {
                return false;
            }
        }
        return true;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A0A((C02760Gg) r1);
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0028, code lost:
        if (r8.A0E(r4) == false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0FM A07(X.AnonymousClass0FM r8, X.AnonymousClass0FM r9) {
        /*
            r7 = this;
            X.0Gg r8 = (X.C02760Gg) r8
            X.0Gg r9 = (X.C02760Gg) r9
            if (r9 == 0) goto L_0x0044
            if (r8 != 0) goto L_0x000c
            r9.A0A(r7)
        L_0x000b:
            return r9
        L_0x000c:
            X.04b r0 = r7.mMetricsMap
            int r6 = r0.size()
            r5 = 0
        L_0x0013:
            if (r5 >= r6) goto L_0x000b
            X.04b r0 = r7.mMetricsMap
            java.lang.Object r4 = r0.A07(r5)
            java.lang.Class r4 = (java.lang.Class) r4
            boolean r0 = r7.A0E(r4)
            if (r0 == 0) goto L_0x002a
            boolean r0 = r8.A0E(r4)
            r3 = 1
            if (r0 != 0) goto L_0x002b
        L_0x002a:
            r3 = 0
        L_0x002b:
            if (r3 == 0) goto L_0x003e
            X.0FM r2 = r9.A09(r4)
            if (r2 == 0) goto L_0x003e
            X.0FM r1 = r7.A09(r4)
            X.0FM r0 = r8.A09(r4)
            r1.A07(r0, r2)
        L_0x003e:
            r9.A0D(r4, r3)
            int r5 = r5 + 1
            goto L_0x0013
        L_0x0044:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "CompositeMetrics doesn't support nullable results"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02760Gg.A07(X.0FM, X.0FM):X.0FM");
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A08(AnonymousClass0FM r1, AnonymousClass0FM r2) {
        C02760Gg r22 = (C02760Gg) r2;
        A0B((C02760Gg) r1, r22);
        return r22;
    }

    public AnonymousClass0FM A09(Class cls) {
        return (AnonymousClass0FM) cls.cast(this.mMetricsMap.get(cls));
    }

    public void A0A(C02760Gg r7) {
        int size = this.mMetricsMap.size();
        for (int i = 0; i < size; i++) {
            Class cls = (Class) this.mMetricsMap.A07(i);
            AnonymousClass0FM A09 = r7.A09(cls);
            if (A09 != null) {
                A09(cls).A06(A09);
                A0D(cls, r7.A0E(cls));
            } else {
                A0D(cls, false);
            }
        }
    }

    public void A0B(C02760Gg r7, C02760Gg r8) {
        boolean z;
        if (r8 == null) {
            throw new IllegalArgumentException("CompositeMetrics doesn't support nullable results");
        } else if (r7 == null) {
            r8.A0A(this);
        } else {
            int size = this.mMetricsMap.size();
            for (int i = 0; i < size; i++) {
                Class cls = (Class) this.mMetricsMap.A07(i);
                if (A0E(cls) && r7.A0E(cls)) {
                    AnonymousClass0FM A09 = r8.A09(cls);
                    if (A09 != null) {
                        A09(cls).A08(r7.A09(cls), A09);
                    }
                } else if (A0E(cls)) {
                    r8.A09(cls).A06(A09(cls));
                } else if (r7.A0E(cls)) {
                    r8.A09(cls).A06(r7.A09(cls));
                } else {
                    z = false;
                    r8.A0D(cls, z);
                }
                z = true;
                r8.A0D(cls, z);
            }
        }
    }

    public void A0C(Class cls, AnonymousClass0FM r4) {
        this.mMetricsMap.put(cls, r4);
        this.mMetricsValid.put(cls, Boolean.FALSE);
    }

    public void A0D(Class cls, boolean z) {
        Boolean bool;
        AnonymousClass04b r1 = this.mMetricsValid;
        if (z) {
            bool = Boolean.TRUE;
        } else {
            bool = Boolean.FALSE;
        }
        r1.put(cls, bool);
    }

    public boolean A0E(Class cls) {
        Boolean bool = (Boolean) this.mMetricsValid.get(cls);
        if (bool == null || !bool.booleanValue()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.mMetricsMap.hashCode() * 31) + this.mMetricsValid.hashCode();
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("Composite Metrics{\n");
        int size = this.mMetricsMap.size();
        for (int i = 0; i < size; i++) {
            sb.append(this.mMetricsMap.A09(i));
            if (A0E((Class) this.mMetricsMap.A07(i))) {
                str = " [valid]";
            } else {
                str = " [invalid]";
            }
            sb.append(str);
            sb.append(10);
        }
        sb.append("}");
        return sb.toString();
    }
}
