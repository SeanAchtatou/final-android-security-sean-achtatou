package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0Td  reason: invalid class name */
public interface AnonymousClass0Td {
    public static final int[] A00 = {-1};

    AnonymousClass0Ti AsW();

    void BeF(C04270Tg r1);

    void BeL(C04270Tg r1, String str, String str2);

    void BeM(C04270Tg r1);

    void BeQ(C04270Tg r1, String str, AnonymousClass0lr r3, long j, boolean z, int i);

    void BeR(C04270Tg r1);

    void BeT(C04270Tg r1);

    void BeU(C04270Tg r1);

    void BeV(int i, int i2, C04270Tg r3);

    void BfD(PerformanceLoggingEvent performanceLoggingEvent);

    void Bkk(int i, int i2);

    boolean Bkl(int i, int i2);

    void CB4(QuickPerformanceLogger quickPerformanceLogger);
}
