package X;

import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0Bm  reason: invalid class name and case insensitive filesystem */
public final class C01750Bm {
    public final String A00;
    public final String A01;
    public final Map A02 = new HashMap();
    private final long A03;

    public JSONObject A00() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.A01);
            jSONObject.put("time", String.format(null, "%.3f", Double.valueOf(((double) this.A03) / 1000.0d)));
            jSONObject.putOpt("module", this.A00);
            if (!this.A02.isEmpty()) {
                JSONObject jSONObject2 = new JSONObject();
                for (Map.Entry entry : this.A02.entrySet()) {
                    jSONObject2.put((String) entry.getKey(), entry.getValue());
                }
                jSONObject.put("extra", jSONObject2);
                return jSONObject;
            }
        } catch (JSONException e) {
            C010708t.A0S("AnalyticsEvent", e, "Failed to serialize");
        }
        return jSONObject;
    }

    public C01750Bm(String str, String str2) {
        AnonymousClass0A1.A00(str);
        AnonymousClass0A1.A00(str2);
        this.A03 = System.currentTimeMillis();
        this.A01 = str;
        this.A00 = str2;
    }

    public void A01(Map map) {
        String obj;
        for (Map.Entry entry : map.entrySet()) {
            String obj2 = entry.getKey().toString();
            if (entry.getValue() == null) {
                obj = BuildConfig.FLAVOR;
            } else {
                obj = entry.getValue().toString();
            }
            this.A02.put(obj2, obj);
        }
    }

    public String toString() {
        return A00().toString();
    }
}
