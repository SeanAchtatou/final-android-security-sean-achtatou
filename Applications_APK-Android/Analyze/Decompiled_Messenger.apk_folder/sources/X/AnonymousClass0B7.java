package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.facebook.rti.common.time.RealtimeSinceBootClock;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0B7  reason: invalid class name */
public final class AnonymousClass0B7 {
    private static final Map A0K = new AnonymousClass0B8();
    public final AnonymousClass0B9 A00;
    public final ConcurrentMap A01 = new ConcurrentHashMap();
    private final Context A02;
    private final AnonymousClass0AW A03;
    private final RealtimeSinceBootClock A04;
    private final AnonymousClass0AS A05;
    private final C01690Bg A06;
    private final C01420Ad A07;
    private final C01570At A08;
    private final C01470Aj A09;
    private final String A0A;
    private final HashMap A0B;
    private final HashMap A0C;
    private final boolean A0D;
    public volatile C01770Bo A0E;
    public volatile String A0F = BuildConfig.FLAVOR;
    public volatile String A0G = BuildConfig.FLAVOR;
    public volatile String A0H = BuildConfig.FLAVOR;
    public volatile String A0I = BuildConfig.FLAVOR;
    public volatile String A0J = BuildConfig.FLAVOR;

    public static synchronized AtomicLong A04(AnonymousClass0B7 r2, C01650Bb r3) {
        AtomicLong atomicLong;
        synchronized (r2) {
            if (!r2.A0B.containsKey(r3)) {
                r2.A0B.put(r3, new AtomicLong());
            }
            atomicLong = (AtomicLong) r2.A0B.get(r3);
        }
        return atomicLong;
    }

    public synchronized C01870By A07(Class cls) {
        String name;
        Object obj;
        try {
            name = cls.getName();
            if (!this.A0C.containsKey(name)) {
                if (cls == C01880Bz.class) {
                    obj = new C01880Bz(this.A02, this.A0A, this.A05, this.A04, this.A0D);
                } else if (cls == AnonymousClass0C1.class) {
                    obj = new AnonymousClass0C1(this.A02, this.A0A, this.A05, this.A04, this.A0D);
                } else if (cls == AnonymousClass0C2.class) {
                    obj = new AnonymousClass0C2(this.A02, this.A0A, this.A05, this.A04, this.A0D);
                } else {
                    obj = (C01870By) cls.newInstance();
                }
                this.A0C.put(name, obj);
            }
        } catch (Exception e) {
            throw new RuntimeException("Incorrect stat category used:", e);
        }
        return (C01870By) this.A0C.get(name);
    }

    public static AnonymousClass0Mo A00(AnonymousClass0B7 r6) {
        String str;
        String str2;
        String upperCase;
        String str3;
        String upperCase2;
        String str4;
        String upperCase3;
        String str5;
        AnonymousClass0Mo r2 = (AnonymousClass0Mo) r6.A07(AnonymousClass0Mo.class);
        r2.A02(AnonymousClass0Q5.A0D, r6.A0A);
        r2.A02(AnonymousClass0Q5.A02, r6.A0F);
        r2.A02(AnonymousClass0Q5.A0A, r6.A0H);
        SharedPreferences A002 = AnonymousClass0B2.A00(r6.A02, AnonymousClass07B.A01);
        r2.A02(AnonymousClass0Q5.A0F, String.valueOf(A002.getInt("year_class", 0)));
        r2.A02(AnonymousClass0Q5.A07, A03(r6.A03.AbP(AnonymousClass07B.A0q).getAll()));
        r2.A02(AnonymousClass0Q5.A06, A03(AnonymousClass0B2.A00(r6.A02, AnonymousClass07B.A0Y).getAll()));
        C01690Bg r0 = r6.A06;
        if (r0 != null) {
            AnonymousClass0Q5 r1 = AnonymousClass0Q5.A01;
            if (((Boolean) r0.get()).booleanValue()) {
                str5 = "fg";
            } else {
                str5 = "bg";
            }
            r2.A02(r1, str5);
        }
        AnonymousClass0Q5 r12 = AnonymousClass0Q5.A0C;
        if (r6.A09.A00()) {
            str = "1";
        } else {
            str = "0";
        }
        r2.A02(r12, str);
        C01540Aq A003 = r6.A07.A00("phone", TelephonyManager.class);
        AnonymousClass0Q5 r13 = AnonymousClass0Q5.A03;
        if (A003.A02()) {
            str2 = ((TelephonyManager) A003.A01()).getNetworkCountryIso();
        } else {
            str2 = BuildConfig.FLAVOR;
        }
        if (str2 == null) {
            upperCase = null;
        } else {
            upperCase = str2.toUpperCase();
        }
        r2.A02(r13, upperCase);
        AnonymousClass0Q5 r3 = AnonymousClass0Q5.A09;
        NetworkInfo A042 = r6.A08.A04();
        if (A042 == null || TextUtils.isEmpty(A042.getTypeName())) {
            str3 = "none";
        } else {
            str3 = A042.getTypeName();
        }
        if (str3 == null) {
            upperCase2 = null;
        } else {
            upperCase2 = str3.toUpperCase();
        }
        r2.A02(r3, upperCase2);
        AnonymousClass0Q5 r32 = AnonymousClass0Q5.A08;
        NetworkInfo A043 = r6.A08.A04();
        if (A043 == null || TextUtils.isEmpty(A043.getSubtypeName())) {
            str4 = "none";
        } else {
            str4 = A043.getSubtypeName();
        }
        if (str4 == null) {
            upperCase3 = null;
        } else {
            upperCase3 = str4.toUpperCase();
        }
        r2.A02(r32, upperCase3);
        r2.A02(AnonymousClass0Q5.A05, Boolean.valueOf(A002.getBoolean("is_employee", false)));
        r2.A02(AnonymousClass0Q5.A0E, r6.A0J);
        r2.A02(AnonymousClass0Q5.A04, r6.A0G);
        r2.A02(AnonymousClass0Q5.A0B, r6.A0I);
        return r2;
    }

    private AnonymousClass08I A01(long j) {
        long A012;
        AnonymousClass08I r4 = (AnonymousClass08I) A07(AnonymousClass08I.class);
        ((AtomicLong) r4.A00(AnonymousClass08J.A09)).set(j);
        ((AtomicLong) r4.A00(AnonymousClass08J.A0B)).set(this.A08.A01());
        AtomicLong atomicLong = (AtomicLong) r4.A00(AnonymousClass08J.A0C);
        C01570At r5 = this.A08;
        synchronized (r5) {
            A012 = r5.A00 + r5.A01();
        }
        atomicLong.set(A012);
        ((AtomicLong) r4.A00(AnonymousClass08J.A0E)).set(SystemClock.elapsedRealtime() - A04(this, C01650Bb.ServiceCreatedTimestamp).get());
        return r4;
    }

    private static String A03(Map map) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry entry : map.entrySet()) {
            if (z) {
                z = false;
            } else {
                sb.append(";");
            }
            sb.append((String) entry.getKey());
            sb.append("|");
            sb.append(String.valueOf(entry.getValue()));
        }
        return sb.toString();
    }

    public C04140Sd A05(long j) {
        return new C04140Sd(A00(this), A01(j), null, (C02050Cq) A07(C02050Cq.class), null, null, null, null, true, false);
    }

    public C04140Sd A06(long j, boolean z) {
        return new C04140Sd(A00(this), A01(j), (C01850Bw) A07(C01850Bw.class), null, this.A00.A00(z), (C01880Bz) A07(C01880Bz.class), (AnonymousClass0C1) A07(AnonymousClass0C1.class), (AnonymousClass0C2) A07(AnonymousClass0C2.class), false, true);
    }

    public void A08(String str, String str2, String str3, boolean z) {
        boolean booleanValue;
        String str4;
        String str5;
        C01690Bg r0 = this.A06;
        if (r0 == null) {
            booleanValue = false;
        } else {
            booleanValue = ((Boolean) r0.get()).booleanValue();
        }
        boolean z2 = false;
        if (SystemClock.elapsedRealtime() - AnonymousClass0CN.A02.A00 > 17000) {
            z2 = true;
        }
        String str6 = AnonymousClass0CN.A02.A01;
        if (str6 != null && ((!z && AnonymousClass0CL.A05.name().equals(str)) || (z && AnonymousClass0CL.A06.name().equals(str)))) {
            str = AnonymousClass08S.A0P(str, "_", str6);
        }
        if (booleanValue) {
            str4 = "_FG";
        } else {
            str4 = "_BG";
        }
        String A0J2 = AnonymousClass08S.A0J(str, str4);
        String str7 = "fg";
        if (z2) {
            str5 = "rw";
            if (!booleanValue) {
                ((AnonymousClass0C1) A07(AnonymousClass0C1.class)).A03(1, "tc", "bg", str5, str3);
            }
            ((AnonymousClass0C1) A07(AnonymousClass0C1.class)).A03(1, "tc", str7, str5, str3);
        } else {
            str5 = "nw";
            if (!booleanValue) {
                ((AnonymousClass0C1) A07(AnonymousClass0C1.class)).A03(1, "tc", "bg", str5, str3);
            }
            ((AnonymousClass0C1) A07(AnonymousClass0C1.class)).A03(1, "tc", str7, str5, str3);
        }
        if (!booleanValue) {
            str7 = "bg";
        }
        if (!TextUtils.isEmpty(str2)) {
            if (str2.startsWith("/")) {
                A0J2 = str2.substring(1);
            } else {
                A0J2 = str2;
            }
        }
        ((AnonymousClass0C2) A07(AnonymousClass0C2.class)).A03(1, A0J2, str7);
        AnonymousClass0CN.A02.A00 = SystemClock.elapsedRealtime();
    }

    public static String A02(List list) {
        ListIterator listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            String str = (String) listIterator.next();
            if (A0K.containsKey(str)) {
                listIterator.set(String.valueOf(A0K.get(str)));
            } else {
                C010708t.A0P("MqttHealthStatsHelper", "appPkgName %s not found in encoding map", str);
            }
        }
        return TextUtils.join(";", list);
    }

    public AnonymousClass0B7(Context context, C01420Ad r3, String str, C01570At r5, C01470Aj r6, RealtimeSinceBootClock realtimeSinceBootClock, AnonymousClass0AS r8, C01690Bg r9, boolean z, AnonymousClass0AW r11) {
        this.A02 = context;
        this.A07 = r3;
        this.A0A = str;
        this.A08 = r5;
        this.A09 = r6;
        this.A00 = new AnonymousClass0B9();
        this.A05 = r8;
        this.A04 = realtimeSinceBootClock;
        this.A06 = r9;
        this.A0B = new HashMap();
        this.A0C = new HashMap();
        this.A0D = z;
        this.A03 = r11;
    }
}
