package X;

/* renamed from: X.0yb  reason: invalid class name and case insensitive filesystem */
public final class C17250yb implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C17250yb(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0036, code lost:
        if (r7.A0L() == false) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Bl1(android.content.Context r11, android.content.Intent r12, X.AnonymousClass06Y r13) {
        /*
            r10 = this;
            r0 = 618176548(0x24d8a024, float:9.3946356E-17)
            int r6 = X.AnonymousClass09Y.A00(r0)
            X.0r6 r5 = r10.A00
            r0 = 168(0xa8, float:2.35E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            android.os.Parcelable r8 = r12.getParcelableExtra(r0)
            com.facebook.user.model.UserKey r8 = (com.facebook.user.model.UserKey) r8
            r1 = -1
            r0 = 166(0xa6, float:2.33E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            int r2 = r12.getIntExtra(r0, r1)
            if (r2 == r1) goto L_0x0091
            r0 = 167(0xa7, float:2.34E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            android.os.Parcelable r7 = r12.getParcelableExtra(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r7 = (com.facebook.messaging.model.threadkey.ThreadKey) r7
            r1 = 1
            if (r7 == 0) goto L_0x0038
            boolean r3 = r7.A0L()
            r0 = 1
            if (r3 != 0) goto L_0x0039
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r0 == 0) goto L_0x0106
            X.3wp r4 = new X.3wp
            r4.<init>(r7, r8)
        L_0x0040:
            if (r0 == 0) goto L_0x0100
            java.util.Map r0 = r5.A0L
            java.lang.Object r9 = r0.get(r4)
            X.1v0 r9 = (X.C37291v0) r9
            if (r9 != 0) goto L_0x005a
            X.1v0 r9 = new X.1v0
            r9.<init>()
            com.facebook.user.model.UserKey r0 = r4.A01
            r9.A09 = r0
            java.util.Map r0 = r5.A0L
            r0.put(r4, r9)
        L_0x005a:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = X.C30107Epg.A00(r0)
            if (r2 == r0) goto L_0x0063
            r1 = 0
        L_0x0063:
            r9.A0C = r1
            java.lang.String r0 = "extra_typing_attribution"
            android.os.Parcelable r0 = r12.getParcelableExtra(r0)
            com.facebook.messaging.typingattribution.TypingAttributionData r0 = (com.facebook.messaging.typingattribution.TypingAttributionData) r0
            r9.A08 = r0
            java.lang.String r0 = "extra_typing_persona_info"
            android.os.Parcelable r0 = r12.getParcelableExtra(r0)
            com.facebook.messaging.model.messagemetadata.MessagePlatformPersona r0 = (com.facebook.messaging.model.messagemetadata.MessagePlatformPersona) r0
            r9.A07 = r0
            com.facebook.messaging.typingattribution.TypingAttributionData r0 = r9.A08
            if (r0 == 0) goto L_0x0098
            r2 = 10
            int r1 = X.AnonymousClass1Y3.AEy
            X.0UN r0 = r5.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.CUJ r1 = (X.CUJ) r1
            com.facebook.messaging.typingattribution.TypingAttributionData r0 = r9.A08
            boolean r0 = r1.A01(r0)
            if (r0 != 0) goto L_0x0098
        L_0x0091:
            r0 = 839770173(0x320de03d, float:8.258266E-9)
            X.AnonymousClass09Y.A01(r0, r6)
            return
        L_0x0098:
            boolean r0 = r9.A0C
            if (r0 == 0) goto L_0x00b0
            r2 = 7
            int r1 = X.AnonymousClass1Y3.AgK
            X.0UN r0 = r5.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.06B r0 = (X.AnonymousClass06B) r0
            long r2 = r0.now()
            r0 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r0
            r9.A03 = r2
        L_0x00b0:
            if (r7 == 0) goto L_0x00fc
            boolean r0 = r7.A0N()
            if (r0 != 0) goto L_0x00fc
            boolean r0 = r7.A0L()
            if (r0 == 0) goto L_0x0091
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r4.A00
            X.0V0 r1 = r5.A0J
            monitor-enter(r1)
            X.0V0 r0 = r5.A0J     // Catch:{ all -> 0x0109 }
            java.util.Collection r0 = r0.AbK(r2)     // Catch:{ all -> 0x0109 }
            if (r0 != 0) goto L_0x00cd
            r3 = 0
            goto L_0x00d2
        L_0x00cd:
            X.0Xy r3 = new X.0Xy     // Catch:{ all -> 0x0109 }
            r3.<init>(r0)     // Catch:{ all -> 0x0109 }
        L_0x00d2:
            monitor-exit(r1)     // Catch:{ all -> 0x0109 }
            java.util.Map r0 = r5.A0L
            java.lang.Object r2 = r0.get(r4)
            X.1v0 r2 = (X.C37291v0) r2
            if (r2 != 0) goto L_0x00e5
            X.18m r0 = X.C195218m.A0A
        L_0x00df:
            com.facebook.user.model.UserKey r1 = r4.A01
            X.AnonymousClass0r6.A08(r5, r3, r1, r0, r4)
            goto L_0x0091
        L_0x00e5:
            X.1H2 r1 = new X.1H2
            r1.<init>()
            boolean r0 = r2.A0C
            r1.A09 = r0
            com.facebook.messaging.typingattribution.TypingAttributionData r0 = r2.A08
            r1.A06 = r0
            com.facebook.messaging.model.messagemetadata.MessagePlatformPersona r0 = r2.A07
            r1.A05 = r0
            X.18m r0 = new X.18m
            r0.<init>(r1)
            goto L_0x00df
        L_0x00fc:
            X.AnonymousClass0r6.A0A(r5, r8)
            goto L_0x0091
        L_0x0100:
            X.1v0 r9 = X.AnonymousClass0r6.A01(r5, r8)
            goto L_0x005a
        L_0x0106:
            r4 = 0
            goto L_0x0040
        L_0x0109:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0109 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17250yb.Bl1(android.content.Context, android.content.Intent, X.06Y):void");
    }
}
