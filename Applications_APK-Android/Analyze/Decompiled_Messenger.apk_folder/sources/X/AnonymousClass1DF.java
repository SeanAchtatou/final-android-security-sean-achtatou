package X;

import com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams;
import com.facebook.payments.contactinfo.model.ContactInfoFormInput;

/* renamed from: X.1DF  reason: invalid class name */
public final class AnonymousClass1DF extends C06020ai {
    public final /* synthetic */ ContactInfoCommonFormParams A00;
    public final /* synthetic */ C54242m6 A01;
    public final /* synthetic */ ContactInfoFormInput A02;

    public AnonymousClass1DF(C54242m6 r1, ContactInfoCommonFormParams contactInfoCommonFormParams, ContactInfoFormInput contactInfoFormInput) {
        this.A01 = r1;
        this.A00 = contactInfoCommonFormParams;
        this.A02 = contactInfoFormInput;
    }
}
