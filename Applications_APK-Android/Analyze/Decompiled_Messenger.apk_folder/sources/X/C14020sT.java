package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.text.DecimalFormat;

/* renamed from: X.0sT  reason: invalid class name and case insensitive filesystem */
public final class C14020sT {
    private static DecimalFormat A04 = new DecimalFormat("##0.0");
    public final long A00;
    public final long A01;
    public final long A02;
    private final long A03;

    private static String A00(long j, long j2) {
        double d = (((double) j) * 1.0d) / 1048576.0d;
        double d2 = (((double) j2) * 1.0d) / 1048576.0d;
        return StringFormatUtil.formatStrLocaleSafe("Max: %sM Used: %sM %s%%", A04.format(d), A04.format(d2), A04.format((d2 * 100.0d) / d));
    }

    public String toString() {
        return StringFormatUtil.formatStrLocaleSafe("Memory: JAVA [%s]  NATIVE [%s]", A00(this.A01, this.A02), A00(this.A03, -1));
    }

    public C14020sT(Runtime runtime) {
        this.A02 = runtime.totalMemory() - runtime.freeMemory();
        long maxMemory = runtime.maxMemory();
        this.A01 = maxMemory;
        this.A00 = maxMemory - this.A02;
        this.A03 = maxMemory;
    }
}
