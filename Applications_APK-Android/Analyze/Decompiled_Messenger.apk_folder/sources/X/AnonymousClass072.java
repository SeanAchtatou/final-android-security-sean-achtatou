package X;

import com.facebook.profilo.core.ProvidersRegistry;
import java.util.List;
import java.util.TreeMap;

/* renamed from: X.072  reason: invalid class name */
public final class AnonymousClass072 implements AnonymousClass057 {
    public TreeMap A00;
    public TreeMap A01;
    public TreeMap A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final boolean A06;

    public AnonymousClass072(int i, List list, int i2, boolean z, TreeMap treeMap, TreeMap treeMap2, TreeMap treeMap3) {
        if (i > 0) {
            this.A03 = i;
            this.A04 = ProvidersRegistry.A00.A00(list);
            this.A05 = i2;
            this.A06 = z;
            this.A02 = treeMap;
            this.A00 = treeMap2;
            this.A01 = treeMap3;
            return;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0A("coinflip_sample_rate (", i, ") <= 0"));
    }
}
