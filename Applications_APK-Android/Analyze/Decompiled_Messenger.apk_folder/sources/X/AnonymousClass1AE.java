package X;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1AE  reason: invalid class name */
public final class AnonymousClass1AE {
    public Set A00 = new HashSet();

    public void A00(String str, int i, int i2, C21681Ih r12, String str2) {
        for (AnonymousClass1AE A002 : this.A00) {
            A002.A00(str, i, i2, r12, str2);
        }
    }

    public void A01(String str, int i, int i2, String str2) {
        for (AnonymousClass1AE A01 : this.A00) {
            A01.A01(str, i, i2, str2);
        }
    }

    public void A02(String str, int i, C21681Ih r5, String str2) {
        for (AnonymousClass1AE A02 : this.A00) {
            A02.A02(str, i, r5, str2);
        }
    }

    public void A03(String str, int i, C21681Ih r5, String str2) {
        for (AnonymousClass1AE A03 : this.A00) {
            A03.A03(str, i, r5, str2);
        }
    }

    public void A04(String str, int i, String str2) {
        for (AnonymousClass1AE A04 : this.A00) {
            A04.A04(str, i, str2);
        }
    }

    public void A05(String str, Object obj, Object obj2, String str2, String str3, Boolean bool, String str4) {
        for (AnonymousClass1AE A05 : this.A00) {
            String str5 = str;
            A05.A05(str5, obj, obj2, str2, str3, bool, str4);
        }
    }

    public AnonymousClass1AE(Collection collection) {
        if (collection != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                AnonymousClass1AE r1 = (AnonymousClass1AE) it.next();
                if (r1 != null) {
                    this.A00.add(r1);
                }
            }
        }
    }
}
