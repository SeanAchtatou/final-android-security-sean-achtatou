package X;

import java.util.regex.Pattern;

/* renamed from: X.0Q9  reason: invalid class name */
public final class AnonymousClass0Q9 {
    public String A00;
    public String A01;
    public boolean A02;
    public boolean A03;
    public final Pattern A04;

    public boolean A00() {
        String A032;
        return this.A03 && (A032 = AnonymousClass0Eq.A00().A03(this.A01)) != null && A032.equals(this.A00);
    }

    public AnonymousClass0Q9() {
        String str = "(\\p{XDigit}*)$";
        this.A04 = Pattern.compile(AnonymousClass08S.A0P("^(\\w+[\\w\\.]*)", "_", str == null ? "0" : str));
    }
}
