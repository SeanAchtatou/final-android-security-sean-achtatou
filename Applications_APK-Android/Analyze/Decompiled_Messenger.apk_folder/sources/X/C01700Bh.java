package X;

import android.util.Log;

/* renamed from: X.0Bh  reason: invalid class name and case insensitive filesystem */
public final class C01700Bh {
    public static volatile boolean A00 = true;

    public static void A00(boolean z, String str) {
        if (!z) {
            Log.w(C01700Bh.class.getName(), str);
            if (!A00) {
                throw new AssertionError(str);
            }
        }
    }
}
