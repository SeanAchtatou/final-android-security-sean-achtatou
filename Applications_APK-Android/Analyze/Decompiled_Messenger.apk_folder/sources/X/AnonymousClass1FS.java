package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FS  reason: invalid class name */
public final class AnonymousClass1FS implements AnonymousClass1FI {
    private static volatile AnonymousClass1FS A00;

    public static final AnonymousClass1FS A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass1FS.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass1FS();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public ImmutableMap get() {
        ImmutableMap.Builder builder = ImmutableMap.builder();
        builder.put(new SubscribeTopic(TurboLoader.Locator.$const$string(90), 0), AnonymousClass1FP.ALWAYS);
        return builder.build();
    }
}
