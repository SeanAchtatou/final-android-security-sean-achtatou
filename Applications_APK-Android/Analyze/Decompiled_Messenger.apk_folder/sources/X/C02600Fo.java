package X;

/* renamed from: X.0Fo  reason: invalid class name and case insensitive filesystem */
public final class C02600Fo extends AnonymousClass0FM {
    public int ligerFullPowerTimeS;
    public int ligerLowPowerTimeS;
    public int ligerRequestCount;
    public long ligerRxBytes;
    public long ligerTxBytes;
    public int ligerWakeupCount;
    public int mqttFullPowerTimeS;
    public int mqttLowPowerTimeS;
    public int mqttRequestCount;
    public long mqttRxBytes;
    public long mqttTxBytes;
    public int mqttWakeupCount;
    public int proxygenActiveRadioTimeS;
    public int proxygenTailRadioTimeS;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C02600Fo r8 = (C02600Fo) obj;
                if (!(this.mqttFullPowerTimeS == r8.mqttFullPowerTimeS && this.mqttLowPowerTimeS == r8.mqttLowPowerTimeS && this.mqttTxBytes == r8.mqttTxBytes && this.mqttRxBytes == r8.mqttRxBytes && this.mqttRequestCount == r8.mqttRequestCount && this.mqttWakeupCount == r8.mqttWakeupCount && this.ligerFullPowerTimeS == r8.ligerFullPowerTimeS && this.ligerLowPowerTimeS == r8.ligerLowPowerTimeS && this.ligerTxBytes == r8.ligerTxBytes && this.ligerRxBytes == r8.ligerRxBytes && this.ligerRequestCount == r8.ligerRequestCount && this.ligerWakeupCount == r8.ligerWakeupCount && this.proxygenActiveRadioTimeS == r8.proxygenActiveRadioTimeS && this.proxygenTailRadioTimeS == r8.proxygenTailRadioTimeS)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    private void A00(C02600Fo r3) {
        this.mqttFullPowerTimeS = r3.mqttFullPowerTimeS;
        this.mqttLowPowerTimeS = r3.mqttLowPowerTimeS;
        this.mqttTxBytes = r3.mqttTxBytes;
        this.mqttRxBytes = r3.mqttRxBytes;
        this.mqttRequestCount = r3.mqttRequestCount;
        this.mqttWakeupCount = r3.mqttWakeupCount;
        this.ligerFullPowerTimeS = r3.ligerFullPowerTimeS;
        this.ligerLowPowerTimeS = r3.ligerLowPowerTimeS;
        this.ligerTxBytes = r3.ligerTxBytes;
        this.ligerRxBytes = r3.ligerRxBytes;
        this.ligerRequestCount = r3.ligerRequestCount;
        this.ligerWakeupCount = r3.ligerWakeupCount;
        this.proxygenActiveRadioTimeS = r3.proxygenActiveRadioTimeS;
        this.proxygenTailRadioTimeS = r3.proxygenTailRadioTimeS;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((C02600Fo) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02600Fo r52 = (C02600Fo) r5;
        C02600Fo r62 = (C02600Fo) r6;
        if (r62 == null) {
            r62 = new C02600Fo();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.mqttFullPowerTimeS = this.mqttFullPowerTimeS - r52.mqttFullPowerTimeS;
        r62.mqttLowPowerTimeS = this.mqttLowPowerTimeS - r52.mqttLowPowerTimeS;
        r62.mqttTxBytes = this.mqttTxBytes - r52.mqttTxBytes;
        r62.mqttRxBytes = this.mqttRxBytes - r52.mqttRxBytes;
        r62.mqttRequestCount = this.mqttRequestCount - r52.mqttRequestCount;
        r62.mqttWakeupCount = this.mqttWakeupCount - r52.mqttWakeupCount;
        r62.ligerFullPowerTimeS = this.ligerFullPowerTimeS - r52.ligerFullPowerTimeS;
        r62.ligerLowPowerTimeS = this.ligerLowPowerTimeS - r52.ligerLowPowerTimeS;
        r62.ligerTxBytes = this.ligerTxBytes - r52.ligerTxBytes;
        r62.ligerRxBytes = this.ligerRxBytes - r52.ligerRxBytes;
        r62.ligerRequestCount = this.ligerRequestCount - r52.ligerRequestCount;
        r62.ligerWakeupCount = this.ligerWakeupCount - r52.ligerWakeupCount;
        r62.proxygenActiveRadioTimeS = this.proxygenActiveRadioTimeS - r52.proxygenActiveRadioTimeS;
        r62.proxygenTailRadioTimeS = this.proxygenTailRadioTimeS - r52.proxygenTailRadioTimeS;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02600Fo r52 = (C02600Fo) r5;
        C02600Fo r62 = (C02600Fo) r6;
        if (r62 == null) {
            r62 = new C02600Fo();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.mqttFullPowerTimeS = this.mqttFullPowerTimeS + r52.mqttFullPowerTimeS;
        r62.mqttLowPowerTimeS = this.mqttLowPowerTimeS + r52.mqttLowPowerTimeS;
        r62.mqttTxBytes = this.mqttTxBytes + r52.mqttTxBytes;
        r62.mqttRxBytes = this.mqttRxBytes + r52.mqttRxBytes;
        r62.mqttRequestCount = this.mqttRequestCount + r52.mqttRequestCount;
        r62.mqttWakeupCount = this.mqttWakeupCount + r52.mqttWakeupCount;
        r62.ligerFullPowerTimeS = this.ligerFullPowerTimeS + r52.ligerFullPowerTimeS;
        r62.ligerLowPowerTimeS = this.ligerLowPowerTimeS + r52.ligerLowPowerTimeS;
        r62.ligerTxBytes = this.ligerTxBytes + r52.ligerTxBytes;
        r62.ligerRxBytes = this.ligerRxBytes + r52.ligerRxBytes;
        r62.ligerRequestCount = this.ligerRequestCount + r52.ligerRequestCount;
        r62.ligerWakeupCount = this.ligerWakeupCount + r52.ligerWakeupCount;
        r62.proxygenActiveRadioTimeS = this.proxygenActiveRadioTimeS + r52.proxygenActiveRadioTimeS;
        r62.proxygenTailRadioTimeS = this.proxygenTailRadioTimeS + r52.proxygenTailRadioTimeS;
        return r62;
    }

    public int hashCode() {
        long j = this.mqttTxBytes;
        long j2 = this.mqttRxBytes;
        long j3 = this.ligerTxBytes;
        long j4 = this.ligerRxBytes;
        return (((((((((((((((((((((((((this.mqttFullPowerTimeS * 31) + this.mqttLowPowerTimeS) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.mqttRequestCount) * 31) + this.mqttWakeupCount) * 31) + this.ligerFullPowerTimeS) * 31) + this.ligerLowPowerTimeS) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + this.ligerRequestCount) * 31) + this.ligerWakeupCount) * 31) + this.proxygenActiveRadioTimeS) * 31) + this.proxygenTailRadioTimeS;
    }

    public String toString() {
        return "ProxygenMetrics{mqttFullPowerTimeS=" + this.mqttFullPowerTimeS + ", mqttLowPowerTimeS=" + this.mqttLowPowerTimeS + ", mqttTxBytes=" + this.mqttTxBytes + ", mqttRxBytes=" + this.mqttRxBytes + ", mqttRequestCount=" + this.mqttRequestCount + ", mqttWakeupCount=" + this.mqttWakeupCount + ", ligerFullPowerTimeS=" + this.ligerFullPowerTimeS + ", ligerLowPowerTimeS=" + this.ligerLowPowerTimeS + ", ligerTxBytes=" + this.ligerTxBytes + ", ligerRxBytes=" + this.ligerRxBytes + ", ligerRequestCount=" + this.ligerRequestCount + ", ligerWakeupCount=" + this.ligerWakeupCount + ", proxygenActiveRadioTimeS=" + this.proxygenActiveRadioTimeS + ", proxygenTailRadioTimeS=" + this.proxygenTailRadioTimeS + '}';
    }
}
