package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0VP  reason: invalid class name */
public final class AnonymousClass0VP {
    public static final AnonymousClass0VP A0O = new AnonymousClass0VP();
    public int A00 = 0;
    public int A01 = Integer.MAX_VALUE;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public boolean A0I = false;
    public boolean A0J = false;
    public boolean A0K = false;
    public String[] A0L;
    private int A0M;
    private int A0N;

    public static void A03(AnonymousClass0VP r4) {
        r4.A02(0);
        if (r4.A07() > 1) {
            r4.A02(r4.A07() - 1);
        }
        if (r4.A00 == 0) {
            File file = new File("/proc/cpuinfo");
            if (file.exists()) {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine != null) {
                            if (readLine.startsWith("cpu MHz")) {
                                int parseFloat = (int) (Float.parseFloat(readLine.substring(readLine.lastIndexOf(58) + 2)) * 1000.0f);
                                if (parseFloat > r4.A00) {
                                    r4.A00 = parseFloat;
                                }
                                if (parseFloat < r4.A01) {
                                    r4.A01 = parseFloat;
                                }
                            }
                        }
                    } catch (Exception e) {
                        C010708t.A0M("ProcessorInfoUtil", "Unable to read a CPU core maximum frequency", e);
                        r4.A00 = -1;
                    } catch (Throwable th) {
                        bufferedReader.close();
                        throw th;
                    }
                }
                bufferedReader.close();
            }
        }
        int i = r4.A00;
        if (i <= r4.A01) {
            if (i == 0) {
                r4.A00 = -1;
            }
            r4.A01 = -1;
        }
    }

    public synchronized int A07() {
        int i;
        i = this.A0M;
        if (i == 0) {
            try {
                this.A0M = AnonymousClass0VQ.A01();
            } catch (Exception e) {
                C010708t.A0M("ProcessorInfoUtil", "Unable to get reliable CPU Core count", e);
            }
            i = this.A0M;
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        if (r3 == null) goto L_0x0038;
     */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001c A[SYNTHETIC, Splitter:B:9:0x001c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String A00(java.lang.String r7) {
        /*
            java.lang.String r6 = "Error closing file reader for %s"
            java.lang.String r5 = "ProcessorInfoUtil"
            r4 = 0
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ IOException -> 0x0029, all -> 0x0018 }
            r3.<init>(r7)     // Catch:{ IOException -> 0x0029, all -> 0x0018 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x002a, all -> 0x0016 }
            r0 = 128(0x80, float:1.794E-43)
            r1.<init>(r3, r0)     // Catch:{ IOException -> 0x002a, all -> 0x0016 }
            java.lang.String r4 = r1.readLine()     // Catch:{ IOException -> 0x002a, all -> 0x0016 }
            goto L_0x002c
        L_0x0016:
            r2 = move-exception
            goto L_0x001a
        L_0x0018:
            r2 = move-exception
            r3 = r4
        L_0x001a:
            if (r3 == 0) goto L_0x0028
            r3.close()     // Catch:{ IOException -> 0x0020 }
            goto L_0x0028
        L_0x0020:
            r1 = move-exception
            java.lang.Object[] r0 = new java.lang.Object[]{r7}
            X.C010708t.A0W(r5, r1, r6, r0)
        L_0x0028:
            throw r2
        L_0x0029:
            r3 = r4
        L_0x002a:
            if (r3 == 0) goto L_0x0038
        L_0x002c:
            r3.close()     // Catch:{ IOException -> 0x0030 }
            return r4
        L_0x0030:
            r1 = move-exception
            java.lang.Object[] r0 = new java.lang.Object[]{r7}
            X.C010708t.A0W(r5, r1, r6, r0)
        L_0x0038:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VP.A00(java.lang.String):java.lang.String");
    }

    private static final String A01(String str) {
        if (str == null || str.length() != 0) {
            return str;
        }
        return null;
    }

    private void A02(int i) {
        File file = new File(StringFormatUtil.formatStrLocaleSafe("/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_max_freq", Integer.valueOf(i)));
        if (file.exists()) {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            try {
                int parseInt = Integer.parseInt(bufferedReader.readLine());
                if (parseInt > this.A00) {
                    this.A00 = parseInt;
                }
                if (parseInt < this.A01) {
                    this.A01 = parseInt;
                }
            } finally {
                bufferedReader.close();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01b0, code lost:
        if (r0 == false) goto L_0x01b2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01fd A[SYNTHETIC, Splitter:B:100:0x01fd] */
    /* JADX WARNING: Removed duplicated region for block: B:126:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01f3 A[SYNTHETIC, Splitter:B:94:0x01f3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void A04(X.AnonymousClass0VP r9) {
        /*
            java.lang.String r2 = "ProcessorInfoUtil"
            r6 = 0
            r0 = 60
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Class r5 = java.lang.Class.forName(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r3 = 1
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            java.lang.Class[] r1 = new java.lang.Class[]{r0}     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "get"
            java.lang.reflect.Method r4 = r5.getMethod(r0, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.board.platform"
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A08 = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.boot.hardware"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A09 = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.mtk.hardware"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0F = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.hardware"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0C = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.hardware.alter"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0D = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.product.platform"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0H = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.mediatek.platform"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0E = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.chipname"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0A = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.arch"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A07 = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.config.cpu_info_display"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0B = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = "ro.product.cpu.abilist"
            r1[r6] = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.Object r0 = r4.invoke(r5, r1)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            java.lang.String r0 = A01(r0)     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0G = r0     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r9.A0I = r3     // Catch:{ IllegalArgumentException -> 0x00e6, ClassNotFoundException -> 0x00e0, NoSuchMethodException -> 0x00dc, IllegalAccessException -> 0x00d8, InvocationTargetException -> 0x00d4, LinkageError -> 0x00d0, SecurityException -> 0x00e8 }
            r0 = 1
            goto L_0x00ef
        L_0x00d0:
            r1 = move-exception
            java.lang.String r0 = "Unable to load android.os.SystemProperties class"
            goto L_0x00eb
        L_0x00d4:
            r1 = move-exception
            java.lang.String r0 = "Method SystemProperties.get(String) throwed an exception"
            goto L_0x00eb
        L_0x00d8:
            r1 = move-exception
            java.lang.String r0 = "Method SystemProperties.get(String) is unaccessible"
            goto L_0x00eb
        L_0x00dc:
            r1 = move-exception
            java.lang.String r0 = "Unable to find SystemProperties.get(String) method"
            goto L_0x00eb
        L_0x00e0:
            r1 = move-exception
            java.lang.String r0 = "Unable to find android.os.SystemProperties class"
            X.C010708t.A0M(r2, r0, r1)
        L_0x00e6:
            r0 = 0
            goto L_0x00ef
        L_0x00e8:
            r1 = move-exception
            java.lang.String r0 = "Unable to access SystemProperties.get(String) method"
        L_0x00eb:
            X.C010708t.A0M(r2, r0, r1)
            r0 = 0
        L_0x00ef:
            if (r0 != 0) goto L_0x0212
            java.lang.String r3 = "Unable to close reader for build.prop"
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "/system/build.prop"
            r1.<init>(r0)
            boolean r0 = r1.exists()
            r4 = 1
            if (r0 == 0) goto L_0x0206
            r6 = 0
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01eb }
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ IOException -> 0x01eb }
            r0.<init>(r1)     // Catch:{ IOException -> 0x01eb }
            r5.<init>(r0)     // Catch:{ IOException -> 0x01eb }
        L_0x010c:
            java.lang.String r7 = r5.readLine()     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            if (r7 == 0) goto L_0x01e4
            r0 = 61
            int r1 = r7.indexOf(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r0 = -1
            if (r1 == r0) goto L_0x010c
            r8 = 0
            java.lang.String r6 = r7.substring(r8, r1)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            int r0 = r1 + 1
            java.lang.String r1 = r7.substring(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            int r0 = r6.length()     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            if (r0 <= 0) goto L_0x010c
            int r7 = r1.length()     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            if (r7 <= 0) goto L_0x010c
            r0 = 92
            if (r7 <= r0) goto L_0x013a
            java.lang.String r1 = r1.substring(r8, r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
        L_0x013a:
            int r0 = r6.hashCode()     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            switch(r0) {
                case -2100278214: goto L_0x0142;
                case -1898584205: goto L_0x014c;
                case -927486879: goto L_0x0156;
                case -453804423: goto L_0x0160;
                case -76013021: goto L_0x016a;
                case 253923299: goto L_0x0174;
                case 288343824: goto L_0x017f;
                case 1092788584: goto L_0x018a;
                case 1319524711: goto L_0x0194;
                case 1386574083: goto L_0x019f;
                case 2045310540: goto L_0x01a9;
                default: goto L_0x0141;
            }     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
        L_0x0141:
            goto L_0x01b2
        L_0x0142:
            java.lang.String r0 = "ro.mediatek.platform"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 1
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x014c:
            java.lang.String r0 = "ro.boot.hardware"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 3
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x0156:
            java.lang.String r0 = "ro.hardware.alter"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 6
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x0160:
            java.lang.String r0 = "ro.hardware"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 5
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x016a:
            java.lang.String r0 = "ro.product.platform"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 2
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x0174:
            java.lang.String r0 = "ro.config.cpu_info_display"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 9
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x017f:
            java.lang.String r0 = "ro.product.cpu.abilist"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 10
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x018a:
            java.lang.String r0 = "ro.chipname"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 7
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x0194:
            java.lang.String r0 = "ro.arch"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 8
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x019f:
            java.lang.String r0 = "ro.mtk.hardware"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 4
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b2
        L_0x01a9:
            java.lang.String r0 = "ro.board.platform"
            boolean r0 = r6.equals(r0)     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            r6 = 0
            if (r0 != 0) goto L_0x01b3
        L_0x01b2:
            r6 = -1
        L_0x01b3:
            switch(r6) {
                case 0: goto L_0x01b8;
                case 1: goto L_0x01bc;
                case 2: goto L_0x01c0;
                case 3: goto L_0x01c4;
                case 4: goto L_0x01c8;
                case 5: goto L_0x01cc;
                case 6: goto L_0x01d0;
                case 7: goto L_0x01d4;
                case 8: goto L_0x01d8;
                case 9: goto L_0x01dc;
                case 10: goto L_0x01e0;
                default: goto L_0x01b6;
            }     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
        L_0x01b6:
            goto L_0x010c
        L_0x01b8:
            r9.A08 = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01bc:
            r9.A0E = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01c0:
            r9.A0H = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01c4:
            r9.A09 = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01c8:
            r9.A0F = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01cc:
            r9.A0C = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01d0:
            r9.A0D = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01d4:
            r9.A0A = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01d8:
            r9.A07 = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01dc:
            r9.A0B = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01e0:
            r9.A0G = r1     // Catch:{ IOException -> 0x01e8, all -> 0x01fa }
            goto L_0x010c
        L_0x01e4:
            r5.close()     // Catch:{ IOException -> 0x020c }
            goto L_0x0210
        L_0x01e8:
            r1 = move-exception
            r6 = r5
            goto L_0x01ec
        L_0x01eb:
            r1 = move-exception
        L_0x01ec:
            java.lang.String r0 = "Unable to read build.prop"
            X.C010708t.A0M(r2, r0, r1)     // Catch:{ all -> 0x01f7 }
            if (r6 == 0) goto L_0x0210
            r6.close()     // Catch:{ IOException -> 0x020c }
            goto L_0x0210
        L_0x01f7:
            r1 = move-exception
            r5 = r6
            goto L_0x01fb
        L_0x01fa:
            r1 = move-exception
        L_0x01fb:
            if (r5 == 0) goto L_0x0205
            r5.close()     // Catch:{ IOException -> 0x0201 }
            goto L_0x0205
        L_0x0201:
            r0 = move-exception
            X.C010708t.A0M(r2, r3, r0)
        L_0x0205:
            throw r1
        L_0x0206:
            java.lang.String r0 = "build.prop file missing"
            X.C010708t.A0J(r2, r0)
            goto L_0x0210
        L_0x020c:
            r0 = move-exception
            X.C010708t.A0M(r2, r3, r0)
        L_0x0210:
            r9.A0I = r4
        L_0x0212:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VP.A04(X.0VP):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00fd, code lost:
        if (r10.equals("CPU variant") == false) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01df, code lost:
        if (r9 != 6) goto L_0x01e1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0244 A[SYNTHETIC, Splitter:B:131:0x0244] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0251 A[SYNTHETIC, Splitter:B:138:0x0251] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void A05(X.AnonymousClass0VP r15) {
        /*
            java.lang.String r3 = "Unable to close reader for cpuinfo"
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "/proc/cpuinfo"
            r1.<init>(r0)
            boolean r0 = r1.exists()
            r4 = 1
            java.lang.String r2 = "ProcessorInfoUtil"
            if (r0 == 0) goto L_0x025a
            r6 = 0
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x023c }
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ IOException -> 0x023c }
            r0.<init>(r1)     // Catch:{ IOException -> 0x023c }
            r5.<init>(r0)     // Catch:{ IOException -> 0x023c }
            X.4oQ r8 = new X.4oQ     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r8.<init>()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r7.<init>()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r6 = 0
            r1 = 0
            r14 = 0
        L_0x002a:
            java.lang.String r11 = r5.readLine()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            if (r11 == 0) goto L_0x022a
            r0 = 58
            int r9 = r11.indexOf(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r0 = -1
            if (r9 == r0) goto L_0x002a
            java.lang.String r0 = r11.substring(r6, r9)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r10 = r0.trim()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            int r0 = r9 + 1
            java.lang.String r0 = r11.substring(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r9 = r0.trim()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            int r0 = r10.length()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            if (r0 <= 0) goto L_0x002a
            int r11 = r9.length()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            if (r11 <= 0) goto L_0x002a
            int r0 = r10.hashCode()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r12 = 4
            switch(r0) {
                case -1815500307: goto L_0x00f6;
                case -1542724959: goto L_0x00eb;
                case -1509145992: goto L_0x00e1;
                case -1094759278: goto L_0x00d7;
                case -226015139: goto L_0x00cc;
                case 97513095: goto L_0x00c1;
                case 104069929: goto L_0x00b7;
                case 181553672: goto L_0x00ac;
                case 516911339: goto L_0x00a2;
                case 542854003: goto L_0x0098;
                case 547394780: goto L_0x008e;
                case 909208690: goto L_0x0083;
                case 1256489867: goto L_0x0079;
                case 1429357118: goto L_0x006d;
                case 2046689570: goto L_0x0061;
                default: goto L_0x005f;
            }     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
        L_0x005f:
            goto L_0x00ff
        L_0x0061:
            java.lang.String r0 = "model name"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 12
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x006d:
            java.lang.String r0 = "stepping"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 8
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x0079:
            java.lang.String r0 = "CPU part"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 3
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x0083:
            java.lang.String r0 = "Processor"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 11
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x008e:
            java.lang.String r0 = "cpu family"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 6
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x0098:
            java.lang.String r0 = "CPU revision"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 4
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00a2:
            java.lang.String r0 = "CPU architecture"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 5
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00ac:
            java.lang.String r0 = "Hardware"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 14
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00b7:
            java.lang.String r0 = "model"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 7
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00c1:
            java.lang.String r0 = "flags"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 10
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00cc:
            java.lang.String r0 = "Features"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 9
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00d7:
            java.lang.String r0 = "processor"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 0
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00e1:
            java.lang.String r0 = "CPU implementer"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 1
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00eb:
            java.lang.String r0 = "MSM Hardware"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 13
            if (r0 != 0) goto L_0x0100
            goto L_0x00ff
        L_0x00f6:
            java.lang.String r0 = "CPU variant"
            boolean r0 = r10.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 2
            if (r0 != 0) goto L_0x0100
        L_0x00ff:
            r13 = -1
        L_0x0100:
            r10 = 128(0x80, float:1.794E-43)
            r0 = 15
            switch(r13) {
                case 0: goto L_0x020d;
                case 1: goto L_0x0204;
                case 2: goto L_0x01fb;
                case 3: goto L_0x01f3;
                case 4: goto L_0x014b;
                case 5: goto L_0x01ab;
                case 6: goto L_0x017c;
                case 7: goto L_0x0150;
                case 8: goto L_0x014b;
                case 9: goto L_0x013d;
                case 10: goto L_0x012f;
                case 11: goto L_0x0121;
                case 12: goto L_0x0121;
                case 13: goto L_0x0115;
                case 14: goto L_0x0109;
                default: goto L_0x0107;
            }     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
        L_0x0107:
            goto L_0x002a
        L_0x0109:
            int r0 = java.lang.Math.min(r10, r11)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r0 = r9.substring(r6, r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r15.A05 = r0     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x0115:
            int r0 = java.lang.Math.min(r10, r11)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r0 = r9.substring(r6, r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r15.A06 = r0     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x0121:
            r0 = 48
            int r0 = java.lang.Math.min(r0, r11)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r0 = r9.substring(r6, r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r15.A04 = r0     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x012f:
            r0 = 1024(0x400, float:1.435E-42)
            int r0 = java.lang.Math.min(r0, r11)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r0 = r9.substring(r6, r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r15.A03 = r0     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x013d:
            r0 = 256(0x100, float:3.59E-43)
            int r0 = java.lang.Math.min(r0, r11)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r0 = r9.substring(r6, r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r15.A03 = r0     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x014b:
            r8.A00(r9, r0, r6)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x0150:
            int r12 = r8.A01     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r0 = -983281(0xfffffffffff0ff0f, float:NaN)
            r12 = r12 & r0
            r8.A01 = r12     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            int r11 = r8.A00     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r11 = r11 & r0
            r8.A00 = r11     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ NumberFormatException -> 0x002a }
            r0 = r9 & 15
            int r9 = r9 >> 4
            int r0 = r0 << 4
            r10 = r0 & 240(0xf0, float:3.36E-43)
            r10 = r10 | r12
            r8.A01 = r10     // Catch:{ NumberFormatException -> 0x002a }
            int r9 = r9 << 16
            r0 = 983040(0xf0000, float:1.377532E-39)
            r9 = r9 & r0
            r9 = r9 | r10
            r8.A01 = r9     // Catch:{ NumberFormatException -> 0x002a }
            r0 = -267452176(0xfffffffff00f00f0, float:-1.7702996E29)
            r11 = r11 | r0
            r8.A00 = r11     // Catch:{ NumberFormatException -> 0x002a }
            goto L_0x002a
        L_0x017c:
            int r12 = r8.A01     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r0 = -267390721(0xfffffffff00ff0ff, float:-1.7819081E29)
            r12 = r12 & r0
            r8.A01 = r12     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            int r11 = r8.A00     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r11 = r11 & r0
            r8.A00 = r11     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ NumberFormatException -> 0x002a }
            r0 = 15
            int r0 = java.lang.Math.min(r9, r0)     // Catch:{ NumberFormatException -> 0x002a }
            int r9 = r9 - r0
            int r0 = r0 << 8
            r10 = r0 & 3840(0xf00, float:5.381E-42)
            r10 = r10 | r12
            r8.A01 = r10     // Catch:{ NumberFormatException -> 0x002a }
            int r9 = r9 << 20
            r0 = 267386880(0xff00000, float:2.3665827E-29)
            r9 = r9 & r0
            r9 = r9 | r10
            r8.A01 = r9     // Catch:{ NumberFormatException -> 0x002a }
            r0 = -1044736(0xfffffffffff00f00, float:NaN)
            r11 = r11 | r0
            r8.A00 = r11     // Catch:{ NumberFormatException -> 0x002a }
            goto L_0x002a
        L_0x01ab:
            int r11 = r8.A01     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r0 = -983041(0xfffffffffff0ffff, float:NaN)
            r11 = r11 & r0
            r8.A01 = r11     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            int r10 = r8.A00     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r10 = r10 & r0
            r8.A00 = r10     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 15
            r12 = 6
            int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ NumberFormatException -> 0x01c3 }
            r0 = 7
            if (r9 >= r0) goto L_0x01e4
            goto L_0x01de
        L_0x01c3:
            java.lang.String r0 = "AArch64"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            if (r0 != 0) goto L_0x01e4
            java.lang.String r0 = "5TEJ"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            if (r0 == 0) goto L_0x01d4
            goto L_0x01e3
        L_0x01d4:
            java.lang.String r0 = "5TE"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r13 = 5
            if (r0 != 0) goto L_0x01e4
            goto L_0x01e1
        L_0x01de:
            r13 = 7
            if (r9 == r12) goto L_0x01e4
        L_0x01e1:
            r13 = 0
            goto L_0x01e4
        L_0x01e3:
            r13 = 6
        L_0x01e4:
            if (r13 == 0) goto L_0x002a
            int r9 = r13 << 16
            r0 = 983040(0xf0000, float:1.377532E-39)
            r9 = r9 & r0
            r11 = r11 | r9
            r8.A01 = r11     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r10 = r10 | r0
            r8.A00 = r10     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x01f3:
            r0 = 65520(0xfff0, float:9.1813E-41)
            r8.A01(r9, r0, r12)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x01fb:
            r10 = 15728640(0xf00000, float:2.2040519E-38)
            r0 = 20
            r8.A01(r9, r10, r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x0204:
            r10 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = 24
            r8.A01(r9, r10, r0)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            goto L_0x002a
        L_0x020d:
            int r10 = java.lang.Integer.parseInt(r9)     // Catch:{ NumberFormatException -> 0x0212 }
            goto L_0x0213
        L_0x0212:
            r10 = -1
        L_0x0213:
            if (r10 == r1) goto L_0x0227
            int r9 = r8.A00     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r0 = 0
            if (r9 == 0) goto L_0x021b
            r0 = 1
        L_0x021b:
            if (r0 != 0) goto L_0x021f
            if (r14 == 0) goto L_0x0222
        L_0x021f:
            r8.A02(r7, r1)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
        L_0x0222:
            r8.A01 = r6     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r8.A00 = r6     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r1 = r10
        L_0x0227:
            r14 = 1
            goto L_0x002a
        L_0x022a:
            r8.A02(r7, r1)     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            java.lang.String r0 = r7.toString()     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r15.A02 = r0     // Catch:{ IOException -> 0x0239, all -> 0x0237 }
            r5.close()     // Catch:{ IOException -> 0x0248 }
            goto L_0x025f
        L_0x0237:
            r1 = move-exception
            goto L_0x024f
        L_0x0239:
            r1 = move-exception
            r6 = r5
            goto L_0x023d
        L_0x023c:
            r1 = move-exception
        L_0x023d:
            java.lang.String r0 = "Unable to read cpuinfo"
            X.C010708t.A0M(r2, r0, r1)     // Catch:{ all -> 0x024d }
            if (r6 == 0) goto L_0x025f
            r6.close()     // Catch:{ IOException -> 0x0248 }
            goto L_0x025f
        L_0x0248:
            r0 = move-exception
            X.C010708t.A0M(r2, r3, r0)
            goto L_0x025f
        L_0x024d:
            r1 = move-exception
            r5 = r6
        L_0x024f:
            if (r5 == 0) goto L_0x0259
            r5.close()     // Catch:{ IOException -> 0x0255 }
            goto L_0x0259
        L_0x0255:
            r0 = move-exception
            X.C010708t.A0M(r2, r3, r0)
        L_0x0259:
            throw r1
        L_0x025a:
            java.lang.String r0 = "CPU Info file missing"
            X.C010708t.A0J(r2, r0)
        L_0x025f:
            r15.A0K = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VP.A05(X.0VP):void");
    }

    public int A06() {
        if (this.A0N == 0) {
            this.A0N = Math.max(Runtime.getRuntime().availableProcessors(), 1);
        }
        return this.A0N;
    }

    public String A08(int i) {
        String r0;
        if (!this.A0J) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < 7; i2++) {
                int A072 = A07();
                if (A072 == -1) {
                    A072 = A06();
                }
                C98684nf r10 = new C98684nf();
                for (int i3 = 0; i3 < A072; i3++) {
                    Integer valueOf = Integer.valueOf(i3);
                    String formatStrLocaleSafe = StringFormatUtil.formatStrLocaleSafe("/sys/devices/system/cpu/cpu%d/cache/index%d/", valueOf, Integer.valueOf(i2));
                    StringBuilder sb = new StringBuilder();
                    String A002 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "level"));
                    String str = null;
                    if (A002 != null) {
                        sb.append('L');
                        sb.append(A002);
                        String A003 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "type"));
                        if (A003 != null) {
                            char c = 65535;
                            int hashCode = A003.hashCode();
                            if (hashCode != 2122698) {
                                if (hashCode != 1377720690) {
                                    if (hashCode == 1969394798 && A003.equals("Instruction")) {
                                        c = 1;
                                    }
                                } else if (A003.equals("Unified")) {
                                    c = 0;
                                }
                            } else if (A003.equals("Data")) {
                                c = 2;
                            }
                            if (c != 0) {
                                if (c == 1) {
                                    sb.append('I');
                                } else if (c != 2) {
                                    sb.append('[');
                                    sb.append(A003);
                                    sb.append(']');
                                } else {
                                    sb.append('D');
                                }
                            }
                            String A004 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "size"));
                            sb.append('|');
                            if (A004 == null) {
                                sb.append('?');
                            } else {
                                sb.append(A004);
                            }
                            String A005 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "shared_cpu_list"));
                            if (A005 == null) {
                                sb.append("@?");
                            } else if (!A005.equals(Integer.toString(i3))) {
                                sb.append('@');
                                sb.append(A005);
                            }
                            String A006 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "ways_of_associativity"));
                            sb.append('|');
                            if (A006 == null) {
                                sb.append('?');
                            } else {
                                sb.append(A006);
                            }
                            String A007 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "number_of_sets"));
                            sb.append('|');
                            if (A007 == null) {
                                sb.append('?');
                            } else {
                                sb.append(A007);
                            }
                            String A008 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "coherency_line_size"));
                            sb.append('|');
                            if (A008 == null) {
                                sb.append('?');
                            } else {
                                sb.append(A008);
                            }
                            String A009 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "physical_line_partition"));
                            sb.append('|');
                            if (A009 == null) {
                                sb.append('?');
                            } else {
                                sb.append(A009);
                            }
                            String A0010 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "write_policy"));
                            sb.append('|');
                            if (A0010 == null) {
                                sb.append('?');
                            } else {
                                sb.append(A0010);
                            }
                            String A0011 = A00(AnonymousClass08S.A0J(formatStrLocaleSafe, "allocation_policy"));
                            sb.append('|');
                            if (A0011 == null) {
                                sb.append('?');
                            } else {
                                sb.append(A0011);
                            }
                            str = sb.toString();
                        }
                    }
                    if (str != null) {
                        if (r10.A00.containsKey(str)) {
                            ((List) r10.A00.get(str)).add(valueOf);
                        } else {
                            ArrayList arrayList2 = new ArrayList();
                            arrayList2.add(valueOf);
                            r10.A00.put(str, arrayList2);
                        }
                    }
                }
                if (r10.A00.isEmpty()) {
                    r0 = null;
                } else {
                    r0 = r10.toString();
                }
                if (r0 == null) {
                    break;
                }
                arrayList.add(r0);
            }
            if (arrayList.isEmpty()) {
                this.A0L = null;
            } else {
                String[] strArr = new String[arrayList.size()];
                arrayList.toArray(strArr);
                this.A0L = strArr;
            }
            this.A0J = true;
        }
        String[] strArr2 = this.A0L;
        if (strArr2 == null || i >= strArr2.length) {
            return null;
        }
        return strArr2[i];
    }

    private AnonymousClass0VP() {
    }
}
