package X;

import android.content.Context;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.inject.ContextScoped;
import com.facebook.messaging.model.messagemetadata.MessageMetadataAtTextRange;
import com.facebook.messaging.model.messagemetadata.TimestampMetadata;
import com.facebook.messaging.model.messages.Message;
import com.google.common.collect.ImmutableList;
import java.util.concurrent.TimeUnit;

@ContextScoped
/* renamed from: X.1Bd  reason: invalid class name and case insensitive filesystem */
public final class C20161Bd {
    private static C04470Uu A08;
    public AnonymousClass0UN A00;
    private int A01 = -1;
    public final Context A02;
    public final C04310Tq A03;
    public final C04310Tq A04;
    public final C04310Tq A05;
    private final C22351Kz A06;
    private final C04310Tq A07;

    public static Spanned A00(C20161Bd r14, Message message, boolean z, AnonymousClass3Z0 r17) {
        String trim;
        SpannableStringBuilder spannableStringBuilder;
        CharSequence A042;
        Message message2 = message;
        C20161Bd r6 = r14;
        if (C06850cB.A0A(message.A10)) {
            trim = C57262rh.A04((C57262rh) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AnD, r14.A00), message, null, true);
        } else {
            trim = message.A10.trim();
        }
        boolean z2 = z;
        if (!((C34521pk) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B4l, r14.A00)).A01() || (A042 = ((AnonymousClass8B2) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AUg, r14.A00)).A04(trim, AnonymousClass07B.A0C)) == null || trim.equals(A042)) {
            spannableStringBuilder = null;
        } else {
            String A0P = AnonymousClass08S.A0P("\n\n ", r14.A02.getResources().getString(2131822260), " ");
            Spannable.Factory instance = Spannable.Factory.getInstance();
            spannableStringBuilder = new SpannableStringBuilder(instance.newSpannable(((Object) A042) + A0P + trim));
            spannableStringBuilder.setSpan(new StyleSpan(2), A042.length(), spannableStringBuilder.length(), 33);
            spannableStringBuilder.setSpan(new TextAppearanceSpan(null, 0, r14.A02.getResources().getDimensionPixelSize(2132148371), null, null), A042.length(), spannableStringBuilder.length(), 33);
            r6.A02(spannableStringBuilder, z2);
        }
        if (spannableStringBuilder != null) {
            return spannableStringBuilder;
        }
        AnonymousClass3Q5 r11 = (AnonymousClass3Q5) AnonymousClass1XX.A03(AnonymousClass1Y3.A2k, r6.A00);
        ImmutableList immutableList = message.A0Y;
        if (immutableList == null || immutableList.isEmpty() || !((ViewerContext) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AfE, r6.A00)).mIsPageContext) {
            return r6.A04(trim, z2, r11.A01(message), true);
        }
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Spannable.Factory.getInstance().newSpannable(trim));
        int indexOf = message.A10.indexOf(trim);
        if (indexOf >= 0 && ((ViewerContext) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AfE, r6.A00)).mIsPageContext) {
            boolean A062 = ((AnonymousClass23R) r6.A05.get()).A06(message.A0U);
            C24971Xv it = message.A0Y.iterator();
            while (it.hasNext()) {
                MessageMetadataAtTextRange messageMetadataAtTextRange = (MessageMetadataAtTextRange) it.next();
                if (C29382EYh.A03(messageMetadataAtTextRange) && !A062) {
                    int i = messageMetadataAtTextRange.A01 - indexOf;
                    int i2 = i + messageMetadataAtTextRange.A00;
                    if (i >= 0 && i2 <= spannableStringBuilder2.length()) {
                        long millis = TimeUnit.SECONDS.toMillis(((TimestampMetadata) messageMetadataAtTextRange.A02).A00);
                        int i3 = AnonymousClass1Y3.AfE;
                        AnonymousClass0UN r8 = r6.A00;
                        if (((ViewerContext) AnonymousClass1XX.A02(1, i3, r8)).mIsPageContext) {
                            spannableStringBuilder2.setSpan(new C100884rZ((AnonymousClass3NE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Ax9, r8), new C100934re(r17, message2, millis), message2.A0U.A01), i, i2, 0);
                        }
                    }
                }
            }
        }
        r6.A03(spannableStringBuilder2, r11.A01(message2));
        r6.A02(spannableStringBuilder2, z2);
        return spannableStringBuilder2;
    }

    public static final C20161Bd A01(AnonymousClass1XY r4) {
        C20161Bd r0;
        synchronized (C20161Bd.class) {
            C04470Uu A002 = C04470Uu.A00(A08);
            A08 = A002;
            try {
                if (A002.A03(r4)) {
                    A08.A00 = new C20161Bd((AnonymousClass1XY) A08.A01());
                }
                C04470Uu r1 = A08;
                r0 = (C20161Bd) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A08.A02();
                throw th;
            }
        }
        return r0;
    }

    private void A02(Editable editable, boolean z) {
        C22351Kz r0 = this.A06;
        if (r0 != null) {
            if (z) {
                r0.AMy(editable);
                return;
            }
            if (this.A01 == -1) {
                Context context = this.A02;
                this.A01 = AnonymousClass0HQ.A02(context, 2130970031, context.getResources().getDimensionPixelSize(2132148460)) - C007106r.A00(this.A02, 1.0f);
            }
            this.A06.ANh(editable, this.A01);
        }
    }

    private void A03(Editable editable, boolean z) {
        if (((C67333Og) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AeA, this.A00)).A00.Aem(284885180880008L) && !((C67333Og) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AeA, this.A00)).A00.Aem(284885180748934L)) {
            ((CLI) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BSV, this.A00)).A01(editable, z);
        }
    }

    public Spanned A04(String str, boolean z, boolean z2, boolean z3) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Spannable.Factory.getInstance().newSpannable(str));
        if (z3) {
            A03(spannableStringBuilder, z2);
        }
        A02(spannableStringBuilder, z);
        return spannableStringBuilder;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003f, code lost:
        if (r3.A06.BH9(r4.A10) == false) goto L_0x0041;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A05(com.facebook.messaging.model.messages.Message r4) {
        /*
            r3 = this;
            X.0Tq r0 = r3.A07
            java.lang.Object r1 = r0.get()
            X.3Yy r1 = (X.AnonymousClass3Yy) r1
            java.lang.String r0 = r4.A0q
            java.lang.Object r2 = r1.A03(r0)
            X.3Yz r2 = (X.C69773Yz) r2
            if (r2 != 0) goto L_0x001c
            X.3Yz r2 = new X.3Yz
            r2.<init>()
            java.lang.String r0 = r4.A0q
            r1.A05(r0, r2)
        L_0x001c:
            java.lang.Boolean r0 = r2.A00
            if (r0 != 0) goto L_0x0048
            java.lang.String r0 = r4.A10
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 != 0) goto L_0x0041
            com.google.common.collect.ImmutableList r0 = r4.A0X
            if (r0 == 0) goto L_0x0032
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0041
        L_0x0032:
            X.2yU r0 = r4.A06
            if (r0 != 0) goto L_0x0041
            X.1Kz r1 = r3.A06
            java.lang.String r0 = r4.A10
            boolean r1 = r1.BH9(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0042
        L_0x0041:
            r0 = 0
        L_0x0042:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r2.A00 = r0
        L_0x0048:
            boolean r0 = r0.booleanValue()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20161Bd.A05(com.facebook.messaging.model.messages.Message):boolean");
    }

    private C20161Bd(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(7, r3);
        this.A02 = AnonymousClass1YA.A00(r3);
        this.A06 = C22341Ky.A00(r3);
        this.A03 = C26681bq.A05(r3);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.A3N, r3);
        this.A04 = AnonymousClass0VG.A00(AnonymousClass1Y3.A3x, r3);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.Anz, r3);
    }
}
