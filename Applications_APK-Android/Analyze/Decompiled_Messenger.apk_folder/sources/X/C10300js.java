package X;

import java.io.Serializable;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0js  reason: invalid class name and case insensitive filesystem */
public final class C10300js implements Serializable {
    public static final C10020jP CORE_TYPE_BOOL = new C10020jP(Boolean.TYPE);
    public static final C10020jP CORE_TYPE_INT = new C10020jP(Integer.TYPE);
    public static final C10020jP CORE_TYPE_LONG = new C10020jP(Long.TYPE);
    public static final C10020jP CORE_TYPE_STRING = new C10020jP(String.class);
    public static final C10030jR[] NO_TYPES = new C10030jR[0];
    public static final C10300js instance = new C10300js();
    private static final long serialVersionUID = 1;
    public transient C28321ec _cachedArrayListType;
    public transient C28321ec _cachedHashMapType;
    public final C11880o6[] _modifiers;
    public final C10320ju _parser;
    public final C10310jt _typeCache;

    public static C10030jR constructSimpleType(Class cls, C10030jR[] r10) {
        Class cls2 = cls;
        TypeVariable[] typeParameters = cls.getTypeParameters();
        int length = typeParameters.length;
        C10030jR[] r7 = r10;
        int length2 = r10.length;
        if (length == length2) {
            String[] strArr = new String[length];
            for (int i = 0; i < length; i++) {
                strArr[i] = typeParameters[i].getName();
            }
            return new C10020jP(cls2, strArr, r7, null, null, false);
        }
        throw new IllegalArgumentException("Parameter type mismatch for " + cls.getName() + ": expected " + length + " parameters, was given " + length2);
    }

    public static C10030jR _collectionType(C10300js r3, Class cls) {
        C10030jR r1;
        C10030jR[] findTypeParameters = r3.findTypeParameters(cls, Collection.class, new C28311eb(r3, null, cls, null));
        if (findTypeParameters == null) {
            r1 = new C10020jP(Object.class);
        } else if (findTypeParameters.length == 1) {
            r1 = findTypeParameters[0];
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0P("Strange Collection type ", cls.getName(), ": can not determine type parameters"));
        }
        return C28331ed.construct(cls, r1);
    }

    private C28321ec _doFindSuperInterfaceChain(C28321ec r6, Class cls) {
        C28321ec r0;
        Class cls2 = r6._rawClass;
        Type[] genericInterfaces = cls2.getGenericInterfaces();
        if (genericInterfaces != null) {
            int length = genericInterfaces.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    r0 = _findSuperInterfaceChain(genericInterfaces[i], cls);
                    if (r0 != null) {
                        break;
                    }
                    i++;
                } else {
                    break;
                }
            }
        }
        Type genericSuperclass = cls2.getGenericSuperclass();
        if (genericSuperclass == null || (r0 = _findSuperInterfaceChain(genericSuperclass, cls)) == null) {
            return null;
        }
        r0._subType = r6;
        r6._superType = r0;
        return r6;
    }

    private C28321ec _findSuperClassChain(Type type, Class cls) {
        C28321ec _findSuperClassChain;
        C28321ec r1 = new C28321ec(type);
        Class cls2 = r1._rawClass;
        if (cls2 == cls) {
            return r1;
        }
        Type genericSuperclass = cls2.getGenericSuperclass();
        if (genericSuperclass == null || (_findSuperClassChain = _findSuperClassChain(genericSuperclass, cls)) == null) {
            return null;
        }
        _findSuperClassChain._subType = r1;
        r1._superType = _findSuperClassChain;
        return r1;
    }

    private C28321ec _findSuperInterfaceChain(Type type, Class cls) {
        Class cls2;
        C28321ec r3 = new C28321ec(type);
        Class<ArrayList> cls3 = r3._rawClass;
        if (cls3 == cls) {
            return new C28321ec(type);
        }
        if (cls3 == HashMap.class && cls == (cls2 = Map.class)) {
            synchronized (this) {
                if (this._cachedHashMapType == null) {
                    C28321ec deepCloneWithoutSubtype = r3.deepCloneWithoutSubtype();
                    _doFindSuperInterfaceChain(deepCloneWithoutSubtype, cls2);
                    this._cachedHashMapType = deepCloneWithoutSubtype._superType;
                }
                C28321ec deepCloneWithoutSubtype2 = this._cachedHashMapType.deepCloneWithoutSubtype();
                r3._superType = deepCloneWithoutSubtype2;
                deepCloneWithoutSubtype2._subType = r3;
            }
            return r3;
        } else if (cls3 != ArrayList.class || cls != List.class) {
            return _doFindSuperInterfaceChain(r3, cls);
        } else {
            synchronized (this) {
                if (this._cachedArrayListType == null) {
                    C28321ec deepCloneWithoutSubtype3 = r3.deepCloneWithoutSubtype();
                    _doFindSuperInterfaceChain(deepCloneWithoutSubtype3, List.class);
                    this._cachedArrayListType = deepCloneWithoutSubtype3._superType;
                }
                C28321ec deepCloneWithoutSubtype4 = this._cachedArrayListType.deepCloneWithoutSubtype();
                r3._superType = deepCloneWithoutSubtype4;
                deepCloneWithoutSubtype4._subType = r3;
            }
            return r3;
        }
    }

    public static C10030jR _mapType(C10300js r3, Class cls) {
        C10030jR[] findTypeParameters = r3.findTypeParameters(cls, Map.class, new C28311eb(r3, null, cls, null));
        if (findTypeParameters == null) {
            Class<Object> cls2 = Object.class;
            return C180610a.construct(cls, new C10020jP(cls2), new C10020jP(cls2));
        } else if (findTypeParameters.length == 2) {
            return C180610a.construct(cls, findTypeParameters[0], findTypeParameters[1]);
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0P("Strange Map type ", cls.getName(), ": can not determine type parameters"));
        }
    }

    public static C10030jR unknownType() {
        return new C10020jP(Object.class);
    }

    public C10030jR _constructType(Type type, C28311eb r12) {
        String obj;
        C10030jR _constructType;
        int length;
        C10030jR[] r2;
        if (type instanceof Class) {
            _constructType = _fromClass((Class) type, r12);
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Class cls = (Class) parameterizedType.getRawType();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            if (actualTypeArguments == null) {
                length = 0;
            } else {
                length = actualTypeArguments.length;
            }
            if (length == 0) {
                r2 = NO_TYPES;
            } else {
                r2 = new C10030jR[length];
                for (int i = 0; i < length; i++) {
                    r2[i] = _constructType(actualTypeArguments[i], r12);
                }
            }
            Class<Map> cls2 = Map.class;
            if (cls2.isAssignableFrom(cls)) {
                C10030jR[] findTypeParameters = findTypeParameters(constructSimpleType(cls, r2), cls2);
                int length2 = findTypeParameters.length;
                if (length2 == 2) {
                    _constructType = C180610a.construct(cls, findTypeParameters[0], findTypeParameters[1]);
                } else {
                    throw new IllegalArgumentException(AnonymousClass08S.A0R("Could not find 2 type parameters for Map class ", cls.getName(), " (found ", length2, ")"));
                }
            } else {
                Class<Collection> cls3 = Collection.class;
                if (cls3.isAssignableFrom(cls)) {
                    C10030jR[] findTypeParameters2 = findTypeParameters(constructSimpleType(cls, r2), cls3);
                    int length3 = findTypeParameters2.length;
                    if (length3 == 1) {
                        _constructType = C28331ed.construct(cls, findTypeParameters2[0]);
                    } else {
                        throw new IllegalArgumentException(AnonymousClass08S.A0R("Could not find 1 type parameter for Collection class ", cls.getName(), " (found ", length3, ")"));
                    }
                } else if (length == 0) {
                    _constructType = new C10020jP(cls);
                } else {
                    _constructType = constructSimpleType(cls, r2);
                }
            }
        } else if (type instanceof C10030jR) {
            return (C10030jR) type;
        } else {
            if (type instanceof GenericArrayType) {
                _constructType = BMA.construct(_constructType(((GenericArrayType) type).getGenericComponentType(), r12), null, null);
            } else if (type instanceof TypeVariable) {
                TypeVariable typeVariable = (TypeVariable) type;
                if (r12 == null) {
                    _constructType = new C10020jP(Object.class);
                } else {
                    String name = typeVariable.getName();
                    _constructType = r12.findType(name);
                    if (_constructType == null) {
                        Type[] bounds = typeVariable.getBounds();
                        r12._addPlaceholder(name);
                        _constructType = _constructType(bounds[0], r12);
                    }
                }
            } else if (type instanceof WildcardType) {
                _constructType = _constructType(((WildcardType) type).getUpperBounds()[0], r12);
            } else {
                if (type == null) {
                    obj = "[null]";
                } else {
                    obj = type.toString();
                }
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Unrecognized Type: ", obj));
            }
        }
        C11880o6[] r4 = this._modifiers;
        if (r4 != null && !_constructType.isContainerType()) {
            for (C11880o6 modifyType : r4) {
                _constructType = modifyType.modifyType(_constructType, type, r12, this);
            }
        }
        return _constructType;
    }

    public C10030jR _fromClass(Class cls, C28311eb r6) {
        C10030jR r0;
        C10030jR r1;
        if (cls == String.class) {
            return CORE_TYPE_STRING;
        }
        if (cls == Boolean.TYPE) {
            return CORE_TYPE_BOOL;
        }
        if (cls == Integer.TYPE) {
            return CORE_TYPE_INT;
        }
        if (cls == Long.TYPE) {
            return CORE_TYPE_LONG;
        }
        C29041fm r3 = new C29041fm(cls);
        synchronized (this._typeCache) {
            try {
                r0 = (C10030jR) this._typeCache.get(r3);
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (r0 != null) {
            return r0;
        }
        if (cls.isArray()) {
            r1 = BMA.construct(_constructType(cls.getComponentType(), null), null, null);
        } else {
            if (!cls.isEnum()) {
                if (Map.class.isAssignableFrom(cls)) {
                    r1 = _mapType(this, cls);
                } else if (Collection.class.isAssignableFrom(cls)) {
                    r1 = _collectionType(this, cls);
                }
            }
            r1 = new C10020jP(cls);
        }
        synchronized (this._typeCache) {
            try {
                this._typeCache.put(r3, r1);
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
        return r1;
    }

    public C10030jR constructFromCanonical(String str) {
        C10320ju r1 = this._parser;
        C189068qJ r2 = new C189068qJ(str.trim());
        C10030jR parseType = C10320ju.parseType(r1, r2);
        if (!r2.hasMoreTokens()) {
            return parseType;
        }
        throw C10320ju._problem(r2, "Unexpected tokens after complete type");
    }

    public C10030jR constructSpecializedType(C10030jR r4, Class cls) {
        if (!(r4 instanceof C10020jP) || (!cls.isArray() && !Map.class.isAssignableFrom(cls) && !Collection.class.isAssignableFrom(cls))) {
            return r4.narrowBy(cls);
        }
        if (r4._class.isAssignableFrom(cls)) {
            C10030jR _fromClass = _fromClass(cls, new C28311eb(this, null, r4._class, null));
            Object valueHandler = r4.getValueHandler();
            if (valueHandler != null) {
                _fromClass = _fromClass.withValueHandler(valueHandler);
            }
            Object typeHandler = r4.getTypeHandler();
            if (typeHandler != null) {
                return _fromClass.withTypeHandler(typeHandler);
            }
            return _fromClass;
        }
        throw new IllegalArgumentException("Class " + cls.getClass().getName() + " not subtype of " + r4);
    }

    private C10300js() {
        this._typeCache = new C10310jt(16, 100);
        this._parser = new C10320ju(this);
        this._modifiers = null;
    }

    public C10300js(C10320ju r4, C11880o6[] r5) {
        this._typeCache = new C10310jt(16, 100);
        this._parser = r4;
        this._modifiers = r5;
    }

    public C10030jR[] findTypeParameters(C10030jR r5, Class cls) {
        Class cls2 = r5._class;
        if (cls2 != cls) {
            return findTypeParameters(cls2, cls, new C28311eb(this, null, cls2, r5));
        }
        int containedTypeCount = r5.containedTypeCount();
        if (containedTypeCount == 0) {
            return null;
        }
        C10030jR[] r2 = new C10030jR[containedTypeCount];
        for (int i = 0; i < containedTypeCount; i++) {
            r2[i] = r5.containedType(i);
        }
        return r2;
    }

    private C10030jR[] findTypeParameters(Class cls, Class cls2, C28311eb r11) {
        C28321ec _findSuperClassChain;
        if (cls2.isInterface()) {
            _findSuperClassChain = _findSuperInterfaceChain(cls, cls2);
        } else {
            _findSuperClassChain = _findSuperClassChain(cls, cls2);
        }
        if (_findSuperClassChain == null) {
            throw new IllegalArgumentException(AnonymousClass08S.A0S("Class ", cls.getName(), " is not a subtype of ", cls2.getName()));
        }
        while (true) {
            C28321ec r3 = _findSuperClassChain._superType;
            if (r3 == null) {
                break;
            }
            _findSuperClassChain = r3;
            Class cls3 = r3._rawClass;
            C28311eb r7 = new C28311eb(this, null, cls3, null);
            ParameterizedType parameterizedType = r3._genericType;
            boolean z = false;
            if (parameterizedType != null) {
                z = true;
            }
            if (z) {
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                TypeVariable[] typeParameters = cls3.getTypeParameters();
                int length = actualTypeArguments.length;
                for (int i = 0; i < length; i++) {
                    r7.addBinding(typeParameters[i].getName(), _constructType(actualTypeArguments[i], r11));
                }
            }
            r11 = r7;
        }
        boolean z2 = false;
        if (_findSuperClassChain._genericType != null) {
            z2 = true;
        }
        if (!z2) {
            return null;
        }
        if (r11._bindings == null) {
            C28311eb._resolve(r11);
        }
        if (r11._bindings.size() == 0) {
            return C28311eb.NO_TYPES;
        }
        return (C10030jR[]) r11._bindings.values().toArray(new C10030jR[r11._bindings.size()]);
    }
}
