package X;

/* renamed from: X.0Wb  reason: invalid class name and case insensitive filesystem */
public final class C04760Wb {
    public AnonymousClass0UN A00;

    public static final C04760Wb A00(AnonymousClass1XY r1) {
        return new C04760Wb(r1);
    }

    public static boolean A01(String str) {
        if (str.contains("mobileconfig_emergency_push_shadow_test") || str.contains("mobileconfig_emergency_push_test_shadow") || str.contains("mobileconfig_propagation_test_config")) {
            return true;
        }
        return false;
    }

    public C04760Wb(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
