package X;

import com.facebook.acra.ACRA;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.forker.Process;
import com.facebook.messaging.model.messages.Message;
import com.facebook.proxygen.TraceFieldType;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.18M  reason: invalid class name */
public final class AnonymousClass18M {
    private static volatile AnonymousClass18M A07;
    public final DeprecatedAnalyticsLogger A00;
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();
    private final AnonymousClass06B A03;
    private final C30231hi A04;
    private final AnonymousClass18N A05;
    private final C193517u A06;

    public static String A01(byte[] bArr) {
        int i;
        int min = Math.min(10, bArr.length);
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        while (true) {
            i = min - 1;
            if (i2 >= i) {
                break;
            }
            sb.append(Byte.valueOf(bArr[i2]));
            sb.append(',');
            i2++;
        }
        if (min > 0) {
            sb.append((int) bArr[i]);
        }
        return sb.toString();
    }

    public static final AnonymousClass18M A00(AnonymousClass1XY r8) {
        if (A07 == null) {
            synchronized (AnonymousClass18M.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        A07 = new AnonymousClass18M(C06920cI.A00(applicationInjector), AnonymousClass18N.A00(applicationInjector), AnonymousClass067.A02(), C193517u.A00(applicationInjector), C30231hi.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static void A02(AnonymousClass18M r11, String str, Message message, List list, boolean z) {
        Integer num;
        String str2;
        AnonymousClass47P r0;
        long longValue;
        AnonymousClass47P r1;
        Preconditions.checkNotNull(message.A0q);
        Preconditions.checkNotNull(list);
        HashMap hashMap = new HashMap();
        hashMap.put(TraceFieldType.MsgId, message.A0q);
        C30231hi r5 = r11.A04;
        if (((double) r5.A01.nextInt(Integer.MAX_VALUE)) >= 1.0737418235000001E8d) {
            switch (r5.A01.nextInt(AnonymousClass07B.A00(8).length)) {
                case 0:
                    num = AnonymousClass07B.A00;
                    break;
                case 1:
                    num = AnonymousClass07B.A01;
                    break;
                case 2:
                    num = AnonymousClass07B.A0C;
                    break;
                case 3:
                    num = AnonymousClass07B.A0N;
                    break;
                case 4:
                    num = AnonymousClass07B.A0Y;
                    break;
                case 5:
                    num = AnonymousClass07B.A0i;
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    num = AnonymousClass07B.A0n;
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    num = AnonymousClass07B.A0o;
                    break;
                default:
                    throw new IllegalArgumentException("Unknown tincan msg type");
            }
            switch (num.intValue()) {
                case 1:
                    str2 = "i";
                    break;
                case 2:
                    str2 = "v";
                    break;
                case 3:
                    str2 = "l";
                    break;
                case 4:
                    str2 = "a";
                    break;
                case 5:
                    str2 = "s";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    str2 = "h";
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    str2 = "g";
                    break;
                default:
                    str2 = "t";
                    break;
            }
        } else {
            str2 = r5.A00.A03(message);
        }
        HashMap hashMap2 = new HashMap();
        hashMap2.put(TraceFieldType.MsgType, str2);
        hashMap2.put("epsilon", "0.050000");
        hashMap2.put("num_types", String.valueOf(AnonymousClass07B.A00(8).length));
        hashMap.putAll(hashMap2);
        C30231hi r52 = r11.A04;
        if (((double) r52.A01.nextInt(Integer.MAX_VALUE)) < 1.0737418235000001E8d) {
            Integer num2 = message.A0l;
            if (num2 != null) {
                longValue = (long) (num2.intValue() / AnonymousClass1Y3.A87);
            } else {
                longValue = 0;
            }
        } else {
            switch (r52.A01.nextInt(AnonymousClass47P.values().length)) {
                case 0:
                    r0 = AnonymousClass47P.A03;
                    break;
                case 1:
                    r0 = AnonymousClass47P.A02;
                    break;
                case 2:
                    r0 = AnonymousClass47P.A09;
                    break;
                case 3:
                    r0 = AnonymousClass47P.A0B;
                    break;
                case 4:
                    r0 = AnonymousClass47P.A06;
                    break;
                case 5:
                    r0 = AnonymousClass47P.A01;
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    r0 = AnonymousClass47P.A08;
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    r0 = AnonymousClass47P.A0A;
                    break;
                case 8:
                    r0 = AnonymousClass47P.A05;
                    break;
                case Process.SIGKILL:
                    r0 = AnonymousClass47P.A07;
                    break;
                case AnonymousClass1Y3.A01 /*10*/:
                    r0 = AnonymousClass47P.A0C;
                    break;
                case AnonymousClass1Y3.A02 /*11*/:
                    r0 = AnonymousClass47P.A04;
                    break;
                default:
                    r0 = AnonymousClass47P.A0D;
                    break;
            }
            longValue = r0.value.longValue();
        }
        HashMap hashMap3 = new HashMap();
        AnonymousClass47P[] values = AnonymousClass47P.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i < length) {
                r1 = values[i];
                if (r1.value.longValue() != longValue) {
                    i++;
                }
            } else {
                r1 = AnonymousClass47P.A0D;
            }
        }
        hashMap3.put(C99084oO.$const$string(14), String.valueOf(r1.value));
        hashMap3.put("num_expiration_times", String.valueOf(AnonymousClass47P.values().length));
        hashMap.putAll(hashMap3);
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        for (int i2 = 0; i2 < list.size(); i2++) {
            C22338Aw9 aw9 = (C22338Aw9) list.get(i2);
            arrayNode.add(aw9.user_id + '_' + aw9.instance_id);
        }
        hashMap.put("recipients", arrayNode);
        hashMap.put("is_multi", Boolean.valueOf(z));
        A03(r11, str, hashMap);
    }

    public static void A03(AnonymousClass18M r4, String str, Map map) {
        C22361La A042 = r4.A00.A04(str, false);
        if (A042.A0B()) {
            A042.A06("pigeon_reserved_keyword_module", "tincan_reliability");
            A042.A08(map);
            A042.A06("tincan_device_id", r4.A05.A02());
            A042.A03("client_timestamp_ms", r4.A03.now());
            A042.A07("mqtt_connected", r4.A06.A05());
            A042.A03("mqtt_last_connection_timestamp_ms", r4.A06.A02());
            A042.A0A();
        }
    }

    public void A04(C22045AnM anM, boolean z, boolean z2, boolean z3, Exception exc, String str) {
        C22337Aw7 aw7 = anM.A00;
        String A002 = C1936695v.A00(aw7.nonce);
        HashMap hashMap = new HashMap();
        hashMap.put("nonce_content", new String(aw7.nonce));
        if (A002 != null) {
            hashMap.put(TraceFieldType.MsgId, A002);
            hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, Boolean.valueOf(z));
            hashMap.put("is_multi", Boolean.valueOf(z3));
            hashMap.put("is_dr_sent", Boolean.valueOf(z2));
            if (exc != null) {
                hashMap.put("exception_type", exc.getClass().getSimpleName());
            }
            if (str != null) {
                if (exc != null) {
                    str = AnonymousClass08S.A0P(str, ". ", exc.getMessage());
                }
                hashMap.put("error_info", str);
            }
        } else {
            hashMap.put(TraceFieldType.MsgId, "message_id_decoding_failed");
        }
        A03(this, "tincan_msg_delivery", hashMap);
    }

    public void A05(Integer num, C22045AnM anM, String str, Exception exc) {
        int i;
        C11670nb r2 = new C11670nb("tincan_errors");
        if (1 - num.intValue() != 0) {
            i = 0;
        } else {
            i = 1;
        }
        r2.A09("error_type", i);
        if (anM != null) {
            r2.A0D("packet_key", anM.A01);
            C22337Aw7 aw7 = anM.A00;
            if (aw7 != null) {
                r2.A0D("package_type", String.valueOf(aw7.type));
                r2.A09("nonce_length", anM.A00.nonce.length);
                r2.A0D("nonce_content", new String(anM.A00.nonce));
            }
        }
        StringBuilder sb = new StringBuilder();
        if (str == null) {
            str = BuildConfig.FLAVOR;
        }
        sb.append(str);
        sb.append(";");
        if (exc != null) {
            r2.A0D("exception_type", exc.getClass().getSimpleName());
            r2.A0D(TurboLoader.Locator.$const$string(13), AnonymousClass0D4.A01(exc));
            sb.append(exc.getMessage());
        }
        r2.A0D("error_info", sb.toString());
        this.A00.A09(r2);
    }

    public void A06(String str, String str2) {
        if (!"Current registration state: COMPLETED".equals(str2)) {
            C11670nb r2 = new C11670nb("tincan_registration");
            r2.A0D("event_type", "register_skipped");
            r2.A0D("registration_reason", str);
            r2.A0D("skip_reason", str2);
            this.A00.A09(r2);
        }
    }

    public void A07(boolean z, String str, long j, long j2, Integer num, String str2, byte[] bArr) {
        int i;
        int i2;
        HashMap hashMap = new HashMap();
        hashMap.put(TraceFieldType.MsgId, str);
        if (z) {
            long now = this.A03.now();
            hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, 1);
            hashMap.put("last_send_latency", Long.valueOf(now - j2));
            hashMap.put("latency", Long.valueOf(now - j));
            if (this.A02.containsKey(str)) {
                hashMap.put("sender_key_fanout", this.A02.get(str));
                this.A02.remove(str);
            }
        } else {
            hashMap.put(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, 0);
            if (num != null) {
                int intValue = num.intValue();
                switch (intValue) {
                    case 1:
                    case 2:
                    case 5:
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    case AnonymousClass1Y3.A02 /*11*/:
                    case AnonymousClass1Y3.A03 /*12*/:
                    case 16:
                    case Process.SIGSTOP:
                    case 20:
                        i = 6;
                        break;
                    case 3:
                        i = 5;
                        break;
                    case 4:
                    case 13:
                    case 15:
                    default:
                        i = 0;
                        break;
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                        i = 1;
                        break;
                    case 8:
                    case Process.SIGKILL:
                        i = 4;
                        break;
                    case AnonymousClass1Y3.A01 /*10*/:
                    case 14:
                    case 17:
                    case Process.SIGCONT:
                    case AnonymousClass1Y3.A05 /*21*/:
                    case AnonymousClass1Y3.A06 /*22*/:
                        i = 2;
                        break;
                }
                hashMap.put("error_type", Integer.valueOf(i));
                switch (intValue) {
                    case 1:
                        i2 = 0;
                        break;
                    case 2:
                        i2 = 1;
                        break;
                    case 3:
                        i2 = 2;
                        break;
                    case 4:
                        i2 = 3;
                        break;
                    case 5:
                        i2 = 4;
                        break;
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        i2 = 5;
                        break;
                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                        i2 = 6;
                        break;
                    case 8:
                        i2 = 7;
                        break;
                    case Process.SIGKILL:
                        i2 = 8;
                        break;
                    case AnonymousClass1Y3.A01 /*10*/:
                        i2 = 9;
                        break;
                    case AnonymousClass1Y3.A02 /*11*/:
                        i2 = 10;
                        break;
                    case AnonymousClass1Y3.A03 /*12*/:
                        i2 = 11;
                        break;
                    case 13:
                        i2 = 12;
                        break;
                    case 14:
                        i2 = 13;
                        break;
                    case 15:
                        i2 = 14;
                        break;
                    case 16:
                        i2 = 15;
                        break;
                    case 17:
                        i2 = 16;
                        break;
                    case Process.SIGCONT:
                        i2 = 17;
                        break;
                    case Process.SIGSTOP:
                        i2 = 18;
                        break;
                    case 20:
                        i2 = 19;
                        break;
                    case AnonymousClass1Y3.A05 /*21*/:
                        i2 = 20;
                        break;
                    case AnonymousClass1Y3.A06 /*22*/:
                        i2 = 21;
                        break;
                    default:
                        i2 = -1;
                        break;
                }
                hashMap.put(C22298Ase.$const$string(44), Integer.valueOf(i2));
            }
            if (str2 != null) {
                hashMap.put("error_info", str2);
            }
            if (bArr != null) {
                hashMap.put("nonce_content", A01(bArr));
            }
        }
        A03(this, "tincan_msg_send", hashMap);
    }

    public void A08(byte[] bArr, boolean z, String str) {
        String str2 = new String(bArr);
        if (this.A01.containsKey(str2)) {
            C22096AoS aoS = (C22096AoS) this.A01.get(str2);
            C11670nb r3 = new C11670nb("tincan_registration");
            r3.A0D("event_type", "register_result");
            r3.A0D("nonce_content", A01(bArr));
            r3.A0D("registration_reason", aoS.A01);
            r3.A0D("tincan_device_id", aoS.A00);
            r3.A0E("registration_result", z);
            if (str != null) {
                r3.A0D(AnonymousClass80H.$const$string(AnonymousClass1Y3.A4s), str);
            }
            this.A00.A09(r3);
            this.A01.remove(str2);
        }
    }

    private AnonymousClass18M(DeprecatedAnalyticsLogger deprecatedAnalyticsLogger, AnonymousClass18N r3, AnonymousClass06B r4, C193517u r5, C30231hi r6) {
        this.A00 = deprecatedAnalyticsLogger;
        this.A05 = r3;
        this.A03 = r4;
        this.A06 = r5;
        this.A04 = r6;
    }
}
