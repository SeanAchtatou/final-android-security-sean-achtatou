package X;

import android.net.Uri;
import com.facebook.proxygen.utils.CircularEventLog;
import com.facebook.proxygen.utils.EventBaseFuncLog;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Closeables;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ke  reason: invalid class name and case insensitive filesystem */
public final class C10660ke implements C08210er {
    public static CircularEventLog A01;
    public static EventBaseFuncLog A02;
    private static final Class A03 = C10660ke.class;
    private static volatile C10660ke A04;
    private final C25051Yd A00;

    public String getName() {
        return "NetworkEventLog";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    public void prepareDataForWriting() {
    }

    public static final C10660ke A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C10660ke.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C10660ke(AnonymousClass0WT.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public boolean shouldSendAsync() {
        return this.A00.Aeo(281651071811861L, false);
    }

    private C10660ke(C25051Yd r1) {
        this.A00 = r1;
    }

    public Map getExtraFileFromWorkerThread(File file) {
        PrintWriter printWriter;
        PrintWriter printWriter2;
        ImmutableMap.Builder builder = ImmutableMap.builder();
        CircularEventLog circularEventLog = A01;
        if (circularEventLog != null && circularEventLog.mInitialized) {
            try {
                File file2 = new File(file, "fb_liger_reporting");
                Uri fromFile = Uri.fromFile(file2);
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                try {
                    printWriter = new PrintWriter(new OutputStreamWriter(fileOutputStream));
                    for (String println : A01.getLogLines()) {
                        printWriter.println(println);
                    }
                    Closeables.A00(printWriter, false);
                    Closeables.A00(fileOutputStream, false);
                    builder.put("fb_liger_reporting", fromFile.toString());
                    File file3 = new File(file, "fb_liger_recent_evb_calls");
                    Uri fromFile2 = Uri.fromFile(file3);
                    FileOutputStream fileOutputStream2 = new FileOutputStream(file3);
                    try {
                        printWriter2 = new PrintWriter(new OutputStreamWriter(fileOutputStream2));
                        printWriter2.println(A02.getRecentCallsDetails());
                        Closeables.A00(printWriter2, false);
                        Closeables.A00(fileOutputStream2, false);
                        builder.put("fb_liger_recent_evb_calls", fromFile2.toString());
                        File file4 = new File(file, "liger_quic_logs");
                        Uri fromFile3 = Uri.fromFile(file4);
                        fileOutputStream = new FileOutputStream(file4);
                        printWriter = new PrintWriter(new OutputStreamWriter(fileOutputStream));
                        for (String println2 : A01.getQuicLogLines()) {
                            printWriter.println(println2);
                        }
                        Closeables.A00(printWriter, false);
                        Closeables.A00(fileOutputStream, false);
                        builder.put("liger_quic_logs", fromFile3.toString());
                    } catch (Throwable th) {
                        th = th;
                        Closeables.A00(fileOutputStream2, false);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    Closeables.A00(fileOutputStream, false);
                    throw th;
                }
            } catch (IOException e) {
                C010708t.A09(A03, "Exception saving liger trace", e);
                throw e;
            }
        }
        return builder.build();
    }
}
