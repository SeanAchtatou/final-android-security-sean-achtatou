package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.150  reason: invalid class name */
public final class AnonymousClass150 extends AnonymousClass15T {
    private static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) C04350Ue.A00.A09("path_provider/"));
    public AnonymousClass0UN A00;
    public final AnonymousClass1Y7 A01;

    public static JSONObject A00(AnonymousClass150 r4, AnonymousClass1Y7 r5) {
        try {
            return new JSONObject(((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r4.A00)).B4F(r5, "{}"));
        } catch (JSONException unused) {
            return new JSONObject();
        }
    }

    public AnonymousClass150(AnonymousClass1XY r3, String str) {
        super(str);
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = (AnonymousClass1Y7) A02.A09(AnonymousClass08S.A0J(str, "/"));
    }
}
