package X;

import com.google.common.base.Preconditions;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Mj  reason: invalid class name and case insensitive filesystem */
public final class C22671Mj extends AbstractExecutorService {
    public final Integer A00;
    public final /* synthetic */ C22651Mh A01;

    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        return false;
    }

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    public void shutdown() {
    }

    public List shutdownNow() {
        return null;
    }

    public C22671Mj(C22651Mh r1, Integer num) {
        this.A01 = r1;
        this.A00 = num;
    }

    public void execute(Runnable runnable) {
        AnonymousClass0VS r0;
        String str;
        C22651Mh r2 = this.A01;
        Integer num = this.A00;
        Preconditions.checkNotNull(runnable);
        C04650Vo r1 = r2.A00;
        if (r1 != null) {
            int intValue = num.intValue();
            switch (intValue) {
                case 0:
                    r0 = AnonymousClass0VS.NORMAL;
                    break;
                case 1:
                    r0 = AnonymousClass0VS.FOREGROUND;
                    break;
                case 2:
                    r0 = AnonymousClass0VS.IMPORTANT;
                    break;
                default:
                    switch (intValue) {
                        case 1:
                            str = "NORMAL";
                            break;
                        case 2:
                            str = "HIGH";
                            break;
                        default:
                            str = "LOW";
                            break;
                    }
                    throw new IllegalArgumentException(AnonymousClass08S.A0J("Bad taskPriority: ", str));
            }
            r1.CID(runnable, r0);
            return;
        }
        AnonymousClass07A.A04(r2, new A59(runnable, num), -609831878);
    }
}
