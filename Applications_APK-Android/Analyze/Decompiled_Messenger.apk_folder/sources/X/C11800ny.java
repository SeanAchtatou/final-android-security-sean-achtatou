package X;

/* renamed from: X.0ny  reason: invalid class name and case insensitive filesystem */
public interface C11800ny {
    void addBeanSerializerModifier(C11740ns r1);

    void addDeserializers(AnonymousClass1c7 r1);

    void addSerializers(C11730nr r1);

    void addTypeModifier(C11880o6 r1);
}
