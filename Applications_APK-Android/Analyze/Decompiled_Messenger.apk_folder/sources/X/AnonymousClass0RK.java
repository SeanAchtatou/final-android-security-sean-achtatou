package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0RK  reason: invalid class name */
public final class AnonymousClass0RK {
    public AnonymousClass0B0 A00;
    private AlarmManager A01;
    private Context A02;
    private C009207y A03;
    private C012309k A04;
    private Map A05;

    public void A00(String str) {
        PendingIntent pendingIntent = (PendingIntent) this.A05.remove(str);
        if (pendingIntent != null) {
            this.A03.A05(this.A01, pendingIntent);
        }
        AnonymousClass0DD AY8 = this.A00.AY8();
        AY8.BzB(str, 120000);
        AY8.commit();
    }

    public void A01(String str, String str2, String str3) {
        Intent intent = new Intent("com.facebook.rti.fbns.intent.REGISTER_RETRY");
        intent.setClassName(this.A02, str3);
        intent.putExtra("pkg_name", str);
        intent.putExtra("appid", str2);
        this.A04.A01(intent);
        PendingIntent service = PendingIntent.getService(this.A02, 0, intent, 134217728);
        this.A05.put(str, service);
        long j = this.A00.getLong(str, 120000);
        long elapsedRealtime = SystemClock.elapsedRealtime() + j;
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            this.A03.A04(this.A01, 2, elapsedRealtime, service);
        } else if (i >= 19) {
            this.A03.A02(this.A01, 2, elapsedRealtime, service);
        } else {
            this.A01.set(2, elapsedRealtime, service);
        }
        long j2 = j * 2;
        if (j2 > 86400000) {
            j2 = 86400000;
        }
        AnonymousClass0DD AY8 = this.A00.AY8();
        AY8.BzB(str, j2);
        AY8.commit();
    }

    public AnonymousClass0RK(Context context, C01420Ad r4, C012309k r5, C009207y r6, AnonymousClass0AW r7) {
        this.A02 = context;
        C01540Aq A002 = r4.A00("alarm", AlarmManager.class);
        if (A002.A02()) {
            this.A01 = (AlarmManager) A002.A01();
            this.A00 = r7.AbP(AnonymousClass07B.A0p);
            this.A04 = r5;
            this.A03 = r6;
            this.A05 = new HashMap();
            return;
        }
        throw new IllegalArgumentException("Cannot acquire Alarm service");
    }
}
