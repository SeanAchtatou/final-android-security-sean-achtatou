package X;

/* renamed from: X.1zv  reason: invalid class name and case insensitive filesystem */
public final class C39981zv implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.composer.powerups.PowerupsResultAdapter$PowerupsClickListener$3$1";
    public final /* synthetic */ double A00;
    public final /* synthetic */ DDG A01;

    public C39981zv(DDG ddg, double d) {
        this.A01 = ddg;
        this.A00 = d;
    }

    public void run() {
        this.A01.A00.setProgress((int) (this.A00 * 100.0d));
    }
}
