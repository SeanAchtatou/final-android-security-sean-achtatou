package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;

/* renamed from: X.0BW  reason: invalid class name */
public final class AnonymousClass0BW {
    public static final String A0D = AnonymousClass08S.A0J(AnonymousClass0BW.class.getCanonicalName(), ".ACTION_ALARM.");
    public boolean A00;
    public boolean A01;
    public final int A02;
    public final AlarmManager A03;
    public final PendingIntent A04;
    public final BroadcastReceiver A05;
    public final Context A06;
    public final Handler A07;
    public final C009207y A08;
    public final String A09;
    private final int A0A;
    private final AnonymousClass0AV A0B;
    public volatile Runnable A0C;

    public synchronized void A00() {
        if (this.A00) {
            this.A00 = false;
            this.A08.A05(this.A03, this.A04);
        }
    }

    public synchronized void A01() {
        synchronized (this) {
            if (!this.A01) {
                this.A01 = this.A08.A08(this.A06, this.A05, new IntentFilter(this.A09), null, this.A07);
            }
        }
        if (!this.A00) {
            long AnM = (long) ((this.A0B.AnM() + this.A0A) * AnonymousClass1Y3.A87);
            this.A00 = true;
            long elapsedRealtime = SystemClock.elapsedRealtime() + AnM;
            try {
                if (this.A02 >= 19) {
                    this.A08.A02(this.A03, 2, elapsedRealtime, this.A04);
                } else {
                    this.A03.set(2, elapsedRealtime, this.A04);
                }
            } catch (Throwable th) {
                this.A00 = false;
                C010708t.A0V("PingUnreceivedAlarm", th, "alarm_failed; intervalSec=%s", Long.valueOf(AnM / 1000));
            }
        }
        return;
    }

    public AnonymousClass0BW(Context context, C01420Ad r6, String str, Handler handler, AnonymousClass0AV r9, C009207y r10) {
        this.A06 = context;
        StringBuilder sb = new StringBuilder(A0D);
        sb.append(str);
        String packageName = context.getPackageName();
        if (!TextUtils.isEmpty(packageName)) {
            sb.append('.');
            sb.append(packageName);
        }
        this.A09 = sb.toString();
        C01540Aq A002 = r6.A00("alarm", AlarmManager.class);
        if (A002.A02()) {
            this.A03 = (AlarmManager) A002.A01();
            this.A02 = Build.VERSION.SDK_INT;
            this.A07 = handler;
            this.A0B = r9;
            this.A08 = r10;
            this.A0A = r9.Ay8();
            this.A05 = new AnonymousClass0BX(this);
            Intent intent = new Intent(this.A09);
            intent.setPackage(this.A06.getPackageName());
            this.A04 = PendingIntent.getBroadcast(this.A06, 0, intent, 134217728);
            return;
        }
        throw new IllegalArgumentException("Cannot acquire Alarm service");
    }
}
