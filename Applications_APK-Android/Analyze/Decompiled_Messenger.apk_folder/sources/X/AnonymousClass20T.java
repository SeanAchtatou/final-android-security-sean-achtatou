package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.User;

/* renamed from: X.20T  reason: invalid class name */
public final class AnonymousClass20T implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.composer.moredrawer.MoreDrawerUnitItemAdapter$7$1";
    public final /* synthetic */ AnonymousClass7Ph A00;
    public final /* synthetic */ User A01;

    public AnonymousClass20T(AnonymousClass7Ph r1, User user) {
        this.A00 = r1;
        this.A01 = user;
    }

    public void run() {
        if (this.A01 != null) {
            AnonymousClass7Ph r0 = this.A00;
            ThreadKey threadKey = r0.A01;
            C79743rA r1 = r0.A00;
            if (threadKey.equals(r1.A03)) {
                r1.A0G(threadKey);
            }
        }
    }
}
