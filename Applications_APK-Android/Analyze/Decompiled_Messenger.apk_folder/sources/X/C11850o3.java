package X;

import android.app.Activity;

/* renamed from: X.0o3  reason: invalid class name and case insensitive filesystem */
public final class C11850o3 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.AnalyticsActivityListener$2";
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AnonymousClass1EY A01;

    public C11850o3(AnonymousClass1EY r1, Activity activity) {
        this.A01 = r1;
        this.A00 = activity;
    }

    public void run() {
        AnonymousClass1EY r0 = this.A01;
        ((AnonymousClass146) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A5w, r0.A00)).A0B(this.A00);
    }
}
