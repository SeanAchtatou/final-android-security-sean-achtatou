package X;

import java.util.Random;

/* renamed from: X.0BM  reason: invalid class name */
public final class AnonymousClass0BM {
    public int A00;
    public int A01 = -2;
    public final int A02;
    private final int A03;

    public String toString() {
        return String.format(null, "ParameterizedRetryState (%d,%d): multiplier:%d, interval:%d", Integer.valueOf(this.A02), Integer.valueOf(this.A03), Integer.valueOf(this.A01), Integer.valueOf(this.A00));
    }

    public AnonymousClass0BM(int i, int i2) {
        this.A02 = i;
        this.A03 = i2;
        new Random();
        this.A00 = i;
    }
}
