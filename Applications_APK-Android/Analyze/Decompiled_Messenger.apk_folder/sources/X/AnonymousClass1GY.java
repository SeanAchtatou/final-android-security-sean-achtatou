package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.interstitial.triggers.InterstitialTrigger;

/* renamed from: X.1GY  reason: invalid class name */
public final class AnonymousClass1GY implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InterstitialTrigger(parcel);
    }

    public Object[] newArray(int i) {
        return new InterstitialTrigger[i];
    }
}
