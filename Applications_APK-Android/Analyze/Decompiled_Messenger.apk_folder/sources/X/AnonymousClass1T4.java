package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1T4  reason: invalid class name */
public final class AnonymousClass1T4 {
    private static volatile AnonymousClass1T4 A01;
    public final AnonymousClass15Q A00;

    public int A01(int i, int i2, int i3) {
        return Math.min(Math.max(Math.round(((float) i) / ((float) i2)) - 1, 0), i3);
    }

    public static final AnonymousClass1T4 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1T4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1T4(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass1T4(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass15Q.A01(r2);
    }
}
