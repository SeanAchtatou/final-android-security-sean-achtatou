package X;

import java.util.List;

/* renamed from: X.1ID  reason: invalid class name */
public abstract class AnonymousClass1ID {
    public int A00() {
        List list = ((AnonymousClass1RK) this).A02;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public int A01() {
        List list = ((AnonymousClass1RK) this).A03;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public boolean A02(int i, int i2) {
        AnonymousClass1RK r1 = (AnonymousClass1RK) this;
        Object obj = r1.A03.get(i);
        Object obj2 = r1.A02.get(i2);
        if (obj == obj2) {
            return true;
        }
        AnonymousClass10N r2 = r1.A00;
        if (r2 == null) {
            return obj.equals(obj2);
        }
        AnonymousClass1Kn r12 = new AnonymousClass1Kn();
        r12.A01 = obj;
        r12.A00 = obj2;
        return ((Boolean) r2.A00.Alq().AXa(r2, r12)).booleanValue();
    }

    public boolean A03(int i, int i2) {
        AnonymousClass1RK r1 = (AnonymousClass1RK) this;
        Object obj = r1.A03.get(i);
        Object obj2 = r1.A02.get(i2);
        if (obj == obj2) {
            return true;
        }
        AnonymousClass10N r2 = r1.A01;
        if (r2 == null) {
            return obj.equals(obj2);
        }
        C21931Jg r12 = new C21931Jg();
        r12.A01 = obj;
        r12.A00 = obj2;
        return ((Boolean) r2.A00.Alq().AXa(r2, r12)).booleanValue();
    }
}
