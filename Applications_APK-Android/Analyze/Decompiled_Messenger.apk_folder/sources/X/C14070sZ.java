package X;

/* renamed from: X.0sZ  reason: invalid class name and case insensitive filesystem */
public final class C14070sZ extends C28141eK {
    /* JADX WARNING: Removed duplicated region for block: B:22:0x000d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.Intent A00(X.C14070sZ r7, android.content.Intent r8, android.content.Context r9, java.util.List r10) {
        /*
            java.util.ArrayList r4 = new java.util.ArrayList
            int r0 = r10.size()
            r4.<init>(r0)
            java.util.Iterator r6 = r10.iterator()
        L_0x000d:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0048
            java.lang.Object r5 = r6.next()
            android.content.pm.ComponentInfo r5 = (android.content.pm.ComponentInfo) r5
            android.content.pm.ApplicationInfo r0 = r5.applicationInfo
            if (r0 == 0) goto L_0x0046
            java.lang.String r1 = r0.packageName
            java.lang.String r0 = r9.getPackageName()
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0030
            r0 = 1
        L_0x002a:
            if (r0 == 0) goto L_0x000d
            r4.add(r5)
            goto L_0x000d
        L_0x0030:
            boolean r0 = r7.A0H()
            if (r0 == 0) goto L_0x0046
            X.04e r3 = r7.A00
            java.lang.String r0 = "Detected different package name component but fail open: "
            java.lang.String r2 = X.AnonymousClass08S.A0J(r0, r1)
            r1 = 0
            java.lang.String r0 = "InternalIntentScope"
            r3.C2T(r0, r2, r1)
            r0 = 1
            goto L_0x002a
        L_0x0046:
            r0 = 0
            goto L_0x002a
        L_0x0048:
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x0059
            X.04e r3 = r7.A00
            r2 = 0
            java.lang.String r1 = "InternalIntentScope"
            java.lang.String r0 = "No matching internal components"
            r3.C2T(r1, r0, r2)
            return r2
        L_0x0059:
            r0 = 0
            java.lang.Object r0 = r4.get(r0)
            android.content.pm.ComponentInfo r0 = (android.content.pm.ComponentInfo) r0
            android.content.ComponentName r2 = new android.content.ComponentName
            java.lang.String r1 = r0.packageName
            java.lang.String r0 = r0.name
            r2.<init>(r1, r0)
            r8.setComponent(r2)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14070sZ.A00(X.0sZ, android.content.Intent, android.content.Context, java.util.List):android.content.Intent");
    }

    public C14070sZ(C008807t r2, AnonymousClass04e r3) {
        this(r2, r3, new C28151eL());
    }

    public C14070sZ(C008807t r2, AnonymousClass04e r3, C28151eL r4) {
        super(r2, r3, false);
    }
}
