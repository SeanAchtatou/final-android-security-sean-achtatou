package X;

import android.os.Handler;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.00S  reason: invalid class name */
public final class AnonymousClass00S {
    public static final AnonymousClass020 A00 = new AnonymousClass020(null);
    public static final ReferenceQueue A01 = new ReferenceQueue();
    public static final ReferenceQueue A02 = new ReferenceQueue();
    private static final AtomicInteger A03 = new AtomicInteger(0);

    public static Runnable A00(Runnable runnable, int i) {
        if (runnable == null) {
            throw new NullPointerException("runnable for Handler call is null");
        } else if (!TraceEvents.isEnabled(AnonymousClass00n.A01)) {
            return runnable;
        } else {
            AnonymousClass0Hi r4 = new AnonymousClass0Hi(runnable, Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 16, 0, 0, i, 0, 0), i);
            AnonymousClass020 r3 = A00;
            synchronized (r3) {
                AnonymousClass020 r2 = r3.A00;
                while (r2 != r3 && r2.get() != runnable) {
                    r2 = r2.A00;
                }
                if (r2 == r3) {
                    r2 = new AnonymousClass020(runnable, A01);
                    AnonymousClass020 r0 = r3.A00;
                    r0.A03 = r2;
                    r2.A00 = r0;
                    r2.A03 = r3;
                    r3.A00 = r2;
                }
                AnonymousClass020 r1 = new AnonymousClass020(r4, A02);
                AnonymousClass020 r02 = r2.A02;
                r02.A01 = r1;
                r1.A02 = r02;
                r1.A01 = r2;
                r2.A02 = r1;
            }
            A01();
            return r4;
        }
    }

    private static void A01() {
        if (TraceEvents.isEnabled(AnonymousClass00n.A01) && A03.incrementAndGet() >= 50 && Thread.currentThread().getId() != 1) {
            synchronized (A00) {
                if (A03.get() >= 50) {
                    while (true) {
                        Reference poll = A01.poll();
                        if (poll == null) {
                            break;
                        }
                        ((AnonymousClass020) poll).A00();
                    }
                    while (true) {
                        Reference poll2 = A02.poll();
                        if (poll2 == null) {
                            break;
                        }
                        ((AnonymousClass020) poll2).A00();
                    }
                    A03.set(0);
                }
            }
        }
    }

    public static void A02(Handler handler, Runnable runnable) {
        handler.removeCallbacks(runnable);
        AnonymousClass020 r3 = A00;
        synchronized (r3) {
            AnonymousClass020 r2 = r3.A00;
            while (r2 != r3 && r2.get() != runnable) {
                r2 = r2.A00;
            }
            if (r2 != r3) {
                for (AnonymousClass020 r1 = r2.A02; r1 != r2; r1 = r1.A02) {
                    Runnable runnable2 = (Runnable) r1.get();
                    if (runnable2 != null) {
                        handler.removeCallbacks(runnable2);
                    }
                }
            }
        }
        A01();
    }

    public static void A03(Handler handler, Runnable runnable, int i) {
        handler.postAtFrontOfQueue(A00(runnable, i));
    }

    public static boolean A04(Handler handler, Runnable runnable, int i) {
        return handler.post(A00(runnable, i));
    }

    public static boolean A05(Handler handler, Runnable runnable, long j, int i) {
        return handler.postDelayed(A00(runnable, i), j);
    }
}
