package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1r5  reason: invalid class name and case insensitive filesystem */
public final class C35351r5 implements AnonymousClass1FI {
    public static final HashSet A00 = new HashSet(Arrays.asList("/br_sr", "/rs_req"));
    public static final HashSet A01 = new HashSet(Arrays.asList("/sr_res", "/rs_resp"));
    private static volatile C35351r5 A02;

    public static final C35351r5 A00(AnonymousClass1XY r3) {
        if (A02 == null) {
            synchronized (C35351r5.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A02 = new C35351r5();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public ImmutableMap get() {
        ImmutableMap.Builder builder = new ImmutableMap.Builder();
        Iterator it = A01.iterator();
        while (it.hasNext()) {
            builder.put(new SubscribeTopic((String) it.next(), AnonymousClass08G.A00(AnonymousClass07B.A01)), AnonymousClass1FP.ALWAYS);
        }
        return builder.build();
    }
}
