package X;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/* renamed from: X.0at  reason: invalid class name and case insensitive filesystem */
public final class C06120at {
    public List A00;
    public Map A01;
    public Pattern A02;
    public final Object A03;
    public final String A04;
    public final /* synthetic */ C06110ar A05;

    public C06120at(C06110ar r1, String str, Object obj) {
        this.A05 = r1;
        this.A04 = str;
        this.A03 = obj;
    }
}
