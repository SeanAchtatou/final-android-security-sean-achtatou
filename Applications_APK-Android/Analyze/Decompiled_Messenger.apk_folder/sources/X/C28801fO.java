package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1fO  reason: invalid class name and case insensitive filesystem */
public final class C28801fO {
    private static volatile C28801fO A00;

    public static final C28801fO A01(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C28801fO.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C28801fO();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C28801fO A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
