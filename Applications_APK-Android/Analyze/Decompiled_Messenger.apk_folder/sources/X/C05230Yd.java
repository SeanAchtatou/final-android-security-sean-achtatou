package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.0Yd  reason: invalid class name and case insensitive filesystem */
public abstract class C05230Yd {
    public Boolean A00 = null;
    public Boolean A01 = null;
    public final File A02;
    public final File A03;

    public static void A00(C05230Yd r3) {
        r3.A00 = Boolean.valueOf(new File(r3.A02, "enable_dialtone_mode").exists());
    }

    public static void A01(C05230Yd r3) {
        r3.A01 = Boolean.valueOf(new File(r3.A03, "enable_dialtone_mode").exists());
    }

    public C05230Yd(Context context) {
        this.A02 = new File(context.getCacheDir(), "fb_dialtone_signal");
        this.A03 = new File(context.getFilesDir(), "fb_dialtone_signal");
    }
}
