package X;

import java.util.Map;

/* renamed from: X.1Op  reason: invalid class name and case insensitive filesystem */
public final class C23201Op {
    public final int A00;
    public final Map A01;
    public final String[] A02;

    public int A00(String str) {
        Integer num = (Integer) this.A01.get(str);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    public C23201Op(String[] strArr, Map map) {
        this.A02 = strArr;
        this.A01 = map;
        this.A00 = strArr.length;
    }
}
