package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Uq  reason: invalid class name */
public final class AnonymousClass1Uq extends AnonymousClass1Ur {
    private static volatile AnonymousClass1Uq A00;

    public static final AnonymousClass1Uq A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (AnonymousClass1Uq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new AnonymousClass1Uq(AnonymousClass1YA.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private AnonymousClass1Uq(Context context) {
        super(context);
    }
}
