package X;

import android.content.Context;
import com.facebook.stash.core.Stash;
import java.io.File;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1rY  reason: invalid class name and case insensitive filesystem */
public final class C35641rY implements AnonymousClass1LS {
    private static volatile C35641rY A01;
    private final Stash A00;

    public void ATT(String str, File file) {
    }

    public static final C35641rY A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (C35641rY.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A01 = new C35641rY(AnonymousClass1YA.A02(applicationInjector), C30931is.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public File BDA(String str) {
        File insert = this.A00.insert(str);
        if (insert == null) {
            return null;
        }
        File parentFile = insert.getParentFile();
        if (parentFile == null) {
            C010708t.A0D(C35641rY.class, "Unable to resolve parent directory for: %s", insert.toString());
            return null;
        }
        parentFile.mkdirs();
        return insert;
    }

    public File getResource(String str) {
        if (this.A00.hasKey(str)) {
            return this.A00.getResource(str);
        }
        return null;
    }

    public boolean remove(String str) {
        return this.A00.remove(str);
    }

    private C35641rY(Context context, C30931is r8) {
        File file = new File(C35651rZ.A00(context, false, "ras_blobs", "latest", "sessionless"));
        C30941it r3 = new C30941it();
        r3.A03 = "ras_blobs";
        r3.A00 = C30961iv.A00(20971520);
        r3.A01 = new AnonymousClass0ZZ(C35611rV.A00);
        this.A00 = r8.A01(file, r3.A00());
    }
}
