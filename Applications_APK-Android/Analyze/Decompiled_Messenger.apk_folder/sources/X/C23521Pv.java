package X;

import android.net.Uri;

/* renamed from: X.1Pv  reason: invalid class name and case insensitive filesystem */
public final class C23521Pv {
    public Uri A00 = null;
    public C196579Mo A01 = null;
    public C23541Px A02 = C23541Px.A09;
    public C23561Pz A03 = C23561Pz.HIGH;
    public AnonymousClass36w A04 = null;
    public AnonymousClass1Q1 A05 = null;
    public C23411Pk A06;
    public AnonymousClass1OM A07 = AnonymousClass1OM.DEFAULT;
    public C23531Pw A08 = C23531Pw.FULL_FETCH;
    public AnonymousClass32I A09 = null;
    public Boolean A0A = null;
    public Boolean A0B = null;
    public boolean A0C = true;
    public boolean A0D = false;
    public boolean A0E = AnonymousClass1PL.A0Q.A00;

    public static C23521Pv A00(Uri uri) {
        C23521Pv r0 = new C23521Pv();
        C05520Zg.A02(uri);
        r0.A00 = uri;
        return r0;
    }

    public static C23521Pv A01(AnonymousClass1Q0 r2) {
        C23521Pv A002 = A00(r2.A02);
        A002.A02 = r2.A04;
        A002.A01 = r2.A03;
        A002.A07 = r2.A09;
        A002.A0D = r2.A0G;
        A002.A08 = r2.A0A;
        A002.A09 = r2.A0B;
        A002.A0E = r2.A0H;
        A002.A03 = r2.A05;
        A002.A04 = r2.A06;
        A002.A06 = r2.A08;
        A002.A05 = r2.A07;
        A002.A0A = r2.A0C;
        return A002;
    }

    public AnonymousClass1Q0 A02() {
        String scheme;
        Uri uri = this.A00;
        if (uri != null) {
            if (C006206h.A05(uri)) {
                if (!uri.isAbsolute()) {
                    throw new C91164Wy("Resource URI path must be absolute.");
                } else if (!this.A00.getPath().isEmpty()) {
                    try {
                        Integer.parseInt(this.A00.getPath().substring(1));
                    } catch (NumberFormatException unused) {
                        throw new C91164Wy("Resource URI path must be a resource id.");
                    }
                } else {
                    throw new C91164Wy("Resource URI must not be empty");
                }
            }
            Uri uri2 = this.A00;
            if (uri2 == null) {
                scheme = null;
            } else {
                scheme = uri2.getScheme();
            }
            if (!"asset".equals(scheme) || uri2.isAbsolute()) {
                return new AnonymousClass1Q0(this);
            }
            throw new C91164Wy("Asset URI path must be absolute.");
        }
        throw new C91164Wy("Source must be set!");
    }

    private C23521Pv() {
    }
}
