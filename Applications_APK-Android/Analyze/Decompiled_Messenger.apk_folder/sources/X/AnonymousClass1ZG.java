package X;

import com.google.common.base.MoreObjects;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ZG  reason: invalid class name */
public final class AnonymousClass1ZG implements AnonymousClass0Z1 {
    private static volatile AnonymousClass1ZG A00;

    public String Amj() {
        return "fresco";
    }

    public static final AnonymousClass1ZG A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass1ZG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass1ZG();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public String getCustomData(Throwable th) {
        MoreObjects.ToStringHelper toStringHelper = new MoreObjects.ToStringHelper("Fresco");
        C22581Ma A04 = C22581Ma.A04();
        AnonymousClass217 r2 = new AnonymousClass217("ImagePipelineFactory");
        AnonymousClass217.A00(r2, "bitmapCountingMemoryCache", A04.A01.A0C());
        AnonymousClass217.A00(r2, "encodedCountingMemoryCache", A04.A02.A0C());
        toStringHelper.add("pipeline", r2.toString());
        AnonymousClass217 r22 = new AnonymousClass217("SharedReference");
        AnonymousClass217.A00(r22, "live_objects_count", String.valueOf(AnonymousClass1SA.A03.size()));
        toStringHelper.add("SharedReference", r22.toString());
        return toStringHelper.toString();
    }
}
