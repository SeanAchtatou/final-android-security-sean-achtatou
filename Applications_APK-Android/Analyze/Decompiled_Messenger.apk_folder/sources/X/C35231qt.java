package X;

import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.1qt  reason: invalid class name and case insensitive filesystem */
public final class C35231qt implements C193117q {
    public final /* synthetic */ ThreadListFragment A00;

    public C35231qt(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public void BPU() {
        ThreadListFragment threadListFragment = this.A00;
        AnonymousClass10D r1 = threadListFragment.A0x;
        InterstitialTrigger interstitialTrigger = AnonymousClass3VY.A07;
        ThreadListFragment.A0M(threadListFragment);
        r1.A02(interstitialTrigger);
    }
}
