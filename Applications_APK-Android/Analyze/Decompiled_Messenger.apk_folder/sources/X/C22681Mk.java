package X;

import java.util.Comparator;

/* renamed from: X.1Mk  reason: invalid class name and case insensitive filesystem */
public final class C22681Mk implements Comparator {
    public int compare(Object obj, Object obj2) {
        Integer num;
        Integer num2;
        Runnable runnable = (Runnable) obj;
        Runnable runnable2 = (Runnable) obj2;
        if (runnable instanceof A59) {
            num = ((A59) runnable).A00;
        } else {
            num = AnonymousClass07B.A01;
        }
        int intValue = num.intValue();
        if (runnable2 instanceof A59) {
            num2 = ((A59) runnable2).A00;
        } else {
            num2 = AnonymousClass07B.A01;
        }
        return num2.intValue() - intValue;
    }
}
