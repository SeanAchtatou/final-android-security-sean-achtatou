package X;

import com.facebook.messaging.model.threads.ThreadSummary;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Js  reason: invalid class name and case insensitive filesystem */
public final class C22051Js implements C23831Rc {
    private static volatile C22051Js A01;
    private final AnonymousClass1JT A00;

    public static final C22051Js A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C22051Js.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C22051Js(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public C49252bu B3Q(ThreadSummary threadSummary) {
        AnonymousClass1JT r1 = this.A00;
        if (!AnonymousClass1JT.A01(threadSummary) || r1.A02(threadSummary)) {
            return null;
        }
        return new C49252bu(2131833225, AnonymousClass1JZ.A0E);
    }

    private C22051Js(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1JT.A00(r2);
    }
}
