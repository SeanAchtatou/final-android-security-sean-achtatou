package X;

import android.util.SparseArray;
import com.facebook.acra.ACRA;
import com.facebook.common.util.TriState;
import com.facebook.forker.Process;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0Tg  reason: invalid class name and case insensitive filesystem */
public final class C04270Tg implements C04280Th {
    public static final C08920gC A0c = new C08910gB(500);
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public long A0D;
    public SparseArray A0E;
    public SparseArray A0F;
    public AnonymousClass00T A0G;
    public TriState A0H;
    public C11100lt A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public short A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    private C04270Tg A0W;
    public final AnonymousClass0Tj A0X = new AnonymousClass0Tj();
    public final ArrayList A0Y = new ArrayList();
    public final ArrayList A0Z = new ArrayList();
    public final ArrayList A0a = new ArrayList();
    public final AtomicInteger A0b = new AtomicInteger();

    public void clear() {
        this.A08 = 0;
        this.A01 = 0;
        this.A0C = 0;
        this.A0D = 0;
        this.A09 = 0;
        this.A02 = 0;
        this.A06 = 0;
        this.A0A = 0;
        this.A0Q = false;
        this.A0R = false;
        this.A0J = null;
        this.A0S = false;
        this.A0V = false;
        this.A0U = false;
        this.A0G = null;
        this.A0H = TriState.UNSET;
        this.A0Y.clear();
        this.A0Z.clear();
        this.A0a.clear();
        this.A0X.A04();
        C11100lt r3 = this.A0I;
        if (r3 != null) {
            r3.A01 = 0;
            r3.A00 = 0;
            Arrays.fill(r3.A07, (Object) null);
            Arrays.fill(r3.A06, (Object) null);
            Arrays.fill(r3.A05, (Object) null);
            ArrayList arrayList = r3.A02;
            if (arrayList != null) {
                arrayList.clear();
            }
        }
        this.A0M = 0;
        this.A0B = 0;
        this.A0N = false;
        this.A0P = false;
        this.A0T = false;
        this.A05 = 0;
        this.A0E = null;
        this.A0F = null;
        this.A07 = 0;
        this.A0K = null;
        this.A03 = 0;
        this.A04 = 0;
        this.A00 = 7;
        this.A0L = null;
    }

    public static void A00(C04270Tg r1, String str, String str2, int i) {
        r1.A0Y.add(str);
        r1.A0Y.add(str2);
        r1.A0Z.add(Integer.valueOf(i));
    }

    public String A01(String str) {
        int i;
        String A022;
        int i2;
        if (this.A0O) {
            AnonymousClass0Tj r4 = this.A0X;
            if (r4.A03 != 0) {
                String str2 = null;
                int i3 = 0;
                int i4 = 0;
                int i5 = 0;
                for (int i6 = 0; i6 < r4.A03; i6++) {
                    boolean equals = str.equals(r4.A05.get(i6));
                    byte b = r4.A07[i6];
                    switch (b) {
                        case 1:
                            if (equals) {
                                i = i3 + 1;
                                A022 = (String) r4.A06.get(i3);
                                i3 = i;
                                break;
                            }
                            i3++;
                            break;
                        case 2:
                            if (equals) {
                                i2 = i4 + 1;
                                str2 = Integer.toString((int) r4.A09[i4]);
                                i4 = i2;
                                break;
                            }
                            i4++;
                            break;
                        case 3:
                            if (equals) {
                                i2 = i4 + 1;
                                str2 = Long.toString(r4.A09[i4]);
                                i4 = i2;
                                break;
                            }
                            i4++;
                            break;
                        case 4:
                            if (equals) {
                                i = i3 + 1;
                                A022 = AnonymousClass36y.A03((String[]) r4.A06.get(i3));
                                i3 = i;
                                break;
                            }
                            i3++;
                            break;
                        case 5:
                            if (equals) {
                                i = i3 + 1;
                                A022 = AnonymousClass36y.A01((int[]) r4.A06.get(i3));
                                i3 = i;
                                break;
                            }
                            i3++;
                            break;
                        case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                            if (!equals) {
                                i5++;
                                break;
                            } else {
                                str2 = Double.toString(r4.A08[i5]);
                                i5++;
                                break;
                            }
                        case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                            if (equals) {
                                i = i3 + 1;
                                A022 = AnonymousClass36y.A00((double[]) r4.A06.get(i3));
                                i3 = i;
                                break;
                            }
                            i3++;
                            break;
                        case 8:
                            if (equals) {
                                i2 = i4 + 1;
                                boolean z = false;
                                if (r4.A09[i4] != 0) {
                                    z = true;
                                }
                                str2 = Boolean.toString(z);
                                i4 = i2;
                                break;
                            }
                            i4++;
                            break;
                        case Process.SIGKILL:
                            if (equals) {
                                i = i3 + 1;
                                A022 = AnonymousClass36y.A04((boolean[]) r4.A06.get(i3));
                                i3 = i;
                                break;
                            }
                            i3++;
                            break;
                        case AnonymousClass1Y3.A01 /*10*/:
                            if (equals) {
                                i = i3 + 1;
                                A022 = AnonymousClass36y.A02((long[]) r4.A06.get(i3));
                                i3 = i;
                                break;
                            }
                            i3++;
                            break;
                        default:
                            throw new UnsupportedOperationException(AnonymousClass08S.A09("Unsupported type ", b));
                    }
                    if (equals) {
                        return str2;
                    }
                }
            }
            return null;
        }
        int size = this.A0Y.size();
        for (int i7 = 0; i7 < size; i7 += 2) {
            if (str.equals(this.A0Y.get(i7))) {
                return (String) this.A0Y.get(i7 + 1);
            }
        }
        return null;
    }

    public List A02() {
        if (this.A0O) {
            return this.A0X.A03();
        }
        return this.A0Y;
    }

    public void A03(TriState triState, boolean z) {
        if (z || this.A0H != TriState.YES) {
            if (triState == null) {
                triState = TriState.UNSET;
            }
            this.A0H = triState;
        }
    }

    public void A04(String str, double d) {
        if (this.A0O) {
            AnonymousClass0Tj r4 = this.A0X;
            r4.A05.add(str);
            int i = r4.A00;
            double[] dArr = r4.A08;
            int length = dArr.length;
            if (i == length) {
                r4.A08 = Arrays.copyOf(dArr, (int) (((double) length) * 1.4d));
            }
            double[] dArr2 = r4.A08;
            int i2 = r4.A00;
            r4.A00 = i2 + 1;
            dArr2[i2] = d;
            AnonymousClass0Tj.A00(r4, (byte) 6);
            return;
        }
        A00(this, str, String.valueOf(d), 5);
    }

    public void A05(String str, int i) {
        if (this.A0O) {
            AnonymousClass0Tj r2 = this.A0X;
            r2.A05.add(str);
            AnonymousClass0Tj.A01(r2, (long) i);
            AnonymousClass0Tj.A00(r2, (byte) 2);
            return;
        }
        A00(this, str, String.valueOf(i), 2);
    }

    public void A06(String str, long j) {
        if (this.A0O) {
            AnonymousClass0Tj r1 = this.A0X;
            r1.A05.add(str);
            AnonymousClass0Tj.A01(r1, j);
            AnonymousClass0Tj.A00(r1, (byte) 3);
            return;
        }
        A00(this, str, String.valueOf(j), 2);
    }

    public void A07(String str, String str2) {
        if (this.A0O) {
            AnonymousClass0Tj r1 = this.A0X;
            r1.A05.add(str);
            r1.A06.add(str2);
            AnonymousClass0Tj.A00(r1, (byte) 1);
        } else {
            A00(this, str, str2, 1);
        }
        if (str.equals("tag_name")) {
            this.A0S = true;
        }
    }

    public void A08(String str, boolean z) {
        long j;
        if (this.A0O) {
            AnonymousClass0Tj r2 = this.A0X;
            r2.A05.add(str);
            if (z) {
                j = 1;
            } else {
                j = 0;
            }
            AnonymousClass0Tj.A01(r2, j);
            AnonymousClass0Tj.A00(r2, (byte) 8);
            return;
        }
        A00(this, str, String.valueOf(z), 7);
    }

    public void A09(String str, String[] strArr) {
        if (this.A0O) {
            AnonymousClass0Tj r2 = this.A0X;
            r2.A05.add(str);
            r2.A06.add(Arrays.copyOf(strArr, strArr.length));
            AnonymousClass0Tj.A00(r2, (byte) 4);
            return;
        }
        A00(this, str, AnonymousClass36y.A03(strArr), 3);
    }

    public Object Avh() {
        return this.A0W;
    }

    public void BqE() {
        this.A0b.incrementAndGet();
    }

    public void C9o(Object obj) {
        this.A0W = (C04270Tg) obj;
    }

    public int getMarkerId() {
        return this.A02;
    }
}
