package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0Rm  reason: invalid class name and case insensitive filesystem */
public final class C04020Rm {
    public static String A00(int i) {
        switch (i) {
            case 1:
                return "MESSENGER_SEND_MESSAGE";
            case 2:
                return "MESSENGER_WARM_START";
            case 3:
                return "MESSENGER_COLD_START";
            case 4:
                return "MESSENGER_LUKE_WARM_START";
            case 5:
                return "MESSENGER_EXTERNAL_INTENT";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR /*6*/:
                return "MESSENGER_NAVIGATION_EVENT";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "MESSENGER_NAVIGATION";
            case 8:
                return "MESSENGER_THREADLIST_TO_THREADVIEW";
            case Process.SIGKILL /*9*/:
            case AnonymousClass1Y3.A03 /*12*/:
            case 13:
            case 14:
            case 15:
            case 16:
            case 20:
            case AnonymousClass1Y3.A06 /*22*/:
            case AnonymousClass1Y3.A07 /*24*/:
            case 35:
            case AnonymousClass1Y3.A0D /*37*/:
            case AnonymousClass1Y3.A0E /*38*/:
            case AnonymousClass1Y3.A0F /*39*/:
            case AnonymousClass1Y3.A0G /*40*/:
            case AnonymousClass1Y3.A0H /*41*/:
            case 42:
            case 43:
            case AnonymousClass1Y3.A0M /*51*/:
            case AnonymousClass1Y3.A0O /*53*/:
            case 54:
            case 55:
            case AnonymousClass1Y3.A0P /*56*/:
            case AnonymousClass1Y3.A0Q /*58*/:
            case 59:
            case AnonymousClass1Y3.A0R /*60*/:
            case 69:
            case 72:
            case AnonymousClass1Y3.A0W /*73*/:
            case 74:
            case AnonymousClass1Y3.A0a /*80*/:
            case 87:
            case 93:
            case AnonymousClass1Y3.A0g /*96*/:
            case 100:
            case AnonymousClass1Y3.A0i /*101*/:
            case AnonymousClass1Y3.A0j /*102*/:
            case 103:
            case AnonymousClass1Y3.A0k /*104*/:
            case AnonymousClass1Y3.A0l /*105*/:
            case AnonymousClass1Y3.A0o /*109*/:
            case AnonymousClass1Y3.A0p /*110*/:
            case 111:
            case AnonymousClass1Y3.A0q /*113*/:
            case 115:
            case 116:
            case AnonymousClass1Y3.A0r /*117*/:
            case 118:
            case AnonymousClass1Y3.A0u /*121*/:
            case 123:
            case 124:
            case AnonymousClass1Y3.A0w /*125*/:
            case AnonymousClass1Y3.A0x /*126*/:
            case 127:
            case 128:
            case AnonymousClass1Y3.A0y /*130*/:
            case AnonymousClass1Y3.A0z /*131*/:
            case AnonymousClass1Y3.A11 /*135*/:
            case AnonymousClass1Y3.A13 /*138*/:
            case 139:
            case AnonymousClass1Y3.A14 /*140*/:
            case AnonymousClass1Y3.A15 /*141*/:
            case AnonymousClass1Y3.A16 /*142*/:
            case AnonymousClass1Y3.A17 /*144*/:
            case 145:
            case 146:
            case AnonymousClass1Y3.A1B /*150*/:
            case 160:
            case 164:
            case 165:
            case AnonymousClass1Y3.A1K /*171*/:
            case AnonymousClass1Y3.A1N /*175*/:
            case AnonymousClass1Y3.A1O /*176*/:
            case AnonymousClass1Y3.A1P /*177*/:
            case 178:
            case AnonymousClass1Y3.A1U /*188*/:
            case AnonymousClass1Y3.A1X /*191*/:
            default:
                return "UNDEFINED_QPL_EVENT";
            case AnonymousClass1Y3.A01 /*10*/:
                return "MESSENGER_EXTERNAL_TO_THREADVIEW";
            case AnonymousClass1Y3.A02 /*11*/:
                return "MESSENGER_THREADLIST_DB_FETCH";
            case 17:
                return "MESSENGER_THREAD_DATA_FETCH";
            case Process.SIGCONT /*18*/:
                return "MESSENGER_THREADLIST_DATA_FETCH";
            case Process.SIGSTOP /*19*/:
                return "MESSENGER_FIRST_COLD_START";
            case AnonymousClass1Y3.A05 /*21*/:
                return "MESSENGER_THREAD_LIST_FRAGMENT_CREATE";
            case 23:
                return "MESSENGER_THREAD_VIEW_FRAGMENT_CREATE";
            case 25:
                return "MESSENGER_THREAD_VIEW_MESSAGES_FRAGMENT_CREATE";
            case AnonymousClass1Y3.A08 /*26*/:
                return "MESSENGER_CANONICAL_PRESENCE_CHECKS";
            case AnonymousClass1Y3.A09 /*27*/:
                return "MESSENGER_DISPLAYED_PAGE_PRESENCE_ONLINE_VALUES";
            case 28:
                return "MESSENGER_CANONICAL_PRESENCE_DOUBLESTALE";
            case 29:
                return "MESSENGER_CANONICAL_PRESENCE_LASTREAD_STALE_MORETHANTHRESHOLD";
            case AnonymousClass1Y3.A0A /*30*/:
                return "MESSENGER_CANONICAL_PRESENCE_LASTREAD_STALE_LESSTHANTHRESHOLD";
            case AnonymousClass1Y3.A0B /*31*/:
                return "MESSENGER_CANONICAL_PRESENCE_LASTSENT_STALE_LESSTHANTHRESHOLD";
            case 32:
                return "MESSENGER_CANONICAL_PRESENCE_LASTSENT_STALE_MORETHANTHRESHOLD";
            case 33:
                return "MESSENGER_CANONICAL_NEW_PRESENCE_PUSH";
            case AnonymousClass1Y3.A0C /*34*/:
                return "MESSENGER_CANONICAL_NO_PRESENCE";
            case 36:
                return "MESSENGER_USER_TYPING";
            case AnonymousClass1Y3.A0I /*44*/:
                return "MESSENGER_MAIN_ACTIVITY_CREATE";
            case AnonymousClass1Y3.A0J /*45*/:
                return "MESSENGER_HOME_CREATE_VIEW";
            case AnonymousClass1Y3.A0K /*46*/:
                return "MESSENGER_HOME_CREATE_VIEW_TO_ONACTIVITYCREATED";
            case AnonymousClass1Y3.A0L /*47*/:
                return "MESSENGER_HOME_ONACTIVITYCREATED";
            case 48:
                return "MESSENGER_HOME_ONACTIVITYCREATED_TO_THREAD_LIST_CREATE";
            case 49:
                return "MESSENGER_THREAD_LIST_LAYOUT";
            case 50:
                return "MESSENGER_THREAD_LIST_LAYOUT_TO_DRAW";
            case AnonymousClass1Y3.A0N /*52*/:
                return "MESSENGER_MESSAGE_SEND_EXPERIENCE";
            case 57:
                return "MESSENGER_THREAD_LIST_SCROLL";
            case 61:
                return "MESSENGER_LOCAL_MEDIA_LOAD";
            case 62:
                return "MESSENGER_CAMERA_PHOTO_PROCESSING";
            case AnonymousClass1Y3.A0S /*63*/:
                return "MESSENGER_CAMERA_VIDEO_PROCESSING";
            case 64:
                return "MESSENGER_CAMERA_EFFECT_PICKER_LOAD";
            case AnonymousClass1Y3.A0T /*65*/:
                return "MESSENGER_CAMERA_EFFECT_SEARCH";
            case 66:
                return "MESSENGER_CAMERA_EFFECT_FETCH_CACHE";
            case 67:
                return "MESSENGER_CAMERA_EFFECT_FETCH_NETWORK";
            case AnonymousClass1Y3.A0U /*68*/:
                return "MESSENGER_CAMERA_EFFECT_APPLY";
            case 70:
                return "MESSENGER_CAMERA_EFFECT_PICKER_FEATURED_LOAD";
            case AnonymousClass1Y3.A0V /*71*/:
                return "MESSENGER_CAMERA_EFFECT_PICKER_SECTION_LOAD";
            case AnonymousClass1Y3.A0X /*75*/:
                return "MESSENGER_MEASURE_PASS";
            case AnonymousClass1Y3.A0Y /*76*/:
                return "MESSENGER_LAYOUT_PASS";
            case AnonymousClass1Y3.A0Z /*77*/:
                return "MESSENGER_DRAW_PASS";
            case 78:
                return "MESSENGER_UI_DATA_REFRESH";
            case 79:
                return "MESSENGER_COMPONENTS_REFRESH";
            case AnonymousClass1Y3.A0b /*81*/:
                return "MESSENGER_THREAD_MESSAGES_SCROLL";
            case 82:
                return "MESSENGER_MESSENGER_ANDROID_FETCH_THREADSUMMARY_PERF";
            case 83:
                return "MESSENGER_MESSENGER_ANDROID_FETCH_MESSAGES_PERF";
            case AnonymousClass1Y3.A0c /*84*/:
                return "MESSENGER_MESSENGER_ANDROID_FETCH_USERLIST_PERF";
            case 85:
                return "MESSENGER_MESSENGER_ANDROID_FETCH_THREAD_WITHOUT_OPTIMIZATION";
            case 86:
                return "MESSENGER_MESSENGER_ANDROID_FETCH_THREAD_WITH_OPTIMIZATION";
            case 88:
                return "MESSENGER_ANDROID_OMNISTORE_COMPONENT_MANAGER_INIT";
            case AnonymousClass1Y3.A0d /*89*/:
                return "MESSENGER_ANDROID_OMNISTORE_ON_SENDER_AVAILABLE";
            case AnonymousClass1Y3.A0e /*90*/:
                return "MESSENGER_ANDROID_OMNISTORE_PROVIDE_SUBSCRIPTION_INFO";
            case 91:
                return "MESSENGER_ANDROID_OMNISTORE_ON_COLLECTION_AVAILABLE";
            case 92:
                return "MESSENGER_ANDROID_OMNISTORE_COMPONENT_MANAGER_OPEN";
            case AnonymousClass1Y3.A0f /*94*/:
                return "MESSENGER_TINCAN_PRIVATE_ATTACHMENT_LOGGING";
            case 95:
                return "MESSENGER_LOADING_LATENCY";
            case 97:
                return "MESSENGER_ANDROID_TAB_NAVIGATION";
            case 98:
                return "MESSENGER_INBOX_LOAD_MORE_WAIT";
            case 99:
                return "MESSENGER_MESSAGES_LOAD_MORE_WAIT";
            case AnonymousClass1Y3.A0m /*106*/:
                return "MESSENGER_INBOX_DISPLAY";
            case AnonymousClass1Y3.A0n /*107*/:
                return "MESSENGER_INBOX_DAY_UNIT_DISPLAY";
            case 108:
                return "MESSENGER_THREAD_LIST_DISPLAY";
            case 112:
                return "MESSENGER_FETCH_MORE_THREADS";
            case 114:
                return "MESSENGER_THREADLIST_TO_THREADVIEW_V2";
            case AnonymousClass1Y3.A0s /*119*/:
                return "MESSENGER_CRITICAL_PATH_GRAPHQL_DELAY";
            case AnonymousClass1Y3.A0t /*120*/:
                return "MESSENGER_DELTA_APPLICATION_DONE";
            case AnonymousClass1Y3.A0v /*122*/:
                return "MESSENGER_CRITICAL_PATH_TASK_DELAY";
            case 129:
                return "MESSENGER_CAMERA_TIME_TO_CAPTURE_PHOTO";
            case 132:
                return "MESSENGER_CAMERA_TIME_TO_INTERACT";
            case AnonymousClass1Y3.A10 /*133*/:
                return "MESSENGER_CAMERA_TIME_BACK_FROM_PREVIEW";
            case 134:
                return "MESSENGER_CAMERA_TIME_TO_NATIVE_CAPTURE_PHOTO";
            case AnonymousClass1Y3.A12 /*136*/:
                return "MESSENGER_RTC_EFFECT_REMOVED";
            case 137:
                return "MESSENGER_ASSISTANT_VOICE_RESPONSE";
            case 143:
                return "MESSENGER_MESSAGE_SEND_PERFORMANCE_FUNNEL";
            case AnonymousClass1Y3.A18 /*147*/:
                return "MESSENGER_MEDIA_TRAY_INIT";
            case AnonymousClass1Y3.A19 /*148*/:
                return "MESSENGER_MEDIA_TRAY_PREPARE_ATTACHMENT";
            case AnonymousClass1Y3.A1A /*149*/:
                return "MESSENGER_DEEP_LINKING_THREAD_FETCH";
            case 151:
                return "MESSENGER_COLD_START_TO_THREADVIEW";
            case 152:
                return "MESSENGER_ANDROID_GROUP_CREATE_FLOW";
            case AnonymousClass1Y3.A1C /*153*/:
                return "MESSENGER_ANDROID_GROUP_SHARE_LINK_FLOW";
            case 154:
                return "MESSENGER_MEDIA_PICKER_INIT";
            case 155:
                return "MESSENGER_GROUP_MEMBER_REQUESTS";
            case AnonymousClass1Y3.A1D /*156*/:
                return "MESSENGER_REACTION";
            case AnonymousClass1Y3.A1E /*157*/:
                return "MESSENGER_REACTION_PANEL_SHOW_UP";
            case 158:
                return "MESSENGER_ANDROID_OMNIPICKER";
            case 159:
                return "MESSENGER_ANDROID_GROUP_ADD_MEMBERS_FLOW";
            case AnonymousClass1Y3.A1F /*161*/:
                return "MESSENGER_POSTCAPTURE_FACE_DETECTION";
            case AnonymousClass1Y3.A1G /*162*/:
                return "MESSENGER_THREADVIEW_TO_THREADLIST_BACK_NAV";
            case AnonymousClass1Y3.A1H /*163*/:
                return "MESSENGER_THREAD_METADATA_FETCH";
            case 166:
                return "MESSENGER_COLD_START_IN_BACKGROUND";
            case AnonymousClass1Y3.A1I /*167*/:
                return "MESSENGER_CRITICAL_PATH_ACTIVE";
            case 168:
                return "MESSENGER_CAMERA_TIME_TO_DISPLAY_PHOTO";
            case 169:
                return "MESSENGER_MEDIA_TRAY_SCROLL";
            case AnonymousClass1Y3.A1J /*170*/:
                return "MESSENGER_MEDIA_PICKER_SCROLL";
            case AnonymousClass1Y3.A1L /*172*/:
                return "MESSENGER_CAMERA_TIME_TO_START_VIDEO_CAPTURE";
            case 173:
                return "MESSENGER_CAMERA_TIME_TO_STOP_VIDEO_CAPTURE";
            case AnonymousClass1Y3.A1M /*174*/:
                return "MESSENGER_MONTAGE_AND_ACTIVE_NOW_LOADING_ANDROID";
            case 179:
                return "MESSENGER_MQTT_COLD_START_INIT";
            case AnonymousClass1Y3.A1Q /*180*/:
                return "MESSENGER_ANDROID_SEQUENTIAL_RANKING_LOAD_TIME";
            case AnonymousClass1Y3.A1R /*181*/:
                return "MESSENGER_POST_CHROME_ANDROID";
            case 182:
                return "MESSENGER_THREADS_DB_AUTO_MIGRATION_UPGRADE_TIME";
            case 183:
                return "MESSENGER_THREADS_DB_AUTO_MIGRATION_DATA_MIGRATION_TIME";
            case 184:
                return "MESSENGER_MONTAGE_VIEWER_LOAD_TTRC";
            case AnonymousClass1Y3.A1S /*185*/:
                return "MESSENGER_VOICE_RECORDING_TO_RESPONSE_ANDROID";
            case 186:
                return "MESSENGER_MONTAGE_AND_ACTIVE_NOW_HSCROLL";
            case AnonymousClass1Y3.A1T /*187*/:
                return "MESSENGER_RTC_SNAPSHOT";
            case AnonymousClass1Y3.A1V /*189*/:
                return "MESSENGER_RTC_PEER_SNAPSHOT";
            case AnonymousClass1Y3.A1W /*190*/:
                return "MESSENGER_RTC_SELF_SNAPSHOT";
            case 192:
                return "MESSENGER_MONTAGE_PEOPLE_TRAY_LOAD";
            case 193:
                return "MESSENGER_MONTAGE_OMNI_DELTA_HANDLING";
            case AnonymousClass1Y3.A1Y /*194*/:
                return "MESSENGER_MONTAGE_INBOX_LOAD_TTI";
            case 195:
                return "MESSENGER_M_SUGGESTIONS_GENERATED_ANDROID";
            case AnonymousClass1Y3.A1Z /*196*/:
                return "MESSENGER_INBOX_UNIT_DB_FETCH";
            case AnonymousClass1Y3.A1a /*197*/:
                return "MESSENGER_SCROLL_PERF";
            case 198:
                return "MESSENGER_MONTAGE_GIF_STICKER_TRANSCODING";
            case AnonymousClass1Y3.A1b /*199*/:
                return "MESSENGER_MONTAGE_PROCESS_MEDIA";
            case AnonymousClass1Y3.A1c /*200*/:
                return "MESSENGER_THREADLIST_TO_THREADVIEW_EXTERNAL";
            case AnonymousClass1Y3.A1d /*201*/:
                return "MESSENGER_MONTAGE_COMPOSER_LOAD_TTRC";
        }
    }
}
