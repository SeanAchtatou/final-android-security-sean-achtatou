package X;

import com.facebook.resources.impl.qt.model.QTStringResources;
import com.google.common.base.Optional;
import java.nio.ByteBuffer;
import java.util.Map;

/* renamed from: X.0fi  reason: invalid class name and case insensitive filesystem */
public abstract class C08650fi {
    public String A01(int i, int i2) {
        Optional absent;
        if (!(this instanceof QTStringResources)) {
            C08640fh r3 = (C08640fh) this;
            if (!r3.A05) {
                return r3.A01.A02(i, i2);
            }
            C08640fh.A00(r3, i2);
            Map map = r3.A04;
            Integer valueOf = Integer.valueOf(i);
            String str = (String) map.get(valueOf);
            if (str == null && (str = r3.A01.A02(i, i2)) != null) {
                r3.A04.put(valueOf, str);
            }
            return str;
        }
        QTStringResources qTStringResources = (QTStringResources) this;
        if (!qTStringResources.A02.equals(qTStringResources.A03.get())) {
            return null;
        }
        try {
            AnonymousClass0lk r32 = qTStringResources.A01;
            Map map2 = r32.A02;
            Integer valueOf2 = Integer.valueOf(i);
            if (map2.containsKey(valueOf2)) {
                C172457xJ A07 = r32.A00.A07(((Integer) r32.A02.get(valueOf2)).intValue());
                AnonymousClass7WQ r2 = new AnonymousClass7WQ();
                int A02 = A07.A02(6);
                if (A02 != 0) {
                    int A01 = A07.A01(A02 + A07.A00);
                    ByteBuffer byteBuffer = A07.A01;
                    r2.A00 = A01;
                    r2.A01 = byteBuffer;
                } else {
                    r2 = null;
                }
                absent = AnonymousClass0lk.A00(r2, i2);
                if (i2 != 0 && !absent.isPresent()) {
                    absent = AnonymousClass0lk.A00(r2, 0);
                }
            } else {
                absent = Optional.absent();
            }
            if (!absent.isPresent()) {
                return null;
            }
            AnonymousClass8N6 r5 = qTStringResources.A00;
            AnonymousClass07A.A04(r5.A04, new AnonymousClass8N8(r5, i, i2, (AnonymousClass85I) absent.get()), 828366373);
            return ((AnonymousClass85I) absent.get()).A06();
        } catch (Exception e) {
            C010708t.A0V("com.facebook.resources.impl.qt.model.QTStringResources", e, "Failed to get QT string resource. resource_id : %d, gender : %s, userId : %s", Integer.valueOf(i), C07230cz.A01(i2), qTStringResources.A02);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:66:0x019e A[Catch:{ Exception -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A02(int r13, int r14, X.AnonymousClass1ZB r15) {
        /*
            r12 = this;
            boolean r0 = r12 instanceof com.facebook.resources.impl.qt.model.QTStringResources
            r10 = r15
            if (r0 != 0) goto L_0x0058
            r3 = r12
            X.0fh r3 = (X.C08640fh) r3
            boolean r0 = r3.A05
            if (r0 == 0) goto L_0x0051
            X.C08640fh.A00(r3, r14)
            java.util.Map r0 = r3.A03
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            java.lang.Object r2 = r0.get(r1)
            X.3jp r2 = (X.C75423jp) r2
            if (r2 != 0) goto L_0x002a
            X.0fk r0 = r3.A01
            X.3jp r2 = r0.A01(r13, r14)
            if (r2 == 0) goto L_0x002a
            java.util.Map r0 = r3.A03
            r0.put(r1, r2)
        L_0x002a:
            if (r2 == 0) goto L_0x01d8
            java.util.Map r1 = r2.A00
            int r0 = r15.ordinal()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r1.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x0050
            X.1ZB r0 = X.AnonymousClass1ZB.OTHER
            java.util.Map r1 = r2.A00
            int r0 = r0.ordinal()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r1.get(r0)
            java.lang.String r0 = (java.lang.String) r0
        L_0x0050:
            return r0
        L_0x0051:
            X.0fk r0 = r3.A01
            X.3jp r2 = r0.A01(r13, r14)
            goto L_0x002a
        L_0x0058:
            r5 = r12
            com.facebook.resources.impl.qt.model.QTStringResources r5 = (com.facebook.resources.impl.qt.model.QTStringResources) r5
            java.lang.String r1 = r5.A02
            X.0Tq r0 = r5.A03
            java.lang.Object r0 = r0.get()
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x01d8
            X.0lk r3 = r5.A01     // Catch:{ Exception -> 0x01c2 }
            java.util.Map r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x01c2 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ Exception -> 0x01c2 }
            if (r0 == 0) goto L_0x0159
            X.7xH r1 = r3.A00     // Catch:{ Exception -> 0x01c2 }
            java.util.Map r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Exception -> 0x01c2 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x01c2 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01c2 }
            X.7xI r2 = r1.A06(r0)     // Catch:{ Exception -> 0x01c2 }
            X.7WP r3 = new X.7WP     // Catch:{ Exception -> 0x01c2 }
            r3.<init>()     // Catch:{ Exception -> 0x01c2 }
            r0 = 6
            int r1 = r2.A02(r0)     // Catch:{ Exception -> 0x01c2 }
            if (r1 == 0) goto L_0x00b1
            int r0 = r2.A00     // Catch:{ Exception -> 0x01c2 }
            int r1 = r1 + r0
            int r1 = r2.A01(r1)     // Catch:{ Exception -> 0x01c2 }
            java.nio.ByteBuffer r0 = r2.A01     // Catch:{ Exception -> 0x01c2 }
            r3.A00 = r1     // Catch:{ Exception -> 0x01c2 }
            r3.A01 = r0     // Catch:{ Exception -> 0x01c2 }
        L_0x00a2:
            int r0 = r15.ordinal()     // Catch:{ Exception -> 0x01c2 }
            switch(r0) {
                case 0: goto L_0x015e;
                case 1: goto L_0x0139;
                case 2: goto L_0x0118;
                case 3: goto L_0x00f7;
                case 4: goto L_0x00d5;
                case 5: goto L_0x00b3;
                default: goto L_0x00a9;
            }     // Catch:{ Exception -> 0x01c2 }
        L_0x00a9:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r0 = "Unexpected PluralCategory"
            r1.<init>(r0)     // Catch:{ Exception -> 0x01c2 }
            throw r1     // Catch:{ Exception -> 0x01c2 }
        L_0x00b1:
            r3 = 0
            goto L_0x00a2
        L_0x00b3:
            X.7WQ r2 = new X.7WQ     // Catch:{ Exception -> 0x01c2 }
            r2.<init>()     // Catch:{ Exception -> 0x01c2 }
            r0 = 14
            int r1 = r3.A02(r0)     // Catch:{ Exception -> 0x01c2 }
            if (r1 == 0) goto L_0x00d3
            int r0 = r3.A00     // Catch:{ Exception -> 0x01c2 }
            int r1 = r1 + r0
            int r1 = r3.A01(r1)     // Catch:{ Exception -> 0x01c2 }
            java.nio.ByteBuffer r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            r2.A00 = r1     // Catch:{ Exception -> 0x01c2 }
            r2.A01 = r0     // Catch:{ Exception -> 0x01c2 }
        L_0x00cd:
            com.google.common.base.Optional r1 = com.google.common.base.Optional.fromNullable(r2)     // Catch:{ Exception -> 0x01c2 }
            goto L_0x017b
        L_0x00d3:
            r2 = 0
            goto L_0x00cd
        L_0x00d5:
            X.7WQ r2 = new X.7WQ     // Catch:{ Exception -> 0x01c2 }
            r2.<init>()     // Catch:{ Exception -> 0x01c2 }
            r0 = 12
            int r1 = r3.A02(r0)     // Catch:{ Exception -> 0x01c2 }
            if (r1 == 0) goto L_0x00f5
            int r0 = r3.A00     // Catch:{ Exception -> 0x01c2 }
            int r1 = r1 + r0
            int r1 = r3.A01(r1)     // Catch:{ Exception -> 0x01c2 }
            java.nio.ByteBuffer r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            r2.A00 = r1     // Catch:{ Exception -> 0x01c2 }
            r2.A01 = r0     // Catch:{ Exception -> 0x01c2 }
        L_0x00ef:
            com.google.common.base.Optional r1 = com.google.common.base.Optional.fromNullable(r2)     // Catch:{ Exception -> 0x01c2 }
            goto L_0x017b
        L_0x00f5:
            r2 = 0
            goto L_0x00ef
        L_0x00f7:
            X.7WQ r2 = new X.7WQ     // Catch:{ Exception -> 0x01c2 }
            r2.<init>()     // Catch:{ Exception -> 0x01c2 }
            r0 = 10
            int r1 = r3.A02(r0)     // Catch:{ Exception -> 0x01c2 }
            if (r1 == 0) goto L_0x0116
            int r0 = r3.A00     // Catch:{ Exception -> 0x01c2 }
            int r1 = r1 + r0
            int r1 = r3.A01(r1)     // Catch:{ Exception -> 0x01c2 }
            java.nio.ByteBuffer r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            r2.A00 = r1     // Catch:{ Exception -> 0x01c2 }
            r2.A01 = r0     // Catch:{ Exception -> 0x01c2 }
        L_0x0111:
            com.google.common.base.Optional r1 = com.google.common.base.Optional.fromNullable(r2)     // Catch:{ Exception -> 0x01c2 }
            goto L_0x017b
        L_0x0116:
            r2 = 0
            goto L_0x0111
        L_0x0118:
            X.7WQ r2 = new X.7WQ     // Catch:{ Exception -> 0x01c2 }
            r2.<init>()     // Catch:{ Exception -> 0x01c2 }
            r0 = 8
            int r1 = r3.A02(r0)     // Catch:{ Exception -> 0x01c2 }
            if (r1 == 0) goto L_0x0137
            int r0 = r3.A00     // Catch:{ Exception -> 0x01c2 }
            int r1 = r1 + r0
            int r1 = r3.A01(r1)     // Catch:{ Exception -> 0x01c2 }
            java.nio.ByteBuffer r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            r2.A00 = r1     // Catch:{ Exception -> 0x01c2 }
            r2.A01 = r0     // Catch:{ Exception -> 0x01c2 }
        L_0x0132:
            com.google.common.base.Optional r1 = com.google.common.base.Optional.fromNullable(r2)     // Catch:{ Exception -> 0x01c2 }
            goto L_0x017b
        L_0x0137:
            r2 = 0
            goto L_0x0132
        L_0x0139:
            X.7WQ r2 = new X.7WQ     // Catch:{ Exception -> 0x01c2 }
            r2.<init>()     // Catch:{ Exception -> 0x01c2 }
            r0 = 6
            int r1 = r3.A02(r0)     // Catch:{ Exception -> 0x01c2 }
            if (r1 == 0) goto L_0x0157
            int r0 = r3.A00     // Catch:{ Exception -> 0x01c2 }
            int r1 = r1 + r0
            int r1 = r3.A01(r1)     // Catch:{ Exception -> 0x01c2 }
            java.nio.ByteBuffer r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            r2.A00 = r1     // Catch:{ Exception -> 0x01c2 }
            r2.A01 = r0     // Catch:{ Exception -> 0x01c2 }
        L_0x0152:
            com.google.common.base.Optional r1 = com.google.common.base.Optional.fromNullable(r2)     // Catch:{ Exception -> 0x01c2 }
            goto L_0x017b
        L_0x0157:
            r2 = 0
            goto L_0x0152
        L_0x0159:
            com.google.common.base.Optional r2 = com.google.common.base.Optional.absent()     // Catch:{ Exception -> 0x01c2 }
            goto L_0x0198
        L_0x015e:
            X.7WQ r2 = new X.7WQ     // Catch:{ Exception -> 0x01c2 }
            r2.<init>()     // Catch:{ Exception -> 0x01c2 }
            r0 = 4
            int r1 = r3.A02(r0)     // Catch:{ Exception -> 0x01c2 }
            if (r1 == 0) goto L_0x01c0
            int r0 = r3.A00     // Catch:{ Exception -> 0x01c2 }
            int r1 = r1 + r0
            int r1 = r3.A01(r1)     // Catch:{ Exception -> 0x01c2 }
            java.nio.ByteBuffer r0 = r3.A01     // Catch:{ Exception -> 0x01c2 }
            r2.A00 = r1     // Catch:{ Exception -> 0x01c2 }
            r2.A01 = r0     // Catch:{ Exception -> 0x01c2 }
        L_0x0177:
            com.google.common.base.Optional r1 = com.google.common.base.Optional.fromNullable(r2)     // Catch:{ Exception -> 0x01c2 }
        L_0x017b:
            boolean r0 = r1.isPresent()     // Catch:{ Exception -> 0x01c2 }
            if (r0 == 0) goto L_0x0159
            java.lang.Object r1 = r1.get()     // Catch:{ Exception -> 0x01c2 }
            X.7WQ r1 = (X.AnonymousClass7WQ) r1     // Catch:{ Exception -> 0x01c2 }
            com.google.common.base.Optional r2 = X.AnonymousClass0lk.A00(r1, r14)     // Catch:{ Exception -> 0x01c2 }
            if (r14 == 0) goto L_0x0198
            boolean r0 = r2.isPresent()     // Catch:{ Exception -> 0x01c2 }
            if (r0 != 0) goto L_0x0198
            r0 = 0
            com.google.common.base.Optional r2 = X.AnonymousClass0lk.A00(r1, r0)     // Catch:{ Exception -> 0x01c2 }
        L_0x0198:
            boolean r0 = r2.isPresent()     // Catch:{ Exception -> 0x01c2 }
            if (r0 == 0) goto L_0x01d8
            X.8N6 r7 = r5.A00     // Catch:{ Exception -> 0x01c2 }
            java.lang.Object r11 = r2.get()     // Catch:{ Exception -> 0x01c2 }
            X.85I r11 = (X.AnonymousClass85I) r11     // Catch:{ Exception -> 0x01c2 }
            java.util.concurrent.ExecutorService r1 = r7.A04     // Catch:{ Exception -> 0x01c2 }
            X.8N7 r6 = new X.8N7     // Catch:{ Exception -> 0x01c2 }
            r8 = r13
            r9 = r14
            r6.<init>(r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x01c2 }
            r0 = 1695081478(0x6508e406, float:4.040301E22)
            X.AnonymousClass07A.A04(r1, r6, r0)     // Catch:{ Exception -> 0x01c2 }
            java.lang.Object r0 = r2.get()     // Catch:{ Exception -> 0x01c2 }
            X.85I r0 = (X.AnonymousClass85I) r0     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r0 = r0.A06()     // Catch:{ Exception -> 0x01c2 }
            return r0
        L_0x01c0:
            r2 = 0
            goto L_0x0177
        L_0x01c2:
            r4 = move-exception
            java.lang.String r3 = "com.facebook.resources.impl.qt.model.QTStringResources"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            java.lang.String r1 = X.C07230cz.A01(r14)
            java.lang.String r0 = r5.A02
            java.lang.Object[] r1 = new java.lang.Object[]{r2, r1, r15, r0}
            java.lang.String r0 = "Failed to get QT plural resource. resource_id : %d, gender : %s, category : %s userId : %s"
            X.C010708t.A0V(r3, r4, r0, r1)
        L_0x01d8:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08650fi.A02(int, int, X.1ZB):java.lang.String");
    }

    public String[] A03(int i, int i2) {
        if (this instanceof QTStringResources) {
            return null;
        }
        C08640fh r3 = (C08640fh) this;
        if (!r3.A05) {
            return r3.A01.A03(i, i2);
        }
        C08640fh.A00(r3, i2);
        Map map = r3.A02;
        Integer valueOf = Integer.valueOf(i);
        String[] strArr = (String[]) map.get(valueOf);
        if (strArr == null && (strArr = r3.A01.A03(i, i2)) != null) {
            r3.A02.put(valueOf, strArr);
        }
        return strArr;
    }
}
