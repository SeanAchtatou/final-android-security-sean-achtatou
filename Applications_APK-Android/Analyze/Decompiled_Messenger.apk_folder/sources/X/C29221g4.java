package X;

import android.database.Cursor;
import java.util.Iterator;

/* renamed from: X.1g4  reason: invalid class name and case insensitive filesystem */
public final class C29221g4 implements Iterable, AutoCloseable {
    private final Cursor A00;

    public void close() {
        this.A00.close();
    }

    public Iterator iterator() {
        return new AnonymousClass13X(this.A00);
    }

    public C29221g4(Cursor cursor) {
        this.A00 = cursor;
    }
}
