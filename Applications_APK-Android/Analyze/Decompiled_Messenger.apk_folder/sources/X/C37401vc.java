package X;

import android.graphics.Bitmap;

/* renamed from: X.1vc  reason: invalid class name and case insensitive filesystem */
public final class C37401vc implements C37391vb {
    private final C37391vb A00;
    public final /* synthetic */ C30561iH A01;

    public C37401vc(C30561iH r1, C37391vb r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void C0t(Object obj) {
        this.A00.C0t((Bitmap) obj);
    }

    public Object get(int i) {
        try {
            return (Bitmap) this.A00.get(i);
        } catch (OutOfMemoryError e) {
            C30561iH r1 = this.A01;
            if (r1.A00 == null || !((Boolean) r1.A01.get()).booleanValue()) {
                throw e;
            }
            C133746Nn r12 = C133746Nn.OnSystemMemoryCriticallyLowWhileAppInForeground;
            this.A01.A00.trim(r12);
            trim(r12);
            return (Bitmap) this.A00.get(i);
        }
    }

    public void trim(C133746Nn r2) {
        this.A00.trim(r2);
    }
}
