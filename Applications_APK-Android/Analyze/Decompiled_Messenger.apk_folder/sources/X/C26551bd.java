package X;

import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.quicklog.PerformanceLoggingEvent;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1bd  reason: invalid class name and case insensitive filesystem */
public final class C26551bd extends AnonymousClass0f8 {
    private static volatile C26551bd A00;

    public String Azg() {
        return ErrorReportingConstants.ENDPOINT;
    }

    public static final C26551bd A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C26551bd.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C26551bd();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        String str = (String) obj;
        String str2 = (String) obj2;
        C000300h.A01(str);
        C000300h.A01(str2);
        performanceLoggingEvent.A0B("start_endpoint", str);
        performanceLoggingEvent.A0B("end_endpoint", str2);
    }

    public long Azh() {
        return C08350fD.A05;
    }

    public Class B3O() {
        return String.class;
    }

    public Object CGO() {
        String A02 = AnonymousClass01P.A02();
        if (A02 == null) {
            return BuildConfig.FLAVOR;
        }
        return A02;
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A03;
    }
}
