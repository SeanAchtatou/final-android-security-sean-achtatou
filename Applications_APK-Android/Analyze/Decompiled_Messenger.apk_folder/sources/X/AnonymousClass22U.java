package X;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.facebook.common.util.TriState;
import com.facebook.proxygen.LigerSamplePolicy;
import com.google.common.base.Preconditions;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.22U  reason: invalid class name */
public final class AnonymousClass22U implements AnonymousClass0WP, AnonymousClass0YZ, C05130Xt, C25101Yi {
    private static volatile AnonymousClass22U A0A;
    public AnonymousClass0UN A00;
    public final AnonymousClass00K A01;
    public final boolean A02;
    private final Object A03 = new Object();
    private final WeakHashMap A04 = new WeakHashMap();
    public volatile boolean A05 = false;
    private volatile int A06 = 0;
    private volatile boolean A07 = false;
    private volatile boolean A08 = false;
    private volatile boolean A09 = false;

    private void A05(boolean z) {
        Message obtain;
        if (z) {
            this.A09 = true;
            obtain = Message.obtain();
            obtain.what = 5;
        } else {
            if (!this.A02 && !this.A05) {
                this.A05 = true;
                A04(this);
            }
            obtain = Message.obtain();
            obtain.what = 4;
        }
        ((C190148sp) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBm, this.A00)).A08(obtain);
    }

    public void BM0() {
        this.A07 = true;
        A04(this);
        AnonymousClass00S.A05((Handler) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Abm, this.A00), new C855944c(this), 5001, -1240899870);
    }

    private int A00() {
        if (!this.A08) {
            return 0;
        }
        if (!this.A07) {
            return 1;
        }
        A06();
        if (((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, this.A00)).A0C() == TriState.YES) {
            return 3;
        }
        if (this.A09 && !this.A05) {
            return 2;
        }
        if (this.A09 || A06()) {
            return 3;
        }
        return 2;
    }

    public static final AnonymousClass22U A01(AnonymousClass1XY r6) {
        if (A0A == null) {
            synchronized (AnonymousClass22U.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A0A = new AnonymousClass22U(applicationInjector, AnonymousClass0UU.A05(applicationInjector), AnonymousClass1YA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    private AnonymousClass0XX A02(C190228sx r14) {
        boolean z;
        AnonymousClass0XV r1;
        AnonymousClass0XV r3 = r14.A00;
        if (r3 != AnonymousClass0XV.APPLICATION_LOADED_HIGH_PRIORITY || !((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C88854Mo) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Asu, this.A00)).A00)).Aem(281908768539478L)) {
            switch (r3.ordinal()) {
                case 3:
                case 4:
                case 5:
                    z = true;
                    break;
                default:
                    z = false;
                    break;
            }
            if (z && r3 != (r1 = AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE) && this.A01.A2B) {
                r14.A00 = r1;
            }
        } else {
            r14.A00 = AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY;
        }
        A00();
        if (((C25121Yk) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BTs, this.A00)).A02()) {
            r14.A02 = (C180598Ya) AnonymousClass1XX.A02(9, AnonymousClass1Y3.BDT, this.A00);
        }
        int i = AnonymousClass1Y3.BBm;
        AnonymousClass0UN r32 = this.A00;
        r14.A01 = (C190148sp) AnonymousClass1XX.A02(4, i, r32);
        r14.A03 = (AnonymousClass09P) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Amr, r32);
        boolean z2 = true;
        boolean z3 = false;
        if (r14.A05 != null) {
            z3 = true;
        }
        Preconditions.checkState(z3, "Must set a description for instrumentation!");
        boolean z4 = false;
        if (r14.A06 == null) {
            z4 = true;
        }
        boolean z5 = false;
        if (r14.A04 == null) {
            z5 = true;
        }
        Preconditions.checkState(z4 ^ z5, "Must set exactly one of a runnable and a callable!");
        boolean z6 = false;
        if (r14.A00 != null) {
            z6 = true;
        }
        Preconditions.checkState(z6, "Must set a priority for a task!");
        boolean z7 = false;
        if (r14.A01 != null) {
            z7 = true;
        }
        Preconditions.checkState(z7, "Must set the implementation object");
        boolean z8 = false;
        if (r14.A07 != null) {
            z8 = true;
        }
        Preconditions.checkState(z8, "Must set an executor service!");
        if (r14.A03 == null) {
            z2 = false;
        }
        Preconditions.checkState(z2, "Must set an error reporter!");
        int andIncrement = C190158sq.A0A.getAndIncrement();
        C190158sq r5 = new C190158sq(r14.A02.AVX(r14.A04, r14.A06, andIncrement, r14.A05, r14.A00, "LWAppChoreo/p%d/%s"), andIncrement, r14.A05, r14.A00, r14.A07, r14.A01, r14.A03);
        A04(this);
        Message obtain = Message.obtain();
        int i2 = 2;
        if (r5.A05) {
            i2 = 3;
        }
        obtain.what = i2;
        obtain.obj = r5;
        ((C190148sp) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBm, this.A00)).A08(obtain);
        return r5.A01;
    }

    private ExecutorService A03(Integer num) {
        int i;
        int i2;
        if (num == AnonymousClass07B.A00) {
            i = 1;
            i2 = AnonymousClass1Y3.ADs;
        } else {
            i = 2;
            i2 = AnonymousClass1Y3.Aoc;
        }
        return (ExecutorService) AnonymousClass1XX.A02(i, i2, this.A00);
    }

    public static void A04(AnonymousClass22U r5) {
        boolean z;
        if (r5.A06 != 3) {
            int A002 = r5.A00();
            synchronized (r5) {
                z = false;
                if (A002 > r5.A06) {
                    z = true;
                    r5.A06 = A002;
                }
            }
            if (z) {
                Message obtain = Message.obtain();
                obtain.what = 0;
                obtain.arg1 = A002;
                ((C190148sp) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBm, r5.A00)).A08(obtain);
            }
        }
    }

    private boolean A06() {
        boolean z = false;
        if (((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, this.A00)).A0D > 0) {
            z = true;
        }
        if (!z || ((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, this.A00)).A07() <= LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT) {
            return false;
        }
        return true;
    }

    public void A07() {
        C005505z.A03("LightweightAppChoreographer.onFirstSignalEnded", -855560633);
        try {
            if (this.A02 && !this.A05) {
                this.A05 = true;
                A04(this);
            }
        } finally {
            C005505z.A00(-63350873);
        }
    }

    public void ANq(Object obj) {
        C005505z.A03("LightweightAppChoreographer.addUiLoadingAsyncTask", -712185399);
        try {
            C855844b r5 = new C855844b(this, obj);
            synchronized (this.A03) {
                boolean isEmpty = this.A04.isEmpty();
                C855844b r2 = (C855844b) this.A04.get(obj);
                if (r2 != null) {
                    AnonymousClass00S.A02((Handler) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Abm, this.A00), r2);
                }
                this.A04.put(obj, r5);
                int i = AnonymousClass1Y3.Abm;
                AnonymousClass0UN r22 = this.A00;
                AnonymousClass00S.A05((Handler) AnonymousClass1XX.A02(5, i, r22), r5, ((C25121Yk) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BTs, r22)).A01(), -1902857678);
                if (isEmpty) {
                    A05(true);
                }
            }
            this.A09 = true;
            C005505z.A00(-1499451966);
        } catch (Throwable th) {
            C005505z.A00(171124148);
            throw th;
        }
    }

    public boolean BHL() {
        boolean z;
        if (((C25121Yk) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BTs, this.A00)).A05()) {
            return ((C28461eq) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Aq4, this.A00)).A0A();
        }
        synchronized (this.A03) {
            z = false;
            if (!this.A04.isEmpty()) {
                z = true;
            }
        }
        return z;
    }

    public void C25(Object obj) {
        C005505z.A03("LightweightAppChoreographer.removeUiLoadingAsyncTask", 387894726);
        try {
            synchronized (this.A03) {
                C855844b r4 = (C855844b) this.A04.get(obj);
                if (r4 != null) {
                    AnonymousClass00S.A02((Handler) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Abm, this.A00), r4);
                }
                boolean z = false;
                if (this.A04.remove(obj) != null) {
                    z = true;
                }
                if (z && this.A02 && !this.A05) {
                    this.A05 = true;
                    A04(this);
                }
                if (this.A04.isEmpty()) {
                    A05(false);
                }
            }
            C005505z.A00(-2057407069);
        } catch (Throwable th) {
            C005505z.A00(587068627);
            throw th;
        }
    }

    public AnonymousClass0XX CIB(String str, Runnable runnable, AnonymousClass0XV r4, ExecutorService executorService) {
        C190228sx r0 = new C190228sx();
        r0.A05 = str;
        r0.A04 = runnable;
        r0.A00 = r4;
        r0.A07 = executorService;
        return A02(r0);
    }

    public AnonymousClass0XX CIG(String str, Runnable runnable, AnonymousClass0XV r5, Integer num) {
        C190228sx r1 = new C190228sx();
        r1.A05 = str;
        r1.A04 = runnable;
        r1.A00 = r5;
        r1.A07 = A03(num);
        return A02(r1);
    }

    public AnonymousClass0XX CIH(String str, Callable callable, AnonymousClass0XV r5, Integer num) {
        C190228sx r1 = new C190228sx();
        r1.A06 = callable;
        r1.A05 = str;
        r1.A00 = r5;
        r1.A07 = A03(num);
        return A02(r1);
    }

    public void start() {
        int i = AnonymousClass1Y3.BTs;
        if (((C25121Yk) AnonymousClass1XX.A02(10, i, this.A00)).A05() || ((C25121Yk) AnonymousClass1XX.A02(10, i, this.A00)).A03()) {
            int i2 = AnonymousClass1Y3.Aq4;
            ((C28461eq) AnonymousClass1XX.A02(8, i2, this.A00)).A04(this);
            C28461eq r3 = (C28461eq) AnonymousClass1XX.A02(8, i2, this.A00);
            synchronized (r3.A06) {
                C28461eq.A02(r3, new AnonymousClass4LI(r3, r3.A04, this));
            }
        }
        if (!((C25121Yk) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BTs, this.A00)).A03()) {
            ((AnonymousClass1Z4) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Akn, this.A00)).A03(this);
        }
        this.A08 = true;
        A04(this);
    }

    private AnonymousClass22U(AnonymousClass1XY r5, C001500z r6, Context context) {
        this.A00 = new AnonymousClass0UN(11, r5);
        this.A01 = AnonymousClass00J.A01(context.getApplicationContext());
        C190148sp r2 = (C190148sp) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBm, this.A00);
        C190188st r0 = (C190188st) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AYJ, r2.A01);
        r0.A06 = r2;
        r0.A03.start();
        this.A02 = AnonymousClass0Y5.A00(context, r6).A03;
    }

    public boolean BDj() {
        A04(this);
        if (this.A06 >= 3) {
            return true;
        }
        return false;
    }

    public boolean BFP() {
        A04(this);
        if (((AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, this.A00)).A0D > 0) {
            return true;
        }
        return false;
    }

    public void BQF(boolean z, boolean z2) {
        C005505z.A06("LightweightAppChoreographer.onBusyStateChanged(isBusy = %b, isInitialState = %b)", Boolean.valueOf(z), Boolean.valueOf(z2), -2124873434);
        if (!z2 || z) {
            try {
                A05(z);
            } finally {
                C005505z.A00(322980909);
            }
        } else {
            C005505z.A00(614487075);
        }
    }

    public void Btw(boolean z) {
        Message obtain = Message.obtain();
        int i = 6;
        if (z) {
            i = 7;
        }
        obtain.what = i;
        ((C190148sp) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBm, this.A00)).A08(obtain);
    }
}
