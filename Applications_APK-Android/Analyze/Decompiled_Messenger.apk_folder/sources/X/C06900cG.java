package X;

import com.google.common.collect.ImmutableList;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: X.0cG  reason: invalid class name and case insensitive filesystem */
public class C06900cG {
    public final ConcurrentHashMap A00 = new ConcurrentHashMap();
    private final ThreadLocal A01 = new ThreadLocal();

    public void A02(C414625o r6) {
        if (r6 != null) {
            Class A002 = r6.A00();
            Queue queue = (Queue) this.A00.get(A002);
            if (queue == null) {
                queue = new ConcurrentLinkedQueue();
                Queue queue2 = (Queue) this.A00.putIfAbsent(A002, queue);
                if (queue2 != null) {
                    queue = queue2;
                }
            }
            Iterator it = queue.iterator();
            boolean z = false;
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                if (weakReference.get() == r6) {
                    z = true;
                } else if (weakReference.get() == null) {
                    it.remove();
                }
            }
            if (!z) {
                queue.add(new WeakReference(r6));
            }
        }
    }

    public void A03(C414625o r4) {
        Queue queue;
        if (r4 != null && (queue = (Queue) this.A00.get(r4.A00())) != null) {
            Iterator it = queue.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                if (weakReference.get() == r4) {
                    weakReference.clear();
                    it.remove();
                    return;
                }
            }
        }
    }

    public void A04(AnonymousClass24A r6) {
        if (r6 != null) {
            Queue queue = (Queue) this.A01.get();
            if (queue == null) {
                queue = new ArrayDeque();
                this.A01.set(queue);
            }
            if (queue.isEmpty()) {
                queue.add(r6);
                while (!queue.isEmpty()) {
                    AnonymousClass24A r3 = (AnonymousClass24A) queue.peek();
                    try {
                        Queue queue2 = (Queue) this.A00.get(r3.getClass());
                        if (queue2 != null) {
                            Iterator it = queue2.iterator();
                            while (it.hasNext()) {
                                if (((WeakReference) it.next()).get() == null) {
                                    it.remove();
                                }
                            }
                            if (!queue2.isEmpty()) {
                                A05(r3, ImmutableList.copyOf((Collection) queue2));
                            }
                        }
                    } finally {
                        queue.remove();
                    }
                }
                return;
            }
            queue.add(r6);
        }
    }

    public void A05(AnonymousClass24A r4, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C414625o r1 = (C414625o) ((WeakReference) it.next()).get();
            if (r1 != null && r1.A02(r4)) {
                r1.A01(r4);
            }
        }
    }
}
