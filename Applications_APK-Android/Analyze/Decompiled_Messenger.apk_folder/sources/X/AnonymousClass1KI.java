package X;

import java.util.concurrent.Callable;

/* renamed from: X.1KI  reason: invalid class name */
public final class AnonymousClass1KI implements Callable {
    public final /* synthetic */ AnonymousClass1KH A00;

    public AnonymousClass1KI(AnonymousClass1KH r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r1.A0i != false) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x015e, code lost:
        if (r13.A1L(r2) == false) goto L_0x0160;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object call() {
        /*
            r25 = this;
            r15 = r25
            X.1KH r1 = r15.A00
            monitor-enter(r1)
            X.1KH r0 = r15.A00     // Catch:{ all -> 0x020d }
            boolean r0 = r0.A0H     // Catch:{ all -> 0x020d }
            r14 = 0
            monitor-exit(r1)
            if (r0 != 0) goto L_0x020c
            X.1KH r3 = r15.A00
            com.facebook.litho.ComponentTree r1 = r3.A0I
            boolean r0 = r1.A0g
            if (r0 != 0) goto L_0x001b
            boolean r0 = r1.A0i
            r22 = r14
            if (r0 == 0) goto L_0x001d
        L_0x001b:
            r22 = r3
        L_0x001d:
            X.0p4 r10 = X.AnonymousClass1KH.A00(r3)
            X.0zR r13 = r3.A03
            com.facebook.litho.ComponentTree r0 = r3.A0I
            int r2 = r0.A0R
            int r0 = r3.A02
            r24 = r0
            int r0 = r3.A00
            r23 = r0
            boolean r1 = r3.A09
            X.15v r12 = r3.A04
            int r0 = r3.A01
            r18 = r0
            java.lang.String r11 = r3.A05
            r19 = r13
            X.38i r9 = r10.A05()
            boolean r17 = X.C27041cY.A02()
            if (r17 == 0) goto L_0x0089
            if (r11 == 0) goto L_0x0050
            java.lang.String r0 = "extra:"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r11)
            X.C27041cY.A01(r0)
        L_0x0050:
            java.lang.String r6 = "LayoutState.calculate_"
            java.lang.String r5 = r13.A1A()
            java.lang.String r4 = "_"
            r0 = r18
            java.lang.String r0 = X.C188915v.A02(r0)
            java.lang.String r4 = X.AnonymousClass08S.A0S(r6, r5, r4, r0)
            X.0gP r0 = X.C27041cY.A00
            X.0gQ r5 = r0.APo(r4)
            java.lang.String r0 = "treeId"
            r5.AOn(r0, r2)
            int r4 = r13.A00
            java.lang.String r0 = "rootId"
            r5.AOn(r0, r4)
            java.lang.String r4 = android.view.View.MeasureSpec.toString(r24)
            java.lang.String r0 = "widthSpec"
            r5.AOo(r0, r4)
            java.lang.String r4 = android.view.View.MeasureSpec.toString(r23)
            java.lang.String r0 = "heightSpec"
            r5.AOo(r0, r4)
            r5.flush()
        L_0x0089:
            if (r12 == 0) goto L_0x0094
            X.1Lz r0 = r12.A0C
            r16 = r0
        L_0x008f:
            if (r9 == 0) goto L_0x00a0
            r0 = 16
            goto L_0x0097
        L_0x0094:
            r16 = r14
            goto L_0x008f
        L_0x0097:
            X.3eb r0 = r9.BLg(r10, r0)     // Catch:{ all -> 0x0210 }
            X.3eb r8 = X.C637138j.A00(r10, r9, r0)     // Catch:{ all -> 0x0210 }
            goto L_0x00a1
        L_0x00a0:
            r8 = r14
        L_0x00a1:
            r7 = 0
            r6 = 1
            if (r8 == 0) goto L_0x00d4
            java.lang.String r4 = "component"
            java.lang.String r0 = r13.A1A()     // Catch:{ all -> 0x0210 }
            r8.A02(r4, r0)     // Catch:{ all -> 0x0210 }
            java.lang.String r4 = "calculate_layout_state_source"
            r0 = r18
            java.lang.String r0 = X.C188915v.A02(r0)     // Catch:{ all -> 0x0210 }
            r8.A02(r4, r0)     // Catch:{ all -> 0x0210 }
            java.lang.String r4 = "is_background_layout"
            boolean r5 = X.C191216w.A00()     // Catch:{ all -> 0x0210 }
            r0 = 0
            if (r5 != 0) goto L_0x00c3
            r0 = 1
        L_0x00c3:
            r8.A03(r4, r0)     // Catch:{ all -> 0x0210 }
            java.lang.String r4 = "tree_diff_enabled"
            r0 = 0
            if (r16 == 0) goto L_0x00cc
            r0 = 1
        L_0x00cc:
            r8.A03(r4, r0)     // Catch:{ all -> 0x0210 }
            java.lang.String r0 = "attribution"
            r8.A02(r0, r11)     // Catch:{ all -> 0x0210 }
        L_0x00d4:
            r3 = r13
            monitor-enter(r3)     // Catch:{ all -> 0x0210 }
            boolean r0 = r13.A0C     // Catch:{ all -> 0x0209 }
            if (r0 != 0) goto L_0x01f2
            r13.A0C = r6     // Catch:{ all -> 0x0209 }
            monitor-exit(r3)     // Catch:{ all -> 0x0210 }
            X.15v r5 = new X.15v     // Catch:{ all -> 0x0210 }
            r5.<init>(r10)     // Catch:{ all -> 0x0210 }
            X.1kq r4 = new X.1kq     // Catch:{ all -> 0x0210 }
            r20 = r4
            r21 = r5
            r20.<init>(r21, r22)     // Catch:{ all -> 0x0210 }
            r10.A06 = r4     // Catch:{ all -> 0x0210 }
            r5.A0U = r1     // Catch:{ all -> 0x0210 }
            r5.A00 = r2     // Catch:{ all -> 0x0210 }
            if (r12 == 0) goto L_0x00f4
            goto L_0x00f6
        L_0x00f4:
            r0 = -1
            goto L_0x00f8
        L_0x00f6:
            int r0 = r12.A0Y     // Catch:{ all -> 0x0210 }
        L_0x00f8:
            r5.A06 = r0     // Catch:{ all -> 0x0210 }
            android.content.Context r1 = r10.A09     // Catch:{ all -> 0x0210 }
            java.lang.String r0 = "accessibility"
            java.lang.Object r1 = r1.getSystemService(r0)     // Catch:{ all -> 0x0210 }
            android.view.accessibility.AccessibilityManager r1 = (android.view.accessibility.AccessibilityManager) r1     // Catch:{ all -> 0x0210 }
            r5.A0A = r1     // Catch:{ all -> 0x0210 }
            boolean r0 = X.C17560z6.A01     // Catch:{ all -> 0x0210 }
            if (r0 != 0) goto L_0x010d
            X.C17560z6.A00(r1)     // Catch:{ all -> 0x0210 }
        L_0x010d:
            boolean r0 = X.C17560z6.A00     // Catch:{ all -> 0x0210 }
            r5.A0R = r0     // Catch:{ all -> 0x0210 }
            r5.A0B = r13     // Catch:{ all -> 0x0210 }
            r0 = r24
            r5.A08 = r0     // Catch:{ all -> 0x0210 }
            r0 = r23
            r5.A05 = r0     // Catch:{ all -> 0x0210 }
            java.lang.String r0 = r13.A1A()     // Catch:{ all -> 0x0210 }
            r5.A0J = r0     // Catch:{ all -> 0x0210 }
            r5.A0S = r6     // Catch:{ all -> 0x0210 }
            r3 = r14
            if (r12 == 0) goto L_0x0160
            X.0yx r0 = r12.A0D     // Catch:{ all -> 0x0210 }
            if (r0 == 0) goto L_0x0160
            com.facebook.litho.ComponentTree r0 = r10.A05     // Catch:{ all -> 0x0210 }
            if (r0 == 0) goto L_0x0131
            boolean r0 = r0.A0b     // Catch:{ all -> 0x0210 }
            goto L_0x0133
        L_0x0131:
            boolean r0 = X.AnonymousClass07c.isReconciliationEnabled     // Catch:{ all -> 0x0210 }
        L_0x0133:
            if (r0 == 0) goto L_0x0160
            X.1JC r0 = r10.A0C     // Catch:{ all -> 0x0210 }
            if (r0 == 0) goto L_0x0160
            boolean r0 = r0.A0A()     // Catch:{ all -> 0x0210 }
            if (r0 == 0) goto L_0x0160
            X.0zR r2 = r12.A0B     // Catch:{ all -> 0x0210 }
            if (r2 != r13) goto L_0x0145
            r0 = 1
            goto L_0x0157
        L_0x0145:
            if (r2 == 0) goto L_0x0156
            if (r13 == 0) goto L_0x0156
            java.lang.Class r1 = r2.getClass()     // Catch:{ all -> 0x0210 }
            java.lang.Class r0 = r13.getClass()     // Catch:{ all -> 0x0210 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0210 }
            goto L_0x0157
        L_0x0156:
            r0 = 0
        L_0x0157:
            if (r0 == 0) goto L_0x0160
            boolean r0 = r13.A1L(r2)     // Catch:{ all -> 0x0210 }
            r1 = 1
            if (r0 != 0) goto L_0x0161
        L_0x0160:
            r1 = 0
        L_0x0161:
            if (r1 != 0) goto L_0x0167
            if (r12 == 0) goto L_0x0167
            r12.A0D = r14     // Catch:{ all -> 0x0210 }
        L_0x0167:
            if (r14 != 0) goto L_0x0180
            if (r1 == 0) goto L_0x016c
            goto L_0x016e
        L_0x016c:
            r0 = r14
            goto L_0x0170
        L_0x016e:
            X.0yx r0 = r12.A0D     // Catch:{ all -> 0x0210 }
        L_0x0170:
            r18 = r10
            r20 = r24
            r21 = r23
            r22 = r0
            r23 = r16
            r24 = r8
            X.0yx r3 = X.C31951kr.A01(r18, r19, r20, r21, r22, r23, r24)     // Catch:{ all -> 0x0210 }
        L_0x0180:
            X.0p4 r0 = r3.AiS()     // Catch:{ all -> 0x0210 }
            if (r0 == 0) goto L_0x018c
            X.0p4 r0 = r3.AiS()     // Catch:{ all -> 0x0210 }
            r0.A06 = r4     // Catch:{ all -> 0x0210 }
        L_0x018c:
            r5.A0D = r3     // Catch:{ all -> 0x0210 }
            X.0zP r0 = X.C07070ca.A00(r3)     // Catch:{ all -> 0x0210 }
            r5.A0H = r0     // Catch:{ all -> 0x0210 }
            r5.A0S = r7     // Catch:{ all -> 0x0210 }
            boolean r0 = r4.A00()     // Catch:{ all -> 0x0210 }
            if (r0 == 0) goto L_0x01ae
            r5.A0o = r6     // Catch:{ all -> 0x0210 }
            if (r8 == 0) goto L_0x01a3
            r9.BJI(r8)     // Catch:{ all -> 0x0210 }
        L_0x01a3:
            if (r17 == 0) goto L_0x01e4
            X.C27041cY.A00()
            if (r11 == 0) goto L_0x01e4
            X.C27041cY.A00()
            goto L_0x01e4
        L_0x01ae:
            if (r8 == 0) goto L_0x01b5
            java.lang.String r0 = "start_collect_results"
            r8.A00(r0)     // Catch:{ all -> 0x0210 }
        L_0x01b5:
            X.C188915v.A04(r10, r5)     // Catch:{ all -> 0x0210 }
            r4.A01 = r14     // Catch:{ all -> 0x0210 }
            r4.A00 = r14     // Catch:{ all -> 0x0210 }
            if (r8 == 0) goto L_0x01c6
            java.lang.String r0 = "end_collect_results"
            r8.A00(r0)     // Catch:{ all -> 0x0210 }
            r9.BJI(r8)     // Catch:{ all -> 0x0210 }
        L_0x01c6:
            if (r17 == 0) goto L_0x01d0
            X.C27041cY.A00()
            if (r11 == 0) goto L_0x01d0
            X.C27041cY.A00()
        L_0x01d0:
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A01
            r0 = 1
            r2.addAndGet(r0)
            boolean r0 = X.C191216w.A00()
            if (r0 == 0) goto L_0x01e4
            java.util.concurrent.atomic.AtomicLong r2 = X.C32121lB.A02
            r0 = 1
            r2.addAndGet(r0)
        L_0x01e4:
            X.1KH r1 = r15.A00
            monitor-enter(r1)
            X.1KH r0 = r15.A00     // Catch:{ all -> 0x01ef }
            boolean r0 = r0.A0H     // Catch:{ all -> 0x01ef }
            monitor-exit(r1)
            if (r0 != 0) goto L_0x020c
            return r5
        L_0x01ef:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01ef }
            goto L_0x021b
        L_0x01f2:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0209 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0209 }
            r1.<init>()     // Catch:{ all -> 0x0209 }
            java.lang.String r0 = "Duplicate layout of a component: "
            r1.append(r0)     // Catch:{ all -> 0x0209 }
            r1.append(r13)     // Catch:{ all -> 0x0209 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0209 }
            r2.<init>(r0)     // Catch:{ all -> 0x0209 }
            throw r2     // Catch:{ all -> 0x0209 }
        L_0x0209:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0210 }
            throw r0     // Catch:{ all -> 0x0210 }
        L_0x020c:
            return r14
        L_0x020d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x020d }
            goto L_0x021b
        L_0x0210:
            r0 = move-exception
            if (r17 == 0) goto L_0x021b
            X.C27041cY.A00()
            if (r11 == 0) goto L_0x021b
            X.C27041cY.A00()
        L_0x021b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1KI.call():java.lang.Object");
    }
}
