package X;

import android.content.Context;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.NotificationSetting;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0u1  reason: invalid class name and case insensitive filesystem */
public final class C14780u1 {
    public AnonymousClass0UN A00;
    public final Context A01;
    public final C15230uw A02;
    public final FbSharedPreferences A03;
    @LoggedInUser
    public final C04310Tq A04;
    private final DeprecatedAnalyticsLogger A05;
    private final C04310Tq A06;

    public static final C14780u1 A00(AnonymousClass1XY r1) {
        return new C14780u1(r1);
    }

    public static final C14780u1 A01(AnonymousClass1XY r1) {
        return new C14780u1(r1);
    }

    public NotificationSetting A02() {
        if (!((Boolean) this.A06.get()).booleanValue()) {
            return NotificationSetting.A05;
        }
        return NotificationSetting.A00(this.A03.At2(C05690aA.A0z, 0));
    }

    public NotificationSetting A03(ThreadKey threadKey) {
        if (threadKey == null) {
            return NotificationSetting.A06;
        }
        AnonymousClass1Y7 A062 = C05690aA.A06(threadKey);
        if (this.A03.BBh(A062)) {
            return NotificationSetting.A00(this.A03.At2(A062, 0));
        }
        return NotificationSetting.A06;
    }

    public NotificationSetting A04(ThreadKey threadKey) {
        if (threadKey == null) {
            return NotificationSetting.A06;
        }
        AnonymousClass1Y7 A08 = C05690aA.A08(threadKey);
        if (this.A03.BBh(A08)) {
            return NotificationSetting.A00(this.A03.At2(A08, 0));
        }
        return NotificationSetting.A06;
    }

    public ImmutableList A05(List list) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            if (A04(threadSummary.A0S) != NotificationSetting.A06) {
                builder.add((Object) threadSummary);
            }
        }
        return builder.build();
    }

    public String A06(long j) {
        Date date = new Date(j * 1000);
        String format = DateFormat.getTimeFormat(this.A01).format(date);
        if (DateUtils.isToday(date.getTime() - 86400000)) {
            return this.A01.getString(2131834015, format);
        }
        return format;
    }

    public void A08(ThreadKey threadKey) {
        if (threadKey != null) {
            AnonymousClass1Y7 A08 = C05690aA.A08(threadKey);
            AnonymousClass1Y7 A042 = C05690aA.A04(threadKey);
            DeprecatedAnalyticsLogger deprecatedAnalyticsLogger = this.A05;
            C11670nb r3 = new C11670nb("set");
            r3.A0D("pigeon_reserved_keyword_obj_type", "notification_settings");
            r3.A0C("thread_key", threadKey);
            r3.A0D("value", "unmute");
            r3.A0D("pigeon_reserved_keyword_module", "ConversationsSettingsView");
            deprecatedAnalyticsLogger.A09(r3);
            C30281hn edit = this.A03.edit();
            edit.BzA(A08, 0);
            edit.putBoolean(A042, false);
            edit.commit();
            ((C189216c) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B7s, this.A00)).A08();
        }
    }

    private C14780u1(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A03 = FbSharedPreferencesModule.A00(r3);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.Aki, r3);
        this.A04 = AnonymousClass0WY.A01(r3);
        this.A01 = AnonymousClass1YA.A00(r3);
        this.A05 = C06920cI.A00(r3);
        this.A02 = C15230uw.A01(r3);
    }

    public String A07(NotificationSetting notificationSetting) {
        Context context;
        int i;
        Integer A022 = notificationSetting.A02();
        long j = notificationSetting.A00;
        switch (A022.intValue()) {
            case 0:
                context = this.A01;
                i = 2131830742;
                break;
            case 1:
                context = this.A01;
                i = 2131830741;
                break;
            case 2:
                return this.A01.getString(2131830749, A06(j));
            default:
                throw new UnsupportedOperationException();
        }
        return context.getString(i);
    }
}
