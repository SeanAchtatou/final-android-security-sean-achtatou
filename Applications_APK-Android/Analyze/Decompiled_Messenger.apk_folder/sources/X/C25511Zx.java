package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.graphql.executor.GraphQLResult;
import com.facebook.graphql.query.GQSQStringShape0S0000000_I0;
import com.facebook.graphservice.interfaces.TreeSerializer;
import com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Objects;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.MapMakerInternalMap;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.1Zx  reason: invalid class name and case insensitive filesystem */
public final class C25511Zx {
    public AnonymousClass0UN A00;
    public final AnonymousClass06B A01 = AnonymousClass067.A02();
    public final C05660a7 A02;
    public final C25581a4 A03;
    public final C09250go A04;
    public final C06080ao A05;
    public final C25051Yd A06;
    public final FbSharedPreferences A07;
    public final Long A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final ConcurrentMap A0E;
    private final C05160Xw A0F;
    private final C09470hQ A0G;

    private C26931cN A00(C09500hT r5) {
        GQSQStringShape0S0000000_I0 A022 = ((InboxUnitGraphQLQueryExecutorHelper) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AGk, this.A00)).A02(this.A0C);
        A022.A09("serviceType", this.A0C);
        A022.A09("fetch_type", r5.A02);
        String str = this.A09;
        if (!Platform.stringIsNullOrEmpty(str)) {
            A022.A09("categoryId", str);
        }
        String str2 = this.A0D;
        if (!Platform.stringIsNullOrEmpty(str2)) {
            A022.A08("unit_ids", ImmutableList.of(str2));
        }
        Long l = this.A08;
        if (l != null) {
            A022.A09("gameListThreadId", String.valueOf(l));
        }
        String str3 = this.A0A;
        if (!Platform.stringIsNullOrEmpty(str3)) {
            A022.A09("gameListQueryText", str3);
        }
        String str4 = this.A0B;
        if (!Platform.stringIsNullOrEmpty(str4)) {
            A022.A09("gameListSubUnitId", str4);
        }
        A022.A05("separate_ads_fetch_enabled", Boolean.valueOf(this.A06.Aem(282428460500357L)));
        C26931cN A002 = C26931cN.A00(A022);
        A002.A0B(C09290gx.NETWORK_ONLY);
        A002.A0D(((C16900xx) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AoI, this.A00)).A01());
        return A002;
    }

    public static GraphQLResult A01(C25511Zx r3, C26931cN r4) {
        C005505z.A03("makeRequest", 970478802);
        try {
            return (GraphQLResult) C46802Rw.A01(((C11170mD) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASp, r3.A00)).A04(r4), C50232dd.class);
        } finally {
            C005505z.A00(-101245446);
        }
    }

    /* JADX INFO: finally extract failed */
    public static AnonymousClass100 A03(C25511Zx r16, AnonymousClass102 r17, C25611a7 r18) {
        ImmutableList immutableList;
        C25511Zx r3 = r16;
        Preconditions.checkArgument(r3.A07());
        C005505z.A03("doDbFetch", -1212260624);
        C25611a7 r2 = r18;
        try {
            switch (r2.ordinal()) {
                case 0:
                    immutableList = r3.A03.A00();
                    break;
                case 1:
                    C25581a4 r0 = r3.A03;
                    int i = AnonymousClass1Y3.BBd;
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, r0.A00)).markerStart(5505220);
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, r0.A00)).markerTag(5505220, "top");
                    SQLiteDatabase A062 = r0.A02.A06();
                    C25601a6 A012 = C06160ax.A01(C06160ax.A02(C06160ax.A03(C06070an.A00.A00, "1"), C06160ax.A03(C06070an.A07.A00, GraphQLMessengerInboxUnitType.A02.name())), r0.A01);
                    Cursor query = A062.query("units", new String[]{C06070an.A06.A00}, A012.A02(), A012.A04(), null, null, C06070an.A02.A00);
                    try {
                        ImmutableList.Builder builder = ImmutableList.builder();
                        while (query.moveToNext()) {
                            byte[] blob = query.getBlob(0);
                            if (blob != null) {
                                builder.add((Object) ((C27161ck) ((TreeSerializer) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Aij, r0.A00)).deserializeTreeFromByteBuffer(ByteBuffer.wrap(blob), C27161ck.class, 738428414)));
                            } else {
                                throw new RuntimeException("Unable to load messenger inbox, no blob or tree_blob available.");
                            }
                        }
                        immutableList = builder.build();
                        query.close();
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r0.A00)).markerEnd(5505220, 2);
                        break;
                    } catch (IOException e) {
                        C010708t.A0L("InboxUnitStoreReaderWriter", "Unable to deserialize inbox unit into tree", e);
                        throw new RuntimeException(e);
                    } catch (Throwable th) {
                        query.close();
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r0.A00)).markerEnd(5505220, 2);
                        throw th;
                    }
                default:
                    throw new IllegalStateException();
            }
            Class<GSMBuilderShape0S0000000> cls = GSMBuilderShape0S0000000.class;
            GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("Viewer", cls, 1649765866);
            GSMBuilderShape0S0000000 gSMBuilderShape0S00000002 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MessengerInboxUnitsConnection", cls, 921501124);
            gSMBuilderShape0S00000002.setTreeList("nodes", C27161ck.A02(immutableList, C05850aR.A02()));
            gSMBuilderShape0S0000000.setTree("messenger_inbox_units", (GSTModelShape1S0000000) gSMBuilderShape0S00000002.getResult(GSTModelShape1S0000000.class, 921501124));
            GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSMBuilderShape0S0000000.getResult(GSTModelShape1S0000000.class, 1649765866);
            C005505z.A00(-152544873);
            return A04(r3, r17, r2, gSTModelShape1S0000000);
        } catch (Throwable th2) {
            C005505z.A00(-189582414);
            throw th2;
        }
    }

    public static AnonymousClass100 A04(C25511Zx r8, AnonymousClass102 r9, C25611a7 r10, GSTModelShape1S0000000 gSTModelShape1S0000000) {
        C005505z.A03("convertInboxV2QueryModel", -1371662124);
        try {
            return new AnonymousClass100(r8, new AnonymousClass101(r9, r10, r8.A01.now(), ((C07900eM) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A0D, r8.A00)).A07(gSTModelShape1S0000000)));
        } finally {
            C005505z.A00(-1452148220);
        }
    }

    public static void A06(C25511Zx r3, C27161ck r4, C21899AkD akD) {
        r3.A0F.CIC(new C29719Egv(r3, r4, akD.A04));
        ((C11170mD) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASp, r3.A00)).A06(akD, C137796c1.A02);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0054, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005e, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0068, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r0 == false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A07() {
        /*
            r4 = this;
            java.lang.String r1 = r4.A0C
            int r0 = r1.hashCode()
            r3 = 1
            r2 = 0
            switch(r0) {
                case -1413460057: goto L_0x0061;
                case -1364830452: goto L_0x0057;
                case -1275542162: goto L_0x004d;
                case -863395354: goto L_0x0043;
                case -862177807: goto L_0x0039;
                case -201186231: goto L_0x002f;
                case 810790232: goto L_0x0025;
                case 976714582: goto L_0x001b;
                case 1148238464: goto L_0x0010;
                default: goto L_0x000b;
            }
        L_0x000b:
            r1 = -1
        L_0x000c:
            switch(r1) {
                case 0: goto L_0x006b;
                case 1: goto L_0x006b;
                case 2: goto L_0x006b;
                case 3: goto L_0x006b;
                case 4: goto L_0x006b;
                case 5: goto L_0x006b;
                case 6: goto L_0x006b;
                case 7: goto L_0x006b;
                case 8: goto L_0x006b;
                default: goto L_0x000f;
            }
        L_0x000f:
            return r2
        L_0x0010:
            java.lang.String r0 = "MESSENGER_DISCOVERY_GAMES_M4"
            boolean r0 = r1.equals(r0)
            r1 = 8
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x001b:
            java.lang.String r0 = "MESSENGER_GROUPS_TAB"
            boolean r0 = r1.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0025:
            java.lang.String r0 = "MESSENGER_INBOX2"
            boolean r0 = r1.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x002f:
            java.lang.String r0 = "MESSENGER_DISCOVERY_BUSINESSES"
            boolean r0 = r1.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0039:
            java.lang.String r0 = "WORKCHAT_DISCOVERY_BOTS"
            boolean r0 = r1.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0043:
            java.lang.String r0 = "MESSENGER_DISCOVERY_GAMES"
            boolean r0 = r1.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x004d:
            java.lang.String r0 = "MESSENGER_DISCOVERY_FOR_YOU"
            boolean r0 = r1.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0057:
            java.lang.String r0 = "MESSENGER_DISCOVERY_M3_BUSINESSES"
            boolean r0 = r1.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0061:
            java.lang.String r0 = "MESSENGER_DISCOVERY_BOTS"
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x006b:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25511Zx.A07():boolean");
    }

    public C25511Zx(AnonymousClass1XY r4, C25521Zy r5, C05680a9 r6, String str, String str2, String str3, Long l, String str4, String str5) {
        C07940eQ r1 = new C07940eQ();
        r1.A06(MapMakerInternalMap.Strength.WEAK);
        this.A0E = r1.A02();
        this.A00 = new AnonymousClass0UN(8, r4);
        this.A07 = FbSharedPreferencesModule.A00(r4);
        this.A04 = C09250go.A00(r4);
        this.A05 = C06080ao.A00(r4);
        this.A0F = AnonymousClass0UX.A0F(r4);
        this.A06 = AnonymousClass0WT.A00(r4);
        Preconditions.checkNotNull(str);
        this.A0C = str;
        this.A09 = str2;
        this.A0D = str3;
        this.A08 = l;
        this.A0A = str4;
        this.A0B = str5;
        this.A03 = r5.A00(str, Strings.nullToEmpty(str2), Strings.nullToEmpty(str3));
        String str6 = this.A0C;
        this.A02 = new C05660a7(str6, str2, str3);
        this.A0G = r6.A01(str6);
    }

    public static C26971cR A02(C25511Zx r11, C09500hT r12) {
        if (r11.A07()) {
            C26931cN A002 = r11.A00(r12);
            if (Objects.equal(A002.A00.Azr(A002, (AnonymousClass0m4) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AZ6, r11.A00)), r11.A05.A03(new C11500nH(C05660a7.A00(r11.A02, "last_successful_cache_key"))))) {
                long A022 = r11.A05.A02(new C11500nH(C05660a7.A00(r11.A02, "last_full_successful_fetch_ms")), 0);
                if (A022 != 0) {
                    long now = r11.A01.now();
                    if (A022 < now - r11.A0G.AtJ()) {
                        return C26971cR.STALE_DATA_NEEDS_FULL_SERVER_FETCH;
                    }
                    boolean z = false;
                    if (!r11.A05.A08(r11.A02.A01(), false)) {
                        long A023 = now - r11.A05.A02(new C11500nH(C05660a7.A00(r11.A02, "last_successful_fetch_ms")), 0);
                        long AtG = r11.A0G.AtG();
                        long AtF = r11.A0G.AtF();
                        if (AtF >= AtG) {
                            z = true;
                        }
                        C12870q8.A00(z, "Wifi cache should be more willing to be invalidated");
                        if (A023 <= AtF && (A023 < AtG || !((InboxUnitGraphQLQueryExecutorHelper) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AGk, r11.A00)).A03())) {
                            return C26971cR.UP_TO_DATE;
                        }
                    }
                    return C26971cR.STALE_DATA_NEEDS_INCREMENTAL_SERVER_FETCH;
                }
            }
        }
        return C26971cR.NO_DATA;
    }

    /* JADX INFO: finally extract failed */
    public static AnonymousClass100 A05(C25511Zx r10, C09500hT r11) {
        ImmutableList A0M;
        SQLiteDatabase A062;
        C26931cN A002 = r10.A00(r11);
        GraphQLResult A012 = A01(r10, A002);
        if (r10.A07()) {
            C005505z.A03("writeFullResultToDatabase", -6904857);
            try {
                GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) A012.A03;
                String $const$string = ECX.$const$string(80);
                if (gSTModelShape1S0000000 != null) {
                    GSTModelShape1S0000000 A1n = gSTModelShape1S0000000.A1n();
                    if (!(A1n == null || (A0M = A1n.A0M(104993457, C27161ck.class, 738428414)) == null)) {
                        A062 = r10.A04.A06();
                        C007406x.A01(A062, 856375034);
                        r10.A03.A02();
                        C24971Xv it = A0M.iterator();
                        int i = 0;
                        boolean z = true;
                        boolean z2 = false;
                        while (it.hasNext()) {
                            C27161ck r1 = (C27161ck) it.next();
                            if (r1.A0U() == null) {
                                ((C07380dK) AnonymousClass1XX.A03(AnonymousClass1Y3.BQ3, r10.A00)).A03("inbox_unit_null_id");
                            } else {
                                r10.A03.A03(r1, i, z);
                                GraphQLMessengerInboxUnitType A0Q = r1.A0Q();
                                if (z && A0Q != GraphQLMessengerInboxUnitType.A0I) {
                                    z2 = true;
                                } else if (A0Q == GraphQLMessengerInboxUnitType.A0I) {
                                    z = false;
                                }
                                i++;
                            }
                        }
                        r10.A05.A05(new C11500nH(C05660a7.A00(r10.A02, "last_successful_fetch_ms")), A012.A00);
                        r10.A05.A05(new C11500nH(C05660a7.A00(r10.A02, "last_full_successful_fetch_ms")), A012.A00);
                        r10.A05.A06(new C11500nH(C05660a7.A00(r10.A02, "last_successful_cache_key")), A002.A00.Azr(A002, (AnonymousClass0m4) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AZ6, r10.A00)));
                        r10.A05.A07(r10.A02.A01(), false);
                        A062.setTransactionSuccessful();
                        C30281hn edit = r10.A07.edit();
                        edit.putBoolean(C05690aA.A0c, z2);
                        edit.commit();
                        C007406x.A02(A062, 1686734684);
                        C005505z.A00(163191245);
                    }
                }
                ((AnonymousClass09P) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Amr, r10.A00)).CGS($const$string, "Null result from server");
                C005505z.A00(569318133);
            } catch (Throwable th) {
                C005505z.A00(-1974532152);
                throw th;
            }
        }
        return A04(r10, AnonymousClass102.FROM_SERVER, C25611a7.ALL, (GSTModelShape1S0000000) A012.A03);
    }
}
