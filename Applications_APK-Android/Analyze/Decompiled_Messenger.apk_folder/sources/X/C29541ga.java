package X;

import android.app.Activity;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1ga  reason: invalid class name and case insensitive filesystem */
public final class C29541ga implements C13030qT {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ C29531gZ A01;

    public C29541ga(C29531gZ r1, Activity activity) {
        this.A01 = r1;
        this.A00 = activity;
    }

    public void Bn3() {
        AnonymousClass13y.A01(this.A00.getWindow(), (MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, this.A01.A00));
    }
}
