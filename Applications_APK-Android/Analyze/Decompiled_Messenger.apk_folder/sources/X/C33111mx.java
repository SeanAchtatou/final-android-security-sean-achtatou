package X;

/* renamed from: X.1mx  reason: invalid class name and case insensitive filesystem */
public final class C33111mx {
    public Object A00;
    public final Object A01;

    public String toString() {
        return "Diff{mPrevious=" + this.A01 + ", mNext=" + this.A00 + '}';
    }

    public C33111mx(Object obj, Object obj2) {
        this.A01 = obj;
        this.A00 = obj2;
    }
}
