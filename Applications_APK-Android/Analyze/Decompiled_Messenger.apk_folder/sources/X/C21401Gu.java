package X;

import android.content.res.Resources;
import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Gu  reason: invalid class name and case insensitive filesystem */
public final class C21401Gu {
    private static volatile C21401Gu A01;
    public AnonymousClass0UN A00;

    public static final C21401Gu A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C21401Gu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C21401Gu(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public int A01() {
        FbSharedPreferences fbSharedPreferences;
        AnonymousClass1Y7 r3;
        int i;
        boolean AbO = ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A5H, false);
        int i2 = AnonymousClass1Y3.B6q;
        AnonymousClass0UN r2 = this.A00;
        if (AbO) {
            fbSharedPreferences = (FbSharedPreferences) AnonymousClass1XX.A02(0, i2, r2);
            r3 = C10990lD.A05;
            i = AnonymousClass3DO.A00((Resources) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AEg, r2), 2132083255, null);
        } else {
            fbSharedPreferences = (FbSharedPreferences) AnonymousClass1XX.A02(0, i2, r2);
            r3 = C10990lD.A05;
            i = -8963329;
        }
        return fbSharedPreferences.AqN(r3, i);
    }

    private C21401Gu(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
