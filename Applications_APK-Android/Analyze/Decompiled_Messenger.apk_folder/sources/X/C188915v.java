package X;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import com.facebook.acra.ACRA;
import com.facebook.litho.ComponentTree;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.15v  reason: invalid class name and case insensitive filesystem */
public final class C188915v {
    private static Map A0p;
    public static final Comparator A0q = new AnonymousClass1KK();
    public static final Comparator A0r = new AnonymousClass1KJ();
    private static final Object A0s = new Object();
    private static final AtomicInteger A0t = new AtomicInteger(1);
    public int A00 = -1;
    public int A01 = 0;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06 = -1;
    public int A07;
    public int A08;
    public long A09 = -1;
    public AccessibilityManager A0A;
    public C17770zR A0B;
    public C22571Lz A0C;
    public C17470yx A0D;
    public AnonymousClass1M0 A0E;
    public C17720zM A0F;
    public AnonymousClass1JC A0G;
    public C17750zP A0H;
    public AnonymousClass38Y A0I;
    public String A0J;
    public List A0K;
    public List A0L;
    public List A0M;
    public List A0N;
    public Map A0O;
    public Map A0P;
    public Map A0Q;
    public boolean A0R = false;
    public boolean A0S;
    public boolean A0T = true;
    public boolean A0U = false;
    private int A0V = -1;
    private int A0W = 0;
    private C17750zP A0X;
    public final int A0Y;
    public final C17610zB A0Z = new C17610zB(8);
    public final AnonymousClass0p4 A0a;
    public final C31461jj A0b;
    public final ArrayList A0c = new ArrayList();
    public final ArrayList A0d = new ArrayList();
    public final List A0e = new ArrayList(8);
    public final List A0f;
    public final Map A0g = new HashMap();
    public final Map A0h;
    public final Map A0i = new LinkedHashMap();
    public final boolean A0j;
    private final int A0k;
    private final Map A0l = new HashMap();
    private final Set A0m = new HashSet();
    public volatile boolean A0n = true;
    public volatile boolean A0o;

    private static C17730zN A00(C17770zR r20, long j, C188915v r23, C17470yx r24, boolean z, int i, boolean z2, boolean z3) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        C17650zF r4;
        C17770zR r19 = r20;
        boolean A092 = C17770zR.A09(r19);
        C188915v r10 = r23;
        int i8 = r10.A0V;
        int i9 = 0;
        if (i8 >= 0) {
            Rect rect = ((C17730zN) r10.A0e.get(i8)).A08;
            i2 = rect.left;
            i3 = rect.top;
        } else {
            i2 = 0;
            i3 = 0;
        }
        int BA8 = r10.A02 + r24.BA8();
        int BAE = r10.A03 + r24.BAE();
        int width = r24.getWidth() + BA8;
        int height = r24.getHeight() + BAE;
        if (z) {
            i4 = r24.Awt();
        } else {
            i4 = 0;
        }
        if (z) {
            i5 = r24.Awv();
        } else {
            i5 = 0;
        }
        if (z) {
            i6 = r24.Awu();
        } else {
            i6 = 0;
        }
        if (z) {
            i7 = r24.Aws();
        } else {
            i7 = 0;
        }
        C31401jd Avp = r24.Avp();
        C17750zP r14 = null;
        if (A092) {
            r4 = new C17650zF();
            if (z && r24.BG6()) {
                if (r4.A03 == null) {
                    Rect rect2 = new Rect();
                    r4.A03 = rect2;
                    rect2.set(i4, i5, i6, i7);
                } else {
                    throw new IllegalStateException("Padding already initialized for this ViewNodeInfo.");
                }
            }
            r4.A06 = r24.B16();
            int i10 = BA8 - i2;
            int i11 = BAE - i3;
            int i12 = width - i2;
            int i13 = height - i3;
            if (r24.BBu()) {
                int B6l = r24.B6l();
                int B6n = r24.B6n();
                int B6m = r24.B6m();
                int B6k = r24.B6k();
                if (!(B6l == 0 && B6n == 0 && B6m == 0 && B6k == 0)) {
                    if (r4.A02 == null) {
                        Rect rect3 = new Rect();
                        r4.A02 = rect3;
                        rect3.set(i10 - B6l, i11 - B6n, i12 + B6m, i13 + B6k);
                    } else {
                        throw new IllegalStateException("ExpandedTouchBounds already initialized for this ViewNodeInfo.");
                    }
                }
            }
        } else {
            BA8 += i4;
            BAE += i5;
            width -= i6;
            height -= i7;
            if (Avp != null && Avp.A08 == 2) {
                i9 = 2;
            }
            r4 = null;
            Avp = null;
        }
        Rect rect4 = new Rect(BA8, BAE, width, height);
        if (z2) {
            i9 |= 1;
        }
        if (z3) {
            i9 |= 4;
        } else {
            r14 = r10.A0X;
        }
        return new C17730zN(Avp, r4, r19, rect4, i2, i3, i9, j, i, r10.A0k, r14);
    }

    private static C17730zN A01(C17470yx r15, C188915v r16, C17730zN r17, AnonymousClass38U r18, Drawable drawable, int i, boolean z) {
        boolean z2;
        long j;
        boolean z3;
        C15060uf r4 = new C15060uf(drawable);
        C17470yx r8 = r15;
        r4.A1D(AnonymousClass0p4.A00(r15.AiS(), r4));
        C17730zN r3 = r17;
        if (r17 != null) {
            C17770zR r2 = r3.A09;
            if (r4.A10()) {
                z3 = r4.A12(r2, r4);
            } else {
                z3 = true;
            }
            z2 = !z3;
        } else {
            z2 = false;
        }
        if (r17 != null) {
            j = r3.A02;
        } else {
            j = -1;
        }
        boolean A022 = C27041cY.A02();
        if (A022) {
            C27041cY.A01(AnonymousClass08S.A0J("onBoundsDefined:", r8.getSimpleName()));
        }
        C188915v r22 = r16;
        r4.A0e(r22.A0a, r8);
        if (A022) {
            C27041cY.A00();
        }
        C17730zN A002 = A00(r4, r22.A09, r22, r8, false, 2, r22.A0T, z);
        int i2 = i;
        r22.A05(A002, r22.A01, i2, j, z2, r18);
        A07(r22, A002);
        C17610zB r5 = r22.A0Z;
        int size = r22.A0e.size() - 1;
        if (r5 != null) {
            r5.A0D(A002.A02, Integer.valueOf(size));
        }
        C17720zM r0 = r22.A0F;
        if (r0 != null) {
            r0.A04(i2, A002);
        }
        return A002;
    }

    private void A05(C17730zN r18, int i, int i2, long j, boolean z, AnonymousClass38U r24) {
        long j2;
        int i3;
        AnonymousClass38U r8 = r24;
        if (this.A0E == null) {
            this.A0E = new AnonymousClass1M0();
        }
        AnonymousClass1M0 r12 = this.A0E;
        if (r12.A00 == null) {
            r12.A00 = new C17610zB(2);
        }
        int i4 = i;
        if (i < 0 || i4 > 255) {
            throw new IllegalArgumentException(AnonymousClass08S.A09("Level must be non-negative and no greater than 255 actual level ", i4));
        }
        C17730zN r11 = r18;
        C17770zR r0 = r11.A09;
        if (r0 != null) {
            j2 = (long) r0.A00;
        } else {
            j2 = 0;
        }
        int i5 = i2;
        long j3 = (((long) i4) << 19) | 0 | (j2 << 27) | (((long) i5) << 16);
        if (j <= 0 || ((int) ((j >> 19) & 255)) != i4) {
            i3 = -1;
        } else {
            i3 = (int) (j & 65535);
        }
        C17610zB r6 = r12.A00;
        int i6 = 0;
        int intValue = ((Integer) r6.A08(j3, 0)).intValue();
        if (i3 < intValue) {
            i3 = intValue + 1;
        } else {
            i6 = 2;
            if (z) {
                i6 = 1;
            }
        }
        r11.A01 = i6;
        if (i3 < 0 || i3 > 65535) {
            throw new IllegalArgumentException(AnonymousClass08S.A09("Sequence must be non-negative and no greater than 65535 actual sequence ", i3));
        }
        r11.A02 = j3 | ((long) i3);
        if (r24 != null) {
            if (r8.A00 != i5) {
                r8 = new AnonymousClass38U(r8.A02, r8.A01, r8.A03, i5);
            }
            r11.A03 = r8;
        }
        r6.A0D(j3, Integer.valueOf(i3 + 1));
    }

    private static void A06(C188915v r15) {
        C17750zP r2;
        String sb;
        String str;
        char c;
        C17720zM r3 = r15.A0F;
        if (r3 != null) {
            boolean z = false;
            if (r3.A00 == 0) {
                z = true;
            }
            if (!z && (r2 = r15.A0X) != null) {
                if (r2.A00 == 3) {
                    if (!r15.A0m.contains(r2) && r15.A0i.put(r2, r3) != null) {
                        r15.A0i.remove(r2);
                        r15.A0m.add(r2);
                    }
                } else if (r15.A0i.put(r2, r3) != null) {
                    Integer num = AnonymousClass07B.A0C;
                    StringBuilder sb2 = new StringBuilder("The transitionId '");
                    sb2.append(r2);
                    sb2.append("' is defined multiple times in the same layout. TransitionIDs must be unique.\n");
                    sb2.append("Tree:\n");
                    C17470yx r6 = r15.A0D;
                    if (r6 == null) {
                        sb = "null";
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        LinkedList linkedList = new LinkedList();
                        linkedList.addLast(null);
                        linkedList.addLast(r6);
                        int i = 0;
                        while (!linkedList.isEmpty()) {
                            C17470yx r22 = (C17470yx) linkedList.removeLast();
                            if (r22 == null) {
                                i--;
                            } else {
                                C17770zR B5K = r22.B5K();
                                if (B5K != null) {
                                    if (r22 != r6) {
                                        sb3.append(10);
                                        Iterator it = linkedList.iterator();
                                        it.next();
                                        it.next();
                                        for (int i2 = 0; i2 < i - 1; i2++) {
                                            boolean z2 = false;
                                            if (it.next() == null) {
                                                z2 = true;
                                            }
                                            if (!z2) {
                                                do {
                                                } while (it.next() != null);
                                            }
                                            if (z2) {
                                                c = ' ';
                                            } else {
                                                c = "│";
                                            }
                                            sb3.append(c);
                                            sb3.append(' ');
                                        }
                                        if (linkedList.getLast() == null) {
                                            str = "└╴";
                                        } else {
                                            str = "├╴";
                                        }
                                        sb3.append(str);
                                    }
                                    sb3.append(B5K.A1A());
                                    if (B5K.A0B || r22.BBv() || r22.B5X() != null) {
                                        sb3.append('[');
                                        if (B5K.A0B) {
                                            sb3.append("manual.key=\"");
                                            sb3.append(B5K.A19());
                                            sb3.append("\";");
                                        }
                                        if (r22.BBv()) {
                                            sb3.append("trans.key=\"");
                                            sb3.append(r22.B73());
                                            sb3.append("\";");
                                        }
                                        if (r22.B5X() != null) {
                                            sb3.append("test.key=\"");
                                            sb3.append(r22.B5X());
                                            sb3.append("\";");
                                        }
                                        sb3.append(']');
                                    }
                                    if (r22.Ah4() != 0) {
                                        linkedList.addLast(null);
                                        for (int Ah4 = r22.Ah4() - 1; Ah4 >= 0; Ah4--) {
                                            linkedList.addLast(r22.Ah3(Ah4));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                        sb = sb3.toString();
                    }
                    sb2.append(sb);
                    C09070gU.A01(num, "LayoutState:DuplicateTransitionIds", sb2.toString());
                }
                r15.A0F = null;
                r15.A0X = null;
            }
        }
    }

    private static void A07(C188915v r1, C17730zN r2) {
        r2.A00 = r1.A0e.size();
        r1.A0e.add(r2);
        r1.A0d.add(r2);
        r1.A0c.add(r2);
    }

    private boolean A08(C17470yx r4) {
        C17470yx r2 = this.A0D;
        if (r2.BFx()) {
            if (r4 == r2.AvV()) {
                return true;
            }
            return false;
        } else if (r4 == r2) {
            return true;
        } else {
            return false;
        }
    }

    public int A09(long j) {
        return ((Integer) this.A0Z.A08(j, -1)).intValue();
    }

    public C17730zN A0A(int i) {
        return (C17730zN) this.A0e.get(i);
    }

    public boolean A0B() {
        AccessibilityManager accessibilityManager = this.A0A;
        if (!C17560z6.A01) {
            C17560z6.A00(accessibilityManager);
        }
        if (C17560z6.A00 == this.A0R) {
            return true;
        }
        return false;
    }

    public C188915v(AnonymousClass0p4 r5) {
        ArrayList arrayList;
        this.A0a = r5;
        this.A0Y = A0t.getAndIncrement();
        this.A0G = this.A0a.A0C;
        C31461jj r1 = null;
        if (AnonymousClass07c.isEndToEndTestRun) {
            arrayList = new ArrayList(8);
        } else {
            arrayList = null;
        }
        this.A0f = arrayList;
        this.A0k = r5.A03().getConfiguration().orientation;
        this.A0h = new HashMap();
        this.A0K = new ArrayList();
        ComponentTree componentTree = r5.A05;
        if (componentTree != null) {
            this.A0j = componentTree.A0e;
        }
        this.A0b = this.A0j ? new C31461jj() : r1;
        this.A0N = new ArrayList(8);
    }

    public static String A02(int i) {
        switch (i) {
            case -2:
                return "test";
            case -1:
                return "none";
            case 0:
                return "setRoot";
            case 1:
                return "setRootAsync";
            case 2:
                return "setSizeSpec";
            case 3:
                return "setSizeSpecAsync";
            case 4:
                return "updateStateSync";
            case 5:
                return "updateStateAsync";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return "measure";
            default:
                throw new RuntimeException(AnonymousClass08S.A09("Unknown calculate layout source: ", i));
        }
    }

    public static void A04(AnonymousClass0p4 r9, C188915v r10) {
        boolean z;
        C17610zB r0;
        if (!r9.A0J()) {
            boolean A022 = C27041cY.A02();
            int i = r10.A08;
            int i2 = r10.A05;
            C17470yx r3 = r10.A0D;
            int mode = View.MeasureSpec.getMode(i);
            if (mode == Integer.MIN_VALUE) {
                r10.A07 = Math.min(r3.getWidth(), View.MeasureSpec.getSize(i));
            } else if (mode == 0) {
                r10.A07 = r3.getWidth();
            } else if (mode == 1073741824) {
                r10.A07 = View.MeasureSpec.getSize(i);
            }
            int mode2 = View.MeasureSpec.getMode(i2);
            if (mode2 == Integer.MIN_VALUE) {
                r10.A04 = Math.min(r3.getHeight(), View.MeasureSpec.getSize(i2));
            } else if (mode2 == 0) {
                r10.A04 = r3.getHeight();
            } else if (mode2 == 1073741824) {
                r10.A04 = View.MeasureSpec.getSize(i2);
            }
            AnonymousClass1M0 r02 = r10.A0E;
            if (!(r02 == null || (r0 = r02.A00) == null)) {
                r0.A09();
            }
            r10.A09 = -1;
            if (r3 != AnonymousClass0p4.A0F) {
                if (A022) {
                    C27041cY.A01("collectResults");
                }
                A03(r9, null, r3, r10, null);
                if (A022) {
                    C27041cY.A00();
                }
                if (A022) {
                    C27041cY.A01("sortMountableOutputs");
                }
                Collections.sort(r10.A0d, A0r);
                Collections.sort(r10.A0c, A0q);
                if (r10.A0j) {
                    C31461jj r32 = r10.A0b;
                    List list = r10.A0N;
                    r32.A02 = null;
                    r32.A01 = null;
                    r32.A02 = null;
                    r32.A01 = null;
                    r32.A00 = null;
                    r32.A05 = null;
                    int size = list.size();
                    for (int i3 = 0; i3 < size; i3++) {
                        AnonymousClass117 r4 = (AnonymousClass117) list.get(i3);
                        if (!(r4.A04 == null && r4.A07 == null)) {
                            if (r32.A00 == null) {
                                r32.A00 = new ArrayList(2);
                            }
                            r32.A00.add(new C31471jk(r4));
                        }
                        if (r4.A05 != null) {
                            if (r32.A02 == null) {
                                r32.A02 = new ArrayList(2);
                                r32.A01 = new ArrayList(2);
                            }
                            EVI evi = new EVI(r4);
                            r32.A02.add(evi);
                            r32.A01.add(evi);
                        }
                        if (!(r4.A09 == null && r4.A06 == null)) {
                            if (r32.A03 == null) {
                                r32.A03 = new ArrayList(2);
                                r32.A04 = new ArrayList(2);
                            }
                            EVH evh = new EVH(r4);
                            r32.A03.add(evh);
                            r32.A04.add(evh);
                        }
                        if (r4.A08 != null) {
                            if (r32.A05 == null) {
                                r32.A05 = new ArrayList(2);
                            }
                            r32.A05.add(r4);
                        }
                    }
                    ArrayList arrayList = r32.A03;
                    if (arrayList != null) {
                        Collections.sort(arrayList, C31491jm.A07);
                        Collections.sort(r32.A04, C31491jm.A06);
                    }
                    ArrayList arrayList2 = r32.A02;
                    if (arrayList2 != null) {
                        Collections.sort(arrayList2, C31491jm.A07);
                        Collections.sort(r32.A01, C31491jm.A06);
                    }
                    r10.A0N.clear();
                }
                if (A022) {
                    C27041cY.A00();
                }
                ComponentTree componentTree = r9.A05;
                if (componentTree != null) {
                    z = componentTree.A0b;
                } else {
                    z = AnonymousClass07c.isReconciliationEnabled;
                }
                if (!z && !AnonymousClass07c.useInternalNodesForLayoutDiffing && !AnonymousClass07c.isDebugModeEnabled && !AnonymousClass07c.isEndToEndTestRun) {
                    r10.A0D = null;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01b0, code lost:
        if (r5.A09 != 1) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01b8, code lost:
        if (r5.A07 != 1) goto L_0x01ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01c7, code lost:
        if (r3 == false) goto L_0x01c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0251, code lost:
        if (X.C17770zR.A09(r15.B5K()) != false) goto L_0x0253;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0254, code lost:
        if (r2 != false) goto L_0x0200;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00ee, code lost:
        if (r22 == null) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00fa, code lost:
        if (r15.AOl() == false) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x016e, code lost:
        if (r2 == false) goto L_0x0170;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x017d, code lost:
        if (r5.A08 == 2) goto L_0x017f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0185, code lost:
        if (r5.A0T == null) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x018d, code lost:
        if (r5.A0C == null) goto L_0x018f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0198, code lost:
        if (r5.A05 == 0.0f) goto L_0x019a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01a0, code lost:
        if (r5.A0D == null) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01a8, code lost:
        if (r5.A0W == false) goto L_0x01aa;
     */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x021a  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0223  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x023b  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x038d  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x03b2  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x03c0  */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x0404  */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0412  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x0440  */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x0443  */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x04c7  */
    /* JADX WARNING: Removed duplicated region for block: B:279:0x04d5 A[LOOP:5: B:278:0x04d3->B:279:0x04d5, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:280:0x04e5  */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x0501  */
    /* JADX WARNING: Removed duplicated region for block: B:300:0x0593  */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x05a7  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x0647  */
    /* JADX WARNING: Removed duplicated region for block: B:325:0x0660  */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x0671  */
    /* JADX WARNING: Removed duplicated region for block: B:346:0x06da  */
    /* JADX WARNING: Removed duplicated region for block: B:376:0x0765  */
    /* JADX WARNING: Removed duplicated region for block: B:394:0x07a8  */
    /* JADX WARNING: Removed duplicated region for block: B:408:0x07cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A03(X.AnonymousClass0p4 r35, X.AnonymousClass38U r36, X.C17470yx r37, X.C188915v r38, X.C22571Lz r39) {
        /*
            r6 = r35
            boolean r0 = r6.A0J()
            if (r0 != 0) goto L_0x07f2
            r15 = r37
            boolean r0 = r15.BBZ()
            if (r0 == 0) goto L_0x0013
            r15.BK0()
        L_0x0013:
            X.0zR r20 = r15.B5K()
            boolean r24 = X.C27041cY.A02()
            boolean r0 = X.AnonymousClass07c.isDebugHierarchyEnabled
            r18 = 0
            if (r0 == 0) goto L_0x00be
            java.util.List r3 = r15.Ahs()
            X.38U r1 = new X.38U
            r2 = 3
            r4 = r36
            r0 = r20
            r1.<init>(r4, r0, r3, r2)
        L_0x002f:
            boolean r2 = r15.BFx()
            r5 = r39
            r0 = r38
            if (r2 == 0) goto L_0x00c2
            if (r24 == 0) goto L_0x0077
            java.lang.String r3 = "resolveNestedTree:"
            java.lang.String r2 = r15.getSimpleName()
            java.lang.String r3 = X.AnonymousClass08S.A0J(r3, r2)
            X.0gP r2 = X.C27041cY.A00
            X.0gQ r4 = r2.APo(r3)
            java.lang.String r3 = "EXACTLY "
            int r2 = r15.getWidth()
            java.lang.String r3 = X.AnonymousClass08S.A09(r3, r2)
            java.lang.String r2 = "widthSpec"
            r4.AOo(r2, r3)
            java.lang.String r3 = "EXACTLY "
            int r2 = r15.getHeight()
            java.lang.String r3 = X.AnonymousClass08S.A09(r3, r2)
            java.lang.String r2 = "heightSpec"
            r4.AOo(r2, r3)
            X.0zR r2 = r15.B5K()
            int r3 = r2.A00
            java.lang.String r2 = "rootComponentId"
            r4.AOn(r2, r3)
            r4.flush()
        L_0x0077:
            int r3 = r15.getWidth()
            r2 = 1073741824(0x40000000, float:2.0)
            int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r2)
            int r3 = r15.getHeight()
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r2)
            X.0yx r4 = X.C31951kr.A03(r6, r15, r4, r2)
            if (r24 == 0) goto L_0x0092
            X.C27041cY.A00()
        L_0x0092:
            X.0yx r2 = X.AnonymousClass0p4.A0F
            if (r4 == r2) goto L_0x07f2
            int r3 = r0.A02
            int r2 = r15.BA8()
            int r3 = r3 + r2
            r0.A02 = r3
            int r3 = r0.A03
            int r2 = r15.BAE()
            int r3 = r3 + r2
            r0.A03 = r3
            A03(r6, r1, r4, r0, r5)
            int r2 = r0.A02
            int r1 = r15.BA8()
            int r2 = r2 - r1
            r0.A02 = r2
            int r2 = r0.A03
            int r1 = r15.BAE()
            int r2 = r2 - r1
            r0.A03 = r2
            return
        L_0x00be:
            r1 = r18
            goto L_0x002f
        L_0x00c2:
            int r3 = r15.Apg()
            r2 = 8
            r4 = 1
            if (r3 != r2) goto L_0x00e0
            r15.BCN(r4)
            int r7 = r15.Ah4()
            r6 = 0
        L_0x00d3:
            if (r6 >= r7) goto L_0x00e0
            X.0yx r3 = r15.Ah3(r6)
            r2 = 4
            r3.BCN(r2)
            int r6 = r6 + 1
            goto L_0x00d3
        L_0x00e0:
            boolean r3 = r0.A0U
            X.1Lz r22 = r15.AkV()
            boolean r2 = X.C17770zR.A07(r20)
            if (r2 == 0) goto L_0x00f0
            r23 = 1
            if (r22 != 0) goto L_0x00f2
        L_0x00f0:
            r23 = 0
        L_0x00f2:
            if (r23 == 0) goto L_0x00fc
            boolean r2 = r15.AOl()
            r34 = 1
            if (r2 != 0) goto L_0x00fe
        L_0x00fc:
            r34 = 0
        L_0x00fe:
            if (r3 == 0) goto L_0x0292
            boolean r2 = X.AnonymousClass07c.useInternalNodesForLayoutDiffing
            if (r2 == 0) goto L_0x0261
            r21 = r15
        L_0x0106:
            if (r39 != 0) goto L_0x010c
            r2 = r21
            r0.A0C = r2
        L_0x010c:
            int r2 = r0.A0W
            r6 = 2
            if (r2 != r6) goto L_0x0118
            X.1jd r3 = r15.AwQ()
            r2 = 2
            r3.A08 = r2
        L_0x0118:
            boolean r2 = r0.A08(r15)
            if (r2 != 0) goto L_0x0200
            X.0zR r2 = r15.B5K()
            boolean r2 = X.C17770zR.A09(r2)
            if (r2 != 0) goto L_0x025e
            boolean r2 = r15.BF5()
            if (r2 != 0) goto L_0x0200
            X.0zR r7 = r15.B5K()
            X.1jd r5 = r15.Avp()
            r19 = 0
            r3 = 1
            if (r5 == 0) goto L_0x0141
            boolean r2 = r5.A02()
            if (r2 != 0) goto L_0x0149
        L_0x0141:
            if (r7 == 0) goto L_0x025b
            boolean r2 = r7.A0x()
            if (r2 == 0) goto L_0x025b
        L_0x0149:
            r8 = 1
        L_0x014a:
            int r7 = r15.Apg()
            boolean r2 = r0.A0R
            if (r2 == 0) goto L_0x0257
            if (r7 == r6) goto L_0x0257
            if (r8 != 0) goto L_0x0162
            if (r5 == 0) goto L_0x0160
            java.lang.CharSequence r2 = r5.A0S
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 == 0) goto L_0x0162
        L_0x0160:
            if (r7 == 0) goto L_0x0257
        L_0x0162:
            r17 = 1
        L_0x0164:
            if (r5 == 0) goto L_0x0170
            X.10N r7 = r5.A0G
            r2 = 0
            if (r7 == 0) goto L_0x016c
            r2 = 1
        L_0x016c:
            r16 = 1
            if (r2 != 0) goto L_0x0172
        L_0x0170:
            r16 = 0
        L_0x0172:
            if (r5 == 0) goto L_0x017f
            boolean r2 = r5.A01()
            if (r2 == 0) goto L_0x017f
            int r2 = r5.A08
            r14 = 1
            if (r2 != r6) goto L_0x0180
        L_0x017f:
            r14 = 0
        L_0x0180:
            if (r5 == 0) goto L_0x0187
            java.lang.Object r2 = r5.A0T
            r13 = 1
            if (r2 != 0) goto L_0x0188
        L_0x0187:
            r13 = 0
        L_0x0188:
            if (r5 == 0) goto L_0x018f
            android.util.SparseArray r2 = r5.A0C
            r12 = 1
            if (r2 != 0) goto L_0x0190
        L_0x018f:
            r12 = 0
        L_0x0190:
            if (r5 == 0) goto L_0x019a
            float r6 = r5.A05
            r2 = 0
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            r11 = 1
            if (r2 != 0) goto L_0x019b
        L_0x019a:
            r11 = 0
        L_0x019b:
            if (r5 == 0) goto L_0x01a2
            android.view.ViewOutlineProvider r2 = r5.A0D
            r10 = 1
            if (r2 != 0) goto L_0x01a3
        L_0x01a2:
            r10 = 0
        L_0x01a3:
            if (r5 == 0) goto L_0x01aa
            boolean r2 = r5.A0W
            r8 = 1
            if (r2 != 0) goto L_0x01ab
        L_0x01aa:
            r8 = 0
        L_0x01ab:
            if (r5 == 0) goto L_0x01b2
            int r2 = r5.A09
            r7 = 1
            if (r2 == r3) goto L_0x01b3
        L_0x01b2:
            r7 = 0
        L_0x01b3:
            if (r5 == 0) goto L_0x01ba
            int r2 = r5.A07
            r6 = 1
            if (r2 == r3) goto L_0x01bb
        L_0x01ba:
            r6 = 0
        L_0x01bb:
            if (r5 == 0) goto L_0x01c9
            int r5 = r5.A0A
            r2 = 8388608(0x800000, float:1.17549435E-38)
            r5 = r5 & r2
            r3 = 0
            if (r5 == 0) goto L_0x01c6
            r3 = 1
        L_0x01c6:
            r2 = 1
            if (r3 != 0) goto L_0x01ca
        L_0x01c9:
            r2 = 0
        L_0x01ca:
            if (r16 != 0) goto L_0x01e0
            if (r14 != 0) goto L_0x01e0
            if (r13 != 0) goto L_0x01e0
            if (r12 != 0) goto L_0x01e0
            if (r11 != 0) goto L_0x01e0
            if (r10 != 0) goto L_0x01e0
            if (r8 != 0) goto L_0x01e0
            if (r2 != 0) goto L_0x01e0
            if (r17 != 0) goto L_0x01e0
            if (r7 != 0) goto L_0x01e0
            if (r6 == 0) goto L_0x01e2
        L_0x01e0:
            r19 = 1
        L_0x01e2:
            if (r19 != 0) goto L_0x0200
            java.util.List r2 = r15.Ahs()
            java.util.Iterator r3 = r2.iterator()
        L_0x01ec:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x023e
            java.lang.Object r2 = r3.next()
            X.0zR r2 = (X.C17770zR) r2
            if (r2 == 0) goto L_0x01ec
            boolean r2 = r2.A1J()
            if (r2 == 0) goto L_0x01ec
        L_0x0200:
            r36 = 1
        L_0x0202:
            long r2 = r0.A09
            r16 = r2
            int r2 = r0.A0V
            r39 = r2
            X.0zP r2 = r0.A0X
            r38 = r2
            X.0zM r2 = r0.A0F
            r37 = r2
            X.0zP r2 = X.C07070ca.A00(r15)
            r0.A0X = r2
            if (r2 == 0) goto L_0x023b
            X.0zM r2 = new X.0zM
            r2.<init>()
        L_0x021f:
            r0.A0F = r2
            if (r36 == 0) goto L_0x0359
            X.0zR r2 = r15.B5K()
            boolean r2 = X.C17770zR.A09(r2)
            if (r2 == 0) goto L_0x0296
            boolean r2 = r0.A08(r15)
            if (r2 != 0) goto L_0x0296
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "We shouldn't insert a host as a parent of a View"
            r1.<init>(r0)
            throw r1
        L_0x023b:
            r2 = r18
            goto L_0x021f
        L_0x023e:
            java.lang.String r2 = r15.B73()
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0253
            X.0zR r2 = r15.B5K()
            boolean r3 = X.C17770zR.A09(r2)
            r2 = 1
            if (r3 == 0) goto L_0x0254
        L_0x0253:
            r2 = 0
        L_0x0254:
            if (r2 == 0) goto L_0x025e
            goto L_0x0200
        L_0x0257:
            r17 = 0
            goto L_0x0164
        L_0x025b:
            r8 = 0
            goto L_0x014a
        L_0x025e:
            r36 = 0
            goto L_0x0202
        L_0x0261:
            X.1lA r21 = new X.1lA
            r21.<init>()
            int r3 = r15.AsD()
            r2 = r21
            r2.C8r(r3)
            int r3 = r15.Ary()
            r2.C8l(r3)
            float r3 = r15.As2()
            r2.C8n(r3)
            float r3 = r15.As1()
            r2.C8m(r3)
            X.0zR r3 = r15.B5K()
            r2.C70(r3)
            if (r39 == 0) goto L_0x0106
            r5.AMg(r2)
            goto L_0x0106
        L_0x0292:
            r21 = r18
            goto L_0x010c
        L_0x0296:
            X.0zH r5 = new X.0zH
            r5.<init>()
            java.util.List r2 = r15.Ahs()
            android.util.SparseArray r9 = new android.util.SparseArray
            r9.<init>()
            java.util.Iterator r11 = r2.iterator()
        L_0x02a8:
            boolean r2 = r11.hasNext()
            if (r2 == 0) goto L_0x02d3
            java.lang.Object r2 = r11.next()
            X.0zR r2 = (X.C17770zR) r2
            android.util.SparseArray r8 = r2.A13()
            if (r8 == 0) goto L_0x02a8
            r6 = 0
        L_0x02bb:
            int r2 = r8.size()
            if (r6 >= r2) goto L_0x02a8
            int r3 = r8.keyAt(r6)
            java.lang.Object r2 = r8.get(r3)
            X.0zZ r2 = (X.C17840zZ) r2
            if (r2 == 0) goto L_0x02d0
            r9.append(r3, r2)
        L_0x02d0:
            int r6 = r6 + 1
            goto L_0x02bb
        L_0x02d3:
            r5.A00 = r9
            boolean r2 = r0.A08(r15)
            if (r2 == 0) goto L_0x0493
            r2 = 0
        L_0x02dd:
            r30 = 0
            int r31 = r15.Apg()
            boolean r32 = r15.BER()
            r33 = 0
            r28 = r0
            r29 = r15
            r25 = r5
            r26 = r2
            X.0zN r6 = A00(r25, r26, r28, r29, r30, r31, r32, r33)
            X.0zF r3 = r6.A0C
            if (r3 == 0) goto L_0x0305
            boolean r2 = r15.BBs()
            if (r2 == 0) goto L_0x048b
            int r2 = r15.B3n()
            r3.A00 = r2
        L_0x0305:
            A07(r0, r6)
            java.util.List r2 = r0.A0e
            int r2 = r2.size()
            int r5 = r2 + -1
            if (r21 == 0) goto L_0x0317
            r2 = r21
            r2.C8J(r6)
        L_0x0317:
            boolean r2 = r0.A08(r15)
            if (r2 == 0) goto L_0x047c
            r2 = 0
            r6.A02 = r2
            if (r1 == 0) goto L_0x032b
            r9 = 3
            int r2 = r1.A00
            if (r2 != r9) goto L_0x046f
            r8 = r1
        L_0x0329:
            r6.A03 = r8
        L_0x032b:
            r2 = 2
            r6.A01 = r2
        L_0x032e:
            X.0zB r8 = r0.A0Z
            if (r8 == 0) goto L_0x033b
            long r2 = r6.A02
            java.lang.Integer r7 = java.lang.Integer.valueOf(r5)
            r8.A0D(r2, r7)
        L_0x033b:
            X.0zM r3 = r0.A0F
            r2 = 3
            if (r3 == 0) goto L_0x0343
            r3.A04(r2, r6)
        L_0x0343:
            A06(r0)
            int r2 = r0.A01
            int r2 = r2 + r4
            r0.A01 = r2
            java.util.List r2 = r0.A0e
            java.lang.Object r2 = r2.get(r5)
            X.0zN r2 = (X.C17730zN) r2
            long r2 = r2.A02
            r0.A09 = r2
            r0.A0V = r5
        L_0x0359:
            boolean r2 = r0.A0T
            r19 = r2
            if (r36 != 0) goto L_0x0367
            if (r2 == 0) goto L_0x046c
            boolean r2 = r15.BER()
            if (r2 == 0) goto L_0x046c
        L_0x0367:
            r2 = 1
        L_0x0368:
            r0.A0T = r2
            X.0zR r5 = r15.B5K()
            if (r5 == 0) goto L_0x0469
            java.lang.Integer r3 = r5.A0U()
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            if (r3 == r2) goto L_0x0469
            long r2 = r0.A09
            r10 = 1
            int r11 = r15.Apg()
            boolean r4 = r0.A0T
            r8 = r0
            r9 = r15
            r6 = r2
            r12 = r4
            r13 = r36
            X.0zN r4 = A00(r5, r6, r8, r9, r10, r11, r12, r13)
        L_0x038b:
            if (r4 == 0) goto L_0x03ac
            if (r23 == 0) goto L_0x0465
            X.0zN r2 = r22.AiP()
            if (r2 == 0) goto L_0x0465
            X.0zN r2 = r22.AiP()
            long r2 = r2.A02
        L_0x039b:
            int r5 = r0.A01
            r28 = r0
            r29 = r4
            r31 = 0
            r30 = r5
            r32 = r2
            r35 = r1
            r28.A05(r29, r30, r31, r32, r34, r35)
        L_0x03ac:
            android.graphics.drawable.Drawable r3 = r15.Ads()
            if (r3 == 0) goto L_0x03ba
            if (r4 == 0) goto L_0x0445
            X.0zF r2 = r4.A0C
            if (r2 == 0) goto L_0x0445
            r2.A04 = r3
        L_0x03ba:
            boolean r2 = X.C17770zR.A07(r20)
            if (r2 == 0) goto L_0x0443
            if (r24 == 0) goto L_0x03cf
            java.lang.String r3 = "onBoundsDefined:"
            java.lang.String r2 = r15.getSimpleName()
            java.lang.String r2 = X.AnonymousClass08S.A0J(r3, r2)
            X.C27041cY.A01(r2)
        L_0x03cf:
            r2 = r20
            X.0p4 r3 = r2.A03
            r2.A0e(r3, r15)
            if (r24 == 0) goto L_0x03db
            X.C27041cY.A00()
        L_0x03db:
            A07(r0, r4)
            X.0zB r6 = r0.A0Z
            java.util.List r2 = r0.A0e
            int r2 = r2.size()
            int r5 = r2 + -1
            if (r6 == 0) goto L_0x03f3
            long r2 = r4.A02
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r6.A0D(r2, r5)
        L_0x03f3:
            X.0zM r3 = r0.A0F
            r7 = 0
            if (r3 == 0) goto L_0x03fb
            r3.A04(r7, r4)
        L_0x03fb:
            if (r21 == 0) goto L_0x0402
            r2 = r21
            r2.C78(r4)
        L_0x0402:
            if (r20 == 0) goto L_0x0440
            r2 = r20
            X.0p4 r2 = r2.A03
        L_0x0408:
            if (r2 == 0) goto L_0x043a
            com.facebook.litho.ComponentTree r2 = r2.A05
            if (r2 == 0) goto L_0x043a
            boolean r2 = r2.A0c
        L_0x0410:
            if (r2 == 0) goto L_0x04ad
            java.util.ArrayList r9 = r15.B76()
            if (r9 == 0) goto L_0x0497
            int r8 = r9.size()
            r6 = 0
        L_0x041d:
            if (r6 >= r8) goto L_0x0497
            java.lang.Object r5 = r9.get(r6)
            X.0zQ r5 = (X.C17760zQ) r5
            java.util.List r2 = r0.A0M
            if (r2 != 0) goto L_0x0430
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0.A0M = r2
        L_0x0430:
            java.util.List r3 = r0.A0M
            java.lang.String r2 = r0.A0J
            X.C07070ca.A02(r5, r3, r2)
            int r6 = r6 + 1
            goto L_0x041d
        L_0x043a:
            r2 = 0
            boolean r2 = X.C07070ca.A03(r2)
            goto L_0x0410
        L_0x0440:
            r2 = r18
            goto L_0x0408
        L_0x0443:
            r7 = 0
            goto L_0x0402
        L_0x0445:
            if (r22 == 0) goto L_0x0462
            X.0zN r32 = r22.Adz()
        L_0x044b:
            r30 = r15
            r31 = r0
            r33 = r1
            r35 = 1
            r34 = r3
            X.0zN r3 = A01(r30, r31, r32, r33, r34, r35, r36)
            if (r21 == 0) goto L_0x03ba
            r2 = r21
            r2.C6H(r3)
            goto L_0x03ba
        L_0x0462:
            r32 = r18
            goto L_0x044b
        L_0x0465:
            r2 = -1
            goto L_0x039b
        L_0x0469:
            r4 = 0
            goto L_0x038b
        L_0x046c:
            r2 = 0
            goto L_0x0368
        L_0x046f:
            X.38U r8 = new X.38U
            X.38U r7 = r1.A02
            X.0zR r3 = r1.A01
            java.util.List r2 = r1.A03
            r8.<init>(r7, r3, r2, r9)
            goto L_0x0329
        L_0x047c:
            int r2 = r0.A01
            r10 = 3
            r11 = -1
            r13 = 0
            r7 = r0
            r8 = r6
            r9 = r2
            r14 = r1
            r7.A05(r8, r9, r10, r11, r13, r14)
            goto L_0x032e
        L_0x048b:
            android.animation.StateListAnimator r2 = r15.B3m()
            r3.A01 = r2
            goto L_0x0305
        L_0x0493:
            long r2 = r0.A09
            goto L_0x02dd
        L_0x0497:
            java.util.ArrayList r3 = r15.Ahu()
            if (r3 == 0) goto L_0x04ad
            java.util.List r2 = r0.A0L
            if (r2 != 0) goto L_0x04a8
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0.A0L = r2
        L_0x04a8:
            java.util.List r2 = r0.A0L
            r2.addAll(r3)
        L_0x04ad:
            int r3 = r0.A02
            int r2 = r15.BA8()
            int r3 = r3 + r2
            r0.A02 = r3
            int r3 = r0.A03
            int r2 = r15.BAE()
            int r3 = r3 + r2
            r0.A03 = r3
            int r8 = r0.A0W
            X.1jd r2 = r15.Avp()
            if (r2 == 0) goto L_0x04e5
            X.1jd r2 = r15.Avp()
            int r2 = r2.A08
        L_0x04cd:
            r0.A0W = r2
            int r6 = r15.Ah4()
        L_0x04d3:
            if (r7 >= r6) goto L_0x04e7
            X.0p4 r5 = r15.AiS()
            X.0yx r3 = r15.Ah3(r7)
            r2 = r21
            A03(r5, r1, r3, r0, r2)
            int r7 = r7 + 1
            goto L_0x04d3
        L_0x04e5:
            r2 = 0
            goto L_0x04cd
        L_0x04e7:
            r0.A0W = r8
            int r3 = r0.A02
            int r2 = r15.BA8()
            int r3 = r3 - r2
            r0.A02 = r3
            int r3 = r0.A03
            int r2 = r15.BAE()
            int r3 = r3 - r2
            r0.A03 = r3
            boolean r2 = r15.CDz()
            if (r2 == 0) goto L_0x058d
            if (r22 == 0) goto L_0x06d4
            X.0zN r25 = r22.Aev()
        L_0x0507:
            boolean r2 = r15.CDz()
            if (r2 == 0) goto L_0x07ea
            X.0zG r5 = r15.C0A()
            X.0zG r3 = X.C17660zG.RTL
            r2 = 0
            if (r5 != r3) goto L_0x0517
            r2 = 1
        L_0x0517:
            float[] r10 = r15.Aex()
            int[] r9 = r15.Aeu()
            if (r2 == 0) goto L_0x06d0
            X.10G r8 = X.AnonymousClass10G.RIGHT
        L_0x0523:
            if (r2 == 0) goto L_0x06cc
            X.10G r7 = X.AnonymousClass10G.LEFT
        L_0x0527:
            X.38V r6 = new X.38V
            r6.<init>()
            android.graphics.PathEffect r2 = r15.Aew()
            r6.A08 = r2
            int r2 = X.AnonymousClass38W.A01(r9, r8)
            r6.A05 = r2
            X.10G r5 = X.AnonymousClass10G.TOP
            int r2 = X.AnonymousClass38W.A01(r9, r5)
            r6.A07 = r2
            int r2 = X.AnonymousClass38W.A01(r9, r7)
            r6.A06 = r2
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            int r2 = X.AnonymousClass38W.A01(r9, r3)
            r6.A04 = r2
            int r2 = r15.AsF(r8)
            float r2 = (float) r2
            r6.A01 = r2
            int r2 = r15.AsF(r5)
            float r2 = (float) r2
            r6.A03 = r2
            int r2 = r15.AsF(r7)
            float r2 = (float) r2
            r6.A02 = r2
            int r2 = r15.AsF(r3)
            float r2 = (float) r2
            r6.A00 = r2
            int r2 = r10.length
            float[] r2 = java.util.Arrays.copyOf(r10, r2)
            r6.A09 = r2
            X.38X r2 = new X.38X
            r2.<init>(r6)
            r28 = 4
            r23 = r15
            r24 = r0
            r26 = r1
            r27 = r2
            r29 = r36
            X.0zN r3 = A01(r23, r24, r25, r26, r27, r28, r29)
            if (r21 == 0) goto L_0x058d
            r2 = r21
            r2.C6O(r3)
        L_0x058d:
            android.graphics.drawable.Drawable r6 = r15.AnL()
            if (r6 == 0) goto L_0x05a1
            if (r4 == 0) goto L_0x06af
            X.0zF r5 = r4.A0C
            if (r5 == 0) goto L_0x06af
            int r3 = android.os.Build.VERSION.SDK_INT
            r2 = 23
            if (r3 < r2) goto L_0x06af
            r5.A05 = r6
        L_0x05a1:
            boolean r2 = r15.BBx()
            if (r2 == 0) goto L_0x060b
            int r13 = r0.A02
            int r2 = r15.BA8()
            int r13 = r13 + r2
            int r12 = r0.A03
            int r2 = r15.BAE()
            int r12 = r12 + r2
            int r11 = r15.getWidth()
            int r11 = r11 + r13
            int r10 = r15.getHeight()
            int r10 = r10 + r12
            X.10N r14 = r15.B9Y()
            X.10N r9 = r15.AnI()
            X.10N r8 = r15.B7X()
            X.10N r7 = r15.Anq()
            X.10N r6 = r15.Aqk()
            X.10N r5 = r15.B9W()
            X.117 r3 = new X.117
            r3.<init>()
            X.0zR r2 = r15.B5K()
            r3.A03 = r2
            android.graphics.Rect r2 = r3.A0A
            r2.set(r13, r12, r11, r10)
            float r2 = r15.B9Z()
            r3.A01 = r2
            float r2 = r15.B9b()
            r3.A02 = r2
            r3.A09 = r14
            r3.A04 = r9
            r3.A07 = r8
            r3.A05 = r7
            r3.A06 = r6
            r3.A08 = r5
            java.util.List r2 = r0.A0N
            r2.add(r3)
            if (r21 == 0) goto L_0x060b
            r2 = r21
            r2.CDF(r3)
        L_0x060b:
            java.util.List r2 = r0.A0f
            if (r2 == 0) goto L_0x0650
            java.lang.String r2 = r15.B5X()
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0650
            int r8 = r0.A02
            int r2 = r15.BA8()
            int r8 = r8 + r2
            int r7 = r0.A03
            int r2 = r15.BAE()
            int r7 = r7 + r2
            int r6 = r15.getWidth()
            int r6 = r6 + r8
            int r3 = r15.getHeight()
            int r3 = r3 + r7
            X.2FU r5 = new X.2FU
            r5.<init>()
            java.lang.String r2 = r15.B5X()
            r5.A02 = r2
            android.graphics.Rect r2 = r5.A03
            r2.set(r8, r7, r6, r3)
            long r2 = r0.A09
            r5.A00 = r2
            if (r4 == 0) goto L_0x064b
            long r2 = r4.A02
            r5.A01 = r2
        L_0x064b:
            java.util.List r2 = r0.A0f
            r2.add(r5)
        L_0x0650:
            java.util.ArrayList r3 = r15.BA2()
            if (r3 == 0) goto L_0x06d8
            boolean r2 = r3.isEmpty()
            if (r2 != 0) goto L_0x06d8
            X.38Y r2 = r0.A0I
            if (r2 != 0) goto L_0x0667
            X.38Y r2 = new X.38Y
            r2.<init>()
            r0.A0I = r2
        L_0x0667:
            java.util.Iterator r9 = r3.iterator()
        L_0x066b:
            boolean r2 = r9.hasNext()
            if (r2 == 0) goto L_0x06d8
            java.lang.Object r2 = r9.next()
            X.38a r2 = (X.AnonymousClass38a) r2
            X.38Y r3 = r0.A0I
            java.lang.String r8 = r2.A02
            X.EvT r7 = r2.A00
            X.0zR r6 = r2.A01
            java.util.Map r2 = r3.A00
            if (r2 != 0) goto L_0x068a
            java.util.LinkedHashMap r2 = new java.util.LinkedHashMap
            r2.<init>()
            r3.A00 = r2
        L_0x068a:
            java.lang.String r5 = "_"
            int r2 = r7.hashCode()
            java.lang.String r5 = X.AnonymousClass08S.A0L(r8, r5, r2)
            java.util.Map r2 = r3.A00
            java.lang.Object r2 = r2.get(r5)
            X.38b r2 = (X.AnonymousClass38b) r2
            if (r2 != 0) goto L_0x06a9
            java.util.Map r3 = r3.A00
            X.38b r2 = new X.38b
            r2.<init>(r8, r7, r6)
            r3.put(r5, r2)
            goto L_0x066b
        L_0x06a9:
            java.util.List r2 = r2.A02
            r2.add(r6)
            goto L_0x066b
        L_0x06af:
            if (r22 == 0) goto L_0x06c9
            X.0zN r9 = r22.AnN()
        L_0x06b5:
            r12 = 2
            r7 = r15
            r8 = r0
            r10 = r1
            r11 = r6
            r13 = r36
            X.0zN r3 = A01(r7, r8, r9, r10, r11, r12, r13)
            if (r21 == 0) goto L_0x05a1
            r2 = r21
            r2.C84(r3)
            goto L_0x05a1
        L_0x06c9:
            r9 = r18
            goto L_0x06b5
        L_0x06cc:
            X.10G r7 = X.AnonymousClass10G.RIGHT
            goto L_0x0527
        L_0x06d0:
            X.10G r8 = X.AnonymousClass10G.LEFT
            goto L_0x0523
        L_0x06d4:
            r25 = r18
            goto L_0x0507
        L_0x06d8:
            if (r20 == 0) goto L_0x0761
            android.graphics.Rect r5 = new android.graphics.Rect
            r5.<init>()
            if (r4 == 0) goto L_0x0740
            android.graphics.Rect r2 = r4.A08
            r5.set(r2)
        L_0x06e6:
            java.util.List r2 = r15.Ahs()
            java.util.Iterator r7 = r2.iterator()
        L_0x06ee:
            boolean r2 = r7.hasNext()
            if (r2 == 0) goto L_0x0761
            java.lang.Object r6 = r7.next()
            X.0zR r6 = (X.C17770zR) r6
            android.graphics.Rect r4 = new android.graphics.Rect
            r4.<init>()
            r4.set(r5)
            X.0p4 r2 = r6.A03
            if (r2 == 0) goto L_0x0729
            com.facebook.litho.ComponentTree r2 = r2.A05
            if (r2 == 0) goto L_0x0729
            java.util.List r2 = r0.A0K
            if (r2 == 0) goto L_0x0711
            r2.add(r6)
        L_0x0711:
            boolean r2 = r6.A0u()
            if (r2 == 0) goto L_0x0729
            java.util.Map r2 = r0.A0O
            if (r2 != 0) goto L_0x0722
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            r0.A0O = r2
        L_0x0722:
            java.util.Map r3 = r0.A0O
            java.lang.String r2 = r6.A06
            r3.put(r2, r6)
        L_0x0729:
            java.lang.String r3 = r6.A06
            if (r3 == 0) goto L_0x0732
            java.util.Map r2 = r0.A0g
            r2.put(r3, r4)
        L_0x0732:
            X.5zX r3 = r6.A05
            r2 = 0
            if (r3 == 0) goto L_0x0738
            r2 = 1
        L_0x0738:
            if (r2 == 0) goto L_0x06ee
            java.util.Map r2 = r0.A0l
            r2.put(r3, r4)
            goto L_0x06ee
        L_0x0740:
            int r4 = r0.A02
            int r2 = r15.BA8()
            int r4 = r4 + r2
            r5.left = r4
            int r3 = r0.A03
            int r2 = r15.BAE()
            int r3 = r3 + r2
            r5.top = r3
            int r2 = r15.getWidth()
            int r4 = r4 + r2
            r5.right = r4
            int r2 = r15.getHeight()
            int r3 = r3 + r2
            r5.bottom = r3
            goto L_0x06e6
        L_0x0761:
            boolean r2 = X.AnonymousClass07c.enableLithoViewDebugOverlay
            if (r2 == 0) goto L_0x07a8
            boolean r2 = r0.A08(r15)
            if (r2 == 0) goto L_0x07c6
            int r3 = r0.A00
            java.lang.Object r5 = X.C188915v.A0s
            monitor-enter(r5)
            java.util.Map r2 = X.C188915v.A0p     // Catch:{ all -> 0x07a5 }
            if (r2 != 0) goto L_0x077b
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x07a5 }
            r2.<init>()     // Catch:{ all -> 0x07a5 }
            X.C188915v.A0p = r2     // Catch:{ all -> 0x07a5 }
        L_0x077b:
            java.util.Map r2 = X.C188915v.A0p     // Catch:{ all -> 0x07a5 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x07a5 }
            java.lang.Object r4 = r2.get(r3)     // Catch:{ all -> 0x07a5 }
            java.util.List r4 = (java.util.List) r4     // Catch:{ all -> 0x07a5 }
            if (r4 != 0) goto L_0x078e
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x07a5 }
            r4.<init>()     // Catch:{ all -> 0x07a5 }
        L_0x078e:
            boolean r2 = X.C191216w.A00()     // Catch:{ all -> 0x07a5 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x07a5 }
            r4.add(r2)     // Catch:{ all -> 0x07a5 }
            java.util.Map r2 = X.C188915v.A0p     // Catch:{ all -> 0x07a5 }
            r2.put(r3, r4)     // Catch:{ all -> 0x07a5 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x07a5 }
            r3.<init>(r4)     // Catch:{ all -> 0x07a5 }
            monitor-exit(r5)     // Catch:{ all -> 0x07a5 }
            goto L_0x07b6
        L_0x07a5:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x07a5 }
            goto L_0x07b5
        L_0x07a8:
            java.util.Map r1 = X.C188915v.A0p
            if (r1 == 0) goto L_0x07c6
            java.lang.Object r1 = X.C188915v.A0s
            monitor-enter(r1)
            X.C188915v.A0p = r18     // Catch:{ all -> 0x07b3 }
            monitor-exit(r1)     // Catch:{ all -> 0x07b3 }
            goto L_0x07c6
        L_0x07b3:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x07b3 }
        L_0x07b5:
            throw r0
        L_0x07b6:
            r5 = 0
            X.38Z r2 = new X.38Z
            r2.<init>(r3)
            r8 = 2
            r3 = r15
            r4 = r0
            r6 = r1
            r7 = r2
            r9 = r36
            A01(r3, r4, r5, r6, r7, r8, r9)
        L_0x07c6:
            long r2 = r0.A09
            int r1 = (r2 > r16 ? 1 : (r2 == r16 ? 0 : -1))
            if (r1 == 0) goto L_0x07da
            r1 = r16
            r0.A09 = r1
            r1 = r39
            r0.A0V = r1
            int r1 = r0.A01
            int r1 = r1 + -1
            r0.A01 = r1
        L_0x07da:
            r1 = r19
            r0.A0T = r1
            A06(r0)
            r1 = r38
            r0.A0X = r1
            r1 = r37
            r0.A0F = r1
            return
        L_0x07ea:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "This node does not support drawing border color"
            r1.<init>(r0)
            throw r1
        L_0x07f2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C188915v.A03(X.0p4, X.38U, X.0yx, X.15v, X.1Lz):void");
    }
}
