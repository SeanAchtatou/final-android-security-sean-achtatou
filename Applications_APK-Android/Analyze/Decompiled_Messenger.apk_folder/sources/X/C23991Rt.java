package X;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;

/* renamed from: X.1Rt  reason: invalid class name and case insensitive filesystem */
public final class C23991Rt implements Runnable {
    public static final String __redex_internal_original_name = "bolts.Task$4";
    public final /* synthetic */ C30405Evg A00;
    public final /* synthetic */ C23951Rp A01;
    public final /* synthetic */ Callable A02;

    public C23991Rt(C30405Evg evg, C23951Rp r2, Callable callable) {
        this.A00 = evg;
        this.A01 = r2;
        this.A02 = callable;
    }

    public void run() {
        if (this.A00 != null) {
            AnonymousClass7S9 r0 = null;
            if (r0.A00()) {
                this.A01.A00();
                return;
            }
        }
        try {
            this.A01.A02(this.A02.call());
        } catch (CancellationException unused) {
            this.A01.A00();
        } catch (Exception e) {
            this.A01.A01(e);
        }
    }
}
