package X;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;

/* renamed from: X.1Wh  reason: invalid class name */
public final class AnonymousClass1Wh implements AnonymousClass1Wi {
    public Queue A00 = new ArrayDeque();
    public final Map A01 = new HashMap();
    public final Executor A02;

    public AnonymousClass1Wh(Executor executor) {
        this.A02 = executor;
    }
}
