package X;

/* renamed from: X.0FH  reason: invalid class name */
public final class AnonymousClass0FH {
    public static volatile Boolean A00;

    static {
        AnonymousClass0FI r2 = new AnonymousClass0FI();
        if (AnonymousClass00I.A03) {
            AnonymousClass00I.A01(AnonymousClass00I.A00, r2);
        }
    }

    public static void A00() {
        String A02 = AnonymousClass00I.A02("debug.atrace.app_cmdlines");
        if (A02.length() != 0) {
            String[] split = A02.split(",");
            String A002 = AnonymousClass00H.A00();
            for (String equals : split) {
                if (A002.equals(equals)) {
                    A00 = true;
                    return;
                }
            }
        }
        A00 = false;
    }
}
