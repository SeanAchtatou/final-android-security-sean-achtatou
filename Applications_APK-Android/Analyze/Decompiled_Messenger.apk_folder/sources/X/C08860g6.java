package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0g6  reason: invalid class name and case insensitive filesystem */
public final class C08860g6 {
    private static volatile C08860g6 A06;
    public AnonymousClass0UN A00;
    public Integer A01 = AnonymousClass07B.A00;
    public boolean A02 = true;
    public boolean A03 = true;
    public boolean A04 = true;
    public final C08900gA A05;

    public static final C08860g6 A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (C08860g6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new C08860g6(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public void A01(int i) {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(4, AnonymousClass1Y3.APr, this.A00)).C4C(new C08970gH(this, i));
        if (((AnonymousClass0WE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BEI, this.A00)).A05() && ((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, this.A00)).AbO(239, false)) {
            this.A01 = AnonymousClass07B.A00;
            this.A04 = true;
            this.A02 = true;
            this.A03 = true;
            ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, this.A00)).CH3(this.A05);
            ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, this.A00)).AOM(this.A05, "start_type", Integer.toString(i));
        }
    }

    private C08860g6(AnonymousClass1XY r5) {
        C08900gA r0;
        AnonymousClass0UN r2 = new AnonymousClass0UN(5, r5);
        this.A00 = r2;
        if (!((AnonymousClass0WE) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BEI, r2)).A05() || !((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A1q, false)) {
            r0 = C08870g7.A1Q;
        } else {
            r0 = C08870g7.A1R;
        }
        this.A05 = r0;
    }
}
