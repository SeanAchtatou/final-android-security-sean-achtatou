package X;

import androidx.fragment.app.Fragment;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.1Ey  reason: invalid class name and case insensitive filesystem */
public final class C21081Ey {
    public final Fragment A00;
    public final Set A01 = new CopyOnWriteArraySet();

    public C21081Ey(Fragment fragment) {
        this.A00 = fragment;
    }
}
