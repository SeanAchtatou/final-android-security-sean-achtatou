package X;

import java.lang.ref.WeakReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1kT  reason: invalid class name and case insensitive filesystem */
public final class C31841kT {
    private static volatile C31841kT A01;
    public WeakReference A00 = new WeakReference(null);

    public static final C31841kT A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C31841kT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C31841kT();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
