package X;

import android.database.Cursor;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableList;
import java.util.Collection;

@UserScoped
/* renamed from: X.1e7  reason: invalid class name and case insensitive filesystem */
public final class C28011e7 {
    private static C05540Zi A06;
    private static final String[] A07;
    private final AnonymousClass0jD A00;
    private final AnonymousClass06B A01;
    private final C12640pj A02;
    private final C12660pl A03;
    private final Boolean A04;
    private final C04310Tq A05;

    static {
        String[] strArr = new String[54];
        System.arraycopy(new String[]{"user_key", "first_name", "last_name", "name", "username", "is_messenger_user", "profile_pic_square", "profile_type", "allow_admin_create_appointment", "is_commerce", "is_partial", "user_rank", "is_blocked_by_viewer", "is_message_blocked_by_viewer", "commerce_page_type", "can_viewer_message", "commerce_page_settings", "is_friend", "last_fetch_time", "montage_thread_fbid", "is_broadcast_recipient_holdout", "is_messenger_bot", "is_vc_endpoint", "is_messenger_promotion_blocked_by_viewer", ECX.$const$string(166), ECX.$const$string(AnonymousClass1Y3.A1A), "is_messenger_platform_bot"}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{"user_call_to_actions", ECX.$const$string(AnonymousClass1Y3.A15), ECX.$const$string(AnonymousClass1Y3.A18), "does_accept_user_feedback", "extension_properties", "viewer_connection_status", "is_memorialized", "nested_menu_call_to_actions", "managing_ps", "is_aloha_proxy_confirmed", "aloha_proxy_user_owners", "instant_game_channel", "is_messenger_welcome_page_cta_enabled", "is_message_ignored_by_viewer", "aloha_proxy_users_owned", "is_viewer_subscribed_to_message_updates", "is_conversation_ice_breaker_enabled", "is_work_user", "is_viewer_coworker", "favorite_color", "is_viewer_managing_parent", "inbox_profile_pic_uri", "inbox_profile_pic_file_path", "work_info", "messaging_actor_type", "is_business_active", "is_verified"}, 0, strArr, 27, 27);
        A07 = strArr;
    }

    public static final C28011e7 A00(AnonymousClass1XY r9) {
        C28011e7 r0;
        synchronized (C28011e7.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r9)) {
                    AnonymousClass1XY r1 = (AnonymousClass1XY) A06.A01();
                    C05540Zi r02 = A06;
                    AnonymousClass06B A022 = AnonymousClass067.A02();
                    C04310Tq A023 = C25771aN.A02(r1);
                    C12630pi.A02(r1);
                    r02.A00 = new C28011e7(A022, A023, new C12640pj(), new C12660pl(r1), AnonymousClass0jD.A00(r1), AnonymousClass0UU.A08(r1));
                }
                C05540Zi r12 = A06;
                r0 = (C28011e7) r12.A00;
                r12.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:97|98) */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x04a8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x04b0, code lost:
        throw new java.lang.RuntimeException("Unexpected serialization exception", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x04bf, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x04c3, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
        r31 = X.AnonymousClass07B.A00;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:101:0x02e1 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:97:0x02d0 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.common.collect.ImmutableList A01(X.C28011e7 r53, java.util.Collection r54) {
        /*
            java.lang.String r1 = "DbFetchThreadUsersHandler.doThreadUsersQuery"
            r0 = 351687803(0x14f6547b, float:2.4872974E-26)
            X.C005505z.A03(r1, r0)
            java.lang.String r50 = "user_key"
            r1 = r54
            if (r54 == 0) goto L_0x000f
            goto L_0x0012
        L_0x000f:
            r5 = 0
            r6 = 0
            goto L_0x0029
        L_0x0012:
            X.1gr r0 = new X.1gr     // Catch:{ all -> 0x04c4 }
            r0.<init>()     // Catch:{ all -> 0x04c4 }
            java.util.Collection r1 = X.AnonymousClass0TH.A00(r1, r0)     // Catch:{ all -> 0x04c4 }
            r0 = r50
            X.0av r0 = X.C06160ax.A04(r0, r1)     // Catch:{ all -> 0x04c4 }
            java.lang.String r5 = r0.A02()     // Catch:{ all -> 0x04c4 }
            java.lang.String[] r6 = r0.A04()     // Catch:{ all -> 0x04c4 }
        L_0x0029:
            r1 = r53
            X.0Tq r0 = r1.A05     // Catch:{ all -> 0x04c4 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x04c4 }
            X.1aN r0 = (X.C25771aN) r0     // Catch:{ all -> 0x04c4 }
            android.database.sqlite.SQLiteDatabase r2 = r0.A06()     // Catch:{ all -> 0x04c4 }
            java.lang.String r3 = "thread_users"
            java.lang.String[] r4 = X.C28011e7.A07     // Catch:{ all -> 0x04c4 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r4 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x04c4 }
            com.google.common.collect.ImmutableList$Builder r53 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x04bf }
        L_0x0046:
            boolean r0 = r4.moveToNext()     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x04b1
            r0 = r50
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            com.facebook.user.model.UserKey r15 = com.facebook.user.model.UserKey.A02(r0)     // Catch:{ all -> 0x04bf }
            com.facebook.user.model.Name r14 = new com.facebook.user.model.Name     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "first_name"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r3 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "last_name"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "name"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            r14.<init>(r3, r2, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "username"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r54 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "profile_pic_square"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            if (r2 == 0) goto L_0x04a5
            X.0jD r0 = r1.A00     // Catch:{ all -> 0x04bf }
            com.fasterxml.jackson.databind.JsonNode r0 = r0.A02(r2)     // Catch:{ all -> 0x04bf }
            com.facebook.user.profilepic.PicSquare r13 = X.C12630pi.A03(r0)     // Catch:{ all -> 0x04bf }
        L_0x009d:
            java.lang.String r0 = "is_messenger_user"
            boolean r5 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "allow_admin_create_appointment"
            boolean r12 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_commerce"
            boolean r11 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_partial"
            boolean r10 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_blocked_by_viewer"
            boolean r52 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_message_blocked_by_viewer"
            boolean r51 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "can_viewer_message"
            boolean r49 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "user_rank"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            float r48 = r4.getFloat(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "profile_type"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r47 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "messaging_actor_type"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            if (r0 != 0) goto L_0x049f
            X.1aC r8 = X.C25661aC.UNSET     // Catch:{ all -> 0x04bf }
        L_0x00e9:
            java.lang.String r0 = "montage_thread_fbid"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r46 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_broadcast_recipient_holdout"
            boolean r45 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_message_ignored_by_viewer"
            boolean r44 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "favorite_color"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r43 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "work_info"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r42 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "commerce_page_type"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ IllegalArgumentException -> 0x012a }
            java.lang.String r2 = r4.getString(r0)     // Catch:{ IllegalArgumentException -> 0x012a }
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)     // Catch:{ IllegalArgumentException -> 0x012a }
            if (r0 != 0) goto L_0x0128
            java.lang.Integer r3 = X.AnonymousClass1BW.A00(r2)     // Catch:{ IllegalArgumentException -> 0x012a }
            goto L_0x012b
        L_0x0128:
            r3 = 0
            goto L_0x012b
        L_0x012a:
            r3 = 0
        L_0x012b:
            java.lang.String r0 = "commerce_page_settings"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            if (r2 == 0) goto L_0x0212
            java.lang.String r0 = "[]"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x04bf }
            if (r0 != 0) goto L_0x0212
            X.0jD r0 = r1.A00     // Catch:{ all -> 0x04bf }
            com.fasterxml.jackson.databind.JsonNode r7 = r0.A02(r2)     // Catch:{ all -> 0x04bf }
            com.google.common.collect.ImmutableList$Builder r2 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x04bf }
            java.lang.String r6 = "commerce_faq_enabled"
            boolean r0 = r7.has(r6)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x0160
            com.fasterxml.jackson.databind.JsonNode r0 = r7.get(r6)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x0160
            X.1wW r0 = X.C37891wW.COMMERCE_FAQ_ENABLED     // Catch:{ all -> 0x04bf }
            r2.add(r0)     // Catch:{ all -> 0x04bf }
        L_0x0160:
            java.lang.String r6 = "in_messenger_shopping_enabled"
            boolean r0 = r7.has(r6)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x0177
            com.fasterxml.jackson.databind.JsonNode r0 = r7.get(r6)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x0177
            X.1wW r0 = X.C37891wW.IN_MESSENGER_SHOPPING_ENABLED     // Catch:{ all -> 0x04bf }
            r2.add(r0)     // Catch:{ all -> 0x04bf }
        L_0x0177:
            java.lang.String r6 = "commerce_nux_enabled"
            boolean r0 = r7.has(r6)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x018e
            com.fasterxml.jackson.databind.JsonNode r0 = r7.get(r6)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x018e
            X.1wW r0 = X.C37891wW.COMMERCE_NUX_ENABLED     // Catch:{ all -> 0x04bf }
            r2.add(r0)     // Catch:{ all -> 0x04bf }
        L_0x018e:
            java.lang.String r6 = "structured_menu_enabled"
            boolean r0 = r7.has(r6)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01a5
            com.fasterxml.jackson.databind.JsonNode r0 = r7.get(r6)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01a5
            X.1wW r0 = X.C37891wW.STRUCTURED_MENU_ENABLED     // Catch:{ all -> 0x04bf }
            r2.add(r0)     // Catch:{ all -> 0x04bf }
        L_0x01a5:
            java.lang.String r6 = "user_control_topic_manage_enabled"
            boolean r0 = r7.has(r6)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01bc
            com.fasterxml.jackson.databind.JsonNode r0 = r7.get(r6)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01bc
            X.1wW r0 = X.C37891wW.USER_CONTROL_TOPIC_MANAGE_ENABLED     // Catch:{ all -> 0x04bf }
            r2.add(r0)     // Catch:{ all -> 0x04bf }
        L_0x01bc:
            java.lang.String r6 = "null_state_cta_button_always_enabled"
            boolean r0 = r7.has(r6)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01d3
            com.fasterxml.jackson.databind.JsonNode r0 = r7.get(r6)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01d3
            X.1wW r0 = X.C37891wW.NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED     // Catch:{ all -> 0x04bf }
            r2.add(r0)     // Catch:{ all -> 0x04bf }
        L_0x01d3:
            java.lang.String r6 = "composer_input_disabled"
            boolean r0 = r7.has(r6)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01ea
            com.fasterxml.jackson.databind.JsonNode r0 = r7.get(r6)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x01ea
            X.1wW r0 = X.C37891wW.COMPOSER_INPUT_DISABLED     // Catch:{ all -> 0x04bf }
            r2.add(r0)     // Catch:{ all -> 0x04bf }
        L_0x01ea:
            com.google.common.collect.ImmutableList r41 = r2.build()     // Catch:{ all -> 0x04bf }
        L_0x01ee:
            java.lang.String r0 = "user_call_to_actions"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            if (r0 != 0) goto L_0x020d
            com.google.common.collect.ImmutableList r40 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x04bf }
        L_0x01fc:
            java.lang.String r0 = "extension_properties"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x0217
            goto L_0x0215
        L_0x020d:
            com.google.common.collect.ImmutableList r40 = X.C29941hE.A02(r0)     // Catch:{ all -> 0x04bf }
            goto L_0x01fc
        L_0x0212:
            com.google.common.collect.ImmutableList r41 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x04bf }
            goto L_0x01ee
        L_0x0215:
            r9 = 0
            goto L_0x0254
        L_0x0217:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)     // Catch:{ all -> 0x04bf }
            r9 = 0
            if (r0 != 0) goto L_0x0254
            X.0jE r0 = X.AnonymousClass0jE.getInstance()     // Catch:{ Exception -> 0x0254 }
            com.fasterxml.jackson.databind.JsonNode r6 = r0.readTree(r2)     // Catch:{ Exception -> 0x0254 }
            X.37P r2 = new X.37P     // Catch:{ all -> 0x04bf }
            r2.<init>()     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "resume_url"
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ all -> 0x04bf }
            r2.A02 = r0     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "resume_text"
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ all -> 0x04bf }
            r2.A01 = r0     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "payment_policy"
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ all -> 0x04bf }
            r2.A00 = r0     // Catch:{ all -> 0x04bf }
            com.facebook.messaging.business.messengerextensions.model.MessengerExtensionProperties r9 = new com.facebook.messaging.business.messengerextensions.model.MessengerExtensionProperties     // Catch:{ all -> 0x04bf }
            r9.<init>(r2)     // Catch:{ all -> 0x04bf }
        L_0x0254:
            java.lang.String r0 = "is_friend"
            boolean r39 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "last_fetch_time"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            long r26 = r4.getLong(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_messenger_bot"
            boolean r38 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_vc_endpoint"
            boolean r37 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_messenger_promotion_blocked_by_viewer"
            boolean r36 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_messenger_platform_bot"
            boolean r35 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "does_accept_user_feedback"
            boolean r34 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_messenger_welcome_page_cta_enabled"
            boolean r33 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_conversation_ice_breaker_enabled"
            boolean r32 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "viewer_connection_status"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            if (r0 != 0) goto L_0x02cb
            java.lang.Integer r31 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x04bf }
        L_0x029c:
            java.lang.String r0 = "is_memorialized"
            boolean r30 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "nested_menu_call_to_actions"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            if (r2 != 0) goto L_0x02c4
            com.google.common.collect.ImmutableList r29 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x04bf }
        L_0x02b0:
            java.lang.String r0 = "instant_game_channel"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x02ef
            X.0pl r7 = r1.A03     // Catch:{ all -> 0x04bf }
            r6 = r0
            r28 = 0
            if (r0 == 0) goto L_0x02f1
            goto L_0x02d3
        L_0x02c4:
            X.0pj r0 = r1.A02     // Catch:{ all -> 0x04bf }
            com.google.common.collect.ImmutableList r29 = r0.A01(r2)     // Catch:{ all -> 0x04bf }
            goto L_0x02b0
        L_0x02cb:
            java.lang.Integer r31 = X.C29751gv.A00(r0)     // Catch:{ IllegalArgumentException -> 0x02d0 }
            goto L_0x029c
        L_0x02d0:
            java.lang.Integer r31 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x04bf }
            goto L_0x029c
        L_0x02d3:
            X.0jE r2 = r7.A01     // Catch:{ IOException -> 0x02e1 }
            X.2cM r0 = new X.2cM     // Catch:{ IOException -> 0x02e1 }
            r0.<init>(r7)     // Catch:{ IOException -> 0x02e1 }
            java.lang.Object r0 = r2.readValue(r6, r0)     // Catch:{ IOException -> 0x02e1 }
            com.facebook.messaging.games.model.InstantGameChannel r0 = (com.facebook.messaging.games.model.InstantGameChannel) r0     // Catch:{ IOException -> 0x02e1 }
            goto L_0x02ec
        L_0x02e1:
            X.09P r6 = r7.A00     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = "DbInstantGameChannelSerialization"
            java.lang.String r0 = "Failed to deserialize InstantGameChannel"
            r6.CGS(r2, r0)     // Catch:{ all -> 0x04bf }
            r0 = r28
        L_0x02ec:
            r28 = r0
            goto L_0x02f1
        L_0x02ef:
            r28 = 0
        L_0x02f1:
            java.lang.String r0 = "managing_ps"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            X.0jD r6 = r1.A00     // Catch:{ all -> 0x04bf }
            r2 = r0
            if (r0 == 0) goto L_0x0333
            java.lang.String r0 = "[]"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x04bf }
            if (r0 != 0) goto L_0x0333
            com.fasterxml.jackson.databind.JsonNode r0 = r6.A02(r2)     // Catch:{ all -> 0x04bf }
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x04bf }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x04bf }
        L_0x0314:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x0336
            java.lang.Object r2 = r7.next()     // Catch:{ all -> 0x04bf }
            com.fasterxml.jackson.databind.JsonNode r2 = (com.fasterxml.jackson.databind.JsonNode) r2     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "managing_parent_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r2.get(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ all -> 0x04bf }
            com.facebook.user.model.ManagingParent r0 = new com.facebook.user.model.ManagingParent     // Catch:{ all -> 0x04bf }
            r0.<init>(r2)     // Catch:{ all -> 0x04bf }
            r6.add(r0)     // Catch:{ all -> 0x04bf }
            goto L_0x0314
        L_0x0333:
            com.google.common.collect.ImmutableList r16 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x04bf }
            goto L_0x033a
        L_0x0336:
            com.google.common.collect.ImmutableList r16 = r6.build()     // Catch:{ all -> 0x04bf }
        L_0x033a:
            java.lang.String r0 = "is_aloha_proxy_confirmed"
            boolean r25 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "aloha_proxy_user_owners"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            com.google.common.collect.ImmutableList r24 = X.C12630pi.A05(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "aloha_proxy_users_owned"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            r6 = r0
            if (r0 != 0) goto L_0x03f1
            com.google.common.collect.ImmutableList r17 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x04bf }
        L_0x035d:
            java.lang.String r0 = "is_viewer_subscribed_to_message_updates"
            boolean r23 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_work_user"
            boolean r22 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_viewer_coworker"
            boolean r21 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_viewer_managing_parent"
            boolean r20 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_business_active"
            boolean r19 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "is_verified"
            boolean r18 = A02(r4, r0)     // Catch:{ all -> 0x04bf }
            com.facebook.user.profilepic.ProfilePicUriWithFilePath r7 = new com.facebook.user.profilepic.ProfilePicUriWithFilePath     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "inbox_profile_pic_uri"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r2 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "inbox_profile_pic_file_path"
            int r0 = r4.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x04bf }
            r7.<init>(r2, r0)     // Catch:{ all -> 0x04bf }
            java.lang.Boolean r0 = r1.A04     // Catch:{ all -> 0x04bf }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x03ef
            com.facebook.user.model.WorkUserInfo r6 = X.C12630pi.A00(r42)     // Catch:{ all -> 0x04bf }
        L_0x03a6:
            X.0cv r2 = new X.0cv     // Catch:{ all -> 0x04bf }
            r2.<init>()     // Catch:{ all -> 0x04bf }
            X.1aB r0 = r15.type     // Catch:{ all -> 0x04bf }
            java.lang.String r15 = r15.id     // Catch:{ all -> 0x04bf }
            r2.A03(r0, r15)     // Catch:{ all -> 0x04bf }
            r2.A0J = r14     // Catch:{ all -> 0x04bf }
            r0 = r54
            r2.A0x = r0     // Catch:{ all -> 0x04bf }
            r2.A1B = r5     // Catch:{ all -> 0x04bf }
            r2.A0P = r13     // Catch:{ all -> 0x04bf }
            r0 = r47
            r2.A13 = r0     // Catch:{ all -> 0x04bf }
            X.1aC r0 = X.C25661aC.A00(r47)     // Catch:{ all -> 0x04bf }
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ all -> 0x04bf }
            r2.A0G = r0     // Catch:{ all -> 0x04bf }
            r5 = r2
            r2.A18 = r12     // Catch:{ all -> 0x04bf }
            r2.A19 = r11     // Catch:{ all -> 0x04bf }
            r2.A1i = r10     // Catch:{ all -> 0x04bf }
            r0 = r48
            r2.A01 = r0     // Catch:{ all -> 0x04bf }
            r0 = r51
            r2.A1a = r0     // Catch:{ all -> 0x04bf }
            r0 = r52
            r2.A1N = r0     // Catch:{ all -> 0x04bf }
            r2.A0a = r3     // Catch:{ all -> 0x04bf }
            r0 = r49
            r2.A1F = r0     // Catch:{ all -> 0x04bf }
            r0 = r41
            r2.A0R = r0     // Catch:{ all -> 0x04bf }
            r0 = r39
            r2.A1W = r0     // Catch:{ all -> 0x04bf }
            r2 = r26
            r5.A0A = r2     // Catch:{ all -> 0x04bf }
            goto L_0x0412
        L_0x03ef:
            r6 = 0
            goto L_0x03a6
        L_0x03f1:
            java.lang.String r0 = "[]"
            boolean r0 = r6.equals(r0)     // Catch:{ all -> 0x04bf }
            if (r0 == 0) goto L_0x03fd
            com.google.common.collect.ImmutableList r17 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x04bf }
            goto L_0x035d
        L_0x03fd:
            X.0jE r2 = X.AnonymousClass0jE.getInstance()     // Catch:{ all -> 0x04bf }
            X.37R r0 = new X.37R     // Catch:{ IOException -> 0x04a8 }
            r0.<init>()     // Catch:{ IOException -> 0x04a8 }
            java.lang.Object r17 = r2.readValue(r6, r0)     // Catch:{ IOException -> 0x04a8 }
            r0 = r17
            com.google.common.collect.ImmutableList r0 = (com.google.common.collect.ImmutableList) r0     // Catch:{ IOException -> 0x04a8 }
            r17 = r0
            goto L_0x035d
        L_0x0412:
            if (r46 == 0) goto L_0x0415
            goto L_0x0418
        L_0x0415:
            r2 = 0
            goto L_0x041c
        L_0x0418:
            long r2 = java.lang.Long.parseLong(r46)     // Catch:{ all -> 0x04bf }
        L_0x041c:
            r5.A0C = r2     // Catch:{ all -> 0x04bf }
            r0 = r45
            r5.A1O = r0     // Catch:{ all -> 0x04bf }
            r0 = r38
            r5.A1c = r0     // Catch:{ all -> 0x04bf }
            r0 = r37
            r5.A1j = r0     // Catch:{ all -> 0x04bf }
            r0 = r36
            r5.A1f = r0     // Catch:{ all -> 0x04bf }
            r0 = r35
            r5.A1e = r0     // Catch:{ all -> 0x04bf }
            r0 = r40
            r5.A0Y = r0     // Catch:{ all -> 0x04bf }
            r0 = r34
            r5.A1H = r0     // Catch:{ all -> 0x04bf }
            r5.A0H = r9     // Catch:{ all -> 0x04bf }
            com.google.common.base.Preconditions.checkNotNull(r31)     // Catch:{ all -> 0x04bf }
            r0 = r31
            r5.A0e = r0     // Catch:{ all -> 0x04bf }
            r0 = r30
            r5.A1Z = r0     // Catch:{ all -> 0x04bf }
            r0 = r29
            r5.A0X = r0     // Catch:{ all -> 0x04bf }
            r0 = r16
            r5.A0Z = r0     // Catch:{ all -> 0x04bf }
            r0 = r25
            r5.A1M = r0     // Catch:{ all -> 0x04bf }
            r0 = r24
            r5.A0S = r0     // Catch:{ all -> 0x04bf }
            r0 = r28
            r5.A0I = r0     // Catch:{ all -> 0x04bf }
            r0 = r33
            r5.A1g = r0     // Catch:{ all -> 0x04bf }
            r0 = r44
            r5.A1b = r0     // Catch:{ all -> 0x04bf }
            r0 = r17
            r5.A0T = r0     // Catch:{ all -> 0x04bf }
            r0 = r23
            r5.A1m = r0     // Catch:{ all -> 0x04bf }
            r0 = r32
            r5.A1Q = r0     // Catch:{ all -> 0x04bf }
            r0 = r22
            r5.A1C = r0     // Catch:{ all -> 0x04bf }
            r0 = r21
            r5.A1R = r0     // Catch:{ all -> 0x04bf }
            r0 = r43
            r5.A0q = r0     // Catch:{ all -> 0x04bf }
            r0 = r20
            r5.A1l = r0     // Catch:{ all -> 0x04bf }
            r5.A0Q = r7     // Catch:{ all -> 0x04bf }
            r5.A0O = r6     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "DbFetchThreadUsersHandler"
            r5.A0p = r0     // Catch:{ all -> 0x04bf }
            com.google.common.base.Preconditions.checkNotNull(r8)     // Catch:{ all -> 0x04bf }
            r5.A0G = r8     // Catch:{ all -> 0x04bf }
            r0 = r19
            r5.A1P = r0     // Catch:{ all -> 0x04bf }
            r0 = r18
            r5.A1k = r0     // Catch:{ all -> 0x04bf }
            com.facebook.user.model.User r2 = r5.A02()     // Catch:{ all -> 0x04bf }
            r0 = r53
            r0.add(r2)     // Catch:{ all -> 0x04bf }
            goto L_0x0046
        L_0x049f:
            X.1aC r8 = X.C25661aC.valueOf(r0)     // Catch:{ all -> 0x04bf }
            goto L_0x00e9
        L_0x04a5:
            r13 = 0
            goto L_0x009d
        L_0x04a8:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x04bf }
            java.lang.String r0 = "Unexpected serialization exception"
            r1.<init>(r0, r2)     // Catch:{ all -> 0x04bf }
            throw r1     // Catch:{ all -> 0x04bf }
        L_0x04b1:
            com.google.common.collect.ImmutableList r1 = r53.build()     // Catch:{ all -> 0x04bf }
            r4.close()     // Catch:{ all -> 0x04c4 }
            r0 = -1124246074(0xffffffffbcfd5dc6, float:-0.030928504)
            X.C005505z.A00(r0)
            return r1
        L_0x04bf:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x04c4 }
            throw r0     // Catch:{ all -> 0x04c4 }
        L_0x04c4:
            r1 = move-exception
            r0 = -2066602813(0xffffffff84d224c3, float:-4.9404523E-36)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28011e7.A01(X.1e7, java.util.Collection):com.google.common.collect.ImmutableList");
    }

    private C28011e7(AnonymousClass06B r1, C04310Tq r2, C12640pj r3, C12660pl r4, AnonymousClass0jD r5, Boolean bool) {
        this.A01 = r1;
        this.A05 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r5;
        this.A04 = bool;
    }

    private static boolean A02(Cursor cursor, String str) {
        if (cursor.getInt(cursor.getColumnIndexOrThrow(str)) != 0) {
            return true;
        }
        return false;
    }

    public User A03(UserKey userKey) {
        ImmutableList A042 = A04(ImmutableSet.A04(userKey));
        if (A042.size() != 1) {
            return null;
        }
        User user = (User) A042.get(0);
        if (this.A01.now() - user.A00 <= 86400000) {
            return user;
        }
        userKey.toString();
        return null;
    }

    public ImmutableList A04(Collection collection) {
        if (collection.isEmpty()) {
            return RegularImmutableList.A02;
        }
        return A01(this, collection);
    }
}
