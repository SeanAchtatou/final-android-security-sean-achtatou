package X;

/* renamed from: X.079  reason: invalid class name */
public final class AnonymousClass079 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.base.lwperf.perfstats.FsStats$FsStatsReporter$1";
    public final /* synthetic */ AnonymousClass078 A00;

    public AnonymousClass079(AnonymousClass078 r1) {
        this.A00 = r1;
    }

    public void run() {
        this.A00.A03.set(C012109i.A01().A05(AnonymousClass07B.A00) >> 10);
        this.A00.A00.open();
        this.A00.A02.set(false);
    }
}
