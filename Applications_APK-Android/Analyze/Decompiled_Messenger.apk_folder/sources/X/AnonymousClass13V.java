package X;

import android.net.Uri;

/* renamed from: X.13V  reason: invalid class name */
public final class AnonymousClass13V implements AnonymousClass13Q {
    private final String A00;
    private final C04310Tq A01;

    public String AdC() {
        return null;
    }

    public String Ad9() {
        return (String) this.A01.get();
    }

    public Uri.Builder AdB() {
        return Uri.parse(AnonymousClass08S.A0J("https://api.", this.A00)).buildUpon();
    }

    public Uri.Builder AoB() {
        return Uri.parse(AnonymousClass08S.A0J("https://graph.", this.A00)).buildUpon();
    }

    public Uri.Builder AoC() {
        return Uri.parse(AnonymousClass08S.A0J("https://graph-video.", this.A00)).buildUpon();
    }

    public Uri.Builder AoJ() {
        return Uri.parse(AnonymousClass08S.A0J("http://h.", this.A00)).buildUpon();
    }

    public Uri.Builder Auq() {
        return Uri.parse(AnonymousClass08S.A0J("https://m.", this.A00)).buildUpon();
    }

    public Uri.Builder B2G() {
        return Uri.parse(AnonymousClass08S.A0J("https://graph.secure.", this.A00)).buildUpon();
    }

    public Uri.Builder B2H() {
        return Uri.parse(AnonymousClass08S.A0J("https://secure.", this.A00)).buildUpon();
    }

    public String getDomain() {
        return this.A00;
    }

    public AnonymousClass13V(String str, C04310Tq r2) {
        this.A00 = str;
        this.A01 = r2;
    }
}
