package X;

import com.facebook.acra.constants.ErrorReportingConstants;
import org.json.JSONObject;

/* renamed from: X.1Oi  reason: invalid class name and case insensitive filesystem */
public class C23131Oi {
    public long A00;
    public Integer A01;
    public Integer A02;
    public String A03;
    public String A04;
    public String A05;

    public static void A00(JSONObject jSONObject, C23131Oi r3) {
        Integer num;
        Integer num2;
        r3.A04 = jSONObject.optString("name", null);
        r3.A03 = jSONObject.optString("cctype", null);
        r3.A05 = jSONObject.optString("policy_id", null);
        if (jSONObject.has("version")) {
            num = Integer.valueOf(jSONObject.getInt("version"));
        } else {
            num = null;
        }
        r3.A02 = num;
        if (jSONObject.has(ErrorReportingConstants.SOFT_ERROR_OCCURRENCE_COUNT)) {
            num2 = Integer.valueOf(jSONObject.getInt(ErrorReportingConstants.SOFT_ERROR_OCCURRENCE_COUNT));
        } else {
            num2 = null;
        }
        r3.A01 = num2;
        r3.A00 = jSONObject.optLong("timestamp");
    }
}
