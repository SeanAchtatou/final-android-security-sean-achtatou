package X;

import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.1oI  reason: invalid class name and case insensitive filesystem */
public final class C33881oI {
    public ThreadKey A00;
    public ImmutableList A01 = RegularImmutableList.A02;
    public boolean A02;
    public boolean A03;
    public boolean A04 = true;

    public MessagesCollection A00() {
        Preconditions.checkNotNull(this.A01);
        return new MessagesCollection(this);
    }

    public void A01(ImmutableList immutableList) {
        Preconditions.checkNotNull(immutableList);
        this.A01 = immutableList;
    }
}
