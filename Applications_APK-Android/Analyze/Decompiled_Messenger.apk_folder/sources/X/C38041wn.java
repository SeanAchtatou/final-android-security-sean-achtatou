package X;

import android.util.Log;

/* renamed from: X.1wn  reason: invalid class name and case insensitive filesystem */
public final class C38041wn implements C21460AFb {
    public final C21489AGj[] A00;
    private final int[] A01;

    public C21451AEs CJE(int i, int i2) {
        int i3 = 0;
        while (true) {
            int[] iArr = this.A01;
            if (i3 >= iArr.length) {
                Log.e("BaseMediaChunkOutput", AnonymousClass08S.A09("Unmatched track of type: ", i2));
                return new C21450AEr();
            } else if (i2 == iArr[i3]) {
                return this.A00[i3];
            } else {
                i3++;
            }
        }
    }

    public void A00(long j) {
        for (C21489AGj aGj : this.A00) {
            if (!(aGj == null || aGj.A00 == j)) {
                aGj.A00 = j;
                aGj.A07 = true;
            }
        }
    }

    public C38041wn(int[] iArr, C21489AGj[] aGjArr) {
        this.A01 = iArr;
        this.A00 = aGjArr;
    }
}
