package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.http.interfaces.RequestPriority;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Qc  reason: invalid class name */
public final class AnonymousClass0Qc implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.profilo.upload.BackgroundUploadServiceImpl";
    private AnonymousClass0UN A00;
    public final Set A01 = new HashSet(10);
    public final ExecutorService A02;
    private final C10740km A03;
    private final C03810Ps A04;
    private final C03840Pw A05;
    private final ExecutorService A06;

    public static synchronized void A02(AnonymousClass0Qc r5, List list, C003804w r7, boolean z) {
        boolean contains;
        synchronized (r5) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                File file = (File) it.next();
                synchronized (r5.A01) {
                    contains = r5.A01.contains(file);
                }
                if (!contains) {
                    if (file.exists()) {
                        r5.A03(file);
                        AnonymousClass07A.A04(r5.A06, new C03770Pl(r5, file, r7, z), 2022060906);
                    }
                }
            }
        }
    }

    public static final C03790Po A00(AnonymousClass1XY r1) {
        return new C03790Po(r1);
    }

    public static void A01(AnonymousClass0Qc r7, File file, C003804w r9, boolean z) {
        if (z || r7.A05.A07()) {
            C47692Xl r5 = new C47692Xl();
            r5.A01 = RequestPriority.A03;
            try {
                if (((Boolean) r7.A03.A07(r7.A04, file, r5, CallerContext.A04(AnonymousClass0Qc.class))).booleanValue()) {
                    if (!z) {
                        C03840Pw.A06(r7.A05, -file.length());
                    }
                    if (r9 != null) {
                        AnonymousClass07A.A04(r7.A02, new AnonymousClass0Pm(r9, file), 1672186791);
                        return;
                    }
                    return;
                }
                C010708t.A0P("BackgroundUploadServiceImpl", "Upload failed for trace %s", file.getName());
                if (r9 != null) {
                    AnonymousClass07A.A04(r7.A02, new C03780Pn(r9, file), -1163001501);
                }
            } catch (Exception e) {
                C010708t.A0V("BackgroundUploadServiceImpl", e, "Upload failed for trace %s", file.getName());
                if (r9 != null) {
                    AnonymousClass07A.A04(r7.A02, new C03780Pn(r9, file), -1163001501);
                }
            }
        }
    }

    public void A04(C004405h r8) {
        C03840Pw r4 = this.A05;
        r4.A01 = r8;
        boolean z = false;
        if (C03840Pw.A01(r4, Long.MIN_VALUE) == Long.MIN_VALUE) {
            z = true;
        }
        if (z) {
            C03840Pw.A04(r4, (TimeUnit.MILLISECONDS.toSeconds(r4.A00.now()) - r4.A01.B7n()) - 1);
        }
        C03840Pw.A05(r4, C03840Pw.A00(r4));
        C03840Pw.A06(r4, 0);
    }

    public AnonymousClass0Qc(AnonymousClass1XY r3, C03840Pw r4, ExecutorService executorService, ExecutorService executorService2, C10740km r7, C03810Ps r8) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A06 = executorService2;
        this.A05 = r4;
        this.A02 = executorService;
        this.A03 = r7;
        this.A04 = r8;
    }

    private void A03(File file) {
        String str;
        String str2;
        String absolutePath = file.getAbsolutePath();
        if (!file.getName().endsWith(".log") || !absolutePath.contains("/upload")) {
            AnonymousClass1XX.A03(AnonymousClass1Y3.A6S, this.A00);
            StringBuilder sb = new StringBuilder();
            AnonymousClass05m r0 = C004505k.A00().A03;
            File file2 = r0.A06;
            File file3 = r0.A03;
            String str3 = null;
            try {
                str2 = file.getCanonicalPath();
                try {
                    str = file2.getCanonicalPath();
                    try {
                        str3 = file3.getCanonicalPath();
                    } catch (IOException unused) {
                    }
                } catch (IOException unused2) {
                    str = null;
                }
            } catch (IOException unused3) {
                str2 = null;
                str = null;
            }
            sb.append("Suspicious upload (absolute): ");
            sb.append(absolutePath);
            sb.append(10);
            if (str2 != null) {
                sb.append("Suspicious upload (canonical): ");
                sb.append(str2);
                sb.append(10);
            }
            sb.append("Is trimmable: ");
            sb.append(AnonymousClass05m.A09.accept(file2, file.getName()));
            sb.append(10);
            sb.append("Is untrimmable: ");
            sb.append(AnonymousClass05m.A0A.accept(file2, file.getName()));
            sb.append(10);
            sb.append("Upload directory (absolute): ");
            sb.append(file2.getAbsolutePath());
            sb.append(10);
            if (str != null) {
                sb.append("Upload directory (canonical): ");
                sb.append(str);
                sb.append(10);
            }
            sb.append("Base trace folder (absolute): ");
            sb.append(file3.getAbsolutePath());
            sb.append(10);
            if (str3 != null) {
                sb.append("Base trace folder (canonical): ");
                sb.append(str3);
                sb.append(10);
            }
            sb.append("upload/ files: ");
            File[] listFiles = file2.listFiles();
            if (listFiles != null) {
                for (File absolutePath2 : listFiles) {
                    sb.append(absolutePath2.getAbsolutePath());
                    sb.append(',');
                    sb.append(' ');
                }
            } else {
                sb.append("<none>");
            }
            ((AnonymousClass09P) AnonymousClass1XX.A03(AnonymousClass1Y3.Amr, this.A00)).CGU("profilo_upload_suspicious_file_v3", sb.toString(), new Throwable(), 1);
        }
    }
}
