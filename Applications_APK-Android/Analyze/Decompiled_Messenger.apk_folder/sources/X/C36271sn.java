package X;

import com.facebook.proxygen.TraceFieldType;
import java.io.Serializable;
import java.util.Arrays;

/* renamed from: X.1sn  reason: invalid class name and case insensitive filesystem */
public final class C36271sn implements C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("hostIpAddress", (byte) 11, 3);
    private static final C36241sk A01 = new C36241sk("hostName", (byte) 11, 1);
    private static final C36241sk A02 = new C36241sk(TraceFieldType.Port, (byte) 8, 2);
    private static final C36231sj A03 = new C36231sj("PhpTierOverrideHostPort");
    public final String hostIpAddress;
    public final String hostName;
    public final Integer port;

    public boolean equals(Object obj) {
        C36271sn r5;
        if (obj == null || !(obj instanceof C36271sn) || (r5 = (C36271sn) obj) == null) {
            return false;
        }
        if (this == r5) {
            return true;
        }
        String str = this.hostName;
        boolean z = false;
        if (str != null) {
            z = true;
        }
        String str2 = r5.hostName;
        boolean z2 = false;
        if (str2 != null) {
            z2 = true;
        }
        if ((z || z2) && (!z || !z2 || !str.equals(str2))) {
            return false;
        }
        Integer num = this.port;
        boolean z3 = false;
        if (num != null) {
            z3 = true;
        }
        Integer num2 = r5.port;
        boolean z4 = false;
        if (num2 != null) {
            z4 = true;
        }
        if ((z3 || z4) && (!z3 || !z4 || !num.equals(num2))) {
            return false;
        }
        String str3 = this.hostIpAddress;
        boolean z5 = false;
        if (str3 != null) {
            z5 = true;
        }
        String str4 = r5.hostIpAddress;
        boolean z6 = false;
        if (str4 != null) {
            z6 = true;
        }
        if (!z5 && !z6) {
            return true;
        }
        if (!z5 || !z6 || !str3.equals(str4)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    public void CNX(C35781ro r3) {
        r3.A0i(A03);
        String str = this.hostName;
        if (str != null) {
            boolean z = false;
            if (str != null) {
                z = true;
            }
            if (z) {
                r3.A0e(A01);
                r3.A0j(this.hostName);
                r3.A0S();
            }
        }
        Integer num = this.port;
        if (num != null) {
            boolean z2 = false;
            if (num != null) {
                z2 = true;
            }
            if (z2) {
                r3.A0e(A02);
                r3.A0c(this.port.intValue());
                r3.A0S();
            }
        }
        String str2 = this.hostIpAddress;
        if (str2 != null) {
            boolean z3 = false;
            if (str2 != null) {
                z3 = true;
            }
            if (z3) {
                r3.A0e(A00);
                r3.A0j(this.hostIpAddress);
                r3.A0S();
            }
        }
        r3.A0T();
        r3.A0X();
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{this.hostName, this.port, this.hostIpAddress});
    }

    public C36271sn(String str, Integer num, String str2) {
        this.hostName = str;
        this.port = num;
        this.hostIpAddress = str2;
    }

    public String CJ9(int i, boolean z) {
        return B36.A06(this, i, z);
    }
}
