package X;

import android.content.Context;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Executor;

/* renamed from: X.07O  reason: invalid class name */
public final class AnonymousClass07O {
    public Context A00 = null;
    public File A01 = null;
    public String A02 = null;
    public Executor A03 = AnonymousClass07P.A06;
    public final ArrayList A04 = new ArrayList();

    public AnonymousClass07P A00() {
        C000300h.A01(this.A01);
        for (int i = 0; i < this.A04.size(); i++) {
            AnonymousClass07R r3 = (AnonymousClass07R) this.A04.get(i);
            r3.A00 = new File(this.A01, r3.A01);
        }
        return new AnonymousClass07P(this);
    }
}
