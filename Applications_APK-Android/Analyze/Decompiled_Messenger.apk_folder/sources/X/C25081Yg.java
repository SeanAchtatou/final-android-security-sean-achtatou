package X;

import android.os.Bundle;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;

/* renamed from: X.1Yg  reason: invalid class name and case insensitive filesystem */
public final class C25081Yg implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appjobs.scheduler.impl.AppJobsSchedulerImpl$1";
    public final /* synthetic */ long A00;
    public final /* synthetic */ Bundle A01;
    public final /* synthetic */ AnonymousClass1YR A02;
    public final /* synthetic */ String A03;

    public C25081Yg(AnonymousClass1YR r1, String str, long j, Bundle bundle) {
        this.A02 = r1;
        this.A03 = str;
        this.A00 = j;
        this.A01 = bundle;
    }

    public void run() {
        int i;
        AnonymousClass1YR r1 = this.A02;
        String str = this.A03;
        long j = this.A00;
        Bundle bundle = this.A01;
        r1.A00.A05((C25221Yu) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A2T, r1.A02), str, C25211Yt.A01(str), j, bundle);
        if ("app_foregrounded".equals(str) && r1.A03.compareAndSet(true, false)) {
            r1.A01.A05((C25221Yu) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A2T, r1.A02), "first_app_foregrounded", C25211Yt.A00("first_app_foregrounded"), j, bundle);
        }
        r1.A01.A05((C25221Yu) AnonymousClass1XX.A02(4, AnonymousClass1Y3.A2T, r1.A02), str, C25211Yt.A00(str), j, bundle);
        AnonymousClass1YR r4 = this.A02;
        String str2 = this.A03;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ap7, r4.A02)).A01("appjobs_android_trigger_fired"), 55);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("trigger_name", str2);
            AnonymousClass1YS r12 = r4.A01;
            if (!r12.A04) {
                i = r12.A03.get();
            } else {
                i = -1;
            }
            uSLEBaseShape0S0000000.A0B("num_fg_bg_triggers_in_queue", Integer.valueOf(i));
            uSLEBaseShape0S0000000.A06();
        }
    }
}
