package X;

/* renamed from: X.1sf  reason: invalid class name and case insensitive filesystem */
public enum C36191sf implements C36201sg {
    DEFAULT(1),
    G2(2),
    G3(3),
    G4(4),
    G5(5),
    LTE(6),
    WIFI(7);
    
    private final int value;

    public int getValue() {
        return this.value;
    }

    private C36191sf(int i) {
        this.value = i;
    }
}
