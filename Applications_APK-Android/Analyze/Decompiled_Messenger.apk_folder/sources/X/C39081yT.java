package X;

/* renamed from: X.1yT  reason: invalid class name and case insensitive filesystem */
public final class C39081yT extends Exception {
    public C39081yT() {
        super("Couldn't get contacts collection.");
    }

    public C39081yT(Throwable th) {
        super("Couldn't get contacts collection.", th);
    }
}
