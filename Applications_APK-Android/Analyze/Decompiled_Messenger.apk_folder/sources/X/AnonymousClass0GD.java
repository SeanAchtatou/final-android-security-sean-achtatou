package X;

import android.os.RemoteException;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/* renamed from: X.0GD  reason: invalid class name */
public final class AnonymousClass0GD implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector$3";
    public final /* synthetic */ SkywalkerSubscriptionConnector A00;
    public final /* synthetic */ String A01;

    public AnonymousClass0GD(SkywalkerSubscriptionConnector skywalkerSubscriptionConnector, String str) {
        this.A00 = skywalkerSubscriptionConnector;
        this.A01 = str;
    }

    public void run() {
        ArrayNode createArrayNode = this.A00.A02.createArrayNode();
        createArrayNode.add(this.A01);
        ObjectNode A03 = SkywalkerSubscriptionConnector.A02(null, createArrayNode, null);
        C36991uR BvK = this.A00.A01.BvK();
        try {
            BvK.A07("/pubsub", A03, LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT);
        } catch (RemoteException e) {
            C010708t.A08(SkywalkerSubscriptionConnector.A08, "Remote exception for unsubscribe", e);
        } catch (Throwable th) {
            BvK.A06();
            throw th;
        }
        BvK.A06();
        synchronized (this.A00) {
            if (this.A00.A04.containsKey(this.A01)) {
                this.A00.A04.remove(this.A01);
            }
            if (this.A00.A03.containsKey(this.A01)) {
                this.A00.A03.remove(this.A01);
            }
        }
    }
}
