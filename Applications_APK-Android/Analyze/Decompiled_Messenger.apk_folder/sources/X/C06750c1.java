package X;

import android.net.Uri;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;

/* renamed from: X.0c1  reason: invalid class name and case insensitive filesystem */
public abstract class C06750c1 {
    public static Comparator A02 = new C06740c0();
    public final int A00;
    public final Uri A01;

    public void A00() {
        if (this instanceof C25411Zn) {
            ((C25411Zn) this).A00 = false;
        } else if (this instanceof C04320Ts) {
            ((C04320Ts) this).A01.clear();
        } else if (!(this instanceof C06760c2)) {
            C06730bz r1 = (C06730bz) this;
            r1.A01.A02.clear();
            r1.A00 = false;
        } else {
            ((C06760c2) this).A00 = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if (X.C06100aq.A00(r1, r5).isPresent() != false) goto L_0x0024;
     */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.net.Uri r5, X.C55532oE r6) {
        /*
            r4 = this;
            boolean r0 = r4 instanceof X.C25411Zn
            if (r0 != 0) goto L_0x0066
            boolean r0 = r4 instanceof X.C04320Ts
            if (r0 != 0) goto L_0x0087
            boolean r0 = r4 instanceof X.C06760c2
            if (r0 != 0) goto L_0x004f
            r2 = r4
            X.0bz r2 = (X.C06730bz) r2
            X.0aq r1 = r2.A01
            android.net.Uri r0 = r1.A00
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x0024
            com.google.common.base.Optional r0 = X.C06100aq.A00(r1, r5)
            boolean r1 = r0.isPresent()
            r0 = 0
            if (r1 == 0) goto L_0x0025
        L_0x0024:
            r0 = 1
        L_0x0025:
            if (r0 == 0) goto L_0x0074
            X.0aq r1 = r2.A01
            com.google.common.base.Optional r2 = X.C06100aq.A00(r1, r5)
            boolean r0 = r2.isPresent()
            if (r0 == 0) goto L_0x004e
            java.util.Set r1 = r1.A02
            java.lang.Object r0 = r2.get()
            boolean r0 = r1.contains(r0)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            r6.A00 = r1
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x004e
        L_0x004b:
            r0 = 1
            r6.A01 = r0
        L_0x004e:
            return
        L_0x004f:
            r1 = r4
            X.0c2 r1 = (X.C06760c2) r1
            android.net.Uri r0 = X.C06980cP.A05
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x004e
            boolean r0 = r1.A00
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6.A00 = r0
            r0 = 1
            r6.A01 = r0
            return
        L_0x0066:
            r1 = r4
            X.1Zn r1 = (X.C25411Zn) r1
            android.net.Uri r0 = X.C06980cP.A08
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x004e
            boolean r1 = r1.A00
            goto L_0x007e
        L_0x0074:
            android.net.Uri r0 = X.C06980cP.A00
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x004e
            boolean r1 = r2.A00
        L_0x007e:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            r6.A00 = r0
            if (r1 == 0) goto L_0x004e
            goto L_0x004b
        L_0x0087:
            r3 = r4
            X.0Ts r3 = (X.C04320Ts) r3
            X.0ar r1 = r3.A00     // Catch:{ 1w4 -> 0x00c4 }
            java.lang.String r0 = r5.toString()     // Catch:{ 1w4 -> 0x00c4 }
            X.2oH r2 = r1.A02(r0)     // Catch:{ 1w4 -> 0x00c4 }
            if (r2 == 0) goto L_0x00c4
            r1 = 1
            java.lang.Object r0 = r2.A01     // Catch:{ 1w4 -> 0x00c4 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ 1w4 -> 0x00c4 }
            int r0 = r0.intValue()     // Catch:{ 1w4 -> 0x00c4 }
            if (r1 != r0) goto L_0x00c4
            android.os.Bundle r1 = r2.A00     // Catch:{ 1w4 -> 0x00c4 }
            java.lang.String r0 = "thread_id"
            java.lang.String r1 = r1.getString(r0)     // Catch:{ 1w4 -> 0x00c4 }
            java.util.Map r0 = r3.A01     // Catch:{ 1w4 -> 0x00c4 }
            java.lang.Object r1 = r0.get(r1)     // Catch:{ 1w4 -> 0x00c4 }
            java.util.Set r1 = (java.util.Set) r1     // Catch:{ 1w4 -> 0x00c4 }
            if (r1 == 0) goto L_0x00c4
            java.lang.Object r0 = r6.A00     // Catch:{ 1w4 -> 0x00c4 }
            if (r0 != 0) goto L_0x00bd
            java.util.HashSet r0 = X.C25011Xz.A03()     // Catch:{ 1w4 -> 0x00c4 }
            r6.A00 = r0     // Catch:{ 1w4 -> 0x00c4 }
        L_0x00bd:
            java.lang.Object r0 = r6.A00     // Catch:{ 1w4 -> 0x00c4 }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ 1w4 -> 0x00c4 }
            r0.addAll(r1)     // Catch:{ 1w4 -> 0x00c4 }
        L_0x00c4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06750c1.A01(android.net.Uri, X.2oE):void");
    }

    public void A02(Bundle bundle) {
        if (this instanceof C25411Zn) {
            ((C25411Zn) this).A00 = bundle.getBoolean(C06980cP.A08.getPath(), false);
        } else if (this instanceof C04320Ts) {
            C04320Ts r5 = (C04320Ts) this;
            r5.A01.clear();
            Bundle bundle2 = bundle.getBundle(r5.A01.getAuthority());
            for (String next : bundle2.keySet()) {
                r5.A01.put(next, C25011Xz.A04(bundle2.getStringArrayList(next)));
            }
        } else if (!(this instanceof C06760c2)) {
            C06730bz r3 = (C06730bz) this;
            C06100aq r4 = r3.A01;
            r4.A02.clear();
            ArrayList<String> stringArrayList = bundle.getStringArrayList(r4.A01);
            if (stringArrayList == null) {
                C010708t.A0B(C06100aq.class, "%s should not be null in the bundle, it happened because some bad upgrade had happened.", r4.A01);
            } else {
                for (String add : stringArrayList) {
                    r4.A02.add(add);
                }
            }
            r3.A00 = bundle.getBoolean(C06980cP.A00.getPath(), false);
        } else {
            ((C06760c2) this).A00 = bundle.getBoolean("is_extension_expanded_and_notifications_enabled", false);
        }
    }

    public void A03(Bundle bundle) {
        String path;
        boolean z;
        if (this instanceof C25411Zn) {
            path = C06980cP.A08.getPath();
            z = ((C25411Zn) this).A00;
        } else if (this instanceof C04320Ts) {
            C04320Ts r4 = (C04320Ts) this;
            Bundle bundle2 = new Bundle();
            for (Map.Entry entry : r4.A01.entrySet()) {
                bundle2.putStringArrayList((String) entry.getKey(), C04300To.A03((Iterable) entry.getValue()));
            }
            bundle.putBundle(r4.A01.getAuthority(), bundle2);
            return;
        } else if (!(this instanceof C06760c2)) {
            C06730bz r2 = (C06730bz) this;
            C06100aq r0 = r2.A01;
            bundle.putStringArrayList(r0.A01, C04300To.A03(r0.A02));
            path = C06980cP.A00.getPath();
            z = r2.A00;
        } else {
            bundle.putBoolean("is_extension_expanded_and_notifications_enabled", ((C06760c2) this).A00);
            return;
        }
        bundle.putBoolean(path, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (X.C06100aq.A00(r1, r7).isPresent() != false) goto L_0x0031;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(android.net.Uri r7, java.lang.Object r8) {
        /*
            r6 = this;
            boolean r0 = r6 instanceof X.C25411Zn
            if (r0 != 0) goto L_0x0044
            boolean r0 = r6 instanceof X.C04320Ts
            if (r0 != 0) goto L_0x00b0
            boolean r0 = r6 instanceof X.C06760c2
            if (r0 != 0) goto L_0x009e
            r3 = r6
            X.0bz r3 = (X.C06730bz) r3
            android.net.Uri r0 = X.C06980cP.A02
            boolean r0 = r0.equals(r7)
            r2 = 1
            if (r0 == 0) goto L_0x001c
            r3.A00()
        L_0x001b:
            return r2
        L_0x001c:
            X.0aq r1 = r3.A01
            android.net.Uri r0 = r1.A00
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0031
            com.google.common.base.Optional r0 = X.C06100aq.A00(r1, r7)
            boolean r1 = r0.isPresent()
            r0 = 0
            if (r1 == 0) goto L_0x0032
        L_0x0031:
            r0 = 1
        L_0x0032:
            if (r0 == 0) goto L_0x005d
            X.0aq r1 = r3.A01
            android.net.Uri r0 = r1.A00
            boolean r0 = r0.equals(r7)
            if (r0 == 0) goto L_0x0074
            java.util.Set r0 = r1.A02
            r0.clear()
            return r2
        L_0x0044:
            r3 = r6
            X.1Zn r3 = (X.C25411Zn) r3
            android.net.Uri r0 = X.C06980cP.A08
            boolean r0 = r0.equals(r7)
            r2 = 0
            if (r0 == 0) goto L_0x001b
            boolean r1 = r3.A00
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            boolean r0 = r0.equals(r8)
            r3.A00 = r0
            if (r1 == r0) goto L_0x001b
            goto L_0x0072
        L_0x005d:
            android.net.Uri r0 = X.C06980cP.A00
            boolean r0 = r0.equals(r7)
            r2 = 0
            if (r0 == 0) goto L_0x001b
            boolean r1 = r3.A00
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            boolean r0 = r0.equals(r8)
            r3.A00 = r0
            if (r1 == r0) goto L_0x001b
        L_0x0072:
            r2 = 1
            return r2
        L_0x0074:
            com.google.common.base.Optional r2 = X.C06100aq.A00(r1, r7)
            boolean r0 = r2.isPresent()
            if (r0 == 0) goto L_0x009c
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0091
            java.util.Set r1 = r1.A02
            java.lang.Object r0 = r2.get()
            boolean r2 = r1.add(r0)
            return r2
        L_0x0091:
            java.util.Set r1 = r1.A02
            java.lang.Object r0 = r2.get()
            boolean r2 = r1.remove(r0)
            return r2
        L_0x009c:
            r2 = 0
            return r2
        L_0x009e:
            r3 = r6
            X.0c2 r3 = (X.C06760c2) r3
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            boolean r2 = r0.equals(r8)
            boolean r1 = r3.A00
            r0 = 0
            if (r1 == r2) goto L_0x00ad
            r0 = 1
        L_0x00ad:
            r3.A00 = r2
            return r0
        L_0x00b0:
            r4 = r6
            X.0Ts r4 = (X.C04320Ts) r4
            android.net.Uri r0 = X.C06980cP.A06
            boolean r0 = r0.equals(r7)
            r5 = 0
            r3 = 1
            if (r0 == 0) goto L_0x00cb
            java.util.Map r0 = r4.A01
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0117
            java.util.Map r0 = r4.A01
            r0.clear()
            return r3
        L_0x00cb:
            X.0ar r1 = r4.A00     // Catch:{ 1w4 -> 0x0117 }
            java.lang.String r0 = r7.toString()     // Catch:{ 1w4 -> 0x0117 }
            X.2oH r2 = r1.A02(r0)     // Catch:{ 1w4 -> 0x0117 }
            if (r2 == 0) goto L_0x0117
            java.lang.Object r0 = r2.A01     // Catch:{ 1w4 -> 0x0117 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ 1w4 -> 0x0117 }
            int r0 = r0.intValue()     // Catch:{ 1w4 -> 0x0117 }
            java.lang.String r1 = "thread_id"
            if (r0 == 0) goto L_0x0107
            if (r0 != r3) goto L_0x0117
            android.os.Bundle r0 = r2.A00     // Catch:{ 1w4 -> 0x0117 }
            java.lang.String r3 = r0.getString(r1)     // Catch:{ 1w4 -> 0x0117 }
            java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch:{ 1w4 -> 0x0117 }
            java.util.Map r0 = r4.A01     // Catch:{ 1w4 -> 0x0117 }
            java.lang.Object r1 = r0.get(r3)     // Catch:{ 1w4 -> 0x0117 }
            java.util.Set r1 = (java.util.Set) r1     // Catch:{ 1w4 -> 0x0117 }
            if (r1 != 0) goto L_0x0102
            java.util.HashSet r1 = X.C25011Xz.A03()     // Catch:{ 1w4 -> 0x0117 }
            java.util.Map r0 = r4.A01     // Catch:{ 1w4 -> 0x0117 }
            r0.put(r3, r1)     // Catch:{ 1w4 -> 0x0117 }
        L_0x0102:
            boolean r5 = r1.add(r2)     // Catch:{ 1w4 -> 0x0117 }
            return r5
        L_0x0107:
            android.os.Bundle r0 = r2.A00     // Catch:{ 1w4 -> 0x0117 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ 1w4 -> 0x0117 }
            java.util.Map r0 = r4.A01     // Catch:{ 1w4 -> 0x0117 }
            java.lang.Object r0 = r0.remove(r1)     // Catch:{ 1w4 -> 0x0117 }
            if (r0 == 0) goto L_0x0117
            r5 = 1
            return r5
        L_0x0117:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06750c1.A04(android.net.Uri, java.lang.Object):boolean");
    }

    public C06750c1(Uri uri, int i) {
        this.A01 = uri;
        this.A00 = i;
    }
}
