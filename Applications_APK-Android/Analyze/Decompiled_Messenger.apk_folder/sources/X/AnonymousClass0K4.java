package X;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: X.0K4  reason: invalid class name */
public final class AnonymousClass0K4 {
    private static AnonymousClass0K4 A01;
    public final SharedPreferences A00;

    public static synchronized AnonymousClass0K4 A00(Context context) {
        AnonymousClass0K4 r0;
        synchronized (AnonymousClass0K4.class) {
            if (A01 == null) {
                A01 = new AnonymousClass0K4(context);
            }
            r0 = A01;
        }
        return r0;
    }

    private AnonymousClass0K4(Context context) {
        this.A00 = context.getSharedPreferences("com.facebook.analytics.appstatelogger.AppStateBroadcastReceiver", 0);
    }
}
