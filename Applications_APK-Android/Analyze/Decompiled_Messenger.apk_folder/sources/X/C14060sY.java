package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.0sY  reason: invalid class name and case insensitive filesystem */
public final class C14060sY implements AnonymousClass0ZM {
    public final /* synthetic */ C13980sP A00;

    public C14060sY(C13980sP r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r4) {
        for (C13030qT Bn3 : this.A00.A03) {
            Bn3.Bn3();
        }
    }
}
