package X;

import android.os.Process;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.facebook.common.util.TriState;
import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.quicklog.PerformanceLoggingEvent;
import com.facebook.quicklog.QuickPerformanceLogger;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0fU  reason: invalid class name and case insensitive filesystem */
public final class C08520fU implements QuickPerformanceLogger {
    public static final long A0J = ((C08350fD.A01 | C08350fD.A07) | C08350fD.A0A);
    public static final String A0K = "QuickPerformanceLoggerImpl";
    public static volatile int[] A0L;
    public final AnonymousClass06B A00;
    public final AnonymousClass09O A01;
    public final C25531Zz A02;
    public final C39851zi A03;
    public final C09580i5 A04;
    public final C25401Zm A05;
    public final C08510fT A06;
    public final C12210op A07;
    public final C08610fe A08;
    public final C08490fR A09;
    public final Random A0A;
    public final C04310Tq A0B;
    public final boolean A0C;
    public final C26121aw[] A0D;
    private final SparseIntArray A0E = new SparseIntArray();
    private final C08530fV A0F;
    public volatile C09160gd A0G;
    private volatile TriState A0H;
    private volatile TriState A0I;
    public final C08230et[] mDataProviders;
    public final C08560fY[] mEventDecorators;

    public static void A07(C08520fU r2, String str, int i) {
        A08(r2, str, i, null, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000f, code lost:
        if (X.C010708t.A0X(3) == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A08(X.C08520fU r12, java.lang.String r13, int r14, java.lang.String r15, java.lang.String r16) {
        /*
            r9 = r15
            r11 = r16
            boolean r0 = A0C(r12)
            if (r0 == 0) goto L_0x0011
            r0 = 3
            boolean r1 = X.C010708t.A0X(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x0040
            java.lang.String r4 = X.C08520fU.A0K
            X.0i5 r0 = r12.A04
            java.lang.String r6 = r0.A0C()
            java.lang.String r7 = X.C03020Ho.A00(r14)
            java.lang.Integer r8 = java.lang.Integer.valueOf(r14)
            r3 = 3
            java.lang.String r0 = ""
            if (r15 != 0) goto L_0x002a
            r9 = r0
        L_0x002a:
            if (r16 != 0) goto L_0x0041
            r10 = r0
        L_0x002d:
            if (r16 != 0) goto L_0x0030
            r11 = r0
        L_0x0030:
            r5 = r13
            java.lang.Object[] r2 = new java.lang.Object[]{r5, r6, r7, r8, r9, r10, r11}
            java.lang.String r1 = "%s: (%s) %s (%d) %s%s%s"
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r0 = java.lang.String.format(r0, r1, r2)
            A04(r3, r4, r0)
        L_0x0040:
            return
        L_0x0041:
            java.lang.String r10 = ":"
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.A08(X.0fU, java.lang.String, int, java.lang.String, java.lang.String):void");
    }

    public static boolean A09(int i, C12210op r3) {
        return r3 != null && r3.Bwo() && !A0A(i, r3);
    }

    private static boolean A0B(int i, C12210op r3) {
        return r3 != null && r3.Bwm() && !A0A(i, r3);
    }

    public void A0V(int i, short s, long j, TimeUnit timeUnit, int i2, C29549Ed4 ed4) {
        int i3 = i;
        if (!A09(i3, this.A07)) {
            for (Integer intValue : this.A04.A0D(i3, -1)) {
                int intValue2 = intValue.intValue();
                A0T(i3, intValue2, s, j, timeUnit, null, null, null, i2, this.A0G);
                C29549Ed4 ed42 = ed4;
                if (ed4 != null) {
                    ed42.A00(i3, intValue2);
                }
            }
        }
    }

    public void markerGenerate(int i, short s, int i2) {
        long j = (long) i2;
        if (j > 0) {
            j = TimeUnit.MILLISECONDS.toNanos(j);
        }
        A05(i, s, null, j, null);
    }

    public void markerGenerateForLegacy(int i, short s, String str, int i2) {
        long j = (long) i2;
        if (j > 0) {
            j = TimeUnit.MILLISECONDS.toNanos(j);
        }
        A05(i, s, str, j, null);
    }

    public void markerGenerateWithAnnotations(int i, short s, int i2, Map map) {
        long j = (long) i2;
        if (j > 0) {
            j = TimeUnit.MILLISECONDS.toNanos(j);
        }
        A05(i, s, null, j, map);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        if (r1 == false) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int A00(int r5, boolean r6) {
        /*
            r4 = this;
            X.1Zz r1 = r4.A02
            boolean r0 = r1.BGR()
            r3 = 1
            if (r0 == 0) goto L_0x0010
            boolean r0 = r1.BG7()
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            return r3
        L_0x0010:
            boolean r0 = A0D(r4)
            if (r0 != 0) goto L_0x000f
            android.util.SparseIntArray r2 = r4.A0E
            monitor-enter(r2)
            android.util.SparseIntArray r1 = r4.A0E     // Catch:{ all -> 0x0066 }
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            int r1 = r1.get(r5, r0)     // Catch:{ all -> 0x0066 }
            monitor-exit(r2)     // Catch:{ all -> 0x0066 }
            if (r1 == r0) goto L_0x002b
            X.1Zm r0 = r4.A05
            int r0 = r0.BzU(r1)
            return r0
        L_0x002b:
            if (r6 == 0) goto L_0x000f
            X.0op r0 = r4.A07
            if (r0 == 0) goto L_0x0038
            boolean r1 = r0.Bwn()
            r0 = 1
            if (r1 != 0) goto L_0x0039
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r0 != 0) goto L_0x000f
            X.1zi r1 = r4.A03
            if (r1 == 0) goto L_0x0051
            java.util.concurrent.atomic.AtomicBoolean r0 = r1.A03
            boolean r0 = r0.getAndSet(r3)
            if (r0 != 0) goto L_0x0051
            X.1zj r2 = r1.A01
            r1 = 27787268(0x1a80004, float:6.1713476E-38)
            java.lang.String r0 = "markerId"
            r2.A06(r1, r0, r5)
        L_0x0051:
            X.0op r0 = r4.A07
            if (r0 == 0) goto L_0x005c
            boolean r0 = r0.CEd()
            if (r0 == 0) goto L_0x005c
            return r3
        L_0x005c:
            X.1Zm r1 = r4.A05
            r0 = 2147483647(0x7fffffff, float:NaN)
            int r3 = r1.BzU(r0)
            return r3
        L_0x0066:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0066 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.A00(int, boolean):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r1 == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(X.C08520fU r2, int r3, int r4, boolean r5, boolean r6) {
        /*
            if (r5 == 0) goto L_0x0007
            int r0 = r2.A00(r3, r6)
            return r0
        L_0x0007:
            if (r4 == 0) goto L_0x0019
            X.0op r0 = r2.A07
            if (r0 == 0) goto L_0x0014
            boolean r1 = r0.Bwn()
            r0 = 1
            if (r1 != 0) goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 == 0) goto L_0x0019
            r0 = 1
            return r0
        L_0x0019:
            X.1Zm r0 = r2.A05
            int r0 = r0.BzU(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.A01(X.0fU, int, int, boolean, boolean):int");
    }

    public static long A02(C08520fU r3, long j, TimeUnit timeUnit) {
        if (j == -1) {
            return r3.A01.nowNanos();
        }
        return timeUnit.toNanos(j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0054, code lost:
        if (r4.Bwq() == false) goto L_0x0056;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.C04270Tg A03(int r13, int r14, long r15, java.util.concurrent.TimeUnit r17, boolean r18, boolean r19, boolean r20, int r21, int r22) {
        /*
            r12 = this;
            r10 = r22
            r2 = -1
            r3 = 2147483647(0x7fffffff, float:NaN)
            r1 = 1
            r6 = 0
            if (r22 != 0) goto L_0x009a
            X.1Zm r0 = r12.A05
            int r1 = r0.B1q(r13)
            r5 = 0
            if (r1 != r2) goto L_0x0014
            r5 = 1
        L_0x0014:
            if (r5 != 0) goto L_0x002a
            boolean r0 = A0C(r12)
            if (r0 != 0) goto L_0x002a
            boolean r0 = A0D(r12)
            if (r0 != 0) goto L_0x002a
            X.1Zz r0 = r12.A02
            boolean r0 = r0.BGR()
            if (r0 == 0) goto L_0x002b
        L_0x002a:
            r6 = 1
        L_0x002b:
            int r10 = A01(r12, r13, r1, r6, r5)
        L_0x002f:
            if (r10 == r3) goto L_0x00ba
            X.1Zm r0 = r12.A05
            long r0 = r0.AuS(r13)
            java.util.Random r2 = r12.A0A
            int r8 = r2.nextInt(r3)
            X.06B r2 = r12.A00
            long r2 = r2.now()
            r9 = r20 ^ 1
            X.0i5 r7 = r12.A04
            java.lang.String r7 = r7.A0C()
            X.0op r4 = r12.A07
            if (r4 == 0) goto L_0x0056
            boolean r4 = r4.Bwq()
            r11 = 1
            if (r4 != 0) goto L_0x0057
        L_0x0056:
            r11 = 0
        L_0x0057:
            X.0gC r4 = X.C04270Tg.A0c
            X.0Th r4 = r4.A01()
            X.0Tg r4 = (X.C04270Tg) r4
            r4.A0O = r11
            r4.A02 = r13
            r4.A06 = r10
            r4.A0A = r0
            r4.A0Q = r6
            r4.A0R = r5
            r10 = r17
            r5 = r15
            long r0 = r10.toNanos(r5)
            r4.A0C = r0
            r0 = r18
            r4.A0V = r0
            long r0 = r10.toNanos(r5)
            r4.A0B = r0
            r4.A0D = r2
            r4.A01 = r14
            r4.A08 = r8
            r0 = 1
            r4.A0M = r0
            r1 = r19
            r4.A0N = r1
            r4.A0P = r0
            r4.A0T = r9
            r0 = 0
            r4.A09 = r0
            r0 = r21
            r4.A07 = r0
            r4.A0K = r7
            return r4
        L_0x009a:
            r0 = -3
            if (r10 != r0) goto L_0x00a2
            r10 = 2147483647(0x7fffffff, float:NaN)
            r5 = 0
            goto L_0x002f
        L_0x00a2:
            r5 = 0
            if (r10 != r2) goto L_0x00a6
            r5 = 1
        L_0x00a6:
            r0 = -2
            if (r10 != r0) goto L_0x00aa
            r6 = 1
        L_0x00aa:
            if (r5 != 0) goto L_0x00ae
            if (r6 == 0) goto L_0x002f
        L_0x00ae:
            X.1Zm r0 = r12.A05
            int r0 = r0.B1q(r13)
            int r10 = A01(r12, r13, r0, r1, r5)
            goto L_0x002f
        L_0x00ba:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.A03(int, int, long, java.util.concurrent.TimeUnit, boolean, boolean, boolean, int, int):X.0Tg");
    }

    private void A05(int i, short s, String str, long j, Map map) {
        C04270Tg A032;
        List list;
        int i2 = i;
        if (!A09(i2, this.A07) && (A032 = A03(i2, 0, -1, TimeUnit.NANOSECONDS, true, false, true, Process.myTid(), 0)) != null) {
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    A032.A07((String) entry.getKey(), (String) entry.getValue());
                }
            }
            PerformanceLoggingEvent performanceLoggingEvent = (PerformanceLoggingEvent) PerformanceLoggingEvent.A0c.A01();
            long j2 = j;
            performanceLoggingEvent.A08 = j2;
            if (A032.A0O) {
                performanceLoggingEvent.A02(A032.A0X);
            } else {
                List A022 = A032.A02();
                if (A032.A0O) {
                    list = A032.A0X.A02();
                } else {
                    list = A032.A0Z;
                }
                performanceLoggingEvent.A0E(A022, list);
            }
            int i3 = performanceLoggingEvent.A07 & -16711681;
            performanceLoggingEvent.A07 = i3;
            performanceLoggingEvent.A07 = 131072 | i3;
            performanceLoggingEvent.A06 = i2;
            performanceLoggingEvent.A0R = s;
            performanceLoggingEvent.A0C = this.A00.now();
            performanceLoggingEvent.A0B = this.A01.nowNanos();
            performanceLoggingEvent.A02 = A032.A08;
            performanceLoggingEvent.A05 = A032.A06;
            performanceLoggingEvent.A0A = A032.A0A;
            performanceLoggingEvent.A0X = A032.A0Q;
            performanceLoggingEvent.A0Y = A032.A0R;
            performanceLoggingEvent.A0S = 1;
            performanceLoggingEvent.A09 = j2;
            performanceLoggingEvent.A0M = str;
            performanceLoggingEvent.A0N = this.A04.A0C();
            performanceLoggingEvent.A0W = A032.A0P;
            C04270Tg.A0c.A02(A032);
            this.A09.A00(new C14000sR(this, performanceLoggingEvent));
        }
    }

    public static void A06(C08520fU r12, PerformanceLoggingEvent performanceLoggingEvent) {
        String str;
        if (performanceLoggingEvent.A0V) {
            TriState A002 = r12.A0F.A00();
            if (A002 == TriState.YES) {
                PerformanceLoggingEvent.A0c.A02(performanceLoggingEvent);
                return;
            } else if (A002 == TriState.UNSET) {
                C08490fR r0 = r12.A09;
                r0.A00.schedule(new BJ9(r12, performanceLoggingEvent), (long) LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT, TimeUnit.MILLISECONDS);
                return;
            }
        }
        if (!performanceLoggingEvent.A0W || r12.A02.BDf()) {
            PerformanceLoggingEvent.A0c.A02(performanceLoggingEvent);
            return;
        }
        performanceLoggingEvent.A0L = (AnonymousClass1J8) r12.A0B.get();
        if (A0C(r12)) {
            StringBuilder sb = new StringBuilder();
            if (performanceLoggingEvent.A0M != null) {
                sb.append(" ID=");
                sb.append(C03020Ho.A00(performanceLoggingEvent.A06));
            }
            C11100lt r1 = performanceLoggingEvent.A0J;
            if (r1 != null) {
                r1.A01(new AnonymousClass498(sb));
                sb.append(' ');
            }
            if (!performanceLoggingEvent.A01().isEmpty()) {
                String str2 = null;
                int i = 0;
                for (String str3 : performanceLoggingEvent.A01()) {
                    i++;
                    if (i % 2 == 0) {
                        sb.append(", ");
                        sb.append(str2);
                        sb.append("=");
                        sb.append(str3);
                    } else {
                        str2 = str3;
                    }
                }
            }
            if (!performanceLoggingEvent.A0P.isEmpty()) {
                sb.append(" ");
                sb.append(performanceLoggingEvent.A00());
            }
            AnonymousClass1GD r2 = performanceLoggingEvent.A0K;
            if (r2 != null) {
                sb.append(" metadata=");
                HashMap hashMap = new HashMap();
                r2.A01(new C87204Dt(hashMap));
                sb.append(hashMap);
            }
            String str4 = A0K;
            String str5 = performanceLoggingEvent.A0N;
            String str6 = performanceLoggingEvent.A0M;
            if (str6 == null) {
                str6 = C03020Ho.A00(performanceLoggingEvent.A06);
            }
            String A003 = AnonymousClass0I1.A00(performanceLoggingEvent.A0R);
            Integer valueOf = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(performanceLoggingEvent.A08));
            boolean z = performanceLoggingEvent.A0Y;
            boolean z2 = performanceLoggingEvent.A0X;
            if (z) {
                str = "missing_config";
            } else if (z2) {
                str = "always_on";
            } else {
                str = "random_sampling";
            }
            A04(5, str4, String.format(Locale.US, "QPLSent - (%s) %s %s %d[ms] %s (1:%d) %s", str5, str6, A003, valueOf, str, Integer.valueOf(performanceLoggingEvent.A05), sb.toString()));
        }
        r12.A09.A00(performanceLoggingEvent);
    }

    private static boolean A0A(int i, C12210op r8) {
        int[] iArr;
        if (A0L != null) {
            iArr = A0L;
        } else {
            synchronized (C08520fU.class) {
                if (A0L != null) {
                    iArr = A0L;
                } else {
                    String Bwl = r8.Bwl();
                    if (Bwl == null) {
                        iArr = new int[0];
                        A0L = iArr;
                    } else {
                        String[] split = Bwl.split(",");
                        int length = split.length;
                        iArr = new int[length];
                        for (int i2 = 0; i2 < length; i2++) {
                            try {
                                iArr[i2] = Integer.parseInt(split[i2]);
                            } catch (NumberFormatException unused) {
                            }
                        }
                        Arrays.sort(iArr);
                        A0L = iArr;
                    }
                }
            }
        }
        if (Arrays.binarySearch(iArr, i) >= 0) {
            return true;
        }
        return false;
    }

    public static boolean A0C(C08520fU r2) {
        if (r2.A0H == TriState.UNSET) {
            r2.A0H = r2.A02.BG3();
        }
        return r2.A0H.asBoolean(false);
    }

    public static boolean A0D(C08520fU r2) {
        if (r2.A0I == TriState.UNSET) {
            r2.A0I = r2.A02.BGm();
        }
        return r2.A0I.asBoolean(false);
    }

    public void A0E(int i, int i2, int i3, long j, TimeUnit timeUnit) {
        int i4 = i;
        if (!A09(i, this.A07)) {
            long j2 = j;
            this.A04.A0F(i4, i2, this.A08, i3, j2, timeUnit, this.A0G, null);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0027, code lost:
        if (X.C010708t.A0X(3) == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(int r23, int r24, int r25, java.lang.String r26, X.AnonymousClass0lr r27, android.util.SparseArray r28, long r29, java.util.concurrent.TimeUnit r31, int r32, int r33) {
        /*
            r22 = this;
            r5 = r22
            X.0op r0 = r5.A07
            r8 = r23
            boolean r0 = A09(r8, r0)
            if (r0 != 0) goto L_0x0079
            r4 = 0
            X.1zi r0 = r5.A03
            if (r0 == 0) goto L_0x0015
            X.Ed3 r4 = r0.A01(r8)
        L_0x0015:
            java.lang.String r2 = "markerPoint"
            r16 = r27
            r3 = r16
            boolean r0 = A0C(r5)
            if (r0 == 0) goto L_0x0029
            r0 = 3
            boolean r1 = X.C010708t.A0X(r0)
            r0 = 1
            if (r1 != 0) goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            r15 = r26
            if (r0 == 0) goto L_0x0034
            if (r27 != 0) goto L_0x004c
            r0 = 0
        L_0x0031:
            A08(r5, r2, r8, r15, r0)
        L_0x0034:
            r6 = -1
            r1 = r29
            int r0 = (r29 > r6 ? 1 : (r29 == r6 ? 0 : -1))
            r3 = 0
            if (r0 != 0) goto L_0x003e
            r3 = 1
        L_0x003e:
            if (r3 == 0) goto L_0x0051
            boolean r0 = r5.A0C
            if (r0 == 0) goto L_0x0051
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Autotime in lockless is illegal (time won't be valid when method is executed)"
            r1.<init>(r0)
            throw r1
        L_0x004c:
            java.lang.String r0 = r3.toString()
            goto L_0x0031
        L_0x0051:
            r0 = r31
            long r11 = A02(r5, r1, r0)
            X.0i5 r7 = r5.A04
            java.util.concurrent.TimeUnit r13 = java.util.concurrent.TimeUnit.NANOSECONDS
            r14 = r3 ^ 1
            X.0gd r0 = r5.A0G
            r18 = r28
            r10 = r25
            r17 = r32
            r19 = r33
            r9 = r24
            r20 = r0
            r21 = r4
            r7.A0E(r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            X.1zi r0 = r5.A03
            if (r0 == 0) goto L_0x0079
            if (r4 == 0) goto L_0x0079
            r0.A04(r4)
        L_0x0079:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.A0F(int, int, int, java.lang.String, X.0lr, android.util.SparseArray, long, java.util.concurrent.TimeUnit, int, int):void");
    }

    public void A0G(int i, int i2, int i3, String str, String str2, SparseArray sparseArray, long j, TimeUnit timeUnit, int i4, int i5) {
        Ed3 ed3;
        AnonymousClass0lr r2;
        C39851zi r0;
        C39851zi r02 = this.A03;
        int i6 = i;
        if (r02 != null) {
            ed3 = r02.A01(i6);
        } else {
            ed3 = null;
        }
        if (!A09(i6, this.A07)) {
            String str3 = str2;
            String str4 = str;
            A08(this, "markerPoint", i6, str4, str3);
            long j2 = j;
            boolean z = false;
            if (j == -1) {
                z = true;
            }
            if (!z || !this.A0C) {
                long A022 = A02(this, j2, timeUnit);
                C09580i5 r7 = this.A04;
                TimeUnit timeUnit2 = TimeUnit.NANOSECONDS;
                boolean z2 = !z;
                C09160gd r6 = this.A0G;
                int i7 = i2;
                if (str2 == null) {
                    r2 = null;
                } else {
                    if (C09580i5.A09(r7, C09580i5.A00(i6, i7), r6)) {
                        if (str2 == null) {
                            r2 = null;
                        } else {
                            r2 = new AnonymousClass0lr();
                            r2.A00("__key", str3, 1);
                            r2.A03 = true;
                        }
                    }
                    r0 = this.A03;
                    if (r0 != null && ed3 != null) {
                        r0.A04(ed3);
                        return;
                    }
                    return;
                }
                r7.A0E(i6, i7, i3, A022, timeUnit2, z2, str4, r2, i4, sparseArray, i5, r6, ed3);
                r0 = this.A03;
                if (r0 != null) {
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("Autotime in lockless is illegal (time won't be valid when method is executed)");
        }
    }

    public void A0H(int i, int i2, long j, TimeUnit timeUnit, C29549Ed4 ed4) {
        int i3 = i;
        if (!A09(i, this.A07)) {
            for (Integer intValue : this.A04.A0D(i, -1)) {
                int intValue2 = intValue.intValue();
                if (!A09(i, this.A07)) {
                    int i4 = i2;
                    long j2 = j;
                    this.A04.A0F(i3, intValue2, this.A08, i4, j2, timeUnit, this.A0G, ed4);
                }
            }
        }
    }

    public void A0J(int i, int i2, String str, double d, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r4 = this.A04;
            C09160gd r3 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r4, A002, r3)) {
                synchronized (r4.A04) {
                    C04270Tg A042 = C09580i5.A04(r4, A002);
                    if (C09580i5.A0A(A042, r3)) {
                        A042.A07 = i3;
                        A042.A04(str, d);
                        r3.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0K(int i, int i2, String str, int i3, int i4) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r4 = this.A04;
            C09160gd r3 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r4, A002, r3)) {
                synchronized (r4.A04) {
                    C04270Tg A042 = C09580i5.A04(r4, A002);
                    if (C09580i5.A0A(A042, r3)) {
                        A042.A07 = i4;
                        A042.A05(str, i3);
                        r3.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0L(int i, int i2, String str, long j, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r4 = this.A04;
            C09160gd r3 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r4, A002, r3)) {
                synchronized (r4.A04) {
                    C04270Tg A042 = C09580i5.A04(r4, A002);
                    if (C09580i5.A0A(A042, r3)) {
                        A042.A07 = i3;
                        A042.A06(str, j);
                        r3.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0M(int i, int i2, String str, String str2, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r4 = this.A04;
            C09160gd r3 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r4, A002, r3)) {
                synchronized (r4.A04) {
                    C04270Tg A042 = C09580i5.A04(r4, A002);
                    if (C09580i5.A0A(A042, r3)) {
                        A042.A07 = i3;
                        A042.A07(str, str2);
                        r3.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0N(int i, int i2, String str, boolean z, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r4 = this.A04;
            C09160gd r3 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r4, A002, r3)) {
                synchronized (r4.A04) {
                    C04270Tg A042 = C09580i5.A04(r4, A002);
                    if (C09580i5.A0A(A042, r3)) {
                        A042.A07 = i3;
                        A042.A08(str, z);
                        r3.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0O(int i, int i2, String str, double[] dArr, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r2 = this.A04;
            C09160gd r4 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r2, A002, r4)) {
                synchronized (r2.A04) {
                    C04270Tg A042 = C09580i5.A04(r2, A002);
                    if (C09580i5.A0A(A042, r4)) {
                        A042.A07 = i3;
                        if (A042.A0O) {
                            AnonymousClass0Tj r22 = A042.A0X;
                            r22.A05.add(str);
                            r22.A06.add(Arrays.copyOf(dArr, dArr.length));
                            AnonymousClass0Tj.A00(r22, (byte) 7);
                        } else {
                            C04270Tg.A00(A042, str, AnonymousClass36y.A00(dArr), 6);
                        }
                        r4.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0P(int i, int i2, String str, int[] iArr, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r2 = this.A04;
            C09160gd r4 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r2, A002, r4)) {
                synchronized (r2.A04) {
                    C04270Tg A042 = C09580i5.A04(r2, A002);
                    if (C09580i5.A0A(A042, r4)) {
                        A042.A07 = i3;
                        if (A042.A0O) {
                            AnonymousClass0Tj r22 = A042.A0X;
                            r22.A05.add(str);
                            r22.A06.add(Arrays.copyOf(iArr, iArr.length));
                            AnonymousClass0Tj.A00(r22, (byte) 5);
                        } else {
                            C04270Tg.A00(A042, str, AnonymousClass36y.A01(iArr), 4);
                        }
                        r4.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0Q(int i, int i2, String str, long[] jArr, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r2 = this.A04;
            C09160gd r4 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r2, A002, r4)) {
                synchronized (r2.A04) {
                    C04270Tg A042 = C09580i5.A04(r2, A002);
                    if (C09580i5.A0A(A042, r4)) {
                        A042.A07 = i3;
                        if (A042.A0O) {
                            AnonymousClass0Tj r22 = A042.A0X;
                            r22.A05.add(str);
                            r22.A06.add(Arrays.copyOf(jArr, jArr.length));
                            AnonymousClass0Tj.A00(r22, (byte) 10);
                        } else {
                            C04270Tg.A00(A042, str, AnonymousClass36y.A02(jArr), 4);
                        }
                        r4.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0R(int i, int i2, String str, String[] strArr, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r4 = this.A04;
            C09160gd r3 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r4, A002, r3)) {
                synchronized (r4.A04) {
                    C04270Tg A042 = C09580i5.A04(r4, A002);
                    if (C09580i5.A0A(A042, r3)) {
                        A042.A07 = i3;
                        A042.A09(str, strArr);
                        r3.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0S(int i, int i2, String str, boolean[] zArr, int i3) {
        Ed3 ed3;
        C39851zi r0 = this.A03;
        if (r0 != null) {
            ed3 = r0.A01(i);
        } else {
            ed3 = null;
        }
        if (!A09(i, this.A07)) {
            C09580i5 r2 = this.A04;
            C09160gd r6 = this.A0G;
            int A002 = C09580i5.A00(i, i2);
            if (C09580i5.A09(r2, A002, r6)) {
                synchronized (r2.A04) {
                    C04270Tg A042 = C09580i5.A04(r2, A002);
                    if (C09580i5.A0A(A042, r6)) {
                        A042.A07 = i3;
                        if (A042.A0O) {
                            AnonymousClass0Tj r22 = A042.A0X;
                            r22.A05.add(str);
                            r22.A06.add(Arrays.copyOf(zArr, zArr.length));
                            AnonymousClass0Tj.A00(r22, (byte) 9);
                        } else {
                            StringBuilder sb = new StringBuilder();
                            int i4 = 0;
                            for (boolean append : zArr) {
                                sb.append(append);
                                sb.append(",,,");
                            }
                            int length = sb.length();
                            if (r3 > 0) {
                                i4 = 3;
                            }
                            sb.setLength(length - i4);
                            C04270Tg.A00(A042, str, sb.toString(), 8);
                        }
                        r6.A00(A042);
                    }
                }
            }
            C39851zi r02 = this.A03;
            if (r02 != null && ed3 != null) {
                r02.A03(ed3);
            }
        }
    }

    public void A0U(int i, long j, int i2, long j2, TimeUnit timeUnit, C29549Ed4 ed4) {
        int i3 = i;
        if (!A09(i, this.A07)) {
            for (Integer intValue : this.A04.A0D(i, this.A01.nowNanos() - j)) {
                int intValue2 = intValue.intValue();
                long j3 = j2;
                if (!A09(i, this.A07)) {
                    int i4 = i2;
                    this.A04.A0F(i3, intValue2, this.A08, i4, j3, timeUnit, this.A0G, ed4);
                }
            }
        }
    }

    public void A0W(short s, boolean z, long j, TimeUnit timeUnit, int i, C29549Ed4 ed4) {
        if (A0C(this)) {
            A04(3, A0K, "endAllMarkers");
        }
        C09580i5 r12 = this.A04;
        C08610fe r8 = this.A08;
        C09160gd r7 = this.A0G;
        C29549Ed4 ed42 = ed4;
        ArrayList<C04270Tg> arrayList = new ArrayList<>();
        ArrayList<C04270Tg> arrayList2 = new ArrayList<>();
        synchronized (r12.A04) {
            synchronized (r12.A01) {
                int A012 = C09580i5.A01(r12);
                for (int i2 = 0; i2 < A012; i2++) {
                    C04270Tg A062 = C09580i5.A06(r12, i2);
                    if (!z || !A062.A0T) {
                        arrayList2.add(A062);
                    } else {
                        arrayList.add(A062);
                    }
                }
                synchronized (r12.A01) {
                    r12.A01.clear();
                }
                for (C04270Tg r6 : arrayList) {
                    C09580i5.A07(r12, C09580i5.A00(r6.A02, r6.A01), r6);
                }
                for (C04270Tg r13 : arrayList2) {
                    PerformanceLoggingEvent performanceLoggingEvent = null;
                    r13.A0F = r12.A0B(r13.A0A, null);
                    C09580i5.A08(r12, r13.A0E, r13.A0A);
                    short s2 = s;
                    long j2 = j;
                    TimeUnit timeUnit2 = timeUnit;
                    if (r13.A0P) {
                        performanceLoggingEvent = C09580i5.A03(r12, r13, j2, timeUnit2, s2, true, true, 2);
                    }
                    r13.A0M = s2;
                    r13.A07 = i;
                    r13.A0B = timeUnit2.toNanos(j2);
                    r8.A00(r13.A02, performanceLoggingEvent);
                    r12.A02.remove(C09580i5.A00(r13.A02, r13.A01));
                    if (ed4 != null) {
                        ed42.A00(r13.A02, r13.A01);
                    }
                    C09220gk.A00(r7.A04, r13, 2);
                    C04270Tg.A0c.A02(r13);
                }
                arrayList2.size();
            }
        }
    }

    public void clearAlwaysOnSampleRate(int i) {
        if (!A09(i, this.A07)) {
            synchronized (this.A0E) {
                this.A0E.delete(i);
            }
        }
    }

    public void clearMarkerWhiteListTags(int i, int i2) {
        if (!A09(i, this.A07)) {
            C09580i5 r0 = this.A04;
            synchronized (r0.A04) {
                r0.A02.remove(C09580i5.A00(i, i2));
            }
        }
    }

    public void clearModuleTags(short s) {
        if (!A09(0, this.A07)) {
            C09580i5 r0 = this.A04;
            synchronized (r0.A04) {
                r0.A05[s].clear();
            }
        }
    }

    public long currentMonotonicTimestamp() {
        return TimeUnit.NANOSECONDS.toMillis(this.A01.nowNanos());
    }

    public long currentMonotonicTimestampNanos() {
        return this.A01.nowNanos();
    }

    public void endAllInstancesOfMarker(int i, short s) {
        A0V(i, s, -1, TimeUnit.NANOSECONDS, Process.myTid(), null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003c, code lost:
        if (r11.A02.BGR() != false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0065, code lost:
        if (r9.Bwq() == false) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x011e, code lost:
        if (r0.Bwq() == false) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0172, code lost:
        if (r0.Bwq() == false) goto L_0x0174;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00dd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C21878Ajh markEventBuilder(int r24, java.lang.String r25) {
        /*
            r23 = this;
            r11 = r23
            X.1zi r0 = r11.A03
            r15 = r24
            if (r0 == 0) goto L_0x01a5
            X.Ed3 r10 = r0.A01(r15)
        L_0x000c:
            long r16 = r23.currentMonotonicTimestampNanos()
            X.06B r0 = r11.A00
            long r0 = r0.now()
            X.1Zm r2 = r11.A05
            int r5 = r2.B1q(r15)
            X.1Zm r2 = r11.A05
            long r3 = r2.AuS(r15)
            r2 = -1
            r12 = 0
            if (r5 != r2) goto L_0x0027
            r12 = 1
        L_0x0027:
            if (r12 != 0) goto L_0x003e
            boolean r2 = A0C(r23)
            if (r2 != 0) goto L_0x003e
            boolean r2 = A0D(r23)
            if (r2 != 0) goto L_0x003e
            X.1Zz r2 = r11.A02
            boolean r2 = r2.BGR()
            r6 = 0
            if (r2 == 0) goto L_0x003f
        L_0x003e:
            r6 = 1
        L_0x003f:
            int r5 = A01(r11, r15, r5, r6, r12)
            X.0gd r7 = r11.A0G
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r5 == r2) goto L_0x00e4
            java.util.Random r7 = r11.A0A
            int r8 = r7.nextInt(r2)
            java.util.concurrent.TimeUnit r14 = java.util.concurrent.TimeUnit.NANOSECONDS
            int r7 = android.os.Process.myTid()
            X.0i5 r2 = r11.A04
            java.lang.String r2 = r2.A0C()
            X.0op r9 = r11.A07
            if (r9 == 0) goto L_0x0067
            boolean r9 = r9.Bwq()
            r13 = 1
            if (r9 != 0) goto L_0x0068
        L_0x0067:
            r13 = 0
        L_0x0068:
            r21 = r16
            X.0gC r9 = X.C04270Tg.A0c
            X.0Th r9 = r9.A01()
            X.0Tg r9 = (X.C04270Tg) r9
            r9.A0O = r13
            r9.A02 = r15
            r9.A06 = r5
            r9.A0A = r3
            r9.A0Q = r6
            r9.A0R = r12
            r18 = r14
            r19 = r21
            long r5 = r18.toNanos(r19)
            r9.A0C = r5
            r5 = 1
            r9.A0V = r5
            r12 = r14
            r13 = r21
            long r5 = r12.toNanos(r13)
            r9.A0B = r5
            r9.A0D = r0
            r0 = 0
            r9.A01 = r0
            r9.A08 = r8
            r1 = 1
            r9.A0M = r1
            r9.A0N = r0
            r9.A0P = r1
            r9.A0T = r1
            r0 = 0
            r9.A09 = r0
            r9.A07 = r7
            r9.A0K = r2
            X.0i5 r0 = r11.A04
        L_0x00ae:
            android.util.SparseArray r0 = r0.A0B(r3, r10)
            r9.A0F = r0
        L_0x00b4:
            r0 = 7
            r9.A00 = r0
            r9.A0A = r3
            r0 = r25
            r9.A0L = r0
            if (r10 == 0) goto L_0x00c7
            long r0 = r23.currentMonotonicTimestampNanos()
            long r0 = r0 - r16
            r10.A03 = r0
        L_0x00c7:
            java.lang.ThreadLocal r0 = X.C29545Ecy.A03
            java.lang.Object r2 = r0.get()
            X.Ecy r2 = (X.C29545Ecy) r2
            if (r2 != 0) goto L_0x00dd
            X.Ecy r2 = new X.Ecy
            r2.<init>()
        L_0x00d6:
            r2.A01 = r9
            r2.A02 = r11
            r2.A00 = r10
            return r2
        L_0x00dd:
            java.lang.ThreadLocal r1 = X.C29545Ecy.A03
            r0 = 0
            r1.set(r0)
            goto L_0x00d6
        L_0x00e4:
            if (r7 == 0) goto L_0x014f
            X.0gk r6 = r7.A04
            X.0gj r8 = r6.A02
            r0 = 0
            if (r8 == 0) goto L_0x00f9
            X.0Td[] r5 = r6.A03
            if (r5 == 0) goto L_0x00f9
            long r5 = r6.A01
            long r0 = r8.A00(r15, r0)
            long r0 = r0 | r5
        L_0x00f9:
            r8 = 0
            int r5 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            r0 = 0
            if (r5 == 0) goto L_0x0101
            r0 = 1
        L_0x0101:
            if (r0 == 0) goto L_0x014f
            java.util.Random r0 = r11.A0A
            int r14 = r0.nextInt(r2)
            java.util.concurrent.TimeUnit r12 = java.util.concurrent.TimeUnit.NANOSECONDS
            int r7 = android.os.Process.myTid()
            X.0i5 r0 = r11.A04
            java.lang.String r2 = r0.A0C()
            X.0op r0 = r11.A07
            if (r0 == 0) goto L_0x0120
            boolean r0 = r0.Bwq()
            r6 = 1
            if (r0 != 0) goto L_0x0121
        L_0x0120:
            r6 = 0
        L_0x0121:
            r5 = 0
            r0 = r16
            r13 = 1
            X.0gC r9 = X.C04270Tg.A0c
            X.0Th r9 = r9.A01()
            X.0Tg r9 = (X.C04270Tg) r9
            r9.A0O = r6
            r9.A02 = r15
            r9.A01 = r5
            long r5 = r12.toNanos(r0)
            r9.A0C = r5
            r9.A0V = r13
            r9.A08 = r14
            r9.A0T = r13
            r5 = 0
            r9.A09 = r5
            long r0 = r12.toNanos(r0)
            r9.A0B = r0
            r9.A07 = r7
            r9.A0K = r2
            goto L_0x00b4
        L_0x014f:
            if (r7 == 0) goto L_0x01a8
            boolean r0 = r7.A03(r15)
            if (r0 == 0) goto L_0x01a8
            java.util.Random r0 = r11.A0A
            int r14 = r0.nextInt(r2)
            java.util.concurrent.TimeUnit r13 = java.util.concurrent.TimeUnit.NANOSECONDS
            int r8 = android.os.Process.myTid()
            X.0i5 r0 = r11.A04
            java.lang.String r7 = r0.A0C()
            X.0op r0 = r11.A07
            if (r0 == 0) goto L_0x0174
            boolean r0 = r0.Bwq()
            r1 = 1
            if (r0 != 0) goto L_0x0175
        L_0x0174:
            r1 = 0
        L_0x0175:
            r0 = 0
            r5 = r16
            r12 = 1
            X.0gC r9 = X.C04270Tg.A0c
            X.0Th r9 = r9.A01()
            X.0Tg r9 = (X.C04270Tg) r9
            r9.A0O = r1
            r9.A02 = r15
            r9.A01 = r0
            long r0 = r13.toNanos(r5)
            r9.A0C = r0
            r9.A0V = r12
            r9.A08 = r14
            r9.A0T = r12
            r0 = 0
            r9.A09 = r0
            long r0 = r13.toNanos(r5)
            r9.A0B = r0
            r9.A07 = r8
            r9.A0K = r7
            X.0i5 r0 = r11.A04
            goto L_0x00ae
        L_0x01a5:
            r10 = 0
            goto L_0x000c
        L_0x01a8:
            if (r10 == 0) goto L_0x01af
            X.0gC r0 = X.Ed3.A0B
            r0.A02(r10)
        L_0x01af:
            X.Ak2 r0 = X.Ak2.A00
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.markEventBuilder(int, java.lang.String):X.Ajh");
    }

    public void markerEndAtPoint(int i, int i2, short s, String str) {
        int i3 = i;
        short s2 = s;
        A0T(i3, i2, s2, -1, TimeUnit.NANOSECONDS, null, str, null, Process.myTid(), null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003b, code lost:
        if (r8.A02.BGR() != false) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void markerResampleOnNewConfig(int r9, int r10) {
        /*
            r8 = this;
            X.0op r0 = r8.A07
            boolean r0 = A09(r9, r0)
            if (r0 != 0) goto L_0x008f
            X.0i5 r2 = r8.A04
            int r0 = X.C09580i5.A00(r9, r10)
            java.lang.Object r1 = r2.A04
            monitor-enter(r1)
            X.0Tg r0 = X.C09580i5.A04(r2, r0)     // Catch:{ all -> 0x008c }
            if (r0 != 0) goto L_0x0019
            r0 = 0
            goto L_0x001b
        L_0x0019:
            boolean r0 = r0.A0R     // Catch:{ all -> 0x008c }
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x008c }
            if (r0 == 0) goto L_0x008f
            X.1Zm r0 = r8.A05
            int r1 = r0.B1q(r9)
            r0 = -1
            if (r1 == r0) goto L_0x008f
            boolean r0 = A0C(r8)
            r3 = 0
            if (r0 != 0) goto L_0x003d
            boolean r0 = A0D(r8)
            if (r0 != 0) goto L_0x003d
            X.1Zz r0 = r8.A02
            boolean r0 = r0.BGR()
            r7 = 0
            if (r0 == 0) goto L_0x003e
        L_0x003d:
            r7 = 1
        L_0x003e:
            int r6 = A01(r8, r9, r1, r7, r3)
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r6 != r0) goto L_0x004b
            r8.markerCancel(r9, r10)
            return
        L_0x004b:
            java.lang.String r4 = X.C08520fU.A0K
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r7)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            java.lang.Object[] r3 = new java.lang.Object[]{r2, r1, r0}
            java.lang.String r2 = "markerResampleOnNewConfig, update event sampling info: %s, %s, %s"
            r1 = 3
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r0 = java.lang.String.format(r0, r2, r3)
            A04(r1, r4, r0)
            X.0i5 r5 = r8.A04
            X.1Zm r0 = r8.A05
            long r2 = r0.AuS(r9)
            r4 = 0
            int r0 = X.C09580i5.A00(r9, r10)
            java.lang.Object r1 = r5.A04
            monitor-enter(r1)
            X.0Tg r0 = X.C09580i5.A04(r5, r0)     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x0087
            r0.A06 = r6     // Catch:{ all -> 0x0089 }
            r0.A0A = r2     // Catch:{ all -> 0x0089 }
            r0.A0Q = r7     // Catch:{ all -> 0x0089 }
            r0.A0R = r4     // Catch:{ all -> 0x0089 }
        L_0x0087:
            monitor-exit(r1)     // Catch:{ all -> 0x0089 }
            return
        L_0x0089:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0089 }
            goto L_0x008e
        L_0x008c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x008c }
        L_0x008e:
            throw r0
        L_0x008f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.markerResampleOnNewConfig(int, int):void");
    }

    public void markerStartWithCounterForLegacy(int i, int i2, String str, long j, boolean z, AnonymousClass00T r26, TriState triState) {
        long j2 = j;
        int i3 = i;
        int i4 = i2;
        String str2 = str;
        A0I(i3, i4, j2, TimeUnit.MILLISECONDS, str2, z, true, r26, null, triState, true, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerTagForLegacy(int i, int i2, String str, String str2, boolean z) {
        boolean z2;
        if (!z) {
            C04270Tg A042 = C09580i5.A04(this.A04, C09580i5.A00(i, i2));
            if (A042 != null) {
                z2 = A042.A0S;
            } else {
                z2 = false;
            }
            if (z2) {
                return;
            }
        }
        markerAnnotate(i, i2, "tag_name", str);
        markerAnnotate(i, i2, "tag_value", str2);
    }

    public void moduleTag(short s, String str) {
        if (!A09(0, this.A07)) {
            C09580i5 r0 = this.A04;
            synchronized (r0.A04) {
                ArrayList[] arrayListArr = r0.A05;
                if (arrayListArr[s] == null) {
                    arrayListArr[s] = new ArrayList();
                }
                arrayListArr[s].add(str);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004f, code lost:
        if (r1 == false) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int sampleRateForMarker(int r6) {
        /*
            r5 = this;
            X.0op r0 = r5.A07
            boolean r0 = A0B(r6, r0)
            if (r0 == 0) goto L_0x0019
            X.0op r0 = r5.A07
            if (r0 == 0) goto L_0x0011
            boolean r0 = r0.Bwk()
            return r0
        L_0x0011:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "We should never get faked value when not in experiment. Having GKs as null means we are not in experiment"
            r1.<init>(r0)
            throw r1
        L_0x0019:
            X.1Zm r0 = r5.A05
            int r4 = r0.B1q(r6)
            r0 = -1
            r3 = 0
            r2 = 1
            r1 = 0
            if (r4 != r0) goto L_0x0026
            r1 = 1
        L_0x0026:
            if (r1 != 0) goto L_0x003c
            boolean r0 = A0C(r5)
            if (r0 != 0) goto L_0x003c
            boolean r0 = A0D(r5)
            if (r0 != 0) goto L_0x003c
            X.1Zz r0 = r5.A02
            boolean r0 = r0.BGR()
            if (r0 == 0) goto L_0x003d
        L_0x003c:
            r3 = 1
        L_0x003d:
            if (r3 == 0) goto L_0x0044
            int r0 = r5.A00(r6, r1)
            return r0
        L_0x0044:
            if (r4 == 0) goto L_0x0055
            X.0op r0 = r5.A07
            if (r0 == 0) goto L_0x0051
            boolean r1 = r0.Bwn()
            r0 = 1
            if (r1 != 0) goto L_0x0052
        L_0x0051:
            r0 = 0
        L_0x0052:
            if (r0 == 0) goto L_0x0055
            return r2
        L_0x0055:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.sampleRateForMarker(int):int");
    }

    public void setAlwaysOnSampleRate(int i, int i2) {
        if (A09(i, this.A07)) {
            return;
        }
        if (i2 != Integer.MIN_VALUE) {
            synchronized (this.A0E) {
                this.A0E.put(i, i2);
            }
            return;
        }
        throw new IllegalArgumentException();
    }

    public void setMarkerWhiteListTags(int i, int i2, Collection collection) {
        if (!A09(i, this.A07)) {
            C09580i5 r0 = this.A04;
            synchronized (r0.A04) {
                r0.A02.put(C09580i5.A00(i, i2), collection);
            }
        }
    }

    public void updateListenerMarkers() {
        if (!A09(0, this.A07)) {
            C09160gd r0 = this.A0G;
            this.A0G = new C09160gd(r0.A05, r0.A01, this.A07, r0.A00);
        }
    }

    public C08520fU(C04310Tq r13, C25401Zm r14, AnonymousClass09O r15, AnonymousClass06B r16, C25531Zz r17, C08530fV r18, C08490fR r19, AnonymousClass0Td[] r20, C26121aw[] r21, C08230et[] r22, C08560fY[] r23, C12210op r24, C08510fT r25, C39851zi r26, boolean z, AnonymousClass1ZJ r28) {
        AnonymousClass0Td[] r3;
        TriState triState = TriState.UNSET;
        this.A0H = triState;
        this.A0I = triState;
        this.A0A = new Random();
        this.A08 = new C08610fe(this);
        this.A0B = r13;
        this.A05 = r14;
        this.A01 = r15;
        this.A00 = r16;
        this.A02 = r17;
        C08530fV r5 = r18;
        this.A0F = r5;
        this.A09 = r19;
        C08230et[] r7 = r22;
        this.mDataProviders = r7;
        this.mEventDecorators = r23;
        C12210op r8 = r24;
        this.A07 = r8;
        this.A06 = r25;
        C39851zi r32 = r26;
        this.A03 = r32;
        this.A0D = r21;
        C08620ff r6 = new C08620ff(this);
        C12210op r0 = this.A07;
        this.A0G = new C09160gd(r20, r32, r0 == null ? new C29551Ed8() : r0, r28);
        boolean z2 = z;
        this.A04 = new C09580i5(r5, r6, r7, r8, z2, this.A06, this.A01);
        this.A0C = z2;
        if (!z && (r3 = this.A0G.A05) != null) {
            for (AnonymousClass0Td CB4 : r3) {
                CB4.CB4(this);
            }
        }
    }

    private static void A04(int i, String str, String str2) {
        String str3;
        int length = str2.length();
        int i2 = length / AnonymousClass1Y3.AX2;
        int i3 = 0;
        int i4 = 1;
        if (length % AnonymousClass1Y3.AX2 == 0) {
            i4 = 0;
        }
        int i5 = i2 + i4;
        while (i3 < i5) {
            if (i3 > 0) {
                str3 = "...";
            } else {
                str3 = BuildConfig.FLAVOR;
            }
            int i6 = i3 * AnonymousClass1Y3.AX2;
            i3++;
            String A0J2 = AnonymousClass08S.A0J(str3, str2.substring(i6, Math.min(i3 * AnonymousClass1Y3.AX2, length)));
            if (!(i == 3 || i == 4)) {
                if (i == 5) {
                    C010708t.A0J(str, A0J2);
                } else if (i == 6) {
                    C010708t.A0I(str, A0J2);
                }
            }
        }
    }

    public void endAllMarkers(short s, boolean z) {
        A0W(s, z, currentMonotonicTimestampNanos(), TimeUnit.NANOSECONDS, Process.myTid(), null);
    }

    public void markEvent(int i, String str, int i2) {
        C21878Ajh markEventBuilder = markEventBuilder(i, str);
        markEventBuilder.C8t(i2);
        markEventBuilder.C2Q();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:64:0x012a, code lost:
        if (r1 != false) goto L_0x012d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0I(int r47, int r48, long r49, java.util.concurrent.TimeUnit r51, java.lang.String r52, boolean r53, boolean r54, X.AnonymousClass00T r55, android.util.SparseArray r56, com.facebook.common.util.TriState r57, boolean r58, int r59, int r60, boolean r61, int r62, X.C09160gd r63) {
        /*
            r46 = this;
            r9 = r55
            r18 = r60
            r3 = r46
            X.1zi r1 = r3.A03
            r45 = r47
            if (r1 == 0) goto L_0x0034
            r0 = r45
            X.Ed3 r2 = r1.A01(r0)
        L_0x0012:
            X.0op r1 = r3.A07
            r0 = r45
            boolean r0 = A09(r0, r1)
            if (r0 != 0) goto L_0x024b
            r5 = -1
            r0 = r49
            int r4 = (r49 > r5 ? 1 : (r49 == r5 ? 0 : -1))
            r13 = 0
            if (r4 != 0) goto L_0x0026
            r13 = 1
        L_0x0026:
            if (r13 == 0) goto L_0x0036
            boolean r4 = r3.A0C
            if (r4 == 0) goto L_0x0036
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Autotime in lockless is illegal (time won't be valid when method is executed)"
            r1.<init>(r0)
            throw r1
        L_0x0034:
            r2 = 0
            goto L_0x0012
        L_0x0036:
            r4 = r51
            long r23 = A02(r3, r0, r4)
            r31 = r48
            r15 = r63
            if (r61 == 0) goto L_0x004d
            X.0ge r5 = r15.A02
            r4 = r45
            r1 = r31
            r0 = 1
            int r18 = X.C09170ge.A00(r5, r4, r1, r0)
        L_0x004d:
            java.util.concurrent.TimeUnit r14 = java.util.concurrent.TimeUnit.NANOSECONDS
            r32 = r45
            r21 = r31
            r4 = r23
            r7 = r32
            r8 = r21
            boolean r0 = r3.isMarkerOn(r7, r8)
            r19 = r56
            r12 = r57
            r39 = r59
            if (r0 == 0) goto L_0x012c
            X.0i5 r7 = r3.A04
            X.06B r0 = r3.A00
            long r10 = r0.now()
            r17 = r9
            if (r55 != 0) goto L_0x0087
            r17 = 0
            r22 = r17
        L_0x0075:
            r20 = r32
            int r1 = X.C09580i5.A00(r20, r21)
            boolean r6 = X.C09580i5.A09(r7, r1, r15)
            r16 = 0
            if (r6 == 0) goto L_0x0126
            java.lang.Object r8 = r7.A04
            monitor-enter(r8)
            goto L_0x0091
        L_0x0087:
            boolean r0 = r9.A0F
            if (r0 != 0) goto L_0x008e
            r17.A02()
        L_0x008e:
            r22 = r9
            goto L_0x0075
        L_0x0091:
            X.0Tg r6 = X.C09580i5.A04(r7, r1)     // Catch:{ all -> 0x0122 }
            boolean r0 = X.C09580i5.A0A(r6, r15)     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x0120
            long r0 = r14.toNanos(r4)     // Catch:{ all -> 0x0122 }
            r6.A0C = r0     // Catch:{ all -> 0x0122 }
            r6.A0V = r13     // Catch:{ all -> 0x0122 }
            long r0 = r14.toNanos(r4)     // Catch:{ all -> 0x0122 }
            r6.A0B = r0     // Catch:{ all -> 0x0122 }
            r6.A0D = r10     // Catch:{ all -> 0x0122 }
            r4 = 1
            r6.A0M = r4     // Catch:{ all -> 0x0122 }
            r0 = r16
            r6.A0S = r0     // Catch:{ all -> 0x0122 }
            boolean r0 = r6.A0O     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x00d8
            X.0Tj r0 = r6.A0X     // Catch:{ all -> 0x0122 }
            r0.A04()     // Catch:{ all -> 0x0122 }
        L_0x00bb:
            java.util.ArrayList r0 = r6.A0a     // Catch:{ all -> 0x0122 }
            r0.clear()     // Catch:{ all -> 0x0122 }
            r0 = r22
            r6.A0G = r0     // Catch:{ all -> 0x0122 }
            r0 = r39
            r6.A07 = r0     // Catch:{ all -> 0x0122 }
            int r0 = r6.A03     // Catch:{ all -> 0x0122 }
            r0 = r18 | r0
            r6.A03 = r0     // Catch:{ all -> 0x0122 }
            android.util.SparseArray r5 = r6.A0E     // Catch:{ all -> 0x0122 }
            if (r5 == 0) goto L_0x00e3
            long r0 = r6.A0A     // Catch:{ all -> 0x0122 }
            X.C09580i5.A08(r7, r5, r0)     // Catch:{ all -> 0x0122 }
            goto L_0x00e3
        L_0x00d8:
            java.util.ArrayList r0 = r6.A0Y     // Catch:{ all -> 0x0122 }
            r0.clear()     // Catch:{ all -> 0x0122 }
            java.util.ArrayList r0 = r6.A0Z     // Catch:{ all -> 0x0122 }
            r0.clear()     // Catch:{ all -> 0x0122 }
            goto L_0x00bb
        L_0x00e3:
            if (r17 == 0) goto L_0x00e6
            goto L_0x00f6
        L_0x00e6:
            if (r56 == 0) goto L_0x00ed
            r0 = r19
            r6.A0E = r0     // Catch:{ all -> 0x0122 }
            goto L_0x00fd
        L_0x00ed:
            long r0 = r6.A0A     // Catch:{ all -> 0x0122 }
            android.util.SparseArray r0 = X.C09580i5.A02(r7, r0, r2)     // Catch:{ all -> 0x0122 }
            r6.A0E = r0     // Catch:{ all -> 0x0122 }
            goto L_0x00fd
        L_0x00f6:
            long r0 = r6.A0A     // Catch:{ all -> 0x0122 }
            long r10 = X.C08520fU.A0J     // Catch:{ all -> 0x0122 }
            long r0 = r0 | r10
            r6.A0A = r0     // Catch:{ all -> 0x0122 }
        L_0x00fd:
            int r0 = r6.A05     // Catch:{ all -> 0x0122 }
            int r0 = r0 + r4
            r6.A05 = r0     // Catch:{ all -> 0x0122 }
            if (r57 == 0) goto L_0x0105
            goto L_0x0111
        L_0x0105:
            boolean r0 = r7.A0I()     // Catch:{ all -> 0x0122 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)     // Catch:{ all -> 0x0122 }
            r6.A03(r0, r4)     // Catch:{ all -> 0x0122 }
            goto L_0x0118
        L_0x0111:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x0122 }
            if (r12 == r0) goto L_0x0105
            r6.A03(r12, r4)     // Catch:{ all -> 0x0122 }
        L_0x0118:
            X.0gk r1 = r15.A04     // Catch:{ all -> 0x0122 }
            r0 = 3
            X.C09220gk.A00(r1, r6, r0)     // Catch:{ all -> 0x0122 }
            monitor-exit(r8)     // Catch:{ all -> 0x0122 }
            goto L_0x0128
        L_0x0120:
            monitor-exit(r8)     // Catch:{ all -> 0x0122 }
            goto L_0x0126
        L_0x0122:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0122 }
            goto L_0x01f2
        L_0x0126:
            r1 = 0
            goto L_0x0129
        L_0x0128:
            r1 = 1
        L_0x0129:
            r0 = 1
            if (r1 != 0) goto L_0x012d
        L_0x012c:
            r0 = 0
        L_0x012d:
            r1 = 1
            if (r0 == 0) goto L_0x013e
            X.1zi r0 = r3.A03
            if (r0 == 0) goto L_0x024b
            if (r2 == 0) goto L_0x024b
            r2.A06 = r1
            r2.A05 = r1
            r0.A02(r2)
            return
        L_0x013e:
            r29 = r39
            r27 = r53
            r30 = r62
            r28 = r58
            r22 = r21
            r25 = r14
            r26 = r13
            r20 = r3
            r21 = r32
            X.0Tg r4 = r20.A03(r21, r22, r23, r25, r26, r27, r28, r29, r30)
            if (r4 == 0) goto L_0x0169
            X.1zi r0 = r3.A03
            if (r0 == 0) goto L_0x0169
            int r6 = r4.A02
            X.1zj r5 = r0.A01
            r1 = 27787271(0x1a80007, float:6.171349E-38)
            java.lang.String r0 = "tracked_marker_id"
            boolean r0 = r5.A06(r1, r0, r6)
            r4.A0U = r0
        L_0x0169:
            java.util.concurrent.TimeUnit r35 = java.util.concurrent.TimeUnit.NANOSECONDS
            r1 = r45
            r5 = r52
            if (r4 == 0) goto L_0x01f3
            java.lang.String r0 = "onMarkerStart"
            A07(r3, r0, r1)
            r4.A0J = r5
            r1 = r9
            if (r55 != 0) goto L_0x019f
            r1 = 0
        L_0x017c:
            r4.A0G = r1
            if (r1 == 0) goto L_0x0187
            long r0 = r4.A0A
            long r5 = X.C08520fU.A0J
            long r0 = r0 | r5
            r4.A0A = r0
        L_0x0187:
            X.0i5 r8 = r3.A04
            r9 = r19
            int r1 = r4.A02
            int r0 = r4.A01
            int r5 = X.C09580i5.A00(r1, r0)
            if (r56 != 0) goto L_0x019b
            long r0 = r4.A0A
            android.util.SparseArray r9 = X.C09580i5.A02(r8, r0, r2)
        L_0x019b:
            java.lang.Object r6 = r8.A04
            monitor-enter(r6)
            goto L_0x01a7
        L_0x019f:
            boolean r0 = r9.A0F
            if (r0 != 0) goto L_0x017c
            r9.A02()
            goto L_0x017c
        L_0x01a7:
            r4.A0E = r9     // Catch:{ all -> 0x01f0 }
            r1 = 0
            if (r57 == 0) goto L_0x01ad
            goto L_0x01b9
        L_0x01ad:
            boolean r0 = r8.A0I()     // Catch:{ all -> 0x01f0 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)     // Catch:{ all -> 0x01f0 }
            r4.A03(r0, r1)     // Catch:{ all -> 0x01f0 }
            goto L_0x01c0
        L_0x01b9:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x01f0 }
            if (r12 == r0) goto L_0x01ad
            r4.A03(r12, r1)     // Catch:{ all -> 0x01f0 }
        L_0x01c0:
            r0 = r18
            r4.A03 = r0     // Catch:{ all -> 0x01f0 }
            android.util.SparseArray r1 = r8.A01     // Catch:{ all -> 0x01f0 }
            monitor-enter(r1)     // Catch:{ all -> 0x01f0 }
            android.util.SparseArray r0 = r8.A01     // Catch:{ all -> 0x01ed }
            int r7 = r0.indexOfKey(r5)     // Catch:{ all -> 0x01ed }
            monitor-exit(r1)     // Catch:{ all -> 0x01ed }
            if (r7 < 0) goto L_0x01e1
            X.0Tg r5 = X.C09580i5.A06(r8, r7)     // Catch:{ all -> 0x01f0 }
            android.util.SparseArray r1 = r8.A01     // Catch:{ all -> 0x01f0 }
            monitor-enter(r1)     // Catch:{ all -> 0x01f0 }
            android.util.SparseArray r0 = r8.A01     // Catch:{ all -> 0x01de }
            r0.setValueAt(r7, r4)     // Catch:{ all -> 0x01de }
            monitor-exit(r1)     // Catch:{ all -> 0x01de }
            goto L_0x01e5
        L_0x01de:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01de }
            goto L_0x01ef
        L_0x01e1:
            X.C09580i5.A07(r8, r5, r4)     // Catch:{ all -> 0x01f0 }
            r5 = 0
        L_0x01e5:
            X.0gk r1 = r15.A04     // Catch:{ all -> 0x01f0 }
            r0 = 1
            X.C09220gk.A00(r1, r4, r0)     // Catch:{ all -> 0x01f0 }
            monitor-exit(r6)     // Catch:{ all -> 0x01f0 }
            goto L_0x021f
        L_0x01ed:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01ed }
        L_0x01ef:
            throw r0     // Catch:{ all -> 0x01f0 }
        L_0x01f0:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x01f0 }
        L_0x01f2:
            throw r0
        L_0x01f3:
            java.lang.String r0 = "markerNotStarted"
            A07(r3, r0, r1)
            java.util.Random r6 = r3.A0A
            r0 = 2147483647(0x7fffffff, float:NaN)
            int r37 = r6.nextInt(r0)
            X.0i5 r6 = r3.A04
            X.1Zm r0 = r3.A05
            r29 = r6
            r30 = r1
            r32 = r5
            r33 = r23
            r36 = r13
            r38 = r28
            r40 = r18
            r41 = r0
            r42 = r19
            r43 = r15
            r44 = r2
            r29.A0G(r30, r31, r32, r33, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44)
            goto L_0x022a
        L_0x021f:
            if (r5 == 0) goto L_0x022a
            r0 = 3
            X.C010708t.A0X(r0)
            X.0gC r0 = X.C04270Tg.A0c
            r0.A02(r5)
        L_0x022a:
            X.1zi r0 = r3.A03
            if (r0 == 0) goto L_0x024b
            if (r2 == 0) goto L_0x024b
            if (r4 != 0) goto L_0x0248
            X.1Zm r1 = r3.A05
            r0 = r45
            int r0 = r1.B1q(r0)
        L_0x023a:
            r2.A01 = r0
            r0 = 0
            if (r4 == 0) goto L_0x0240
            r0 = 1
        L_0x0240:
            r2.A06 = r0
            X.1zi r0 = r3.A03
            r0.A02(r2)
            return
        L_0x0248:
            int r0 = r4.A06
            goto L_0x023a
        L_0x024b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.A0I(int, int, long, java.util.concurrent.TimeUnit, java.lang.String, boolean, boolean, X.00T, android.util.SparseArray, com.facebook.common.util.TriState, boolean, int, int, boolean, int, X.0gd):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: com.facebook.quicklog.PerformanceLoggingEvent} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v7, resolved type: java.lang.String[]} */
    /* JADX WARN: Type inference failed for: r7v0 */
    /* JADX WARN: Type inference failed for: r7v2 */
    /* JADX WARN: Type inference failed for: r7v9 */
    /* JADX WARN: Type inference failed for: r7v10 */
    /* JADX WARN: Type inference failed for: r7v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00dd, code lost:
        if (r20.A03(r2.A02) != false) goto L_0x00df;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0084 A[Catch:{ all -> 0x0317 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0085 A[Catch:{ all -> 0x0317 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0156 A[Catch:{ all -> 0x0314 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0165 A[Catch:{ all -> 0x0314 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0T(int r37, int r38, short r39, long r40, java.util.concurrent.TimeUnit r42, com.facebook.common.util.TriState r43, java.lang.String r44, android.util.SparseArray r45, int r46, X.C09160gd r47) {
        /*
            r36 = this;
            r13 = r45
            r20 = r47
            r3 = r36
            X.1zi r0 = r3.A03
            r15 = r37
            if (r0 == 0) goto L_0x0036
            X.Ed3 r9 = r0.A01(r15)
        L_0x0010:
            X.0op r0 = r3.A07
            boolean r0 = A09(r15, r0)
            if (r0 != 0) goto L_0x031a
            r22 = -1
            r0 = r40
            int r2 = (r40 > r22 ? 1 : (r40 == r22 ? 0 : -1))
            r21 = 0
            if (r2 != 0) goto L_0x0024
            r21 = 1
        L_0x0024:
            r4 = r44
            if (r21 == 0) goto L_0x0038
            boolean r2 = r3.A0C
            if (r2 == 0) goto L_0x0038
            if (r44 != 0) goto L_0x0038
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Autotime in lockless is illegal (time won't be valid when method is executed)"
            r1.<init>(r0)
            throw r1
        L_0x0036:
            r9 = 0
            goto L_0x0010
        L_0x0038:
            r2 = r42
            long r16 = A02(r3, r0, r2)
            if (r47 != 0) goto L_0x0044
            X.0gd r0 = r3.A0G
            r20 = r0
        L_0x0044:
            r0 = r20
            X.0ge r0 = r0.A02
            r8 = 2
            r1 = r38
            X.C09170ge.A00(r0, r15, r1, r8)
            X.0i5 r0 = r3.A04
            r35 = r0
            java.util.concurrent.TimeUnit r19 = java.util.concurrent.TimeUnit.NANOSECONDS
            r14 = r0
            r24 = r43
            int r1 = X.C09580i5.A00(r15, r1)
            java.lang.Object r12 = r0.A04
            monitor-enter(r12)
            X.0Tg r2 = X.C09580i5.A05(r0, r1)     // Catch:{ all -> 0x0317 }
            r7 = 0
            if (r2 == 0) goto L_0x00a3
            android.util.SparseArray r11 = r2.A0E     // Catch:{ all -> 0x0317 }
            long r0 = r2.A0A     // Catch:{ all -> 0x0317 }
            X.0lt r10 = r2.A0I     // Catch:{ all -> 0x0317 }
            if (r44 == 0) goto L_0x00b3
            if (r10 == 0) goto L_0x0081
            r6 = 0
        L_0x0070:
            int r5 = r10.A01     // Catch:{ all -> 0x0317 }
            if (r6 >= r5) goto L_0x0081
            java.lang.String[] r5 = r10.A07     // Catch:{ all -> 0x0317 }
            r5 = r5[r6]     // Catch:{ all -> 0x0317 }
            boolean r5 = r4.equals(r5)     // Catch:{ all -> 0x0317 }
            if (r5 != 0) goto L_0x0082
            int r6 = r6 + 1
            goto L_0x0070
        L_0x0081:
            r6 = -1
        L_0x0082:
            if (r6 < 0) goto L_0x0085
            goto L_0x00a7
        L_0x0085:
            X.2Ny r5 = new X.2Ny     // Catch:{ all -> 0x0317 }
            java.lang.String r1 = "Point "
            java.lang.String r0 = " is not found. You should endOnPoint only when point been reported"
            java.lang.String r4 = X.AnonymousClass08S.A0P(r1, r4, r0)     // Catch:{ all -> 0x0317 }
            if (r10 == 0) goto L_0x009b
            int r3 = r10.A01     // Catch:{ all -> 0x0317 }
            java.lang.String[] r7 = new java.lang.String[r3]     // Catch:{ all -> 0x0317 }
            java.lang.String[] r1 = r10.A07     // Catch:{ all -> 0x0317 }
            r0 = 0
            java.lang.System.arraycopy(r1, r0, r7, r0, r3)     // Catch:{ all -> 0x0317 }
        L_0x009b:
            int r1 = r2.A02     // Catch:{ all -> 0x0317 }
            int r0 = r2.A01     // Catch:{ all -> 0x0317 }
            r5.<init>(r4, r7, r1, r0)     // Catch:{ all -> 0x0317 }
            throw r5     // Catch:{ all -> 0x0317 }
        L_0x00a3:
            r0 = 0
            r11 = r7
            goto L_0x00b3
        L_0x00a7:
            android.util.SparseArray[] r4 = r10.A05     // Catch:{ all -> 0x0317 }
            r13 = r4[r6]     // Catch:{ all -> 0x0317 }
            long r4 = r2.A0C     // Catch:{ all -> 0x0317 }
            long[] r10 = r10.A04     // Catch:{ all -> 0x0317 }
            r16 = r10[r6]     // Catch:{ all -> 0x0317 }
            long r16 = r16 + r4
        L_0x00b3:
            monitor-exit(r12)     // Catch:{ all -> 0x0317 }
            if (r13 != 0) goto L_0x00ba
            android.util.SparseArray r13 = r14.A0B(r0, r9)
        L_0x00ba:
            X.C09580i5.A08(r14, r11, r0)
            java.lang.Object r14 = r14.A04
            monitor-enter(r14)
            if (r2 == 0) goto L_0x027d
            r4 = r19
            r5 = r16
            long r11 = r4.toNanos(r5)     // Catch:{ all -> 0x0314 }
            long r0 = r2.A0C     // Catch:{ all -> 0x0314 }
            long r11 = r11 - r0
            r2.A0F = r13     // Catch:{ all -> 0x0314 }
            boolean r0 = r2.A0P     // Catch:{ all -> 0x0314 }
            r13 = r39
            if (r0 != 0) goto L_0x00df
            int r0 = r2.A02     // Catch:{ all -> 0x0314 }
            r4 = r20
            boolean r0 = r4.A03(r0)     // Catch:{ all -> 0x0314 }
            if (r0 == 0) goto L_0x0245
        L_0x00df:
            X.00T r6 = r2.A0G     // Catch:{ all -> 0x0314 }
            if (r6 == 0) goto L_0x0225
            boolean r0 = r6.A0F     // Catch:{ all -> 0x0314 }
            if (r0 == 0) goto L_0x0225
            boolean r0 = r6.A0E     // Catch:{ all -> 0x0314 }
            if (r0 != 0) goto L_0x0225
            int r7 = android.os.Process.myTid()     // Catch:{ all -> 0x0314 }
            int r0 = android.os.Process.getThreadPriority(r7)     // Catch:{ all -> 0x0314 }
            r6.A01 = r0     // Catch:{ all -> 0x0314 }
            long r4 = android.os.Process.getElapsedCpuTime()     // Catch:{ all -> 0x0314 }
            long r0 = r6.A06     // Catch:{ all -> 0x0314 }
            long r4 = r4 - r0
            r6.A06 = r4     // Catch:{ all -> 0x0314 }
            java.lang.String r0 = "/proc/self/stat"
            long[] r10 = X.AnonymousClass00U.A01(r0)     // Catch:{ all -> 0x0314 }
            r0 = 0
            r4 = r10[r0]     // Catch:{ all -> 0x0314 }
            long r0 = r6.A08     // Catch:{ all -> 0x0314 }
            long r4 = r4 - r0
            r6.A08 = r4     // Catch:{ all -> 0x0314 }
            r4 = r10[r8]     // Catch:{ all -> 0x0314 }
            long r0 = r6.A07     // Catch:{ all -> 0x0314 }
            long r4 = r4 - r0
            r6.A07 = r4     // Catch:{ all -> 0x0314 }
            int r0 = r6.A02     // Catch:{ all -> 0x0314 }
            if (r7 != r0) goto L_0x01f6
            long r4 = android.os.SystemClock.currentThreadTimeMillis()     // Catch:{ all -> 0x0314 }
            long r0 = r6.A09     // Catch:{ all -> 0x0314 }
            long r4 = r4 - r0
            r6.A09 = r4     // Catch:{ all -> 0x0314 }
            long r4 = X.AnonymousClass00U.A00()     // Catch:{ all -> 0x0314 }
            long r0 = r6.A0A     // Catch:{ all -> 0x0314 }
            long r4 = r4 - r0
            r6.A0A = r4     // Catch:{ all -> 0x0314 }
        L_0x0129:
            long r0 = r6.A03     // Catch:{ all -> 0x0314 }
            r7 = -1
            int r4 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r4 != 0) goto L_0x013d
            long r0 = r6.A04     // Catch:{ all -> 0x0314 }
            int r4 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r4 != 0) goto L_0x013d
            long r0 = r6.A05     // Catch:{ all -> 0x0314 }
            int r4 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r4 == 0) goto L_0x016e
        L_0x013d:
            X.00Z r10 = X.AnonymousClass00Y.A00()     // Catch:{ all -> 0x0314 }
            long r4 = r6.A03     // Catch:{ all -> 0x0314 }
            int r0 = (r4 > r22 ? 1 : (r4 == r22 ? 0 : -1))
            if (r0 == 0) goto L_0x01f2
            long r0 = r10.A00     // Catch:{ all -> 0x0314 }
            int r18 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r18 == 0) goto L_0x01f2
            long r0 = r0 - r4
            r6.A03 = r0     // Catch:{ all -> 0x0314 }
        L_0x0150:
            long r4 = r6.A04     // Catch:{ all -> 0x0314 }
            int r0 = (r4 > r22 ? 1 : (r4 == r22 ? 0 : -1))
            if (r0 == 0) goto L_0x01ee
            long r0 = r10.A02     // Catch:{ all -> 0x0314 }
            int r18 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r18 == 0) goto L_0x01ee
            long r0 = r0 - r4
            r6.A04 = r0     // Catch:{ all -> 0x0314 }
        L_0x015f:
            long r4 = r6.A05     // Catch:{ all -> 0x0314 }
            int r0 = (r4 > r22 ? 1 : (r4 == r22 ? 0 : -1))
            if (r0 == 0) goto L_0x01eb
            long r0 = r10.A04     // Catch:{ all -> 0x0314 }
            int r10 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r10 == 0) goto L_0x01eb
            long r0 = r0 - r4
            r6.A05 = r0     // Catch:{ all -> 0x0314 }
        L_0x016e:
            X.03b r0 = new X.03b     // Catch:{ all -> 0x0314 }
            r0.<init>()     // Catch:{ all -> 0x0314 }
            r6.A0B = r0     // Catch:{ all -> 0x0314 }
            com.facebook.common.dextricks.stats.ClassLoadingStats r5 = com.facebook.common.dextricks.stats.ClassLoadingStats.A00()     // Catch:{ all -> 0x0314 }
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r4 = r6.A0C     // Catch:{ all -> 0x0314 }
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r1 = new com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats     // Catch:{ all -> 0x0314 }
            int r26 = r5.getClassLoadsAttempted()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A00     // Catch:{ all -> 0x0314 }
            int r26 = r26 - r0
            int r27 = r5.getClassLoadsFailed()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A01     // Catch:{ all -> 0x0314 }
            int r27 = r27 - r0
            int r28 = r5.getDexFileQueries()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A02     // Catch:{ all -> 0x0314 }
            int r28 = r28 - r0
            int r29 = r5.getLocatorAssistedClassLoads()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A04     // Catch:{ all -> 0x0314 }
            int r29 = r29 - r0
            int r30 = r5.getIncorrectDfaGuesses()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A03     // Catch:{ all -> 0x0314 }
            int r30 = r30 - r0
            int r31 = r5.getTurboLoaderMapGenerationSuccesses()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A08     // Catch:{ all -> 0x0314 }
            int r31 = r31 - r0
            int r32 = r5.getTurboLoaderMapGenerationFailures()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A07     // Catch:{ all -> 0x0314 }
            int r32 = r32 - r0
            int r33 = r5.getTurboLoaderClassLocationSuccesses()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A06     // Catch:{ all -> 0x0314 }
            int r33 = r33 - r0
            int r34 = r5.getTurboLoaderClassLocationFailures()     // Catch:{ all -> 0x0314 }
            int r0 = r4.A05     // Catch:{ all -> 0x0314 }
            int r34 = r34 - r0
            r25 = r1
            r25.<init>(r26, r27, r28, r29, r30, r31, r32, r33, r34)     // Catch:{ all -> 0x0314 }
            r6.A0C = r1     // Catch:{ all -> 0x0314 }
            long r0 = r6.A06     // Catch:{ all -> 0x0314 }
            r7 = 0
            int r4 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r4 < 0) goto L_0x0210
            long r0 = r6.A07     // Catch:{ all -> 0x0314 }
            int r4 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r4 < 0) goto L_0x0210
            long r0 = r6.A08     // Catch:{ all -> 0x0314 }
            int r4 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r4 < 0) goto L_0x0210
            boolean r0 = r6.A0E     // Catch:{ all -> 0x0314 }
            if (r0 == 0) goto L_0x0201
            int r4 = r6.A02     // Catch:{ all -> 0x0314 }
            r1 = -1
            r0 = 1
            if (r4 != r1) goto L_0x0202
            goto L_0x0201
        L_0x01eb:
            r6.A05 = r7     // Catch:{ all -> 0x0314 }
            goto L_0x016e
        L_0x01ee:
            r6.A04 = r7     // Catch:{ all -> 0x0314 }
            goto L_0x015f
        L_0x01f2:
            r6.A03 = r7     // Catch:{ all -> 0x0314 }
            goto L_0x0150
        L_0x01f6:
            r0 = -1
            r6.A02 = r0     // Catch:{ all -> 0x0314 }
            r0 = -1
            r6.A09 = r0     // Catch:{ all -> 0x0314 }
            r6.A0A = r0     // Catch:{ all -> 0x0314 }
            goto L_0x0129
        L_0x0201:
            r0 = 0
        L_0x0202:
            if (r0 == 0) goto L_0x0222
            long r0 = r6.A09     // Catch:{ all -> 0x0314 }
            int r4 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r4 < 0) goto L_0x0210
            long r0 = r6.A0A     // Catch:{ all -> 0x0314 }
            int r4 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r4 >= 0) goto L_0x0222
        L_0x0210:
            java.lang.String r1 = "PerfStats"
            r0 = 5
            boolean r0 = android.util.Log.isLoggable(r1, r0)     // Catch:{ all -> 0x0314 }
            if (r0 == 0) goto L_0x021e
            java.lang.String r0 = "Negative values detected for PerfStats, discarding stats."
            android.util.Log.w(r1, r0)     // Catch:{ all -> 0x0314 }
        L_0x021e:
            X.AnonymousClass00T.A00(r6)     // Catch:{ all -> 0x0314 }
            goto L_0x0225
        L_0x0222:
            r0 = 1
            r6.A0E = r0     // Catch:{ all -> 0x0314 }
        L_0x0225:
            r4 = 0
            if (r43 == 0) goto L_0x0258
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x0314 }
            r0 = r24
            if (r0 == r1) goto L_0x0258
            r2.A03(r0, r4)     // Catch:{ all -> 0x0314 }
        L_0x0231:
            r28 = 0
            r26 = r19
            r29 = 1
            r30 = 2
            r22 = r35
            r23 = r2
            r24 = r16
            r27 = r13
            com.facebook.quicklog.PerformanceLoggingEvent r7 = X.C09580i5.A03(r22, r23, r24, r26, r27, r28, r29, r30)     // Catch:{ all -> 0x0314 }
        L_0x0245:
            r4 = r19
            r5 = r16
            long r0 = r4.toNanos(r5)     // Catch:{ all -> 0x0314 }
            r2.A0B = r0     // Catch:{ all -> 0x0314 }
            r2.A0M = r13     // Catch:{ all -> 0x0314 }
            r2.A09 = r11     // Catch:{ all -> 0x0314 }
            boolean r0 = r2.A0V     // Catch:{ all -> 0x0314 }
            if (r0 != 0) goto L_0x0268
            goto L_0x0266
        L_0x0258:
            r0 = r35
            boolean r0 = r0.A0I()     // Catch:{ all -> 0x0314 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)     // Catch:{ all -> 0x0314 }
            r2.A03(r0, r4)     // Catch:{ all -> 0x0314 }
            goto L_0x0231
        L_0x0266:
            r21 = 0
        L_0x0268:
            r0 = r21
            r2.A0V = r0     // Catch:{ all -> 0x0314 }
            r0 = r46
            r2.A07 = r0     // Catch:{ all -> 0x0314 }
            r0 = r20
            X.0gk r1 = r0.A04     // Catch:{ all -> 0x0314 }
            r0 = 2
            X.C09220gk.A00(r1, r2, r0)     // Catch:{ all -> 0x0314 }
            X.0gC r0 = X.C04270Tg.A0c     // Catch:{ all -> 0x0314 }
            r0.A02(r2)     // Catch:{ all -> 0x0314 }
        L_0x027d:
            monitor-exit(r14)     // Catch:{ all -> 0x0314 }
            r4 = 1
            r1 = 0
            if (r7 == 0) goto L_0x02cc
            X.0fV r0 = r3.A0F
            com.facebook.common.util.TriState r0 = r0.A00()
            r7.A0H = r0
            java.lang.String r0 = "markerEnd"
            A07(r3, r0, r15)
            X.0fR r1 = r3.A09
            X.0sR r0 = new X.0sR
            r0.<init>(r3, r7)
            r1.A00(r0)
            boolean r0 = r3.A0C
            if (r0 == 0) goto L_0x02a4
            X.1zi r0 = r3.A03
            if (r0 == 0) goto L_0x02a4
            r0.A05(r4)
        L_0x02a4:
            X.1zi r0 = r3.A03
            if (r0 == 0) goto L_0x031a
            if (r9 == 0) goto L_0x031a
            if (r7 != 0) goto L_0x02c9
            X.1Zm r0 = r3.A05
            int r1 = r0.B1q(r15)
        L_0x02b2:
            r9.A01 = r1
            if (r7 != 0) goto L_0x02b7
            r4 = 0
        L_0x02b7:
            r9.A06 = r4
            X.1zi r5 = r3.A03
            if (r4 != 0) goto L_0x02d8
            r0 = 1
            if (r1 <= r0) goto L_0x02d8
            java.util.Random r0 = r5.A02
            int r0 = r0.nextInt(r1)
            if (r0 == 0) goto L_0x02d8
            return
        L_0x02c9:
            int r1 = r7.A05
            goto L_0x02b2
        L_0x02cc:
            boolean r0 = r3.A0C
            if (r0 == 0) goto L_0x02a4
            X.1zi r0 = r3.A03
            if (r0 == 0) goto L_0x02a4
            r0.A05(r1)
            goto L_0x02a4
        L_0x02d8:
            X.09O r0 = r5.A00
            long r1 = r0.nowNanos()
            long r3 = r9.A04
            long r1 = r1 - r3
            X.1zj r4 = r5.A01
            java.lang.String r0 = "MARKER_END_TIME"
            com.facebook.quicklog.PerformanceLoggingEvent r2 = X.C39861zj.A02(r4, r0, r9, r1)
            boolean r0 = X.C39861zj.A04(r4)
            if (r0 != 0) goto L_0x02fa
            X.0gC r0 = com.facebook.quicklog.PerformanceLoggingEvent.A0c
            r0.A02(r2)
        L_0x02f4:
            X.0gC r0 = X.Ed3.A0B
            r0.A02(r9)
            return
        L_0x02fa:
            boolean r1 = r9.A06
            java.lang.String r0 = "is_sampled"
            r2.A0C(r0, r1)
            X.C39861zj.A03(r4, r9, r2)
            X.0fR r0 = r4.A01
            r0.A00(r2)
            java.lang.ThreadLocal r1 = r4.A02
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r1.set(r0)
            goto L_0x02f4
        L_0x0314:
            r0 = move-exception
            monitor-exit(r14)     // Catch:{ all -> 0x0314 }
            goto L_0x0319
        L_0x0317:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x0317 }
        L_0x0319:
            throw r0
        L_0x031a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08520fU.A0T(int, int, short, long, java.util.concurrent.TimeUnit, com.facebook.common.util.TriState, java.lang.String, android.util.SparseArray, int, X.0gd):void");
    }

    public void cancelAllInstancesOfMarker(int i) {
        dropAllInstancesOfMarker(i);
    }

    public void cancelAllInstancesOfMarker(int i, int i2) {
        dropAllInstancesOfMarker(i, i2);
    }

    public void dropAllInstancesOfMarker(int i) {
        A0H(i, Process.myTid(), currentMonotonicTimestampNanos(), TimeUnit.NANOSECONDS, null);
    }

    public void dropAllInstancesOfMarker(int i, int i2) {
        long j = (long) i2;
        if (j > 0) {
            j = TimeUnit.MILLISECONDS.toNanos(j);
        }
        A0U(i, j, Process.myTid(), currentMonotonicTimestampNanos(), TimeUnit.NANOSECONDS, null);
    }

    public boolean isMarkerOn(int i) {
        if (A0B(i, this.A07)) {
            C12210op r0 = this.A07;
            if (r0 != null) {
                return r0.Bwk();
            }
            throw new IllegalStateException("We should never get faked value when not in experiment. Having GKs as null means we are not in experiment");
        }
        return C09580i5.A09(this.A04, C09580i5.A00(i, 0), this.A0G);
    }

    public boolean isMarkerOn(int i, int i2) {
        if (A0B(i, this.A07)) {
            C12210op r0 = this.A07;
            if (r0 != null) {
                return r0.Bwk();
            }
            throw new IllegalStateException("We should never get faked value when not in experiment. Having GKs as null means we are not in experiment");
        }
        return C09580i5.A09(this.A04, C09580i5.A00(i, i2), this.A0G);
    }

    public void markerAnnotate(int i, int i2, String str, double d) {
        int i3 = i;
        String str2 = str;
        A0J(i3, i2, str2, d, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, int i3) {
        int i4 = i;
        String str2 = str;
        A0K(i4, i2, str2, i3, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, long j) {
        int i3 = i;
        String str2 = str;
        A0L(i3, i2, str2, j, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, String str2) {
        int i3 = i;
        String str3 = str;
        A0M(i3, i2, str3, str2, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, boolean z) {
        int i3 = i;
        String str2 = str;
        A0N(i3, i2, str2, z, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, double[] dArr) {
        int i3 = i;
        String str2 = str;
        A0O(i3, i2, str2, dArr, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, int[] iArr) {
        int i3 = i;
        String str2 = str;
        A0P(i3, i2, str2, iArr, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, long[] jArr) {
        int i3 = i;
        String str2 = str;
        A0Q(i3, i2, str2, jArr, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, String[] strArr) {
        int i3 = i;
        String str2 = str;
        A0R(i3, i2, str2, strArr, Process.myTid());
    }

    public void markerAnnotate(int i, int i2, String str, boolean[] zArr) {
        int i3 = i;
        String str2 = str;
        A0S(i3, i2, str2, zArr, Process.myTid());
    }

    public void markerAnnotate(int i, String str, double d) {
        int i2 = i;
        A0J(i2, 0, str, d, Process.myTid());
    }

    public void markerAnnotate(int i, String str, int i2) {
        int i3 = i;
        A0K(i3, 0, str, i2, Process.myTid());
    }

    public void markerAnnotate(int i, String str, long j) {
        int i2 = i;
        A0L(i2, 0, str, j, Process.myTid());
    }

    public void markerAnnotate(int i, String str, String str2) {
        int i2 = i;
        A0M(i2, 0, str, str2, Process.myTid());
    }

    public void markerAnnotate(int i, String str, boolean z) {
        int i2 = i;
        A0N(i2, 0, str, z, Process.myTid());
    }

    public void markerAnnotate(int i, String str, double[] dArr) {
        int i2 = i;
        A0O(i2, 0, str, dArr, Process.myTid());
    }

    public void markerAnnotate(int i, String str, int[] iArr) {
        int i2 = i;
        A0P(i2, 0, str, iArr, Process.myTid());
    }

    public void markerAnnotate(int i, String str, long[] jArr) {
        int i2 = i;
        A0Q(i2, 0, str, jArr, Process.myTid());
    }

    public void markerAnnotate(int i, String str, String[] strArr) {
        int i2 = i;
        A0R(i2, 0, str, strArr, Process.myTid());
    }

    public void markerAnnotate(int i, String str, boolean[] zArr) {
        int i2 = i;
        A0S(i2, 0, str, zArr, Process.myTid());
    }

    public void markerCancel(int i) {
        markerCancel(i, 0);
    }

    public void markerCancel(int i, int i2) {
        markerCancel(i, i2, 4);
    }

    public void markerCancel(int i, int i2, short s) {
        markerDrop(i, i2);
    }

    public void markerCancel(int i, short s) {
        markerCancel(i, 0, s);
    }

    public void markerDrop(int i) {
        markerDrop(i, 0);
    }

    public void markerDrop(int i, int i2) {
        A0E(i, i2, Process.myTid(), currentMonotonicTimestampNanos(), TimeUnit.NANOSECONDS);
    }

    public void markerEnd(int i, int i2, short s) {
        int i3 = i;
        markerEnd(i3, i2, s, -1, TimeUnit.NANOSECONDS, null);
    }

    public void markerEnd(int i, int i2, short s, long j) {
        int i3 = i;
        short s2 = s;
        markerEnd(i3, i2, s2, j, TimeUnit.MILLISECONDS);
    }

    public void markerEnd(int i, int i2, short s, long j, TriState triState) {
        int i3 = i;
        markerEnd(i3, i2, s, j, TimeUnit.MILLISECONDS, triState);
    }

    public void markerEnd(int i, int i2, short s, long j, TimeUnit timeUnit) {
        markerEnd(i, i2, s, j, timeUnit, null);
    }

    public void markerEnd(int i, int i2, short s, long j, TimeUnit timeUnit, TriState triState) {
        int i3 = i;
        TimeUnit timeUnit2 = timeUnit;
        A0T(i3, i2, s, j, timeUnit2, triState, null, null, Process.myTid(), null);
    }

    public void markerEnd(int i, short s) {
        markerEnd(i, 0, s, -1, TimeUnit.NANOSECONDS, null);
    }

    public void markerEnd(int i, short s, long j) {
        int i2 = i;
        markerEnd(i2, s, j, TimeUnit.MILLISECONDS);
    }

    public void markerEnd(int i, short s, long j, TimeUnit timeUnit) {
        markerEnd(i, 0, s, j, timeUnit, null);
    }

    public void markerPoint(int i, int i2, int i3, String str, AnonymousClass0lr r15, long j, int i4) {
        int i5 = i2;
        int i6 = i;
        int i7 = i3;
        String str2 = str;
        AnonymousClass0lr r5 = r15;
        markerPoint(i6, i5, i7, str2, r5, j, TimeUnit.MILLISECONDS, i4);
    }

    public void markerPoint(int i, int i2, int i3, String str, AnonymousClass0lr r17, long j, TimeUnit timeUnit, int i4) {
        AnonymousClass0lr r5 = r17;
        if (r17 == null) {
            r5 = null;
        } else {
            r5.A03 = true;
        }
        String str2 = str;
        int i5 = i2;
        TimeUnit timeUnit2 = timeUnit;
        int i6 = i;
        int i7 = i3;
        A0F(i6, i5, i7, str2, r5, null, j, timeUnit2, i4, Process.myTid());
    }

    public void markerPoint(int i, int i2, String str) {
        int i3 = i;
        markerPoint(i3, i2, str, (String) null, -1, TimeUnit.NANOSECONDS);
    }

    public void markerPoint(int i, int i2, String str, long j) {
        int i3 = i;
        String str2 = str;
        markerPoint(i3, i2, str2, j, TimeUnit.MILLISECONDS);
    }

    public void markerPoint(int i, int i2, String str, long j, TimeUnit timeUnit) {
        markerPoint(i, i2, str, (String) null, j, timeUnit);
    }

    public void markerPoint(int i, int i2, String str, String str2) {
        int i3 = i;
        String str3 = str;
        markerPoint(i3, i2, str3, str2, -1, TimeUnit.NANOSECONDS);
    }

    public void markerPoint(int i, int i2, String str, String str2, long j) {
        int i3 = i;
        String str3 = str2;
        markerPoint(i3, i2, str, str3, j, TimeUnit.MILLISECONDS);
    }

    public void markerPoint(int i, int i2, String str, String str2, long j, int i3) {
        int i4 = i;
        markerPoint(i4, i2, str, str2, j, TimeUnit.MILLISECONDS, i3);
    }

    public void markerPoint(int i, int i2, String str, String str2, long j, TimeUnit timeUnit) {
        int i3 = i;
        if (!A09(i, this.A07)) {
            markerPoint(i3, i2, str, str2, j, timeUnit, 0);
        }
    }

    public void markerPoint(int i, int i2, String str, String str2, long j, TimeUnit timeUnit, int i3) {
        int i4 = i2;
        int i5 = i;
        String str3 = str;
        String str4 = str2;
        long j2 = j;
        A0G(i5, i4, 7, str3, str4, null, j2, timeUnit, i3, Process.myTid());
    }

    public void markerPoint(int i, String str) {
        markerPoint(i, 0, str, (String) null, -1, TimeUnit.NANOSECONDS);
    }

    public void markerPoint(int i, String str, long j) {
        int i2 = i;
        markerPoint(i2, str, j, TimeUnit.MILLISECONDS);
    }

    public void markerPoint(int i, String str, long j, TimeUnit timeUnit) {
        markerPoint(i, 0, str, (String) null, j, timeUnit);
    }

    public void markerPoint(int i, String str, String str2) {
        int i2 = i;
        markerPoint(i2, 0, str, str2, -1, TimeUnit.NANOSECONDS);
    }

    public void markerPoint(int i, String str, String str2, long j) {
        int i2 = i;
        String str3 = str2;
        markerPoint(i2, str, str3, j, TimeUnit.MILLISECONDS);
    }

    public void markerPoint(int i, String str, String str2, long j, TimeUnit timeUnit) {
        markerPoint(i, 0, str, str2, j, timeUnit);
    }

    public void markerStart(int i) {
        A0I(i, 0, -1, TimeUnit.NANOSECONDS, null, false, false, null, null, null, true, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStart(int i, int i2) {
        A0I(i, i2, -1, TimeUnit.NANOSECONDS, null, false, false, null, null, null, true, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStart(int i, int i2, long j) {
        int i3 = i;
        markerStart(i3, i2, j, TimeUnit.MILLISECONDS);
    }

    public void markerStart(int i, int i2, long j, TimeUnit timeUnit) {
        int i3 = i;
        long j2 = j;
        A0I(i3, i2, j2, timeUnit, null, false, false, null, null, null, true, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStart(int i, int i2, long j, TimeUnit timeUnit, boolean z) {
        int i3 = i;
        TimeUnit timeUnit2 = timeUnit;
        A0I(i3, i2, j, timeUnit2, null, z, false, null, null, null, true, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStart(int i, int i2, long j, boolean z) {
        int i3 = i;
        markerStart(i3, i2, j, TimeUnit.MILLISECONDS, z);
    }

    public void markerStart(int i, int i2, String str, String str2) {
        markerStart(i, i2);
        markerAnnotate(i, i2, str, str2);
    }

    public void markerStart(int i, int i2, String str, String str2, long j) {
        int i3 = i;
        String str3 = str2;
        markerStart(i3, i2, str, str3, j, TimeUnit.MILLISECONDS);
    }

    public void markerStart(int i, int i2, String str, String str2, long j, TimeUnit timeUnit) {
        markerStart(i, i2, j, timeUnit);
        markerAnnotate(i, i2, str, str2);
    }

    public void markerStart(int i, int i2, boolean z) {
        int i3 = i;
        A0I(i3, i2, -1, TimeUnit.NANOSECONDS, null, false, false, null, null, null, z, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStart(int i, String str, String str2) {
        markerStart(i);
        markerAnnotate(i, str, str2);
    }

    public void markerStart(int i, String str, String str2, long j) {
        int i2 = i;
        String str3 = str2;
        markerStart(i2, str, str3, j, TimeUnit.MILLISECONDS);
    }

    public void markerStart(int i, String str, String str2, long j, TimeUnit timeUnit) {
        markerStart(i, 0, j, timeUnit);
        markerAnnotate(i, str, str2);
    }

    public void markerStart(int i, boolean z) {
        A0I(i, 0, -1, TimeUnit.NANOSECONDS, null, false, false, null, null, null, z, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStartWithCancelPolicy(int i, boolean z, int i2, long j) {
        int i3 = i;
        int i4 = i2;
        markerStartWithCancelPolicy(i3, z, i4, j, TimeUnit.MILLISECONDS);
    }

    public void markerStartWithCancelPolicy(int i, boolean z, int i2, long j, TimeUnit timeUnit) {
        int i3 = i;
        int i4 = i2;
        TimeUnit timeUnit2 = timeUnit;
        long j2 = j;
        A0I(i3, i4, j2, timeUnit2, null, false, false, null, null, null, z, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStartWithCounter(int i, int i2) {
        A0I(i, i2, -1, TimeUnit.NANOSECONDS, null, false, true, null, null, null, true, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerStartWithCounter(int i, int i2, long j) {
        int i3 = i2;
        A0I(i, i3, j, TimeUnit.MILLISECONDS, null, false, true, null, null, null, true, Process.myTid(), 0, true, 0, this.A0G);
    }

    public void markerTag(int i, int i2, String str) {
        if (!A09(i, this.A07)) {
            this.A04.A0H(i, i2, str, this.A0G);
        }
    }

    public void markerTag(int i, String str) {
        if (!A09(i, this.A07)) {
            this.A04.A0H(i, 0, str, this.A0G);
        }
    }

    public C21061Ew withMarker(int i) {
        return withMarker(i, 0);
    }

    public C21061Ew withMarker(int i, int i2) {
        if (A09(i, this.A07)) {
            return AnonymousClass1F4.A00;
        }
        C09580i5 r2 = this.A04;
        C09160gd r1 = this.A0G;
        synchronized (r2.A04) {
            C04270Tg A042 = C09580i5.A04(r2, C09580i5.A00(i, i2));
            if (C09580i5.A0A(A042, r1)) {
                C09150gc r12 = r2.A03;
                Object obj = r12.A00.get();
                if (obj != null) {
                    r12.A00.set(null);
                } else {
                    obj = r12.A00();
                }
                C21021Es r22 = (C21021Es) obj;
                r22.A06 = A042.A0b.get();
                r22.A08 = A042;
                r22.A07 = 7;
                return r22;
            }
            AnonymousClass1F4 r23 = AnonymousClass1F4.A00;
            return r23;
        }
    }
}
