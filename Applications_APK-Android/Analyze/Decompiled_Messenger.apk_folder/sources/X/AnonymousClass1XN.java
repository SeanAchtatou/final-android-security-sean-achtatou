package X;

/* renamed from: X.1XN  reason: invalid class name */
public final class AnonymousClass1XN implements Runnable {
    public static final String __redex_internal_original_name = "androidx.lifecycle.ProcessLifecycleOwner$1";
    public final /* synthetic */ AnonymousClass1XJ A00;

    public AnonymousClass1XN(AnonymousClass1XJ r1) {
        this.A00 = r1;
    }

    public void run() {
        AnonymousClass1XJ r1 = this.A00;
        if (r1.A00 == 0) {
            r1.A05 = true;
            r1.A07.A08(C14820uB.ON_PAUSE);
        }
        AnonymousClass1XJ r2 = this.A00;
        if (r2.A01 == 0 && r2.A05) {
            r2.A07.A08(C14820uB.ON_STOP);
            r2.A06 = true;
        }
    }
}
