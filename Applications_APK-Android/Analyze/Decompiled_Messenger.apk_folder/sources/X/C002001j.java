package X;

import android.util.Pair;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.01j  reason: invalid class name and case insensitive filesystem */
public final class C002001j {
    public int A00;
    public AnonymousClass0Nd A01;
    public List A02;

    public void A00(String str, Throwable th) {
        AnonymousClass0Nd r0;
        synchronized (this) {
            int i = this.A00;
            if (i > 0) {
                this.A00 = i - 1;
                r0 = this.A01;
                if (r0 == null) {
                    if (this.A02 == null) {
                        this.A02 = new ArrayList();
                    }
                    this.A02.add(Pair.create(str, th));
                }
            }
            r0 = null;
        }
        if (r0 != null) {
            AnonymousClass07A.A04(r0.A01, new AnonymousClass0Js(r0, th, str), 1111228372);
        }
    }

    public C002001j(int i) {
        this.A00 = i;
    }
}
