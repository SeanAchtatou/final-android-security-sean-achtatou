package X;

import android.util.SparseArray;
import java.util.HashSet;

/* renamed from: X.0G9  reason: invalid class name */
public final class AnonymousClass0G9 extends HashSet<String> {
    public AnonymousClass0G9(int i) {
        super(i);
        SparseArray sparseArray = AnonymousClass0AI.A00;
        add(sparseArray.get(46));
        add(sparseArray.get(47));
        add(sparseArray.get(69));
        add(sparseArray.get(70));
        add(sparseArray.get(95));
        add(sparseArray.get(96));
        add(sparseArray.get(59));
        add(sparseArray.get(97));
        add(sparseArray.get(76));
        add(sparseArray.get(137));
        add(sparseArray.get(79));
        add(sparseArray.get(80));
    }
}
