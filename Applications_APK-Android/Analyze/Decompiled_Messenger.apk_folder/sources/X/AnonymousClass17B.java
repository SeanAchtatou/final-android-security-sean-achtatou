package X;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

/* renamed from: X.17B  reason: invalid class name */
public final class AnonymousClass17B extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$34";
    public final /* synthetic */ C190716r A00;
    public final /* synthetic */ ArrayList A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass17B(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, ArrayList arrayList) {
        super(executorService, r3, str);
        this.A00 = r1;
        this.A01 = arrayList;
    }
}
