package X;

import android.content.Context;
import android.graphics.Rect;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.rtc.services.RtcVideoChatHeadService;

/* renamed from: X.22W  reason: invalid class name */
public final class AnonymousClass22W implements View.OnTouchListener {
    public static final AnonymousClass3E8 A0E = AnonymousClass3E8.A01(40.0d, 7.0d);
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public GestureDetector A06;
    public AnonymousClass3E6 A07;
    public AnonymousClass3E6 A08;
    public boolean A09;
    public boolean A0A;
    public final Context A0B;
    public final C162167ew A0C;
    public final C64873Dz A0D;

    public static int A00(AnonymousClass22W r1, int i) {
        Rect A042 = RtcVideoChatHeadService.A04(r1.A0C.A00);
        return Math.min(A042.bottom, Math.max(A042.top, i));
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!(!this.A0C.A00.A0d)) {
            return false;
        }
        float rawX = motionEvent.getRawX() - motionEvent.getX();
        float rawY = motionEvent.getRawY() - motionEvent.getY();
        motionEvent.offsetLocation(rawX, rawY);
        boolean onTouchEvent = this.A06.onTouchEvent(motionEvent);
        motionEvent.offsetLocation(-rawX, -rawY);
        int action = motionEvent.getAction();
        if (action == 0) {
            RtcVideoChatHeadService.A0C(this.A0C.A00);
            return onTouchEvent;
        } else if (action == 1 ? onTouchEvent : action != 3) {
            return onTouchEvent;
        } else {
            RtcVideoChatHeadService.A0I(this.A0C.A00);
            return onTouchEvent;
        }
    }

    public AnonymousClass22W(AnonymousClass1XY r2, C162167ew r3) {
        this.A0D = C64873Dz.A00(r2);
        this.A0B = AnonymousClass1YA.A00(r2);
        this.A0C = r3;
    }
}
