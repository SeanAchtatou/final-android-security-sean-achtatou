package X;

/* renamed from: X.1b5  reason: invalid class name and case insensitive filesystem */
public final class C26211b5 {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A06.A09("2gempathy");
        A05 = r1;
        A06 = (AnonymousClass1Y7) r1.A09("enabled");
        AnonymousClass1Y7 r12 = A05;
        A02 = (AnonymousClass1Y7) r12.A09("last_enabled");
        A03 = (AnonymousClass1Y7) r12.A09("last_expired");
        A04 = (AnonymousClass1Y7) r12.A09("last_wall");
        A01 = (AnonymousClass1Y7) r12.A09("last_auto_on_time");
        A00 = (AnonymousClass1Y7) r12.A09("auto_on_count");
    }
}
