package X;

import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.1eb  reason: invalid class name and case insensitive filesystem */
public final class C28311eb {
    public static final C10030jR[] NO_TYPES = new C10030jR[0];
    public static final C10030jR UNBOUND = new C10020jP(Object.class);
    public Map _bindings;
    public final Class _contextClass;
    public final C10030jR _contextType;
    private final C28311eb _parentBindings;
    public HashSet _placeholders;
    public final C10300js _typeFactory;

    public static void _resolve(C28311eb r4) {
        int containedTypeCount;
        r4._resolveBindings(r4._contextClass);
        C10030jR r0 = r4._contextType;
        if (r0 != null && (containedTypeCount = r0.containedTypeCount()) > 0) {
            for (int i = 0; i < containedTypeCount; i++) {
                C10030jR r02 = r4._contextType;
                r4.addBinding(r02.containedTypeName(i), r02.containedType(i));
            }
        }
        if (r4._bindings == null) {
            r4._bindings = Collections.emptyMap();
        }
    }

    private void _resolveBindings(Type type) {
        Class cls;
        int length;
        int length2;
        if (type != null) {
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                if (actualTypeArguments != null && (length2 = actualTypeArguments.length) > 0) {
                    Class cls2 = (Class) parameterizedType.getRawType();
                    TypeVariable[] typeParameters = cls2.getTypeParameters();
                    int length3 = typeParameters.length;
                    if (length3 == length2) {
                        for (int i = 0; i < length2; i++) {
                            String name = typeParameters[i].getName();
                            Map map = this._bindings;
                            if (map == null) {
                                this._bindings = new LinkedHashMap();
                            } else if (map.containsKey(name)) {
                            }
                            _addPlaceholder(name);
                            this._bindings.put(name, this._typeFactory._constructType(actualTypeArguments[i], this));
                        }
                    } else {
                        throw new IllegalArgumentException("Strange parametrized type (in class " + cls2.getName() + "): number of type arguments != number of type parameters (" + length2 + " vs " + length3 + ")");
                    }
                }
                cls = (Class) parameterizedType.getRawType();
            } else if (type instanceof Class) {
                cls = (Class) type;
                Class<?> declaringClass = cls.getDeclaringClass();
                if (declaringClass != null && !declaringClass.isAssignableFrom(cls)) {
                    _resolveBindings(cls.getDeclaringClass());
                }
                TypeVariable[] typeParameters2 = cls.getTypeParameters();
                if (typeParameters2 != null && (length = typeParameters2.length) > 0) {
                    C10030jR[] r8 = null;
                    C10030jR r0 = this._contextType;
                    if (r0 != null && cls.isAssignableFrom(r0._class)) {
                        r8 = this._typeFactory.findTypeParameters(this._contextType, cls);
                    }
                    for (int i2 = 0; i2 < length; i2++) {
                        TypeVariable typeVariable = typeParameters2[i2];
                        String name2 = typeVariable.getName();
                        Type type2 = typeVariable.getBounds()[0];
                        if (type2 != null) {
                            Map map2 = this._bindings;
                            if (map2 == null) {
                                this._bindings = new LinkedHashMap();
                            } else if (map2.containsKey(name2)) {
                            }
                            _addPlaceholder(name2);
                            if (r8 != null) {
                                this._bindings.put(name2, r8[i2]);
                            } else {
                                this._bindings.put(name2, this._typeFactory._constructType(type2, this));
                            }
                        }
                    }
                }
            } else {
                return;
            }
            _resolveBindings(cls.getGenericSuperclass());
            for (Type _resolveBindings : cls.getGenericInterfaces()) {
                _resolveBindings(_resolveBindings);
            }
        }
    }

    public void _addPlaceholder(String str) {
        if (this._placeholders == null) {
            this._placeholders = new HashSet();
        }
        this._placeholders.add(str);
    }

    public void addBinding(String str, C10030jR r3) {
        Map map = this._bindings;
        if (map == null || map.size() == 0) {
            this._bindings = new LinkedHashMap();
        }
        this._bindings.put(str, r3);
    }

    public C10030jR findType(String str) {
        String name;
        if (this._bindings == null) {
            _resolve(this);
        }
        C10030jR r0 = (C10030jR) this._bindings.get(str);
        if (r0 != null) {
            return r0;
        }
        HashSet hashSet = this._placeholders;
        if (hashSet == null || !hashSet.contains(str)) {
            C28311eb r02 = this._parentBindings;
            if (r02 != null) {
                return r02.findType(str);
            }
            Class cls = this._contextClass;
            if (cls == null || cls.getEnclosingClass() == null || Modifier.isStatic(this._contextClass.getModifiers())) {
                Class cls2 = this._contextClass;
                if (cls2 == null) {
                    C10030jR r03 = this._contextType;
                    if (r03 != null) {
                        name = r03.toString();
                    } else {
                        name = "UNKNOWN";
                    }
                } else {
                    name = cls2.getName();
                }
                throw new IllegalArgumentException(AnonymousClass08S.A0T("Type variable '", str, "' can not be resolved (with context of class ", name, ")"));
            }
        }
        return UNBOUND;
    }

    public String toString() {
        if (this._bindings == null) {
            _resolve(this);
        }
        StringBuilder sb = new StringBuilder("[TypeBindings for ");
        C10030jR r0 = this._contextType;
        if (r0 != null) {
            sb.append(r0.toString());
        } else {
            sb.append(this._contextClass.getName());
        }
        sb.append(": ");
        sb.append(this._bindings);
        sb.append("]");
        return sb.toString();
    }

    public C28311eb(C10300js r1, C28311eb r2, Class cls, C10030jR r4) {
        this._typeFactory = r1;
        this._parentBindings = r2;
        this._contextClass = cls;
        this._contextType = r4;
    }
}
