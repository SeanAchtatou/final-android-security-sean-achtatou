package X;

import com.facebook.acra.ACRA;
import com.facebook.attribution.AttributionStateSerializer;
import com.facebook.forker.Process;
import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.1ZR  reason: invalid class name */
public final class AnonymousClass1ZR extends AnonymousClass1ZU {
    private static volatile AnonymousClass1ZR A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass1ZR A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1ZR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1ZR(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public boolean A05(int i, C25051Yd r18) {
        String str = "com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration";
        C25051Yd r13 = r18;
        switch (i) {
            case 0:
            case 1:
                if (A02("com.facebook.accessibility.logging.TouchExplorationStateChangeDetector", r13)) {
                    return true;
                }
                return false;
            case 2:
            case 3:
                if (A02("com.facebook.analytics.counterlogger.CommunicationScheduler", r13)) {
                    return true;
                }
                return false;
            case 4:
                str = "com.facebook.analytics.mobileconfigreliability.MobileConfigSampledAccessListenerImpl";
                break;
            case 5:
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                if (A02("com.facebook.analytics.timespent.TimeSpentEventReporter", r13)) {
                    return true;
                }
                return false;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                str = "com.facebook.apk_testing.ApkTestingExposureLogger";
                break;
            case 8:
                str = "com.facebook.attribution.LatStatusJob";
                break;
            case Process.SIGKILL:
            case AnonymousClass1Y3.A01:
            case AnonymousClass1Y3.A02:
                if (A02("com.facebook.battery.cpuspin.di.FbCpuSpinScheduler", r13)) {
                    return true;
                }
                return false;
            case AnonymousClass1Y3.A03:
            case 13:
                if (A02("com.facebook.battery.instrumentation.BatteryMetricsReporter", r13)) {
                    return true;
                }
                return false;
            case 14:
                str = "com.facebook.battery.samsung.SamsungWarningNotificationLogger";
                break;
            case 15:
            case 16:
                if (A02("com.facebook.common.activitycleaner.ActivityStackManager", r13)) {
                    return true;
                }
                return false;
            case 17:
            case Process.SIGCONT:
                if (A02("com.facebook.common.activitycleaner.ActivityStackResetter", r13)) {
                    return true;
                }
                return false;
            case Process.SIGSTOP:
            case 20:
                if (A02("com.facebook.common.combinedthreadpool.asyncinit.CombinedThreadPoolLoggerAppStateListener", r13)) {
                    return true;
                }
                return false;
            case AnonymousClass1Y3.A05:
                str = "com.facebook.common.connectionstatus.FbDataConnectionManager";
                break;
            case AnonymousClass1Y3.A06:
                str = "com.facebook.common.errorreporting.memory.LeakMemoryDumper";
                break;
            case 23:
            case AnonymousClass1Y3.A07:
                break;
            case 25:
            case AnonymousClass1Y3.A08:
                str = "com.facebook.common.memory.leaklistener.MemoryLeakListener";
                break;
            case AnonymousClass1Y3.A09:
                str = "com.facebook.common.memory.manager.MemoryManager";
                break;
            case 28:
                str = "com.facebook.common.memory.LargeHeapOverrideConfig";
                break;
            case 29:
                str = "com.facebook.common.netchecker.NetChecker";
                break;
            case AnonymousClass1Y3.A0A:
                str = "com.facebook.common.noncriticalinit.NonCriticalInitializer";
                break;
            case AnonymousClass1Y3.A0B:
                str = "com.facebook.common.userinteraction.UserInteractionHistory";
                break;
            case 32:
                str = "com.facebook.compactdiskmodule.CompactDiskFlushDispatcher";
                break;
            case 33:
            case AnonymousClass1Y3.A0C:
                str = "com.facebook.conditionalworker.ConditionalWorkerManager";
                break;
            case 35:
                str = "com.facebook.config.background.impl.ConfigurationConditionalWorkerInfo";
                break;
            case 36:
                str = "com.facebook.device.resourcemonitor.ResourceManager";
                break;
            case AnonymousClass1Y3.A0D:
            case AnonymousClass1Y3.A0E:
                str = "com.facebook.device.resourcemonitor.ResourceMonitor";
                break;
            case AnonymousClass1Y3.A0F:
                str = "com.facebook.device.resourcemonitor.activemonitoring.ResourceManagerActiveMonitoring";
                break;
            case AnonymousClass1Y3.A0G:
                str = "com.facebook.device_id.UniqueFamilyDeviceIdBroadcastSender";
                break;
            case AnonymousClass1Y3.A0H:
                str = "com.facebook.diskfootprint.cleaner.FileCleaner";
                break;
            case 42:
            case 43:
                str = "com.facebook.entitypresence.EntityPresenceManager";
                break;
            case AnonymousClass1Y3.A0I:
                str = "com.facebook.fling.analytics.FlingProfileLogger";
                break;
            case AnonymousClass1Y3.A0J:
                str = "com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver";
                break;
            case AnonymousClass1Y3.A0K:
            case AnonymousClass1Y3.A0L:
                str = "com.facebook.funnellogger.FunnelLoggerImpl";
                break;
            case 48:
                str = "com.facebook.graphql.executor.OfflineMutationsManager";
                break;
            case 49:
                str = "com.facebook.graphql.fleetbeacon.FleetBeaconStartupTrigger";
                break;
            case 50:
                str = "com.facebook.graphql.modelutil.parcel.ModelParcelHelperInitQPLAppJob";
                break;
            case AnonymousClass1Y3.A0M:
                str = "com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl";
                break;
            case AnonymousClass1Y3.A0N:
                str = "com.facebook.growth.sem.SemColdStartLogger";
                break;
            case AnonymousClass1Y3.A0O:
            case 54:
            case 55:
                if (A02("com.facebook.http.networkstatelogger.NetworkStateLogger", r13)) {
                    return true;
                }
                return false;
            case AnonymousClass1Y3.A0P:
                str = "com.facebook.interstitial.manager.InterstitialDataCleaner";
                break;
            case 57:
            case AnonymousClass1Y3.A0Q:
                str = "com.facebook.keyframes.fb.FbKeyframesAppStateManager";
                break;
            case 59:
            case AnonymousClass1Y3.A0R:
                str = "com.facebook.location.foreground.ForegroundLocationFrameworkController";
                break;
            case 61:
            case 62:
                str = "com.facebook.memory.fbmemorymanager.FBMemoryManagerHooks";
                break;
            case AnonymousClass1Y3.A0S:
            case 64:
                str = "com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener";
                break;
            case AnonymousClass1Y3.A0T:
                str = "com.facebook.messaging.analytics.perf.MessagingInteractionStateManager";
                break;
            case 66:
                str = "com.facebook.messaging.analytics.perf.PostStartupTracker";
                break;
            case 67:
                str = "com.facebook.messaging.chatheads.ChatHeadsInitializer";
                break;
            case AnonymousClass1Y3.A0U:
                str = "com.facebook.messaging.chatheads.service.VideoServiceAppStateListener";
                break;
            case 69:
            case 70:
                str = "com.facebook.messaging.contacts.loader.nonwork.ContactObserversRegistrationHandler";
                break;
            case AnonymousClass1Y3.A0V:
                str = "com.facebook.messaging.contactstab.loader.StatusController";
                break;
            case 72:
                str = "com.facebook.messaging.cowatch.tracker.LivingRoomThreadTracker";
                break;
            case AnonymousClass1Y3.A0W:
                str = "com.facebook.messaging.filelogger.MessagingFileLogger";
                break;
            case 74:
                str = "com.facebook.messaging.inbox2.activenow.loader.PrefetcherManager";
                break;
            case AnonymousClass1Y3.A0X:
                str = "com.facebook.messaging.instagram.contactimport.InstagramContactImportBadgingController.InstagramContactImprtTriggerRegistration";
                break;
            case AnonymousClass1Y3.A0Y:
                str = "com.facebook.messaging.instagram.fetch.EligibleInstagramAccountBackgroundFetcher";
                break;
            case AnonymousClass1Y3.A0Z:
                str = "com.facebook.messaging.notify.logging.conditionalworkerimpl.PushSettingsReporter";
                break;
            case 78:
            case 79:
                str = "com.facebook.messaging.screenshotdetection.ThreadScreenshotDetector";
                break;
            case AnonymousClass1Y3.A0a:
                str = "com.facebook.messaging.searchnullstate.PrefetcherManager";
                break;
            case AnonymousClass1Y3.A0b:
                str = "com.facebook.messaging.selfupdate2.SelfUpdate2AppStateListener";
                break;
            case 82:
                str = "com.facebook.mobileconfig.init.MobileConfigApi2LoggerImpl";
                break;
            case 83:
                str = "com.facebook.perf.startupstatemachine.StartupStateMachine";
                break;
            case AnonymousClass1Y3.A0c:
                str = "com.facebook.privacypermissionsnapshots.fb.PrivacyPermissionSnapshotsLoggerController";
                break;
            case 85:
                str = "com.facebook.push.crossapp.PendingReportedPackages";
                break;
            case 86:
            case 87:
            case 88:
            case AnonymousClass1Y3.A0d:
                if (A02("com.facebook.push.mqtt.service.MqttClientStateManager", r13)) {
                    return true;
                }
                return false;
            case AnonymousClass1Y3.A0e:
                str = "com.facebook.quicklog.dataproviders.IoStatsProvider";
                break;
            case 91:
            case 92:
                str = "com.facebook.reactivesocket.AndroidLifecycleHandler";
                break;
            case 93:
                str = "com.facebook.resources.impl.DrawableCounterLogger";
                break;
            case AnonymousClass1Y3.A0f:
                str = "com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector";
                break;
            case 95:
            case AnonymousClass1Y3.A0g:
                str = "com.facebook.storage.bigfoot.apps.fbapps.FBAppsBigFootForegroundWorker";
                break;
            case 97:
                str = "com.facebook.storage.cask.fbapps.controllers.FBEvictionPluginController";
                break;
            case 98:
                str = "com.facebook.storage.cask.fbapps.controllers.FBMaxSizePluginController";
                break;
            case 99:
                str = "com.facebook.storage.cask.fbapps.controllers.FBStaleRemovalPluginController";
                break;
            case 100:
                str = "com.facebook.storage.cleaner.PathSizeOverflowCleaner";
                break;
            case AnonymousClass1Y3.A0i:
            case AnonymousClass1Y3.A0j:
                str = "com.facebook.storage.diskio.ProcIOStatsOverallReporting";
                break;
            case 103:
            case AnonymousClass1Y3.A0k:
                str = "com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor";
                break;
            case AnonymousClass1Y3.A0l:
                str = "com.facebook.storage.trash.FbTrashManager";
                break;
            case AnonymousClass1Y3.A0m:
            case AnonymousClass1Y3.A0n:
            case 108:
                if (A02("com.facebook.surfaces.fb.PrewarmingJobsQueue", r13)) {
                    return true;
                }
                return false;
            case AnonymousClass1Y3.A0o:
            case AnonymousClass1Y3.A0p:
                str = "com.facebook.sync.SyncInitializer";
                break;
            case 111:
            case 112:
                str = "com.facebook.tigon.nativeservice.common.NativePlatformContextHolder";
                break;
            case AnonymousClass1Y3.A0q:
            case 114:
            case 115:
            case 116:
                if (A02("com.facebook.tigon.reliablemedia.ReliableMediaMonitor", r13)) {
                    return true;
                }
                return false;
            case AnonymousClass1Y3.A0r:
            case 118:
                str = "com.facebook.tigon.tigonliger.TigonLigerService";
                break;
            case AnonymousClass1Y3.A0s:
                str = "com.facebook.ui.titlebar.abtest.WhiteChromeActivityStack";
                break;
            case AnonymousClass1Y3.A0t:
            case AnonymousClass1Y3.A0u:
                str = "com.facebook.uievaluations.uievaluationsrunner.UIEvaluationsRunner";
                break;
            case AnonymousClass1Y3.A0v:
            case 123:
                str = AnonymousClass80H.$const$string(19);
                break;
            case 124:
                str = "com.facebook.video.exoserviceclient.HeroServiceInitializerAppJobInit";
                break;
            case AnonymousClass1Y3.A0w:
            case AnonymousClass1Y3.A0x:
                str = "com.facebook.video.fbgrootplayer.VideoAudioVolumeObserversManager";
                break;
            case 127:
                str = "com.facebook.video.plugins.AutoplayIntentSignalMonitor";
                break;
            case 128:
                str = "com.facebook.voltron.fbdownloader.FbAppJobScheduledPrefetcher";
                break;
            case 129:
            case AnonymousClass1Y3.A0y:
            case AnonymousClass1Y3.A0z:
            case 132:
                if (A02("com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs", r13)) {
                    return true;
                }
                return false;
            case AnonymousClass1Y3.A10:
                str = "com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler";
                break;
            case 134:
            case AnonymousClass1Y3.A11:
                str = "com.facebook.xanalytics.provider.NativeXAnalyticsProvider";
                break;
            case AnonymousClass1Y3.A12:
                str = "com.facebook.zero.carriersignal.CarrierSignalController";
                break;
            case 137:
            case AnonymousClass1Y3.A13:
                str = "com.facebook.zero.cms.ZeroCmsUtil";
                break;
            case 139:
            case AnonymousClass1Y3.A14:
                str = "com.facebook.zero.header.ZeroHeaderRequestManager.LocalZeroHeaderRequestManagerReceiverRegistration";
                break;
            case AnonymousClass1Y3.A15:
                str = "com.facebook.zero.paidbalance.PaidBalanceController";
                break;
            case AnonymousClass1Y3.A16:
                str = "com.facebook.zero.LocalZeroTokenManagerReceiverRegistration";
                break;
            case 143:
                str = "com.facebook.zero.service.ZeroInterstitialEligibilityManager";
                break;
            case AnonymousClass1Y3.A17:
                str = "com.facebook.profilo.blackbox.breakpad.BreakpadDumpProcessJob";
                break;
            case 145:
            case 146:
                str = "com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager";
                break;
            case AnonymousClass1Y3.A18:
                str = "com.facebook.profilo.mmapbuf.MmapBufferProcessJob";
                break;
            default:
                return false;
        }
        if (A02(str, r13)) {
            return true;
        }
        return false;
    }

    private AnonymousClass1ZR(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public static void A01(FbSharedPreferences fbSharedPreferences, int i) {
        C30281hn edit = fbSharedPreferences.edit();
        AnonymousClass1Y7 A002 = AttributionStateSerializer.A00(i);
        if (fbSharedPreferences.BBh(A002)) {
            edit.C1B(A002);
            edit.commit();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        if (r3.equals("com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0044, code lost:
        if (r3.equals("com.facebook.battery.cpuspin.di.FbCpuSpinScheduler") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        if (r3.equals("com.facebook.messaging.filelogger.MessagingFileLogger") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005a, code lost:
        if (r3.equals("com.facebook.messaging.notify.logging.conditionalworkerimpl.PushSettingsReporter") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0064, code lost:
        if (r3.equals("com.facebook.diskfootprint.cleaner.FileCleaner") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006f, code lost:
        if (r3.equals("com.facebook.zero.cms.ZeroCmsUtil") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007a, code lost:
        if (r3.equals("com.facebook.zero.header.ZeroHeaderRequestManager.LocalZeroHeaderRequestManagerReceiverRegistration") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0084, code lost:
        if (r3.equals("com.facebook.accessibility.logging.TouchExplorationStateChangeDetector") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
        if (r3.equals("com.facebook.push.mqtt.service.MqttClientStateManager") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009a, code lost:
        if (r3.equals("com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a5, code lost:
        if (r3.equals("com.facebook.device.resourcemonitor.activemonitoring.ResourceManagerActiveMonitoring") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b1, code lost:
        if (r3.equals("com.facebook.interstitial.manager.InterstitialDataCleaner") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00bc, code lost:
        if (r3.equals("com.facebook.common.memory.LargeHeapOverrideConfig") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00c7, code lost:
        if (r3.equals("com.facebook.common.memory.manager.MemoryManager") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d3, code lost:
        if (r3.equals("com.facebook.video.plugins.AutoplayIntentSignalMonitor") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00df, code lost:
        if (r3.equals("com.facebook.messaging.chatheads.ChatHeadsInitializer") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00eb, code lost:
        if (r3.equals("com.facebook.video.exoserviceclient.HeroServiceInitializerAppJobInit") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f7, code lost:
        if (r3.equals("com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0103, code lost:
        if (r3.equals("com.facebook.messaging.inbox2.activenow.loader.PrefetcherManager") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x010f, code lost:
        if (r3.equals("com.facebook.privacypermissionsnapshots.fb.PrivacyPermissionSnapshotsLoggerController") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x011b, code lost:
        if (r3.equals("com.facebook.tigon.reliablemedia.ReliableMediaMonitor") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0127, code lost:
        if (r3.equals("com.facebook.messaging.chatheads.service.VideoServiceAppStateListener") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0133, code lost:
        if (r3.equals("com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x013f, code lost:
        if (r3.equals("com.facebook.messaging.contactstab.loader.StatusController") == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x014a, code lost:
        if (r3.equals("com.facebook.http.networkstatelogger.NetworkStateLogger") == false) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean A02(java.lang.String r3, X.C25051Yd r4) {
        /*
            int r0 = r3.hashCode()
            r1 = 1
            switch(r0) {
                case -2099007165: goto L_0x0143;
                case -1965750728: goto L_0x0137;
                case -1941835534: goto L_0x012b;
                case -1497149617: goto L_0x011f;
                case -1487473140: goto L_0x0113;
                case -1460988741: goto L_0x0107;
                case -1297766007: goto L_0x00fb;
                case -793461576: goto L_0x00ef;
                case -755384689: goto L_0x00e3;
                case -670493985: goto L_0x00d7;
                case -620335881: goto L_0x00cb;
                case -490574106: goto L_0x00c0;
                case -349979888: goto L_0x00b5;
                case 83729976: goto L_0x00a9;
                case 462025196: goto L_0x009e;
                case 843995959: goto L_0x0093;
                case 918691125: goto L_0x0087;
                case 941573401: goto L_0x007d;
                case 995582049: goto L_0x0072;
                case 1193461523: goto L_0x0067;
                case 1338411451: goto L_0x005d;
                case 1590343159: goto L_0x0052;
                case 1620117619: goto L_0x0047;
                case 1676404886: goto L_0x003d;
                case 2014459781: goto L_0x0032;
                default: goto L_0x0008;
            }
        L_0x0008:
            r2 = -1
        L_0x0009:
            switch(r2) {
                case 0: goto L_0x01f6;
                case 1: goto L_0x01e1;
                case 2: goto L_0x01d7;
                case 3: goto L_0x01cd;
                case 4: goto L_0x01d7;
                case 5: goto L_0x01c4;
                case 6: goto L_0x0025;
                case 7: goto L_0x01ba;
                case 8: goto L_0x01b0;
                case 9: goto L_0x01fb;
                case 10: goto L_0x01cd;
                case 11: goto L_0x01ab;
                case 12: goto L_0x01a1;
                case 13: goto L_0x0198;
                case 14: goto L_0x01cd;
                case 15: goto L_0x018e;
                case 16: goto L_0x000d;
                case 17: goto L_0x0184;
                case 18: goto L_0x017a;
                case 19: goto L_0x0170;
                case 20: goto L_0x0164;
                case 21: goto L_0x01d7;
                case 22: goto L_0x0158;
                case 23: goto L_0x01b0;
                case 24: goto L_0x014e;
                default: goto L_0x000c;
            }
        L_0x000c:
            return r1
        L_0x000d:
            r0 = 285207303296513(0x1036500001601, double:1.40911130501832E-309)
            boolean r2 = r4.Aem(r0)
            r0 = 285207303362050(0x1036500011602, double:1.409111305342116E-309)
            boolean r0 = r4.Aem(r0)
            if (r2 != 0) goto L_0x0030
            r1 = 0
            if (r0 == 0) goto L_0x000c
            goto L_0x0030
        L_0x0025:
            X.00z r2 = com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver.A03
            X.00z r0 = X.C001500z.A03
            if (r2 == r0) goto L_0x0030
            X.00z r0 = X.C001500z.A07
            r1 = 0
            if (r2 != r0) goto L_0x000c
        L_0x0030:
            r1 = 1
            return r1
        L_0x0032:
            java.lang.String r0 = "com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs"
            boolean r0 = r3.equals(r0)
            r2 = 21
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x003d:
            java.lang.String r0 = "com.facebook.battery.cpuspin.di.FbCpuSpinScheduler"
            boolean r0 = r3.equals(r0)
            r2 = 1
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0047:
            java.lang.String r0 = "com.facebook.messaging.filelogger.MessagingFileLogger"
            boolean r0 = r3.equals(r0)
            r2 = 13
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0052:
            java.lang.String r0 = "com.facebook.messaging.notify.logging.conditionalworkerimpl.PushSettingsReporter"
            boolean r0 = r3.equals(r0)
            r2 = 15
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x005d:
            java.lang.String r0 = "com.facebook.diskfootprint.cleaner.FileCleaner"
            boolean r0 = r3.equals(r0)
            r2 = 5
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0067:
            java.lang.String r0 = "com.facebook.zero.cms.ZeroCmsUtil"
            boolean r0 = r3.equals(r0)
            r2 = 23
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0072:
            java.lang.String r0 = "com.facebook.zero.header.ZeroHeaderRequestManager.LocalZeroHeaderRequestManagerReceiverRegistration"
            boolean r0 = r3.equals(r0)
            r2 = 24
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x007d:
            java.lang.String r0 = "com.facebook.accessibility.logging.TouchExplorationStateChangeDetector"
            boolean r0 = r3.equals(r0)
            r2 = 0
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0087:
            java.lang.String r0 = "com.facebook.push.mqtt.service.MqttClientStateManager"
            boolean r0 = r3.equals(r0)
            r2 = 17
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0093:
            java.lang.String r0 = "com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver"
            boolean r0 = r3.equals(r0)
            r2 = 6
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x009e:
            java.lang.String r0 = "com.facebook.device.resourcemonitor.activemonitoring.ResourceManagerActiveMonitoring"
            boolean r0 = r3.equals(r0)
            r2 = 4
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00a9:
            java.lang.String r0 = "com.facebook.interstitial.manager.InterstitialDataCleaner"
            boolean r0 = r3.equals(r0)
            r2 = 8
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00b5:
            java.lang.String r0 = "com.facebook.common.memory.LargeHeapOverrideConfig"
            boolean r0 = r3.equals(r0)
            r2 = 3
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00c0:
            java.lang.String r0 = "com.facebook.common.memory.manager.MemoryManager"
            boolean r0 = r3.equals(r0)
            r2 = 2
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00cb:
            java.lang.String r0 = "com.facebook.video.plugins.AutoplayIntentSignalMonitor"
            boolean r0 = r3.equals(r0)
            r2 = 20
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00d7:
            java.lang.String r0 = "com.facebook.messaging.chatheads.ChatHeadsInitializer"
            boolean r0 = r3.equals(r0)
            r2 = 10
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00e3:
            java.lang.String r0 = "com.facebook.video.exoserviceclient.HeroServiceInitializerAppJobInit"
            boolean r0 = r3.equals(r0)
            r2 = 19
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00ef:
            java.lang.String r0 = "com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler"
            boolean r0 = r3.equals(r0)
            r2 = 22
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x00fb:
            java.lang.String r0 = "com.facebook.messaging.inbox2.activenow.loader.PrefetcherManager"
            boolean r0 = r3.equals(r0)
            r2 = 14
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0107:
            java.lang.String r0 = "com.facebook.privacypermissionsnapshots.fb.PrivacyPermissionSnapshotsLoggerController"
            boolean r0 = r3.equals(r0)
            r2 = 16
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0113:
            java.lang.String r0 = "com.facebook.tigon.reliablemedia.ReliableMediaMonitor"
            boolean r0 = r3.equals(r0)
            r2 = 18
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x011f:
            java.lang.String r0 = "com.facebook.messaging.chatheads.service.VideoServiceAppStateListener"
            boolean r0 = r3.equals(r0)
            r2 = 11
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x012b:
            java.lang.String r0 = "com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener"
            boolean r0 = r3.equals(r0)
            r2 = 9
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0137:
            java.lang.String r0 = "com.facebook.messaging.contactstab.loader.StatusController"
            boolean r0 = r3.equals(r0)
            r2 = 12
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x0143:
            java.lang.String r0 = "com.facebook.http.networkstatelogger.NetworkStateLogger"
            boolean r0 = r3.equals(r0)
            r2 = 7
            if (r0 != 0) goto L_0x0009
            goto L_0x0008
        L_0x014e:
            r0 = 285224483493392(0x1036900051610, double:1.409196186468987E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x0158:
            r0 = 285224484935202(0x10369001b1622, double:1.409196193592475E-309)
            boolean r0 = r4.Aem(r0)
            r1 = r0 ^ 1
            return r1
        L_0x0164:
            r1 = 287324722503063(0x1055200051d97, double:1.41957274589633E-309)
            X.0XE r0 = X.AnonymousClass0XE.A07
            boolean r1 = r4.Aer(r1, r0)
            return r1
        L_0x0170:
            r0 = 286135023639021(0x1043d007119ed, double:1.413694852520133E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x017a:
            r0 = 282986805201005(0x101600000086d, double:1.398140586761856E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x0184:
            r0 = 285224484148761(0x10369000f1619, double:1.40919618970694E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x018e:
            r0 = 282943855855692(0x101560005084c, double:1.39792838880155E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x0198:
            java.lang.Boolean r0 = com.facebook.messaging.filelogger.MessagingFileLogger.A02
            boolean r0 = r0.booleanValue()
            r1 = r0 ^ 1
            return r1
        L_0x01a1:
            r0 = 282501488510580(0x100ef00df0674, double:1.39574280372093E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x01ab:
            boolean r1 = com.facebook.messaging.chatheads.service.VideoServiceAppStateListener.A01(r4)
            return r1
        L_0x01b0:
            r0 = 285224484673054(0x103690017161e, double:1.40919619229729E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x01ba:
            r0 = 282046207886375(0x1008500080427, double:1.393493418564563E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x01c4:
            java.lang.Boolean r0 = com.facebook.diskfootprint.cleaner.FileCleaner.A08
            boolean r0 = r0.booleanValue()
            r1 = r0 ^ 1
            return r1
        L_0x01cd:
            r0 = 285224483821076(0x10369000a1614, double:1.40919618808796E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x01d7:
            r0 = 285224484935202(0x10369001b1622, double:1.409196193592475E-309)
            boolean r1 = r4.Aem(r0)
            return r1
        L_0x01e1:
            boolean r0 = com.facebook.battery.cpuspin.di.FbCpuSpinScheduler.A05
            if (r0 == 0) goto L_0x01e8
            boolean r1 = com.facebook.battery.cpuspin.di.FbCpuSpinScheduler.A04
            return r1
        L_0x01e8:
            r2 = 281775624553181(0x10046000202dd, double:1.39215655927188E-309)
            boolean r0 = r4.Aem(r2)
            com.facebook.battery.cpuspin.di.FbCpuSpinScheduler.A04 = r0
            com.facebook.battery.cpuspin.di.FbCpuSpinScheduler.A05 = r1
            return r0
        L_0x01f6:
            boolean r1 = com.facebook.accessibility.logging.TouchExplorationStateChangeDetector.A01()
            return r1
        L_0x01fb:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ZR.A02(java.lang.String, X.1Yd):boolean");
    }

    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r5v55 */
    /* JADX WARN: Type inference failed for: r5v67, types: [X.3be] */
    /* JADX WARN: Type inference failed for: r5v108 */
    /* JADX WARN: Type inference failed for: r5v109 */
    /* JADX WARN: Type inference failed for: r5v110 */
    /* JADX WARN: Type inference failed for: r5v111 */
    /* JADX WARN: Type inference failed for: r5v112 */
    /* JADX WARNING: Code restructure failed: missing block: B:415:0x0c34, code lost:
        if (r4 == false) goto L_0x0c36;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:679:0x1619, code lost:
        if (((X.C06240bB) X.AnonymousClass1XX.A02(1, r2, r5.A00)).A0B() != false) goto L_0x161b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:908:0x1daa, code lost:
        if (r9 != null) goto L_0x1dac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:946:0x1ee1, code lost:
        if (((X.AnonymousClass0Ud) X.AnonymousClass1XX.A02(5, r2, r9.A00)).A0V != false) goto L_0x1ee3;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:1088:0x20b4  */
    /* JADX WARNING: Removed duplicated region for block: B:1150:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x05cb  */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x0951  */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x0966  */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x098a  */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x099f  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x000c  */
    /* JADX WARNING: Removed duplicated region for block: B:701:0x171e  */
    /* JADX WARNING: Removed duplicated region for block: B:874:0x1cec  */
    /* JADX WARNING: Removed duplicated region for block: B:877:0x1cff  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(int r39, X.C25271Yz r40) {
        /*
            r38 = this;
            r4 = 0
            r6 = 1
            r0 = r38
            r5 = r40
            switch(r39) {
                case 0: goto L_0x2031;
                case 1: goto L_0x2013;
                case 2: goto L_0x2004;
                case 3: goto L_0x1ff5;
                case 4: goto L_0x1fb3;
                case 5: goto L_0x1f9e;
                case 6: goto L_0x1f89;
                case 7: goto L_0x1ebc;
                case 8: goto L_0x1a84;
                case 9: goto L_0x1a5c;
                case 10: goto L_0x1a45;
                case 11: goto L_0x1a2e;
                case 12: goto L_0x1a0c;
                case 13: goto L_0x19ea;
                case 14: goto L_0x19d3;
                case 15: goto L_0x19bd;
                case 16: goto L_0x1990;
                case 17: goto L_0x1981;
                case 18: goto L_0x1952;
                case 19: goto L_0x193d;
                case 20: goto L_0x1928;
                case 21: goto L_0x18f6;
                case 22: goto L_0x18e7;
                case 23: goto L_0x180e;
                case 24: goto L_0x17f5;
                case 25: goto L_0x17dc;
                case 26: goto L_0x17c3;
                case 27: goto L_0x17b4;
                case 28: goto L_0x179f;
                case 29: goto L_0x1782;
                case 30: goto L_0x176f;
                case 31: goto L_0x1741;
                case 32: goto L_0x1723;
                case 33: goto L_0x170a;
                case 34: goto L_0x16fb;
                case 35: goto L_0x16ea;
                case 36: goto L_0x16db;
                case 37: goto L_0x16ca;
                case 38: goto L_0x16b9;
                case 39: goto L_0x16a0;
                case 40: goto L_0x15f1;
                case 41: goto L_0x15cf;
                case 42: goto L_0x1586;
                case 43: goto L_0x152b;
                case 44: goto L_0x14ad;
                case 45: goto L_0x1485;
                case 46: goto L_0x1468;
                case 47: goto L_0x1450;
                case 48: goto L_0x1435;
                case 49: goto L_0x12f0;
                case 50: goto L_0x12c6;
                case 51: goto L_0x1250;
                case 52: goto L_0x11e1;
                case 53: goto L_0x1176;
                case 54: goto L_0x10d6;
                case 55: goto L_0x10c7;
                case 56: goto L_0x10ad;
                case 57: goto L_0x106a;
                case 58: goto L_0x1031;
                case 59: goto L_0x0ffe;
                case 60: goto L_0x0fcb;
                case 61: goto L_0x0f60;
                case 62: goto L_0x0f37;
                case 63: goto L_0x0ef8;
                case 64: goto L_0x0eb7;
                case 65: goto L_0x0ea2;
                case 66: goto L_0x0e77;
                case 67: goto L_0x0e68;
                case 68: goto L_0x0e59;
                case 69: goto L_0x0e11;
                case 70: goto L_0x0ddd;
                case 71: goto L_0x0dbc;
                case 72: goto L_0x0d7e;
                case 73: goto L_0x0d57;
                case 74: goto L_0x0d48;
                case 75: goto L_0x0d2f;
                case 76: goto L_0x0cdc;
                case 77: goto L_0x0cb4;
                case 78: goto L_0x0c84;
                case 79: goto L_0x0c55;
                case 80: goto L_0x0bc1;
                case 81: goto L_0x0b91;
                case 82: goto L_0x0b2d;
                case 83: goto L_0x0b05;
                case 84: goto L_0x0a28;
                case 85: goto L_0x09a6;
                case 86: goto L_0x097a;
                case 87: goto L_0x096d;
                case 88: goto L_0x0941;
                case 89: goto L_0x0934;
                case 90: goto L_0x0925;
                case 91: goto L_0x08f9;
                case 92: goto L_0x08b0;
                case 93: goto L_0x08a1;
                case 94: goto L_0x087f;
                case 95: goto L_0x082f;
                case 96: goto L_0x081e;
                case 97: goto L_0x080f;
                case 98: goto L_0x0800;
                case 99: goto L_0x07f1;
                case 100: goto L_0x0736;
                case 101: goto L_0x0712;
                case 102: goto L_0x06ee;
                case 103: goto L_0x06c9;
                case 104: goto L_0x06af;
                case 105: goto L_0x06a0;
                case 106: goto L_0x065f;
                case 107: goto L_0x05eb;
                case 108: goto L_0x05d3;
                case 109: goto L_0x05af;
                case 110: goto L_0x059c;
                case 111: goto L_0x058d;
                case 112: goto L_0x057e;
                case 113: goto L_0x056f;
                case 114: goto L_0x0560;
                case 115: goto L_0x0551;
                case 116: goto L_0x0542;
                case 117: goto L_0x0533;
                case 118: goto L_0x0524;
                case 119: goto L_0x04e2;
                case 120: goto L_0x04cb;
                case 121: goto L_0x04b2;
                case 122: goto L_0x0468;
                case 123: goto L_0x0421;
                case 124: goto L_0x03e3;
                case 125: goto L_0x03b8;
                case 126: goto L_0x038e;
                case 127: goto L_0x0380;
                case 128: goto L_0x02e8;
                case 129: goto L_0x02ce;
                case 130: goto L_0x02ab;
                case 131: goto L_0x0291;
                case 132: goto L_0x0261;
                case 133: goto L_0x01fe;
                case 134: goto L_0x01de;
                case 135: goto L_0x01c6;
                case 136: goto L_0x019a;
                case 137: goto L_0x0177;
                case 138: goto L_0x0162;
                case 139: goto L_0x0134;
                case 140: goto L_0x010e;
                case 141: goto L_0x00d5;
                case 142: goto L_0x0099;
                case 143: goto L_0x008a;
                case 144: goto L_0x007c;
                case 145: goto L_0x0057;
                case 146: goto L_0x002a;
                case 147: goto L_0x001c;
                default: goto L_0x0009;
            }
        L_0x0009:
            r6 = 0
        L_0x000a:
            if (r6 <= 0) goto L_0x001b
            int r1 = X.AnonymousClass1Y3.Azb
            X.0UN r0 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            X.1ZJ r0 = (X.AnonymousClass1ZJ) r0
            java.util.concurrent.atomic.AtomicInteger r0 = r0.A03
            r0.getAndAdd(r6)
        L_0x001b:
            return
        L_0x001c:
            int r2 = X.AnonymousClass1Y3.Ajm
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.profilo.mmapbuf.MmapBufferProcessJob r1 = (com.facebook.profilo.mmapbuf.MmapBufferProcessJob) r1
            r1.A01()
            goto L_0x000a
        L_0x002a:
            int r2 = X.AnonymousClass1Y3.Avi
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager r3 = (com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager) r3
            boolean r1 = r3.A00
            if (r1 != 0) goto L_0x000a
            boolean r1 = r3.A02
            if (r1 == 0) goto L_0x0054
            boolean r1 = X.AnonymousClass050.A05(r4)
            if (r1 == 0) goto L_0x0054
            boolean r1 = com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager.A01()
            if (r1 == 0) goto L_0x0054
            java.lang.String r2 = "Profilo/BlackBoxState"
            java.lang.String r1 = "Abort on app going into background"
            android.util.Log.w(r2, r1)
            X.AnonymousClass050.A02()
            r3.A01 = r6
        L_0x0054:
            r3.A00 = r6
            goto L_0x000a
        L_0x0057:
            int r2 = X.AnonymousClass1Y3.Avi
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager r3 = (com.facebook.profilo.blackbox.BlackBoxAppStateAwareManager) r3
            boolean r1 = r3.A00
            if (r1 == 0) goto L_0x0079
            boolean r1 = r3.A01
            if (r1 == 0) goto L_0x0079
            boolean r1 = r3.A02
            if (r1 == 0) goto L_0x0079
            java.lang.String r2 = "Profilo/BlackBoxState"
            java.lang.String r1 = "Start on app going into foreground"
            android.util.Log.w(r2, r1)
            X.AnonymousClass050.A03()
            r3.A01 = r4
        L_0x0079:
            r3.A00 = r4
            goto L_0x000a
        L_0x007c:
            int r2 = X.AnonymousClass1Y3.A0h
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.profilo.blackbox.breakpad.BreakpadDumpProcessJob r1 = (com.facebook.profilo.blackbox.breakpad.BreakpadDumpProcessJob) r1
            r1.A01()
            goto L_0x000a
        L_0x008a:
            int r2 = X.AnonymousClass1Y3.BOt
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.zero.service.ZeroInterstitialEligibilityManager r1 = (com.facebook.zero.service.ZeroInterstitialEligibilityManager) r1
            r1.A01()
            goto L_0x000a
        L_0x0099:
            int r2 = X.AnonymousClass1Y3.BDA
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.zero.LocalZeroTokenManagerReceiverRegistration r5 = (com.facebook.zero.LocalZeroTokenManagerReceiverRegistration) r5
            int r3 = X.AnonymousClass1Y3.AcD
            X.0UN r2 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 582(0x246, float:8.16E-43)
            com.facebook.common.util.TriState r2 = r2.Ab9(r1)
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.NO
            if (r2 == r1) goto L_0x000a
            r3 = 2
            int r2 = X.AnonymousClass1Y3.B9g
            X.0UN r1 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1eP r2 = (X.C28191eP) r2
            monitor-enter(r5)
            boolean r1 = r5.A01     // Catch:{ all -> 0x208f }
            if (r1 == 0) goto L_0x00d0
            java.lang.String r1 = "Enter app with pending token fetch"
            r2.A0O(r1)     // Catch:{ all -> 0x208f }
            r5.A01 = r4     // Catch:{ all -> 0x208f }
            goto L_0x11de
        L_0x00d0:
            r2.A0L()     // Catch:{ all -> 0x208f }
            goto L_0x11de
        L_0x00d5:
            int r2 = X.AnonymousClass1Y3.Ab4
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.zero.paidbalance.PaidBalanceController r4 = (com.facebook.zero.paidbalance.PaidBalanceController) r4
            int r3 = X.AnonymousClass1Y3.AcD
            X.0UN r2 = r4.A02
            r1 = 11
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 582(0x246, float:8.16E-43)
            com.facebook.common.util.TriState r2 = r2.Ab9(r1)
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.NO
            if (r2 == r1) goto L_0x000a
            boolean r1 = r4.A03()
            if (r1 != 0) goto L_0x0102
            X.1eS r1 = X.C28221eS.DISABLED
            com.facebook.zero.paidbalance.PaidBalanceController.A02(r4, r1)
            goto L_0x000a
        L_0x0102:
            X.1eS r1 = X.C28221eS.UNKNOWN
            com.facebook.zero.paidbalance.PaidBalanceController.A02(r4, r1)
            X.2aG r1 = X.C48332aG.A01
            r4.A04(r1)
            goto L_0x000a
        L_0x010e:
            int r2 = X.AnonymousClass1Y3.BSy
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            X.1ma r5 = (X.C32941ma) r5
            int r2 = X.AnonymousClass1Y3.B4u
            X.0UN r1 = r5.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            X.1qv r3 = (X.C35251qv) r3
            monitor-enter(r5)
            boolean r1 = r5.A01     // Catch:{ all -> 0x205c }
            if (r1 != 0) goto L_0x012a
            monitor-exit(r5)     // Catch:{ all -> 0x205c }
            goto L_0x000a
        L_0x012a:
            r5.A01 = r4     // Catch:{ all -> 0x205c }
            monitor-exit(r5)     // Catch:{ all -> 0x205c }
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r3.A05(r4, r1)
            goto L_0x000a
        L_0x0134:
            int r2 = X.AnonymousClass1Y3.BSy
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            X.1ma r4 = (X.C32941ma) r4
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 285224484607517(0x103690016161d, double:1.409196191973496E-309)
            boolean r1 = r3.Aem(r1)
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.B4u
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            X.1qv r1 = (X.C35251qv) r1
            X.C32941ma.A01(r4, r1)
            goto L_0x000a
        L_0x0162:
            int r2 = X.AnonymousClass1Y3.AJ5
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.zero.cms.ZeroCmsUtil r2 = (com.facebook.zero.cms.ZeroCmsUtil) r2
            boolean r1 = com.facebook.zero.cms.ZeroCmsUtil.A04(r2)
            if (r1 == 0) goto L_0x000a
            r2.A06(r4)
            goto L_0x000a
        L_0x0177:
            int r2 = X.AnonymousClass1Y3.AJ5
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.zero.cms.ZeroCmsUtil r1 = (com.facebook.zero.cms.ZeroCmsUtil) r1
            com.facebook.zero.cms.ZeroCmsUtil.A02(r1)     // Catch:{ RuntimeException -> 0x0186 }
            goto L_0x000a
        L_0x0186:
            r3 = 2
            int r2 = X.AnonymousClass1Y3.Amr
            X.0UN r1 = r1.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.09P r3 = (X.AnonymousClass09P) r3
            java.lang.String r2 = "ZeroCmsUtil"
            java.lang.String r1 = "zero cms init causes runtime exception"
            r3.CGS(r2, r1)
            goto L_0x000a
        L_0x019a:
            int r2 = X.AnonymousClass1Y3.BCE
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.zero.carriersignal.CarrierSignalController r4 = (com.facebook.zero.carriersignal.CarrierSignalController) r4
            int r3 = X.AnonymousClass1Y3.AcD
            X.0UN r2 = r4.A00
            r1 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 582(0x246, float:8.16E-43)
            com.facebook.common.util.TriState r2 = r2.Ab9(r1)
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.NO
            if (r2 == r1) goto L_0x000a
            boolean r1 = r4.A05()
            if (r1 == 0) goto L_0x000a
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r4.A03(r1)
            goto L_0x000a
        L_0x01c6:
            int r2 = X.AnonymousClass1Y3.AN4
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.xanalytics.provider.NativeXAnalyticsProvider r2 = (com.facebook.xanalytics.provider.NativeXAnalyticsProvider) r2
            com.facebook.xanalytics.XAnalyticsNative r1 = r2.A02
            r1.flush()
            java.util.concurrent.ScheduledFuture r2 = r2.A01
            if (r2 == 0) goto L_0x000a
            r2.cancel(r4)
            goto L_0x000a
        L_0x01de:
            int r2 = X.AnonymousClass1Y3.AN4
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.xanalytics.provider.NativeXAnalyticsProvider r2 = (com.facebook.xanalytics.provider.NativeXAnalyticsProvider) r2
            java.util.concurrent.ScheduledExecutorService r7 = r2.A04
            X.9dE r8 = new X.9dE
            r8.<init>(r2)
            java.util.concurrent.TimeUnit r13 = java.util.concurrent.TimeUnit.MILLISECONDS
            r9 = 30000(0x7530, double:1.4822E-319)
            r11 = 180000(0x2bf20, double:8.8932E-319)
            java.util.concurrent.ScheduledFuture r1 = r7.scheduleAtFixedRate(r8, r9, r11, r13)
            r2.A01 = r1
            goto L_0x000a
        L_0x01fe:
            int r2 = X.AnonymousClass1Y3.AEw
            X.0UN r1 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler r7 = (com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler) r7
            java.lang.String r5 = "FbAppModuleDownloaderInitHandler"
            r3 = 2
            int r2 = X.AnonymousClass1Y3.B0M     // Catch:{ IOException -> 0x0259 }
            X.0UN r1 = r7.A00     // Catch:{ IOException -> 0x0259 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ IOException -> 0x0259 }
            X.3cW r1 = (X.C71503cW) r1     // Catch:{ IOException -> 0x0259 }
            r1.A02()     // Catch:{ IOException -> 0x0259 }
            int r2 = X.AnonymousClass1Y3.A7m     // Catch:{ IOException -> 0x0259 }
            X.0UN r1 = r7.A00     // Catch:{ IOException -> 0x0259 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)     // Catch:{ IOException -> 0x0259 }
            X.0Ej r1 = (X.AnonymousClass0Ej) r1     // Catch:{ IOException -> 0x0259 }
            r1.A04()     // Catch:{ IOException -> 0x0259 }
            r3 = 3
            int r2 = X.AnonymousClass1Y3.AXI     // Catch:{ IOException -> 0x0259 }
            X.0UN r1 = r7.A00     // Catch:{ IOException -> 0x0259 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ IOException -> 0x0259 }
            X.2Dn r1 = (X.C43222Dn) r1     // Catch:{ IOException -> 0x0259 }
            r1.A0C()     // Catch:{ IOException -> 0x0259 }
            int r2 = X.AnonymousClass1Y3.Awq     // Catch:{ IOException -> 0x0259 }
            X.0UN r1 = r7.A00     // Catch:{ IOException -> 0x0259 }
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)     // Catch:{ IOException -> 0x0259 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x0259 }
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.AcD     // Catch:{ IOException -> 0x0259 }
            X.0UN r1 = r7.A00     // Catch:{ IOException -> 0x0259 }
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)     // Catch:{ IOException -> 0x0259 }
            X.1YI r4 = (X.AnonymousClass1YI) r4     // Catch:{ IOException -> 0x0259 }
            r3 = 0
            int r2 = X.AnonymousClass1Y3.B7a     // Catch:{ IOException -> 0x0259 }
            X.0UN r1 = r7.A00     // Catch:{ IOException -> 0x0259 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ IOException -> 0x0259 }
            X.3cQ r1 = (X.C71443cQ) r1     // Catch:{ IOException -> 0x0259 }
            com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler.A01(r4, r1)     // Catch:{ IOException -> 0x0259 }
            goto L_0x000a
        L_0x0259:
            r2 = move-exception
            java.lang.String r1 = "Init failure"
            X.C010708t.A0T(r5, r2, r1)
            goto L_0x000a
        L_0x0261:
            int r2 = X.AnonymousClass1Y3.AWm
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs r5 = (com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs) r5
            int r2 = X.AnonymousClass1Y3.Awq
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.AcD
            X.0UN r1 = r5.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            X.1YI r4 = (X.AnonymousClass1YI) r4
            r3 = 0
            int r2 = X.AnonymousClass1Y3.B7a
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.3cQ r1 = (X.C71443cQ) r1
            com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitHandler.A01(r4, r1)
            goto L_0x000a
        L_0x0291:
            int r2 = X.AnonymousClass1Y3.AWm
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs r1 = (com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs) r1
            int r3 = X.AnonymousClass1Y3.AXI
            X.0UN r2 = r1.A00
            r1 = 3
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.2Dn r1 = (X.C43222Dn) r1
            r1.A0C()
            goto L_0x000a
        L_0x02ab:
            int r2 = X.AnonymousClass1Y3.AWm
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs r1 = (com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs) r1
            java.lang.String r4 = "FbAppModuleDownloaderInitAppJobs"
            int r2 = X.AnonymousClass1Y3.A7m     // Catch:{ IOException -> 0x02c6 }
            X.0UN r1 = r1.A00     // Catch:{ IOException -> 0x02c6 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)     // Catch:{ IOException -> 0x02c6 }
            X.0Ej r1 = (X.AnonymousClass0Ej) r1     // Catch:{ IOException -> 0x02c6 }
            r1.A04()     // Catch:{ IOException -> 0x02c6 }
            goto L_0x000a
        L_0x02c6:
            r2 = move-exception
            java.lang.String r1 = "backgroundInitializerInit failure"
            X.C010708t.A0T(r4, r2, r1)
            goto L_0x000a
        L_0x02ce:
            int r2 = X.AnonymousClass1Y3.AWm
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs r1 = (com.facebook.voltron.fbdownloader.FbAppModuleDownloaderInitAppJobs) r1
            int r3 = X.AnonymousClass1Y3.B0M
            X.0UN r2 = r1.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.3cW r1 = (X.C71503cW) r1
            r1.A02()
            goto L_0x000a
        L_0x02e8:
            int r2 = X.AnonymousClass1Y3.A8b
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.voltron.fbdownloader.FbAppJobScheduledPrefetcher r1 = (com.facebook.voltron.fbdownloader.FbAppJobScheduledPrefetcher) r1
            X.90e r5 = r1.A00
            boolean r1 = r5.A02
            if (r1 != 0) goto L_0x0321
            r1 = 0
        L_0x02f9:
            if (r1 == 0) goto L_0x000a
            r5.A00 = r4
            X.8NA r2 = new X.8NA
            X.0XQ r1 = r5.A01
            r2.<init>(r1)
            java.util.Set r7 = r2.A00()
            r1 = 3
            boolean r1 = X.C010708t.A0X(r1)
            if (r1 == 0) goto L_0x0330
            java.util.Iterator r3 = r7.iterator()
            r2 = 1
        L_0x0314:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x0330
            r3.next()
            if (r2 == 0) goto L_0x0314
            r2 = 0
            goto L_0x0314
        L_0x0321:
            X.0XQ r2 = r5.A01
            java.lang.String r1 = "AppModules::NeedToFallbackDownload"
            X.0aI r2 = r2.A00(r1)
            java.lang.String r1 = "key::NeedFallback"
            boolean r1 = r2.A0B(r1, r4)
            goto L_0x02f9
        L_0x0330:
            boolean r1 = r7.isEmpty()
            if (r1 == 0) goto L_0x034d
            X.0XQ r2 = r5.A01
            java.lang.String r1 = "AppModules::NeedToFallbackDownload"
            X.0aI r1 = r2.A00(r1)
            X.16O r3 = r1.A06()
            java.lang.String r2 = "key::NeedFallback"
            r1 = 0
            r3.A0D(r2, r1)
            r3.A05()
            goto L_0x000a
        L_0x034d:
            boolean r1 = r5.A03
            if (r1 == 0) goto L_0x0376
            java.util.concurrent.CountDownLatch r4 = new java.util.concurrent.CountDownLatch
            int r1 = r7.size()
            r4.<init>(r1)
            java.util.Iterator r3 = r7.iterator()
        L_0x035e:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x000a
            java.lang.Object r2 = r3.next()
            java.lang.String r2 = (java.lang.String) r2
            X.1Gd r1 = new X.1Gd
            r1.<init>()
            r1.add(r2)
            X.C1925690e.A00(r5, r4, r1)
            goto L_0x035e
        L_0x0376:
            java.util.concurrent.CountDownLatch r1 = new java.util.concurrent.CountDownLatch
            r1.<init>(r6)
            X.C1925690e.A00(r5, r1, r7)
            goto L_0x000a
        L_0x0380:
            int r2 = X.AnonymousClass1Y3.B92
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.video.plugins.AutoplayIntentSignalMonitor r1 = (com.facebook.video.plugins.AutoplayIntentSignalMonitor) r1
            r1.A01 = r4
            goto L_0x000a
        L_0x038e:
            int r2 = X.AnonymousClass1Y3.B2O
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.video.fbgrootplayer.VideoAudioVolumeObserversManager r1 = (com.facebook.video.fbgrootplayer.VideoAudioVolumeObserversManager) r1
            int r3 = X.AnonymousClass1Y3.A3f
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.8JV r4 = (X.AnonymousClass8JV) r4
            int r3 = X.AnonymousClass1Y3.BCt
            X.0UN r2 = r4.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            android.content.Context r1 = (android.content.Context) r1
            android.content.ContentResolver r2 = r1.getContentResolver()
            X.8JU r1 = r4.A03
            r2.unregisterContentObserver(r1)
            goto L_0x000a
        L_0x03b8:
            int r2 = X.AnonymousClass1Y3.B2O
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.video.fbgrootplayer.VideoAudioVolumeObserversManager r1 = (com.facebook.video.fbgrootplayer.VideoAudioVolumeObserversManager) r1
            int r3 = X.AnonymousClass1Y3.A3f
            X.0UN r2 = r1.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.8JV r5 = (X.AnonymousClass8JV) r5
            int r3 = X.AnonymousClass1Y3.BCt
            X.0UN r2 = r5.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            android.content.Context r1 = (android.content.Context) r1
            android.content.ContentResolver r4 = r1.getContentResolver()
            android.net.Uri r3 = android.provider.Settings.System.CONTENT_URI
            X.8JU r2 = r5.A03
            r4.registerContentObserver(r3, r6, r2)
            goto L_0x000a
        L_0x03e3:
            int r2 = X.AnonymousClass1Y3.Afn
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.video.exoserviceclient.HeroServiceInitializerAppJobInit r4 = (com.facebook.video.exoserviceclient.HeroServiceInitializerAppJobInit) r4
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r4.A00
            r1 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 286135023704558(0x1043d007219ee, double:1.41369485284393E-309)
            boolean r1 = r3.Aem(r1)
            if (r1 == 0) goto L_0x0412
            int r2 = X.AnonymousClass1Y3.A10
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            com.facebook.video.exoserviceclient.FbVpsController r1 = (com.facebook.video.exoserviceclient.FbVpsController) r1
            r1.A03()
            goto L_0x000a
        L_0x0412:
            int r2 = X.AnonymousClass1Y3.A10
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            com.facebook.video.exoserviceclient.FbVpsController r1 = (com.facebook.video.exoserviceclient.FbVpsController) r1
            r1.A01()
            goto L_0x000a
        L_0x0421:
            java.util.WeakHashMap r2 = new java.util.WeakHashMap
            java.util.Map r1 = X.AnonymousClass4L4.A00
            r2.<init>(r1)
            java.util.Map r1 = java.util.Collections.unmodifiableMap(r2)
            java.util.Set r1 = r1.entrySet()
            java.util.Iterator r6 = r1.iterator()
        L_0x0434:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L_0x04af
            java.lang.Object r2 = r6.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r1 = r2.getValue()
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0434
            java.lang.Object r1 = r2.getKey()
            if (r1 == 0) goto L_0x0434
            r2.getKey()
            X.8Qc r5 = X.C179348Qc.A0P
            r1 = 1
            android.os.Handler r3 = r5.A07
            X.8PX r2 = new X.8PX
            r2.<init>(r5, r1)
            r1 = 909851376(0x363b3af0, float:2.7899478E-6)
            X.AnonymousClass00S.A04(r3, r2, r1)
            int r4 = r4 + 1
            goto L_0x0434
        L_0x0468:
            java.util.WeakHashMap r2 = new java.util.WeakHashMap
            java.util.Map r1 = X.AnonymousClass4L4.A00
            r2.<init>(r1)
            java.util.Map r1 = java.util.Collections.unmodifiableMap(r2)
            java.util.Set r1 = r1.entrySet()
            java.util.Iterator r6 = r1.iterator()
        L_0x047b:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L_0x04af
            java.lang.Object r2 = r6.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r1 = r2.getValue()
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x047b
            java.lang.Object r1 = r2.getKey()
            if (r1 == 0) goto L_0x047b
            r2.getKey()
            X.8Qc r5 = X.C179348Qc.A0P
            r1 = 0
            android.os.Handler r3 = r5.A07
            X.8PX r2 = new X.8PX
            r2.<init>(r5, r1)
            r1 = 909851376(0x363b3af0, float:2.7899478E-6)
            X.AnonymousClass00S.A04(r3, r2, r1)
            int r4 = r4 + 1
            goto L_0x047b
        L_0x04af:
            r6 = r4
            goto L_0x000a
        L_0x04b2:
            int r2 = X.AnonymousClass1Y3.AUd
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.uievaluations.uievaluationsrunner.UIEvaluationsRunner r1 = (com.facebook.uievaluations.uievaluationsrunner.UIEvaluationsRunner) r1
            r1.A0A = r4
            boolean r1 = r1.A0C
            if (r1 == 0) goto L_0x000a
            java.lang.String r2 = "is_accessibility_enabled"
            java.lang.String r1 = "false"
            java.lang.System.setProperty(r2, r1)
            goto L_0x000a
        L_0x04cb:
            int r2 = X.AnonymousClass1Y3.AUd
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.uievaluations.uievaluationsrunner.UIEvaluationsRunner r3 = (com.facebook.uievaluations.uievaluationsrunner.UIEvaluationsRunner) r3
            boolean r1 = r3.A0B
            if (r1 == 0) goto L_0x000a
            X.1LM r2 = r5.A00
            java.lang.Runnable r1 = r3.A07
            r2.A00(r1)
            goto L_0x000a
        L_0x04e2:
            int r2 = X.AnonymousClass1Y3.ATg
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.ui.titlebar.abtest.WhiteChromeActivityStack r4 = (com.facebook.ui.titlebar.abtest.WhiteChromeActivityStack) r4
            boolean r1 = r4.A03
            if (r1 != 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.BCt
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            android.content.Context r1 = (android.content.Context) r1
            android.content.Context r2 = r1.getApplicationContext()
            java.lang.Class<android.app.Application> r1 = android.app.Application.class
            java.lang.Object r3 = X.AnonymousClass065.A00(r2, r1)
            X.C000300h.A01(r3)
            android.app.Application r3 = (android.app.Application) r3
            X.7jf r1 = new X.7jf
            r2 = 0
            r1.<init>(r4)
            r3.registerActivityLifecycleCallbacks(r1)
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference
            r1.<init>(r2)
            r4.A01 = r1
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference
            r1.<init>(r2)
            r4.A02 = r1
            r4.A03 = r6
            goto L_0x000a
        L_0x0524:
            int r2 = X.AnonymousClass1Y3.Aj0
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.tigonliger.TigonLigerService r2 = (com.facebook.tigon.tigonliger.TigonLigerService) r2
            r2.onAppStateChange(r6)
            goto L_0x000a
        L_0x0533:
            int r2 = X.AnonymousClass1Y3.Aj0
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.tigonliger.TigonLigerService r2 = (com.facebook.tigon.tigonliger.TigonLigerService) r2
            r2.onAppStateChange(r4)
            goto L_0x000a
        L_0x0542:
            int r2 = X.AnonymousClass1Y3.Apf
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.reliablemedia.ReliableMediaMonitor r1 = (com.facebook.tigon.reliablemedia.ReliableMediaMonitor) r1
            r1.background()
            goto L_0x000a
        L_0x0551:
            int r2 = X.AnonymousClass1Y3.Apf
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.reliablemedia.ReliableMediaMonitor r1 = (com.facebook.tigon.reliablemedia.ReliableMediaMonitor) r1
            r1.foreground()
            goto L_0x000a
        L_0x0560:
            int r2 = X.AnonymousClass1Y3.Apf
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.reliablemedia.ReliableMediaMonitor r1 = (com.facebook.tigon.reliablemedia.ReliableMediaMonitor) r1
            r1.loginComplete()
            goto L_0x000a
        L_0x056f:
            int r2 = X.AnonymousClass1Y3.Apf
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.reliablemedia.ReliableMediaMonitor r1 = (com.facebook.tigon.reliablemedia.ReliableMediaMonitor) r1
            r1.initialize()
            goto L_0x000a
        L_0x057e:
            int r2 = X.AnonymousClass1Y3.Avw
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.nativeservice.common.NativePlatformContextHolder r1 = (com.facebook.tigon.nativeservice.common.NativePlatformContextHolder) r1
            r1.onBackgroundAppJob()
            goto L_0x000a
        L_0x058d:
            int r2 = X.AnonymousClass1Y3.Avw
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.tigon.nativeservice.common.NativePlatformContextHolder r1 = (com.facebook.tigon.nativeservice.common.NativePlatformContextHolder) r1
            r1.onForegroundAppJob()
            goto L_0x000a
        L_0x059c:
            int r2 = X.AnonymousClass1Y3.BDZ
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.sync.SyncInitializer r3 = (com.facebook.sync.SyncInitializer) r3
            boolean r1 = com.facebook.sync.SyncInitializer.A04(r3)
            if (r1 == 0) goto L_0x000a
            java.lang.String r2 = "app_foregrounded_immediate"
            goto L_0x05c1
        L_0x05af:
            int r2 = X.AnonymousClass1Y3.BDZ
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.sync.SyncInitializer r3 = (com.facebook.sync.SyncInitializer) r3
            boolean r1 = com.facebook.sync.SyncInitializer.A04(r3)
            if (r1 == 0) goto L_0x000a
            java.lang.String r2 = "app_foregrounded"
        L_0x05c1:
            java.lang.String r1 = com.facebook.sync.SyncInitializer.A01(r3)
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x000a
            r3.init()
            com.facebook.sync.SyncInitializer.A02(r3)
            goto L_0x000a
        L_0x05d3:
            int r2 = X.AnonymousClass1Y3.B1x
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.surfaces.fb.PrewarmingJobsQueue r4 = (com.facebook.surfaces.fb.PrewarmingJobsQueue) r4
            java.util.concurrent.atomic.AtomicBoolean r3 = r4.A05
            r1 = 0
            boolean r1 = r3.compareAndSet(r6, r1)
            if (r1 == 0) goto L_0x000a
            com.facebook.surfaces.fb.PrewarmingJobsQueue.A02(r4)
            goto L_0x000a
        L_0x05eb:
            int r2 = X.AnonymousClass1Y3.B1x
            X.0UN r1 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.surfaces.fb.PrewarmingJobsQueue r8 = (com.facebook.surfaces.fb.PrewarmingJobsQueue) r8
            java.util.concurrent.atomic.AtomicBoolean r3 = r8.A05
            boolean r1 = r3.compareAndSet(r4, r6)
            if (r1 == 0) goto L_0x000a
            boolean r1 = r8.A07
            if (r1 == 0) goto L_0x0606
            com.facebook.surfaces.fb.PrewarmingJobsQueue.A04(r8)
            goto L_0x000a
        L_0x0606:
            java.lang.Object r3 = r8.A02
            monitor-enter(r3)
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x2062 }
            java.util.Map r1 = r8.A04     // Catch:{ all -> 0x2062 }
            java.util.Collection r1 = r1.values()     // Catch:{ all -> 0x2062 }
            r2.<init>(r1)     // Catch:{ all -> 0x2062 }
            monitor-exit(r3)     // Catch:{ all -> 0x2062 }
            java.util.Iterator r4 = r2.iterator()
        L_0x0619:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x000a
            java.lang.Object r10 = r4.next()
            X.Eq0 r10 = (X.C30124Eq0) r10
            java.lang.Object r2 = r8.A02
            monitor-enter(r2)
            boolean r1 = com.facebook.surfaces.fb.PrewarmingJobsQueue.A08(r8, r10)     // Catch:{ all -> 0x205f }
            if (r1 == 0) goto L_0x0630
            monitor-exit(r2)     // Catch:{ all -> 0x205f }
            goto L_0x0619
        L_0x0630:
            r10.A01 = r6     // Catch:{ all -> 0x205f }
            monitor-exit(r2)     // Catch:{ all -> 0x205f }
            java.lang.ref.WeakReference r12 = com.facebook.surfaces.fb.PrewarmingJobsQueue.A01(r8)
            r11 = 0
            r13 = 0
            r9 = 0
            r3 = 0
            X.Eq2 r7 = new X.Eq2
            r7.<init>(r8, r9, r10, r11, r12, r13)
            int r2 = X.AnonymousClass1Y3.ACz
            X.0UN r1 = r8.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0bZ r2 = (X.C06480bZ) r2
            X.Epx r14 = new X.Epx
            r15 = r8
            r16 = r10
            r17 = r7
            r18 = r11
            r19 = r11
            r14.<init>(r15, r16, r17, r18, r19)
            r1 = 363682679(0x15ad5b77, float:7.001845E-26)
            X.AnonymousClass07A.A04(r2, r14, r1)
            goto L_0x0619
        L_0x065f:
            int r2 = X.AnonymousClass1Y3.B1x
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.surfaces.fb.PrewarmingJobsQueue r5 = (com.facebook.surfaces.fb.PrewarmingJobsQueue) r5
            java.util.concurrent.atomic.AtomicReference r1 = r5.A06
            java.lang.Object r1 = r1.get()
            X.3Ao r1 = (X.C64233Ao) r1
            if (r1 != 0) goto L_0x000a
            X.3Ao r4 = new X.3Ao
            r2 = 0
            r4.<init>(r5)
            java.util.concurrent.atomic.AtomicReference r1 = r5.A06
            boolean r1 = r1.compareAndSet(r2, r4)
            if (r1 == 0) goto L_0x000a
            r3 = 2
            int r2 = X.AnonymousClass1Y3.BAJ
            X.0UN r1 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.3d8 r2 = (X.C71833d8) r2
            monitor-enter(r2)
            java.util.List r1 = r2.A01     // Catch:{ all -> 0x2065 }
            if (r1 != 0) goto L_0x0698
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x2065 }
            r1.<init>()     // Catch:{ all -> 0x2065 }
            r2.A01 = r1     // Catch:{ all -> 0x2065 }
        L_0x0698:
            java.util.List r1 = r2.A01     // Catch:{ all -> 0x2065 }
            r1.add(r4)     // Catch:{ all -> 0x2065 }
            monitor-exit(r2)
            goto L_0x000a
        L_0x06a0:
            int r2 = X.AnonymousClass1Y3.AN9
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.trash.FbTrashManager r1 = (com.facebook.storage.trash.FbTrashManager) r1
            r1.A03()
            goto L_0x000a
        L_0x06af:
            int r2 = X.AnonymousClass1Y3.BFB
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor r3 = (com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor) r3
            monitor-enter(r3)
            monitor-enter(r3)     // Catch:{ all -> 0x206b }
            java.util.concurrent.ScheduledFuture r2 = r3.A01     // Catch:{ all -> 0x2068 }
            if (r2 == 0) goto L_0x06c5
            r2.cancel(r4)     // Catch:{ all -> 0x2068 }
            r1 = 0
            r3.A01 = r1     // Catch:{ all -> 0x2068 }
        L_0x06c5:
            monitor-exit(r3)     // Catch:{ all -> 0x206b }
            monitor-exit(r3)
            goto L_0x000a
        L_0x06c9:
            int r2 = X.AnonymousClass1Y3.BFB
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor r2 = (com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor) r2
            monitor-enter(r2)
            monitor-enter(r2)     // Catch:{ all -> 0x2071 }
            java.util.concurrent.ScheduledFuture r1 = r2.A01     // Catch:{ all -> 0x206e }
            if (r1 != 0) goto L_0x06ea
            java.util.concurrent.ScheduledExecutorService r7 = r2.A07     // Catch:{ all -> 0x206e }
            java.lang.Runnable r8 = r2.A05     // Catch:{ all -> 0x206e }
            r9 = 0
            r11 = 60000(0xea60, double:2.9644E-319)
            java.util.concurrent.TimeUnit r13 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x206e }
            java.util.concurrent.ScheduledFuture r1 = r7.scheduleAtFixedRate(r8, r9, r11, r13)     // Catch:{ all -> 0x206e }
            r2.A01 = r1     // Catch:{ all -> 0x206e }
        L_0x06ea:
            monitor-exit(r2)     // Catch:{ all -> 0x2071 }
            monitor-exit(r2)
            goto L_0x000a
        L_0x06ee:
            int r2 = X.AnonymousClass1Y3.Aa7
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.diskio.ProcIOStatsOverallReporting r3 = (com.facebook.storage.diskio.ProcIOStatsOverallReporting) r3
            boolean r1 = r3.A04
            if (r1 == 0) goto L_0x000a
            int r1 = r3.A00
            if (r1 != 0) goto L_0x0709
            java.lang.String r2 = "ProcIOStats"
            java.lang.String r1 = "Foreground app state was skipped, got background twice"
            X.AnonymousClass02w.A0B(r2, r1)
            goto L_0x000a
        L_0x0709:
            X.09h r2 = X.C012009h.A00()
            com.facebook.storage.diskio.ProcIOStatsOverallReporting.A02(r3, r2, r4)
            goto L_0x000a
        L_0x0712:
            int r2 = X.AnonymousClass1Y3.Aa7
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.diskio.ProcIOStatsOverallReporting r3 = (com.facebook.storage.diskio.ProcIOStatsOverallReporting) r3
            boolean r1 = r3.A04
            if (r1 == 0) goto L_0x000a
            int r1 = r3.A00
            if (r1 != r6) goto L_0x072d
            java.lang.String r2 = "ProcIOStats"
            java.lang.String r1 = "Foreground app state was skipped, got foreground twice"
            X.AnonymousClass02w.A0B(r2, r1)
            goto L_0x000a
        L_0x072d:
            X.09h r1 = X.C012009h.A00()
            com.facebook.storage.diskio.ProcIOStatsOverallReporting.A02(r3, r1, r6)
            goto L_0x000a
        L_0x0736:
            int r2 = X.AnonymousClass1Y3.Av8
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.cleaner.PathSizeOverflowCleaner r5 = (com.facebook.storage.cleaner.PathSizeOverflowCleaner) r5
            boolean r1 = r5.A03
            if (r1 == 0) goto L_0x000a
            java.util.HashMap r1 = r5.A01
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x000a
            X.09i r1 = X.C012109i.A01()
            r1.A08()
            X.09i r2 = X.C012109i.A01()
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            long r7 = r2.A05(r1)
            r2 = 419430400(0x19000000, double:2.072261515E-315)
            int r1 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            r13 = 0
            if (r1 >= 0) goto L_0x0766
            r13 = 1
        L_0x0766:
            java.util.HashMap r1 = r5.A01
            java.util.Set r1 = r1.keySet()
            java.util.Iterator r12 = r1.iterator()
        L_0x0770:
            boolean r1 = r12.hasNext()
            if (r1 == 0) goto L_0x000a
            java.lang.Object r7 = r12.next()
            java.lang.String r7 = (java.lang.String) r7
            if (r13 == 0) goto L_0x07ee
            java.util.HashMap r1 = r5.A02
        L_0x0780:
            java.lang.Object r1 = r1.get(r7)
            java.lang.Long r1 = (java.lang.Long) r1
            long r10 = r1.longValue()
            java.lang.String r4 = "PathSizeOverflowCleaner"
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x07d9 }
            r9.<init>(r7)     // Catch:{ Exception -> 0x07d9 }
            boolean r1 = r9.exists()     // Catch:{ Exception -> 0x07d9 }
            if (r1 == 0) goto L_0x0770
            long r2 = com.facebook.storage.cleaner.PathSizeOverflowCleaner.A00(r9)     // Catch:{ Exception -> 0x07d9 }
            int r1 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r1 < 0) goto L_0x0770
            boolean r1 = r9.isDirectory()     // Catch:{ Exception -> 0x07d9 }
            if (r1 != 0) goto L_0x07bf
            boolean r1 = r9.delete()     // Catch:{ Exception -> 0x07d9 }
            if (r1 != 0) goto L_0x0770
            int r2 = X.AnonymousClass1Y3.Amr     // Catch:{ Exception -> 0x07d9 }
            X.0UN r1 = r5.A00     // Catch:{ Exception -> 0x07d9 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r1)     // Catch:{ Exception -> 0x07d9 }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ Exception -> 0x07d9 }
            java.lang.String r1 = "Failed to remove the file "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r7)     // Catch:{ Exception -> 0x07d9 }
            r2.CGS(r4, r1)     // Catch:{ Exception -> 0x07d9 }
            goto L_0x0770
        L_0x07bf:
            boolean r1 = X.AnonymousClass9AZ.A01(r9)     // Catch:{ Exception -> 0x07d9 }
            if (r1 != 0) goto L_0x0770
            int r2 = X.AnonymousClass1Y3.Amr     // Catch:{ Exception -> 0x07d9 }
            X.0UN r1 = r5.A00     // Catch:{ Exception -> 0x07d9 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r1)     // Catch:{ Exception -> 0x07d9 }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ Exception -> 0x07d9 }
            java.lang.String r1 = "Failed to remove the folder "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r7)     // Catch:{ Exception -> 0x07d9 }
            r2.CGS(r4, r1)     // Catch:{ Exception -> 0x07d9 }
            goto L_0x0770
        L_0x07d9:
            r3 = move-exception
            int r2 = X.AnonymousClass1Y3.Amr
            X.0UN r1 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "Exception trying to remove the path "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r7)
            r2.softReport(r4, r1, r3)
            goto L_0x0770
        L_0x07ee:
            java.util.HashMap r1 = r5.A01
            goto L_0x0780
        L_0x07f1:
            int r2 = X.AnonymousClass1Y3.AW2
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.cask.fbapps.controllers.FBStaleRemovalPluginController r1 = (com.facebook.storage.cask.fbapps.controllers.FBStaleRemovalPluginController) r1
            r1.A02()
            goto L_0x000a
        L_0x0800:
            int r2 = X.AnonymousClass1Y3.B0f
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.cask.fbapps.controllers.FBMaxSizePluginController r1 = (com.facebook.storage.cask.fbapps.controllers.FBMaxSizePluginController) r1
            r1.A02()
            goto L_0x000a
        L_0x080f:
            int r2 = X.AnonymousClass1Y3.AjB
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.cask.fbapps.controllers.FBEvictionPluginController r1 = (com.facebook.storage.cask.fbapps.controllers.FBEvictionPluginController) r1
            r1.A03()
            goto L_0x000a
        L_0x081e:
            int r2 = X.AnonymousClass1Y3.BJx
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.bigfoot.apps.fbapps.FBAppsBigFootForegroundWorker r1 = (com.facebook.storage.bigfoot.apps.fbapps.FBAppsBigFootForegroundWorker) r1
            java.util.concurrent.atomic.AtomicBoolean r2 = r1.A02
            r2.set(r4)
            goto L_0x000a
        L_0x082f:
            int r2 = X.AnonymousClass1Y3.BJx
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.storage.bigfoot.apps.fbapps.FBAppsBigFootForegroundWorker r4 = (com.facebook.storage.bigfoot.apps.fbapps.FBAppsBigFootForegroundWorker) r4
            int r3 = X.AnonymousClass1Y3.Al7
            X.0UN r2 = r4.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.2hg r1 = (X.C51742hg) r1
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 283540856048633(0x101e100010bf9, double:1.40087796166048E-309)
            boolean r1 = r3.Aem(r1)
            if (r1 == 0) goto L_0x000a
            java.util.concurrent.atomic.AtomicBoolean r1 = r4.A02
            r1.set(r6)
            java.util.concurrent.atomic.AtomicBoolean r1 = r4.A03
            boolean r1 = r1.getAndSet(r6)
            if (r1 != 0) goto L_0x000a
            r3 = 5
            int r2 = X.AnonymousClass1Y3.ARn
            X.0UN r1 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            java.util.concurrent.ScheduledExecutorService r3 = (java.util.concurrent.ScheduledExecutorService) r3
            X.8Ld r2 = new X.8Ld
            r2.<init>(r4)
            r1 = -589167943(0xffffffffdce202b9, float:-5.08930707E17)
            X.AnonymousClass07A.A04(r3, r2, r1)
            goto L_0x000a
        L_0x087f:
            int r2 = X.AnonymousClass1Y3.Agz
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector r2 = (com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector) r2
            monitor-enter(r2)
            java.util.Map r1 = r2.A03     // Catch:{ all -> 0x2074 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x2074 }
            if (r1 != 0) goto L_0x089e
            boolean r1 = com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector.A07(r2)     // Catch:{ all -> 0x2074 }
            if (r1 != 0) goto L_0x089e
            monitor-exit(r2)     // Catch:{ all -> 0x2074 }
            com.facebook.rti.shared.skywalker.SkywalkerSubscriptionConnector.A05(r2)
            goto L_0x000a
        L_0x089e:
            monitor-exit(r2)     // Catch:{ all -> 0x2074 }
            goto L_0x000a
        L_0x08a1:
            int r2 = X.AnonymousClass1Y3.AGI
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.resources.impl.DrawableCounterLogger r1 = (com.facebook.resources.impl.DrawableCounterLogger) r1
            com.facebook.resources.impl.DrawableCounterLogger.A01(r1)
            goto L_0x000a
        L_0x08b0:
            int r2 = X.AnonymousClass1Y3.A0b
            X.0UN r1 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.reactivesocket.AndroidLifecycleHandler r8 = (com.facebook.reactivesocket.AndroidLifecycleHandler) r8
            java.lang.ref.WeakReference r1 = r8.A02
            if (r1 == 0) goto L_0x000a
            java.lang.Object r1 = r1.get()
            if (r1 == 0) goto L_0x000a
            java.lang.ref.WeakReference r1 = r8.A02
            java.lang.Object r9 = r1.get()
            X.8Nm r9 = (X.C178898Nm) r9
            X.1Yd r4 = r8.A03
            r2 = 564487551714158(0x201660000036e, double:2.788939068069995E-309)
            r1 = 0
            int r7 = r4.AqL(r2, r1)
            if (r7 <= 0) goto L_0x08f4
            int r2 = X.AnonymousClass1Y3.B1j
            X.0UN r1 = r8.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.0VK r5 = (X.AnonymousClass0VK) r5
            X.8Nl r4 = new X.8Nl
            r4.<init>(r9)
            long r2 = (long) r7
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS
            X.0Y0 r1 = r5.C4Y(r4, r2, r1)
            r8.A01 = r1
            goto L_0x000a
        L_0x08f4:
            r9.onBackground()
            goto L_0x000a
        L_0x08f9:
            int r2 = X.AnonymousClass1Y3.A0b
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.reactivesocket.AndroidLifecycleHandler r4 = (com.facebook.reactivesocket.AndroidLifecycleHandler) r4
            java.lang.ref.WeakReference r1 = r4.A02
            if (r1 == 0) goto L_0x000a
            java.lang.Object r1 = r1.get()
            if (r1 == 0) goto L_0x000a
            java.lang.ref.WeakReference r1 = r4.A02
            java.lang.Object r3 = r1.get()
            X.8Nm r3 = (X.C178898Nm) r3
            com.google.common.util.concurrent.ListenableFuture r2 = r4.A01
            if (r2 == 0) goto L_0x0920
            r1 = 0
            r2.cancel(r1)
            r1 = 0
            r4.A01 = r1
        L_0x0920:
            r3.onForeground()
            goto L_0x000a
        L_0x0925:
            int r2 = X.AnonymousClass1Y3.AYx
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.quicklog.dataproviders.IoStatsProvider r1 = (com.facebook.quicklog.dataproviders.IoStatsProvider) r1
            com.facebook.quicklog.dataproviders.IoStatsProvider.A00(r1)
            goto L_0x000a
        L_0x0934:
            int r2 = X.AnonymousClass1Y3.AGf
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.push.mqtt.service.MqttClientStateManager r5 = (com.facebook.push.mqtt.service.MqttClientStateManager) r5
            java.lang.String r4 = "app_backgrounded"
            goto L_0x094d
        L_0x0941:
            int r2 = X.AnonymousClass1Y3.AGf
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.push.mqtt.service.MqttClientStateManager r5 = (com.facebook.push.mqtt.service.MqttClientStateManager) r5
            java.lang.String r4 = "app_backgrounded_immediate"
        L_0x094d:
            java.lang.String r1 = r5.A01
            if (r1 != 0) goto L_0x095e
            X.1Yd r3 = r5.A0D
            r1 = 848174437696051(0x3036900110233, double:4.190538513463356E-309)
            java.lang.String r1 = r3.B4C(r1)
            r5.A01 = r1
        L_0x095e:
            java.lang.String r1 = r5.A01
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x000a
            java.lang.String r1 = "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"
            com.facebook.push.mqtt.service.MqttClientStateManager.A03(r5, r1)
            goto L_0x000a
        L_0x096d:
            int r2 = X.AnonymousClass1Y3.AGf
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.push.mqtt.service.MqttClientStateManager r5 = (com.facebook.push.mqtt.service.MqttClientStateManager) r5
            java.lang.String r4 = "app_foregrounded"
            goto L_0x0986
        L_0x097a:
            int r2 = X.AnonymousClass1Y3.AGf
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.push.mqtt.service.MqttClientStateManager r5 = (com.facebook.push.mqtt.service.MqttClientStateManager) r5
            java.lang.String r4 = "app_foregrounded_immediate"
        L_0x0986:
            java.lang.String r1 = r5.A02
            if (r1 != 0) goto L_0x0997
            X.1Yd r3 = r5.A0D
            r1 = 848174437630514(0x3036900100232, double:4.19053851313956E-309)
            java.lang.String r1 = r3.B4C(r1)
            r5.A02 = r1
        L_0x0997:
            java.lang.String r1 = r5.A02
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x000a
            java.lang.String r1 = "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"
            com.facebook.push.mqtt.service.MqttClientStateManager.A03(r5, r1)
            goto L_0x000a
        L_0x09a6:
            int r2 = X.AnonymousClass1Y3.A6s
            X.0UN r1 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.push.crossapp.PendingReportedPackages r7 = (com.facebook.push.crossapp.PendingReportedPackages) r7
            int r3 = X.AnonymousClass1Y3.BCt
            X.0UN r2 = r7.A00
            r1 = 3
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            android.content.Context r1 = (android.content.Context) r1
            java.lang.String r1 = r1.getPackageName()
            boolean r1 = X.C134876Sm.A01(r1)
            if (r1 == 0) goto L_0x000a
            int r3 = X.AnonymousClass1Y3.AeN
            X.0UN r2 = r7.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.0h3 r1 = (X.C09340h3) r1
            boolean r1 = r1.A0Q()
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r7.A00
            r5 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r2 = (com.facebook.prefs.shared.FbSharedPreferences) r2
            X.1Y7 r1 = com.facebook.push.crossapp.PendingReportedPackages.A01
            java.util.Set r2 = r2.Arq(r1)
            boolean r1 = r2.isEmpty()
            if (r1 != 0) goto L_0x000a
            java.util.Iterator r8 = r2.iterator()
        L_0x09f0:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x000a
            java.lang.Object r2 = r8.next()
            X.1Y7 r2 = (X.AnonymousClass1Y7) r2
            X.1Y7 r1 = com.facebook.push.crossapp.PendingReportedPackages.A01
            java.lang.String r4 = r2.A06(r1)
            r3 = 2
            int r2 = X.AnonymousClass1Y3.A1G
            X.0UN r1 = r7.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0sy r1 = (X.C14290sy) r1
            android.content.pm.PackageInfo r1 = r1.A04(r4, r5)
            if (r1 == 0) goto L_0x0a17
            r7.A01(r4)
            goto L_0x09f0
        L_0x0a17:
            r3 = 3
            int r2 = X.AnonymousClass1Y3.BCt
            X.0UN r1 = r7.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            android.content.Context r2 = (android.content.Context) r2
            java.lang.String r1 = "retry"
            com.facebook.push.crossapp.PackageRemovedReporterService.A01(r2, r4, r1)
            goto L_0x09f0
        L_0x0a28:
            int r2 = X.AnonymousClass1Y3.Aok
            X.0UN r1 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.privacypermissionsnapshots.fb.PrivacyPermissionSnapshotsLoggerController r8 = (com.facebook.privacypermissionsnapshots.fb.PrivacyPermissionSnapshotsLoggerController) r8
            int r3 = X.AnonymousClass1Y3.B2Z
            X.0UN r2 = r8.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r4, r3, r2)
            com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore r7 = (com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore) r7
            java.lang.Long r1 = r7.A02()     // Catch:{ ClassCastException -> 0x0a49 }
            long r4 = r1.longValue()     // Catch:{ ClassCastException -> 0x0a49 }
            r1 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r1
            int r3 = (int) r4     // Catch:{ ClassCastException -> 0x0a49 }
            goto L_0x0a4a
        L_0x0a49:
            r3 = 0
        L_0x0a4a:
            int r1 = r7.A01()
            int r3 = r3 - r1
            int r2 = r7.A00
            r1 = 0
            if (r3 < r2) goto L_0x0a55
            r1 = 1
        L_0x0a55:
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.Ae0
            X.0UN r1 = r8.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.8aP r5 = (X.C180908aP) r5
            com.facebook.privacypermissionsnapshots.core.PrivacyPermissionStatusesFetcher r1 = r5.A01     // Catch:{ NullPointerException -> 0x0afb }
            com.google.common.collect.ImmutableMap r7 = r1.A00()     // Catch:{ NullPointerException -> 0x0afb }
            int r2 = X.AnonymousClass1Y3.B2Z     // Catch:{ NullPointerException -> 0x0afb }
            X.0UN r1 = r5.A00     // Catch:{ NullPointerException -> 0x0afb }
            r3 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ NullPointerException -> 0x0afb }
            com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore r1 = (com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore) r1     // Catch:{ NullPointerException -> 0x0afb }
            int r1 = r1.A01()     // Catch:{ NullPointerException -> 0x0afb }
            if (r1 != 0) goto L_0x0a8e
            X.0UN r1 = r5.A00     // Catch:{ NullPointerException -> 0x0afb }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ NullPointerException -> 0x0afb }
            com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore r1 = (com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore) r1     // Catch:{ NullPointerException -> 0x0afb }
            java.lang.Long r1 = r1.A02()     // Catch:{ ClassCastException -> 0x0a8d }
            long r2 = r1.longValue()     // Catch:{ ClassCastException -> 0x0a8d }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r8
            int r1 = (int) r2     // Catch:{ ClassCastException -> 0x0a8d }
            goto L_0x0a8e
        L_0x0a8d:
            r1 = 0
        L_0x0a8e:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)     // Catch:{ NullPointerException -> 0x0afb }
            int r3 = X.AnonymousClass1Y3.Ap7     // Catch:{ NullPointerException -> 0x0afb }
            X.0UN r2 = r5.A00     // Catch:{ NullPointerException -> 0x0afb }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ NullPointerException -> 0x0afb }
            X.1ZE r2 = (X.AnonymousClass1ZE) r2     // Catch:{ NullPointerException -> 0x0afb }
            java.lang.String r1 = "pdu_permissions_snapshot"
            X.0bW r3 = r2.A01(r1)     // Catch:{ NullPointerException -> 0x0afb }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ NullPointerException -> 0x0afb }
            r1 = 498(0x1f2, float:6.98E-43)
            r2.<init>(r3, r1)     // Catch:{ NullPointerException -> 0x0afb }
            boolean r1 = r2.A0G()     // Catch:{ NullPointerException -> 0x0afb }
            if (r1 == 0) goto L_0x000a
            java.lang.String r1 = "last_lookup_timestamp"
            r2.A0B(r1, r4)     // Catch:{ NullPointerException -> 0x0afb }
            java.lang.String r1 = "permission_statuses"
            r2.A0F(r1, r7)     // Catch:{ NullPointerException -> 0x0afb }
            r2.A06()     // Catch:{ NullPointerException -> 0x0afb }
            int r3 = X.AnonymousClass1Y3.B2Z     // Catch:{ NullPointerException -> 0x0afb }
            X.0UN r2 = r5.A00     // Catch:{ NullPointerException -> 0x0afb }
            r1 = 0
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r1, r3, r2)     // Catch:{ NullPointerException -> 0x0afb }
            com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore r9 = (com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore) r9     // Catch:{ NullPointerException -> 0x0afb }
            java.lang.Long r1 = r9.A02()     // Catch:{ ClassCastException -> 0x0ad4 }
            long r7 = r1.longValue()     // Catch:{ ClassCastException -> 0x0ad4 }
            r1 = 1000(0x3e8, double:4.94E-321)
            long r7 = r7 / r1
            int r4 = (int) r7     // Catch:{ ClassCastException -> 0x0ad4 }
            goto L_0x0ad5
        L_0x0ad4:
            r4 = 0
        L_0x0ad5:
            r3 = 0
            if (r4 >= 0) goto L_0x0ad9
            r4 = 0
        L_0x0ad9:
            int r2 = X.AnonymousClass1Y3.B6q     // Catch:{ ClassCastException -> 0x0af1 }
            X.0UN r1 = r9.A00     // Catch:{ ClassCastException -> 0x0af1 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ ClassCastException -> 0x0af1 }
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1     // Catch:{ ClassCastException -> 0x0af1 }
            X.1hn r2 = r1.edit()     // Catch:{ ClassCastException -> 0x0af1 }
            X.1Y8 r1 = com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore.A01     // Catch:{ ClassCastException -> 0x0af1 }
            r2.Bz6(r1, r4)     // Catch:{ ClassCastException -> 0x0af1 }
            r2.commit()     // Catch:{ ClassCastException -> 0x0af1 }
            goto L_0x000a
        L_0x0af1:
            r3 = move-exception
            java.lang.String r2 = "com.facebook.privacypermissionsnapshots.fb.FBPrivacyPermissionLastLookupStore"
            java.lang.String r1 = "Error while saving last lookup timestamp"
            X.C010708t.A0R(r2, r3, r1)     // Catch:{ NullPointerException -> 0x0afb }
            goto L_0x000a
        L_0x0afb:
            r3 = move-exception
            java.lang.String r2 = r5.A02
            java.lang.String r1 = "Last lookup stamp might not be available."
            X.C010708t.A0R(r2, r3, r1)
            goto L_0x000a
        L_0x0b05:
            int r2 = X.AnonymousClass1Y3.B68
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.perf.startupstatemachine.StartupStateMachine r4 = (com.facebook.perf.startupstatemachine.StartupStateMachine) r4
            monitor-enter(r4)
            int r1 = r4.A00     // Catch:{ all -> 0x2077 }
            r3 = 2
            r2 = 3
            if (r1 != r2) goto L_0x0b18
            r4.A00 = r3     // Catch:{ all -> 0x2077 }
        L_0x0b18:
            int r1 = r4.A02     // Catch:{ all -> 0x2077 }
            if (r1 != r2) goto L_0x0b1e
            r4.A02 = r3     // Catch:{ all -> 0x2077 }
        L_0x0b1e:
            int r1 = r4.A03     // Catch:{ all -> 0x2077 }
            if (r1 != r2) goto L_0x0b24
            r4.A03 = r3     // Catch:{ all -> 0x2077 }
        L_0x0b24:
            int r1 = r4.A01     // Catch:{ all -> 0x2077 }
            if (r1 != r2) goto L_0x0b2a
            r4.A01 = r3     // Catch:{ all -> 0x2077 }
        L_0x0b2a:
            monitor-exit(r4)
            goto L_0x000a
        L_0x0b2d:
            int r2 = X.AnonymousClass1Y3.B7r
            X.0UN r1 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.mobileconfig.init.MobileConfigApi2LoggerImpl r7 = (com.facebook.mobileconfig.init.MobileConfigApi2LoggerImpl) r7
            java.lang.Object r5 = r7.A01
            monitor-enter(r5)
            java.util.Map r1 = r7.A02     // Catch:{ all -> 0x207a }
            r1.size()     // Catch:{ all -> 0x207a }
            java.util.Map r1 = r7.A02     // Catch:{ all -> 0x207a }
            java.util.Set r1 = r1.entrySet()     // Catch:{ all -> 0x207a }
            java.util.Iterator r4 = r1.iterator()     // Catch:{ all -> 0x207a }
        L_0x0b49:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x207a }
            if (r1 == 0) goto L_0x0b84
            java.lang.Object r3 = r4.next()     // Catch:{ all -> 0x207a }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ all -> 0x207a }
            java.lang.Object r1 = r3.getKey()     // Catch:{ all -> 0x207a }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ all -> 0x207a }
            int r2 = r1.intValue()     // Catch:{ all -> 0x207a }
            java.lang.Object r1 = r3.getValue()     // Catch:{ all -> 0x207a }
            android.util.Pair r1 = (android.util.Pair) r1     // Catch:{ all -> 0x207a }
            java.lang.Object r1 = r1.first     // Catch:{ all -> 0x207a }
            X.0Wc r1 = (X.C04770Wc) r1     // Catch:{ all -> 0x207a }
            java.lang.String r2 = com.facebook.mobileconfig.init.MobileConfigApi2LoggerImpl.A01(r7, r2, r1)     // Catch:{ all -> 0x207a }
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)     // Catch:{ all -> 0x207a }
            if (r1 != 0) goto L_0x0b49
            r3.getKey()     // Catch:{ all -> 0x207a }
            java.lang.Object r1 = r3.getValue()     // Catch:{ all -> 0x207a }
            android.util.Pair r1 = (android.util.Pair) r1     // Catch:{ all -> 0x207a }
            java.lang.Object r1 = r1.second     // Catch:{ all -> 0x207a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x207a }
            com.facebook.mobileconfig.init.MobileConfigApi2LoggerImpl.A03(r7, r2, r1)     // Catch:{ all -> 0x207a }
            goto L_0x0b49
        L_0x0b84:
            java.util.Map r1 = r7.A02     // Catch:{ all -> 0x207a }
            r1.clear()     // Catch:{ all -> 0x207a }
            monitor-exit(r5)     // Catch:{ all -> 0x207a }
            java.util.concurrent.atomic.AtomicBoolean r2 = r7.A03
            r2.set(r6)
            goto L_0x000a
        L_0x0b91:
            int r2 = X.AnonymousClass1Y3.AqG
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.selfupdate2.SelfUpdate2AppStateListener r5 = (com.facebook.messaging.selfupdate2.SelfUpdate2AppStateListener) r5
            int r3 = X.AnonymousClass1Y3.B5j
            X.0UN r2 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.0Ud r1 = (X.AnonymousClass0Ud) r1
            android.app.Activity r4 = r1.A0B()
            if (r4 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.Abn
            X.0UN r1 = r5.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.14u r3 = (X.C187114u) r3
            r2 = 0
            android.content.Intent r1 = r3.A06(r4, r2, r2)
            if (r1 == 0) goto L_0x000a
            X.C417626w.A06(r1, r4)
            goto L_0x000a
        L_0x0bc1:
            int r2 = X.AnonymousClass1Y3.ALn
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.searchnullstate.PrefetcherManager r5 = (com.facebook.messaging.searchnullstate.PrefetcherManager) r5
            int r2 = X.AnonymousClass1Y3.BH4
            X.0UN r3 = r5.A00
            r1 = 5
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.00z r2 = (X.C001500z) r2
            X.00z r1 = X.C001500z.A07
            if (r2 != r1) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.A3W
            r1 = 3
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.1CK r1 = (X.AnonymousClass1CK) r1
            X.1CL r1 = r1.A01
            java.lang.Integer r2 = r1.A01()
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            if (r2 != r1) goto L_0x0c36
            r3 = 4
            int r2 = X.AnonymousClass1Y3.A6S
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            android.content.Context r1 = (android.content.Context) r1
            int r2 = X.AnonymousClass8Y7.A00(r1)
            r1 = 2012(0x7dc, float:2.82E-42)
            if (r2 < r1) goto L_0x0c36
            int r3 = X.AnonymousClass1Y3.Ark
            X.0UN r2 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.4Oo r2 = (X.C89324Oo) r2
            r1 = 76
            java.lang.String r1 = X.C99084oO.$const$string(r1)
            java.lang.Long r9 = r2.A01(r1)
            int r3 = X.AnonymousClass1Y3.AgK
            X.0UN r2 = r5.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.06B r1 = (X.AnonymousClass06B) r1
            long r7 = r1.now()
            r1 = 259200000(0xf731400, double:1.280618154E-315)
            long r7 = r7 - r1
            r4 = 0
            if (r9 == 0) goto L_0x0c33
            long r2 = r9.longValue()
            int r1 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r1 < 0) goto L_0x0c33
            r4 = 1
        L_0x0c33:
            r1 = 1
            if (r4 != 0) goto L_0x0c37
        L_0x0c36:
            r1 = 0
        L_0x0c37:
            if (r1 == 0) goto L_0x000a
            r3 = 0
            int r2 = X.AnonymousClass1Y3.Apb
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.3yy r1 = (X.C84173yy) r1
            int r3 = X.AnonymousClass1Y3.AD0
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.73y r2 = (X.C1525173y) r2
            r1 = 0
            r2.CHB(r1)
            goto L_0x000a
        L_0x0c55:
            int r2 = X.AnonymousClass1Y3.ApS
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.screenshotdetection.ThreadScreenshotDetector r3 = (com.facebook.messaging.screenshotdetection.ThreadScreenshotDetector) r3
            X.0pP r2 = r3.A02
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r6)
            boolean r1 = r2.A08(r1)
            if (r1 == 0) goto L_0x000a
            X.8Ul r2 = r3.A04
            r1 = 0
            r2.A00 = r1
            android.content.Context r1 = r3.A00
            android.content.ContentResolver r2 = r1.getContentResolver()
            X.8Ul r1 = r3.A04
            r2.unregisterContentObserver(r1)
            X.8Uq r2 = r3.A05
            java.lang.String r1 = "App went to background."
            r2.BW9(r1)
            goto L_0x000a
        L_0x0c84:
            int r2 = X.AnonymousClass1Y3.ApS
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.screenshotdetection.ThreadScreenshotDetector r5 = (com.facebook.messaging.screenshotdetection.ThreadScreenshotDetector) r5
            X.0pP r2 = r5.A02
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r6)
            boolean r1 = r2.A08(r1)
            if (r1 == 0) goto L_0x000a
            android.content.Context r1 = r5.A00
            android.content.ContentResolver r4 = r1.getContentResolver()
            android.net.Uri r3 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            X.8Ul r2 = r5.A04
            r4.registerContentObserver(r3, r6, r2)
            X.8Ul r1 = r5.A04
            r1.A00 = r5
            X.8Uq r2 = r5.A05
            java.lang.String r1 = "App returned from background."
            r2.BWA(r1)
            goto L_0x000a
        L_0x0cb4:
            int r2 = X.AnonymousClass1Y3.ARh
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.notify.logging.conditionalworkerimpl.PushSettingsReporter r4 = (com.facebook.messaging.notify.logging.conditionalworkerimpl.PushSettingsReporter) r4
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r4.A00
            r1 = 5
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 282943855855692(0x101560005084c, double:1.39792838880155E-309)
            boolean r1 = r3.Aem(r1)
            if (r1 == 0) goto L_0x000a
            r4.A01()
            r4.A02()
            goto L_0x000a
        L_0x0cdc:
            int r2 = X.AnonymousClass1Y3.BSq
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.instagram.fetch.EligibleInstagramAccountBackgroundFetcher r5 = (com.facebook.messaging.instagram.fetch.EligibleInstagramAccountBackgroundFetcher) r5
            int r3 = X.AnonymousClass1Y3.AgK
            X.0UN r2 = r5.A00
            r1 = 3
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.06B r1 = (X.AnonymousClass06B) r1
            long r3 = r1.now()
            int r7 = X.AnonymousClass1Y3.B6q
            X.0UN r2 = r5.A00
            r1 = 4
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r7, r2)
            com.facebook.prefs.shared.FbSharedPreferences r8 = (com.facebook.prefs.shared.FbSharedPreferences) r8
            X.1Y8 r7 = X.C51702hZ.A02
            r1 = 0
            long r1 = r8.At2(r7, r1)
            long r9 = r3 - r1
            int r7 = X.AnonymousClass1Y3.AXb
            X.0UN r2 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r7, r2)
            X.2ha r1 = (X.C51712ha) r1
            X.1Yd r2 = r1.A00
            r7 = 563920616096369(0x200e200010271, double:2.78613803394846E-309)
            r1 = 15
            int r1 = r2.AqO(r7, r1)
            long r7 = (long) r1
            r1 = 60000(0xea60, double:2.9644E-319)
            long r7 = r7 * r1
            int r1 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r1 < 0) goto L_0x000a
            com.facebook.messaging.instagram.fetch.EligibleInstagramAccountBackgroundFetcher.A01(r5, r3)
            goto L_0x000a
        L_0x0d2f:
            int r2 = X.AnonymousClass1Y3.AD6
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            X.4ZY r3 = (X.AnonymousClass4ZY) r3
            int r2 = X.AnonymousClass1Y3.Af2
            X.0UN r1 = r3.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            X.4ZZ r1 = (X.AnonymousClass4ZZ) r1
            X.AnonymousClass4ZY.A01(r3, r1)
            goto L_0x000a
        L_0x0d48:
            int r2 = X.AnonymousClass1Y3.BJP
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.inbox2.activenow.loader.PrefetcherManager r1 = (com.facebook.messaging.inbox2.activenow.loader.PrefetcherManager) r1
            r1.A01()
            goto L_0x000a
        L_0x0d57:
            int r2 = X.AnonymousClass1Y3.AxJ
            X.0UN r1 = r0.A00
            X.AnonymousClass1XX.A03(r2, r1)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            com.facebook.messaging.filelogger.MessagingFileLogger.A02 = r1
            r1 = 0
            if (r1 == 0) goto L_0x000a
            r5 = 0
            monitor-enter(r5)
            boolean r1 = X.C70993be.A02(r5)     // Catch:{ all -> 0x208f }
            if (r1 == 0) goto L_0x11de
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x208f }
            r2 = 3
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.DAYS     // Catch:{ all -> 0x208f }
            long r1 = r4.convert(r2, r1)     // Catch:{ all -> 0x208f }
            X.C70993be.A03(r5, r1)     // Catch:{ all -> 0x208f }
            goto L_0x11de
        L_0x0d7e:
            int r2 = X.AnonymousClass1Y3.ABG
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.cowatch.tracker.LivingRoomThreadTracker r5 = (com.facebook.messaging.cowatch.tracker.LivingRoomThreadTracker) r5
            int r3 = X.AnonymousClass1Y3.BH4
            X.0UN r2 = r5.A01
            r1 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.00z r2 = (X.C001500z) r2
            X.00z r1 = X.C001500z.A07
            if (r2 != r1) goto L_0x000a
            com.facebook.messaging.cowatch.tracker.LivingRoomThreadTracker.A01(r5)
            X.58I r4 = new X.58I
            r4.<init>(r5)
            int r3 = X.AnonymousClass1Y3.AKb
            X.0UN r2 = r5.A01
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0Ut r1 = (X.C04460Ut) r1
            X.0bl r2 = r1.BMm()
            java.lang.String r1 = "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"
            r2.A02(r1, r4)
            X.0c5 r1 = r2.A00()
            r1.A00()
            goto L_0x000a
        L_0x0dbc:
            int r2 = X.AnonymousClass1Y3.Abc
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.contactstab.loader.StatusController r4 = (com.facebook.messaging.contactstab.loader.StatusController) r4
            int r3 = X.AnonymousClass1Y3.BAw
            X.0UN r2 = r4.A00
            r1 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.4LW r2 = (X.AnonymousClass4LW) r2
            X.4MG r1 = new X.4MG
            r1.<init>(r4)
            r2.A01 = r1
            r4.A01(r6)
            goto L_0x000a
        L_0x0ddd:
            int r2 = X.AnonymousClass1Y3.AOs
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.contacts.loader.nonwork.ContactObserversRegistrationHandler r1 = (com.facebook.messaging.contacts.loader.nonwork.ContactObserversRegistrationHandler) r1
            int r2 = X.AnonymousClass1Y3.BH4
            X.0UN r3 = r1.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r3)
            X.00z r2 = (X.C001500z) r2
            X.00z r1 = X.C001500z.A03
            if (r2 == r1) goto L_0x000a
            int r1 = X.AnonymousClass1Y3.BHJ
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r1, r3)
            X.EVk r3 = (X.C29334EVk) r3
            android.os.Handler r2 = r3.A05
            java.lang.Runnable r1 = r3.A03
            X.AnonymousClass00S.A02(r2, r1)
            android.content.ContentResolver r2 = r3.A04
            android.database.ContentObserver r1 = r3.A01
            r2.unregisterContentObserver(r1)
            r1 = 0
            r3.A00 = r1
            goto L_0x000a
        L_0x0e11:
            int r2 = X.AnonymousClass1Y3.AOs
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.contacts.loader.nonwork.ContactObserversRegistrationHandler r1 = (com.facebook.messaging.contacts.loader.nonwork.ContactObserversRegistrationHandler) r1
            int r2 = X.AnonymousClass1Y3.BH4
            X.0UN r3 = r1.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r3)
            X.00z r2 = (X.C001500z) r2
            X.00z r1 = X.C001500z.A03
            if (r2 == r1) goto L_0x000a
            int r1 = X.AnonymousClass1Y3.BHJ
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r4, r1, r3)
            X.EVk r5 = (X.C29334EVk) r5
            int r2 = X.AnonymousClass1Y3.A6S
            X.0UN r1 = r5.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            android.content.Context r2 = (android.content.Context) r2
            boolean r1 = X.C51692hY.A01(r2)
            if (r1 == 0) goto L_0x0e4e
            r1 = 4
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r1)
            int r1 = r2.checkSelfPermission(r1)
            if (r1 == 0) goto L_0x0e4e
            goto L_0x000a
        L_0x0e4e:
            android.content.ContentResolver r4 = r5.A04
            android.net.Uri r3 = android.provider.ContactsContract.Contacts.CONTENT_URI
            android.database.ContentObserver r1 = r5.A01
            r4.registerContentObserver(r3, r6, r1)
            goto L_0x000a
        L_0x0e59:
            int r2 = X.AnonymousClass1Y3.A9K
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.chatheads.service.VideoServiceAppStateListener r1 = (com.facebook.messaging.chatheads.service.VideoServiceAppStateListener) r1
            r1.A02()
            goto L_0x000a
        L_0x0e68:
            int r2 = X.AnonymousClass1Y3.BPf
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.chatheads.ChatHeadsInitializer r1 = (com.facebook.messaging.chatheads.ChatHeadsInitializer) r1
            r1.A01()
            goto L_0x000a
        L_0x0e77:
            int r2 = X.AnonymousClass1Y3.BCF
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.analytics.perf.PostStartupTracker r5 = (com.facebook.messaging.analytics.perf.PostStartupTracker) r5
            int r3 = X.AnonymousClass1Y3.Alk
            X.0UN r2 = r5.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r3, r2)
            X.1cW r1 = (X.C27021cW) r1
            r4 = 5505205(0x5400b5, float:7.714435E-39)
            r1.A02(r4)
            int r3 = X.AnonymousClass1Y3.BBd
            X.0UN r2 = r5.A01
            r1 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r3, r2)
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2
            r1 = 2
            r2.markerEnd(r4, r1)
            goto L_0x000a
        L_0x0ea2:
            int r2 = X.AnonymousClass1Y3.ApC
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.messaging.analytics.perf.MessagingInteractionStateManager r1 = (com.facebook.messaging.analytics.perf.MessagingInteractionStateManager) r1
            X.0zB r2 = r1.A00
            monitor-enter(r2)
            X.0zB r1 = r1.A00     // Catch:{ all -> 0x207d }
            r1.A09()     // Catch:{ all -> 0x207d }
            monitor-exit(r2)     // Catch:{ all -> 0x207d }
            goto L_0x000a
        L_0x0eb7:
            int r2 = X.AnonymousClass1Y3.AUK
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener r1 = (com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener) r1
            int r3 = X.AnonymousClass1Y3.AIJ
            X.0UN r2 = r1.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.1f5 r1 = (X.C28611f5) r1
            X.1fB r2 = r1.A01()
            X.1fB r1 = X.C28671fB.A00()
            X.26L r1 = r1.A06()
            boolean r1 = r1.A01()
            if (r1 == 0) goto L_0x000a
            X.0Tq r1 = r2.A0B
            if (r1 == 0) goto L_0x000a
            X.1fB r1 = X.C28671fB.A00()
            X.1yC r2 = X.C28671fB.A01(r1)
            monitor-enter(r2)
            boolean r1 = X.C39211yg.A00()     // Catch:{ all -> 0x2080 }
            if (r1 != 0) goto L_0x0ef5
            X.2SE r1 = r2.A00     // Catch:{ all -> 0x2080 }
            r1.A03()     // Catch:{ all -> 0x2080 }
        L_0x0ef5:
            monitor-exit(r2)
            goto L_0x000a
        L_0x0ef8:
            int r2 = X.AnonymousClass1Y3.AUK
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener r1 = (com.facebook.memory.fbmemorymanager.LifecycleMemoryLeakListener) r1
            int r3 = X.AnonymousClass1Y3.AIJ
            X.0UN r2 = r1.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.1f5 r1 = (X.C28611f5) r1
            X.1fB r2 = r1.A01()
            X.1fB r1 = X.C28671fB.A00()
            X.26L r1 = r1.A06()
            boolean r1 = r1.A01()
            if (r1 == 0) goto L_0x000a
            X.0Tq r1 = r2.A0B
            if (r1 == 0) goto L_0x000a
            X.1fB r1 = X.C28671fB.A00()
            X.1yC r2 = X.C28671fB.A01(r1)
            boolean r1 = X.C39211yg.A00()
            if (r1 != 0) goto L_0x000a
            X.2SE r1 = r2.A00
            r1.A02()
            goto L_0x000a
        L_0x0f37:
            int r2 = X.AnonymousClass1Y3.As8
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.memory.fbmemorymanager.FBMemoryManagerHooks r1 = (com.facebook.memory.fbmemorymanager.FBMemoryManagerHooks) r1
            int r3 = X.AnonymousClass1Y3.AIJ
            X.0UN r2 = r1.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.1f5 r1 = (X.C28611f5) r1
            X.1fB r1 = r1.A01()
            X.1qk r3 = r1.A04()
            if (r3 == 0) goto L_0x000a
            r1 = 2
            r2 = 0
            r3.A00(r1, r2)
            r1 = 6
            r3.A00(r1, r2)
            goto L_0x000a
        L_0x0f60:
            int r2 = X.AnonymousClass1Y3.As8
            X.0UN r1 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.memory.fbmemorymanager.FBMemoryManagerHooks r8 = (com.facebook.memory.fbmemorymanager.FBMemoryManagerHooks) r8
            int r3 = X.AnonymousClass1Y3.AIJ
            X.0UN r2 = r8.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.1f5 r1 = (X.C28611f5) r1
            X.1fB r1 = r1.A01()
            X.1qk r5 = r1.A04()
            if (r5 == 0) goto L_0x000a
            r1 = 0
            r5.A00(r6, r1)
            r4 = 5
            X.EhX r9 = new X.EhX
            int r2 = X.AnonymousClass1Y3.Azv
            X.0UN r1 = r8.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1qn r1 = (X.C35171qn) r1
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 566325797914250(0x203120003068a, double:2.79802121103063E-309)
            long r10 = r3.At0(r1)
            int r2 = X.AnonymousClass1Y3.Azv
            X.0UN r1 = r8.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1qn r1 = (X.C35171qn) r1
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 566325797979787(0x203120004068b, double:2.79802121135443E-309)
            long r12 = r3.At0(r1)
            java.util.concurrent.TimeUnit r14 = java.util.concurrent.TimeUnit.SECONDS
            r9.<init>(r10, r12, r14)
            r5.A00(r4, r9)
            goto L_0x000a
        L_0x0fcb:
            int r2 = X.AnonymousClass1Y3.A7p
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.location.foreground.ForegroundLocationFrameworkController r4 = (com.facebook.location.foreground.ForegroundLocationFrameworkController) r4
            int r3 = X.AnonymousClass1Y3.BGZ
            X.0UN r2 = r4.A05
            r1 = 8
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.4VN r1 = (X.AnonymousClass4VN) r1
            boolean r1 = r1.A02()
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.Agg
            X.0UN r1 = r4.A05
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            android.os.Handler r3 = (android.os.Handler) r3
            X.6TK r2 = new X.6TK
            r2.<init>(r4)
            r1 = 124868490(0x771578a, float:1.815655E-34)
            X.AnonymousClass00S.A04(r3, r2, r1)
            goto L_0x000a
        L_0x0ffe:
            int r2 = X.AnonymousClass1Y3.A7p
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.location.foreground.ForegroundLocationFrameworkController r4 = (com.facebook.location.foreground.ForegroundLocationFrameworkController) r4
            int r3 = X.AnonymousClass1Y3.BGZ
            X.0UN r2 = r4.A05
            r1 = 8
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.4VN r1 = (X.AnonymousClass4VN) r1
            boolean r1 = r1.A02()
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.Agg
            X.0UN r1 = r4.A05
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            android.os.Handler r3 = (android.os.Handler) r3
            X.6TF r2 = new X.6TF
            r2.<init>(r4)
            r1 = 985895651(0x3ac392e3, float:0.0014921095)
            X.AnonymousClass00S.A04(r3, r2, r1)
            goto L_0x000a
        L_0x1031:
            int r2 = X.AnonymousClass1Y3.AUn
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.keyframes.fb.FbKeyframesAppStateManager r1 = (com.facebook.keyframes.fb.FbKeyframesAppStateManager) r1
            java.lang.Object r7 = r1.A01
            monitor-enter(r7)
            java.util.Set r1 = r1.A00     // Catch:{ all -> 0x2083 }
            if (r1 == 0) goto L_0x1067
            java.util.Iterator r4 = r1.iterator()     // Catch:{ all -> 0x2083 }
        L_0x1046:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x2083 }
            if (r1 == 0) goto L_0x1067
            java.lang.Object r1 = r4.next()     // Catch:{ all -> 0x2083 }
            X.Bea r1 = (X.C23408Bea) r1     // Catch:{ all -> 0x2083 }
            X.Bej r3 = r1.A02     // Catch:{ all -> 0x2083 }
            if (r3 == 0) goto L_0x1046
            boolean r1 = r3.isPlaying()     // Catch:{ all -> 0x2083 }
            if (r1 == 0) goto L_0x1046
            X.1LM r2 = r5.A00     // Catch:{ all -> 0x2083 }
            X.Bel r1 = new X.Bel     // Catch:{ all -> 0x2083 }
            r1.<init>(r3)     // Catch:{ all -> 0x2083 }
            r2.A00(r1)     // Catch:{ all -> 0x2083 }
            goto L_0x1046
        L_0x1067:
            monitor-exit(r7)     // Catch:{ all -> 0x2083 }
            goto L_0x000a
        L_0x106a:
            int r2 = X.AnonymousClass1Y3.AUn
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.keyframes.fb.FbKeyframesAppStateManager r1 = (com.facebook.keyframes.fb.FbKeyframesAppStateManager) r1
            java.lang.Object r7 = r1.A01
            monitor-enter(r7)
            java.util.Set r1 = r1.A00     // Catch:{ all -> 0x2086 }
            if (r1 == 0) goto L_0x10aa
            java.util.Iterator r4 = r1.iterator()     // Catch:{ all -> 0x2086 }
        L_0x107f:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x2086 }
            if (r1 == 0) goto L_0x10aa
            java.lang.Object r1 = r4.next()     // Catch:{ all -> 0x2086 }
            X.Bea r1 = (X.C23408Bea) r1     // Catch:{ all -> 0x2086 }
            X.Bej r3 = r1.A02     // Catch:{ all -> 0x2086 }
            if (r3 == 0) goto L_0x107f
            boolean r1 = r3.isPlaying()     // Catch:{ all -> 0x2086 }
            if (r1 != 0) goto L_0x107f
            X.Bey r1 = r3.A06     // Catch:{ all -> 0x2086 }
            int r2 = r1.A04     // Catch:{ all -> 0x2086 }
            r1 = 0
            if (r2 != 0) goto L_0x109d
            r1 = 1
        L_0x109d:
            if (r1 != 0) goto L_0x107f
            X.1LM r2 = r5.A00     // Catch:{ all -> 0x2086 }
            X.Bek r1 = new X.Bek     // Catch:{ all -> 0x2086 }
            r1.<init>(r3)     // Catch:{ all -> 0x2086 }
            r2.A00(r1)     // Catch:{ all -> 0x2086 }
            goto L_0x107f
        L_0x10aa:
            monitor-exit(r7)     // Catch:{ all -> 0x2086 }
            goto L_0x000a
        L_0x10ad:
            int r2 = X.AnonymousClass1Y3.BK8
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.interstitial.manager.InterstitialDataCleaner r1 = (com.facebook.interstitial.manager.InterstitialDataCleaner) r1
            int r3 = X.AnonymousClass1Y3.BE8
            X.0UN r2 = r1.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1F1 r1 = (X.AnonymousClass1F1) r1
            r1.clearUserData()
            goto L_0x000a
        L_0x10c7:
            int r2 = X.AnonymousClass1Y3.AHc
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.http.networkstatelogger.NetworkStateLogger r1 = (com.facebook.http.networkstatelogger.NetworkStateLogger) r1
            r1.A03()
            goto L_0x000a
        L_0x10d6:
            int r2 = X.AnonymousClass1Y3.AHc
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.http.networkstatelogger.NetworkStateLogger r3 = (com.facebook.http.networkstatelogger.NetworkStateLogger) r3
            monitor-enter(r3)
            int r2 = r3.A00     // Catch:{ all -> 0x2089 }
            if (r2 == 0) goto L_0x1173
            boolean r1 = r3.A0F     // Catch:{ all -> 0x2089 }
            if (r1 != 0) goto L_0x1173
            r3.A0D = r4     // Catch:{ all -> 0x2089 }
            java.util.Random r1 = new java.util.Random     // Catch:{ all -> 0x2089 }
            r1.<init>()     // Catch:{ all -> 0x2089 }
            int r1 = r1.nextInt(r2)     // Catch:{ all -> 0x2089 }
            if (r1 != 0) goto L_0x1173
            r3.A0D = r6     // Catch:{ all -> 0x2089 }
            r5 = 2
            int r2 = X.AnonymousClass1Y3.B5j     // Catch:{ all -> 0x2089 }
            X.0UN r1 = r3.A04     // Catch:{ all -> 0x2089 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ all -> 0x2089 }
            X.0Ud r1 = (X.AnonymousClass0Ud) r1     // Catch:{ all -> 0x2089 }
            java.lang.String r1 = r1.A05     // Catch:{ all -> 0x2089 }
            r3.A0A = r1     // Catch:{ all -> 0x2089 }
            if (r1 != 0) goto L_0x1113
            java.util.UUID r1 = X.C188215g.A00()     // Catch:{ all -> 0x2089 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x2089 }
            r3.A0A = r1     // Catch:{ all -> 0x2089 }
        L_0x1113:
            r5 = 0
            r3.A09 = r5     // Catch:{ all -> 0x2089 }
            r8 = 0
            int r2 = X.AnonymousClass1Y3.BPU     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            X.0UN r1 = r3.A04     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            r7 = 5
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r2, r1)     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            X.25O r1 = (X.AnonymousClass25O) r1     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            android.location.LocationManager r2 = r1.A01     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            java.lang.String r1 = "network"
            boolean r1 = r2.isProviderEnabled(r1)     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            if (r1 != 0) goto L_0x1140
            int r2 = X.AnonymousClass1Y3.BPU     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            X.0UN r1 = r3.A04     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r2, r1)     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            X.25O r1 = (X.AnonymousClass25O) r1     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            android.location.LocationManager r2 = r1.A01     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            java.lang.String r1 = "gps"
            boolean r1 = r2.isProviderEnabled(r1)     // Catch:{ IllegalArgumentException | SecurityException -> 0x1141 }
            if (r1 == 0) goto L_0x1141
        L_0x1140:
            r8 = 1
        L_0x1141:
            if (r8 == 0) goto L_0x114d
            java.util.UUID r1 = X.C188215g.A00()     // Catch:{ all -> 0x2089 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x2089 }
            r3.A09 = r1     // Catch:{ all -> 0x2089 }
        L_0x114d:
            r3.A01 = r4     // Catch:{ all -> 0x2089 }
            r1 = 0
            r3.A03 = r1     // Catch:{ all -> 0x2089 }
            r1 = -1
            r3.A02 = r1     // Catch:{ all -> 0x2089 }
            r3.A07 = r5     // Catch:{ all -> 0x2089 }
            r3.A06 = r5     // Catch:{ all -> 0x2089 }
            r3.A0B = r4     // Catch:{ all -> 0x2089 }
            r3.A05 = r5     // Catch:{ all -> 0x2089 }
            java.util.Map r1 = r3.mServerStats     // Catch:{ all -> 0x2089 }
            r1.clear()     // Catch:{ all -> 0x2089 }
            java.util.Map r1 = r3.mNetworkInfoMap     // Catch:{ all -> 0x2089 }
            r1.clear()     // Catch:{ all -> 0x2089 }
            r3.A0F = r6     // Catch:{ all -> 0x2089 }
            r3.A0E = r4     // Catch:{ all -> 0x2089 }
            java.lang.String r1 = "SESSION_START"
            r3.A08 = r1     // Catch:{ all -> 0x2089 }
            com.facebook.http.networkstatelogger.NetworkStateLogger.A01(r3)     // Catch:{ all -> 0x2089 }
        L_0x1173:
            monitor-exit(r3)
            goto L_0x000a
        L_0x1176:
            int r2 = X.AnonymousClass1Y3.AHc
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.http.networkstatelogger.NetworkStateLogger r5 = (com.facebook.http.networkstatelogger.NetworkStateLogger) r5
            monitor-enter(r5)
            boolean r1 = r5.A0C     // Catch:{ all -> 0x208f }
            if (r1 != 0) goto L_0x11de
            r5.A0C = r6     // Catch:{ all -> 0x208f }
            r3 = 6
            int r2 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x208f }
            X.0UN r1 = r5.A04     // Catch:{ all -> 0x208f }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x208f }
            X.1Yd r3 = (X.C25051Yd) r3     // Catch:{ all -> 0x208f }
            r1 = 563521184661993(0x20085000901e9, double:2.78416458045252E-309)
            long r2 = r3.At0(r1)     // Catch:{ all -> 0x208f }
            int r1 = (int) r2     // Catch:{ all -> 0x208f }
            r5.A00 = r1     // Catch:{ all -> 0x208f }
            if (r1 == 0) goto L_0x11de
            X.4qu r4 = new X.4qu     // Catch:{ all -> 0x208f }
            r4.<init>(r5)     // Catch:{ all -> 0x208f }
            int r3 = X.AnonymousClass1Y3.Awi     // Catch:{ all -> 0x208f }
            X.0UN r2 = r5.A04     // Catch:{ all -> 0x208f }
            r1 = 11
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r3, r2)     // Catch:{ all -> 0x208f }
            X.2Y9 r2 = (X.AnonymousClass2Y9) r2     // Catch:{ all -> 0x208f }
            monitor-enter(r2)     // Catch:{ all -> 0x208f }
            java.util.ArrayList r1 = r2.A05     // Catch:{ all -> 0x208c }
            r1.add(r4)     // Catch:{ all -> 0x208c }
            monitor-exit(r2)     // Catch:{ all -> 0x208f }
            X.4qt r4 = new X.4qt     // Catch:{ all -> 0x208f }
            r4.<init>(r5)     // Catch:{ all -> 0x208f }
            int r3 = X.AnonymousClass1Y3.AKb     // Catch:{ all -> 0x208f }
            X.0UN r2 = r5.A04     // Catch:{ all -> 0x208f }
            r1 = 8
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)     // Catch:{ all -> 0x208f }
            X.0Ut r1 = (X.C04460Ut) r1     // Catch:{ all -> 0x208f }
            X.0bl r2 = r1.BMm()     // Catch:{ all -> 0x208f }
            java.lang.String r1 = "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"
            r2.A02(r1, r4)     // Catch:{ all -> 0x208f }
            java.lang.String r1 = "com.facebook.common.hardware.ACTION_INET_CONDITION_CHANGED"
            r2.A02(r1, r4)     // Catch:{ all -> 0x208f }
            X.0c5 r1 = r2.A00()     // Catch:{ all -> 0x208f }
            r1.A00()     // Catch:{ all -> 0x208f }
        L_0x11de:
            monitor-exit(r5)
            goto L_0x000a
        L_0x11e1:
            int r2 = X.AnonymousClass1Y3.Asb
            X.0UN r1 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.growth.sem.SemColdStartLogger r8 = (com.facebook.growth.sem.SemColdStartLogger) r8
            int r3 = X.AnonymousClass1Y3.B5j
            X.0UN r2 = r8.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0Ud r1 = (X.AnonymousClass0Ud) r1
            long r3 = r1.A0M
            int r5 = X.AnonymousClass1Y3.B6q
            X.0UN r2 = r8.A00
            r1 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r5, r2)
            com.facebook.prefs.shared.FbSharedPreferences r7 = (com.facebook.prefs.shared.FbSharedPreferences) r7
            X.1Y7 r5 = X.AnonymousClass2SR.A02
            r1 = 0
            long r13 = r7.At2(r5, r1)
            int r1 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r1 == 0) goto L_0x1218
            long r11 = r3 - r13
            r9 = 86400000(0x5265c00, double:4.2687272E-316)
            int r1 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r1 <= 0) goto L_0x1237
        L_0x1218:
            r1 = 1000(0x3e8, double:4.94E-321)
            long r1 = r3 / r1
            java.lang.String r7 = java.lang.String.valueOf(r1)
            int r2 = X.AnonymousClass1Y3.AZb
            X.0UN r1 = r8.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.2oJ r1 = (X.C55582oJ) r1
            java.util.concurrent.ExecutorService r5 = r1.A03
            X.4Kb r2 = new X.4Kb
            r2.<init>(r1, r7)
            r1 = 546334316(0x2090666c, float:2.4462326E-19)
            X.AnonymousClass07A.A04(r5, r2, r1)
        L_0x1237:
            int r5 = X.AnonymousClass1Y3.B6q
            X.0UN r2 = r8.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r5, r2)
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1
            X.1hn r2 = r1.edit()
            X.1Y7 r1 = X.AnonymousClass2SR.A02
            r2.BzA(r1, r3)
            r2.commit()
            goto L_0x000a
        L_0x1250:
            int r2 = X.AnonymousClass1Y3.BKC
            X.0UN r1 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl r8 = (com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl) r8
            int r2 = X.AnonymousClass1Y3.ANU
            X.0UN r1 = r8.A00
            r7 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.4RE r4 = (X.AnonymousClass4RE) r4
            java.lang.Boolean r1 = r4.A06
            if (r1 != 0) goto L_0x1282
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r7, r2, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 281904473637712(0x1006400030350, double:1.39279315833353E-309)
            boolean r1 = r3.Aem(r1)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r4.A06 = r1
        L_0x1282:
            java.lang.Boolean r1 = r4.A06
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x000a
            java.util.HashSet r5 = new java.util.HashSet
            java.util.Set r1 = r8.A02
            r5.<init>(r1)
            java.util.Iterator r4 = r5.iterator()
        L_0x1295:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x12c1
            java.lang.Object r3 = r4.next()
            X.A18 r3 = (X.A18) r3
            X.8ed r1 = r3.Azq()
            if (r1 == 0) goto L_0x1295
            int r2 = X.AnonymousClass1Y3.ANU
            X.0UN r1 = r8.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r2, r1)
            X.4RE r2 = (X.AnonymousClass4RE) r2
            X.8ed r1 = r3.Azq()
            java.lang.String r1 = r1.A06
            boolean r1 = r2.A04(r1)
            if (r1 == 0) goto L_0x1295
            r4.remove()
            goto L_0x1295
        L_0x12c1:
            r8.A04(r5)
            goto L_0x000a
        L_0x12c6:
            int r2 = X.AnonymousClass1Y3.B5x
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.graphql.modelutil.parcel.ModelParcelHelperInitQPLAppJob r4 = (com.facebook.graphql.modelutil.parcel.ModelParcelHelperInitQPLAppJob) r4
            com.facebook.quicklog.QuickPerformanceLogger r3 = r4.A01
            com.facebook.quicklog.QuickPerformanceLogger r1 = X.AnonymousClass324.A01
            if (r1 == 0) goto L_0x12dd
            java.lang.String r2 = "ModelParcelHelper"
            java.lang.String r1 = "QPL logger initialized more than once"
            X.C010708t.A0J(r2, r1)
        L_0x12dd:
            X.AnonymousClass324.A01 = r3
            X.1Yd r3 = r4.A00
            X.1Yd r1 = X.AnonymousClass324.A00
            if (r1 == 0) goto L_0x12ec
            java.lang.String r2 = "ModelParcelHelper"
            java.lang.String r1 = "Mobile config initialized more than once"
            X.C010708t.A0J(r2, r1)
        L_0x12ec:
            X.AnonymousClass324.A00 = r3
            goto L_0x000a
        L_0x12f0:
            int r2 = X.AnonymousClass1Y3.B5U
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.graphql.fleetbeacon.FleetBeaconStartupTrigger r4 = (com.facebook.graphql.fleetbeacon.FleetBeaconStartupTrigger) r4
            int r3 = X.AnonymousClass1Y3.B5j
            X.0UN r2 = r4.A00
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0Ud r1 = (X.AnonymousClass0Ud) r1
            boolean r1 = r1.A0G()
            if (r1 != 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 284734857351851(0x102f7000712ab, double:1.406777111910575E-309)
            boolean r1 = r3.Aem(r1)
            r5 = 0
            if (r1 == 0) goto L_0x134e
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 1129159787544804(0x402f7000800e4, double:5.578790596912883E-309)
            double r7 = r3.Aki(r1)
            r2 = 0
            int r1 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x134e
            r2 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r1 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x134d
            java.util.Random r1 = new java.util.Random
            r1.<init>()
            double r2 = r1.nextDouble()
            int r1 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r1 >= 0) goto L_0x134e
        L_0x134d:
            r5 = 1
        L_0x134e:
            if (r5 == 0) goto L_0x13b2
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 566209834452503(0x202f7000d0617, double:2.79744827540441E-309)
            long r7 = r3.At0(r1)
            r2 = 0
            int r1 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x13af
            r3 = 4
            int r2 = X.AnonymousClass1Y3.A8J
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            java.util.Random r1 = (java.util.Random) r1
            boolean r1 = r1.nextBoolean()
            if (r1 == 0) goto L_0x13af
            r1 = 111(0x6f, float:1.56E-43)
            java.lang.String r8 = X.ECX.$const$string(r1)
        L_0x1380:
            r3 = 3
            int r1 = X.AnonymousClass1Y3.Abm
            X.0UN r2 = r4.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r3, r1, r2)
            android.os.Handler r7 = (android.os.Handler) r7
            X.EW3 r5 = new X.EW3
            r5.<init>(r4, r8)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS
            int r1 = X.AnonymousClass1Y3.AOJ
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r1, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 566209834190357(0x202f700090615, double:2.79744827410924E-309)
            long r1 = r3.At0(r1)
            long r2 = r4.toMillis(r1)
            r1 = 1743567072(0x67ecb8e0, float:2.2357777E24)
            X.AnonymousClass00S.A05(r7, r5, r2, r1)
            goto L_0x000a
        L_0x13af:
            java.lang.String r8 = "SINGLE_PUBLISH_TEST_WITH_NO_DELAY"
            goto L_0x1380
        L_0x13b2:
            r3 = 0
            int r2 = X.AnonymousClass1Y3.B1d
            X.0UN r1 = r4.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.EW2 r7 = (X.EW2) r7
            r8 = 281964602983304(0x1007200000388, double:1.39309023677317E-309)
            r10 = 1126389533245481(0x4007200020029, double:5.56510372211749E-309)
            r13 = 844914556469322(0x300720001004a, double:4.17443256022686E-309)
            java.lang.String r12 = "POLLING"
            X.EW2.A02(r7, r8, r10, r12, r13)
            r14 = 281964603179913(0x1007200030389, double:1.393090237744545E-309)
            r16 = 1126389533442090(0x400720005002a, double:5.565103723088866E-309)
            r19 = 844914556665931(0x300720004004b, double:4.174432561198237E-309)
            java.lang.String r18 = "POLLING"
            r13 = r7
            X.EW2.A02(r13, r14, r16, r18, r19)
            r8 = 281964603376522(0x100720006038a, double:1.393090238715923E-309)
            r10 = 1126389533638699(0x400720008002b, double:5.565103724060243E-309)
            r13 = 844914556862540(0x300720007004c, double:4.174432562169615E-309)
            X.EW2.A02(r7, r8, r10, r12, r13)
            r8 = 281964603573131(0x100720009038b, double:1.3930902396873E-309)
            r10 = 1126389533835308(0x40072000b002c, double:5.56510372503162E-309)
            r13 = 844914557059149(0x30072000a004d, double:4.17443256314099E-309)
            java.lang.String r12 = "EXPLICIT"
            X.EW2.A02(r7, r8, r10, r12, r13)
            r14 = 281964603769740(0x10072000c038c, double:1.39309024065868E-309)
            r16 = 1126389534031917(0x40072000e002d, double:5.565103726003E-309)
            r19 = 844914557255758(0x30072000d004e, double:4.17443256411237E-309)
            java.lang.String r18 = "EXPLICIT"
            r13 = r7
            X.EW2.A02(r13, r14, r16, r18, r19)
            r8 = 281964603966349(0x10072000f038d, double:1.393090241630055E-309)
            r10 = 1126389534228526(0x400720011002e, double:5.565103726974376E-309)
            r13 = 844914557452367(0x300720010004f, double:4.174432565083747E-309)
            X.EW2.A02(r7, r8, r10, r12, r13)
            goto L_0x000a
        L_0x1435:
            int r2 = X.AnonymousClass1Y3.AKm
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.graphql.executor.OfflineMutationsManager r2 = (com.facebook.graphql.executor.OfflineMutationsManager) r2
            X.0h3 r1 = r2.A05
            boolean r1 = r1.A0Q()
            if (r1 == 0) goto L_0x000a
            X.BMQ r2 = r2.A07
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r2.A04(r1)
            goto L_0x000a
        L_0x1450:
            int r2 = X.AnonymousClass1Y3.ASQ
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.funnellogger.FunnelLoggerImpl r3 = (com.facebook.funnellogger.FunnelLoggerImpl) r3
            monitor-enter(r3)
            X.14c r2 = r3.A04     // Catch:{ all -> 0x2092 }
            r1 = 6
            android.os.Message r1 = r2.obtainMessage(r1)     // Catch:{ all -> 0x2092 }
            r2.sendMessage(r1)     // Catch:{ all -> 0x2092 }
            monitor-exit(r3)
            goto L_0x000a
        L_0x1468:
            int r2 = X.AnonymousClass1Y3.ASQ
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.funnellogger.FunnelLoggerImpl r3 = (com.facebook.funnellogger.FunnelLoggerImpl) r3
            monitor-enter(r3)
            boolean r1 = r3.A03     // Catch:{ all -> 0x2095 }
            if (r1 == 0) goto L_0x1482
            X.14c r2 = r3.A04     // Catch:{ all -> 0x2095 }
            r1 = 9
            android.os.Message r1 = r2.obtainMessage(r1)     // Catch:{ all -> 0x2095 }
            r2.sendMessage(r1)     // Catch:{ all -> 0x2095 }
        L_0x1482:
            monitor-exit(r3)
            goto L_0x000a
        L_0x1485:
            int r2 = X.AnonymousClass1Y3.A57
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver r3 = (com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver) r3
            monitor-enter(r3)
            boolean r1 = r3.A02     // Catch:{ all -> 0x2098 }
            if (r1 != 0) goto L_0x1497
            monitor-exit(r3)     // Catch:{ all -> 0x2098 }
            goto L_0x000a
        L_0x1497:
            r3.A02 = r4     // Catch:{ all -> 0x2098 }
            monitor-exit(r3)     // Catch:{ all -> 0x2098 }
            int r2 = X.AnonymousClass1Y3.BJf
            X.0UN r1 = r3.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            X.2X9 r2 = (X.AnonymousClass2X9) r2
            X.2DR r1 = r3.A00
            if (r1 == 0) goto L_0x000a
            r1.A01(r2)
            goto L_0x000a
        L_0x14ad:
            int r2 = X.AnonymousClass1Y3.BTS
            X.0UN r1 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.fling.analytics.FlingProfileLogger r7 = (com.facebook.fling.analytics.FlingProfileLogger) r7
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r7.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 284197986963401(0x1027a000f0fc9, double:1.40412461975855E-309)
            boolean r1 = r3.Aem(r1)
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r7.A00
            r5 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r2 = (com.facebook.prefs.shared.FbSharedPreferences) r2
            X.1Y7 r1 = X.AnonymousClass2IZ.A02
            boolean r1 = r2.Aep(r1, r6)
            if (r1 == 0) goto L_0x000a
            r3 = 3
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r7.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1ZE r2 = (X.AnonymousClass1ZE) r2
            java.lang.String r1 = "android_fling_profile"
            X.0bW r2 = r2.A01(r1)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r3 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r1 = 10
            r3.<init>(r2, r1)
            boolean r1 = r3.A0G()
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.BLP
            X.0UN r1 = r7.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.8mQ r1 = (X.C187068mQ) r1
            java.lang.String r2 = r1.A04()
            java.lang.String r1 = "fling_profile"
            r3.A0D(r1, r2)
            r3.A06()
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r7.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1
            X.1hn r2 = r1.edit()
            X.1Y7 r1 = X.AnonymousClass2IZ.A02
            r2.putBoolean(r1, r4)
            r2.commit()
            goto L_0x000a
        L_0x152b:
            int r2 = X.AnonymousClass1Y3.A0Y
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.entitypresence.EntityPresenceManager r4 = (com.facebook.entitypresence.EntityPresenceManager) r4
            int r3 = X.AnonymousClass1Y3.APr
            X.0UN r2 = r4.A01
            r1 = 4
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Y6 r1 = (X.AnonymousClass1Y6) r1
            r1.AOx()
            java.util.Set r1 = r4.A03
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x000a
            java.util.Set r1 = r4.A03
            java.util.Iterator r7 = r1.iterator()
        L_0x1551:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L_0x1571
            java.lang.Object r5 = r7.next()
            X.8NY r5 = (X.AnonymousClass8NY) r5
            java.lang.Integer r1 = r5.A07
            int r3 = r1.intValue()
            r2 = 4
            r1 = 1
            if (r3 == r2) goto L_0x1568
            r1 = 0
        L_0x1568:
            if (r1 == 0) goto L_0x1551
            r2 = 0
            java.lang.String r1 = "app_background"
            com.facebook.entitypresence.EntityPresenceManager.A07(r4, r5, r2, r1)
            goto L_0x1551
        L_0x1571:
            int r2 = X.AnonymousClass1Y3.A9n
            X.0UN r1 = r4.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.0ia r2 = (X.AnonymousClass0ia) r2
            java.lang.Runnable r1 = r4.A02
            r2.A03(r1)
            r1 = -1
            r4.A00 = r1
            goto L_0x000a
        L_0x1586:
            int r2 = X.AnonymousClass1Y3.A0Y
            X.0UN r1 = r0.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.entitypresence.EntityPresenceManager r7 = (com.facebook.entitypresence.EntityPresenceManager) r7
            int r3 = X.AnonymousClass1Y3.APr
            X.0UN r2 = r7.A01
            r1 = 4
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Y6 r1 = (X.AnonymousClass1Y6) r1
            r1.AOx()
            java.util.Set r1 = r7.A03
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x000a
            java.util.Set r1 = r7.A03
            java.util.Iterator r2 = r1.iterator()
        L_0x15ac:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x15b6
            r2.next()
            goto L_0x15ac
        L_0x15b6:
            long r4 = r7.A00
            r2 = -1
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.A9n
            X.0UN r1 = r7.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.0ia r2 = (X.AnonymousClass0ia) r2
            java.lang.Runnable r1 = r7.A02
            r2.A02(r1)
            goto L_0x000a
        L_0x15cf:
            int r2 = X.AnonymousClass1Y3.AQ3
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.diskfootprint.cleaner.FileCleaner r4 = (com.facebook.diskfootprint.cleaner.FileCleaner) r4
            monitor-enter(r4)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x209b }
            com.facebook.diskfootprint.cleaner.FileCleaner.A08 = r1     // Catch:{ all -> 0x209b }
            r3 = 0
            int r2 = X.AnonymousClass1Y3.BFB     // Catch:{ all -> 0x209b }
            X.0UN r1 = r4.A01     // Catch:{ all -> 0x209b }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x209b }
            com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor r1 = (com.facebook.storage.monitor.fbapps.FBAppsStorageResourceMonitor) r1     // Catch:{ all -> 0x209b }
            r1.A01(r4)     // Catch:{ all -> 0x209b }
            monitor-exit(r4)
            goto L_0x000a
        L_0x15f1:
            int r2 = X.AnonymousClass1Y3.AZM
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.device_id.UniqueFamilyDeviceIdBroadcastSender r5 = (com.facebook.device_id.UniqueFamilyDeviceIdBroadcastSender) r5
            int r2 = X.AnonymousClass1Y3.AqT
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.0bB r1 = (X.C06240bB) r1
            boolean r1 = r1.A0A()
            r8 = 0
            if (r1 != 0) goto L_0x161b
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.0bB r1 = (X.C06240bB) r1
            boolean r2 = r1.A0B()
            r1 = 0
            if (r2 == 0) goto L_0x161c
        L_0x161b:
            r1 = 1
        L_0x161c:
            if (r1 == 0) goto L_0x000a
            X.1Yd r4 = r5.A02
            r1 = 563289255969111(0x2004f00020157, double:2.78301870045814E-309)
            r3 = 604800(0x93a80, float:8.47505E-40)
            int r11 = r4.AqL(r1, r3)
            com.facebook.prefs.shared.FbSharedPreferences r4 = r5.A03
            X.1Y7 r3 = X.C09380hD.A02
            r1 = 0
            long r2 = r4.At2(r3, r1)
            X.06B r1 = r5.A01
            long r9 = r1.now()
            long r9 = r9 - r2
            long r3 = (long) r11
            r1 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r1
            int r2 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            r1 = 0
            if (r2 <= 0) goto L_0x1647
            r1 = 1
        L_0x1647:
            if (r1 == 0) goto L_0x000a
            int r2 = X.AnonymousClass1Y3.Ate
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r2, r1)
            X.996 r1 = (X.AnonymousClass996) r1
            r1.A00()
            int r2 = X.AnonymousClass1Y3.AqT
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.0bB r1 = (X.C06240bB) r1
            boolean r1 = r1.A0A()
            if (r1 == 0) goto L_0x167a
            com.facebook.prefs.shared.FbSharedPreferences r1 = r5.A03
            X.1hn r4 = r1.edit()
            X.1Y7 r3 = X.C09380hD.A02
            X.06B r1 = r5.A01
            long r1 = r1.now()
            r4.BzA(r3, r1)
            r4.commit()
        L_0x167a:
            int r2 = X.AnonymousClass1Y3.AqT
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.0bB r1 = (X.C06240bB) r1
            boolean r1 = r1.A0B()
            if (r1 == 0) goto L_0x000a
            com.facebook.prefs.shared.FbSharedPreferences r1 = r5.A03
            X.1hn r4 = r1.edit()
            X.1Y7 r3 = X.C09380hD.A07
            X.06B r1 = r5.A01
            long r1 = r1.now()
            r4.BzA(r3, r1)
            r4.commit()
            goto L_0x000a
        L_0x16a0:
            int r2 = X.AnonymousClass1Y3.ArC
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.device.resourcemonitor.activemonitoring.ResourceManagerActiveMonitoring r1 = (com.facebook.device.resourcemonitor.activemonitoring.ResourceManagerActiveMonitoring) r1
            int r3 = X.AnonymousClass1Y3.AQe
            X.0UN r2 = r1.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            com.facebook.device.resourcemonitor.ResourceManager r1 = (com.facebook.device.resourcemonitor.ResourceManager) r1
            r1.A01()
            goto L_0x000a
        L_0x16b9:
            int r2 = X.AnonymousClass1Y3.BCk
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.device.resourcemonitor.ResourceMonitor r1 = (com.facebook.device.resourcemonitor.ResourceMonitor) r1
            r1.A02 = r4
            com.facebook.device.resourcemonitor.ResourceMonitor.A02(r1)
            goto L_0x000a
        L_0x16ca:
            int r2 = X.AnonymousClass1Y3.BCk
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.device.resourcemonitor.ResourceMonitor r1 = (com.facebook.device.resourcemonitor.ResourceMonitor) r1
            r1.A02 = r6
            com.facebook.device.resourcemonitor.ResourceMonitor.A01(r1)
            goto L_0x000a
        L_0x16db:
            int r2 = X.AnonymousClass1Y3.AQe
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.device.resourcemonitor.ResourceManager r1 = (com.facebook.device.resourcemonitor.ResourceManager) r1
            r1.A02()
            goto L_0x000a
        L_0x16ea:
            int r2 = X.AnonymousClass1Y3.Adh
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.config.background.impl.ConfigurationConditionalWorkerInfo r1 = (com.facebook.config.background.impl.ConfigurationConditionalWorkerInfo) r1
            java.util.concurrent.atomic.AtomicInteger r2 = r1.A01
            r2.set(r6)
            goto L_0x000a
        L_0x16fb:
            int r2 = X.AnonymousClass1Y3.A3U
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.conditionalworker.ConditionalWorkerManager r3 = (com.facebook.conditionalworker.ConditionalWorkerManager) r3
            java.lang.String r1 = "USER_LEFT_APP"
            java.lang.String r2 = "on_app_background"
            goto L_0x1718
        L_0x170a:
            int r2 = X.AnonymousClass1Y3.A3U
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.conditionalworker.ConditionalWorkerManager r3 = (com.facebook.conditionalworker.ConditionalWorkerManager) r3
            java.lang.String r1 = "USER_ENTERED_APP"
            java.lang.String r2 = "on_app_foreground"
        L_0x1718:
            boolean r1 = com.facebook.conditionalworker.ConditionalWorkerManager.A01(r3, r1)
            if (r1 == 0) goto L_0x000a
            r3.A03(r2)
            goto L_0x000a
        L_0x1723:
            int r2 = X.AnonymousClass1Y3.B2h
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.compactdiskmodule.CompactDiskFlushDispatcher r4 = (com.facebook.compactdiskmodule.CompactDiskFlushDispatcher) r4
            boolean r1 = r4.A01
            if (r1 == 0) goto L_0x000a
            r3 = 0
            int r2 = X.AnonymousClass1Y3.Ay7     // Catch:{ RuntimeException -> 0x000a }
            X.0UN r1 = r4.A00     // Catch:{ RuntimeException -> 0x000a }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ RuntimeException -> 0x000a }
            com.facebook.compactdisk.current.CompactDiskManager r1 = (com.facebook.compactdisk.current.CompactDiskManager) r1     // Catch:{ RuntimeException -> 0x000a }
            r1.flush()     // Catch:{ RuntimeException -> 0x000a }
            goto L_0x000a
        L_0x1741:
            int r2 = X.AnonymousClass1Y3.BIw
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.userinteraction.UserInteractionHistory r1 = (com.facebook.common.userinteraction.UserInteractionHistory) r1
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r5 = r1.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r5)
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1
            X.1hn r4 = r1.edit()
            X.1Y7 r3 = com.facebook.common.userinteraction.UserInteractionHistory.A01
            int r2 = X.AnonymousClass1Y3.AYB
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r5)
            X.06A r1 = (X.AnonymousClass06A) r1
            long r1 = r1.now()
            r4.BzA(r3, r1)
            r4.commit()
            goto L_0x000a
        L_0x176f:
            int r2 = X.AnonymousClass1Y3.AM4
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.noncriticalinit.NonCriticalInitializer r1 = (com.facebook.common.noncriticalinit.NonCriticalInitializer) r1
            monitor-enter(r1)
            r1.A01 = r6     // Catch:{ all -> 0x209e }
            com.facebook.common.noncriticalinit.NonCriticalInitializer.A02(r1)     // Catch:{ all -> 0x209e }
            monitor-exit(r1)
            goto L_0x000a
        L_0x1782:
            int r2 = X.AnonymousClass1Y3.Adl
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.netchecker.NetChecker r3 = (com.facebook.common.netchecker.NetChecker) r3
            X.2rX r2 = r3.A0B
            X.2rX r1 = X.C57162rX.CAPTIVE_PORTAL
            if (r2 != r1) goto L_0x000a
            java.util.concurrent.Future r2 = r3.A0C
            com.google.common.util.concurrent.ListenableFuture r1 = com.facebook.common.netchecker.NetChecker.A0D
            if (r2 != r1) goto L_0x000a
            r1 = 10000(0x2710, float:1.4013E-41)
            r3.A04(r1)
            goto L_0x000a
        L_0x179f:
            int r2 = X.AnonymousClass1Y3.Av4
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.memory.LargeHeapOverrideConfig r3 = (com.facebook.common.memory.LargeHeapOverrideConfig) r3
            X.00z r2 = r3.A00
            X.00z r1 = X.C001500z.A03
            if (r2 == r1) goto L_0x000a
            com.facebook.common.memory.LargeHeapOverrideConfig.A01(r3)
            goto L_0x000a
        L_0x17b4:
            int r2 = X.AnonymousClass1Y3.A7u
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.memory.manager.MemoryManager r1 = (com.facebook.common.memory.manager.MemoryManager) r1
            com.facebook.common.memory.manager.MemoryManager.A02(r1)
            goto L_0x000a
        L_0x17c3:
            int r2 = X.AnonymousClass1Y3.AVh
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.memory.leaklistener.MemoryLeakListener r2 = (com.facebook.common.memory.leaklistener.MemoryLeakListener) r2
            monitor-enter(r2)
            boolean r1 = com.facebook.common.memory.leaklistener.MemoryLeakListener.A01(r2)     // Catch:{ all -> 0x20a1 }
            if (r1 == 0) goto L_0x17d9
            X.2SE r1 = r2.A00     // Catch:{ all -> 0x20a1 }
            r1.A03()     // Catch:{ all -> 0x20a1 }
        L_0x17d9:
            monitor-exit(r2)
            goto L_0x000a
        L_0x17dc:
            int r2 = X.AnonymousClass1Y3.AVh
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.memory.leaklistener.MemoryLeakListener r2 = (com.facebook.common.memory.leaklistener.MemoryLeakListener) r2
            monitor-enter(r2)
            boolean r1 = com.facebook.common.memory.leaklistener.MemoryLeakListener.A01(r2)     // Catch:{ all -> 0x20a4 }
            if (r1 == 0) goto L_0x17f2
            X.2SE r1 = r2.A00     // Catch:{ all -> 0x20a4 }
            r1.A02()     // Catch:{ all -> 0x20a4 }
        L_0x17f2:
            monitor-exit(r2)
            goto L_0x000a
        L_0x17f5:
            int r2 = X.AnonymousClass1Y3.AsG
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration r1 = (com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration) r1
            int r3 = X.AnonymousClass1Y3.BTG
            X.0UN r2 = r1.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.8B1 r2 = (X.AnonymousClass8B1) r2
            r1 = 0
            r2.A01 = r1
            goto L_0x000a
        L_0x180e:
            int r2 = X.AnonymousClass1Y3.AsG
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration r5 = (com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration) r5
            int r2 = X.AnonymousClass1Y3.B4l
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            X.1pk r1 = (X.C34521pk) r1
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 286405600353042(0x1047c00121b12, double:1.415031679109756E-309)
            boolean r1 = r3.Aem(r1)
            if (r1 == 0) goto L_0x187d
            int r3 = X.AnonymousClass1Y3.BTG
            X.0UN r2 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.8B1 r1 = (X.AnonymousClass8B1) r1
            X.2U1 r1 = r1.A02()
            java.lang.Integer r2 = X.AnonymousClass8B1.A01(r1)
            X.3w5 r4 = new X.3w5
            r4.<init>()
            com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000 r3 = new com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000
            r1 = 240(0xf0, float:3.36E-43)
            r3.<init>(r1)
            java.lang.String r2 = com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration.A01(r2)
            java.lang.String r1 = "font_capability"
            r3.A09(r1, r2)
            java.lang.String r1 = "input"
            r4.A04(r1, r3)
            int r2 = X.AnonymousClass1Y3.ASp
            X.0UN r1 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            X.0mD r2 = (X.C11170mD) r2
            X.AkD r1 = X.C26931cN.A01(r4)
            com.google.common.util.concurrent.ListenableFuture r2 = r2.A05(r1)
            X.2If r1 = new X.2If
            r1.<init>()
            X.C05350Yp.A07(r2, r1)
        L_0x187d:
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            X.1ZE r2 = (X.AnonymousClass1ZE) r2
            java.lang.String r1 = "myanmar_zawgyi_detection_v2"
            X.0bW r2 = r2.A01(r1)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r3 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r1 = 461(0x1cd, float:6.46E-43)
            r3.<init>(r2, r1)
            boolean r1 = r3.A0G()
            if (r1 == 0) goto L_0x000a
            int r4 = X.AnonymousClass1Y3.BTG
            X.0UN r2 = r5.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.8B1 r1 = (X.AnonymousClass8B1) r1
            X.2U1 r4 = r1.A02()
            int r1 = r4.A01
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.String r1 = "combined_letters_width"
            r3.A0B(r1, r2)
            java.lang.Integer r1 = X.AnonymousClass8B1.A01(r4)
            java.lang.String r2 = com.facebook.common.i18n.zawgyi.ZawgyiFontDetectorIntegration.A01(r1)
            java.lang.String r1 = "detected_burmese_font_support"
            r3.A0D(r1, r2)
            int r1 = r4.A03
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.String r1 = "sdk_version"
            r3.A0B(r1, r2)
            int r1 = r4.A02
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.String r1 = "single_letter_width"
            r3.A0B(r1, r2)
            int r1 = r4.A00
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.String r1 = "aforementioned_width"
            r3.A0B(r1, r2)
            r3.A06()
            goto L_0x000a
        L_0x18e7:
            int r2 = X.AnonymousClass1Y3.ApW
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.errorreporting.memory.LeakMemoryDumper r1 = (com.facebook.common.errorreporting.memory.LeakMemoryDumper) r1
            r1.A01()
            goto L_0x000a
        L_0x18f6:
            int r2 = X.AnonymousClass1Y3.B3e
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.connectionstatus.FbDataConnectionManager r4 = (com.facebook.common.connectionstatus.FbDataConnectionManager) r4
            int r2 = X.AnonymousClass1Y3.ADt
            X.0UN r3 = r4.A00
            r1 = 2
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r2, r3)
            java.util.concurrent.ScheduledExecutorService r8 = (java.util.concurrent.ScheduledExecutorService) r8
            X.1Vx r7 = r4.A01
            int r2 = X.AnonymousClass1Y3.AOJ
            r1 = 6
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.1Yd r5 = (X.C25051Yd) r5
            r3 = 563276371788115(0x2004c000d0153, double:2.78295504414609E-309)
            r1 = 15000(0x3a98, double:7.411E-320)
            long r2 = r5.At1(r3, r1)
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.MILLISECONDS
            r8.schedule(r7, r2, r1)
            goto L_0x000a
        L_0x1928:
            int r2 = X.AnonymousClass1Y3.AQV
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.combinedthreadpool.asyncinit.CombinedThreadPoolLoggerAppStateListener r1 = (com.facebook.common.combinedthreadpool.asyncinit.CombinedThreadPoolLoggerAppStateListener) r1
            X.1uW r2 = r1.A00
            if (r2 == 0) goto L_0x000a
            java.lang.Boolean r1 = java.lang.Boolean.TRUE
            r2.A01(r1)
            goto L_0x000a
        L_0x193d:
            int r2 = X.AnonymousClass1Y3.AQV
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.combinedthreadpool.asyncinit.CombinedThreadPoolLoggerAppStateListener r1 = (com.facebook.common.combinedthreadpool.asyncinit.CombinedThreadPoolLoggerAppStateListener) r1
            X.1uW r2 = r1.A00
            if (r2 == 0) goto L_0x000a
            java.lang.Boolean r1 = java.lang.Boolean.FALSE
            r2.A01(r1)
            goto L_0x000a
        L_0x1952:
            int r2 = X.AnonymousClass1Y3.Aa2
            X.0UN r1 = r0.A00
            java.lang.Object r9 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.activitycleaner.ActivityStackResetter r9 = (com.facebook.common.activitycleaner.ActivityStackResetter) r9
            r8 = 0
            if (r8 == 0) goto L_0x000a
            X.1Yd r3 = r9.A01
            r1 = 565707322951055(0x202820008058f, double:2.794965538709356E-309)
            long r3 = r3.At0(r1)
            com.facebook.common.activitycleaner.ActivityStackResetter.A01(r9)
            java.util.concurrent.atomic.AtomicReference r7 = r9.A03
            X.0VK r5 = r9.A02
            X.9KM r2 = new X.9KM
            r2.<init>(r9, r8)
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.MINUTES
            X.0Y0 r2 = r5.C4Y(r2, r3, r1)
            r7.compareAndSet(r8, r2)
            goto L_0x000a
        L_0x1981:
            int r2 = X.AnonymousClass1Y3.Aa2
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.activitycleaner.ActivityStackResetter r1 = (com.facebook.common.activitycleaner.ActivityStackResetter) r1
            com.facebook.common.activitycleaner.ActivityStackResetter.A01(r1)
            goto L_0x000a
        L_0x1990:
            int r2 = X.AnonymousClass1Y3.A1C
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.activitycleaner.ActivityStackManager r1 = (com.facebook.common.activitycleaner.ActivityStackManager) r1
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r5 = r1.A02
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r5)
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1
            X.1hn r4 = r1.edit()
            X.1Y7 r3 = com.facebook.common.activitycleaner.ActivityStackManager.A06
            int r2 = X.AnonymousClass1Y3.B5j
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r2, r5)
            X.0Ud r1 = (X.AnonymousClass0Ud) r1
            long r1 = r1.A0O
            r4.BzA(r3, r1)
            r4.commit()
            goto L_0x000a
        L_0x19bd:
            int r2 = X.AnonymousClass1Y3.A1C
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.common.activitycleaner.ActivityStackManager r3 = (com.facebook.common.activitycleaner.ActivityStackManager) r3
            X.1LM r2 = r5.A00
            X.BfV r1 = new X.BfV
            r1.<init>(r3)
            r2.A00(r1)
            goto L_0x000a
        L_0x19d3:
            int r2 = X.AnonymousClass1Y3.BMy
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.battery.samsung.SamsungWarningNotificationLogger r1 = (com.facebook.battery.samsung.SamsungWarningNotificationLogger) r1
            java.lang.Object r2 = r1.A02
            monitor-enter(r2)
            X.0Gc r1 = r1.A00     // Catch:{ all -> 0x20a7 }
            if (r1 == 0) goto L_0x19e7
            r1.A01()     // Catch:{ all -> 0x20a7 }
        L_0x19e7:
            monitor-exit(r2)     // Catch:{ all -> 0x20a7 }
            goto L_0x000a
        L_0x19ea:
            int r2 = X.AnonymousClass1Y3.AzB
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.battery.instrumentation.BatteryMetricsReporter r4 = (com.facebook.battery.instrumentation.BatteryMetricsReporter) r4
            boolean r1 = r4.A0F
            if (r1 == 0) goto L_0x000a
            java.util.concurrent.atomic.AtomicBoolean r1 = r4.A0D
            boolean r1 = r1.get()
            if (r1 == 0) goto L_0x000a
            java.lang.String r3 = "foreground"
            r2 = 0
            java.lang.String r1 = com.facebook.battery.instrumentation.BatteryMetricsReporter.A01(r4)
            com.facebook.battery.instrumentation.BatteryMetricsReporter.A03(r4, r3, r1, r2)
            goto L_0x000a
        L_0x1a0c:
            int r2 = X.AnonymousClass1Y3.AzB
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.battery.instrumentation.BatteryMetricsReporter r4 = (com.facebook.battery.instrumentation.BatteryMetricsReporter) r4
            boolean r1 = r4.A0F
            if (r1 == 0) goto L_0x000a
            java.util.concurrent.atomic.AtomicBoolean r1 = r4.A0D
            boolean r1 = r1.get()
            if (r1 == 0) goto L_0x000a
            java.lang.String r3 = "background"
            r2 = 0
            java.lang.String r1 = com.facebook.battery.instrumentation.BatteryMetricsReporter.A01(r4)
            com.facebook.battery.instrumentation.BatteryMetricsReporter.A03(r4, r3, r1, r2)
            goto L_0x000a
        L_0x1a2e:
            int r2 = X.AnonymousClass1Y3.ASn
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.battery.cpuspin.di.FbCpuSpinScheduler r3 = (com.facebook.battery.cpuspin.di.FbCpuSpinScheduler) r3
            boolean r1 = com.facebook.battery.cpuspin.di.FbCpuSpinScheduler.A04
            if (r1 == 0) goto L_0x000a
            X.8oD r2 = r3.A03
            X.8oA r1 = r3.A01
            r2.A00(r1)
            goto L_0x000a
        L_0x1a45:
            int r2 = X.AnonymousClass1Y3.ASn
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.battery.cpuspin.di.FbCpuSpinScheduler r3 = (com.facebook.battery.cpuspin.di.FbCpuSpinScheduler) r3
            boolean r1 = com.facebook.battery.cpuspin.di.FbCpuSpinScheduler.A04
            if (r1 == 0) goto L_0x000a
            X.8oD r2 = r3.A03
            X.8oA r1 = r3.A02
            r2.A00(r1)
            goto L_0x000a
        L_0x1a5c:
            int r2 = X.AnonymousClass1Y3.ASn
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.battery.cpuspin.di.FbCpuSpinScheduler r4 = (com.facebook.battery.cpuspin.di.FbCpuSpinScheduler) r4
            boolean r1 = com.facebook.battery.cpuspin.di.FbCpuSpinScheduler.A04
            if (r1 == 0) goto L_0x000a
            r3 = 0
            int r2 = X.AnonymousClass1Y3.B5j
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0Ud r1 = (X.AnonymousClass0Ud) r1
            boolean r1 = r1.A0G()
            if (r1 == 0) goto L_0x000a
            X.8oD r2 = r4.A03
            X.8oA r1 = r4.A01
            r2.A00(r1)
            goto L_0x000a
        L_0x1a84:
            int r2 = X.AnonymousClass1Y3.A0q
            X.0UN r1 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.attribution.LatStatusJob r4 = (com.facebook.attribution.LatStatusJob) r4
            int r2 = X.AnonymousClass1Y3.AcD
            X.0UN r1 = r4.A00
            r5 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r1)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 543(0x21f, float:7.61E-43)
            r3 = 0
            boolean r1 = r2.AbO(r1, r3)
            if (r1 != 0) goto L_0x1ab4
            int r2 = X.AnonymousClass1Y3.AcD
            X.0UN r1 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r1)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 208(0xd0, float:2.91E-43)
            boolean r1 = r2.AbO(r1, r3)
            if (r1 == 0) goto L_0x1ab5
        L_0x1ab4:
            r3 = 1
        L_0x1ab5:
            if (r3 == 0) goto L_0x000a
            r3 = 0
            int r2 = X.AnonymousClass1Y3.AXA
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0XN r1 = (X.AnonymousClass0XN) r1
            com.facebook.auth.viewercontext.ViewerContext r1 = r1.A08()
            if (r1 == 0) goto L_0x000a
            java.lang.String r1 = r1.mUserId     // Catch:{  }
            long r34 = java.lang.Long.parseLong(r1)     // Catch:{  }
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r1 = 564981472953503(0x201d90000049f, double:2.79137936323111E-309)
            int r22 = r3.AqL(r1, r6)
            r1 = r22
            if (r1 >= r6) goto L_0x1ae7
            r22 = 1
        L_0x1ae7:
            r2 = 2
            int r1 = X.AnonymousClass1Y3.Ayq
            X.0UN r3 = r4.A00
            java.lang.Object r21 = X.AnonymousClass1XX.A02(r2, r1, r3)
            r1 = r21
            X.9yY r1 = (X.C211149yY) r1
            r21 = r1
            r2 = 5
            int r1 = X.AnonymousClass1Y3.A6S
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r3)
            android.content.Context r1 = (android.content.Context) r1
            android.content.Context r20 = r1.getApplicationContext()
            X.9yZ r19 = new X.9yZ
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AgK
            X.0UN r3 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r3)
            X.06B r2 = (X.AnonymousClass06B) r2
            r1 = r19
            r1.<init>(r2)
            int r2 = X.AnonymousClass1Y3.AcD
            r1 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.1YI r3 = (X.AnonymousClass1YI) r3
            r2 = 553(0x229, float:7.75E-43)
            r1 = 0
            boolean r1 = r3.AbO(r2, r1)
            if (r1 != 0) goto L_0x1beb
            r3 = 6
            int r2 = X.AnonymousClass1Y3.BH4
            X.0UN r1 = r4.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.00z r1 = (X.C001500z) r1
            int r1 = r1.ordinal()
            int r1 = 1 - r1
            if (r1 != 0) goto L_0x1beb
            r18 = 1
        L_0x1b3c:
            r2 = r22
            r1 = r21
            r1.A00 = r2
            int r3 = X.AnonymousClass1Y3.B6q
            X.0UN r2 = r1.A01
            r1 = 6
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r3, r2)
            com.facebook.prefs.shared.FbSharedPreferences r5 = (com.facebook.prefs.shared.FbSharedPreferences) r5
            java.lang.String r1 = "AttributionId"
            X.1Y7 r1 = com.facebook.attribution.AttributionStateSerializer.A01(r1)
            r3 = 0
            java.lang.String r8 = r5.B4F(r1, r3)
            java.lang.String r1 = "UserId"
            X.1Y7 r4 = com.facebook.attribution.AttributionStateSerializer.A01(r1)
            r1 = -1
            long r9 = r5.At2(r4, r1)
            java.lang.String r4 = "Timestamp"
            X.1Y7 r4 = com.facebook.attribution.AttributionStateSerializer.A01(r4)
            long r11 = r5.At2(r4, r1)
            java.lang.String r4 = "ExposeAndroidId"
            X.1Y7 r7 = com.facebook.attribution.AttributionStateSerializer.A01(r4)
            r4 = 0
            boolean r13 = r5.Aep(r7, r4)
            java.lang.String r4 = "PreviousAdvertisingId"
            X.1Y7 r4 = com.facebook.attribution.AttributionStateSerializer.A01(r4)
            java.lang.String r14 = r5.B4F(r4, r3)
            java.lang.String r4 = "IsTrackingEnabled"
            X.1Y7 r4 = com.facebook.attribution.AttributionStateSerializer.A01(r4)
            com.facebook.common.util.TriState r4 = r5.Aeq(r4)
            java.lang.Boolean r15 = r4.asBooleanObject()
            boolean r4 = com.google.common.base.Platform.stringIsNullOrEmpty(r8)
            if (r4 != 0) goto L_0x1ba5
            int r4 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x1ba5
            int r4 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x1ba5
            com.facebook.attribution.AttributionState r3 = new com.facebook.attribution.AttributionState
            r7 = r3
            r7.<init>(r8, r9, r11, r13, r14, r15)
        L_0x1ba5:
            int r4 = X.AnonymousClass1Y3.BEf
            r1 = r21
            X.0UN r2 = r1.A01
            r1 = 7
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.1ac r1 = (X.C25921ac) r1
            java.lang.String r32 = r1.B7Z()
            java.lang.String r17 = ""
            if (r32 != 0) goto L_0x1bbc
            r32 = r17
        L_0x1bbc:
            r4 = 5
            int r2 = X.AnonymousClass1Y3.AlF
            r1 = r21
            X.0UN r1 = r1.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.0bB r1 = (X.C06240bB) r1
            java.lang.String r33 = r1.A05()
            if (r33 != 0) goto L_0x1bd1
            r33 = r17
        L_0x1bd1:
            if (r3 == 0) goto L_0x1bd9
            java.lang.String r1 = r3.A04
            if (r1 == 0) goto L_0x1bd9
            r17 = r1
        L_0x1bd9:
            if (r3 == 0) goto L_0x1be9
            java.lang.Boolean r14 = r3.A02
        L_0x1bdd:
            com.google.android.gms.common.GoogleApiAvailability r2 = com.google.android.gms.common.GoogleApiAvailability.A00
            r1 = r20
            int r11 = r2.isGooglePlayServicesAvailable(r1)
            r10 = 0
            if (r11 != 0) goto L_0x1bfd
            goto L_0x1bef
        L_0x1be9:
            r14 = 0
            goto L_0x1bdd
        L_0x1beb:
            r18 = 0
            goto L_0x1b3c
        L_0x1bef:
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r10 = com.google.android.gms.ads.identifier.AdvertisingIdClient.A00(r20)     // Catch:{ 1w0 | 1w1 | IOException -> 0x1bf4 }
            goto L_0x1bfd
        L_0x1bf4:
            r5 = move-exception
            r4 = 3
            java.lang.String r2 = "Failure while using Google Mobile Service sdk to read advertising id."
            r1 = r21
            X.C211149yY.A01(r1, r4, r2, r5)
        L_0x1bfd:
            r9 = 0
            if (r10 != 0) goto L_0x1cdf
            X.8sw r8 = new X.8sw     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
            r8.<init>()     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
            android.content.Intent r4 = new android.content.Intent     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
            java.lang.String r1 = "com.google.android.gms.ads.identifier.service.START"
            r4.<init>(r1)     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
            java.lang.String r1 = "com.google.android.gms"
            r4.setPackage(r1)     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
            r2 = -1705872455(0xffffffff9a5273b9, float:-4.3520462E-23)
            r1 = r20
            boolean r1 = X.C006406k.A02(r1, r4, r8, r6, r2)     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
            if (r1 == 0) goto L_0x1cdf
            com.facebook.attribution.GoogleAdInfo r2 = new com.facebook.attribution.GoogleAdInfo     // Catch:{ all -> 0x1cc9 }
            java.util.concurrent.atomic.AtomicBoolean r4 = r8.A01     // Catch:{ all -> 0x1cc9 }
            boolean r1 = r4.compareAndSet(r6, r6)     // Catch:{ all -> 0x1cc9 }
            if (r1 != 0) goto L_0x1cb3
            java.util.concurrent.BlockingQueue r1 = r8.A00     // Catch:{ all -> 0x1cc9 }
            java.lang.Object r1 = r1.take()     // Catch:{ all -> 0x1cc9 }
            android.os.IBinder r1 = (android.os.IBinder) r1     // Catch:{ all -> 0x1cc9 }
            r2.<init>(r1)     // Catch:{ all -> 0x1cc9 }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r15 = new com.google.android.gms.ads.identifier.AdvertisingIdClient$Info     // Catch:{ all -> 0x1cc9 }
            r1 = 806437267(0x30114193, float:5.284388E-10)
            int r7 = X.C000700l.A03(r1)     // Catch:{ all -> 0x1cc9 }
            android.os.Parcel r5 = android.os.Parcel.obtain()     // Catch:{ all -> 0x1cc9 }
            android.os.Parcel r13 = android.os.Parcel.obtain()     // Catch:{ all -> 0x1cc9 }
            java.lang.String r1 = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService"
            r5.writeInterfaceToken(r1)     // Catch:{ all -> 0x1ca5 }
            android.os.IBinder r12 = r2.A00     // Catch:{ all -> 0x1ca5 }
            r4 = 1
            r12.transact(r6, r5, r13, r9)     // Catch:{ all -> 0x1ca5 }
            r13.readException()     // Catch:{ all -> 0x1ca5 }
            java.lang.String r16 = r13.readString()     // Catch:{ all -> 0x1ca5 }
            r13.recycle()     // Catch:{ all -> 0x1cc9 }
            r5.recycle()     // Catch:{ all -> 0x1cc9 }
            r1 = -1704508678(0xffffffff9a6742fa, float:-4.7823784E-23)
            X.C000700l.A09(r1, r7)     // Catch:{ all -> 0x1cc9 }
            r1 = 231212385(0xdc80561, float:1.2327247E-30)
            int r7 = X.C000700l.A03(r1)     // Catch:{ all -> 0x1cc9 }
            android.os.Parcel r5 = android.os.Parcel.obtain()     // Catch:{ all -> 0x1cc9 }
            android.os.Parcel r13 = android.os.Parcel.obtain()     // Catch:{ all -> 0x1cc9 }
            java.lang.String r1 = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService"
            r5.writeInterfaceToken(r1)     // Catch:{ all -> 0x1cbb }
            r5.writeInt(r6)     // Catch:{ all -> 0x1cbb }
            android.os.IBinder r12 = r2.A00     // Catch:{ all -> 0x1cbb }
            r2 = 2
            r12.transact(r2, r5, r13, r9)     // Catch:{ all -> 0x1cbb }
            r13.readException()     // Catch:{ all -> 0x1cbb }
            int r1 = r13.readInt()     // Catch:{ all -> 0x1cbb }
            if (r1 != 0) goto L_0x1c87
            r4 = 0
        L_0x1c87:
            r13.recycle()     // Catch:{ all -> 0x1cc9 }
            r5.recycle()     // Catch:{ all -> 0x1cc9 }
            r1 = 579455439(0x2289c9cf, float:3.7347598E-18)
            X.C000700l.A09(r1, r7)     // Catch:{ all -> 0x1cc9 }
            r1 = r16
            r15.<init>(r1, r4)     // Catch:{ all -> 0x1cc9 }
            r2 = -1039725912(0xffffffffc2070aa8, float:-33.760406)
            r1 = r20
            X.C006406k.A01(r1, r8, r2)     // Catch:{ RemoteException | InterruptedException -> 0x1ca1 }
            goto L_0x1cdd
        L_0x1ca1:
            r5 = move-exception
            r10 = r15
            r9 = 1
            goto L_0x1cd4
        L_0x1ca5:
            r2 = move-exception
            r13.recycle()     // Catch:{ all -> 0x1cc9 }
            r5.recycle()     // Catch:{ all -> 0x1cc9 }
            r1 = -1810497861(0xffffffff9415febb, float:-7.572814E-27)
            X.C000700l.A09(r1, r7)     // Catch:{ all -> 0x1cc9 }
            goto L_0x1cc8
        L_0x1cb3:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x1cc9 }
            java.lang.String r1 = "Binder already consumed"
            r2.<init>(r1)     // Catch:{ all -> 0x1cc9 }
            goto L_0x1cc8
        L_0x1cbb:
            r2 = move-exception
            r13.recycle()     // Catch:{ all -> 0x1cc9 }
            r5.recycle()     // Catch:{ all -> 0x1cc9 }
            r1 = -1510310342(0xffffffffa5fa7e3a, float:-4.3453621E-16)
            X.C000700l.A09(r1, r7)     // Catch:{ all -> 0x1cc9 }
        L_0x1cc8:
            throw r2     // Catch:{ all -> 0x1cc9 }
        L_0x1cc9:
            r4 = move-exception
            r2 = -1812300829(0xffffffff93fa7be3, float:-6.3231034E-27)
            r1 = r20
            X.C006406k.A01(r1, r8, r2)     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
            throw r4     // Catch:{ RemoteException | InterruptedException -> 0x1cd3 }
        L_0x1cd3:
            r5 = move-exception
        L_0x1cd4:
            r4 = 4
            java.lang.String r2 = "Failure acquiring Google Mobile Service id via interop."
            r1 = r21
            X.C211149yY.A01(r1, r4, r2, r5)
            goto L_0x1cdf
        L_0x1cdd:
            r10 = r15
            r9 = 1
        L_0x1cdf:
            X.98p r1 = new X.98p
            r1.<init>(r10, r9, r11)
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r2 = r1.A01
            boolean r12 = r1.A02
            int r11 = r1.A00
            if (r2 != 0) goto L_0x1cff
            java.lang.String r4 = "Fail to get advertising info."
            r3 = 2
            r1 = r21
            boolean r1 = X.C211149yY.A03(r1, r3)
            if (r1 == 0) goto L_0x000a
            r2 = 0
            r1 = r21
            X.C211149yY.A01(r1, r3, r4, r2)
            goto L_0x000a
        L_0x1cff:
            r1 = r21
            r7 = r3
            int r8 = X.AnonymousClass1Y3.AcD
            X.0UN r5 = r1.A01
            r4 = 3
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r4, r8, r5)
            X.1YI r8 = (X.AnonymousClass1YI) r8
            r5 = 544(0x220, float:7.62E-43)
            r4 = 0
            boolean r4 = r8.AbO(r5, r4)
            r8 = 1
            if (r4 != 0) goto L_0x1d40
            if (r3 == 0) goto L_0x1d40
            if (r14 == 0) goto L_0x1d40
            long r3 = r3.A01
            int r5 = (r3 > r34 ? 1 : (r3 == r34 ? 0 : -1))
            if (r5 != 0) goto L_0x1d40
            boolean r4 = r2.A01
            boolean r3 = r14.booleanValue()
            if (r4 == r3) goto L_0x1d40
            java.lang.String r4 = r2.A00
            java.lang.String r3 = r7.A04
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x1d40
            long r3 = r7.A00
            r13 = r1
            r14 = r3
            r16 = r22
            boolean r1 = X.C211149yY.A04(r13, r14, r16)
            if (r1 != 0) goto L_0x1d40
            r8 = 0
        L_0x1d40:
            if (r8 == 0) goto L_0x000a
            X.9yb r3 = new X.9yb
            java.util.UUID r1 = X.C188215g.A00()
            java.lang.String r28 = r1.toString()
            int r4 = X.AnonymousClass1Y3.B3G
            r1 = r21
            X.0UN r1 = r1.A01
            r5 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r4, r1)
            X.4nR r4 = (X.C98554nR) r4
            r36 = 0
            int r7 = X.AnonymousClass1Y3.A4v     // Catch:{ Exception -> 0x1d8d, all -> 0x20af }
            X.0UN r1 = r4.A00     // Catch:{ Exception -> 0x1d8d, all -> 0x20af }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r7, r1)     // Catch:{ Exception -> 0x1d8d, all -> 0x20af }
            android.content.ContentResolver r1 = (android.content.ContentResolver) r1     // Catch:{ Exception -> 0x1d8d, all -> 0x20af }
            android.net.Uri r23 = X.C98554nR.A01     // Catch:{ Exception -> 0x1d8d, all -> 0x20af }
            r24 = 0
            r25 = 0
            r26 = 0
            r27 = 0
            r22 = r1
            android.database.Cursor r9 = r22.query(r23, r24, r25, r26, r27)     // Catch:{ Exception -> 0x1d8d, all -> 0x20af }
            if (r9 == 0) goto L_0x1daf
            boolean r1 = r9.moveToFirst()     // Catch:{ Exception -> 0x1d91 }
            if (r1 == 0) goto L_0x1dac
            java.lang.String r1 = "attribution_json"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x1d91 }
            if (r1 < 0) goto L_0x1dac
            java.lang.String r36 = r9.getString(r1)     // Catch:{ Exception -> 0x1d91 }
            r9.close()
            goto L_0x1daf
        L_0x1d8d:
            r8 = move-exception
            r9 = r36
            goto L_0x1d92
        L_0x1d91:
            r8 = move-exception
        L_0x1d92:
            r10 = 2
            int r7 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x20aa }
            X.0UN r1 = r4.A00     // Catch:{ all -> 0x20aa }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r10, r7, r1)     // Catch:{ all -> 0x20aa }
            X.09P r7 = (X.AnonymousClass09P) r7     // Catch:{ all -> 0x20aa }
            java.lang.Class r1 = r4.getClass()     // Catch:{ all -> 0x20aa }
            java.lang.String r4 = r1.getName()     // Catch:{ all -> 0x20aa }
            java.lang.String r1 = "Failure acquiring oxygen attribution."
            r7.softReport(r4, r1, r8)     // Catch:{ all -> 0x20aa }
            if (r9 == 0) goto L_0x1daf
        L_0x1dac:
            r9.close()
        L_0x1daf:
            int r4 = X.AnonymousClass1Y3.B3G
            r1 = r21
            X.0UN r1 = r1.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r4, r1)
            X.4nR r1 = (X.C98554nR) r1
            int r4 = X.AnonymousClass1Y3.A6m     // Catch:{ Exception -> 0x1dd0 }
            X.0UN r1 = r1.A00     // Catch:{ Exception -> 0x1dd0 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r4, r1)     // Catch:{ Exception -> 0x1dd0 }
            android.content.pm.PackageManager r4 = (android.content.pm.PackageManager) r4     // Catch:{ Exception -> 0x1dd0 }
            r1 = 70
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r1)     // Catch:{ Exception -> 0x1dd0 }
            java.lang.String r37 = r4.getInstallerPackageName(r1)     // Catch:{ Exception -> 0x1dd0 }
            goto L_0x1dd2
        L_0x1dd0:
            r37 = 0
        L_0x1dd2:
            r29 = r2
            r27 = r3
            r30 = r12
            r31 = r17
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r36, r37)
            if (r18 == 0) goto L_0x1e13
            int r5 = X.AnonymousClass1Y3.B9i
            r1 = r21
            X.0UN r4 = r1.A01
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r6, r5, r4)
            X.0km r4 = (X.C10740km) r4
            r7 = r21
            r1 = r19
            java.lang.Object r5 = r4.A05(r1, r3)     // Catch:{ Exception -> 0x1df6 }
            com.facebook.attribution.AttributionState r5 = (com.facebook.attribution.AttributionState) r5     // Catch:{ Exception -> 0x1df6 }
            goto L_0x1dfe
        L_0x1df6:
            r5 = move-exception
            r4 = 5
            java.lang.String r1 = "Failure while creating and sending attribution state through AttributionIdUpdate."
            X.C211149yY.A01(r7, r4, r1, r5)
            r5 = 0
        L_0x1dfe:
            if (r5 == 0) goto L_0x1e0f
            r4 = 6
            int r3 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r7.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r3, r1)
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1
            com.facebook.attribution.AttributionStateSerializer.A02(r5, r1)
            goto L_0x1e18
        L_0x1e0f:
            X.C211149yY.A02(r7, r3)
            goto L_0x1e18
        L_0x1e13:
            r1 = r21
            X.C211149yY.A02(r1, r3)
        L_0x1e18:
            java.lang.Integer r7 = java.lang.Integer.valueOf(r11)
            int r4 = X.AnonymousClass1Y3.Ap7
            r1 = r21
            X.0UN r3 = r1.A01
            r1 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r4, r3)
            X.1ZE r3 = (X.AnonymousClass1ZE) r3
            java.lang.String r1 = "ads_tracking_status"
            X.0bW r3 = r3.A01(r1)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r4 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r1 = 5
            r4.<init>(r3, r1)
            boolean r1 = r4.A0G()
            if (r1 == 0) goto L_0x000a
            r5 = 0
            if (r2 == 0) goto L_0x1eba
            java.lang.String r3 = r2.A00
        L_0x1e40:
            r1 = 793(0x319, float:1.111E-42)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r1)
            r4.A0D(r1, r3)
            if (r2 == 0) goto L_0x1e58
            boolean r1 = r2.A01
            r1 = r1 ^ 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r1)
            java.lang.String r1 = "tracking_enabled"
            r4.A08(r1, r2)
        L_0x1e58:
            if (r7 == 0) goto L_0x1e5e
            java.lang.String r5 = r7.toString()
        L_0x1e5e:
            java.lang.String r1 = "google_play_service_installation"
            r4.A0D(r1, r5)
            int r3 = X.AnonymousClass1Y3.AcD
            r1 = r21
            X.0UN r2 = r1.A01
            r1 = 3
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1YI r3 = (X.AnonymousClass1YI) r3
            r2 = 552(0x228, float:7.74E-43)
            r1 = 0
            boolean r1 = r3.AbO(r2, r1)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x1e82
            java.lang.String r2 = "previous_advertiser_id"
            r1 = r17
            r4.A0D(r2, r1)
        L_0x1e82:
            r4.A06()
            r3 = 3
            int r2 = X.AnonymousClass1Y3.AcD
            r1 = r21
            X.0UN r1 = r1.A01
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1YI r3 = (X.AnonymousClass1YI) r3
            r2 = 742(0x2e6, float:1.04E-42)
            r1 = 0
            boolean r1 = r3.AbO(r2, r1)
            if (r1 == 0) goto L_0x000a
            r3 = 6
            int r2 = X.AnonymousClass1Y3.B6q
            r1 = r21
            X.0UN r1 = r1.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r2 = (com.facebook.prefs.shared.FbSharedPreferences) r2
            r1 = 2
            A01(r2, r1)
            r1 = 3
            A01(r2, r1)
            r1 = 4
            A01(r2, r1)
            r1 = 5
            A01(r2, r1)
            goto L_0x000a
        L_0x1eba:
            r3 = r5
            goto L_0x1e40
        L_0x1ebc:
            int r2 = X.AnonymousClass1Y3.AjQ
            X.0UN r1 = r0.A00
            java.lang.Object r9 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.apk_testing.ApkTestingExposureLogger r9 = (com.facebook.apk_testing.ApkTestingExposureLogger) r9
            int r2 = X.AnonymousClass1Y3.B5j
            X.0UN r1 = r9.A00
            r3 = 5
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0Ud r1 = (X.AnonymousClass0Ud) r1
            boolean r1 = r1.A0U
            r7 = 0
            if (r1 != 0) goto L_0x1ee3
            X.0UN r1 = r9.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0Ud r1 = (X.AnonymousClass0Ud) r1
            boolean r2 = r1.A0V
            r1 = 0
            if (r2 == 0) goto L_0x1ee4
        L_0x1ee3:
            r1 = 1
        L_0x1ee4:
            if (r1 == 0) goto L_0x000a
            r3 = 3
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r9.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1ZE r2 = (X.AnonymousClass1ZE) r2
            java.lang.String r1 = "android_apk_testing_exposure"
            X.0bW r2 = r2.A01(r1)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r3 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r1 = 6
            r3.<init>(r2, r1)
            boolean r1 = r3.A0G()
            if (r1 == 0) goto L_0x000a
            int r1 = X.AnonymousClass1Y3.A6m     // Catch:{ IllegalArgumentException -> 0x1f1b }
            X.0UN r8 = r9.A00     // Catch:{ IllegalArgumentException -> 0x1f1b }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r6, r1, r8)     // Catch:{ IllegalArgumentException -> 0x1f1b }
            android.content.pm.PackageManager r5 = (android.content.pm.PackageManager) r5     // Catch:{ IllegalArgumentException -> 0x1f1b }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AjL     // Catch:{ IllegalArgumentException -> 0x1f1b }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r8)     // Catch:{ IllegalArgumentException -> 0x1f1b }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IllegalArgumentException -> 0x1f1b }
            java.lang.String r5 = r5.getInstallerPackageName(r1)     // Catch:{ IllegalArgumentException -> 0x1f1b }
            goto L_0x1f1c
        L_0x1f1b:
            r5 = 0
        L_0x1f1c:
            if (r5 != 0) goto L_0x1f20
            java.lang.String r5 = ""
        L_0x1f20:
            int r1 = X.AnonymousClass1Y3.AwX
            X.0UN r8 = r9.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r1, r8)
            X.0d3 r1 = (X.AnonymousClass0d3) r1
            int r9 = r1.A01()
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BH4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r8)
            X.00z r2 = (X.C001500z) r2
            X.00z r1 = X.C001500z.A07
            if (r2 != r1) goto L_0x1f76
            int r1 = X.AnonymousClass1Y3.A6m     // Catch:{ NameNotFoundException -> 0x1f76 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r6, r1, r8)     // Catch:{ NameNotFoundException -> 0x1f76 }
            android.content.pm.PackageManager r4 = (android.content.pm.PackageManager) r4     // Catch:{ NameNotFoundException -> 0x1f76 }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AjL     // Catch:{ NameNotFoundException -> 0x1f76 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r8)     // Catch:{ NameNotFoundException -> 0x1f76 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ NameNotFoundException -> 0x1f76 }
            android.content.pm.PackageInfo r1 = r4.getPackageInfo(r1, r7)     // Catch:{ NameNotFoundException -> 0x1f76 }
            long r1 = r1.lastUpdateTime     // Catch:{ NameNotFoundException -> 0x1f76 }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r7
            int r4 = (int) r1     // Catch:{ NameNotFoundException -> 0x1f76 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r9)     // Catch:{ NameNotFoundException -> 0x1f76 }
            java.lang.String r1 = "build_num"
            r3.A0B(r1, r2)     // Catch:{ NameNotFoundException -> 0x1f76 }
            java.lang.String r1 = "installer"
            r3.A0D(r1, r5)     // Catch:{ NameNotFoundException -> 0x1f76 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)     // Catch:{ NameNotFoundException -> 0x1f76 }
            r1 = 15
            java.lang.String r1 = X.C99084oO.$const$string(r1)     // Catch:{ NameNotFoundException -> 0x1f76 }
            r3.A0B(r1, r2)     // Catch:{ NameNotFoundException -> 0x1f76 }
            r3.A06()     // Catch:{ NameNotFoundException -> 0x1f76 }
            goto L_0x000a
        L_0x1f76:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r9)
            java.lang.String r1 = "build_num"
            r3.A0B(r1, r2)
            java.lang.String r1 = "installer"
            r3.A0D(r1, r5)
            r3.A06()
            goto L_0x000a
        L_0x1f89:
            int r2 = X.AnonymousClass1Y3.B7F
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.analytics.timespent.TimeSpentEventReporter r2 = (com.facebook.analytics.timespent.TimeSpentEventReporter) r2
            boolean r1 = com.facebook.analytics.timespent.TimeSpentEventReporter.A04(r2)
            if (r1 == 0) goto L_0x000a
            com.facebook.analytics.timespent.TimeSpentEventReporter.A01(r2)
            goto L_0x000a
        L_0x1f9e:
            int r2 = X.AnonymousClass1Y3.B7F
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.analytics.timespent.TimeSpentEventReporter r2 = (com.facebook.analytics.timespent.TimeSpentEventReporter) r2
            boolean r1 = com.facebook.analytics.timespent.TimeSpentEventReporter.A05(r2)
            if (r1 == 0) goto L_0x000a
            com.facebook.analytics.timespent.TimeSpentEventReporter.A02(r2)
            goto L_0x000a
        L_0x1fb3:
            int r2 = X.AnonymousClass1Y3.BRU
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.analytics.mobileconfigreliability.MobileConfigSampledAccessListenerImpl r5 = (com.facebook.analytics.mobileconfigreliability.MobileConfigSampledAccessListenerImpl) r5
            java.lang.Object r4 = r5.A03
            monitor-enter(r4)
            r5.A05 = r6     // Catch:{ all -> 0x20ac }
            int r2 = X.AnonymousClass1Y3.AcD     // Catch:{ all -> 0x20ac }
            X.0UN r1 = r5.A01     // Catch:{ all -> 0x20ac }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r6, r2, r1)     // Catch:{ all -> 0x20ac }
            X.1YI r3 = (X.AnonymousClass1YI) r3     // Catch:{ all -> 0x20ac }
            r2 = 285(0x11d, float:4.0E-43)
            r1 = 0
            boolean r1 = r3.AbO(r2, r1)     // Catch:{ all -> 0x20ac }
            r5.A02 = r1     // Catch:{ all -> 0x20ac }
            if (r1 != 0) goto L_0x1fed
            java.util.ArrayList r1 = r5.A04     // Catch:{ all -> 0x20ac }
            java.util.Iterator r2 = r1.iterator()     // Catch:{ all -> 0x20ac }
        L_0x1fdd:
            boolean r1 = r2.hasNext()     // Catch:{ all -> 0x20ac }
            if (r1 == 0) goto L_0x1fed
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x20ac }
            java.lang.Runnable r1 = (java.lang.Runnable) r1     // Catch:{ all -> 0x20ac }
            r1.run()     // Catch:{ all -> 0x20ac }
            goto L_0x1fdd
        L_0x1fed:
            java.util.ArrayList r1 = r5.A04     // Catch:{ all -> 0x20ac }
            r1.clear()     // Catch:{ all -> 0x20ac }
            monitor-exit(r4)     // Catch:{ all -> 0x20ac }
            goto L_0x000a
        L_0x1ff5:
            int r2 = X.AnonymousClass1Y3.ADZ
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.analytics.counterlogger.CommunicationScheduler r2 = (com.facebook.analytics.counterlogger.CommunicationScheduler) r2
            com.facebook.analytics.counterlogger.CommunicationScheduler.A01(r2, r4)
            goto L_0x000a
        L_0x2004:
            int r2 = X.AnonymousClass1Y3.ADZ
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.analytics.counterlogger.CommunicationScheduler r2 = (com.facebook.analytics.counterlogger.CommunicationScheduler) r2
            com.facebook.analytics.counterlogger.CommunicationScheduler.A01(r2, r6)
            goto L_0x000a
        L_0x2013:
            int r2 = X.AnonymousClass1Y3.Ajg
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.accessibility.logging.TouchExplorationStateChangeDetector r1 = (com.facebook.accessibility.logging.TouchExplorationStateChangeDetector) r1
            android.view.accessibility.AccessibilityManager$TouchExplorationStateChangeListener r4 = r1.A00
            if (r4 == 0) goto L_0x000a
            r3 = 2
            int r2 = X.AnonymousClass1Y3.Aph
            X.0UN r1 = r1.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            android.view.accessibility.AccessibilityManager r1 = (android.view.accessibility.AccessibilityManager) r1
            r1.removeTouchExplorationStateChangeListener(r4)
            goto L_0x000a
        L_0x2031:
            int r2 = X.AnonymousClass1Y3.Ajg
            X.0UN r1 = r0.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A03(r2, r1)
            com.facebook.accessibility.logging.TouchExplorationStateChangeDetector r5 = (com.facebook.accessibility.logging.TouchExplorationStateChangeDetector) r5
            int r2 = X.AnonymousClass1Y3.BCt
            X.0UN r1 = r5.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r6, r2, r1)
            android.content.Context r1 = (android.content.Context) r1
            X.AnonymousClass3RG.A01(r1)
            android.view.accessibility.AccessibilityManager$TouchExplorationStateChangeListener r4 = r5.A00
            if (r4 == 0) goto L_0x000a
            r3 = 2
            int r2 = X.AnonymousClass1Y3.Aph
            X.0UN r1 = r5.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            android.view.accessibility.AccessibilityManager r1 = (android.view.accessibility.AccessibilityManager) r1
            r1.addTouchExplorationStateChangeListener(r4)
            goto L_0x000a
        L_0x205c:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x205c }
            goto L_0x20b7
        L_0x205f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x205f }
            goto L_0x20b7
        L_0x2062:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x2062 }
            goto L_0x20b7
        L_0x2065:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x2068:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x206b }
            throw r0     // Catch:{ all -> 0x206b }
        L_0x206b:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x206e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x2071 }
            throw r0     // Catch:{ all -> 0x2071 }
        L_0x2071:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x2074:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x2074 }
            goto L_0x20b7
        L_0x2077:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x207a:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x207a }
            goto L_0x20b7
        L_0x207d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x207d }
            goto L_0x20b7
        L_0x2080:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x2083:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x2083 }
            goto L_0x20b7
        L_0x2086:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x2086 }
            goto L_0x20b7
        L_0x2089:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x208c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x208f }
            throw r0     // Catch:{ all -> 0x208f }
        L_0x208f:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x2092:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x2095:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x2098:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x2098 }
            goto L_0x20b7
        L_0x209b:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x209e:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x20a1:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x20a4:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x20a7:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x20a7 }
            goto L_0x20b7
        L_0x20aa:
            r0 = move-exception
            goto L_0x20b2
        L_0x20ac:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x20ac }
            goto L_0x20b7
        L_0x20af:
            r0 = move-exception
            r9 = r36
        L_0x20b2:
            if (r9 == 0) goto L_0x20b7
            r9.close()
        L_0x20b7:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ZR.A04(int, X.1Yz):void");
    }
}
