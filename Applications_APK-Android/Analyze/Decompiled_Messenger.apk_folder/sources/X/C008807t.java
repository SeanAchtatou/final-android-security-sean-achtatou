package X;

/* renamed from: X.07t  reason: invalid class name and case insensitive filesystem */
public final class C008807t {
    public static final Integer A01 = AnonymousClass07B.A0C;
    public Integer A00;

    public static C008807t A00(long j) {
        if (j < 0 || j >= ((long) AnonymousClass07B.A00(4).length)) {
            return new C008807t();
        }
        return new C008807t(AnonymousClass07B.A00(4)[(int) j]);
    }

    public C008807t() {
        this.A00 = A01;
    }

    private C008807t(Integer num) {
        this.A00 = num;
    }
}
