package X;

import android.util.Pair;

/* renamed from: X.1n9  reason: invalid class name */
public final class AnonymousClass1n9 extends AnonymousClass1RO {
    public final /* synthetic */ C52852jn A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass1n9(C52852jn r1, C23581Qb r2) {
        super(r2);
        this.A00 = r1;
    }

    public static void A00(AnonymousClass1n9 r4) {
        Pair pair;
        synchronized (r4.A00) {
            pair = (Pair) r4.A00.A02.poll();
            if (pair == null) {
                C52852jn r1 = r4.A00;
                r1.A00--;
            }
        }
        if (pair != null) {
            AnonymousClass07A.A04(r4.A00.A03, new C43002Cr(r4, pair), -1860297354);
        }
    }
}
