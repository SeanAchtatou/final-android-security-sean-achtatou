package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.cameracore.mediapipeline.asyncscripting.AsyncScriptingClient;
import com.facebook.cameracore.mediapipeline.asyncscripting.AsyncScriptingService;

/* renamed from: X.0Kr  reason: invalid class name and case insensitive filesystem */
public final class C03260Kr implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.cameracore.mediapipeline.asyncscripting.AsyncScriptingClient$3";
    public final /* synthetic */ AsyncScriptingClient A00;

    public C03260Kr(AsyncScriptingClient asyncScriptingClient) {
        this.A00 = asyncScriptingClient;
    }

    public void run() {
        synchronized (this.A00.mConnections) {
            boolean z = false;
            if (!this.A00.mConnections.isEmpty()) {
                z = true;
            }
            AsyncScriptingClient asyncScriptingClient = this.A00;
            if (z != asyncScriptingClient.mRunning) {
                if (z) {
                    Context context = asyncScriptingClient.mContext;
                    C006406k.A02(context, new Intent(context, AsyncScriptingService.class), this.A00.mServiceConnection, 1, 465794388);
                } else {
                    C006406k.A01(asyncScriptingClient.mContext, asyncScriptingClient.mServiceConnection, -2053558895);
                }
                this.A00.mRunning = z;
            }
        }
    }
}
