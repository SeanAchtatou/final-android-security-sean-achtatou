package X;

/* renamed from: X.0NX  reason: invalid class name */
public final class AnonymousClass0NX {
    public static Enum A00(Class cls, String str, Enum enumR) {
        for (Enum enumR2 : (Enum[]) cls.getEnumConstants()) {
            if (enumR2.name().equalsIgnoreCase(str)) {
                return enumR2;
            }
        }
        return enumR;
    }
}
