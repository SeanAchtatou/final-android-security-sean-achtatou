package X;

import android.content.Context;
import com.google.common.base.Function;

/* renamed from: X.0vW  reason: invalid class name and case insensitive filesystem */
public final class C15580vW implements Function {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C13060qW A01;
    public final /* synthetic */ C33691nz A02;

    public C15580vW(Context context, C33691nz r2, C13060qW r3) {
        this.A00 = context;
        this.A02 = r2;
        this.A01 = r3;
    }

    public /* bridge */ /* synthetic */ Object apply(Object obj) {
        C15520vQ r9 = (C15520vQ) obj;
        return new C15600vY(r9.getClass(), r9.Ap6(this.A00), r9.AiM(this.A00), r9.B5H(), r9.Ae6().Ae7(), new C180710b(this, r9));
    }
}
