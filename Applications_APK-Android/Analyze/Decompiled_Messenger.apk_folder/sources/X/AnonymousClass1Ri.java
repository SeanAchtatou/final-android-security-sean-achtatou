package X;

import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.litho.ComponentTree;
import io.card.payment.BuildConfig;
import java.lang.reflect.Field;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1Ri  reason: invalid class name */
public final class AnonymousClass1Ri implements AnonymousClass1AK, AnonymousClass19H {
    public static Field A18;
    public static final AnonymousClass19L A19 = new AnonymousClass19L();
    public static final Rect A1A = new Rect();
    public static final String A1B = "RecyclerBinder";
    private static final AnonymousClass10L A1C = new AnonymousClass10L();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public RecyclerView A08;
    public AnonymousClass10N A09;
    public C35301r0 A0A;
    public AnonymousClass10L A0B;
    public AnonymousClass6QB A0C;
    public AnonymousClass6QN A0D;
    public C61522z1 A0E;
    public Integer A0F;
    public String A0G;
    public List A0H;
    public AtomicLong A0I;
    public boolean A0J;
    public boolean A0K;
    private int A0L;
    private C27799DjA A0M;
    private boolean A0N;
    public final Runnable A0O;
    public final boolean A0P;
    public final float A0Q;
    public final Handler A0R = new Handler(Looper.getMainLooper());
    public final ViewTreeObserver.OnPreDrawListener A0S;
    public final C20831Dz A0T;
    public final AnonymousClass0p4 A0U;
    public final C198419w A0V;
    public final BTL A0W;
    public final AnonymousClass19N A0X;
    public final C197519n A0Y;
    public final AnonymousClass1A2 A0Z;
    public final AnonymousClass1A7 A0a;
    public final C193217r A0b;
    public final Runnable A0c;
    public final Runnable A0d;
    public final Deque A0e;
    public final Deque A0f;
    public final List A0g = new ArrayList();
    public final List A0h = new ArrayList();
    public final AtomicBoolean A0i;
    public final AtomicBoolean A0j;
    public final boolean A0k;
    public final boolean A0l;
    public final boolean A0m;
    public final boolean A0n;
    public final boolean[] A0o;
    public final boolean[] A0p;
    private final C31551js A0q;
    private final C32691mA A0r;
    private final AnonymousClass6QY A0s;
    private final C198219u A0t;
    private final AnonymousClass1A3 A0u;
    private final C198619y A0v;
    private final AtomicBoolean A0w;
    private final AtomicBoolean A0x;
    private final boolean A0y;
    private final boolean A0z;
    private final boolean A10;
    private final boolean A11;
    private final boolean A12;
    private final boolean A13;
    private final boolean A14;
    private final boolean A15;
    public volatile AnonymousClass10L A16;
    public volatile boolean A17;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0031, code lost:
        r9.A0u.CJQ(0, r5, r6, r7, new X.C35601rU(r9, r1, r2, r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A08(int r10, int r11) {
        /*
            r9 = this;
            r7 = r11
            r6 = r10
            monitor-enter(r9)
            boolean r0 = r9.A0S()     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x003d
            int r2 = r9.A04     // Catch:{ all -> 0x003f }
            r0 = -1
            if (r2 == r0) goto L_0x003d
            r1 = 0
            if (r10 == r0) goto L_0x0013
            if (r11 != r0) goto L_0x0015
        L_0x0013:
            r6 = 0
            r7 = 0
        L_0x0015:
            int r0 = r7 - r6
            int r2 = java.lang.Math.max(r2, r0)     // Catch:{ all -> 0x003f }
            java.util.List r0 = r9.A0h     // Catch:{ all -> 0x003f }
            int r5 = r0.size()     // Catch:{ all -> 0x003f }
            boolean r0 = r9.A0n     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x0027
            r2 = r5
            goto L_0x0030
        L_0x0027:
            float r1 = (float) r2     // Catch:{ all -> 0x003f }
            float r0 = r9.A0Q     // Catch:{ all -> 0x003f }
            float r0 = r0 * r1
            int r0 = (int) r0     // Catch:{ all -> 0x003f }
            int r1 = r6 - r0
            int r2 = r2 + r6
            int r2 = r2 + r0
        L_0x0030:
            monitor-exit(r9)     // Catch:{ all -> 0x003f }
            X.1A3 r3 = r9.A0u
            r4 = 0
            X.1rU r8 = new X.1rU
            r8.<init>(r9, r1, r2, r5)
            r3.CJQ(r4, r5, r6, r7, r8)
            return
        L_0x003d:
            monitor-exit(r9)     // Catch:{ all -> 0x003f }
            return
        L_0x003f:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x003f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ri.A08(int, int):void");
    }

    public final synchronized C21681Ih A0T(int i) {
        return ((AnonymousClass1IK) this.A0h.get(i)).A03();
    }

    public final void A0V(int i) {
        AnonymousClass1IK r3;
        A0L(this, 1);
        if (AnonymousClass1AJ.A00) {
            hashCode();
        }
        synchronized (this) {
            r3 = (AnonymousClass1IK) this.A0h.remove(i);
        }
        this.A0T.A07(i);
        AnonymousClass1A7 r2 = this.A0a;
        boolean z = true;
        if (!r2.A03() && i > r2.A01) {
            z = false;
        }
        r2.A02(z);
        AnonymousClass00S.A04(this.A0R, new C73943h1(r3), -1736326266);
    }

    public final synchronized ComponentTree Ahq(int i) {
        AnonymousClass1IK r4 = (AnonymousClass1IK) this.A0h.get(i);
        int A012 = A01(this, r4);
        int A002 = A00(this, r4);
        if (r4.A0A(A012, A002)) {
            return r4.A02();
        }
        r4.A07(this.A0U, A012, A002, null);
        return r4.A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:82:0x00fe, code lost:
        if (r12.A15 == false) goto L_0x0100;
     */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x016c A[LOOP:0: B:106:0x0166->B:108:0x016c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0142  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BKw(X.AnonymousClass10L r13, int r14, int r15, X.AnonymousClass10N r16) {
        /*
            r12 = this;
            r5 = 1
            r2 = 0
            r7 = 0
            r8 = r16
            if (r16 == 0) goto L_0x0008
            r7 = 1
        L_0x0008:
            X.19N r0 = r12.A0X
            int r6 = r0.B1z()
            if (r6 == 0) goto L_0x0039
            r0 = 1
            if (r6 != r0) goto L_0x0031
            int r0 = android.view.View.MeasureSpec.getMode(r15)
            if (r0 == 0) goto L_0x0029
            if (r7 != 0) goto L_0x004f
            int r0 = android.view.View.MeasureSpec.getMode(r14)
            if (r0 != 0) goto L_0x004f
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Can't use Unspecified width on a vertical scrolling Recycler if dynamic measurement is not allowed"
            r1.<init>(r0)
            throw r1
        L_0x0029:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Height mode has to be EXACTLY OR AT MOST for a vertical scrolling RecyclerView"
            r1.<init>(r0)
            throw r1
        L_0x0031:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            java.lang.String r0 = "The orientation defined by LayoutInfo should be either OrientationHelper.HORIZONTAL or OrientationHelper.VERTICAL"
            r1.<init>(r0)
            throw r1
        L_0x0039:
            int r0 = android.view.View.MeasureSpec.getMode(r14)
            if (r0 == 0) goto L_0x0285
            if (r7 != 0) goto L_0x004f
            int r0 = android.view.View.MeasureSpec.getMode(r15)
            if (r0 != 0) goto L_0x004f
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Can't use Unspecified height on an horizontal scrolling Recycler if dynamic measurement is not allowed"
            r1.<init>(r0)
            throw r1
        L_0x004f:
            r1 = 1073741824(0x40000000, float:2.0)
            r11 = 0
            r0 = 1
            if (r6 != r0) goto L_0x006f
            int r0 = android.view.View.MeasureSpec.getMode(r14)
            if (r0 != r1) goto L_0x0076
        L_0x005b:
            r0 = 1
        L_0x005c:
            if (r0 != 0) goto L_0x0061
            if (r7 == 0) goto L_0x0061
            r11 = 1
        L_0x0061:
            boolean r0 = r12.A0m
            if (r0 == 0) goto L_0x0078
            if (r11 == 0) goto L_0x0078
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Cannot use manual estimated viewport count when the RecyclerBinder needs an item to determine its size!"
            r1.<init>(r0)
            throw r1
        L_0x006f:
            int r0 = android.view.View.MeasureSpec.getMode(r15)
            if (r0 != r1) goto L_0x0076
            goto L_0x005b
        L_0x0076:
            r0 = 0
            goto L_0x005c
        L_0x0078:
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0w
            r0.set(r5)
            monitor-enter(r12)     // Catch:{ all -> 0x0277 }
            int r0 = r12.A06     // Catch:{ all -> 0x0274 }
            r4 = -1
            if (r0 == r4) goto L_0x00b7
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0x     // Catch:{ all -> 0x0274 }
            boolean r0 = r0.get()     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x00b7
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x00b7
            if (r6 == r5) goto L_0x01ec
            int r1 = r12.A05     // Catch:{ all -> 0x0274 }
            X.10L r0 = r12.A0B     // Catch:{ all -> 0x0274 }
            int r0 = r0.A00     // Catch:{ all -> 0x0274 }
            boolean r0 = X.C15050ue.A00(r1, r15, r0)     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x020d
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x00af
            X.10L r0 = r12.A0B     // Catch:{ all -> 0x0274 }
            int r0 = r0.A01     // Catch:{ all -> 0x0274 }
        L_0x00a5:
            r13.A01 = r0     // Catch:{ all -> 0x0274 }
            X.10L r0 = r12.A0B     // Catch:{ all -> 0x0274 }
            int r0 = r0.A00     // Catch:{ all -> 0x0274 }
            r13.A00 = r0     // Catch:{ all -> 0x0274 }
            goto L_0x0206
        L_0x00af:
            int r0 = android.view.View.MeasureSpec.getSize(r14)     // Catch:{ all -> 0x0274 }
            goto L_0x00a5
        L_0x00b4:
            X.C27041cY.A00()     // Catch:{ all -> 0x0274 }
        L_0x00b7:
            r12.A06 = r14     // Catch:{ all -> 0x0274 }
            r12.A05 = r15     // Catch:{ all -> 0x0274 }
            boolean r0 = r12.A0R()     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x00d2
            X.0x6 r3 = r12.A06()     // Catch:{ all -> 0x0274 }
            if (r3 == 0) goto L_0x00d2
            int r1 = android.view.View.MeasureSpec.getSize(r14)     // Catch:{ all -> 0x0274 }
            int r0 = android.view.View.MeasureSpec.getSize(r15)     // Catch:{ all -> 0x0274 }
            r12.A0A(r1, r0, r3)     // Catch:{ all -> 0x0274 }
        L_0x00d2:
            X.10L r7 = r12.A03(r14, r15, r7)     // Catch:{ all -> 0x0274 }
            r3 = 0
            if (r6 == r5) goto L_0x00f2
            if (r11 == 0) goto L_0x00e8
            X.10L r0 = r12.A16     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x00e8
            r12.A09 = r8     // Catch:{ all -> 0x0274 }
            java.util.concurrent.atomic.AtomicBoolean r1 = r12.A0x     // Catch:{ all -> 0x0274 }
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x0102
            goto L_0x0100
        L_0x00e8:
            boolean r1 = r12.A0y     // Catch:{ all -> 0x0274 }
            if (r1 != 0) goto L_0x00f0
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x010f
        L_0x00f0:
            r3 = r8
            goto L_0x010f
        L_0x00f2:
            if (r11 == 0) goto L_0x0107
            X.10L r0 = r12.A16     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x0107
            r12.A09 = r8     // Catch:{ all -> 0x0274 }
            java.util.concurrent.atomic.AtomicBoolean r1 = r12.A0x     // Catch:{ all -> 0x0274 }
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x0102
        L_0x0100:
            r0 = 1
            goto L_0x0103
        L_0x0102:
            r0 = 0
        L_0x0103:
            r1.set(r0)     // Catch:{ all -> 0x0274 }
            goto L_0x0116
        L_0x0107:
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x010c
            r3 = r8
        L_0x010c:
            r12.A09 = r3     // Catch:{ all -> 0x0274 }
            goto L_0x0116
        L_0x010f:
            r12.A09 = r3     // Catch:{ all -> 0x0274 }
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0x     // Catch:{ all -> 0x0274 }
            r0.set(r1)     // Catch:{ all -> 0x0274 }
        L_0x0116:
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x01cf
            X.10L r3 = new X.10L     // Catch:{ all -> 0x0274 }
            r3.<init>()     // Catch:{ all -> 0x0274 }
            int r1 = r7.A01     // Catch:{ all -> 0x0274 }
            int r0 = r7.A00     // Catch:{ all -> 0x0274 }
            r12.A09(r1, r0, r3)     // Catch:{ all -> 0x0274 }
            int r0 = r3.A01     // Catch:{ all -> 0x0274 }
            r13.A01 = r0     // Catch:{ all -> 0x0274 }
            int r0 = r3.A00     // Catch:{ all -> 0x0274 }
            r13.A00 = r0     // Catch:{ all -> 0x0274 }
        L_0x012e:
            X.10L r3 = new X.10L     // Catch:{ all -> 0x0274 }
            int r1 = r13.A01     // Catch:{ all -> 0x0274 }
            int r0 = r13.A00     // Catch:{ all -> 0x0274 }
            r3.<init>(r1, r0)     // Catch:{ all -> 0x0274 }
            r12.A0B = r3     // Catch:{ all -> 0x0274 }
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0j     // Catch:{ all -> 0x0274 }
            r0.set(r5)     // Catch:{ all -> 0x0274 }
            X.DjA r3 = r12.A0M     // Catch:{ all -> 0x0274 }
            if (r3 == 0) goto L_0x014a
            X.DjB r0 = new X.DjB     // Catch:{ all -> 0x0274 }
            r0.<init>(r12)     // Catch:{ all -> 0x0274 }
            r3.A01(r0)     // Catch:{ all -> 0x0274 }
        L_0x014a:
            r6 = r12
            boolean r0 = r12.A0l     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x0160
            boolean r0 = r12.A0K     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x0160
            r12.A00 = r5     // Catch:{ all -> 0x0274 }
            boolean r0 = X.C191216w.A00()     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x0176
            r12.A0U()     // Catch:{ all -> 0x0274 }
        L_0x015e:
            r12.A0K = r5     // Catch:{ all -> 0x0274 }
        L_0x0160:
            java.util.Deque r0 = r12.A0e     // Catch:{ all -> 0x0274 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0274 }
        L_0x0166:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x01d9
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0274 }
            X.6QB r0 = (X.AnonymousClass6QB) r0     // Catch:{ all -> 0x0274 }
            r12.A0E(r0)     // Catch:{ all -> 0x0274 }
            goto L_0x0166
        L_0x0176:
            java.util.List r0 = r12.A0h     // Catch:{ all -> 0x0274 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x0190
            X.10L r0 = r12.A0B     // Catch:{ all -> 0x0274 }
            int r3 = r0.A01     // Catch:{ all -> 0x0274 }
            int r1 = r0.A00     // Catch:{ all -> 0x0274 }
            r0 = 0
            r12.A09(r3, r1, r0)     // Catch:{ all -> 0x0274 }
        L_0x0188:
            X.3aP r1 = X.C70293aP.A02     // Catch:{ all -> 0x0274 }
            X.19w r0 = r12.A0V     // Catch:{ all -> 0x0274 }
            r1.A00(r0)     // Catch:{ all -> 0x0274 }
            goto L_0x015e
        L_0x0190:
            java.util.Deque r0 = r12.A0e     // Catch:{ all -> 0x0274 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x0188
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x0274 }
            r7.<init>()     // Catch:{ all -> 0x0274 }
            java.util.Deque r0 = r12.A0e     // Catch:{ all -> 0x0274 }
            java.lang.Object r0 = r0.getFirst()     // Catch:{ all -> 0x0274 }
            X.6QB r0 = (X.AnonymousClass6QB) r0     // Catch:{ all -> 0x0274 }
            java.util.ArrayList r0 = r0.A03     // Catch:{ all -> 0x0274 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x0274 }
        L_0x01ab:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x01c3
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x0274 }
            X.Evn r1 = (X.C30412Evn) r1     // Catch:{ all -> 0x0274 }
            boolean r0 = r1 instanceof X.C30411Evm     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x01ab
            X.Evm r1 = (X.C30411Evm) r1     // Catch:{ all -> 0x0274 }
            X.1IK r0 = r1.A01     // Catch:{ all -> 0x0274 }
            r7.add(r0)     // Catch:{ all -> 0x0274 }
            goto L_0x01ab
        L_0x01c3:
            r8 = 0
            X.10L r0 = r12.A0B     // Catch:{ all -> 0x0274 }
            int r9 = r0.A01     // Catch:{ all -> 0x0274 }
            int r10 = r0.A00     // Catch:{ all -> 0x0274 }
            r11 = 0
            r6.A0Q(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x0274 }
            goto L_0x0188
        L_0x01cf:
            int r0 = r7.A01     // Catch:{ all -> 0x0274 }
            r13.A01 = r0     // Catch:{ all -> 0x0274 }
            int r0 = r7.A00     // Catch:{ all -> 0x0274 }
            r13.A00 = r0     // Catch:{ all -> 0x0274 }
            goto L_0x012e
        L_0x01d9:
            X.6QB r0 = r12.A0C     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x01e0
            r12.A0E(r0)     // Catch:{ all -> 0x0274 }
        L_0x01e0:
            int r0 = r12.A04     // Catch:{ all -> 0x0274 }
            if (r0 == r4) goto L_0x0206
            int r1 = r12.A01     // Catch:{ all -> 0x0274 }
            int r0 = r12.A02     // Catch:{ all -> 0x0274 }
            r12.A08(r1, r0)     // Catch:{ all -> 0x0274 }
            goto L_0x0206
        L_0x01ec:
            int r1 = r12.A06     // Catch:{ all -> 0x0274 }
            X.10L r0 = r12.A0B     // Catch:{ all -> 0x0274 }
            int r0 = r0.A01     // Catch:{ all -> 0x0274 }
            boolean r0 = X.C15050ue.A00(r1, r14, r0)     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x020d
            X.10L r1 = r12.A0B     // Catch:{ all -> 0x0274 }
            int r0 = r1.A01     // Catch:{ all -> 0x0274 }
            r13.A01 = r0     // Catch:{ all -> 0x0274 }
            boolean r0 = r12.A15     // Catch:{ all -> 0x0274 }
            if (r0 == 0) goto L_0x0208
            int r0 = r1.A00     // Catch:{ all -> 0x0274 }
        L_0x0204:
            r13.A00 = r0     // Catch:{ all -> 0x0274 }
        L_0x0206:
            monitor-exit(r12)     // Catch:{ all -> 0x0274 }
            goto L_0x0264
        L_0x0208:
            int r0 = android.view.View.MeasureSpec.getSize(r15)     // Catch:{ all -> 0x0274 }
            goto L_0x0204
        L_0x020d:
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0j     // Catch:{ all -> 0x0274 }
            r0.set(r2)     // Catch:{ all -> 0x0274 }
            boolean r10 = X.C27041cY.A02()     // Catch:{ all -> 0x0274 }
            if (r10 == 0) goto L_0x021d
            java.lang.String r0 = "invalidateLayoutData"
            X.C27041cY.A01(r0)     // Catch:{ all -> 0x0274 }
        L_0x021d:
            boolean r0 = r12.A0m     // Catch:{ all -> 0x0274 }
            if (r0 != 0) goto L_0x0224
            r0 = -1
            r12.A04 = r0     // Catch:{ all -> 0x0274 }
        L_0x0224:
            r0 = 0
            r12.A16 = r0     // Catch:{ all -> 0x0274 }
            r9 = 0
            java.util.List r0 = r12.A0h     // Catch:{ all -> 0x0274 }
            int r3 = r0.size()     // Catch:{ all -> 0x0274 }
        L_0x022e:
            if (r9 >= r3) goto L_0x023f
            java.util.List r0 = r12.A0h     // Catch:{ all -> 0x0274 }
            java.lang.Object r1 = r0.get(r9)     // Catch:{ all -> 0x0274 }
            X.1IK r1 = (X.AnonymousClass1IK) r1     // Catch:{ all -> 0x0274 }
            monitor-enter(r1)     // Catch:{ all -> 0x0274 }
            r1.A08 = r2     // Catch:{ all -> 0x0271 }
            monitor-exit(r1)     // Catch:{ all -> 0x0274 }
            int r9 = r9 + 1
            goto L_0x022e
        L_0x023f:
            android.os.Looper r1 = android.os.Looper.myLooper()     // Catch:{ all -> 0x0274 }
            android.os.Looper r0 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x0274 }
            if (r1 != r0) goto L_0x024f
            X.1Dz r0 = r12.A0T     // Catch:{ all -> 0x0274 }
            r0.A04()     // Catch:{ all -> 0x0274 }
            goto L_0x0260
        L_0x024f:
            android.os.Handler r1 = r12.A0R     // Catch:{ all -> 0x0274 }
            java.lang.Runnable r0 = r12.A0c     // Catch:{ all -> 0x0274 }
            X.AnonymousClass00S.A02(r1, r0)     // Catch:{ all -> 0x0274 }
            android.os.Handler r3 = r12.A0R     // Catch:{ all -> 0x0274 }
            java.lang.Runnable r1 = r12.A0c     // Catch:{ all -> 0x0274 }
            r0 = -963387034(0xffffffffc693e166, float:-18928.7)
            X.AnonymousClass00S.A04(r3, r1, r0)     // Catch:{ all -> 0x0274 }
        L_0x0260:
            if (r10 == 0) goto L_0x00b7
            goto L_0x00b4
        L_0x0264:
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0w
            r0.set(r2)
            boolean r0 = r12.A17
            if (r0 == 0) goto L_0x0270
            r12.A07()
        L_0x0270:
            return
        L_0x0271:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0274 }
            throw r0     // Catch:{ all -> 0x0274 }
        L_0x0274:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x0274 }
            throw r0     // Catch:{ all -> 0x0277 }
        L_0x0277:
            r1 = move-exception
            java.util.concurrent.atomic.AtomicBoolean r0 = r12.A0w
            r0.set(r2)
            boolean r0 = r12.A17
            if (r0 == 0) goto L_0x0284
            r12.A07()
        L_0x0284:
            throw r1
        L_0x0285:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Width mode has to be EXACTLY OR AT MOST for an horizontal scrolling RecyclerView"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ri.BKw(X.10L, int, int, X.10N):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r0 != false) goto L_0x0048;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void CBs(int r6, int r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            int r1 = r5.A06     // Catch:{ all -> 0x004a }
            r0 = -1
            if (r1 == r0) goto L_0x0037
            r0 = 1073741824(0x40000000, float:2.0)
            int r4 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r0)     // Catch:{ all -> 0x004a }
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r7, r0)     // Catch:{ all -> 0x004a }
            X.19N r0 = r5.A0X     // Catch:{ all -> 0x004a }
            int r2 = r0.B1z()     // Catch:{ all -> 0x004a }
            int r1 = r5.A06     // Catch:{ all -> 0x004a }
            r0 = -1
            if (r1 == r0) goto L_0x0034
            if (r2 == 0) goto L_0x0029
            r0 = 1
            if (r2 != r0) goto L_0x0034
            X.10L r0 = r5.A0B     // Catch:{ all -> 0x004a }
            int r0 = r0.A01     // Catch:{ all -> 0x004a }
            boolean r0 = X.C15050ue.A00(r1, r4, r0)     // Catch:{ all -> 0x004a }
            goto L_0x0035
        L_0x0029:
            int r1 = r5.A05     // Catch:{ all -> 0x004a }
            X.10L r0 = r5.A0B     // Catch:{ all -> 0x004a }
            int r0 = r0.A00     // Catch:{ all -> 0x004a }
            boolean r0 = X.C15050ue.A00(r1, r3, r0)     // Catch:{ all -> 0x004a }
            goto L_0x0035
        L_0x0034:
            r0 = 0
        L_0x0035:
            if (r0 != 0) goto L_0x0048
        L_0x0037:
            X.10L r3 = X.AnonymousClass1Ri.A1C     // Catch:{ all -> 0x004a }
            r0 = 1073741824(0x40000000, float:2.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r0)     // Catch:{ all -> 0x004a }
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r7, r0)     // Catch:{ all -> 0x004a }
            X.10N r0 = r5.A09     // Catch:{ all -> 0x004a }
            r5.BKw(r3, r2, r1, r0)     // Catch:{ all -> 0x004a }
        L_0x0048:
            monitor-exit(r5)
            return
        L_0x004a:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ri.CBs(int, int):void");
    }

    public static int A00(AnonymousClass1Ri r3, AnonymousClass1IK r4) {
        AnonymousClass19N r2;
        int i;
        if (r3.A0y) {
            return 0;
        }
        if (r3.A0S()) {
            r2 = r3.A0X;
            i = View.MeasureSpec.makeMeasureSpec(r3.A0B.A00, 1073741824);
        } else {
            r2 = r3.A0X;
            i = r3.A05;
        }
        return r2.Ah7(i, r4.A03());
    }

    private static int A02(List list, boolean z) {
        int i;
        if (z) {
            i = list.size() - 1;
            while (i >= 0) {
                if (!((AnonymousClass1IK) list.get(i)).A03().C2H()) {
                    i--;
                }
            }
            return -1;
        }
        int i2 = 0;
        int size = list.size();
        while (i < size) {
            if (!((AnonymousClass1IK) list.get(i)).A03().C2H()) {
                i2 = i + 1;
            }
        }
        return -1;
        return i;
    }

    private AnonymousClass10L A03(int i, int i2, boolean z) {
        boolean z2;
        int size;
        int size2;
        AnonymousClass10L r5 = new AnonymousClass10L();
        int B1z = this.A0X.B1z();
        boolean z3 = false;
        if (B1z != 1 ? View.MeasureSpec.getMode(i2) != 1073741824 : View.MeasureSpec.getMode(i) != 1073741824) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (!z2 && z) {
            z3 = true;
        }
        if (B1z != 1) {
            size2 = View.MeasureSpec.getSize(i);
            size = !z3 ? View.MeasureSpec.getSize(i2) : this.A16 != null ? this.A16.A00 : 0;
        } else {
            size = View.MeasureSpec.getSize(i2);
            size2 = !z3 ? View.MeasureSpec.getSize(i) : this.A16 != null ? this.A16.A01 : 0;
        }
        r5.A01 = size2;
        r5.A00 = size;
        return r5;
    }

    public static AnonymousClass1IK A04(AnonymousClass1Ri r10, C21681Ih r11) {
        C31551js r9;
        Object remove;
        if (r10.A0M != null) {
            Object AjM = r11.AjM(C22298Ase.$const$string(AnonymousClass1Y3.A1a));
            if (AjM instanceof String) {
                String str = (String) AjM;
                C15940wE r2 = r10.A0M.A00.A00;
                if (str != null) {
                    synchronized (r2) {
                        remove = r2.A06.remove(str);
                        if (remove != null) {
                            r2.A00 -= C15940wE.A00(r2, str, remove);
                        }
                    }
                    AnonymousClass1IK r3 = (AnonymousClass1IK) remove;
                    if (r3 != null) {
                        Object AjM2 = r11.AjM("prevent_release");
                        if (AjM2 != null) {
                            r3.A03().AMi("prevent_release", AjM2);
                        }
                        return r3;
                    }
                } else {
                    throw new NullPointerException("key == null");
                }
            }
        }
        C32691mA r0 = r10.A0r;
        if (r0 != null) {
            r9 = r0.AVd(r11);
        } else {
            r9 = r10.A0q;
            if (r9 == null) {
                r9 = null;
            }
        }
        AnonymousClass6QY r8 = r10.A0s;
        boolean z = r10.A0z;
        boolean z2 = r10.A13;
        boolean z3 = r10.A14;
        boolean z4 = r10.A12;
        boolean z5 = r10.A11;
        boolean z6 = r10.A10;
        AnonymousClass1IJ r1 = new AnonymousClass1IJ();
        if (r11 == null) {
            r11 = C21621Ib.A01();
        }
        r1.A03 = r11;
        r1.A00 = r9;
        r1.A02 = r8;
        r1.A05 = z;
        r1.A04 = z2;
        r1.A09 = z3;
        r1.A08 = z4;
        r1.A07 = z5;
        r1.A01 = null;
        r1.A06 = z6;
        if (r11 != null) {
            return new AnonymousClass1IK(r1);
        }
        throw new IllegalArgumentException("A RenderInfo must be specified to create a ComponentTreeHolder");
    }

    private C16460x6 A06() {
        int A022;
        if (!this.A0h.isEmpty()) {
            int A023 = A02(this.A0h, this.A0P);
            if (this.A01 >= this.A0h.size() || A023 < 0) {
                return null;
            }
            return new C16460x6(A023, this.A0h);
        } else if (this.A0g.isEmpty() || (A022 = A02(this.A0g, this.A0P)) < 0) {
            return null;
        } else {
            return new C16460x6(A022, this.A0g);
        }
    }

    private void A0A(int i, int i2, C16460x6 r16) {
        C637038i Aht;
        String Asm;
        C72593eb r6;
        int Aia;
        if (!this.A0m) {
            boolean A022 = C27041cY.A02();
            boolean A012 = C35301r0.A01(this.A0A);
            C16460x6 r3 = r16;
            C35541rO r5 = new C35541rO(r3.A01, r3.A00, this.A0h.size() - 1, this.A0P);
            if (A022) {
                C27041cY.A01("maybeScheduleAsyncLayoutsDuringInitRange");
            }
            List list = this.A0h;
            if (list != null && !list.isEmpty()) {
                BTL btl = this.A0W;
                if (btl == null) {
                    Aia = 1;
                } else {
                    Aia = btl.Aia();
                }
                for (int i3 = 0; i3 < Aia; i3++) {
                    A0O(this, r5);
                }
            }
            if (A022) {
                C27041cY.A00();
            }
            AnonymousClass1IK r10 = (AnonymousClass1IK) r3.A01.get(r3.A00);
            int A013 = A01(this, r10);
            int A002 = A00(this, r10);
            if (A012) {
                this.A0A.A03("_firstlayout", "_start", this.A0G);
            }
            if (A022) {
                C27041cY.A01("firstLayout");
            }
            AnonymousClass0p4 r1 = this.A0U;
            if (r1.A05() != null) {
                Aht = r1.A05();
                Asm = r1.A0B();
            } else {
                Aht = r10.A03().Aht();
                Asm = r10.A03().Asm();
            }
            if (Aht == null) {
                r6 = null;
            } else {
                AnonymousClass0p4 r52 = this.A0U;
                r6 = C637138j.A01(Aht, Asm, Aht.BLg(r52, 20), r52.A07);
            }
            try {
                AnonymousClass10L r53 = new AnonymousClass10L();
                r10.A07(this.A0U, A013, A002, r53);
                int max = Math.max(this.A0X.AOk(r53.A01, r53.A00, i, i2), 1);
                this.A16 = r53;
                this.A04 = max;
            } finally {
                if (r6 != null) {
                    Aht.BJI(r6);
                }
                if (A022) {
                    C27041cY.A00();
                }
                if (A012) {
                    this.A0A.A03("_firstlayout", "_end", this.A0G);
                }
            }
        }
    }

    private void A0E(AnonymousClass6QB r4) {
        Iterator it = r4.A03.iterator();
        while (it.hasNext()) {
            C30412Evn evn = (C30412Evn) it.next();
            if (evn instanceof C30411Evm) {
                A0B(((C30411Evm) evn).A01);
            }
        }
    }

    public static void A0F(AnonymousClass1Ri r1) {
        if (r1.A0n && !r1.A0h.isEmpty()) {
            throw new UnsupportedOperationException("Circular lists do not support insert operation");
        }
    }

    public static void A0G(AnonymousClass1Ri r8) {
        if (AnonymousClass07c.isDebugModeEnabled || AnonymousClass07c.isEndToEndTestRun) {
            long id = Thread.currentThread().getId();
            long andSet = r8.A0I.getAndSet(id);
            if (id != andSet && andSet != -1) {
                throw new IllegalStateException("Multiple threads applying change sets at once! (" + andSet + " and " + id + ")");
            }
        }
    }

    public static void A0H(AnonymousClass1Ri r6) {
        boolean z;
        if (!r6.A0f.isEmpty() && r6.A0N) {
            RecyclerView recyclerView = r6.A08;
            if (recyclerView != null && recyclerView.A1B() && recyclerView.isAttachedToWindow()) {
                if (recyclerView.getWindowVisibility() == 0) {
                    ViewParent viewParent = recyclerView;
                    while (true) {
                        if (!(viewParent instanceof View)) {
                            z = recyclerView.getGlobalVisibleRect(A1A);
                            break;
                        }
                        View view = (View) viewParent;
                        if (view.getAlpha() <= 0.0f || view.getVisibility() != 0) {
                            break;
                        }
                        viewParent = view.getParent();
                    }
                }
                z = false;
                if (z) {
                    if (r6.A0f.size() > 20) {
                        r6.A0f.clear();
                        StringBuilder sb = new StringBuilder("recyclerView: ");
                        sb.append(recyclerView);
                        sb.append(", hasPendingAdapterUpdates(): ");
                        sb.append(recyclerView.A1B());
                        sb.append(", isAttachedToWindow(): ");
                        sb.append(recyclerView.isAttachedToWindow());
                        sb.append(", getWindowVisibility(): ");
                        sb.append(recyclerView.getWindowVisibility());
                        sb.append(", vie visible hierarchy: ");
                        ViewParent viewParent2 = recyclerView;
                        ArrayList arrayList = new ArrayList();
                        while (viewParent2 instanceof View) {
                            View view2 = (View) viewParent2;
                            arrayList.add("view=" + view2.getClass().getSimpleName() + ", alpha=" + view2.getAlpha() + ", visibility=" + view2.getVisibility());
                            if (view2.getAlpha() <= 0.0f || view2.getVisibility() != 0) {
                                break;
                            }
                            viewParent2 = view2.getParent();
                        }
                        sb.append(arrayList);
                        sb.append(", getGlobalVisibleRect(): ");
                        sb.append(recyclerView.getGlobalVisibleRect(A1A));
                        sb.append(", isComputingLayout(): ");
                        boolean z2 = false;
                        if (recyclerView.A0A > 0) {
                            z2 = true;
                        }
                        sb.append(z2);
                        sb.append(", isSubAdapter: ");
                        sb.append(false);
                        sb.append(", visible range: [");
                        sb.append(r6.A01);
                        sb.append(", ");
                        sb.append(r6.A02);
                        sb.append("]");
                        Integer num = AnonymousClass07B.A01;
                        C09070gU.A01(num, "RecyclerBinder:DataRenderedNotTriggered", "@OnDataRendered callbacks aren't triggered as expected: " + ((Object) sb));
                        return;
                    }
                    return;
                }
            }
            boolean z3 = false;
            if (recyclerView != null) {
                z3 = true;
            }
            ArrayDeque arrayDeque = new ArrayDeque(r6.A0f);
            r6.A0f.clear();
            AnonymousClass00S.A03(r6.A0R, new C20911Eh(arrayDeque, z3), -2040743435);
        }
    }

    public static void A0I(AnonymousClass1Ri r2) {
        RecyclerView recyclerView = r2.A08;
        if (recyclerView != null && r2.A0a.A03()) {
            recyclerView.removeCallbacks(r2.A0d);
            C15320v6.postOnAnimation(r2.A08, r2.A0d);
        }
        r2.A08(r2.A01, r2.A02);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        if (r3.A00 == r2.A00) goto L_0x0044;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0J(X.AnonymousClass1Ri r4) {
        /*
            java.util.concurrent.atomic.AtomicBoolean r0 = r4.A0j
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x0048
            java.util.concurrent.atomic.AtomicBoolean r0 = r4.A0x
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0014
            boolean r0 = r4.A15
            if (r0 == 0) goto L_0x0049
        L_0x0014:
            X.10L r1 = r4.A0B
            int r0 = r1.A01
            if (r0 == 0) goto L_0x0041
            int r0 = r1.A00
            if (r0 == 0) goto L_0x0041
            int r2 = r4.A06
            int r1 = r4.A05
            r0 = 1
            X.10L r0 = r4.A03(r2, r1, r0)
            X.10L r3 = new X.10L
            r3.<init>()
            int r1 = r0.A01
            int r0 = r0.A00
            r4.A09(r1, r0, r3)
            int r1 = r3.A01
            X.10L r2 = r4.A0B
            int r0 = r2.A01
            if (r1 != r0) goto L_0x0041
            int r1 = r3.A00
            int r0 = r2.A00
            if (r1 == r0) goto L_0x0044
        L_0x0041:
            A0K(r4)
        L_0x0044:
            boolean r0 = r4.A15
            if (r0 != 0) goto L_0x0049
        L_0x0048:
            return
        L_0x0049:
            boolean r0 = r4.A0R()
            if (r0 != 0) goto L_0x006e
            java.util.List r1 = r4.A0h
            boolean r0 = r4.A0P
            int r1 = A02(r1, r0)
            if (r1 < 0) goto L_0x006e
            X.0x6 r3 = new X.0x6
            java.util.List r0 = r4.A0h
            r3.<init>(r1, r0)
            X.10L r0 = r4.A0B
            int r2 = r0.A01
            int r1 = r0.A00
            X.19N r0 = r4.A0X
            r0.B1z()
            r4.A0A(r2, r1, r3)
        L_0x006e:
            A0I(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ri.A0J(X.1Ri):void");
    }

    public static void A0K(AnonymousClass1Ri r3) {
        if (AnonymousClass1AJ.A00) {
            r3.hashCode();
        }
        if (r3.A08 != null) {
            AnonymousClass00S.A02(r3.A0R, r3.A0O);
            r3.A08.removeCallbacks(r3.A0O);
            C15320v6.postOnAnimation(r3.A08, r3.A0O);
            return;
        }
        AnonymousClass00S.A02(r3.A0R, r3.A0O);
        AnonymousClass00S.A04(r3.A0R, r3.A0O, 1770661016);
    }

    public static void A0L(AnonymousClass1Ri r1, int i) {
        if (r1.A0n && !r1.A0h.isEmpty() && r1.A0h.size() != i) {
            throw new UnsupportedOperationException("Circular lists do not support insert operation");
        }
    }

    public static void A0N(AnonymousClass1Ri r2, C30412Evn evn) {
        if (r2.A0C == null) {
            r2.A0C = new AnonymousClass6QB(r2.A00);
        }
        r2.A0C.A03.add(evn);
    }

    public static void A0P(C21681Ih r1) {
        if (r1 == null) {
            throw new RuntimeException("Received null RenderInfo to insert/update!");
        }
    }

    private void A0Q(List list, int i, int i2, int i3, AnonymousClass10L r16) {
        C27380Dbe AWH = this.A0X.AWH(i2, i3);
        if (AWH != null) {
            boolean A022 = C27041cY.A02();
            if (A022) {
                C27041cY.A01("computeLayoutsToFillListViewport");
            }
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
            AnonymousClass10L r3 = new AnonymousClass10L();
            while (AWH.CMu() && i < list.size()) {
                AnonymousClass1IK r9 = (AnonymousClass1IK) list.get(i);
                C21681Ih A032 = r9.A03();
                if (A032.C2K()) {
                    break;
                }
                r9.A07(this.A0U, this.A0X.AhA(makeMeasureSpec, A032), this.A0X.Ah7(makeMeasureSpec2, A032), r3);
                AWH.AMR(A032, r3.A01, r3.A00);
                i++;
            }
            AnonymousClass10L r32 = r16;
            if (r16 != null) {
                int Amp = AWH.Amp();
                if (this.A0X.B1z() == 1) {
                    r32.A01 = i2;
                    r32.A00 = Math.min(Amp, i3);
                } else {
                    r32.A01 = Math.min(Amp, i2);
                    r32.A00 = i3;
                }
            }
            if (A022) {
                C27041cY.A00();
            }
            list.size();
            if (AnonymousClass1AJ.A00) {
                hashCode();
            }
        }
    }

    private boolean A0R() {
        if ((this.A16 == null || this.A04 == -1) && !this.A0m) {
            return false;
        }
        return true;
    }

    private boolean A0S() {
        if (!this.A0j.get() || this.A0x.get()) {
            return false;
        }
        return true;
    }

    public void A0W(int i, int i2) {
        RecyclerView recyclerView = this.A08;
        if (recyclerView != null) {
            AnonymousClass19T r1 = recyclerView.A0L;
            if (r1 instanceof AnonymousClass19S) {
                ((AnonymousClass19S) r1).C4c(i, i2);
                return;
            }
        }
        this.A01 = i;
        this.A03 = i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002d, code lost:
        if (r0 > 0) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0059, code lost:
        if (r9 > ((r2 + r4) - 1)) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        if (r8 > ((r2 + r4) - 1)) goto L_0x0063;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0X(int r8, int r9) {
        /*
            r7 = this;
            boolean r0 = X.AnonymousClass1AJ.A00
            if (r0 == 0) goto L_0x0007
            r7.hashCode()
        L_0x0007:
            monitor-enter(r7)
            java.util.List r0 = r7.A0h     // Catch:{ all -> 0x006d }
            java.lang.Object r6 = r0.remove(r8)     // Catch:{ all -> 0x006d }
            X.1IK r6 = (X.AnonymousClass1IK) r6     // Catch:{ all -> 0x006d }
            java.util.List r0 = r7.A0h     // Catch:{ all -> 0x006d }
            r0.add(r9, r6)     // Catch:{ all -> 0x006d }
            int r5 = r7.A04     // Catch:{ all -> 0x006d }
            r0 = -1
            if (r5 == r0) goto L_0x002f
            float r4 = (float) r9     // Catch:{ all -> 0x006d }
            int r3 = r7.A01     // Catch:{ all -> 0x006d }
            float r2 = (float) r3     // Catch:{ all -> 0x006d }
            float r1 = (float) r5     // Catch:{ all -> 0x006d }
            float r0 = r7.A0Q     // Catch:{ all -> 0x006d }
            float r1 = r1 * r0
            float r2 = r2 - r1
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x002f
            int r3 = r3 + r5
            float r0 = (float) r3     // Catch:{ all -> 0x006d }
            float r0 = r0 + r1
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            r1 = 1
            if (r0 <= 0) goto L_0x0030
        L_0x002f:
            r1 = 0
        L_0x0030:
            monitor-exit(r7)     // Catch:{ all -> 0x006d }
            boolean r0 = r6.A09()
            if (r0 == 0) goto L_0x003c
            if (r1 != 0) goto L_0x003c
            r6.A04()
        L_0x003c:
            X.1Dz r0 = r7.A0T
            X.1A1 r0 = r0.A01
            r0.A01(r8, r9)
            X.1A7 r5 = r7.A0a
            int r4 = r7.A04
            boolean r0 = r5.A03()
            r3 = 1
            if (r0 != 0) goto L_0x0069
            r0 = -1
            if (r4 == r0) goto L_0x0069
            int r2 = r5.A00
            if (r9 < r2) goto L_0x005b
            int r0 = r2 + r4
            int r0 = r0 - r3
            r1 = 1
            if (r9 <= r0) goto L_0x005c
        L_0x005b:
            r1 = 0
        L_0x005c:
            if (r8 < r2) goto L_0x0063
            int r2 = r2 + r4
            int r2 = r2 - r3
            r0 = 1
            if (r8 <= r2) goto L_0x0064
        L_0x0063:
            r0 = 0
        L_0x0064:
            if (r1 != 0) goto L_0x0069
            if (r0 != 0) goto L_0x0069
            r3 = 0
        L_0x0069:
            r5.A02(r3)
            return
        L_0x006d:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x006d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ri.A0X(int, int):void");
    }

    public final void A0a(int i, C21681Ih r5) {
        boolean C2K;
        if (AnonymousClass1AJ.A00) {
            hashCode();
            r5.getName();
        }
        synchronized (this) {
            AnonymousClass1IK r2 = (AnonymousClass1IK) this.A0h.get(i);
            C2K = r2.A03().C2K();
            A0P(r5);
            this.A0Z.A00(r5);
            A0D(r2, r5);
        }
        if (C2K || r5.C2K()) {
            this.A0T.A05(i);
        }
        AnonymousClass1A7 r1 = this.A0a;
        r1.A02(r1.A04(i, 1));
    }

    public final void A0c(int i, List list) {
        if (AnonymousClass1AJ.A00) {
            String[] strArr = new String[list.size()];
            for (int i2 = 0; i2 < list.size(); i2++) {
                strArr[i2] = ((C21681Ih) list.get(i2)).getName();
            }
            hashCode();
            list.size();
            Arrays.toString(strArr);
        }
        synchronized (this) {
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                int i4 = i + i3;
                AnonymousClass1IK r2 = (AnonymousClass1IK) this.A0h.get(i4);
                C21681Ih r1 = (C21681Ih) list.get(i3);
                A0P(r1);
                if (r1.C2K() || r2.A03().C2K()) {
                    this.A0T.A05(i4);
                }
                this.A0Z.A00(r1);
                A0D(r2, r1);
            }
        }
        AnonymousClass1A7 r12 = this.A0a;
        r12.A02(r12.A04(i, list.size()));
    }

    /* renamed from: A0d */
    public void BLE(RecyclerView recyclerView) {
        int size;
        C20861Ec r1;
        RecyclerView recyclerView2 = this.A08;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                CJk(recyclerView2);
            }
            if (this.A17) {
                A0U();
            }
            this.A08 = recyclerView;
            this.A0N = true;
            AnonymousClass19T AsH = this.A0X.AsH();
            int i = 0;
            AsH.A1R(false);
            recyclerView.getPaddingLeft();
            recyclerView.A10(AsH);
            recyclerView.A0v(this.A0T);
            recyclerView.A12(this.A0a.A06);
            if (AsH instanceof AnonymousClass1AW) {
                ((AnonymousClass1AW) AsH).A08.set(new Rect(recyclerView.getPaddingLeft(), recyclerView.getPaddingTop(), recyclerView.getPaddingRight(), recyclerView.getPaddingBottom()));
            }
            if (recyclerView instanceof C15020ub) {
                ((C15020ub) recyclerView).C0j(this.A0Y);
            } else if (recyclerView.getViewTreeObserver() != null) {
                recyclerView.getViewTreeObserver().addOnPreDrawListener(this.A0S);
            }
            this.A0X.CBG(this);
            this.A0a.A01(this.A0v);
            int i2 = this.A01;
            if (i2 != -1 && i2 >= 0 && !this.A0n) {
                Integer num = this.A0F;
                if (num != null) {
                    int i3 = this.A03;
                    if (this.A08 == null) {
                        this.A01 = i2;
                        this.A03 = i3;
                        this.A0F = num;
                    } else {
                        if (num == AnonymousClass07B.A0Y) {
                            i2++;
                        }
                        C621830i A002 = C61312yg.A00(this.A0U.A09, i3, num);
                        A002.A00 = i2;
                        this.A08.A0L.A1I(A002);
                    }
                } else if (AsH instanceof AnonymousClass19S) {
                    ((AnonymousClass19S) AsH).C4c(i2, this.A03);
                } else {
                    recyclerView.A0j(i2);
                }
            } else if (this.A0n) {
                if (this.A0h.isEmpty()) {
                    size = 0;
                } else {
                    size = 1073741823 % this.A0h.size();
                }
                int i4 = 1073741823 - size;
                int i5 = this.A01;
                if (i5 != -1 && i5 >= 0) {
                    i = i5;
                }
                recyclerView.A0j(i4 + i);
                C27372DbW dbW = new C27372DbW(this, recyclerView, AsH);
                recyclerView.A0Q = dbW;
                C15320v6.setAccessibilityDelegate(recyclerView, dbW);
            }
            RecyclerView recyclerView3 = this.A08;
            if (this.A0n) {
                Log.w(A1B, "Sticky header is not supported for circular RecyclerViews");
            } else if (Build.VERSION.SDK_INT < 14) {
                Log.w(A1B, "Sticky header is supported only on ICS (API14) and above");
            } else if (recyclerView3 != null) {
                if (recyclerView3.getParent() instanceof C20861Ec) {
                    r1 = (C20861Ec) recyclerView3.getParent();
                } else {
                    r1 = null;
                }
                if (r1 != null) {
                    AnonymousClass6QN r0 = this.A0D;
                    if (r0 == null) {
                        this.A0E = new C61522z1(this);
                    } else {
                        this.A0E = r0.getController(this);
                    }
                    this.A0E.A0A(r1);
                }
            }
        }
    }

    /* renamed from: A0e */
    public void CJk(RecyclerView recyclerView) {
        boolean z;
        int A0l2;
        AnonymousClass19T AsH = this.A0X.AsH();
        View A0t2 = AsH.A0t(this.A01);
        if (A0t2 != null) {
            AnonymousClass19N r2 = this.A0X;
            AnonymousClass19T AsH2 = r2.AsH();
            if (AsH2 instanceof AnonymousClass19S) {
                z = ((AnonymousClass19S) AsH2).A07;
            } else {
                z = false;
            }
            if (r2.B1z() == 0) {
                if (z) {
                    A0l2 = (recyclerView.getWidth() - AsH.A0g()) - AsH.A0k(A0t2);
                } else {
                    A0l2 = AsH.A0j(A0t2) - AsH.A0f();
                }
            } else if (z) {
                A0l2 = (recyclerView.getHeight() - AsH.A0e()) - AsH.A0i(A0t2);
            } else {
                A0l2 = AsH.A0l(A0t2) - AsH.A0h();
            }
            this.A03 = A0l2;
        } else {
            this.A03 = 0;
        }
        recyclerView.A13(this.A0a.A06);
        if (recyclerView instanceof C15020ub) {
            ((C15020ub) recyclerView).CJu(this.A0Y);
        } else if (recyclerView.getViewTreeObserver() != null) {
            recyclerView.getViewTreeObserver().removeOnPreDrawListener(this.A0S);
        }
        A0H(this);
        recyclerView.A0v(null);
        recyclerView.A10(null);
        AnonymousClass1A7 r22 = this.A0a;
        C198619y r1 = this.A0v;
        if (r1 != null) {
            synchronized (r22) {
                if (!r22.A07.isEmpty()) {
                    r22.A07.remove(r1);
                }
            }
        }
        if (this.A08 == recyclerView) {
            this.A08 = null;
            C61522z1 r0 = this.A0E;
            if (r0 != null) {
                r0.A09();
            }
            this.A0X.CBG(null);
        }
    }

    public boolean ARR() {
        return this.A0J;
    }

    public int AZo() {
        return this.A0X.AZo();
    }

    public int AZq() {
        return this.A0X.AZq();
    }

    public int AZr() {
        return this.A0X.AZr();
    }

    public int AZs() {
        return this.A0X.AZs();
    }

    public int ArU() {
        return this.A0T.ArU();
    }

    public boolean BHP(int i) {
        if (i < 0 || i >= this.A0h.size()) {
            return false;
        }
        return true;
    }

    public boolean BHW() {
        return this.A15;
    }

    public AnonymousClass1Ri(AnonymousClass1F9 r8) {
        boolean z;
        boolean z2;
        BTL btl;
        boolean z3 = false;
        this.A0j = new AtomicBoolean(false);
        this.A0x = new AtomicBoolean(false);
        this.A0I = new AtomicLong(-1);
        this.A0e = new ArrayDeque();
        this.A0i = new AtomicBoolean(false);
        this.A0w = new AtomicBoolean(false);
        this.A0f = new ArrayDeque();
        this.A0O = new C197419m(this);
        this.A0Y = new C197519n(this);
        this.A0S = new C197719p(this);
        this.A0c = new C198119t(this);
        this.A0t = new C198219u(this);
        this.A0V = new C198319v(this);
        this.A06 = -1;
        this.A05 = -1;
        this.A01 = -1;
        this.A02 = -1;
        this.A04 = -1;
        this.A17 = false;
        this.A0N = false;
        this.A00 = 0;
        this.A0K = false;
        this.A0L = 0;
        this.A0G = BuildConfig.FLAVOR;
        this.A0o = new boolean[1];
        this.A0p = new boolean[1];
        AnonymousClass6QY r6 = null;
        this.A0C = null;
        this.A0v = new C198519x(this);
        this.A0d = new C198719z(this);
        this.A0U = r8.A02;
        this.A0k = r8.A0E;
        this.A0T = new AnonymousClass1A0(this);
        this.A0Q = r8.A00;
        this.A0X = r8.A07;
        C32691mA r1 = r8.A06;
        this.A0r = r1;
        this.A0b = r8.A0A;
        this.A10 = r8.A0L;
        if (r1 == null && !((btl = r8.A04) == null && (btl = AnonymousClass07c.threadPoolConfiguration) == null)) {
            this.A0W = btl;
            this.A0q = new BTK(btl);
        }
        this.A0Z = new AnonymousClass1A2(false, 0);
        this.A0n = r8.A0I;
        AnonymousClass19N r5 = this.A0X;
        if (r5.B1z() == 0) {
            z = r8.A0F;
        } else {
            z = false;
        }
        this.A0y = z;
        this.A0s = z ? new AnonymousClass6QY(this) : r6;
        this.A15 = r8.A0N;
        this.A0J = false;
        AnonymousClass19T AsH = r5.AsH();
        z3 = AsH instanceof AnonymousClass19S ? ((AnonymousClass19S) AsH).mStackFromEnd : z3;
        AnonymousClass19T AsH2 = r5.AsH();
        if (AsH2 instanceof AnonymousClass19S) {
            z2 = ((AnonymousClass19S) AsH2).A07;
        } else {
            z2 = false;
        }
        boolean z4 = z3 ^ z2;
        this.A0P = z4;
        if (z4) {
            this.A0u = AnonymousClass1A3.A00;
        } else {
            this.A0u = AnonymousClass1A3.A01;
        }
        this.A0a = new AnonymousClass1A7(-1, -1, r8.A07);
        this.A0H = r8.A0B;
        int i = r8.A01;
        if (i != -1) {
            this.A04 = i;
            this.A0m = true;
        }
        this.A0l = r8.A0G;
        this.A0z = r8.A0H;
        this.A0D = r8.A09;
        this.A14 = r8.A0M;
        this.A13 = r8.A0C;
        this.A12 = r8.A0K;
        this.A11 = r8.A0J;
        this.A0M = r8.A05;
        this.A0A = r8.A03;
    }

    public static int A01(AnonymousClass1Ri r3, AnonymousClass1IK r4) {
        AnonymousClass19N r2;
        int i;
        if (r3.A0S()) {
            r2 = r3.A0X;
            i = View.MeasureSpec.makeMeasureSpec(r3.A0B.A01, 1073741824);
        } else {
            r2 = r3.A0X;
            i = r3.A06;
        }
        return r2.AhA(i, r4.A03());
    }

    public static C30411Evm A05(AnonymousClass1Ri r1, int i, C21681Ih r3) {
        AnonymousClass1IK A042 = A04(r1, r3);
        synchronized (A042) {
            A042.A07 = false;
        }
        return new C30411Evm(i, A042);
    }

    private void A07() {
        if (C191216w.A00()) {
            A0U();
        } else {
            C70293aP.A02.A00(this.A0V);
        }
    }

    private void A09(int i, int i2, AnonymousClass10L r12) {
        int AZq;
        C16460x6 A062;
        boolean A022 = C27041cY.A02();
        if (A022) {
            C27041cY.A01("fillListViewport");
        }
        if (this.A15) {
            AZq = 0;
        } else {
            AZq = this.A0X.AZq();
        }
        int i3 = 0;
        if (AZq != -1) {
            i3 = AZq;
        }
        A0Q(this.A0h, i3, i, i2, r12);
        if (!A0R() && (A062 = A06()) != null) {
            this.A0X.B1z();
            A0A(i, i2, A062);
        }
        if (A022) {
            C27041cY.A00();
        }
    }

    private void A0B(AnonymousClass1IK r5) {
        int A012 = A01(this, r5);
        int A002 = A00(this, r5);
        if (!r5.A0A(A012, A002)) {
            r5.A06(this.A0U, A012, A002, null);
        } else if (r5.A08()) {
            ComponentTree A022 = r5.A02();
            if (A022.A0o != null) {
                A022.A0o = null;
                A07();
            }
        }
    }

    public static void A0C(AnonymousClass1IK r2) {
        boolean z;
        if (r2.A09()) {
            Object AjM = r2.A05.AjM("prevent_release");
            if (AjM instanceof Boolean) {
                z = ((Boolean) AjM).booleanValue();
            } else {
                z = false;
            }
            if (!z && !r2.A03().BGx() && r2.A02() != null && r2.A02().getLithoView() == null) {
                r2.A04();
            }
        }
    }

    private void A0D(AnonymousClass1IK r6, C21681Ih r7) {
        C21681Ih A032 = r6.A03();
        synchronized (r6) {
            synchronized (r6) {
                try {
                    r6.A08 = false;
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
            r6.A05 = r7;
        }
        C32691mA r0 = this.A0r;
        if (r0 != null && r0.CF6(A032, r7)) {
            C31551js AVd = this.A0r.AVd(r7);
            synchronized (r6) {
                r6.A02 = AVd;
                ComponentTree componentTree = r6.A01;
                if (componentTree != null) {
                    synchronized (componentTree.A0Y) {
                        try {
                            AnonymousClass16M r1 = componentTree.A07;
                            if (r1 != null) {
                                componentTree.A0A.C1H(r1);
                            }
                        } catch (Throwable th2) {
                            while (true) {
                                th = th2;
                                break;
                            }
                        }
                    }
                    synchronized (componentTree.A0X) {
                        try {
                            AnonymousClass16I r12 = componentTree.A06;
                            if (r12 != null) {
                                componentTree.A0A.C1H(r12);
                            }
                        } catch (Throwable th3) {
                            while (true) {
                                th = th3;
                                break;
                            }
                        }
                    }
                    componentTree.A0A = ComponentTree.A04(AVd);
                }
            }
        }
    }

    public static void A0M(AnonymousClass1Ri r3, C30411Evm evm) {
        A0N(r3, evm);
        AnonymousClass1IK r2 = evm.A01;
        C198219u r1 = r3.A0t;
        synchronized (r2) {
            ComponentTree componentTree = r2.A01;
            if (componentTree != null) {
                componentTree.A0o = r1;
            } else {
                r2.A04 = r1;
            }
        }
        if (r3.A0S()) {
            r3.A0B(r2);
        }
    }

    public static void A0O(AnonymousClass1Ri r5, C35541rO r6) {
        AnonymousClass1IK A002 = r6.next();
        List list = r5.A0h;
        if (list != null && !list.isEmpty() && A002 != null && r5.A04 == -1) {
            int A012 = A01(r5, A002);
            int A003 = A00(r5, A002);
            if (!A002.A0A(A012, A003)) {
                A002.A06(r5.A0U, A012, A003, new C35551rP(r5, r6, A002));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0095, code lost:
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r6 = r5.A03.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x009c, code lost:
        if (r7 >= r6) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009e, code lost:
        r8 = (X.C30412Evn) r5.A03.get(r7);
        r3 = r8.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a8, code lost:
        if (r3 == 0) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ab, code lost:
        if (r3 == 1) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00ae, code lost:
        if (r3 == 2) goto L_0x00d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b1, code lost:
        if (r3 == 3) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b4, code lost:
        if (r3 == 4) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00b7, code lost:
        if (r3 != 5) goto L_0x0149;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00b9, code lost:
        r8 = (X.C30410Evl) r8;
        A0X(r8.A00, r8.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c3, code lost:
        r8 = (X.C30408Evj) r8;
        A0Y(r8.A01, r8.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00cd, code lost:
        A0V(((X.C30409Evk) r8).A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00d5, code lost:
        r8 = (X.C39381yx) r8;
        A0c(r8.A00, r8.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00df, code lost:
        r8 = (X.C30407Evi) r8;
        A0a(r8.A00, r8.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00e9, code lost:
        r8 = (X.C30411Evm) r8;
        r1 = r8.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00ed, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r0 = r1.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00f1, code lost:
        if (r0 != false) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00f5, code lost:
        if (X.AnonymousClass1AJ.A00 == false) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00f7, code lost:
        hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x00fa, code lost:
        r11.A0Z.A00(r8.A01.A03());
        r11.A0h.add(r8.A00, r8.A01);
        r1 = r8.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0111, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        r1.A07 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0115, code lost:
        r11.A0T.A06(r8.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x011c, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0120, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0158, code lost:
        throw new java.lang.RuntimeException(X.AnonymousClass08S.A09(X.AnonymousClass80H.$const$string(123), r3));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0U() {
        /*
            r11 = this;
            boolean r10 = X.C27041cY.A02()
            if (r10 == 0) goto L_0x000b
            java.lang.String r0 = "applyReadyBatches"
            X.C27041cY.A01(r0)
        L_0x000b:
            java.util.concurrent.atomic.AtomicBoolean r0 = r11.A0i     // Catch:{ all -> 0x0172 }
            boolean r0 = r0.get()     // Catch:{ all -> 0x0172 }
            r4 = 0
            if (r0 == 0) goto L_0x016a
            java.util.concurrent.atomic.AtomicBoolean r0 = r11.A0j     // Catch:{ all -> 0x0172 }
            boolean r0 = r0.get()     // Catch:{ all -> 0x0172 }
            if (r0 == 0) goto L_0x016a
            java.util.concurrent.atomic.AtomicBoolean r0 = r11.A0w     // Catch:{ all -> 0x0172 }
            boolean r0 = r0.get()     // Catch:{ all -> 0x0172 }
            if (r0 != 0) goto L_0x016a
            androidx.recyclerview.widget.RecyclerView r0 = r11.A08     // Catch:{ all -> 0x0172 }
            if (r0 == 0) goto L_0x002f
            int r1 = r0.A0A     // Catch:{ all -> 0x0172 }
            r0 = 0
            if (r1 <= 0) goto L_0x0030
            r0 = 1
            goto L_0x0030
        L_0x002f:
            r0 = 0
        L_0x0030:
            if (r0 == 0) goto L_0x0045
            int r0 = r11.A0L     // Catch:{ all -> 0x0172 }
            int r1 = r0 + 1
            r11.A0L = r1     // Catch:{ all -> 0x0172 }
            r0 = 100
            if (r1 > r0) goto L_0x0162
            X.3aP r1 = X.C70293aP.A02     // Catch:{ all -> 0x0172 }
            X.19w r0 = r11.A0V     // Catch:{ all -> 0x0172 }
            r1.A00(r0)     // Catch:{ all -> 0x0172 }
            goto L_0x016c
        L_0x0045:
            r11.A0L = r4     // Catch:{ all -> 0x0172 }
            r9 = 0
        L_0x0048:
            monitor-enter(r11)     // Catch:{ all -> 0x0172 }
            java.util.Deque r0 = r11.A0e     // Catch:{ all -> 0x015f }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x015f }
            if (r0 == 0) goto L_0x0059
            java.util.concurrent.atomic.AtomicBoolean r0 = r11.A0i     // Catch:{ all -> 0x015f }
            r0.set(r4)     // Catch:{ all -> 0x015f }
        L_0x0056:
            monitor-exit(r11)     // Catch:{ all -> 0x015f }
            goto L_0x0135
        L_0x0059:
            java.util.Deque r0 = r11.A0e     // Catch:{ all -> 0x015f }
            java.lang.Object r5 = r0.peekFirst()     // Catch:{ all -> 0x015f }
            X.6QB r5 = (X.AnonymousClass6QB) r5     // Catch:{ all -> 0x015f }
            int r0 = r5.A00     // Catch:{ all -> 0x015f }
            if (r0 == 0) goto L_0x008a
            java.util.ArrayList r0 = r5.A03     // Catch:{ all -> 0x015f }
            int r3 = r0.size()     // Catch:{ all -> 0x015f }
            r2 = 0
        L_0x006c:
            if (r2 >= r3) goto L_0x008a
            java.util.ArrayList r0 = r5.A03     // Catch:{ all -> 0x015f }
            java.lang.Object r1 = r0.get(r2)     // Catch:{ all -> 0x015f }
            X.Evn r1 = (X.C30412Evn) r1     // Catch:{ all -> 0x015f }
            boolean r0 = r1 instanceof X.C30411Evm     // Catch:{ all -> 0x015f }
            if (r0 == 0) goto L_0x0085
            X.Evm r1 = (X.C30411Evm) r1     // Catch:{ all -> 0x015f }
            X.1IK r0 = r1.A01     // Catch:{ all -> 0x015f }
            boolean r0 = r0.A08()     // Catch:{ all -> 0x015f }
            if (r0 != 0) goto L_0x0085
            goto L_0x0088
        L_0x0085:
            int r2 = r2 + 1
            goto L_0x006c
        L_0x0088:
            r0 = 0
            goto L_0x008b
        L_0x008a:
            r0 = 1
        L_0x008b:
            if (r0 == 0) goto L_0x0056
            java.util.Deque r0 = r11.A0e     // Catch:{ all -> 0x015f }
            r0.pollFirst()     // Catch:{ all -> 0x015f }
            monitor-exit(r11)     // Catch:{ all -> 0x015f }
            r2 = r11
            monitor-enter(r2)     // Catch:{ all -> 0x0172 }
            r7 = 0
            java.util.ArrayList r0 = r5.A03     // Catch:{ all -> 0x015c }
            int r6 = r0.size()     // Catch:{ all -> 0x015c }
        L_0x009c:
            if (r7 >= r6) goto L_0x0120
            java.util.ArrayList r0 = r5.A03     // Catch:{ all -> 0x015c }
            java.lang.Object r8 = r0.get(r7)     // Catch:{ all -> 0x015c }
            X.Evn r8 = (X.C30412Evn) r8     // Catch:{ all -> 0x015c }
            int r3 = r8.A00     // Catch:{ all -> 0x015c }
            if (r3 == 0) goto L_0x00e9
            r0 = 1
            if (r3 == r0) goto L_0x00df
            r0 = 2
            if (r3 == r0) goto L_0x00d5
            r0 = 3
            if (r3 == r0) goto L_0x00cd
            r0 = 4
            if (r3 == r0) goto L_0x00c3
            r0 = 5
            if (r3 != r0) goto L_0x0149
            X.Evl r8 = (X.C30410Evl) r8     // Catch:{ all -> 0x015c }
            int r1 = r8.A00     // Catch:{ all -> 0x015c }
            int r0 = r8.A01     // Catch:{ all -> 0x015c }
            r11.A0X(r1, r0)     // Catch:{ all -> 0x015c }
            goto L_0x011c
        L_0x00c3:
            X.Evj r8 = (X.C30408Evj) r8     // Catch:{ all -> 0x015c }
            int r1 = r8.A01     // Catch:{ all -> 0x015c }
            int r0 = r8.A00     // Catch:{ all -> 0x015c }
            r11.A0Y(r1, r0)     // Catch:{ all -> 0x015c }
            goto L_0x011c
        L_0x00cd:
            X.Evk r8 = (X.C30409Evk) r8     // Catch:{ all -> 0x015c }
            int r0 = r8.A00     // Catch:{ all -> 0x015c }
            r11.A0V(r0)     // Catch:{ all -> 0x015c }
            goto L_0x011c
        L_0x00d5:
            X.1yx r8 = (X.C39381yx) r8     // Catch:{ all -> 0x015c }
            int r1 = r8.A00     // Catch:{ all -> 0x015c }
            java.util.List r0 = r8.A01     // Catch:{ all -> 0x015c }
            r11.A0c(r1, r0)     // Catch:{ all -> 0x015c }
            goto L_0x011c
        L_0x00df:
            X.Evi r8 = (X.C30407Evi) r8     // Catch:{ all -> 0x015c }
            int r1 = r8.A00     // Catch:{ all -> 0x015c }
            X.1Ih r0 = r8.A01     // Catch:{ all -> 0x015c }
            r11.A0a(r1, r0)     // Catch:{ all -> 0x015c }
            goto L_0x011c
        L_0x00e9:
            X.Evm r8 = (X.C30411Evm) r8     // Catch:{ all -> 0x015c }
            X.1IK r1 = r8.A01     // Catch:{ all -> 0x015c }
            monitor-enter(r1)     // Catch:{ all -> 0x015c }
            boolean r0 = r1.A07     // Catch:{ all -> 0x0159 }
            monitor-exit(r1)     // Catch:{ all -> 0x015c }
            if (r0 != 0) goto L_0x011c
            boolean r0 = X.AnonymousClass1AJ.A00     // Catch:{ all -> 0x015c }
            if (r0 == 0) goto L_0x00fa
            r11.hashCode()     // Catch:{ all -> 0x015c }
        L_0x00fa:
            X.1A2 r1 = r11.A0Z     // Catch:{ all -> 0x015c }
            X.1IK r0 = r8.A01     // Catch:{ all -> 0x015c }
            X.1Ih r0 = r0.A03()     // Catch:{ all -> 0x015c }
            r1.A00(r0)     // Catch:{ all -> 0x015c }
            java.util.List r3 = r11.A0h     // Catch:{ all -> 0x015c }
            int r1 = r8.A00     // Catch:{ all -> 0x015c }
            X.1IK r0 = r8.A01     // Catch:{ all -> 0x015c }
            r3.add(r1, r0)     // Catch:{ all -> 0x015c }
            X.1IK r1 = r8.A01     // Catch:{ all -> 0x015c }
            r0 = 1
            monitor-enter(r1)     // Catch:{ all -> 0x015c }
            r1.A07 = r0     // Catch:{ all -> 0x0159 }
            monitor-exit(r1)     // Catch:{ all -> 0x015c }
            X.1Dz r1 = r11.A0T     // Catch:{ all -> 0x015c }
            int r0 = r8.A00     // Catch:{ all -> 0x015c }
            r1.A06(r0)     // Catch:{ all -> 0x015c }
        L_0x011c:
            int r7 = r7 + 1
            goto L_0x009c
        L_0x0120:
            monitor-exit(r2)     // Catch:{ all -> 0x015c }
            X.1Ea r0 = r5.A01     // Catch:{ all -> 0x0172 }
            r0.BVN()     // Catch:{ all -> 0x0172 }
            java.util.Deque r1 = r11.A0f     // Catch:{ all -> 0x0172 }
            X.1Ea r0 = r5.A01     // Catch:{ all -> 0x0172 }
            r1.addLast(r0)     // Catch:{ all -> 0x0172 }
            A0H(r11)     // Catch:{ all -> 0x0172 }
            boolean r0 = r5.A02     // Catch:{ all -> 0x0172 }
            r9 = r9 | r0
            goto L_0x0048
        L_0x0135:
            if (r9 == 0) goto L_0x016c
            X.1r0 r0 = r11.A0A     // Catch:{ all -> 0x0172 }
            boolean r0 = X.C35301r0.A01(r0)     // Catch:{ all -> 0x0172 }
            if (r0 == 0) goto L_0x0145
            X.1r0 r0 = r11.A0A     // Catch:{ all -> 0x0172 }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x0172 }
            r11.A0G = r0     // Catch:{ all -> 0x0172 }
        L_0x0145:
            A0J(r11)     // Catch:{ all -> 0x0172 }
            goto L_0x016c
        L_0x0149:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x015c }
            r0 = 123(0x7b, float:1.72E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ all -> 0x015c }
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r3)     // Catch:{ all -> 0x015c }
            r1.<init>(r0)     // Catch:{ all -> 0x015c }
            throw r1     // Catch:{ all -> 0x015c }
        L_0x0159:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x015c }
            throw r0     // Catch:{ all -> 0x015c }
        L_0x015c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x015c }
            throw r0     // Catch:{ all -> 0x0172 }
        L_0x015f:
            r1 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x015f }
            goto L_0x0169
        L_0x0162:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0172 }
            java.lang.String r0 = "Too many retries -- RecyclerView is stuck in layout."
            r1.<init>(r0)     // Catch:{ all -> 0x0172 }
        L_0x0169:
            throw r1     // Catch:{ all -> 0x0172 }
        L_0x016a:
            r11.A0L = r4     // Catch:{ all -> 0x0172 }
        L_0x016c:
            if (r10 == 0) goto L_0x0171
            X.C27041cY.A00()
        L_0x0171:
            return
        L_0x0172:
            r0 = move-exception
            if (r10 == 0) goto L_0x0178
            X.C27041cY.A00()
        L_0x0178:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ri.A0U():void");
    }

    public final void A0Y(int i, int i2) {
        A0L(this, i2);
        if (AnonymousClass1AJ.A00) {
            hashCode();
        }
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            for (int i3 = 0; i3 < i2; i3++) {
                arrayList.add((AnonymousClass1IK) this.A0h.remove(i));
            }
        }
        this.A0T.A01.A03(i, i2);
        AnonymousClass1A7 r2 = this.A0a;
        boolean z = true;
        if (!r2.A03() && i > r2.A01) {
            z = false;
        }
        r2.A02(z);
        AnonymousClass00S.A04(this.A0R, new C73933h0(arrayList), 596521907);
    }

    public final void A0Z(int i, C21681Ih r4) {
        A0F(this);
        if (AnonymousClass1AJ.A00) {
            hashCode();
            r4.getName();
        }
        A0P(r4);
        AnonymousClass1IK A042 = A04(this, r4);
        synchronized (this) {
            if (!this.A17) {
                this.A0h.add(i, A042);
                this.A0Z.A00(r4);
            } else {
                throw new RuntimeException("Trying to do a sync insert when using asynchronous mutations!");
            }
        }
        this.A0T.A06(i);
        AnonymousClass1A7 r1 = this.A0a;
        r1.A02(r1.A05(i, this.A04));
    }

    public final void A0b(int i, List list) {
        A0F(this);
        int i2 = 0;
        if (AnonymousClass1AJ.A00) {
            String[] strArr = new String[list.size()];
            for (int i3 = 0; i3 < list.size(); i3++) {
                strArr[i3] = ((C21681Ih) list.get(i3)).getName();
            }
            hashCode();
            list.size();
            Arrays.toString(strArr);
        }
        synchronized (this) {
            int size = list.size();
            while (i2 < size) {
                C21681Ih r3 = (C21681Ih) list.get(i2);
                A0P(r3);
                AnonymousClass1IK A042 = A04(this, r3);
                if (!this.A17) {
                    this.A0h.add(i + i2, A042);
                    this.A0Z.A00(r3);
                    i2++;
                } else {
                    throw new RuntimeException("Trying to do a sync insert when using asynchronous mutations!");
                }
            }
        }
        C20831Dz r0 = this.A0T;
        r0.A01.A02(i, list.size());
        AnonymousClass1A7 r1 = this.A0a;
        list.size();
        r1.A02(r1.A05(i, this.A04));
    }

    public void A0f(boolean z, C20841Ea r5) {
        boolean A022 = C27041cY.A02();
        if (A022) {
            C27041cY.A01("notifyChangeSetComplete");
        }
        try {
            if (AnonymousClass1AJ.A00) {
                hashCode();
            }
            if (!this.A17) {
                r5.BVN();
                this.A0f.addLast(r5);
                A0H(this);
                if (z) {
                    if (C35301r0.A01(this.A0A)) {
                        this.A0G = this.A0A.A00;
                    }
                    A0J(this);
                }
                if (!A022) {
                    return;
                }
                return;
            }
            throw new RuntimeException("Trying to do a sync notifyChangeSetComplete when using asynchronous mutations!");
        } finally {
            if (A022) {
                C27041cY.A00();
            }
        }
    }

    public void AXC() {
        ArrayList arrayList;
        if (C191216w.A00()) {
            List list = this.A0h;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                ((AnonymousClass1IK) list.get(i)).A05();
            }
            return;
        }
        synchronized (this) {
            arrayList = new ArrayList(this.A0h);
        }
        AnonymousClass00S.A04(this.A0R, new C73933h0(arrayList), 596521907);
    }

    public boolean BGy(int i) {
        if (!BHP(i) || !((AnonymousClass1IK) this.A0h.get(i)).A03().BGx()) {
            return false;
        }
        return true;
    }

    public /* bridge */ /* synthetic */ void APv(ViewGroup viewGroup) {
    }

    public /* bridge */ /* synthetic */ void CJZ(ViewGroup viewGroup) {
    }
}
