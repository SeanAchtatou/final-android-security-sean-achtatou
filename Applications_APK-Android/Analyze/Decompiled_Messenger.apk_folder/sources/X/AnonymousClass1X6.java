package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.1X6  reason: invalid class name */
public final class AnonymousClass1X6 {
    public static final long A03 = TimeUnit.DAYS.toMillis(7);
    public final long A00;
    public final String A01;
    public final String A02;

    public AnonymousClass1X6(String str, String str2, long j) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = j;
    }
}
