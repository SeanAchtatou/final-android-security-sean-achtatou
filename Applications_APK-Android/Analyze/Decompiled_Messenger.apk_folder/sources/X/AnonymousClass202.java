package X;

import com.facebook.messaging.location.nearbyplacespicker.NearbyPlacesPickerDialogFragment;
import com.facebook.messaging.montage.model.art.ArtItem;

/* renamed from: X.202  reason: invalid class name */
public final class AnonymousClass202 extends C79943rW {
    public final /* synthetic */ CwW A00;
    public final /* synthetic */ ArtItem A01;
    public final /* synthetic */ C26512Czy A02;

    public AnonymousClass202(CwW cwW, ArtItem artItem, C26512Czy czy) {
        this.A00 = cwW;
        this.A01 = artItem;
        this.A02 = czy;
    }

    public void BiO() {
        NearbyPlacesPickerDialogFragment nearbyPlacesPickerDialogFragment = new NearbyPlacesPickerDialogFragment();
        nearbyPlacesPickerDialogFragment.A27(this.A00.A0Q.A17().A0T(), "LOCATION_SHARE_FRAGMENT_TAG", true);
        nearbyPlacesPickerDialogFragment.A00 = new C26404Cxk(this);
    }
}
