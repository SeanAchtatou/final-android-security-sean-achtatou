package X;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0ve  reason: invalid class name and case insensitive filesystem */
public final class C15660ve extends C191817c {
    private C24904CNv A00;
    private AnonymousClass3QL A01;
    public final ArrayList A02 = new ArrayList();

    public boolean A0S(C33781o8 r7, C33781o8 r8, int i, int i2, int i3, int i4) {
        C33781o8 r1 = r7;
        if (r7 == r8) {
            return A0R(r1, i, i2, i3, i4);
        }
        if (r7 != null) {
            A0B(r7);
        }
        if (r8 != null) {
            A0B(r8);
        }
        return false;
    }

    public boolean A0P(C33781o8 r9) {
        int indexOf;
        ObjectAnimator A002;
        C24904CNv cNv = this.A00;
        if (!(cNv == null || (A002 = cNv.A00()) == null)) {
            this.A02.add(A002);
        }
        C54612mh r1 = ((CNh) r9).A00;
        List list = this.A01.A02;
        if (list == null) {
            indexOf = -1;
        } else {
            indexOf = list.indexOf(r1);
        }
        int max = Math.max(0, indexOf);
        View view = r9.A0H;
        view.setTranslationX(-150.0f);
        view.setAlpha(0.0f);
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(r9.A0H, PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0.0f), PropertyValuesHolder.ofFloat(View.ALPHA, 1.0f));
        ofPropertyValuesHolder.setStartDelay((long) (max * 100));
        ofPropertyValuesHolder.addListener(new C29694EgB(this, r9, view));
        this.A02.add(ofPropertyValuesHolder);
        return true;
    }

    public boolean A0Q(C33781o8 r6) {
        ObjectAnimator A002;
        C24904CNv cNv = this.A00;
        if (!(cNv == null || (A002 = cNv.A00()) == null)) {
            this.A02.add(A002);
        }
        r6.A0H.setAlpha(1.0f);
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(r6.A0H, PropertyValuesHolder.ofFloat(View.ALPHA, 0.0f));
        ofPropertyValuesHolder.addListener(new C29692Eg9(this, r6));
        this.A02.add(ofPropertyValuesHolder);
        return true;
    }

    public boolean A0R(C33781o8 r7, int i, int i2, int i3, int i4) {
        View view = r7.A0H;
        float f = (float) i;
        view.setX(f);
        view.setVisibility(0);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, View.X, f, (float) i3);
        ofFloat.addListener(new C29693EgA(this, r7));
        this.A02.add(ofFloat);
        return true;
    }

    public C15660ve(AnonymousClass3QL r2, C24904CNv cNv) {
        this.A01 = r2;
        this.A00 = cNv;
    }
}
