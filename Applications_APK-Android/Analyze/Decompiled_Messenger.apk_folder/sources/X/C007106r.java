package X;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/* renamed from: X.06r  reason: invalid class name and case insensitive filesystem */
public final class C007106r {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    public static int A05(Resources resources, int i) {
        TypedValue typedValue = new TypedValue();
        resources.getValue(i, typedValue, false);
        return Math.round(TypedValue.complexToFloat(typedValue.data));
    }

    public static String A06(Resources resources) {
        int i;
        if (resources == null || (i = resources.getDisplayMetrics().densityDpi) <= 160) {
            return "1";
        }
        if (i >= 320) {
            return "2";
        }
        return "1.5";
    }

    public static int A00(Context context, float f) {
        return (int) ((f * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int A01(Context context, float f) {
        return (int) (f / context.getResources().getDisplayMetrics().density);
    }

    public static int A02(Context context, float f) {
        return (int) ((f * context.getResources().getDisplayMetrics().scaledDensity) + 0.5f);
    }

    public static int A03(Resources resources, float f) {
        return (int) ((f * resources.getDisplayMetrics().density) + 0.5f);
    }

    public static int A04(Resources resources, int i) {
        return Math.round(((float) resources.getDimensionPixelSize(i)) * resources.getConfiguration().fontScale);
    }
}
