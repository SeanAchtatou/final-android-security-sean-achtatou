package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.1Lo  reason: invalid class name and case insensitive filesystem */
public final class C22491Lo extends Handler {
    public final /* synthetic */ C22411Lf A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C22491Lo(C22411Lf r1, Looper looper) {
        super(looper);
        this.A00 = r1;
    }

    public void handleMessage(Message message) {
        int i;
        long j;
        int i2;
        if (message != null && message.what == 1) {
            C22411Lf r5 = this.A00;
            C117635hk r4 = (C117635hk) message.obj;
            boolean z = false;
            if (r4.A04 != null) {
                z = true;
            }
            for (int i3 = 0; i3 < r4.A00; i3++) {
                int i4 = r4.A03[i3];
                boolean z2 = r4.A05[i3];
                if (z) {
                    j = r4.A04[i3];
                } else {
                    j = 0;
                }
                AnonymousClass1T3 r0 = r5.A06;
                AnonymousClass1T3.A01(r0);
                if (z2) {
                    i2 = r0.A02;
                } else {
                    i2 = r0.A03;
                }
                r5.A00(i4, i2, z2, j);
            }
            r4.A00 = 0;
            r4.A01 = 0;
            r4.A02 = null;
            synchronized (r5.A07) {
                C117635hk r2 = r5.A02;
                if (r2 == null || r2.A01 <= 20) {
                    r4.A02 = r2;
                    if (r2 != null) {
                        i = r2.A01 + 1;
                    } else {
                        i = 0;
                    }
                    r4.A01 = i;
                    r5.A02 = r4;
                }
            }
        }
        super.handleMessage(message);
    }
}
