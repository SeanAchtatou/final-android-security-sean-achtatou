package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1cZ  reason: invalid class name and case insensitive filesystem */
public final class C27051cZ implements C09060gT {
    private static volatile C27051cZ A01;
    private AnonymousClass0UN A00;

    public void AYE(Integer num, String str, String str2) {
        AYF(num, str, str2, 0);
    }

    public static final C27051cZ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C27051cZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C27051cZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C27051cZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public void AYF(Integer num, String str, String str2, int i) {
        boolean z = true;
        switch (num.intValue()) {
            case 0:
                C010708t.A0J(AnonymousClass08S.A0J("Litho:", str), str2);
                return;
            case 1:
            case 2:
                AnonymousClass06G A02 = AnonymousClass06F.A02(AnonymousClass08S.A0J("Litho:", str), str2);
                if (num != AnonymousClass07B.A0C) {
                    z = false;
                }
                A02.A04 = z;
                if (i > 0) {
                    A02.A00 = i;
                }
                ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, this.A00)).CGQ(A02.A00());
                return;
            default:
                return;
        }
    }
}
