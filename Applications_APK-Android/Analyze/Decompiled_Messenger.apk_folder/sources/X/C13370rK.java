package X;

import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.View;

/* renamed from: X.0rK  reason: invalid class name and case insensitive filesystem */
public class C13370rK {
    private int A00;
    public final C26902DGw A01;

    public C13370rK A00(int i, DialogInterface.OnClickListener onClickListener) {
        C26902DGw dGw = this.A01;
        dGw.A0I = dGw.A0T.getText(i);
        this.A01.A06 = onClickListener;
        return this;
    }

    public C13370rK A01(int i, DialogInterface.OnClickListener onClickListener) {
        C26902DGw dGw = this.A01;
        dGw.A0J = dGw.A0T.getText(i);
        this.A01.A07 = onClickListener;
        return this;
    }

    public C13370rK A02(int i, DialogInterface.OnClickListener onClickListener) {
        C26902DGw dGw = this.A01;
        dGw.A0K = dGw.A0T.getText(i);
        this.A01.A09 = onClickListener;
        return this;
    }

    public C13370rK A03(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        C26902DGw dGw = this.A01;
        dGw.A0I = charSequence;
        dGw.A06 = onClickListener;
        return this;
    }

    public C13370rK A04(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        C26902DGw dGw = this.A01;
        dGw.A0J = charSequence;
        dGw.A07 = onClickListener;
        return this;
    }

    public C13370rK A05(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        C26902DGw dGw = this.A01;
        dGw.A0K = charSequence;
        dGw.A09 = onClickListener;
        return this;
    }

    public AnonymousClass3At A06() {
        AnonymousClass3At r2 = new AnonymousClass3At(this.A01.A0T, this.A00);
        this.A01.A00(r2.A00);
        r2.setCancelable(this.A01.A0M);
        if (this.A01.A0M) {
            r2.setCanceledOnTouchOutside(true);
        }
        r2.setOnCancelListener(this.A01.A05);
        r2.setOnDismissListener(this.A01.A0A);
        return r2;
    }

    public void A08(int i) {
        C26902DGw dGw = this.A01;
        dGw.A0H = dGw.A0T.getText(i);
    }

    public void A09(int i) {
        C26902DGw dGw = this.A01;
        dGw.A0L = dGw.A0T.getText(i);
    }

    public void A0A(View view) {
        C26902DGw dGw = this.A01;
        dGw.A0F = view;
        dGw.A0Q = false;
    }

    public void A0B(View view, int i, int i2, int i3, int i4) {
        C26902DGw dGw = this.A01;
        dGw.A0F = view;
        dGw.A0Q = true;
        dGw.A02 = i;
        dGw.A04 = i2;
        dGw.A03 = i3;
        dGw.A01 = i4;
    }

    public void A0C(CharSequence charSequence) {
        this.A01.A0H = charSequence;
    }

    public void A0D(CharSequence charSequence) {
        this.A01.A0L = charSequence;
    }

    public void A0E(boolean z) {
        this.A01.A0M = z;
    }

    public void A0F(CharSequence[] charSequenceArr, int i, DialogInterface.OnClickListener onClickListener) {
        C26902DGw dGw = this.A01;
        dGw.A0R = charSequenceArr;
        dGw.A08 = onClickListener;
        dGw.A00 = i;
        dGw.A0P = true;
    }

    public AnonymousClass3At A07() {
        AnonymousClass3At A06 = A06();
        try {
            A06.show();
            return A06;
        } catch (Throwable unused) {
            return A06;
        }
    }

    public C13370rK(Context context) {
        this(context, AnonymousClass3At.A00(context, 0));
    }

    public C13370rK(Context context, int i) {
        this.A01 = new C26902DGw(new ContextThemeWrapper(context, AnonymousClass3At.A00(context, i)));
        this.A00 = i;
    }
}
