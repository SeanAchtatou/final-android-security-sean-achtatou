package X;

import android.database.Observable;
import com.facebook.quicklog.PerformanceLoggingEvent;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0el  reason: invalid class name and case insensitive filesystem */
public final class C08150el extends Observable implements AnonymousClass0Td, C26121aw {
    private static C08150el A02;
    public QuickPerformanceLogger A00 = null;
    public final C25531Zz A01;

    public void BeL(C04270Tg r1, String str, String str2) {
    }

    public void BeQ(C04270Tg r1, String str, AnonymousClass0lr r3, long j, boolean z, int i) {
    }

    public void BeV(int i, int i2, C04270Tg r3) {
    }

    public void Bkk(int i, int i2) {
    }

    public boolean Bkl(int i, int i2) {
        return false;
    }

    public void CB4(QuickPerformanceLogger quickPerformanceLogger) {
    }

    public static C08150el create(C25531Zz r1) {
        if (A02 == null) {
            A02 = new C08150el(r1);
        }
        return A02;
    }

    public static C08150el getInstance() {
        return A02;
    }

    public AnonymousClass0Ti AsW() {
        ArrayList arrayList = this.mObservers;
        if (arrayList != null) {
            synchronized (arrayList) {
                if (!this.mObservers.isEmpty()) {
                    AnonymousClass0Ti r0 = AnonymousClass0Ti.A03;
                    return r0;
                }
            }
        }
        return AnonymousClass0Ti.A04;
    }

    public void BeF(C04270Tg r4) {
        ArrayList arrayList = this.mObservers;
        if (arrayList != null && !arrayList.isEmpty()) {
            synchronized (this.mObservers) {
                Iterator it = this.mObservers.iterator();
                while (it.hasNext()) {
                    ((BJ7) it.next()).BeF(r4);
                }
            }
        }
    }

    public void BeM(C04270Tg r4) {
        ArrayList arrayList;
        if (r4 != null && (arrayList = this.mObservers) != null && arrayList.size() != 0) {
            synchronized (this.mObservers) {
                Iterator it = this.mObservers.iterator();
                while (it.hasNext()) {
                    ((BJ7) it.next()).BeM(r4);
                }
            }
        }
    }

    public void BeR(C04270Tg r4) {
        ArrayList arrayList;
        if (r4 != null && (arrayList = this.mObservers) != null && arrayList.size() != 0) {
            synchronized (this.mObservers) {
                Iterator it = this.mObservers.iterator();
                while (it.hasNext()) {
                    ((BJ7) it.next()).BeR(r4);
                }
            }
        }
    }

    public void BeT(C04270Tg r4) {
        ArrayList arrayList;
        if (r4 != null && (arrayList = this.mObservers) != null && arrayList.size() != 0) {
            C010708t.A0P("QuickPerformanceLoggerImpl", "QPL markerStart - %d", Integer.valueOf(r4.A02));
            synchronized (this.mObservers) {
                Iterator it = this.mObservers.iterator();
                while (it.hasNext()) {
                    ((BJ7) it.next()).BeT(r4);
                }
            }
        }
    }

    public void BeU(C04270Tg r4) {
        ArrayList arrayList;
        if (r4 != null && (arrayList = this.mObservers) != null && arrayList.size() != 0) {
            synchronized (this.mObservers) {
                Iterator it = this.mObservers.iterator();
                while (it.hasNext()) {
                    ((BJ7) it.next()).BeU(r4);
                }
            }
        }
    }

    public void BfD(PerformanceLoggingEvent performanceLoggingEvent) {
        ArrayList arrayList = this.mObservers;
        if (arrayList != null && !arrayList.isEmpty()) {
            synchronized (this.mObservers) {
                Iterator it = this.mObservers.iterator();
                while (it.hasNext()) {
                    ((BJ7) it.next()).BfD(performanceLoggingEvent);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ce, code lost:
        if (r3.A02 == -1) goto L_0x00d0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void CMc(com.facebook.quicklog.PerformanceLoggingEvent r11) {
        /*
            r10 = this;
            X.1Zz r0 = r10.A01
            boolean r0 = r0.BGR()
            if (r0 != 0) goto L_0x0013
            java.util.ArrayList r0 = r10.mObservers
            if (r0 == 0) goto L_0x0012
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0013
        L_0x0012:
            return
        L_0x0013:
            java.lang.String r9 = r11.A0M
            if (r9 != 0) goto L_0x001d
            int r0 = r11.A06
            java.lang.String r9 = X.C03020Ho.A00(r0)
        L_0x001d:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            r5 = 0
            r4 = 0
            java.util.List r0 = r11.A01()
            java.util.Iterator r3 = r0.iterator()
        L_0x002c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0044
            java.lang.Object r1 = r3.next()
            java.lang.String r1 = (java.lang.String) r1
            int r5 = r5 + 1
            int r0 = r5 % 2
            if (r0 != 0) goto L_0x0042
            r2.put(r4, r1)
            goto L_0x002c
        L_0x0042:
            r4 = r1
            goto L_0x002c
        L_0x0044:
            java.util.ArrayList r0 = r11.A0P
            if (r0 == 0) goto L_0x0051
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = "trace_tags"
            r2.put(r0, r1)
        L_0x0051:
            X.00T r3 = r11.A0F
            if (r3 == 0) goto L_0x0117
            boolean r0 = r3.A0E
            if (r0 == 0) goto L_0x0117
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r3.A0C
            int r0 = r0.A00
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "class_load_attempts"
            r2.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r3.A0C
            int r0 = r0.A01
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "class_loads_failed"
            r2.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r3.A0C
            int r0 = r0.A04
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "locator_assists"
            r2.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r3.A0C
            int r0 = r0.A03
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "wrong_dfa_guesses"
            r2.put(r0, r1)
            com.facebook.common.dextricks.stats.ClassLoadingStats$SnapshotStats r0 = r3.A0C
            int r0 = r0.A02
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "dex_queries"
            r2.put(r0, r1)
            int r0 = r3.A00
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "start_pri"
            r2.put(r0, r1)
            int r0 = r3.A01
            java.lang.String r1 = java.lang.Integer.toString(r0)
            java.lang.String r0 = "stop_pri"
            r2.put(r0, r1)
            long r0 = r3.A06
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "ps_cpu_ms"
            r2.put(r0, r1)
            long r0 = r3.A07
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "ps_flt"
            r2.put(r0, r1)
            boolean r0 = r3.A0E
            if (r0 == 0) goto L_0x00d0
            int r4 = r3.A02
            r1 = -1
            r0 = 1
            if (r4 != r1) goto L_0x00d1
        L_0x00d0:
            r0 = 0
        L_0x00d1:
            if (r0 == 0) goto L_0x00e9
            long r0 = r3.A09
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "th_cpu_ms"
            r2.put(r0, r1)
            long r0 = r3.A0A
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "th_flt"
            r2.put(r0, r1)
        L_0x00e9:
            long r0 = r3.A03
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "allocstall"
            r2.put(r0, r1)
            long r0 = r3.A04
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "pages_in"
            r2.put(r0, r1)
            long r0 = r3.A05
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "pages_out"
            r2.put(r0, r1)
            long r0 = r3.A01()
            java.lang.String r1 = java.lang.Long.toString(r0)
            java.lang.String r0 = "avail_disk_spc_kb"
            r2.put(r0, r1)
        L_0x0117:
            java.lang.String r8 = r2.toString()
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.NANOSECONDS
            long r0 = r11.A0B
            long r3 = r2.toMillis(r0)
            long r0 = r11.A08
            long r0 = r2.toMillis(r0)
            int r2 = (int) r0
            long r1 = (long) r2
            short r0 = r11.A0R
            java.lang.String r7 = X.AnonymousClass0I1.A00(r0)
            java.lang.String r6 = "QuickPerformanceLoggerImpl"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r0 = 128(0x80, float:1.794E-43)
            r5.<init>(r0)
            java.lang.String r0 = "Name: "
            r5.append(r0)
            r5.append(r9)
            java.lang.String r0 = "; Params: "
            r5.append(r0)
            r5.append(r8)
            java.lang.String r0 = "; Monotonic Timestamp (ms): "
            r5.append(r0)
            r5.append(r3)
            java.lang.String r0 = "; Elapsed (ms): "
            r5.append(r0)
            r5.append(r1)
            java.lang.String r0 = "; Action: "
            r5.append(r0)
            r5.append(r7)
            java.lang.String r0 = r5.toString()
            X.C010708t.A0J(r6, r0)
            if (r11 == 0) goto L_0x018b
            java.util.ArrayList r2 = r10.mObservers
            if (r2 == 0) goto L_0x018b
            monitor-enter(r2)
            java.util.ArrayList r0 = r10.mObservers     // Catch:{ all -> 0x0188 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0188 }
        L_0x0176:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0188 }
            if (r0 == 0) goto L_0x0186
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0188 }
            X.BJ7 r0 = (X.BJ7) r0     // Catch:{ all -> 0x0188 }
            r0.onPerformanceLoggingEvent(r11)     // Catch:{ all -> 0x0188 }
            goto L_0x0176
        L_0x0186:
            monitor-exit(r2)     // Catch:{ all -> 0x0188 }
            return
        L_0x0188:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0188 }
            throw r0
        L_0x018b:
            java.lang.Class<X.0el> r1 = X.C08150el.class
            java.lang.String r0 = "performanceLoggingEvent/mObservers cannot be null."
            X.C010708t.A06(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08150el.CMc(com.facebook.quicklog.PerformanceLoggingEvent):void");
    }

    public void dummy() {
        super.registerObserver(new BJ7());
        QuickPerformanceLogger quickPerformanceLogger = this.A00;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.updateListenerMarkers();
        }
    }

    public /* bridge */ /* synthetic */ void registerObserver(Object obj) {
        super.registerObserver((BJ7) obj);
        QuickPerformanceLogger quickPerformanceLogger = this.A00;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.updateListenerMarkers();
        }
    }

    public void unregisterObserver(Object obj) {
        super.unregisterObserver((BJ7) obj);
        QuickPerformanceLogger quickPerformanceLogger = this.A00;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.updateListenerMarkers();
        }
    }

    private C08150el(C25531Zz r2) {
        this.A01 = r2;
    }
}
