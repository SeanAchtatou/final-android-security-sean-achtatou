package X;

import android.os.Build;
import android.util.SparseArray;
import com.facebook.litho.ComponentHost;

/* renamed from: X.0zH  reason: invalid class name and case insensitive filesystem */
public final class C17670zH extends C17770zR {
    public SparseArray A00;

    public int A0M() {
        return 45;
    }

    public C17670zH() {
        super("HostComponent");
    }

    public C15690vh A0R() {
        if (AnonymousClass07c.disableComponentHostPool) {
            return new C35621rW();
        }
        return super.A0R();
    }

    public void A0j(AnonymousClass0p4 r3, Object obj) {
        ComponentHost componentHost = (ComponentHost) obj;
        if (Build.VERSION.SDK_INT >= 11) {
            componentHost.setAlpha(1.0f);
        }
    }
}
