package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.resources.impl.loading.LanguagePackInfo;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0kr  reason: invalid class name and case insensitive filesystem */
public final class C10790kr implements CallerContextable {
    private static volatile C10790kr A02 = null;
    public static final String __redex_internal_original_name = "com.facebook.resources.impl.loading.LanguagePackDownloader";
    public AnonymousClass0UN A00;
    public final String A01 = AnonymousClass08S.A0J("i18n", C10790kr.class.getName());

    public static final C10790kr A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C10790kr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C10790kr(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static LanguagePackInfo A01(C10790kr r4, C09770if r5) {
        int i = AnonymousClass1Y3.B9i;
        AnonymousClass0UN r2 = r4.A00;
        return (LanguagePackInfo) ((C10740km) AnonymousClass1XX.A02(2, i, r2)).A06((C191158ux) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AuA, r2), new C191168uy(r5), CallerContext.A04(r4.getClass()));
    }

    private C10790kr(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(10, r3);
    }
}
