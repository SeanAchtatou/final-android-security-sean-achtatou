package X;

import android.content.Context;
import android.os.PowerManager;
import com.facebook.common.connectionstatus.FbDataConnectionManager;
import com.facebook.common.dextricks.DexLibLoader;
import com.facebook.common.perftest.base.PerfTestConfigBase;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.mmapbuf.MmapBufferManager;
import com.google.common.base.Charsets;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.09W  reason: invalid class name */
public final class AnonymousClass09W extends AnonymousClass04v {
    private C05550Zj A00;
    private AnonymousClass0UN A01;

    private String A01() {
        C05550Zj r0;
        String str;
        synchronized (this) {
            if (this.A00 == null) {
                try {
                    File file = new File(((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, this.A01)).getFilesDir(), "mobilelab_test_info");
                    if (!file.exists() || !file.canRead()) {
                        C010708t.A0P("MobileLabTestInfo", "File %s does not exist or can not be read", file.getPath());
                        r0 = new C05550Zj(null);
                        this.A00 = r0;
                    } else {
                        try {
                            str = AnonymousClass0q2.A01(file, Charsets.UTF_8);
                        } catch (IOException e) {
                            C010708t.A0R("MobileLabTestInfo", e, "Failed to read mobile lab test info.");
                            str = "{}";
                        }
                        r0 = new C05550Zj(str);
                        this.A00 = r0;
                    }
                } catch (SecurityException e2) {
                    C010708t.A0R("MobileLabTestInfo", e2, "Failed to check file existance.");
                    r0 = new C05550Zj(null);
                }
            }
        }
        return this.A00.A00;
    }

    public static final AnonymousClass09W A00(AnonymousClass1XY r1) {
        return new AnonymousClass09W(r1);
    }

    private void A02() {
        long j;
        if (!DexLibLoader.deoptTaint) {
            j = 1;
        } else {
            j = 0;
        }
        Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126475, 0, j);
        int i = AnonymousClass1Y3.B3e;
        AnonymousClass0UN r4 = this.A01;
        FbDataConnectionManager fbDataConnectionManager = (FbDataConnectionManager) AnonymousClass1XX.A02(2, i, r4);
        C09340h3 r5 = (C09340h3) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AeN, r4);
        HashMap hashMap = new HashMap();
        if (fbDataConnectionManager != null) {
            hashMap.put("connection_class", fbDataConnectionManager.A06().name());
        }
        if (r5 != null) {
            hashMap.put("network_type", r5.A0K());
            hashMap.put("network_subtype", r5.A0J());
        }
        A03(hashMap, "connection_class", 8126484);
        A03(hashMap, "network_type", 8126485);
        A03(hashMap, "network_subtype", 8126486);
        A03(C163077gR.A00((PowerManager) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A8E, this.A01)), "low_power_state", 9240611);
        C012109i A012 = C012109i.A01();
        HashMap hashMap2 = new HashMap();
        if (A012 != null) {
            long A06 = A012.A06(AnonymousClass07B.A00);
            long A07 = A012.A07(AnonymousClass07B.A00);
            if (A06 >= 0 && A07 > 0) {
                hashMap2.put("free_disk_percent", Integer.valueOf((int) ((A06 * 100) / A07)));
            }
        }
        A03(hashMap2, "free_disk_percent", 8126497);
        int i2 = AnonymousClass1Y3.AiY;
        AnonymousClass0UN r42 = this.A01;
        BOO boo = (BOO) AnonymousClass1XX.A02(7, i2, r42);
        if (boo != null) {
            Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126517, 0, boo.getReactBundleVersion((Context) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCt, r42)));
        }
        String str = PerfTestConfigBase.A00;
        if (str == null) {
            str = A01();
        }
        if (str != null) {
            Logger.writeBytesEntry(0, 1, 57, Logger.writeBytesEntry(0, 1, 56, Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126489, 0, 0), "PERF_TEST_INFO"), str);
        }
    }

    private static void A03(Map map, String str, int i) {
        Object obj;
        String obj2;
        if (str != null && map.containsKey(str) && (obj = map.get(str)) != null && (obj2 = obj.toString()) != null) {
            int writeStandardEntry = Logger.writeStandardEntry(0, 7, 52, 0, 0, i, 0, 0);
            if (str != null) {
                writeStandardEntry = Logger.writeBytesEntry(0, 1, 56, writeStandardEntry, str);
            }
            Logger.writeBytesEntry(0, 1, 57, writeStandardEntry, obj2);
        }
    }

    public void Bds(Throwable th) {
        ((AnonymousClass09P) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Amr, this.A01)).CGa("profilo-handled-exception", th);
    }

    public void Bsa(File file, long j) {
        ((AnonymousClass06R) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AUk, this.A01)).A0D(AnonymousClass07B.A00);
    }

    public void Btg(File file) {
        ((AnonymousClass06R) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AUk, this.A01)).A0D(AnonymousClass07B.A0C);
    }

    public void Bth(File file) {
        ((AnonymousClass06R) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AUk, this.A01)).A0D(AnonymousClass07B.A01);
    }

    public void onTraceAbort(TraceContext traceContext) {
        ((AnonymousClass06R) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AUk, this.A01)).A0E(false, traceContext);
        AnonymousClass08Z.A00 = 0;
    }

    private AnonymousClass09W(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(8, r3);
        String A05 = AnonymousClass01P.A05();
        MmapBufferManager mmapBufferManager = Logger.sMmapBufferManager;
        if (mmapBufferManager != null) {
            mmapBufferManager.nativeUpdateSessionId(A05);
        }
    }

    public void onTraceStart(TraceContext traceContext) {
        String A05 = AnonymousClass01P.A05();
        MmapBufferManager mmapBufferManager = Logger.sMmapBufferManager;
        if (mmapBufferManager != null) {
            mmapBufferManager.nativeUpdateSessionId(A05);
        }
        ((AnonymousClass06R) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AUk, this.A01)).A0E(true, traceContext);
    }

    public void onTraceStop(TraceContext traceContext) {
        A02();
        ((AnonymousClass06R) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AUk, this.A01)).A0E(false, traceContext);
    }
}
