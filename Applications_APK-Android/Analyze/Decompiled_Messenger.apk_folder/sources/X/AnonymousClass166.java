package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.166  reason: invalid class name */
public final class AnonymousClass166 {
    private static volatile AnonymousClass166 A03;
    public Boolean A00 = null;
    private Boolean A01 = null;
    public final C05250Yf A02;

    public static final AnonymousClass166 A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass166.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass166(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public boolean A01() {
        if (this.A01 == null) {
            this.A01 = Boolean.valueOf(this.A02.A00.AbO(95, false));
        }
        return this.A01.booleanValue();
    }

    private AnonymousClass166(AnonymousClass1XY r2) {
        this.A02 = C05250Yf.A00(r2);
    }
}
