package X;

/* renamed from: X.02D  reason: invalid class name */
public final class AnonymousClass02D {
    private static final String[] A00 = {"VmPeak:", "VmSize:", "VmHWM:", "VmRSS:"};

    public static AnonymousClass022 A00() {
        AnonymousClass022 r3 = new AnonymousClass022();
        String[] strArr = A00;
        long[] jArr = new long[strArr.length];
        AnonymousClass00V.A02("/proc/self/status", strArr, jArr);
        r3.A01 = jArr[0];
        r3.A00 = jArr[1];
        r3.A03 = jArr[2];
        r3.A02 = jArr[3];
        return r3;
    }
}
