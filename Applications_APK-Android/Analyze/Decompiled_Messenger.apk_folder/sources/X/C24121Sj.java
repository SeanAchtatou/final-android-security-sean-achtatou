package X;

import android.graphics.Bitmap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Sj  reason: invalid class name and case insensitive filesystem */
public final class C24121Sj {
    private static volatile C24121Sj A02;
    public AnonymousClass0UN A00;
    private final C21401Gu A01;

    public boolean A03(C21381Gs r4, AnonymousClass1Q0 r5) {
        boolean z;
        if (r5 != null) {
            if (r4 != C21381Gs.A0R) {
                z = true;
            } else {
                AnonymousClass1XX.A03(AnonymousClass1Y3.A11, this.A00);
                z = false;
            }
            if (!z || !C006206h.A05(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public static Bitmap A00(AnonymousClass1S9 r1) {
        AnonymousClass1PS A002;
        if (r1 instanceof AnonymousClass1S6) {
            return ((AnonymousClass1S6) r1).A04();
        }
        if (!(r1 instanceof C33291nL)) {
            return null;
        }
        C80973tY A04 = ((C33291nL) r1).A04();
        synchronized (A04) {
            A002 = AnonymousClass1PS.A00(A04.A00);
        }
        if (A002 != null) {
            return (Bitmap) A002.A0A();
        }
        return null;
    }

    public static final C24121Sj A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C24121Sj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C24121Sj(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public int A02(AnonymousClass1H4 r3) {
        if (r3 == null) {
            return 0;
        }
        if (r3.B6D() != C21381Gs.A0R) {
            return r3.B6P();
        }
        C21401Gu r0 = this.A01;
        int B6P = r3.B6P();
        if (B6P == 0) {
            B6P = r0.A01();
        }
        return C012609n.A01(B6P, 0.73f);
    }

    private C24121Sj(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = C21401Gu.A00(r3);
    }
}
