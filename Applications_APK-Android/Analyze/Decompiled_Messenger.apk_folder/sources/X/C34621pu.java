package X;

import android.util.BoostFramework;

/* renamed from: X.1pu  reason: invalid class name and case insensitive filesystem */
public final class C34621pu extends C34631pv {
    private static boolean A01 = true;
    private static boolean A02;
    private final BoostFramework A00;

    public void A03() {
        this.A00.perfLockRelease();
    }

    public final boolean A06() {
        int i;
        if (!A01) {
            return false;
        }
        try {
            if (A02) {
                i = 500;
            } else {
                i = this.A03;
            }
            if (this.A00.perfLockAcquire(i, this.A00) < 0) {
                A02 = true;
                return false;
            }
            A02 = false;
            return true;
        } catch (Error unused) {
            A01 = false;
            return false;
        }
    }

    public C34621pu(BoostFramework boostFramework, int i, int[] iArr) {
        super(i, iArr);
        this.A00 = boostFramework;
    }
}
