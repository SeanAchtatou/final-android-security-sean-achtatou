package X;

import android.animation.ValueAnimator;

/* renamed from: X.0vc  reason: invalid class name and case insensitive filesystem */
public final class C15640vc implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ C15630vb A00;
    public final /* synthetic */ C15160up A01;

    public C15640vc(C15160up r1, C15630vb r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        C15160up r2 = this.A01;
        C15630vb r1 = this.A00;
        r2.A01(floatValue, r1);
        r2.A02(floatValue, r1, false);
        this.A01.invalidateSelf();
    }
}
