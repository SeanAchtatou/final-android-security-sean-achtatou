package X;

/* renamed from: X.1jq  reason: invalid class name and case insensitive filesystem */
public final class C31531jq {
    private final AnonymousClass09P A00;

    public static final C31531jq A00(AnonymousClass1XY r1) {
        return new C31531jq(r1);
    }

    public boolean A01() {
        try {
            AnonymousClass01q.A08("fb");
            AnonymousClass01q.A08("liger");
            return true;
        } catch (Throwable th) {
            this.A00.softReport("Liger.loadLibrary", th.getMessage(), th);
            return false;
        }
    }

    public C31531jq(AnonymousClass1XY r2) {
        this.A00 = C04750Wa.A01(r2);
    }
}
