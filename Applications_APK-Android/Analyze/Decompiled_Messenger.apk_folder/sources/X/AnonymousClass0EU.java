package X;

import java.util.zip.ZipEntry;

/* renamed from: X.0EU  reason: invalid class name */
public final class AnonymousClass0EU extends AnonymousClass0EV implements Comparable {
    public final int A00;
    public final ZipEntry A01;

    public int compareTo(Object obj) {
        return this.A01.compareTo(((AnonymousClass0EU) obj).A01);
    }

    public AnonymousClass0EU(String str, ZipEntry zipEntry, int i) {
        super(str, String.format("pseudo-zip-hash-1-%s-%s-%s-%s", zipEntry.getName(), Long.valueOf(zipEntry.getSize()), Long.valueOf(zipEntry.getCompressedSize()), Long.valueOf(zipEntry.getCrc())));
        this.A01 = zipEntry;
        this.A00 = i;
    }
}
