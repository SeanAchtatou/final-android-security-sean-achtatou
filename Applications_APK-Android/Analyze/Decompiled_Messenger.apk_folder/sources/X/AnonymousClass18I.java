package X;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import java.util.Arrays;

/* renamed from: X.18I  reason: invalid class name */
public class AnonymousClass18I extends AnonymousClass18G {
    public final C30481i7 A00;
    public final Character A01;
    public transient AnonymousClass18G A02;

    public AnonymousClass18G A09(C30481i7 r2, Character ch) {
        if (this instanceof AnonymousClass18H) {
            return new AnonymousClass18H(r2, ch);
        }
        if (!(this instanceof AnonymousClass18L)) {
            return new AnonymousClass18I(r2, ch);
        }
        return new AnonymousClass18L(r2);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass18I)) {
            return false;
        }
        AnonymousClass18I r4 = (AnonymousClass18I) obj;
        if (!this.A00.equals(r4.A00) || !Objects.equal(this.A01, r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A00.hashCode() ^ Arrays.hashCode(new Object[]{this.A01});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BaseEncoding.");
        C30481i7 r1 = this.A00;
        sb.append(r1.toString());
        if (8 % r1.A01 != 0) {
            Character ch = this.A01;
            if (ch == null) {
                sb.append(".omitPadding()");
            } else {
                sb.append(".withPadChar('");
                sb.append(ch);
                sb.append("')");
            }
        }
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r3.matches(r4.charValue()) == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass18I(X.C30481i7 r3, java.lang.Character r4) {
        /*
            r2 = this;
            r2.<init>()
            com.google.common.base.Preconditions.checkNotNull(r3)
            r2.A00 = r3
            if (r4 == 0) goto L_0x0015
            char r0 = r4.charValue()
            boolean r0 = r3.matches(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0016
        L_0x0015:
            r1 = 1
        L_0x0016:
            java.lang.String r0 = "Padding character %s was already in alphabet"
            com.google.common.base.Preconditions.checkArgument(r1, r0, r4)
            r2.A01 = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18I.<init>(X.1i7, java.lang.Character):void");
    }

    public void A0A(Appendable appendable, byte[] bArr, int i, int i2) {
        Preconditions.checkNotNull(appendable);
        Preconditions.checkPositionIndexes(i, i + i2, bArr.length);
        int i3 = 0;
        boolean z = false;
        if (i2 <= this.A00.A02) {
            z = true;
        }
        Preconditions.checkArgument(z);
        long j = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            j = (j | ((long) (bArr[i + i4] & 255))) << 8;
        }
        int i5 = ((i2 + 1) << 3) - this.A00.A01;
        while (i3 < (i2 << 3)) {
            C30481i7 r1 = this.A00;
            appendable.append(r1.A05[((int) (j >>> (i5 - i3))) & r1.A00]);
            i3 += this.A00.A01;
        }
        if (this.A01 != null) {
            while (i3 < (this.A00.A02 << 3)) {
                appendable.append(this.A01.charValue());
                i3 += this.A00.A01;
            }
        }
    }
}
