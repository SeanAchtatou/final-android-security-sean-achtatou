package X;

import android.content.Context;

/* renamed from: X.1Qx  reason: invalid class name and case insensitive filesystem */
public final class C23801Qx extends C20831Dz {
    public final Context A00;
    public final C134246Pv A01;

    public int ArU() {
        return this.A01.A03();
    }

    public C23801Qx(Context context, C134246Pv r2) {
        this.A00 = context;
        this.A01 = r2;
    }
}
