package X;

import android.util.SparseIntArray;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1Zr  reason: invalid class name and case insensitive filesystem */
public final class C25451Zr {
    public final int A00;
    public final SparseIntArray A01;
    public final SparseIntArray A02;

    public static void A02(ObjectOutputStream objectOutputStream, SparseIntArray sparseIntArray) {
        if (sparseIntArray == null) {
            objectOutputStream.writeInt(0);
            return;
        }
        int size = sparseIntArray.size();
        objectOutputStream.writeInt(size);
        for (int i = 0; i < size; i++) {
            int keyAt = sparseIntArray.keyAt(i);
            int valueAt = sparseIntArray.valueAt(i);
            objectOutputStream.writeInt(keyAt);
            objectOutputStream.writeInt(valueAt);
        }
    }

    public int A03(int i) {
        int i2;
        int i3;
        SparseIntArray sparseIntArray = this.A01;
        if (sparseIntArray != null && (i3 = sparseIntArray.get(i, -1)) != -1) {
            return i3;
        }
        SparseIntArray sparseIntArray2 = this.A02;
        if (sparseIntArray2 == null || (i2 = sparseIntArray2.get((short) (i >> 16), -1)) == -1) {
            return this.A00;
        }
        return i2;
    }

    public C25451Zr(int i, SparseIntArray sparseIntArray, SparseIntArray sparseIntArray2) {
        this.A00 = i;
        this.A02 = sparseIntArray;
        this.A01 = sparseIntArray2;
    }

    private static SparseIntArray A00(ObjectInputStream objectInputStream) {
        int readInt = objectInputStream.readInt();
        if (readInt <= 0) {
            return null;
        }
        SparseIntArray sparseIntArray = new SparseIntArray(readInt);
        for (int i = 0; i < readInt; i++) {
            sparseIntArray.put(objectInputStream.readInt(), objectInputStream.readInt());
        }
        return sparseIntArray;
    }

    public static C25451Zr A01(ObjectInputStream objectInputStream, AtomicReference atomicReference) {
        int readInt = objectInputStream.readInt();
        if (atomicReference.get() == null) {
            SparseIntArray A002 = A00(objectInputStream);
            if (atomicReference.get() == null) {
                return new C25451Zr(readInt, A002, A00(objectInputStream));
            }
        }
        return null;
    }
}
