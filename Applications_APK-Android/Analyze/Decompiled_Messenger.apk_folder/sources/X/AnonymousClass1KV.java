package X;

import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1KV  reason: invalid class name */
public final class AnonymousClass1KV {
    public AnonymousClass0UN A00;

    public static final AnonymousClass1KV A01(AnonymousClass1XY r1) {
        return new AnonymousClass1KV(r1);
    }

    public int A02() {
        return ((MigColorScheme) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Anh, this.A00)).B5U().AhV();
    }

    public AnonymousClass1KV(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public static int A00(MigColorScheme migColorScheme) {
        return AnonymousClass1KW.A00(AnonymousClass1KA.A00(migColorScheme.AfS()), migColorScheme);
    }
}
