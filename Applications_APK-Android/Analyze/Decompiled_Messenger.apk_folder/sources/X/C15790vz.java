package X;

/* renamed from: X.0vz  reason: invalid class name and case insensitive filesystem */
public final class C15790vz {
    public AnonymousClass0UN A00;
    public final C13480rV A01;
    public final C22431Lh A02;

    public static final C15790vz A00(AnonymousClass1XY r1) {
        return new C15790vz(r1);
    }

    private C15790vz(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A02 = C22431Lh.A00(r3);
        AnonymousClass0WU.A01(r3);
        this.A01 = C13480rV.A00(r3);
    }
}
