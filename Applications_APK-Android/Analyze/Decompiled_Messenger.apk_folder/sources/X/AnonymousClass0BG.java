package X;

import android.os.Build;
import com.facebook.proxygen.TraceFieldType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0BG  reason: invalid class name */
public final class AnonymousClass0BG {
    public List A00 = new ArrayList();
    private final int A01;
    private final AnonymousClass0B0 A02;
    private final String A03;
    private final TreeSet A04 = new TreeSet(new AnonymousClass0BH());

    public synchronized AnonymousClass0Ry A00(AnonymousClass0Ry r4) {
        Iterator it = A01().iterator();
        while (it.hasNext()) {
            AnonymousClass0Ry r1 = (AnonymousClass0Ry) it.next();
            if (r1.equals(r4)) {
                return r1;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a4, code lost:
        if (r4.A00().isEmpty() != false) goto L_0x00a6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.TreeSet A01() {
        /*
            r7 = this;
            monitor-enter(r7)
            java.util.TreeSet r0 = r7.A04     // Catch:{ all -> 0x00bb }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00bb }
            if (r0 == 0) goto L_0x00b7
            X.0B0 r1 = r7.A02     // Catch:{ all -> 0x00bb }
            if (r1 == 0) goto L_0x00b7
            java.lang.String r0 = r7.A03     // Catch:{ all -> 0x00bb }
            boolean r0 = r1.contains(r0)     // Catch:{ all -> 0x00bb }
            if (r0 == 0) goto L_0x00b7
            X.0B0 r2 = r7.A02     // Catch:{ all -> 0x00bb }
            java.lang.String r1 = r7.A03     // Catch:{ all -> 0x00bb }
            java.lang.String r0 = ""
            java.lang.String r0 = r2.getString(r1, r0)     // Catch:{ all -> 0x00bb }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00af }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x00af }
            java.lang.String r0 = "address_entries"
            org.json.JSONArray r6 = r1.optJSONArray(r0)     // Catch:{ JSONException -> 0x00af }
            if (r6 == 0) goto L_0x00b7
            int r0 = r6.length()     // Catch:{ JSONException -> 0x00af }
            if (r0 <= 0) goto L_0x00b7
            r5 = 0
        L_0x0033:
            int r0 = r6.length()     // Catch:{ JSONException -> 0x00af }
            if (r5 >= r0) goto L_0x00b7
            java.lang.String r2 = r6.getString(r5)     // Catch:{ JSONException -> 0x00af }
            boolean r0 = android.text.TextUtils.isEmpty(r2)     // Catch:{ JSONException -> 0x00af }
            if (r0 == 0) goto L_0x0045
            r4 = 0
            goto L_0x0091
        L_0x0045:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00af }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x00af }
            X.0Ry r4 = new X.0Ry     // Catch:{ JSONException -> 0x00af }
            r4.<init>()     // Catch:{ JSONException -> 0x00af }
            java.lang.String r0 = "host_name"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x00af }
            r4.A02 = r0     // Catch:{ JSONException -> 0x00af }
            java.lang.String r0 = "priority"
            int r0 = r1.optInt(r0)     // Catch:{ JSONException -> 0x00af }
            r4.A01 = r0     // Catch:{ JSONException -> 0x00af }
            java.lang.String r0 = "fail_count"
            int r0 = r1.optInt(r0)     // Catch:{ JSONException -> 0x00af }
            r4.A00 = r0     // Catch:{ JSONException -> 0x00af }
            java.lang.String r0 = "address_list_data"
            org.json.JSONArray r3 = r1.optJSONArray(r0)     // Catch:{ JSONException -> 0x00af }
            if (r3 == 0) goto L_0x0091
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ JSONException -> 0x00af }
            int r0 = r3.length()     // Catch:{ JSONException -> 0x00af }
            r2.<init>(r0)     // Catch:{ JSONException -> 0x00af }
            r1 = 0
        L_0x0079:
            int r0 = r3.length()     // Catch:{ JSONException -> 0x00af }
            if (r1 >= r0) goto L_0x008f
            boolean r0 = r3.isNull(r1)     // Catch:{ JSONException -> 0x00af }
            if (r0 != 0) goto L_0x008c
            java.lang.String r0 = r3.getString(r1)     // Catch:{ JSONException -> 0x00af }
            r2.add(r0)     // Catch:{ JSONException -> 0x00af }
        L_0x008c:
            int r1 = r1 + 1
            goto L_0x0079
        L_0x008f:
            r4.A04 = r2     // Catch:{ JSONException -> 0x00af }
        L_0x0091:
            java.util.List r0 = r4.A04     // Catch:{ JSONException -> 0x00af }
            if (r0 == 0) goto L_0x00a6
            boolean r0 = r0.isEmpty()     // Catch:{ JSONException -> 0x00af }
            if (r0 != 0) goto L_0x00a6
            java.util.List r0 = r4.A00()     // Catch:{ JSONException -> 0x00af }
            boolean r1 = r0.isEmpty()     // Catch:{ JSONException -> 0x00af }
            r0 = 1
            if (r1 == 0) goto L_0x00a7
        L_0x00a6:
            r0 = 0
        L_0x00a7:
            if (r0 == 0) goto L_0x00ac
            r7.A03(r4)     // Catch:{ JSONException -> 0x00af }
        L_0x00ac:
            int r5 = r5 + 1
            goto L_0x0033
        L_0x00af:
            r2 = move-exception
            java.lang.String r1 = "AddressEntries"
            java.lang.String r0 = "Cannot create JSONObject from rawJson"
            X.C010708t.A0R(r1, r2, r0)     // Catch:{ all -> 0x00bb }
        L_0x00b7:
            java.util.TreeSet r0 = r7.A04     // Catch:{ all -> 0x00bb }
            monitor-exit(r7)
            return r0
        L_0x00bb:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BG.A01():java.util.TreeSet");
    }

    public synchronized void A02() {
        synchronized (this) {
            this.A00.clear();
            Iterator it = A01().iterator();
            while (it.hasNext()) {
                this.A00.add((AnonymousClass0Ry) it.next());
            }
        }
        AnonymousClass0B0 r0 = this.A02;
        if (r0 != null) {
            try {
                AnonymousClass0DD AY8 = r0.AY8();
                String str = this.A03;
                JSONObject jSONObject = new JSONObject();
                if (this.A00 != null) {
                    JSONArray jSONArray = new JSONArray();
                    for (AnonymousClass0Ry r8 : this.A00) {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.putOpt(TraceFieldType.HostName, r8.A02);
                        jSONObject2.put("priority", r8.A01);
                        jSONObject2.put("fail_count", r8.A00);
                        if (r8.A04 != null) {
                            JSONArray jSONArray2 = new JSONArray();
                            for (String put : r8.A04) {
                                jSONArray2.put(put);
                            }
                            jSONObject2.put("address_list_data", jSONArray2);
                        }
                        jSONArray.put(jSONObject2.toString());
                    }
                    jSONObject.put("address_entries", jSONArray);
                }
                AY8.BzD(str, jSONObject.toString());
                AY8.commit();
            } catch (JSONException e) {
                C010708t.A0R("AddressEntries", e, "Failed to save addressEntries");
            }
        }
        return;
    }

    public synchronized void A03(AnonymousClass0Ry r3) {
        if (this.A04.size() >= this.A01) {
            if (Build.VERSION.SDK_INT >= 9) {
                this.A04.pollLast();
            } else if (!this.A04.isEmpty()) {
                TreeSet treeSet = this.A04;
                treeSet.remove(treeSet.last());
            }
        }
        this.A04.add(r3);
    }

    public synchronized void A04(AnonymousClass0Ry r2, AnonymousClass0Ry r3) {
        this.A04.remove(r2);
        A03(r3);
    }

    public AnonymousClass0BG(int i, AnonymousClass0B0 r4, String str) {
        this.A01 = i;
        this.A02 = r4;
        this.A03 = str;
    }
}
