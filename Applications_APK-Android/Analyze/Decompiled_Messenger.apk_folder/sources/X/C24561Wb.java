package X;

/* renamed from: X.1Wb  reason: invalid class name and case insensitive filesystem */
public final class C24561Wb extends C24571Wc {
    public final String A00;
    public final String A01;

    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof C24571Wc)) {
                return false;
            }
            C24571Wc r4 = (C24571Wc) obj;
            if (!this.A00.equals(r4.A00()) || !this.A01.equals(r4.A01())) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ((this.A00.hashCode() ^ 1000003) * 1000003) ^ this.A01.hashCode();
    }

    public String toString() {
        return AnonymousClass08S.A0T("LibraryVersion{libraryName=", this.A00, ", version=", this.A01, "}");
    }

    public C24561Wb(String str, String str2) {
        if (str != null) {
            this.A00 = str;
            if (str2 != null) {
                this.A01 = str2;
                return;
            }
            throw new NullPointerException("Null version");
        }
        throw new NullPointerException("Null libraryName");
    }
}
