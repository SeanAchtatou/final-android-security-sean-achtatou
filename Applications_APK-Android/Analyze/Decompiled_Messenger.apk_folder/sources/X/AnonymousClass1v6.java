package X;

import android.content.Context;
import com.facebook.ipc.stories.model.StoryBucket;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1v6  reason: invalid class name */
public final class AnonymousClass1v6 extends C17770zR {
    public static final AtomicBoolean A09 = new AtomicBoolean();
    public static final AtomicInteger A0A = new AtomicInteger();
    public static final AtomicInteger A0B = new AtomicInteger();
    public static final AtomicLong A0C = new AtomicLong();
    public static final AtomicLong A0D = new AtomicLong();
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C73513gF A01;
    @Comparable(type = 13)
    public StoryBucket A02;
    @Comparable(type = 13)
    public StoryCard A03;
    @Comparable(type = 13)
    public C88334Jw A04;
    @Comparable(type = 14)
    public AnonymousClass5CG A05 = new AnonymousClass5CG();
    @Comparable(type = 13)
    public C94354eN A06;
    @Comparable(type = 13)
    public AnonymousClass463 A07;
    public C04310Tq A08;

    public AnonymousClass1v6(Context context) {
        super("StoryViewerReactionStickerContainerComponent");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A00 = new AnonymousClass0UN(8, r2);
        this.A08 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ayx, r2);
    }

    public static void A00(AnonymousClass0p4 r3, int i) {
        if (r3.A04 != null) {
            r3.A0G(new C61322yh(0, Integer.valueOf(i)), "updateState:StoryViewerReactionStickerContainerComponent.updateAffordanceAnimationState");
        }
    }

    public C17770zR A16() {
        AnonymousClass1v6 r1 = (AnonymousClass1v6) super.A16();
        r1.A05 = new AnonymousClass5CG();
        return r1;
    }
}
