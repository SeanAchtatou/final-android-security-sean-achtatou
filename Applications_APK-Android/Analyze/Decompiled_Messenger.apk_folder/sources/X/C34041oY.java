package X;

import android.os.Bundle;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.montage.omnistore.service.model.FetchBucketParams;
import com.google.common.util.concurrent.ListenableFuture;

@UserScoped
/* renamed from: X.1oY  reason: invalid class name and case insensitive filesystem */
public final class C34041oY {
    private static C05540Zi A01;
    public AnonymousClass0UN A00;

    public static final C34041oY A00(AnonymousClass1XY r4) {
        C34041oY r0;
        synchronized (C34041oY.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C34041oY((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C34041oY) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public ListenableFuture A01(FetchBucketParams fetchBucketParams, long j) {
        C005505z.A03("MontageLoadingServiceImpl.loadBucket", -1890179113);
        try {
            Bundle bundle = new Bundle();
            bundle.putParcelable(C99084oO.$const$string(112), fetchBucketParams);
            return AnonymousClass169.A00(((AnonymousClass1TQ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AqV, this.A00)).A01(j).CIF(new AnonymousClass4C5(this, bundle)), new AnonymousClass4G3());
        } finally {
            C005505z.A00(-1549678550);
        }
    }

    private C34041oY(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
