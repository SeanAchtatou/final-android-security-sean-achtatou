package X;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import com.facebook.fblibraries.fblogin.FirstPartySsoSessionInfo;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1ho  reason: invalid class name and case insensitive filesystem */
public final class C30291ho {
    public boolean A00;
    public final JSONObject A01 = new JSONObject();

    public static void A00(C30291ho r0, String str, String str2) {
        try {
            r0.A01.put(str, str2);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void A02(String str, String str2) {
        if (str2 != null) {
            A00(this, AnonymousClass08S.A0J("customKey", str), str2);
        }
    }

    public void A01(Context context, Account account) {
        FirstPartySsoSessionInfo firstPartySsoSessionInfo;
        String str;
        String str2;
        AccountManager accountManager = AccountManager.get(context);
        synchronized (C30291ho.class) {
            String str3 = null;
            if (!this.A00) {
                firstPartySsoSessionInfo = AnonymousClass2X3.A01(context, account, null);
                if (this.A01.length() == 0 && firstPartySsoSessionInfo == null) {
                }
            } else {
                firstPartySsoSessionInfo = null;
            }
            if (firstPartySsoSessionInfo != null) {
                if (!this.A01.has("userId")) {
                    A00(this, "userId", firstPartySsoSessionInfo.A04);
                }
                if (!this.A01.has("accessToken")) {
                    A00(this, "accessToken", firstPartySsoSessionInfo.A01);
                }
                if (!this.A01.has("name")) {
                    A00(this, "name", firstPartySsoSessionInfo.A02);
                }
                if (!this.A01.has("userName") && (str2 = firstPartySsoSessionInfo.A05) != null) {
                    A00(this, "userName", str2);
                }
                if (!this.A01.has("profilePicUrl") && (str = firstPartySsoSessionInfo.A03) != null) {
                    A00(this, "profilePicUrl", str);
                }
                Map map = firstPartySsoSessionInfo.A06;
                if (map != null) {
                    for (Map.Entry entry : map.entrySet()) {
                        if (!this.A01.has(AnonymousClass08S.A0J("customKey", (String) entry.getKey()))) {
                            A02((String) entry.getKey(), (String) entry.getValue());
                        }
                    }
                }
            }
            if (this.A01.length() != 0) {
                str3 = this.A01.toString();
            }
            try {
                accountManager.setUserData(account, "sso_data", str3);
            } catch (SecurityException e) {
                C010708t.A0L("SsoToAccountManagerWriter", "writeToAccount", e);
            } catch (RuntimeException e2) {
                C010708t.A0L("SsoToAccountManagerWriter", "DeadObjectException on setUserData", e2);
            }
        }
    }
}
