package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.1lY  reason: invalid class name and case insensitive filesystem */
public final class C32311lY {
    private static C04470Uu A01;
    public final C25051Yd A00;

    public static final C32311lY A00(AnonymousClass1XY r4) {
        C32311lY r0;
        synchronized (C32311lY.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C32311lY((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C32311lY) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C32311lY(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
