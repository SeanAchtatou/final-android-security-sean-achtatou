package X;

import android.text.SpannableStringBuilder;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.128  reason: invalid class name */
public final class AnonymousClass128 {
    public Set A00;
    public final FbSharedPreferences A01;
    public final AnonymousClass1Y7 A02;

    public static synchronized void A00(AnonymousClass128 r4) {
        Collection A08;
        synchronized (r4) {
            if (r4.A00 == null) {
                String B4F = r4.A01.B4F(r4.A02, null);
                if (B4F == null) {
                    A08 = new ArrayList();
                } else {
                    A08 = C06850cB.A08(B4F, ',');
                }
                r4.A00 = new LinkedHashSet(A08);
            }
        }
    }

    public static synchronized void A01(AnonymousClass128 r5) {
        synchronized (r5) {
            if (C013509w.A02(r5.A00)) {
                C30281hn edit = r5.A01.edit();
                edit.C1B(r5.A02);
                edit.commit();
            } else {
                ArrayList arrayList = new ArrayList(r5.A00);
                String valueOf = String.valueOf(',');
                List<CharSequence> subList = arrayList.subList(Math.max(0, arrayList.size() - 100), Math.min(arrayList.size(), arrayList.size()));
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                boolean z = true;
                for (CharSequence charSequence : subList) {
                    if (z) {
                        z = false;
                    } else {
                        spannableStringBuilder.append((CharSequence) valueOf);
                    }
                    spannableStringBuilder.append(charSequence);
                }
                C30281hn edit2 = r5.A01.edit();
                edit2.BzC(r5.A02, spannableStringBuilder.toString());
                edit2.commit();
            }
        }
    }

    public synchronized void A02(String str) {
        if (str != null) {
            A00(this);
            if (this.A00.add(str)) {
                A01(this);
            }
        }
    }

    public synchronized void A03(String str) {
        if (str != null) {
            A00(this);
            if (this.A00.remove(str)) {
                A01(this);
            }
        }
    }

    public synchronized boolean A04(String str) {
        if (str == null) {
            return false;
        }
        A00(this);
        return this.A00.contains(str);
    }

    public AnonymousClass128(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r2) {
        this.A01 = fbSharedPreferences;
        Preconditions.checkNotNull(r2);
        this.A02 = r2;
    }
}
