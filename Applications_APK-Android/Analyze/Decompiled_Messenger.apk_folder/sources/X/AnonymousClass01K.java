package X;

import java.io.Writer;

/* renamed from: X.01K  reason: invalid class name */
public final class AnonymousClass01K implements AnonymousClass01J {
    public boolean ANg(Writer writer, AnonymousClass0HM r8) {
        boolean z;
        String str;
        writer.append((CharSequence) "\"largest_address_space_chunk_kb\":");
        writer.append((CharSequence) Integer.toString(AnonymousClass024.A00()));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"64bit_build\":");
        if ("arm64" != 0) {
            z = "arm64".contains("64");
        } else {
            z = false;
        }
        if (z) {
            str = "1";
        } else {
            str = "0";
        }
        writer.append((CharSequence) str);
        writer.append((CharSequence) ",");
        String[] strArr = C010108n.A01;
        long[] jArr = C010108n.A00;
        AnonymousClass00V.A02("/proc/meminfo", strArr, jArr);
        writer.append((CharSequence) "\"system_total_mem_kb\":");
        writer.append((CharSequence) Long.toString(jArr[AnonymousClass07B.A00.intValue()]));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"system_free_mem_kb\":");
        writer.append((CharSequence) Long.toString(jArr[AnonymousClass07B.A01.intValue()]));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"system_cached_mem_kb\":");
        writer.append((CharSequence) Long.toString(jArr[AnonymousClass07B.A0N.intValue()]));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"system_anonymous_mem_kb\":");
        writer.append((CharSequence) Long.toString(jArr[AnonymousClass07B.A0n.intValue()]));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"system_kernel_mem_kb\":");
        writer.append((CharSequence) Long.toString(jArr[AnonymousClass07B.A0p.intValue()] + jArr[AnonymousClass07B.A0q.intValue()] + jArr[AnonymousClass07B.A02.intValue()] + jArr[AnonymousClass07B.A03.intValue()]));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"system_disk_buffer_mem_kb\":");
        writer.append((CharSequence) Long.toString(jArr[AnonymousClass07B.A0C.intValue()] + jArr[AnonymousClass07B.A0Y.intValue()] + jArr[AnonymousClass07B.A0i.intValue()] + jArr[AnonymousClass07B.A04.intValue()]));
        writer.append((CharSequence) ",");
        writer.append((CharSequence) "\"system_shared_mem_kb\":");
        writer.append((CharSequence) Long.toString(jArr[AnonymousClass07B.A0o.intValue()]));
        return true;
    }
}
