package X;

import com.facebook.profilo.logger.Logger;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;

/* renamed from: X.0EN  reason: invalid class name */
public final class AnonymousClass0EN {
    public static InputStream A00(URLConnection uRLConnection, int i) {
        return new AnonymousClass0ER(uRLConnection.getInputStream(), i);
    }

    public static OutputStream A01(URLConnection uRLConnection, int i) {
        return new AnonymousClass0EO(uRLConnection.getOutputStream(), i);
    }

    public static void A02(URLConnection uRLConnection, int i) {
        int i2 = AnonymousClass00n.A08;
        int i3 = i;
        int writeStandardEntry = Logger.writeStandardEntry(i2, 6, 22, 0, 0, i3, 0, 0);
        try {
            uRLConnection.connect();
            Logger.writeStandardEntry(i2, 6, 23, 0, 0, i3, writeStandardEntry, 0);
        } catch (Throwable th) {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 23, 0, 0, i3, writeStandardEntry, 0);
            throw th;
        }
    }
}
