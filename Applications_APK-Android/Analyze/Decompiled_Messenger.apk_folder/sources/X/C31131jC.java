package X;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.1jC  reason: invalid class name and case insensitive filesystem */
public final class C31131jC {
    public long[] A00 = null;
    private long A01 = 0;
    private boolean A02 = false;
    private boolean A03 = false;
    public final long A04;

    public static long[] A00(ArrayList arrayList) {
        long[] jArr = new long[4];
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            long[] jArr2 = (long[]) it.next();
            int length = jArr2.length;
            if (length == 4) {
                for (int i = 0; i < length; i++) {
                    jArr[i] = jArr[i] + jArr2[i];
                }
            }
        }
        return jArr;
    }

    public synchronized long[] A02() {
        int length;
        long[] jArr;
        A01(this.A03, this.A02);
        long[] jArr2 = this.A00;
        long j = 0;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            j += jArr2[i];
            i++;
        }
        long j2 = this.A04;
        boolean z = false;
        if (j >= j2) {
            z = true;
        }
        jArr = null;
        if (z) {
            jArr = jArr2;
            long j3 = 0;
            for (long j4 : jArr2) {
                j3 += j4;
            }
            for (int i2 = 0; i2 < length; i2++) {
                jArr2[i2] = (jArr2[i2] * j2) / j3;
            }
            this.A00 = null;
            A01(this.A03, this.A02);
        }
        return jArr;
    }

    public C31131jC(long j) {
        this.A04 = j;
    }

    public boolean A01(boolean z, boolean z2) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long j = this.A01;
        if (elapsedRealtime < j) {
            this.A00 = null;
        }
        long[] jArr = this.A00;
        if (jArr == null) {
            this.A00 = new long[4];
        } else {
            long j2 = elapsedRealtime - j;
            boolean z3 = this.A03;
            boolean z4 = this.A02;
            int i = 0;
            if (z3) {
                i = 2;
            }
            int i2 = i + (z4 ? 1 : 0);
            jArr[i2] = jArr[i2] + j2;
        }
        this.A01 = elapsedRealtime;
        this.A03 = z;
        this.A02 = z2;
        long j3 = 0;
        for (long j4 : this.A00) {
            j3 += j4;
        }
        if (j3 >= this.A04) {
            return true;
        }
        return false;
    }
}
