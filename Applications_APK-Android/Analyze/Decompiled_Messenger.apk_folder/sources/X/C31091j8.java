package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.stash.core.Stash;
import java.io.File;
import java.util.Set;

/* renamed from: X.1j8  reason: invalid class name and case insensitive filesystem */
public abstract class C31091j8 implements Stash {
    public final Stash A00;

    public Set getAllKeys() {
        return this.A00.getAllKeys();
    }

    public File getResource(String str) {
        return this.A00.getResource(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public File getResourcePath(String str) {
        if (!(this instanceof C31111jA)) {
            return this.A00.getResourcePath(str);
        }
        C31111jA r5 = (C31111jA) this;
        int hashCode = ((r5.A00 + AnonymousClass1Y3.A4R) * 31) + str.hashCode();
        r5.A01.markerStart(42991648, hashCode, "stash_name", r5.A02);
        try {
            return r5.A00.getResourcePath(str);
        } finally {
            r5.A01.markerEnd(42991648, hashCode, (short) 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public long getSizeBytes() {
        if (!(this instanceof C31111jA)) {
            return this.A00.getSizeBytes();
        }
        C31111jA r6 = (C31111jA) this;
        r6.A01.markerStart(42991638, r6.A00, "stash_name", r6.A02);
        try {
            return r6.A00.getSizeBytes();
        } finally {
            r6.A01.markerEnd(42991638, r6.A00, (short) 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public boolean hasKey(String str) {
        if (this instanceof C31111jA) {
            C31111jA r5 = (C31111jA) this;
            int hashCode = ((r5.A00 + AnonymousClass1Y3.A4R) * 31) + str.hashCode();
            r5.A01.markerStart(42991636, hashCode, "stash_name", r5.A02);
            short s = 3;
            try {
                boolean hasKey = r5.A00.hasKey(str);
                QuickPerformanceLogger quickPerformanceLogger = r5.A01;
                if (hasKey) {
                    s = 2;
                }
                quickPerformanceLogger.markerEnd(42991636, hashCode, s);
                return hasKey;
            } catch (Throwable th) {
                r5.A01.markerEnd(42991636, hashCode, (short) 3);
                throw th;
            }
        } else if (!(this instanceof C31081j7)) {
            return this.A00.hasKey(str);
        } else {
            C31081j7 r1 = (C31081j7) this;
            if (r1.A01 || r1.A00.contains(str)) {
                return r1.A00.contains(str);
            }
            if (!r1.A00.hasKey(str)) {
                return false;
            }
            r1.A00.add(str);
            return true;
        }
    }

    public File insert(String str) {
        return this.A00.insert(str);
    }

    public boolean remove(String str) {
        return this.A00.remove(str);
    }

    public boolean removeAll() {
        return this.A00.removeAll();
    }

    public C31091j8(Stash stash) {
        this.A00 = stash;
    }
}
