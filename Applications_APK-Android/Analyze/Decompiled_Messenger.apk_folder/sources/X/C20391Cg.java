package X;

import com.facebook.stickers.model.StickerPack;

/* renamed from: X.1Cg  reason: invalid class name and case insensitive filesystem */
public final class C20391Cg extends C06020ai {
    public final /* synthetic */ C55162nd A00;
    public final /* synthetic */ StickerPack A01;

    public C20391Cg(C55162nd r1, StickerPack stickerPack) {
        this.A00 = r1;
        this.A01 = stickerPack;
    }

    public void dispose() {
        super.dispose();
        C010708t.A0B(C55162nd.A07, "Add sticker pack operation for pack %s cancelled.", this.A01.A0B);
        C55162nd.A01(this.A00, false, this.A01);
    }
}
