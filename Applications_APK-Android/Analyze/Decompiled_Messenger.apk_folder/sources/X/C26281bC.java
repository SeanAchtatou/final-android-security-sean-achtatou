package X;

import android.os.Build;
import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.1bC  reason: invalid class name and case insensitive filesystem */
public final class C26281bC {
    public static C26281bC A07;
    public String A00 = "N/A";
    public String A01 = "others";
    public boolean A02 = false;
    public boolean A03 = false;
    public boolean A04 = false;
    public boolean A05 = false;
    public final C26311bF A06;

    public static synchronized C26281bC A00() {
        C26281bC r0;
        synchronized (C26281bC.class) {
            if (A07 == null) {
                A07 = new C26281bC();
            }
            r0 = A07;
        }
        return r0;
    }

    public static boolean A01() {
        String str = Build.FINGERPRINT;
        if (!str.startsWith("generic") && !str.startsWith("unknown")) {
            String str2 = Build.MODEL;
            String $const$string = AnonymousClass80H.$const$string(152);
            if (str2.contains($const$string) || str2.contains(AnonymousClass80H.$const$string(87)) || str2.contains(AnonymousClass80H.$const$string(82)) || Build.MANUFACTURER.contains(AnonymousClass80H.$const$string(93)) || ((Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic")) || $const$string.equals(Build.PRODUCT))) {
                return true;
            }
            return false;
        }
        return true;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("chip_name", this.A00);
            jSONObject.put("chip_vendor", this.A01);
            jSONObject.put("platform_qualcomm", "qualcomm".equals(this.A01));
            jSONObject.put("platform_samsung", "samsung".equals(this.A01));
            jSONObject.put("platform_mediatek", "mediatek".equals(this.A01));
            jSONObject.put("platform_spreadtrum", "spreadtrum".equals(this.A01));
            jSONObject.put("platform_hisilicon", "hisilicon".equals(this.A01));
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0045, code lost:
        if ("samsung".equals(android.os.Build.MANUFACTURER) != false) goto L_0x0047;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C26281bC() {
        /*
            r7 = this;
            r7.<init>()
            java.lang.String r0 = "N/A"
            r7.A00 = r0
            java.lang.String r0 = "others"
            r7.A01 = r0
            r0 = 0
            r7.A05 = r0
            r7.A02 = r0
            r7.A03 = r0
            r7.A04 = r0
            boolean r0 = A01()
            if (r0 == 0) goto L_0x0026
            X.1bE r0 = new X.1bE
            r0.<init>()
            X.1bF r0 = r0.A00()
            r7.A06 = r0
            return
        L_0x0026:
            X.0ei r0 = new X.0ei
            r0.<init>()
            java.lang.String r1 = r0.A00
            r7.A00 = r1
            java.lang.String r0 = r0.A01
            r7.A01 = r0
            java.lang.String r4 = android.os.Build.BRAND
            java.lang.String r2 = "samsung"
            boolean r0 = r2.equals(r4)
            r3 = 0
            if (r0 != 0) goto L_0x0047
            java.lang.String r0 = android.os.Build.MANUFACTURER
            boolean r2 = r2.equals(r0)
            r0 = 0
            if (r2 == 0) goto L_0x0048
        L_0x0047:
            r0 = 1
        L_0x0048:
            r7.A05 = r0
            java.lang.String r0 = "google"
            boolean r0 = r0.equals(r4)
            r7.A02 = r0
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r2 = r4.toLowerCase(r0)
            java.lang.String r0 = "huawei"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0068
            java.lang.String r0 = "HONOR"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x0069
        L_0x0068:
            r3 = 1
        L_0x0069:
            r7.A03 = r3
            java.lang.String r0 = "lge"
            boolean r0 = r0.equals(r4)
            r7.A04 = r0
            java.lang.String r0 = "msm8952"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0084
            int r2 = X.C26291bD.A00()
            r0 = 6
            if (r2 != r0) goto L_0x0084
            java.lang.String r1 = "msm8956"
        L_0x0084:
            r7.A00 = r1
            java.lang.String r2 = r7.A01
            java.lang.String r0 = "qualcomm"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x011e
            X.1bE r2 = new X.1bE
            r2.<init>()
            java.lang.String r0 = "msmnile"
            boolean r0 = r0.equals(r1)
            r4 = 0
            r3 = 4
            r5 = 2
            if (r0 == 0) goto L_0x0185
            X.1bE r6 = new X.1bE
            r6.<init>()
            int[] r2 = new int[r5]
            r2 = {825600, 2841600} // fill-array
            X.1bF r1 = r6.A00
            r0 = 1
            r1.A06 = r0
            r1.A0D = r2
            r1.A08 = r0
            r1 = 3
            int[] r0 = new int[r5]
            r0 = {710400, 2419200} // fill-array
            r6.A01(r1, r0, r3)
            int[] r0 = new int[r5]
            r0 = {300000, 1785600} // fill-array
            r6.A02(r3, r0, r4)
            X.1bF r1 = r6.A00
            r0 = 1
            r1.A09 = r0
            X.1bF r0 = r6.A00()
        L_0x00cd:
            r7.A06 = r0
            java.lang.String r3 = r7.A00
            boolean r1 = r0.A07
            if (r1 == 0) goto L_0x00f4
            if (r3 == 0) goto L_0x00f4
            int r2 = r0.A02
            r1 = 10
            if (r2 != r1) goto L_0x00f5
            java.lang.String r1 = "mt"
            boolean r1 = r3.startsWith(r1)
            if (r1 == 0) goto L_0x00f4
            r1 = 2
            r0.A03 = r1
            r1 = 4
            r0.A05 = r1
            r0.A00 = r1
            int r1 = r0.A04
            if (r1 != 0) goto L_0x01e6
            r1 = 6
            r0.A01 = r1
        L_0x00f4:
            return
        L_0x00f5:
            r1 = 8
            if (r2 != r1) goto L_0x010e
            java.lang.String r1 = "exynos7885"
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x00f4
            r3 = 2
            r0.A03 = r3
            r2 = 6
        L_0x0105:
            r0.A00 = r2
            int r1 = r0.A04
            if (r1 != 0) goto L_0x01eb
            r0.A01 = r3
            return
        L_0x010e:
            r1 = 6
            if (r2 != r1) goto L_0x00f4
            java.lang.String r1 = "exynos"
            boolean r1 = r3.startsWith(r1)
            if (r1 == 0) goto L_0x00f4
            r3 = 2
            r0.A03 = r3
            r2 = 4
            goto L_0x0105
        L_0x011e:
            java.lang.String r0 = "samsung"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01e0
            java.lang.String r0 = "mediatek"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x01da
            X.1bE r2 = new X.1bE
            r2.<init>()
            java.lang.String r0 = "mt6797"
            boolean r0 = r0.equals(r1)
            r6 = 0
            r5 = 4
            r4 = 2
            if (r0 == 0) goto L_0x016c
            X.1bE r3 = new X.1bE
            r3.<init>()
            int[] r2 = new int[r4]
            r2 = {338000, 2314000} // fill-array
            X.1bF r1 = r3.A00
            r1.A06 = r4
            r1.A0D = r2
            r0 = 1
            r1.A08 = r0
            int[] r0 = new int[r4]
            r0 = {325000, 1846000} // fill-array
            r3.A01(r5, r0, r5)
            int[] r0 = new int[r4]
            r0 = {221000, 1391000} // fill-array
            r3.A02(r5, r0, r6)
            X.1bF r1 = r3.A00
            r0 = 1
            r1.A09 = r0
            X.1bF r0 = r3.A00()
            goto L_0x00cd
        L_0x016c:
            java.lang.String r0 = "mt6771"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01e0
            int[] r0 = new int[r4]
            r0 = {793000, 1989000} // fill-array
            r2.A01(r5, r0, r5)
            int[] r0 = new int[r4]
            r0 = {793000, 1989000} // fill-array
            r2.A02(r5, r0, r6)
            goto L_0x019d
        L_0x0185:
            java.lang.String r0 = "msm8994"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01a8
            int[] r0 = new int[r5]
            r0 = {384000, 1958400} // fill-array
            r2.A01(r3, r0, r3)
            int[] r0 = new int[r5]
            r0 = {384000, 1555200} // fill-array
            r2.A02(r3, r0, r4)
        L_0x019d:
            X.1bF r1 = r2.A00
            r0 = 1
            r1.A09 = r0
            X.1bF r0 = r2.A00()
            goto L_0x00cd
        L_0x01a8:
            java.lang.String r0 = "msm8956"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01c1
            int[] r0 = new int[r5]
            r0 = {400000, 1804800} // fill-array
            r2.A01(r5, r0, r3)
            int[] r0 = new int[r5]
            r0 = {400000, 1401600} // fill-array
            r2.A02(r3, r0, r4)
            goto L_0x019d
        L_0x01c1:
            java.lang.String r0 = "msm8992"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01e0
            int[] r0 = new int[r5]
            r0 = {384000, 1824000} // fill-array
            r2.A01(r5, r0, r3)
            int[] r0 = new int[r5]
            r0 = {384000, 1440000} // fill-array
            r2.A02(r3, r0, r4)
            goto L_0x019d
        L_0x01da:
            java.lang.String r0 = "hisilicon"
            boolean r0 = r0.equals(r2)
        L_0x01e0:
            X.1bF r0 = X.C26291bD.A01()
            goto L_0x00cd
        L_0x01e6:
            r1 = 8
            r0.A04 = r1
            return
        L_0x01eb:
            r0.A04 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26281bC.<init>():void");
    }
}
