package X;

import com.facebook.proxygen.MQTTClient;
import java.util.concurrent.Executor;

/* renamed from: X.1se  reason: invalid class name and case insensitive filesystem */
public final class C36181se {
    public final MQTTClient A00;
    public final Executor A01;
    public volatile boolean A02 = false;

    public C36181se(MQTTClient mQTTClient, Executor executor) {
        this.A00 = mQTTClient;
        this.A01 = executor;
    }
}
