package X;

import java.util.Comparator;

/* renamed from: X.1Up  reason: invalid class name */
public final class AnonymousClass1Up implements Comparator {
    public int compare(Object obj, Object obj2) {
        long longValue = ((Long) obj2).longValue();
        long longValue2 = ((Long) obj).longValue();
        if (longValue < longValue2) {
            return -1;
        }
        if (longValue2 == longValue) {
            return 0;
        }
        return 1;
    }
}
