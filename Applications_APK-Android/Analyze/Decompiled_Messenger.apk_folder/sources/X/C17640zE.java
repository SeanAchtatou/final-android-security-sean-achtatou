package X;

import android.os.Build;
import android.view.View;
import com.facebook.acra.ACRA;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0zE  reason: invalid class name and case insensitive filesystem */
public final class C17640zE {
    public final Map A00 = new HashMap();
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();

    public static void A01(C17640zE r2, C17840zZ r3, C17770zR r4) {
        Object obj = (Set) r2.A02.get(r3);
        if (obj == null) {
            obj = new HashSet();
            r2.A02.put(r3, obj);
            r3.A01.add(r2);
        }
        obj.add(r4);
    }

    public static void A00(int i, C17840zZ r2, View view) {
        switch (i) {
            case 1:
                view.setAlpha(((Float) r2.A00).floatValue());
                return;
            case 2:
                view.setTranslationX(((Float) r2.A00).floatValue());
                return;
            case 3:
                view.setTranslationY(((Float) r2.A00).floatValue());
                return;
            case 4:
                view.setScaleX(((Float) r2.A00).floatValue());
                return;
            case 5:
                view.setScaleY(((Float) r2.A00).floatValue());
                return;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                if (Build.VERSION.SDK_INT >= 21) {
                    view.setElevation(((Float) r2.A00).floatValue());
                    return;
                }
                return;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                view.setBackgroundColor(((Integer) r2.A00).intValue());
                return;
            case 8:
                view.setRotation(((Float) r2.A00).floatValue());
                return;
            default:
                return;
        }
    }
}
