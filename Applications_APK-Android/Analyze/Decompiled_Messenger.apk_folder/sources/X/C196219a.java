package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.19a  reason: invalid class name and case insensitive filesystem */
public final class C196219a extends AnonymousClass0W4 {
    private static final C06060am A00 = new C06050al(ImmutableList.of(C196419c.A01));
    private static final ImmutableList A01 = ImmutableList.of(C196419c.A01, C196419c.A02, C196419c.A00, C196419c.A03);

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public C196219a() {
        super("signed_pre_keys", A01, A00);
    }
}
