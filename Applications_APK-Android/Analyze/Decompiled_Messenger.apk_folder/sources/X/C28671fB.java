package X;

/* renamed from: X.1fB  reason: invalid class name and case insensitive filesystem */
public final class C28671fB {
    public static C28671fB A0L;
    public AHu A00;
    public AHz A01;
    private C39211yg A02;
    private C38921yD A03;
    private C38911yC A04;
    private C35141qk A05;
    private AnonymousClass50V A06;
    private AnonymousClass26L A07;
    private AHs A08;
    public final C04310Tq A09;
    public final C04310Tq A0A;
    public final C04310Tq A0B;
    public final C04310Tq A0C;
    public final C04310Tq A0D = new AnonymousClass0YE(this);
    public final C04310Tq A0E;
    private final C04310Tq A0F;
    private final C04310Tq A0G;
    private final C04310Tq A0H;
    private final C04310Tq A0I;
    private final C04310Tq A0J;
    private final C04310Tq A0K;

    public static synchronized C38911yC A01(C28671fB r2) {
        C38911yC r0;
        synchronized (r2) {
            if (r2.A0B != null && r2.A04 == null) {
                AnonymousClass06K.A01("MemoryManager.getLeakDetector", -899198208);
                r2.A04 = (C38911yC) r2.A0B.get();
                AnonymousClass06K.A00(-1284454266);
            }
            r0 = r2.A04;
        }
        return r0;
    }

    public synchronized C39211yg A02() {
        if (this.A02 == null && this.A0H != null) {
            AnonymousClass06K.A01("MemoryManager.getMemoryMetricsManager", -177547454);
            this.A02 = (C39211yg) this.A0H.get();
            AnonymousClass06K.A00(1502549100);
        }
        return this.A02;
    }

    public synchronized C38921yD A03() {
        if (this.A03 == null) {
            AnonymousClass06K.A01("MemoryManager.getMemoryUploader", 929584891);
            this.A03 = (C38921yD) this.A0K.get();
            AnonymousClass06K.A00(-1053800489);
        }
        return this.A03;
    }

    public synchronized C35141qk A04() {
        if (this.A05 == null) {
            AnonymousClass06K.A01("MemoryManager.getMemoryMetricsManager", 302857392);
            this.A05 = (C35141qk) this.A0J.get();
            AnonymousClass06K.A00(385021668);
        }
        return this.A05;
    }

    public synchronized AnonymousClass50V A05() {
        if (this.A06 == null) {
            AnonymousClass06K.A01("MemoryManager.getMemoryDumpSoftErrorReporter", -2083447823);
            this.A06 = (AnonymousClass50V) this.A0G.get();
            AnonymousClass06K.A00(-1234847045);
        }
        return this.A06;
    }

    public synchronized AnonymousClass26L A06() {
        if (this.A07 == null) {
            AnonymousClass06K.A01("MemoryManager.getMemoryDumpingConfig", 1669482005);
            this.A07 = (AnonymousClass26L) this.A0I.get();
            AnonymousClass06K.A00(-53493370);
        }
        return this.A07;
    }

    public synchronized AHs A07() {
        if (this.A08 == null) {
            AnonymousClass06K.A01("MemoryManager.getMemoryDumpMetadataStore", 101052068);
            this.A08 = (AHs) this.A0F.get();
            AnonymousClass06K.A00(-111454692);
        }
        return this.A08;
    }

    public static C28671fB A00() {
        C28671fB r0 = A0L;
        if (r0 != null) {
            return r0;
        }
        throw new RuntimeException("Stub!");
    }

    public C28671fB(C28621f6 r2) {
        C04310Tq r0 = r2.A00;
        C000300h.A01(r0);
        this.A09 = r0;
        C04310Tq r02 = r2.A09;
        C000300h.A01(r02);
        this.A0K = r02;
        C04310Tq r03 = r2.A01;
        C000300h.A01(r03);
        this.A0A = r03;
        C04310Tq r04 = r2.A07;
        C000300h.A01(r04);
        this.A0I = r04;
        C04310Tq r05 = r2.A05;
        C000300h.A01(r05);
        this.A0G = r05;
        C04310Tq r06 = r2.A04;
        C000300h.A01(r06);
        this.A0F = r06;
        C04310Tq r07 = r2.A0A;
        C000300h.A01(r07);
        this.A0E = r07;
        this.A0B = r2.A02;
        this.A0C = r2.A03;
        this.A0J = r2.A08;
        this.A0H = r2.A06;
    }
}
