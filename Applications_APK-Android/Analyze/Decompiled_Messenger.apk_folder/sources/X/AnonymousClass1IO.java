package X;

import java.util.Comparator;

/* renamed from: X.1IO  reason: invalid class name */
public final class AnonymousClass1IO implements Comparator {
    public int compare(Object obj, Object obj2) {
        AnonymousClass1IR r3 = (AnonymousClass1IR) obj;
        AnonymousClass1IR r4 = (AnonymousClass1IR) obj2;
        int i = r3.A01 - r4.A01;
        if (i == 0) {
            return r3.A02 - r4.A02;
        }
        return i;
    }
}
