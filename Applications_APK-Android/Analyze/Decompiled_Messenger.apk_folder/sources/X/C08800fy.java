package X;

import java.util.Arrays;
import java.util.HashSet;

/* renamed from: X.0fy  reason: invalid class name and case insensitive filesystem */
public final class C08800fy extends HashSet<AnonymousClass0TO> {
    public C08800fy() {
        addAll(Arrays.asList(AnonymousClass0TN.A0J, AnonymousClass0TN.A0M, AnonymousClass0TN.A0K, AnonymousClass0TN.A0O, AnonymousClass0TN.A0H, AnonymousClass0TN.A0X, AnonymousClass0TN.A0Q));
        addAll(AnonymousClass0TN.A0s);
    }
}
