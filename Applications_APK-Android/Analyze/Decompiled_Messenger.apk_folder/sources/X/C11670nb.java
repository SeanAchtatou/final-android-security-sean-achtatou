package X;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Map;

/* renamed from: X.0nb  reason: invalid class name and case insensitive filesystem */
public class C11670nb {
    public Map A00;
    public long A01 = -1;
    public long A02 = -1;
    public ArrayNode A03;
    public ObjectNode A04;
    public String A05;
    public String A06 = "AUTO_SET";
    public String A07 = "AUTO_SET";
    public boolean A08;

    public static synchronized Map A00(C11670nb r1, boolean z) {
        Map map;
        synchronized (r1) {
            if (r1.A00 == null && z) {
                r1.A00 = AnonymousClass0TG.A04();
            }
            map = r1.A00;
        }
        return map;
    }

    public static void A01(C11670nb r2) {
        if (r2.A04 == null) {
            r2.A04 = new ObjectNode(JsonNodeFactory.instance);
        }
    }

    public static void A02(C11670nb r4, Map map, boolean z) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                Object value = entry.getValue();
                if (value instanceof JsonNode) {
                    r4.A0B((String) entry.getKey(), (JsonNode) value);
                } else if (value instanceof String) {
                    r4.A0D((String) entry.getKey(), (String) value);
                } else if (!z || !(value instanceof Boolean)) {
                    r4.A0C((String) entry.getKey(), value);
                } else {
                    r4.A0E((String) entry.getKey(), ((Boolean) value).booleanValue());
                }
            }
        }
    }

    public String A04() {
        ObjectNode objectNode = this.A04;
        if (objectNode != null) {
            return objectNode.toString();
        }
        return null;
    }

    public String A05() {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        objectNode.put("time", AnonymousClass5DC.A00(this.A02));
        objectNode.put("log_type", "client_event");
        objectNode.put("name", this.A05);
        String str = this.A06;
        if (!(str == null || str == "AUTO_SET")) {
            A0D("process", str);
        }
        ArrayNode arrayNode = this.A03;
        if (arrayNode != null) {
            A0B("enabled_features", arrayNode);
        }
        ObjectNode objectNode2 = this.A04;
        if (objectNode2 != null) {
            objectNode.put("extra", objectNode2);
        }
        return objectNode.toString();
    }

    public void A0C(String str, Object obj) {
        if (obj != null) {
            A0D(str, obj.toString());
        }
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{"client_event", this.A05, A07("pigeon_reserved_keyword_module")});
    }

    public String toString() {
        return AnonymousClass08S.A0T("client_event", ":", this.A05, ":", A07("pigeon_reserved_keyword_module"));
    }

    public C11670nb(String str) {
        this.A05 = str;
    }

    public String A06(String str) {
        JsonNode jsonNode;
        Preconditions.checkArgument(!C06850cB.A0B(str), "Invalid Key");
        ObjectNode objectNode = this.A04;
        if (objectNode == null || (jsonNode = objectNode.get(str)) == null) {
            jsonNode = null;
        }
        if (jsonNode == null) {
            return null;
        }
        return jsonNode.toString();
    }

    public String A07(String str) {
        JsonNode jsonNode;
        Preconditions.checkArgument(!C06850cB.A0B(str), "Invalid Key");
        ObjectNode objectNode = this.A04;
        if (objectNode == null || (jsonNode = objectNode.get(str)) == null) {
            jsonNode = null;
        }
        if (jsonNode == null) {
            return null;
        }
        return jsonNode.asText();
    }

    public void A08(String str, double d) {
        A01(this);
        this.A04.put(str, d);
    }

    public void A09(String str, int i) {
        A01(this);
        this.A04.put(str, i);
    }

    public void A0A(String str, long j) {
        A01(this);
        this.A04.put(str, j);
    }

    public void A0B(String str, JsonNode jsonNode) {
        A01(this);
        this.A04.put(str, jsonNode);
    }

    public void A0D(String str, String str2) {
        A01(this);
        if (str2 != null) {
            this.A04.put(str, str2);
        }
    }

    public void A0E(String str, boolean z) {
        A01(this);
        this.A04.put(str, z);
    }
}
