package X;

/* renamed from: X.10W  reason: invalid class name */
public abstract class AnonymousClass10W {
    public abstract void addChildAt(AnonymousClass10W r1, int i);

    public abstract void calculateLayout(float f, float f2);

    public abstract AnonymousClass10W cloneWithoutChildren();

    public abstract void copyStyle(AnonymousClass10W r1);

    public abstract void dirty();

    public abstract AnonymousClass10W getChildAt(int i);

    public abstract int getChildCount();

    public abstract Object getData();

    public abstract AnonymousClass2N4 getDisplay();

    public abstract AnonymousClass10X getFlexDirection();

    public abstract AnonymousClass1K1 getHeight();

    public abstract float getLayoutBorder(AnonymousClass10G r1);

    public abstract C17660zG getLayoutDirection();

    public abstract float getLayoutHeight();

    public abstract float getLayoutPadding(AnonymousClass10G r1);

    public abstract float getLayoutWidth();

    public abstract float getLayoutX();

    public abstract float getLayoutY();

    public abstract AnonymousClass10W getOwner();

    public abstract C17660zG getStyleDirection();

    public abstract AnonymousClass1K1 getWidth();

    public abstract boolean hasNewLayout();

    public abstract void markLayoutSeen();

    public abstract AnonymousClass10W removeChildAt(int i);

    public abstract void reset();

    public abstract void setAlignContent(C14940uO r1);

    public abstract void setAlignItems(C14940uO r1);

    public abstract void setAlignSelf(C14940uO r1);

    public abstract void setAspectRatio(float f);

    public abstract void setBaselineFunction(AnonymousClass22K r1);

    public abstract void setBorder(AnonymousClass10G r1, float f);

    public abstract void setData(Object obj);

    public abstract void setDirection(C17660zG r1);

    public abstract void setDisplay(AnonymousClass2N4 r1);

    public abstract void setFlex(float f);

    public abstract void setFlexBasis(float f);

    public abstract void setFlexBasisPercent(float f);

    public abstract void setFlexDirection(AnonymousClass10X r1);

    public abstract void setFlexGrow(float f);

    public abstract void setFlexShrink(float f);

    public abstract void setHeight(float f);

    public abstract void setHeightAuto();

    public abstract void setHeightPercent(float f);

    public abstract void setIsReferenceBaseline(boolean z);

    public abstract void setJustifyContent(C14950uP r1);

    public abstract void setMargin(AnonymousClass10G r1, float f);

    public abstract void setMarginAuto(AnonymousClass10G r1);

    public abstract void setMarginPercent(AnonymousClass10G r1, float f);

    public abstract void setMaxHeight(float f);

    public abstract void setMaxHeightPercent(float f);

    public abstract void setMaxWidth(float f);

    public abstract void setMaxWidthPercent(float f);

    public abstract void setMeasureFunction(AnonymousClass119 r1);

    public abstract void setMinHeight(float f);

    public abstract void setMinHeightPercent(float f);

    public abstract void setMinWidth(float f);

    public abstract void setMinWidthPercent(float f);

    public abstract void setPadding(AnonymousClass10G r1, float f);

    public abstract void setPaddingPercent(AnonymousClass10G r1, float f);

    public abstract void setPosition(AnonymousClass10G r1, float f);

    public abstract void setPositionPercent(AnonymousClass10G r1, float f);

    public abstract void setPositionType(AnonymousClass1LC r1);

    public abstract void setWidth(float f);

    public abstract void setWidthAuto();

    public abstract void setWidthPercent(float f);

    public abstract void setWrap(AnonymousClass6I0 r1);
}
