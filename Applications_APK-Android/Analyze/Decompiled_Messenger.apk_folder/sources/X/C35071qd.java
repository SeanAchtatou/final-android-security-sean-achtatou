package X;

import java.util.HashMap;
import java.util.Map;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1qd  reason: invalid class name and case insensitive filesystem */
public final class C35071qd extends Enum {
    public static Map A00;
    private static final /* synthetic */ C35071qd[] A01;
    public static final C35071qd A02;
    public static final C35071qd A03;
    public static final C35071qd A04;
    public static final C35071qd A05;
    public static final C35071qd A06;
    public static final C35071qd A07;
    public static final C35071qd A08;
    public static final C35071qd A09;
    public static final C35071qd A0A;
    public static final C35071qd A0B;
    public static final C35071qd A0C;
    private final String mCategoryName;

    static {
        C35071qd r2 = new C35071qd("RECENTS_TAB", 0, "Recents Tab");
        C35071qd r4 = new C35071qd("GROUPS_TAB", 1, "Groups Tab");
        A05 = r4;
        C35071qd r5 = new C35071qd("PEOPLE_TAB", 2, "People Tab");
        A07 = r5;
        C35071qd r6 = new C35071qd("SETTINGS_TAB", 3, "Settings Tab");
        A0A = r6;
        C35071qd r7 = new C35071qd("COMPOSE_MESSAGE_FLOW", 4, "Compose Message Flow");
        A03 = r7;
        C35071qd r8 = new C35071qd("QUICK_CAM", 5, "QuickCam");
        A08 = r8;
        C35071qd r9 = new C35071qd("MEDIA_TRAY", 6, "Media Tray");
        C35071qd r10 = new C35071qd("STICKERS", 7, "Stickers");
        A0B = r10;
        C35071qd r11 = new C35071qd("VOICE_CLIPS", 8, "Voice Clips");
        A0C = r11;
        C35071qd r12 = new C35071qd("P2P", 9, "P2P");
        A06 = r12;
        C35071qd r13 = new C35071qd("SEARCH", 10, "Search");
        A09 = r13;
        C35071qd r14 = new C35071qd("ADD_CONTACT_FLOW", 11, "Add Contact Flow");
        A02 = r14;
        C35071qd r15 = new C35071qd(AnonymousClass80H.$const$string(29), 12, "Dialog");
        A04 = r15;
        A01 = new C35071qd[]{r2, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15};
        HashMap hashMap = new HashMap();
        A00 = hashMap;
        hashMap.put("thread_list", r2);
        A00.put("people", A07);
        A00.put("groups_tab", A05);
        A00.put("settings", A0A);
        A00.put("thread", A03);
        A00.put(C22298Ase.$const$string(39), A08);
        A00.put(AnonymousClass24B.$const$string(AnonymousClass1Y3.A2b), A0B);
        A00.put("payment_tray_popup", A06);
        A00.put("audio_popup", A0C);
        A00.put("search", A09);
        Map map = A00;
        C35071qd r22 = A02;
        map.put(C99084oO.$const$string(AnonymousClass1Y3.A20), r22);
        A00.put(C99084oO.$const$string(AnonymousClass1Y3.A26), r22);
        A00.put("dialog", A04);
    }

    public static C35071qd valueOf(String str) {
        return (C35071qd) Enum.valueOf(C35071qd.class, str);
    }

    public static C35071qd[] values() {
        return (C35071qd[]) A01.clone();
    }

    public String toString() {
        return this.mCategoryName;
    }

    private C35071qd(String str, int i, String str2) {
        this.mCategoryName = str2;
    }
}
