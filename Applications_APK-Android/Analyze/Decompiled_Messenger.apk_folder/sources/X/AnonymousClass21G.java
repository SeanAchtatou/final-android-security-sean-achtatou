package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.21G  reason: invalid class name */
public final class AnonymousClass21G extends AnonymousClass7Z0 {
    private static volatile AnonymousClass21G A00 = null;
    public static final String __redex_internal_original_name = "com.facebook.messaging.service.methods.MarkSpamThreadMethod";

    public AnonymousClass21G() {
        super(AnonymousClass07B.A0C);
    }

    public static final AnonymousClass21G A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass21G.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass21G();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
