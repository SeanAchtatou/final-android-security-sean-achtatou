package X;

import android.os.Bundle;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.common.util.TriState;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.http.interfaces.RequestPriority;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.zero.protocol.params.FetchZeroHeaderRequestParams;
import com.facebook.zero.protocol.params.SendZeroHeaderRequestParams;
import com.facebook.zero.protocol.results.FetchZeroHeaderRequestResult;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import com.google.common.util.concurrent.ListenableFuture;
import io.card.payment.BuildConfig;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1qv  reason: invalid class name and case insensitive filesystem */
public final class C35251qv implements C05700aB, CallerContextable {
    private static final CallerContext A04 = CallerContext.A04(C35251qv.class);
    private static volatile C35251qv A05 = null;
    public static final String __redex_internal_original_name = "com.facebook.zero.header.ZeroHeaderRequestManager";
    public AnonymousClass0UN A00;
    public ListenableFuture A01;
    private final C04310Tq A02;
    private final C04310Tq A03;

    public static C27211cp A00(C35251qv r3, String str, Bundle bundle) {
        CallerContext callerContext;
        int i = AnonymousClass1Y3.B1W;
        AnonymousClass0UN r2 = r3.A00;
        BlueServiceOperationFactory blueServiceOperationFactory = (BlueServiceOperationFactory) AnonymousClass1XX.A02(0, i, r2);
        if (((C91734Zq) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AYg, r2)).A00.Aem(18296564975994015L)) {
            callerContext = CallerContext.A07(C35251qv.class, "MAGIC_LOGOUT_TAG");
        } else {
            callerContext = A04;
        }
        C27211cp CGe = blueServiceOperationFactory.newInstance(str, bundle, 1, callerContext).CGe();
        CGe.A01(RequestPriority.A04);
        return CGe;
    }

    public static final C35251qv A01(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C35251qv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C35251qv(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static ListenableFuture A02(C35251qv r5, FetchZeroHeaderRequestResult fetchZeroHeaderRequestResult) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("sendZeroHeaderRequestParams", new SendZeroHeaderRequestParams(fetchZeroHeaderRequestResult));
        C27211cp A002 = A00(r5, "send_zero_header_request", bundle);
        C05350Yp.A08(A002, new C95984hy(r5), (ExecutorService) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BKH, r5.A00));
        return A002;
    }

    public static void A03(C35251qv r3, String str, String str2, String str3) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(6, AnonymousClass1Y3.Ap7, r3.A00)).A01("zero_header_request"), 603);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("carrier_id", str3);
            uSLEBaseShape0S0000000.A0D(C99084oO.$const$string(24), str);
            uSLEBaseShape0S0000000.A0D("usage", str2);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public static boolean A04(C35251qv r4) {
        if (!((C47852Yi) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHR, r4.A00)).A03().equals("wifi") || !((Boolean) r4.A03.get()).booleanValue() || ((FbSharedPreferences) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B6q, r4.A00)).Aep(C05730aE.A03, false)) {
            return false;
        }
        return true;
    }

    public void A05(boolean z, Integer num) {
        C14880uI r1;
        String str;
        if (!((TriState) this.A02.get()).asBoolean(false) && num == AnonymousClass07B.A01) {
            r1 = C14880uI.A0E;
            str = "Zero header request not sent because header request feature is not enabled";
        } else if (A04(this)) {
            r1 = C14880uI.A0E;
            str = "Zero header request not sent because user is connected to a WiFi network";
        } else {
            ListenableFuture listenableFuture = this.A01;
            if (listenableFuture != null) {
                listenableFuture.cancel(true);
            }
            Bundle bundle = new Bundle();
            int i = AnonymousClass1Y3.AHR;
            bundle.putParcelable("fetchZeroHeaderRequestParams", new FetchZeroHeaderRequestParams(((C47852Yi) AnonymousClass1XX.A02(1, i, this.A00)).A02(), ((C47852Yi) AnonymousClass1XX.A02(1, i, this.A00)).A03(), ((FbSharedPreferences) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B6q, this.A00)).B4F(C10730kl.A03, BuildConfig.FLAVOR), AnonymousClass99V.A00(num), z));
            this.A01 = A00(this, "fetch_zero_header_request", bundle);
            A03(this, "fetch_header_request", AnonymousClass99V.A00(num), BuildConfig.FLAVOR);
            ListenableFuture listenableFuture2 = this.A01;
            C82983wr r2 = new C82983wr();
            int i2 = AnonymousClass1Y3.BKH;
            C05350Yp.A08(listenableFuture2, r2, (ExecutorService) AnonymousClass1XX.A02(3, i2, this.A00));
            AnonymousClass169.A03(this.A01, new AnonymousClass99T(this), (ExecutorService) AnonymousClass1XX.A02(3, i2, this.A00));
            return;
        }
        C05350Yp.A03(OperationResult.A02(r1, str));
    }

    public ImmutableSet AzO() {
        ListenableFuture listenableFuture = this.A01;
        if (listenableFuture != null) {
            listenableFuture.cancel(true);
        }
        return RegularImmutableSet.A05;
    }

    private C35251qv(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(8, r3);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.AE6, r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.AfW, r3);
    }
}
