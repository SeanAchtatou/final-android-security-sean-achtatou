package X;

import android.util.SparseBooleanArray;
import com.facebook.common.util.TriState;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0gm  reason: invalid class name and case insensitive filesystem */
public final class C09230gm implements C09240gn {
    private SparseBooleanArray A00 = null;
    private TriState A01 = TriState.UNSET;
    public final C25051Yd A02;
    private final Map A03 = new HashMap();
    private final int[] A04 = {5505026, 5505028, 5505027, 5505032, 5505081, 5505105};
    private final int[] A05 = new int[0];

    public static final C09230gm A00(AnonymousClass1XY r1) {
        return new C09230gm(r1);
    }

    public long Ail(int i) {
        long j;
        long j2;
        Map map = this.A03;
        Integer valueOf = Integer.valueOf(i);
        if (map.containsKey(valueOf)) {
            Long l = (Long) this.A03.get(valueOf);
            if (l == null) {
                return 2000;
            }
            return l.longValue();
        }
        switch (i) {
            case 5505026:
                j = this.A02.At0(567107482027860L);
                j2 = j * 1000;
                break;
            case 5505027:
                j = this.A02.At0(567107481831249L);
                j2 = j * 1000;
                break;
            case 5505028:
                j = this.A02.At0(567107481896786L);
                j2 = j * 1000;
                break;
            case 5505032:
                j = this.A02.At0(567107481962323L);
                j2 = j * 1000;
                break;
            case 5505081:
            case 5505105:
                j = this.A02.At0(567107481765712L);
                j2 = j * 1000;
                break;
            default:
                j2 = 2000;
                break;
        }
        this.A03.put(valueOf, Long.valueOf(j2));
        return j2;
    }

    public int[] Aip() {
        return this.A04;
    }

    public SparseBooleanArray Ast() {
        if (this.A00 == null) {
            this.A00 = new SparseBooleanArray();
        }
        return this.A00;
    }

    public int[] Azv() {
        return this.A05;
    }

    public boolean BEI(int i) {
        if (this.A01.isSet()) {
            this.A01 = TriState.valueOf(!this.A02.Aem(281788509324002L));
        }
        return this.A01.asBoolean(true);
    }

    private C09230gm(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass0WT.A00(r2);
    }
}
