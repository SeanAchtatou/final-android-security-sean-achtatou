package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0bW  reason: invalid class name and case insensitive filesystem */
public class C06450bW {
    public AnonymousClass0ZF A00;

    public void A02() {
        if (!(this instanceof C06440bV)) {
            AnonymousClass0ZF r4 = this.A00;
            r4.A06 = AnonymousClass16C.A00(AnonymousClass07B.A00) | r4.A06;
            r4.A0E();
        }
    }

    public void A03(String str, AnonymousClass49g r6) {
        if (this instanceof C06440bV) {
            return;
        }
        if (r6 == null) {
            this.A00.A0A(str, null);
            return;
        }
        Object value = r6.getValue();
        if (value instanceof String) {
            this.A00.A0A(str, (String) value);
        } else if (value instanceof Number) {
            this.A00.A09(str, (Number) value);
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0J("Enum type expects String or Number, but got: ", value.toString()));
        }
    }

    public void A04(String str, C73033fO r4) {
        if (this instanceof C06440bV) {
            return;
        }
        if (r4 == null) {
            this.A00.A0A(str, null);
        } else {
            A0D(str, r4.A00);
        }
    }

    public void A05(String str, Boolean bool) {
        if (!(this instanceof C06440bV)) {
            this.A00.A08(str, bool);
        }
    }

    public void A06(String str, Double d) {
        if (!(this instanceof C06440bV)) {
            this.A00.A09(str, d);
        }
    }

    public void A07(String str, Float f) {
        if (!(this instanceof C06440bV)) {
            this.A00.A09(str, f);
        }
    }

    public void A08(String str, Integer num) {
        if (!(this instanceof C06440bV)) {
            this.A00.A09(str, num);
        }
    }

    public void A09(String str, Long l) {
        if (!(this instanceof C06440bV)) {
            this.A00.A09(str, l);
        }
    }

    public void A0A(String str, Object obj) {
        if (this instanceof C06440bV) {
            return;
        }
        if (obj == null) {
            this.A00.A0A(str, null);
        } else if (obj instanceof Number) {
            this.A00.A09(str, (Number) obj);
        } else if (obj instanceof String) {
            this.A00.A0A(str, (String) obj);
        } else if (obj instanceof Map) {
            A0D(str, (Map) obj);
        } else if (obj instanceof Set) {
            A0E(str, (Set) obj);
        } else if (obj instanceof List) {
            A0C(str, (List) obj);
        } else {
            throw new RuntimeException(AnonymousClass08S.A0J("Unsupported type: ", obj.toString()));
        }
    }

    public void A0B(String str, String str2) {
        if (!(this instanceof C06440bV)) {
            this.A00.A0A(str, str2);
        }
    }

    public void A0C(String str, List list) {
        if (this instanceof C06440bV) {
            return;
        }
        if (list == null) {
            this.A00.A0A(str, null);
        } else {
            A00(this.A00.A0B().A0E(str), list);
        }
    }

    public void A0D(String str, Map map) {
        if (this instanceof C06440bV) {
            return;
        }
        if (map == null) {
            this.A00.A0A(str, null);
        } else {
            A01(this.A00.A0B().A0F(str), map);
        }
    }

    public void A0E(String str, Set set) {
        if (this instanceof C06440bV) {
            return;
        }
        if (set == null) {
            this.A00.A0A(str, null);
        } else {
            A00(this.A00.A0B().A0E(str), set);
        }
    }

    public boolean A0F() {
        if (!(this instanceof C06440bV)) {
            return this.A00.A0G();
        }
        return false;
    }

    public C06450bW(AnonymousClass0ZF r1) {
        this.A00 = r1;
    }

    private void A00(C16910xz r5, Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (next != null) {
                if (next instanceof AnonymousClass2SH) {
                    next = ((AnonymousClass2SH) next).A00();
                }
                if (next instanceof Number) {
                    C16910xz.A00(r5, (Number) next);
                } else if (next instanceof Boolean) {
                    C16910xz.A00(r5, (Boolean) next);
                } else if (next instanceof String) {
                    C16910xz.A00(r5, (String) next);
                } else if (next instanceof Map) {
                    A01(r5.A0F(), (Map) next);
                } else if (next instanceof Collection) {
                    A00(r5.A0E(), (Collection) next);
                } else if (next instanceof C73033fO) {
                    A01(r5.A0F(), ((C73033fO) next).A00);
                } else if (next instanceof AnonymousClass49g) {
                    Object value = ((AnonymousClass49g) next).getValue();
                    if (value instanceof String) {
                        C16910xz.A00(r5, (String) value);
                    } else if (value instanceof Number) {
                        C16910xz.A00(r5, (Number) value);
                    } else {
                        throw new IllegalArgumentException(AnonymousClass08S.A0J("Enum type expects String and Number, but got: ", next.toString()));
                    }
                } else {
                    throw new RuntimeException(AnonymousClass08S.A0J("Unsupported type: ", next.toString()));
                }
            }
        }
    }

    private void A01(C12240os r6, Map map) {
        for (Object next : map.keySet()) {
            Object obj = map.get(next);
            if (obj != null) {
                if (obj instanceof AnonymousClass2SH) {
                    obj = ((AnonymousClass2SH) obj).A00();
                }
                if (obj instanceof Number) {
                    r6.A0J(next.toString(), (Number) obj);
                } else if (obj instanceof Boolean) {
                    C12240os.A01(r6, next.toString(), (Boolean) obj);
                } else if (obj instanceof String) {
                    r6.A0K(next.toString(), (String) obj);
                } else if (obj instanceof Map) {
                    A01(r6.A0F(next.toString()), (Map) obj);
                } else if (obj instanceof Collection) {
                    A00(r6.A0E(next.toString()), (Collection) obj);
                } else if (obj instanceof C73033fO) {
                    A01(r6.A0F(next.toString()), ((C73033fO) obj).A00);
                } else if (obj instanceof AnonymousClass49g) {
                    Object value = ((AnonymousClass49g) obj).getValue();
                    if (value instanceof String) {
                        r6.A0K(next.toString(), (String) value);
                    } else if (value instanceof Number) {
                        r6.A0J(next.toString(), (Number) value);
                    } else {
                        throw new IllegalArgumentException(AnonymousClass08S.A0J("Enum type expects String and Number, but got: ", obj.toString()));
                    }
                } else {
                    throw new RuntimeException(AnonymousClass08S.A0J("Unsupported type: ", next.toString()));
                }
            }
        }
    }
}
