package X;

/* renamed from: X.1wv  reason: invalid class name and case insensitive filesystem */
public final class C38121wv implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.video.heroplayer.service.liveprefetcher.DashLiveSegmentPrefetcher$SimplePrefetchTask";
    private final int A00;
    public final /* synthetic */ ABR A01;

    public C38121wv(ABR abr, int i) {
        this.A01 = abr;
        this.A00 = i;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:134|135|136|137|(1:139)|140) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:162|163|164|165|166|167|168|(2:170|171)(3:174|175|176)) */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x02f0, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x02f1, code lost:
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x007b, code lost:
        if (r1.A01.A0J == false) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0087, code lost:
        if (r1.A01.A0K == false) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0093, code lost:
        if (r1.A01.A0L == false) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009f, code lost:
        if (r1.A01.A0H == false) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ec, code lost:
        if (r11.A0B == false) goto L_0x00ee;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:136:0x02df */
    /* JADX WARNING: Missing exception handler attribute for start block: B:167:0x030e */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x02e5 A[Catch:{ all -> 0x02f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0314 A[Catch:{ IOException -> 0x02fe, all -> 0x0323 }] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x031f A[SYNTHETIC, Splitter:B:174:0x031f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r52 = this;
        L_0x0000:
            r4 = r52
            X.ABR r0 = r4.A01     // Catch:{ InterruptedException -> 0x0433 }
            java.util.concurrent.BlockingQueue r0 = r0.A0L     // Catch:{ InterruptedException -> 0x0433 }
            java.lang.Object r0 = r0.take()     // Catch:{ InterruptedException -> 0x0433 }
            X.1x7 r0 = (X.C38241x7) r0     // Catch:{ InterruptedException -> 0x0433 }
            X.AD1 r1 = r0.A03     // Catch:{ InterruptedException -> 0x0433 }
            android.net.Uri r1 = r1.A02     // Catch:{ InterruptedException -> 0x0433 }
            r51 = r1
            X.ABR r2 = r4.A01
            boolean r1 = r2.A0D
            if (r1 == 0) goto L_0x002c
            X.ABz r3 = r2.A0I
            X.9Pd r1 = r0.A04
            java.lang.String r2 = r1.A03
            r1 = r51
            byte[] r1 = r3.A01(r2, r1)
            if (r1 == 0) goto L_0x002c
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r0.A00(r1)
            goto L_0x0000
        L_0x002c:
            java.lang.Integer r2 = r0.A01
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            if (r2 != r1) goto L_0x0000
            r17 = 1
            r16 = 0
            X.A9G r3 = new X.A9G     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3.<init>()     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.A9I r5 = new X.A9I     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.ABR r1 = r4.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.ABn r2 = r1.A02     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r1 = 0
            r5.<init>(r1, r2)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AAZ r1 = r5.A00     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3.A00(r1)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.ABO r1 = r0.A06     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.ABd r1 = r1.A01()     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AAe r6 = r0.A05     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r8 = 1
            if (r6 == 0) goto L_0x00bf
            X.A9E r5 = new X.A9E     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.9Pd r11 = r0.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r20 = 0
            r23 = 1
            java.lang.String r10 = r0.A08     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r25 = 0
            X.A85 r2 = X.A85.A00     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r26 = r2.A01()     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r27 = 0
            java.lang.String r9 = r0.A07     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AD1 r2 = r0.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.Integer r7 = r2.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r30 = 0
            r31 = 0
            if (r1 == 0) goto L_0x007d
            X.AC6 r2 = r1.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r2 = r2.A0J     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r32 = 1
            if (r2 != 0) goto L_0x007f
        L_0x007d:
            r32 = 0
        L_0x007f:
            if (r1 == 0) goto L_0x0089
            X.AC6 r2 = r1.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r2 = r2.A0K     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r33 = 1
            if (r2 != 0) goto L_0x008b
        L_0x0089:
            r33 = 0
        L_0x008b:
            if (r1 == 0) goto L_0x0095
            X.AC6 r2 = r1.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r2 = r2.A0L     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r34 = 1
            if (r2 != 0) goto L_0x0097
        L_0x0095:
            r34 = 0
        L_0x0097:
            if (r1 == 0) goto L_0x00a1
            X.AC6 r2 = r1.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r2 = r2.A0H     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r35 = 1
            if (r2 != 0) goto L_0x00a3
        L_0x00a1:
            r35 = 0
        L_0x00a3:
            X.ABR r2 = r4.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r2 = r2.A08     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r37 = 0
            r38 = 0
            r24 = r10
            r28 = r9
            r29 = r7
            r36 = r2
            r18 = r5
            r19 = r11
            r22 = r6
            r18.<init>(r19, r20, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3.A00(r5)     // Catch:{ IOException | RuntimeException -> 0x0363 }
        L_0x00bf:
            X.ErM r2 = r0.A02     // Catch:{ IOException | RuntimeException -> 0x0363 }
            if (r2 == 0) goto L_0x00c8
            X.ErL r2 = r2.A0Z     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3.A00(r2)     // Catch:{ IOException | RuntimeException -> 0x0363 }
        L_0x00c8:
            X.AD1 r2 = r0.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.Integer r2 = r2.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            if (r2 != 0) goto L_0x00d0
            java.lang.Integer r2 = X.AnonymousClass07B.A00     // Catch:{ IOException | RuntimeException -> 0x0363 }
        L_0x00d0:
            int r28 = X.C74813iX.A00(r2)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.1wq r2 = new X.1wq     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.9Pd r10 = r0.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.ABR r11 = r4.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r9 = r11.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.ABz r7 = r11.A0I     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.A7z r6 = r3.A00     // Catch:{ IOException | RuntimeException -> 0x0363 }
            if (r1 == 0) goto L_0x00ee
            X.AC6 r1 = r1.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r1 = r1.A0L     // Catch:{ IOException | RuntimeException -> 0x0363 }
            if (r1 == 0) goto L_0x00ee
            boolean r1 = r11.A0B     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r25 = 1
            if (r1 != 0) goto L_0x00f0
        L_0x00ee:
            r25 = 0
        L_0x00f0:
            boolean r5 = r11.A0G     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r1 = r11.A06     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r18 = r2
            X.A85 r24 = X.A85.A00     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r19 = r10
            r20 = r9
            r21 = r7
            r22 = r3
            r23 = r6
            r26 = r5
            r27 = r1
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.A8a r1 = new X.A8a     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AD1 r3 = r0.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            android.net.Uri r12 = r3.A02     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r41 = 0
            r48 = 0
            X.A8b r20 = new X.A8b     // Catch:{ IOException | RuntimeException -> 0x0363 }
            com.google.android.exoplayer2.Format r5 = r3.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r11 = r5.A0O     // Catch:{ IOException | RuntimeException -> 0x0363 }
            int r5 = r5.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            long r6 = (long) r5     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r24 = 0
            int r10 = r3.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            int r9 = r3.A00     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r27 = -1
            r29 = 0
            r30 = 0
            r31 = -1
            r32 = -1
            X.A8Z r5 = new X.A8Z     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.9Pd r3 = r0.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r3 = r3.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r5.<init>(r3, r8)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r35 = 0
            r37 = 0
            r38 = -1
            r40 = 0
            r22 = r6
            r25 = r10
            r26 = r9
            r34 = r5
            r21 = r11
            r20.<init>(r21, r22, r24, r25, r26, r27, r28, r29, r30, r31, r32, r34, r35, r37, r38, r40)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r42 = 0
            r44 = 0
            r46 = -1
            r49 = 0
            r39 = r1
            r40 = r12
            r50 = r20
            r39.<init>(r40, r41, r42, r44, r46, r48, r49, r50)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.ABR r3 = r4.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            boolean r3 = r3.A09     // Catch:{ IOException | RuntimeException -> 0x0363 }
            if (r3 == 0) goto L_0x01a4
            X.AD1 r3 = r0.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            com.google.android.exoplayer2.Format r3 = r3.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r5 = r3.A0O     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3 = 649(0x289, float:9.1E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r3)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r1.A01(r3, r5)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AD1 r3 = r0.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            com.google.android.exoplayer2.Format r3 = r3.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            int r3 = r3.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r5 = java.lang.Integer.toString(r3)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3 = 648(0x288, float:9.08E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r3)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r1.A01(r3, r5)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r5 = "1"
            java.lang.String r3 = "x-fb-abr-is-prefetch"
            r1.A01(r3, r5)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r3 = "x-fb-abr-is-live"
            r1.A01(r3, r5)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AD1 r3 = r0.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.Integer r3 = r3.A04     // Catch:{ IOException | RuntimeException -> 0x0363 }
            int r3 = X.C74813iX.A00(r3)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r5 = java.lang.Integer.toString(r3)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3 = 650(0x28a, float:9.11E-43)
            java.lang.String r3 = X.AnonymousClass80H.$const$string(r3)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r1.A01(r3, r5)     // Catch:{ IOException | RuntimeException -> 0x0363 }
        L_0x01a4:
            monitor-enter(r2)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.1wr r8 = new X.1wr     // Catch:{ all -> 0x0360 }
            X.9Pd r3 = r2.A06     // Catch:{ all -> 0x0360 }
            java.lang.String r5 = r3.A03     // Catch:{ all -> 0x0360 }
            android.net.Uri r3 = r1.A04     // Catch:{ all -> 0x0360 }
            r8.<init>(r5, r3)     // Catch:{ all -> 0x0360 }
            X.ACJ r7 = X.C38071wq.A0F     // Catch:{ all -> 0x0360 }
            monitor-enter(r7)     // Catch:{ all -> 0x0360 }
            X.9QQ r3 = r7.A00(r8)     // Catch:{ all -> 0x035d }
            if (r3 != 0) goto L_0x0332
            X.ABz r6 = r2.A03     // Catch:{ all -> 0x035d }
            X.9Pd r3 = r2.A06     // Catch:{ all -> 0x035d }
            java.lang.String r5 = r3.A03     // Catch:{ all -> 0x035d }
            android.net.Uri r3 = r1.A04     // Catch:{ all -> 0x035d }
            byte[] r3 = r6.A01(r5, r3)     // Catch:{ all -> 0x035d }
            if (r3 != 0) goto L_0x0332
            X.A9V r3 = new X.A9V     // Catch:{ all -> 0x035d }
            X.A85 r12 = r2.A04     // Catch:{ all -> 0x035d }
            java.lang.String r11 = r2.A08     // Catch:{ all -> 0x035d }
            X.A8b r5 = r1.A05     // Catch:{ all -> 0x035d }
            boolean r6 = r5.A0E     // Catch:{ all -> 0x035d }
            boolean r5 = r2.A0A     // Catch:{ all -> 0x035d }
            if (r5 == 0) goto L_0x01d6
            goto L_0x01d9
        L_0x01d6:
            int r21 = X.C38071wq.A0C     // Catch:{ all -> 0x035d }
            goto L_0x01dd
        L_0x01d9:
            if (r6 == 0) goto L_0x01d6
            r21 = 8500(0x2134, float:1.1911E-41)
        L_0x01dd:
            if (r5 == 0) goto L_0x01e4
            if (r6 == 0) goto L_0x01e4
            r22 = 8500(0x2134, float:1.1911E-41)
            goto L_0x01e6
        L_0x01e4:
            int r22 = X.C38071wq.A0D     // Catch:{ all -> 0x035d }
        L_0x01e6:
            X.ABz r10 = r2.A03     // Catch:{ all -> 0x035d }
            X.A9c r9 = r2.A05     // Catch:{ all -> 0x035d }
            X.A7z r6 = r2.A07     // Catch:{ all -> 0x035d }
            java.lang.String r26 = X.C38071wq.A0E     // Catch:{ all -> 0x035d }
            boolean r5 = r2.A09     // Catch:{ all -> 0x035d }
            r20 = r11
            r23 = r10
            r24 = r9
            r25 = r6
            r27 = r5
            r18 = r3
            r19 = r12
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27)     // Catch:{ all -> 0x035d }
            r7.A02(r8, r3)     // Catch:{ all -> 0x035d }
            monitor-exit(r7)     // Catch:{ all -> 0x035d }
            r5 = 1
            X.A8a r5 = X.C38071wq.A00(r2, r1, r5)     // Catch:{ IOException -> 0x0329 }
            X.9Pd r10 = r2.A06     // Catch:{ IOException -> 0x0329 }
            android.net.Uri r11 = r1.A04     // Catch:{ IOException -> 0x0329 }
            X.A9c r6 = r3.A01     // Catch:{ IOException -> 0x0329 }
            if (r6 == 0) goto L_0x0217
            X.3iW r1 = X.C74803iW.NOT_CACHED     // Catch:{ IOException -> 0x0329 }
            r6.Bsn(r5, r1)     // Catch:{ IOException -> 0x0329 }
        L_0x0217:
            monitor-enter(r3)     // Catch:{ IOException -> 0x0329 }
            r9 = 0
            r3.A04 = r9     // Catch:{ all -> 0x0326 }
            r13 = -1
            r3.A01 = r13     // Catch:{ all -> 0x0326 }
            r3.A00 = r13     // Catch:{ all -> 0x0326 }
            r3.A00 = r13     // Catch:{ all -> 0x0326 }
            r1 = r17
            r3.A06 = r1     // Catch:{ all -> 0x0326 }
            monitor-exit(r3)     // Catch:{ all -> 0x0326 }
            X.A85 r1 = r3.A0B     // Catch:{ IOException -> 0x0329 }
            r18 = r1
            java.lang.String r15 = r3.A0C     // Catch:{ IOException -> 0x0329 }
            X.A9c r14 = r3.A01     // Catch:{ IOException -> 0x0329 }
            X.A7z r12 = r3.A03     // Catch:{ IOException -> 0x0329 }
            int r6 = r3.A08     // Catch:{ IOException -> 0x0329 }
            int r1 = r3.A09     // Catch:{ IOException -> 0x0329 }
            r19 = r15
            r20 = r14
            r21 = r12
            r22 = r6
            r23 = r1
            r24 = r10
            X.A9S r12 = r18.A00(r19, r20, r21, r22, r23, r24)     // Catch:{ IOException -> 0x0329 }
            java.lang.String r1 = r3.A03     // Catch:{ IOException -> 0x0329 }
            if (r1 == 0) goto L_0x0262
            java.lang.String r6 = r11.getHost()     // Catch:{ IOException -> 0x0329 }
            java.lang.String r1 = "m-livestream-lookaside.facebook.com"
            boolean r1 = r6.equals(r1)     // Catch:{ IOException -> 0x0329 }
            if (r1 == 0) goto L_0x0262
            java.lang.String r6 = "OAuth "
            java.lang.String r1 = r3.A03     // Catch:{ IOException -> 0x0329 }
            java.lang.String r6 = X.AnonymousClass08S.A0J(r6, r1)     // Catch:{ IOException -> 0x0329 }
            java.lang.String r1 = "Authorization"
            r12.CBO(r1, r6)     // Catch:{ IOException -> 0x0329 }
        L_0x0262:
            long r5 = r12.BvJ(r5)     // Catch:{ IOException -> 0x02fe }
            monitor-enter(r3)     // Catch:{ IOException -> 0x02fe }
            X.A9c r1 = r3.A01     // Catch:{ all -> 0x02fb }
            r3.A01(r1)     // Catch:{ all -> 0x02fb }
            int r14 = (int) r5     // Catch:{ all -> 0x02fb }
            r3.A01 = r14     // Catch:{ all -> 0x02fb }
            r1 = 0
            if (r14 != r13) goto L_0x0273
            r1 = 1
        L_0x0273:
            r3.A05 = r1     // Catch:{ all -> 0x02fb }
            X.A9S r1 = r3.A02     // Catch:{ all -> 0x02fb }
            if (r1 == 0) goto L_0x0293
            java.util.Map r5 = r1.B1C()     // Catch:{ all -> 0x02fb }
            if (r5 == 0) goto L_0x0293
            java.lang.String r1 = "X-FB-Video-Livehead"
            java.lang.Object r1 = r5.get(r1)     // Catch:{ all -> 0x02fb }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x02fb }
            if (r1 == 0) goto L_0x0293
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x02fb }
            if (r1 != 0) goto L_0x0293
            r1 = r17
            r3.A05 = r1     // Catch:{ all -> 0x02fb }
        L_0x0293:
            r1 = r16
            r3.A00 = r1     // Catch:{ all -> 0x02fb }
            int r1 = r3.A01     // Catch:{ all -> 0x02fb }
            r13 = 1048576(0x100000, float:1.469368E-39)
            if (r1 > r13) goto L_0x02a0
            if (r1 < 0) goto L_0x02a0
            r13 = r1
        L_0x02a0:
            byte[] r6 = new byte[r13]     // Catch:{ all -> 0x02fb }
            r3.A02 = r12     // Catch:{ all -> 0x02fb }
            r3.A04 = r6     // Catch:{ all -> 0x02fb }
            r3.notifyAll()     // Catch:{ all -> 0x02fb }
            monitor-exit(r3)     // Catch:{ all -> 0x02fb }
        L_0x02aa:
            boolean r1 = r3.A06     // Catch:{ IOException -> 0x02fe }
            if (r1 == 0) goto L_0x02d1
            int r5 = r3.A00     // Catch:{ IOException -> 0x02fe }
            if (r5 >= r13) goto L_0x02d1
            int r1 = r13 - r5
            int r5 = r12.read(r6, r5, r1)     // Catch:{ IOException -> 0x02fe }
            monitor-enter(r3)     // Catch:{ IOException -> 0x02fe }
            if (r5 >= 0) goto L_0x02bc
            goto L_0x02c8
        L_0x02bc:
            int r1 = r3.A00     // Catch:{ all -> 0x02ce }
            int r1 = r1 + r5
            r3.A00 = r1     // Catch:{ all -> 0x02ce }
            if (r5 <= 0) goto L_0x02c6
            r3.notifyAll()     // Catch:{ all -> 0x02ce }
        L_0x02c6:
            monitor-exit(r3)     // Catch:{ all -> 0x02ce }
            goto L_0x02aa
        L_0x02c8:
            int r1 = r3.A00     // Catch:{ all -> 0x02ce }
            r3.A01 = r1     // Catch:{ all -> 0x02ce }
            monitor-exit(r3)     // Catch:{ all -> 0x02ce }
            goto L_0x02d1
        L_0x02ce:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02ce }
            goto L_0x02fd
        L_0x02d1:
            monitor-enter(r3)     // Catch:{ IOException -> 0x02fe }
            r1 = r16
            r3.A06 = r1     // Catch:{ all -> 0x02f8 }
            int r5 = r3.A01     // Catch:{ all -> 0x02f8 }
            int r1 = r3.A00     // Catch:{ all -> 0x02f8 }
            if (r5 != r1) goto L_0x02f3
            r12.close()     // Catch:{ IOException -> 0x02df }
        L_0x02df:
            r3.A02 = r9     // Catch:{ all -> 0x02f0 }
            X.ABz r12 = r3.A0A     // Catch:{ all -> 0x02f0 }
            if (r12 == 0) goto L_0x02ee
            java.lang.String r6 = r10.A03     // Catch:{ all -> 0x02f0 }
            byte[] r5 = r3.A04     // Catch:{ all -> 0x02f0 }
            int r1 = r3.A01     // Catch:{ all -> 0x02f0 }
            r12.A00(r6, r11, r5, r1)     // Catch:{ all -> 0x02f0 }
        L_0x02ee:
            r12 = r9
            goto L_0x02f3
        L_0x02f0:
            r1 = move-exception
            r12 = r9
            goto L_0x02f9
        L_0x02f3:
            r3.notifyAll()     // Catch:{ all -> 0x02f8 }
            monitor-exit(r3)     // Catch:{ all -> 0x02f8 }
            goto L_0x031a
        L_0x02f8:
            r1 = move-exception
        L_0x02f9:
            monitor-exit(r3)     // Catch:{ all -> 0x02f8 }
            goto L_0x02fd
        L_0x02fb:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02fb }
        L_0x02fd:
            throw r1     // Catch:{ IOException -> 0x02fe }
        L_0x02fe:
            r5 = move-exception
            X.A9c r1 = r3.A01     // Catch:{ IOException -> 0x0329 }
            if (r1 == 0) goto L_0x0306
            r1.Bsl(r5)     // Catch:{ IOException -> 0x0329 }
        L_0x0306:
            monitor-enter(r3)     // Catch:{ IOException -> 0x0329 }
            r1 = r16
            r3.A06 = r1     // Catch:{ all -> 0x0323 }
            r12.close()     // Catch:{ IOException -> 0x030e }
        L_0x030e:
            r3.A02 = r9     // Catch:{ all -> 0x0323 }
            int r1 = r3.A00     // Catch:{ all -> 0x0323 }
            if (r1 < 0) goto L_0x031f
            r3.A04 = r5     // Catch:{ all -> 0x0323 }
            r3.notifyAll()     // Catch:{ all -> 0x0323 }
            monitor-exit(r3)     // Catch:{ all -> 0x0323 }
        L_0x031a:
            r3.A01 = r9     // Catch:{ IOException -> 0x0329 }
            int r3 = r3.A00     // Catch:{ IOException -> 0x0329 }
            goto L_0x0336
        L_0x031f:
            r3.notifyAll()     // Catch:{ all -> 0x0323 }
            throw r5     // Catch:{ all -> 0x0323 }
        L_0x0323:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0323 }
            goto L_0x0328
        L_0x0326:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0326 }
        L_0x0328:
            throw r1     // Catch:{ IOException -> 0x0329 }
        L_0x0329:
            r1 = move-exception
            monitor-enter(r7)     // Catch:{ all -> 0x0360 }
            r7.A01(r8)     // Catch:{ all -> 0x0330 }
        L_0x032e:
            monitor-exit(r7)     // Catch:{ all -> 0x0330 }
            goto L_0x035f
        L_0x0330:
            r1 = move-exception
            goto L_0x032e
        L_0x0332:
            monitor-exit(r7)     // Catch:{ all -> 0x035d }
            monitor-exit(r2)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r3 = 0
            goto L_0x0337
        L_0x0336:
            monitor-exit(r2)     // Catch:{ IOException | RuntimeException -> 0x0363 }
        L_0x0337:
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ IOException | RuntimeException -> 0x0363 }
            r0.A00(r1)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r7 = "DashLiveSegmentPrefetcher"
            java.lang.String r6 = "[thread=%d] Prefetch is done, fetched: %d, url=%s, cacheKey=%s"
            int r1 = r4.A00     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AD1 r1 = r0.A03     // Catch:{ IOException | RuntimeException -> 0x0363 }
            java.lang.String r2 = r1.A05     // Catch:{ IOException | RuntimeException -> 0x0363 }
            if (r2 != 0) goto L_0x0352
            java.lang.String r2 = "null"
        L_0x0352:
            r1 = r51
            java.lang.Object[] r1 = new java.lang.Object[]{r5, r3, r1, r2}     // Catch:{ IOException | RuntimeException -> 0x0363 }
            X.AnonymousClass8X8.A01(r7, r6, r1)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            goto L_0x0000
        L_0x035d:
            r1 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x035d }
        L_0x035f:
            throw r1     // Catch:{ all -> 0x0360 }
        L_0x0360:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ IOException | RuntimeException -> 0x0363 }
            throw r1     // Catch:{ IOException | RuntimeException -> 0x0363 }
        L_0x0363:
            r5 = move-exception
            boolean r1 = r5 instanceof X.C197189Pf
            if (r1 == 0) goto L_0x03f9
            java.lang.String r3 = "DashLiveSegmentPrefetcher"
            java.lang.Object[] r2 = new java.lang.Object[]{r51}
            java.lang.String r1 = "Invalid response happens while fetching %s"
            X.AnonymousClass8X8.A04(r3, r5, r1, r2)
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r0.A00(r1)
            X.9Pf r5 = (X.C197189Pf) r5
            int r2 = r5.responseCode
            r1 = 404(0x194, float:5.66E-43)
            if (r2 == r1) goto L_0x039b
            r1 = 410(0x19a, float:5.75E-43)
            if (r2 != r1) goto L_0x0000
            X.AAe r1 = r0.A05
            if (r1 == 0) goto L_0x0000
            X.8Sr r2 = new X.8Sr
            X.9Pd r0 = r0.A04
            java.lang.String r3 = r0.A03
            r6 = 0
            r7 = -1
            r4 = 0
            r5 = 410(0x19a, float:5.75E-43)
            r2.<init>(r3, r4, r5, r6, r7)
            r1.A00(r2)
            goto L_0x0000
        L_0x039b:
            X.ABR r1 = r4.A01
            boolean r1 = r1.A0E
            if (r1 == 0) goto L_0x0000
            java.util.concurrent.atomic.AtomicBoolean r4 = r0.A0A
            monitor-enter(r4)
            java.util.concurrent.atomic.AtomicBoolean r2 = r0.A0A     // Catch:{ all -> 0x0430 }
            r1 = 1
            boolean r1 = r2.getAndSet(r1)     // Catch:{ all -> 0x0430 }
            if (r1 != 0) goto L_0x03f6
            java.util.Collection r1 = r0.A09     // Catch:{ all -> 0x0430 }
            java.util.Iterator r3 = r1.iterator()     // Catch:{ all -> 0x0430 }
        L_0x03b3:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x0430 }
            if (r1 == 0) goto L_0x03c5
            java.lang.Object r2 = r3.next()     // Catch:{ all -> 0x0430 }
            X.1x7 r2 = (X.C38241x7) r2     // Catch:{ all -> 0x0430 }
            java.lang.Integer r1 = X.AnonymousClass07B.A0N     // Catch:{ all -> 0x0430 }
            r2.A00(r1)     // Catch:{ all -> 0x0430 }
            goto L_0x03b3
        L_0x03c5:
            X.ABO r5 = r0.A06     // Catch:{ all -> 0x0430 }
            java.lang.Object r3 = r5.A0F     // Catch:{ all -> 0x0430 }
            monitor-enter(r3)     // Catch:{ all -> 0x0430 }
            java.lang.Integer r1 = r5.A0M     // Catch:{ all -> 0x042d }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x042d }
            if (r1 != r0) goto L_0x03f5
            int r0 = r5.A03     // Catch:{ all -> 0x042d }
            if (r0 <= 0) goto L_0x03f5
            java.util.concurrent.atomic.AtomicInteger r0 = r5.A0H     // Catch:{ all -> 0x042d }
            int r0 = r0.intValue()     // Catch:{ all -> 0x042d }
            if (r0 <= 0) goto L_0x03f5
            java.util.concurrent.atomic.AtomicInteger r0 = r5.A0H     // Catch:{ all -> 0x042d }
            int r0 = r0.decrementAndGet()     // Catch:{ all -> 0x042d }
            if (r0 < 0) goto L_0x03f5
            java.lang.Integer r0 = X.AnonymousClass07B.A0i     // Catch:{ all -> 0x042d }
            r5.A0M = r0     // Catch:{ all -> 0x042d }
            android.os.Handler r2 = r5.A08     // Catch:{ all -> 0x042d }
            X.ABJ r1 = new X.ABJ     // Catch:{ all -> 0x042d }
            r1.<init>(r5)     // Catch:{ all -> 0x042d }
            r0 = -865772510(0xffffffffcc655c22, float:-6.012532E7)
            X.AnonymousClass00S.A04(r2, r1, r0)     // Catch:{ all -> 0x042d }
        L_0x03f5:
            monitor-exit(r3)     // Catch:{ all -> 0x042d }
        L_0x03f6:
            monitor-exit(r4)     // Catch:{ all -> 0x0430 }
            goto L_0x0000
        L_0x03f9:
            int r1 = r0.A00
            int r1 = r1 + -1
            r0.A00 = r1
            if (r1 <= 0) goto L_0x041b
            java.lang.String r3 = "DashLiveSegmentPrefetcher"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            r1 = r51
            java.lang.Object[] r2 = new java.lang.Object[]{r1, r2}
            java.lang.String r1 = "Error happens while fetching %s retry remain: %d"
            X.AnonymousClass8X8.A04(r3, r5, r1, r2)
            X.ABR r1 = r4.A01
            java.util.concurrent.BlockingQueue r1 = r1.A0L
            r1.add(r0)
            goto L_0x0000
        L_0x041b:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r0.A00(r1)
            java.lang.String r2 = "DashLiveSegmentPrefetcher"
            java.lang.Object[] r1 = new java.lang.Object[]{r51}
            java.lang.String r0 = "Error happens while fetching %s"
            X.AnonymousClass8X8.A04(r2, r5, r0, r1)
            goto L_0x0000
        L_0x042d:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x042d }
            throw r0     // Catch:{ all -> 0x0430 }
        L_0x0430:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0430 }
            throw r0
        L_0x0433:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38121wv.run():void");
    }
}
