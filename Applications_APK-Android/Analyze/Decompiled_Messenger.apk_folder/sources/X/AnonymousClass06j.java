package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.06j  reason: invalid class name */
public abstract class AnonymousClass06j {
    public final C007306v A00;

    public boolean A02(Context context, Object obj, Intent intent) {
        return A03(context, obj, intent, null);
    }

    public abstract void A04(Object obj, Intent intent);

    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0171, code lost:
        if (r1.equals(r15.getClass().getName()) != false) goto L_0x0173;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0035, code lost:
        if (r1.matcher(r15.getClass().getName()).matches() != false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x018c, code lost:
        if (r0 == false) goto L_0x018e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0190, code lost:
        if (r1 != false) goto L_0x0193;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x008d, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008c, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00fa, code lost:
        if (r4.equals("scheme") == false) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x014b, code lost:
        if (r0 == false) goto L_0x014d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0154  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(android.content.Context r19, java.lang.Object r20, android.content.Intent r21, X.C06630bp r22) {
        /*
            r18 = this;
            r14 = r18
            X.06v r0 = r14.A00
            boolean r0 = r0.CDp()
            if (r0 == 0) goto L_0x01a0
            X.06v r0 = r14.A00
            X.1ZT[] r12 = r0.AjO()
            r15 = r20
            r13 = r21
            if (r12 == 0) goto L_0x0147
            int r11 = r12.length
            if (r11 <= 0) goto L_0x0147
            r10 = 0
        L_0x001a:
            if (r10 >= r11) goto L_0x0147
            r9 = r12[r10]
            r8 = r22
            java.util.regex.Pattern r1 = r9.A03
            if (r1 == 0) goto L_0x0037
            java.lang.Class r0 = r15.getClass()
            java.lang.String r0 = r0.getName()
            java.util.regex.Matcher r0 = r1.matcher(r0)
            boolean r1 = r0.matches()
            r0 = 0
            if (r1 == 0) goto L_0x0038
        L_0x0037:
            r0 = 1
        L_0x0038:
            if (r0 == 0) goto L_0x014d
            r0 = 0
            X.0bo r4 = X.C13360rJ.A00(r13, r0)     // Catch:{ JSONException -> 0x0145 }
            X.8BA r3 = r9.A00     // Catch:{ JSONException -> 0x0145 }
            if (r3 == 0) goto L_0x0076
            if (r4 == 0) goto L_0x014d
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0145 }
            r2.<init>()     // Catch:{ JSONException -> 0x0145 }
            int r1 = r4.A00     // Catch:{ JSONException -> 0x0145 }
            java.lang.String r0 = "caller_uid"
            r2.put(r0, r1)     // Catch:{ JSONException -> 0x0145 }
            java.lang.String r1 = r4.A01()     // Catch:{ JSONException -> 0x0145 }
            if (r1 == 0) goto L_0x005c
            java.lang.String r0 = "caller_package_name"
            r2.put(r0, r1)     // Catch:{ JSONException -> 0x0145 }
        L_0x005c:
            java.lang.String r1 = r4.A03     // Catch:{ JSONException -> 0x0145 }
            if (r1 == 0) goto L_0x0065
            java.lang.String r0 = "caller_version_name"
            r2.put(r0, r1)     // Catch:{ JSONException -> 0x0145 }
        L_0x0065:
            java.lang.String r1 = r4.A02     // Catch:{ JSONException -> 0x0145 }
            if (r1 == 0) goto L_0x006e
            java.lang.String r0 = "caller_domain"
            r2.put(r0, r1)     // Catch:{ JSONException -> 0x0145 }
        L_0x006e:
            boolean r0 = r3.A01(r2)     // Catch:{ JSONException -> 0x0145 }
            if (r0 != 0) goto L_0x0076
            goto L_0x014d
        L_0x0076:
            if (r22 != 0) goto L_0x007d
            r0 = 0
            X.0bp r8 = X.C49822cv.A00(r13, r0, r0)     // Catch:{ JSONException -> 0x0145 }
        L_0x007d:
            r7 = 0
            if (r8 != 0) goto L_0x0082
            r1 = r7
        L_0x0081:
            goto L_0x0085
        L_0x0082:
            java.util.List r1 = r8.A00     // Catch:{ JSONException -> 0x0145 }
            goto L_0x0081
        L_0x0085:
            X.0bq r0 = r9.A02     // Catch:{ JSONException -> 0x0145 }
            if (r0 == 0) goto L_0x008f
            if (r1 == 0) goto L_0x0132
            goto L_0x0096
        L_0x008c:
            r0 = 0
        L_0x008d:
            if (r0 == 0) goto L_0x00a0
        L_0x008f:
            r0 = 1
        L_0x0090:
            if (r0 == 0) goto L_0x014d
            if (r8 == 0) goto L_0x0137
            goto L_0x0135
        L_0x0096:
            boolean r0 = r1.isEmpty()     // Catch:{ JSONException -> 0x0145 }
            if (r0 != 0) goto L_0x0132
            java.util.Iterator r17 = r1.iterator()     // Catch:{ JSONException -> 0x0145 }
        L_0x00a0:
            boolean r0 = r17.hasNext()     // Catch:{ JSONException -> 0x0145 }
            if (r0 == 0) goto L_0x0132
            java.lang.Object r6 = r17.next()     // Catch:{ JSONException -> 0x0145 }
            android.net.Uri r6 = (android.net.Uri) r6     // Catch:{ JSONException -> 0x0145 }
            X.0bq r0 = r9.A02     // Catch:{ JSONException -> 0x0145 }
            java.util.Map r0 = r0.A00     // Catch:{ JSONException -> 0x0145 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ JSONException -> 0x0145 }
            java.util.Iterator r16 = r0.iterator()     // Catch:{ JSONException -> 0x0145 }
        L_0x00b8:
            boolean r0 = r16.hasNext()     // Catch:{ JSONException -> 0x0145 }
            if (r0 == 0) goto L_0x012f
            java.lang.Object r5 = r16.next()     // Catch:{ JSONException -> 0x0145 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ JSONException -> 0x0145 }
            java.lang.Object r4 = r5.getKey()     // Catch:{ JSONException -> 0x0145 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ JSONException -> 0x0145 }
            int r0 = r4.hashCode()     // Catch:{ JSONException -> 0x0145 }
            r3 = 3
            r2 = 2
            r1 = 1
            switch(r0) {
                case -907987547: goto L_0x00f3;
                case 3433509: goto L_0x00e9;
                case 107944136: goto L_0x00df;
                case 1475610435: goto L_0x00d5;
                default: goto L_0x00d4;
            }     // Catch:{ JSONException -> 0x0145 }
        L_0x00d4:
            goto L_0x00fc
        L_0x00d5:
            java.lang.String r0 = "authority"
            boolean r4 = r4.equals(r0)     // Catch:{ JSONException -> 0x0145 }
            r0 = 1
            if (r4 != 0) goto L_0x00fd
            goto L_0x00fc
        L_0x00df:
            java.lang.String r0 = "query"
            boolean r4 = r4.equals(r0)     // Catch:{ JSONException -> 0x0145 }
            r0 = 3
            if (r4 != 0) goto L_0x00fd
            goto L_0x00fc
        L_0x00e9:
            java.lang.String r0 = "path"
            boolean r4 = r4.equals(r0)     // Catch:{ JSONException -> 0x0145 }
            r0 = 2
            if (r4 != 0) goto L_0x00fd
            goto L_0x00fc
        L_0x00f3:
            java.lang.String r0 = "scheme"
            boolean r4 = r4.equals(r0)     // Catch:{ JSONException -> 0x0145 }
            r0 = 0
            if (r4 != 0) goto L_0x00fd
        L_0x00fc:
            r0 = -1
        L_0x00fd:
            if (r0 == 0) goto L_0x0118
            if (r0 == r1) goto L_0x0113
            if (r0 == r2) goto L_0x010e
            if (r0 == r3) goto L_0x0109
            r1 = 0
        L_0x0106:
            if (r1 == 0) goto L_0x008c
            goto L_0x011d
        L_0x0109:
            java.lang.String r1 = r6.getQuery()     // Catch:{ JSONException -> 0x0145 }
            goto L_0x0106
        L_0x010e:
            java.lang.String r1 = r6.getPath()     // Catch:{ JSONException -> 0x0145 }
            goto L_0x0106
        L_0x0113:
            java.lang.String r1 = r6.getAuthority()     // Catch:{ JSONException -> 0x0145 }
            goto L_0x0106
        L_0x0118:
            java.lang.String r1 = r6.getScheme()     // Catch:{ JSONException -> 0x0145 }
            goto L_0x0106
        L_0x011d:
            java.lang.Object r0 = r5.getValue()     // Catch:{ JSONException -> 0x0145 }
            java.util.regex.Pattern r0 = (java.util.regex.Pattern) r0     // Catch:{ JSONException -> 0x0145 }
            java.util.regex.Matcher r0 = r0.matcher(r1)     // Catch:{ JSONException -> 0x0145 }
            boolean r0 = r0.matches()     // Catch:{ JSONException -> 0x0145 }
            if (r0 != 0) goto L_0x00b8
            goto L_0x008c
        L_0x012f:
            r0 = 1
            goto L_0x008d
        L_0x0132:
            r0 = 0
            goto L_0x0090
        L_0x0135:
            org.json.JSONObject r7 = r8.A01     // Catch:{ JSONException -> 0x0145 }
        L_0x0137:
            X.8BA r0 = r9.A01     // Catch:{ JSONException -> 0x0145 }
            if (r0 != 0) goto L_0x013c
            goto L_0x0149
        L_0x013c:
            if (r7 != 0) goto L_0x0140
            r0 = 0
            goto L_0x014a
        L_0x0140:
            boolean r0 = r0.A01(r7)     // Catch:{ JSONException -> 0x0145 }
            goto L_0x014a
        L_0x0145:
            r1 = 0
            goto L_0x014e
        L_0x0147:
            r0 = 0
            goto L_0x0151
        L_0x0149:
            r0 = 1
        L_0x014a:
            r1 = 1
            if (r0 != 0) goto L_0x014e
        L_0x014d:
            r1 = 0
        L_0x014e:
            if (r1 == 0) goto L_0x0198
            r0 = 1
        L_0x0151:
            r8 = 0
            if (r0 != 0) goto L_0x019c
            X.06v r0 = r14.A00
            X.09U[] r6 = r0.AqR()
            int r5 = r6.length
            r4 = 0
        L_0x015c:
            if (r4 >= r5) goto L_0x01a0
            r7 = r6[r4]
            java.lang.String r1 = r7.A02
            if (r1 == 0) goto L_0x0173
            java.lang.Class r0 = r15.getClass()
            java.lang.String r0 = r0.getName()
            boolean r1 = r1.equals(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0174
        L_0x0173:
            r0 = 1
        L_0x0174:
            if (r0 == 0) goto L_0x0192
            android.content.IntentFilter r3 = r7.A01
            if (r3 == 0) goto L_0x018e
            android.content.ContentResolver r2 = r7.A00
            r1 = 0
            java.lang.String r0 = "TAG"
            int r0 = r3.match(r2, r13, r8, r0)
            if (r0 <= 0) goto L_0x0186
            r1 = 1
        L_0x0186:
            boolean r0 = r7.A03
            if (r0 == 0) goto L_0x018f
            r0 = r1
            r1 = 0
            if (r0 != 0) goto L_0x018f
        L_0x018e:
            r1 = 1
        L_0x018f:
            r0 = 1
            if (r1 != 0) goto L_0x0193
        L_0x0192:
            r0 = 0
        L_0x0193:
            if (r0 != 0) goto L_0x019c
            int r4 = r4 + 1
            goto L_0x015c
        L_0x0198:
            int r10 = r10 + 1
            goto L_0x001a
        L_0x019c:
            r14.A04(r15, r13)
            return r8
        L_0x01a0:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass06j.A03(android.content.Context, java.lang.Object, android.content.Intent, X.0bp):boolean");
    }

    public AnonymousClass06j(C007306v r1) {
        this.A00 = r1;
    }
}
