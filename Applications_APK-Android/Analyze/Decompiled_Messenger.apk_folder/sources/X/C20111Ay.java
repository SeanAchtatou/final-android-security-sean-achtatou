package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.inbox2.items.InboxTrackableItem;

@UserScoped
/* renamed from: X.1Ay  reason: invalid class name and case insensitive filesystem */
public final class C20111Ay implements AnonymousClass1AZ {
    private static C05540Zi A01;
    private final C19891Ac A00;

    public boolean CEL() {
        return false;
    }

    public static final C20111Ay A00(AnonymousClass1XY r4) {
        C20111Ay r0;
        synchronized (C20111Ay.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C20111Ay((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C20111Ay) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public C33901oK Apl() {
        return C33901oK.A0M;
    }

    public void BMG(C24201Sr r4) {
        C19891Ac r2 = this.A00;
        r2.A01.add(Long.valueOf(((InboxTrackableItem) r4.A05).ArZ()));
    }

    public void BMH(C24201Sr r4) {
        C19891Ac r2 = this.A00;
        r2.A01.remove(Long.valueOf(((InboxTrackableItem) r4.A05).ArZ()));
    }

    public void C8h(boolean z) {
        C19891Ac r1 = this.A00;
        r1.A01.isEmpty();
        r1.A00 = z;
        for (C111205Rn A002 : r1.A02) {
            A002.A00(z);
        }
    }

    public void CKW(C24201Sr r4, boolean z) {
        InboxTrackableItem inboxTrackableItem = (InboxTrackableItem) r4.A05;
        if (z) {
            C19891Ac r2 = this.A00;
            r2.A01.add(Long.valueOf(inboxTrackableItem.ArZ()));
            return;
        }
        C19891Ac r22 = this.A00;
        r22.A01.remove(Long.valueOf(inboxTrackableItem.ArZ()));
    }

    private C20111Ay(AnonymousClass1XY r2) {
        this.A00 = C19891Ac.A00(r2);
    }
}
