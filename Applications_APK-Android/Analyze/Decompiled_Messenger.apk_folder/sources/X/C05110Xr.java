package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Throwables;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* renamed from: X.0Xr  reason: invalid class name and case insensitive filesystem */
public final class C05110Xr implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.init.impl.FbAppInitializerInternal$1";
    public final /* synthetic */ AnonymousClass0Y7 A00;
    public final /* synthetic */ AnonymousClass0VL A01;

    public C05110Xr(AnonymousClass0Y7 r1, AnonymousClass0VL r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void run() {
        ArrayList<Future> arrayList;
        AnonymousClass0Y7 r8 = this.A00;
        AnonymousClass0VL r7 = this.A01;
        long now = ((AnonymousClass069) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BBa, r8.A01)).now();
        C005505z.A03("FbAppInitializer-HiPri", 1606865405);
        try {
            C005505z.A03("initializeGatekeeperStore", -511272236);
            AnonymousClass0Y7.A05(r8, r7);
            C005505z.A00(-391955865);
            if (AnonymousClass0Y7.A07(r8)) {
                C005505z.A03("HiPri-execute-tasks-parallel", -370640697);
                try {
                    int i = AnonymousClass1Y3.AfY;
                    AnonymousClass0UN r5 = r8.A01;
                    arrayList = new ArrayList<>(((C04830Wi) AnonymousClass1XX.A02(7, i, r5)).BKI() + ((C04830Wi) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AnK, r5)).BKI());
                    ListenableFuture CIC = r7.CIC(new C05440Yy(r8));
                    C04830Wi r9 = (C04830Wi) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AfY, r8.A01);
                    int BKI = r9.BKI();
                    for (int i2 = 0; i2 < BKI; i2++) {
                        arrayList.add(r7.CIC(new AnonymousClass0YM(r8, r9, "INeedInit.NeedsHighPriorityInitOnBackgroundThreadWithoutSharedPref")));
                    }
                    C07360dI.A00(CIC);
                } catch (ExecutionException e) {
                    Throwables.throwIfUnchecked(e.getCause());
                } catch (Throwable th) {
                    th = th;
                    C005505z.A00(1702611828);
                    throw th;
                }
                C04830Wi r52 = (C04830Wi) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AnK, r8.A01);
                int BKI2 = r52.BKI();
                for (int i3 = 0; i3 < BKI2; i3++) {
                    arrayList.add(r7.CIC(new AnonymousClass0YM(r8, r52, "INeedInit.HighPriorityInitOnBackgroundThread")));
                }
                for (Future A002 : arrayList) {
                    try {
                        C07360dI.A00(A002);
                    } catch (ExecutionException e2) {
                        Throwables.throwIfUnchecked(e2.getCause());
                    }
                }
                C005505z.A00(-2043044403);
            } else {
                AnonymousClass0Y7.A02(r8);
                AnonymousClass0Y7.A04(r8, (C04830Wi) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AfY, r8.A01), "INeedInit.NeedsHighPriorityInitOnBackgroundThreadWithoutSharedPref");
                AnonymousClass0Y7.A04(r8, (C04830Wi) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AnK, r8.A01), "INeedInit.HighPriorityInitOnBackgroundThread");
            }
            C005505z.A01(-741127437);
            long now2 = ((AnonymousClass069) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BBa, r8.A01)).now() - now;
            QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BBd, r8.A01);
            quickPerformanceLogger.setAlwaysOnSampleRate(3997697, 5);
            synchronized (r8.A09) {
                quickPerformanceLogger.markerGenerateWithAnnotations(3997697, 2, (int) now2, r8.A09);
            }
            AnonymousClass0Y7.A01(r8);
            ((AnonymousClass0YZ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AZQ, this.A00.A01)).BM0();
        } catch (Throwable th2) {
            C005505z.A01(1786377667);
            throw th2;
        }
    }
}
