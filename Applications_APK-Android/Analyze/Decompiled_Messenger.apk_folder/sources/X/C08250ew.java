package X;

import android.app.Activity;
import android.app.ActivityThread;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0ew  reason: invalid class name and case insensitive filesystem */
public final class C08250ew implements Application.ActivityLifecycleCallbacks {
    public static C08250ew A02;
    private WeakReference A00 = new WeakReference(null);
    private final C08020eY[] A01;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
        Activity activity2 = (Activity) this.A00.get();
        this.A00 = new WeakReference(activity);
        if (activity2 == null) {
            for (C08020eY AOG : this.A01) {
                AOG.AOG();
            }
        }
    }

    public void onActivityStopped(Activity activity) {
        Activity activity2 = (Activity) this.A00.get();
        if (activity2 == null || activity2 == activity) {
            for (C08020eY AOF : this.A01) {
                AOF.AOF();
            }
            this.A00 = new WeakReference(null);
        }
    }

    public C08250ew(Context context, List list) {
        Application application = null;
        Context applicationContext = context.getApplicationContext();
        this.A01 = (C08020eY[]) list.toArray(new C08020eY[list.size()]);
        if ((applicationContext instanceof Application) || ((applicationContext = applicationContext.getApplicationContext()) != null && (applicationContext instanceof Application))) {
            application = (Application) applicationContext;
        }
        if (application != null) {
            ActivityThread activityThread = AnonymousClass00d.A00;
            if (activityThread == null) {
                activityThread = ActivityThread.currentActivityThread();
                AnonymousClass00d.A00 = activityThread;
            }
            Activity activity = null;
            if (activityThread != null) {
                try {
                    Field declaredField = ActivityThread.class.getDeclaredField("mActivities");
                    declaredField.setAccessible(true);
                    Map map = (Map) declaredField.get(activityThread);
                    if (map != null && !map.isEmpty()) {
                        Iterator it = map.values().iterator();
                        C26521ba r2 = null;
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Object next = it.next();
                            r2 = r2 == null ? new C26521ba(next) : r2;
                            if (!r2.A02.getBoolean(next)) {
                                activity = (Activity) r2.A01.get(next);
                                break;
                            }
                        }
                    }
                } catch (Error | Exception unused) {
                }
            }
            this.A00 = new WeakReference(activity);
            application.registerActivityLifecycleCallbacks(this);
        }
    }
}
