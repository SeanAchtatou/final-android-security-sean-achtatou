package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.collect.ImmutableSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1ae  reason: invalid class name and case insensitive filesystem */
public abstract class C25941ae implements AnonymousClass0ZM {
    public AnonymousClass1Y7 A00;
    public AnonymousClass1Y7 A01;
    public Set A02;
    public AtomicBoolean A03 = new AtomicBoolean();
    private final AnonymousClass0US A04;

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a5, code lost:
        if (r3 != null) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00aa, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(com.facebook.prefs.shared.FbSharedPreferences r15, X.AnonymousClass1Y7 r16, java.lang.Object r17) {
        /*
            r14 = this;
            r8 = r17
            boolean r0 = r14 instanceof X.C05710aC
            r2 = r16
            if (r0 != 0) goto L_0x0119
            boolean r0 = r14 instanceof X.C05800aL
            if (r0 != 0) goto L_0x00ab
            boolean r0 = r14 instanceof X.C26021am
            if (r0 != 0) goto L_0x0035
            X.9Ae r8 = (X.C194379Ae) r8
            r3 = 0
            boolean r0 = r15.Aep(r2, r3)
            if (r0 == 0) goto L_0x0034
            r2 = 5400000(0x5265c0, float:7.567012E-39)
            int r1 = X.AnonymousClass1Y3.AXl
            X.0UN r0 = r8.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.25S r5 = (X.AnonymousClass25S) r5
            java.lang.Class<com.facebook.http.common.DelayEmpathyDelayWorker> r4 = com.facebook.http.common.DelayEmpathyDelayWorker.class
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.SECONDS
            long r1 = (long) r2
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r0 = r3.convert(r1, r0)
            r5.A02(r4, r0)
        L_0x0034:
            return
        L_0x0035:
            X.5Ae r8 = (X.C106965Ae) r8
            com.facebook.messaging.model.threadkey.ThreadKey r4 = X.C05690aA.A00(r2)
            if (r4 == 0) goto L_0x0086
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r4)
            if (r0 != 0) goto L_0x0034
            X.00z r1 = r8.A01
            X.00z r0 = X.C001500z.A08
            if (r1 == r0) goto L_0x004c
            X.0il r3 = X.C09830il.A00
            goto L_0x006b
        L_0x004c:
            X.0aY r3 = r8.A00
            X.0Zo r2 = new X.0Zo
            r2.<init>()
            r0 = 1
            r2.A07 = r0
            long r0 = r4.A04
            java.lang.String r0 = java.lang.Long.toString(r0)
            r2.A05 = r0
            java.lang.String r0 = ""
            r2.A01 = r0
            com.facebook.auth.viewercontext.ViewerContext r0 = new com.facebook.auth.viewercontext.ViewerContext
            r0.<init>(r2)
            X.0il r3 = r3.Byt(r0)
        L_0x006b:
            X.0Tq r0 = r8.A03     // Catch:{ all -> 0x00a2 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00a2 }
            X.5AY r0 = (X.AnonymousClass5AY) r0     // Catch:{ all -> 0x00a2 }
            java.util.concurrent.ExecutorService r2 = r0.A02     // Catch:{ all -> 0x00a2 }
            X.5Aa r1 = new X.5Aa     // Catch:{ all -> 0x00a2 }
            r1.<init>(r0, r4)     // Catch:{ all -> 0x00a2 }
            r0 = -27444411(0xfffffffffe5d3b45, float:-7.3516783E37)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ all -> 0x00a2 }
            if (r3 == 0) goto L_0x0034
            r3.close()
            return
        L_0x0086:
            X.1Y7 r0 = X.C05690aA.A0z
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0034
            X.2sx r4 = r8.A02
            java.util.concurrent.ExecutorService r3 = r4.A02
            X.5Ad r2 = new X.5Ad
            java.lang.Class r1 = X.C57982sx.A03
            java.lang.String r0 = "synchronizeAfterClientChange"
            r2.<init>(r4, r1, r0)
            r0 = 1974573901(0x75b19b4d, float:4.5028634E32)
            X.AnonymousClass07A.A04(r3, r2, r0)
            return
        L_0x00a2:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00a4 }
        L_0x00a4:
            r0 = move-exception
            if (r3 == 0) goto L_0x00aa
            r3.close()     // Catch:{ all -> 0x00aa }
        L_0x00aa:
            throw r0
        L_0x00ab:
            X.4i5 r8 = (X.C96034i5) r8
            com.facebook.prefs.shared.FbSharedPreferences r2 = r8.A04
            X.1Y7 r1 = X.C26211b5.A06
            r0 = 0
            boolean r0 = r2.Aep(r1, r0)
            if (r0 == 0) goto L_0x0100
            r9 = 3600000(0x36ee80, double:1.7786363E-317)
            X.069 r0 = r8.A03
            long r6 = r0.now()
            long r4 = r6 + r9
            X.06B r0 = r8.A02
            long r2 = r0.now()
            long r2 = r2 + r9
            X.25T r11 = r8.A01
            X.069 r0 = r8.A03
            long r0 = r0.now()
            long r0 = r0 + r9
            android.content.Context r13 = r8.A00
            android.content.Intent r12 = new android.content.Intent
            java.lang.String r9 = "com.facebook.messaging.trafficcontrol.ACTION_CANCEL_EMPATHY"
            r12.<init>(r9)
            r10 = 0
            r9 = 134217728(0x8000000, float:3.85186E-34)
            android.app.PendingIntent r10 = X.C64633Cq.A01(r13, r10, r12, r9)
            r9 = 3
            r11.A03(r9, r0, r10)
            com.facebook.prefs.shared.FbSharedPreferences r0 = r8.A04
            X.1hn r1 = r0.edit()
            X.1Y7 r0 = X.C26211b5.A02
            r1.BzA(r0, r6)
            X.1Y7 r0 = X.C26211b5.A03
            r1.BzA(r0, r4)
            X.1Y7 r0 = X.C26211b5.A04
            r1.BzA(r0, r2)
            r1.commit()
            return
        L_0x0100:
            X.25T r4 = r8.A01
            android.content.Context r3 = r8.A00
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r0 = "com.facebook.messaging.trafficcontrol.ACTION_CANCEL_EMPATHY"
            r2.<init>(r0)
            r1 = 0
            r0 = 134217728(0x8000000, float:3.85186E-34)
            android.app.PendingIntent r0 = X.C64633Cq.A01(r3, r1, r2, r0)
            r4.A06(r0)
            X.C96034i5.A01(r8)
            return
        L_0x0119:
            X.0aD r8 = (X.C05720aD) r8
            r0 = 1
            boolean r0 = r15.Aep(r2, r0)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            monitor-enter(r8)
            X.C05720aD.A02(r8)     // Catch:{ all -> 0x012f }
            java.util.Map r0 = r8.A02     // Catch:{ all -> 0x012f }
            r0.put(r2, r1)     // Catch:{ all -> 0x012f }
            monitor-exit(r8)
            return
        L_0x012f:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25941ae.A01(com.facebook.prefs.shared.FbSharedPreferences, X.1Y7, java.lang.Object):void");
    }

    public final void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r3) {
        A01(fbSharedPreferences, r3, this.A04.get());
    }

    public C25941ae(AnonymousClass0US r2, AnonymousClass1Y7 r3, Integer num) {
        switch (num.intValue()) {
            case 0:
                this.A04 = r2;
                this.A00 = r3;
                return;
            case 1:
                this.A04 = r2;
                this.A01 = r3;
                return;
            default:
                throw new IllegalArgumentException();
        }
    }

    public C25941ae(AnonymousClass0US r2, Set set) {
        this.A04 = r2;
        this.A02 = ImmutableSet.A0A(set);
    }
}
