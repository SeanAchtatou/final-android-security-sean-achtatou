package X;

import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.orca.threadlist.ThreadListFragment;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.1AP  reason: invalid class name */
public final class AnonymousClass1AP extends AnonymousClass1A9 {
    public final /* synthetic */ ThreadListFragment A00;

    public void A07(RecyclerView recyclerView, int i) {
        if (i == 0) {
            ThreadListFragment threadListFragment = this.A00;
            threadListFragment.A1K = ((C19911Ae) AnonymousClass1XX.A02(20, AnonymousClass1Y3.AOb, threadListFragment.A0O)).A02(true);
            threadListFragment.A0M.disable();
            if (threadListFragment.A0K != null) {
                String str = threadListFragment.A1K;
                if (str != null) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, threadListFragment.A0O)).markerAnnotate(5505081, "thread_list_types", str);
                }
                threadListFragment.A0K.A01();
            }
        } else if (i == 1) {
            this.A00.A0M.enable();
            this.A00.A0K.A02();
            ((C19911Ae) AnonymousClass1XX.A02(20, AnonymousClass1Y3.AOb, this.A00.A0O)).A01.A09();
        }
        if (!this.A00.A1a && Build.VERSION.SDK_INT <= 19) {
            if (i == 0) {
                AnonymousClass08D.A02(1);
            } else {
                AnonymousClass08D.A01(1);
            }
        }
    }

    public AnonymousClass1AP(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }
}
