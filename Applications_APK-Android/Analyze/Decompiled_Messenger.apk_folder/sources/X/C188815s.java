package X;

import android.content.Context;
import com.facebook.cameracore.mediapipeline.arclass.common.ARClassSource;
import com.facebook.cameracore.mediapipeline.dataproviders.framebrightness.interfaces.FrameBrightnessDataProviderConfig;
import com.facebook.messaging.composer.abtest.ComposerFeature;
import com.google.common.base.Objects;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;

/* renamed from: X.15s  reason: invalid class name and case insensitive filesystem */
public final class C188815s extends C20831Dz {
    public int A00 = 0;
    public int A01 = -1;
    public AnonymousClass0UN A02;
    public AnonymousClass853 A03;
    public ImmutableList A04;
    public ImmutableMap A05;
    public String A06;
    public String A07;
    public boolean A08 = false;
    public final Context A09;
    public final DDV A0A;
    public final DD8 A0B;

    public C188815s(AnonymousClass1XY r21, Context context, C79753rB r23, DDV ddv) {
        AnonymousClass1XY r2 = r21;
        this.A02 = new AnonymousClass0UN(2, r2);
        C83533xm r0 = new C83533xm(r2);
        this.A0A = ddv;
        Context context2 = context;
        this.A09 = context2;
        DD8 dd8 = new DD8(r0, context2, r23);
        this.A0B = dd8;
        if (dd8.A01 == null) {
            AI1 ai1 = new AI1((AnonymousClass88Q) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B47, dd8.A05), C22298Ase.$const$string(86), C188215g.A00().toString(), new C26949DIu(((C179738Si) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AtP, dd8.A05)).A01()));
            dd8.A03 = ai1;
            AnonymousClass8N1 r3 = new AnonymousClass8N1(new BC9(dd8));
            r3.A00 = ai1;
            DDB A012 = ((AnonymousClass8N0) AnonymousClass1XX.A02(14, AnonymousClass1Y3.BTl, dd8.A05)).A01(r3);
            dd8.A01 = A012;
            dd8.A02 = new C27215DXh(A012.A03, (AnonymousClass0V4) AnonymousClass1XX.A02(12, AnonymousClass1Y3.Awb, dd8.A05));
            int i = AnonymousClass1Y3.AZ5;
            AnonymousClass0UN r1 = dd8.A05;
            C38771xy A032 = ((C206069nS) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AWc, r1)).A03();
            Context context3 = dd8.A0C;
            String productSessionId = dd8.A03.getProductSessionId();
            int i2 = AnonymousClass1Y3.AZG;
            AnonymousClass0UN r22 = dd8.A05;
            ARClassSource A022 = ((C206069nS) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AWc, r22)).A02();
            int i3 = AnonymousClass1Y3.Amr;
            AnonymousClass0UN r12 = dd8.A05;
            C27112DRs A023 = ((AnonymousClass2DC) AnonymousClass1XX.A02(7, i, r1)).A02(A032, new DS5(productSessionId, (AnonymousClass831) AnonymousClass1XX.A02(0, i2, r22), (DSC) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ApI, r22), A022, (AnonymousClass09P) AnonymousClass1XX.A02(6, i3, r12), C206069nS.A00((C206069nS) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AWc, r12), true, false, 1), new FrameBrightnessDataProviderConfig(), new C1935995k(context3, C22298Ase.$const$string(86))));
            dd8.A06 = A023;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BZN(A023));
            dd8.A01.A03.A0M(arrayList);
        }
        this.A07 = context2.getResources().getString(2131833727);
        String[] split = ((ComposerFeature) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A85, this.A02)).A01.B4C(851026294997782L).split(",");
        ImmutableMap.Builder builder = ImmutableMap.builder();
        for (String str : split) {
            if (!str.isEmpty()) {
                String[] split2 = str.split(":");
                builder.put(Long.valueOf(split2[0]), split2[1]);
            }
        }
        this.A05 = builder.build();
    }

    public static String A00(C188815s r2, long j) {
        String str = r2.A06;
        if ((str == null || str.isEmpty()) && (str = (String) r2.A05.get(Long.valueOf(j))) == null) {
            str = r2.A07;
        }
        return str.replace("\"", "\\\"");
    }

    public void A0F(String str) {
        if (str != null && !Objects.equal(this.A06, str)) {
            DD8 dd8 = this.A0B;
            dd8.A0E.clear();
            EvictingQueue evictingQueue = dd8.A08;
            if (evictingQueue != null) {
                evictingQueue.clear();
            }
            EvictingQueue evictingQueue2 = dd8.A07;
            if (evictingQueue2 != null) {
                evictingQueue2.clear();
            }
            this.A06 = str;
            A04();
        }
    }

    public int ArU() {
        if (this.A04 != null) {
            return Integer.MAX_VALUE;
        }
        return 0;
    }
}
