package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.0ww  reason: invalid class name and case insensitive filesystem */
public final class C16370ww extends Animation {
    public final /* synthetic */ C20871Ed A00;

    public C16370ww(C20871Ed r1) {
        this.A00 = r1;
    }

    public void applyTransformation(float f, Transformation transformation) {
        this.A00.A09(f);
    }
}
