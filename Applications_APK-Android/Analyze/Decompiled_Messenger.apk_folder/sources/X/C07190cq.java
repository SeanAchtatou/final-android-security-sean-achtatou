package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0cq  reason: invalid class name and case insensitive filesystem */
public abstract class C07190cq {
    private static final char[] A00 = "0123456789abcdef".toCharArray();

    public int A00() {
        C25441Zq r5 = (C25441Zq) this;
        int length = r5.bytes.length;
        boolean z = false;
        if (length >= 4) {
            z = true;
        }
        Preconditions.checkState(z, "HashCode#asInt() requires >= 4 bytes (it only has %s bytes).", length);
        byte[] bArr = r5.bytes;
        return ((bArr[3] & 255) << 24) | ((bArr[1] & 255) << 8) | (bArr[0] & 255) | ((bArr[2] & 255) << 16);
    }

    public int A01() {
        return ((C25441Zq) this).bytes.length << 3;
    }

    public long A02() {
        C25441Zq r3 = (C25441Zq) this;
        int length = r3.bytes.length;
        boolean z = false;
        if (length >= 8) {
            z = true;
        }
        Preconditions.checkState(z, "HashCode#asLong() requires >= 8 bytes (it only has %s bytes).", length);
        byte[] bArr = r3.bytes;
        long j = (long) (bArr[0] & 255);
        for (int i = 1; i < Math.min(bArr.length, 8); i++) {
            j |= (((long) bArr[i]) & 255) << (i << 3);
        }
        return j;
    }

    public boolean A03(C07190cq r7) {
        C25441Zq r5 = (C25441Zq) this;
        boolean z = false;
        if (r5.bytes.length == r7.A04().length) {
            int i = 0;
            z = true;
            while (true) {
                byte[] bArr = r5.bytes;
                if (i >= bArr.length) {
                    break;
                }
                boolean z2 = false;
                if (bArr[i] == r7.A04()[i]) {
                    z2 = true;
                }
                z &= z2;
                i++;
            }
        }
        return z;
    }

    public abstract byte[] A05();

    public byte[] A04() {
        if (!(this instanceof C25441Zq)) {
            return A05();
        }
        return ((C25441Zq) this).bytes;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C07190cq)) {
            return false;
        }
        C07190cq r4 = (C07190cq) obj;
        if (A01() != r4.A01() || !A03(r4)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        if (A01() >= 32) {
            return A00();
        }
        byte[] A04 = A04();
        byte b = A04[0] & 255;
        for (int i = 1; i < A04.length; i++) {
            b |= (A04[i] & 255) << (i << 3);
        }
        return b;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(r4 << 1);
        for (byte b : A04()) {
            char[] cArr = A00;
            sb.append(cArr[(b >> 4) & 15]);
            sb.append(cArr[b & 15]);
        }
        return sb.toString();
    }
}
