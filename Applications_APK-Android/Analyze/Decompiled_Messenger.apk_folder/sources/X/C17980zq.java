package X;

import com.facebook.messaging.model.threads.GroupThreadAssociatedObject;
import com.facebook.messaging.model.threads.GroupThreadData;
import com.facebook.messaging.model.threads.JoinableInfo;
import com.facebook.messaging.model.threads.SyncedGroupData;
import com.facebook.workshared.syncedgroups.model.WorkSyncGroupModelData;

/* renamed from: X.0zq  reason: invalid class name and case insensitive filesystem */
public final class C17980zq {
    public long A00;
    public long A01;
    public GroupThreadAssociatedObject A02;
    public AnonymousClass104 A03 = AnonymousClass104.NONE;
    public JoinableInfo A04 = new JoinableInfo(new C28681fC());
    public SyncedGroupData A05;
    public WorkSyncGroupModelData A06;
    public String A07;
    public String A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;

    public void A00(GroupThreadData groupThreadData) {
        this.A07 = groupThreadData.A07;
        this.A02 = groupThreadData.A02;
        this.A09 = groupThreadData.A0B;
        this.A00 = groupThreadData.A00;
        this.A04 = groupThreadData.A04;
        this.A08 = groupThreadData.A08;
        this.A0A = groupThreadData.A09;
        this.A01 = groupThreadData.A01;
        this.A0B = groupThreadData.A0A;
        this.A03 = groupThreadData.A03;
        this.A05 = groupThreadData.A05;
        this.A06 = groupThreadData.A06;
    }
}
