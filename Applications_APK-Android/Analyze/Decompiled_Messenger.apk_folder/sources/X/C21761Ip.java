package X;

/* renamed from: X.1Ip  reason: invalid class name and case insensitive filesystem */
public final class C21761Ip {
    private static final C16400x0 A00;
    private static final C16400x0 A01;
    private static final C16400x0 A02;
    private static final C16400x0 A03;

    static {
        AnonymousClass10J r0 = C22381Lc.A01;
        AnonymousClass10J r3 = C22381Lc.A00;
        A01 = new C16400x0(r0, r3, r3);
        AnonymousClass10J r1 = AnonymousClass10J.A0G;
        AnonymousClass10J r02 = AnonymousClass10J.A0F;
        A00 = new C16400x0(r1, r02, r02);
        AnonymousClass10J r2 = AnonymousClass10J.A0H;
        AnonymousClass10J r12 = AnonymousClass10J.A0L;
        A03 = new C16400x0(r2, r12, r3);
        A02 = new C16400x0(r2, r12, r12);
    }

    public static C16400x0 A00(Integer num) {
        switch (num.intValue()) {
            case 0:
                return A01;
            case 1:
                return A00;
            case 2:
                return A03;
            case 3:
                return A02;
            default:
                throw new IllegalArgumentException("Unsupported thread item text style param type!");
        }
    }
}
