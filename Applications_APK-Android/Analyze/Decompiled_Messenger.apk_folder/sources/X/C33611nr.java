package X;

import android.content.Context;
import com.facebook.forker.Process;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableSet;
import java.io.File;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1nr  reason: invalid class name and case insensitive filesystem */
public final class C33611nr implements AnonymousClass1YQ, C05460Za {
    private static final Set A08 = ImmutableSet.A05(AnonymousClass0jG.A00, AnonymousClass0jG.A05);
    private static volatile C33611nr A09;
    public Process A00 = null;
    public boolean A01;
    public boolean A02;
    public final Context A03;
    public final File A04;
    private final C05000Xg A05;
    private final FbSharedPreferences A06;
    private final C04310Tq A07;

    private synchronized void A01() {
        this.A01 = false;
        Process process = this.A00;
        if (process != null) {
            process.destroy();
            this.A00 = null;
        }
    }

    public static synchronized void A02(C33611nr r3) {
        synchronized (r3) {
            boolean booleanValue = ((Boolean) r3.A07.get()).booleanValue();
            r3.A01 = booleanValue;
            if (booleanValue) {
                synchronized (r3) {
                    if (r3.A00 == null && !r3.A02) {
                        r3.A02 = true;
                        new Thread(new C182638de(r3), "logcat-manager").start();
                    }
                }
            } else {
                r3.A01();
            }
        }
    }

    public synchronized void clearUserData() {
        A01();
        File[] listFiles = this.A04.listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!"lock".equals(file.getName())) {
                    file.delete();
                }
            }
        }
    }

    public String getSimpleName() {
        return "LogcatFbSdcardLogger";
    }

    public synchronized void init() {
        int A032 = C000700l.A03(184376632);
        this.A06.C0h(A08, new C51332gS(this));
        this.A05.A00(new C51342gT(this), 479);
        A02(this);
        File dir = this.A03.getDir("logcat", 0);
        if (dir.exists()) {
            A03(dir);
        }
        C000700l.A09(-626104124, A032);
    }

    public static final C33611nr A00(AnonymousClass1XY r5) {
        if (A09 == null) {
            synchronized (C33611nr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A09 = new C33611nr(applicationInjector, AnonymousClass1YA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    private C33611nr(AnonymousClass1XY r3, Context context) {
        this.A06 = FbSharedPreferencesModule.A00(r3);
        this.A05 = C04990Xf.A00(r3);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.Ax2, r3);
        this.A03 = context;
        this.A04 = context.getDir("logcat_flash_logs", 0);
    }

    private static void A03(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    A03(file2);
                } else {
                    file2.delete();
                }
            }
        }
        file.delete();
    }
}
