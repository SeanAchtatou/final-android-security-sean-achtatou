package X;

import android.view.View;
import android.view.ViewParent;
import com.facebook.litho.ComponentHost;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0zb  reason: invalid class name and case insensitive filesystem */
public final class C17860zb {
    public C70253aL A00;
    public final C07870eJ A01 = new C07870eJ();
    public final C17600zA A02;
    public final C31421jf A03 = new C31421jf();
    public final C49342c3 A04 = new C49342c3(this);
    public final C49322c1 A05 = new C49322c1(this);
    public final AnonymousClass2PZ A06 = new AnonymousClass2PZ(this);
    public final ArrayList A07 = new ArrayList();
    public final Map A08 = new HashMap();
    public final Map A09 = new HashMap();

    private static void A02(int i) {
        if (i != -1 && i != 0 && i != 1 && i != 2) {
            throw new RuntimeException(AnonymousClass08S.A09("Unknown changeType: ", i));
        }
    }

    public static void A06(C17860zb r2, C17720zM r3, boolean z) {
        Object obj = r3.A01[3];
        if (obj instanceof View) {
            r2.A03((View) obj, z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r0 != false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        if (r0 == false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0038, code lost:
        r7.A05 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003a, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private X.C70253aL A00(X.C17680zI r10, X.C17750zP r11, X.AnonymousClass1M8 r12) {
        /*
            r9 = this;
            X.1jf r0 = r9.A03
            java.util.Map r0 = r0.A02
            java.lang.Object r7 = r0.get(r11)
            X.0zc r7 = (X.C17870zc) r7
            boolean r0 = X.C46182Pb.A00
            if (r0 == 0) goto L_0x0011
            r12.getName()
        L_0x0011:
            r8 = 0
            if (r7 == 0) goto L_0x001c
            X.0zM r0 = r7.A01
            if (r0 != 0) goto L_0x001d
            X.0zM r0 = r7.A03
            if (r0 != 0) goto L_0x001d
        L_0x001c:
            return r8
        L_0x001d:
            int r2 = r7.A00
            A02(r2)
            r6 = 1
            if (r2 != 0) goto L_0x002d
            X.3aD r1 = r10.A03
            r0 = 0
            if (r1 == 0) goto L_0x002b
            r0 = 1
        L_0x002b:
            if (r0 == 0) goto L_0x0038
        L_0x002d:
            r3 = 2
            if (r2 != r3) goto L_0x003b
            X.3aD r1 = r10.A04
            r0 = 0
            if (r1 == 0) goto L_0x0036
            r0 = 1
        L_0x0036:
            if (r0 != 0) goto L_0x003b
        L_0x0038:
            r7.A05 = r6
            return r8
        L_0x003b:
            java.util.Map r0 = r7.A06
            java.lang.Object r5 = r0.get(r12)
            X.3aG r5 = (X.C70203aG) r5
            X.3aH r2 = new X.3aH
            r2.<init>(r11, r12)
            if (r5 == 0) goto L_0x0076
            X.3aT r0 = r5.A01
            float r4 = r0.A00
        L_0x004e:
            int r0 = r7.A00
            if (r0 == r3) goto L_0x006d
            X.0zM r0 = r7.A03
            java.lang.Object r0 = r0.A02()
            X.0zN r0 = (X.C17730zN) r0
            float r3 = r12.Ab1(r0)
        L_0x005e:
            if (r5 == 0) goto L_0x0090
            java.lang.Float r0 = r5.A04
            if (r0 == 0) goto L_0x0090
            float r0 = r0.floatValue()
            int r0 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0095
            return r8
        L_0x006d:
            X.3aD r1 = r10.A04
            X.2PZ r0 = r9.A06
            float r3 = r1.C3j(r0, r2)
            goto L_0x005e
        L_0x0076:
            int r0 = r7.A00
            if (r0 == 0) goto L_0x0087
            X.0zM r0 = r7.A01
            java.lang.Object r0 = r0.A02()
            X.0zN r0 = (X.C17730zN) r0
            float r4 = r12.Ab1(r0)
            goto L_0x004e
        L_0x0087:
            X.3aD r1 = r10.A03
            X.2PZ r0 = r9.A06
            float r4 = r1.C3j(r0, r2)
            goto L_0x004e
        L_0x0090:
            int r0 = (r4 > r3 ? 1 : (r4 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0095
            return r8
        L_0x0095:
            X.3aI r1 = new X.3aI
            r1.<init>(r2, r3)
            X.1R9 r0 = r10.A02
            X.3aK r3 = r0.AUs(r1)
            X.2c1 r1 = r9.A05
            java.util.concurrent.CopyOnWriteArrayList r0 = r3.A00
            r0.add(r1)
            if (r5 != 0) goto L_0x00bc
            X.3aG r5 = new X.3aG
            r5.<init>()
            X.3aT r1 = new X.3aT
            X.0zM r0 = r7.A02
            r1.<init>(r0, r12)
            r5.A01 = r1
            java.util.Map r0 = r7.A06
            r0.put(r12, r5)
        L_0x00bc:
            X.3aT r0 = r5.A01
            r0.A06(r4)
            int r0 = r5.A00
            int r0 = r0 + r6
            r5.A00 = r0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r1.add(r2)
            java.util.Map r0 = r9.A08
            r0.put(r3, r1)
            java.util.Map r1 = r9.A09
            java.lang.Float r0 = java.lang.Float.valueOf(r4)
            r1.put(r2, r0)
            java.lang.String r0 = r10.A05
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00ef
            X.0eJ r2 = r9.A01
            int r1 = r3.hashCode()
            java.lang.String r0 = r10.A05
            r2.A09(r1, r0)
        L_0x00ef:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17860zb.A00(X.0zI, X.0zP, X.1M8):X.3aL");
    }

    public static C70253aL A01(C17860zb r7, C17760zQ r8) {
        C17750zP r0;
        C17750zP r02;
        if (r8 instanceof C17680zI) {
            C17680zI r82 = (C17680zI) r8;
            C31431jg r03 = r82.A01;
            ArrayList arrayList = new ArrayList();
            C17740zO r1 = r03.A00;
            switch (r1.A00.intValue()) {
                case 0:
                case 5:
                    for (C17750zP r12 : r7.A03.A02.keySet()) {
                        if (((C17870zc) r7.A03.A02.get(r12)).A04) {
                            r7.A04(r82, r12, arrayList);
                        }
                    }
                    break;
                case 1:
                    String str = (String) r1.A01;
                    Map map = (Map) r7.A03.A03.get(r82.A00);
                    if (map != null) {
                        r02 = (C17750zP) map.get(str);
                    } else {
                        r02 = null;
                    }
                    r7.A04(r82, r02, arrayList);
                    break;
                case 2:
                    String[] strArr = (String[]) r1.A01;
                    String str2 = r82.A00;
                    for (String str3 : strArr) {
                        Map map2 = (Map) r7.A03.A03.get(str2);
                        if (map2 != null) {
                            r0 = (C17750zP) map2.get(str3);
                        } else {
                            r0 = null;
                        }
                        if (r0 != null) {
                            r7.A04(r82, r0, arrayList);
                        }
                    }
                    break;
                case 3:
                    r7.A04(r82, (C17750zP) r7.A03.A01.get((String) r1.A01), arrayList);
                    break;
                case 4:
                    String[] strArr2 = (String[]) r1.A01;
                    for (String str4 : strArr2) {
                        C17750zP r04 = (C17750zP) r7.A03.A01.get(str4);
                        if (r04 != null) {
                            r7.A04(r82, r04, arrayList);
                        }
                    }
                    break;
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            if (arrayList.size() == 1) {
                return (C70253aL) arrayList.get(0);
            }
            return new C80303sD(0, arrayList);
        } else if (r8 instanceof C17850za) {
            C17850za r83 = (C17850za) r8;
            ArrayList arrayList2 = r83.A00;
            ArrayList arrayList3 = new ArrayList();
            int size = arrayList2.size();
            for (int i = 0; i < size; i++) {
                C70253aL A012 = A01(r7, (C17760zQ) arrayList2.get(i));
                if (A012 != null) {
                    arrayList3.add(A012);
                }
            }
            if (!arrayList3.isEmpty()) {
                return r83.A04(arrayList3);
            }
            return null;
        } else {
            throw new RuntimeException("Unhandled Transition type: " + r8);
        }
    }

    private void A03(View view, boolean z) {
        if (view instanceof ComponentHost) {
            ComponentHost componentHost = (ComponentHost) view;
            if (!z) {
                componentHost.A0F();
            } else if (componentHost.A0F) {
                componentHost.A0F = false;
                componentHost.setClipChildren(componentHost.A0G);
            }
        }
        ViewParent parent = view.getParent();
        if (parent instanceof ComponentHost) {
            A03((View) parent, z);
        }
    }

    private void A04(C17680zI r4, C17750zP r5, ArrayList arrayList) {
        C17690zJ r1 = r4.A01.A01;
        int i = 0;
        switch (r1.A00.intValue()) {
            case 0:
                AnonymousClass1M8[] r12 = (AnonymousClass1M8[]) r1.A01;
                while (i < r12.length) {
                    C70253aL A002 = A00(r4, r5, r12[i]);
                    if (A002 != null) {
                        arrayList.add(A002);
                    }
                    i++;
                }
                return;
            case 1:
                C70253aL A003 = A00(r4, r5, (AnonymousClass1M8) r1.A01);
                if (A003 != null) {
                    arrayList.add(A003);
                    return;
                }
                return;
            case 2:
                break;
            default:
                return;
        }
        while (true) {
            AnonymousClass1M8[] r13 = C17700zK.A07;
            if (i < r13.length) {
                C70253aL A004 = A00(r4, r5, r13[i]);
                if (A004 != null) {
                    arrayList.add(A004);
                }
                i++;
            } else {
                return;
            }
        }
    }

    public static void A05(C17870zc r2) {
        if (r2.A01 != null) {
            r2.A01 = null;
        }
        if (r2.A03 != null) {
            r2.A03 = null;
        }
    }

    public static void A07(C17860zb r6, C17750zP r7, C17720zM r8, C17720zM r9) {
        C17730zN r5;
        Map map;
        C17870zc r3 = (C17870zc) r6.A03.A02.get(r7);
        if (r3 == null) {
            r3 = new C17870zc();
            C31421jf r4 = r6.A03;
            if (r4.A02.put(r7, r3) == null) {
                int i = r7.A00;
                if (i == 1) {
                    map = r4.A01;
                } else if (i == 2) {
                    String str = r7.A01;
                    map = (Map) r4.A03.get(str);
                    if (map == null) {
                        map = new LinkedHashMap();
                        r4.A03.put(str, map);
                    }
                } else if (i == 3) {
                    map = r4.A00;
                } else {
                    throw new RuntimeException(AnonymousClass08S.A09("Unknown TransitionId type ", i));
                }
                map.put(r7.A02, r7);
            }
        }
        if (r8 == null && r9 == null) {
            throw new RuntimeException("Both current and next LayoutOutput groups were null!");
        }
        if (r8 == null && r9 != null) {
            r3.A00 = 0;
        } else if (r8 == null || r9 == null) {
            r3.A00 = 2;
        } else {
            r3.A00 = 1;
        }
        r3.A01 = r8;
        r3.A03 = r9;
        if (r9 != null) {
            r5 = (C17730zN) r9.A02();
        } else {
            r5 = null;
        }
        for (AnonymousClass1M8 r2 : r3.A06.keySet()) {
            C70203aG r1 = (C70203aG) r3.A06.get(r2);
            if (r5 == null) {
                r1.A03 = null;
            } else {
                r1.A03 = Float.valueOf(r2.Ab1(r5));
            }
        }
        r3.A04 = true;
        if (C46182Pb.A00) {
            A02(r3.A00);
        }
    }

    public static void A08(C17860zb r7, C17870zc r8, C17720zM r9) {
        C17720zM r1 = r8.A02;
        if (r1 != null || r9 != null) {
            if (r1 == null || !r1.equals(r9)) {
                Map map = r8.A06;
                if (r1 != null) {
                    for (AnonymousClass1M8 r4 : map.keySet()) {
                        C17720zM r3 = r8.A02;
                        short s = r3.A00;
                        for (int i = 0; i < s; i++) {
                            r4.C3Y(r3.A03(i));
                        }
                    }
                    A06(r7, r8.A02, true);
                }
                for (C70203aG r0 : map.values()) {
                    C70333aT r12 = r0.A01;
                    C70333aT.A02(r12, r9);
                    C70333aT.A01(r12, r12.A00);
                }
                if (r9 != null) {
                    A06(r7, r9, false);
                }
                r8.A02 = r9;
            }
        }
    }

    public void A09(C17750zP r2, C17720zM r3) {
        C17870zc r0 = (C17870zc) this.A03.A02.get(r2);
        if (r0 != null) {
            A08(this, r0, r3);
        }
    }

    public C17860zb(C17600zA r2) {
        this.A02 = r2;
    }
}
