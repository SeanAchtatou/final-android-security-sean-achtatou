package X;

import android.content.Context;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.MapMakerInternalMap;
import java.util.concurrent.ConcurrentMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ws  reason: invalid class name and case insensitive filesystem */
public final class C16330ws implements AnonymousClass1YQ {
    private static volatile C16330ws A09;
    public AnonymousClass0UN A00;
    public ConcurrentMap A01;
    public boolean A02;
    private C06870cD A03;
    public final AnonymousClass09P A04;
    public final C09340h3 A05;
    public final FbSharedPreferences A06;
    private final Context A07;
    private volatile C35021qY A08 = C35021qY.WIFI_UNKNOWN;

    public String getSimpleName() {
        return "DeviceConditionHelper";
    }

    public static final C16330ws A00(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (C16330ws.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new C16330ws(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    public static void A01(C16330ws r3, C35021qY r4) {
        if (r3.A08 != r4) {
            r3.A08 = r4;
            C07410dQ r2 = new C07410dQ();
            synchronized (r3) {
                for (AnonymousClass0j8 A012 : r3.A01.keySet()) {
                    r2.A01(A012);
                }
            }
            C24971Xv it = r2.build().iterator();
            while (it.hasNext()) {
                ((AnonymousClass0j8) it.next()).Bv6(r3);
            }
        }
    }

    public Boolean A02() {
        NetworkInfo A0E = this.A05.A0E();
        if (A0E == null || Build.VERSION.SDK_INT < 3) {
            return null;
        }
        return Boolean.valueOf(A0E.isRoaming());
    }

    public boolean A03(boolean z) {
        C35021qY r0;
        if (this.A02) {
            return false;
        }
        if (z || this.A08 == C35021qY.WIFI_UNKNOWN) {
            NetworkInfo A0E = this.A05.A0E();
            if (A0E == null || A0E.getType() != 1) {
                A01(this, C35021qY.WIFI_OFF);
            } else {
                if (A0E.isConnected()) {
                    r0 = C35021qY.WIFI_ON;
                } else {
                    r0 = C35021qY.WIFI_UNKNOWN;
                }
                A01(this, r0);
            }
        }
        if (this.A08 == C35021qY.WIFI_ON) {
            return true;
        }
        return false;
    }

    private C16330ws(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A07 = AnonymousClass1YA.A00(r3);
        this.A05 = C09340h3.A01(r3);
        this.A06 = FbSharedPreferencesModule.A00(r3);
        this.A04 = C04750Wa.A01(r3);
        C07940eQ r1 = new C07940eQ();
        r1.A06(MapMakerInternalMap.Strength.WEAK);
        this.A01 = r1.A02();
    }

    public void init() {
        int A032 = C000700l.A03(-1538485177);
        AnonymousClass8YI r6 = new AnonymousClass8YI(this);
        AnonymousClass8YJ r8 = new AnonymousClass8YJ(this);
        AnonymousClass8YK r10 = new AnonymousClass8YK(this);
        String $const$string = TurboLoader.Locator.$const$string(3);
        C06870cD r4 = new C06870cD("android.net.wifi.supplicant.CONNECTION_CHANGE", r6, "android.net.wifi.STATE_CHANGE", r8, $const$string, r10);
        this.A03 = r4;
        this.A07.registerReceiver(r4, new IntentFilter("android.net.wifi.supplicant.CONNECTION_CHANGE"));
        this.A07.registerReceiver(this.A03, new IntentFilter("android.net.wifi.STATE_CHANGE"));
        this.A07.registerReceiver(this.A03, new IntentFilter($const$string));
        this.A06.C0f(AnonymousClass8YM.A00, new AnonymousClass8YL(this));
        this.A02 = this.A06.Aep(AnonymousClass8YM.A00, false);
        C000700l.A09(-575468432, A032);
    }
}
