package X;

import android.content.Intent;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.00l  reason: invalid class name and case insensitive filesystem */
public final class C000700l {
    public static int A00(int i) {
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 26, 0, 0, i, 0, 0);
    }

    public static int A01(int i) {
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 30, 0, 0, i, 0, 0);
    }

    public static int A02(int i) {
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 34, 0, 0, i, 0, 0);
    }

    public static int A03(int i) {
        return Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 22, 0, 0, i, 0, 0);
    }

    public static int A04(int i) {
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 28, 0, 0, i, 0, 0);
    }

    public static int A05(int i) {
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 1, 0, 0, i, 0, 0);
    }

    public static int A06(int i) {
        return Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 36, 0, 0, i, 0, 0);
    }

    public static void A07(int i, int i2) {
        Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 27, 0, 0, i, i2, 0);
    }

    public static void A08(int i, int i2) {
        Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 35, 0, 0, i, i2, 0);
    }

    public static void A09(int i, int i2) {
        Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 23, 0, 0, i, i2, 0);
    }

    public static void A0A(int i, int i2) {
        Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 29, 0, 0, i, i2, 0);
    }

    public static void A0B(int i, int i2) {
        Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 2, 0, 0, i, i2, 0);
    }

    public static void A0C(int i, int i2) {
        Logger.writeStandardEntry(AnonymousClass00n.A04, 6, 37, 0, 0, i, i2, 0);
    }

    public static void A0D(Intent intent, int i, int i2) {
        String str;
        int i3 = AnonymousClass00n.A04;
        if (intent != null) {
            str = intent.getAction();
        } else {
            str = null;
        }
        int i4 = i;
        int i5 = i2;
        if (str == null) {
            Logger.writeStandardEntry(i3, 6, 31, 0, 0, i4, i5, 0);
        } else if (TraceEvents.isEnabled(i3)) {
            Logger.writeBytesEntry(i3, 1, 57, Logger.writeBytesEntry(i3, 1, 56, Logger.writeStandardEntry(i3, 7, 31, 0, 0, i4, i5, 0), "Intent action"), str);
        }
    }
}
