package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.messages.Message;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.user.model.UserKey;

/* renamed from: X.1J7  reason: invalid class name */
public final class AnonymousClass1J7 extends C17770zR {
    public static final C22111Jy A09 = C22111Jy.A04;
    public static final AnonymousClass1J0 A0A = AnonymousClass1J0.A09;
    public static final MigColorScheme A0B = C17190yT.A00();
    @Comparable(type = 3)
    public int A00 = 0;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public Message A02;
    @Comparable(type = 13)
    public C22111Jy A03 = A09;
    @Comparable(type = 13)
    public AnonymousClass1J0 A04 = A0A;
    @Comparable(type = 13)
    public MigColorScheme A05 = A0B;
    @Comparable(type = 13)
    public UserKey A06;
    @Comparable(type = 13)
    public String A07;
    @Comparable(type = 3)
    public boolean A08;

    public AnonymousClass1J7(Context context) {
        super("M4MigSingleProfileImage");
        this.A01 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
