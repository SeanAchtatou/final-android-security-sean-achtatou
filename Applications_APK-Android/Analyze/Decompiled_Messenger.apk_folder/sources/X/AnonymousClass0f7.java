package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.samsung.android.os.SemPerfManager;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/* renamed from: X.0f7  reason: invalid class name */
public abstract class AnonymousClass0f7 {
    public static final Queue A08 = new LinkedList();
    public int A00;
    public boolean A01;
    public C08290f0[] A02;
    public final int A03;
    public final Handler A04;
    public final List A05;
    private final C08130ej A06;
    private volatile boolean A07;

    public int A02() {
        if (!(this instanceof C08240eu)) {
            return ((this instanceof C35911s1) || (this instanceof C34631pv)) ? 1 : 10;
        }
        return -1;
    }

    public void A03() {
        if (this instanceof C08240eu) {
            return;
        }
        if (!(this instanceof C35901s0)) {
            ((C26581bg) this).A00.A01();
        } else if (((C35901s0) this).A00) {
            SemPerfManager.onSmoothScrollEvent(false);
        } else {
            SemPerfManager.onScrollEvent(false);
        }
    }

    public boolean A06() {
        if (this instanceof C08240eu) {
            return true;
        }
        if (!(this instanceof C35901s0)) {
            ((C26581bg) this).A00.A02();
            return true;
        } else if (((C35901s0) this).A00) {
            SemPerfManager.onSmoothScrollEvent(true);
            return true;
        } else {
            SemPerfManager.onScrollEvent(true);
            return true;
        }
    }

    public synchronized boolean A08(int i, Integer num) {
        A01(true, true, false, i, num);
        return false;
    }

    private AnonymousClass8TO A00(boolean z, boolean z2, boolean z3, int i, Integer num, boolean z4) {
        AnonymousClass8TO r1;
        Queue queue = A08;
        synchronized (queue) {
            r1 = null;
            if (!queue.isEmpty()) {
                r1 = (AnonymousClass8TO) queue.remove();
            }
        }
        if (r1 == null) {
            r1 = new AnonymousClass8TO();
        }
        int i2 = this.A00;
        r1.A02 = this;
        r1.A07 = z;
        r1.A06 = z2;
        r1.A05 = z3;
        r1.A00 = i2;
        if (i != 0) {
            r1.A03 = num;
            r1.A01 = i;
        }
        r1.A04.set(z4);
        return r1;
    }

    private void A01(boolean z, boolean z2, boolean z3, int i, Integer num) {
        if (this.A07) {
            AnonymousClass00S.A04(this.A04, A00(z, z2, z3, i, num, false), -1846179944);
        }
    }

    public final void A04(C08290f0 r4) {
        synchronized (this.A05) {
            if (!this.A05.contains(r4)) {
                this.A05.add(r4);
                List list = this.A05;
                this.A02 = (C08290f0[]) list.toArray(new C08290f0[list.size()]);
                this.A07 = true;
            }
        }
    }

    public synchronized void A05(boolean z) {
        boolean z2;
        if (!(this instanceof C08240eu)) {
            synchronized (this) {
                synchronized (this) {
                    z2 = this.A01;
                }
                if (z2) {
                    boolean z3 = z;
                    A01(false, true, z3, 0, null);
                    A03();
                    this.A06.A00.removeMessages(0, this);
                    this.A01 = false;
                    A01(false, false, z3, 0, null);
                }
            }
            return;
        }
        synchronized (((C08240eu) this)) {
        }
    }

    public synchronized boolean A07(int i, Integer num) {
        boolean A082;
        boolean z;
        int i2 = i;
        Integer num2 = num;
        if (!(this instanceof C08240eu)) {
            synchronized (this) {
                synchronized (this) {
                    z = this.A01;
                }
                if (z) {
                    boolean A083 = A08(i2, num2);
                    return A083;
                }
                A01(true, true, true, i2, num2);
                this.A00++;
                try {
                    boolean A062 = A06();
                    this.A01 = A062;
                    if (A062) {
                        C08130ej r3 = this.A06;
                        int i3 = this.A03;
                        Message obtain = Message.obtain(r3.A00, 0);
                        obtain.obj = this;
                        r3.A00.sendMessageDelayed(obtain, (long) i3);
                    }
                    A01(true, false, this.A01, i2, num2);
                    boolean z2 = this.A01;
                    return z2;
                } catch (Exception e) {
                    AnonymousClass00S.A04(this.A04, A00(true, false, false, i2, num2, true), -1143204749);
                    throw e;
                }
            }
        } else {
            C08240eu r1 = (C08240eu) this;
            synchronized (r1) {
                A082 = r1.A08(i2, num2);
            }
            return A082;
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public AnonymousClass0f7() {
        this(5000);
        new C26341bI();
    }

    public AnonymousClass0f7(int i) {
        C08130ej r0;
        this.A05 = new ArrayList();
        this.A02 = new C08290f0[0];
        this.A07 = false;
        this.A00 = 0;
        this.A01 = false;
        this.A03 = i;
        synchronized (C08130ej.class) {
            if (C08130ej.A01 == null) {
                Looper mainLooper = Looper.getMainLooper();
                if (C26191b3.A04 == null) {
                    C26191b3.A04 = new C26191b3(null);
                }
                C08130ej.A01 = new C08130ej(mainLooper, C26191b3.A04);
            }
            r0 = C08130ej.A01;
        }
        this.A06 = r0;
        this.A04 = r0.A00;
    }
}
