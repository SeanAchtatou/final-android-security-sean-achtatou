package X;

import com.facebook.yoga.YogaNative;

/* renamed from: X.1KP  reason: invalid class name */
public final class AnonymousClass1KP extends AnonymousClass1KD {
    public void finalize() {
        int A03 = C000700l.A03(-1238630231);
        try {
            long j = this.A00;
            if (j != 0) {
                this.A00 = 0;
                YogaNative.jni_YGConfigFreeJNI(j);
            }
        } finally {
            super.finalize();
            C000700l.A09(184493016, A03);
        }
    }
}
