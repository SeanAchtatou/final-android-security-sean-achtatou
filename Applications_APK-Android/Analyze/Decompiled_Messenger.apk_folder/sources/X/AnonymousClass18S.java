package X;

import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.attachment.ImageData;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.18S  reason: invalid class name */
public final class AnonymousClass18S {
    private static volatile AnonymousClass18S A00;

    public static final AnonymousClass18S A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass18S.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass18S();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static boolean A01(Attachment attachment) {
        ImageData imageData = attachment.A03;
        if (imageData == null || imageData.A02 == null) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r1.startsWith("audio/") == false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A02(com.facebook.messaging.model.attachment.Attachment r3) {
        /*
            java.lang.String r2 = r3.A08
            java.lang.String r1 = r3.A0C
            if (r1 == 0) goto L_0x000f
            java.lang.String r0 = "audio/"
            boolean r1 = r1.startsWith(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x0015
            r0 = 1
            if (r2 != 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18S.A02(com.facebook.messaging.model.attachment.Attachment):boolean");
    }

    public static boolean A03(Attachment attachment) {
        String str = attachment.A0C;
        if (str == null || !str.startsWith(AnonymousClass80H.$const$string(64))) {
            return false;
        }
        return true;
    }
}
