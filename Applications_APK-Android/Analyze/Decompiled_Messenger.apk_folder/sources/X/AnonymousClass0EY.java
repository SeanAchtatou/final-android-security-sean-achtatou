package X;

/* renamed from: X.0EY  reason: invalid class name */
public final class AnonymousClass0EY {
    public static final C02350Ed A00 = new C02340Ec();
    public static final C02350Ed A01 = new C02360Ee();
    private static final C02320Ea A02 = new AnonymousClass0EZ();
    private static final ThreadLocal A03 = new C02330Eb();

    public static C02320Ea A00(long j, C02350Ed r8, String str) {
        if (!AnonymousClass08Z.A05(j)) {
            return A02;
        }
        C02970Hj r5 = (C02970Hj) A03.get();
        r5.A00 = j;
        r5.A02 = r8;
        r5.A03 = str;
        C02980Hk r4 = r5.A01;
        for (int i = 0; i < r4.A00; i++) {
            r4.A01[i] = null;
        }
        r4.A00 = 0;
        return r5;
    }
}
