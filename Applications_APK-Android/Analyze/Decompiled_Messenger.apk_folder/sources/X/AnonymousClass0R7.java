package X;

import android.os.Build;
import android.webkit.WebSettings;

/* renamed from: X.0R7  reason: invalid class name */
public final class AnonymousClass0R7 {
    public static void A00(WebSettings webSettings) {
        webSettings.setAllowFileAccess(false);
        webSettings.setAllowContentAccess(false);
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            webSettings.setAllowFileAccessFromFileURLs(false);
            webSettings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (i >= 21) {
            webSettings.setMixedContentMode(1);
        }
    }
}
