package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.google.common.util.concurrent.SettableFuture;

/* renamed from: X.1ym  reason: invalid class name and case insensitive filesystem */
public final class C39271ym extends AnimatorListenerAdapter {
    public final /* synthetic */ SettableFuture A00;

    public C39271ym(SettableFuture settableFuture) {
        this.A00 = settableFuture;
    }

    public void onAnimationCancel(Animator animator) {
        this.A00.cancel(false);
    }

    public void onAnimationEnd(Animator animator) {
        this.A00.set(null);
    }
}
