package X;

/* renamed from: X.1LZ  reason: invalid class name */
public class AnonymousClass1LZ {
    public int A00() {
        if (!(this instanceof AnonymousClass15N)) {
            return 0;
        }
        return (int) ((AnonymousClass15N) this).A00.At0(564461781975909L);
    }

    public int A01() {
        if (!(this instanceof AnonymousClass15N)) {
            return 0;
        }
        return (int) ((AnonymousClass15N) this).A00.At0(564461782041446L);
    }

    public String A02() {
        if (!(this instanceof AnonymousClass15N)) {
            return null;
        }
        return ((AnonymousClass15N) this).A00.B4C(845936758882510L);
    }

    public boolean A03() {
        if (!(this instanceof AnonymousClass15N)) {
            return true;
        }
        return ((AnonymousClass15N) this).A00.Aem(2306125351773996384L);
    }

    public boolean A04() {
        if (!(this instanceof AnonymousClass15N)) {
            return false;
        }
        return ((AnonymousClass15N) this).A00.Aem(282986805201005L);
    }
}
