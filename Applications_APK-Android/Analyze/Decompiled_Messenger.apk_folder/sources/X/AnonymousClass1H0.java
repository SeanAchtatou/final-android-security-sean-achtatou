package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.user.model.User;
import com.facebook.user.model.WorkUserInfo;

@UserScoped
/* renamed from: X.1H0  reason: invalid class name */
public final class AnonymousClass1H0 {
    private static C05540Zi A02;
    private AnonymousClass0UN A00;
    private final Boolean A01;

    public static final AnonymousClass1H0 A00(AnonymousClass1XY r4) {
        AnonymousClass1H0 r0;
        synchronized (AnonymousClass1H0.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass1H0((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass1H0) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public boolean A01(User user) {
        boolean z;
        String str = (String) AnonymousClass1XX.A03(AnonymousClass1Y3.Awq, this.A00);
        if (user == null || str == null || str.equals(user.A0j) || !user.A1A) {
            return false;
        }
        WorkUserInfo workUserInfo = user.A0R;
        if (workUserInfo != null) {
            z = workUserInfo.A03;
        } else {
            z = user.A1O;
        }
        return !z;
    }

    public boolean A02(User user) {
        if (this.A01.booleanValue() || !A01(user)) {
            return false;
        }
        return true;
    }

    private AnonymousClass1H0(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = AnonymousClass1H1.A02(r3);
    }
}
