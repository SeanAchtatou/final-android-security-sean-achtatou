package X;

import android.util.SparseArray;
import com.facebook.quicklog.PerformanceLoggingEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0i5  reason: invalid class name and case insensitive filesystem */
public final class C09580i5 {
    public C12210op A00;
    public final SparseArray A01 = new SparseArray();
    public final SparseArray A02 = new SparseArray();
    public final C09150gc A03;
    public final Object A04 = new Object();
    public final ArrayList[] A05;
    private final AnonymousClass09O A06;
    private final C08510fT A07;
    private final Object A08 = new Object();
    private final boolean A09;
    private final C08230et[] A0A;
    private volatile Boolean A0B;

    public static int A00(int i, int i2) {
        return i ^ (i2 * 179426549);
    }

    public static int A01(C09580i5 r2) {
        int size;
        synchronized (r2.A01) {
            size = r2.A01.size();
        }
        return size;
    }

    public static SparseArray A02(C09580i5 r14, long j, Ed3 ed3) {
        C08360fE AuU;
        long nowNanos;
        SparseArray sparseArray = null;
        if (r14.A0A != null && j != 0) {
            C12210op r0 = r14.A00;
            if (r0 == null) {
                AuU = null;
            } else {
                AuU = r0.AuU();
            }
            Ed3 ed32 = ed3;
            if (ed3 == null) {
                nowNanos = 0;
            } else {
                nowNanos = r14.A06.nowNanos();
            }
            int i = 0;
            while (true) {
                C08230et[] r1 = r14.A0A;
                if (i >= r1.length) {
                    break;
                }
                C08230et r3 = r1[i];
                if (!((r3.Azh() & j) == 0 || AuU == null || !r3.BEZ(AuU))) {
                    if (sparseArray == null) {
                        sparseArray = new SparseArray();
                    }
                    sparseArray.put(Long.numberOfTrailingZeros(r3.Azh()), r3.CGf());
                    if (ed3 != null) {
                        long nowNanos2 = r14.A06.nowNanos();
                        ed32.A09.add(Integer.valueOf(i));
                        ed32.A0A.add(Long.valueOf(nowNanos2 - nowNanos));
                        nowNanos = nowNanos2;
                    }
                }
                i++;
            }
        }
        return sparseArray;
    }

    public static PerformanceLoggingEvent A03(C09580i5 r5, C04270Tg r6, long j, TimeUnit timeUnit, short s, boolean z, boolean z2, int i) {
        List list;
        ArrayList arrayList;
        PerformanceLoggingEvent performanceLoggingEvent = (PerformanceLoggingEvent) PerformanceLoggingEvent.A0c.A01();
        performanceLoggingEvent.A08 = timeUnit.toNanos(j) - r6.A0C;
        if (r6.A0O) {
            performanceLoggingEvent.A02(r6.A0X);
        } else {
            List A022 = r6.A02();
            if (r6.A0O) {
                list = r6.A0X.A02();
            } else {
                list = r6.A0Z;
            }
            performanceLoggingEvent.A0E(A022, list);
        }
        ArrayList arrayList2 = r6.A0a;
        if (arrayList2 != null && !arrayList2.isEmpty()) {
            performanceLoggingEvent.A0P.addAll(arrayList2);
        }
        C11100lt r1 = r6.A0I;
        r6.A0I = null;
        performanceLoggingEvent.A0J = r1;
        short s2 = (short) (r6.A02 >> 16);
        if (s2 >= 0 && s2 <= 806 && (arrayList = r5.A05[s2]) != null && !arrayList.isEmpty()) {
            performanceLoggingEvent.A0P.addAll(arrayList);
        }
        int i2 = performanceLoggingEvent.A07 & -16711681;
        performanceLoggingEvent.A07 = i2;
        performanceLoggingEvent.A07 = ((i & 255) << 16) | i2;
        performanceLoggingEvent.A06 = r6.A02;
        performanceLoggingEvent.A0R = s;
        performanceLoggingEvent.A0C = r6.A0D;
        performanceLoggingEvent.A0B = r6.A0C;
        performanceLoggingEvent.A02 = r6.A08;
        performanceLoggingEvent.A05 = r6.A06;
        performanceLoggingEvent.A0A = r6.A0A;
        performanceLoggingEvent.A0X = r6.A0Q;
        performanceLoggingEvent.A0Y = r6.A0R;
        performanceLoggingEvent.A0S = r6.A0M;
        performanceLoggingEvent.A09 = timeUnit.toNanos(j) - r6.A0B;
        performanceLoggingEvent.A0M = r6.A0J;
        performanceLoggingEvent.A0V = r6.A0N;
        performanceLoggingEvent.A0F = r6.A0G;
        performanceLoggingEvent.A0G = r6.A0H;
        performanceLoggingEvent.A04 = r6.A05;
        if (z2) {
            SparseArray sparseArray = r6.A0E;
            r6.A0E = null;
            performanceLoggingEvent.A0D = sparseArray;
            SparseArray sparseArray2 = r6.A0F;
            r6.A0F = null;
            performanceLoggingEvent.A0E = sparseArray2;
        } else {
            performanceLoggingEvent.A0D = null;
            performanceLoggingEvent.A0E = null;
        }
        performanceLoggingEvent.A0U = z;
        performanceLoggingEvent.A0N = r6.A0K;
        performanceLoggingEvent.A0Z = r6.A0U;
        performanceLoggingEvent.A0W = r6.A0P;
        return performanceLoggingEvent;
    }

    public static C04270Tg A04(C09580i5 r2, int i) {
        C04270Tg r0;
        synchronized (r2.A01) {
            r0 = (C04270Tg) r2.A01.get(i);
        }
        return r0;
    }

    public static C04270Tg A05(C09580i5 r3, int i) {
        C04270Tg r1;
        synchronized (r3.A01) {
            r1 = (C04270Tg) r3.A01.get(i);
            if (r1 != null) {
                r3.A01.remove(i);
            }
        }
        return r1;
    }

    public static C04270Tg A06(C09580i5 r2, int i) {
        C04270Tg r0;
        synchronized (r2.A01) {
            r0 = (C04270Tg) r2.A01.valueAt(i);
        }
        return r0;
    }

    public static void A07(C09580i5 r2, int i, C04270Tg r4) {
        synchronized (r2.A01) {
            r2.A01.put(i, r4);
        }
    }

    public static void A08(C09580i5 r11, SparseArray sparseArray, long j) {
        C08360fE AuU;
        Object obj;
        if (r11.A0A != null && j != 0) {
            C12210op r0 = r11.A00;
            if (r0 == null) {
                AuU = null;
            } else {
                AuU = r0.AuU();
            }
            for (C08230et r3 : r11.A0A) {
                if (!((r3.Azh() & j) == 0 || AuU == null || !r3.BEZ(AuU))) {
                    int numberOfTrailingZeros = Long.numberOfTrailingZeros(r3.Azh());
                    if (sparseArray != null) {
                        obj = sparseArray.get(numberOfTrailingZeros);
                    } else {
                        obj = null;
                    }
                    r3.CHl(obj);
                }
            }
        }
    }

    public static boolean A09(C09580i5 r2, int i, C09160gd r4) {
        boolean A0A2;
        synchronized (r2.A01) {
            A0A2 = A0A((C04270Tg) r2.A01.get(i), r4);
        }
        return A0A2;
    }

    public static boolean A0A(C04270Tg r1, C09160gd r2) {
        if (r1 == null) {
            return false;
        }
        if (r1.A0P || r2.A02(r1.A02)) {
            return true;
        }
        return false;
    }

    public SparseArray A0B(long j, Ed3 ed3) {
        long nowNanos;
        C08360fE AuU;
        if (this.A0A == null || j == 0) {
            return null;
        }
        SparseArray sparseArray = new SparseArray();
        Ed3 ed32 = ed3;
        if (ed3 == null) {
            nowNanos = 0;
        } else {
            nowNanos = this.A06.nowNanos();
        }
        int i = 0;
        while (true) {
            C08230et[] r1 = this.A0A;
            if (i >= r1.length) {
                return sparseArray;
            }
            C08230et r4 = r1[i];
            C12210op r0 = this.A00;
            if (r0 == null) {
                AuU = null;
            } else {
                AuU = r0.AuU();
            }
            if (!((r4.Azh() & j) == 0 || AuU == null || !r4.BEZ(AuU))) {
                sparseArray.put(Long.numberOfTrailingZeros(r4.Azh()), r4.CGO());
                if (ed3 != null) {
                    long nowNanos2 = this.A06.nowNanos();
                    ed32.A09.add(Integer.valueOf(i));
                    ed32.A0A.add(Long.valueOf(nowNanos2 - nowNanos));
                    nowNanos = nowNanos2;
                }
            }
            i++;
        }
    }

    public String A0C() {
        if (this.A09) {
            return "lockless_1_0";
        }
        return "blocking_2_0";
    }

    public List A0D(int i, long j) {
        ArrayList arrayList = new ArrayList();
        synchronized (this.A04) {
            synchronized (this.A01) {
                int A012 = A01(this);
                for (int i2 = 0; i2 < A012; i2++) {
                    C04270Tg A062 = A06(this, i2);
                    if (A062.A02 == i && (j == -1 || A062.A0C < j)) {
                        arrayList.add(Integer.valueOf(A062.A01));
                    }
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        if (r8.A0B.booleanValue() == false) goto L_0x0010;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0031, code lost:
        r6 = r8.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
        monitor-enter(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r5 = r8.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0038, code lost:
        monitor-enter(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r4 = A01(r8);
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003e, code lost:
        if (r3 >= r4) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0040, code lost:
        A06(r8, r3).A03(com.facebook.common.util.TriState.valueOf(true), false);
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0050, code lost:
        monitor-exit(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        monitor-exit(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0052, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0056, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x005b, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0I() {
        /*
            r8 = this;
            X.02L r0 = X.AnonymousClass02L.A05
            boolean r7 = r0.A05()
            java.lang.Boolean r0 = r8.A0B
            if (r0 == 0) goto L_0x0011
            boolean r0 = r0.booleanValue()
            if (r0 != r7) goto L_0x0011
        L_0x0010:
            return r7
        L_0x0011:
            java.lang.Object r1 = r8.A08
            monitor-enter(r1)
            java.lang.Boolean r0 = r8.A0B     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0022
            java.lang.Boolean r0 = r8.A0B     // Catch:{ all -> 0x0059 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0059 }
            if (r0 != r7) goto L_0x0022
            monitor-exit(r1)     // Catch:{ all -> 0x0059 }
            goto L_0x0035
        L_0x0022:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x0059 }
            r8.A0B = r0     // Catch:{ all -> 0x0059 }
            monitor-exit(r1)     // Catch:{ all -> 0x0059 }
            java.lang.Boolean r0 = r8.A0B
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r6 = r8.A04
            monitor-enter(r6)
            goto L_0x0036
        L_0x0035:
            return r7
        L_0x0036:
            android.util.SparseArray r5 = r8.A01     // Catch:{ all -> 0x0056 }
            monitor-enter(r5)     // Catch:{ all -> 0x0056 }
            int r4 = A01(r8)     // Catch:{ all -> 0x0053 }
            r3 = 0
        L_0x003e:
            if (r3 >= r4) goto L_0x0050
            X.0Tg r2 = A06(r8, r3)     // Catch:{ all -> 0x0053 }
            r0 = 1
            r1 = 0
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)     // Catch:{ all -> 0x0053 }
            r2.A03(r0, r1)     // Catch:{ all -> 0x0053 }
            int r3 = r3 + 1
            goto L_0x003e
        L_0x0050:
            monitor-exit(r5)     // Catch:{ all -> 0x0053 }
            monitor-exit(r6)     // Catch:{ all -> 0x0056 }
            return r7
        L_0x0053:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0053 }
            throw r0     // Catch:{ all -> 0x0056 }
        L_0x0056:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0056 }
            goto L_0x005b
        L_0x0059:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0059 }
        L_0x005b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09580i5.A0I():boolean");
    }

    public C09580i5(C08530fV r2, C09150gc r3, C08230et[] r4, C12210op r5, boolean z, C08510fT r7, AnonymousClass09O r8) {
        this.A0A = r4;
        this.A07 = r7;
        this.A06 = r8;
        this.A05 = new ArrayList[AnonymousClass1Y3.A6c];
        this.A0B = r2.A00().asBooleanObject();
        this.A00 = r5;
        this.A03 = r3;
        this.A09 = z;
    }

    public void A0F(int i, int i2, C08610fe r8, int i3, long j, TimeUnit timeUnit, C09160gd r13, C29549Ed4 ed4) {
        int A002 = A00(i, i2);
        synchronized (this.A04) {
            C04270Tg A052 = A05(this, A002);
            if (A052 != null) {
                SparseArray sparseArray = A052.A0E;
                if (sparseArray != null) {
                    A08(this, sparseArray, A052.A0A);
                }
                A052.A07 = i3;
                A052.A0B = timeUnit.toNanos(j);
                if (ed4 != null) {
                    ed4.A00(A052.A02, A052.A01);
                }
                C08520fU.A07(r8.A00, "markerDropped", i);
                C09220gk.A00(r13.A04, A052, 4);
                C04270Tg.A0c.A02(A052);
            }
        }
    }

    public void A0H(int i, int i2, String str, C09160gd r7) {
        int A002 = A00(i, i2);
        if (A09(this, A002, r7)) {
            synchronized (this.A04) {
                C04270Tg A042 = A04(this, A002);
                if (A0A(A042, r7)) {
                    A042.A0a.add(str);
                }
            }
        }
    }

    public void A0E(int i, int i2, int i3, long j, TimeUnit timeUnit, boolean z, String str, AnonymousClass0lr r46, int i4, SparseArray sparseArray, int i5, C09160gd r50, Ed3 ed3) {
        boolean z2;
        long j2;
        boolean z3;
        TimeUnit timeUnit2;
        long j3;
        int i6;
        C09160gd r14;
        AnonymousClass0lr r24;
        String str2;
        long j4;
        int i7;
        SparseArray sparseArray2 = sparseArray;
        int A002 = A00(i, i2);
        synchronized (this.A04) {
            try {
                C04270Tg A042 = A04(this, A002);
                z2 = false;
                j2 = 0;
                z3 = z;
                timeUnit2 = timeUnit;
                j3 = j;
                i6 = i3;
                r14 = r50;
                r24 = r46;
                str2 = str;
                if (A042 != null) {
                    A042.A07 = i5;
                    if (A0A(A042, r14)) {
                        j4 = timeUnit2.toNanos(j3) - A042.A0C;
                        long j5 = A042.A0A;
                        i7 = A042.A08;
                        if (j5 == 0 || i4 == 0) {
                            TimeUnit timeUnit3 = TimeUnit.NANOSECONDS;
                            long j6 = j4;
                            if (A042.A0I == null) {
                                A042.A0I = new C11100lt();
                            }
                            A042.A0I.A00(j6, timeUnit3, i6, str2, r24, null);
                            A042.A0B = A042.A0C + j4;
                            r14.A01(A042, str2, r24, timeUnit2.toMillis(j3), z3, i6);
                            j2 = j5;
                        } else {
                            z2 = true;
                            j2 = j5;
                        }
                    }
                }
                j4 = 0;
                i7 = 0;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (z2) {
            if (sparseArray == null) {
                sparseArray2 = A0B(j2, ed3);
            }
            synchronized (this.A04) {
                try {
                    C04270Tg A043 = A04(this, A002);
                    if (A043 != null && A043.A08 == i7 && A0A(A043, r14)) {
                        TimeUnit timeUnit4 = TimeUnit.NANOSECONDS;
                        int i8 = i6;
                        String str3 = str2;
                        AnonymousClass0lr r35 = r24;
                        if (A043.A0I == null) {
                            A043.A0I = new C11100lt();
                        }
                        A043.A0I.A00(j4, timeUnit4, i8, str3, r35, sparseArray2);
                        A043.A0B = A043.A0C + j4;
                        r14.A01(A043, str2, r24, timeUnit2.toMillis(j3), z3, i6);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public void A0G(int i, int i2, String str, long j, TimeUnit timeUnit, boolean z, int i3, boolean z2, int i4, int i5, C25401Zm r29, SparseArray sparseArray, C09160gd r31, Ed3 ed3) {
        long j2;
        int indexOfKey;
        C04270Tg r11;
        boolean z3;
        SparseArray sparseArray2 = sparseArray;
        int i6 = i;
        int i7 = i2;
        int A002 = A00(i6, i7);
        C09160gd r10 = r31;
        if (r10.A02(i6)) {
            if (sparseArray != null || !r10.A03(i6)) {
                j2 = 0;
            } else {
                j2 = r29.AuS(i6);
                sparseArray2 = A02(this, j2, ed3);
            }
            synchronized (this.A04) {
                synchronized (this.A01) {
                    indexOfKey = this.A01.indexOfKey(A002);
                }
                boolean z4 = z;
                TimeUnit timeUnit2 = timeUnit;
                long j3 = j;
                if (indexOfKey >= 0) {
                    r11 = A06(this, indexOfKey);
                    r11.A0C = timeUnit2.toNanos(j3);
                    r11.A0V = z4;
                } else {
                    boolean z5 = false;
                    if (!z2) {
                        z5 = true;
                    }
                    String A0C = A0C();
                    C12210op r112 = this.A00;
                    if (r112 == null) {
                        z3 = false;
                    } else {
                        z3 = r112.Bwq();
                    }
                    r11 = (C04270Tg) C04270Tg.A0c.A01();
                    r11.A0O = z3;
                    r11.A02 = i6;
                    r11.A01 = i7;
                    r11.A0C = timeUnit2.toNanos(j3);
                    r11.A0V = z4;
                    r11.A08 = i3;
                    r11.A0T = z5;
                    r11.A09 = 0;
                    r11.A0B = timeUnit2.toNanos(j3);
                    r11.A07 = i4;
                    r11.A0K = A0C;
                    A07(this, A002, r11);
                    r11.A0J = str;
                }
                r11.A0A = j2;
                r11.A0E = sparseArray2;
                r11.A03 = i5;
                C09220gk.A00(r10.A04, r11, 1);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006d, code lost:
        if (r4 == false) goto L_0x00b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006f, code lost:
        r24 = A0B(r0, r41);
        r4 = r4.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0079, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0080, code lost:
        if (r9 == r6.A0b.get()) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0082, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0083, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0084, code lost:
        r20 = java.util.concurrent.TimeUnit.NANOSECONDS;
        r18 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008a, code lost:
        if (r6.A0I != null) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008c, code lost:
        r6.A0I = new X.C11100lt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0093, code lost:
        r21 = r27;
        r6.A0I.A00(r18, r20, r21, r22, r23, r24);
        r6.A0B = r6.A0C + r14;
        r10.A01(r6, r22, r23, r7.toMillis(r2), r26, r21);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b0, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b1, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b2, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b5, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b8, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0J(X.C04270Tg r29, int r30, int r31, long r32, java.util.concurrent.TimeUnit r34, boolean r35, java.lang.String r36, X.AnonymousClass0lr r37, int r38, int r39, X.C09160gd r40, X.Ed3 r41) {
        /*
            r28 = this;
            r0 = r28
            java.lang.Object r8 = r0.A04
            monitor-enter(r8)
            r6 = r29
            java.util.concurrent.atomic.AtomicInteger r0 = r6.A0b     // Catch:{ all -> 0x00b6 }
            int r0 = r0.get()     // Catch:{ all -> 0x00b6 }
            r16 = 0
            r9 = r30
            if (r9 == r0) goto L_0x0015
            monitor-exit(r8)     // Catch:{ all -> 0x00b6 }
            return r16
        L_0x0015:
            r0 = r39
            r6.A07 = r0     // Catch:{ all -> 0x00b6 }
            r7 = r34
            r2 = r32
            long r14 = r7.toNanos(r2)     // Catch:{ all -> 0x00b6 }
            long r0 = r6.A0C     // Catch:{ all -> 0x00b6 }
            long r14 = r14 - r0
            long r0 = r6.A0A     // Catch:{ all -> 0x00b6 }
            r10 = r40
            boolean r4 = A0A(r6, r10)     // Catch:{ all -> 0x00b6 }
            r13 = 1
            r26 = r35
            r27 = r31
            r23 = r37
            r22 = r36
            if (r4 == 0) goto L_0x006b
            r11 = 0
            int r4 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r4 == 0) goto L_0x0041
            if (r38 == 0) goto L_0x0041
            r4 = 1
            goto L_0x006c
        L_0x0041:
            java.util.concurrent.TimeUnit r20 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00b6 }
            r24 = 0
            r18 = r14
            X.0lt r4 = r6.A0I     // Catch:{ all -> 0x00b6 }
            if (r4 != 0) goto L_0x0052
            X.0lt r4 = new X.0lt     // Catch:{ all -> 0x00b6 }
            r4.<init>()     // Catch:{ all -> 0x00b6 }
            r6.A0I = r4     // Catch:{ all -> 0x00b6 }
        L_0x0052:
            X.0lt r4 = r6.A0I     // Catch:{ all -> 0x00b6 }
            r21 = r27
            r17 = r4
            r17.A00(r18, r20, r21, r22, r23, r24)     // Catch:{ all -> 0x00b6 }
            long r4 = r6.A0C     // Catch:{ all -> 0x00b6 }
            long r4 = r4 + r14
            r6.A0B = r4     // Catch:{ all -> 0x00b6 }
            long r24 = r7.toMillis(r2)     // Catch:{ all -> 0x00b6 }
            r20 = r10
            r21 = r6
            r20.A01(r21, r22, r23, r24, r26, r27)     // Catch:{ all -> 0x00b6 }
        L_0x006b:
            r4 = 0
        L_0x006c:
            monitor-exit(r8)     // Catch:{ all -> 0x00b6 }
            if (r4 == 0) goto L_0x00b5
            r4 = r28
            r5 = r41
            android.util.SparseArray r24 = r4.A0B(r0, r5)
            java.lang.Object r4 = r4.A04
            monitor-enter(r4)
            java.util.concurrent.atomic.AtomicInteger r0 = r6.A0b     // Catch:{ all -> 0x00b2 }
            int r0 = r0.get()     // Catch:{ all -> 0x00b2 }
            if (r9 == r0) goto L_0x0084
            monitor-exit(r4)     // Catch:{ all -> 0x00b2 }
            return r16
        L_0x0084:
            java.util.concurrent.TimeUnit r20 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00b2 }
            r18 = r14
            X.0lt r0 = r6.A0I     // Catch:{ all -> 0x00b2 }
            if (r0 != 0) goto L_0x0093
            X.0lt r0 = new X.0lt     // Catch:{ all -> 0x00b2 }
            r0.<init>()     // Catch:{ all -> 0x00b2 }
            r6.A0I = r0     // Catch:{ all -> 0x00b2 }
        L_0x0093:
            X.0lt r0 = r6.A0I     // Catch:{ all -> 0x00b2 }
            r21 = r27
            r17 = r0
            r17.A00(r18, r20, r21, r22, r23, r24)     // Catch:{ all -> 0x00b2 }
            long r0 = r6.A0C     // Catch:{ all -> 0x00b2 }
            long r0 = r0 + r14
            r6.A0B = r0     // Catch:{ all -> 0x00b2 }
            long r18 = r7.toMillis(r2)     // Catch:{ all -> 0x00b2 }
            r14 = r10
            r15 = r6
            r16 = r22
            r17 = r23
            r20 = r26
            r14.A01(r15, r16, r17, r18, r20, r21)     // Catch:{ all -> 0x00b2 }
            monitor-exit(r4)     // Catch:{ all -> 0x00b2 }
            return r13
        L_0x00b2:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00b2 }
            goto L_0x00b8
        L_0x00b5:
            return r13
        L_0x00b6:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00b6 }
        L_0x00b8:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09580i5.A0J(X.0Tg, int, int, long, java.util.concurrent.TimeUnit, boolean, java.lang.String, X.0lr, int, int, X.0gd, X.Ed3):boolean");
    }
}
