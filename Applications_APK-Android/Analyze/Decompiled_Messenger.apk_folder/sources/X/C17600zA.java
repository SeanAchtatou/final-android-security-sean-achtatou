package X;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import com.facebook.common.dextricks.DalvikConstants;
import com.facebook.common.dextricks.DexStore;
import com.facebook.litho.ComponentHost;
import com.facebook.litho.ComponentTree;
import com.facebook.litho.LithoView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0zA  reason: invalid class name and case insensitive filesystem */
public final class C17600zA {
    public static final Rect A0V = new Rect();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public C188915v A05;
    public C17760zQ A06;
    public C17860zb A07;
    public C31451ji A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public int[] A0E;
    public long[] A0F;
    public final Rect A0G = new Rect();
    public final C17610zB A0H;
    public final C17610zB A0I = new C17610zB();
    public final C17610zB A0J;
    public final AnonymousClass0p4 A0K;
    public final C17640zE A0L;
    public final LithoView A0M;
    public final C17830zY A0N;
    public final C17630zD A0O;
    public final C17620zC A0P;
    public final HashSet A0Q;
    public final Map A0R;
    public final Map A0S;
    public final Map A0T;
    public final Set A0U;

    public static C17880zd A01(View view) {
        if (view instanceof ComponentHost) {
            return ((ComponentHost) view).A07;
        }
        return (C17880zd) view.getTag(2131297256);
    }

    public static C17890ze A02(View view) {
        if (view instanceof ComponentHost) {
            return ((ComponentHost) view).A09;
        }
        return (C17890ze) view.getTag(2131297258);
    }

    public static C17970zp A03(View view) {
        if (view instanceof ComponentHost) {
            return ((ComponentHost) view).A0A;
        }
        return (C17970zp) view.getTag(2131297260);
    }

    public static String A04(C17600zA r9, C17830zY r10) {
        long j;
        C17770zR r0;
        String A1A;
        Object obj;
        String str;
        Object obj2;
        C17610zB r02 = r9.A0J;
        int A032 = r02.A03(r10);
        boolean z = false;
        int i = -1;
        if (A032 > -1) {
            j = r02.A04(A032);
            int i2 = 0;
            while (true) {
                long[] jArr = r9.A0F;
                if (i2 >= jArr.length) {
                    break;
                } else if (j == jArr[i2]) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
        } else {
            j = -1;
        }
        ComponentTree componentTree = r9.A0M.A03;
        if (componentTree == null) {
            A1A = "<null_component_tree>";
        } else {
            synchronized (componentTree) {
                r0 = componentTree.A05;
            }
            A1A = r0.A1A();
        }
        StringBuilder sb = new StringBuilder("rootComponent=");
        sb.append(A1A);
        sb.append(", index=");
        sb.append(i);
        sb.append(", mapIndex=");
        sb.append(A032);
        sb.append(", id=");
        sb.append(j);
        sb.append(", disappearRange=[");
        sb.append(r9.A01);
        sb.append(",");
        sb.append(r9.A00);
        sb.append("], contentType=");
        if (r10.A00() != null) {
            obj = r10.A00().getClass();
        } else {
            obj = "<null_content>";
        }
        sb.append(obj);
        sb.append(", component=");
        C17770zR r03 = r10.A04;
        if (r03 != null) {
            str = r03.A1A();
        } else {
            str = "<null_component>";
        }
        sb.append(str);
        sb.append(", transitionId=");
        sb.append(r10.A07);
        sb.append(", host=");
        ComponentHost componentHost = r10.A05;
        if (componentHost != null) {
            obj2 = componentHost.getClass();
        } else {
            obj2 = "<null_host>";
        }
        sb.append(obj2);
        sb.append(", isRootHost=");
        if (r9.A0I.A07(0) == r10.A05) {
            z = true;
        }
        sb.append(z);
        return sb.toString();
    }

    public static void A05(View view, boolean z) {
        if (view instanceof LithoView) {
            LithoView lithoView = (LithoView) view;
            if (!lithoView.A0f()) {
                return;
            }
            if (!z) {
                lithoView.A0W(new Rect(0, 0, view.getWidth(), view.getHeight()), false);
            } else {
                lithoView.A0R();
            }
        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                A05(viewGroup.getChildAt(i), z);
            }
        }
    }

    private void A06(AnonymousClass0p4 r5, C17830zY r6) {
        C17610zB r1;
        int A032;
        C17750zP r2 = r6.A07;
        C17860zb r12 = this.A07;
        if (!(r12 == null || r2 == null)) {
            r12.A09(r2, null);
        }
        Object A002 = r6.A00();
        if (A002 instanceof ComponentHost) {
            ComponentHost componentHost = (ComponentHost) A002;
            for (int A0C2 = componentHost.A0C() - 1; A0C2 >= 0; A0C2--) {
                A06(r5, (C17830zY) componentHost.A02.A05(A0C2));
            }
            if (componentHost.A0C() > 0) {
                throw new IllegalStateException("Recursively unmounting items from a ComponentHost, left some items behind maybe because not tracked by its MountState");
            }
        }
        ComponentHost componentHost2 = r6.A05;
        ComponentHost.A0A(componentHost2);
        C07870eJ r22 = componentHost2.A02;
        if (r22.A01) {
            C07870eJ.A00(r22);
        }
        int i = 0;
        while (true) {
            if (i < r22.A00) {
                if (r22.A03[i] == r6) {
                    break;
                }
                i++;
            } else {
                i = -1;
                break;
            }
        }
        componentHost2.A0J(r22.A02(i), r6);
        A09(r6);
        A08(r6);
        if (r6.A04.A0v() && (A032 = (r1 = this.A0H).A03(r6)) > 0) {
            r1.A0A(A032);
        }
        A07(r6);
        try {
            r6.A01(r5.A09, "unmountDisappearingItemChild");
        } catch (C37781wF e) {
            throw new RuntimeException(AnonymousClass08S.A0P(e.getMessage(), " ", A04(this, r6)));
        }
    }

    private void A07(C17830zY r4) {
        if (this.A0J.A03(r4) > -1) {
            C09070gU.A01(AnonymousClass07B.A01, "MountState:DanglingContentDuringAnim", AnonymousClass08S.A0J("Got dangling mount content during animation: ", A04(this, r4)));
        }
    }

    private void A08(C17830zY r5) {
        C17770zR r3 = r5.A04;
        Object A002 = r5.A00();
        AnonymousClass0p4 r1 = r3.A03;
        if (r1 == null) {
            r1 = this.A0K;
        }
        if (r5.A09) {
            r3.A0k(r1, A002);
            r5.A09 = false;
        }
        r3.A0l(r1, A002);
    }

    public static void A09(C17830zY r2) {
        C17770zR r1 = r2.A04;
        if (C17770zR.A09(r1)) {
            A0B(r2, r1 instanceof C17670zH);
        }
    }

    public static void A0A(C17830zY r8) {
        int i;
        int i2;
        int i3;
        int i4;
        C31411je r2;
        C17770zR r5 = r8.A04;
        if (C17770zR.A09(r5)) {
            View view = (View) r8.A00();
            C31401jd r4 = r8.A06;
            if (r4 != null) {
                AnonymousClass10N r22 = r4.A0E;
                if (r22 != null) {
                    C17880zd A012 = A01(view);
                    if (A012 == null) {
                        A012 = new C17880zd();
                        if (view instanceof ComponentHost) {
                            ComponentHost componentHost = (ComponentHost) view;
                            componentHost.A07 = A012;
                            componentHost.setOnClickListener(A012);
                        } else {
                            view.setOnClickListener(A012);
                            view.setTag(2131297256, A012);
                        }
                    }
                    A012.A00 = r22;
                    view.setClickable(true);
                }
                AnonymousClass10N r6 = r4.A0I;
                if (r6 != null) {
                    C17890ze A022 = A02(view);
                    if (A022 == null) {
                        A022 = new C17890ze();
                        View view2 = view;
                        if (view instanceof ComponentHost) {
                            ComponentHost componentHost2 = (ComponentHost) view2;
                            componentHost2.A09 = A022;
                            componentHost2.setOnLongClickListener(A022);
                        } else {
                            view.setOnLongClickListener(A022);
                            view.setTag(2131297258, A022);
                        }
                    }
                    A022.A00 = r6;
                    view.setLongClickable(true);
                }
                AnonymousClass10N r62 = r4.A0G;
                if (r62 != null) {
                    boolean z = view instanceof ComponentHost;
                    if (z) {
                        r2 = ((ComponentHost) view).A08;
                    } else {
                        r2 = (C31411je) view.getTag(2131297257);
                    }
                    if (r2 == null) {
                        r2 = new C31411je();
                        if (z) {
                            ComponentHost componentHost3 = (ComponentHost) view;
                            componentHost3.A08 = r2;
                            componentHost3.setOnFocusChangeListener(r2);
                        } else {
                            view.setOnFocusChangeListener(r2);
                            view.setTag(2131297257, r2);
                        }
                    }
                    r2.A00 = r62;
                }
                AnonymousClass10N r63 = r4.A0Q;
                if (r63 != null) {
                    C17970zp A032 = A03(view);
                    if (A032 == null) {
                        A032 = new C17970zp();
                        if (view instanceof ComponentHost) {
                            ComponentHost componentHost4 = (ComponentHost) view;
                            componentHost4.A0A = A032;
                            componentHost4.setOnTouchListener(A032);
                        } else {
                            view.setOnTouchListener(A032);
                            view.setTag(2131297260, A032);
                        }
                    }
                    A032.A00 = r63;
                }
                AnonymousClass10N r1 = r4.A0H;
                if (r1 != null && (view instanceof ComponentHost)) {
                    ((ComponentHost) view).A0B = r1;
                }
                boolean z2 = view instanceof ComponentHost;
                if (z2 || r4.A02()) {
                    view.setTag(2131297259, r4);
                }
                Object obj = r4.A0T;
                if (z2) {
                    ((ComponentHost) view).A0D = obj;
                } else {
                    view.setTag(obj);
                }
                SparseArray sparseArray = r4.A0C;
                SparseArray sparseArray2 = sparseArray;
                if (sparseArray != null) {
                    if (z2) {
                        ((ComponentHost) view).A00 = sparseArray;
                    } else {
                        int size = sparseArray.size();
                        for (int i5 = 0; i5 < size; i5++) {
                            view.setTag(sparseArray2.keyAt(i5), sparseArray2.valueAt(i5));
                        }
                    }
                }
                float f = r4.A05;
                if (f != 0.0f) {
                    C15320v6.setElevation(view, f);
                }
                ViewOutlineProvider viewOutlineProvider = r4.A0D;
                ViewOutlineProvider viewOutlineProvider2 = viewOutlineProvider;
                if (viewOutlineProvider != null && Build.VERSION.SDK_INT >= 21) {
                    view.setOutlineProvider(viewOutlineProvider2);
                }
                boolean z3 = r4.A0W;
                boolean z4 = z3;
                if (z3 && Build.VERSION.SDK_INT >= 21) {
                    view.setClipToOutline(z4);
                }
                boolean z5 = false;
                if ((r4.A0A & DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE) != 0) {
                    z5 = true;
                }
                if (z5 && (view instanceof ViewGroup)) {
                    ((ViewGroup) view).setClipChildren(r4.A0V);
                }
                CharSequence charSequence = r4.A0S;
                if (!TextUtils.isEmpty(charSequence)) {
                    view.setContentDescription(charSequence);
                }
                int i6 = r4.A09;
                if (i6 == 1) {
                    view.setFocusable(true);
                } else if (i6 == 2) {
                    view.setFocusable(false);
                }
                int i7 = r4.A07;
                if (i7 == 1) {
                    view.setClickable(true);
                } else if (i7 == 2) {
                    view.setClickable(false);
                }
                int i8 = r4.A08;
                if (i8 == 1) {
                    view.setEnabled(true);
                } else if (i8 == 2) {
                    view.setEnabled(false);
                }
                int i9 = r4.A0B;
                if (i9 == 1) {
                    view.setSelected(true);
                } else if (i9 == 2) {
                    view.setSelected(false);
                }
                if (Build.VERSION.SDK_INT >= 11) {
                    boolean z6 = false;
                    if ((r4.A0A & DexStore.LOAD_RESULT_WITH_VDEX_ODEX) != 0) {
                        z6 = true;
                    }
                    if (z6) {
                        float f2 = r4.A04;
                        view.setScaleX(f2);
                        view.setScaleY(f2);
                    }
                }
                if (Build.VERSION.SDK_INT >= 11) {
                    boolean z7 = false;
                    if ((r4.A0A & 1048576) != 0) {
                        z7 = true;
                    }
                    if (z7) {
                        view.setAlpha(r4.A00);
                    }
                }
                if (Build.VERSION.SDK_INT >= 11) {
                    boolean z8 = false;
                    if ((r4.A0A & 2097152) != 0) {
                        z8 = true;
                    }
                    if (z8) {
                        view.setRotation(r4.A01);
                    }
                }
                if (Build.VERSION.SDK_INT >= 11) {
                    boolean z9 = false;
                    if ((r4.A0A & 33554432) != 0) {
                        z9 = true;
                    }
                    if (z9) {
                        view.setRotationX(r4.A02);
                    }
                }
                if (Build.VERSION.SDK_INT >= 11) {
                    boolean z10 = false;
                    if ((r4.A0A & 67108864) != 0) {
                        z10 = true;
                    }
                    if (z10) {
                        view.setRotationY(r4.A03);
                    }
                }
            }
            int i10 = r8.A00;
            if (i10 != 0) {
                C15320v6.setImportantForAccessibility(view, i10);
            }
            C17650zF r64 = r8.A08;
            if (r64 != null) {
                StateListAnimator stateListAnimator = r64.A01;
                int i11 = r64.A00;
                if (!(stateListAnimator == null && i11 == 0)) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        if (stateListAnimator == null) {
                            stateListAnimator = AnimatorInflater.loadStateListAnimator(view.getContext(), i11);
                        }
                        view.setStateListAnimator(stateListAnimator);
                    } else {
                        throw new IllegalStateException("MountState has a ViewNodeInfo with stateListAnimator, however the current Android version doesn't support stateListAnimator on Views");
                    }
                }
                if (!(r5 instanceof C17670zH)) {
                    Drawable drawable = r64.A04;
                    if (drawable != null) {
                        if (Build.VERSION.SDK_INT < 16) {
                            view.setBackgroundDrawable(drawable);
                        } else {
                            view.setBackground(drawable);
                        }
                    }
                    Rect rect = r64.A03;
                    boolean z11 = false;
                    if (rect != null) {
                        z11 = true;
                    }
                    if (z11) {
                        if (rect != null) {
                            i = rect.left;
                        } else {
                            i = 0;
                        }
                        if (rect != null) {
                            i2 = rect.top;
                        } else {
                            i2 = 0;
                        }
                        if (rect != null) {
                            i3 = rect.right;
                        } else {
                            i3 = 0;
                        }
                        if (rect != null) {
                            i4 = rect.bottom;
                        } else {
                            i4 = 0;
                        }
                        view.setPadding(i, i2, i3, i4);
                    }
                    Drawable drawable2 = r64.A05;
                    if (drawable2 != null) {
                        if (Build.VERSION.SDK_INT >= 23) {
                            view.setForeground(drawable2);
                        } else {
                            throw new IllegalStateException("MountState has a ViewNodeInfo with foreground however the current Android version doesn't support foreground on Views");
                        }
                    }
                    if (Build.VERSION.SDK_INT >= 17) {
                        int i12 = 2;
                        switch (r64.A06.ordinal()) {
                            case 1:
                                i12 = 0;
                                break;
                            case 2:
                                i12 = 1;
                                break;
                        }
                        view.setLayoutDirection(i12);
                    }
                }
            }
        }
    }

    public static void A0C(C17600zA r16, int i, C17610zB r18) {
        int i2;
        C17870zc r8;
        C17720zM r3;
        C17720zM r0;
        int i3 = i;
        C17600zA r5 = r16;
        C17830zY A0M2 = r5.A0M(i3);
        long nanoTime = System.nanoTime();
        if (A0M2 != null) {
            long j = r5.A0F[i3];
            if (j == 0) {
                A0B(A0M2, true);
                return;
            }
            r5.A0J.A0B(j);
            Object A002 = A0M2.A00();
            C17610zB r9 = r18;
            if ((A002 instanceof ComponentHost) && !(A002 instanceof LithoView)) {
                ComponentHost componentHost = (ComponentHost) A002;
                for (int A0C2 = componentHost.A0C() - 1; A0C2 >= 0; A0C2--) {
                    C17610zB r1 = r5.A0J;
                    long A042 = r1.A04(r1.A03((C17830zY) componentHost.A02.A05(A0C2)));
                    int length = r5.A0F.length - 1;
                    while (true) {
                        if (length < 0) {
                            break;
                        } else if (r5.A0F[length] == A042) {
                            A0C(r5, length, r9);
                            break;
                        } else {
                            length--;
                        }
                    }
                }
                if (componentHost.A0C() > 0) {
                    throw new IllegalStateException("Recursively unmounting items from a ComponentHost, left some items behind maybe because not tracked by its MountState");
                }
            }
            if (A002 instanceof C20881Ee) {
                ArrayList arrayList = new ArrayList();
                ((C20881Ee) A002).BMi(arrayList);
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    ((LithoView) arrayList.get(size)).A0V();
                }
            }
            A0M2.A05.A0J(i3, A0M2);
            A09(A0M2);
            C17770zR r6 = A0M2.A04;
            if (r6 instanceof C17670zH) {
                ComponentHost componentHost2 = (ComponentHost) A002;
                r9.A0A(r9.A03(componentHost2));
                A0F(r5, componentHost2);
            }
            r5.A08(A0M2);
            C17750zP r11 = A0M2.A07;
            boolean z = false;
            if (r11 != null) {
                z = true;
            }
            if (z) {
                if (j == 0) {
                    i2 = 3;
                } else {
                    i2 = (int) ((j >> 16) & 7);
                }
                C17860zb r10 = r5.A07;
                if (!(r10 == null || r11 == null || (r8 = (C17870zc) r10.A03.A02.get(r11)) == null || (r3 = r8.A02) == null || r3.A01[i2] == null)) {
                    if (r3.A00 > 1) {
                        r0 = new C17720zM(r3);
                        r0.A05(i2, null);
                    } else {
                        r0 = null;
                    }
                    C17860zb.A08(r10, r8, r0);
                }
            }
            if (r6.A0v()) {
                r5.A0H.A0B(r5.A0F[i3]);
            }
            try {
                A0M2.A01(r5.A0K.A09, "unmountItem");
                C17630zD r12 = r5.A0O;
                if (r12.A0F) {
                    r12.A09.add(Double.valueOf(((double) (System.nanoTime() - nanoTime)) / 1000000.0d));
                    r5.A0O.A08.add(r6.A1A());
                    r5.A0O.A03++;
                }
            } catch (C37781wF e) {
                throw new RuntimeException(AnonymousClass08S.A0P(e.getMessage(), " ", A04(r5, A0M2)));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0016, code lost:
        if (X.C17770zR.A09(r8) == false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0E(X.C17600zA r7, X.C17770zR r8, java.lang.Object r9) {
        /*
            X.0p4 r0 = r8.A03
            if (r0 != 0) goto L_0x0006
            X.0p4 r0 = r7.A0K
        L_0x0006:
            r8.A0g(r0, r9)
            X.0zE r6 = r7.A0L
            boolean r0 = r8.A1J()
            if (r0 == 0) goto L_0x0018
            boolean r0 = X.C17770zR.A09(r8)
            r2 = 1
            if (r0 != 0) goto L_0x0019
        L_0x0018:
            r2 = 0
        L_0x0019:
            X.0zZ[] r0 = r8.A1M()
            int r1 = r0.length
            r7 = 0
            r0 = 0
            if (r1 <= 0) goto L_0x0023
            r0 = 1
        L_0x0023:
            if (r2 != 0) goto L_0x0028
            if (r0 != 0) goto L_0x0028
            return
        L_0x0028:
            java.util.HashSet r5 = new java.util.HashSet
            r5.<init>()
            if (r2 == 0) goto L_0x0053
            android.util.SparseArray r4 = r8.A13()
            r3 = 0
        L_0x0034:
            int r0 = r4.size()
            if (r3 >= r0) goto L_0x0053
            int r2 = r4.keyAt(r3)
            java.lang.Object r1 = r4.valueAt(r3)
            X.0zZ r1 = (X.C17840zZ) r1
            r0 = r9
            android.view.View r0 = (android.view.View) r0
            X.C17640zE.A00(r2, r1, r0)
            X.C17640zE.A01(r6, r1, r8)
            r5.add(r1)
            int r3 = r3 + 1
            goto L_0x0034
        L_0x0053:
            X.0zZ[] r2 = r8.A1M()
        L_0x0057:
            int r0 = r2.length
            if (r7 >= r0) goto L_0x006a
            r1 = r2[r7]
            java.lang.Object r0 = r1.A00
            r8.A1B(r7, r0, r9)
            X.C17640zE.A01(r6, r1, r8)
            r5.add(r1)
            int r7 = r7 + 1
            goto L_0x0057
        L_0x006a:
            java.util.Map r0 = r6.A00
            r0.put(r8, r5)
            java.util.Map r0 = r6.A01
            r0.put(r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17600zA.A0E(X.0zA, X.0zR, java.lang.Object):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        if (r1 != false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        if (r1 != false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0F(X.C17600zA r6, com.facebook.litho.ComponentHost r7) {
        /*
            java.util.ArrayList r0 = r7.A0E
            if (r0 == 0) goto L_0x000b
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            if (r0 == 0) goto L_0x0051
            java.util.ArrayList r0 = r7.A0E
            if (r0 == 0) goto L_0x0019
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x001a
        L_0x0019:
            r0 = 0
        L_0x001a:
            if (r0 != 0) goto L_0x0033
            r5 = 0
        L_0x001d:
            r4 = 0
            int r3 = r5.size()
        L_0x0022:
            if (r4 >= r3) goto L_0x0051
            X.0zb r2 = r6.A07
            java.lang.Object r1 = r5.get(r4)
            X.0zP r1 = (X.C17750zP) r1
            r0 = 0
            r2.A09(r1, r0)
            int r4 = r4 + 1
            goto L_0x0022
        L_0x0033:
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r2 = 0
            java.util.ArrayList r0 = r7.A0E
            int r1 = r0.size()
        L_0x003f:
            if (r2 >= r1) goto L_0x001d
            java.util.ArrayList r0 = r7.A0E
            java.lang.Object r0 = r0.get(r2)
            X.0zY r0 = (X.C17830zY) r0
            X.0zP r0 = r0.A07
            r5.add(r0)
            int r2 = r2 + 1
            goto L_0x003f
        L_0x0051:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17600zA.A0F(X.0zA, com.facebook.litho.ComponentHost):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005b, code lost:
        if (X.C49992dC.A00(r6.A00, r5.A01) == false) goto L_0x007f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        if (X.C49992dC.A00(r6.A00, r5.A01) == false) goto L_0x007f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007f, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f4, code lost:
        r0 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0I(X.C17750zP r5, X.C17760zQ r6, X.AnonymousClass1M8 r7, X.C17710zL r8) {
        /*
            boolean r0 = r6 instanceof X.C17850za
            r3 = 0
            if (r0 == 0) goto L_0x001b
            X.0za r6 = (X.C17850za) r6
            java.util.ArrayList r2 = r6.A00
            int r1 = r2.size()
        L_0x000d:
            if (r3 >= r1) goto L_0x0104
            java.lang.Object r0 = r2.get(r3)
            X.0zQ r0 = (X.C17760zQ) r0
            A0I(r5, r0, r7, r8)
            int r3 = r3 + 1
            goto L_0x000d
        L_0x001b:
            boolean r0 = r6 instanceof X.C17680zI
            if (r0 == 0) goto L_0x00d7
            X.0zI r6 = (X.C17680zI) r6
            X.1jg r0 = r6.A01
            X.0zO r0 = r0.A00
            java.lang.Integer r3 = r0.A00
            int r0 = r3.intValue()
            switch(r0) {
                case 0: goto L_0x0073;
                case 1: goto L_0x0075;
                case 2: goto L_0x0053;
                case 3: goto L_0x00a5;
                case 4: goto L_0x005e;
                case 5: goto L_0x0073;
                default: goto L_0x002e;
            }
        L_0x002e:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r1 = "Didn't handle type: "
            if (r3 == 0) goto L_0x0050
            switch(r0) {
                case 1: goto L_0x004d;
                case 2: goto L_0x004a;
                case 3: goto L_0x0047;
                case 4: goto L_0x0044;
                case 5: goto L_0x0041;
                default: goto L_0x0037;
            }
        L_0x0037:
            java.lang.String r0 = "ALL"
        L_0x0039:
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x0041:
            java.lang.String r0 = "AUTO_LAYOUT"
            goto L_0x0039
        L_0x0044:
            java.lang.String r0 = "GLOBAL_KEY_SET"
            goto L_0x0039
        L_0x0047:
            java.lang.String r0 = "GLOBAL_KEY"
            goto L_0x0039
        L_0x004a:
            java.lang.String r0 = "LOCAL_KEY_SET"
            goto L_0x0039
        L_0x004d:
            java.lang.String r0 = "LOCAL_KEY"
            goto L_0x0039
        L_0x0050:
            java.lang.String r0 = "null"
            goto L_0x0039
        L_0x0053:
            java.lang.String r1 = r6.A00
            java.lang.String r0 = r5.A01
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 != 0) goto L_0x005e
            goto L_0x007f
        L_0x005e:
            X.1jg r0 = r6.A01
            X.0zO r0 = r0.A00
            java.lang.Object r4 = r0.A01
            java.lang.String[] r4 = (java.lang.String[]) r4
            java.lang.String r3 = r5.A02
            int r2 = r4.length
            r1 = 0
        L_0x006a:
            if (r1 >= r2) goto L_0x007f
            r0 = r4[r1]
            if (r0 == r3) goto L_0x0073
            int r1 = r1 + 1
            goto L_0x006a
        L_0x0073:
            r0 = 1
            goto L_0x0080
        L_0x0075:
            java.lang.String r1 = r6.A00
            java.lang.String r0 = r5.A01
            boolean r0 = X.C49992dC.A00(r1, r0)
            if (r0 != 0) goto L_0x00a5
        L_0x007f:
            r0 = 0
        L_0x0080:
            if (r0 == 0) goto L_0x0104
            X.1jg r0 = r6.A01
            X.0zJ r3 = r0.A01
            java.lang.Integer r0 = r3.A00
            int r0 = r0.intValue()
            switch(r0) {
                case 0: goto L_0x00b9;
                case 1: goto L_0x00b2;
                case 2: goto L_0x00c8;
                default: goto L_0x008f;
            }
        L_0x008f:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Didn't handle type: "
            r1.<init>(r0)
            java.lang.Object r0 = r3.A01
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x00a5:
            java.lang.String r1 = r5.A02
            X.1jg r0 = r6.A01
            X.0zO r0 = r0.A00
            java.lang.Object r0 = r0.A01
            boolean r0 = r1.equals(r0)
            goto L_0x0080
        L_0x00b2:
            java.lang.Object r0 = r3.A01
            boolean r0 = r7.equals(r0)
            goto L_0x00f5
        L_0x00b9:
            java.lang.Object r3 = r3.A01
            X.1M8[] r3 = (X.AnonymousClass1M8[]) r3
            int r2 = r3.length
            r1 = 0
        L_0x00bf:
            if (r1 >= r2) goto L_0x00d5
            r0 = r3[r1]
            if (r0 == r7) goto L_0x00f4
            int r1 = r1 + 1
            goto L_0x00bf
        L_0x00c8:
            X.1M8[] r3 = X.C17700zK.A07
            int r2 = r3.length
            r1 = 0
        L_0x00cc:
            if (r1 >= r2) goto L_0x00d5
            r0 = r3[r1]
            if (r0 == r7) goto L_0x00f4
            int r1 = r1 + 1
            goto L_0x00cc
        L_0x00d5:
            r0 = 0
            goto L_0x00f5
        L_0x00d7:
            boolean r0 = r6 instanceof X.C31441jh
            if (r0 == 0) goto L_0x0105
            X.1jh r6 = (X.C31441jh) r6
            r6.A00()
            java.util.ArrayList r2 = r6.A05
            int r1 = r2.size()
        L_0x00e6:
            if (r3 >= r1) goto L_0x0104
            java.lang.Object r0 = r2.get(r3)
            X.0zQ r0 = (X.C17760zQ) r0
            A0I(r5, r0, r7, r8)
            int r3 = r3 + 1
            goto L_0x00e6
        L_0x00f4:
            r0 = 1
        L_0x00f5:
            if (r0 == 0) goto L_0x0104
            r0 = 1
            r8.A01 = r0
            X.3aD r1 = r6.A03
            r0 = 0
            if (r1 == 0) goto L_0x0100
            r0 = 1
        L_0x0100:
            if (r0 == 0) goto L_0x0104
            r8.A00 = r6
        L_0x0104:
            return
        L_0x0105:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Unhandled transition type: "
            r1.<init>(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17600zA.A0I(X.0zP, X.0zQ, X.1M8, X.0zL):void");
    }

    public static void A0J(Object obj, int i, int i2, int i3, int i4, boolean z) {
        if (obj instanceof View) {
            AnonymousClass1LA.A01((View) obj, i, i2, i3, i4, z);
        } else if (obj instanceof Drawable) {
            ((Drawable) obj).setBounds(i, i2, i3, i4);
        } else {
            throw new IllegalStateException("Unsupported mounted content " + obj);
        }
    }

    public static boolean A0K(C17600zA r2, C188915v r3) {
        if (!r2.A09) {
            return false;
        }
        if (r2.A02 == r3.A00 || r2.A0A) {
            return true;
        }
        return false;
    }

    public C17830zY A0M(int i) {
        return (C17830zY) this.A0J.A07(this.A0F[i]);
    }

    public List A0N() {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            C17610zB r2 = this.A0J;
            if (i >= r2.A01()) {
                return arrayList;
            }
            C17830zY r1 = (C17830zY) r2.A07(r2.A04(i));
            if (r1 != null && (r1.A00() instanceof C20881Ee)) {
                ((C20881Ee) r1.A00()).BMi(arrayList);
            }
            i++;
        }
    }

    public void A0O() {
        if (this.A08 != null) {
            boolean A022 = C27041cY.A02();
            if (A022) {
                C27041cY.A01("MountState.clearIncrementalItems");
            }
            C31451ji r1 = this.A08;
            if (r1 != null) {
                C31491jm r0 = r1.A02;
                if (r0 != null) {
                    r0.A03();
                }
                C31491jm r02 = r1.A00;
                if (r02 != null) {
                    r02.A03();
                }
                C31491jm r03 = r1.A01;
                if (r03 != null) {
                    r03.A03();
                }
                C31451ji.A00(r1);
            }
            if (!A022) {
                return;
            }
        } else {
            boolean A023 = C27041cY.A02();
            if (A023) {
                C27041cY.A01("MountState.clearIncrementalItems");
            }
            ArrayList arrayList = new ArrayList();
            for (String str : this.A0T.keySet()) {
                C22121Jz r12 = (C22121Jz) this.A0T.get(str);
                if (r12.A03) {
                    r12.A03 = false;
                } else {
                    arrayList.add(str);
                }
            }
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                String str2 = (String) arrayList.get(i);
                C22121Jz r4 = (C22121Jz) this.A0T.get(str2);
                AnonymousClass10N r04 = r4.A01;
                AnonymousClass10N r3 = r4.A02;
                AnonymousClass10N r2 = r4.A05;
                if (r04 != null) {
                    C33181nA.A02(r04);
                }
                boolean z = false;
                if ((r4.A00 & 32) != 0) {
                    z = true;
                }
                if (z) {
                    r4.A00(false);
                    if (r3 != null) {
                        C33181nA.A03(r3);
                    }
                }
                if (r2 != null) {
                    C33181nA.A05(r2, 0, 0.0f);
                }
                r4.A04 = false;
                this.A0T.remove(str2);
            }
            if (!A023) {
                return;
            }
        }
        C27041cY.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0032, code lost:
        if (X.C17770zR.A09(r5) == false) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0P() {
        /*
            r10 = this;
            long[] r0 = r10.A0F
            if (r0 == 0) goto L_0x0090
            boolean r9 = X.C27041cY.A02()
            if (r9 == 0) goto L_0x000f
            java.lang.String r0 = "MountState.unbind"
            X.C27041cY.A01(r0)
        L_0x000f:
            long[] r0 = r10.A0F
            int r8 = r0.length
            r7 = 0
        L_0x0013:
            if (r7 >= r8) goto L_0x0088
            X.0zY r6 = r10.A0M(r7)
            if (r6 == 0) goto L_0x004a
            boolean r0 = r6.A09
            if (r0 == 0) goto L_0x004a
            X.0zR r5 = r6.A04
            java.lang.Object r4 = r6.A00()
            X.0zE r3 = r10.A0L
            boolean r0 = r5.A1J()
            if (r0 == 0) goto L_0x0034
            boolean r1 = X.C17770zR.A09(r5)
            r0 = 1
            if (r1 != 0) goto L_0x0035
        L_0x0034:
            r0 = 0
        L_0x0035:
            if (r0 != 0) goto L_0x004d
            X.0zZ[] r0 = r5.A1M()
            int r0 = r0.length
            if (r0 != 0) goto L_0x004d
        L_0x003e:
            X.0p4 r0 = r5.A03
            if (r0 != 0) goto L_0x0044
            X.0p4 r0 = r10.A0K
        L_0x0044:
            r5.A0m(r0, r4)
            r0 = 0
            r6.A09 = r0
        L_0x004a:
            int r7 = r7 + 1
            goto L_0x0013
        L_0x004d:
            java.util.Map r0 = r3.A01
            r0.remove(r5)
            java.util.Map r0 = r3.A00
            java.lang.Object r0 = r0.get(r5)
            java.util.Set r0 = (java.util.Set) r0
            if (r0 == 0) goto L_0x003e
            java.util.Iterator r2 = r0.iterator()
        L_0x0060:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x003e
            java.lang.Object r1 = r2.next()
            X.0zZ r1 = (X.C17840zZ) r1
            java.util.Map r0 = r3.A02
            java.lang.Object r0 = r0.get(r1)
            java.util.Set r0 = (java.util.Set) r0
            r0.remove(r5)
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0060
            java.util.Map r0 = r3.A02
            r0.remove(r1)
            java.util.Set r0 = r1.A01
            r0.remove(r3)
            goto L_0x0060
        L_0x0088:
            r10.A0O()
            if (r9 == 0) goto L_0x0090
            X.C27041cY.A00()
        L_0x0090:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17600zA.A0P():void");
    }

    public void A0R(C188915v r9, ComponentTree componentTree) {
        if (!this.A0D) {
            ArrayList arrayList = new ArrayList();
            List list = r9.A0M;
            if (list != null) {
                arrayList.addAll(list);
            }
            List list2 = r9.A0L;
            if (!(list2 == null || list2.isEmpty() || componentTree.A0D == null)) {
                List list3 = list2;
                if (list2 != null && 0 < list3.size()) {
                    list3.get(0);
                    throw new RuntimeException("Trying to apply previous render data to component that doesn't support it");
                }
            }
            List list4 = r9.A0L;
            if (list4 != null) {
                int size = list4.size();
                for (int i = 0; i < size; i++) {
                    C17770zR r2 = (C17770zR) list4.get(i);
                    C17760zQ A0S2 = r2.A0S(r2.A03);
                    if (A0S2 != null) {
                        C07070ca.A01(A0S2, r2.A06);
                    }
                    if (A0S2 != null) {
                        C07070ca.A02(A0S2, arrayList, r9.A0J);
                    }
                }
            }
            String str = r9.A0J;
            synchronized (componentTree) {
                AnonymousClass1JC r6 = componentTree.A0E;
                if (r6 != null) {
                    synchronized (r6) {
                        Map map = r6.A06;
                        if (map != null) {
                            for (List list5 : map.values()) {
                                int size2 = list5.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    C07070ca.A02((C17760zQ) list5.get(i2), arrayList, str);
                                }
                            }
                            r6.A06 = null;
                        }
                    }
                }
            }
            C17710zL r7 = new C17710zL();
            C17710zL r62 = new C17710zL();
            C17750zP r5 = r9.A0H;
            if (r5 != null) {
                int i3 = 0;
                int size3 = arrayList.size();
                while (i3 < size3) {
                    C17760zQ r1 = (C17760zQ) arrayList.get(i3);
                    if (r1 != null) {
                        A0I(r5, r1, C17700zK.A04, r7);
                        A0I(r5, r1, C17700zK.A01, r62);
                        i3++;
                    } else {
                        throw new IllegalStateException("NULL_TRANSITION when collecting root bounds anim. Root: " + r9.A0J + ", root TransitionId: " + r5);
                    }
                }
            }
            C17760zQ r0 = null;
            if (!r7.A01) {
                r7 = null;
            }
            if (!r62.A01) {
                r62 = null;
            }
            componentTree.A0G = r7;
            componentTree.A0F = r62;
            if (!arrayList.isEmpty()) {
                if (arrayList.size() == 1) {
                    r0 = (C17760zQ) arrayList.get(0);
                } else {
                    r0 = new C21661If(arrayList);
                }
            }
            this.A06 = r0;
            this.A0D = true;
        }
    }

    public C17600zA(LithoView lithoView) {
        HashMap hashMap = null;
        this.A0P = new C17620zC();
        this.A0O = new C17630zD();
        this.A02 = -1;
        this.A0A = false;
        this.A01 = -1;
        this.A00 = -1;
        this.A0Q = new HashSet();
        this.A0R = new LinkedHashMap();
        this.A0D = false;
        this.A0U = new HashSet();
        this.A0L = new C17640zE();
        this.A0J = new C17610zB();
        this.A0T = new HashMap();
        this.A0H = new C17610zB();
        LithoView lithoView2 = lithoView;
        this.A0K = lithoView.A0H;
        this.A0M = lithoView;
        this.A09 = true;
        this.A0S = AnonymousClass07c.isEndToEndTestRun ? new HashMap() : hashMap;
        C17650zF r6 = new C17650zF();
        r6.A06 = C17660zG.INHERIT;
        this.A0N = new C17830zY(new C17670zH(), lithoView2, lithoView, null, r6, 0, 0, lithoView.getContext().getResources().getConfiguration().orientation, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        r8 = r8 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(X.C188915v r9, int r10) {
        /*
            X.0zN r0 = r9.A0A(r10)
            long r1 = r0.A02
            int r8 = r10 + 1
            java.util.List r0 = r9.A0e
            int r7 = r0.size()
        L_0x000e:
            if (r8 >= r7) goto L_0x002f
            X.0zN r0 = r9.A0A(r8)
        L_0x0014:
            long r5 = r0.A07
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x002c
            r3 = 0
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0023
            int r0 = r8 + -1
            return r0
        L_0x0023:
            int r0 = r9.A09(r5)
            X.0zN r0 = r9.A0A(r0)
            goto L_0x0014
        L_0x002c:
            int r8 = r8 + 1
            goto L_0x000e
        L_0x002f:
            java.util.List r0 = r9.A0e
            int r0 = r0.size()
            int r0 = r0 + -1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17600zA.A00(X.15v, int):int");
    }

    private static void A0B(C17830zY r8, boolean z) {
        C17970zp A032;
        C31411je r1;
        C17890ze A022;
        C17880zd A012;
        View view = (View) r8.A00();
        C31401jd r4 = r8.A06;
        if (r4 != null) {
            if (!(r4.A0E == null || (A012 = A01(view)) == null)) {
                A012.A00 = null;
            }
            if (!(r4.A0I == null || (A022 = A02(view)) == null)) {
                A022.A00 = null;
            }
            if (r4.A0G != null) {
                if (view instanceof ComponentHost) {
                    r1 = ((ComponentHost) view).A08;
                } else {
                    r1 = (C31411je) view.getTag(2131297257);
                }
                if (r1 != null) {
                    r1.A00 = null;
                }
            }
            if (!(r4.A0Q == null || (A032 = A03(view)) == null)) {
                A032.A00 = null;
            }
            if (r4.A0H != null && (view instanceof ComponentHost)) {
                ((ComponentHost) view).A0B = null;
            }
            boolean z2 = view instanceof ComponentHost;
            if (z2) {
                ((ComponentHost) view).A0D = null;
            } else {
                view.setTag(null);
            }
            SparseArray sparseArray = r4.A0C;
            SparseArray sparseArray2 = sparseArray;
            if (z2) {
                ((ComponentHost) view).A00 = null;
            } else if (sparseArray != null) {
                int size = sparseArray.size();
                for (int i = 0; i < size; i++) {
                    view.setTag(sparseArray2.keyAt(i), null);
                }
            }
            if (r4.A05 != 0.0f) {
                C15320v6.setElevation(view, 0.0f);
            }
            if (r4.A0D != null && Build.VERSION.SDK_INT >= 21) {
                view.setOutlineProvider(ViewOutlineProvider.BACKGROUND);
            }
            if (r4.A0W && Build.VERSION.SDK_INT >= 21) {
                view.setClipToOutline(false);
            }
            if (!r4.A0V && (view instanceof ViewGroup)) {
                ((ViewGroup) view).setClipChildren(true);
            }
            if (!TextUtils.isEmpty(r4.A0S)) {
                view.setContentDescription(null);
            }
            if (Build.VERSION.SDK_INT >= 11) {
                boolean z3 = false;
                if ((r4.A0A & DexStore.LOAD_RESULT_WITH_VDEX_ODEX) != 0) {
                    z3 = true;
                }
                if (z3) {
                    if (view.getScaleX() != 1.0f) {
                        view.setScaleX(1.0f);
                    }
                    if (view.getScaleY() != 1.0f) {
                        view.setScaleY(1.0f);
                    }
                }
            }
            if (Build.VERSION.SDK_INT >= 11) {
                boolean z4 = false;
                if ((r4.A0A & 1048576) != 0) {
                    z4 = true;
                }
                if (z4 && view.getAlpha() != 1.0f) {
                    view.setAlpha(1.0f);
                }
            }
            if (Build.VERSION.SDK_INT >= 11) {
                boolean z5 = false;
                if ((r4.A0A & 2097152) != 0) {
                    z5 = true;
                }
                if (z5 && view.getRotation() != 0.0f) {
                    view.setRotation(0.0f);
                }
            }
            if (Build.VERSION.SDK_INT >= 11) {
                boolean z6 = false;
                if ((r4.A0A & 33554432) != 0) {
                    z6 = true;
                }
                if (z6 && view.getRotationX() != 0.0f) {
                    view.setRotationX(0.0f);
                }
            }
            if (Build.VERSION.SDK_INT >= 11) {
                boolean z7 = false;
                if ((r4.A0A & 67108864) != 0) {
                    z7 = true;
                }
                if (z7 && view.getRotationY() != 0.0f) {
                    view.setRotationY(0.0f);
                }
            }
        }
        boolean z8 = true;
        if ((r8.A02 & 1) != 1) {
            z8 = false;
        }
        view.setClickable(z8);
        boolean z9 = false;
        if ((r8.A02 & 2) == 2) {
            z9 = true;
        }
        view.setLongClickable(z9);
        boolean z10 = false;
        if ((r8.A02 & 4) == 4) {
            z10 = true;
        }
        view.setFocusable(z10);
        boolean z11 = false;
        if ((r8.A02 & 8) == 8) {
            z11 = true;
        }
        view.setEnabled(z11);
        boolean z12 = false;
        if ((r8.A02 & 16) == 16) {
            z12 = true;
        }
        view.setSelected(z12);
        if (r8.A00 != 0) {
            C15320v6.setImportantForAccessibility(view, 0);
        }
        boolean z13 = view instanceof ComponentHost;
        if (z13 || view.getTag(2131297259) != null) {
            view.setTag(2131297259, null);
            if (!z13) {
                C15320v6.setAccessibilityDelegate(view, null);
            }
        }
        C17650zF r42 = r8.A08;
        if (r42 != null) {
            if (!(r42.A01 == null && r42.A00 == 0)) {
                if (Build.VERSION.SDK_INT >= 21) {
                    view.setStateListAnimator(null);
                } else {
                    throw new IllegalStateException("MountState has a ViewNodeInfo with stateListAnimator, however the current Android version doesn't support stateListAnimator on Views");
                }
            }
            if (!z) {
                boolean z14 = false;
                if (r42.A03 != null) {
                    z14 = true;
                }
                if (z14) {
                    try {
                        view.setPadding(0, 0, 0, 0);
                    } catch (NullPointerException e) {
                        throw new NullPointerException(AnonymousClass08S.A0U("Component: ", r8.A04.A1A(), ", view: ", view.getClass().getSimpleName(), ", message: ", e.getMessage()));
                    }
                }
                if (r42.A04 != null) {
                    if (Build.VERSION.SDK_INT < 16) {
                        view.setBackgroundDrawable(null);
                    } else {
                        view.setBackground(null);
                    }
                }
                if (r42.A05 != null) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        view.setForeground(null);
                    } else {
                        throw new IllegalStateException("MountState has a ViewNodeInfo with foreground however the current Android version doesn't support foreground on Views");
                    }
                }
                if (Build.VERSION.SDK_INT >= 17) {
                    view.setLayoutDirection(2);
                }
            }
        }
    }

    public static void A0D(C17600zA r24, int i, C17730zN r26, C188915v r27) {
        Object ALb;
        AnonymousClass1KE r0;
        Map AmD;
        long nanoTime = System.nanoTime();
        C17730zN r7 = r26;
        long j = r7.A07;
        C17600zA r6 = r24;
        ComponentHost componentHost = (ComponentHost) r6.A0I.A07(j);
        if (componentHost == null) {
            C188915v r4 = r27;
            int A092 = r4.A09(j);
            A0D(r6, A092, r4.A0A(A092), r4);
            componentHost = (ComponentHost) r6.A0I.A07(j);
        }
        C17770zR r3 = r7.A09;
        if (r3 != null) {
            Context context = r6.A0K.A09;
            C15690vh A002 = C15040ud.A00(context, r3);
            if (A002 == null) {
                ALb = r3.A0V(context);
            } else {
                ALb = A002.ALb(context, r3);
            }
            AnonymousClass0p4 r2 = r3.A03;
            if (r2 == null) {
                r2 = r6.A0K;
            }
            r3.A0h(r2, ALb);
            if (r3 instanceof C17670zH) {
                ComponentHost componentHost2 = (ComponentHost) ALb;
                long j2 = r7.A02;
                componentHost2.A0O(true);
                r6.A0I.A0D(j2, componentHost2);
            }
            Object obj = ALb;
            C17830zY r15 = new C17830zY(r3, componentHost, obj, r7.A0A, r7.A0C, r7.A04, r7.A05, r7.A06, r7.A0B);
            int i2 = i;
            r6.A0J.A0D(r6.A0F[i], r15);
            if (r3.A0v()) {
                r6.A0H.A0D(r6.A0F[i], r15);
            }
            Rect rect = A0V;
            r7.A00(rect);
            componentHost.A0K(i2, r15, rect);
            A0A(r15);
            A0E(r6, r3, ALb);
            r15.A09 = true;
            Rect rect2 = A0V;
            r7.A00(rect2);
            A0J(r15.A00(), rect2.left, rect2.top, rect2.right, rect2.bottom, true);
            C17630zD r1 = r6.A0O;
            if (r1.A0F) {
                r1.A06.add(Double.valueOf(((double) (System.nanoTime() - nanoTime)) / 1000000.0d));
                r6.A0O.A07.add(r3.A1A());
                C17630zD r12 = r6.A0O;
                r12.A01++;
                List list = r12.A05;
                C637038i A052 = r2.A05();
                AnonymousClass0p4 r02 = r3.A03;
                String str = null;
                if (!(r02 == null || (r0 = r02.A07) == null || (AmD = A052.AmD(r0)) == null)) {
                    StringBuilder sb = new StringBuilder(AmD.size() << 4);
                    for (Map.Entry entry : AmD.entrySet()) {
                        sb.append((String) entry.getKey());
                        sb.append(':');
                        sb.append((String) entry.getValue());
                        sb.append(';');
                    }
                    str = sb.toString();
                }
                list.add(str);
                return;
            }
            return;
        }
        throw new RuntimeException("Trying to mount a LayoutOutput with a null Component.");
    }

    public static void A0G(C17600zA r8, C188915v r9, int i, boolean z) {
        int A002 = A00(r9, i);
        for (int i2 = i; i2 <= A002; i2++) {
            if (z) {
                int[] iArr = r8.A0E;
                iArr[i2] = iArr[i2] + 1;
            } else {
                int[] iArr2 = r8.A0E;
                int i3 = iArr2[i2] - 1;
                iArr2[i2] = i3;
                if (i3 < 0) {
                    C09070gU.A01(AnonymousClass07B.A0C, "MountState:InvalidAnimLockIndices", "Decremented animation lock count below 0!");
                    r8.A0E[i2] = 0;
                }
            }
        }
        C17730zN A0A2 = r9.A0A(i);
        while (true) {
            long j = A0A2.A07;
            if (j != 0) {
                int A092 = r9.A09(j);
                int[] iArr3 = r8.A0E;
                int i4 = iArr3[A092];
                if (z) {
                    iArr3[A092] = i4 + 1;
                } else {
                    int i5 = i4 - 1;
                    iArr3[A092] = i5;
                    if (i5 < 0) {
                        C09070gU.A01(AnonymousClass07B.A0C, "MountState:InvalidAnimLockIndices", "Decremented animation lock count below 0!");
                        r8.A0E[A092] = 0;
                    }
                }
                A0A2 = r9.A0A(A092);
            } else {
                return;
            }
        }
    }

    public static void A0H(C17600zA r7, C17720zM r8) {
        C17610zB r1;
        int A032;
        C17750zP r2 = ((C17830zY) r8.A02()).A07;
        C17860zb r12 = r7.A07;
        if (!(r12 == null || r2 == null)) {
            r12.A09(r2, null);
        }
        short s = r8.A00;
        int i = 0;
        while (i < s) {
            C17830zY r4 = (C17830zY) r8.A03(i);
            if (r8.A01(i) == 3) {
                ComponentHost componentHost = (ComponentHost) r4.A00();
                for (int A0C2 = componentHost.A0C() - 1; A0C2 >= 0; A0C2--) {
                    r7.A06(r7.A0K, (C17830zY) componentHost.A02.A05(A0C2));
                }
                if (componentHost.A0C() > 0) {
                    throw new IllegalStateException("Recursively unmounting items from a ComponentHost, left some items behind maybe because not tracked by its MountState");
                }
            }
            r4.A05.A0L(r4);
            A09(r4);
            r7.A08(r4);
            if (r4.A04.A0v() && (A032 = (r1 = r7.A0H).A03(r4)) > 0) {
                r1.A0A(A032);
            }
            r7.A07(r4);
            try {
                r4.A01(r7.A0K.A09, "endUnmountDisappearingItem");
                i++;
            } catch (C37781wF e) {
                throw new RuntimeException(AnonymousClass08S.A0P(e.getMessage(), " ", A04(r7, r4)));
            }
        }
    }

    public static boolean A0L(C17600zA r3, C188915v r4, int i) {
        C188915v r0;
        C17750zP r1;
        C17870zc r02;
        if (!A0K(r3, r4)) {
            return false;
        }
        boolean z = false;
        if (r3.A06 != null) {
            z = true;
        }
        if (!z || r3.A07 == null || (r0 = r3.A05) == null || (r1 = r0.A0A(i).A0B) == null || (r02 = (C17870zc) r3.A07.A03.A02.get(r1)) == null || r02.A00 != 2) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:104:0x021e, code lost:
        if (r22 == false) goto L_0x0220;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01be, code lost:
        if (r2.equals(r32) == false) goto L_0x01c0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Q(X.C188915v r34, android.graphics.Rect r35, X.C72593eb r36) {
        /*
            r33 = this;
            boolean r30 = X.C27041cY.A02()
            java.lang.String r6 = "VISIBILITY_HANDLERS_END"
            r7 = r36
            if (r36 == 0) goto L_0x000f
            java.lang.String r0 = "VISIBILITY_HANDLERS_START"
            r7.A00(r0)     // Catch:{ all -> 0x03eb }
        L_0x000f:
            if (r30 == 0) goto L_0x0016
            java.lang.String r0 = "processVisibilityOutputs"
            X.C27041cY.A01(r0)     // Catch:{ all -> 0x03eb }
        L_0x0016:
            r8 = r34
            boolean r0 = r8.A0j     // Catch:{ all -> 0x03eb }
            r9 = r33
            r31 = r35
            if (r0 == 0) goto L_0x0162
            X.1ji r0 = r9.A08     // Catch:{ all -> 0x03eb }
            if (r0 != 0) goto L_0x002f
            com.facebook.litho.LithoView r1 = r9.A0M     // Catch:{ all -> 0x03eb }
            if (r1 == 0) goto L_0x03e0
            X.1ji r0 = new X.1ji     // Catch:{ all -> 0x03eb }
            r0.<init>(r1)     // Catch:{ all -> 0x03eb }
            r9.A08 = r0     // Catch:{ all -> 0x03eb }
        L_0x002f:
            X.1ji r5 = r9.A08     // Catch:{ all -> 0x03eb }
            boolean r3 = r9.A09     // Catch:{ all -> 0x03eb }
            X.1jj r4 = r8.A0b     // Catch:{ all -> 0x03eb }
            android.graphics.Rect r8 = r9.A0G     // Catch:{ all -> 0x03eb }
            com.facebook.litho.LithoView r0 = r5.A03     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x015a
            X.1jm r0 = r5.A00     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x00ca
            if (r3 == 0) goto L_0x00ba
            java.util.ArrayList r10 = r4.A00     // Catch:{ all -> 0x03eb }
            java.util.List r0 = r5.A06     // Catch:{ all -> 0x03eb }
            r0.clear()     // Catch:{ all -> 0x03eb }
            java.util.List r0 = r5.A05     // Catch:{ all -> 0x03eb }
            r0.clear()     // Catch:{ all -> 0x03eb }
            if (r10 == 0) goto L_0x00ba
            java.util.List r0 = r5.A06     // Catch:{ all -> 0x03eb }
            r0.addAll(r10)     // Catch:{ all -> 0x03eb }
            java.util.List r0 = r5.A05     // Catch:{ all -> 0x03eb }
            r0.addAll(r10)     // Catch:{ all -> 0x03eb }
            boolean r0 = r10.isEmpty()     // Catch:{ all -> 0x03eb }
            if (r0 != 0) goto L_0x00ba
            r9 = 0
            int r2 = r10.size()     // Catch:{ all -> 0x03eb }
        L_0x0064:
            if (r9 >= r2) goto L_0x00ac
            java.lang.Object r12 = r10.get(r9)     // Catch:{ all -> 0x03eb }
            X.1jk r12 = (X.C31471jk) r12     // Catch:{ all -> 0x03eb }
            com.facebook.litho.LithoView r0 = r5.A03     // Catch:{ all -> 0x03eb }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ all -> 0x03eb }
            android.view.View r0 = (android.view.View) r0     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x00a9
            int r1 = r0.getWidth()     // Catch:{ all -> 0x03eb }
            int r0 = r0.getHeight()     // Catch:{ all -> 0x03eb }
            int r1 = r1 * r0
            int r13 = r1 / 2
            X.117 r0 = r12.A00     // Catch:{ all -> 0x03eb }
            android.graphics.Rect r11 = r0.A0A     // Catch:{ all -> 0x03eb }
            boolean r0 = r11.isEmpty()     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x008c
            goto L_0x0096
        L_0x008c:
            int r1 = r11.width()     // Catch:{ all -> 0x03eb }
            int r0 = r11.height()     // Catch:{ all -> 0x03eb }
            int r0 = r0 * r1
            goto L_0x0097
        L_0x0096:
            r0 = 0
        L_0x0097:
            if (r0 < r13) goto L_0x00a3
            r1 = 1056964608(0x3f000000, float:0.5)
            float r0 = (float) r13     // Catch:{ all -> 0x03eb }
            float r1 = r1 * r0
            float r1 = r1 / r0
            X.117 r0 = r12.A00     // Catch:{ all -> 0x03eb }
            r0.A00 = r1     // Catch:{ all -> 0x03eb }
            goto L_0x00a9
        L_0x00a3:
            X.117 r1 = r12.A00     // Catch:{ all -> 0x03eb }
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.A00 = r0     // Catch:{ all -> 0x03eb }
        L_0x00a9:
            int r9 = r9 + 1
            goto L_0x0064
        L_0x00ac:
            java.util.List r1 = r5.A06     // Catch:{ all -> 0x03eb }
            java.util.Comparator r0 = X.C31491jm.A07     // Catch:{ all -> 0x03eb }
            java.util.Collections.sort(r1, r0)     // Catch:{ all -> 0x03eb }
            java.util.List r1 = r5.A05     // Catch:{ all -> 0x03eb }
            java.util.Comparator r0 = X.C31491jm.A06     // Catch:{ all -> 0x03eb }
            java.util.Collections.sort(r1, r0)     // Catch:{ all -> 0x03eb }
        L_0x00ba:
            X.1jm r2 = r5.A00     // Catch:{ all -> 0x03eb }
            java.util.List r1 = r5.A06     // Catch:{ all -> 0x03eb }
            java.util.List r0 = r5.A05     // Catch:{ all -> 0x03eb }
            r9 = r2
            r10 = r3
            r11 = r1
            r12 = r0
            r13 = r31
            r14 = r8
            r9.A04(r10, r11, r12, r13, r14)     // Catch:{ all -> 0x03eb }
        L_0x00ca:
            X.1jm r2 = r5.A02     // Catch:{ all -> 0x03eb }
            if (r2 == 0) goto L_0x00dc
            java.util.ArrayList r1 = r4.A03     // Catch:{ all -> 0x03eb }
            java.util.ArrayList r0 = r4.A04     // Catch:{ all -> 0x03eb }
            r9 = r2
            r10 = r3
            r11 = r1
            r12 = r0
            r13 = r31
            r14 = r8
            r9.A04(r10, r11, r12, r13, r14)     // Catch:{ all -> 0x03eb }
        L_0x00dc:
            X.1jm r2 = r5.A01     // Catch:{ all -> 0x03eb }
            if (r2 == 0) goto L_0x00ee
            java.util.ArrayList r1 = r4.A02     // Catch:{ all -> 0x03eb }
            java.util.ArrayList r0 = r4.A01     // Catch:{ all -> 0x03eb }
            r9 = r2
            r10 = r3
            r11 = r1
            r12 = r0
            r13 = r31
            r14 = r8
            r9.A04(r10, r11, r12, r13, r14)     // Catch:{ all -> 0x03eb }
        L_0x00ee:
            java.util.ArrayList r9 = r4.A05     // Catch:{ all -> 0x03eb }
            if (r9 == 0) goto L_0x0155
            boolean r0 = r9.isEmpty()     // Catch:{ all -> 0x03eb }
            if (r0 != 0) goto L_0x0155
            r8 = 0
            r4 = 0
        L_0x00fa:
            int r0 = r9.size()     // Catch:{ all -> 0x03eb }
            if (r4 >= r0) goto L_0x03e0
            java.lang.Object r3 = r9.get(r4)     // Catch:{ all -> 0x03eb }
            X.117 r3 = (X.AnonymousClass117) r3     // Catch:{ all -> 0x03eb }
            android.graphics.Rect r1 = new android.graphics.Rect     // Catch:{ all -> 0x03eb }
            r1.<init>()     // Catch:{ all -> 0x03eb }
            android.graphics.Rect r11 = r3.A0A     // Catch:{ all -> 0x03eb }
            r0 = r31
            boolean r0 = r1.setIntersect(r11, r0)     // Catch:{ all -> 0x03eb }
            if (r0 != 0) goto L_0x0132
            java.util.Map r1 = r5.A04     // Catch:{ all -> 0x03eb }
            java.lang.String r0 = r3.A00()     // Catch:{ all -> 0x03eb }
            boolean r0 = r1.containsKey(r0)     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x0127
            X.10N r1 = r3.A08     // Catch:{ all -> 0x03eb }
            r0 = 0
            X.C33181nA.A05(r1, r8, r0)     // Catch:{ all -> 0x03eb }
        L_0x0127:
            java.util.Map r1 = r5.A04     // Catch:{ all -> 0x03eb }
            java.lang.String r0 = r3.A00()     // Catch:{ all -> 0x03eb }
            r1.remove(r0)     // Catch:{ all -> 0x03eb }
            goto L_0x03e0
        L_0x0132:
            int r10 = r1.bottom     // Catch:{ all -> 0x03eb }
            int r0 = r1.top     // Catch:{ all -> 0x03eb }
            int r10 = r10 - r0
            X.10N r2 = r3.A08     // Catch:{ all -> 0x03eb }
            r0 = 1120403456(0x42c80000, float:100.0)
            r11.width()     // Catch:{ all -> 0x03eb }
            float r1 = (float) r10     // Catch:{ all -> 0x03eb }
            float r1 = r1 * r0
            int r0 = r11.height()     // Catch:{ all -> 0x03eb }
            float r0 = (float) r0     // Catch:{ all -> 0x03eb }
            float r1 = r1 / r0
            X.C33181nA.A05(r2, r10, r1)     // Catch:{ all -> 0x03eb }
            java.util.Map r1 = r5.A04     // Catch:{ all -> 0x03eb }
            java.lang.String r0 = r3.A00()     // Catch:{ all -> 0x03eb }
            r1.put(r0, r3)     // Catch:{ all -> 0x03eb }
            int r4 = r4 + 1
            goto L_0x00fa
        L_0x0155:
            X.C31451ji.A00(r5)     // Catch:{ all -> 0x03eb }
            goto L_0x03e0
        L_0x015a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x03eb }
            java.lang.String r0 = "Trying to process visibility outputs but module has not been initialized with a LithoView"
            r1.<init>(r0)     // Catch:{ all -> 0x03eb }
            throw r1     // Catch:{ all -> 0x03eb }
        L_0x0162:
            if (r35 == 0) goto L_0x03e0
            X.0zD r0 = r9.A0O     // Catch:{ all -> 0x03eb }
            boolean r5 = r0.A0F     // Catch:{ all -> 0x03eb }
            boolean r29 = X.C27041cY.A02()     // Catch:{ all -> 0x03eb }
            if (r5 == 0) goto L_0x017a
            long r16 = java.lang.System.nanoTime()     // Catch:{ all -> 0x03eb }
        L_0x0172:
            java.util.List r0 = r8.A0N     // Catch:{ all -> 0x03eb }
            int r28 = r0.size()     // Catch:{ all -> 0x03eb }
            r4 = 0
            goto L_0x017d
        L_0x017a:
            r16 = 0
            goto L_0x0172
        L_0x017d:
            r0 = r28
            if (r4 >= r0) goto L_0x03c6
            java.util.List r0 = r8.A0N     // Catch:{ all -> 0x03eb }
            java.lang.Object r27 = r0.get(r4)     // Catch:{ all -> 0x03eb }
            r0 = r27
            X.117 r0 = (X.AnonymousClass117) r0     // Catch:{ all -> 0x03eb }
            r27 = r0
            java.lang.String r26 = "Unknown"
            if (r29 == 0) goto L_0x01a6
            X.0zR r0 = r0.A03     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x0196
            goto L_0x0199
        L_0x0196:
            r1 = r26
            goto L_0x019d
        L_0x0199:
            java.lang.String r1 = r0.A1A()     // Catch:{ all -> 0x03eb }
        L_0x019d:
            java.lang.String r0 = "visibilityHandlers:"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r1)     // Catch:{ all -> 0x03eb }
            X.C27041cY.A01(r0)     // Catch:{ all -> 0x03eb }
        L_0x01a6:
            r0 = r27
            android.graphics.Rect r0 = r0.A0A     // Catch:{ all -> 0x03eb }
            r32 = r0
            android.graphics.Rect r2 = X.C17600zA.A0V     // Catch:{ all -> 0x03eb }
            r1 = r0
            r0 = r31
            boolean r15 = r2.setIntersect(r1, r0)     // Catch:{ all -> 0x03eb }
            if (r15 == 0) goto L_0x01c0
            r0 = r32
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x03eb }
            r12 = 1
            if (r0 != 0) goto L_0x01c1
        L_0x01c0:
            r12 = 0
        L_0x01c1:
            java.lang.String r11 = r27.A00()     // Catch:{ all -> 0x03eb }
            java.util.Map r0 = r9.A0T     // Catch:{ all -> 0x03eb }
            java.lang.Object r3 = r0.get(r11)     // Catch:{ all -> 0x03eb }
            X.1Jz r3 = (X.C22121Jz) r3     // Catch:{ all -> 0x03eb }
            if (r3 == 0) goto L_0x01d4
            boolean r0 = r3.A04     // Catch:{ all -> 0x03eb }
            r3.A04 = r12     // Catch:{ all -> 0x03eb }
            goto L_0x01d5
        L_0x01d4:
            r0 = 0
        L_0x01d5:
            if (r12 == 0) goto L_0x01e8
            if (r0 == 0) goto L_0x01e8
            boolean r0 = X.AnonymousClass07c.skipVisChecksForFullyVisible     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x01e8
            if (r29 == 0) goto L_0x01e2
            X.C27041cY.A00()     // Catch:{ all -> 0x03eb }
        L_0x01e2:
            boolean r0 = r9.A09     // Catch:{ all -> 0x03eb }
            r3.A03 = r0     // Catch:{ all -> 0x03eb }
            goto L_0x03c2
        L_0x01e8:
            if (r5 == 0) goto L_0x01eb
            goto L_0x01ee
        L_0x01eb:
            r18 = 0
            goto L_0x01f2
        L_0x01ee:
            long r18 = java.lang.System.nanoTime()     // Catch:{ all -> 0x03eb }
        L_0x01f2:
            r0 = r27
            X.10N r0 = r0.A09     // Catch:{ all -> 0x03eb }
            r25 = r0
            r0 = r27
            X.10N r13 = r0.A04     // Catch:{ all -> 0x03eb }
            X.10N r10 = r0.A07     // Catch:{ all -> 0x03eb }
            X.10N r0 = r0.A05     // Catch:{ all -> 0x03eb }
            r24 = r0
            r0 = r27
            X.10N r14 = r0.A06     // Catch:{ all -> 0x03eb }
            X.10N r2 = r0.A08     // Catch:{ all -> 0x03eb }
            if (r15 == 0) goto L_0x0220
            android.graphics.Rect r23 = X.C17600zA.A0V     // Catch:{ all -> 0x03eb }
            float r15 = r0.A01     // Catch:{ all -> 0x03eb }
            float r1 = r0.A02     // Catch:{ all -> 0x03eb }
            r22 = 1
            r20 = 0
            int r0 = (r15 > r20 ? 1 : (r15 == r20 ? 0 : -1))
            if (r0 != 0) goto L_0x0227
            int r0 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r0 != 0) goto L_0x0227
        L_0x021c:
            r21 = 1
            if (r22 != 0) goto L_0x0222
        L_0x0220:
            r21 = 0
        L_0x0222:
            r20 = 0
            if (r3 == 0) goto L_0x028f
            goto L_0x0259
        L_0x0227:
            r0 = r32
            int r21 = r0.height()     // Catch:{ all -> 0x03eb }
            int r0 = r23.height()     // Catch:{ all -> 0x03eb }
            float r0 = (float) r0     // Catch:{ all -> 0x03eb }
            r20 = r0
            r0 = r21
            float r0 = (float) r0     // Catch:{ all -> 0x03eb }
            float r15 = r15 * r0
            int r15 = (r20 > r15 ? 1 : (r20 == r15 ? 0 : -1))
            r0 = 0
            if (r15 < 0) goto L_0x023e
            r0 = 1
        L_0x023e:
            if (r0 == 0) goto L_0x0256
            r0 = r32
            int r0 = r0.width()     // Catch:{ all -> 0x03eb }
            int r15 = r23.width()     // Catch:{ all -> 0x03eb }
            float r15 = (float) r15     // Catch:{ all -> 0x03eb }
            float r0 = (float) r0     // Catch:{ all -> 0x03eb }
            float r1 = r1 * r0
            int r1 = (r15 > r1 ? 1 : (r15 == r1 ? 0 : -1))
            r0 = 0
            if (r1 < 0) goto L_0x0253
            r0 = 1
        L_0x0253:
            if (r0 == 0) goto L_0x0256
            goto L_0x021c
        L_0x0256:
            r22 = 0
            goto L_0x021c
        L_0x0259:
            r3.A02 = r10     // Catch:{ all -> 0x03eb }
            r3.A01 = r14     // Catch:{ all -> 0x03eb }
            if (r21 != 0) goto L_0x0288
            if (r14 == 0) goto L_0x0264
            X.C33181nA.A02(r14)     // Catch:{ all -> 0x03eb }
        L_0x0264:
            if (r2 == 0) goto L_0x0267
            goto L_0x0269
        L_0x0267:
            r1 = 0
            goto L_0x026e
        L_0x0269:
            r0 = 0
            r1 = 0
            X.C33181nA.A05(r2, r1, r0)     // Catch:{ all -> 0x03eb }
        L_0x026e:
            int r0 = r3.A00     // Catch:{ all -> 0x03eb }
            r15 = r0 & 32
            r0 = 0
            if (r15 == 0) goto L_0x0276
            r0 = 1
        L_0x0276:
            if (r0 == 0) goto L_0x0282
            r3.A00(r1)     // Catch:{ all -> 0x03eb }
            X.10N r0 = r3.A02     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x0282
            X.C33181nA.A03(r0)     // Catch:{ all -> 0x03eb }
        L_0x0282:
            java.util.Map r0 = r9.A0T     // Catch:{ all -> 0x03eb }
            r0.remove(r11)     // Catch:{ all -> 0x03eb }
            goto L_0x028d
        L_0x0288:
            boolean r0 = r9.A09     // Catch:{ all -> 0x03eb }
            r3.A03 = r0     // Catch:{ all -> 0x03eb }
            goto L_0x028f
        L_0x028d:
            r3 = r20
        L_0x028f:
            if (r21 == 0) goto L_0x0390
            if (r3 != 0) goto L_0x02ac
            r0 = r27
            X.0zR r0 = r0.A03     // Catch:{ all -> 0x03eb }
            X.1Jz r3 = new X.1Jz     // Catch:{ all -> 0x03eb }
            r3.<init>(r14, r10, r2)     // Catch:{ all -> 0x03eb }
            boolean r0 = r9.A09     // Catch:{ all -> 0x03eb }
            r3.A03 = r0     // Catch:{ all -> 0x03eb }
            r3.A04 = r12     // Catch:{ all -> 0x03eb }
            java.util.Map r0 = r9.A0T     // Catch:{ all -> 0x03eb }
            r0.put(r11, r3)     // Catch:{ all -> 0x03eb }
            if (r25 == 0) goto L_0x02ac
            X.C33181nA.A04(r25)     // Catch:{ all -> 0x03eb }
        L_0x02ac:
            if (r13 != 0) goto L_0x02b0
            if (r10 == 0) goto L_0x0323
        L_0x02b0:
            android.graphics.Rect r15 = X.C17600zA.A0V     // Catch:{ all -> 0x03eb }
            com.facebook.litho.LithoView r0 = r9.A0M     // Catch:{ all -> 0x03eb }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ all -> 0x03eb }
            android.view.View r0 = (android.view.View) r0     // Catch:{ all -> 0x03eb }
            r14 = 0
            if (r0 == 0) goto L_0x02f2
            int r1 = r0.getWidth()     // Catch:{ all -> 0x03eb }
            int r0 = r0.getHeight()     // Catch:{ all -> 0x03eb }
            int r1 = r1 * r0
            int r11 = r1 / 2
            r1 = r32
            boolean r0 = r1.isEmpty()     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x02d2
            r1 = 0
            goto L_0x02db
        L_0x02d2:
            int r0 = r1.width()     // Catch:{ all -> 0x03eb }
            int r1 = r1.height()     // Catch:{ all -> 0x03eb }
            int r1 = r1 * r0
        L_0x02db:
            boolean r0 = r15.isEmpty()     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x02e2
            goto L_0x02ec
        L_0x02e2:
            int r12 = r15.width()     // Catch:{ all -> 0x03eb }
            int r0 = r15.height()     // Catch:{ all -> 0x03eb }
            int r0 = r0 * r12
            goto L_0x02ed
        L_0x02ec:
            r0 = 0
        L_0x02ed:
            if (r1 < r11) goto L_0x02f5
            if (r0 < r11) goto L_0x02f2
            r14 = 1
        L_0x02f2:
            if (r14 == 0) goto L_0x0310
            goto L_0x02fc
        L_0x02f5:
            r0 = r32
            boolean r14 = r0.equals(r15)     // Catch:{ all -> 0x03eb }
            goto L_0x02f2
        L_0x02fc:
            int r0 = r3.A00     // Catch:{ all -> 0x03eb }
            r1 = r0 & 32
            r0 = 0
            if (r1 == 0) goto L_0x0304
            r0 = 1
        L_0x0304:
            if (r0 != 0) goto L_0x0323
            r0 = 1
            r3.A00(r0)     // Catch:{ all -> 0x03eb }
            if (r13 == 0) goto L_0x0323
            X.C33181nA.A00(r13)     // Catch:{ all -> 0x03eb }
            goto L_0x0323
        L_0x0310:
            int r0 = r3.A00     // Catch:{ all -> 0x03eb }
            r1 = r0 & 32
            r0 = 0
            if (r1 == 0) goto L_0x0318
            r0 = 1
        L_0x0318:
            if (r0 == 0) goto L_0x0323
            r0 = 0
            r3.A00(r0)     // Catch:{ all -> 0x03eb }
            if (r10 == 0) goto L_0x0323
            X.C33181nA.A03(r10)     // Catch:{ all -> 0x03eb }
        L_0x0323:
            if (r24 == 0) goto L_0x0377
            int r12 = r3.A00     // Catch:{ all -> 0x03eb }
            r10 = 30
            r1 = r12 & r10
            r0 = 0
            if (r1 != r10) goto L_0x032f
            r0 = 1
        L_0x032f:
            if (r0 != 0) goto L_0x0377
            android.graphics.Rect r11 = X.C17600zA.A0V     // Catch:{ all -> 0x03eb }
            r0 = r32
            int r1 = r0.top     // Catch:{ all -> 0x03eb }
            int r0 = r11.top     // Catch:{ all -> 0x03eb }
            if (r1 != r0) goto L_0x033f
            r0 = r12 | 4
            r3.A00 = r0     // Catch:{ all -> 0x03eb }
        L_0x033f:
            r0 = r32
            int r1 = r0.bottom     // Catch:{ all -> 0x03eb }
            int r0 = r11.bottom     // Catch:{ all -> 0x03eb }
            if (r1 != r0) goto L_0x034d
            int r0 = r3.A00     // Catch:{ all -> 0x03eb }
            r0 = r0 | 16
            r3.A00 = r0     // Catch:{ all -> 0x03eb }
        L_0x034d:
            r0 = r32
            int r1 = r0.left     // Catch:{ all -> 0x03eb }
            int r0 = r11.left     // Catch:{ all -> 0x03eb }
            if (r1 != r0) goto L_0x035b
            int r0 = r3.A00     // Catch:{ all -> 0x03eb }
            r0 = r0 | 2
            r3.A00 = r0     // Catch:{ all -> 0x03eb }
        L_0x035b:
            r0 = r32
            int r1 = r0.right     // Catch:{ all -> 0x03eb }
            int r0 = r11.right     // Catch:{ all -> 0x03eb }
            if (r1 != r0) goto L_0x0369
            int r0 = r3.A00     // Catch:{ all -> 0x03eb }
            r0 = r0 | 8
            r3.A00 = r0     // Catch:{ all -> 0x03eb }
        L_0x0369:
            int r3 = r3.A00     // Catch:{ all -> 0x03eb }
            r1 = 30
            r3 = r3 & r10
            r0 = 0
            if (r3 != r1) goto L_0x0372
            r0 = 1
        L_0x0372:
            if (r0 == 0) goto L_0x0377
            X.C33181nA.A01(r24)     // Catch:{ all -> 0x03eb }
        L_0x0377:
            if (r2 == 0) goto L_0x0390
            android.graphics.Rect r0 = X.C17600zA.A0V     // Catch:{ all -> 0x03eb }
            int r3 = r0.bottom     // Catch:{ all -> 0x03eb }
            int r0 = r0.top     // Catch:{ all -> 0x03eb }
            int r3 = r3 - r0
            r0 = 1120403456(0x42c80000, float:100.0)
            r32.width()     // Catch:{ all -> 0x03eb }
            float r1 = (float) r3     // Catch:{ all -> 0x03eb }
            float r1 = r1 * r0
            int r0 = r32.height()     // Catch:{ all -> 0x03eb }
            float r0 = (float) r0     // Catch:{ all -> 0x03eb }
            float r1 = r1 / r0
            X.C33181nA.A05(r2, r3, r1)     // Catch:{ all -> 0x03eb }
        L_0x0390:
            if (r5 == 0) goto L_0x03bd
            r0 = r27
            X.0zR r0 = r0.A03     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x039c
            java.lang.String r26 = r0.A1A()     // Catch:{ all -> 0x03eb }
        L_0x039c:
            X.0zD r0 = r9.A0O     // Catch:{ all -> 0x03eb }
            java.util.List r10 = r0.A0D     // Catch:{ all -> 0x03eb }
            long r2 = java.lang.System.nanoTime()     // Catch:{ all -> 0x03eb }
            long r2 = r2 - r18
            double r0 = (double) r2     // Catch:{ all -> 0x03eb }
            r2 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r0 = r0 / r2
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ all -> 0x03eb }
            r10.add(r0)     // Catch:{ all -> 0x03eb }
            X.0zD r0 = r9.A0O     // Catch:{ all -> 0x03eb }
            java.util.List r1 = r0.A0C     // Catch:{ all -> 0x03eb }
            r0 = r26
            r1.add(r0)     // Catch:{ all -> 0x03eb }
        L_0x03bd:
            if (r29 == 0) goto L_0x03c2
            X.C27041cY.A00()     // Catch:{ all -> 0x03eb }
        L_0x03c2:
            int r4 = r4 + 1
            goto L_0x017d
        L_0x03c6:
            boolean r0 = r9.A09     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x03cd
            r9.A0O()     // Catch:{ all -> 0x03eb }
        L_0x03cd:
            if (r5 == 0) goto L_0x03e0
            X.0zD r4 = r9.A0O     // Catch:{ all -> 0x03eb }
            long r0 = java.lang.System.nanoTime()     // Catch:{ all -> 0x03eb }
            long r0 = r0 - r16
            double r2 = (double) r0     // Catch:{ all -> 0x03eb }
            r0 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r2 = r2 / r0
            r4.A00 = r2     // Catch:{ all -> 0x03eb }
        L_0x03e0:
            if (r30 == 0) goto L_0x03e5
            X.C27041cY.A00()
        L_0x03e5:
            if (r36 == 0) goto L_0x03ea
            r7.A00(r6)
        L_0x03ea:
            return
        L_0x03eb:
            r0 = move-exception
            if (r30 == 0) goto L_0x03f1
            X.C27041cY.A00()
        L_0x03f1:
            if (r36 == 0) goto L_0x03f6
            r7.A00(r6)
        L_0x03f6:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17600zA.A0Q(X.15v, android.graphics.Rect, X.3eb):void");
    }
}
