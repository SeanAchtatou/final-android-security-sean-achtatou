package X;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/* renamed from: X.0je  reason: invalid class name and case insensitive filesystem */
public interface C10160je {
    boolean isCreatorVisible(C183512m r1);

    boolean isFieldVisible(C29091fr r1);

    boolean isGetterVisible(C29141fw r1);

    boolean isIsGetterVisible(C29141fw r1);

    boolean isSetterVisible(C29141fw r1);

    C10160je with(JsonAutoDetect jsonAutoDetect);

    C10160je withCreatorVisibility(C10180jg r1);

    C10160je withFieldVisibility(C10180jg r1);

    C10160je withGetterVisibility(C10180jg r1);

    C10160je withIsGetterVisibility(C10180jg r1);

    C10160je withSetterVisibility(C10180jg r1);

    C10160je withVisibility(AnonymousClass0o9 r1, C10180jg r2);
}
