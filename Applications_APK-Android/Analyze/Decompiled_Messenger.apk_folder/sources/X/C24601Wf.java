package X;

/* renamed from: X.1Wf  reason: invalid class name and case insensitive filesystem */
public final class C24601Wf {
    public final int A00;
    public final int A01;
    public final Class A02;

    public boolean equals(Object obj) {
        if (!(obj instanceof C24601Wf)) {
            return false;
        }
        C24601Wf r4 = (C24601Wf) obj;
        if (this.A02 == r4.A02 && this.A01 == r4.A01 && this.A00 == r4.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((this.A02.hashCode() ^ 1000003) * 1000003) ^ this.A01) * 1000003) ^ this.A00;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.A02);
        sb.append(", type=");
        int i = this.A01;
        boolean z = true;
        if (i == 1) {
            str = "required";
        } else if (i == 0) {
            str = "optional";
        } else {
            str = "set";
        }
        sb.append(str);
        sb.append(", direct=");
        if (this.A00 != 0) {
            z = false;
        }
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    public C24601Wf(Class cls, int i, int i2) {
        AnonymousClass09E.A02(cls, "Null dependency anInterface.");
        this.A02 = cls;
        this.A01 = i;
        this.A00 = i2;
    }
}
