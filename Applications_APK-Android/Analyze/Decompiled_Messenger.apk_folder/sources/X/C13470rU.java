package X;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;

/* renamed from: X.0rU  reason: invalid class name and case insensitive filesystem */
public class C13470rU extends Fragment implements C12950qG {
    public static final String __redex_internal_original_name = "androidx.fragment.app.CustomFragment";

    public boolean A1b(Menu menu, MenuInflater menuInflater) {
        if (!A2I(menu, menuInflater)) {
            return super.A1b(menu, menuInflater);
        }
        return false;
    }

    public void A1y() {
        if (this instanceof C13460rT) {
            C005505z.A05("%s.onDestroyView", C05260Yg.A00(((C13460rT) this).getClass()), 1958964608);
        }
    }

    public void A1z() {
        if (this instanceof C13460rT) {
            C13460rT r3 = (C13460rT) this;
            ((AnonymousClass09P) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Amr, r3.A01)).removeCustomData(AnonymousClass08S.A09("FRAGMENT_", r3.hashCode()));
            C005505z.A05("%s.onPause", C05260Yg.A00(r3.getClass()), -1045401406);
        }
    }

    public void A20() {
        if (this instanceof C13460rT) {
            C005505z.A05("%s.onStop", C05260Yg.A00(((C13460rT) this).getClass()), 1870958454);
        }
    }

    public void A22() {
    }

    public void A23() {
    }

    public void A24() {
    }

    public void A25() {
    }

    public void A26() {
    }

    public void A27() {
    }

    public void A28() {
    }

    public void A29() {
    }

    public void A2A() {
    }

    public void A2B(Bundle bundle) {
        if (this instanceof C13460rT) {
            C13460rT r3 = (C13460rT) this;
            C005505z.A05("%s.onActivityCreated", C05260Yg.A00(r3.getClass()), -1456222182);
            C21081Ey r4 = r3.A00;
            synchronized (r4) {
                for (AnonymousClass165 r2 : r4.A01) {
                    try {
                        C005505z.A03(r2.getClass().getSimpleName(), -670199265);
                        r2.BPa(r4.A00, bundle);
                        C005505z.A00(-831791865);
                    } catch (Throwable th) {
                        C005505z.A00(1705507990);
                        throw th;
                    }
                }
            }
        }
    }

    public void A2C(Bundle bundle) {
    }

    public void A2D(Bundle bundle) {
    }

    public void A2E(Bundle bundle) {
    }

    public void A2F(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
    }

    public void A2G(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, View view) {
    }

    public void A2H(boolean z, boolean z2) {
    }

    public boolean A2I(Menu menu, MenuInflater menuInflater) {
        boolean z = this instanceof C13460rT;
        return false;
    }

    public LayoutInflater A13(Bundle bundle) {
        C13020qR r0 = this.A0N;
        if (r0 != null) {
            LayoutInflater cloneInContext = r0.A02().cloneInContext(this.A0N.A01);
            A17();
            AnonymousClass14W.A00(cloneInContext, this.A0O.A0O);
            return cloneInContext;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }

    public void A1T(boolean z) {
        if (this.A0a != z && this.A0e) {
            super.A1T(z);
        }
    }

    public final void A1W(boolean z) {
        C13060qW r0;
        boolean z2 = this.A0i;
        if (!z2 && z && this.A0F < 3 && (r0 = this.A0P) != null) {
            r0.A0o(this);
        }
        super.A1W(z);
        A2H(z, z2);
    }

    public void A21() {
        C13020qR r1 = this.A0N;
        if (r1 != null && this.A0a && A1X() && !this.A0b && this.A0e) {
            r1.A04();
        }
    }

    public void A1C() {
        try {
            A28();
            super.A1C();
        } finally {
            A22();
        }
    }

    public void A1D() {
        try {
            A1y();
            super.A1D();
        } finally {
            A23();
        }
    }

    public void A1E() {
        try {
            A1z();
            super.A1E();
        } finally {
            A24();
        }
    }

    public void A1F() {
        try {
            A29();
            super.A1F();
        } finally {
            A25();
        }
    }

    public void A1G() {
        try {
            A2A();
            super.A1G();
        } finally {
            A26();
        }
    }

    public void A1H() {
        try {
            A20();
            super.A1H();
        } finally {
            A27();
        }
    }

    public void A1M(Bundle bundle) {
        try {
            A2B(bundle);
            super.A1M(bundle);
        } finally {
            A2C(bundle);
        }
    }

    public void A1N(Bundle bundle) {
        try {
            A2E(bundle);
            super.A1N(bundle);
        } finally {
            A2D(bundle);
        }
    }

    public void A1Q(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        try {
            A2F(layoutInflater, viewGroup, bundle);
            super.A1Q(layoutInflater, viewGroup, bundle);
        } finally {
            A2G(layoutInflater, viewGroup, bundle, this.A0I);
        }
    }
}
