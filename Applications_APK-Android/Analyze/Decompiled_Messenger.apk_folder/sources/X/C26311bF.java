package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.1bF  reason: invalid class name and case insensitive filesystem */
public final class C26311bF {
    public int A00 = 0;
    public int A01;
    public int A02 = 0;
    public int A03 = 0;
    public int A04;
    public int A05 = 0;
    public int A06 = 0;
    public boolean A07 = false;
    public boolean A08 = false;
    public boolean A09 = false;
    public int[] A0A = {-1, -1};
    public int[] A0B = {-1, -1};
    public int[] A0C = {-1, -1};
    public int[] A0D = {-1, -1};

    public boolean A00() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (this.A02 <= 0) {
            return false;
        }
        if (this.A07) {
            int[] iArr = this.A0C;
            boolean z = false;
            if (iArr != null && (i5 = iArr[0]) > 0 && (i6 = iArr[1]) > 0 && i5 <= i6) {
                z = true;
            }
            if (!z) {
                return false;
            }
            int[] iArr2 = this.A0A;
            boolean z2 = false;
            if (iArr2 != null && (i3 = iArr2[0]) > 0 && (i4 = iArr2[1]) > 0 && i3 <= i4) {
                z2 = true;
            }
            if (z2) {
                return true;
            }
            return false;
        }
        int[] iArr3 = this.A0B;
        if (iArr3 == null || (i = iArr3[0]) <= 0 || (i2 = iArr3[1]) <= 0 || i > i2) {
            return false;
        }
        return true;
    }

    public String toString() {
        if (!A00()) {
            return TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1G);
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("cores", this.A02);
            jSONObject.put("is_biglittle", this.A07);
            if (this.A07) {
                jSONObject.put("little_freq_min", this.A0C[0]);
                jSONObject.put("little_freq_max", this.A0C[1]);
                jSONObject.put("big_freq_min", this.A0A[0]);
                jSONObject.put("big_freq_max", this.A0A[1]);
                jSONObject.put("little_cores", this.A03);
                jSONObject.put("big_cores", this.A00);
                int i = this.A05;
                if (i != 0) {
                    jSONObject.put("mid_cores", i);
                }
                jSONObject.put("little_index", this.A04);
                jSONObject.put("big_index", this.A01);
            } else {
                jSONObject.put("freq_min", this.A0B[0]);
                jSONObject.put("freq_max", this.A0B[1]);
            }
            jSONObject.put("prebuild", this.A09);
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }
}
