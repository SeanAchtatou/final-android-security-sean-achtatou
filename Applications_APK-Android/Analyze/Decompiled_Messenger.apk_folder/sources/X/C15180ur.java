package X;

import android.view.animation.Interpolator;

/* renamed from: X.0ur  reason: invalid class name and case insensitive filesystem */
public abstract class C15180ur implements Interpolator {
    private final float A00;
    private final float[] A01;

    public float getInterpolation(float f) {
        float f2 = 1.0f;
        if (f < 1.0f) {
            f2 = 0.0f;
            if (f > 0.0f) {
                float[] fArr = this.A01;
                int length = fArr.length;
                int min = Math.min((int) (((float) (length - 1)) * f), length - 2);
                float f3 = this.A00;
                float f4 = fArr[min];
                return f4 + (((f - (((float) min) * f3)) / f3) * (fArr[min + 1] - f4));
            }
        }
        return f2;
    }

    public C15180ur(float[] fArr) {
        this.A01 = fArr;
        this.A00 = 1.0f / ((float) (fArr.length - 1));
    }
}
