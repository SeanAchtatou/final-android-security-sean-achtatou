package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1GJ  reason: invalid class name */
public final class AnonymousClass1GJ extends C17770zR {
    @Comparable(type = 13)
    public AnonymousClass1BQ A00;
    @Comparable(type = 13)
    public InboxMoreThreadsItem A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 3)
    public boolean A03;

    public AnonymousClass1GJ() {
        super("InboxMoreThreadsComponent");
    }
}
