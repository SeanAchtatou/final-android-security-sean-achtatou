package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1vK  reason: invalid class name */
public final class AnonymousClass1vK extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public DGF A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 5)
    public ImmutableList A03;

    public AnonymousClass1vK(Context context) {
        super("M4MessengerAvailabilityMigrationConsentLayout");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }
}
