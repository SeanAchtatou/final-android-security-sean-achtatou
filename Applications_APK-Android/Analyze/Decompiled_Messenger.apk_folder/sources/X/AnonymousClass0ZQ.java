package X;

import android.content.IntentFilter;
import android.os.Handler;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.0ZQ  reason: invalid class name */
public final class AnonymousClass0ZQ extends AnonymousClass0Wl {
    public final AnonymousClass0ZR A00;
    public final C05980ae A01;

    public String getSimpleName() {
        return "INeedInitForBroadcastReceiverRegister";
    }

    public static final AnonymousClass0ZQ A00(AnonymousClass1XY r1) {
        return new AnonymousClass0ZQ(r1);
    }

    private AnonymousClass0ZQ(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0ZR.A00(r2);
        this.A01 = new C05980ae(r2);
    }

    /* JADX INFO: finally extract failed */
    public void init() {
        int A03 = C000700l.A03(1742658241);
        C005505z.A03("INeedInitForBroadcastReceiverRegister-RegisterActionReceivers", 480857615);
        try {
            C05980ae r7 = this.A01;
            AnonymousClass0ZR r10 = this.A00;
            Handler handler = (Handler) AnonymousClass1XX.A03(AnonymousClass1Y3.AcC, r7.A00);
            AnonymousClass1ZJ r5 = (AnonymousClass1ZJ) AnonymousClass1XX.A03(AnonymousClass1Y3.AM2, r7.A00);
            C04450Us A012 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("com.facebook.common.connectionstatus.FbDataConnectionManager.DATA_CONNECTION_STATE_CHANGE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.analytics.ConnectionStatusLogger.ConnectionStatusLoggerReceiverRegistration)", -1246847748);
            A012.A02(new C05960ac(r7.A01, intentFilter, r5), intentFilter, null);
            C005505z.A00(457883682);
            C04450Us A013 = r10.A01(AnonymousClass07B.A01);
            IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addAction("android.intent.action.TIME_SET");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.analytics.timespent.TimeSpentEventReporter.TimeChangeReceiverRegistration)", -1232816527);
            A013.A02(new C05960ac(r7.A02, intentFilter2, r5), intentFilter2, null);
            C005505z.A00(-68119760);
            C04450Us A014 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter3 = new IntentFilter();
            intentFilter3.addAction("com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP");
            intentFilter3.addAction("com.facebook.common.appstate.AppStateManager.USER_LEFT_APP");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.analytics.timespent.TimeSpentEventReporter.UserActivityReceiverRegistration)", 794511151);
            A014.A02(new C05960ac(r7.A0D, intentFilter3, r5), intentFilter3, handler);
            C04450Us A015 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter4 = new IntentFilter();
            intentFilter4.addAction("com.facebook.businessintegrity.gdpr.triggers.USER_IN_CONSENTS_FLOW");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.businessintegrity.gdpr.triggers.ConsentsFlowBroadcaster.ConsentsFlowBroadcastReceiverRegistration)", -554190582);
            A015.A02(new C05960ac(r7.A0M, intentFilter4, r5), intentFilter4, null);
            C005505z.A00(1206661331);
            C04450Us A016 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter5 = new IntentFilter();
            r7.A0N.get();
            String[] strArr = {"com.facebook.http.protocol.CHECKPOINT_API_EXCEPTION", C06680bu.A0K};
            for (int i = 0; i < 2; i++) {
                intentFilter5.addAction(strArr[i]);
            }
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.checkpoint.CheckpointBroadcaster.CheckpointBroadcasterReceiverRegistration)", -205678029);
            A016.A02(new C05960ac(r7.A0N, intentFilter5, r5), intentFilter5, null);
            C005505z.A00(1701336384);
            C04450Us A017 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter6 = new IntentFilter();
            intentFilter6.addAction(C99084oO.$const$string(AnonymousClass1Y3.A15));
            intentFilter6.addAction(C99084oO.$const$string(AnonymousClass1Y3.A16));
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.checkpoint.listener.CheckpointActivityListener.CheckpointBroadcasterReceiverRegistration)", 1973563981);
            A017.A02(new C05960ac(r7.A0O, intentFilter6, r5), intentFilter6, null);
            C005505z.A00(-1681189557);
            C04450Us A018 = r10.A01(AnonymousClass07B.A00);
            IntentFilter intentFilter7 = new IntentFilter();
            String[] strArr2 = {((C06090ap) AnonymousClass1XX.A03(AnonymousClass1Y3.AhJ, ((C06690bv) r7.A0P.get()).A00)).A02("NEARBY_FRIENDS_SETTINGS_CHANGED_ACTION")};
            for (int i2 = 0; i2 < 1; i2++) {
                intentFilter7.addAction(strArr2[i2]);
            }
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.contacts.service.ContactChatContextRefresher.NearbyFriendsSettingsChangeReceiverRegistration)", -17195730);
            A018.A02(new C05960ac(r7.A0P, intentFilter7, r5), intentFilter7, null);
            C005505z.A00(-833962932);
            C04450Us A019 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter8 = new IntentFilter();
            intentFilter8.addAction("com.facebook.zero.ZERO_RATING_DISABLED_ON_WIFI");
            intentFilter8.addAction("com.facebook.zero.ZERO_RATING_STATE_UNREGISTERED_REASON");
            intentFilter8.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.dialtone.DialtoneController.LocalDialtoneControllerReceiverRegistration)", 1407218750);
            A019.A02(new C05960ac(r7.A0Q, intentFilter8, r5), intentFilter8, null);
            C005505z.A00(14092272);
            C04450Us A0110 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter9 = new IntentFilter();
            intentFilter9.addAction("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.fos.headersv2.fb4aorca.ZeroHeadersNetworkChangeBroadcastReceiver)", -744668657);
            A0110.A02(new C05960ac(r7.A0R, intentFilter9, r5), intentFilter9, handler);
            C04450Us A0111 = r10.A01(AnonymousClass07B.A0N);
            IntentFilter intentFilter10 = new IntentFilter();
            intentFilter10.addAction(AnonymousClass80H.$const$string(15));
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.languages.switchercommonex.LocaleChangeBroadcastReceiverRegistration)", 13336293);
            A0111.A02(new C05960ac(r7.A0S, intentFilter10, r5), intentFilter10, null);
            C005505z.A00(1559748069);
            C04450Us A0112 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter11 = new IntentFilter();
            intentFilter11.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.business.omnistore.internal.PlatformMenuOmnistoreManagerImpl.LoginCompleteBroadcastReceiver)", -939291443);
            A0112.A02(new C05960ac(r7.A03, intentFilter11, r5), intentFilter11, null);
            C005505z.A00(1091241321);
            C04450Us A0113 = r10.A01(AnonymousClass07B.A01);
            IntentFilter intentFilter12 = new IntentFilter();
            intentFilter12.addAction("android.intent.action.TIME_SET");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.clockskew.ClockSkewChecker.ClockSkewCheckerOnInitBroadcastReceiverRegistration)", -2064516477);
            A0113.A02(new C05960ac(r7.A04, intentFilter12, r5), intentFilter12, null);
            C005505z.A00(-422048629);
            C04450Us A0114 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter13 = new IntentFilter();
            intentFilter13.addAction(C99084oO.$const$string(47));
            intentFilter13.addAction(C99084oO.$const$string(143));
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.instagram.contactimport.InstagramContactImportBadgingController.InstagramContactImprtTriggerRegistration)", -1381411033);
            A0114.A02(new C05960ac(r7.A05, intentFilter13, r5), intentFilter13, handler);
            C04450Us A0115 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter14 = new IntentFilter();
            intentFilter14.addAction(C99084oO.$const$string(1));
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.instagram.fetch.EligibleInstagramAccountBackgroundFetcher.EligibleInstagramAccountFetcherReceiveRegistration)", 1334316148);
            A0115.A02(new C05960ac(r7.A06, intentFilter14, r5), intentFilter14, handler);
            C04450Us A0116 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter15 = new IntentFilter();
            intentFilter15.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.profilepicture.MessengerIGRegProfilePictureUploadBroadcastReceiverRegistration)", -466501627);
            A0116.A02(new C05960ac(r7.A07, intentFilter15, r5), intentFilter15, handler);
            C04450Us A0117 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter16 = new IntentFilter();
            r7.A08.get();
            String str = C06680bu.A0r;
            String[] strArr3 = {str};
            for (int i3 = 0; i3 < 1; i3++) {
                intentFilter16.addAction(strArr3[i3]);
            }
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.sms.SmsFallbackNumberNotificationHandler.ReceiverRegistration)", -381785798);
            A0117.A02(new C05960ac(r7.A08, intentFilter16, r5), intentFilter16, null);
            C005505z.A00(-1102031777);
            C04450Us A0118 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter17 = new IntentFilter();
            r7.A09.get();
            String[] strArr4 = {str};
            for (int i4 = 0; i4 < 1; i4++) {
                intentFilter17.addAction(strArr4[i4]);
            }
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.sms.business.SmsBusinessThreadManager.SmsContactsChangedReceiver)", -1879044434);
            A0118.A02(new C05960ac(r7.A09, intentFilter17, r5), intentFilter17, null);
            C005505z.A00(-1478354712);
            C04450Us A0119 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter18 = new IntentFilter();
            intentFilter18.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.tincan.external.EagerKeyGenerator.LoginCompleteReceiverRegistration)", 2057776821);
            A0119.A02(new C05960ac(r7.A0A, intentFilter18, r5), intentFilter18, null);
            C005505z.A00(-971881837);
            C04450Us A0120 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter19 = new IntentFilter();
            intentFilter19.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.tincan.messenger.AttachmentUploadRetryColdStartTrigger.LoginCompleteReceiverRegistration)", 1766736923);
            A0120.A02(new C05960ac(r7.A0B, intentFilter19, r5), intentFilter19, null);
            C005505z.A00(-264626173);
            C04450Us A0121 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter20 = new IntentFilter();
            intentFilter20.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            intentFilter20.addAction(AnonymousClass80H.$const$string(18));
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.tincan.messenger.TincanDeviceManager.OnInitBroadcastReceiverRegistration)", -679628804);
            A0121.A02(new C05960ac(r7.A0C, intentFilter20, r5), intentFilter20, null);
            C005505z.A00(1767081860);
            C04450Us A0122 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter21 = new IntentFilter();
            intentFilter21.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.tincan.messenger.TincanPreKeyManager.OnInitBroadcastReceiverRegistration)", -855025205);
            A0122.A02(new C05960ac(r7.A0E, intentFilter21, r5), intentFilter21, null);
            C005505z.A00(-404121000);
            C04450Us A0123 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter22 = new IntentFilter();
            intentFilter22.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.messaging.tincan.inbound.MessageReceiver.OnInitBroadcastReceiverRegistration)", -1256873960);
            A0123.A02(new C05960ac(r7.A0F, intentFilter22, r5), intentFilter22, null);
            C005505z.A00(1930825285);
            C04450Us A0124 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter23 = new IntentFilter();
            intentFilter23.addAction(TurboLoader.Locator.$const$string(2));
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.omnistore.module.synchronous.SynchronousOmnistoreBroadcastReceiver)", -1684317090);
            A0124.A02(new C05960ac(r7.A0G, intentFilter23, r5), intentFilter23, null);
            C005505z.A00(1301837927);
            C04450Us A0125 = r10.A01(AnonymousClass07B.A01);
            IntentFilter intentFilter24 = new IntentFilter();
            intentFilter24.addAction("com.facebook.zero.ACTION_FORCE_ZERO_HEADER_REFRESH");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.zero.header.ZeroHeaderRequestManager.CrossProcessZeroHeaderRequestManagerReceiverRegistration)", 29897437);
            A0125.A02(new C05960ac(r7.A0H, intentFilter24, r5), intentFilter24, null);
            C005505z.A00(32403546);
            C04450Us A0126 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter25 = new IntentFilter();
            intentFilter25.addAction("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.zero.header.ZeroHeaderRequestManager.LocalZeroHeaderRequestManagerReceiverRegistration)", 684260883);
            A0126.A02(new C05960ac(r7.A0I, intentFilter25, r5), intentFilter25, null);
            C005505z.A00(-1546039547);
            C04450Us A0127 = r10.A01(AnonymousClass07B.A01);
            IntentFilter intentFilter26 = new IntentFilter();
            intentFilter26.addAction("com.facebook.zero.ZERO_RATING_CLEAR_SETTINGS");
            intentFilter26.addAction(AnonymousClass80H.$const$string(57));
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.zero.CrossProcessZeroTokenManagerReceiverRegistration)", -1335204316);
            A0127.A02(new C05960ac(r7.A0J, intentFilter26, r5), intentFilter26, null);
            C005505z.A00(68646480);
            C04450Us A0128 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter27 = new IntentFilter();
            intentFilter27.addAction("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED");
            intentFilter27.addAction("com.facebook.zero.ZERO_HEADER_REFRESH_COMPLETED");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.zero.LocalZeroTokenManagerReceiverRegistration)", -314694760);
            A0128.A02(new C05960ac(r7.A0K, intentFilter27, r5), intentFilter27, handler);
            C04450Us A0129 = r10.A01(AnonymousClass07B.A0C);
            IntentFilter intentFilter28 = new IntentFilter();
            intentFilter28.addAction("com.facebook.zero.ZERO_HEADER_REFRESH_COMPLETED");
            intentFilter28.addAction("com.facebook.zero.ACTION_ZERO_INTERSTITIAL_REFRESH");
            intentFilter28.addAction("com.facebook.growth.constants.nux_completed");
            intentFilter28.addAction("com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE");
            C005505z.A03("BroadcastReceiverRegistryEntry registerReceiver(com.facebook.zero.service.ZeroInterstitialEligibilityManager.LocalZeroInterstitialEligibilityManagerReceiverRegistration)", -553927106);
            A0129.A02(new C05960ac(r7.A0L, intentFilter28, r5), intentFilter28, handler);
            C005505z.A00(63689009);
            C000700l.A09(-2085399553, A03);
        } catch (Throwable th) {
            C005505z.A00(-606725322);
            throw th;
        }
    }
}
