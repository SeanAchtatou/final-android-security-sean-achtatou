package X;

/* renamed from: X.0Gk  reason: invalid class name and case insensitive filesystem */
public final class C02790Gk implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        AnonymousClass0FP r62 = (AnonymousClass0FP) r6;
        long j = r62.cameraOpenTimeMs;
        if (j != 0) {
            r7.AMV("camera_open_time_ms", j);
        }
        long j2 = r62.cameraPreviewTimeMs;
        if (j2 != 0) {
            r7.AMV("camera_preview_time_ms", j2);
        }
    }
}
