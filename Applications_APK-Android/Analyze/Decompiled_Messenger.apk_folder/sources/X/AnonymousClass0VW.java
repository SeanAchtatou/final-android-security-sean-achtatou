package X;

/* renamed from: X.0VW  reason: invalid class name */
public abstract class AnonymousClass0VW {
    private static volatile int A00;

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001d, code lost:
        if (X.AnonymousClass0VW.A00 == 1) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00() {
        /*
            int r0 = X.AnonymousClass0VW.A00
            r3 = 0
            r2 = 1
            if (r0 == 0) goto L_0x000c
            int r0 = X.AnonymousClass0VW.A00
            if (r0 != r2) goto L_0x000b
            r3 = 1
        L_0x000b:
            return r3
        L_0x000c:
            java.lang.Class<X.0VW> r1 = X.AnonymousClass0VW.class
            monitor-enter(r1)
            int r0 = X.AnonymousClass0VW.A00     // Catch:{ all -> 0x0022 }
            if (r0 == 0) goto L_0x0018
            int r0 = X.AnonymousClass0VW.A00     // Catch:{ all -> 0x0022 }
            if (r0 != r2) goto L_0x0020
            goto L_0x001f
        L_0x0018:
            r0 = -1
            X.AnonymousClass0VW.A00 = r0     // Catch:{ all -> 0x0022 }
            int r0 = X.AnonymousClass0VW.A00     // Catch:{ all -> 0x0022 }
            if (r0 != r2) goto L_0x0020
        L_0x001f:
            r3 = 1
        L_0x0020:
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            return r3
        L_0x0022:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0022 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0VW.A00():boolean");
    }

    private AnonymousClass0VW() {
    }
}
