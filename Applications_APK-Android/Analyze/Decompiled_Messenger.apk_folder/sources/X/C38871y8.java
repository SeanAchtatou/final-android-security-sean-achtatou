package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.1y8  reason: invalid class name and case insensitive filesystem */
public final class C38871y8 extends AnonymousClass0W1 {
    public C38871y8() {
        super("messaging_emoji_schema", 1, ImmutableList.of(new B33()));
    }

    public static final C38871y8 A00() {
        return new C38871y8();
    }
}
