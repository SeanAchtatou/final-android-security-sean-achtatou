package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* renamed from: X.1Rn  reason: invalid class name and case insensitive filesystem */
public final class C23931Rn implements Executor {
    public void execute(Runnable runnable) {
        AnonymousClass00S.A04(new Handler(Looper.getMainLooper()), runnable, -663043565);
    }
}
