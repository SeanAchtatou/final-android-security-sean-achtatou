package X;

import com.google.common.base.Throwables;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/* renamed from: X.0UZ  reason: invalid class name */
public final class AnonymousClass0UZ extends Handler {
    public void close() {
    }

    public void flush() {
    }

    public void publish(LogRecord logRecord) {
        String message;
        if (logRecord.getLevel() == Level.SEVERE && logRecord.getThrown() != null && (message = logRecord.getMessage()) != null && message.startsWith("RuntimeException while executing runnable ")) {
            throw Throwables.propagate(logRecord.getThrown());
        }
    }
}
