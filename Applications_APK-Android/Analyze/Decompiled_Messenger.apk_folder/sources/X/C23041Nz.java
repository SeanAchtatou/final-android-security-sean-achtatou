package X;

import java.nio.ByteBuffer;

/* renamed from: X.1Nz  reason: invalid class name and case insensitive filesystem */
public interface C23041Nz {
    C23031Ny decode(long j, int i, C23541Px r4);

    C23031Ny decode(ByteBuffer byteBuffer, C23541Px r2);
}
