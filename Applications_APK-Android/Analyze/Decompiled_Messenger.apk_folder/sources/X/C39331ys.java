package X;

import com.facebook.orca.threadview.ThreadViewMessagesFragment;

/* renamed from: X.1ys  reason: invalid class name and case insensitive filesystem */
public final class C39331ys implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.orca.threadview.ThreadViewMessagesFragment$11";
    public final /* synthetic */ ThreadViewMessagesFragment A00;

    public C39331ys(ThreadViewMessagesFragment threadViewMessagesFragment) {
        this.A00 = threadViewMessagesFragment;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
        if (r3 > -1) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r5.A00
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r0 = r0.A0p
            java.lang.String r4 = "search_in_conversation"
            if (r0 == 0) goto L_0x0025
            java.lang.String r0 = r0.A01
            boolean r0 = X.C06850cB.A0B(r0)
            if (r0 != 0) goto L_0x0025
            com.facebook.orca.threadview.ThreadViewMessagesFragment r1 = r5.A00
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r0 = r1.A0p
            java.lang.String r0 = r0.A01
            int r3 = com.facebook.orca.threadview.ThreadViewMessagesFragment.A05(r1, r0)
            com.facebook.orca.threadview.ThreadViewMessagesFragment r2 = r5.A00
            com.facebook.messaging.threadview.params.MessageDeepLinkInfo r1 = r2.A0p
            r0 = 1
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0f(r2, r1, r0)
            r0 = -1
            if (r3 <= r0) goto L_0x002a
        L_0x0025:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r5.A00
            com.facebook.orca.threadview.ThreadViewMessagesFragment.A0i(r0, r4)
        L_0x002a:
            com.facebook.orca.threadview.ThreadViewMessagesFragment r0 = r5.A00
            X.3Ez r0 = r0.A0e
            r0.AYd()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39331ys.run():void");
    }
}
