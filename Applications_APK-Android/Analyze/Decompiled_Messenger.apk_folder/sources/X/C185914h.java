package X;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.fragment.app.Fragment;
import com.facebook.common.util.TriState;
import com.facebook.content.SecureContextHelper;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.sms.defaultapp.ComposeSmsActivity;
import com.facebook.messaging.sms.defaultapp.PrivilegedSmsReceiver;
import com.facebook.messaging.sms.defaultapp.SmsDefaultAppDialogActivity;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.14h  reason: invalid class name and case insensitive filesystem */
public final class C185914h {
    private static TriState A07 = TriState.UNSET;
    private static volatile C185914h A08;
    public Context A00;
    public C04460Ut A01;
    public AnonymousClass0UN A02;
    public C10960l9 A03;
    private ExecutorService A04;
    public final Runnable A05 = new AnonymousClass14i(this);
    public final List A06 = new ArrayList(1);

    public static void A02(C185914h r3, Intent intent, Context context) {
        try {
            ((SecureContextHelper) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A7S, r3.A02)).CHH(intent, context);
        } catch (ActivityNotFoundException e) {
            C010708t.A0T("SmsDefaultAppManager", e, "Unable to start system setting to turn off SMS integration");
        }
    }

    public static boolean A03(Context context) {
        try {
            ComponentName componentName = new ComponentName(context, ComposeSmsActivity.class);
            ComponentName componentName2 = new ComponentName(context, PrivilegedSmsReceiver.class);
            PackageManager packageManager = context.getPackageManager();
            return packageManager.getComponentEnabledSetting(componentName) == 1 && packageManager.getComponentEnabledSetting(componentName2) == 1;
        } catch (Exception unused) {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (((X.AnonymousClass1Y6) X.AnonymousClass1XX.A02(3, X.AnonymousClass1Y3.APr, r4.A02)).BHM() != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.C138436dA r5, java.lang.Runnable r6) {
        /*
            r4 = this;
            r3 = 0
            if (r6 == 0) goto L_0x0015
            r2 = 3
            int r1 = X.AnonymousClass1Y3.APr
            X.0UN r0 = r4.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Y6 r0 = (X.AnonymousClass1Y6) r0
            boolean r1 = r0.BHM()
            r0 = 0
            if (r1 == 0) goto L_0x0016
        L_0x0015:
            r0 = 1
        L_0x0016:
            com.google.common.base.Preconditions.checkState(r0)
            int r1 = X.AnonymousClass1Y3.AFw
            X.0UN r0 = r4.A02
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1Ha r3 = (X.C21451Ha) r3
            java.lang.String r0 = "sms_takeover_ro_action"
            X.0nb r2 = X.C21451Ha.A01(r0)
            java.lang.String r1 = r5.toString()
            java.lang.String r0 = "call_context"
            r2.A0D(r0, r1)
            X.C21451Ha.A05(r3, r2)
            if (r6 == 0) goto L_0x003c
            java.util.List r0 = r4.A06
            r0.add(r6)
        L_0x003c:
            r0 = 0
            r4.A06(r5, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C185914h.A07(X.6dA, java.lang.Runnable):void");
    }

    public void A08(Object obj, Context context, boolean z) {
        try {
            ComponentName componentName = new ComponentName(context, ComposeSmsActivity.class);
            ComponentName componentName2 = new ComponentName(context, PrivilegedSmsReceiver.class);
            PackageManager packageManager = context.getPackageManager();
            Integer A052 = this.A03.A05();
            if (A03(context) != z) {
                int i = 2;
                if (z) {
                    i = 1;
                }
                packageManager.setComponentEnabledSetting(componentName, i, 1);
                this.A03.A07();
            }
            if (packageManager.getComponentEnabledSetting(componentName2) != 1) {
                packageManager.setComponentEnabledSetting(componentName2, 1, 1);
            }
            Integer A053 = this.A03.A05();
            if (A052 != A053) {
                ((C21451Ha) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AFw, this.A02)).A0E(obj, A052, A053);
            }
        } catch (Exception e) {
            C010708t.A0T("SmsDefaultAppManager", e, "Failed to enable SMS components");
        }
    }

    private AnonymousClass3At A00(Context context, C138436dA r11, Intent intent, DialogInterface.OnCancelListener onCancelListener) {
        Integer num;
        Integer num2;
        C138436dA r5 = r11;
        if (r11 == C138436dA.A0Q) {
            num = 2131833163;
            num2 = 17039370;
        } else {
            num = 2131833162;
            num2 = 2131833161;
        }
        C13500rX r2 = new C13500rX(context);
        r2.A08(num.intValue());
        r2.A02(num2.intValue(), new AnonymousClass53l(this, intent, context));
        r2.A01.A05 = new C1056553m(this, r5, intent, context, onCancelListener);
        return r2.A06();
    }

    public static final C185914h A01(AnonymousClass1XY r4) {
        if (A08 == null) {
            synchronized (C185914h.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r4);
                if (A002 != null) {
                    try {
                        A08 = new C185914h(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public void A04() {
        AnonymousClass9OX.A01 = ((C10970lA) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AGC, this.A02)).A00.AbO(AnonymousClass1Y3.A6m, false);
    }

    public void A05(Context context, C138436dA r5, DialogInterface.OnCancelListener onCancelListener) {
        Intent intent;
        TriState triState;
        if (Build.VERSION.SDK_INT >= 23) {
            intent = new Intent(AnonymousClass80H.$const$string(127));
            intent.addCategory(AnonymousClass24B.$const$string(54));
        } else {
            intent = new Intent("android.settings.WIRELESS_SETTINGS");
        }
        if (A07 == TriState.UNSET) {
            if (intent.resolveActivity(context.getPackageManager()) == null) {
                triState = TriState.NO;
            } else {
                triState = TriState.YES;
            }
            A07 = triState;
        }
        if (A07 == TriState.NO) {
            AnonymousClass07A.A04(this.A04, new AnonymousClass9QW(this, context, r5), -1441898810);
        } else if (Build.VERSION.SDK_INT < 23 || r5 == C138436dA.A0Q) {
            A00(context, r5, intent, onCancelListener).show();
        } else {
            A02(this, intent, context);
        }
    }

    public void A06(C138436dA r5, Fragment fragment) {
        Intent intent = new Intent(this.A00, SmsDefaultAppDialogActivity.class);
        intent.setFlags(805306368);
        intent.putExtra(AnonymousClass80H.$const$string(43), r5);
        if (fragment == null) {
            ((SecureContextHelper) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A7S, this.A02)).startFacebookActivity(intent, this.A00);
        } else {
            ((SecureContextHelper) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A7S, this.A02)).CGt(intent, AnonymousClass1Y3.AJP, fragment);
        }
    }

    private C185914h(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(7, r3);
        this.A00 = AnonymousClass1YA.A00(r3);
        this.A03 = C10960l9.A00(r3);
        FbSharedPreferencesModule.A00(r3);
        this.A04 = AnonymousClass0UX.A0Z(r3);
        this.A01 = C04430Uq.A02(r3);
        AnonymousClass0WY.A02(r3);
    }

    public boolean A09(ThreadKey threadKey) {
        if (!ThreadKey.A0E(threadKey) || this.A03.A08()) {
            return false;
        }
        return true;
    }
}
