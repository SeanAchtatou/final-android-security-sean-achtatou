package X;

import android.content.Context;
import android.util.Log;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/* renamed from: X.0F3  reason: invalid class name */
public final class AnonymousClass0F3 {
    public static AnonymousClass0F3 A27;
    public static final Object A28 = new Object();
    public final double A00;
    public final double A01;
    public final double A02;
    public final double A03;
    public final double A04;
    public final double A05;
    public final double A06;
    public final double A07;
    public final double A08;
    public final double A09;
    public final float A0A;
    public final float A0B;
    public final float A0C;
    public final int A0D;
    public final int A0E;
    public final int A0F;
    public final int A0G;
    public final int A0H;
    public final int A0I;
    public final int A0J;
    public final int A0K;
    public final int A0L;
    public final int A0M;
    public final int A0N;
    public final int A0O;
    public final int A0P;
    public final int A0Q;
    public final int A0R;
    public final int A0S;
    public final int A0T;
    public final int A0U;
    public final int A0V;
    public final int A0W;
    public final int A0X;
    public final int A0Y;
    public final int A0Z;
    public final int A0a;
    public final int A0b;
    public final int A0c;
    public final long A0d;
    public final boolean A0e;
    public final boolean A0f;
    public final boolean A0g;
    public final boolean A0h;
    public final boolean A0i;
    public final boolean A0j;
    public final boolean A0k;
    public final boolean A0l;
    public final boolean A0m;
    public final boolean A0n;
    public final boolean A0o;
    public final boolean A0p;
    public final boolean A0q;
    public final boolean A0r;
    public final boolean A0s;
    public final boolean A0t;
    public final boolean A0u;
    public final boolean A0v;
    public final boolean A0w;
    public final boolean A0x;
    public final boolean A0y;
    public final boolean A0z;
    public final boolean A10;
    public final boolean A11;
    public final boolean A12;
    public final boolean A13;
    public final boolean A14;
    public final boolean A15;
    public final boolean A16;
    public final boolean A17;
    public final boolean A18;
    public final boolean A19;
    public final boolean A1A;
    public final boolean A1B;
    public final boolean A1C;
    public final boolean A1D;
    public final boolean A1E;
    public final boolean A1F;
    public final boolean A1G;
    public final boolean A1H;
    public final boolean A1I;
    public final boolean A1J;
    public final boolean A1K;
    public final boolean A1L;
    public final boolean A1M;
    public final boolean A1N;
    public final boolean A1O;
    public final boolean A1P;
    public final boolean A1Q;
    public final boolean A1R;
    public final boolean A1S;
    public final boolean A1T;
    public final boolean A1U;
    public final boolean A1V;
    public final boolean A1W;
    public final boolean A1X;
    public final boolean A1Y;
    public final boolean A1Z;
    public final boolean A1a;
    public final boolean A1b;
    public final boolean A1c;
    public final boolean A1d;
    public final boolean A1e;
    public final boolean A1f;
    public final boolean A1g;
    public final boolean A1h;
    public final boolean A1i;
    public final boolean A1j;
    public final boolean A1k;
    public final boolean A1l;
    public final boolean A1m;
    public final boolean A1n;
    public final boolean A1o;
    public final boolean A1p;
    public final boolean A1q;
    public final boolean A1r;
    public final boolean A1s;
    public final boolean A1t;
    public final boolean A1u;
    public final boolean A1v;
    public final boolean A1w;
    public final boolean A1x;
    public final boolean A1y;
    public final boolean A1z;
    public final boolean A20;
    public final boolean A21;
    public final boolean A22;
    public final boolean A23;
    public final boolean A24;
    public final boolean A25;
    public final boolean A26;

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:51:0x02a2=Splitter:B:51:0x02a2, B:66:0x02ca=Splitter:B:66:0x02ca} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass0F3 A02(android.content.Context r152) {
        /*
            X.0F3 r0 = X.AnonymousClass0F3.A27
            if (r0 != 0) goto L_0x02d6
            java.lang.Object r9 = X.AnonymousClass0F3.A28
            monitor-enter(r9)
            X.0F3 r0 = X.AnonymousClass0F3.A27     // Catch:{ all -> 0x02d3 }
            if (r0 != 0) goto L_0x0020
            r7 = r152
            java.io.File r0 = A03(r7)     // Catch:{ all -> 0x02d3 }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x02d3 }
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x0023
            X.0F3 r10 = new X.0F3     // Catch:{ all -> 0x02d3 }
            r10.<init>()     // Catch:{ all -> 0x02d3 }
        L_0x001e:
            X.AnonymousClass0F3.A27 = r10     // Catch:{ all -> 0x02d3 }
        L_0x0020:
            monitor-exit(r9)     // Catch:{ all -> 0x02d3 }
            goto L_0x02d6
        L_0x0023:
            java.lang.String r6 = "risky_startup_config"
            java.io.File r1 = r7.getFileStreamPath(r6)     // Catch:{ all -> 0x02ce }
            boolean r0 = r1.exists()     // Catch:{ all -> 0x02ce }
            if (r0 != 0) goto L_0x0036
            X.0F3 r10 = new X.0F3     // Catch:{ all -> 0x02ce }
            r10.<init>()     // Catch:{ all -> 0x02ce }
            goto L_0x02c1
        L_0x0036:
            r4 = 0
            r0 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ IOException | BufferUnderflowException -> 0x02a9 }
            r5.<init>(r1)     // Catch:{ IOException | BufferUnderflowException -> 0x02a9 }
            r2 = 1280(0x500, float:1.794E-42)
            byte[] r8 = new byte[r2]     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r4 = java.lang.Math.min(r2, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            r3 = 0
            r2 = 0
        L_0x0047:
            if (r3 >= r4) goto L_0x0053
            int r1 = r4 - r3
            int r2 = r5.read(r8, r3, r1)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            if (r2 < 0) goto L_0x0053
            int r3 = r3 + r2
            goto L_0x0047
        L_0x0053:
            r1 = -1
            if (r2 != r1) goto L_0x005b
            if (r3 != 0) goto L_0x005b
        L_0x0058:
            if (r1 > 0) goto L_0x0064
            goto L_0x005d
        L_0x005b:
            r1 = r3
            goto L_0x0058
        L_0x005d:
            X.0F3 r10 = new X.0F3     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            r10.<init>()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            goto L_0x02a2
        L_0x0064:
            java.nio.ByteBuffer r1 = java.nio.ByteBuffer.wrap(r8, r0, r1)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r11 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r12 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r13 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r14 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r15 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r16 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r17 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r18 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r19 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r20 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r21 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r22 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r23 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r24 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r25 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r26 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r27 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r28 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r29 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r30 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r31 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r32 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r33 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r34 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r35 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r36 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r37 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r38 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r39 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r40 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r41 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r42 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r43 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r44 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r45 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r46 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r47 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r48 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r49 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r50 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r51 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r52 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r53 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r54 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            r2 = 0
            double r55 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r57 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r59 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r61 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r63 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r65 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r67 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r69 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r71 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r72 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r73 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r74 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r75 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r76 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r77 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r78 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r79 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r80 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r81 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r82 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r83 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r84 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r85 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r86 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r87 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r88 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r89 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r90 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r91 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r93 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            r4 = 9
            int r94 = A01(r1, r4)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            r4 = 1
            int r95 = A01(r1, r4)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r96 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r97 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r98 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r99 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r100 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r101 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r102 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r103 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r104 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r105 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r106 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r107 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r108 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            double r109 = A00(r1, r2)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r111 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r112 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r113 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r114 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r115 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r2 = r1.hasRemaining()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            if (r2 != 0) goto L_0x01f2
            r116 = 0
            goto L_0x01f6
        L_0x01f2:
            long r116 = r1.getLong()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
        L_0x01f6:
            boolean r118 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r2 = r1.hasRemaining()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            if (r2 != 0) goto L_0x0203
            r119 = 0
            goto L_0x0207
        L_0x0203:
            float r119 = r1.getFloat()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
        L_0x0207:
            boolean r120 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r2 = r1.hasRemaining()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            if (r2 != 0) goto L_0x0214
            r121 = 0
            goto L_0x0218
        L_0x0214:
            float r121 = r1.getFloat()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
        L_0x0218:
            boolean r122 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r2 = r1.hasRemaining()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            if (r2 != 0) goto L_0x0225
            r123 = 0
            goto L_0x0229
        L_0x0225:
            float r123 = r1.getFloat()     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
        L_0x0229:
            boolean r124 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r125 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r126 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r127 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r128 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r129 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r130 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r131 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r132 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r133 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r134 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r135 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r136 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r137 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r138 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r139 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r140 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r141 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r142 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r143 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r144 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r145 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r146 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r147 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r148 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r149 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r150 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            boolean r151 = A07(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            int r152 = A01(r1, r0)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            X.0F3 r10 = new X.0F3     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
            r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r57, r59, r61, r63, r65, r67, r69, r71, r72, r73, r74, r75, r76, r77, r78, r79, r80, r81, r82, r83, r84, r85, r86, r87, r88, r89, r90, r91, r93, r94, r95, r96, r97, r98, r99, r100, r101, r102, r103, r104, r105, r106, r107, r108, r109, r111, r112, r113, r114, r115, r116, r118, r119, r120, r121, r122, r123, r124, r125, r126, r127, r128, r129, r130, r131, r132, r133, r134, r135, r136, r137, r138, r139, r140, r141, r142, r143, r144, r145, r146, r147, r148, r149, r150, r151, r152)     // Catch:{ IOException | BufferUnderflowException -> 0x02a6, all -> 0x02c6 }
        L_0x02a2:
            A06(r5)     // Catch:{ all -> 0x02ce }
            goto L_0x02c1
        L_0x02a6:
            r3 = move-exception
            r4 = r5
            goto L_0x02aa
        L_0x02a9:
            r3 = move-exception
        L_0x02aa:
            java.lang.String r2 = "RiskyStartupConfig"
            java.lang.String r1 = "Cannot read file %s"
            java.lang.Object[] r0 = new java.lang.Object[]{r6}     // Catch:{ all -> 0x02c8 }
            java.lang.String r0 = java.lang.String.format(r1, r0)     // Catch:{ all -> 0x02c8 }
            android.util.Log.e(r2, r0, r3)     // Catch:{ all -> 0x02c8 }
            X.0F3 r10 = new X.0F3     // Catch:{ all -> 0x02c8 }
            r10.<init>()     // Catch:{ all -> 0x02c8 }
            A06(r4)     // Catch:{ all -> 0x02ce }
        L_0x02c1:
            A05(r7)     // Catch:{ all -> 0x02d3 }
            goto L_0x001e
        L_0x02c6:
            r0 = move-exception
            goto L_0x02ca
        L_0x02c8:
            r0 = move-exception
            r5 = r4
        L_0x02ca:
            A06(r5)     // Catch:{ all -> 0x02ce }
            throw r0     // Catch:{ all -> 0x02ce }
        L_0x02ce:
            r0 = move-exception
            A05(r7)     // Catch:{ all -> 0x02d3 }
            throw r0     // Catch:{ all -> 0x02d3 }
        L_0x02d3:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x02d3 }
            throw r0
        L_0x02d6:
            X.0F3 r0 = X.AnonymousClass0F3.A27
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0F3.A02(android.content.Context):X.0F3");
    }

    public static void A04(Context context) {
        File dir = context.getDir("RiskyStartupConfig", 4);
        if (dir.exists()) {
            for (File file : dir.listFiles()) {
                if (!file.delete()) {
                    Log.w("RiskyStartupConfig", String.format("Could not delete config read state: %s", file));
                }
            }
            if (!dir.delete()) {
                Log.w("RiskyStartupConfig", "Could not delete all risky start up config state");
            }
        }
    }

    private static void A05(Context context) {
        File A032 = A03(context);
        if (!A032.exists()) {
            try {
                if (!A032.createNewFile()) {
                    Log.w("RiskyStartupConfig", String.format("Could not create has read marker file for %s", A032.getName()));
                }
            } catch (IOException e) {
                Log.e("RiskyStartupConfig", String.format("Error creating has read marker file for %s", A032.getName()), e);
            }
        }
    }

    public static void A06(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.w("RiskyStartupConfig", String.format("error closing %s", closeable), e);
            }
        }
    }

    public int hashCode() {
        return (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((this.A0y ? 1 : 0) + true) * 31) + (this.A0q ? 1 : 0)) * 31) + (this.A0r ? 1 : 0)) * 31) + (this.A0p ? 1 : 0)) * 31) + (this.A0m ? 1 : 0)) * 31) + this.A0E) * 31) + (this.A0n ? 1 : 0)) * 31) + this.A0F) * 31) + (this.A0o ? 1 : 0)) * 31) + this.A0G) * 31) + (this.A26 ? 1 : 0)) * 31) + this.A0c) * 31) + (this.A1f ? 1 : 0)) * 31) + this.A0M) * 31) + (this.A1m ? 1 : 0)) * 31) + this.A0Q) * 31) + (this.A10 ? 1 : 0)) * 31) + this.A0H) * 31) + (this.A0l ? 1 : 0)) * 31) + this.A0D) * 31) + (this.A23 ? 1 : 0)) * 31) + (this.A22 ? 1 : 0)) * 31) + (this.A0w ? 1 : 0)) * 31) + (this.A0v ? 1 : 0)) * 31) + (this.A20 ? 1 : 0)) * 31) + (this.A1q ? 1 : 0)) * 31) + (this.A1w ? 1 : 0)) * 31) + this.A0Z) * 31) + (this.A1z ? 1 : 0)) * 31) + this.A0a) * 31) + (this.A1r ? 1 : 0)) * 31) + this.A0U) * 31) + (this.A1t ? 1 : 0)) * 31) + this.A0W) * 31) + (this.A1v ? 1 : 0)) * 31) + this.A0Y) * 31) + (this.A1s ? 1 : 0)) * 31) + this.A0V) * 31) + (this.A1y ? 1 : 0)) * 31) + (this.A1x ? 1 : 0)) * 31) + (this.A1p ? 1 : 0)) * 31) + (this.A1Q ? 1 : 0)) * 31) + Double.valueOf(this.A01).hashCode()) * 31) + Double.valueOf(this.A02).hashCode()) * 31) + Double.valueOf(this.A03).hashCode()) * 31) + Double.valueOf(this.A09).hashCode()) * 31) + Double.valueOf(this.A07).hashCode()) * 31) + Double.valueOf(this.A08).hashCode()) * 31) + Double.valueOf(this.A04).hashCode()) * 31) + Double.valueOf(this.A00).hashCode()) * 31) + (this.A0z ? 1 : 0)) * 31) + (this.A1L ? 1 : 0)) * 31) + (this.A1P ? 1 : 0)) * 31) + (this.A24 ? 1 : 0)) * 31) + (this.A14 ? 1 : 0)) * 31) + (this.A1J ? 1 : 0)) * 31) + (this.A1T ? 1 : 0)) * 31) + (this.A1G ? 1 : 0)) * 31) + (this.A1H ? 1 : 0)) * 31) + (this.A18 ? 1 : 0)) * 31) + (this.A1F ? 1 : 0)) * 31) + (this.A1R ? 1 : 0)) * 31) + (this.A12 ? 1 : 0)) * 31) + (this.A1M ? 1 : 0)) * 31) + (this.A1V ? 1 : 0)) * 31) + (this.A1I ? 1 : 0)) * 31) + (this.A1U ? 1 : 0)) * 31) + (this.A13 ? 1 : 0)) * 31) + (this.A1O ? 1 : 0)) * 31) + (this.A1W ? 1 : 0)) * 31) + Double.valueOf(this.A05).hashCode()) * 31) + (this.A1N ? 1 : 0)) * 31) + this.A0I) * 31) + this.A0J) * 31) + (this.A1E ? 1 : 0)) * 31) + (this.A16 ? 1 : 0)) * 31) + (this.A15 ? 1 : 0)) * 31) + (this.A19 ? 1 : 0)) * 31) + (this.A1C ? 1 : 0)) * 31) + (this.A1K ? 1 : 0)) * 31) + (this.A1B ? 1 : 0)) * 31) + (this.A1A ? 1 : 0)) * 31) + (this.A11 ? 1 : 0)) * 31) + (this.A1S ? 1 : 0)) * 31) + (this.A1D ? 1 : 0)) * 31) + (this.A17 ? 1 : 0)) * 31) + (this.A25 ? 1 : 0)) * 31) + Double.valueOf(this.A06).hashCode()) * 31) + (this.A1a ? 1 : 0)) * 31) + this.A0L) * 31) + (this.A1Y ? 1 : 0)) * 31) + this.A0K) * 31) + (this.A1Z ? 1 : 0)) * 31) + Long.valueOf(this.A0d).hashCode()) * 31) + (this.A1X ? 1 : 0)) * 31) + Float.valueOf(this.A0A).hashCode()) * 31) + (this.A1e ? 1 : 0)) * 31) + Float.valueOf(this.A0C).hashCode()) * 31) + (this.A1d ? 1 : 0)) * 31) + Float.valueOf(this.A0B).hashCode()) * 31) + (this.A1c ? 1 : 0)) * 31) + (this.A1b ? 1 : 0)) * 31) + (this.A1l ? 1 : 0)) * 31) + (this.A1j ? 1 : 0)) * 31) + (this.A1k ? 1 : 0)) * 31) + this.A0P) * 31) + (this.A1i ? 1 : 0)) * 31) + this.A0O) * 31) + (this.A1h ? 1 : 0)) * 31) + this.A0N) * 31) + (this.A0j ? 1 : 0)) * 31) + (this.A1g ? 1 : 0)) * 31) + (this.A0s ? 1 : 0)) * 31) + this.A0T) * 31) + (this.A0t ? 1 : 0)) * 31) + this.A0b) * 31) + (this.A21 ? 1 : 0)) * 31) + (this.A0g ? 1 : 0)) * 31) + (this.A0k ? 1 : 0)) * 31) + (this.A0x ? 1 : 0)) * 31) + (this.A0f ? 1 : 0)) * 31) + (this.A0e ? 1 : 0)) * 31) + (this.A0u ? 1 : 0)) * 31) + (this.A0i ? 1 : 0)) * 31) + (this.A0h ? 1 : 0)) * 31) + (this.A1n ? 1 : 0)) * 31) + this.A0R) * 31) + (this.A1o ? 1 : 0)) * 31) + this.A0S;
    }

    public String toString() {
        return "[" + getClass().getSimpleName() + " enableJit: " + this.A0y + " disableJit: " + this.A0q + " disableProfiler: " + this.A0r + " createCustomJitOrPgoOptions: " + this.A0p + " code_cache_initial_capacity_enabled: " + this.A0m + " code_cache_initial_capacity: " + this.A0E + " code_cache_max_capacity_enabled: " + this.A0n + " code_cache_max_capacity: " + this.A0F + " compile_threshold_enabled: " + this.A0o + " compile_threshold: " + this.A0G + " warmup_threshold_enabled: " + this.A26 + " warmup_threshold: " + this.A0c + " osr_threshold_enabled: " + this.A1f + " osr_threshold: " + this.A0M + " priority_thread_weight_enabled: " + this.A1m + " priority_thread_weight: " + this.A0Q + " invoke_transition_weight_enabled: " + this.A10 + " invoke_transition_weight: " + this.A0H + " code_cache_capacity_enabled: " + this.A0l + " code_cache_capacity: " + this.A0D + " save_profiling_info_enabled: " + this.A23 + " save_profiling_info: " + this.A22 + " dump_info_on_shutdown_enabled: " + this.A0w + " dump_info_on_shutdown: " + this.A0v + " profiler_should_override: " + this.A20 + " profiler_enabled: " + this.A1q + " profiler_min_save_period_ms_enabled: " + this.A1w + " profiler_min_save_period_ms: " + this.A0Z + " profiler_save_resolved_classes_delay_ms_enabled: " + this.A1z + " profiler_save_resolved_classes_delay_ms: " + this.A0a + " profiler_hot_startup_method_samples_enabled: " + this.A1r + " profiler_hot_startup_method_samples: " + this.A0U + " profiler_min_methods_to_save_enabled: " + this.A1u + " profiler_min_methods_to_save: " + this.A0X + " profiler_min_classes_to_save_enabled: " + this.A1t + " profiler_min_classes_to_save: " + this.A0W + " profiler_min_notification_before_wake_enabled: " + this.A1v + " profiler_min_notification_before_wake: " + this.A0Y + " profiler_max_notification_before_wake_enabled: " + this.A1s + " profiler_max_notification_before_wake: " + this.A0V + " profiler_profile_boot_class_path_enabled: " + this.A1y + " profiler_profile_boot_class_path: " + this.A1x + " profiler_change_profile_path: " + this.A1p + " jitHackRunOnlyOnMainProcess: " + this.A1Q + " code_cache_initial_capacity_ratio: " + this.A01 + " code_cache_max_capacity_ratio: " + this.A02 + " compile_threshold_ratio: " + this.A03 + " warmup_threshold_ratio: " + this.A09 + " osr_threshold_ratio: " + this.A07 + " priority_thread_weight_ratio: " + this.A08 + " invoke_transition_weight_ratio: " + this.A04 + " code_cache_capacity_ratio: " + this.A00 + " enableJitMallocArenaPool: " + this.A0z + " jitBailOnRecommendedDevices: " + this.A1L + " jitFixupApplyMprotectFix: " + this.A1P + " stopJitForColdStart: " + this.A24 + " jitApplyArenaAllocRetryFix: " + this.A14 + " jitBackportJitWeighing: " + this.A1J + " jitModifyCurrent: " + this.A1T + " jitApplyMprotectJitCreationOverride: " + this.A1G + " jitApplyPerformMoveBugFix: " + this.A1H + " jitApplyCustomJitPriorities: " + this.A18 + " jitApplyMobileConfigInlining: " + this.A1F + " jitIgnoreMalformed: " + this.A1R + " jitAlwaysVerify: " + this.A12 + " jitEnableAfterDexesLoaded: " + this.A1M + " jitVerifyCanFindPrimaryDex: " + this.A1V + " jitApplyUseRemapMprotectPtSafeTL: " + this.A1I + " jitOverrideJitCompilerCompileMethod: " + this.A1U + " jitAlwaysVerifyOnJitCompile: " + this.A13 + " jitFailOnSoftVerificationFailure: " + this.A1O + " max_code_cache_initial_capacity_enabled: " + this.A1W + " max_code_cache_initial_capacity_mb: " + this.A05 + " jitEnableThreadPoolPriority: " + this.A1N + " jitThreadPoolPriority: " + this.A0I + " jitThreadPoolThreadCount: " + this.A0J + " jitApplyMmapRaceFix: " + this.A1E + " jitApplyArenaRetryFastHook: " + this.A16 + " jitApplyArenaMallocFallback: " + this.A15 + " jitApplyCustomQCCompilerFix: " + this.A19 + " jitApplyGenInvokeNoLineFix: " + this.A1C + " jitBailOnQcDevices: " + this.A1K + " jitApplyGcJitFix: " + this.A1B + " jitApplyFlushInstructionCacheFix: " + this.A1A + " jitAllowMultipleVerifications: " + this.A11 + " jitLogGenInvokeNoInlineBug: " + this.A1S + " jitApplyMirMethodLoweringInfoResolveFix: " + this.A1D + " jitApplyCallIntoJavaFromJitThreadFix: " + this.A17 + " tryPeriodicPgoProfileCompilation: " + this.A25 + " minPgoProfileRecompileTimeDays: " + this.A06 + " old_profiler_period_s_enabled: " + this.A1a + " old_profiler_period_s_: " + this.A0L + " old_profiler_duration_s_enabled: " + this.A1Y + " old_profiler_duration_s_: " + this.A0K + " old_profiler_interval_us_enabled: " + this.A1Z + " old_profiler_interval_us_: " + this.A0d + " old_profiler_backoff_coefficient_enabled: " + this.A1X + " old_profiler_backoff_coefficient_: " + this.A0A + " old_profiler_top_k_threshold_enabled: " + this.A1e + " old_profiler_top_k_threshold_: " + this.A0C + " old_profiler_top_k_change_threshold_enabled: " + this.A1d + " old_profiler_top_k_change_threshold_: " + this.A0B + " old_profiler_start_immediately_enabled: " + this.A1c + " old_profiler_start_immediately_: " + this.A1b + " pgo_recomp_use_jobsvc: " + this.A1l + " pgo_recomp_requires_charging: " + this.A1j + " pgo_recomp_requires_device_idle: " + this.A1k + " pgo_recomp_idle_opt_period_ms: " + this.A0P + " pgo_custom_min_new_methods_for_compilation_enabled: " + this.A1i + " pgo_custom_min_new_methods_for_compilation: " + this.A0O + " pgo_custom_min_new_classes_for_compilation_enabled: " + this.A1h + " pgo_custom_min_new_classes_for_compilation: " + this.A0N + " applyStringInitMapFix: " + this.A0j + " overrideProfilerThread: " + this.A1g + " disableProfilerThreadTimeoutCheck: " + this.A0s + " profilerThreadTimeoutSecs: " + this.A0T + " disableRestartThresholdOnRecomp: " + this.A0t + " restartImportanceThreshold: " + this.A0b + " restartOnlyIfScreenOff: " + this.A21 + " applyBarrierFix: " + this.A0g + " applyThreadListRaceFix: " + this.A0k + " enableAospBugFixes: " + this.A0x + " aospBugFixStringInitMapRace: " + this.A0f + " aospBugFixBarrierDtor: " + this.A0e + " doNotStartProfilerWithJit: " + this.A0u + " applyProfileSaverProfilingThreadHooks: " + this.A0i + " applyNotifyJitActivityFix: " + this.A0h + " profile_saver_profiling_thread_ioprio_class_enabled: " + this.A1n + " profile_saver_profiling_thread_ioprio_class: " + this.A0R + " profile_saver_profiling_thread_ioprio_priority_enabled: " + this.A1o + " profile_saver_profiling_thread_ioprio_priority: " + this.A0S + "]";
    }

    private static double A00(ByteBuffer byteBuffer, double d) {
        if (!byteBuffer.hasRemaining()) {
            return d;
        }
        return byteBuffer.getDouble();
    }

    private static int A01(ByteBuffer byteBuffer, int i) {
        if (!byteBuffer.hasRemaining()) {
            return i;
        }
        return byteBuffer.getInt();
    }

    private static File A03(Context context) {
        String str;
        AnonymousClass00M A002 = AnonymousClass00M.A00();
        if (A002 == null || A002.A05()) {
            str = "#MAIN#";
        } else {
            str = A002.A04();
        }
        return new File(context.getDir("RiskyStartupConfig", 4), AnonymousClass08S.A0J("RSC_READ_", str));
    }

    private static boolean A07(ByteBuffer byteBuffer, boolean z) {
        if (!byteBuffer.hasRemaining()) {
            return z;
        }
        if (byteBuffer.getInt() != 1) {
            return false;
        }
        return true;
    }

    public AnonymousClass0F3() {
        this(false, false, false, false, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, false, false, false, false, false, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, false, false, false, 0.0d, 0.0d, 0.0d, 0.0d, 0.0d, 0.0d, 0.0d, 0.0d, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 0.0d, false, 9, 1, false, false, false, false, false, false, false, false, false, false, false, false, false, 0.0d, false, 0, false, 0, false, 0, false, 0.0f, false, 0.0f, false, 0.0f, false, false, false, false, false, 0, false, 0, false, 0, false, false, false, 0, false, 0, false, false, false, false, false, false, false, false, false, false, 0, false, 0);
    }

    public AnonymousClass0F3(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, int i, boolean z6, int i2, boolean z7, int i3, boolean z8, int i4, boolean z9, int i5, boolean z10, int i6, boolean z11, int i7, boolean z12, int i8, boolean z13, boolean z14, boolean z15, boolean z16, boolean z17, boolean z18, boolean z19, int i9, boolean z20, int i10, boolean z21, int i11, boolean z22, int i12, boolean z23, int i13, boolean z24, int i14, boolean z25, int i15, boolean z26, boolean z27, boolean z28, boolean z29, double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, boolean z30, boolean z31, boolean z32, boolean z33, boolean z34, boolean z35, boolean z36, boolean z37, boolean z38, boolean z39, boolean z40, boolean z41, boolean z42, boolean z43, boolean z44, boolean z45, boolean z46, boolean z47, boolean z48, boolean z49, double d9, boolean z50, int i16, int i17, boolean z51, boolean z52, boolean z53, boolean z54, boolean z55, boolean z56, boolean z57, boolean z58, boolean z59, boolean z60, boolean z61, boolean z62, boolean z63, double d10, boolean z64, int i18, boolean z65, int i19, boolean z66, long j, boolean z67, float f, boolean z68, float f2, boolean z69, float f3, boolean z70, boolean z71, boolean z72, boolean z73, boolean z74, int i20, boolean z75, int i21, boolean z76, int i22, boolean z77, boolean z78, boolean z79, int i23, boolean z80, int i24, boolean z81, boolean z82, boolean z83, boolean z84, boolean z85, boolean z86, boolean z87, boolean z88, boolean z89, boolean z90, int i25, boolean z91, int i26) {
        this.A0y = z;
        this.A0q = z2;
        this.A0r = z3;
        this.A0p = z4;
        this.A0m = z5;
        this.A0E = i;
        this.A0n = z6;
        this.A0F = i2;
        this.A0o = z7;
        this.A0G = i3;
        this.A26 = z8;
        this.A0c = i4;
        this.A1f = z9;
        this.A0M = i5;
        this.A1m = z10;
        this.A0Q = i6;
        this.A10 = z11;
        this.A0H = i7;
        this.A0l = z12;
        this.A0D = i8;
        this.A23 = z13;
        this.A22 = z14;
        this.A0w = z15;
        this.A0v = z16;
        this.A20 = z17;
        this.A1q = z18;
        this.A1w = z19;
        this.A0Z = i9;
        this.A1z = z20;
        this.A0a = i10;
        this.A1r = z21;
        this.A0U = i11;
        this.A1u = z22;
        this.A0X = i12;
        this.A1t = z23;
        this.A0W = i13;
        this.A1v = z24;
        this.A0Y = i14;
        this.A1s = z25;
        this.A0V = i15;
        this.A1y = z26;
        this.A1x = z27;
        this.A1p = z28;
        this.A1Q = z29;
        this.A01 = d;
        this.A02 = d2;
        this.A03 = d3;
        this.A09 = d4;
        this.A07 = d5;
        this.A08 = d6;
        this.A04 = d7;
        this.A00 = d8;
        this.A0z = z30;
        this.A1L = z31;
        this.A1P = z32;
        this.A24 = z33;
        this.A14 = z34;
        this.A1J = z35;
        this.A1T = z36;
        this.A1G = z37;
        this.A1H = z38;
        this.A18 = z39;
        this.A1F = z40;
        this.A1R = z41;
        this.A12 = z42;
        this.A1M = z43;
        this.A1V = z44;
        this.A1I = z45;
        this.A1U = z46;
        this.A13 = z47;
        this.A1O = z48;
        this.A1W = z49;
        this.A05 = d9;
        this.A1N = z50;
        this.A0I = i16;
        this.A0J = i17;
        this.A1E = z51;
        this.A16 = z52;
        this.A15 = z53;
        this.A19 = z54;
        this.A1C = z55;
        this.A1K = z56;
        this.A1B = z57;
        this.A1A = z58;
        this.A11 = z59;
        this.A1S = z60;
        this.A1D = z61;
        this.A17 = z62;
        this.A25 = z63;
        this.A06 = d10;
        this.A1a = z64;
        this.A0L = i18;
        this.A1Y = z65;
        this.A0K = i19;
        this.A1Z = z66;
        this.A0d = j;
        this.A1X = z67;
        this.A0A = f;
        this.A1e = z68;
        this.A0C = f2;
        this.A1d = z69;
        this.A0B = f3;
        this.A1c = z70;
        this.A1b = z71;
        this.A1l = z72;
        this.A1j = z73;
        this.A1k = z74;
        this.A0P = i20;
        this.A1i = z75;
        this.A0O = i21;
        this.A1h = z76;
        this.A0N = i22;
        this.A0j = z77;
        this.A1g = z78;
        this.A0s = z79;
        this.A0T = i23;
        this.A0t = z80;
        this.A0b = i24;
        this.A21 = z81;
        this.A0g = z82;
        this.A0k = z83;
        this.A0x = z84;
        this.A0f = z85;
        this.A0e = z86;
        this.A0u = z87;
        this.A0i = z88;
        this.A0h = z89;
        this.A1n = z90;
        this.A0R = i25;
        this.A1o = z91;
        this.A0S = i26;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AnonymousClass0F3)) {
            return false;
        }
        AnonymousClass0F3 r7 = (AnonymousClass0F3) obj;
        if (this.A0y == r7.A0y && this.A0q == r7.A0q && this.A0r == r7.A0r && this.A0p == r7.A0p && this.A0m == r7.A0m && this.A0E == r7.A0E && this.A0n == r7.A0n && this.A0F == r7.A0F && this.A0o == r7.A0o && this.A0G == r7.A0G && this.A26 == r7.A26 && this.A0c == r7.A0c && this.A1f == r7.A1f && this.A0M == r7.A0M && this.A1m == r7.A1m && this.A0Q == r7.A0Q && this.A10 == r7.A10 && this.A0H == r7.A0H && this.A0l == r7.A0l && this.A0D == r7.A0D && this.A23 == r7.A23 && this.A22 == r7.A22 && this.A0w == r7.A0w && this.A0v == r7.A0v && this.A20 == r7.A20 && this.A1q == r7.A1q && this.A1w == r7.A1w && this.A0Z == r7.A0Z && this.A1z == r7.A1z && this.A0a == r7.A0a && this.A1r == r7.A1r && this.A0U == r7.A0U && this.A1u == r7.A1u && this.A0X == r7.A0X && this.A1t == r7.A1t && this.A0W == r7.A0W && this.A1v == r7.A1v && this.A0Y == r7.A0Y && this.A1s == r7.A1s && this.A0V == r7.A0V && this.A1y == r7.A1y && this.A1x == r7.A1x && this.A1p == r7.A1p && this.A1Q == r7.A1Q && this.A01 == r7.A01 && this.A02 == r7.A02 && this.A03 == r7.A03 && this.A09 == r7.A09 && this.A07 == r7.A07 && this.A08 == r7.A08 && this.A04 == r7.A04 && this.A00 == r7.A00 && this.A0z == r7.A0z && this.A1L == r7.A1L && this.A1P == r7.A1P && this.A24 == r7.A24 && this.A14 == r7.A14 && this.A1J == r7.A1J && this.A1T == r7.A1T && this.A1G == r7.A1G && this.A1H == r7.A1H && this.A18 == r7.A18 && this.A1F == r7.A1F && this.A1R == r7.A1R && this.A12 == r7.A12 && this.A1M == r7.A1M && this.A1V == r7.A1V && this.A1I == r7.A1I && this.A1U == r7.A1U && this.A13 == r7.A13 && this.A1O == r7.A1O && this.A1W == r7.A1W && this.A05 == r7.A05 && this.A1N == r7.A1N && this.A0I == r7.A0I && this.A0J == r7.A0J && this.A1E == r7.A1E && this.A16 == r7.A16 && this.A15 == r7.A15 && this.A19 == r7.A19 && this.A1C == r7.A1C && this.A1K == r7.A1K && this.A1B == r7.A1B && this.A1A == r7.A1A && this.A11 == r7.A11 && this.A1S == r7.A1S && this.A1D == r7.A1D && this.A17 == r7.A17 && this.A25 == r7.A25 && this.A06 == r7.A06 && this.A1a == r7.A1a && this.A0L == r7.A0L && this.A1Y == r7.A1Y && this.A0K == r7.A0K && this.A1Z == r7.A1Z && this.A0d == r7.A0d && this.A1X == r7.A1X && this.A0A == r7.A0A && this.A1e == r7.A1e && this.A0C == r7.A0C && this.A1d == r7.A1d && this.A0B == r7.A0B && this.A1c == r7.A1c && this.A1b == r7.A1b && this.A1l == r7.A1l && this.A1j == r7.A1j && this.A1k == r7.A1k && this.A0P == r7.A0P && this.A1i == r7.A1i && this.A0O == r7.A0O && this.A1h == r7.A1h && this.A0N == r7.A0N && this.A0j == r7.A0j && this.A1g == r7.A1g && this.A0s == r7.A0s && this.A0T == r7.A0T && this.A0t == r7.A0t && this.A0b == r7.A0b && this.A21 == r7.A21 && this.A0g == r7.A0g && this.A0k == r7.A0k && this.A0x == r7.A0x && this.A0f == r7.A0f && this.A0e == r7.A0e && this.A0u == r7.A0u && this.A0i == r7.A0i && this.A0h == r7.A0h && this.A1n == r7.A1n && this.A0R == r7.A0R && this.A1o == r7.A1o && this.A0S == r7.A0S) {
            return true;
        }
        return false;
    }
}
