package X;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1rc  reason: invalid class name and case insensitive filesystem */
public final class C35681rc {
    public static final Set A00 = new HashSet(Arrays.asList('<', '>', ':', '\"', '/', '\\', '|', '?', '*'));

    public static String A00(String str) {
        char[] charArray = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < charArray.length) {
            char c = charArray[i];
            if (c == '%') {
                sb.append((char) Integer.parseInt(new String(charArray, i + 1, 2), 16));
                i += 2;
            } else {
                sb.append(c);
            }
            i++;
        }
        return sb.toString();
    }
}
