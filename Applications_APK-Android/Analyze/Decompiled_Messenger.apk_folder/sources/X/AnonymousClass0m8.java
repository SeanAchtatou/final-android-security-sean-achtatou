package X;

/* renamed from: X.0m8  reason: invalid class name */
public enum AnonymousClass0m8 {
    FACEBOOK("facebookMessages"),
    SMS("smsMessages"),
    TINCAN("tincanMessages");
    
    public final String logName;

    private AnonymousClass0m8(String str) {
        this.logName = str;
    }
}
