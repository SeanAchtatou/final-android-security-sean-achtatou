package X;

import io.card.payment.BuildConfig;
import java.util.HashSet;

/* renamed from: X.1r0  reason: invalid class name and case insensitive filesystem */
public abstract class C35301r0 {
    private static final HashSet A03;
    public String A00 = BuildConfig.FLAVOR;
    public final HashSet A01 = new HashSet();
    private final HashSet A02 = new HashSet();

    public abstract void A02(String str);

    public abstract boolean A04();

    static {
        HashSet hashSet = new HashSet();
        A03 = hashSet;
        hashSet.add("_changeset");
        A03.add("_firstlayout");
    }

    private static String A00(String str, String str2, String str3) {
        String str4;
        StringBuilder sb = new StringBuilder("litho");
        if (A03.contains(str)) {
            if (C191216w.A00()) {
                str4 = "_ui";
            } else {
                str4 = "_bg";
            }
            sb.append(str4);
        }
        if (!str2.isEmpty()) {
            sb.append('_');
            sb.append(str2);
        }
        sb.append(str);
        sb.append(str3);
        return sb.toString();
    }

    public static boolean A01(C35301r0 r1) {
        if (r1 == null || !r1.A04()) {
            return false;
        }
        return true;
    }

    public void A03(String str, String str2, String str3) {
        if (str2.equals("_start")) {
            this.A02.add(A00(str, str3, BuildConfig.FLAVOR));
        } else if (str2.equals("_end") && !this.A02.remove(A00(str, str3, BuildConfig.FLAVOR))) {
            return;
        }
        String A002 = A00(str, str3, str2);
        if (!this.A01.contains(A002)) {
            A02(A002);
            this.A01.add(A002);
        }
    }
}
