package X;

import android.os.Build;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1cf  reason: invalid class name and case insensitive filesystem */
public final class C27111cf {
    private static volatile C27111cf A03;
    private AnonymousClass0UN A00;
    public final C25051Yd A01;
    private final Boolean A02;

    public static final C27111cf A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C27111cf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C27111cf(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public boolean A01() {
        if (((Boolean) AnonymousClass1XX.A03(AnonymousClass1Y3.AUm, this.A00)).booleanValue() || this.A01.Aem(282866546182066L)) {
            return true;
        }
        return false;
    }

    public boolean A02() {
        if (this.A02.booleanValue()) {
            return false;
        }
        if (A03() || A01()) {
            return true;
        }
        return false;
    }

    public boolean A03() {
        if (Build.VERSION.SDK_INT < 19 || A01()) {
            return false;
        }
        return true;
    }

    private C27111cf(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = AnonymousClass0WT.A00(r3);
        this.A02 = AnonymousClass0UU.A08(r3);
    }
}
