package X;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ser.std.BooleanSerializer;
import com.fasterxml.jackson.databind.ser.std.CalendarSerializer;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.databind.ser.std.NullSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers$DoubleSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers$FloatSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers$IntLikeSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers$IntegerSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers$LongSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers$NumberSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers$ShortSerializer;
import com.fasterxml.jackson.databind.ser.std.SqlDateSerializer;
import com.fasterxml.jackson.databind.ser.std.SqlTimeSerializer;
import com.fasterxml.jackson.databind.ser.std.StdDelegatingSerializer;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers$AtomicBooleanSerializer;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers$AtomicIntegerSerializer;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers$AtomicLongSerializer;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers$AtomicReferenceSerializer;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers$ClassSerializer;
import com.fasterxml.jackson.databind.ser.std.StdJdkSerializers$FileSerializer;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.databind.ser.std.TokenBufferSerializer;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

/* renamed from: X.0nW  reason: invalid class name and case insensitive filesystem */
public abstract class C11630nW extends C26861cG implements Serializable {
    public static final HashMap _concrete;
    public static final HashMap _concreteLazy = new HashMap();
    public final C11720nq _factoryConfig;

    public static boolean usesStaticTyping(C10450k8 r3, C10120ja r4, CY1 cy1) {
        if (cy1 != null) {
            return false;
        }
        AnonymousClass290 findSerializationTyping = r3.getAnnotationIntrospector().findSerializationTyping(r4.getClassInfo());
        if (findSerializationTyping != null) {
            return findSerializationTyping == AnonymousClass290.STATIC;
        }
        return r3.isEnabled(C26771bz.USE_STATIC_TYPING);
    }

    public abstract JsonSerializer createSerializer(C11260mU r1, C10030jR r2);

    public abstract Iterable customSerializers();

    public abstract C26861cG withConfig(C11720nq r1);

    static {
        HashMap hashMap = new HashMap();
        _concrete = hashMap;
        hashMap.put(String.class.getName(), new StringSerializer());
        ToStringSerializer toStringSerializer = ToStringSerializer.instance;
        HashMap hashMap2 = _concrete;
        hashMap2.put(StringBuffer.class.getName(), toStringSerializer);
        hashMap2.put(StringBuilder.class.getName(), toStringSerializer);
        hashMap2.put(Character.class.getName(), toStringSerializer);
        hashMap2.put(Character.TYPE.getName(), toStringSerializer);
        NumberSerializers$IntegerSerializer numberSerializers$IntegerSerializer = new NumberSerializers$IntegerSerializer();
        hashMap2.put(Integer.class.getName(), numberSerializers$IntegerSerializer);
        hashMap2.put(Integer.TYPE.getName(), numberSerializers$IntegerSerializer);
        String name = Long.class.getName();
        NumberSerializers$LongSerializer numberSerializers$LongSerializer = NumberSerializers$LongSerializer.instance;
        hashMap2.put(name, numberSerializers$LongSerializer);
        hashMap2.put(Long.TYPE.getName(), numberSerializers$LongSerializer);
        String name2 = Byte.class.getName();
        NumberSerializers$IntLikeSerializer numberSerializers$IntLikeSerializer = NumberSerializers$IntLikeSerializer.instance;
        hashMap2.put(name2, numberSerializers$IntLikeSerializer);
        hashMap2.put(Byte.TYPE.getName(), numberSerializers$IntLikeSerializer);
        String name3 = Short.class.getName();
        NumberSerializers$ShortSerializer numberSerializers$ShortSerializer = NumberSerializers$ShortSerializer.instance;
        hashMap2.put(name3, numberSerializers$ShortSerializer);
        hashMap2.put(Short.TYPE.getName(), numberSerializers$ShortSerializer);
        String name4 = Float.class.getName();
        NumberSerializers$FloatSerializer numberSerializers$FloatSerializer = NumberSerializers$FloatSerializer.instance;
        hashMap2.put(name4, numberSerializers$FloatSerializer);
        hashMap2.put(Float.TYPE.getName(), numberSerializers$FloatSerializer);
        String name5 = Double.class.getName();
        NumberSerializers$DoubleSerializer numberSerializers$DoubleSerializer = NumberSerializers$DoubleSerializer.instance;
        hashMap2.put(name5, numberSerializers$DoubleSerializer);
        hashMap2.put(Double.TYPE.getName(), numberSerializers$DoubleSerializer);
        hashMap2.put(Boolean.TYPE.getName(), new BooleanSerializer(true));
        hashMap2.put(Boolean.class.getName(), new BooleanSerializer(false));
        NumberSerializers$NumberSerializer numberSerializers$NumberSerializer = new NumberSerializers$NumberSerializer();
        hashMap2.put(BigInteger.class.getName(), numberSerializers$NumberSerializer);
        hashMap2.put(BigDecimal.class.getName(), numberSerializers$NumberSerializer);
        hashMap2.put(Calendar.class.getName(), CalendarSerializer.instance);
        DateSerializer dateSerializer = DateSerializer.instance;
        hashMap2.put(Date.class.getName(), dateSerializer);
        hashMap2.put(Timestamp.class.getName(), dateSerializer);
        HashMap hashMap3 = _concreteLazy;
        hashMap3.put(java.sql.Date.class.getName(), SqlDateSerializer.class);
        hashMap3.put(Time.class.getName(), SqlTimeSerializer.class);
        HashMap hashMap4 = new HashMap();
        ToStringSerializer toStringSerializer2 = ToStringSerializer.instance;
        hashMap4.put(URL.class, toStringSerializer2);
        hashMap4.put(URI.class, toStringSerializer2);
        hashMap4.put(Currency.class, toStringSerializer2);
        hashMap4.put(UUID.class, toStringSerializer2);
        hashMap4.put(Pattern.class, toStringSerializer2);
        Class<Locale> cls = Locale.class;
        hashMap4.put(cls, toStringSerializer2);
        hashMap4.put(cls, toStringSerializer2);
        hashMap4.put(AtomicReference.class, StdJdkSerializers$AtomicReferenceSerializer.class);
        hashMap4.put(AtomicBoolean.class, StdJdkSerializers$AtomicBooleanSerializer.class);
        hashMap4.put(AtomicInteger.class, StdJdkSerializers$AtomicIntegerSerializer.class);
        hashMap4.put(AtomicLong.class, StdJdkSerializers$AtomicLongSerializer.class);
        hashMap4.put(File.class, StdJdkSerializers$FileSerializer.class);
        hashMap4.put(Class.class, StdJdkSerializers$ClassSerializer.class);
        hashMap4.put(Void.TYPE, NullSerializer.class);
        for (Map.Entry entry : hashMap4.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof JsonSerializer) {
                hashMap2.put(((Class) entry.getKey()).getName(), (JsonSerializer) value);
            } else if (value instanceof Class) {
                _concreteLazy.put(((Class) entry.getKey()).getName(), (Class) value);
            } else {
                throw new IllegalStateException(AnonymousClass08S.A0J("Internal error: unrecognized value of type ", entry.getClass().getName()));
            }
        }
        _concreteLazy.put(C11700no.class.getName(), TokenBufferSerializer.class);
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fasterxml.jackson.databind.JsonSerializer createKeySerializer(X.C10450k8 r6, X.C10030jR r7, com.fasterxml.jackson.databind.JsonSerializer r8) {
        /*
            r5 = this;
            java.lang.Class r0 = r7._class
            X.0jR r0 = r6.constructType(r0)
            X.0ja r4 = r6.introspectClassAnnotations(r0)
            X.0nq r0 = r5._factoryConfig
            X.0nr[] r3 = r0._additionalKeySerializers
            int r1 = r3.length
            r0 = 0
            if (r1 <= 0) goto L_0x0013
            r0 = 1
        L_0x0013:
            r2 = 0
            if (r0 == 0) goto L_0x0031
            X.1fy r0 = new X.1fy
            r0.<init>(r3)
            java.util.Iterator r1 = r0.iterator()
        L_0x001f:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0031
            java.lang.Object r0 = r1.next()
            X.0nr r0 = (X.C11730nr) r0
            com.fasterxml.jackson.databind.JsonSerializer r2 = r0.findSerializer(r6, r7, r4)
            if (r2 == 0) goto L_0x001f
        L_0x0031:
            if (r2 != 0) goto L_0x0079
            if (r8 != 0) goto L_0x003f
            if (r7 == 0) goto L_0x0076
            java.lang.Class r1 = r7._class
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r1 != r0) goto L_0x005c
            com.fasterxml.jackson.databind.JsonSerializer r8 = X.BU4.DEFAULT_STRING_SERIALIZER
        L_0x003f:
            X.0nq r1 = r5._factoryConfig
            boolean r0 = r1.hasSerializerModifiers()
            if (r0 == 0) goto L_0x007b
            X.0ns[] r1 = r1._modifiers
            X.1fy r0 = new X.1fy
            r0.<init>(r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x0052:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x007b
            r1.next()
            goto L_0x0052
        L_0x005c:
            java.lang.Class<java.lang.Object> r0 = java.lang.Object.class
            if (r1 == r0) goto L_0x0076
            java.lang.Class<java.util.Date> r0 = java.util.Date.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x006b
            com.fasterxml.jackson.databind.JsonSerializer r8 = com.fasterxml.jackson.databind.ser.std.StdKeySerializers$DateKeySerializer.instance
            goto L_0x003f
        L_0x006b:
            java.lang.Class<java.util.Calendar> r0 = java.util.Calendar.class
            boolean r0 = r0.isAssignableFrom(r1)
            if (r0 == 0) goto L_0x0076
            com.fasterxml.jackson.databind.JsonSerializer r8 = com.fasterxml.jackson.databind.ser.std.StdKeySerializers$CalendarKeySerializer.instance
            goto L_0x003f
        L_0x0076:
            com.fasterxml.jackson.databind.JsonSerializer r8 = X.BU4.DEFAULT_KEY_SERIALIZER
            goto L_0x003f
        L_0x0079:
            r8 = r2
            goto L_0x003f
        L_0x007b:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11630nW.createKeySerializer(X.0k8, X.0jR, com.fasterxml.jackson.databind.JsonSerializer):com.fasterxml.jackson.databind.JsonSerializer");
    }

    public CY1 createTypeSerializer(C10450k8 r6, C10030jR r7) {
        Collection collectAndResolveSubtypes;
        C10070jV classInfo = r6.introspectClassAnnotations(r6.constructType(r7._class)).getClassInfo();
        C10140jc annotationIntrospector = r6.getAnnotationIntrospector();
        CXQ findTypeResolver = annotationIntrospector.findTypeResolver(r6, classInfo, r7);
        if (findTypeResolver == null) {
            findTypeResolver = r6._base._typeResolverBuilder;
            collectAndResolveSubtypes = null;
        } else {
            collectAndResolveSubtypes = r6._subtypeResolver.collectAndResolveSubtypes(classInfo, r6, annotationIntrospector);
        }
        if (findTypeResolver == null) {
            return null;
        }
        return findTypeResolver.buildTypeSerializer(r6, r7, collectAndResolveSubtypes);
    }

    public final C26861cG withAdditionalSerializers(C11730nr r6) {
        C11720nq r4 = this._factoryConfig;
        if (r6 != null) {
            return withConfig(new C11720nq((C11730nr[]) C11830o1.insertInListNoDup(r4._additionalSerializers, r6), r4._additionalKeySerializers, r4._modifiers));
        }
        throw new IllegalArgumentException("Can not pass null Serializers");
    }

    public final C26861cG withSerializerModifier(C11740ns r6) {
        C11720nq r4 = this._factoryConfig;
        if (r6 != null) {
            return withConfig(new C11720nq(r4._additionalSerializers, r4._additionalKeySerializers, (C11740ns[]) C11830o1.insertInListNoDup(r4._modifiers, r6)));
        }
        throw new IllegalArgumentException("Can not pass null modifier");
    }

    public C11630nW(C11720nq r1) {
        this._factoryConfig = r1 == null ? new C11720nq() : r1;
    }

    public static C10030jR modifySecondaryTypesByAnnotation(C10450k8 r12, C10080jW r13, C10030jR r14) {
        C10140jc annotationIntrospector = r12.getAnnotationIntrospector();
        if (r14.isContainerType()) {
            Class findSerializationKeyType = annotationIntrospector.findSerializationKeyType(r13, r14.getKeyType());
            if (findSerializationKeyType != null) {
                if (r14 instanceof C180610a) {
                    try {
                        C180610a r2 = (C180610a) r14;
                        C10030jR r1 = r2._keyType;
                        if (findSerializationKeyType == r1._class) {
                            r14 = r2;
                        } else {
                            r14 = new C180610a(r2._class, r1.widenBy(findSerializationKeyType), r2._valueType, r2._valueHandler, r2._typeHandler, r2._asStatic);
                        }
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException("Failed to narrow key type " + r14 + " with key-type annotation (" + findSerializationKeyType.getName() + "): " + e.getMessage());
                    }
                } else {
                    throw new IllegalArgumentException("Illegal key-type annotation: type " + r14 + " is not a Map type");
                }
            }
            Class findSerializationContentType = annotationIntrospector.findSerializationContentType(r13, r14.getContentType());
            if (findSerializationContentType != null) {
                try {
                    return r14.widenContentsBy(findSerializationContentType);
                } catch (IllegalArgumentException e2) {
                    throw new IllegalArgumentException("Failed to narrow content type " + r14 + " with content-type annotation (" + findSerializationContentType.getName() + "): " + e2.getMessage());
                }
            }
        }
        return r14;
    }

    public JsonSerializer findSerializerFromAnnotation(C11260mU r5, C10080jW r6) {
        C422428v converterInstance;
        Object findSerializer = r5.getAnnotationIntrospector().findSerializer(r6);
        if (findSerializer == null) {
            return null;
        }
        JsonSerializer serializerInstance = r5.serializerInstance(r6, findSerializer);
        Object findSerializationConverter = r5.getAnnotationIntrospector().findSerializationConverter(r6);
        if (findSerializationConverter == null) {
            converterInstance = null;
        } else {
            converterInstance = r5.converterInstance(r6, findSerializationConverter);
        }
        if (converterInstance != null) {
            return new StdDelegatingSerializer(converterInstance, converterInstance.getOutputType(r5.getTypeFactory()), serializerInstance);
        }
        return serializerInstance;
    }
}
