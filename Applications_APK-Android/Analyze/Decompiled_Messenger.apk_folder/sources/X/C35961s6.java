package X;

import com.facebook.graphql.enums.GraphQLMessengerCallToActionRenderStyle;
import com.facebook.graphql.enums.GraphQLMessengerCallToActionType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1s6  reason: invalid class name and case insensitive filesystem */
public interface C35961s6 extends C12200oo {
    ImmutableList Abl();

    GraphQLMessengerCallToActionType Abm();

    String Abp();

    String Abq();

    GSTModelShape1S0000000 Ag2();

    GSTModelShape1S0000000 Ait();

    GraphQLMessengerCallToActionRenderStyle Aiu();

    boolean Aqt();

    boolean ArA();

    boolean ArG();

    String Asu();

    String AvP();

    String Awz();

    GSTModelShape1S0000000 AxZ();

    GSTModelShape1S0000000 B84();

    GSTModelShape1S0000000 B9r();

    String getId();
}
