package X;

/* renamed from: X.08B  reason: invalid class name */
public final class AnonymousClass08B {
    public static C004205c A00 = new C013009r();
    public static C004205c A01 = new C004305g();

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.common.util.TriState A00(java.util.List r2, X.C004205c r3, com.facebook.common.util.TriState r4) {
        /*
            boolean r0 = r2.isEmpty()
            if (r0 == 0) goto L_0x0009
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            return r0
        L_0x0009:
            java.util.Iterator r2 = r2.iterator()
            java.lang.Object r1 = r2.next()
            com.facebook.common.util.TriState r1 = (com.facebook.common.util.TriState) r1
        L_0x0013:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0025
            java.lang.Object r0 = r2.next()
            java.lang.Object r1 = r3.ATP(r1, r0)
            com.facebook.common.util.TriState r1 = (com.facebook.common.util.TriState) r1
            if (r1 != r4) goto L_0x0013
        L_0x0025:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass08B.A00(java.util.List, X.05c, com.facebook.common.util.TriState):com.facebook.common.util.TriState");
    }
}
