package X;

import com.facebook.acra.ACRA;

/* renamed from: X.10P  reason: invalid class name */
public final class AnonymousClass10P {
    static {
        AnonymousClass07B.A00(7);
    }

    public static int A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return 2132148234;
            case 2:
                return 2132148255;
            case 3:
                return 2132148246;
            case 4:
                return 2132148225;
            case 5:
                return 2132148269;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return 2132148263;
            default:
                return 2132148239;
        }
    }
}
