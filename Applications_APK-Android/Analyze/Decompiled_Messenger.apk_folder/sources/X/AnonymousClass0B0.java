package X;

import java.util.Map;

/* renamed from: X.0B0  reason: invalid class name */
public interface AnonymousClass0B0 {
    AnonymousClass0DD AY8();

    boolean contains(String str);

    Map getAll();

    boolean getBoolean(String str, boolean z);

    int getInt(String str, int i);

    long getLong(String str, long j);

    String getString(String str, String str2);
}
