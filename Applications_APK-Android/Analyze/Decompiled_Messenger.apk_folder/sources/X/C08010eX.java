package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import com.facebook.acra.ACRA;
import java.util.HashSet;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.0eX  reason: invalid class name and case insensitive filesystem */
public abstract class C08010eX extends C26141ay implements C08020eY {
    public C08110eh A00;
    public int[] A01 = new int[0];
    public final Context A02;
    public final Handler A03;
    public final C26321bG A04;
    public final C08080ee A05;
    public final C08040ea A06;
    public final C08030eZ A07;
    public volatile boolean A08 = false;
    private volatile boolean A09 = false;

    public C08280ez A09() {
        if (!(this instanceof C08070ed)) {
            return null;
        }
        return (C08280ez) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ATa, ((C08070ed) this).A00);
    }

    public abstract void A0A();

    public boolean A0C() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B50, ((C08070ed) this).A00)).AbO(98, false);
    }

    public boolean A0D() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, ((C08070ed) this).A00)).AbO(634, false);
    }

    public boolean A0E() {
        if (!(this instanceof C08070ed)) {
            return false;
        }
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, ((C08070ed) this).A00)).AbO(633, false);
    }

    public boolean A0F(int i) {
        return true;
    }

    public void AOF() {
        C08040ea.A06 = false;
    }

    public void AOG() {
        C08040ea.A06 = true;
    }

    public static C07210ct A00(int i, JSONObject jSONObject) {
        int i2;
        int i3;
        JSONObject optJSONObject;
        JSONObject jSONObject2 = jSONObject.getJSONObject("optimization");
        JSONObject jSONObject3 = jSONObject2.getJSONObject("type");
        JSONArray optJSONArray = jSONObject2.optJSONArray("blacklistedInstanceKeys");
        HashSet hashSet = new HashSet();
        if (optJSONArray != null) {
            i2 = optJSONArray.length();
        } else {
            i2 = 0;
        }
        for (int i4 = 0; i4 < i2; i4++) {
            hashSet.add(Integer.valueOf(optJSONArray.getInt(i4)));
        }
        int[] A012 = C07210ct.A01();
        int length = A012.length;
        int i5 = 0;
        while (true) {
            if (i5 >= length) {
                i3 = -1;
                break;
            }
            i3 = A012[i5];
            if (!jSONObject3.isNull(C07210ct.A00(i3))) {
                break;
            }
            i5++;
        }
        if (!(i3 == -1 || (optJSONObject = jSONObject3.optJSONObject(C07210ct.A00(i3))) == null)) {
            if (i3 == 10) {
                int optInt = optJSONObject.optInt("timeoutInMillis");
                C26501bY r2 = new C08310f2().A00;
                r2.A01 = optInt;
                return new C07210ct(10, i, r2, hashSet);
            } else if (i3 == 11) {
                int optInt2 = optJSONObject.optInt("timeoutInMillis");
                C26501bY r22 = new C08310f2().A00;
                r22.A01 = optInt2;
                r22.A03 = i;
                return new C07210ct(11, i, r22, hashSet);
            } else if (i3 == 13) {
                return new C07210ct(13, i, null, hashSet);
            } else {
                switch (i3) {
                    case 1:
                        int optInt3 = optJSONObject.optInt("timeout");
                        int optInt4 = optJSONObject.optInt("frequency");
                        C26501bY r23 = new C08310f2().A00;
                        r23.A03 = optInt4;
                        r23.A01 = optInt3;
                        return new C07210ct(1, i, r23, hashSet);
                    case 2:
                        int optInt5 = optJSONObject.optInt("timeout");
                        int optInt6 = optJSONObject.optInt("frequency");
                        C26501bY r24 = new C08310f2().A00;
                        r24.A03 = optInt6;
                        r24.A01 = optInt5;
                        return new C07210ct(2, i, r24, hashSet);
                    case 3:
                        int optInt7 = optJSONObject.optInt("timeoutInMillis");
                        C26501bY r25 = new C08310f2().A00;
                        r25.A01 = optInt7;
                        return new C07210ct(3, i, r25, hashSet);
                    case 4:
                        C26501bY r26 = new C08310f2().A00;
                        r26.A03 = i;
                        return new C07210ct(4, i, r26, hashSet);
                    case 5:
                        int i6 = optJSONObject.optJSONArray("boostedThreads").getInt(0);
                        int i7 = optJSONObject.optJSONArray("enabledCoreConfigs").getInt(0);
                        C26501bY r27 = new C08310f2().A00;
                        r27.A03 = i6;
                        r27.A00 = i7;
                        return new C07210ct(5, i, r27, hashSet);
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        int optInt8 = optJSONObject.optInt("priority");
                        C26501bY r28 = new C08310f2().A00;
                        r28.A03 = optInt8;
                        return new C07210ct(6, i, r28, hashSet);
                }
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r6 = new android.util.SparseArray();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r7 = new android.util.SparseArray();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00ed */
    /* JADX WARNING: Missing exception handler attribute for start block: B:72:0x01bb */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C08010eX r15) {
        /*
            boolean r0 = r15.A09
            if (r0 != 0) goto L_0x0275
            monitor-enter(r15)
            boolean r0 = r15.A09     // Catch:{ all -> 0x0272 }
            if (r0 != 0) goto L_0x0270
            X.1bG r5 = r15.A04     // Catch:{ all -> 0x0272 }
            boolean r0 = X.C26321bG.A01(r5)     // Catch:{ all -> 0x0272 }
            if (r0 != 0) goto L_0x0023
            X.1bU r4 = new X.1bU     // Catch:{ all -> 0x0272 }
            r3 = 20
            r2 = 0
            r1 = 0
            r4.<init>(r3, r2, r1, r2)     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = X.C26481bW.A00(r3)     // Catch:{ all -> 0x0272 }
            r4.A01(r0)     // Catch:{ all -> 0x0272 }
            r5.A04 = r4     // Catch:{ all -> 0x0272 }
        L_0x0023:
            X.1bG r6 = r15.A04     // Catch:{ all -> 0x0272 }
            X.1bU r5 = new X.1bU     // Catch:{ all -> 0x0272 }
            r4 = 1
            r3 = 0
            r2 = 0
            r5.<init>(r4, r3, r2, r3)     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = "init_from_data_readers"
            r5.A01(r0)     // Catch:{ all -> 0x0272 }
            r6.A03 = r5     // Catch:{ all -> 0x0272 }
            X.0ez r9 = r15.A09()     // Catch:{ all -> 0x0272 }
            if (r9 == 0) goto L_0x0223
            X.1Yd r2 = r9.A00     // Catch:{ all -> 0x0272 }
            r0 = 848028407693860(0x3034700000224, double:4.18981702938991E-309)
            java.lang.String r1 = r2.B4C(r0)     // Catch:{ all -> 0x0272 }
            android.util.SparseArray r6 = new android.util.SparseArray     // Catch:{ all -> 0x0272 }
            r6.<init>()     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = "EMPTY"
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0272 }
            if (r0 != 0) goto L_0x00f2
            android.util.SparseArray r6 = new android.util.SparseArray     // Catch:{ all -> 0x0272 }
            r6.<init>()     // Catch:{ all -> 0x0272 }
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00ed }
            r11.<init>(r1)     // Catch:{ JSONException -> 0x00ed }
            int[] r10 = X.C07210ct.A01()     // Catch:{ JSONException -> 0x00ed }
            int r8 = r10.length     // Catch:{ JSONException -> 0x00ed }
            r7 = 0
        L_0x0062:
            if (r7 >= r8) goto L_0x00f2
            r5 = r10[r7]     // Catch:{ JSONException -> 0x00ed }
            java.lang.String r1 = X.C07210ct.A00(r5)     // Catch:{ JSONException -> 0x00ed }
            org.json.JSONArray r0 = r11.optJSONArray(r1)     // Catch:{ JSONException -> 0x00ed }
            if (r0 == 0) goto L_0x00e9
            org.json.JSONArray r4 = r11.getJSONArray(r1)     // Catch:{ JSONException -> 0x00ed }
            int r3 = r4.length()     // Catch:{ JSONException -> 0x00ed }
            r2 = 0
        L_0x0079:
            if (r2 >= r3) goto L_0x00e9
            org.json.JSONObject r12 = r4.optJSONObject(r2)     // Catch:{ JSONException -> 0x00ed }
            X.0kk r1 = new X.0kk     // Catch:{ JSONException -> 0x00ed }
            r1.<init>()     // Catch:{ JSONException -> 0x00ed }
            java.lang.String r0 = "osVersion"
            java.lang.String r14 = r12.optString(r0)     // Catch:{ JSONException -> 0x00ed }
            java.lang.String r13 = "null"
            boolean r0 = r14.equals(r13)     // Catch:{ JSONException -> 0x00ed }
            if (r0 != 0) goto L_0x0094
            r1.A04 = r14     // Catch:{ JSONException -> 0x00ed }
        L_0x0094:
            java.lang.String r0 = "deviceModel"
            java.lang.String r14 = r12.optString(r0)     // Catch:{ JSONException -> 0x00ed }
            boolean r0 = r14.equals(r13)     // Catch:{ JSONException -> 0x00ed }
            if (r0 != 0) goto L_0x00a2
            r1.A03 = r14     // Catch:{ JSONException -> 0x00ed }
        L_0x00a2:
            java.lang.String r0 = "deviceBrand"
            java.lang.String r14 = r12.optString(r0)     // Catch:{ JSONException -> 0x00ed }
            boolean r0 = r14.equals(r13)     // Catch:{ JSONException -> 0x00ed }
            if (r0 != 0) goto L_0x00b0
            r1.A02 = r14     // Catch:{ JSONException -> 0x00ed }
        L_0x00b0:
            java.lang.String r0 = "deviceChipset"
            java.lang.String r14 = r12.optString(r0)     // Catch:{ JSONException -> 0x00ed }
            boolean r0 = r14.equals(r13)     // Catch:{ JSONException -> 0x00ed }
            if (r0 != 0) goto L_0x00be
            r1.A01 = r14     // Catch:{ JSONException -> 0x00ed }
        L_0x00be:
            java.lang.String r0 = "appVersion"
            java.lang.String r12 = r12.optString(r0)     // Catch:{ JSONException -> 0x00ed }
            boolean r0 = r12.equals(r13)     // Catch:{ JSONException -> 0x00ed }
            if (r0 != 0) goto L_0x00cc
            r1.A00 = r12     // Catch:{ JSONException -> 0x00ed }
        L_0x00cc:
            java.lang.Object r0 = r6.get(r5)     // Catch:{ JSONException -> 0x00ed }
            if (r0 != 0) goto L_0x00da
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ JSONException -> 0x00ed }
            r0.<init>()     // Catch:{ JSONException -> 0x00ed }
            r6.put(r5, r0)     // Catch:{ JSONException -> 0x00ed }
        L_0x00da:
            java.lang.Object r0 = r6.get(r5)     // Catch:{ JSONException -> 0x00ed }
            java.util.List r0 = (java.util.List) r0     // Catch:{ JSONException -> 0x00ed }
            r0.add(r1)     // Catch:{ JSONException -> 0x00ed }
            r6.put(r5, r0)     // Catch:{ JSONException -> 0x00ed }
            int r2 = r2 + 1
            goto L_0x0079
        L_0x00e9:
            int r7 = r7 + 1
            goto L_0x0062
        L_0x00ed:
            android.util.SparseArray r6 = new android.util.SparseArray     // Catch:{ all -> 0x0272 }
            r6.<init>()     // Catch:{ all -> 0x0272 }
        L_0x00f2:
            X.0eh r1 = new X.0eh     // Catch:{ all -> 0x0272 }
            android.content.Context r0 = r15.A02     // Catch:{ all -> 0x0272 }
            r1.<init>(r0, r6)     // Catch:{ all -> 0x0272 }
            r15.A00 = r1     // Catch:{ all -> 0x0272 }
            X.1Yd r2 = r9.A00     // Catch:{ all -> 0x0272 }
            r0 = 848032702661157(0x3034800000225, double:4.189838249347823E-309)
            java.lang.String r3 = r2.B4C(r0)     // Catch:{ all -> 0x0272 }
            android.util.SparseArray r7 = new android.util.SparseArray     // Catch:{ all -> 0x0272 }
            r7.<init>()     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = "EMPTY"
            boolean r0 = r3.equals(r0)     // Catch:{ all -> 0x0272 }
            if (r0 != 0) goto L_0x01c0
            java.lang.String r2 = "config_v2"
            android.util.SparseArray r7 = new android.util.SparseArray     // Catch:{ JSONException -> 0x01bb }
            r7.<init>()     // Catch:{ JSONException -> 0x01bb }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01bb }
            r1.<init>(r3)     // Catch:{ JSONException -> 0x01bb }
            boolean r0 = r1.has(r2)     // Catch:{ JSONException -> 0x01bb }
            if (r0 == 0) goto L_0x0170
            org.json.JSONArray r11 = r1.optJSONArray(r2)     // Catch:{ JSONException -> 0x01bb }
            if (r11 == 0) goto L_0x0170
            int r10 = r11.length()     // Catch:{ JSONException -> 0x01bb }
            r9 = 0
        L_0x0130:
            if (r9 >= r10) goto L_0x01c0
            org.json.JSONObject r8 = r11.getJSONObject(r9)     // Catch:{ JSONException -> 0x01bb }
            org.json.JSONObject r1 = r11.getJSONObject(r9)     // Catch:{ JSONException -> 0x01bb }
            java.lang.String r0 = "markers"
            org.json.JSONArray r5 = r1.getJSONArray(r0)     // Catch:{ JSONException -> 0x01bb }
            int r4 = r5.length()     // Catch:{ JSONException -> 0x01bb }
            r3 = 0
        L_0x0145:
            if (r3 >= r4) goto L_0x016d
            int r0 = r5.getInt(r3)     // Catch:{ JSONException -> 0x01bb }
            X.0ct r2 = A00(r0, r8)     // Catch:{ JSONException -> 0x01bb }
            if (r2 == 0) goto L_0x016a
            int r12 = r2.A04     // Catch:{ JSONException -> 0x01bb }
            java.lang.Object r0 = r7.get(r12)     // Catch:{ JSONException -> 0x01bb }
            if (r0 != 0) goto L_0x0161
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ JSONException -> 0x01bb }
            r0.<init>()     // Catch:{ JSONException -> 0x01bb }
            r7.put(r12, r0)     // Catch:{ JSONException -> 0x01bb }
        L_0x0161:
            java.lang.Object r0 = r7.get(r12)     // Catch:{ JSONException -> 0x01bb }
            java.util.List r0 = (java.util.List) r0     // Catch:{ JSONException -> 0x01bb }
            r0.add(r2)     // Catch:{ JSONException -> 0x01bb }
        L_0x016a:
            int r3 = r3 + 1
            goto L_0x0145
        L_0x016d:
            int r9 = r9 + 1
            goto L_0x0130
        L_0x0170:
            java.lang.String r0 = "actions"
            org.json.JSONArray r8 = r1.getJSONArray(r0)     // Catch:{ JSONException -> 0x01bb }
            int r5 = r8.length()     // Catch:{ JSONException -> 0x01bb }
            r4 = 0
        L_0x017b:
            if (r4 >= r5) goto L_0x01c0
            org.json.JSONObject r2 = r8.optJSONObject(r4)     // Catch:{ JSONException -> 0x01bb }
            java.lang.String r0 = "trigger"
            org.json.JSONObject r1 = r2.getJSONObject(r0)     // Catch:{ JSONException -> 0x01bb }
            java.lang.String r0 = "qpl"
            org.json.JSONObject r1 = r1.getJSONObject(r0)     // Catch:{ JSONException -> 0x01bb }
            java.lang.String r0 = "markerId"
            int r1 = r1.getInt(r0)     // Catch:{ JSONException -> 0x01bb }
            java.lang.String r0 = "type"
            org.json.JSONObject r0 = r2.getJSONObject(r0)     // Catch:{ JSONException -> 0x01bb }
            X.0ct r3 = A00(r1, r0)     // Catch:{ JSONException -> 0x01bb }
            if (r3 == 0) goto L_0x01b8
            int r2 = r3.A04     // Catch:{ JSONException -> 0x01bb }
            java.lang.Object r0 = r7.get(r2)     // Catch:{ JSONException -> 0x01bb }
            if (r0 != 0) goto L_0x01af
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ JSONException -> 0x01bb }
            r0.<init>()     // Catch:{ JSONException -> 0x01bb }
            r7.put(r2, r0)     // Catch:{ JSONException -> 0x01bb }
        L_0x01af:
            java.lang.Object r0 = r7.get(r2)     // Catch:{ JSONException -> 0x01bb }
            java.util.List r0 = (java.util.List) r0     // Catch:{ JSONException -> 0x01bb }
            r0.add(r3)     // Catch:{ JSONException -> 0x01bb }
        L_0x01b8:
            int r4 = r4 + 1
            goto L_0x017b
        L_0x01bb:
            android.util.SparseArray r7 = new android.util.SparseArray     // Catch:{ all -> 0x0272 }
            r7.<init>()     // Catch:{ all -> 0x0272 }
        L_0x01c0:
            int r0 = r7.size()     // Catch:{ all -> 0x0272 }
            if (r0 == 0) goto L_0x0223
            int[] r8 = X.C07210ct.A01()     // Catch:{ all -> 0x0272 }
            int r5 = r8.length     // Catch:{ all -> 0x0272 }
            r4 = 0
        L_0x01cc:
            if (r4 >= r5) goto L_0x0207
            r1 = r8[r4]     // Catch:{ all -> 0x0272 }
            boolean r0 = r15.A0G(r1)     // Catch:{ all -> 0x0272 }
            if (r0 != 0) goto L_0x0204
            java.lang.Object r2 = r7.get(r1)     // Catch:{ all -> 0x0272 }
            java.util.List r2 = (java.util.List) r2     // Catch:{ all -> 0x0272 }
            if (r2 == 0) goto L_0x0204
            X.0ee r0 = r15.A02     // Catch:{ all -> 0x0272 }
            X.0f6 r1 = r0.A01(r1)     // Catch:{ all -> 0x0272 }
            r0 = 0
            if (r1 == 0) goto L_0x01e8
            r0 = 1
        L_0x01e8:
            if (r0 == 0) goto L_0x0204
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0272 }
        L_0x01ee:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0272 }
            if (r0 == 0) goto L_0x0204
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0272 }
            X.0ct r1 = (X.C07210ct) r1     // Catch:{ all -> 0x0272 }
            int r0 = r1.A03     // Catch:{ all -> 0x0272 }
            X.0ea r0 = X.C26141ay.A04(r15, r0)     // Catch:{ all -> 0x0272 }
            r0.A01(r1)     // Catch:{ all -> 0x0272 }
            goto L_0x01ee
        L_0x0204:
            int r4 = r4 + 1
            goto L_0x01cc
        L_0x0207:
            X.1bG r5 = r15.A04     // Catch:{ all -> 0x0272 }
            int r0 = r7.size()     // Catch:{ all -> 0x0272 }
            int r4 = r6.size()     // Catch:{ all -> 0x0272 }
            X.1bU r3 = r5.A03     // Catch:{ all -> 0x0272 }
            long r1 = (long) r0     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = "optimizations_count"
            r3.A02(r0, r1)     // Catch:{ all -> 0x0272 }
            long r1 = (long) r4     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = "blacklists_count"
            r3.A02(r0, r1)     // Catch:{ all -> 0x0272 }
            r0 = 0
            X.C26321bG.A00(r5, r3, r0)     // Catch:{ all -> 0x0272 }
        L_0x0223:
            X.1bG r6 = r15.A04     // Catch:{ all -> 0x0272 }
            X.1bU r5 = new X.1bU     // Catch:{ all -> 0x0272 }
            r4 = 1
            r3 = 0
            r2 = 0
            r5.<init>(r4, r3, r2, r3)     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = "init_from_optimizations"
            r5.A01(r0)     // Catch:{ all -> 0x0272 }
            r6.A05 = r5     // Catch:{ all -> 0x0272 }
            r15.A0A()     // Catch:{ all -> 0x0272 }
            X.1bG r2 = r15.A04     // Catch:{ all -> 0x0272 }
            X.1bU r1 = r2.A05     // Catch:{ all -> 0x0272 }
            X.C26321bG.A00(r2, r1, r3)     // Catch:{ all -> 0x0272 }
            android.util.SparseArray r0 = r15.A01     // Catch:{ all -> 0x0272 }
            int r5 = r0.size()     // Catch:{ all -> 0x0272 }
            int[] r0 = new int[r5]     // Catch:{ all -> 0x0272 }
            r15.A01 = r0     // Catch:{ all -> 0x0272 }
            r2 = 0
        L_0x0249:
            if (r2 >= r5) goto L_0x0258
            int[] r1 = r15.A01     // Catch:{ all -> 0x0272 }
            android.util.SparseArray r0 = r15.A01     // Catch:{ all -> 0x0272 }
            int r0 = r0.keyAt(r2)     // Catch:{ all -> 0x0272 }
            r1[r2] = r0     // Catch:{ all -> 0x0272 }
            int r2 = r2 + 1
            goto L_0x0249
        L_0x0258:
            r15.A09 = r4     // Catch:{ all -> 0x0272 }
            X.1bG r4 = r15.A04     // Catch:{ all -> 0x0272 }
            boolean r0 = X.C26321bG.A01(r4)     // Catch:{ all -> 0x0272 }
            if (r0 != 0) goto L_0x0270
            X.1bU r3 = r4.A04     // Catch:{ all -> 0x0272 }
            long r1 = (long) r5     // Catch:{ all -> 0x0272 }
            java.lang.String r0 = "markers_count"
            r3.A02(r0, r1)     // Catch:{ all -> 0x0272 }
            X.1bU r1 = r4.A04     // Catch:{ all -> 0x0272 }
            r0 = 0
            X.C26321bG.A00(r4, r1, r0)     // Catch:{ all -> 0x0272 }
        L_0x0270:
            monitor-exit(r15)     // Catch:{ all -> 0x0272 }
            return
        L_0x0272:
            r0 = move-exception
            monitor-exit(r15)     // Catch:{ all -> 0x0272 }
            throw r0
        L_0x0275:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08010eX.A01(X.0eX):void");
    }

    public final void A0B(int i, AnonymousClass0f6 r4) {
        C08080ee r1 = this.A05;
        synchronized (r1) {
            r1.A00.put(i, r4);
        }
    }

    public boolean A0G(int i) {
        List<C10720kk> list;
        String str;
        String str2;
        String str3;
        String str4;
        boolean z;
        C08110eh r3 = this.A00;
        if (r3.A05.size() == 0 || (list = (List) r3.A05.get(i)) == null || list.isEmpty()) {
            return false;
        }
        for (C10720kk r7 : list) {
            String str5 = r3.A04;
            String str6 = r3.A03;
            String str7 = r3.A01;
            String str8 = r3.A02;
            String str9 = r3.A00;
            String str10 = r7.A04;
            if ((str10 == null || str10.equals(str5)) && (((str = r7.A03) == null || str.equals(str6)) && (((str2 = r7.A01) == null || str2.equals(str7)) && (((str3 = r7.A02) == null || str3.equals(str8)) && ((str4 = r7.A00) == null || str4.startsWith(str9) || str4.compareTo(str9) >= 0))))) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }

    public C08010eX(Context context, C08030eZ r7) {
        this.A02 = context.getApplicationContext();
        this.A07 = r7;
        C26161b0 A002 = C26161b0.A00();
        this.A06 = new C08040ea(Integer.MIN_VALUE, r7.A01(), A002);
        this.A05 = C08080ee.A01;
        this.A00 = new C08110eh(context, new SparseArray());
        this.A03 = new Handler(this.A07.A01.getLooper());
        this.A00 = r7.A01();
        Looper looper = this.A07.A01.getLooper();
        C26191b3 r2 = this.A00;
        synchronized (C08130ej.class) {
            if (C08130ej.A01 == null) {
                C08130ej.A01 = new C08130ej(looper, r2);
            }
        }
        this.A04 = new C26321bG(A002);
    }
}
