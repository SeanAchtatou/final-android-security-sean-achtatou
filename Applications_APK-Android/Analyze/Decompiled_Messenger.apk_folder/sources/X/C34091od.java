package X;

/* renamed from: X.1od  reason: invalid class name and case insensitive filesystem */
public final class C34091od extends C34071ob {
    private final boolean A00;

    public String toString() {
        if (this.A00) {
            return "snapdragon_v2_auto";
        }
        return "snapdragon_v2";
    }

    public C34091od(C26311bF r1, boolean z) {
        super(r1);
        this.A00 = z;
    }
}
