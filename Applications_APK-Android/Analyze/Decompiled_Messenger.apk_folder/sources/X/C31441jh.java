package X;

import java.util.ArrayList;

/* renamed from: X.1jh  reason: invalid class name and case insensitive filesystem */
public abstract class C31441jh extends C17760zQ {
    public C17740zO A00;
    public C17690zJ A01;
    public C70173aD A02;
    public C70173aD A03;
    public AnonymousClass1R9 A04 = C17760zQ.A01;
    public ArrayList A05 = new ArrayList();

    public void A00() {
        C17690zJ r2 = this.A01;
        if (r2 != null) {
            this.A05.add(new C17680zI(new C31431jg(this.A00, r2), this.A04, this.A02, this.A03, null));
            this.A01 = null;
            this.A04 = C17760zQ.A01;
            this.A02 = null;
            this.A03 = null;
        }
    }
}
