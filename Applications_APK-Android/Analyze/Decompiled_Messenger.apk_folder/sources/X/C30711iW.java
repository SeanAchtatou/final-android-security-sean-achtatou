package X;

/* renamed from: X.1iW  reason: invalid class name and case insensitive filesystem */
public final class C30711iW implements C22761Ms {
    public final /* synthetic */ C22781Mu A00;

    public C30711iW(C22781Mu r1) {
        this.A00 = r1;
    }

    public AnonymousClass1S9 AWg(AnonymousClass1NY r4, int i, C33271nJ r6, C23541Px r7) {
        C22761Ms r0;
        AnonymousClass1NY.A05(r4);
        AnonymousClass1O3 r1 = r4.A07;
        if (r1 == AnonymousClass1SI.A06) {
            return this.A00.A03(r4, i, r6, r7);
        }
        if (r1 == AnonymousClass1SI.A03) {
            C22781Mu r2 = this.A00;
            AnonymousClass1NY.A05(r4);
            if (r4.A05 != -1) {
                AnonymousClass1NY.A05(r4);
                if (r4.A01 != -1) {
                    if (r7.A08 || (r0 = r2.A00) == null) {
                        return r2.A02(r4, r7);
                    }
                    return r0.AWg(r4, i, r6, r7);
                }
            }
            throw new C37871wU("image width or height is incorrect", r4);
        } else if (r1 == AnonymousClass1SI.A08) {
            return this.A00.A01.AWg(r4, i, r6, r7);
        } else {
            if (r1 != AnonymousClass1O3.A02) {
                return this.A00.A02(r4, r7);
            }
            throw new C37871wU("unknown image format", r4);
        }
    }
}
