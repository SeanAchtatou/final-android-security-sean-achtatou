package X;

import android.util.SparseArray;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1A2  reason: invalid class name */
public final class AnonymousClass1A2 {
    private int A00;
    public final Map A01 = new HashMap();
    public final int A02;
    public final SparseArray A03 = new SparseArray();
    public final boolean A04;

    public AnonymousClass1A2(boolean z, int i) {
        this.A04 = z;
        this.A02 = i;
        this.A00 = i + 1;
    }

    public void A00(C21681Ih r5) {
        int i;
        if (r5.C2K()) {
            boolean z = this.A04;
            if (z && !r5.BBN()) {
                throw new IllegalStateException("If you enable custom viewTypes, you must provide a customViewType in ViewRenderInfo.");
            } else if (!z && r5.BBN()) {
                throw new IllegalStateException("You must enable custom viewTypes to provide customViewType in ViewRenderInfo.");
            } else if (!z || this.A02 != r5.B9K()) {
                C37481vk B9A = r5.B9A();
                if (this.A01.containsKey(B9A)) {
                    i = ((Integer) this.A01.get(B9A)).intValue();
                } else {
                    if (r5.BBN()) {
                        i = r5.B9K();
                    } else {
                        i = this.A00;
                        this.A00 = i + 1;
                    }
                    this.A03.put(i, B9A);
                    this.A01.put(B9A, Integer.valueOf(i));
                }
                if (!r5.BBN()) {
                    r5.CDA(i);
                }
            } else {
                throw new IllegalStateException("CustomViewType cannot be the same as ComponentViewType.");
            }
        }
    }
}
