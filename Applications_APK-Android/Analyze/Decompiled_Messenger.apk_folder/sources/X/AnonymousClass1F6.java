package X;

import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.interstitial.api.FQLFetchInterstitialResult;
import com.facebook.interstitial.api.GraphQLInterstitialsResult;
import com.google.common.collect.ImmutableSet;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1F6  reason: invalid class name */
public final class AnonymousClass1F6 implements AnonymousClass0lE {
    private static volatile AnonymousClass1F6 A01;
    public AnonymousClass0UN A00;

    public C70683b7 A03(String str, String str2, int i) {
        Parcelable parcelable;
        if (i == 0) {
            JSONObject jSONObject = new JSONObject(str2);
            int i2 = jSONObject.getInt("rank");
            String optString = jSONObject.optString("nux_id");
            long j = jSONObject.getLong("fetchTimeMs");
            if (!TextUtils.isEmpty(jSONObject.optString("nux_data"))) {
                parcelable = A02(jSONObject.getString("nux_data"), str);
            } else {
                parcelable = null;
            }
            return new FQLFetchInterstitialResult(i2, optString, parcelable, j);
        } else if (i != 1) {
            return null;
        } else {
            try {
                JSONObject jSONObject2 = new JSONObject(str2);
                return new GraphQLInterstitialsResult(null, jSONObject2.optLong("fetchTimeMs", 0), jSONObject2.optBoolean("valid", false), jSONObject2.optString("nuxId", null), jSONObject2.optInt("rank", 0), jSONObject2.optInt("maxViews", 0), jSONObject2.optString("tree_model", null));
            } catch (Exception e) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A00)).softReport("InterstitialRepository", "Failed to de-serialize interstitial data", e);
                return null;
            }
        }
    }

    public static final AnonymousClass1F6 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1F6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1F6(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static synchronized void A01(C30281hn r3) {
        synchronized (AnonymousClass1F6.class) {
            Class<AnonymousClass1F6> cls = AnonymousClass1F6.class;
            synchronized (cls) {
                r3.C24(C21001Eq.A03);
                r3.C24(C21001Eq.A05);
            }
            synchronized (cls) {
                r3.C24(C21001Eq.A07);
                r3.C1B(C21001Eq.A01);
            }
        }
    }

    public Parcelable A02(String str, String str2) {
        C32821mO A04 = ((C31511jo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQQ, this.A00)).A04(str2);
        Class cls = null;
        if (A04 == null) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A00)).CGS("InterstitialRepository", AnonymousClass08S.A0S("No controller available for nux_id ", str2, " and nux_type ", BuildConfig.FLAVOR));
        } else if (A04 instanceof C22081Jv) {
            cls = ((C22081Jv) A04).AmL();
        }
        if (cls != null) {
            try {
                return (Parcelable) ((AnonymousClass0jJ) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AmL, this.A00)).readValue(str, cls);
            } catch (Exception e) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Amr, this.A00)).softReport("InterstitialRepository", AnonymousClass08S.A0J("Failed to parse data for interstitial ", str2), e);
            }
        }
        return null;
    }

    public ImmutableSet Avs() {
        return ImmutableSet.A05(C21001Eq.A0A, C21001Eq.A09);
    }

    private AnonymousClass1F6(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
    }
}
