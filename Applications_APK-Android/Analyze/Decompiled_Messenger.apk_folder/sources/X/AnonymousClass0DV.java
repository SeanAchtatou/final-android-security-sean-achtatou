package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0DV  reason: invalid class name */
public final class AnonymousClass0DV extends C03160Kg {
    public long A00() {
        return -5544646103548483595L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        AnonymousClass0FP r32 = (AnonymousClass0FP) r3;
        dataOutput.writeLong(r32.cameraOpenTimeMs);
        dataOutput.writeLong(r32.cameraPreviewTimeMs);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        AnonymousClass0FP r32 = (AnonymousClass0FP) r3;
        r32.cameraOpenTimeMs = dataInput.readLong();
        r32.cameraPreviewTimeMs = dataInput.readLong();
        return true;
    }
}
