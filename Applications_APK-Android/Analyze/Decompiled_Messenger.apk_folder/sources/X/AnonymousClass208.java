package X;

import android.graphics.drawable.Drawable;
import com.google.common.base.Objects;
import java.util.Arrays;

/* renamed from: X.208  reason: invalid class name */
public final class AnonymousClass208 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final Drawable A07;

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AnonymousClass208)) {
            return false;
        }
        AnonymousClass208 r4 = (AnonymousClass208) obj;
        return Objects.equal(this.A07, r4.A07) && this.A02 == r4.A02 && this.A03 == r4.A03 && this.A00 == r4.A00 && this.A04 == r4.A04 && this.A06 == r4.A06 && this.A05 == r4.A05 && this.A01 == r4.A01;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A07, Integer.valueOf(this.A02), Integer.valueOf(this.A03), Integer.valueOf(this.A00), Integer.valueOf(this.A04), Integer.valueOf(this.A06), Integer.valueOf(this.A05), Integer.valueOf(this.A01)});
    }

    public AnonymousClass208(Drawable drawable, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.A07 = drawable;
        this.A02 = i;
        this.A03 = i2;
        this.A00 = i3;
        this.A04 = i4;
        this.A06 = i5;
        this.A05 = i6;
        this.A01 = i7;
    }
}
