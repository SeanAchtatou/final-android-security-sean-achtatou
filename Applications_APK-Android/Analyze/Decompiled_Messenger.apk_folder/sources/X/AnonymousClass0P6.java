package X;

/* renamed from: X.0P6  reason: invalid class name */
public final class AnonymousClass0P6 extends C02020Cn {
    public /* bridge */ /* synthetic */ Object A00() {
        return (AnonymousClass0CK) super.A00();
    }

    public /* bridge */ /* synthetic */ Object A01() {
        return (AnonymousClass0Rv) super.A01();
    }

    public AnonymousClass0CK A02() {
        return (AnonymousClass0CK) super.A00();
    }

    public AnonymousClass0Rv A03() {
        return (AnonymousClass0Rv) super.A01();
    }

    public String toString() {
        return AnonymousClass08S.A0P(super.toString(), " ", ((AnonymousClass0CK) super.A00()).toString());
    }

    public AnonymousClass0P6(C01990Ck r1, AnonymousClass0Rv r2, AnonymousClass0CK r3) {
        super(r1, r2, r3);
    }
}
