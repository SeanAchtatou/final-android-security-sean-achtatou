package X;

import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.SparseArray;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0Dt  reason: invalid class name and case insensitive filesystem */
public final class C02250Dt {
    public static int A04 = -1;
    public static final ThreadLocal A05 = new C02260Du();
    public static final Comparator A06 = new C02270Dv();
    public static final AtomicInteger A07 = new AtomicInteger(1);
    public int A00;
    public int A01;
    public C02280Dw[] A02 = new C02280Dw[100];
    public final SparseArray A03 = new SparseArray();

    public static void A00(C02250Dt r6, C02280Dw r7) {
        int i = r6.A01 << 3;
        int i2 = r6.A00;
        if (i > i2) {
            int i3 = 0;
            for (int i4 = 0; i4 < i2; i4++) {
                C02280Dw[] r1 = r6.A02;
                C02280Dw r0 = r1[i4];
                if (r0 != null) {
                    r1[i3] = r0;
                    i3++;
                }
            }
            r6.A00 = i3;
            r6.A01 = 0;
        }
        int i5 = r6.A00;
        C02280Dw[] r12 = r6.A02;
        int length = r12.length;
        if (i5 >= length) {
            r6.A02 = (C02280Dw[]) Arrays.copyOf(r12, (length << 1) + 1);
        }
        C02280Dw[] r2 = r6.A02;
        int i6 = r6.A00;
        r6.A00 = i6 + 1;
        r2[i6] = r7;
    }

    public void A02(int i, int i2, String str) {
        List subList = Arrays.asList(this.A02).subList(0, this.A00);
        SparseArray sparseArray = this.A03;
        int i3 = i2;
        if (C010708t.A0X(i3)) {
            List list = subList;
            StringBuilder sb = new StringBuilder();
            long A002 = C02300Dy.A00();
            long j = -1;
            long j2 = -1;
            boolean z = false;
            int i4 = 0;
            int i5 = 0;
            while (i5 < subList.size()) {
                C02280Dw r2 = (C02280Dw) list.get(i5);
                C02280Dw r19 = r2;
                if (r2 != null) {
                    if (!z) {
                        if (r2.A00 == i) {
                            r2 = r19;
                            j = r2.A01;
                            z = true;
                        }
                    }
                    Integer num = r2.A05;
                    Integer num2 = num;
                    if (num != AnonymousClass07B.A0i) {
                        if (num == AnonymousClass07B.A01 || num == AnonymousClass07B.A0N) {
                            if (i4 == 0) {
                                C010708t.A05(AnonymousClass0OV.A00, "Trace contains a stop event without a corresponding start: " + list);
                            } else {
                                i4--;
                            }
                        }
                        sb.append(" ");
                        C02280Dw r7 = r19;
                        if (j2 == -1) {
                            sb.append("-----");
                        } else {
                            sb.append(C02280Dw.A01((r7.A01 - j2) / 1000000));
                        }
                        sb.append(" ");
                        sb.append(C02280Dw.A00((r7.A01 - j) / 1000000));
                        Integer num3 = r7.A05;
                        if (num3 == AnonymousClass07B.A00) {
                            sb.append(" Start    ...     ...   ");
                        } else if (num3 == AnonymousClass07B.A0C) {
                            sb.append(" AStart   ...     ...   ");
                        } else if (num3 == AnonymousClass07B.A01 || num3 == AnonymousClass07B.A0N) {
                            sb.append(" Done ");
                            sb.append(C02280Dw.A01((r7.A01 - r7.A02) / 1000000));
                            sb.append(" ms ");
                            sb.append(C02280Dw.A01(r7.A03 - r7.A04));
                            sb.append(" ms ");
                        } else if (num3 != AnonymousClass07B.A0i) {
                            sb.append(" Comment  ...     ...   ");
                        }
                        for (int i6 = 0; i6 < i4; i6++) {
                            sb.append("|  ");
                        }
                        sb.append(r7.A02());
                        sb.append(" ");
                        j2 = r7.A01;
                        sb.append("\n");
                        Integer num4 = num2;
                        if (num4 == AnonymousClass07B.A00 || num4 == AnonymousClass07B.A0C) {
                            i4++;
                        }
                    }
                }
                i5++;
                list = subList;
            }
            if (sparseArray.size() != 0) {
                sb.append(" Unstopped timers:\n");
                int size = sparseArray.size();
                for (int i7 = 0; i7 < size; i7++) {
                    C02280Dw r1 = (C02280Dw) sparseArray.valueAt(i7);
                    long j3 = r1.A01;
                    sb.append("  ");
                    sb.append(r1);
                    sb.append(" (");
                    sb.append((A002 - j3) / 1000000);
                    sb.append(" ms, started at ");
                    sb.append(C02280Dw.A00(j3 / 1000000));
                    sb.append("\n");
                }
            }
            String sb2 = sb.toString();
            if (!TextUtils.isEmpty(sb2)) {
                String str2 = str;
                if (sb2.length() > 4000) {
                    String[] split = sb2.split("\n");
                    StringBuilder sb3 = new StringBuilder();
                    AnonymousClass0OV.A00(sb3, 0, null, null);
                    int length = sb3.length();
                    int i8 = 0;
                    int i9 = 0;
                    while (i8 < split.length) {
                        String str3 = null;
                        if (i8 != 0) {
                            str3 = split[i8 - 1];
                        }
                        String str4 = split[i8];
                        if (length == 0 || str4.length() + length < 4000) {
                            sb3.append(str4);
                            sb3.append("\n");
                            length += str4.length() + 1;
                            i8++;
                        } else {
                            C010708t.A02(i3, str2, sb3.toString());
                            sb3.setLength(0);
                            i9++;
                            AnonymousClass0OV.A00(sb3, i9, str3, str4);
                            length = sb3.length();
                        }
                    }
                    if (length > 0) {
                        C010708t.A02(i3, str2, sb3.toString());
                        return;
                    }
                    return;
                }
                StringBuilder sb4 = new StringBuilder();
                AnonymousClass0OV.A00(sb4, 0, null, null);
                sb4.append(sb2);
                C010708t.A02(i3, str2, sb4.toString());
            }
        }
    }

    public long A01(int i, long j, boolean z) {
        long j2;
        Integer num;
        long A002 = C02300Dy.A00();
        if (!z) {
            j2 = 3000000;
        } else {
            j2 = j * 1000000;
        }
        int indexOfKey = this.A03.indexOfKey(i);
        if (indexOfKey < 0) {
            return -1;
        }
        C02280Dw r2 = (C02280Dw) this.A03.valueAt(indexOfKey);
        if (Build.VERSION.SDK_INT >= 11) {
            this.A03.removeAt(indexOfKey);
        } else {
            this.A03.remove(i);
        }
        long j3 = r2.A01;
        long j4 = A002 - j3;
        if (j4 < j2) {
            int i2 = this.A00 - 1;
            while (true) {
                if (i2 < 0) {
                    break;
                }
                C02280Dw[] r1 = this.A02;
                if (r1[i2] == r2) {
                    r1[i2] = null;
                    this.A01++;
                    C02280Dw.A08.A02(r2);
                    break;
                }
                i2--;
            }
            return j4;
        }
        if (r2.A05 == AnonymousClass07B.A00) {
            num = AnonymousClass07B.A01;
        } else {
            num = AnonymousClass07B.A0N;
        }
        int i3 = r2.A00;
        String str = r2.A06;
        Object[] objArr = r2.A07;
        long j5 = r2.A03;
        C02280Dw r22 = (C02280Dw) C02280Dw.A08.A01();
        r22.A05 = num;
        r22.A00 = i3;
        r22.A07 = objArr;
        r22.A06 = str;
        r22.A03 = SystemClock.currentThreadTimeMillis();
        r22.A01 = C02300Dy.A00();
        r22.A02 = j3;
        r22.A04 = j5;
        A00(this, r22);
        return j4;
    }
}
