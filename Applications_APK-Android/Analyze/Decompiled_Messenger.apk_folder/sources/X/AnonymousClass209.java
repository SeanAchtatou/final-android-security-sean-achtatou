package X;

/* renamed from: X.209  reason: invalid class name */
public final class AnonymousClass209 {
    public final int A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    public AnonymousClass209(String str, String str2, String str3, String str4, int i, String str5) {
        this.A03 = str;
        this.A02 = str2;
        this.A04 = str3;
        this.A05 = str4;
        this.A00 = i;
        this.A01 = str5;
    }
}
