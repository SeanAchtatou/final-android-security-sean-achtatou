package X;

/* renamed from: X.1NB  reason: invalid class name */
public final class AnonymousClass1NB {
    public static volatile AnonymousClass1NC A00;

    private static AnonymousClass1NC A00() {
        if (A00 == null) {
            synchronized (AnonymousClass1NB.class) {
                if (A00 == null) {
                    A00 = new AnonymousClass1ND();
                }
            }
        }
        return A00;
    }

    public static void A01() {
        A00().AYV();
    }

    public static void A02(String str) {
        A00().APm(str);
    }

    public static boolean A03() {
        return A00().BHG();
    }

    private AnonymousClass1NB() {
    }
}
