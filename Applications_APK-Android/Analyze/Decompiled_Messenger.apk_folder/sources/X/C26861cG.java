package X;

import com.fasterxml.jackson.databind.JsonSerializer;

/* renamed from: X.1cG  reason: invalid class name and case insensitive filesystem */
public abstract class C26861cG {
    public abstract JsonSerializer createKeySerializer(C10450k8 r1, C10030jR r2, JsonSerializer jsonSerializer);

    public abstract JsonSerializer createSerializer(C11260mU r1, C10030jR r2);

    public abstract CY1 createTypeSerializer(C10450k8 r1, C10030jR r2);

    public abstract C26861cG withAdditionalSerializers(C11730nr r1);

    public abstract C26861cG withSerializerModifier(C11740ns r1);
}
