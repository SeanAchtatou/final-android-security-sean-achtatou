package X;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* renamed from: X.0e7  reason: invalid class name and case insensitive filesystem */
public abstract class C07760e7 extends WeakReference implements C07740e5 {
    public final C07740e5 A00;
    public final int A01;

    public int AoV() {
        return this.A01;
    }

    public C07740e5 Avg() {
        return this.A00;
    }

    public C07760e7(ReferenceQueue referenceQueue, Object obj, int i, C07740e5 r4) {
        super(obj, referenceQueue);
        this.A01 = i;
        this.A00 = r4;
    }

    public Object getKey() {
        return get();
    }
}
