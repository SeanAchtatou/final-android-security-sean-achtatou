package X;

import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.SparseIntArray;
import android.util.TypedValue;
import java.lang.reflect.Method;

/* renamed from: X.0Xb  reason: invalid class name and case insensitive filesystem */
public abstract class C04960Xb {
    public static final Method A04;
    public int A00;
    public SparseIntArray A01;
    public SparseIntArray A02;
    public final C25831aT A03;

    static {
        Method method = null;
        if (Build.VERSION.SDK_INT < 28) {
            try {
                method = AssetManager.class.getDeclaredMethod("loadResourceValue", Integer.TYPE, Short.TYPE, TypedValue.class, Boolean.TYPE);
                method.setAccessible(true);
            } catch (NoSuchMethodException e) {
                AnonymousClass02w.A05(C04960Xb.class, "Unable to find loadResourceValue method with reflection", e);
            }
        }
        A04 = method;
    }

    public static Drawable.ConstantState A01(Drawable drawable, int i, boolean z) {
        if (drawable != null) {
            return drawable.getConstantState();
        }
        if (!z) {
            return null;
        }
        throw new RuntimeException(AnonymousClass08S.A0J("Unable to inflate custom drawable with id: 0x", Integer.toHexString(i)));
    }

    private static SparseIntArray A02(TypedValue typedValue, Resources resources, int[] iArr) {
        int length;
        if (iArr == null || (length = iArr.length) == 0) {
            return new SparseIntArray();
        }
        SparseIntArray sparseIntArray = new SparseIntArray(length);
        for (int i : iArr) {
            if (A03(typedValue, resources, i)) {
                sparseIntArray.put(typedValue.data, i);
            }
        }
        if (sparseIntArray.size() != 0) {
            return sparseIntArray;
        }
        throw new IllegalStateException("Could not initialize any custom drawables");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    public static boolean A03(TypedValue typedValue, Resources resources, int i) {
        Method method = A04;
        if (method != null) {
            Class<C04960Xb> cls = C04960Xb.class;
            try {
                if (((Integer) method.invoke(resources.getAssets(), Integer.valueOf(i), (short) 0, typedValue, false)).intValue() >= 0) {
                    return true;
                }
                AnonymousClass02w.A01(cls, AnonymousClass08S.A0J("Custom resource not found #0x", Integer.toHexString(i)));
                return false;
            } catch (Exception e) {
                AnonymousClass02w.A05(cls, "Unable to invoke loadResourceValue method with reflection", e);
                return false;
            }
        } else {
            try {
                resources.getValue(i, typedValue, false);
                return true;
            } catch (Resources.NotFoundException e2) {
                AnonymousClass02w.A05(C04960Xb.class, AnonymousClass08S.A0J("Unable to getValue for resource #0x", Integer.toHexString(i)), e2);
                return false;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007c, code lost:
        if (r4 != null) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x0081 */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C04960Xb(android.content.res.Resources r9, int[] r10, int[] r11) {
        /*
            r8 = this;
            r8.<init>()
            android.content.res.AssetManager r1 = r9.getAssets()
            java.lang.String r0 = "drawables.bin"
            java.io.InputStream r4 = r1.open(r0)     // Catch:{ Exception -> 0x0082 }
            int r1 = r4.available()     // Catch:{ all -> 0x0079 }
            r0 = 8
            if (r1 < r0) goto L_0x0071
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0079 }
            r4.read(r0)     // Catch:{ all -> 0x0079 }
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r0)     // Catch:{ all -> 0x0079 }
            int r7 = r0.getInt()     // Catch:{ all -> 0x0079 }
            int r6 = r0.getInt()     // Catch:{ all -> 0x0079 }
            int r0 = r7 + r6
            int r2 = r0 << 3
            byte[] r1 = X.AnonymousClass0h4.A01(r4, r2)     // Catch:{ all -> 0x0079 }
            int r0 = r1.length     // Catch:{ all -> 0x0079 }
            if (r0 != r2) goto L_0x0069
            java.nio.ByteBuffer r5 = java.nio.ByteBuffer.wrap(r1)     // Catch:{ all -> 0x0079 }
            android.util.SparseIntArray r3 = new android.util.SparseIntArray     // Catch:{ all -> 0x0079 }
            r3.<init>(r7)     // Catch:{ all -> 0x0079 }
            r2 = 0
        L_0x003b:
            if (r2 >= r7) goto L_0x004b
            int r1 = r5.getInt()     // Catch:{ all -> 0x0079 }
            int r0 = r5.getInt()     // Catch:{ all -> 0x0079 }
            r3.append(r1, r0)     // Catch:{ all -> 0x0079 }
            int r2 = r2 + 1
            goto L_0x003b
        L_0x004b:
            r8.A02 = r3     // Catch:{ all -> 0x0079 }
            android.util.SparseIntArray r3 = new android.util.SparseIntArray     // Catch:{ all -> 0x0079 }
            r3.<init>(r6)     // Catch:{ all -> 0x0079 }
            r2 = 0
        L_0x0053:
            if (r2 >= r6) goto L_0x0063
            int r1 = r5.getInt()     // Catch:{ all -> 0x0079 }
            int r0 = r5.getInt()     // Catch:{ all -> 0x0079 }
            r3.append(r1, r0)     // Catch:{ all -> 0x0079 }
            int r2 = r2 + 1
            goto L_0x0053
        L_0x0063:
            r8.A01 = r3     // Catch:{ all -> 0x0079 }
            r4.close()     // Catch:{ Exception -> 0x0082 }
            goto L_0x0082
        L_0x0069:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0079 }
            java.lang.String r0 = "Custom drawable file missing entries"
            r1.<init>(r0)     // Catch:{ all -> 0x0079 }
            goto L_0x0078
        L_0x0071:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0079 }
            java.lang.String r0 = "Invalid custom drawables file"
            r1.<init>(r0)     // Catch:{ all -> 0x0079 }
        L_0x0078:
            throw r1     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x007b }
        L_0x007b:
            r0 = move-exception
            if (r4 == 0) goto L_0x0081
            r4.close()     // Catch:{ all -> 0x0081 }
        L_0x0081:
            throw r0     // Catch:{ Exception -> 0x0082 }
        L_0x0082:
            android.util.TypedValue r4 = new android.util.TypedValue
            r4.<init>()
            android.util.SparseIntArray r0 = r8.A02
            r1 = 0
            if (r0 == 0) goto L_0x0092
            int r0 = r0.size()
            if (r0 != 0) goto L_0x00c9
        L_0x0092:
            android.util.SparseIntArray r0 = A02(r4, r9, r10)
            r8.A02 = r0
            int r0 = r0.size()
            if (r0 == 0) goto L_0x00c9
            if (r10 == 0) goto L_0x00c9
            X.1aT r0 = new X.1aT
            r0.<init>(r9, r10)
            r8.A03 = r0
        L_0x00a7:
            android.util.SparseIntArray r0 = r8.A01
            if (r0 == 0) goto L_0x00b1
            int r0 = r0.size()
            if (r0 != 0) goto L_0x00b7
        L_0x00b1:
            android.util.SparseIntArray r0 = A02(r4, r9, r11)
            r8.A01 = r0
        L_0x00b7:
            r3 = 0
            if (r10 == 0) goto L_0x00cc
            int r2 = r10.length
            r1 = 0
        L_0x00bc:
            if (r1 >= r2) goto L_0x00cc
            r0 = r10[r1]
            boolean r0 = A03(r4, r9, r0)
            if (r0 != 0) goto L_0x00dc
            int r1 = r1 + 1
            goto L_0x00bc
        L_0x00c9:
            r8.A03 = r1
            goto L_0x00a7
        L_0x00cc:
            if (r11 == 0) goto L_0x00e1
            int r1 = r11.length
        L_0x00cf:
            if (r3 >= r1) goto L_0x00e1
            r0 = r11[r3]
            boolean r0 = A03(r4, r9, r0)
            if (r0 != 0) goto L_0x00dc
            int r3 = r3 + 1
            goto L_0x00cf
        L_0x00dc:
            int r0 = r4.assetCookie
            r8.A00 = r0
            return
        L_0x00e1:
            java.lang.Class<X.0Xb> r1 = X.C04960Xb.class
            java.lang.String r0 = "Unable to set asset cookie"
            X.AnonymousClass02w.A02(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04960Xb.<init>(android.content.res.Resources, int[], int[]):void");
    }

    public static int A00(Resources resources) {
        int i;
        Configuration configuration = resources.getConfiguration();
        int i2 = ((AnonymousClass1Y3.A4R + configuration.orientation) * 31) + configuration.screenLayout;
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 26) {
            i2 = (i2 * 31) + configuration.colorMode;
        }
        int i4 = ((i2 * 31) + configuration.uiMode) * 31;
        if (i3 < 17) {
            i = resources.getDisplayMetrics().densityDpi;
        } else {
            i = configuration.densityDpi;
        }
        return i4 + i;
    }
}
