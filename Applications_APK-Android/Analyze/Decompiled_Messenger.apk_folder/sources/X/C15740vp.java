package X;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.common.dextricks.DexStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0vp  reason: invalid class name and case insensitive filesystem */
public final class C15740vp {
    public int A00;
    public int A01;
    public C15680vg A02;
    public ArrayList A03 = null;
    public final ArrayList A04;
    public final ArrayList A05 = new ArrayList();
    public final List A06;
    public final /* synthetic */ RecyclerView A07;

    public View A04(int i) {
        return A05(i, false, Long.MAX_VALUE).A0H;
    }

    public C15740vp(RecyclerView recyclerView) {
        this.A07 = recyclerView;
        ArrayList arrayList = new ArrayList();
        this.A04 = arrayList;
        this.A06 = Collections.unmodifiableList(arrayList);
        this.A01 = 2;
        this.A00 = 2;
    }

    public static void A01(C15740vp r2) {
        for (int size = r2.A05.size() - 1; size >= 0; size--) {
            A02(r2, size);
        }
        r2.A05.clear();
        if (RecyclerView.A1C) {
            C37121uj r22 = r2.A07.A0I;
            int[] iArr = r22.A03;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            r22.A00 = 0;
        }
    }

    public static void A02(C15740vp r2, int i) {
        r2.A0B((C33781o8) r2.A05.get(i), true);
        r2.A05.remove(i);
    }

    public int A03(int i) {
        if (i >= 0) {
            RecyclerView recyclerView = this.A07;
            C15930wD r1 = recyclerView.A0y;
            if (i < r1.A00()) {
                if (!r1.A08) {
                    return i;
                }
                return C16150wZ.A01(recyclerView.A0G, i, 0);
            }
        }
        RecyclerView recyclerView2 = this.A07;
        throw new IndexOutOfBoundsException(AnonymousClass08S.A0C("invalid position ", i, ". State item count is ", recyclerView2.A0y.A00(), recyclerView2.A0d()));
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x019b, code lost:
        if (r1 < 0) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
        if (r9 != null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x0493, code lost:
        if (r9.A0D() == false) goto L_0x044c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x04c6, code lost:
        if (r1 < 0) goto L_0x04c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x04c9, code lost:
        if (r0 == false) goto L_0x044c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x054c, code lost:
        if (r1 == false) goto L_0x054e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x0455  */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x046d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C33781o8 A05(int r15, boolean r16, long r17) {
        /*
            r14 = this;
            if (r15 < 0) goto L_0x05e8
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wD r1 = r0.A0y
            int r0 = r1.A00()
            if (r15 >= r0) goto L_0x05e8
            boolean r0 = r1.A08
            r5 = 0
            r3 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0369
            java.util.ArrayList r0 = r14.A03
            if (r0 == 0) goto L_0x0366
            int r11 = r0.size()
            if (r11 == 0) goto L_0x0366
            r10 = 0
            r6 = 0
        L_0x001f:
            r8 = 32
            if (r6 >= r11) goto L_0x0320
            java.util.ArrayList r0 = r14.A03
            java.lang.Object r9 = r0.get(r6)
            X.1o8 r9 = (X.C33781o8) r9
            int r1 = r9.A00
            r7 = r1 & 32
            r0 = 0
            if (r7 == 0) goto L_0x0033
            r0 = 1
        L_0x0033:
            if (r0 != 0) goto L_0x031c
            int r0 = r9.A05()
            if (r0 != r15) goto L_0x031c
            r8 = r8 | r1
            r9.A00 = r8
        L_0x003e:
            r13 = 1
            if (r9 != 0) goto L_0x0042
        L_0x0041:
            r13 = 0
        L_0x0042:
            if (r9 != 0) goto L_0x00af
            java.util.ArrayList r0 = r14.A04
            int r8 = r0.size()
            r1 = 0
            r7 = 0
        L_0x004c:
            if (r7 >= r8) goto L_0x025b
            java.util.ArrayList r0 = r14.A04
            java.lang.Object r9 = r0.get(r7)
            X.1o8 r9 = (X.C33781o8) r9
            int r6 = r9.A00
            r10 = r6 & 32
            r0 = 0
            if (r10 == 0) goto L_0x005e
            r0 = 1
        L_0x005e:
            if (r0 != 0) goto L_0x0257
            int r0 = r9.A05()
            if (r0 != r15) goto L_0x0257
            boolean r0 = r9.A0D()
            if (r0 != 0) goto L_0x0257
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wD r0 = r0.A0y
            boolean r0 = r0.A08
            if (r0 != 0) goto L_0x007a
            boolean r0 = r9.A0E()
            if (r0 != 0) goto L_0x0257
        L_0x007a:
            r0 = 32
            r0 = r0 | r6
            r9.A00 = r0
        L_0x007f:
            if (r9 == 0) goto L_0x00af
            boolean r0 = r9.A0E()
            if (r0 == 0) goto L_0x0219
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wD r0 = r0.A0y
            boolean r10 = r0.A08
        L_0x008d:
            if (r10 != 0) goto L_0x0216
            if (r16 != 0) goto L_0x00ae
            r6 = 4
            int r0 = r9.A00
            r6 = r6 | r0
            r9.A00 = r6
            X.0vp r1 = r9.A08
            r0 = 0
            if (r1 == 0) goto L_0x009d
            r0 = 1
        L_0x009d:
            if (r0 == 0) goto L_0x0208
            androidx.recyclerview.widget.RecyclerView r1 = r14.A07
            android.view.View r0 = r9.A0H
            r1.removeDetachedView(r0, r2)
            X.0vp r0 = r9.A08
            r0.A0A(r9)
        L_0x00ab:
            r14.A09(r9)
        L_0x00ae:
            r9 = r5
        L_0x00af:
            if (r9 != 0) goto L_0x0405
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wZ r1 = r0.A0G
            int r7 = X.C16150wZ.A01(r1, r15, r2)
            if (r7 < 0) goto L_0x0397
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r0 = r0.A0J
            int r0 = r0.ArU()
            if (r7 >= r0) goto L_0x0397
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r0 = r0.A0J
            int r6 = r0.getItemViewType(r7)
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r1 = r0.A0J
            boolean r0 = r1.hasStableIds()
            if (r0 == 0) goto L_0x0123
            long r11 = r1.getItemId(r7)
            java.util.ArrayList r0 = r14.A04
            int r0 = r0.size()
            int r8 = r0 + -1
        L_0x00e3:
            if (r8 < 0) goto L_0x01cf
            java.util.ArrayList r0 = r14.A04
            java.lang.Object r9 = r0.get(r8)
            X.1o8 r9 = (X.C33781o8) r9
            long r0 = r9.A07
            int r10 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r10 != 0) goto L_0x01cb
            int r1 = r9.A00
            r10 = r1 & 32
            r0 = 0
            if (r10 == 0) goto L_0x00fb
            r0 = 1
        L_0x00fb:
            if (r0 != 0) goto L_0x01cb
            int r0 = r9.A01
            if (r6 != r0) goto L_0x01aa
            r10 = 32
            r10 = r10 | r1
            r9.A00 = r10
            boolean r0 = r9.A0E()
            if (r0 == 0) goto L_0x011e
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wD r0 = r0.A0y
            boolean r0 = r0.A08
            if (r0 != 0) goto L_0x011e
            r8 = 2
            r1 = 14
            r0 = r1 ^ -1
            r10 = r10 & r0
            r8 = r8 & r1
            r8 = r8 | r10
            r9.A00 = r8
        L_0x011e:
            if (r9 == 0) goto L_0x0123
            r9.A04 = r7
            r13 = 1
        L_0x0123:
            if (r9 != 0) goto L_0x0175
            X.0vg r0 = r14.A02
            if (r0 != 0) goto L_0x0130
            X.0vg r0 = new X.0vg
            r0.<init>()
            r14.A02 = r0
        L_0x0130:
            X.0vg r0 = r14.A02
            android.util.SparseArray r0 = r0.A01
            java.lang.Object r1 = r0.get(r6)
            X.1R1 r1 = (X.AnonymousClass1R1) r1
            if (r1 == 0) goto L_0x01a7
            java.util.ArrayList r0 = r1.A02
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x01a7
            java.util.ArrayList r7 = r1.A02
            int r0 = r7.size()
            int r1 = r0 + -1
        L_0x014c:
            if (r1 < 0) goto L_0x01a7
            java.lang.Object r0 = r7.get(r1)
            X.1o8 r0 = (X.C33781o8) r0
            boolean r0 = r0.A0C()
            if (r0 != 0) goto L_0x01a4
            java.lang.Object r0 = r7.remove(r1)
            X.1o8 r0 = (X.C33781o8) r0
            r9 = r0
        L_0x0161:
            if (r0 == 0) goto L_0x0175
            r9.A07()
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A19
            if (r0 == 0) goto L_0x0175
            android.view.View r1 = r9.A0H
            boolean r0 = r1 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x0175
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            r14.A00(r1, r2)
        L_0x0175:
            if (r9 != 0) goto L_0x0405
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r0 == 0) goto L_0x01a1
            long r10 = java.lang.System.nanoTime()
        L_0x017f:
            r7 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            int r0 = (r17 > r7 ? 1 : (r17 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x036c
            X.0vg r0 = r14.A02
            X.1R1 r0 = X.C15680vg.A00(r0, r6)
            long r0 = r0.A01
            r8 = 0
            int r7 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r7 == 0) goto L_0x019d
            long r7 = r10 + r0
            int r1 = (r7 > r17 ? 1 : (r7 == r17 ? 0 : -1))
            r0 = 0
            if (r1 >= 0) goto L_0x019e
        L_0x019d:
            r0 = 1
        L_0x019e:
            if (r0 != 0) goto L_0x036c
            return r5
        L_0x01a1:
            r10 = 0
            goto L_0x017f
        L_0x01a4:
            int r1 = r1 + -1
            goto L_0x014c
        L_0x01a7:
            r0 = 0
            r9 = r5
            goto L_0x0161
        L_0x01aa:
            if (r16 != 0) goto L_0x01cb
            java.util.ArrayList r0 = r14.A04
            r0.remove(r8)
            androidx.recyclerview.widget.RecyclerView r10 = r14.A07
            android.view.View r1 = r9.A0H
            r10.removeDetachedView(r1, r2)
            android.view.View r0 = r9.A0H
            X.1o8 r1 = androidx.recyclerview.widget.RecyclerView.A05(r0)
            r1.A08 = r5
            r1.A0F = r2
            int r0 = r1.A00
            r0 = r0 & -33
            r1.A00 = r0
            r14.A09(r1)
        L_0x01cb:
            int r8 = r8 + -1
            goto L_0x00e3
        L_0x01cf:
            java.util.ArrayList r0 = r14.A05
            int r0 = r0.size()
            int r8 = r0 + -1
        L_0x01d7:
            if (r8 < 0) goto L_0x0205
            java.util.ArrayList r0 = r14.A05
            java.lang.Object r9 = r0.get(r8)
            X.1o8 r9 = (X.C33781o8) r9
            long r0 = r9.A07
            int r10 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r10 != 0) goto L_0x0202
            boolean r0 = r9.A0C()
            if (r0 != 0) goto L_0x0202
            int r0 = r9.A01
            if (r6 != r0) goto L_0x01fa
            if (r16 != 0) goto L_0x011e
            java.util.ArrayList r0 = r14.A05
            r0.remove(r8)
            goto L_0x011e
        L_0x01fa:
            if (r16 != 0) goto L_0x0202
            A02(r14, r8)
            r9 = r5
            goto L_0x011e
        L_0x0202:
            int r8 = r8 + -1
            goto L_0x01d7
        L_0x0205:
            r9 = r5
            goto L_0x011e
        L_0x0208:
            r1 = r6 & 32
            r0 = 0
            if (r1 == 0) goto L_0x020e
            r0 = 1
        L_0x020e:
            if (r0 == 0) goto L_0x00ab
            r0 = r6 & -33
            r9.A00 = r0
            goto L_0x00ab
        L_0x0216:
            r13 = 1
            goto L_0x00af
        L_0x0219:
            int r1 = r9.A04
            if (r1 < 0) goto L_0x0586
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r0 = r0.A0J
            int r0 = r0.ArU()
            if (r1 >= r0) goto L_0x0586
            androidx.recyclerview.widget.RecyclerView r1 = r14.A07
            X.0wD r0 = r1.A0y
            boolean r0 = r0.A08
            r10 = 0
            if (r0 != 0) goto L_0x023e
            X.1Dz r1 = r1.A0J
            int r0 = r9.A04
            int r1 = r1.getItemViewType(r0)
            int r0 = r9.A01
            if (r1 == r0) goto L_0x023e
            goto L_0x008d
        L_0x023e:
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r7 = r0.A0J
            boolean r0 = r7.hasStableIds()
            if (r0 == 0) goto L_0x0254
            long r0 = r9.A07
            int r6 = r9.A04
            long r7 = r7.getItemId(r6)
            int r6 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r6 != 0) goto L_0x008d
        L_0x0254:
            r10 = 1
            goto L_0x008d
        L_0x0257:
            int r7 = r7 + 1
            goto L_0x004c
        L_0x025b:
            if (r16 != 0) goto L_0x02eb
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wh r9 = r0.A0H
            java.util.List r0 = r9.A02
            int r8 = r0.size()
            r7 = 0
        L_0x0268:
            if (r7 >= r8) goto L_0x02e9
            java.util.List r0 = r9.A02
            java.lang.Object r6 = r0.get(r7)
            android.view.View r6 = (android.view.View) r6
            X.0wj r0 = r9.A01
            X.1o8 r10 = r0.Ah9(r6)
            int r0 = r10.A05()
            if (r0 != r15) goto L_0x02e6
            boolean r0 = r10.A0D()
            if (r0 != 0) goto L_0x02e6
            boolean r0 = r10.A0E()
            if (r0 != 0) goto L_0x02e6
        L_0x028a:
            if (r6 == 0) goto L_0x02eb
            X.1o8 r9 = androidx.recyclerview.widget.RecyclerView.A05(r6)
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wh r8 = r0.A0H
            X.0wj r0 = r8.A01
            int r7 = r0.BCT(r6)
            if (r7 < 0) goto L_0x05d4
            X.0wk r1 = r8.A00
            boolean r0 = r1.A06(r7)
            if (r0 == 0) goto L_0x05c0
            r1.A03(r7)
            X.C16220wh.A01(r8, r6)
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wh r1 = r0.A0H
            X.0wj r0 = r1.A01
            int r7 = r0.BCT(r6)
            r8 = -1
            if (r7 == r8) goto L_0x02c5
            X.0wk r1 = r1.A00
            boolean r0 = r1.A06(r7)
            if (r0 != 0) goto L_0x02c5
            int r0 = r1.A01(r7)
            int r7 = r7 - r0
            r8 = r7
        L_0x02c5:
            r0 = -1
            if (r8 == r0) goto L_0x05a3
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wh r7 = r0.A0H
            int r1 = X.C16220wh.A00(r7, r8)
            X.0wk r0 = r7.A00
            r0.A07(r1)
            X.0wj r0 = r7.A01
            r0.AXE(r1)
            r14.A08(r6)
            r1 = 8224(0x2020, float:1.1524E-41)
            int r0 = r9.A00
            r1 = r1 | r0
            r9.A00 = r1
            goto L_0x007f
        L_0x02e6:
            int r7 = r7 + 1
            goto L_0x0268
        L_0x02e9:
            r6 = r5
            goto L_0x028a
        L_0x02eb:
            java.util.ArrayList r0 = r14.A05
            int r6 = r0.size()
        L_0x02f1:
            if (r1 >= r6) goto L_0x0319
            java.util.ArrayList r0 = r14.A05
            java.lang.Object r9 = r0.get(r1)
            X.1o8 r9 = (X.C33781o8) r9
            boolean r0 = r9.A0D()
            if (r0 != 0) goto L_0x0316
            int r0 = r9.A05()
            if (r0 != r15) goto L_0x0316
            boolean r0 = r9.A0C()
            if (r0 != 0) goto L_0x0316
            if (r16 != 0) goto L_0x007f
            java.util.ArrayList r0 = r14.A05
            r0.remove(r1)
            goto L_0x007f
        L_0x0316:
            int r1 = r1 + 1
            goto L_0x02f1
        L_0x0319:
            r9 = r5
            goto L_0x007f
        L_0x031c:
            int r6 = r6 + 1
            goto L_0x001f
        L_0x0320:
            androidx.recyclerview.widget.RecyclerView r1 = r14.A07
            X.1Dz r0 = r1.A0J
            boolean r0 = r0.hasStableIds()
            if (r0 == 0) goto L_0x0366
            X.0wZ r1 = r1.A0G
            int r1 = X.C16150wZ.A01(r1, r15, r2)
            if (r1 <= 0) goto L_0x0366
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r0 = r0.A0J
            int r0 = r0.ArU()
            if (r1 >= r0) goto L_0x0366
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r0 = r0.A0J
            long r12 = r0.getItemId(r1)
        L_0x0344:
            if (r10 >= r11) goto L_0x0366
            java.util.ArrayList r0 = r14.A03
            java.lang.Object r9 = r0.get(r10)
            X.1o8 r9 = (X.C33781o8) r9
            int r7 = r9.A00
            r1 = r7 & 32
            r0 = 0
            if (r1 == 0) goto L_0x0356
            r0 = 1
        L_0x0356:
            if (r0 != 0) goto L_0x0363
            long r0 = r9.A07
            int r6 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r6 != 0) goto L_0x0363
            r8 = r8 | r7
            r9.A00 = r8
            goto L_0x003e
        L_0x0363:
            int r10 = r10 + 1
            goto L_0x0344
        L_0x0366:
            r9 = r5
            goto L_0x003e
        L_0x0369:
            r9 = r5
            goto L_0x0041
        L_0x036c:
            androidx.recyclerview.widget.RecyclerView r7 = r14.A07
            X.1Dz r5 = r7.A0J
            java.lang.String r1 = "RV CreateView"
            r0 = -227498666(0xfffffffff270a556, float:-4.766482E30)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ all -> 0x038f }
            X.1o8 r9 = r5.BV1(r7, r6)     // Catch:{ all -> 0x038f }
            android.view.View r0 = r9.A0H     // Catch:{ all -> 0x038f }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ all -> 0x038f }
            if (r0 != 0) goto L_0x0387
            r9.A01 = r6     // Catch:{ all -> 0x038f }
            goto L_0x03ca
        L_0x0387:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x038f }
            java.lang.String r0 = "ViewHolder views must not be attached when created. Ensure that you are not passing 'true' to the attachToRoot parameter of LayoutInflater.inflate(..., boolean attachToRoot)"
            r1.<init>(r0)     // Catch:{ all -> 0x038f }
            throw r1     // Catch:{ all -> 0x038f }
        L_0x038f:
            r1 = move-exception
            r0 = 1241124621(0x49fa0f0d, float:2048481.6)
            X.AnonymousClass06K.A00(r0)
            throw r1
        L_0x0397:
            java.lang.IndexOutOfBoundsException r3 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "Inconsistency detected. Invalid item position "
            r2.<init>(r0)
            r2.append(r15)
            java.lang.String r0 = "(offset:"
            r2.append(r0)
            r2.append(r7)
            java.lang.String r0 = ").state:"
            r2.append(r0)
            androidx.recyclerview.widget.RecyclerView r1 = r14.A07
            X.0wD r0 = r1.A0y
            int r0 = r0.A00()
            r2.append(r0)
            java.lang.String r0 = r1.A0d()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            throw r3
        L_0x03ca:
            r0 = 54678396(0x342537c, float:5.710731E-37)
            X.AnonymousClass06K.A00(r0)
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r0 == 0) goto L_0x03e3
            android.view.View r0 = r9.A0H
            androidx.recyclerview.widget.RecyclerView r1 = androidx.recyclerview.widget.RecyclerView.A06(r0)
            if (r1 == 0) goto L_0x03e3
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r1)
            r9.A0C = r0
        L_0x03e3:
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r0 == 0) goto L_0x0582
            long r7 = java.lang.System.nanoTime()
        L_0x03eb:
            X.0vg r0 = r14.A02
            long r7 = r7 - r10
            X.1R1 r12 = X.C15680vg.A00(r0, r6)
            long r0 = r12.A01
            r10 = 0
            int r5 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r5 == 0) goto L_0x0403
            r10 = 4
            long r0 = r0 / r10
            r5 = 3
            long r0 = r0 * r5
            long r7 = r7 / r10
            long r0 = r0 + r7
            r7 = r0
        L_0x0403:
            r12.A01 = r7
        L_0x0405:
            if (r13 == 0) goto L_0x0439
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wD r7 = r0.A0y
            boolean r0 = r7.A08
            if (r0 != 0) goto L_0x0439
            r6 = 8192(0x2000, float:1.14794E-41)
            int r5 = r9.A00
            r1 = r6 & r5
            r0 = 0
            if (r1 == 0) goto L_0x0419
            r0 = 1
        L_0x0419:
            if (r0 == 0) goto L_0x0439
            r0 = r6 ^ -1
            r5 = r5 & r0
            r2 = r2 & r6
            r2 = r2 | r5
            r9.A00 = r2
            boolean r0 = r7.A0B
            if (r0 == 0) goto L_0x0439
            X.C20221Bj.A00(r9)
            r9.A06()
            X.2xx r1 = new X.2xx
            r1.<init>()
            r1.A00(r9)
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            r0.A16(r9, r1)
        L_0x0439:
            androidx.recyclerview.widget.RecyclerView r5 = r14.A07
            X.0wD r0 = r5.A0y
            boolean r0 = r0.A08
            if (r0 == 0) goto L_0x047d
            int r1 = r9.A00
            r0 = 1
            r1 = r1 & r3
            if (r1 != 0) goto L_0x0448
            r0 = 0
        L_0x0448:
            if (r0 == 0) goto L_0x047d
            r9.A05 = r15
        L_0x044c:
            r5 = 0
        L_0x044d:
            android.view.View r0 = r9.A0H
            android.view.ViewGroup$LayoutParams r2 = r0.getLayoutParams()
            if (r2 != 0) goto L_0x046d
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            android.view.ViewGroup$LayoutParams r2 = r0.generateDefaultLayoutParams()
        L_0x045b:
            X.1R7 r2 = (X.AnonymousClass1R7) r2
            android.view.View r0 = r9.A0H
            r0.setLayoutParams(r2)
        L_0x0462:
            r2.mViewHolder = r9
            if (r13 == 0) goto L_0x046b
            if (r5 == 0) goto L_0x046b
        L_0x0468:
            r2.A00 = r3
            return r9
        L_0x046b:
            r3 = 0
            goto L_0x0468
        L_0x046d:
            androidx.recyclerview.widget.RecyclerView r1 = r14.A07
            boolean r0 = r1.checkLayoutParams(r2)
            if (r0 != 0) goto L_0x047a
            android.view.ViewGroup$LayoutParams r2 = r1.generateLayoutParams(r2)
            goto L_0x045b
        L_0x047a:
            X.1R7 r2 = (X.AnonymousClass1R7) r2
            goto L_0x0462
        L_0x047d:
            int r2 = r9.A00
            r1 = 1
            r0 = r2 & r3
            if (r0 != 0) goto L_0x0485
            r1 = 0
        L_0x0485:
            if (r1 == 0) goto L_0x0495
            r1 = r2 & 2
            r0 = 0
            if (r1 == 0) goto L_0x048d
            r0 = 1
        L_0x048d:
            if (r0 != 0) goto L_0x0495
            boolean r0 = r9.A0D()
            if (r0 == 0) goto L_0x044c
        L_0x0495:
            X.0wZ r1 = r5.A0G
            r0 = 0
            int r5 = X.C16150wZ.A01(r1, r15, r0)
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            r9.A0B = r0
            int r6 = r9.A01
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r0 == 0) goto L_0x04cc
            long r10 = java.lang.System.nanoTime()
        L_0x04aa:
            r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            int r0 = (r17 > r1 ? 1 : (r17 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x04cf
            X.0vg r0 = r14.A02
            X.1R1 r0 = X.C15680vg.A00(r0, r6)
            long r0 = r0.A00
            r6 = 0
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x04c8
            long r6 = r10 + r0
            int r1 = (r6 > r17 ? 1 : (r6 == r17 ? 0 : -1))
            r0 = 0
            if (r1 >= 0) goto L_0x04c9
        L_0x04c8:
            r0 = 1
        L_0x04c9:
            if (r0 != 0) goto L_0x04cf
            goto L_0x044c
        L_0x04cc:
            r10 = 0
            goto L_0x04aa
        L_0x04cf:
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.1Dz r6 = r0.A0J
            r9.A04 = r5
            boolean r0 = r6.hasStableIds()
            if (r0 == 0) goto L_0x04e1
            long r0 = r6.getItemId(r5)
            r9.A07 = r0
        L_0x04e1:
            r2 = 519(0x207, float:7.27E-43)
            int r1 = r9.A00
            r0 = r2 ^ -1
            r1 = r1 & r0
            r0 = r3 & r2
            r0 = r0 | r1
            r9.A00 = r0
            r1 = 1983356480(0x76379e40, float:9.310547E32)
            java.lang.String r0 = "RV OnBindView"
            X.AnonymousClass06K.A01(r0, r1)
            java.util.List r0 = r9.A06()
            r6.A0E(r9, r5, r0)
            java.util.List r0 = r9.A0D
            if (r0 == 0) goto L_0x0503
            r0.clear()
        L_0x0503:
            int r0 = r9.A00
            r0 = r0 & -1025(0xfffffffffffffbff, float:NaN)
            r9.A00 = r0
            android.view.View r0 = r9.A0H
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            boolean r0 = r1 instanceof X.AnonymousClass1R7
            if (r0 == 0) goto L_0x0517
            X.1R7 r1 = (X.AnonymousClass1R7) r1
            r1.A01 = r3
        L_0x0517:
            r0 = -1474762564(0xffffffffa818e8bc, float:-8.488161E-15)
            X.AnonymousClass06K.A00(r0)
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r0 == 0) goto L_0x057f
            long r5 = java.lang.System.nanoTime()
        L_0x0525:
            X.0vg r1 = r14.A02
            int r0 = r9.A01
            long r5 = r5 - r10
            X.1R1 r12 = X.C15680vg.A00(r1, r0)
            long r0 = r12.A00
            r7 = 0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x053f
            r10 = 4
            long r0 = r0 / r10
            r7 = 3
            long r0 = r0 * r7
            long r5 = r5 / r10
            long r0 = r0 + r5
            r5 = r0
        L_0x053f:
            r12.A00 = r5
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            android.view.accessibility.AccessibilityManager r0 = r0.A0v
            if (r0 == 0) goto L_0x054e
            boolean r1 = r0.isEnabled()
            r0 = 1
            if (r1 != 0) goto L_0x054f
        L_0x054e:
            r0 = 0
        L_0x054f:
            if (r0 == 0) goto L_0x0572
            android.view.View r2 = r9.A0H
            int r0 = X.C15320v6.getImportantForAccessibility(r2)
            if (r0 != 0) goto L_0x055c
            X.C15320v6.setImportantForAccessibility(r2, r3)
        L_0x055c:
            boolean r0 = X.C15320v6.hasAccessibilityDelegate(r2)
            if (r0 != 0) goto L_0x0572
            r1 = 16384(0x4000, float:2.2959E-41)
            int r0 = r9.A00
            r1 = r1 | r0
            r9.A00 = r1
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wl r0 = r0.A0Q
            X.0uv r0 = r0.A00
            X.C15320v6.setAccessibilityDelegate(r2, r0)
        L_0x0572:
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            X.0wD r0 = r0.A0y
            boolean r0 = r0.A08
            if (r0 == 0) goto L_0x057c
            r9.A05 = r15
        L_0x057c:
            r5 = 1
            goto L_0x044d
        L_0x057f:
            r5 = 0
            goto L_0x0525
        L_0x0582:
            r7 = 0
            goto L_0x03eb
        L_0x0586:
            java.lang.IndexOutOfBoundsException r2 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Inconsistency detected. Invalid view holder adapter position"
            r1.<init>(r0)
            r1.append(r9)
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            java.lang.String r0 = r0.A0d()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x05a3:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "layout index should not be -1 after unhiding a view:"
            r1.<init>(r0)
            r1.append(r9)
            androidx.recyclerview.widget.RecyclerView r0 = r14.A07
            java.lang.String r0 = r0.A0d()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x05c0:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "trying to unhide a view that was not hidden"
            r1.<init>(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x05d4:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "view is not a child, cannot hide "
            r1.<init>(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x05e8:
            java.lang.IndexOutOfBoundsException r3 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "Invalid item position "
            r2.<init>(r0)
            r2.append(r15)
            java.lang.String r0 = "("
            r2.append(r0)
            r2.append(r15)
            java.lang.String r0 = "). Item count:"
            r2.append(r0)
            androidx.recyclerview.widget.RecyclerView r1 = r14.A07
            X.0wD r0 = r1.A0y
            int r0 = r0.A00()
            r2.append(r0)
            java.lang.String r0 = r1.A0d()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15740vp.A05(int, boolean, long):X.1o8");
    }

    public void A06() {
        int i;
        AnonymousClass19T r0 = this.A07.A0L;
        if (r0 != null) {
            i = r0.A03;
        } else {
            i = 0;
        }
        this.A00 = this.A01 + i;
        for (int size = this.A05.size() - 1; size >= 0 && this.A05.size() > this.A00; size--) {
            A02(this, size);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        if (X.C15320v6.hasTransientState(r10.A0H) == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        if (r1 == false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004d, code lost:
        if (X.C15320v6.hasTransientState(r10.A0H) != false) goto L_0x004f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005c A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00a2 A[LOOP:1: B:56:0x00a2->B:65:0x00c4, LOOP_START, PHI: r7 
      PHI: (r7v3 int) = (r7v1 int), (r7v4 int) binds: [B:55:0x00a0, B:65:0x00c4] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(X.C33781o8 r10) {
        /*
            r9 = this;
            X.0vp r1 = r10.A08
            r0 = 0
            if (r1 == 0) goto L_0x0006
            r0 = 1
        L_0x0006:
            r5 = 0
            r6 = 1
            if (r0 != 0) goto L_0x010f
            android.view.View r0 = r10.A0H
            android.view.ViewParent r0 = r0.getParent()
            if (r0 != 0) goto L_0x010f
            boolean r0 = r10.A0B()
            if (r0 != 0) goto L_0x00f2
            boolean r0 = r10.A0F()
            if (r0 != 0) goto L_0x00e0
            int r0 = r10.A00
            r0 = r0 & 16
            if (r0 != 0) goto L_0x002d
            android.view.View r0 = r10.A0H
            boolean r0 = X.C15320v6.hasTransientState(r0)
            r8 = 1
            if (r0 != 0) goto L_0x002e
        L_0x002d:
            r8 = 0
        L_0x002e:
            androidx.recyclerview.widget.RecyclerView r0 = r9.A07
            X.1Dz r0 = r0.A0J
            if (r0 == 0) goto L_0x003d
            if (r8 == 0) goto L_0x003d
            boolean r1 = r0.A0A(r10)
            r0 = 1
            if (r1 != 0) goto L_0x003e
        L_0x003d:
            r0 = 0
        L_0x003e:
            if (r0 != 0) goto L_0x0064
            int r0 = r10.A00
            r0 = r0 & 16
            if (r0 != 0) goto L_0x004f
            android.view.View r0 = r10.A0H
            boolean r1 = X.C15320v6.hasTransientState(r0)
            r0 = 1
            if (r1 == 0) goto L_0x0050
        L_0x004f:
            r0 = 0
        L_0x0050:
            if (r0 != 0) goto L_0x0064
            r1 = 0
        L_0x0053:
            androidx.recyclerview.widget.RecyclerView r0 = r9.A07
            X.0wA r0 = r0.A10
            r0.A03(r10)
            if (r1 != 0) goto L_0x0063
            if (r5 != 0) goto L_0x0063
            if (r8 == 0) goto L_0x0063
            r0 = 0
            r10.A0B = r0
        L_0x0063:
            return
        L_0x0064:
            int r0 = r9.A00
            if (r0 <= 0) goto L_0x00de
            r1 = 526(0x20e, float:7.37E-43)
            int r0 = r10.A00
            r1 = r1 & r0
            r0 = 0
            if (r1 == 0) goto L_0x0071
            r0 = 1
        L_0x0071:
            if (r0 != 0) goto L_0x00de
            java.util.ArrayList r0 = r9.A05
            int r7 = r0.size()
            int r0 = r9.A00
            if (r7 < r0) goto L_0x0084
            if (r7 <= 0) goto L_0x0084
            A02(r9, r5)
            int r7 = r7 + -1
        L_0x0084:
            boolean r0 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r0 == 0) goto L_0x00c7
            if (r7 <= 0) goto L_0x00c7
            androidx.recyclerview.widget.RecyclerView r0 = r9.A07
            X.1uj r0 = r0.A0I
            int r4 = r10.A04
            int[] r3 = r0.A03
            if (r3 == 0) goto L_0x00dc
            int r0 = r0.A00
            int r2 = r0 << 1
            r1 = 0
        L_0x0099:
            if (r1 >= r2) goto L_0x00dc
            r0 = r3[r1]
            if (r0 != r4) goto L_0x00d9
            r0 = 1
        L_0x00a0:
            if (r0 != 0) goto L_0x00c7
        L_0x00a2:
            int r7 = r7 + -1
            if (r7 < 0) goto L_0x00c6
            java.util.ArrayList r0 = r9.A05
            java.lang.Object r0 = r0.get(r7)
            X.1o8 r0 = (X.C33781o8) r0
            int r4 = r0.A04
            androidx.recyclerview.widget.RecyclerView r0 = r9.A07
            X.1uj r0 = r0.A0I
            int[] r3 = r0.A03
            if (r3 == 0) goto L_0x00d7
            int r0 = r0.A00
            int r2 = r0 << 1
            r1 = 0
        L_0x00bd:
            if (r1 >= r2) goto L_0x00d7
            r0 = r3[r1]
            if (r0 != r4) goto L_0x00d4
            r0 = 1
        L_0x00c4:
            if (r0 != 0) goto L_0x00a2
        L_0x00c6:
            int r7 = r7 + r6
        L_0x00c7:
            java.util.ArrayList r0 = r9.A05
            r0.add(r7, r10)
            r1 = 1
        L_0x00cd:
            if (r1 != 0) goto L_0x0053
            r9.A0B(r10, r6)
            r5 = 1
            goto L_0x0053
        L_0x00d4:
            int r1 = r1 + 2
            goto L_0x00bd
        L_0x00d7:
            r0 = 0
            goto L_0x00c4
        L_0x00d9:
            int r1 = r1 + 2
            goto L_0x0099
        L_0x00dc:
            r0 = 0
            goto L_0x00a0
        L_0x00de:
            r1 = 0
            goto L_0x00cd
        L_0x00e0:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."
            androidx.recyclerview.widget.RecyclerView r0 = r9.A07
            java.lang.String r0 = r0.A0d()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x00f2:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Tmp detached view should be removed from RecyclerView before it can be recycled: "
            r1.<init>(r0)
            r1.append(r10)
            androidx.recyclerview.widget.RecyclerView r0 = r9.A07
            java.lang.String r0 = r0.A0d()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x010f:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = "Scrapped or attached views may not be recycled. isScrap:"
            r2.<init>(r0)
            X.0vp r1 = r10.A08
            r0 = 0
            if (r1 == 0) goto L_0x011e
            r0 = 1
        L_0x011e:
            r2.append(r0)
            java.lang.String r0 = " isAttached:"
            r2.append(r0)
            android.view.View r0 = r10.A0H
            android.view.ViewParent r0 = r0.getParent()
            if (r0 == 0) goto L_0x012f
            r5 = 1
        L_0x012f:
            r2.append(r5)
            androidx.recyclerview.widget.RecyclerView r0 = r9.A07
            java.lang.String r0 = r0.A0d()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15740vp.A09(X.1o8):void");
    }

    public void A0A(C33781o8 r2) {
        if (r2.A0F) {
            this.A03.remove(r2);
        } else {
            this.A04.remove(r2);
        }
        r2.A08 = null;
        r2.A0F = false;
        r2.A00 &= -33;
    }

    private void A00(ViewGroup viewGroup, boolean z) {
        for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = viewGroup.getChildAt(childCount);
            if (childAt instanceof ViewGroup) {
                A00((ViewGroup) childAt, true);
            }
        }
        if (!z) {
            return;
        }
        if (viewGroup.getVisibility() == 4) {
            viewGroup.setVisibility(0);
            viewGroup.setVisibility(4);
            return;
        }
        int visibility = viewGroup.getVisibility();
        viewGroup.setVisibility(4);
        viewGroup.setVisibility(visibility);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0031, code lost:
        if (X.C15320v6.hasTransientState(r3.A0H) != false) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(android.view.View r5) {
        /*
            r4 = this;
            X.1o8 r3 = androidx.recyclerview.widget.RecyclerView.A05(r5)
            boolean r0 = r3.A0B()
            if (r0 == 0) goto L_0x0010
            androidx.recyclerview.widget.RecyclerView r1 = r4.A07
            r0 = 0
            r1.removeDetachedView(r5, r0)
        L_0x0010:
            X.0vp r1 = r3.A08
            r0 = 0
            if (r1 == 0) goto L_0x0016
            r0 = 1
        L_0x0016:
            if (r0 == 0) goto L_0x003e
            r1.A0A(r3)
        L_0x001b:
            r4.A09(r3)
            androidx.recyclerview.widget.RecyclerView r0 = r4.A07
            X.1Bj r0 = r0.A0K
            if (r0 == 0) goto L_0x003d
            int r0 = r3.A00
            r0 = r0 & 16
            if (r0 != 0) goto L_0x0033
            android.view.View r0 = r3.A0H
            boolean r1 = X.C15320v6.hasTransientState(r0)
            r0 = 1
            if (r1 == 0) goto L_0x0034
        L_0x0033:
            r0 = 0
        L_0x0034:
            if (r0 != 0) goto L_0x003d
            androidx.recyclerview.widget.RecyclerView r0 = r4.A07
            X.1Bj r0 = r0.A0K
            r0.A0A(r3)
        L_0x003d:
            return
        L_0x003e:
            int r2 = r3.A00
            r1 = r2 & 32
            r0 = 0
            if (r1 == 0) goto L_0x0046
            r0 = 1
        L_0x0046:
            if (r0 == 0) goto L_0x001b
            r0 = r2 & -33
            r3.A00 = r0
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15740vp.A07(android.view.View):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (r1.A0H(r2, r2.A06()) != false) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.view.View r5) {
        /*
            r4 = this;
            X.1o8 r2 = androidx.recyclerview.widget.RecyclerView.A05(r5)
            r3 = 12
            int r1 = r2.A00
            r3 = r3 & r1
            r0 = 0
            if (r3 == 0) goto L_0x000d
            r0 = 1
        L_0x000d:
            if (r0 != 0) goto L_0x0041
            r1 = r1 & 2
            r0 = 0
            if (r1 == 0) goto L_0x0015
            r0 = 1
        L_0x0015:
            if (r0 == 0) goto L_0x0041
            androidx.recyclerview.widget.RecyclerView r0 = r4.A07
            X.1Bj r1 = r0.A0K
            if (r1 == 0) goto L_0x0028
            java.util.List r0 = r2.A06()
            boolean r1 = r1.A0H(r2, r0)
            r0 = 0
            if (r1 == 0) goto L_0x0029
        L_0x0028:
            r0 = 1
        L_0x0029:
            if (r0 != 0) goto L_0x0041
            java.util.ArrayList r0 = r4.A03
            if (r0 != 0) goto L_0x0036
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r4.A03 = r0
        L_0x0036:
            r0 = 1
            r2.A08 = r4
            r2.A0F = r0
            java.util.ArrayList r0 = r4.A03
            r0.add(r2)
            return
        L_0x0041:
            boolean r0 = r2.A0D()
            if (r0 == 0) goto L_0x0067
            boolean r0 = r2.A0E()
            if (r0 != 0) goto L_0x0067
            androidx.recyclerview.widget.RecyclerView r3 = r4.A07
            X.1Dz r0 = r3.A0J
            boolean r0 = r0.hasStableIds()
            if (r0 != 0) goto L_0x0067
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool."
            java.lang.String r0 = r3.A0d()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        L_0x0067:
            r0 = 0
            r2.A08 = r4
            r2.A0F = r0
            java.util.ArrayList r0 = r4.A04
            r0.add(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15740vp.A08(android.view.View):void");
    }

    public void A0B(C33781o8 r6, boolean z) {
        RecyclerView.A0H(r6);
        int i = r6.A00;
        boolean z2 = false;
        if ((16384 & i) != 0) {
            z2 = true;
        }
        if (z2) {
            r6.A00 = (0 & DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET) | (i & (16384 ^ -1));
            C15320v6.setAccessibilityDelegate(r6.A0H, null);
        }
        if (z) {
            C26821DCx dCx = this.A07.A0P;
            if (dCx != null) {
                dCx.Bus(r6);
            }
            C20831Dz r0 = this.A07.A0J;
            if (r0 != null) {
                r0.A0D(r6);
            }
            RecyclerView recyclerView = this.A07;
            if (recyclerView.A0y != null) {
                recyclerView.A10.A03(r6);
            }
        }
        r6.A0B = null;
        if (this.A02 == null) {
            this.A02 = new C15680vg();
        }
        C15680vg r3 = this.A02;
        int i2 = r6.A01;
        ArrayList arrayList = C15680vg.A00(r3, i2).A02;
        r3.A01.get(i2);
        if (5 > arrayList.size()) {
            r6.A07();
            arrayList.add(r6);
        }
    }
}
