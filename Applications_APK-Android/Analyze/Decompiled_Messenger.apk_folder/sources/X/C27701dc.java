package X;

import java.util.Map;

/* renamed from: X.1dc  reason: invalid class name and case insensitive filesystem */
public final class C27701dc implements Map.Entry {
    public C27701dc A00;
    public C27701dc A01;
    public final Object A02;
    public final Object A03;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C27701dc)) {
            return false;
        }
        C27701dc r4 = (C27701dc) obj;
        return this.A02.equals(r4.A02) && this.A03.equals(r4.A03);
    }

    public Object getKey() {
        return this.A02;
    }

    public Object getValue() {
        return this.A03;
    }

    public int hashCode() {
        return this.A02.hashCode() ^ this.A03.hashCode();
    }

    public Object setValue(Object obj) {
        throw new UnsupportedOperationException("An entry modification is not supported");
    }

    public String toString() {
        return this.A02 + "=" + this.A03;
    }

    public C27701dc(Object obj, Object obj2) {
        this.A02 = obj;
        this.A03 = obj2;
    }
}
