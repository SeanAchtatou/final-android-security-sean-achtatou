package X;

/* renamed from: X.1ZX  reason: invalid class name */
public final class AnonymousClass1ZX extends AnonymousClass1ZY implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.scheduler.RunnableJob";
    public final Runnable A00;
    public final String A01;

    public void run() {
        this.A00.run();
    }

    public AnonymousClass1ZX(AnonymousClass1ZP r2) {
        super(r2);
        this.A01 = r2.A01;
        this.A00 = r2.A00;
    }
}
