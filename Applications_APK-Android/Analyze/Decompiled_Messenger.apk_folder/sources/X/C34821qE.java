package X;

import androidx.fragment.app.Fragment;

/* renamed from: X.1qE  reason: invalid class name and case insensitive filesystem */
public final class C34821qE {
    private AnonymousClass0UN A00;

    public static final C34821qE A00(AnonymousClass1XY r1) {
        return new C34821qE(r1);
    }

    public Fragment A01() {
        if (((C25051Yd) AnonymousClass1XX.A03(AnonymousClass1Y3.AOJ, this.A00)).Aem(283119949187257L)) {
            return new C46992Su();
        }
        return new C34831qF();
    }

    private C34821qE(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
