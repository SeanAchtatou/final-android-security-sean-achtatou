package X;

import android.content.Context;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.facebook.payments.ui.PaymentFormEditTextView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0zR  reason: invalid class name and case insensitive filesystem */
public abstract class C17770zR extends C17780zS implements C17810zV, AnonymousClass0zW, C17820zX, Cloneable {
    public static final AtomicInteger A0F = new AtomicInteger(1);
    private static final C17840zZ[] A0G = new C17840zZ[0];
    public int A00;
    public SparseIntArray A01;
    public AnonymousClass11G A02;
    public AnonymousClass0p4 A03;
    public AnonymousClass10N A04;
    public C128015zX A05;
    public String A06;
    public String A07;
    public List A08;
    public Map A09;
    public AtomicBoolean A0A;
    public boolean A0B;
    public boolean A0C;
    private String A0D;
    private final String A0E;

    public static boolean A06(int i, Collection collection, Collection collection2) {
        if (i >= 1) {
            if (!(collection == null && collection2 == null)) {
                if (collection != null) {
                    if (collection2 == null || collection.size() != collection2.size()) {
                        return false;
                    }
                } else if (collection2 != null) {
                    return false;
                }
                Iterator it = collection.iterator();
                Iterator it2 = collection2.iterator();
                while (it.hasNext() && it2.hasNext()) {
                    if (i == 1) {
                        if (!((C17770zR) it.next()).A1L((C17770zR) it2.next())) {
                            return false;
                        }
                    } else if (!A06(i - 1, (Collection) it.next(), (Collection) it2.next())) {
                        return false;
                    }
                }
            }
            return true;
        }
        throw new IllegalArgumentException("Level cannot be < 1");
    }

    public static boolean A0A(C17770zR r3, C17770zR r4, boolean z) {
        if (r3 != r4) {
            if (r4 == null || r3.getClass() != r4.getClass()) {
                return false;
            }
            if (r3.A00 != r4.A00) {
                if (z) {
                    return A0C(r3, r4, true);
                }
                return A0C(r3, r4, false);
            }
        }
        return true;
    }

    public C17770zR A15() {
        if (!(this instanceof C21841Ix)) {
            return null;
        }
        return ((C21841Ix) this).A00;
    }

    public AnonymousClass11I A18() {
        if (this instanceof AnonymousClass1KX) {
            return ((AnonymousClass1KX) this).A0A;
        }
        if (this instanceof AnonymousClass1JF) {
            return ((AnonymousClass1JF) this).A03;
        }
        if (this instanceof C37301v3) {
            return ((C37301v3) this).A07;
        }
        if (this instanceof C31971kt) {
            return ((C31971kt) this).A06;
        }
        if (this instanceof C37311v4) {
            return ((C37311v4) this).A05;
        }
        if (this instanceof AnonymousClass1v5) {
            return ((AnonymousClass1v5) this).A05;
        }
        if (this instanceof AnonymousClass1v6) {
            return ((AnonymousClass1v6) this).A05;
        }
        if (this instanceof AnonymousClass1JJ) {
            return ((AnonymousClass1JJ) this).A01;
        }
        if (this instanceof AnonymousClass1JK) {
            return ((AnonymousClass1JK) this).A03;
        }
        if (this instanceof AnonymousClass11H) {
            return ((AnonymousClass11H) this).A04;
        }
        if (this instanceof C37321v7) {
            return ((C37321v7) this).A04;
        }
        if (this instanceof AnonymousClass1v8) {
            return ((AnonymousClass1v8) this).A02;
        }
        if (this instanceof AnonymousClass1JN) {
            return ((AnonymousClass1JN) this).A02;
        }
        if (this instanceof AnonymousClass1JO) {
            return ((AnonymousClass1JO) this).A02;
        }
        if (!(this instanceof AnonymousClass1JE)) {
            return null;
        }
        return ((AnonymousClass1JE) this).A07;
    }

    public void A1C(C17770zR r3) {
        if (this instanceof AnonymousClass1KX) {
            AnonymousClass1KX r1 = (AnonymousClass1KX) this;
            AnonymousClass1KX r32 = (AnonymousClass1KX) r3;
            r1.A0E = r32.A0E;
            r1.A0F = r32.A0F;
            r1.A0G = r32.A0G;
            r1.A0H = r32.A0H;
        } else if (this instanceof AnonymousClass1NM) {
            AnonymousClass1NM r12 = (AnonymousClass1NM) this;
            AnonymousClass1NM r33 = (AnonymousClass1NM) r3;
            r12.A0B = r33.A0B;
            r12.A04 = r33.A04;
            r12.A05 = r33.A05;
            r12.A0C = r33.A0C;
            r12.A0D = r33.A0D;
            r12.A0E = r33.A0E;
            r12.A06 = r33.A06;
            r12.A07 = r33.A07;
            r12.A0F = r33.A0F;
            r12.A0G = r33.A0G;
            r12.A0H = r33.A0H;
            r12.A0I = r33.A0I;
            r12.A0J = r33.A0J;
        } else if (this instanceof AnonymousClass1v5) {
            ((AnonymousClass1v5) this).A01 = ((AnonymousClass1v5) r3).A01;
        } else if (this instanceof AnonymousClass1vC) {
            AnonymousClass1vC r13 = (AnonymousClass1vC) this;
            AnonymousClass1vC r34 = (AnonymousClass1vC) r3;
            r13.A02 = r34.A02;
            r13.A06 = r34.A06;
            r13.A07 = r34.A07;
        } else if (this instanceof C37331vE) {
            ((C37331vE) this).A00 = ((C37331vE) r3).A00;
        } else if (this instanceof C14910uL) {
            C14910uL r14 = (C14910uL) this;
            C14910uL r35 = (C14910uL) r3;
            r14.A0m = r35.A0m;
            r14.A0n = r35.A0n;
            r14.A0S = r35.A0S;
            r14.A0e = r35.A0e;
            r14.A0f = r35.A0f;
            r14.A0b = r35.A0b;
            r14.A0T = r35.A0T;
            r14.A0d = r35.A0d;
        } else if (this instanceof AnonymousClass10H) {
            AnonymousClass10H r15 = (AnonymousClass10H) this;
            AnonymousClass10H r36 = (AnonymousClass10H) r3;
            r15.A03 = r36.A03;
            r15.A02 = r36.A02;
            r15.A04 = r36.A04;
        } else if (this instanceof AnonymousClass1JE) {
            ((AnonymousClass1JE) this).A05 = ((AnonymousClass1JE) r3).A05;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        if (A0U() != X.AnonymousClass07B.A00) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1F(X.AnonymousClass0p4 r12, int r13, int r14, X.AnonymousClass10L r15) {
        /*
            r11 = this;
            r4 = r12
            X.1kq r0 = r12.A06
            if (r0 != 0) goto L_0x0085
            r2 = 0
        L_0x0006:
            if (r2 == 0) goto L_0x0089
            java.util.Map r1 = r2.A0h
            int r0 = r11.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r3 = r1.get(r0)
            X.0yx r3 = (X.C17470yx) r3
            r6 = r13
            r7 = r14
            if (r3 == 0) goto L_0x0036
            int r1 = r3.AsD()
            int r0 = r3.getWidth()
            boolean r0 = X.C15050ue.A00(r1, r13, r0)
            if (r0 == 0) goto L_0x0036
            int r1 = r3.Ary()
            int r0 = r3.getHeight()
            boolean r0 = X.C15050ue.A00(r1, r14, r0)
            if (r0 != 0) goto L_0x0078
        L_0x0036:
            java.util.Map r1 = r2.A0h
            int r0 = r11.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.remove(r0)
            r8 = 0
            r9 = 0
            r10 = 0
            r5 = r11
            X.0yx r3 = X.C31951kr.A01(r4, r5, r6, r7, r8, r9, r10)
            java.util.Map r1 = r2.A0h
            int r0 = r11.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.put(r0, r3)
            if (r11 == 0) goto L_0x005f
            java.lang.Integer r2 = r11.A0U()
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 1
            if (r2 == r1) goto L_0x0060
        L_0x005f:
            r0 = 0
        L_0x0060:
            if (r0 == 0) goto L_0x0078
            r3.C8r(r13)
            r3.C8l(r14)
            int r0 = r3.getWidth()
            float r0 = (float) r0
            r3.C8n(r0)
            int r0 = r3.getHeight()
            float r0 = (float) r0
            r3.C8m(r0)
        L_0x0078:
            int r0 = r3.getWidth()
            r15.A01 = r0
            int r0 = r3.getHeight()
            r15.A00 = r0
            return
        L_0x0085:
            X.15v r2 = r0.A01
            goto L_0x0006
        L_0x0089:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r11.A1A()
            java.lang.String r0 = ": Trying to measure a component outside of a LayoutState calculation. If that is what you must do, see Component#measureMightNotCacheInternalNode."
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17770zR.A1F(X.0p4, int, int, X.10L):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0008, code lost:
        if (r1 == null) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1G(X.AnonymousClass0p4 r11, int r12, int r13, X.AnonymousClass10L r14) {
        /*
            r10 = this;
            r3 = r11
            X.1kq r0 = r11.A06
            if (r0 == 0) goto L_0x000a
            X.15v r1 = r0.A01
            r0 = 1
            if (r1 != 0) goto L_0x000b
        L_0x000a:
            r0 = 0
        L_0x000b:
            r5 = r12
            r6 = r13
            if (r0 == 0) goto L_0x0013
            r10.A1F(r11, r12, r13, r14)
            return
        L_0x0013:
            X.1JC r0 = r11.A0C
            if (r0 != 0) goto L_0x0023
            X.0p4 r2 = new X.0p4
            X.1JC r1 = new X.1JC
            r0 = 0
            r1.<init>(r0)
            r2.<init>(r11, r1, r0, r0)
            r3 = r2
        L_0x0023:
            r7 = 0
            r8 = 0
            r9 = 0
            r4 = r10
            X.0yx r1 = X.C31951kr.A01(r3, r4, r5, r6, r7, r8, r9)
            int r0 = r1.getWidth()
            r14.A01 = r0
            int r0 = r1.getHeight()
            r14.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17770zR.A1G(X.0p4, int, int, X.10L):void");
    }

    public void A1H(AnonymousClass1RE r1) {
    }

    public void A1I(String str) {
        this.A0B = true;
        this.A0D = str;
    }

    public boolean A1K() {
        return false;
    }

    public C17790zT Alq() {
        return this;
    }

    public static String A05(String str, int i) {
        if (i == 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str.length() + 4);
        sb.append(str);
        sb.append('!');
        sb.append(i);
        return sb.toString();
    }

    public static boolean A07(C17770zR r2) {
        if (r2 == null || r2.A0U() == AnonymousClass07B.A00) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0009, code lost:
        if (r3.A0U() != X.AnonymousClass07B.A00) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A08(X.C17770zR r3) {
        /*
            if (r3 == 0) goto L_0x000b
            java.lang.Integer r2 = r3.A0U()
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 1
            if (r2 == r1) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            if (r0 == 0) goto L_0x0015
            boolean r1 = r3.A0s()
            r0 = 1
            if (r1 != 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17770zR.A08(X.0zR):boolean");
    }

    public static boolean A09(C17770zR r2) {
        if (r2 == null || r2.A0U() != AnonymousClass07B.A0C) {
            return false;
        }
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0155, code lost:
        if (r1 != null) goto L_0x0157;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0160 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x016a A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0C(java.lang.Object r11, java.lang.Object r12, boolean r13) {
        /*
            if (r11 == 0) goto L_0x0179
            if (r12 == 0) goto L_0x0179
            java.lang.Class r1 = r11.getClass()
            java.lang.Class r0 = r12.getClass()
            if (r1 != r0) goto L_0x0179
            java.lang.reflect.Field[] r7 = r1.getDeclaredFields()
            int r6 = r7.length
            r5 = 0
            r4 = 0
        L_0x0015:
            r10 = 1
            if (r4 >= r6) goto L_0x0178
            r9 = r7[r4]
            java.lang.Class<com.facebook.litho.annotations.Comparable> r3 = com.facebook.litho.annotations.Comparable.class
            boolean r0 = r9.isAnnotationPresent(r3)
            if (r0 == 0) goto L_0x016a
            java.lang.Class r2 = r9.getType()
            r9.setAccessible(r10)     // Catch:{ IllegalAccessException -> 0x016f }
            java.lang.Object r1 = r9.get(r11)     // Catch:{ IllegalAccessException -> 0x016f }
            java.lang.Object r8 = r9.get(r12)     // Catch:{ IllegalAccessException -> 0x016f }
            r9.setAccessible(r5)     // Catch:{ IllegalAccessException -> 0x016f }
            java.lang.annotation.Annotation r0 = r9.getAnnotation(r3)     // Catch:{ IncompatibleClassChangeError | NullPointerException -> 0x016e }
            com.facebook.litho.annotations.Comparable r0 = (com.facebook.litho.annotations.Comparable) r0     // Catch:{ IncompatibleClassChangeError | NullPointerException -> 0x016e }
            int r0 = r0.type()     // Catch:{ IncompatibleClassChangeError | NullPointerException -> 0x016e }
            switch(r0) {
                case 0: goto L_0x0043;
                case 1: goto L_0x0056;
                case 2: goto L_0x0069;
                case 3: goto L_0x0157;
                case 4: goto L_0x0118;
                case 5: goto L_0x0123;
                case 6: goto L_0x0130;
                case 7: goto L_0x0130;
                case 8: goto L_0x0130;
                case 9: goto L_0x0130;
                case 10: goto L_0x013d;
                case 11: goto L_0x0148;
                case 12: goto L_0x0148;
                case 13: goto L_0x0155;
                case 14: goto L_0x0161;
                case 15: goto L_0x013d;
                default: goto L_0x0041;
            }     // Catch:{ IncompatibleClassChangeError | NullPointerException -> 0x016e }
        L_0x0041:
            goto L_0x016a
        L_0x0043:
            java.lang.Float r1 = (java.lang.Float) r1
            float r1 = r1.floatValue()
            java.lang.Float r8 = (java.lang.Float) r8
            float r0 = r8.floatValue()
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 == 0) goto L_0x016a
            return r5
        L_0x0056:
            java.lang.Double r1 = (java.lang.Double) r1
            double r2 = r1.doubleValue()
            java.lang.Double r8 = (java.lang.Double) r8
            double r0 = r8.doubleValue()
            int r0 = java.lang.Double.compare(r2, r0)
            if (r0 == 0) goto L_0x016a
            return r5
        L_0x0069:
            java.lang.Class r2 = r2.getComponentType()
            java.lang.Class r0 = java.lang.Byte.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x0083
            byte[] r1 = (byte[]) r1
            byte[] r8 = (byte[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
        L_0x007f:
            r0 = 0
        L_0x0080:
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x0083:
            java.lang.Class r0 = java.lang.Short.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x0096
            short[] r1 = (short[]) r1
            short[] r8 = (short[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x0096:
            java.lang.Class r0 = java.lang.Character.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x00a9
            char[] r1 = (char[]) r1
            char[] r8 = (char[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x00a9:
            java.lang.Class r0 = java.lang.Integer.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x00bc
            int[] r1 = (int[]) r1
            int[] r8 = (int[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x00bc:
            java.lang.Class r0 = java.lang.Long.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x00cf
            long[] r1 = (long[]) r1
            long[] r8 = (long[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x00cf:
            java.lang.Class r0 = java.lang.Float.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x00e2
            float[] r1 = (float[]) r1
            float[] r8 = (float[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x00e2:
            java.lang.Class r0 = java.lang.Double.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x00f5
            double[] r1 = (double[]) r1
            double[] r8 = (double[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x00f5:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 == 0) goto L_0x0109
            boolean[] r1 = (boolean[]) r1
            boolean[] r8 = (boolean[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x0109:
            java.lang.Object[] r1 = (java.lang.Object[]) r1
            java.lang.Object[] r8 = (java.lang.Object[]) r8
            boolean r0 = java.util.Arrays.equals(r1, r8)
            if (r0 != 0) goto L_0x0115
            goto L_0x007f
        L_0x0115:
            r0 = 1
            goto L_0x0080
        L_0x0118:
            X.0yA r1 = (X.C17000yA) r1
            X.0yA r8 = (X.C17000yA) r8
            boolean r0 = r1.BEd(r8)
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x0123:
            java.util.Collection r1 = (java.util.Collection) r1
            java.util.Collection r8 = (java.util.Collection) r8
            if (r1 == 0) goto L_0x015e
            boolean r0 = r1.equals(r8)
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x0130:
            int r0 = r0 + -5
            java.util.Collection r1 = (java.util.Collection) r1
            java.util.Collection r8 = (java.util.Collection) r8
            boolean r0 = A06(r0, r1, r8)
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x013d:
            if (r1 == 0) goto L_0x015e
            X.0zW r1 = (X.AnonymousClass0zW) r1
            boolean r0 = r1.BEe(r8)
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x0148:
            if (r1 == 0) goto L_0x015e
            X.10N r1 = (X.AnonymousClass10N) r1
            X.10N r8 = (X.AnonymousClass10N) r8
            boolean r0 = r1.A01(r8)
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x0155:
            if (r1 == 0) goto L_0x015e
        L_0x0157:
            boolean r0 = r1.equals(r8)
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x015e:
            if (r8 == 0) goto L_0x016a
            return r5
        L_0x0161:
            if (r13 == 0) goto L_0x016a
            boolean r0 = A0C(r1, r8, r10)
            if (r0 != 0) goto L_0x016a
            return r5
        L_0x016a:
            int r4 = r4 + 1
            goto L_0x0015
        L_0x016e:
            return r5
        L_0x016f:
            r2 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unable to get fields by reflection."
            r1.<init>(r0, r2)
            throw r1
        L_0x0178:
            return r10
        L_0x0179:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "The input is invalid."
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17770zR.A0C(java.lang.Object, java.lang.Object, boolean):boolean");
    }

    public SparseArray A13() {
        if (!(this instanceof C17670zH)) {
            return null;
        }
        return ((C17670zH) this).A00;
    }

    public AnonymousClass11G A14() {
        if (this.A02 == null) {
            this.A02 = new AnonymousClass11G();
        }
        return this.A02;
    }

    public String A19() {
        if (this.A0D == null && !this.A0B) {
            this.A0D = Integer.toString(this.A00);
        }
        return this.A0D;
    }

    public void A1B(int i, Object obj, Object obj2) {
        if (!(this instanceof AnonymousClass1v9)) {
            throw new RuntimeException("Components that have dynamic Props must override this method");
        }
        AnonymousClass1v9 r0 = (AnonymousClass1v9) this;
        if (i == 0) {
            ((PaymentFormEditTextView) obj2).A0X((String) r0.A02.A00);
        }
    }

    public void A1D(AnonymousClass0p4 r6) {
        this.A03 = r6;
        C17470yx r0 = null;
        if (r0 != null) {
            AnonymousClass0p4 AiS = r0.AiS();
            Context context = r6.A09;
            if (context != AiS.A09) {
                Integer num = AnonymousClass07B.A01;
                C09070gU.A01(num, "Component:MismatchingBaseContext", "Found mismatching base contexts between the Component's Context (" + context + ") and the Context used in willRender (" + AiS.A09 + ")!");
            }
        }
    }

    public void A1E(AnonymousClass0p4 r21) {
        AnonymousClass11I r3;
        ArrayList arrayList;
        List<C61322yh> list;
        C17760zQ r2;
        String A19;
        int i;
        C188915v r32;
        int intValue;
        AnonymousClass0p4 r1 = r21;
        if ((AnonymousClass07c.isDebugModeEnabled || AnonymousClass07c.useGlobalKeys) && this.A06 == null) {
            if (AnonymousClass07c.useNewGenerateMechanismForGlobalKeys) {
                C31941kq r22 = r1.A06;
                if (r22 == null) {
                    r32 = null;
                } else {
                    r32 = r22.A01;
                }
                if (r32 != null) {
                    C17770zR r6 = r1.A04;
                    if (r6 == null) {
                        A19 = A19();
                    } else {
                        if (r6.A06 == null) {
                            C09070gU.A01(AnonymousClass07B.A01, "LayoutState:NullParentKey", AnonymousClass08S.A0V("Trying to generate parent-based key for component ", A1A(), " , but parent ", r6.A1A(), " has a null global key \".", " This is most likely a configuration mistake,", " check the value of ComponentsConfiguration.useGlobalKeys."));
                        }
                        String str = r6.A06;
                        if (str == null) {
                            str = "null";
                        }
                        String A002 = AnonymousClass14L.A00(str, A19());
                        if (this.A0B) {
                            if (r32.A0Q == null) {
                                r32.A0Q = new HashMap();
                            }
                            Integer num = (Integer) r32.A0Q.get(A002);
                            if (num == null) {
                                num = 0;
                            }
                            Map map = r32.A0Q;
                            intValue = num.intValue();
                            map.put(A002, Integer.valueOf(intValue + 1));
                            if (intValue != 0) {
                                C09070gU.A01(AnonymousClass07B.A00, "LayoutState:DuplicateManualKey", AnonymousClass08S.A0U("The manual key ", A19(), " you are setting on this ", A1A(), " is a duplicate and will be changed into a unique one. ", "This will result in unexpected behavior if you don't change it."));
                            }
                        } else {
                            if (r32.A0P == null) {
                                r32.A0P = new HashMap();
                            }
                            Integer num2 = (Integer) r32.A0P.get(A002);
                            if (num2 == null) {
                                num2 = 0;
                            }
                            Map map2 = r32.A0P;
                            intValue = num2.intValue();
                            map2.put(A002, Integer.valueOf(intValue + 1));
                        }
                        A19 = A05(A002, intValue);
                    }
                } else {
                    throw new IllegalStateException(AnonymousClass08S.A0J(A1A(), ": Trying to generate global key of component outside of a LayoutState calculation."));
                }
            } else {
                C17770zR r7 = r1.A04;
                A19 = A19();
                if (r7 != null) {
                    String str2 = r7.A06;
                    if (str2 == null) {
                        C09070gU.A01(AnonymousClass07B.A01, "Component:NullParentKey", AnonymousClass08S.A0V("Trying to generate parent-based key for component ", A1A(), " , but parent ", r7.A1A(), " has a null global key \".", " This is most likely a configuration mistake,", " check the value of ComponentsConfiguration.useGlobalKeys."));
                        A19 = AnonymousClass08S.A0J("null", A19);
                    } else {
                        String A003 = AnonymousClass14L.A00(str2, A19);
                        if (this.A0B) {
                            if (r7.A09 == null) {
                                r7.A09 = new HashMap();
                            }
                            if (r7.A09.containsKey(A003)) {
                                i = ((Integer) r7.A09.get(A003)).intValue();
                            } else {
                                i = 0;
                            }
                            r7.A09.put(A003, Integer.valueOf(i + 1));
                            if (i != 0) {
                                C09070gU.A01(AnonymousClass07B.A00, "Component:DuplicateManualKey", AnonymousClass08S.A0U("The manual key ", A19, " you are setting on this ", A1A(), " is a duplicate and will be changed into a unique one. ", "This will result in unexpected behavior if you don't change it."));
                            }
                            A19 = A05(A003, i);
                        } else {
                            if (r7.A01 == null) {
                                r7.A01 = new SparseIntArray();
                            }
                            int i2 = this.A00;
                            int i3 = r7.A01.get(i2, 0);
                            r7.A01.put(i2, i3 + 1);
                            A19 = A05(A003, i3);
                        }
                    }
                }
            }
            this.A06 = A19;
        }
        A1D(AnonymousClass0p4.A00(r1, this));
        A0o(this.A03.A07);
        if (A0w()) {
            AnonymousClass1JC r62 = r1.A0C;
            AnonymousClass1JC.A04(r62);
            synchronized (r62) {
                if (r62.A00 == null) {
                    r62.A00 = new HashSet();
                }
            }
            if (A0w()) {
                String str3 = this.A06;
                synchronized (r62) {
                    try {
                        r3 = (AnonymousClass11I) r62.A08.get(str3);
                        r62.A00.add(str3);
                    } catch (Throwable th) {
                        while (true) {
                            th = th;
                        }
                    }
                }
                if (r3 != null) {
                    A0n(r3, A18());
                } else {
                    A0Z(this.A03);
                }
                synchronized (r62) {
                    try {
                        Map map3 = r62.A07;
                        arrayList = null;
                        if (map3 == null) {
                            list = null;
                        } else {
                            list = (List) map3.get(str3);
                        }
                    } catch (Throwable th2) {
                        while (true) {
                            th = th2;
                            break;
                        }
                    }
                }
                if (list != null) {
                    for (C61322yh applyStateUpdate : list) {
                        AnonymousClass11I A18 = A18();
                        A18.applyStateUpdate(applyStateUpdate);
                        if (A18 instanceof C21371Gr) {
                            r2 = ((C21371Gr) A18).consumeTransition();
                        } else {
                            r2 = null;
                        }
                        if (r2 != null) {
                            if (arrayList == null) {
                                arrayList = new ArrayList();
                            }
                            arrayList.add(r2);
                        }
                    }
                    C32121lB.A00.addAndGet((long) list.size());
                    synchronized (r62) {
                        try {
                            r62.A07.remove(str3);
                            Map map4 = r62.A05;
                            if (map4 != null) {
                                map4.remove(str3);
                            }
                            r62.A02.put(str3, list);
                        } catch (Throwable th3) {
                            th = th3;
                            throw th;
                        }
                    }
                }
                synchronized (r62) {
                    try {
                        r62.A08.put(str3, A18());
                        if (arrayList != null && !arrayList.isEmpty()) {
                            AnonymousClass1JC.A03(r62);
                            r62.A06.put(str3, arrayList);
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        throw th;
                    }
                }
            }
        }
        if (AnonymousClass07c.enableOnErrorHandling && this.A04 == null) {
            C17810zV r4 = r1.A04;
            if (r4 == null) {
                r4 = new AnonymousClass38c();
            }
            this.A04 = new AnonymousClass10N(r4, C17780zS.A02, new Object[]{r1});
        }
        AtomicBoolean atomicBoolean = this.A0A;
        if (atomicBoolean != null) {
            atomicBoolean.set(true);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0007 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A1J() {
        /*
            r2 = this;
            boolean r0 = r2 instanceof X.C17670zH
            if (r0 != 0) goto L_0x0009
            r0 = 0
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            r0 = 1
        L_0x0008:
            return r0
        L_0x0009:
            r0 = r2
            X.0zH r0 = (X.C17670zH) r0
            android.util.SparseArray r1 = r0.A00
            r0 = 0
            if (r1 == 0) goto L_0x0008
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17770zR.A1J():boolean");
    }

    public C17840zZ[] A1M() {
        if (!(this instanceof AnonymousClass1v9)) {
            return A0G;
        }
        return ((AnonymousClass1v9) this).A04;
    }

    public /* bridge */ /* synthetic */ boolean BEe(Object obj) {
        if (this instanceof AnonymousClass1KX) {
            return ((AnonymousClass1KX) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1v9) {
            return ((AnonymousClass1v9) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1NM) {
            return ((AnonymousClass1NM) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1I8) {
            return ((AnonymousClass1I8) this).A1L((C17770zR) obj);
        }
        if (this instanceof C31981ku) {
            return ((C31981ku) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1KF) {
            return ((AnonymousClass1KF) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1vA) {
            return ((AnonymousClass1vA) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1JU) {
            return ((AnonymousClass1JU) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1vB) {
            return ((AnonymousClass1vB) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1v5) {
            return ((AnonymousClass1v5) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1vC) {
            return ((AnonymousClass1vC) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1vD) {
            return ((AnonymousClass1vD) this).A1L((C17770zR) obj);
        }
        if (this instanceof C37331vE) {
            return ((C37331vE) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1JV) {
            return ((AnonymousClass1JV) this).A1L((C17770zR) obj);
        }
        if (this instanceof C17020yC) {
            return ((C17020yC) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass10I) {
            return ((AnonymousClass10I) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1JW) {
            return ((AnonymousClass1JW) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1JX) {
            return ((AnonymousClass1JX) this).A1L((C17770zR) obj);
        }
        if (this instanceof C14910uL) {
            return ((C14910uL) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass10H) {
            return ((AnonymousClass10H) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1II) {
            return ((AnonymousClass1II) this).A1L((C17770zR) obj);
        }
        if (this instanceof C21841Ix) {
            return ((C21841Ix) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass1KM) {
            return ((AnonymousClass1KM) this).A1L((C17770zR) obj);
        }
        if (this instanceof C16980y8) {
            return ((C16980y8) this).A1L((C17770zR) obj);
        }
        if (this instanceof C17670zH) {
            return ((C17670zH) this).A1L((C17770zR) obj);
        }
        if (this instanceof C15060uf) {
            return ((C15060uf) this).A1L((C17770zR) obj);
        }
        if (this instanceof AnonymousClass11D) {
            return ((AnonymousClass11D) this).A1L((C17770zR) obj);
        }
        if (!(this instanceof AnonymousClass1JE)) {
            return A1L((C17770zR) obj);
        }
        return ((AnonymousClass1JE) this).A1L((C17770zR) obj);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0B(X.AnonymousClass0p4 r1, X.C17770zR r2) {
        /*
            boolean r0 = A08(r2)
            if (r0 != 0) goto L_0x001f
            if (r2 == 0) goto L_0x0026
            if (r1 == 0) goto L_0x0024
            X.1kq r0 = r1.A06
            if (r0 != 0) goto L_0x0021
            r0 = 0
        L_0x000f:
            if (r0 == 0) goto L_0x0024
            java.util.Map r1 = r0.A0h
            int r0 = r2.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r1.containsKey(r0)
        L_0x001d:
            if (r0 == 0) goto L_0x0026
        L_0x001f:
            r0 = 1
            return r0
        L_0x0021:
            X.15v r0 = r0.A01
            goto L_0x000f
        L_0x0024:
            r0 = 0
            goto L_0x001d
        L_0x0026:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17770zR.A0B(X.0p4, X.0zR):boolean");
    }

    public C17770zR A16() {
        try {
            C17770zR r2 = (C17770zR) super.clone();
            r2.A06 = null;
            r2.A0C = false;
            r2.A0B = false;
            r2.A0A = new AtomicBoolean();
            r2.A03 = null;
            r2.A01 = null;
            r2.A09 = null;
            return r2;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public C17770zR A17(AnonymousClass0p4 r4) {
        C17770zR A16 = A16();
        A16.A06 = this.A06;
        A16.A1C(this);
        A16.A1E(r4);
        A16.A03.A07 = A0T(r4, r4.A07);
        return A16;
    }

    public String A1A() {
        C17770zR A15 = A15();
        if (A15 == null) {
            return this.A0E;
        }
        String str = this.A0E;
        while (A15.A15() != null) {
            A15 = A15.A15();
        }
        return AnonymousClass08S.A0S(str, "(", A15.A1A(), ")");
    }

    public C17770zR() {
        this.A00 = A0F.getAndIncrement();
        this.A0A = new AtomicBoolean();
        this.A0C = false;
        this.A0E = getClass().getSimpleName();
    }

    public C17770zR(String str) {
        this.A00 = A0F.getAndIncrement();
        this.A0A = new AtomicBoolean();
        this.A0C = false;
        this.A0E = str;
    }

    public C17770zR(String str, int i) {
        super(i);
        this.A00 = A0F.getAndIncrement();
        this.A0A = new AtomicBoolean();
        this.A0C = false;
        this.A0E = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1101:0x0b1b, code lost:
        if (r1.equals(r9.A0X) == false) goto L_0x0b1d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x0270, code lost:
        if (r9.A04 != null) goto L_0x0272;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x031d, code lost:
        if (r1.equals(r9.A04) == false) goto L_0x031f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:839:0x0888, code lost:
        if (r1.equals(r9.A00) == false) goto L_0x088a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:277:0x02f6 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A1L(X.C17770zR r9) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.AnonymousClass1KX
            if (r0 != 0) goto L_0x0a17
            boolean r0 = r8 instanceof X.AnonymousClass1v9
            if (r0 != 0) goto L_0x09c2
            boolean r0 = r8 instanceof X.AnonymousClass1NM
            if (r0 != 0) goto L_0x0917
            boolean r0 = r8 instanceof X.AnonymousClass1I8
            if (r0 != 0) goto L_0x08a1
            boolean r0 = r8 instanceof X.C31981ku
            if (r0 != 0) goto L_0x0826
            boolean r0 = r8 instanceof X.AnonymousClass1KF
            if (r0 != 0) goto L_0x07e9
            boolean r0 = r8 instanceof X.AnonymousClass1vA
            if (r0 != 0) goto L_0x0740
            boolean r0 = r8 instanceof X.AnonymousClass1JU
            if (r0 != 0) goto L_0x06d9
            boolean r0 = r8 instanceof X.AnonymousClass1vB
            if (r0 != 0) goto L_0x0678
            boolean r0 = r8 instanceof X.AnonymousClass1v5
            if (r0 != 0) goto L_0x05e6
            boolean r0 = r8 instanceof X.AnonymousClass1vC
            if (r0 != 0) goto L_0x0591
            boolean r0 = r8 instanceof X.AnonymousClass1vD
            if (r0 != 0) goto L_0x04e8
            boolean r0 = r8 instanceof X.C37331vE
            if (r0 != 0) goto L_0x047c
            boolean r0 = r8 instanceof X.AnonymousClass1JV
            if (r0 != 0) goto L_0x0451
            boolean r0 = r8 instanceof X.C17020yC
            if (r0 != 0) goto L_0x03f6
            boolean r0 = r8 instanceof X.AnonymousClass10I
            if (r0 != 0) goto L_0x0383
            boolean r0 = r8 instanceof X.AnonymousClass1JW
            if (r0 != 0) goto L_0x0346
            boolean r0 = r8 instanceof X.AnonymousClass1JX
            if (r0 != 0) goto L_0x02f8
            boolean r0 = r8 instanceof X.C14910uL
            if (r0 != 0) goto L_0x0af0
            boolean r0 = r8 instanceof X.AnonymousClass10H
            if (r0 != 0) goto L_0x0863
            boolean r0 = r8 instanceof X.AnonymousClass1II
            if (r0 != 0) goto L_0x02dc
            boolean r0 = r8 instanceof X.C21841Ix
            if (r0 != 0) goto L_0x02b1
            boolean r0 = r8 instanceof X.AnonymousClass1KM
            if (r0 != 0) goto L_0x02f0
            boolean r0 = r8 instanceof X.C16980y8
            if (r0 != 0) goto L_0x021d
            boolean r0 = r8 instanceof X.C17670zH
            if (r0 != 0) goto L_0x0215
            boolean r0 = r8 instanceof X.C15060uf
            if (r0 != 0) goto L_0x01f9
            boolean r0 = r8 instanceof X.AnonymousClass11D
            if (r0 != 0) goto L_0x0166
            boolean r0 = r8 instanceof X.AnonymousClass1JE
            if (r0 != 0) goto L_0x0077
            boolean r0 = X.AnonymousClass07c.shouldCompareStateInIsEquivalentTo
            boolean r0 = A0A(r8, r9, r0)
            return r0
        L_0x0077:
            r2 = r8
            X.1JE r2 = (X.AnonymousClass1JE) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1JE r9 = (X.AnonymousClass1JE) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            com.facebook.common.callercontext.CallerContext r1 = r2.A02
            if (r1 == 0) goto L_0x009f
            com.facebook.common.callercontext.CallerContext r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00a4
            return r3
        L_0x009f:
            com.facebook.common.callercontext.CallerContext r0 = r9.A02
            if (r0 == 0) goto L_0x00a4
            return r3
        L_0x00a4:
            X.DdQ r1 = r2.A04
            if (r1 == 0) goto L_0x00b1
            X.DdQ r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00b6
            return r3
        L_0x00b1:
            X.DdQ r0 = r9.A04
            if (r0 == 0) goto L_0x00b6
            return r3
        L_0x00b6:
            float r1 = r2.A00
            float r0 = r9.A00
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            X.Ddd r1 = r2.A06
            if (r1 == 0) goto L_0x00cd
            X.Ddd r0 = r9.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00d2
            return r3
        L_0x00cd:
            X.Ddd r0 = r9.A06
            if (r0 == 0) goto L_0x00d2
            return r3
        L_0x00d2:
            X.3JC r1 = r2.A08
            if (r1 == 0) goto L_0x00df
            X.3JC r0 = r9.A08
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00e4
            return r3
        L_0x00df:
            X.3JC r0 = r9.A08
            if (r0 == 0) goto L_0x00e4
            return r3
        L_0x00e4:
            android.net.Uri r1 = r2.A01
            if (r1 == 0) goto L_0x00f1
            android.net.Uri r0 = r9.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00f6
            return r3
        L_0x00f1:
            android.net.Uri r0 = r9.A01
            if (r0 == 0) goto L_0x00f6
            return r3
        L_0x00f6:
            X.7Mz r0 = r2.A07
            com.facebook.common.callercontext.CallerContext r1 = r0.callerContextWithContextChain
            if (r1 == 0) goto L_0x0107
            X.7Mz r0 = r9.A07
            com.facebook.common.callercontext.CallerContext r0 = r0.callerContextWithContextChain
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x010e
            return r3
        L_0x0107:
            X.7Mz r0 = r9.A07
            com.facebook.common.callercontext.CallerContext r0 = r0.callerContextWithContextChain
            if (r0 == 0) goto L_0x010e
            return r3
        L_0x010e:
            X.7Mz r0 = r2.A07
            X.DdP r1 = r0.frescoState
            if (r1 == 0) goto L_0x011f
            X.7Mz r0 = r9.A07
            X.DdP r0 = r0.frescoState
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0126
            return r3
        L_0x011f:
            X.7Mz r0 = r9.A07
            X.DdP r0 = r0.frescoState
            if (r0 == 0) goto L_0x0126
            return r3
        L_0x0126:
            X.7Mz r0 = r2.A07
            X.DdP r1 = r0.lastFrescoState
            if (r1 == 0) goto L_0x0137
            X.7Mz r0 = r9.A07
            X.DdP r0 = r0.lastFrescoState
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x013e
            return r3
        L_0x0137:
            X.7Mz r0 = r9.A07
            X.DdP r0 = r0.lastFrescoState
            if (r0 == 0) goto L_0x013e
            return r3
        L_0x013e:
            X.7Mz r0 = r2.A07
            java.util.concurrent.atomic.AtomicReference r1 = r0.prefetchData
            if (r1 == 0) goto L_0x014f
            X.7Mz r0 = r9.A07
            java.util.concurrent.atomic.AtomicReference r0 = r0.prefetchData
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0156
            return r3
        L_0x014f:
            X.7Mz r0 = r9.A07
            java.util.concurrent.atomic.AtomicReference r0 = r0.prefetchData
            if (r0 == 0) goto L_0x0156
            return r3
        L_0x0156:
            com.facebook.common.callercontext.ContextChain r1 = r2.A03
            com.facebook.common.callercontext.ContextChain r0 = r9.A03
            if (r1 == 0) goto L_0x0163
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x0163:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x0166:
            r2 = r8
            X.11D r2 = (X.AnonymousClass11D) r2
            r6 = 1
            if (r2 == r9) goto L_0x02b0
            r5 = 0
            if (r9 == 0) goto L_0x0272
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0272
            X.11D r9 = (X.AnonymousClass11D) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x02b0
            java.util.List r1 = r2.A04
            if (r1 == 0) goto L_0x01b7
            java.util.List r0 = r9.A04
            if (r0 == 0) goto L_0x0272
            int r1 = r1.size()
            java.util.List r0 = r9.A04
            int r0 = r0.size()
            if (r1 != r0) goto L_0x0272
            java.util.List r0 = r2.A04
            int r4 = r0.size()
            r3 = 0
        L_0x019c:
            if (r3 >= r4) goto L_0x01bc
            java.util.List r0 = r2.A04
            java.lang.Object r1 = r0.get(r3)
            X.0zR r1 = (X.C17770zR) r1
            java.util.List r0 = r9.A04
            java.lang.Object r0 = r0.get(r3)
            X.0zR r0 = (X.C17770zR) r0
            boolean r0 = r1.A1L(r0)
            if (r0 == 0) goto L_0x0272
            int r3 = r3 + 1
            goto L_0x019c
        L_0x01b7:
            java.util.List r0 = r9.A04
            if (r0 == 0) goto L_0x01bc
            return r5
        L_0x01bc:
            X.0uO r1 = r2.A01
            if (r1 == 0) goto L_0x01c9
            X.0uO r0 = r9.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01ce
            return r5
        L_0x01c9:
            X.0uO r0 = r9.A01
            if (r0 == 0) goto L_0x01ce
            return r5
        L_0x01ce:
            X.0uO r1 = r2.A00
            if (r1 == 0) goto L_0x01db
            X.0uO r0 = r9.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01e0
            return r5
        L_0x01db:
            X.0uO r0 = r9.A00
            if (r0 == 0) goto L_0x01e0
            return r5
        L_0x01e0:
            X.0uP r1 = r2.A02
            if (r1 == 0) goto L_0x01ed
            X.0uP r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01f2
            return r5
        L_0x01ed:
            X.0uP r0 = r9.A02
            if (r0 == 0) goto L_0x01f2
            return r5
        L_0x01f2:
            boolean r1 = r2.A05
            boolean r0 = r9.A05
            if (r1 == r0) goto L_0x02b0
            return r5
        L_0x01f9:
            r2 = r8
            X.0uf r2 = (X.C15060uf) r2
            if (r2 == r9) goto L_0x02f6
            if (r9 == 0) goto L_0x02ee
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x02ee
            X.0uf r9 = (X.C15060uf) r9
            android.graphics.drawable.Drawable r1 = r2.A02
            android.graphics.drawable.Drawable r0 = r9.A02
            boolean r0 = X.C49982dB.A00(r1, r0)
            return r0
        L_0x0215:
            r1 = r8
            X.0zH r1 = (X.C17670zH) r1
            r0 = 0
            if (r1 != r9) goto L_0x02f7
            goto L_0x02f6
        L_0x021d:
            r2 = r8
            X.0y8 r2 = (X.C16980y8) r2
            r6 = 1
            if (r2 == r9) goto L_0x02b0
            r5 = 0
            if (r9 == 0) goto L_0x0272
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0272
            X.0y8 r9 = (X.C16980y8) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x02b0
            java.util.List r1 = r2.A04
            if (r1 == 0) goto L_0x026e
            java.util.List r0 = r9.A04
            if (r0 == 0) goto L_0x0272
            int r1 = r1.size()
            java.util.List r0 = r9.A04
            int r0 = r0.size()
            if (r1 != r0) goto L_0x0272
            java.util.List r0 = r2.A04
            int r4 = r0.size()
            r3 = 0
        L_0x0253:
            if (r3 >= r4) goto L_0x0273
            java.util.List r0 = r2.A04
            java.lang.Object r1 = r0.get(r3)
            X.0zR r1 = (X.C17770zR) r1
            java.util.List r0 = r9.A04
            java.lang.Object r0 = r0.get(r3)
            X.0zR r0 = (X.C17770zR) r0
            boolean r0 = r1.A1L(r0)
            if (r0 == 0) goto L_0x0272
            int r3 = r3 + 1
            goto L_0x0253
        L_0x026e:
            java.util.List r0 = r9.A04
            if (r0 == 0) goto L_0x0273
        L_0x0272:
            return r5
        L_0x0273:
            X.0uO r1 = r2.A01
            if (r1 == 0) goto L_0x0280
            X.0uO r0 = r9.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0285
            return r5
        L_0x0280:
            X.0uO r0 = r9.A01
            if (r0 == 0) goto L_0x0285
            return r5
        L_0x0285:
            X.0uO r1 = r2.A00
            if (r1 == 0) goto L_0x0292
            X.0uO r0 = r9.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0297
            return r5
        L_0x0292:
            X.0uO r0 = r9.A00
            if (r0 == 0) goto L_0x0297
            return r5
        L_0x0297:
            X.0uP r1 = r2.A02
            if (r1 == 0) goto L_0x02a4
            X.0uP r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02a9
            return r5
        L_0x02a4:
            X.0uP r0 = r9.A02
            if (r0 == 0) goto L_0x02a9
            return r5
        L_0x02a9:
            boolean r1 = r2.A05
            boolean r0 = r9.A05
            if (r1 == r0) goto L_0x02b0
            return r5
        L_0x02b0:
            return r6
        L_0x02b1:
            r4 = r8
            X.1Ix r4 = (X.C21841Ix) r4
            r3 = 1
            if (r4 == r9) goto L_0x08a0
            r2 = 0
            if (r9 == 0) goto L_0x088a
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x088a
            X.1Ix r9 = (X.C21841Ix) r9
            int r1 = r4.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x08a0
            X.0zR r1 = r4.A00
            X.0zR r0 = r9.A00
            if (r1 == 0) goto L_0x02d9
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08a0
            return r2
        L_0x02d9:
            if (r0 == 0) goto L_0x08a0
            return r2
        L_0x02dc:
            r0 = r8
            X.1II r0 = (X.AnonymousClass1II) r0
            if (r0 == r9) goto L_0x02f6
            if (r9 == 0) goto L_0x02ee
            java.lang.Class r1 = r0.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x02ee
            goto L_0x02f6
        L_0x02ee:
            r0 = 0
            return r0
        L_0x02f0:
            r1 = r8
            X.1KM r1 = (X.AnonymousClass1KM) r1
            r0 = 0
            if (r1 != r9) goto L_0x02f7
        L_0x02f6:
            r0 = 1
        L_0x02f7:
            return r0
        L_0x02f8:
            r5 = r8
            X.1JX r5 = (X.AnonymousClass1JX) r5
            r7 = 1
            if (r5 == r9) goto L_0x0345
            r6 = 0
            if (r9 == 0) goto L_0x031f
            java.lang.Class r1 = r5.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x031f
            X.1JX r9 = (X.AnonymousClass1JX) r9
            int r1 = r5.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0345
            java.lang.String r1 = r5.A04
            if (r1 == 0) goto L_0x0320
            java.lang.String r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0325
        L_0x031f:
            return r6
        L_0x0320:
            java.lang.String r0 = r9.A04
            if (r0 == 0) goto L_0x0325
            return r6
        L_0x0325:
            long r3 = r5.A00
            long r1 = r9.A00
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x031f
            long r3 = r5.A01
            long r1 = r9.A01
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x031f
            java.lang.String r1 = r5.A05
            java.lang.String r0 = r9.A05
            if (r1 == 0) goto L_0x0342
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0345
            return r6
        L_0x0342:
            if (r0 == 0) goto L_0x0345
            return r6
        L_0x0345:
            return r7
        L_0x0346:
            r4 = r8
            X.1JW r4 = (X.AnonymousClass1JW) r4
            r3 = 1
            if (r4 == r9) goto L_0x08a0
            r2 = 0
            if (r9 == 0) goto L_0x088a
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x088a
            X.1JW r9 = (X.AnonymousClass1JW) r9
            int r1 = r4.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x08a0
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r4.A02
            if (r1 == 0) goto L_0x036e
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0373
            return r2
        L_0x036e:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A02
            if (r0 == 0) goto L_0x0373
            return r2
        L_0x0373:
            X.2Fx r1 = r4.A01
            X.2Fx r0 = r9.A01
            if (r1 == 0) goto L_0x0380
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08a0
            return r2
        L_0x0380:
            if (r0 == 0) goto L_0x08a0
            return r2
        L_0x0383:
            r2 = r8
            X.10I r2 = (X.AnonymousClass10I) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.10I r9 = (X.AnonymousClass10I) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r2.A05
            if (r1 == 0) goto L_0x03ab
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03b0
            return r3
        L_0x03ab:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A05
            if (r0 == 0) goto L_0x03b0
            return r3
        L_0x03b0:
            java.lang.String r1 = r2.A07
            if (r1 == 0) goto L_0x03bd
            java.lang.String r0 = r9.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03c2
            return r3
        L_0x03bd:
            java.lang.String r0 = r9.A07
            if (r0 == 0) goto L_0x03c2
            return r3
        L_0x03c2:
            java.lang.CharSequence r1 = r2.A06
            if (r1 == 0) goto L_0x03cf
            java.lang.CharSequence r0 = r9.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03d4
            return r3
        L_0x03cf:
            java.lang.CharSequence r0 = r9.A06
            if (r0 == 0) goto L_0x03d4
            return r3
        L_0x03d4:
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 != r0) goto L_0x0b1d
            boolean r1 = r2.A08
            boolean r0 = r9.A08
            if (r1 != r0) goto L_0x0b1d
            boolean r1 = r2.A09
            boolean r0 = r9.A09
            if (r1 != r0) goto L_0x0b1d
            X.9gP r1 = r2.A04
            X.9gP r0 = r9.A04
            if (r1 == 0) goto L_0x03f3
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x03f3:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x03f6:
            r2 = r8
            X.0yC r2 = (X.C17020yC) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.0yC r9 = (X.C17020yC) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            boolean r1 = r2.A06
            boolean r0 = r9.A06
            if (r1 != r0) goto L_0x0b1d
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r2.A04
            if (r1 == 0) goto L_0x0424
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0429
            return r3
        L_0x0424:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A04
            if (r0 == 0) goto L_0x0429
            return r3
        L_0x0429:
            java.lang.CharSequence r1 = r2.A05
            if (r1 == 0) goto L_0x0436
            java.lang.CharSequence r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x043b
            return r3
        L_0x0436:
            java.lang.CharSequence r0 = r9.A05
            if (r0 == 0) goto L_0x043b
            return r3
        L_0x043b:
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 != r0) goto L_0x0b1d
            X.2Nf r1 = r2.A03
            X.2Nf r0 = r9.A03
            if (r1 == 0) goto L_0x044e
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x044e:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x0451:
            r4 = r8
            X.1JV r4 = (X.AnonymousClass1JV) r4
            r3 = 1
            if (r4 == r9) goto L_0x08a0
            r2 = 0
            if (r9 == 0) goto L_0x088a
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x088a
            X.1JV r9 = (X.AnonymousClass1JV) r9
            int r1 = r4.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x08a0
            java.lang.String r1 = r4.A01
            java.lang.String r0 = r9.A01
            if (r1 == 0) goto L_0x0479
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08a0
            return r2
        L_0x0479:
            if (r0 == 0) goto L_0x08a0
            return r2
        L_0x047c:
            r2 = r8
            X.1vE r2 = (X.C37331vE) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1vE r9 = (X.C37331vE) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            X.4O9 r1 = r2.A05
            if (r1 == 0) goto L_0x04a4
            X.4O9 r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04a9
            return r3
        L_0x04a4:
            X.4O9 r0 = r9.A05
            if (r0 == 0) goto L_0x04a9
            return r3
        L_0x04a9:
            com.facebook.ipc.stories.model.AudienceControlData r1 = r2.A03
            if (r1 == 0) goto L_0x04b6
            com.facebook.ipc.stories.model.AudienceControlData r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04bb
            return r3
        L_0x04b6:
            com.facebook.ipc.stories.model.AudienceControlData r0 = r9.A03
            if (r0 == 0) goto L_0x04bb
            return r3
        L_0x04bb:
            com.facebook.ipc.stories.model.StoryCard r1 = r2.A04
            if (r1 == 0) goto L_0x04c8
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04cd
            return r3
        L_0x04c8:
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A04
            if (r0 == 0) goto L_0x04cd
            return r3
        L_0x04cd:
            X.3gF r1 = r2.A02
            if (r1 == 0) goto L_0x04da
            X.3gF r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04df
            return r3
        L_0x04da:
            X.3gF r0 = r9.A02
            if (r0 == 0) goto L_0x04df
            return r3
        L_0x04df:
            java.lang.String r1 = r2.A06
            java.lang.String r0 = r9.A06
            if (r1 != 0) goto L_0x07e2
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x04e8:
            r2 = r8
            X.1vD r2 = (X.AnonymousClass1vD) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1vD r9 = (X.AnonymousClass1vD) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            com.facebook.ipc.stories.model.StoryBucket r1 = r2.A02
            if (r1 == 0) goto L_0x0510
            com.facebook.ipc.stories.model.StoryBucket r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0515
            return r3
        L_0x0510:
            com.facebook.ipc.stories.model.StoryBucket r0 = r9.A02
            if (r0 == 0) goto L_0x0515
            return r3
        L_0x0515:
            X.4Jw r1 = r2.A04
            if (r1 == 0) goto L_0x0522
            X.4Jw r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0527
            return r3
        L_0x0522:
            X.4Jw r0 = r9.A04
            if (r0 == 0) goto L_0x0527
            return r3
        L_0x0527:
            java.lang.String r1 = r2.A08
            if (r1 == 0) goto L_0x0534
            java.lang.String r0 = r9.A08
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0539
            return r3
        L_0x0534:
            java.lang.String r0 = r9.A08
            if (r0 == 0) goto L_0x0539
            return r3
        L_0x0539:
            X.4Rw r1 = r2.A05
            if (r1 == 0) goto L_0x0546
            X.4Rw r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x054b
            return r3
        L_0x0546:
            X.4Rw r0 = r9.A05
            if (r0 == 0) goto L_0x054b
            return r3
        L_0x054b:
            com.facebook.ipc.stories.model.StoryCard r1 = r2.A03
            if (r1 == 0) goto L_0x0558
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x055d
            return r3
        L_0x0558:
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A03
            if (r0 == 0) goto L_0x055d
            return r3
        L_0x055d:
            X.3gF r1 = r2.A01
            if (r1 == 0) goto L_0x056a
            X.3gF r0 = r9.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x056f
            return r3
        L_0x056a:
            X.3gF r0 = r9.A01
            if (r0 == 0) goto L_0x056f
            return r3
        L_0x056f:
            X.46A r1 = r2.A07
            if (r1 == 0) goto L_0x057c
            X.46A r0 = r9.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0581
            return r3
        L_0x057c:
            X.46A r0 = r9.A07
            if (r0 == 0) goto L_0x0581
            return r3
        L_0x0581:
            X.4O4 r1 = r2.A06
            X.4O4 r0 = r9.A06
            if (r1 == 0) goto L_0x058e
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x058e:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x0591:
            r2 = r8
            X.1vC r2 = (X.AnonymousClass1vC) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1vC r9 = (X.AnonymousClass1vC) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 != r0) goto L_0x0b1d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r2.A03
            if (r1 == 0) goto L_0x05bf
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x05c4
            return r3
        L_0x05bf:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r9.A03
            if (r0 == 0) goto L_0x05c4
            return r3
        L_0x05c4:
            X.44o r1 = r2.A05
            if (r1 == 0) goto L_0x05d1
            X.44o r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x05d6
            return r3
        L_0x05d1:
            X.44o r0 = r9.A05
            if (r0 == 0) goto L_0x05d6
            return r3
        L_0x05d6:
            android.view.View$OnTouchListener r1 = r2.A01
            android.view.View$OnTouchListener r0 = r9.A01
            if (r1 == 0) goto L_0x05e3
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x05e3:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x05e6:
            r2 = r8
            X.1v5 r2 = (X.AnonymousClass1v5) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1v5 r9 = (X.AnonymousClass1v5) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            X.4Jw r1 = r2.A04
            if (r1 == 0) goto L_0x060e
            X.4Jw r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0613
            return r3
        L_0x060e:
            X.4Jw r0 = r9.A04
            if (r0 == 0) goto L_0x0613
            return r3
        L_0x0613:
            X.4Ky r1 = r2.A06
            if (r1 == 0) goto L_0x0620
            X.4Ky r0 = r9.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0625
            return r3
        L_0x0620:
            X.4Ky r0 = r9.A06
            if (r0 == 0) goto L_0x0625
            return r3
        L_0x0625:
            X.463 r1 = r2.A08
            if (r1 == 0) goto L_0x0632
            X.463 r0 = r9.A08
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0637
            return r3
        L_0x0632:
            X.463 r0 = r9.A08
            if (r0 == 0) goto L_0x0637
            return r3
        L_0x0637:
            com.facebook.ipc.stories.model.StoryCard r1 = r2.A03
            if (r1 == 0) goto L_0x0644
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0649
            return r3
        L_0x0644:
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A03
            if (r0 == 0) goto L_0x0649
            return r3
        L_0x0649:
            X.3gF r1 = r2.A02
            if (r1 == 0) goto L_0x0656
            X.3gF r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x065b
            return r3
        L_0x0656:
            X.3gF r0 = r9.A02
            if (r0 == 0) goto L_0x065b
            return r3
        L_0x065b:
            X.46A r1 = r2.A07
            if (r1 == 0) goto L_0x0668
            X.46A r0 = r9.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x066d
            return r3
        L_0x0668:
            X.46A r0 = r9.A07
            if (r0 == 0) goto L_0x066d
            return r3
        L_0x066d:
            X.5CS r0 = r2.A05
            boolean r1 = r0.shouldAnimateScale
            X.5CS r0 = r9.A05
            boolean r0 = r0.shouldAnimateScale
            if (r1 == r0) goto L_0x0cac
            return r3
        L_0x0678:
            r2 = r8
            X.1vB r2 = (X.AnonymousClass1vB) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1vB r9 = (X.AnonymousClass1vB) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            X.4Jw r1 = r2.A02
            if (r1 == 0) goto L_0x06a0
            X.4Jw r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x06a5
            return r3
        L_0x06a0:
            X.4Jw r0 = r9.A02
            if (r0 == 0) goto L_0x06a5
            return r3
        L_0x06a5:
            X.4gh r1 = r2.A03
            if (r1 == 0) goto L_0x06b2
            X.4gh r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x06b7
            return r3
        L_0x06b2:
            X.4gh r0 = r9.A03
            if (r0 == 0) goto L_0x06b7
            return r3
        L_0x06b7:
            com.facebook.ipc.stories.model.StoryCard r1 = r2.A01
            if (r1 == 0) goto L_0x06c4
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x06c9
            return r3
        L_0x06c4:
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A01
            if (r0 == 0) goto L_0x06c9
            return r3
        L_0x06c9:
            X.46A r1 = r2.A04
            X.46A r0 = r9.A04
            if (r1 == 0) goto L_0x06d6
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x06d6:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x06d9:
            r2 = r8
            X.1JU r2 = (X.AnonymousClass1JU) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1JU r9 = (X.AnonymousClass1JU) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            com.facebook.ipc.stories.model.StoryBucket r1 = r2.A02
            if (r1 == 0) goto L_0x0701
            com.facebook.ipc.stories.model.StoryBucket r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0706
            return r3
        L_0x0701:
            com.facebook.ipc.stories.model.StoryBucket r0 = r9.A02
            if (r0 == 0) goto L_0x0706
            return r3
        L_0x0706:
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 != r0) goto L_0x0b1d
            com.facebook.ipc.stories.model.StoryCard r1 = r2.A03
            if (r1 == 0) goto L_0x0719
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x071e
            return r3
        L_0x0719:
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A03
            if (r0 == 0) goto L_0x071e
            return r3
        L_0x071e:
            X.4cr r1 = r2.A04
            if (r1 == 0) goto L_0x072b
            X.4cr r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0730
            return r3
        L_0x072b:
            X.4cr r0 = r9.A04
            if (r0 == 0) goto L_0x0730
            return r3
        L_0x0730:
            X.463 r1 = r2.A05
            X.463 r0 = r9.A05
            if (r1 == 0) goto L_0x073d
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x073d:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x0740:
            r2 = r8
            X.1vA r2 = (X.AnonymousClass1vA) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1vA r9 = (X.AnonymousClass1vA) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            X.4O9 r1 = r2.A06
            if (r1 == 0) goto L_0x0768
            X.4O9 r0 = r9.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x076d
            return r3
        L_0x0768:
            X.4O9 r0 = r9.A06
            if (r0 == 0) goto L_0x076d
            return r3
        L_0x076d:
            com.facebook.ipc.stories.model.StoryCard r1 = r2.A02
            if (r1 == 0) goto L_0x077a
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x077f
            return r3
        L_0x077a:
            com.facebook.ipc.stories.model.StoryCard r0 = r9.A02
            if (r0 == 0) goto L_0x077f
            return r3
        L_0x077f:
            java.lang.String r1 = r2.A07
            if (r1 == 0) goto L_0x078c
            java.lang.String r0 = r9.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0791
            return r3
        L_0x078c:
            java.lang.String r0 = r9.A07
            if (r0 == 0) goto L_0x0791
            return r3
        L_0x0791:
            X.3gF r1 = r2.A01
            if (r1 == 0) goto L_0x079e
            X.3gF r0 = r9.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x07a3
            return r3
        L_0x079e:
            X.3gF r0 = r9.A01
            if (r0 == 0) goto L_0x07a3
            return r3
        L_0x07a3:
            X.43z r1 = r2.A03
            if (r1 == 0) goto L_0x07b0
            X.43z r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x07b5
            return r3
        L_0x07b0:
            X.43z r0 = r9.A03
            if (r0 == 0) goto L_0x07b5
            return r3
        L_0x07b5:
            X.46A r1 = r2.A05
            if (r1 == 0) goto L_0x07c2
            X.46A r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x07c7
            return r3
        L_0x07c2:
            X.46A r0 = r9.A05
            if (r0 == 0) goto L_0x07c7
            return r3
        L_0x07c7:
            X.4O4 r1 = r2.A04
            if (r1 == 0) goto L_0x07d4
            X.4O4 r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x07d9
            return r3
        L_0x07d4:
            X.4O4 r0 = r9.A04
            if (r0 == 0) goto L_0x07d9
            return r3
        L_0x07d9:
            java.lang.String r1 = r2.A08
            java.lang.String r0 = r9.A08
            if (r1 != 0) goto L_0x07e2
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x07e2:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x07e9:
            r4 = r8
            X.1KF r4 = (X.AnonymousClass1KF) r4
            r3 = 1
            if (r4 == r9) goto L_0x08a0
            r2 = 0
            if (r9 == 0) goto L_0x088a
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x088a
            X.1KF r9 = (X.AnonymousClass1KF) r9
            int r1 = r4.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x08a0
            com.google.common.collect.ImmutableList r1 = r4.A02
            if (r1 == 0) goto L_0x0811
            com.google.common.collect.ImmutableList r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0816
            return r2
        L_0x0811:
            com.google.common.collect.ImmutableList r0 = r9.A02
            if (r0 == 0) goto L_0x0816
            return r2
        L_0x0816:
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r4.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A01
            if (r1 == 0) goto L_0x0823
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08a0
            return r2
        L_0x0823:
            if (r0 == 0) goto L_0x08a0
            return r2
        L_0x0826:
            r4 = r8
            X.1ku r4 = (X.C31981ku) r4
            r3 = 1
            if (r4 == r9) goto L_0x08a0
            r2 = 0
            if (r9 == 0) goto L_0x088a
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x088a
            X.1ku r9 = (X.C31981ku) r9
            int r1 = r4.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x08a0
            com.google.common.collect.ImmutableList r1 = r4.A02
            if (r1 == 0) goto L_0x084e
            com.google.common.collect.ImmutableList r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0853
            return r2
        L_0x084e:
            com.google.common.collect.ImmutableList r0 = r9.A02
            if (r0 == 0) goto L_0x0853
            return r2
        L_0x0853:
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r4.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r9.A01
            if (r1 == 0) goto L_0x0860
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08a0
            return r2
        L_0x0860:
            if (r0 == 0) goto L_0x08a0
            return r2
        L_0x0863:
            r4 = r8
            X.10H r4 = (X.AnonymousClass10H) r4
            r3 = 1
            if (r4 == r9) goto L_0x08a0
            r2 = 0
            if (r9 == 0) goto L_0x088a
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x088a
            X.10H r9 = (X.AnonymousClass10H) r9
            int r1 = r4.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x08a0
            android.graphics.drawable.Drawable r1 = r4.A00
            if (r1 == 0) goto L_0x088b
            android.graphics.drawable.Drawable r0 = r9.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0890
        L_0x088a:
            return r2
        L_0x088b:
            android.graphics.drawable.Drawable r0 = r9.A00
            if (r0 == 0) goto L_0x0890
            return r2
        L_0x0890:
            android.widget.ImageView$ScaleType r1 = r4.A01
            android.widget.ImageView$ScaleType r0 = r9.A01
            if (r1 == 0) goto L_0x089d
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08a0
            return r2
        L_0x089d:
            if (r0 == 0) goto L_0x08a0
            return r2
        L_0x08a0:
            return r3
        L_0x08a1:
            r2 = r8
            X.1I8 r2 = (X.AnonymousClass1I8) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1I8 r9 = (X.AnonymousClass1I8) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            X.1I9 r1 = r2.A03
            if (r1 == 0) goto L_0x08c9
            X.1I9 r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08ce
            return r3
        L_0x08c9:
            X.1I9 r0 = r9.A03
            if (r0 == 0) goto L_0x08ce
            return r3
        L_0x08ce:
            java.lang.CharSequence r1 = r2.A04
            if (r1 == 0) goto L_0x08db
            java.lang.CharSequence r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08e0
            return r3
        L_0x08db:
            java.lang.CharSequence r0 = r9.A04
            if (r0 == 0) goto L_0x08e0
            return r3
        L_0x08e0:
            java.lang.Integer r1 = r2.A05
            if (r1 == 0) goto L_0x08ed
            java.lang.Integer r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x08f2
            return r3
        L_0x08ed:
            java.lang.Integer r0 = r9.A05
            if (r0 == 0) goto L_0x08f2
            return r3
        L_0x08f2:
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A01
            int r0 = r9.A01
            if (r1 != r0) goto L_0x0b1d
            android.widget.ImageView$ScaleType r1 = r2.A02
            if (r1 == 0) goto L_0x090b
            android.widget.ImageView$ScaleType r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0910
            return r3
        L_0x090b:
            android.widget.ImageView$ScaleType r0 = r9.A02
            if (r0 == 0) goto L_0x0910
            return r3
        L_0x0910:
            boolean r1 = r2.A06
            boolean r0 = r9.A06
            if (r1 == r0) goto L_0x0cac
            return r3
        L_0x0917:
            r2 = r8
            X.1NM r2 = (X.AnonymousClass1NM) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1NM r9 = (X.AnonymousClass1NM) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            android.graphics.drawable.Drawable r1 = r2.A00
            if (r1 == 0) goto L_0x093f
            android.graphics.drawable.Drawable r0 = r9.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0944
            return r3
        L_0x093f:
            android.graphics.drawable.Drawable r0 = r9.A00
            if (r0 == 0) goto L_0x0944
            return r3
        L_0x0944:
            X.0zR r1 = r2.A01
            if (r1 == 0) goto L_0x0951
            X.0zR r0 = r9.A01
            boolean r0 = r1.A1L(r0)
            if (r0 != 0) goto L_0x0956
            return r3
        L_0x0951:
            X.0zR r0 = r9.A01
            if (r0 == 0) goto L_0x0956
            return r3
        L_0x0956:
            X.0zR r1 = r2.A02
            if (r1 == 0) goto L_0x0963
            X.0zR r0 = r9.A02
            boolean r0 = r1.A1L(r0)
            if (r0 != 0) goto L_0x0968
            return r3
        L_0x0963:
            X.0zR r0 = r9.A02
            if (r0 == 0) goto L_0x0968
            return r3
        L_0x0968:
            X.1MF r1 = r2.A09
            if (r1 == 0) goto L_0x0975
            X.1MF r0 = r9.A09
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x097a
            return r3
        L_0x0975:
            X.1MF r0 = r9.A09
            if (r0 == 0) goto L_0x097a
            return r3
        L_0x097a:
            X.0zR r1 = r2.A03
            if (r1 == 0) goto L_0x0989
            X.0zR r0 = r9.A03
            boolean r0 = r1.A1L(r0)     // Catch:{ all -> 0x0987 }
            if (r0 != 0) goto L_0x098e
            return r3
        L_0x0987:
            r0 = move-exception
            throw r0
        L_0x0989:
            X.0zR r0 = r9.A03
            if (r0 == 0) goto L_0x098e
            return r3
        L_0x098e:
            java.lang.String r1 = r2.A0K
            if (r1 == 0) goto L_0x099b
            java.lang.String r0 = r9.A0K
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x09a0
            return r3
        L_0x099b:
            java.lang.String r0 = r9.A0K
            if (r0 == 0) goto L_0x09a0
            return r3
        L_0x09a0:
            X.1MD r1 = r2.A0A
            if (r1 == 0) goto L_0x09ad
            X.1MD r0 = r9.A0A
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x09b2
            return r3
        L_0x09ad:
            X.1MD r0 = r9.A0A
            if (r0 == 0) goto L_0x09b2
            return r3
        L_0x09b2:
            X.1BY r1 = r2.A08
            X.1BY r0 = r9.A08
            if (r1 == 0) goto L_0x09bf
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x09bf:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x09c2:
            r2 = r8
            X.1v9 r2 = (X.AnonymousClass1v9) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1v9 r9 = (X.AnonymousClass1v9) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            java.lang.String r1 = r2.A03
            if (r1 == 0) goto L_0x09ea
            java.lang.String r0 = r9.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x09ef
            return r3
        L_0x09ea:
            java.lang.String r0 = r9.A03
            if (r0 == 0) goto L_0x09ef
            return r3
        L_0x09ef:
            X.0zZ r1 = r2.A02
            if (r1 == 0) goto L_0x09fc
            X.0zZ r0 = r9.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0a01
            return r3
        L_0x09fc:
            X.0zZ r0 = r9.A02
            if (r0 == 0) goto L_0x0a01
            return r3
        L_0x0a01:
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 != r0) goto L_0x0b1d
            android.view.View$OnClickListener r1 = r2.A01
            android.view.View$OnClickListener r0 = r9.A01
            if (r1 == 0) goto L_0x0a14
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x0a14:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x0a17:
            r2 = r8
            X.1KX r2 = (X.AnonymousClass1KX) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.1KX r9 = (X.AnonymousClass1KX) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            boolean r1 = r2.A0I
            boolean r0 = r9.A0I
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A02
            int r0 = r9.A02
            if (r1 != r0) goto L_0x0b1d
            X.1KZ r1 = r2.A0C
            if (r1 == 0) goto L_0x0a4b
            X.1KZ r0 = r9.A0C
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0a50
            return r3
        L_0x0a4b:
            X.1KZ r0 = r9.A0C
            if (r0 == 0) goto L_0x0a50
            return r3
        L_0x0a50:
            boolean r1 = r2.A0J
            boolean r0 = r9.A0J
            if (r1 != r0) goto L_0x0b1d
            android.graphics.Path r1 = r2.A06
            if (r1 == 0) goto L_0x0a63
            android.graphics.Path r0 = r9.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0a68
            return r3
        L_0x0a63:
            android.graphics.Path r0 = r9.A06
            if (r0 == 0) goto L_0x0a68
            return r3
        L_0x0a68:
            int r1 = r2.A03
            int r0 = r9.A03
            if (r1 != r0) goto L_0x0b1d
            android.graphics.drawable.Drawable r1 = r2.A08
            if (r1 == 0) goto L_0x0a7b
            android.graphics.drawable.Drawable r0 = r9.A08
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0a80
            return r3
        L_0x0a7b:
            android.graphics.drawable.Drawable r0 = r9.A08
            if (r0 == 0) goto L_0x0a80
            return r3
        L_0x0a80:
            X.1JY r1 = r2.A0B
            if (r1 == 0) goto L_0x0a8d
            X.1JY r0 = r9.A0B
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0a92
            return r3
        L_0x0a8d:
            X.1JY r0 = r9.A0B
            if (r0 == 0) goto L_0x0a92
            return r3
        L_0x0a92:
            boolean r1 = r2.A0K
            boolean r0 = r9.A0K
            if (r1 != r0) goto L_0x0b1d
            float r1 = r2.A00
            float r0 = r9.A00
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            int r1 = r2.A04
            int r0 = r9.A04
            if (r1 != r0) goto L_0x0b1d
            X.1KU r1 = r2.A0D
            if (r1 == 0) goto L_0x0ab5
            X.1KU r0 = r9.A0D
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0aba
            return r3
        L_0x0ab5:
            X.1KU r0 = r9.A0D
            if (r0 == 0) goto L_0x0aba
            return r3
        L_0x0aba:
            int r1 = r2.A05
            int r0 = r9.A05
            if (r1 != r0) goto L_0x0b1d
            float r1 = r2.A01
            float r0 = r9.A01
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            android.graphics.Typeface r1 = r2.A07
            if (r1 == 0) goto L_0x0ad7
            android.graphics.Typeface r0 = r9.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0adc
            return r3
        L_0x0ad7:
            android.graphics.Typeface r0 = r9.A07
            if (r0 == 0) goto L_0x0adc
            return r3
        L_0x0adc:
            X.1KY r0 = r2.A0A
            X.1Kc r1 = r0.userTileDrawableCachingBuilder
            X.1KY r0 = r9.A0A
            X.1Kc r0 = r0.userTileDrawableCachingBuilder
            if (r1 == 0) goto L_0x0aed
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x0aed:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x0af0:
            r2 = r8
            X.0uL r2 = (X.C14910uL) r2
            r4 = 1
            if (r2 == r9) goto L_0x0cac
            r3 = 0
            if (r9 == 0) goto L_0x0b1d
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r9.getClass()
            if (r1 != r0) goto L_0x0b1d
            X.0uL r9 = (X.C14910uL) r9
            int r1 = r2.A00
            int r0 = r9.A00
            if (r1 == r0) goto L_0x0cac
            boolean r1 = r2.A0g
            boolean r0 = r9.A0g
            if (r1 != r0) goto L_0x0b1d
            X.1JS r1 = r2.A0X
            if (r1 == 0) goto L_0x0b1e
            X.1JS r0 = r9.A0X
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0b23
        L_0x0b1d:
            return r3
        L_0x0b1e:
            X.1JS r0 = r9.A0X
            if (r0 == 0) goto L_0x0b23
            return r3
        L_0x0b23:
            int r1 = r2.A07
            int r0 = r9.A07
            if (r1 != r0) goto L_0x0b1d
            float r1 = r2.A00
            float r0 = r9.A00
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            boolean r1 = r2.A0h
            boolean r0 = r9.A0h
            if (r1 != r0) goto L_0x0b1d
            java.lang.CharSequence r1 = r2.A0a
            if (r1 == 0) goto L_0x0b46
            java.lang.CharSequence r0 = r9.A0a
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0b4b
            return r3
        L_0x0b46:
            java.lang.CharSequence r0 = r9.A0a
            if (r0 == 0) goto L_0x0b4b
            return r3
        L_0x0b4b:
            android.text.TextUtils$TruncateAt r1 = r2.A0U
            if (r1 == 0) goto L_0x0b58
            android.text.TextUtils$TruncateAt r0 = r9.A0U
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0b5d
            return r3
        L_0x0b58:
            android.text.TextUtils$TruncateAt r0 = r9.A0U
            if (r0 == 0) goto L_0x0b5d
            return r3
        L_0x0b5d:
            float r1 = r2.A01
            float r0 = r9.A01
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            boolean r1 = r2.A0i
            boolean r0 = r9.A0i
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A08
            int r0 = r9.A08
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A09
            int r0 = r9.A09
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0A
            int r0 = r9.A0A
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0B
            int r0 = r9.A0B
            if (r1 != r0) goto L_0x0b1d
            boolean r1 = r2.A0j
            boolean r0 = r9.A0j
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0C
            int r0 = r9.A0C
            if (r1 != r0) goto L_0x0b1d
            r1 = 0
            int r0 = java.lang.Float.compare(r1, r1)
            if (r0 != 0) goto L_0x0b1d
            float r1 = r2.A02
            float r0 = r9.A02
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            int r1 = r2.A0D
            int r0 = r9.A0D
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0E
            int r0 = r9.A0E
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0F
            int r0 = r9.A0F
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0G
            int r0 = r9.A0G
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0H
            int r0 = r9.A0H
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0I
            int r0 = r9.A0I
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0J
            int r0 = r9.A0J
            if (r1 != r0) goto L_0x0b1d
            boolean r1 = r2.A0k
            boolean r0 = r9.A0k
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0K
            int r0 = r9.A0K
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0L
            int r0 = r9.A0L
            if (r1 != r0) goto L_0x0b1d
            float r1 = r2.A03
            float r0 = r9.A03
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            float r1 = r2.A04
            float r0 = r9.A04
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            float r1 = r2.A05
            float r0 = r9.A05
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            boolean r1 = r2.A0l
            boolean r0 = r9.A0l
            if (r1 != r0) goto L_0x0b1d
            float r1 = r2.A06
            float r0 = r9.A06
            int r0 = java.lang.Float.compare(r1, r0)
            if (r0 != 0) goto L_0x0b1d
            X.2CC r1 = r2.A0Z
            if (r1 == 0) goto L_0x0c19
            X.2CC r0 = r9.A0Z
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0c1e
            return r3
        L_0x0c19:
            X.2CC r0 = r9.A0Z
            if (r0 == 0) goto L_0x0c1e
            return r3
        L_0x0c1e:
            java.lang.CharSequence r1 = r2.A0c
            if (r1 == 0) goto L_0x0c2b
            java.lang.CharSequence r0 = r9.A0c
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0c30
            return r3
        L_0x0c2b:
            java.lang.CharSequence r0 = r9.A0c
            if (r0 == 0) goto L_0x0c30
            return r3
        L_0x0c30:
            android.text.Layout$Alignment r1 = r2.A0R
            if (r1 == 0) goto L_0x0c3d
            android.text.Layout$Alignment r0 = r9.A0R
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0c42
            return r3
        L_0x0c3d:
            android.text.Layout$Alignment r0 = r9.A0R
            if (r0 == 0) goto L_0x0c42
            return r3
        L_0x0c42:
            int r1 = r2.A0M
            int r0 = r9.A0M
            if (r1 != r0) goto L_0x0b1d
            android.content.res.ColorStateList r1 = r2.A0P
            if (r1 == 0) goto L_0x0c55
            android.content.res.ColorStateList r0 = r9.A0P
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0c5a
            return r3
        L_0x0c55:
            android.content.res.ColorStateList r0 = r9.A0P
            if (r0 == 0) goto L_0x0c5a
            return r3
        L_0x0c5a:
            X.1Kv r1 = r2.A0V
            if (r1 == 0) goto L_0x0c67
            X.1Kv r0 = r9.A0V
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0c6c
            return r3
        L_0x0c67:
            X.1Kv r0 = r9.A0V
            if (r0 == 0) goto L_0x0c6c
            return r3
        L_0x0c6c:
            X.10N r1 = r2.A0W
            if (r1 == 0) goto L_0x0c79
            X.10N r0 = r9.A0W
            boolean r0 = r1.A01(r0)
            if (r0 != 0) goto L_0x0c7e
            return r3
        L_0x0c79:
            X.10N r0 = r9.A0W
            if (r0 == 0) goto L_0x0c7e
            return r3
        L_0x0c7e:
            int r1 = r2.A0N
            int r0 = r9.A0N
            if (r1 != r0) goto L_0x0b1d
            int r1 = r2.A0O
            int r0 = r9.A0O
            if (r1 != r0) goto L_0x0b1d
            android.graphics.Typeface r1 = r2.A0Q
            if (r1 == 0) goto L_0x0c97
            android.graphics.Typeface r0 = r9.A0Q
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0c9c
            return r3
        L_0x0c97:
            android.graphics.Typeface r0 = r9.A0Q
            if (r0 == 0) goto L_0x0c9c
            return r3
        L_0x0c9c:
            X.0uM r1 = r2.A0Y
            X.0uM r0 = r9.A0Y
            if (r1 == 0) goto L_0x0ca9
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0cac
            return r3
        L_0x0ca9:
            if (r0 == 0) goto L_0x0cac
            return r3
        L_0x0cac:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17770zR.A1L(X.0zR):boolean");
    }
}
