package X;

import android.view.View;

/* renamed from: X.0zd  reason: invalid class name and case insensitive filesystem */
public final class C17880zd implements View.OnClickListener {
    public AnonymousClass10N A00;

    public void onClick(View view) {
        int A05 = C000700l.A05(1773592086);
        AnonymousClass10N r2 = this.A00;
        if (r2 != null) {
            if (C33181nA.A00 == null) {
                C33181nA.A00 = new C48132Zq();
            }
            C48132Zq r1 = C33181nA.A00;
            r1.A00 = view;
            r2.A00.Alq().AXa(r2, r1);
            C33181nA.A00.A00 = null;
        }
        C000700l.A0B(495625439, A05);
    }
}
