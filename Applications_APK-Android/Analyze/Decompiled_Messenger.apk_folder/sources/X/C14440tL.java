package X;

import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.base.Predicate;

/* renamed from: X.0tL  reason: invalid class name and case insensitive filesystem */
public final class C14440tL implements Predicate {
    public boolean apply(Object obj) {
        ThreadSummary threadSummary = (ThreadSummary) obj;
        if (threadSummary == null || !threadSummary.A17) {
            return false;
        }
        return true;
    }
}
