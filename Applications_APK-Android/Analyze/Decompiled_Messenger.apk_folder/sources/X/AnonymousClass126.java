package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.126  reason: invalid class name */
public final class AnonymousClass126 {
    public Map A00 = null;
    private final Integer A01;
    private final Integer A02;

    public Map A00() {
        Map map = this.A00;
        if (map != null) {
            return map;
        }
        HashMap hashMap = new HashMap();
        this.A00 = hashMap;
        Integer num = this.A01;
        if (num != null) {
            hashMap.put("device_auto_time_setting", String.valueOf(num));
        }
        Integer num2 = this.A02;
        if (num2 != null) {
            this.A00.put("device_auto_time_zone_setting", String.valueOf(num2));
        }
        return this.A00;
    }

    public AnonymousClass126(Integer num, Integer num2) {
        this.A01 = num;
        this.A02 = num2;
    }
}
