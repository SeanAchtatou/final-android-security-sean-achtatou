package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1C3  reason: invalid class name */
public final class AnonymousClass1C3 extends AnonymousClass0W4 {
    public static final ImmutableList A00;
    private static final C06060am A01;

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    static {
        AnonymousClass0W6 r2 = C30611iM.A02;
        A01 = new C06050al(ImmutableList.of(r2));
        A00 = ImmutableList.of(r2, C30611iM.A00, C30611iM.A01);
    }

    public AnonymousClass1C3() {
        super(C99084oO.$const$string(26), A00, A01);
    }
}
