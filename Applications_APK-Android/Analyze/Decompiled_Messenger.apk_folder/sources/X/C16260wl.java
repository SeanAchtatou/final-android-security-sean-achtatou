package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.common.dextricks.DexStore;

/* renamed from: X.0wl  reason: invalid class name and case insensitive filesystem */
public class C16260wl extends C15220uv {
    public final C15220uv A00 = new C16270wm(this);
    public final RecyclerView A01;

    public C16260wl(RecyclerView recyclerView) {
        this.A01 = recyclerView;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0027 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0D(android.view.View r6, int r7, android.os.Bundle r8) {
        /*
            r5 = this;
            boolean r0 = super.A0D(r6, r7, r8)
            if (r0 == 0) goto L_0x0008
            r0 = 1
            return r0
        L_0x0008:
            androidx.recyclerview.widget.RecyclerView r0 = r5.A01
            boolean r0 = r0.A1B()
            if (r0 != 0) goto L_0x0087
            androidx.recyclerview.widget.RecyclerView r0 = r5.A01
            X.19T r3 = r0.A0L
            if (r3 == 0) goto L_0x0087
            androidx.recyclerview.widget.RecyclerView r2 = r3.A08
            if (r2 == 0) goto L_0x0029
            r0 = 4096(0x1000, float:5.74E-42)
            r1 = 1
            if (r7 == r0) goto L_0x0057
            r0 = 8192(0x2000, float:1.14794E-41)
            if (r7 == r0) goto L_0x002b
            r2 = 0
        L_0x0024:
            r1 = 0
        L_0x0025:
            if (r2 != 0) goto L_0x0080
            if (r1 != 0) goto L_0x0080
        L_0x0029:
            r0 = 0
            return r0
        L_0x002b:
            r4 = -1
            boolean r0 = r2.canScrollVertically(r4)
            if (r0 == 0) goto L_0x0055
            int r1 = r3.A01
            int r0 = r3.A0h()
            int r1 = r1 - r0
            int r0 = r3.A0e()
            int r1 = r1 - r0
            int r2 = -r1
        L_0x003f:
            androidx.recyclerview.widget.RecyclerView r0 = r3.A08
            boolean r0 = r0.canScrollHorizontally(r4)
            if (r0 == 0) goto L_0x0024
            int r1 = r3.A04
            int r0 = r3.A0f()
            int r1 = r1 - r0
            int r0 = r3.A0g()
            int r1 = r1 - r0
            int r1 = -r1
            goto L_0x0025
        L_0x0055:
            r2 = 0
            goto L_0x003f
        L_0x0057:
            boolean r0 = r2.canScrollVertically(r1)
            if (r0 == 0) goto L_0x007e
            int r2 = r3.A01
            int r0 = r3.A0h()
            int r2 = r2 - r0
            int r0 = r3.A0e()
            int r2 = r2 - r0
        L_0x0069:
            androidx.recyclerview.widget.RecyclerView r0 = r3.A08
            boolean r0 = r0.canScrollHorizontally(r1)
            if (r0 == 0) goto L_0x0024
            int r1 = r3.A04
            int r0 = r3.A0f()
            int r1 = r1 - r0
            int r0 = r3.A0g()
            int r1 = r1 - r0
            goto L_0x0025
        L_0x007e:
            r2 = 0
            goto L_0x0069
        L_0x0080:
            androidx.recyclerview.widget.RecyclerView r0 = r3.A08
            r0.A0q(r1, r2)
            r0 = 1
            return r0
        L_0x0087:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16260wl.A0D(android.view.View, int, android.os.Bundle):boolean");
    }

    public void A0H(View view, AccessibilityEvent accessibilityEvent) {
        AnonymousClass19T r0;
        super.A0H(view, accessibilityEvent);
        accessibilityEvent.setClassName(RecyclerView.class.getName());
        if ((view instanceof RecyclerView) && !this.A01.A1B() && (r0 = ((RecyclerView) view).A0L) != null) {
            r0.A1l(accessibilityEvent);
        }
    }

    public void A0I(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        AnonymousClass19T r5;
        super.A0I(view, accessibilityNodeInfoCompat);
        accessibilityNodeInfoCompat.A0J(RecyclerView.class.getName());
        if (!this.A01.A1B() && (r5 = this.A01.A0L) != null) {
            RecyclerView recyclerView = r5.A08;
            C15740vp r4 = recyclerView.A0w;
            C15930wD r2 = recyclerView.A0y;
            if (recyclerView.canScrollVertically(-1) || r5.A08.canScrollHorizontally(-1)) {
                accessibilityNodeInfoCompat.A09(DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED);
                accessibilityNodeInfoCompat.A02.setScrollable(true);
            }
            if (r5.A08.canScrollVertically(1) || r5.A08.canScrollHorizontally(1)) {
                accessibilityNodeInfoCompat.A09(DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED);
                accessibilityNodeInfoCompat.A02.setScrollable(true);
            }
            accessibilityNodeInfoCompat.A0P(AnonymousClass8NI.A00(r5.A1e(r4, r2), r5.A1d(r4, r2), false, 0));
        }
    }
}
