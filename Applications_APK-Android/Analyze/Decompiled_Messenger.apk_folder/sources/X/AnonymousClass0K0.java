package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0K0  reason: invalid class name */
public final class AnonymousClass0K0 implements FilenameFilter {
    private final FilenameFilter[] A00;

    public boolean accept(File file, String str) {
        for (FilenameFilter accept : this.A00) {
            if (accept.accept(file, str)) {
                return false;
            }
        }
        return true;
    }

    public AnonymousClass0K0(FilenameFilter... filenameFilterArr) {
        this.A00 = filenameFilterArr;
    }
}
