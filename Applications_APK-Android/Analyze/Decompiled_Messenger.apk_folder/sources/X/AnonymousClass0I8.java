package X;

/* renamed from: X.0I8  reason: invalid class name */
public final class AnonymousClass0I8 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.protocol.MqttClient$9";
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass0C8 A01;
    public final /* synthetic */ Object A02;

    public AnonymousClass0I8(AnonymousClass0C8 r1, int i, Object obj) {
        this.A01 = r1;
        this.A00 = i;
        this.A02 = obj;
    }

    public void run() {
        try {
            this.A01.A0C.C5P(this.A00, this.A02);
        } catch (Throwable th) {
            this.A01.A0B.A07(th, "Mqtt Uncaught Exception", "sendPubAck");
            AnonymousClass0C8.A03(this.A01, AnonymousClass0CE.A01(th), AnonymousClass0CG.A05, th);
        }
    }
}
