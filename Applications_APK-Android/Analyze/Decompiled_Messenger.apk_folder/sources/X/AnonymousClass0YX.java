package X;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.LruCache;
import android.util.TypedValue;
import com.facebook.resources.impl.DrawableCounterLogger;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0YX  reason: invalid class name */
public final class AnonymousClass0YX extends C05760aH implements AnonymousClass1Z5 {
    public static Field A0D;
    public static Method A0E;
    public static Method A0F;
    public static boolean A0G;
    private static volatile AnonymousClass0YX A0H;
    public LruCache A00;
    public C05390Yt A01;
    public Object A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    private TypedValue A06;
    private boolean A07;
    private boolean A08;
    private final C05170Xx A09;
    private final DrawableCounterLogger A0A;
    private final AnonymousClass1ZA A0B;
    private final C05290Yj A0C;

    public String getSimpleName() {
        return "DownloadedFbResources";
    }

    public synchronized void init() {
        C000700l.A09(-1521566093, C000700l.A03(-973034468));
    }

    private XmlResourceParser A00(C05320Ym r4) {
        AnonymousClass15E r0 = (AnonymousClass15E) this.A01.get(r4);
        if (r0 == null) {
            return null;
        }
        return (XmlResourceParser) A0F.invoke(r0.A02, new Object[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0100, code lost:
        if (r3 == null) goto L_0x0105;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0102, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00a1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        X.C05370Yr.A04(r1);
        r0 = new X.C06170ay(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00b1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        X.C05370Yr.A04(r1);
        r0 = new X.C06170ay(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00c4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x00e3, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x00e4, code lost:
        if (r3 == null) goto L_0x0105;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x00f1, code lost:
        throw ((android.content.res.Resources.NotFoundException) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x00f2, code lost:
        r3 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:96:0x00f3 */
    /* JADX WARNING: Removed duplicated region for block: B:84:? A[ExcHandler: IOException | IllegalAccessException | XmlPullParserException (unused java.lang.Throwable), SYNTHETIC, Splitter:B:7:0x0016] */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[ExcHandler: IOException | IllegalAccessException | XmlPullParserException (unused java.lang.Throwable), PHI: r3 
      PHI: (r3v12 android.content.res.XmlResourceParser) = (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v15 android.content.res.XmlResourceParser), (r3v15 android.content.res.XmlResourceParser) binds: [B:46:0x0081, B:66:0x00b2, B:62:0x00ab, B:50:0x008b, B:60:0x00a2, B:61:?, B:56:0x009b, B:43:0x0078, B:29:0x0055, B:30:?, B:31:0x0057, B:37:0x006c, B:35:0x0068, B:36:?] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:29:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x00ef A[Catch:{ all -> 0x00f6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:? A[ExcHandler: 0ay | RuntimeException (unused java.lang.Throwable), SYNTHETIC, Splitter:B:7:0x0016] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x00f3 A[PHI: r3 
      PHI: (r3v4 android.content.res.XmlResourceParser) = (r3v5 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v14 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v11 android.content.res.XmlResourceParser), (r3v15 android.content.res.XmlResourceParser), (r3v15 android.content.res.XmlResourceParser) binds: [B:94:0x00f2, B:46:0x0081, B:66:0x00b2, B:62:0x00ab, B:50:0x008b, B:60:0x00a2, B:61:?, B:56:0x009b, B:43:0x0078, B:29:0x0055, B:30:?, B:31:0x0057, B:37:0x006c, B:35:0x0068, B:36:?] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:96:0x00f3] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:96:0x00f3=Splitter:B:96:0x00f3, B:89:0x00e9=Splitter:B:89:0x00e9} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable A01(int r20, android.content.res.Resources.Theme r21) {
        /*
            r19 = this;
            java.lang.String r6 = "drawable"
            r2 = r19
            boolean r0 = r2.A04
            r7 = 0
            if (r0 == 0) goto L_0x0109
            boolean r0 = r2.A03
            if (r0 == 0) goto L_0x0109
            boolean r0 = r2.A07
            if (r0 != 0) goto L_0x0109
            android.util.TypedValue r10 = r2.A03()
            r4 = 1
            java.lang.Object r8 = X.C05370Yr.A02(r2)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            r11 = r20
            X.C05370Yr.A03(r8, r11, r10, r4)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            java.lang.CharSequence r0 = r10.string     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            if (r0 == 0) goto L_0x0028
            java.lang.String r15 = r0.toString()     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            goto L_0x0029
        L_0x0028:
            r15 = r7
        L_0x0029:
            r12 = r21
            if (r15 == 0) goto L_0x00d6
            java.lang.String r0 = ".xml"
            boolean r0 = r15.endsWith(r0)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            if (r0 == 0) goto L_0x00d6
            X.0Ym r5 = new X.0Ym     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            r5.<init>(r11, r6)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            X.Diw r0 = X.C05370Yr.A01()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00cc, 0ay | RuntimeException -> 0x00f2, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3 }
            if (r0 == 0) goto L_0x00c6
            android.graphics.drawable.Drawable r1 = r0.CJT(r8, r2, r10, r12)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00cc, 0ay | RuntimeException -> 0x00f2, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3 }
            if (r1 == 0) goto L_0x004f
            X.0Yt r0 = r2.A01     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            r0.get(r5)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            r2.A05(r10)
            return r1
        L_0x004f:
            android.content.res.XmlResourceParser r3 = r2.A00(r5)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            if (r3 != 0) goto L_0x0074
            int r0 = r10.assetCookie     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            X.Diw r13 = X.C05370Yr.A01()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x0072, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            if (r13 == 0) goto L_0x006c
            r14 = r8
            r16 = r11
            r18 = r6
            r17 = r0
            android.content.res.XmlResourceParser r3 = r13.BIf(r14, r15, r16, r17, r18)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x0072, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r2.A06(r5, r3)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            goto L_0x0074
        L_0x006c:
            X.0ay r0 = new X.0ay     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x0072, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r0.<init>()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x0072, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            throw r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x0072, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
        L_0x0072:
            r1 = move-exception
            goto L_0x00bb
        L_0x0074:
            if (r3 == 0) goto L_0x0105
            if (r21 == 0) goto L_0x007d
            android.graphics.drawable.Drawable r1 = android.graphics.drawable.Drawable.createFromXml(r2, r3, r12)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            goto L_0x0081
        L_0x007d:
            android.graphics.drawable.Drawable r1 = android.graphics.drawable.Drawable.createFromXml(r2, r3)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
        L_0x0081:
            X.Diw r0 = X.C05370Yr.A01()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00b1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            if (r0 == 0) goto L_0x00ab
            android.graphics.drawable.Drawable r1 = r0.AOj(r1, r12)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00b1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            X.Diw r0 = X.C05370Yr.A01()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00a1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            if (r0 == 0) goto L_0x009b
            r0.C4R(r8, r10, r12, r1)     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00a1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r3.close()
            r2.A05(r10)
            return r1
        L_0x009b:
            X.0ay r0 = new X.0ay     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00a1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r0.<init>()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00a1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            throw r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00a1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
        L_0x00a1:
            r1 = move-exception
            X.C05370Yr.A04(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            X.0ay r0 = new X.0ay     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r0.<init>(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            goto L_0x00c3
        L_0x00ab:
            X.0ay r0 = new X.0ay     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00b1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r0.<init>()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00b1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            throw r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00b1, 0ay | RuntimeException -> 0x00f3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
        L_0x00b1:
            r1 = move-exception
            X.C05370Yr.A04(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            X.0ay r0 = new X.0ay     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r0.<init>(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            goto L_0x00c3
        L_0x00bb:
            X.C05370Yr.A04(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            X.0ay r0 = new X.0ay     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
            r0.<init>(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
        L_0x00c3:
            throw r0     // Catch:{ 0ay | RuntimeException -> 0x00f3, InvocationTargetException -> 0x00c4, IOException | IllegalAccessException | XmlPullParserException -> 0x00e4 }
        L_0x00c4:
            r0 = move-exception
            goto L_0x00e9
        L_0x00c6:
            X.0ay r0 = new X.0ay     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00cc, 0ay | RuntimeException -> 0x00f2, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3 }
            r0.<init>()     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00cc, 0ay | RuntimeException -> 0x00f2, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3 }
            throw r0     // Catch:{ ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException -> 0x00cc, 0ay | RuntimeException -> 0x00f2, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3 }
        L_0x00cc:
            r1 = move-exception
            X.C05370Yr.A04(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            X.0ay r0 = new X.0ay     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            r0.<init>(r1)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            throw r0     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
        L_0x00d6:
            r13 = 1
            r9 = r2
            android.graphics.drawable.Drawable r0 = X.C05370Yr.A00(r8, r9, r10, r11, r12, r13)     // Catch:{ 0ay | RuntimeException -> 0x00f2, InvocationTargetException -> 0x00e7, IOException | IllegalAccessException | XmlPullParserException -> 0x00e3, all -> 0x00e0 }
            r2.A05(r10)
            return r0
        L_0x00e0:
            r0 = move-exception
            r3 = r7
            goto L_0x00f7
        L_0x00e3:
            r3 = r7
        L_0x00e4:
            if (r3 == 0) goto L_0x0105
            goto L_0x0102
        L_0x00e7:
            r0 = move-exception
            r3 = r7
        L_0x00e9:
            java.lang.Throwable r0 = r0.getCause()     // Catch:{ all -> 0x00f6 }
            if (r0 == 0) goto L_0x0100
            android.content.res.Resources$NotFoundException r0 = (android.content.res.Resources.NotFoundException) r0     // Catch:{ all -> 0x00f6 }
            throw r0     // Catch:{ all -> 0x00f6 }
        L_0x00f2:
            r3 = r7
        L_0x00f3:
            r2.A07 = r4     // Catch:{ all -> 0x00f6 }
            goto L_0x0100
        L_0x00f6:
            r0 = move-exception
        L_0x00f7:
            if (r3 == 0) goto L_0x00fc
            r3.close()
        L_0x00fc:
            r2.A05(r10)
            throw r0
        L_0x0100:
            if (r3 == 0) goto L_0x0105
        L_0x0102:
            r3.close()
        L_0x0105:
            r2.A05(r10)
            return r7
        L_0x0109:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0YX.A01(int, android.content.res.Resources$Theme):android.graphics.drawable.Drawable");
    }

    private Drawable A02(int i, Resources.Theme theme) {
        String str;
        if (this.A04 && this.A05 && !this.A08) {
            TypedValue A032 = A03();
            try {
                Object A022 = C05370Yr.A02(this);
                C05370Yr.A03(A022, i, A032, true);
                Drawable A002 = C05370Yr.A00(A022, this, A032, i, theme, true);
                CharSequence charSequence = A032.string;
                if (charSequence != null) {
                    str = charSequence.toString();
                } else {
                    str = null;
                }
                if (!(A002 == null || str == null || !str.endsWith(".xml"))) {
                    LruCache lruCache = this.A00;
                    Integer valueOf = Integer.valueOf(i);
                    if (((C05420Yw) lruCache.get(valueOf)) == null) {
                        LruCache lruCache2 = this.A00;
                        A002.getConstantState();
                        lruCache2.put(valueOf, new C05420Yw());
                    }
                }
                return A002;
            } catch (C06170ay unused) {
                this.A08 = true;
            } catch (RuntimeException unused2) {
                this.A08 = true;
                return null;
            }
        }
        return null;
    }

    private TypedValue A03() {
        Object obj = this.A02;
        TypedValue typedValue = null;
        if (obj != null) {
            synchronized (obj) {
                TypedValue typedValue2 = this.A06;
                if (typedValue2 != null) {
                    this.A06 = null;
                    typedValue = typedValue2;
                }
            }
        }
        if (typedValue == null) {
            return new TypedValue();
        }
        return typedValue;
    }

    public static final AnonymousClass0YX A04(AnonymousClass1XY r11) {
        Resources AeF;
        if (A0H == null) {
            synchronized (AnonymousClass0YX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0H, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        Context A022 = AnonymousClass1YA.A02(applicationInjector);
                        Context A003 = AnonymousClass1YA.A00(applicationInjector);
                        if (!(A003 instanceof AnonymousClass005)) {
                            AeF = A003.getResources();
                        } else {
                            AeF = ((AnonymousClass005) A003).AeF();
                        }
                        A0H = new AnonymousClass0YX(applicationInjector, A022, AeF, AnonymousClass1ZC.A02(applicationInjector), C05290Yj.A00(applicationInjector), C05170Xx.A02(applicationInjector), AnonymousClass0WT.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0H;
    }

    private void A05(TypedValue typedValue) {
        Object obj = this.A02;
        if (obj != null) {
            synchronized (obj) {
                if (this.A06 == null) {
                    this.A06 = typedValue;
                }
            }
        }
    }

    private void A06(C05320Ym r5, XmlResourceParser xmlResourceParser) {
        if (xmlResourceParser != null) {
            try {
                Object obj = A0D.get(xmlResourceParser);
                this.A01.put(r5, new AnonymousClass15E(obj, (XmlResourceParser) A0F.invoke(obj, new Object[0])));
            } catch (IllegalAccessException unused) {
            }
        }
    }

    public AnonymousClass0WW AkC() {
        return AnonymousClass0WW.A05;
    }

    public XmlResourceParser getAnimation(int i) {
        if (this.A04) {
            C05320Ym r1 = new C05320Ym(i, "anim");
            try {
                XmlResourceParser A002 = A00(r1);
                if (A002 != null) {
                    return A002;
                }
                XmlResourceParser animation = super.getAnimation(i);
                A06(r1, animation);
                return animation;
            } catch (Exception unused) {
                this.A04 = false;
            }
        }
        return super.getAnimation(i);
    }

    public XmlResourceParser getLayout(int i) {
        try {
            C005505z.A03("DownloadedFbResources::getLayout", 1809651211);
            return super.getLayout(i);
        } finally {
            C005505z.A00(-1128610304);
        }
    }

    public CharSequence getQuantityText(int i, int i2) {
        if (i != 0) {
            int charAt = super.getQuantityText(2131689538, i2).charAt(0) - '0';
            if (charAt >= 0) {
                AnonymousClass1ZB[] r1 = AnonymousClass1ZB.A00;
                if (charAt < r1.length) {
                    AnonymousClass1ZB r5 = r1[charAt];
                    C05290Yj r4 = this.A0C;
                    if ((-65536 & i) == 2131689472) {
                        Integer num = (Integer) AnonymousClass1XX.A03(AnonymousClass1Y3.AZz, r4.A00);
                        for (C08650fi A022 : C05290Yj.A01(r4, i)) {
                            String A023 = A022.A02(i, num.intValue(), r5);
                            if (A023 != null) {
                                return A023;
                            }
                        }
                    }
                    return ((Resources) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Axy, r4.A00)).getQuantityString(i, i2);
                }
            }
            throw new RuntimeException("Requesting a PluralCategory that does not exists");
        }
        throw new Resources.NotFoundException("Resource id 0x0 requested, this probably means a string resource is missing");
    }

    public String[] getStringArray(int i) {
        if (i != 0) {
            A08(i);
            C05290Yj r4 = this.A0C;
            if ((-16777216 & i) == 2130706432) {
                Integer num = (Integer) AnonymousClass1XX.A03(AnonymousClass1Y3.AZz, r4.A00);
                for (C08650fi A032 : C05290Yj.A01(r4, i)) {
                    String[] A033 = A032.A03(i, num.intValue());
                    if (A033 != null) {
                        return A033;
                    }
                }
            }
            return ((Resources) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Axy, r4.A00)).getStringArray(i);
        }
        throw new Resources.NotFoundException("Resource id 0x0 requested, this probably means a string resource is missing");
    }

    public XmlResourceParser getXml(int i) {
        if (this.A04) {
            C05320Ym r1 = new C05320Ym(i, "xml");
            try {
                XmlResourceParser A002 = A00(r1);
                if (A002 != null) {
                    return A002;
                }
                XmlResourceParser xml = super.getXml(i);
                A06(r1, xml);
                return xml;
            } catch (Exception unused) {
                this.A04 = false;
            }
        }
        return super.getXml(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (r1 < 0) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.InputStream openRawResource(int r3) {
        /*
            r2 = this;
            X.0Xx r0 = r2.A09
            int[] r0 = r0.A04
            if (r0 == 0) goto L_0x000d
            int r1 = java.util.Arrays.binarySearch(r0, r3)
            r0 = 1
            if (r1 >= 0) goto L_0x000e
        L_0x000d:
            r0 = 0
        L_0x000e:
            if (r0 == 0) goto L_0x0026
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream
            java.lang.String r0 = "CD_"
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r3)
            byte[] r0 = r0.getBytes()
            r1.<init>(r0)
        L_0x001f:
            if (r1 != 0) goto L_0x0025
            java.io.InputStream r1 = super.openRawResource(r3)
        L_0x0025:
            return r1
        L_0x0026:
            r1 = 0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0YX.openRawResource(int):java.io.InputStream");
    }

    private AnonymousClass0YX(AnonymousClass1XY r9, Context context, Resources resources, C14650tk r12, C05290Yj r13, C05170Xx r14, C25051Yd r15) {
        super(resources, r12);
        boolean z;
        this.A0A = DrawableCounterLogger.A00(r9);
        this.A0C = r13;
        this.A09 = r14;
        r15.Aem(285568080549618L);
        long At0 = r15.At0(566695164905195L);
        r14.A03 = At0 < 0;
        r14.A00 = At0;
        r14.A06(this);
        C05350Yp.A08(r13.A03, new C05310Yl(r14), (Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, r14.A01));
        this.A0B = new AnonymousClass1ZA();
        AnonymousClass00K A012 = AnonymousClass00J.A01(context);
        int i = A012.A0h;
        int i2 = A012.A0Q;
        int i3 = A012.A0F;
        this.A03 = A012.A11;
        if (i > 0 && !C05370Yr.A00) {
            try {
                C27787Diw A013 = C05370Yr.A01();
                if (A013 != null) {
                    A013.CKn(this, i);
                }
            } catch (Exception unused) {
                C05370Yr.A00 = true;
            }
        }
        if (i2 > 0) {
            try {
                if (!A0G) {
                    Method declaredMethod = Class.forName("android.content.res.XmlBlock").getDeclaredMethod("newParser", new Class[0]);
                    A0F = declaredMethod;
                    declaredMethod.setAccessible(true);
                    Class<?> cls = Class.forName("android.content.res.XmlBlock$Parser");
                    Field declaredField = cls.getDeclaredField("mBlock");
                    A0D = declaredField;
                    declaredField.setAccessible(true);
                    Method declaredMethod2 = cls.getDeclaredMethod("close", new Class[0]);
                    A0E = declaredMethod2;
                    declaredMethod2.setAccessible(true);
                    A0G = true;
                }
                this.A02 = new Object();
                this.A01 = new C05390Yt(i2);
                z = true;
            } catch (Exception e) {
                Log.e("DownloadedFbResources", "initLruXmlResourceHack failed", e);
                z = false;
            }
            this.A04 = z;
        }
        if (i3 > 0) {
            this.A02 = new Object();
            this.A00 = new LruCache(i3);
            this.A05 = true;
        }
    }

    public void A0A(Locale locale) {
        super.A0A(locale);
        this.A0C.A09();
    }

    public CharSequence[] getTextArray(int i) {
        return getStringArray(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        if (r1 < 0) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0050, code lost:
        if (r1 < 0) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable getDrawable(int r5) {
        /*
            r4 = this;
            java.lang.String r1 = "Resources.getDrawable"
            r0 = -96175841(0xfffffffffa44791f, float:-2.550367E35)
            X.C005505z.A03(r1, r0)
            com.facebook.resources.impl.DrawableCounterLogger r0 = r4.A0A     // Catch:{ all -> 0x007c }
            r0.A02(r5)     // Catch:{ all -> 0x007c }
            r3 = 0
            r2 = 0
        L_0x000f:
            int[] r1 = X.C14580td.A00     // Catch:{ all -> 0x007c }
            int r0 = r1.length     // Catch:{ all -> 0x007c }
            if (r2 >= r0) goto L_0x0021
            r0 = r1[r2]     // Catch:{ all -> 0x007c }
            if (r0 != r5) goto L_0x001e
            android.graphics.drawable.ColorDrawable r1 = new android.graphics.drawable.ColorDrawable     // Catch:{ all -> 0x007c }
            r1.<init>(r3)     // Catch:{ all -> 0x007c }
            goto L_0x0022
        L_0x001e:
            int r2 = r2 + 1
            goto L_0x000f
        L_0x0021:
            r1 = 0
        L_0x0022:
            if (r1 == 0) goto L_0x002b
            r0 = -918944036(0xffffffffc93a06dc, float:-761965.75)
        L_0x0027:
            X.C005505z.A00(r0)
            return r1
        L_0x002b:
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007c }
            int[] r0 = r0.A04     // Catch:{ all -> 0x007c }
            if (r0 == 0) goto L_0x0038
            int r1 = java.util.Arrays.binarySearch(r0, r5)     // Catch:{ all -> 0x007c }
            r0 = 1
            if (r1 >= 0) goto L_0x0039
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r0 == 0) goto L_0x0045
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007c }
            android.graphics.drawable.Drawable r1 = r0.A05(r5, r4)     // Catch:{ all -> 0x007c }
            r0 = -2101061534(0xffffffff82c45862, float:-2.885034E-37)
            goto L_0x0027
        L_0x0045:
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007c }
            int[] r0 = r0.A05     // Catch:{ all -> 0x007c }
            if (r0 == 0) goto L_0x0052
            int r1 = java.util.Arrays.binarySearch(r0, r5)     // Catch:{ all -> 0x007c }
            r0 = 1
            if (r1 >= 0) goto L_0x0053
        L_0x0052:
            r0 = 0
        L_0x0053:
            if (r0 == 0) goto L_0x005f
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007c }
            android.graphics.drawable.Drawable r1 = r0.A04(r5, r4)     // Catch:{ all -> 0x007c }
            r0 = -1023515216(0xffffffffc2fe65b0, float:-127.19861)
            goto L_0x0027
        L_0x005f:
            r0 = 0
            android.graphics.drawable.Drawable r1 = r4.A02(r5, r0)     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x006a
            r0 = -1265348100(0xffffffffb49451fc, float:-2.76268E-7)
            goto L_0x0027
        L_0x006a:
            android.graphics.drawable.Drawable r1 = r4.A01(r5, r0)     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x0074
            r0 = 114013078(0x6cbb396, float:7.662399E-35)
            goto L_0x0027
        L_0x0074:
            android.graphics.drawable.Drawable r1 = super.getDrawable(r5)     // Catch:{ all -> 0x007c }
            r0 = -1267930501(0xffffffffb46cea7b, float:-2.2064516E-7)
            goto L_0x0027
        L_0x007c:
            r1 = move-exception
            r0 = 296408171(0x11aad46b, float:2.6952183E-28)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0YX.getDrawable(int):android.graphics.drawable.Drawable");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        if (r1 < 0) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0050, code lost:
        if (r1 < 0) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable getDrawable(int r5, android.content.res.Resources.Theme r6) {
        /*
            r4 = this;
            java.lang.String r1 = "Resources.getDrawable_withTheme"
            r0 = -1394909793(0xffffffffacdb5d9f, float:-6.2347484E-12)
            X.C005505z.A03(r1, r0)
            com.facebook.resources.impl.DrawableCounterLogger r0 = r4.A0A     // Catch:{ all -> 0x007b }
            r0.A02(r5)     // Catch:{ all -> 0x007b }
            r3 = 0
            r2 = 0
        L_0x000f:
            int[] r1 = X.C14580td.A00     // Catch:{ all -> 0x007b }
            int r0 = r1.length     // Catch:{ all -> 0x007b }
            if (r2 >= r0) goto L_0x0021
            r0 = r1[r2]     // Catch:{ all -> 0x007b }
            if (r0 != r5) goto L_0x001e
            android.graphics.drawable.ColorDrawable r1 = new android.graphics.drawable.ColorDrawable     // Catch:{ all -> 0x007b }
            r1.<init>(r3)     // Catch:{ all -> 0x007b }
            goto L_0x0022
        L_0x001e:
            int r2 = r2 + 1
            goto L_0x000f
        L_0x0021:
            r1 = 0
        L_0x0022:
            if (r1 == 0) goto L_0x002b
            r0 = 1432549323(0x5562f7cb, float:1.55971182E13)
        L_0x0027:
            X.C005505z.A00(r0)
            return r1
        L_0x002b:
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007b }
            int[] r0 = r0.A04     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x0038
            int r1 = java.util.Arrays.binarySearch(r0, r5)     // Catch:{ all -> 0x007b }
            r0 = 1
            if (r1 >= 0) goto L_0x0039
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r0 == 0) goto L_0x0045
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007b }
            android.graphics.drawable.Drawable r1 = r0.A05(r5, r4)     // Catch:{ all -> 0x007b }
            r0 = -821438232(0xffffffffcf09d8e8, float:-2.31269376E9)
            goto L_0x0027
        L_0x0045:
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007b }
            int[] r0 = r0.A05     // Catch:{ all -> 0x007b }
            if (r0 == 0) goto L_0x0052
            int r1 = java.util.Arrays.binarySearch(r0, r5)     // Catch:{ all -> 0x007b }
            r0 = 1
            if (r1 >= 0) goto L_0x0053
        L_0x0052:
            r0 = 0
        L_0x0053:
            if (r0 == 0) goto L_0x005f
            X.0Xx r0 = r4.A09     // Catch:{ all -> 0x007b }
            android.graphics.drawable.Drawable r1 = r0.A04(r5, r4)     // Catch:{ all -> 0x007b }
            r0 = -234902(0xfffffffffffc6a6a, float:NaN)
            goto L_0x0027
        L_0x005f:
            android.graphics.drawable.Drawable r1 = r4.A02(r5, r6)     // Catch:{ all -> 0x007b }
            if (r1 == 0) goto L_0x0069
            r0 = 1570356440(0x5d99bcd8, float:1.38474583E18)
            goto L_0x0027
        L_0x0069:
            android.graphics.drawable.Drawable r1 = r4.A01(r5, r6)     // Catch:{ all -> 0x007b }
            if (r1 == 0) goto L_0x0073
            r0 = -2143387847(0xffffffff803e7f39, float:-5.73944E-39)
            goto L_0x0027
        L_0x0073:
            android.graphics.drawable.Drawable r1 = super.getDrawable(r5, r6)     // Catch:{ all -> 0x007b }
            r0 = -1523522590(0xffffffffa530e3e2, float:-1.5342776E-16)
            goto L_0x0027
        L_0x007b:
            r1 = move-exception
            r0 = 2018476422(0x784f8186, float:1.6834883E34)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0YX.getDrawable(int, android.content.res.Resources$Theme):android.graphics.drawable.Drawable");
    }

    public String getQuantityString(int i, int i2) {
        return getQuantityText(i, i2).toString();
    }

    public String getQuantityString(int i, int i2, Object... objArr) {
        return String.format(this.A0C.A07(), getQuantityString(i, i2), objArr);
    }

    public String getString(int i) {
        return getText(i).toString();
    }

    public String getString(int i, Object... objArr) {
        return String.format(this.A0C.A07(), getString(i), objArr);
    }

    public CharSequence getText(int i) {
        if (i != 0) {
            A08(i);
            return this.A0C.A06(i);
        }
        throw new Resources.NotFoundException("Resource id 0x0 requested, this probably means a string resource is missing");
    }

    public CharSequence getText(int i, CharSequence charSequence) {
        A08(i);
        CharSequence A062 = i != 0 ? this.A0C.A06(i) : null;
        return A062 == null ? charSequence : A062;
    }
}
