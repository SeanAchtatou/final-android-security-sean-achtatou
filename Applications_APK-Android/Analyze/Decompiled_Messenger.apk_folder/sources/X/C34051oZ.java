package X;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1oZ  reason: invalid class name and case insensitive filesystem */
public final class C34051oZ implements C33631nt {
    public static final C34051oZ A00 = new C34051oZ();

    public void clear() {
    }

    public boolean containsKey(Object obj) {
        return false;
    }

    public boolean containsValue(Object obj) {
        return false;
    }

    public boolean isEmpty() {
        return true;
    }

    public void putAll(Map map) {
    }

    public int size() {
        return 0;
    }

    public Set entrySet() {
        return C25011Xz.A03();
    }

    public Set keySet() {
        return C25011Xz.A03();
    }

    public Object put(Object obj, Object obj2) {
        return null;
    }

    public Collection values() {
        return C25011Xz.A03();
    }

    public Object get(Object obj) {
        return null;
    }

    public Object remove(Object obj) {
        return null;
    }

    private C34051oZ() {
    }
}
