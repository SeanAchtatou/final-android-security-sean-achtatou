package X;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0ZO  reason: invalid class name */
public final class AnonymousClass0ZO implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.init.AppInitLock$1";
    public final /* synthetic */ AnonymousClass0UW A00;

    public AnonymousClass0ZO(AnonymousClass0UW r1) {
        this.A00 = r1;
    }

    public void run() {
        ArrayList arrayList;
        AnonymousClass00C.A01(8, "Notify AppInitLock Listeners", -1788330346);
        try {
            AnonymousClass0UW r4 = this.A00;
            synchronized (r4) {
                arrayList = r4.A00;
                r4.A00 = new ArrayList();
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ((AnonymousClass0Y2) it.next()).A00();
            }
            AnonymousClass00C.A00(8, -371220433);
        } catch (Throwable th) {
            AnonymousClass00C.A00(8, 1173979348);
            throw th;
        }
    }
}
