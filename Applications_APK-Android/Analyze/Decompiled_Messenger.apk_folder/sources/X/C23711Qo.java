package X;

import java.util.Map;

/* renamed from: X.1Qo  reason: invalid class name and case insensitive filesystem */
public abstract class C23711Qo extends C24241Sv {
    public static final String __redex_internal_original_name = "com.facebook.imagepipeline.producers.StatefulProducerRunnable";
    private final AnonymousClass1QK A00;
    private final C23581Qb A01;
    private final AnonymousClass1MN A02;
    private final String A03;

    public void A04(Object obj) {
        if (this instanceof AnonymousClass1R2) {
            AnonymousClass1NY.A04((AnonymousClass1NY) obj);
        }
    }

    public Map A06(Object obj) {
        return null;
    }

    public void A02() {
        AnonymousClass1MN r4 = this.A02;
        AnonymousClass1QK r3 = this.A00;
        String str = this.A03;
        r4.C3Q(r3, str);
        r4.Bk2(r3, str, null);
        this.A01.onCancellation();
    }

    public void A03(Exception exc) {
        AnonymousClass1MN r4 = this.A02;
        AnonymousClass1QK r3 = this.A00;
        String str = this.A03;
        r4.C3Q(r3, str);
        r4.Bk4(r3, str, exc, null);
        this.A01.BYh(exc);
    }

    public void A05(Object obj) {
        Map map;
        AnonymousClass1MN r3 = this.A02;
        AnonymousClass1QK r2 = this.A00;
        String str = this.A03;
        if (r3.C3Q(r2, str)) {
            map = A06(obj);
        } else {
            map = null;
        }
        r3.Bk6(r2, str, map);
        this.A01.Bgg(obj, 1);
    }

    public C23711Qo(C23581Qb r1, AnonymousClass1MN r2, AnonymousClass1QK r3, String str) {
        this.A01 = r1;
        this.A02 = r2;
        this.A03 = str;
        this.A00 = r3;
        r2.Bk8(r3, str);
    }
}
