package X;

import java.util.concurrent.Executor;

/* renamed from: X.0D6  reason: invalid class name */
public final class AnonymousClass0D6 {
    public final Runnable A00;
    public final Executor A01;

    public void A00() {
        try {
            AnonymousClass07A.A04(this.A01, this.A00, 369104524);
        } catch (RuntimeException e) {
            C010708t.A0W("ExecutionList", e, "RuntimeException while executing runnable=%s with executor=%s", this.A00, this.A01);
        }
    }

    public AnonymousClass0D6(Runnable runnable, Executor executor) {
        this.A00 = runnable;
        this.A01 = executor;
    }
}
