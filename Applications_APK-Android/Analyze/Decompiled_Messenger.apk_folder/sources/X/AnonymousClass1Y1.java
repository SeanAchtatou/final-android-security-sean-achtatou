package X;

import java.util.NoSuchElementException;

/* renamed from: X.1Y1  reason: invalid class name */
public final class AnonymousClass1Y1 extends C24971Xv {
    public boolean A00;
    public final /* synthetic */ Object A01;

    public AnonymousClass1Y1(Object obj) {
        this.A01 = obj;
    }

    public boolean hasNext() {
        return !this.A00;
    }

    public Object next() {
        if (!this.A00) {
            this.A00 = true;
            return this.A01;
        }
        throw new NoSuchElementException();
    }
}
