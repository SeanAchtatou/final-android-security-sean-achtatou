package X;

import java.util.List;

/* renamed from: X.1Qy  reason: invalid class name and case insensitive filesystem */
public class C23811Qy {
    public void A00() {
        boolean remove;
        List list;
        AnonymousClass1QK r3;
        List list2;
        List list3;
        if (this instanceof C23721Qp) {
            C23721Qp r2 = (C23721Qp) this;
            r2.A00.A01();
            r2.A01.A01.C1H(r2.A00);
        } else if (this instanceof AnonymousClass1QY) {
            AnonymousClass1QY r22 = (AnonymousClass1QY) this;
            r22.A01.A02.A03();
            r22.A01.A00 = true;
            r22.A00.onCancellation();
        } else if (this instanceof AnonymousClass1R5) {
            AnonymousClass1R5 r4 = (AnonymousClass1R5) this;
            synchronized (r4.A01) {
                remove = r4.A01.A06.remove(r4.A00);
                list = null;
                if (!remove) {
                    r3 = null;
                    list2 = null;
                } else if (r4.A01.A06.isEmpty()) {
                    r3 = r4.A01.A02;
                    list2 = null;
                } else {
                    List A02 = AnonymousClass1R4.A02(r4.A01);
                    list2 = AnonymousClass1R4.A03(r4.A01);
                    list3 = AnonymousClass1R4.A01(r4.A01);
                    r3 = null;
                    list = A02;
                }
                list3 = null;
            }
            AnonymousClass1QK.A01(list);
            AnonymousClass1QK.A02(list2);
            AnonymousClass1QK.A00(list3);
            if (r3 != null) {
                if (!r4.A01.A07.A03 || r3.A08()) {
                    r3.A05();
                } else {
                    AnonymousClass1QK.A02(r3.A04(C23561Pz.LOW));
                }
            }
            if (remove) {
                ((C23581Qb) r4.A00.first).onCancellation();
            }
        } else if (this instanceof C33241nG) {
            ((C33241nG) this).A00.set(true);
        } else if (this instanceof AnonymousClass1SO) {
            AnonymousClass1SO r1 = (AnonymousClass1SO) this;
            if (r1.A01) {
                AnonymousClass1RQ r12 = r1.A00;
                AnonymousClass1RQ.A01(r12, true);
                r12.A00.onCancellation();
            }
        }
    }

    public void A01() {
        if (this instanceof AnonymousClass1QY) {
            AnonymousClass1QY r1 = (AnonymousClass1QY) this;
            if (r1.A01.A01.A07()) {
                r1.A01.A02.A04();
            }
        } else if (this instanceof AnonymousClass1R5) {
            AnonymousClass1QK.A00(AnonymousClass1R4.A01(((AnonymousClass1R5) this).A01));
        } else if (this instanceof AnonymousClass1SO) {
            AnonymousClass1SO r12 = (AnonymousClass1SO) this;
            if (r12.A00.A02.A07()) {
                r12.A00.A03.A04();
            }
        }
    }

    public void A02() {
        if (this instanceof AnonymousClass1R5) {
            AnonymousClass1QK.A01(AnonymousClass1R4.A02(((AnonymousClass1R5) this).A01));
        }
    }

    public void A03() {
        if (this instanceof AnonymousClass1R5) {
            AnonymousClass1QK.A02(AnonymousClass1R4.A03(((AnonymousClass1R5) this).A01));
        }
    }
}
