package X;

/* renamed from: X.15E  reason: invalid class name */
public final class AnonymousClass15E {
    public boolean A00;
    public final Object A01;
    public final Object A02;

    public AnonymousClass15E(Object obj, Object obj2) {
        this.A02 = obj;
        this.A01 = obj2;
    }

    public void finalize() {
        int A03 = C000700l.A03(-1115268532);
        try {
            if (!this.A00) {
                try {
                    AnonymousClass0YX.A0E.invoke(this.A01, new Object[0]);
                } catch (Exception unused) {
                }
                this.A00 = true;
            }
        } finally {
            super.finalize();
            C000700l.A09(417821695, A03);
        }
    }
}
