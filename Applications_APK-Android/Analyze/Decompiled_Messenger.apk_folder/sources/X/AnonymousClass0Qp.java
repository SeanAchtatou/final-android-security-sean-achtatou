package X;

import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.0Qp  reason: invalid class name */
public final class AnonymousClass0Qp extends InputStream {
    private int A00;
    public final /* synthetic */ AnonymousClass0NW A01;

    public AnonymousClass0Qp(AnonymousClass0NW r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    public int available() {
        return this.A00;
    }

    public void close() {
        while (available() > 0) {
            skip((long) available());
        }
    }

    public int read() {
        if (this.A00 == 0) {
            return -1;
        }
        int read = this.A01.A01.read();
        if (read != -1) {
            this.A00--;
            return read;
        }
        throw new IOException("compressed stream terminated prematurely");
    }

    public int read(byte[] bArr, int i, int i2) {
        if (i2 > 0 && this.A00 == 0) {
            return -1;
        }
        int read = this.A01.A01.read(bArr, i, Math.min(this.A00, i2));
        if (read > 0) {
            this.A00 -= read;
        }
        return read;
    }
}
