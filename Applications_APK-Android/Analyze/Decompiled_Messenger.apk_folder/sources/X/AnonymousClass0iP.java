package X;

import com.google.common.base.Predicate;
import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0iP  reason: invalid class name */
public final class AnonymousClass0iP implements FilenameFilter {
    public final /* synthetic */ Predicate A00;

    public AnonymousClass0iP(Predicate predicate) {
        this.A00 = predicate;
    }

    public boolean accept(File file, String str) {
        return this.A00.apply(str);
    }
}
