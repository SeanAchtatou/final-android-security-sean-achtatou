package X;

import android.os.SystemClock;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1RU  reason: invalid class name */
public final class AnonymousClass1RU {
    public int A00 = 0;
    public long A01 = 0;
    public long A02 = 0;
    public AnonymousClass1NY A03 = null;
    public Integer A04 = AnonymousClass07B.A00;
    public final C23961Rq A05;
    public final Runnable A06 = new AnonymousClass1RV(this);
    public final Executor A07;
    private final int A08;
    private final Runnable A09 = new AnonymousClass1SK(this);

    public void A03() {
        AnonymousClass1NY r1;
        synchronized (this) {
            r1 = this.A03;
            this.A03 = null;
            this.A00 = 0;
        }
        AnonymousClass1NY.A04(r1);
    }

    private void A00(long j) {
        Runnable A022 = C27491dH.A02(this.A09, "JobScheduler_enqueueJob");
        if (j > 0) {
            if (AnonymousClass4NW.A00 == null) {
                AnonymousClass4NW.A00 = Executors.newSingleThreadScheduledExecutor();
            }
            AnonymousClass4NW.A00.schedule(A022, j, TimeUnit.MILLISECONDS);
            return;
        }
        A022.run();
    }

    public AnonymousClass1RU(Executor executor, C23961Rq r4, int i) {
        this.A07 = executor;
        this.A05 = r4;
        this.A08 = i;
    }

    public static void A01(AnonymousClass1RU r6) {
        long j;
        boolean z;
        long uptimeMillis = SystemClock.uptimeMillis();
        synchronized (r6) {
            if (r6.A04 == AnonymousClass07B.A0N) {
                j = Math.max(r6.A01 + ((long) r6.A08), uptimeMillis);
                z = true;
                r6.A02 = uptimeMillis;
                r6.A04 = AnonymousClass07B.A01;
            } else {
                r6.A04 = AnonymousClass07B.A00;
                j = 0;
                z = false;
            }
        }
        if (z) {
            r6.A00(j - uptimeMillis);
        }
    }

    public static boolean A02(AnonymousClass1NY r2, int i) {
        if (!AnonymousClass1QU.A03(i)) {
            boolean z = false;
            if ((i & 4) == 4) {
                z = true;
            }
            if (z || AnonymousClass1NY.A07(r2)) {
                return true;
            }
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0035, code lost:
        if (r1 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        A00(r4 - r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
            r6 = this;
            long r2 = android.os.SystemClock.uptimeMillis()
            monitor-enter(r6)
            X.1NY r1 = r6.A03     // Catch:{ all -> 0x003c }
            int r0 = r6.A00     // Catch:{ all -> 0x003c }
            boolean r0 = A02(r1, r0)     // Catch:{ all -> 0x003c }
            r1 = 0
            if (r0 != 0) goto L_0x0012
            monitor-exit(r6)     // Catch:{ all -> 0x003c }
            return
        L_0x0012:
            java.lang.Integer r0 = r6.A04     // Catch:{ all -> 0x003c }
            int r0 = r0.intValue()     // Catch:{ all -> 0x003c }
            switch(r0) {
                case 0: goto L_0x001c;
                case 1: goto L_0x001b;
                case 2: goto L_0x002e;
                default: goto L_0x001b;
            }     // Catch:{ all -> 0x003c }
        L_0x001b:
            goto L_0x0032
        L_0x001c:
            long r4 = r6.A01     // Catch:{ all -> 0x003c }
            int r0 = r6.A08     // Catch:{ all -> 0x003c }
            long r0 = (long) r0     // Catch:{ all -> 0x003c }
            long r4 = r4 + r0
            long r4 = java.lang.Math.max(r4, r2)     // Catch:{ all -> 0x003c }
            r6.A02 = r2     // Catch:{ all -> 0x003c }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x003c }
            r6.A04 = r0     // Catch:{ all -> 0x003c }
            r1 = 1
            goto L_0x0034
        L_0x002e:
            java.lang.Integer r0 = X.AnonymousClass07B.A0N     // Catch:{ all -> 0x003c }
            r6.A04 = r0     // Catch:{ all -> 0x003c }
        L_0x0032:
            r4 = 0
        L_0x0034:
            monitor-exit(r6)     // Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x003b
            long r4 = r4 - r2
            r6.A00(r4)
        L_0x003b:
            return
        L_0x003c:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x003c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1RU.A04():void");
    }

    public boolean A05(AnonymousClass1NY r3, int i) {
        AnonymousClass1NY r1;
        if (!A02(r3, i)) {
            return false;
        }
        synchronized (this) {
            r1 = this.A03;
            this.A03 = AnonymousClass1NY.A03(r3);
            this.A00 = i;
        }
        AnonymousClass1NY.A04(r1);
        return true;
    }
}
