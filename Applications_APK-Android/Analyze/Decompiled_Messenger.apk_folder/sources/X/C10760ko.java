package X;

import android.telephony.TelephonyManager;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ko  reason: invalid class name and case insensitive filesystem */
public final class C10760ko {
    private static volatile C10760ko A0A;
    private AnonymousClass2F0 A00;
    public final TelephonyManager A01;
    public final AnonymousClass1Y6 A02;
    public final Runnable A03 = new C10770kp(this);
    public final Runnable A04 = new C10780kq(this);
    public volatile Class A05;
    public volatile boolean A06;
    public volatile boolean A07;
    public volatile boolean A08;
    private volatile boolean A09 = true;

    public static AnonymousClass2F0 A00(C10760ko r1) {
        r1.A02.AOz();
        if (r1.A00 == null) {
            r1.A00 = new AnonymousClass2F0(r1);
        }
        return r1.A00;
    }

    public static final C10760ko A01(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (C10760ko.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new C10760ko(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public void A02(boolean z) {
        Runnable runnable;
        if (this.A09 != z) {
            this.A09 = z;
            int i = 0;
            if (this.A09) {
                i = 32;
            }
            AnonymousClass1Y6 r1 = this.A02;
            if (r1.BHM()) {
                this.A01.listen(A00(this), i);
                return;
            }
            if (i == 0) {
                runnable = this.A04;
            } else {
                runnable = this.A03;
            }
            r1.BxR(runnable);
        }
    }

    private C10760ko(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass0UX.A07(r2);
        this.A01 = C04490Ux.A0c(r2);
    }
}
