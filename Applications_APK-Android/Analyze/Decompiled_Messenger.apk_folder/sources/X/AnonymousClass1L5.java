package X;

/* renamed from: X.1L5  reason: invalid class name */
public final class AnonymousClass1L5 {
    public static long A00(int i, int i2) {
        int floatToRawIntBits = Float.floatToRawIntBits((float) i);
        return ((long) Float.floatToRawIntBits((float) i2)) | (((long) floatToRawIntBits) << 32);
    }
}
