package X;

import java.util.HashSet;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1me  reason: invalid class name and case insensitive filesystem */
public final class C32981me {
    private static volatile C32981me A01;
    public final Set A00 = new HashSet();

    public static final C32981me A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C32981me.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C32981me();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
