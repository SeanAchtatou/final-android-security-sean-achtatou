package X;

import android.os.Bundle;
import com.facebook.acra.ACRA;
import com.facebook.acra.LogCatCollector;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.util.TriState;
import com.facebook.forker.Process;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.tincan.messenger.TincanPreKeyManager;
import com.facebook.omnistore.OmnistoreIOException;
import com.facebook.omnistore.module.OmnistoreStoredProcedureComponent;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1tB  reason: invalid class name and case insensitive filesystem */
public abstract class C36471tB implements OmnistoreStoredProcedureComponent {
    public static final Class A04 = C36471tB.class;
    public static final Charset A05 = Charset.forName(LogCatCollector.UTF_8_ENCODING);
    private static final Random A06 = new Random();
    private static final AtomicInteger A07 = new AtomicInteger();
    public AnonymousClass2WT A00;
    public Set A01 = C25011Xz.A08();
    private final int A02;
    private final DeprecatedAnalyticsLogger A03;

    public void A07() {
        if (!(this instanceof C36461tA) && !(this instanceof C36611tQ) && !(this instanceof C36621tR) && !(this instanceof C36761tg) && !(this instanceof C36751tf) && !(this instanceof C36781ti)) {
            if (this instanceof C36771th) {
                for (C22086AoB A09 : ((C36771th) this).A01) {
                    C22086AoB.A09(A09, "TincanDeviceManager.onRegistrationAvailable");
                }
            } else if (!(this instanceof C36491tD) && !(this instanceof C36631tS) && !(this instanceof C32391lg)) {
                boolean z = this instanceof C36741te;
            }
        }
    }

    public void A08() {
        if (!(this instanceof C36461tA) && !(this instanceof C36611tQ) && !(this instanceof C36621tR) && !(this instanceof C36761tg) && !(this instanceof C36751tf) && !(this instanceof C36781ti) && !(this instanceof C36771th) && !(this instanceof C36491tD) && !(this instanceof C36631tS) && !(this instanceof C32391lg)) {
            boolean z = this instanceof C36741te;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void A09(C22344AwL awL) {
        byte[] bArr;
        ThreadKey A062;
        AwO awO;
        byte[] bArr2;
        boolean z;
        AnonymousClass94R r8;
        Map map;
        AnonymousClass94R r2;
        Long l;
        Integer num;
        Class cls;
        AnonymousClass94R r3;
        byte[] bArr3;
        C22344AwL awL2 = awL;
        if (this instanceof C36461tA) {
            C36461tA r22 = (C36461tA) this;
            if (awL == null) {
                C010708t.A05(C36461tA.A03, "Error deserialising 'set primary device' response");
                for (C22086AoB aoB : r22.A01) {
                    synchronized (aoB) {
                    }
                }
            } else if (awL2.result == AnonymousClass94R.A0B) {
                for (C22086AoB aoB2 : r22.A01) {
                    synchronized (aoB2) {
                        ((C55592oK) aoB2.A06.get()).A02(C22086AoB.A0D, true);
                        aoB2.A0A = TriState.YES;
                    }
                }
            } else {
                for (C22086AoB aoB3 : r22.A01) {
                    synchronized (aoB3) {
                    }
                }
            }
        } else if (this instanceof C36611tQ) {
            C36611tQ r23 = (C36611tQ) this;
            if (awL == null) {
                C010708t.A05(C36611tQ.A03, "Could not deserialise pre-key upload response");
                for (TincanPreKeyManager tincanPreKeyManager : r23.A01) {
                    synchronized (tincanPreKeyManager) {
                        TincanPreKeyManager.A01(tincanPreKeyManager, AnonymousClass07B.A0Y);
                    }
                }
            } else if (awL2.result == AnonymousClass94R.A0B) {
                for (TincanPreKeyManager tincanPreKeyManager2 : r23.A01) {
                    synchronized (tincanPreKeyManager2) {
                        ((C55592oK) tincanPreKeyManager2.A08.get()).A06(TincanPreKeyManager.A0P, Long.toString(tincanPreKeyManager2.A01.now()));
                        TincanPreKeyManager.A01(tincanPreKeyManager2, AnonymousClass07B.A0N);
                    }
                }
            } else {
                for (TincanPreKeyManager tincanPreKeyManager3 : r23.A01) {
                    synchronized (tincanPreKeyManager3) {
                        TincanPreKeyManager.A01(tincanPreKeyManager3, AnonymousClass07B.A0Y);
                    }
                }
            }
        } else if (this instanceof C36621tR) {
            C36621tR r32 = (C36621tR) this;
            if (!(awL == null || (bArr = awL2.nonce) == null || awL2.result == null || (A062 = r32.A06(bArr)) == null)) {
                if (awL2.result != AnonymousClass94R.A0B) {
                    for (TincanPreKeyManager A09 : r32.A01) {
                        A09.A09(A062, awL2.result);
                    }
                    return;
                }
                AwH awH = awL2.body;
                if (awH != null && awH.A05(3)) {
                    for (TincanPreKeyManager tincanPreKeyManager4 : r32.A01) {
                        Long l2 = awL2.date_micros;
                        AwH awH2 = awL2.body;
                        int i = awH2.setField_;
                        if (i == 3) {
                            tincanPreKeyManager4.A0B(A062, l2, (Aw8) awH2.value_, false);
                            tincanPreKeyManager4.A08(A062);
                        } else {
                            throw new RuntimeException(AnonymousClass08S.A0J("Cannot get field 'lookup_response_payload' because union is currently set to ", awH2.getFieldDesc(i).A01));
                        }
                    }
                    return;
                }
            }
            C010708t.A05(C36621tR.A03, "Could not deserialise LookupResponsePayload");
        } else if (this instanceof C36761tg) {
            C36761tg r24 = (C36761tg) this;
            if (awL == null) {
                C010708t.A05(C36761tg.A03, "Error deserialising 'set not primary device' response");
                for (C22086AoB aoB4 : r24.A01) {
                    synchronized (aoB4) {
                    }
                }
            } else if (awL2.result == AnonymousClass94R.A0B) {
                for (C22086AoB aoB5 : r24.A01) {
                    synchronized (aoB5) {
                        ((C55592oK) aoB5.A06.get()).A02(C22086AoB.A0D, false);
                        aoB5.A0A = TriState.NO;
                    }
                }
            } else {
                for (C22086AoB aoB6 : r24.A01) {
                    synchronized (aoB6) {
                    }
                }
            }
        } else if (this instanceof C36751tf) {
            C36751tf r6 = (C36751tf) this;
            if (awL == null) {
                C010708t.A05(C36751tf.A02, "Error deserialising 'device enable' response");
                for (C22086AoB A0F : r6.A01) {
                    A0F.A0F(null);
                }
                return;
            }
            AnonymousClass94R r25 = awL2.result;
            if (r25 == null || r25 != AnonymousClass94R.A0B) {
                for (C22086AoB A0F2 : r6.A01) {
                    A0F2.A0F(awL2.result);
                }
                return;
            }
            AwH awH3 = awL2.body;
            if (awH3 == null) {
                awO = null;
            } else {
                int i2 = awH3.setField_;
                if (i2 == 8) {
                    awO = (AwO) awH3.value_;
                } else {
                    throw new RuntimeException(AnonymousClass08S.A0J("Cannot get field 'enable_device_response_payload' because union is currently set to ", awH3.getFieldDesc(i2).A01));
                }
            }
            for (C22086AoB aoB7 : r6.A01) {
                List list = awO == null ? null : awO.account_devices;
                int i3 = AnonymousClass1Y3.AZJ;
                ((C36511tF) AnonymousClass1XX.A02(11, i3, aoB7.A00)).A03();
                ((C36511tF) AnonymousClass1XX.A02(11, i3, aoB7.A00)).A04(list);
                aoB7.A04.A00(new C22338Aw9(Long.valueOf(Long.parseLong(((AnonymousClass0XN) AnonymousClass1XX.A02(10, AnonymousClass1Y3.ArW, aoB7.A00)).A09().A0j)), ((AnonymousClass18N) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BRm, aoB7.A00)).A02()), true);
                C22086AoB.A06(aoB7);
            }
        } else if (this instanceof C36781ti) {
            C36781ti r33 = (C36781ti) this;
            if (awL == null || (bArr2 = awL2.nonce) == null) {
                C010708t.A05(C36781ti.A03, "Error deserialising 'disable device' response");
                Iterator it = r33.A01.iterator();
                while (it.hasNext()) {
                    it.next();
                    C010708t.A0J("TincanDeviceManager", "Unable to disable device. SP failed");
                }
                return;
            }
            AnonymousClass94R r0 = awL2.result;
            if (r0 != null) {
                switch (r0.ordinal()) {
                    case 0:
                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                        z = true;
                        break;
                    default:
                        z = false;
                        break;
                }
                if (z) {
                    String str = new String(bArr2, Charset.defaultCharset());
                    C22338Aw9 A002 = C22037AnD.A00(str.substring(str.indexOf(58) + 1));
                    for (C22086AoB aoB8 : r33.A01) {
                        aoB8.A04.A00(A002, false);
                        C22086AoB.A06(aoB8);
                    }
                    return;
                }
            }
            Iterator it2 = r33.A01.iterator();
            while (it2.hasNext()) {
                it2.next();
                C010708t.A0J("TincanDeviceManager", "Unable to disable device. SP failed");
            }
        } else if (this instanceof C36771th) {
            C36771th r7 = (C36771th) this;
            if (awL == null) {
                C36771th.A01(r7);
                return;
            }
            if (awL2.result != AnonymousClass94R.A0B) {
                r7.A01.A08(awL2.nonce, false, "Registration failed");
                for (C22086AoB aoB9 : r7.A01) {
                    synchronized (aoB9) {
                        if (C22086AoB.A02(aoB9) == AnonymousClass07B.A0C) {
                            C010708t.A0J("TincanDeviceManager", "Tincan device registration failed, but already registered!");
                        } else {
                            C22086AoB.A07(aoB9, AnonymousClass07B.A0N);
                        }
                    }
                }
            }
            AwH awH4 = awL2.body;
            if (awH4 == null || !awH4.A05(2)) {
                C36771th.A01(r7);
                r7.A01.A08(awL2.nonce, false, "Bad Stored Procedure Result");
                return;
            }
            int i4 = awH4.setField_;
            if (i4 == 2) {
                C22342AwJ awJ = (C22342AwJ) awH4.value_;
                boolean booleanValue = awJ.is_primary_device.booleanValue();
                for (C22086AoB aoB10 : r7.A01) {
                    byte[] bArr4 = awL2.nonce;
                    List list2 = awJ.account_devices;
                    synchronized (aoB10) {
                        ((C55592oK) aoB10.A06.get()).A02(C22086AoB.A0D, booleanValue);
                        aoB10.A0A = TriState.valueOf(booleanValue);
                        C22086AoB.A07(aoB10, AnonymousClass07B.A0C);
                        ((C55592oK) aoB10.A06.get()).A02(C22086AoB.A0C, true);
                        Integer A022 = C22086AoB.A02(aoB10);
                        if (A022 != AnonymousClass07B.A0C) {
                            String A003 = C21815AiC.A00(A022);
                            C11670nb r34 = new C11670nb("tincan_registration");
                            r34.A0D("event_type", "registration_state_set_failure");
                            r34.A0D("nonce_content", AnonymousClass18M.A01(bArr4));
                            r34.A0D("registration_state", A003);
                            ((AnonymousClass18M) AnonymousClass1XX.A02(14, AnonymousClass1Y3.Atj, aoB10.A00)).A00.A09(r34);
                        }
                        AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ASK, aoB10.A00), new C22044AnL(aoB10, list2), -1818490581);
                    }
                }
                r7.A01.A08(awL2.nonce, true, null);
                return;
            }
            throw new RuntimeException(AnonymousClass08S.A0J("Cannot get field 'register_response_payload' because union is currently set to ", awH4.getFieldDesc(i4).A01));
        } else if (this instanceof C36491tD) {
            C36491tD r72 = (C36491tD) this;
            if (awL == null) {
                C010708t.A05(C36491tD.A04, "Could not deserialise create thread response");
                C36491tD.A01(r72, null, null, null);
                return;
            }
            ThreadKey A063 = r72.A06(awL2.nonce);
            if (A063 == null) {
                C010708t.A05(C36491tD.A04, "Could not determine the thread created.");
                C36491tD.A01(r72, null, null, awL2.nonce);
                return;
            }
            AwH awH5 = awL2.body;
            if (awH5 == null || (r8 = awL2.result) == null) {
                C010708t.A05(C36491tD.A04, "Could not deserialise create thread response");
            } else if (!awH5.A05(5)) {
                C010708t.A0B(C36491tD.A04, "Could not deserialise MultiEndpointThread; Error: %d", r8);
            } else {
                if (r8 == AnonymousClass94R.A0B) {
                    int i5 = awH5.setField_;
                    if (i5 == 5) {
                        C22351AwW awW = (C22351AwW) awH5.value_;
                        if (awW != null) {
                            for (C22035AnB anB : r72.A01) {
                                C22336Aw6 aw6 = awW.user_devices;
                                Map map2 = aw6 == null ? null : aw6.participants_list;
                                byte[] bArr5 = awL2.nonce;
                                Long valueOf = Long.valueOf(Long.parseLong((String) anB.A00.A09.get()));
                                ImmutableList.Builder builder = ImmutableList.builder();
                                boolean z2 = false;
                                for (Map.Entry entry : map2.entrySet()) {
                                    List<C22338Aw9> list3 = (List) entry.getValue();
                                    if (!list3.isEmpty()) {
                                        builder.addAll((Iterable) list3);
                                        if (((Long) entry.getKey()).equals(valueOf)) {
                                            String A023 = anB.A00.A04.A02();
                                            for (C22338Aw9 aw9 : list3) {
                                                String str2 = aw9.instance_id;
                                                if (str2 != null && str2.equals(A023)) {
                                                    z2 = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!z2) {
                                    anB.A01(A063, AnonymousClass94R.A0A, bArr5);
                                } else {
                                    ImmutableList build = builder.build();
                                    C22023Amu amu = anB.A00;
                                    amu.A08.A00(amu.A04.A02(), A063.A0G(), null, null);
                                    ImmutableList A0A = anB.A00.A01.A0A(A063);
                                    if (!anB.A00.A04.A00.Aem(283326108010823L) || anB.A00.A04.A00.Aem(283326108928338L) || A0A.size() != build.size()) {
                                        anB.A00.A01.A0D(A063);
                                        anB.A00.A01.A0G(A063, build);
                                        C22023Amu amu2 = anB.A00;
                                        amu2.A01.A0F(A063, amu2.A04.A02());
                                        anB.A00.A01.A0F(A063, AnonymousClass07B.A00);
                                    }
                                    anB.A00.A08.A00(A063);
                                }
                            }
                            return;
                        }
                    } else {
                        throw new RuntimeException(AnonymousClass08S.A0J("Cannot get field 'create_thread_response_payload' because union is currently set to ", awH5.getFieldDesc(i5).A01));
                    }
                }
                C36491tD.A01(r72, A063, r8, awL2.nonce);
                return;
            }
            C36491tD.A01(r72, A063, awL2.result, awL2.nonce);
        } else if (this instanceof C36631tS) {
            C36631tS r9 = (C36631tS) this;
            if (awL == null || awL2.result == null) {
                C010708t.A05(C36631tS.A03, "Could not deserialise batch lookup response");
                return;
            }
            ThreadKey A064 = r9.A06(awL2.nonce);
            if (A064 == null) {
                C010708t.A06(C36631tS.A03, "Unable to determine thread response is for");
                return;
            }
            AwH awH6 = awL2.body;
            if (awH6 == null || !awH6.A05(6)) {
                C010708t.A05(C36631tS.A03, "Could not deserialise BatchLookupResponse");
                AnonymousClass94R r26 = awL2.result;
                for (TincanPreKeyManager A092 : r9.A01) {
                    A092.A09(A064, r26);
                }
                return;
            }
            AnonymousClass94R r27 = awL2.result;
            if (r27 != AnonymousClass94R.A0B) {
                for (TincanPreKeyManager A093 : r9.A01) {
                    A093.A09(A064, r27);
                }
                return;
            }
            int i6 = awH6.setField_;
            if (i6 == 6) {
                C22341AwI awI = (C22341AwI) awH6.value_;
                if (!(awI == null || (map = awI.lookup_results) == null)) {
                    for (Map.Entry value : map.entrySet()) {
                        for (Aw8 aw8 : (List) value.getValue()) {
                            for (TincanPreKeyManager A0B : r9.A01) {
                                A0B.A0B(A064, awL2.date_micros, aw8, true);
                            }
                        }
                    }
                }
                for (TincanPreKeyManager A08 : r9.A01) {
                    A08.A08(A064);
                }
                return;
            }
            throw new RuntimeException(AnonymousClass08S.A0J("Cannot get field 'batch_lookup_response_payload' because union is currently set to ", awH6.getFieldDesc(i6).A01));
        } else if (this instanceof C32391lg) {
            C32391lg r62 = (C32391lg) this;
            if (awL == null || (r2 = awL2.result) == null) {
                C010708t.A05(C32391lg.A06, "Could not deserialise send result");
                return;
            }
            switch (r2.ordinal()) {
                case 0:
                    AwH awH7 = awL2.body;
                    if (awH7 != null && awH7.A05(4)) {
                        int i7 = awH7.setField_;
                        if (i7 == 4) {
                            AwS awS = (AwS) awH7.value_;
                            if (!(awS == null || (l = awS.server_time_micros) == null)) {
                                long longValue = l.longValue();
                                byte[] bArr6 = awS.facebook_hmac;
                                for (C22021Ams ams : r62.A01) {
                                    String A004 = C1936695v.A00(awL2.nonce);
                                    if (A004 != null) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("message_id", A004);
                                        bundle.putLong("timestamp_us", longValue);
                                        bundle.putByteArray("facebook_hmac", bArr6);
                                        ams.A04.newInstance("TincanPostSendMessageUpdate", bundle, 1, CallerContext.A04(C22021Ams.class)).CGe();
                                    }
                                }
                                return;
                            }
                        } else {
                            throw new RuntimeException(AnonymousClass08S.A0J("Cannot get field 'send_result_payload' because union is currently set to ", awH7.getFieldDesc(i7).A01));
                        }
                    }
                    C010708t.A05(C32391lg.A06, "Could not deserialise send result body");
                    return;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    AwH awH8 = awL2.body;
                    if (awH8 != null) {
                        C22336Aw6 A094 = awH8.A09();
                        for (C22021Ams ams2 : r62.A01) {
                            byte[] bArr7 = awL2.nonce;
                            Map map3 = A094.participants_list;
                            String A005 = C1936695v.A00(bArr7);
                            if (A005 != null) {
                                ams2.A09.A07(false, A005, 0, 0, AnonymousClass07B.A0n, null, null);
                                Message A0C = ams2.A0G.A0C(A005);
                                if (A0C == null) {
                                    C010708t.A0I(C22298Ase.$const$string(AnonymousClass1Y3.A12), "Unable to find message where recipient list changed.");
                                } else {
                                    C22021Ams.A03(ams2, A005, A0C.A0U, map3);
                                }
                            }
                        }
                        return;
                    }
                    return;
                default:
                    C010708t.A0B(C32391lg.A06, "Error sending message %d", Integer.valueOf(B36.A02(r2)));
                    for (C22021Ams ams3 : r62.A01) {
                        byte[] bArr8 = awL2.nonce;
                        AnonymousClass94R r82 = awL2.result;
                        String A006 = C1936695v.A00(bArr8);
                        if (A006 != null) {
                            Message A0C2 = ams3.A0G.A0C(A006);
                            ThreadKey threadKey = A0C2 != null ? A0C2.A0U : null;
                            if (r82 != null) {
                                switch (r82.ordinal()) {
                                    case 1:
                                        String A007 = C1936695v.A00(bArr8);
                                        if (A007 != null) {
                                            ams3.A09.A07(false, A007, 0, 0, AnonymousClass07B.A0n, null, null);
                                            Bundle bundle2 = new Bundle();
                                            bundle2.putString("message_id", A007);
                                            ams3.A04.newInstance("TincanRecipientsChanged", bundle2, 1, CallerContext.A04(C22021Ams.class)).CGe();
                                        }
                                        ams3.A0N.A07(threadKey);
                                        break;
                                    case 2:
                                    case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                                    case 8:
                                    case Process.SIGKILL:
                                    case AnonymousClass1Y3.A02:
                                    case AnonymousClass1Y3.A03:
                                    default:
                                        num = AnonymousClass07B.A0N;
                                        C22021Ams.A04(ams3, A006, num, AnonymousClass08S.A0J("ResultCode=", r82.toString()));
                                        break;
                                    case 3:
                                        ams3.A09.A07(false, A006, 0, 0, AnonymousClass07B.A02, null, null);
                                        int i8 = 2131821331;
                                        if (ams3.A0K.A02()) {
                                            i8 = 2131821311;
                                        }
                                        C22021Ams.A05(ams3, A006, ams3.A00.getString(i8));
                                        break;
                                    case 4:
                                        ams3.A0F.A02(threadKey, false, false);
                                        num = AnonymousClass07B.A0N;
                                        C22021Ams.A04(ams3, A006, num, AnonymousClass08S.A0J("ResultCode=", r82.toString()));
                                        break;
                                    case 5:
                                        if (!ams3.A0I.A05(threadKey, true)) {
                                            C22021Ams.A05(ams3, A006, ams3.A00.getString(2131821330));
                                        }
                                        ams3.A0N.A07(threadKey);
                                        ams3.A09.A07(false, A006, 0, 0, AnonymousClass07B.A06, null, null);
                                        break;
                                    case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                                        ams3.A0I.A04(threadKey);
                                        num = AnonymousClass07B.A0F;
                                        C22021Ams.A04(ams3, A006, num, AnonymousClass08S.A0J("ResultCode=", r82.toString()));
                                        break;
                                    case AnonymousClass1Y3.A01:
                                        String A008 = C1936695v.A00(bArr8);
                                        if (A008 == null) {
                                            break;
                                        } else {
                                            C22021Ams.A06(ams3, A008, ams3.A00.getString(2131821339), AnonymousClass07B.A01);
                                            C22021Ams.A05(ams3, A008, ams3.A00.getString(2131821344));
                                            break;
                                        }
                                    case 13:
                                        if (ams3.A0T.Aem(283326108993875L)) {
                                            String r63 = r82.toString();
                                            Integer A032 = An1.A03(((AnonymousClass183) ams3.A0D.A01.get()).A01(), An1.A01(A006, null));
                                            if (A032 != null) {
                                                if (A032.intValue() == 0) {
                                                    C22021Ams.A04(ams3, A006, AnonymousClass07B.A0N, AnonymousClass08S.A0J("ResultCode=", r63));
                                                    break;
                                                }
                                            } else {
                                                ams3.A0D.A04(A006, null);
                                            }
                                            ams3.A0D.A05(A006, null);
                                            C22021Ams.A02(ams3, A006);
                                            break;
                                        }
                                        ams3.A0I.A04(threadKey);
                                        num = AnonymousClass07B.A0F;
                                        C22021Ams.A04(ams3, A006, num, AnonymousClass08S.A0J("ResultCode=", r82.toString()));
                                        break;
                                    case 14:
                                        String A009 = C1936695v.A00(bArr8);
                                        if (A009 == null) {
                                            break;
                                        } else {
                                            C22021Ams.A06(ams3, A009, ams3.A00.getString(2131821339), AnonymousClass07B.A04);
                                            C22021Ams.A05(ams3, A009, ams3.A00.getString(2131821317));
                                            break;
                                        }
                                }
                            } else {
                                C22021Ams.A04(ams3, A006, AnonymousClass07B.A0N, "ResultCode=null");
                            }
                        }
                    }
                    return;
            }
        } else if (!(this instanceof C36741te)) {
            C32401lh r5 = (C32401lh) this;
            if (awL == null || awL2.result == null || (bArr3 = awL2.nonce) == null) {
                C010708t.A05(C32401lh.A08, "Could not deserialise verify checksum response.");
                return;
            }
            ThreadKey A065 = r5.A06(bArr3);
            C32401lh.A01(A065, AnonymousClass08S.A0J("received:", String.valueOf(awL2.result)));
            switch (awL2.result.ordinal()) {
                case 0:
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    Preconditions.checkNotNull(awL2.body);
                    C22336Aw6 A095 = awL2.body.A09();
                    for (C22021Ams A033 : r5.A01) {
                        C22021Ams.A03(A033, null, A065, A095.participants_list);
                    }
                    break;
                default:
                    r5.A04.A07(A065);
                    C010708t.A0C(C32401lh.A08, "Unable to call verify checksum SP : %d ", awL2.result);
                    break;
            }
            switch (awL2.result.ordinal()) {
                case 0:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    r5.A01.A02(A065, true, false);
                    r5.A02.A05(A065, false);
                    if (r5.A03.A00.Aem(283326108010823L)) {
                        Bundle bundle3 = new Bundle();
                        bundle3.putParcelable("thread_key", A065);
                        r5.A00.newInstance("DistributePrekey", bundle3, 1, CallerContext.A04(C32401lh.class)).CGe();
                        return;
                    }
                    return;
                case 1:
                case 2:
                case 3:
                default:
                    return;
                case 4:
                    r5.A01.A02(A065, false, false);
                    return;
                case 5:
                    r5.A02.A05(A065, true);
                    return;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    r5.A02.A04(A065);
                    return;
            }
        } else {
            C36741te r28 = (C36741te) this;
            if (awL == null || (r3 = awL2.result) == null || awL2.nonce == null) {
                cls = C36741te.A03;
            } else if (r3 != AnonymousClass94R.A0B) {
                Class cls2 = C36741te.A03;
                C010708t.A0B(cls2, "response.result = %d", r3);
                C010708t.A05(cls2, "Could not deserialise VerifyPreKeysResponsePayload");
                return;
            } else {
                AwH awH9 = awL2.body;
                if (awH9 == null || !awH9.A05(9)) {
                    cls = C36741te.A03;
                    C010708t.A05(cls, "No valid response payload");
                } else {
                    int i9 = awH9.setField_;
                    if (i9 == 9) {
                        AwG awG = (AwG) awH9.value_;
                        for (TincanPreKeyManager tincanPreKeyManager5 : r28.A01) {
                            AoD aoD = (AoD) tincanPreKeyManager5.A0A.get();
                            List<C22110Aoj> A012 = aoD.A01();
                            AwC awC = awG.signed_pre_key_with_id;
                            boolean z3 = false;
                            for (C22110Aoj aoj : A012) {
                                if (aoj.A00.id_ == awC.public_key_with_id.id.intValue() && Arrays.equals(aoj.A00().A01.A00(), awC.public_key_with_id.public_key)) {
                                    z3 = true;
                                }
                            }
                            if (z3) {
                                AwA awA = awG.last_resort_pre_key;
                                if (!aoD.A03(awA.id.intValue(), awA.public_key)) {
                                    z3 = false;
                                }
                            }
                            if (z3 && !Arrays.equals(((C48942bO) tincanPreKeyManager5.A09.get()).ApI().A00.A00.A00(), awG.identity_key)) {
                                z3 = false;
                            }
                            if (z3) {
                                for (AwA awA2 : awG.pre_keys) {
                                    if (!aoD.A03(awA2.id.intValue(), awA2.public_key)) {
                                        z3 = false;
                                    }
                                }
                            }
                            if (!z3) {
                                tincanPreKeyManager5.A06();
                            }
                        }
                        return;
                    }
                    throw new RuntimeException(AnonymousClass08S.A0J("Cannot get field 'verify_prekeys_response_payload' because union is currently set to ", awH9.getFieldDesc(i9).A01));
                }
            }
            C010708t.A05(cls, "Could not deserialise VerifyPreKeysResponsePayload");
        }
    }

    public synchronized void onSenderAvailable(AnonymousClass2WT r4) {
        try {
            this.A00 = r4;
            A07();
        } catch (Exception e) {
            C010708t.A08(A04, "Error processing available stored procedure sender", e);
        }
        return;
    }

    public synchronized void onSenderInvalidated() {
        try {
            this.A00 = null;
            A08();
        } catch (Exception e) {
            C010708t.A08(A04, "Error processing invalidated stored procedure sender", e);
        }
        return;
    }

    public synchronized void onStoredProcedureResult(ByteBuffer byteBuffer) {
        C22344AwL awL;
        try {
            byte[] array = byteBuffer.array();
            C36291sp r1 = new C36291sp();
            C36311sr r3 = new C36311sr(new ByteArrayInputStream(array));
            C35781ro Azf = r1.Azf(r3);
            try {
                Azf.A0Q();
                Integer num = null;
                AnonymousClass94R r7 = null;
                byte[] bArr = null;
                AwH awH = null;
                Long l = null;
                while (true) {
                    C36241sk A0F = Azf.A0F();
                    byte b = A0F.A00;
                    if (b == 0) {
                        Azf.A0R();
                        awL = new C22344AwL(num, r7, bArr, awH, l);
                        r3.A03();
                        A09(awL);
                    } else {
                        short s = A0F.A02;
                        if (s != 1) {
                            if (s != 2) {
                                if (s != 3) {
                                    if (s != 4) {
                                        if (s == 5 && b == 10) {
                                            l = Long.valueOf(Azf.A0E());
                                            Azf.A0M();
                                        }
                                    } else if (b == 12) {
                                        awH = new AwH();
                                        awH.A03(Azf);
                                        Azf.A0M();
                                    }
                                } else if (b == 11) {
                                    bArr = Azf.A0o();
                                    Azf.A0M();
                                }
                            } else if (b == 8) {
                                int A0C = Azf.A0C();
                                if (A0C == 200) {
                                    r7 = AnonymousClass94R.A0B;
                                } else if (A0C == 308) {
                                    r7 = AnonymousClass94R.A09;
                                } else if (A0C == 409) {
                                    r7 = AnonymousClass94R.A08;
                                } else if (A0C == 415) {
                                    r7 = AnonymousClass94R.A01;
                                } else if (A0C == 423) {
                                    r7 = AnonymousClass94R.A03;
                                } else if (A0C == 426) {
                                    r7 = AnonymousClass94R.A0E;
                                } else if (A0C == 451) {
                                    r7 = AnonymousClass94R.A04;
                                } else if (A0C == 461) {
                                    r7 = AnonymousClass94R.A02;
                                } else if (A0C == 500) {
                                    r7 = AnonymousClass94R.A0C;
                                } else if (A0C == 400) {
                                    r7 = AnonymousClass94R.A05;
                                } else if (A0C == 401) {
                                    r7 = AnonymousClass94R.A0A;
                                } else if (A0C == 505) {
                                    r7 = AnonymousClass94R.A0F;
                                } else if (A0C != 506) {
                                    switch (A0C) {
                                        case AnonymousClass1Y3.A3K:
                                            r7 = AnonymousClass94R.A06;
                                            continue;
                                        case 404:
                                            r7 = AnonymousClass94R.A07;
                                            continue;
                                        case 405:
                                            r7 = AnonymousClass94R.A0G;
                                            continue;
                                        default:
                                            r7 = null;
                                            continue;
                                    }
                                } else {
                                    r7 = AnonymousClass94R.A0D;
                                }
                                Azf.A0M();
                            }
                        } else if (b == 8) {
                            num = Integer.valueOf(Azf.A0C());
                            Azf.A0M();
                        }
                        C74173hO.A00(Azf, b);
                        Azf.A0M();
                    }
                }
            } catch (RuntimeException e) {
                C010708t.A08(A04, e.getMessage(), e);
                awL = null;
                r3.A03();
            } catch (Throwable th) {
                r3.A03();
                throw th;
            }
        } catch (Exception e2) {
            C010708t.A08(A04, "Error processing stored procedure result", e2);
        }
        return;
    }

    public static String A03(String str) {
        return AnonymousClass08S.A03(A07.getAndIncrement(), ":", str);
    }

    public static void A04(C36471tB r3, Exception exc) {
        C11670nb r2 = new C11670nb("tincan_sp_local_failure");
        r2.A0D("sp_id", String.valueOf(r3.A02));
        if (exc != null) {
            r2.A0D("error", exc.getMessage());
        }
        r3.A03.A09(r2);
    }

    public static byte[] A05() {
        byte[] bArr = new byte[32];
        Random random = A06;
        synchronized (random) {
            random.nextBytes(bArr);
        }
        return bArr;
    }

    public ThreadKey A06(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        String str = new String(bArr, A05);
        return ThreadKey.A06(str.substring(str.indexOf(58) + 1));
    }

    public void A0A(Object obj) {
        this.A01.add(obj);
    }

    public void A0B(byte[] bArr) {
        try {
            this.A00.ARD(bArr);
        } catch (OmnistoreIOException e) {
            A04(this, e);
            throw new RuntimeException(e);
        } catch (Exception e2) {
            A04(this, e2);
            throw e2;
        }
    }

    public boolean A0C() {
        if (this.A00 != null) {
            return true;
        }
        return false;
    }

    public int provideStoredProcedureId() {
        return this.A02;
    }

    public C36471tB(int i, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        this.A02 = i;
        this.A03 = deprecatedAnalyticsLogger;
    }
}
