package X;

import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.graphql.query.GQSQStringShape1S0000000_I1;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.20Q  reason: invalid class name */
public final class AnonymousClass20Q implements C177128Fu {
    public AnonymousClass0UN A00;
    public C04810Wg A01;
    public final AtomicReference A02 = new AtomicReference();

    public static final AnonymousClass20Q A00(AnonymousClass1XY r1) {
        return new AnonymousClass20Q(r1);
    }

    public void C6d(C04810Wg r1) {
        this.A01 = r1;
    }

    public void CGl(String str, ViewerContext viewerContext) {
        this.A02.set(null);
        ((AnonymousClass7NO) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BLy, this.A00)).A01();
        this.A02.set(str);
        GQSQStringShape1S0000000_I1 gQSQStringShape1S0000000_I1 = new GQSQStringShape1S0000000_I1(3);
        gQSQStringShape1S0000000_I1.A09("living_room_id", str);
        C26931cN A002 = C26931cN.A00(gQSQStringShape1S0000000_I1);
        A002.A0B(C09290gx.FULLY_CACHED);
        if (viewerContext != null) {
            A002.A0A(viewerContext);
        }
        ((AnonymousClass7NO) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BLy, this.A00)).A02(AnonymousClass08S.A09(str, hashCode()), A002, new AnonymousClass4P8(this), (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BIG, this.A00));
    }

    public void stop() {
        this.A02.set(null);
        ((AnonymousClass7NO) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BLy, this.A00)).A01();
    }

    private AnonymousClass20Q(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
