package X;

/* renamed from: X.0xI  reason: invalid class name and case insensitive filesystem */
public final class C16540xI {
    public C16040wO A00 = C16040wO.INBOX;
    public boolean A01 = false;
    public final C16560xK A02;
    private final C15960wG A03 = new C16570xL(this);
    private final C13420rP A04;

    public static void A00(C16540xI r3, C16040wO r4) {
        C24971Xv it = r3.A04.A01(r4).B6e().iterator();
        while (it.hasNext()) {
            ((C15520vQ) it.next()).Ae6().CIL(r3.A03);
        }
    }

    public static void A01(C16540xI r3, C16040wO r4) {
        C24971Xv it = r3.A04.A01(r4).B6e().iterator();
        while (it.hasNext()) {
            ((C15520vQ) it.next()).Ae6().CK1(r3.A03);
        }
    }

    public C16540xI(C13420rP r2, C16560xK r3) {
        this.A04 = r2;
        this.A02 = r3;
    }
}
