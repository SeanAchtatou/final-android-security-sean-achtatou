package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.11W  reason: invalid class name */
public final class AnonymousClass11W extends C15380vC {
    private static volatile AnonymousClass11W A06;
    public AnonymousClass0UN A00;
    public final AnonymousClass12B A01 = new AnonymousClass12B(this);
    public final C16510xE A02;
    public final AnonymousClass12I A03;
    public final AnonymousClass0r6 A04;
    public final AnonymousClass12A A05 = new C182711a(this);

    public static final AnonymousClass11W A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (AnonymousClass11W.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new AnonymousClass11W(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private AnonymousClass11W(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A04 = C14890uJ.A00(r3);
        this.A03 = AnonymousClass12I.A00(r3);
        this.A02 = C16510xE.A00(r3);
    }
}
