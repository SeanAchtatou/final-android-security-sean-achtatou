package X;

import android.content.Context;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.proxygen.LigerSamplePolicy;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.0mk  reason: invalid class name and case insensitive filesystem */
public final class C11330mk implements AnonymousClass07g {
    private static final AnonymousClass1Y7 A05;
    private static final AnonymousClass1Y7 A06;
    private C01730Bk A00 = null;
    private String A01;
    private final AnonymousClass06B A02;
    private final AnonymousClass1EX A03;
    private final SimpleDateFormat A04;

    static {
        AnonymousClass1Y7 r1 = C05690aA.A0m;
        A05 = (AnonymousClass1Y7) r1.A09("mqtt/");
        A06 = (AnonymousClass1Y7) r1.A09("notification/");
    }

    public List A00() {
        AnonymousClass1EX r7 = this.A03;
        ArrayList arrayList = new ArrayList();
        int AqN = r7.A06.AqN((AnonymousClass1Y7) r7.A07.A09("LOGGER_BUFFER_SIZE"), 1);
        int AqN2 = r7.A06.AqN((AnonymousClass1Y7) r7.A07.A09("LOGGER_BUFFER_TAIL"), 0);
        for (int i = 0; i < AqN; i++) {
            File file = new File(r7.A04.getCacheDir(), AnonymousClass08S.A0A(r7.A09, AqN2, ".txt"));
            if (file.exists()) {
                arrayList.add(file);
            }
            AqN2 = (AqN2 + 1) % 5;
        }
        return arrayList;
    }

    public void A01() {
        Future A022;
        C01730Bk r0 = this.A00;
        if (r0 != null) {
            BIq("DumpSys", r0.B5A());
        } else if (this.A01.equals("mqtt_instance")) {
            BIo("SystemDumper not connected");
        }
        AnonymousClass1EX r5 = this.A03;
        synchronized (r5.A08) {
            ArrayList arrayList = r5.A01;
            r5.A01 = new ArrayList();
            A022 = AnonymousClass07A.A02(r5.A0A, new C82713wQ(r5, arrayList), 73269715);
        }
        try {
            A022.get(LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException unused) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0048, code lost:
        if ((r6.A05.now() - r6.A00) > 60000) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BIo(java.lang.String r10) {
        /*
            r9 = this;
            java.text.SimpleDateFormat r3 = r9.A04
            java.util.Date r2 = new java.util.Date
            X.06B r0 = r9.A02
            long r0 = r0.now()
            r2.<init>(r0)
            java.lang.String r1 = r3.format(r2)
            java.lang.String r0 = " "
            java.lang.String r2 = X.AnonymousClass08S.A0P(r1, r0, r10)
            int r0 = r2.length()
            r1 = 500(0x1f4, float:7.0E-43)
            if (r0 <= r1) goto L_0x0024
            r0 = 0
            java.lang.String r2 = r2.substring(r0, r1)
        L_0x0024:
            X.1EX r6 = r9.A03
            java.lang.Object r4 = r6.A08
            monitor-enter(r4)
            r5 = 0
            java.util.ArrayList r0 = r6.A01     // Catch:{ all -> 0x006f }
            r0.add(r2)     // Catch:{ all -> 0x006f }
            java.util.ArrayList r0 = r6.A01     // Catch:{ all -> 0x006f }
            int r1 = r0.size()     // Catch:{ all -> 0x006f }
            r0 = 50
            if (r1 >= r0) goto L_0x004a
            X.06B r0 = r6.A05     // Catch:{ all -> 0x006f }
            long r7 = r0.now()     // Catch:{ all -> 0x006f }
            long r0 = r6.A00     // Catch:{ all -> 0x006f }
            long r7 = r7 - r0
            r2 = 60000(0xea60, double:2.9644E-319)
            int r1 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x004b
        L_0x004a:
            r0 = 1
        L_0x004b:
            if (r0 == 0) goto L_0x005e
            java.util.ArrayList r5 = r6.A01     // Catch:{ all -> 0x006f }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x006f }
            r0.<init>()     // Catch:{ all -> 0x006f }
            r6.A01 = r0     // Catch:{ all -> 0x006f }
            X.06B r0 = r6.A05     // Catch:{ all -> 0x006f }
            long r0 = r0.now()     // Catch:{ all -> 0x006f }
            r6.A00 = r0     // Catch:{ all -> 0x006f }
        L_0x005e:
            if (r5 == 0) goto L_0x006d
            java.util.concurrent.ExecutorService r2 = r6.A0A     // Catch:{ all -> 0x006f }
            X.0dp r1 = new X.0dp     // Catch:{ all -> 0x006f }
            r1.<init>(r6, r5)     // Catch:{ all -> 0x006f }
            r0 = -1792519342(0xffffffff95285352, float:-3.3993058E-26)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ all -> 0x006f }
        L_0x006d:
            monitor-exit(r4)     // Catch:{ all -> 0x006f }
            return
        L_0x006f:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x006f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11330mk.BIo(java.lang.String):void");
    }

    public void BIp(String str, String str2) {
        BIo(AnonymousClass08S.A0S("[", str, "] ", str2));
    }

    public void BIq(String str, Map map) {
        StringBuilder sb = new StringBuilder("[");
        sb.append(str);
        sb.append("] ");
        for (Map.Entry entry : map.entrySet()) {
            sb.append((String) entry.getKey());
            sb.append("=");
            sb.append((String) entry.getValue());
            sb.append("; ");
        }
        BIo(sb.toString());
    }

    public C11330mk(String str, Context context, FbSharedPreferences fbSharedPreferences, AnonymousClass06B r12, ExecutorService executorService) {
        String str2;
        AnonymousClass1Y7 r5;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TurboLoader.Locator.$const$string(116), Locale.US);
        this.A04 = simpleDateFormat;
        this.A01 = str;
        this.A02 = r12;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        boolean equals = str.equals("mqtt_instance");
        if (equals) {
            str2 = "mqtt_log_event";
        } else {
            str2 = "notification_log_event";
        }
        if (equals) {
            r5 = A05;
        } else {
            r5 = A06;
        }
        Context context2 = context;
        this.A03 = new AnonymousClass1EX(context2, fbSharedPreferences, this.A02, str2, r5, executorService, 20000);
    }

    public void CCF(C01730Bk r1) {
        this.A00 = r1;
    }
}
