package X;

import android.content.res.Resources;
import com.facebook.common.locale.Country;
import com.google.common.collect.ImmutableList;

/* renamed from: X.22f  reason: invalid class name and case insensitive filesystem */
public final class C406022f implements C5O {
    public static final ImmutableList A02 = ImmutableList.of(Country.A00("GB"), Country.A00("FR"));
    private final Resources A00;
    private final BNS A01;

    public static final C406022f A00(AnonymousClass1XY r2) {
        return new C406022f(r2, C04490Ux.A0L(r2));
    }

    public String Ali(AnonymousClass9MF r3) {
        Resources resources;
        int i;
        BNT bnt = (BNT) r3;
        if (Country.A00.equals(bnt.A00)) {
            resources = this.A00;
            i = 2131821243;
        } else if (A02.contains(bnt.A00)) {
            resources = this.A00;
            i = 2131821248;
        } else {
            resources = this.A00;
            i = 2131821242;
        }
        return resources.getString(i);
    }

    public int AtK(Country country) {
        return this.A01.AtK(country);
    }

    public boolean BFR(AnonymousClass9MF r2) {
        return this.A01.BFR(r2);
    }

    public C406022f(AnonymousClass1XY r2, Resources resources) {
        this.A01 = new BNS(r2);
        this.A00 = resources;
    }
}
