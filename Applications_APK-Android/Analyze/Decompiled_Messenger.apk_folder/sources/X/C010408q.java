package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.08q  reason: invalid class name and case insensitive filesystem */
public final class C010408q {
    public static boolean A00(Context context) {
        return new File(context.getApplicationInfo().dataDir, "flags/is_employee").exists();
    }
}
