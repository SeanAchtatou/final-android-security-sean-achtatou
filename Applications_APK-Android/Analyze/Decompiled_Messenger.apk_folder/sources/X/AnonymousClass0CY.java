package X;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

/* renamed from: X.0CY  reason: invalid class name */
public final class AnonymousClass0CY extends FutureTask implements C01950Cg {
    private final C01960Ch A00 = new C01960Ch();

    public void addListener(Runnable runnable, Executor executor) {
        boolean z;
        C01960Ch r1 = this.A00;
        AnonymousClass0A1.A00(runnable);
        AnonymousClass0A1.A00(executor);
        synchronized (r1.A01) {
            if (!r1.A00) {
                r1.A01.add(new AnonymousClass0D6(runnable, executor));
                z = false;
            } else {
                z = true;
            }
        }
        if (z) {
            new AnonymousClass0D6(runnable, executor).A00();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0015, code lost:
        if (r2.A01.isEmpty() != false) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        ((X.AnonymousClass0D6) r2.A01.poll()).A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void done() {
        /*
            r3 = this;
            X.0Ch r2 = r3.A00
            java.util.Queue r1 = r2.A01
            monitor-enter(r1)
            boolean r0 = r2.A00     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x0024 }
            return
        L_0x000b:
            r0 = 1
            r2.A00 = r0     // Catch:{ all -> 0x0024 }
            monitor-exit(r1)     // Catch:{ all -> 0x0024 }
        L_0x000f:
            java.util.Queue r0 = r2.A01
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0023
            java.util.Queue r0 = r2.A01
            java.lang.Object r0 = r0.poll()
            X.0D6 r0 = (X.AnonymousClass0D6) r0
            r0.A00()
            goto L_0x000f
        L_0x0023:
            return
        L_0x0024:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0024 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0CY.done():void");
    }

    public AnonymousClass0CY(Runnable runnable, Object obj) {
        super(runnable, obj);
    }

    public AnonymousClass0CY(Callable callable) {
        super(callable);
    }
}
