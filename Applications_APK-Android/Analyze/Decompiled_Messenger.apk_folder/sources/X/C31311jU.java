package X;

import com.facebook.payments.contactinfo.form.ContactInfoCommonFormParams;
import com.facebook.payments.contactinfo.model.ContactInfoFormInput;

/* renamed from: X.1jU  reason: invalid class name and case insensitive filesystem */
public final class C31311jU extends C06020ai {
    public final /* synthetic */ ContactInfoCommonFormParams A00;
    public final /* synthetic */ C54232m5 A01;
    public final /* synthetic */ ContactInfoFormInput A02;
    public final /* synthetic */ boolean A03;

    public C31311jU(C54232m5 r1, ContactInfoCommonFormParams contactInfoCommonFormParams, ContactInfoFormInput contactInfoFormInput, boolean z) {
        this.A01 = r1;
        this.A00 = contactInfoCommonFormParams;
        this.A02 = contactInfoFormInput;
        this.A03 = z;
    }
}
