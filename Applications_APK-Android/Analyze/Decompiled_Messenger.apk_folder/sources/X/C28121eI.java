package X;

import android.app.Activity;
import android.content.Intent;
import com.facebook.secure.intentswitchoff.ActivityIntentSwitchOffDI;

/* renamed from: X.1eI  reason: invalid class name and case insensitive filesystem */
public class C28121eI extends AnonymousClass06j {
    public /* bridge */ /* synthetic */ void A04(Object obj, Intent intent) {
        if (!(this instanceof ActivityIntentSwitchOffDI)) {
            A05((Activity) obj, intent);
        } else {
            ((ActivityIntentSwitchOffDI) this).A05((Activity) obj, intent);
        }
    }

    public void A05(Activity activity, Intent intent) {
        activity.finish();
    }

    public C28121eI(C007306v r1) {
        super(r1);
    }
}
