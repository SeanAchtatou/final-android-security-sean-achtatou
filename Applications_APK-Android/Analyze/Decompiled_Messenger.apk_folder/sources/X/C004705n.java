package X;

import com.facebook.profilo.ipc.TraceContext;
import java.io.File;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.05n  reason: invalid class name and case insensitive filesystem */
public final class C004705n implements AnonymousClass09H {
    public final CopyOnWriteArrayList A00 = new CopyOnWriteArrayList();

    public void BTJ() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).BTJ();
        }
    }

    public void Bds(Throwable th) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).Bds(th);
        }
    }

    public void BgU() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).BgU();
        }
    }

    public void BkO() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).BkO();
        }
    }

    public void BkP(int i) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).BkP(i);
        }
    }

    public void Bsa(File file, long j) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).Bsa(file, j);
        }
    }

    public void Bsb(int i, int i2, int i3, int i4) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).Bsb(i, i2, i3, i4);
        }
    }

    public void Btg(File file) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).Btg(file);
        }
    }

    public void Bth(File file) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).Bth(file);
        }
    }

    public void onTraceAbort(TraceContext traceContext) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).onTraceAbort(traceContext);
        }
    }

    public void onTraceStart(TraceContext traceContext) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).onTraceStart(traceContext);
        }
    }

    public void onTraceStop(TraceContext traceContext) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).onTraceStop(traceContext);
        }
    }

    public void onTraceWriteAbort(long j, int i) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).onTraceWriteAbort(j, i);
        }
    }

    public void onTraceWriteEnd(long j, int i) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).onTraceWriteEnd(j, i);
        }
    }

    public void onTraceWriteStart(long j, int i, String str) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass09H) it.next()).onTraceWriteStart(j, i, str);
        }
    }
}
