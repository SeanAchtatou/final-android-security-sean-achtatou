package X;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.1PZ  reason: invalid class name */
public final class AnonymousClass1PZ implements C23311Pa, C16490xB {
    public C30851ik A00;
    private long A01 = SystemClock.uptimeMillis();
    public final C23321Pb A02;
    public final C23321Pb A03;
    public final AnonymousClass1PY A04;
    private final C23111Og A05;
    private final C30831ii A06;

    private synchronized int A00() {
        return this.A02.A00() - this.A03.A00();
    }

    private synchronized int A01() {
        return this.A02.A01() - this.A03.A01();
    }

    private synchronized AnonymousClass1PS A02(C33251nH r5) {
        synchronized (this) {
            C05520Zg.A02(r5);
            boolean z = false;
            if (!r5.A01) {
                z = true;
            }
            C05520Zg.A05(z);
            r5.A00++;
        }
        return AnonymousClass1PS.A02(r5.A02.A0A(), new C24111Si(this, r5));
    }

    public static synchronized AnonymousClass1PS A03(AnonymousClass1PZ r1, C33251nH r2) {
        AnonymousClass1PS r0;
        synchronized (r1) {
            C05520Zg.A02(r2);
            if (!r2.A01 || r2.A00 != 0) {
                r0 = null;
            } else {
                r0 = r2.A02;
            }
        }
        return r0;
    }

    private synchronized ArrayList A04(int i, int i2) {
        Object obj;
        int max = Math.max(i, 0);
        int max2 = Math.max(i2, 0);
        if (this.A03.A00() <= max && this.A03.A01() <= max2) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        while (true) {
            if (this.A03.A00() <= max && this.A03.A01() <= max2) {
                return arrayList;
            }
            C23321Pb r2 = this.A03;
            synchronized (r2) {
                if (r2.A02.isEmpty()) {
                    obj = null;
                } else {
                    obj = r2.A02.keySet().iterator().next();
                }
            }
            this.A03.A02(obj);
            arrayList.add(this.A02.A02(obj));
        }
    }

    private synchronized void A05(C33251nH r4) {
        C05520Zg.A02(r4);
        boolean z = false;
        if (!r4.A01) {
            z = true;
        }
        C05520Zg.A05(z);
        r4.A01 = true;
    }

    public static void A06(AnonymousClass1PZ r4) {
        ArrayList A042;
        synchronized (r4) {
            C30851ik r0 = r4.A00;
            int min = Math.min(r0.A03, r0.A00 - r4.A00());
            C30851ik r02 = r4.A00;
            A042 = r4.A04(min, Math.min(r02.A04, r02.A02 - r4.A01()));
            r4.A0A(A042);
        }
        r4.A08(A042);
        A09(A042);
    }

    public static synchronized void A07(AnonymousClass1PZ r5) {
        synchronized (r5) {
            if (r5.A01 + r5.A00.A05 <= SystemClock.uptimeMillis()) {
                r5.A01 = SystemClock.uptimeMillis();
                r5.A00 = (C30851ik) r5.A05.get();
            }
        }
    }

    private synchronized void A0A(ArrayList arrayList) {
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                A05((C33251nH) it.next());
            }
        }
    }

    public AnonymousClass1PS AR1(Object obj, AnonymousClass1PS r3) {
        return A0B(obj, r3, null);
    }

    public synchronized boolean AU9(AnonymousClass8SZ r6) {
        ArrayList arrayList;
        C23321Pb r4 = this.A02;
        synchronized (r4) {
            arrayList = new ArrayList(r4.A02.entrySet().size());
            for (Map.Entry entry : r4.A02.entrySet()) {
                if (r6 == null || r6.apply(entry.getKey())) {
                    arrayList.add(entry);
                }
            }
        }
        return !arrayList.isEmpty();
    }

    public int C1M(AnonymousClass8SZ r3) {
        ArrayList A032;
        ArrayList A033;
        synchronized (this) {
            A032 = this.A03.A03(r3);
            A033 = this.A02.A03(r3);
            A0A(A033);
        }
        A08(A033);
        A09(A032);
        A07(this);
        A06(this);
        return A033.size();
    }

    public synchronized boolean contains(Object obj) {
        boolean containsKey;
        C23321Pb r1 = this.A02;
        synchronized (r1) {
            containsKey = r1.A02.containsKey(obj);
        }
        return containsKey;
    }

    public synchronized int getCount() {
        return this.A02.A00();
    }

    public synchronized int getSizeInBytes() {
        return this.A02.A01();
    }

    private void A08(ArrayList arrayList) {
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                AnonymousClass1PS.A05(A03(this, (C33251nH) it.next()));
            }
        }
    }

    private static void A09(ArrayList arrayList) {
        C179848St r2;
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C33251nH r0 = (C33251nH) it.next();
                if (!(r0 == null || (r2 = r0.A03) == null)) {
                    r2.A00(r0.A04, false);
                }
            }
        }
    }

    public String A0C() {
        AnonymousClass217 r2 = new AnonymousClass217("CountingMemoryCache");
        AnonymousClass217.A00(r2, "cached_entries_count:", String.valueOf(this.A02.A00()));
        AnonymousClass217.A00(r2, "cached_entries_size_bytes", String.valueOf(this.A02.A01()));
        AnonymousClass217.A00(r2, "exclusive_entries_count", String.valueOf(this.A03.A00()));
        AnonymousClass217.A00(r2, "exclusive_entries_size_bytes", String.valueOf(this.A03.A01()));
        return r2.toString();
    }

    public void trim(C133746Nn r8) {
        ArrayList A042;
        double B7D = this.A06.B7D(r8);
        synchronized (this) {
            double A012 = (double) this.A02.A01();
            Double.isNaN(A012);
            A042 = A04(Integer.MAX_VALUE, Math.max(0, ((int) (A012 * (1.0d - B7D))) - A01()));
            A0A(A042);
        }
        A08(A042);
        A09(A042);
        A07(this);
        A06(this);
    }

    public AnonymousClass1PZ(AnonymousClass1PY r3, C30831ii r4, C23111Og r5) {
        new WeakHashMap();
        this.A04 = r3;
        this.A03 = new C23321Pb(new C30841ij(r3));
        this.A02 = new C23321Pb(new C30841ij(r3));
        this.A06 = r4;
        this.A05 = r5;
        this.A00 = (C30851ik) r5.get();
    }

    public AnonymousClass1PS A0B(Object obj, AnonymousClass1PS r10, C179848St r11) {
        C33251nH r4;
        AnonymousClass1PS r7;
        AnonymousClass1PS r6;
        boolean z;
        C179848St r2;
        C05520Zg.A02(obj);
        C05520Zg.A02(r10);
        A07(this);
        synchronized (this) {
            r4 = (C33251nH) this.A03.A02(obj);
            C33251nH r0 = (C33251nH) this.A02.A02(obj);
            r7 = null;
            if (r0 != null) {
                A05(r0);
                r6 = A03(this, r0);
            } else {
                r6 = null;
            }
            Object A0A = r10.A0A();
            synchronized (this) {
                int B3B = this.A04.B3B(A0A);
                z = true;
                if (B3B > this.A00.A01 || A00() > this.A00.A00 - 1 || A01() > this.A00.A02 - B3B) {
                    z = false;
                }
            }
            if (z) {
                C33251nH r1 = new C33251nH(obj, r10, r11);
                this.A02.A04(obj, r1);
                r7 = A02(r1);
            }
        }
        AnonymousClass1PS.A05(r6);
        if (!(r4 == null || (r2 = r4.A03) == null)) {
            r2.A00(r4.A04, false);
        }
        A06(this);
        return r7;
    }

    public AnonymousClass1PS Ab8(Object obj) {
        C33251nH r4;
        Object obj2;
        AnonymousClass1PS r3;
        C179848St r2;
        C05520Zg.A02(obj);
        synchronized (this) {
            r4 = (C33251nH) this.A03.A02(obj);
            C23321Pb r1 = this.A02;
            synchronized (r1) {
                obj2 = r1.A02.get(obj);
            }
            C33251nH r0 = (C33251nH) obj2;
            if (r0 != null) {
                r3 = A02(r0);
            } else {
                r3 = null;
            }
        }
        if (!(r4 == null || (r2 = r4.A03) == null)) {
            r2.A00(r4.A04, false);
        }
        A07(this);
        A06(this);
        return r3;
    }
}
