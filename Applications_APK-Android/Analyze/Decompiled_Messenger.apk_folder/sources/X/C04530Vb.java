package X;

import java.util.Collections;
import java.util.Comparator;

/* renamed from: X.0Vb  reason: invalid class name and case insensitive filesystem */
public final class C04530Vb implements Comparator {
    public static C04530Vb A00;
    public static Comparator A01;

    static {
        C04530Vb r0 = new C04530Vb();
        A00 = r0;
        A01 = Collections.reverseOrder(r0);
    }

    public int compare(Object obj, Object obj2) {
        return A00((C07820eD) obj, (C07820eD) obj2);
    }

    public static int A00(C07820eD r5, C07820eD r6) {
        int ordinal = r6.By8().ordinal() - r5.By8().ordinal();
        if (ordinal != 0) {
            return ordinal;
        }
        long C5W = r5.C5W() - r6.C5W();
        if (C5W == 0) {
            return 0;
        }
        if (C5W < 0) {
            return -1;
        }
        return 1;
    }
}
