package X;

import android.net.Uri;
import com.google.common.base.Optional;

/* renamed from: X.1nv  reason: invalid class name and case insensitive filesystem */
public final class C33651nv {
    public final int A00;
    public final long A01;
    public final Uri A02;
    public final Optional A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final boolean A07;
    public final boolean A08;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C33651nv)) {
            return false;
        }
        C33651nv r7 = (C33651nv) obj;
        return this.A02.equals(r7.A02) && this.A00 == r7.A00 && this.A01 == r7.A01 && this.A03.equals(r7.A03) && this.A08 == r7.A08 && this.A07 == r7.A07 && this.A05.equals(r7.A05) && this.A04.equals(r7.A04) && this.A06.equals(r7.A06);
    }

    public int hashCode() {
        return this.A02.hashCode();
    }

    public C33651nv(Uri uri, int i, long j, Optional optional, boolean z, boolean z2, String str, String str2, String str3) {
        this.A02 = uri;
        this.A00 = i;
        this.A01 = j;
        this.A03 = optional;
        this.A08 = z;
        this.A07 = z2;
        this.A05 = str;
        this.A04 = str2;
        this.A06 = str3;
    }
}
