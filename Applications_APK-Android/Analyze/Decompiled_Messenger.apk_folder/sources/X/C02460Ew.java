package X;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0Ew  reason: invalid class name and case insensitive filesystem */
public final class C02460Ew {
    public final Integer A00;
    public final Map A01 = new HashMap();

    private static void A00(AnonymousClass0D8 r3, AnonymousClass08m r4) {
        byte b = r4.A00;
        short s = r4.A01;
        if (b == 2) {
            r3.A00 = Byte.valueOf(b);
            r3.A01 = Short.valueOf(s);
            return;
        }
        AnonymousClass0D8.A00(r3, b, s, (byte) -1);
    }

    public C02460Ew(Integer num) {
        this.A00 = num;
    }

    public void A01(AnonymousClass0D8 r7) {
        byte b;
        AnonymousClass08m r1;
        Map map;
        switch (this.A00.intValue()) {
            case 0:
                r7.A02();
                Map map2 = this.A01;
                AnonymousClass08m r12 = C04080Rt.A0P;
                if (map2.containsKey(r12) && this.A01.get(r12) != null) {
                    A00(r7, r12);
                    r7.A05(((Long) this.A01.get(r12)).longValue());
                }
                Map map3 = this.A01;
                AnonymousClass08m r13 = C04080Rt.A0O;
                if (map3.containsKey(r13) && this.A01.get(r13) != null) {
                    A00(r7, r13);
                    r7.A06((String) this.A01.get(r13));
                }
                Map map4 = this.A01;
                AnonymousClass08m r14 = C04080Rt.A01;
                if (map4.containsKey(r14) && this.A01.get(r14) != null) {
                    A00(r7, r14);
                    r7.A05(((Long) this.A01.get(r14)).longValue());
                }
                Map map5 = this.A01;
                AnonymousClass08m r15 = C04080Rt.A09;
                if (map5.containsKey(r15) && this.A01.get(r15) != null) {
                    A00(r7, r15);
                    r7.A05(((Long) this.A01.get(r15)).longValue());
                }
                Map map6 = this.A01;
                AnonymousClass08m r16 = C04080Rt.A0L;
                if (map6.containsKey(r16) && this.A01.get(r16) != null) {
                    A00(r7, r16);
                    int intValue = ((Integer) this.A01.get(r16)).intValue();
                    AnonymousClass0D8.A01(r7, (intValue >> 31) ^ (intValue << 1));
                }
                Map map7 = this.A01;
                AnonymousClass08m r17 = C04080Rt.A0J;
                if (map7.containsKey(r17) && this.A01.get(r17) != null) {
                    A00(r7, r17);
                    r7.A07(((Boolean) this.A01.get(r17)).booleanValue());
                }
                Map map8 = this.A01;
                AnonymousClass08m r18 = C04080Rt.A0G;
                if (map8.containsKey(r18) && this.A01.get(r18) != null) {
                    A00(r7, r18);
                    r7.A07(((Boolean) this.A01.get(r18)).booleanValue());
                }
                Map map9 = this.A01;
                AnonymousClass08m r19 = C04080Rt.A07;
                if (map9.containsKey(r19) && this.A01.get(r19) != null) {
                    A00(r7, r19);
                    r7.A06((String) this.A01.get(r19));
                }
                Map map10 = this.A01;
                AnonymousClass08m r110 = C04080Rt.A0E;
                if (map10.containsKey(r110) && this.A01.get(r110) != null) {
                    A00(r7, r110);
                    r7.A07(((Boolean) this.A01.get(r110)).booleanValue());
                }
                Map map11 = this.A01;
                AnonymousClass08m r111 = C04080Rt.A0I;
                if (map11.containsKey(r111) && this.A01.get(r111) != null) {
                    A00(r7, r111);
                    int intValue2 = ((Integer) this.A01.get(r111)).intValue();
                    AnonymousClass0D8.A01(r7, (intValue2 >> 31) ^ (intValue2 << 1));
                }
                Map map12 = this.A01;
                AnonymousClass08m r112 = C04080Rt.A0H;
                if (map12.containsKey(r112) && this.A01.get(r112) != null) {
                    A00(r7, r112);
                    int intValue3 = ((Integer) this.A01.get(r112)).intValue();
                    AnonymousClass0D8.A01(r7, (intValue3 >> 31) ^ (intValue3 << 1));
                }
                Map map13 = this.A01;
                AnonymousClass08m r113 = C04080Rt.A03;
                if (map13.containsKey(r113) && this.A01.get(r113) != null) {
                    A00(r7, r113);
                    r7.A05(((Long) this.A01.get(r113)).longValue());
                }
                Map map14 = this.A01;
                AnonymousClass08m r114 = C04080Rt.A02;
                if (map14.containsKey(r114) && this.A01.get(r114) != null) {
                    A00(r7, r114);
                    r7.A06((String) this.A01.get(r114));
                }
                Map map15 = this.A01;
                AnonymousClass08m r115 = C04080Rt.A0N;
                if (map15.containsKey(r115) && this.A01.get(r115) != null) {
                    A00(r7, r115);
                    List<Integer> list = (List) this.A01.get(r115);
                    r7.A04((byte) 8, list.size());
                    for (Integer intValue4 : list) {
                        int intValue5 = intValue4.intValue();
                        AnonymousClass0D8.A01(r7, (intValue5 >> 31) ^ (intValue5 << 1));
                    }
                }
                Map map16 = this.A01;
                AnonymousClass08m r116 = C04080Rt.A05;
                if (map16.containsKey(r116) && this.A01.get(r116) != null) {
                    A00(r7, r116);
                    r7.A06((String) this.A01.get(r116));
                }
                Map map17 = this.A01;
                AnonymousClass08m r117 = C04080Rt.A00;
                if (map17.containsKey(r117) && this.A01.get(r117) != null) {
                    A00(r7, r117);
                    r7.A05(((Long) this.A01.get(r117)).longValue());
                }
                Map map18 = this.A01;
                AnonymousClass08m r118 = C04080Rt.A0K;
                if (map18.containsKey(r118) && this.A01.get(r118) != null) {
                    A00(r7, r118);
                    r7.A07(((Boolean) this.A01.get(r118)).booleanValue());
                }
                Map map19 = this.A01;
                AnonymousClass08m r119 = C04080Rt.A06;
                if (map19.containsKey(r119) && this.A01.get(r119) != null) {
                    A00(r7, r119);
                    byte[] bArr = (byte[]) this.A01.get(r119);
                    int length = bArr.length;
                    AnonymousClass0D8.A01(r7, length);
                    r7.A04.write(bArr, 0, length);
                }
                Map map20 = this.A01;
                AnonymousClass08m r120 = C04080Rt.A0M;
                if (map20.containsKey(r120) && this.A01.get(r120) != null) {
                    A00(r7, r120);
                    r7.A06((String) this.A01.get(r120));
                }
                Map map21 = this.A01;
                AnonymousClass08m r121 = C04080Rt.A08;
                if (map21.containsKey(r121) && this.A01.get(r121) != null) {
                    A00(r7, r121);
                    r7.A06((String) this.A01.get(r121));
                }
                Map map22 = this.A01;
                AnonymousClass08m r122 = C04080Rt.A0F;
                if (map22.containsKey(r122) && this.A01.get(r122) != null) {
                    A00(r7, r122);
                    r7.A05(((Long) this.A01.get(r122)).longValue());
                }
                Map map23 = this.A01;
                AnonymousClass08m r123 = C04080Rt.A04;
                if (map23.containsKey(r123) && this.A01.get(r123) != null) {
                    A00(r7, r123);
                    r7.A04.write(((Byte) this.A01.get(r123)).byteValue());
                }
                Map map24 = this.A01;
                AnonymousClass08m r124 = C04080Rt.A0A;
                if (map24.containsKey(r124) && this.A01.get(r124) != null) {
                    A00(r7, r124);
                    r7.A05(((Long) this.A01.get(r124)).longValue());
                }
                Map map25 = this.A01;
                AnonymousClass08m r125 = C04080Rt.A0B;
                if (map25.containsKey(r125) && this.A01.get(r125) != null) {
                    A00(r7, r125);
                    r7.A06((String) this.A01.get(r125));
                }
                Map map26 = this.A01;
                AnonymousClass08m r126 = C04080Rt.A0C;
                if (map26.containsKey(r126) && this.A01.get(r126) != null) {
                    A00(r7, r126);
                    r7.A06((String) this.A01.get(r126));
                }
                map = this.A01;
                r1 = C04080Rt.A0D;
                if (map.containsKey(r1) && this.A01.get(r1) != null) {
                    A00(r7, r1);
                    r7.A06((String) this.A01.get(r1));
                    break;
                }
            case 1:
                r7.A02();
                Map map27 = this.A01;
                AnonymousClass08m r127 = C04080Rt.A0n;
                if (map27.containsKey(r127) && this.A01.get(r127) != null) {
                    A00(r7, r127);
                    r7.A06((String) this.A01.get(r127));
                }
                Map map28 = this.A01;
                AnonymousClass08m r128 = C04080Rt.A0i;
                if (map28.containsKey(r128) && this.A01.get(r128) != null) {
                    A00(r7, r128);
                    r7.A05(((Long) this.A01.get(r128)).longValue());
                    Map map29 = this.A01;
                    AnonymousClass08m r129 = C04080Rt.A0j;
                    if (map29.containsKey(r129) && this.A01.get(r129) != null) {
                        A00(r7, r129);
                        int intValue6 = ((Integer) this.A01.get(r129)).intValue();
                        AnonymousClass0D8.A01(r7, (intValue6 >> 31) ^ (intValue6 << 1));
                    }
                    Map map30 = this.A01;
                    AnonymousClass08m r130 = C04080Rt.A0d;
                    if (map30.containsKey(r130) && this.A01.get(r130) != null) {
                        A00(r7, r130);
                        int intValue7 = ((Integer) this.A01.get(r130)).intValue();
                        AnonymousClass0D8.A01(r7, (intValue7 >> 31) ^ (intValue7 << 1));
                    }
                    Map map31 = this.A01;
                    AnonymousClass08m r131 = C04080Rt.A0g;
                    if (map31.containsKey(r131) && this.A01.get(r131) != null) {
                        A00(r7, r131);
                        r7.A06((String) this.A01.get(r131));
                    }
                    Map map32 = this.A01;
                    AnonymousClass08m r132 = C04080Rt.A0l;
                    if (map32.containsKey(r132) && this.A01.get(r132) != null) {
                        A00(r7, r132);
                        r7.A06((String) this.A01.get(r132));
                    }
                    Map map33 = this.A01;
                    AnonymousClass08m r133 = C04080Rt.A0m;
                    if (map33.containsKey(r133) && this.A01.get(r133) != null) {
                        A00(r7, r133);
                        int intValue8 = ((Integer) this.A01.get(r133)).intValue();
                        AnonymousClass0D8.A01(r7, (intValue8 >> 31) ^ (intValue8 << 1));
                    }
                    Map map34 = this.A01;
                    AnonymousClass08m r134 = C04080Rt.A0e;
                    if (map34.containsKey(r134) && this.A01.get(r134) != null) {
                        A00(r7, r134);
                        r7.A06((String) this.A01.get(r134));
                    }
                    Map map35 = this.A01;
                    AnonymousClass08m r135 = C04080Rt.A0f;
                    if (map35.containsKey(r135) && this.A01.get(r135) != null) {
                        A00(r7, r135);
                        r7.A06((String) this.A01.get(r135));
                    }
                    Map map36 = this.A01;
                    AnonymousClass08m r136 = C04080Rt.A0k;
                    if (map36.containsKey(r136) && this.A01.get(r136) != null) {
                        A00(r7, r136);
                        r7.A06((String) this.A01.get(r136));
                    }
                    Map map37 = this.A01;
                    AnonymousClass08m r137 = C04080Rt.A0h;
                    if (map37.containsKey(r137) && this.A01.get(r137) != null) {
                        A00(r7, r137);
                        r7.A05(((Long) this.A01.get(r137)).longValue());
                    }
                    Map map38 = this.A01;
                    AnonymousClass08m r138 = C04080Rt.A0o;
                    if (map38.containsKey(r138) && this.A01.get(r138) != null) {
                        A00(r7, r138);
                        r7.A05(((Long) this.A01.get(r138)).longValue());
                        break;
                    }
                } else {
                    throw new IOException("Required field 'GetIrisDiffs.lastSeqId' was not present!");
                }
                break;
            case 2:
                r7.A02();
                Map map39 = this.A01;
                AnonymousClass08m r139 = C04080Rt.A0q;
                if (map39.containsKey(r139) && this.A01.get(r139) != null) {
                    A00(r7, r139);
                    r7.A06((String) this.A01.get(r139));
                }
                Map map40 = this.A01;
                AnonymousClass08m r140 = C04080Rt.A0p;
                if (map40.containsKey(r140) && this.A01.get(r140) != null) {
                    A00(r7, r140);
                    r7.A06((String) this.A01.get(r140));
                }
                map = this.A01;
                r1 = C04080Rt.A0r;
                A00(r7, r1);
                r7.A06((String) this.A01.get(r1));
                break;
            case 3:
                r7.A02();
                Map map41 = this.A01;
                AnonymousClass08m r141 = C04080Rt.A0S;
                if (map41.containsKey(r141) && this.A01.get(r141) != null) {
                    A00(r7, r141);
                    r7.A06((String) this.A01.get(r141));
                }
                Map map42 = this.A01;
                AnonymousClass08m r142 = C04080Rt.A0Q;
                if (map42.containsKey(r142) && this.A01.get(r142) != null) {
                    A00(r7, r142);
                    int intValue9 = ((Integer) this.A01.get(r142)).intValue();
                    AnonymousClass0D8.A01(r7, (intValue9 >> 31) ^ (intValue9 << 1));
                }
                Map map43 = this.A01;
                AnonymousClass08m r143 = C04080Rt.A0R;
                if (map43.containsKey(r143) && this.A01.get(r143) != null) {
                    A00(r7, r143);
                    byte[] bArr2 = (byte[]) this.A01.get(r143);
                    int length2 = bArr2.length;
                    AnonymousClass0D8.A01(r7, length2);
                    r7.A04.write(bArr2, 0, length2);
                    break;
                }
            case 4:
                r7.A02();
                Map map44 = this.A01;
                AnonymousClass08m r144 = C04080Rt.A0U;
                if (map44.containsKey(r144) && this.A01.get(r144) != null) {
                    A00(r7, r144);
                    r7.A06((String) this.A01.get(r144));
                }
                Map map45 = this.A01;
                AnonymousClass08m r145 = C04080Rt.A0b;
                if (map45.containsKey(r145) && this.A01.get(r145) != null) {
                    A00(r7, r145);
                    r7.A06((String) this.A01.get(r145));
                }
                Map map46 = this.A01;
                AnonymousClass08m r146 = C04080Rt.A0a;
                if (map46.containsKey(r146) && this.A01.get(r146) != null) {
                    A00(r7, r146);
                    r7.A06((String) this.A01.get(r146));
                }
                Map map47 = this.A01;
                AnonymousClass08m r147 = C04080Rt.A0V;
                if (map47.containsKey(r147) && this.A01.get(r147) != null) {
                    A00(r7, r147);
                    ((C02460Ew) this.A01.get(r147)).A01(r7);
                }
                Map map48 = this.A01;
                AnonymousClass08m r148 = C04080Rt.A0Y;
                if (map48.containsKey(r148) && this.A01.get(r148) != null) {
                    A00(r7, r148);
                    r7.A06((String) this.A01.get(r148));
                }
                if (this.A01.containsKey(C04080Rt.A0X)) {
                    Map map49 = this.A01;
                    AnonymousClass08m r149 = C04080Rt.A0X;
                    if (map49.get(r149) != null) {
                        A00(r7, r149);
                        List<byte[]> list2 = (List) this.A01.get(r149);
                        r7.A04((byte) 11, list2.size());
                        for (byte[] bArr3 : list2) {
                            int length3 = bArr3.length;
                            AnonymousClass0D8.A01(r7, length3);
                            r7.A04.write(bArr3, 0, length3);
                        }
                    }
                }
                if (this.A01.containsKey(C04080Rt.A0Z)) {
                    Map map50 = this.A01;
                    AnonymousClass08m r150 = C04080Rt.A0Z;
                    if (map50.get(r150) != null) {
                        A00(r7, r150);
                        List<C02460Ew> list3 = (List) this.A01.get(r150);
                        r7.A04((byte) 12, list3.size());
                        for (C02460Ew A012 : list3) {
                            A012.A01(r7);
                        }
                    }
                }
                Map map51 = this.A01;
                AnonymousClass08m r151 = C04080Rt.A0W;
                if (map51.containsKey(r151) && this.A01.get(r151) != null) {
                    A00(r7, r151);
                    List<C02460Ew> list4 = (List) this.A01.get(r151);
                    r7.A04((byte) 12, list4.size());
                    for (C02460Ew A013 : list4) {
                        A013.A01(r7);
                    }
                }
                Map map52 = this.A01;
                AnonymousClass08m r152 = C04080Rt.A0c;
                if (map52.containsKey(r152) && this.A01.get(r152) != null) {
                    A00(r7, r152);
                    r7.A06((String) this.A01.get(r152));
                }
                Map map53 = this.A01;
                AnonymousClass08m r153 = C04080Rt.A0T;
                if (map53.containsKey(r153) && this.A01.get(r153) != null) {
                    A00(r7, r153);
                    Map map54 = (Map) this.A01.get(r153);
                    int size = map54.size();
                    if (size == 0) {
                        b = 0;
                    } else {
                        AnonymousClass0D8.A01(r7, size);
                        byte b2 = AnonymousClass0D8.A06[11];
                        b = (b2 << 4) | b2;
                    }
                    r7.A04.write((byte) b);
                    for (Map.Entry entry : map54.entrySet()) {
                        r7.A06((String) entry.getKey());
                        r7.A06((String) entry.getValue());
                    }
                    break;
                }
            default:
                return;
        }
        r7.A04.write(0);
        r7.A03();
    }
}
