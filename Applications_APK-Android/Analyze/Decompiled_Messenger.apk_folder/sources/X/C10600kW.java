package X;

import android.os.IBinder;

/* renamed from: X.0kW  reason: invalid class name and case insensitive filesystem */
public final class C10600kW implements IBinder.DeathRecipient {
    public final /* synthetic */ C06130au A00;
    public final /* synthetic */ C07930eP A01;

    public C10600kW(C07930eP r1, C06130au r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void binderDied() {
        AnonymousClass00S.A04(this.A01.A01, new AnonymousClass9SQ(this), -2046101089);
    }
}
