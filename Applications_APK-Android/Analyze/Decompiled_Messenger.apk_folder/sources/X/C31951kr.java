package X;

import android.os.Build;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1kr  reason: invalid class name and case insensitive filesystem */
public final class C31951kr {
    public static final boolean A00 = "robolectric".equals(Build.FINGERPRINT);

    private static C17770zR A00(AnonymousClass0p4 r4, C17770zR r5, boolean z) {
        C17770zR r3;
        AtomicBoolean atomicBoolean = r5.A0A;
        if (atomicBoolean == null || !atomicBoolean.getAndSet(true)) {
            r3 = r5;
        } else {
            r3 = r5.A16();
        }
        if (z) {
            r3.A06 = r5.A06;
        }
        AnonymousClass1KE r0 = r4.A07;
        r3.A0o(r0);
        r3.A1E(r4);
        AnonymousClass0p4 r1 = r3.A03;
        r1.A07 = r3.A0T(r1, r0);
        if (AnonymousClass07c.isDebugModeEnabled) {
            String A01 = C49912d4.A01(r1, r3);
            C636838g r12 = (C636838g) C49912d4.A02.get(A01);
            if (r12 != null) {
                r12.applyComponentOverrides(A01, r3);
                r12.applyStateOverrides(A01, r3.A18());
            }
        }
        return r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0051  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C17470yx A01(X.AnonymousClass0p4 r4, X.C17770zR r5, int r6, int r7, X.C17470yx r8, X.C22571Lz r9, X.C72593eb r10) {
        /*
            if (r10 == 0) goto L_0x0009
            if (r8 != 0) goto L_0x0057
            java.lang.String r0 = "start_create_layout"
        L_0x0006:
            r10.A00(r0)
        L_0x0009:
            r4.A03 = r6
            r4.A02 = r7
            java.lang.String r3 = "end_create_layout"
            r0 = 1
            if (r8 != 0) goto L_0x0034
            r2 = 0
            X.0yx r1 = A02(r4, r5, r0, r2)
            X.1kq r0 = r4.A06
            if (r0 != 0) goto L_0x002f
            r0 = 0
        L_0x001c:
            if (r0 == 0) goto L_0x0024
            if (r10 == 0) goto L_0x0023
            r10.A00(r3)
        L_0x0023:
            return r1
        L_0x0024:
            X.1kq r0 = r4.A06
            if (r0 == 0) goto L_0x003c
            X.15v r0 = r0.A01
            if (r0 == 0) goto L_0x003c
            r0.A0n = r2
            goto L_0x003c
        L_0x002f:
            boolean r0 = r0.A00()
            goto L_0x001c
        L_0x0034:
            X.0zR r0 = A00(r4, r5, r0)
            X.0yx r1 = r8.Bzu(r4, r0)
        L_0x003c:
            if (r10 == 0) goto L_0x0045
            if (r8 == 0) goto L_0x0042
            java.lang.String r3 = "end_reconcile_layout"
        L_0x0042:
            r10.A00(r3)
        L_0x0045:
            if (r10 == 0) goto L_0x004c
            java.lang.String r0 = "start_measure"
            r10.A00(r0)
        L_0x004c:
            A04(r4, r1, r6, r7, r9)
            if (r10 == 0) goto L_0x0023
            java.lang.String r0 = "end_measure"
            r10.A00(r0)
            return r1
        L_0x0057:
            java.lang.String r0 = "start_reconcile_layout"
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31951kr.A01(X.0p4, X.0zR, int, int, X.0yx, X.1Lz, X.3eb):X.0yx");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [X.0yx, X.5rD] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static X.C17470yx A02(X.AnonymousClass0p4 r10, X.C17770zR r11, boolean r12, boolean r13) {
        /*
            boolean r9 = X.C27041cY.A02()
            if (r9 == 0) goto L_0x0013
            java.lang.String r1 = "createLayout:"
            java.lang.String r0 = r11.A1A()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            X.C27041cY.A01(r0)
        L_0x0013:
            r1 = 0
            if (r1 == 0) goto L_0x001c
            if (r9 == 0) goto L_0x001b
            X.C27041cY.A00()
        L_0x001b:
            return r1
        L_0x001c:
            X.0zR r11 = A00(r10, r11, r13)     // Catch:{ all -> 0x02a3 }
            X.0p4 r5 = r11.A03     // Catch:{ all -> 0x02a3 }
            boolean r0 = X.C17770zR.A0B(r5, r11)     // Catch:{ all -> 0x02a3 }
            r3 = 1
            if (r0 == 0) goto L_0x002c
            r0 = 1
            if (r12 == 0) goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            if (r0 == 0) goto L_0x0042
            if (r1 == 0) goto L_0x0032
            goto L_0x0038
        L_0x0032:
            X.1Kw r4 = new X.1Kw     // Catch:{ all -> 0x02a3 }
            r4.<init>(r5)     // Catch:{ all -> 0x02a3 }
            goto L_0x003c
        L_0x0038:
            X.0yx r4 = r1.create(r5)     // Catch:{ all -> 0x02a3 }
        L_0x003c:
            X.1KE r0 = r5.A07     // Catch:{ all -> 0x02a3 }
            r4.BJz(r0)     // Catch:{ all -> 0x02a3 }
            goto L_0x00aa
        L_0x0042:
            boolean r0 = r11.A1K()     // Catch:{ all -> 0x02a3 }
            if (r0 == 0) goto L_0x004f
            X.0yz r4 = r11.A0Q(r5)     // Catch:{ all -> 0x02a3 }
            X.0yx r4 = (X.C17470yx) r4     // Catch:{ all -> 0x02a3 }
            goto L_0x00aa
        L_0x004f:
            boolean r0 = X.C17770zR.A07(r11)     // Catch:{ all -> 0x02a3 }
            if (r0 == 0) goto L_0x0069
            if (r1 == 0) goto L_0x0058
            goto L_0x005e
        L_0x0058:
            X.1Kw r1 = new X.1Kw     // Catch:{ all -> 0x02a3 }
            r1.<init>(r5)     // Catch:{ all -> 0x02a3 }
            goto L_0x0062
        L_0x005e:
            X.0yx r1 = r1.create(r5)     // Catch:{ all -> 0x02a3 }
        L_0x0062:
            X.10X r0 = X.AnonymousClass10X.COLUMN     // Catch:{ all -> 0x02a3 }
            X.0yx r4 = r1.Aa9(r0)     // Catch:{ all -> 0x02a3 }
            goto L_0x00aa
        L_0x0069:
            if (r11 == 0) goto L_0x0074
            java.lang.Integer r2 = r11.A0U()     // Catch:{ all -> 0x02a3 }
            java.lang.Integer r1 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x02a3 }
            r0 = 1
            if (r2 == r1) goto L_0x0075
        L_0x0074:
            r0 = 0
        L_0x0075:
            if (r0 == 0) goto L_0x0293
            boolean r0 = X.C17770zR.A08(r11)     // Catch:{ Exception -> 0x008b }
            if (r0 == 0) goto L_0x0086
            int r1 = r5.A03     // Catch:{ Exception -> 0x008b }
            int r0 = r5.A02     // Catch:{ Exception -> 0x008b }
            X.0zR r2 = r11.A0P(r5, r1, r0)     // Catch:{ Exception -> 0x008b }
            goto L_0x0090
        L_0x0086:
            X.0zR r2 = r11.A0O(r5)     // Catch:{ Exception -> 0x008b }
            goto L_0x0090
        L_0x008b:
            r0 = move-exception
            X.C17780zS.A0H(r5, r0)     // Catch:{ all -> 0x02a3 }
            r2 = 0
        L_0x0090:
            if (r2 == 0) goto L_0x0097
            int r0 = r2.A00     // Catch:{ all -> 0x02a3 }
            if (r0 <= 0) goto L_0x0097
            goto L_0x0098
        L_0x0097:
            r2 = 0
        L_0x0098:
            if (r2 != r11) goto L_0x00a1
            X.0yz r4 = r2.A0Q(r5)     // Catch:{ all -> 0x02a3 }
            X.0yx r4 = (X.C17470yx) r4     // Catch:{ all -> 0x02a3 }
            goto L_0x00aa
        L_0x00a1:
            if (r2 == 0) goto L_0x00a9
            r1 = 0
            X.0yx r4 = A02(r5, r2, r1, r1)     // Catch:{ all -> 0x02a3 }
            goto L_0x00aa
        L_0x00a9:
            r4 = 0
        L_0x00aa:
            if (r4 == 0) goto L_0x028b
            X.0yx r0 = X.AnonymousClass0p4.A0F     // Catch:{ all -> 0x02a3 }
            if (r4 == r0) goto L_0x028b
            if (r9 == 0) goto L_0x00b5
            X.C27041cY.A00()
        L_0x00b5:
            if (r9 == 0) goto L_0x00c4
            java.lang.String r1 = "afterCreateLayout:"
            java.lang.String r0 = r11.A1A()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            X.C27041cY.A01(r0)
        L_0x00c4:
            X.0zR r0 = r4.B5K()
            if (r0 != 0) goto L_0x00e5
            boolean r0 = r11.A0s()
            if (r0 == 0) goto L_0x021e
            boolean r0 = X.C17770zR.A07(r11)
            if (r0 == 0) goto L_0x021e
        L_0x00d6:
            if (r3 != 0) goto L_0x00e0
            boolean r0 = X.C17770zR.A0B(r5, r11)
            if (r0 == 0) goto L_0x00e5
            if (r12 != 0) goto L_0x00e5
        L_0x00e0:
            X.119 r0 = X.C17780zS.A01
            r4.C9P(r0)
        L_0x00e5:
            X.11G r3 = r11.A02
            if (r3 == 0) goto L_0x024a
            boolean r0 = X.C17770zR.A08(r11)
            if (r0 == 0) goto L_0x00f1
            if (r12 != 0) goto L_0x024a
        L_0x00f1:
            X.0yx r0 = X.AnonymousClass0p4.A0F
            if (r4 == r0) goto L_0x024a
            int r8 = r3.A01
            int r7 = r3.A02
            if (r8 != 0) goto L_0x00fd
            if (r7 == 0) goto L_0x0115
        L_0x00fd:
            r5.A00 = r8
            r5.A01 = r7
            android.content.Context r2 = r5.A09
            r1 = 0
            int[] r0 = X.C636538d.A00
            android.content.res.TypedArray r0 = r2.obtainStyledAttributes(r1, r0, r8, r7)
            r4.AOc(r0)
            r0.recycle()
            r1 = 0
            r5.A00 = r1
            r5.A01 = r1
        L_0x0115:
            X.1jd r1 = r3.A06
            if (r1 == 0) goto L_0x0120
            X.1jd r0 = r4.AwQ()
            r1.A00(r0)
        L_0x0120:
            byte r0 = r3.A00
            r0 = r0 & 1
            long r0 = (long) r0
            r7 = 0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x0130
            android.graphics.drawable.Drawable r0 = r3.A03
            r4.API(r0)
        L_0x0130:
            byte r0 = r3.A00
            r0 = r0 & 2
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x013e
            java.lang.String r0 = r3.A07
            r4.CIv(r0)
        L_0x013e:
            boolean r0 = r3.A08
            if (r0 == 0) goto L_0x0145
            r4.CNW()
        L_0x0145:
            X.1AL r0 = r3.A04
            if (r0 == 0) goto L_0x014c
            r0.AUX(r4)
        L_0x014c:
            X.0uN r6 = r3.A05
            if (r6 == 0) goto L_0x024a
            int r3 = r6.A03
            r0 = r3 & 1
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x015e
            int r0 = r6.A02
            r4.BCN(r0)
        L_0x015e:
            r0 = r3 & 2
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x016a
            boolean r0 = r6.A0H
            r4.AY1(r0)
        L_0x016a:
            r0 = r3 & 4
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x0176
            android.graphics.drawable.Drawable r0 = r6.A05
            r4.Aac(r0)
        L_0x0176:
            r0 = r3 & 1024(0x400, float:1.435E-42)
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x0180
            r4.CNW()
        L_0x0180:
            r0 = r3 & 8
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x018c
            X.10N r0 = r6.A0D
            r4.CLi(r0)
        L_0x018c:
            r0 = r3 & 16
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x0198
            X.10N r0 = r6.A08
            r4.AaM(r0)
        L_0x0198:
            r0 = r3 & 32
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x01a4
            X.10N r0 = r6.A09
            r4.Aaj(r0)
        L_0x01a4:
            r0 = r3 & 64
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x01b0
            X.10N r0 = r6.A0A
            r4.BDS(r0)
        L_0x01b0:
            r0 = r3 & 128(0x80, float:1.794E-43)
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x01bc
            X.10N r0 = r6.A0B
            r4.CJe(r0)
        L_0x01bc:
            r0 = 65536(0x10000, float:9.18355E-41)
            r0 = r0 & r3
            if (r0 == 0) goto L_0x01c6
            X.10N r0 = r6.A0C
            r4.CLh(r0)
        L_0x01c6:
            r0 = r3 & 512(0x200, float:7.175E-43)
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x01d4
            java.lang.String r1 = r6.A0F
            java.lang.String r0 = r6.A0G
            r4.CJN(r1, r0)
        L_0x01d4:
            int r3 = r6.A03
            r0 = 131072(0x20000, float:1.83671E-40)
            r0 = r0 & r3
            long r0 = (long) r0
            int r2 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r2 == 0) goto L_0x01e3
            java.lang.Integer r0 = r6.A0E
            r4.CJO(r0)
        L_0x01e3:
            r0 = r3 & 2048(0x800, float:2.87E-42)
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x01ef
            float r0 = r6.A00
            r4.CLj(r0)
        L_0x01ef:
            r0 = r3 & 4096(0x1000, float:5.74E-42)
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x01fb
            float r0 = r6.A01
            r4.CLk(r0)
        L_0x01fb:
            r0 = r3 & 256(0x100, float:3.59E-43)
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x0221
            r3 = 0
        L_0x0203:
            int r0 = X.AnonymousClass1IF.A03
            if (r3 >= r0) goto L_0x0221
            X.1IF r0 = r6.A07
            float r2 = r0.A00(r3)
            boolean r0 = X.AnonymousClass1K2.A00(r2)
            if (r0 != 0) goto L_0x021b
            X.10G r1 = X.AnonymousClass10G.A00(r3)
            int r0 = (int) r2
            r4.CJD(r1, r0)
        L_0x021b:
            int r3 = r3 + 1
            goto L_0x0203
        L_0x021e:
            r3 = 0
            goto L_0x00d6
        L_0x0221:
            int r0 = r6.A03
            r0 = r0 & 8192(0x2000, float:1.14794E-41)
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x022f
            X.38W r0 = r6.A06
            r4.AQW(r0)
        L_0x022f:
            int r3 = r6.A03
            r0 = r3 & 16384(0x4000, float:2.2959E-41)
            long r1 = (long) r0
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x023d
            android.animation.StateListAnimator r0 = r6.A04
            r4.CHg(r0)
        L_0x023d:
            r0 = 32768(0x8000, float:4.5918E-41)
            r3 = r3 & r0
            long r1 = (long) r3
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x024a
            r0 = 0
            r4.CHh(r0)
        L_0x024a:
            r4.AOS(r11)
            if (r5 == 0) goto L_0x0285
            com.facebook.litho.ComponentTree r0 = r5.A05
            if (r0 == 0) goto L_0x0285
            boolean r0 = r0.A0c
        L_0x0255:
            if (r0 == 0) goto L_0x0267
            X.0zQ r1 = r11.A0S(r5)
            if (r1 == 0) goto L_0x0262
            java.lang.String r0 = r11.A06
            X.C07070ca.A01(r1, r0)
        L_0x0262:
            if (r1 == 0) goto L_0x0267
            r4.ANp(r1)
        L_0x0267:
            boolean r0 = X.C17770zR.A07(r11)
            if (r0 == 0) goto L_0x0270
            r11.A0c(r5)
        L_0x0270:
            java.util.List r0 = r11.A08
            if (r0 == 0) goto L_0x027f
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x027f
            java.util.List r0 = r11.A08
            r4.ANs(r0)
        L_0x027f:
            if (r9 == 0) goto L_0x0284
            X.C27041cY.A00()
        L_0x0284:
            return r4
        L_0x0285:
            r0 = 0
            boolean r0 = X.C07070ca.A03(r0)
            goto L_0x0255
        L_0x028b:
            X.0yx r0 = X.AnonymousClass0p4.A0F     // Catch:{ all -> 0x02a3 }
            if (r9 == 0) goto L_0x0292
            X.C27041cY.A00()
        L_0x0292:
            return r0
        L_0x0293:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x02a3 }
            java.lang.String r1 = "component:"
            java.lang.String r0 = r11.A1A()     // Catch:{ all -> 0x02a3 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x02a3 }
            r2.<init>(r0)     // Catch:{ all -> 0x02a3 }
            throw r2     // Catch:{ all -> 0x02a3 }
        L_0x02a3:
            r1 = move-exception
            X.38e r0 = new X.38e     // Catch:{ all -> 0x02aa }
            r0.<init>(r11, r1)     // Catch:{ all -> 0x02aa }
            throw r0     // Catch:{ all -> 0x02aa }
        L_0x02aa:
            r0 = move-exception
            if (r9 == 0) goto L_0x02b0
            X.C27041cY.A00()
        L_0x02b0:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31951kr.A02(X.0p4, X.0zR, boolean, boolean):X.0yx");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005f, code lost:
        if (r6.B16() == r9.B16()) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007f, code lost:
        if (r1 == false) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0084, code lost:
        if (r0 != false) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0029, code lost:
        if (r1 == false) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
        if (r0 == false) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C17470yx A03(X.AnonymousClass0p4 r8, X.C17470yx r9, int r10, int r11) {
        /*
            X.0zR r3 = r9.B5K()
            X.0yx r4 = r9.AvV()
            if (r3 == 0) goto L_0x00e8
            if (r4 == 0) goto L_0x002e
            int r2 = r4.AsD()
            int r5 = r4.Ary()
            float r0 = r4.As2()
            float r1 = r4.As1()
            int r0 = (int) r0
            boolean r2 = X.C15050ue.A00(r2, r10, r0)
            int r0 = (int) r1
            boolean r1 = X.C15050ue.A00(r5, r11, r0)
            if (r2 == 0) goto L_0x002b
            r0 = 1
            if (r1 != 0) goto L_0x002c
        L_0x002b:
            r0 = 0
        L_0x002c:
            if (r0 != 0) goto L_0x008d
        L_0x002e:
            X.1kq r0 = r8.A06
            if (r0 != 0) goto L_0x00d0
            r2 = 0
        L_0x0033:
            if (r2 == 0) goto L_0x00d4
            java.util.Map r1 = r2.A0h
            int r0 = r3.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r6 = r1.get(r0)
            X.0yx r6 = (X.C17470yx) r6
            if (r6 == 0) goto L_0x0086
            java.util.Map r1 = r2.A0h
            int r0 = r3.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.remove(r0)
            boolean r0 = r6.BFZ()
            if (r0 != 0) goto L_0x0061
            X.0zG r1 = r6.B16()
            X.0zG r0 = r9.B16()
            r7 = 0
            if (r1 != r0) goto L_0x0062
        L_0x0061:
            r7 = 1
        L_0x0062:
            int r2 = r6.AsD()
            int r5 = r6.Ary()
            float r0 = r6.As2()
            float r1 = r6.As1()
            int r0 = (int) r0
            boolean r2 = X.C15050ue.A00(r2, r10, r0)
            int r0 = (int) r1
            boolean r1 = X.C15050ue.A00(r5, r11, r0)
            if (r2 == 0) goto L_0x0081
            r0 = 1
            if (r1 != 0) goto L_0x0082
        L_0x0081:
            r0 = 0
        L_0x0082:
            if (r7 == 0) goto L_0x0086
            if (r0 != 0) goto L_0x0087
        L_0x0086:
            r6 = 0
        L_0x0087:
            if (r6 == 0) goto L_0x0091
            r4 = r6
        L_0x008a:
            r9.C9n(r4)
        L_0x008d:
            r4.AOw()
            return r4
        L_0x0091:
            if (r4 == 0) goto L_0x0095
            boolean r0 = X.AnonymousClass07c.enableShouldCreateLayoutWithNewSizeSpec
        L_0x0095:
            boolean r0 = X.C31951kr.A00
            if (r0 != 0) goto L_0x00ca
            r0 = r8
        L_0x009a:
            X.1KE r1 = r9.Axp()
            r0.A07 = r1
            r0.A03 = r10
            r0.A02 = r11
            r1 = 1
            X.0yx r4 = A02(r0, r3, r1, r1)
            r9.AUX(r4)
            X.1Lz r0 = r9.AkV()
            A04(r8, r4, r10, r11, r0)
            r4.C8r(r10)
            r4.C8l(r11)
            int r0 = r4.getHeight()
            float r0 = (float) r0
            r4.C8m(r0)
            int r0 = r4.getWidth()
            float r0 = (float) r0
            r4.C8n(r0)
            goto L_0x008a
        L_0x00ca:
            X.0p4 r0 = new X.0p4
            r0.<init>(r8)
            goto L_0x009a
        L_0x00d0:
            X.15v r2 = r0.A01
            goto L_0x0033
        L_0x00d4:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r3 = r3.A1A()
            java.lang.String r2 = ": Trying to access the cached InternalNode for a component outside of a"
            java.lang.String r1 = " LayoutState calculation. If that is what you must do, see"
            java.lang.String r0 = " Component#measureMightNotCacheInternalNode."
            java.lang.String r0 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)
            r4.<init>(r0)
            throw r4
        L_0x00e8:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "A component is required to resolve a nested tree."
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31951kr.A03(X.0p4, X.0yx, int, int):X.0yx");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003b, code lost:
        if (r3.getResources().getConfiguration().getLayoutDirection() == 1) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(X.AnonymousClass0p4 r5, X.C17470yx r6, int r7, int r8, X.C22571Lz r9) {
        /*
            boolean r4 = X.C27041cY.A02()
            if (r4 == 0) goto L_0x0013
            java.lang.String r1 = "measureTree:"
            java.lang.String r0 = r6.getSimpleName()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            X.C27041cY.A01(r0)
        L_0x0013:
            X.0zG r1 = r6.B4P()
            X.0zG r0 = X.C17660zG.INHERIT
            if (r1 != r0) goto L_0x0044
            android.content.Context r3 = r5.A09
            android.content.pm.ApplicationInfo r2 = r3.getApplicationInfo()
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 17
            if (r1 < r0) goto L_0x008f
            int r1 = r2.flags
            r0 = 4194304(0x400000, float:5.877472E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x008f
            android.content.res.Resources r0 = r3.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r1 = r0.getLayoutDirection()
            r0 = 1
            if (r1 != r0) goto L_0x008f
        L_0x003d:
            if (r0 == 0) goto L_0x0044
            X.0zG r0 = X.C17660zG.RTL
            r6.BHv(r0)
        L_0x0044:
            float r0 = r6.B4V()
            boolean r0 = X.AnonymousClass1K2.A00(r0)
            if (r0 == 0) goto L_0x0051
            r6.CC1(r7)
        L_0x0051:
            float r0 = r6.B4Q()
            boolean r0 = X.AnonymousClass1K2.A00(r0)
            if (r0 == 0) goto L_0x005e
            r6.CC0(r8)
        L_0x005e:
            if (r9 == 0) goto L_0x006b
            java.lang.String r0 = "applyDiffNode"
            X.C27041cY.A01(r0)
            A06(r6, r9)
            X.C27041cY.A00()
        L_0x006b:
            int r0 = android.view.View.MeasureSpec.getMode(r7)
            r2 = 2143289344(0x7fc00000, float:NaN)
            if (r0 != 0) goto L_0x0089
            r1 = 2143289344(0x7fc00000, float:NaN)
        L_0x0075:
            int r0 = android.view.View.MeasureSpec.getMode(r8)
            if (r0 == 0) goto L_0x0080
            int r0 = android.view.View.MeasureSpec.getSize(r8)
            float r2 = (float) r0
        L_0x0080:
            r6.AR7(r1, r2)
            if (r4 == 0) goto L_0x0088
            X.C27041cY.A00()
        L_0x0088:
            return
        L_0x0089:
            int r0 = android.view.View.MeasureSpec.getSize(r7)
            float r1 = (float) r0
            goto L_0x0075
        L_0x008f:
            r0 = 0
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31951kr.A04(X.0p4, X.0yx, int, int, X.1Lz):void");
    }

    public static void A05(C17470yx r5) {
        List B7d = r5.B7d();
        if (B7d != null) {
            int size = B7d.size();
            for (int i = 0; i < size; i++) {
                r5.ASH((C17770zR) B7d.get(i));
            }
            r5.B7d().clear();
        }
        int Ah4 = r5.Ah4();
        for (int i2 = 0; i2 < Ah4; i2++) {
            A05(r5.Ah3(i2));
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003c A[Catch:{ all -> 0x0089 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0077 A[Catch:{ all -> 0x0089 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[Catch:{ all -> 0x0089 }, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[Catch:{ all -> 0x0089 }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void A06(X.C17470yx r5, X.C22571Lz r6) {
        /*
            X.0yx r0 = r5.AxD()     // Catch:{ all -> 0x0089 }
            r4 = 0
            r1 = 0
            if (r0 != 0) goto L_0x0009
            r1 = 1
        L_0x0009:
            X.0zR r0 = r5.B5K()     // Catch:{ all -> 0x0089 }
            boolean r0 = X.C17770zR.A08(r0)     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x0019
            if (r1 != 0) goto L_0x0019
            r5.C7h(r6)     // Catch:{ all -> 0x0089 }
            return
        L_0x0019:
            if (r6 == 0) goto L_0x0037
            X.0zR r1 = r5.B5K()     // Catch:{ all -> 0x0089 }
            X.0zR r0 = r6.Ahm()     // Catch:{ all -> 0x0089 }
            if (r1 != r0) goto L_0x0026
            goto L_0x0039
        L_0x0026:
            if (r1 == 0) goto L_0x0037
            if (r0 == 0) goto L_0x0037
            java.lang.Class r1 = r1.getClass()     // Catch:{ all -> 0x0089 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x0089 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0089 }
            goto L_0x003a
        L_0x0037:
            r0 = 0
            goto L_0x003a
        L_0x0039:
            r0 = 1
        L_0x003a:
            if (r0 == 0) goto L_0x0088
            r5.C7h(r6)     // Catch:{ all -> 0x0089 }
            int r3 = r5.Ah4()     // Catch:{ all -> 0x0089 }
            int r2 = r6.Ah4()     // Catch:{ all -> 0x0089 }
            if (r3 == 0) goto L_0x005d
            if (r2 == 0) goto L_0x005d
        L_0x004b:
            if (r4 >= r3) goto L_0x0088
            if (r4 >= r2) goto L_0x0088
            X.0yx r1 = r5.Ah3(r4)     // Catch:{ all -> 0x0089 }
            X.1Lz r0 = r6.Ah2(r4)     // Catch:{ all -> 0x0089 }
            A06(r1, r0)     // Catch:{ all -> 0x0089 }
            int r4 = r4 + 1
            goto L_0x004b
        L_0x005d:
            if (r6 == 0) goto L_0x0074
            X.0zR r3 = r5.B5K()     // Catch:{ all -> 0x0089 }
            if (r3 == 0) goto L_0x0074
            X.0zR r2 = r6.Ahm()     // Catch:{ all -> 0x0089 }
            boolean r0 = r3.A10()     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x0074
            boolean r0 = r3.A12(r3, r2)     // Catch:{ all -> 0x0089 }
            goto L_0x0075
        L_0x0074:
            r0 = 1
        L_0x0075:
            if (r0 != 0) goto L_0x0088
            X.0zR r1 = r5.B5K()     // Catch:{ all -> 0x0089 }
            if (r1 == 0) goto L_0x0084
            X.0zR r0 = r6.Ahm()     // Catch:{ all -> 0x0089 }
            r1.A1C(r0)     // Catch:{ all -> 0x0089 }
        L_0x0084:
            r0 = 1
            r5.C6X(r0)     // Catch:{ all -> 0x0089 }
        L_0x0088:
            return
        L_0x0089:
            r2 = move-exception
            X.0zR r1 = r5.B5K()
            if (r1 == 0) goto L_0x0096
            X.38e r0 = new X.38e
            r0.<init>(r1, r2)
            throw r0
        L_0x0096:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31951kr.A06(X.0yx, X.1Lz):void");
    }
}
