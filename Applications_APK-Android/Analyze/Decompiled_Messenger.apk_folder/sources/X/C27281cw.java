package X;

/* renamed from: X.1cw  reason: invalid class name and case insensitive filesystem */
public final class C27281cw {
    public final C27291cx A00;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r4.equals("MsysBootstrap") == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C27281cw(java.lang.String r4, android.content.Context r5) {
        /*
            r3 = this;
            r3.<init>()
            int r1 = r4.hashCode()
            r0 = 1340373742(0x4fe47aee, float:7.6665231E9)
            if (r1 != r0) goto L_0x0015
            java.lang.String r0 = "MsysBootstrap"
            boolean r0 = r4.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x0016
        L_0x0015:
            r1 = -1
        L_0x0016:
            if (r1 != 0) goto L_0x0020
            X.1cx r0 = new X.1cx
            r0.<init>(r5)
            r3.A00 = r0
            return
        L_0x0020:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r1 = "Invalid registry name \""
            java.lang.String r0 = "\"!"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r4, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27281cw.<init>(java.lang.String, android.content.Context):void");
    }
}
