package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1gB  reason: invalid class name and case insensitive filesystem */
public final class C29291gB extends C29301gC {
    private static volatile C29291gB A00;

    public C29291gB() {
        super(20);
    }

    public static final C29291gB A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C29291gB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C29291gB();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
