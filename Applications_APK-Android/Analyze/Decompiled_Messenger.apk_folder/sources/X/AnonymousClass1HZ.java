package X;

import com.facebook.messaging.model.send.PendingSendQueueKey;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.1HZ  reason: invalid class name */
public final class AnonymousClass1HZ {
    public final Map A00 = AnonymousClass0TG.A04();
    private final AnonymousClass069 A01;

    public int A00() {
        int size;
        int i = 0;
        for (AnonymousClass1v2 r1 : this.A00.values()) {
            synchronized (r1) {
                size = r1.A06.size();
            }
            i += size;
        }
        return i;
    }

    public AnonymousClass1v2 A01(PendingSendQueueKey pendingSendQueueKey) {
        AnonymousClass1v2 r1 = (AnonymousClass1v2) this.A00.get(pendingSendQueueKey);
        if (r1 != null) {
            return r1;
        }
        AnonymousClass1v2 r12 = new AnonymousClass1v2(this.A01, pendingSendQueueKey);
        this.A00.put(pendingSendQueueKey, r12);
        return r12;
    }

    public AnonymousClass1v2 A02(PendingSendQueueKey pendingSendQueueKey) {
        return (AnonymousClass1v2) this.A00.get(pendingSendQueueKey);
    }

    public Collection A03() {
        return Collections.unmodifiableCollection(this.A00.values());
    }

    public AnonymousClass1HZ(AnonymousClass069 r2) {
        this.A01 = r2;
    }
}
