package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0bX  reason: invalid class name and case insensitive filesystem */
public final class C06460bX extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C06480bZ A01;

    public static final C06480bZ A01(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        C06470bY A003 = C06470bY.A00(applicationInjector);
                        A01 = new C06480bZ(A003.A00, AnonymousClass0UX.A0Z(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C06480bZ A00(AnonymousClass1XY r2) {
        C06470bY A002 = C06470bY.A00(r2);
        return new C06480bZ(A002.A00, AnonymousClass0UX.A0X(r2));
    }
}
