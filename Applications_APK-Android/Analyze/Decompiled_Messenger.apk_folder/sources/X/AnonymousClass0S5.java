package X;

import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.0S5  reason: invalid class name */
public final class AnonymousClass0S5 {
    private Socket A00;
    public final InetAddress A01;
    private final int A02;
    private final int A03;
    private final int A04;
    private final AnonymousClass0SS A05;
    private final InetAddress A06;
    private final ScheduledExecutorService A07;

    /* JADX WARNING: Can't wrap try/catch for region: R(5:2|3|(2:5|(2:7|8))|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0023 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.AnonymousClass0S5 r2, java.net.Socket r3, java.net.InetAddress r4, java.net.Socket r5) {
        /*
            r0 = 1
            r3.setTcpNoDelay(r0)
            r0 = 0
            r3.setSoTimeout(r0)
            r3.setKeepAlive(r0)
            java.net.InetSocketAddress r1 = new java.net.InetSocketAddress
            int r0 = r2.A03
            r1.<init>(r4, r0)
            int r0 = r2.A04
            r3.connect(r1, r0)
            monitor-enter(r2)
            java.net.Socket r0 = r2.A00     // Catch:{ all -> 0x0025 }
            if (r0 != 0) goto L_0x0023
            r2.A00 = r3     // Catch:{ all -> 0x0025 }
            if (r5 == 0) goto L_0x0023
            r5.close()     // Catch:{ IOException -> 0x0023 }
        L_0x0023:
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            return
        L_0x0025:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0S5.A00(X.0S5, java.net.Socket, java.net.InetAddress, java.net.Socket):void");
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.net.Socket A01() {
        /*
            r8 = this;
            java.net.Socket r7 = new java.net.Socket
            r7.<init>()
            java.net.Socket r6 = new java.net.Socket
            r6.<init>()
            java.util.concurrent.ScheduledExecutorService r4 = r8.A07
            X.0S6 r3 = new X.0S6
            r3.<init>(r8, r6, r7)
            int r0 = r8.A02
            long r1 = (long) r0
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MILLISECONDS
            java.util.concurrent.ScheduledFuture r2 = r4.schedule(r3, r1, r0)
            r1 = 0
            java.net.InetAddress r0 = r8.A06     // Catch:{ IOException -> 0x0024 }
            A00(r8, r7, r0, r6)     // Catch:{ IOException -> 0x0024 }
            r2.cancel(r1)     // Catch:{ IOException -> 0x0024 }
            goto L_0x002d
        L_0x0024:
            r4 = move-exception
            if (r7 == 0) goto L_0x002a
            r7.close()     // Catch:{ IOException -> 0x002a }
        L_0x002a:
            r2.get()     // Catch:{ Exception -> 0x004c }
        L_0x002d:
            monitor-enter(r8)
            java.net.Socket r0 = r8.A00     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x0041
            boolean r0 = r0.isConnected()     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x0041
            java.net.Socket r0 = r8.A00     // Catch:{ all -> 0x0049 }
            r0.getInetAddress()     // Catch:{ all -> 0x0049 }
            java.net.Socket r0 = r8.A00     // Catch:{ all -> 0x0049 }
            monitor-exit(r8)     // Catch:{ all -> 0x0049 }
            return r0
        L_0x0041:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0049 }
            java.lang.String r0 = "socket connect call succeeded but socket is not connected."
            r1.<init>(r0)     // Catch:{ all -> 0x0049 }
            throw r1     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0049 }
            throw r0
        L_0x004c:
            r3 = move-exception
            if (r6 == 0) goto L_0x0052
            r6.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0052:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 9
            if (r1 < r0) goto L_0x0068
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r1 = "Failed to connect to both sockets: "
            java.lang.String r0 = r4.getMessage()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0, r3)
            throw r2
        L_0x0068:
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r1 = "Failed to connect to both sockets: "
            java.lang.String r0 = r4.getMessage()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0S5.A01():java.net.Socket");
    }

    public AnonymousClass0S5(InetAddress inetAddress, InetAddress inetAddress2, int i, int i2, AnonymousClass0SS r5, ScheduledExecutorService scheduledExecutorService, int i3) {
        this.A05 = r5;
        this.A06 = inetAddress;
        this.A01 = inetAddress2;
        this.A03 = i;
        this.A04 = i2;
        this.A07 = scheduledExecutorService;
        this.A02 = i3;
    }
}
