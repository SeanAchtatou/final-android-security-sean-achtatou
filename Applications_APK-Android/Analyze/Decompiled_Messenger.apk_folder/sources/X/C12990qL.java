package X;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: X.0qL  reason: invalid class name and case insensitive filesystem */
public final class C12990qL extends AnonymousClass0qM {
    public int A00 = 0;
    public C13000qN A01 = new C13000qN();
    public AnonymousClass1XL A02;
    public ArrayList A03 = new ArrayList();
    public boolean A04 = false;
    public boolean A05 = false;
    public final WeakReference A06;

    public static AnonymousClass1XL A02(C12990qL r4, C27671dZ r5) {
        C27701dc r0;
        AnonymousClass1XL r3;
        C13000qN r1 = r4.A01;
        if (r1.A00.containsKey(r5)) {
            r0 = ((C27701dc) r1.A00.get(r5)).A01;
        } else {
            r0 = null;
        }
        AnonymousClass1XL r2 = null;
        if (r0 != null) {
            r3 = ((C27681da) r0.getValue()).A01;
        } else {
            r3 = null;
        }
        if (!r4.A03.isEmpty()) {
            ArrayList arrayList = r4.A03;
            r2 = (AnonymousClass1XL) arrayList.get(arrayList.size() - 1);
        }
        AnonymousClass1XL r12 = r4.A02;
        if (r3 != null && r3.compareTo((Enum) r12) < 0) {
            r12 = r3;
        }
        if (r2 == null || r2.compareTo((Enum) r12) >= 0) {
            return r12;
        }
        return r2;
    }

    public static void A03(C12990qL r8) {
        C14820uB r2;
        AnonymousClass1XL r1;
        C11410n6 r3 = (C11410n6) r8.A06.get();
        if (r3 == null) {
            throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is alreadygarbage collected. It is too late to change lifecycle state.");
        }
        while (true) {
            C13000qN r12 = r8.A01;
            boolean z = true;
            if (!(r12.A00 == 0 || (((C27681da) r12.A02.getValue()).A01 == (r1 = ((C27681da) r8.A01.A01.getValue()).A01) && r8.A02 == r1))) {
                z = false;
            }
            r8.A05 = false;
            if (!z) {
                if (r8.A02.compareTo((Enum) ((C27681da) r8.A01.A02.getValue()).A01) < 0) {
                    C13000qN r22 = r8.A01;
                    C28571f1 r7 = new C28571f1(r22.A01, r22.A02);
                    r22.A03.put(r7, false);
                    while (r7.hasNext() && !r8.A05) {
                        Map.Entry entry = (Map.Entry) r7.next();
                        C27681da r5 = (C27681da) entry.getValue();
                        while (r5.A01.compareTo((Enum) r8.A02) > 0 && !r8.A05) {
                            if (r8.A01.A00.containsKey(entry.getKey())) {
                                AnonymousClass1XL r4 = r5.A01;
                                switch (r4.ordinal()) {
                                    case 0:
                                    case 1:
                                        throw new IllegalArgumentException();
                                    case 2:
                                        r2 = C14820uB.ON_DESTROY;
                                        break;
                                    case 3:
                                        r2 = C14820uB.ON_STOP;
                                        break;
                                    case 4:
                                        r2 = C14820uB.ON_PAUSE;
                                        break;
                                    default:
                                        throw new IllegalArgumentException("Unexpected state value " + r4);
                                }
                                r8.A03.add(A01(r2));
                                r5.A00(r3, r2);
                                ArrayList arrayList = r8.A03;
                                arrayList.remove(arrayList.size() - 1);
                            } else {
                                continue;
                            }
                        }
                    }
                }
                C27701dc r23 = r8.A01.A01;
                if (!r8.A05 && r23 != null && r8.A02.compareTo((Enum) ((C27681da) r23.getValue()).A01) > 0) {
                    C13000qN r0 = r8.A01;
                    C17390yp r52 = new C17390yp(r0);
                    r0.A03.put(r52, false);
                    while (r52.hasNext() && !r8.A05) {
                        Map.Entry entry2 = (Map.Entry) r52.next();
                        C27681da r24 = (C27681da) entry2.getValue();
                        while (r24.A01.compareTo((Enum) r8.A02) < 0 && !r8.A05) {
                            if (r8.A01.A00.containsKey(entry2.getKey())) {
                                r8.A03.add(r24.A01);
                                r24.A00(r3, A00(r24.A01));
                                ArrayList arrayList2 = r8.A03;
                                arrayList2.remove(arrayList2.size() - 1);
                            }
                        }
                    }
                }
            } else {
                return;
            }
        }
    }

    public static void A04(C12990qL r2, AnonymousClass1XL r3) {
        if (r2.A02 != r3) {
            r2.A02 = r3;
            if (r2.A04 || r2.A00 != 0) {
                r2.A05 = true;
                return;
            }
            r2.A04 = true;
            A03(r2);
            r2.A04 = false;
        }
    }

    public C12990qL(C11410n6 r2) {
        this.A06 = new WeakReference(r2);
        this.A02 = AnonymousClass1XL.INITIALIZED;
    }

    public static C14820uB A00(AnonymousClass1XL r3) {
        switch (r3.ordinal()) {
            case 0:
            case 1:
                return C14820uB.ON_CREATE;
            case 2:
                return C14820uB.ON_START;
            case 3:
                return C14820uB.ON_RESUME;
            case 4:
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException("Unexpected state value " + r3);
        }
    }

    public static AnonymousClass1XL A01(C14820uB r3) {
        switch (r3.ordinal()) {
            case 0:
            case 4:
                return AnonymousClass1XL.CREATED;
            case 1:
            case 3:
                return AnonymousClass1XL.STARTED;
            case 2:
                return AnonymousClass1XL.RESUMED;
            case 5:
                return AnonymousClass1XL.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + r3);
        }
    }

    public void A08(C14820uB r2) {
        A04(this, A01(r2));
    }
}
