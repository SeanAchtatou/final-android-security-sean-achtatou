package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0lw  reason: invalid class name */
public final class AnonymousClass0lw extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static C05540Zi A01;
    private static C05540Zi A02;
    private static C05540Zi A03;
    private static C05540Zi A04;
    private static C05540Zi A05;
    private static C05540Zi A06;
    private static C05540Zi A07;
    private static C05540Zi A08;
    private static final Object A09 = new Object();
    private static final Object A0A = new Object();
    private static final Object A0B = new Object();
    private static final Object A0C = new Object();
    private static final Object A0D = new Object();
    private static final Object A0E = new Object();
    private static final Object A0F = new Object();
    private static final Object A0G = new Object();
    private static final Object A0H = new Object();

    public static final C11230mQ A00(AnonymousClass1XY r5) {
        C11230mQ r0;
        synchronized (A09) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C11230mQ(new C27511dJ(r02), AnonymousClass0m2.A00(r02));
                }
                C05540Zi r1 = A00;
                r0 = (C11230mQ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C11230mQ A01(AnonymousClass1XY r5) {
        C11230mQ r0;
        synchronized (A0A) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A01.A01();
                    A01.A00 = new C11230mQ(new C27511dJ(r02), AnonymousClass0m2.A01(r02));
                }
                C05540Zi r1 = A01;
                r0 = (C11230mQ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C11230mQ A02(AnonymousClass1XY r5) {
        C11230mQ r0;
        synchronized (A0B) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A02.A01();
                    A02.A00 = new C11230mQ(new C27511dJ(r02), AnonymousClass0m2.A02(r02));
                }
                C05540Zi r1 = A02;
                r0 = (C11230mQ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C14800u9 A04(AnonymousClass1XY r5) {
        C14800u9 r0;
        synchronized (A0C) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A03.A01();
                    A03.A00 = new C14800u9(new C30171hc(r02), AnonymousClass0m2.A00(r02));
                }
                C05540Zi r1 = A03;
                r0 = (C14800u9) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C14800u9 A05(AnonymousClass1XY r5) {
        C14800u9 r0;
        synchronized (A0D) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A04.A01();
                    A04.A00 = new C14800u9(new C30171hc(r02), AnonymousClass0m2.A01(r02));
                }
                C05540Zi r1 = A04;
                r0 = (C14800u9) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C14800u9 A06(AnonymousClass1XY r5) {
        C14800u9 r0;
        synchronized (A0E) {
            C05540Zi A002 = C05540Zi.A00(A05);
            A05 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A05.A01();
                    A05.A00 = new C14800u9(new C30171hc(r02), AnonymousClass0m2.A02(r02));
                }
                C05540Zi r1 = A05;
                r0 = (C14800u9) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A05.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C11130lx A07(AnonymousClass1XY r9) {
        C11130lx r0;
        synchronized (A0F) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r9)) {
                    AnonymousClass1XY r2 = (AnonymousClass1XY) A06.A01();
                    A06.A00 = new C11130lx(new AnonymousClass0m0(r2), "FacebookCacheServiceHandler", AnonymousClass0m2.A00(r2), A00(r2), AnonymousClass0UQ.A00(AnonymousClass1Y3.A4i, r2));
                }
                C05540Zi r1 = A06;
                r0 = (C11130lx) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C11130lx A08(AnonymousClass1XY r9) {
        C11130lx r0;
        synchronized (A0G) {
            C05540Zi A002 = C05540Zi.A00(A07);
            A07 = A002;
            try {
                if (A002.A03(r9)) {
                    AnonymousClass1XY r2 = (AnonymousClass1XY) A07.A01();
                    A07.A00 = new C11130lx(new AnonymousClass0m0(r2), "SmsCacheServiceHandler", AnonymousClass0m2.A01(r2), A01(r2), AnonymousClass0UQ.A00(AnonymousClass1Y3.BHP, r2));
                }
                C05540Zi r1 = A07;
                r0 = (C11130lx) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A07.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C11130lx A09(AnonymousClass1XY r9) {
        C11130lx r0;
        synchronized (A0H) {
            C05540Zi A002 = C05540Zi.A00(A08);
            A08 = A002;
            try {
                if (A002.A03(r9)) {
                    AnonymousClass1XY r2 = (AnonymousClass1XY) A08.A01();
                    A08.A00 = new C11130lx(new AnonymousClass0m0(r2), "TincanCacheServiceHandler", AnonymousClass0m2.A02(r2), A02(r2), AnonymousClass0UQ.A00(AnonymousClass1Y3.BC1, r2));
                }
                C05540Zi r1 = A08;
                r0 = (C11130lx) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A08.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C14800u9 A03(AnonymousClass1XY r0) {
        return A04(r0);
    }
}
