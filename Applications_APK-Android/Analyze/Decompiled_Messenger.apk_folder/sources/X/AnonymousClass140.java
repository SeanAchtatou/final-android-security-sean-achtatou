package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.140  reason: invalid class name */
public final class AnonymousClass140 implements AnonymousClass0ZM {
    public final /* synthetic */ AnonymousClass13w A00;

    public AnonymousClass140(AnonymousClass13w r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r3) {
        AnonymousClass13w r0 = this.A00;
        r0.A04();
        r0.A05();
    }
}
