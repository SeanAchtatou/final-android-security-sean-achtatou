package X;

import android.net.Uri;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

/* renamed from: X.1QT  reason: invalid class name */
public final class AnonymousClass1QT {
    public final FbSharedPreferences A00;
    public final AnonymousClass1Y7 A01 = ((AnonymousClass1Y7) this.A0A.A09("KEY_CONTENT_LENGTH"));
    public final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) this.A0A.A09("KEY_FETCHER_ANALYTICS_TAG"));
    public final AnonymousClass1Y7 A03 = ((AnonymousClass1Y7) this.A0A.A09("KEY_FETCHER_CALLING_CLASS"));
    public final AnonymousClass1Y7 A04 = ((AnonymousClass1Y7) this.A0A.A09("KEY_FETCHER_FEATURE_TAG"));
    public final AnonymousClass1Y7 A05 = ((AnonymousClass1Y7) this.A0A.A09("KEY_FETCH_TIME_MS"));
    public final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) this.A0A.A09("KEY_FIRST_UI_TIME_MS"));
    public final AnonymousClass1Y7 A07 = ((AnonymousClass1Y7) this.A0A.A09("KEY_IS_CANCELLATION_REQUESTED"));
    public final AnonymousClass1Y7 A08 = ((AnonymousClass1Y7) this.A0A.A09("KEY_IS_PREFETCH"));
    public final AnonymousClass1Y7 A09;
    public final AnonymousClass1Y7 A0A;

    public synchronized Optional A00() {
        Optional of;
        Preconditions.checkState(this.A00.BFQ());
        String B4F = this.A00.B4F(this.A09, null);
        if (B4F == null) {
            return Optional.absent();
        }
        long At2 = this.A00.At2(this.A06, -1);
        Uri parse = Uri.parse(B4F);
        int AqN = this.A00.AqN(this.A01, 0);
        long At22 = this.A00.At2(this.A05, 0);
        if (At2 == -1) {
            of = Optional.absent();
        } else {
            of = Optional.of(Long.valueOf(At2));
        }
        return Optional.of(new C33651nv(parse, AqN, At22, of, this.A00.Aep(this.A08, false), this.A00.Aep(this.A07, false), this.A00.B4F(this.A03, null), this.A00.B4F(this.A02, null), this.A00.B4F(this.A04, null)));
    }

    public AnonymousClass1QT(FbSharedPreferences fbSharedPreferences, String str) {
        this.A00 = fbSharedPreferences;
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) AnonymousClass1MH.A00.A09(AnonymousClass08S.A0J(str, "EFFICIENCY"));
        this.A0A = r1;
        this.A09 = (AnonymousClass1Y7) r1.A09("KEY_URI");
    }
}
