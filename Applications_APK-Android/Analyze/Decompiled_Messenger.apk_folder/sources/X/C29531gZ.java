package X;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

@UserScoped
/* renamed from: X.1gZ  reason: invalid class name and case insensitive filesystem */
public final class C29531gZ {
    private static C05540Zi A01;
    public AnonymousClass0UN A00;

    public static final C29531gZ A00(AnonymousClass1XY r4) {
        C29531gZ r0;
        synchronized (C29531gZ.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C29531gZ((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C29531gZ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C29531gZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public void A01(Activity activity) {
        C11460nC r1;
        AnonymousClass064.A00(activity);
        C29551gb r2 = new C29551gb(this, new C29541ga(this, activity));
        Context context = activity;
        if (!(activity instanceof C11460nC) && (activity instanceof ContextWrapper)) {
            context = activity.getBaseContext();
        }
        if (context instanceof C11460nC) {
            r1 = (C11460nC) context;
        } else {
            r1 = null;
        }
        if (r1 != null) {
            r1.AMa(r2);
        }
        AnonymousClass13y.A01(activity.getWindow(), (MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, this.A00));
    }
}
