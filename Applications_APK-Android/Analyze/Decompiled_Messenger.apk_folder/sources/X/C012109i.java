package X;

import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.09i  reason: invalid class name and case insensitive filesystem */
public final class C012109i {
    private static C012109i A07;
    private static final long A08 = TimeUnit.MINUTES.toMillis(2);
    private long A00;
    private final Lock A01 = new ReentrantLock();
    private volatile StatFs A02 = null;
    private volatile StatFs A03 = null;
    private volatile File A04;
    private volatile File A05;
    private volatile boolean A06 = false;

    private static StatFs A00(StatFs statFs, File file) {
        if (file != null && file.exists()) {
            if (statFs == null) {
                try {
                    return new StatFs(file.getAbsolutePath());
                } catch (IllegalArgumentException unused) {
                } catch (Throwable th) {
                    throw AnonymousClass95E.A00(th);
                }
            } else {
                statFs.restat(file.getAbsolutePath());
                return statFs;
            }
        }
        return null;
    }

    public static synchronized C012109i A01() {
        C012109i r0;
        synchronized (C012109i.class) {
            if (A07 == null) {
                A07 = new C012109i();
            }
            r0 = A07;
        }
        return r0;
    }

    private void A02() {
        if (!this.A06) {
            this.A01.lock();
            try {
                if (!this.A06) {
                    this.A05 = Environment.getDataDirectory();
                    this.A04 = Environment.getExternalStorageDirectory();
                    A04();
                    this.A06 = true;
                }
            } finally {
                this.A01.unlock();
            }
        }
    }

    private void A03() {
        if (this.A01.tryLock()) {
            try {
                if (SystemClock.uptimeMillis() - this.A00 > A08) {
                    A04();
                }
            } finally {
                this.A01.unlock();
            }
        }
    }

    private void A04() {
        this.A03 = A00(this.A03, this.A05);
        this.A02 = A00(this.A02, this.A04);
        this.A00 = SystemClock.uptimeMillis();
    }

    public void A08() {
        if (this.A01.tryLock()) {
            try {
                A02();
                A04();
            } finally {
                this.A01.unlock();
            }
        }
    }

    public long A05(Integer num) {
        StatFs statFs;
        long blockSize;
        long availableBlocks;
        A02();
        A03();
        if (num == AnonymousClass07B.A00) {
            statFs = this.A03;
        } else {
            statFs = this.A02;
        }
        if (statFs == null) {
            return 0;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            blockSize = statFs.getBlockSizeLong();
            availableBlocks = statFs.getAvailableBlocksLong();
        } else {
            blockSize = (long) statFs.getBlockSize();
            availableBlocks = (long) statFs.getAvailableBlocks();
        }
        return blockSize * availableBlocks;
    }

    public long A06(Integer num) {
        StatFs statFs;
        long blockSize;
        long freeBlocks;
        A02();
        A03();
        if (num == AnonymousClass07B.A00) {
            statFs = this.A03;
        } else {
            statFs = this.A02;
        }
        if (statFs == null) {
            return -1;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            blockSize = statFs.getBlockSizeLong();
            freeBlocks = statFs.getFreeBlocksLong();
        } else {
            blockSize = (long) statFs.getBlockSize();
            freeBlocks = (long) statFs.getFreeBlocks();
        }
        return blockSize * freeBlocks;
    }

    public long A07(Integer num) {
        StatFs statFs;
        long blockSize;
        long blockCount;
        A02();
        A03();
        if (num == AnonymousClass07B.A00) {
            statFs = this.A03;
        } else {
            statFs = this.A02;
        }
        if (statFs == null) {
            return -1;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            blockSize = statFs.getBlockSizeLong();
            blockCount = statFs.getBlockCountLong();
        } else {
            blockSize = (long) statFs.getBlockSize();
            blockCount = (long) statFs.getBlockCount();
        }
        return blockSize * blockCount;
    }

    public boolean A09(Integer num, long j) {
        A02();
        long A052 = A05(num);
        if (A052 <= 0 || A052 < j) {
            return true;
        }
        return false;
    }
}
