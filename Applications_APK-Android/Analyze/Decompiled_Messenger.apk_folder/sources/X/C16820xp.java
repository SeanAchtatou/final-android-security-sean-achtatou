package X;

import com.facebook.graphql.enums.GraphQLMessengerAdProductType;
import com.facebook.graphql.enums.GraphQLMessengerDiscoverTabItemAttachmentStyle;
import com.facebook.graphql.enums.GraphQLMessengerDiscoverTabItemMetalineContentType;
import com.facebook.graphql.enums.GraphQLMessengerDiscoverTabItemType;
import com.facebook.graphql.enums.GraphQLMessengerInbox2PageMetalineContentType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape3S0000000;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0xp  reason: invalid class name and case insensitive filesystem */
public final class C16820xp extends C12190on implements C182610z, AnonymousClass110, C16880xv, C182210u, C12200oo {
    public GSTModelShape1S0000000 Abx() {
        return (GSTModelShape1S0000000) A0J(-1172959501, GSTModelShape1S0000000.class, 870548419);
    }

    public ImmutableList Ac2() {
        return A0M(189528236, GSTModelShape1S0000000.class, -1181117631);
    }

    public GSTModelShape1S0000000 Ac5() {
        return (GSTModelShape1S0000000) A0J(-1152579819, GSTModelShape1S0000000.class, 664938463);
    }

    public GSTModelShape1S0000000 Ac8() {
        return (GSTModelShape1S0000000) A0J(-1152373461, GSTModelShape1S0000000.class, -1644768625);
    }

    public ImmutableList AcB() {
        return A0N(-1242153459, GraphQLMessengerAdProductType.A03);
    }

    public GSTModelShape1S0000000 AcD() {
        return (GSTModelShape1S0000000) A0J(171231184, GSTModelShape1S0000000.class, 705696870);
    }

    public GSTModelShape1S0000000 AdK() {
        return (GSTModelShape1S0000000) A0J(1844974813, GSTModelShape1S0000000.class, -115332387);
    }

    public C35961s6 Afi() {
        return (GSTModelShape3S0000000) A0J(-178465831, GSTModelShape3S0000000.class, 423528630);
    }

    public GSTModelShape1S0000000 Afl() {
        return (GSTModelShape1S0000000) A0J(-1972985569, GSTModelShape1S0000000.class, 1966507417);
    }

    public C35961s6 Ajq() {
        return (GSTModelShape3S0000000) A0J(1624984052, GSTModelShape3S0000000.class, 423528630);
    }

    public GSTModelShape1S0000000 AmG() {
        return (GSTModelShape1S0000000) A0J(747380345, GSTModelShape1S0000000.class, 2029365657);
    }

    public /* bridge */ /* synthetic */ C29330EVg Amw() {
        return (C29328EVe) A0J(-1914687952, C29328EVe.class, 1405338267);
    }

    public /* bridge */ /* synthetic */ C29331EVh Amx() {
        return (C29328EVe) A0J(-1914687952, C29328EVe.class, 1405338267);
    }

    public GSTModelShape1S0000000 ApL() {
        return (GSTModelShape1S0000000) A0J(100313435, GSTModelShape1S0000000.class, 1916581064);
    }

    public GSTModelShape1S0000000 ArW() {
        return (GSTModelShape1S0000000) A0J(1521244368, GSTModelShape1S0000000.class, -209408746);
    }

    public GSTModelShape1S0000000 ArX() {
        return (GSTModelShape1S0000000) A0J(61251208, GSTModelShape1S0000000.class, 871306547);
    }

    public AnonymousClass4BD Ara() {
        return (AnonymousClass4BC) A0J(-1961448145, AnonymousClass4BC.class, 1074949999);
    }

    public GraphQLMessengerDiscoverTabItemType Ard() {
        return (GraphQLMessengerDiscoverTabItemType) A0O(-2141142810, GraphQLMessengerDiscoverTabItemType.A03);
    }

    public GSTModelShape1S0000000 Are() {
        return (GSTModelShape1S0000000) A0J(-2141119113, GSTModelShape1S0000000.class, 1418842304);
    }

    public GSTModelShape1S0000000 AtV() {
        return (GSTModelShape1S0000000) A0J(-1451875826, GSTModelShape1S0000000.class, -1333479585);
    }

    public GSTModelShape1S0000000 Aww() {
        return (GSTModelShape1S0000000) A0J(3433103, GSTModelShape1S0000000.class, -53667687);
    }

    public GSTModelShape1S0000000 AzR() {
        return (GSTModelShape1S0000000) A0J(-309425751, GSTModelShape1S0000000.class, 1686511661);
    }

    public GraphQLMessengerInbox2PageMetalineContentType B26() {
        return (GraphQLMessengerInbox2PageMetalineContentType) A0O(1752778939, GraphQLMessengerInbox2PageMetalineContentType.A02);
    }

    public GraphQLMessengerDiscoverTabItemMetalineContentType B28() {
        return (GraphQLMessengerDiscoverTabItemMetalineContentType) A0O(989170613, GraphQLMessengerDiscoverTabItemMetalineContentType.A02);
    }

    public ImmutableList B4W() {
        return A0N(-891774750, GraphQLMessengerDiscoverTabItemAttachmentStyle.A08);
    }

    public GSTModelShape1S0000000 B5L() {
        return (GSTModelShape1S0000000) A0J(-880905839, GSTModelShape1S0000000.class, -4650298);
    }

    private C16820xp(int i, int[] iArr) {
        super(i, iArr);
    }

    public String Aby() {
        return A0P(-2005255391);
    }

    public String Ac1() {
        return A0P(878301664);
    }

    public String Ac3() {
        return A0P(-1381017590);
    }

    public String Ac4() {
        return A0P(1929805195);
    }

    public String Ac6() {
        return A0P(92655287);
    }

    public String AcA() {
        return A0P(910033828);
    }

    public String AcC() {
        return A0P(1058756799);
    }

    public String AcE() {
        return A0P(-1359893700);
    }

    public String Agm() {
        return A0P(50511102);
    }

    public String Amy() {
        return A0P(-1914358044);
    }

    public boolean Aqo() {
        return getBooleanValue(-932608554);
    }

    public boolean Aqs() {
        return getBooleanValue(-1669931649);
    }

    public boolean ArD() {
        return getBooleanValue(-376876422);
    }

    public String B27() {
        return A0P(989151656);
    }

    public boolean B33() {
        return getBooleanValue(463536125);
    }

    public String B3R() {
        return A0P(-823445795);
    }

    public String B6a() {
        return A0P(-1474153103);
    }
}
