package X;

import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.19e  reason: invalid class name and case insensitive filesystem */
public final class C196619e extends AnonymousClass0W4 {
    public static final AnonymousClass0W6 A00 = new AnonymousClass0W6("id", "INTEGER");
    public static final AnonymousClass0W6 A01 = new AnonymousClass0W6("key", "BLOB");
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("timestamp_ms", "INTEGER");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("name", "TEXT");

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C196619e() {
        /*
            r4 = this;
            X.0W6 r3 = X.C196619e.A00
            X.0W6 r2 = X.C196619e.A03
            X.0W6 r1 = X.C196619e.A02
            X.0W6 r0 = X.C196619e.A01
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r3, r2, r1, r0)
            X.0al r1 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            r1.<init>(r0)
            java.lang.String r0 = "user_devices"
            r4.<init>(r0, r2, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C196619e.<init>():void");
    }
}
