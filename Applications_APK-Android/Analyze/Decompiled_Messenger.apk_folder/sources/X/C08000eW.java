package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eW  reason: invalid class name and case insensitive filesystem */
public final class C08000eW extends C08190ep {
    private static volatile C08000eW A00;

    public static final C08000eW A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C08000eW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C08000eW();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (com.facebook.common.dextricks.classid.ClassId.sInitialized == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0Ti AsW() {
        /*
            r3 = this;
            boolean r0 = com.facebook.common.dextricks.classtracing.logger.ClassTracingLogger.sEnabled
            if (r0 == 0) goto L_0x0009
            boolean r1 = com.facebook.common.dextricks.classid.ClassId.sInitialized
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x0018
            r0 = -1
            int[] r2 = new int[]{r0}
            X.0Ti r1 = new X.0Ti
            r0 = 0
            r1.<init>(r0, r2, r0)
            return r1
        L_0x0018:
            X.0Ti r0 = X.AnonymousClass0Ti.A04
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08000eW.AsW():X.0Ti");
    }
}
