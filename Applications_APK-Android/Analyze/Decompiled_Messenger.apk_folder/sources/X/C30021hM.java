package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.common.calltoaction.model.CTAUserConfirmation;

/* renamed from: X.1hM  reason: invalid class name and case insensitive filesystem */
public final class C30021hM implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new CTAUserConfirmation(parcel);
    }

    public Object[] newArray(int i) {
        return new CTAUserConfirmation[i];
    }
}
