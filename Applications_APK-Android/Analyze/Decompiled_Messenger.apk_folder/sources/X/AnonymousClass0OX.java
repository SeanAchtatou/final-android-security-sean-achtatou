package X;

import android.os.Bundle;
import com.facebook.rti.push.service.FbnsService;
import java.util.ArrayList;

/* renamed from: X.0OX  reason: invalid class name */
public final class AnonymousClass0OX implements AnonymousClass0RR {
    public Bundle AXc(FbnsService fbnsService, Bundle bundle) {
        Bundle bundle2 = new Bundle();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        fbnsService.A0c(arrayList, arrayList2);
        bundle2.putStringArrayList("valid_compatible_apps", arrayList);
        bundle2.putStringArrayList("enabled_compatible_apps", arrayList2);
        ArrayList arrayList3 = new ArrayList();
        fbnsService.A0b(arrayList3);
        bundle2.putStringArrayList("registered_apps", arrayList3);
        return bundle2;
    }

    public void AXk(FbnsService fbnsService, Bundle bundle) {
        C010708t.A0I("AppsStatisticsFetcher", "not implemented for AppsStatisticsFetcher");
        throw new IllegalArgumentException("not implemented for AppsStatisticsFetcher");
    }
}
