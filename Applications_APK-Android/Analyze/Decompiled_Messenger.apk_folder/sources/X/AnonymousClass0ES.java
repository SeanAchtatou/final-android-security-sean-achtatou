package X;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* renamed from: X.0ES  reason: invalid class name */
public class AnonymousClass0ES extends AnonymousClass0EL {
    public AnonymousClass0EU[] A00;
    public final ZipFile A01;
    private final AnonymousClass02E A02;
    public final /* synthetic */ C003201y A03;

    public boolean A02(ZipEntry zipEntry, String str) {
        return true;
    }

    public AnonymousClass0ES(C003201y r3, AnonymousClass02E r4) {
        this.A03 = r3;
        this.A01 = new ZipFile(r3.A00);
        this.A02 = r4;
    }

    public final AnonymousClass0ET A00() {
        return new AnonymousClass0ET(A03());
    }

    public final AnonymousClass0EM A01() {
        return new AnonymousClass0EW(this);
    }

    public final AnonymousClass0EU[] A03() {
        if (this.A00 == null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            HashMap hashMap = new HashMap();
            Pattern compile = Pattern.compile(this.A03.A01);
            String[] A022 = C002401o.A02();
            Enumeration<? extends ZipEntry> entries = this.A01.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                Matcher matcher = compile.matcher(zipEntry.getName());
                if (matcher.matches()) {
                    String group = matcher.group(1);
                    String group2 = matcher.group(2);
                    int i = 0;
                    while (true) {
                        if (i >= A022.length) {
                            i = -1;
                            break;
                        }
                        String str = A022[i];
                        if (str != null && group.equals(str)) {
                            break;
                        }
                        i++;
                    }
                    if (i >= 0) {
                        linkedHashSet.add(group);
                        AnonymousClass0EU r0 = (AnonymousClass0EU) hashMap.get(group2);
                        if (r0 == null || i < r0.A00) {
                            hashMap.put(group2, new AnonymousClass0EU(group2, zipEntry, i));
                        }
                    }
                }
            }
            this.A02.A01 = (String[]) linkedHashSet.toArray(new String[linkedHashSet.size()]);
            AnonymousClass0EU[] r6 = (AnonymousClass0EU[]) hashMap.values().toArray(new AnonymousClass0EU[hashMap.size()]);
            Arrays.sort(r6);
            int i2 = 0;
            int i3 = 0;
            while (true) {
                if (i2 >= r4) {
                    break;
                }
                AnonymousClass0EU r02 = r6[i2];
                if (A02(r02.A01, r02.A01)) {
                    i3++;
                } else {
                    r6[i2] = null;
                }
                i2++;
            }
            AnonymousClass0EU[] r3 = new AnonymousClass0EU[i3];
            int i4 = 0;
            for (AnonymousClass0EU r1 : r6) {
                if (r1 != null) {
                    r3[i4] = r1;
                    i4++;
                }
            }
            this.A00 = r3;
        }
        return this.A00;
    }

    public void close() {
        this.A01.close();
    }
}
