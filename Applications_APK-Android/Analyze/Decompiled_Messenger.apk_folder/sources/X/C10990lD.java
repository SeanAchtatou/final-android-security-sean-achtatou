package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.0lD  reason: invalid class name and case insensitive filesystem */
public final class C10990lD implements AnonymousClass0lE {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C = ((AnonymousClass1Y7) A0Z.A09("defaultapp/"));
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F;
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y7 A0H;
    public static final AnonymousClass1Y7 A0I;
    public static final AnonymousClass1Y7 A0J;
    public static final AnonymousClass1Y7 A0K;
    public static final AnonymousClass1Y7 A0L;
    public static final AnonymousClass1Y7 A0M;
    public static final AnonymousClass1Y7 A0N;
    public static final AnonymousClass1Y7 A0O;
    public static final AnonymousClass1Y7 A0P;
    public static final AnonymousClass1Y7 A0Q;
    public static final AnonymousClass1Y7 A0R;
    public static final AnonymousClass1Y7 A0S = ((AnonymousClass1Y7) A0Z.A09("user/"));
    public static final AnonymousClass1Y7 A0T;
    public static final AnonymousClass1Y7 A0U;
    public static final AnonymousClass1Y7 A0V;
    public static final AnonymousClass1Y7 A0W;
    public static final AnonymousClass1Y8 A0X = A0a.A09("defaultapp/");
    public static final AnonymousClass1Y8 A0Y = A0X.A09("sms_local_spam_and_ranking_next_update_time");
    private static final AnonymousClass1Y7 A0Z = ((AnonymousClass1Y7) C04350Ue.A06.A09("sms_integration/"));
    private static final AnonymousClass1Y8 A0a = C04350Ue.A0B.A09("sms_integration/");

    static {
        AnonymousClass1Y7 r1 = A0C;
        A0I = (AnonymousClass1Y7) r1.A09("sms_in_readonly_mode");
        A0T = (AnonymousClass1Y7) r1.A09("sms_readonly_set_time");
        A0A = (AnonymousClass1Y7) r1.A09("sms_fullmode_set_time");
        A0P = (AnonymousClass1Y7) r1.A09("sms_nux_complete");
        A0Q = (AnonymousClass1Y7) r1.A09("sms_nux_v2_seen");
        A0E = (AnonymousClass1Y7) r1.A09("sms_initial_enroll_time");
        A0R = (AnonymousClass1Y7) r1.A09("sms_nux_v2_seen_time");
        A0O = (AnonymousClass1Y7) r1.A09("sms_nux_blocks");
        A0V = (AnonymousClass1Y7) r1.A09("sms_interstitial_seen");
        A07 = (AnonymousClass1Y7) r1.A09("sms_device_status_reported");
        A0K = (AnonymousClass1Y7) r1.A09("sms_is_enabled_for_tracking");
        A0J = (AnonymousClass1Y7) r1.A09("sms_is_default_app_for_tracking");
        A00 = (AnonymousClass1Y7) r1.A09("messenger_been_sms_default_app");
        A0F = (AnonymousClass1Y7) r1.A09("sms_internal_force_nux");
        A0H = (AnonymousClass1Y7) r1.A09("sms_internal_suppress_nux");
        A0G = (AnonymousClass1Y7) r1.A09("sms_internal_no_readonly_notification");
        A0D = (AnonymousClass1Y7) r1.A09("sms_info_qp_disabled");
        A0B = (AnonymousClass1Y7) r1.A09("sms_full_qp_disabled");
        A0U = (AnonymousClass1Y7) r1.A09("sms_returning_readonly_qp_disabled");
        AnonymousClass1Y7 r12 = A0C;
        A05 = (AnonymousClass1Y7) r12.A09("sms_custom_theme_color");
        A0W = (AnonymousClass1Y7) r12.A09("sms_show_sms_white_list");
        A0L = (AnonymousClass1Y7) r12.A09("sms_matching_interstitial_shown");
        A01 = (AnonymousClass1Y7) r12.A09("sms_automatic_matching_enabled");
        A08 = (AnonymousClass1Y7) r12.A09("sms_eligible_for_none_to_full_upsells");
        A0M = (AnonymousClass1Y7) r12.A09("sms_none_to_full_upsell_interstitial_seen");
        AnonymousClass1Y7 r13 = A0Z;
        A02 = (AnonymousClass1Y7) r13.A09("sms_auto_retrieve");
        A03 = (AnonymousClass1Y7) r13.A09("sms_auto_retrieve_roaming");
        A04 = (AnonymousClass1Y7) r13.A09("sms_chat_heads");
        A09 = (AnonymousClass1Y7) r13.A09("sms_fallback_number");
        A06 = (AnonymousClass1Y7) r13.A09("sms_debug_msg_errors/");
        A0N = (AnonymousClass1Y7) r13.A09("sms_notification_sound");
    }

    public static final C10990lD A00() {
        return new C10990lD();
    }

    public ImmutableSet Avs() {
        return ImmutableSet.A04(A0Y);
    }
}
