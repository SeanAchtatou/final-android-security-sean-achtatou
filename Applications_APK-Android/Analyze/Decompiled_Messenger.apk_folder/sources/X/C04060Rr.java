package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0Rr  reason: invalid class name and case insensitive filesystem */
public final class C04060Rr {
    public final List A00;

    public C04060Rr(List list) {
        this.A00 = Collections.unmodifiableList(new ArrayList(list));
    }
}
