package X;

import com.facebook.messaging.business.common.calltoaction.model.AdCallToAction;
import com.facebook.messaging.business.inboxads.common.InboxAdsImage;
import com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo;
import com.facebook.messaging.business.inboxads.common.InboxAdsVideo;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1sC  reason: invalid class name and case insensitive filesystem */
public final class C36021sC {
    public double A00;
    public int A01;
    public int A02;
    public long A03;
    public AdCallToAction A04;
    public InboxAdsImage A05;
    public InboxAdsVideo A06;
    public ImmutableList A07;
    public ImmutableList A08;
    public ImmutableList A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public String A0I;
    public Set A0J;
    public boolean A0K;

    public C36021sC() {
        this.A0J = new HashSet();
        this.A0A = BuildConfig.FLAVOR;
        this.A0B = BuildConfig.FLAVOR;
        this.A0C = BuildConfig.FLAVOR;
        this.A0D = BuildConfig.FLAVOR;
        this.A0E = BuildConfig.FLAVOR;
        this.A0F = BuildConfig.FLAVOR;
        this.A0G = BuildConfig.FLAVOR;
        this.A0H = BuildConfig.FLAVOR;
        this.A0I = BuildConfig.FLAVOR;
        this.A02 = -1;
    }

    public C36021sC(InboxAdsMediaInfo inboxAdsMediaInfo) {
        this.A0J = new HashSet();
        C28931fb.A05(inboxAdsMediaInfo);
        if (inboxAdsMediaInfo instanceof InboxAdsMediaInfo) {
            this.A07 = inboxAdsMediaInfo.A07;
            this.A0A = inboxAdsMediaInfo.A0A;
            this.A0B = inboxAdsMediaInfo.A0B;
            this.A01 = inboxAdsMediaInfo.A01;
            this.A0C = inboxAdsMediaInfo.A0C;
            this.A0D = inboxAdsMediaInfo.A0D;
            this.A03 = inboxAdsMediaInfo.A03;
            this.A08 = inboxAdsMediaInfo.A08;
            this.A00 = inboxAdsMediaInfo.A00;
            this.A04 = inboxAdsMediaInfo.A04;
            this.A0E = inboxAdsMediaInfo.A0E;
            this.A0F = inboxAdsMediaInfo.A0F;
            this.A05 = inboxAdsMediaInfo.A05;
            this.A0K = inboxAdsMediaInfo.A0K;
            this.A09 = inboxAdsMediaInfo.A09;
            this.A0G = inboxAdsMediaInfo.A0G;
            this.A0H = inboxAdsMediaInfo.A0H;
            this.A0I = inboxAdsMediaInfo.A0I;
            this.A02 = inboxAdsMediaInfo.A02;
            this.A06 = inboxAdsMediaInfo.A06;
            this.A0J = new HashSet(inboxAdsMediaInfo.A0J);
            return;
        }
        ImmutableList A002 = inboxAdsMediaInfo.A00();
        this.A07 = A002;
        C28931fb.A06(A002, "adCardTypes");
        this.A0J.add("adCardTypes");
        String Ac6 = inboxAdsMediaInfo.Ac6();
        this.A0A = Ac6;
        C28931fb.A06(Ac6, "adId");
        String Ac7 = inboxAdsMediaInfo.Ac7();
        this.A0B = Ac7;
        C28931fb.A06(Ac7, "adItemId");
        this.A01 = inboxAdsMediaInfo.Ac9();
        String str = inboxAdsMediaInfo.A0C;
        this.A0C = str;
        C28931fb.A06(str, "adTitle");
        String AcF = inboxAdsMediaInfo.AcF();
        this.A0D = AcF;
        C28931fb.A06(AcF, "adToken");
        this.A03 = inboxAdsMediaInfo.AcG();
        ImmutableList A012 = inboxAdsMediaInfo.A01();
        this.A08 = A012;
        C28931fb.A06(A012, "adTypes");
        this.A0J.add("adTypes");
        this.A00 = inboxAdsMediaInfo.A00;
        this.A04 = inboxAdsMediaInfo.A04;
        String str2 = inboxAdsMediaInfo.A0E;
        this.A0E = str2;
        C28931fb.A06(str2, "cardDescription");
        String str3 = inboxAdsMediaInfo.A0F;
        this.A0F = str3;
        C28931fb.A06(str3, "description");
        this.A05 = inboxAdsMediaInfo.A05;
        this.A0K = inboxAdsMediaInfo.A0K;
        ImmutableList AvU = inboxAdsMediaInfo.AvU();
        this.A09 = AvU;
        C28931fb.A06(AvU, "nestedAdItems");
        this.A0J.add("nestedAdItems");
        String str4 = inboxAdsMediaInfo.A0G;
        this.A0G = str4;
        C28931fb.A06(str4, "pageId");
        String str5 = inboxAdsMediaInfo.A0H;
        this.A0H = str5;
        C28931fb.A06(str5, "photoDescription");
        String str6 = inboxAdsMediaInfo.A0I;
        this.A0I = str6;
        C28931fb.A06(str6, "title");
        this.A02 = inboxAdsMediaInfo.B7T();
        this.A06 = inboxAdsMediaInfo.A06;
    }
}
