package X;

import android.text.TextUtils;
import io.card.payment.BuildConfig;

/* renamed from: X.0KE  reason: invalid class name */
public final class AnonymousClass0KE implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.common.analytics.defaultlogger.DefaultAnalyticsLogger$EventRunnable";
    private C01750Bm A00;
    public final /* synthetic */ AnonymousClass0QV A01;

    public AnonymousClass0KE(AnonymousClass0QV r1, C01750Bm r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void run() {
        AnonymousClass0QV r0 = this.A01;
        C01750Bm r3 = this.A00;
        String string = r0.A01.getString("user_id", BuildConfig.FLAVOR);
        if (TextUtils.isEmpty(string)) {
            string = "0";
        }
        r3.A02.put("pk", string);
        AnonymousClass0L0 r02 = this.A01.A00;
        r02.A07.add(this.A00);
        this.A01.A02.removeMessages(1);
        if (this.A01.A00.A07.size() >= 50) {
            AnonymousClass0QV.A00(this.A01);
        } else {
            this.A01.A02.sendEmptyMessageDelayed(1, 300000);
        }
    }
}
