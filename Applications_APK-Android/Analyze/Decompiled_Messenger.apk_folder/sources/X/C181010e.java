package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import com.facebook.litho.annotations.Comparable;
import com.google.common.collect.ImmutableList;

/* renamed from: X.10e  reason: invalid class name and case insensitive filesystem */
public final class C181010e extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public C33691nz A02;
    @Comparable(type = 13)
    public C15350v9 A03;
    @Comparable(type = 5)
    public ImmutableList A04;

    public static Drawable A03(Context context, Bitmap bitmap, float f, int i, int i2, String str, int i3, int i4, C15400vE r19) {
        Context context2 = context;
        C75093j8 A002 = C58782uH.A00(context, str, i, i2);
        int A003 = C007106r.A00(context, (float) i3);
        int A004 = C007106r.A00(context, (float) i4);
        Rect rect = new Rect(A003, A004, A002.getIntrinsicWidth() + A003, A002.getIntrinsicHeight() + A004);
        A002.setBounds(rect);
        Rect rect2 = new Rect((int) (((float) rect.left) / f), (int) (((float) rect.top) / f), (int) (((float) rect.right) / f), (int) (((float) rect.bottom) / f));
        Path A012 = A01(context2, bitmap.getWidth(), bitmap.getHeight(), rect2, f, r19);
        if (A012 == null) {
            return null;
        }
        return new C75103j9(context2, bitmap, A012, A002);
    }

    public C181010e(Context context) {
        super("TabBar");
        this.A01 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }

    public static float A00(Point point, Point point2) {
        int i = point.x;
        return ((float) ((Math.atan2((double) (point2.y - point.y), (double) (point2.x - i)) * 180.0d) / 3.141592653589793d)) + 180.0f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008c, code lost:
        if (r5.bottom < r4.top) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x004e, code lost:
        if (r5.bottom >= r4.bottom) goto L_0x0050;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Path A01(android.content.Context r9, int r10, int r11, android.graphics.Rect r12, float r13, X.C15400vE r14) {
        /*
            android.graphics.Rect r4 = new android.graphics.Rect
            r0 = 0
            r4.<init>(r0, r0, r10, r11)
            r0 = 1077936128(0x40400000, float:3.0)
            int r0 = X.C007106r.A00(r9, r0)
            float r0 = (float) r0
            float r0 = r0 / r13
            int r6 = (int) r0
            android.graphics.Rect r5 = new android.graphics.Rect
            int r3 = r12.left
            int r3 = r3 - r6
            int r2 = r12.top
            int r2 = r2 - r6
            int r1 = r12.right
            int r1 = r1 + r6
            int r0 = r12.bottom
            int r0 = r0 + r6
            r5.<init>(r3, r2, r1, r0)
            int r1 = r5.width()
            int r0 = r5.height()
            r6 = 0
            if (r1 < r0) goto L_0x002c
            r6 = 1
        L_0x002c:
            java.lang.String r3 = "Cutout must not be taller than it is wide. "
            java.lang.String r2 = r4.toShortString()
            java.lang.String r1 = " "
            java.lang.String r0 = r5.toShortString()
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r3, r0)
            com.google.common.base.Preconditions.checkArgument(r6, r0)
            int r1 = r5.left
            int r0 = r4.left
            if (r1 <= r0) goto L_0x0050
            int r1 = r5.bottom
            int r0 = r4.bottom
            r3 = 1
            if (r1 < r0) goto L_0x0051
        L_0x0050:
            r3 = 0
        L_0x0051:
            if (r3 != 0) goto L_0x0078
            java.lang.String r2 = r4.toShortString()
            java.lang.String r1 = " "
            java.lang.String r0 = r5.toShortString()
            java.lang.String r2 = X.AnonymousClass08S.A0P(r2, r1, r0)
            int r0 = r14.A00
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            X.10O r0 = r14.A01
            java.lang.String r0 = r0.name()
            java.lang.Object[] r2 = new java.lang.Object[]{r2, r1, r0}
            java.lang.String r1 = "BadgeCutoutPathFactory"
            java.lang.String r0 = "Invalid cutout! debug info: %s, count: %d, type: %s"
            X.C010708t.A0Q(r1, r0, r2)
        L_0x0078:
            if (r3 != 0) goto L_0x007c
            r7 = 0
            return r7
        L_0x007c:
            android.graphics.Path r7 = new android.graphics.Path
            r7.<init>()
            int r0 = r5.left
            int r6 = r4.right
            if (r0 > r6) goto L_0x008e
            int r2 = r5.bottom
            int r1 = r4.top
            r0 = 1
            if (r2 >= r1) goto L_0x008f
        L_0x008e:
            r0 = 0
        L_0x008f:
            if (r0 != 0) goto L_0x00a6
            android.graphics.RectF r5 = new android.graphics.RectF
            int r0 = r4.left
            float r3 = (float) r0
            int r0 = r4.top
            float r2 = (float) r0
            float r1 = (float) r6
            int r0 = r4.bottom
            float r0 = (float) r0
            r5.<init>(r3, r2, r1, r0)
            android.graphics.Path$Direction r0 = android.graphics.Path.Direction.CW
            r7.addRect(r5, r0)
            return r7
        L_0x00a6:
            int r0 = r5.height()
            int r14 = r0 >> 1
            android.graphics.Point r6 = new android.graphics.Point
            int r1 = r5.left
            int r1 = r1 + r14
            int r0 = r5.centerY()
            r6.<init>(r1, r0)
            int r1 = r4.top
            int r0 = r5.centerY()
            if (r1 >= r0) goto L_0x0172
            android.graphics.Point r10 = new android.graphics.Point
            int r1 = r5.left
            int r0 = r5.centerY()
            r10.<init>(r1, r0)
        L_0x00cb:
            int r0 = r5.height()
            int r2 = r0 >> 1
            android.graphics.Point r9 = new android.graphics.Point
            int r1 = r5.left
            int r1 = r1 + r2
            int r0 = r5.centerY()
            r9.<init>(r1, r0)
            int r8 = r4.right
            int r1 = r5.left
            int r1 = r1 + r2
            if (r8 <= r1) goto L_0x0153
            android.graphics.Point r3 = new android.graphics.Point
            int r0 = r5.bottom
            r3.<init>(r1, r0)
        L_0x00eb:
            float r11 = A00(r10, r6)
            float r9 = A00(r3, r6)
            int r0 = r4.left
            float r1 = (float) r0
            int r0 = r4.top
            float r0 = (float) r0
            r7.moveTo(r1, r0)
            int r0 = r5.left
            float r1 = (float) r0
            int r0 = r4.top
            float r0 = (float) r0
            r7.lineTo(r1, r0)
            int r0 = r10.x
            float r1 = (float) r0
            int r0 = r10.y
            float r0 = (float) r0
            r7.lineTo(r1, r0)
            android.graphics.RectF r10 = new android.graphics.RectF
            int r1 = r6.x
            int r0 = r1 - r14
            float r8 = (float) r0
            int r6 = r6.y
            int r0 = r6 - r14
            float r2 = (float) r0
            int r1 = r1 + r14
            float r1 = (float) r1
            int r6 = r6 + r14
            float r0 = (float) r6
            r10.<init>(r8, r2, r1, r0)
            float r9 = r9 - r11
            r7.arcTo(r10, r11, r9)
            int r0 = r3.x
            float r1 = (float) r0
            int r0 = r3.y
            float r0 = (float) r0
            r7.lineTo(r1, r0)
            int r0 = r4.right
            float r1 = (float) r0
            int r0 = r5.bottom
            float r0 = (float) r0
            r7.lineTo(r1, r0)
            int r0 = r4.right
            float r1 = (float) r0
            int r0 = r4.bottom
            float r0 = (float) r0
            r7.lineTo(r1, r0)
            int r0 = r4.left
            float r1 = (float) r0
            int r0 = r4.bottom
            float r0 = (float) r0
            r7.lineTo(r1, r0)
            int r0 = r4.left
            float r1 = (float) r0
            int r0 = r4.top
            float r0 = (float) r0
            r7.lineTo(r1, r0)
            return r7
        L_0x0153:
            int r13 = r9.x
            int r13 = r13 - r8
            double r0 = (double) r2
            r2 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r11 = java.lang.Math.pow(r0, r2)
            double r0 = (double) r13
            double r0 = java.lang.Math.pow(r0, r2)
            double r11 = r11 - r0
            double r0 = java.lang.Math.sqrt(r11)
            int r2 = (int) r0
            android.graphics.Point r3 = new android.graphics.Point
            int r0 = r9.y
            int r0 = r0 + r2
            r3.<init>(r8, r0)
            goto L_0x00eb
        L_0x0172:
            int r0 = r5.height()
            int r2 = r0 >> 1
            android.graphics.Point r9 = new android.graphics.Point
            int r1 = r5.left
            int r1 = r1 + r2
            float r0 = r5.exactCenterY()
            int r0 = (int) r0
            r9.<init>(r1, r0)
            int r8 = r4.top
            int r0 = r9.y
            int r12 = r8 - r0
            double r0 = (double) r2
            r2 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r10 = java.lang.Math.pow(r0, r2)
            double r0 = (double) r12
            double r0 = java.lang.Math.pow(r0, r2)
            double r10 = r10 - r0
            double r2 = java.lang.Math.sqrt(r10)
            int r1 = (int) r2
            android.graphics.Point r10 = new android.graphics.Point
            int r0 = r9.x
            int r0 = r0 - r1
            r10.<init>(r0, r8)
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C181010e.A01(android.content.Context, int, int, android.graphics.Rect, float, X.0vE):android.graphics.Path");
    }

    public static Drawable A02(Context context, Bitmap bitmap, float f, int i, int i2, int i3, C15400vE r17) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(1);
        gradientDrawable.setColor(i);
        Context context2 = context;
        int A002 = C007106r.A00(context, (float) 8);
        int A003 = C007106r.A00(context, (float) i2);
        int A004 = C007106r.A00(context, (float) i3);
        Rect rect = new Rect(A003, A004, A003 + A002, A002 + A004);
        gradientDrawable.setBounds(rect);
        Rect rect2 = new Rect((int) (((float) rect.left) / f), (int) (((float) rect.top) / f), (int) (((float) rect.right) / f), (int) (((float) rect.bottom) / f));
        Path A012 = A01(context2, bitmap.getWidth(), bitmap.getHeight(), rect2, f, r17);
        if (A012 == null) {
            return null;
        }
        return new C75103j9(context2, bitmap, A012, gradientDrawable);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01bd, code lost:
        if (r0 != r1) goto L_0x01bf;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C17770zR A0O(X.AnonymousClass0p4 r30) {
        /*
            r29 = this;
            r2 = r29
            com.google.common.collect.ImmutableList r0 = r2.A04
            r28 = r0
            int r0 = r2.A00
            r27 = r0
            X.0v9 r10 = r2.A03
            int r1 = X.AnonymousClass1Y3.BBg
            X.0UN r0 = r2.A01
            r9 = 1
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r9, r1, r0)
            java.lang.Boolean r12 = (java.lang.Boolean) r12
            r11 = r30
            X.1we r8 = X.AnonymousClass11D.A00(r11)
            com.google.common.collect.ImmutableList$Builder r7 = new com.google.common.collect.ImmutableList$Builder
            r7.<init>()
            r6 = 0
        L_0x0023:
            int r0 = r28.size()
            if (r6 >= r0) goto L_0x0161
            r0 = r28
            java.lang.Object r13 = r0.get(r6)
            X.10v r13 = (X.C182310v) r13
            r2 = 0
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r5 = new com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000
            r0 = 5
            r5.<init>(r0)
            X.1I8 r0 = new X.1I8
            r0.<init>()
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000.A05(r5, r11, r2, r2, r0)
            X.0wO r0 = r13.A02
            int r0 = r0.hashCode()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.Object r0 = r5.A02
            X.1I8 r0 = (X.AnonymousClass1I8) r0
            r0.A05 = r1
            java.lang.Object r1 = r5.A00
            java.util.BitSet r1 = (java.util.BitSet) r1
            r1.set(r2)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A00
            android.content.res.ColorStateList r0 = r0.B9n()
            int r1 = X.AnonymousClass1KA.A00(r0)
            java.lang.Object r0 = r5.A02
            X.1I8 r0 = (X.AnonymousClass1I8) r0
            r0.A00 = r1
            java.lang.Object r1 = r5.A00
            java.util.BitSet r1 = (java.util.BitSet) r1
            r1.set(r9)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A00
            android.content.res.ColorStateList r0 = r0.B9n()
            int r1 = X.AnonymousClass1KA.A01(r0)
            java.lang.Object r0 = r5.A02
            X.1I8 r0 = (X.AnonymousClass1I8) r0
            r0.A01 = r1
            java.lang.Object r1 = r5.A00
            java.util.BitSet r1 = (java.util.BitSet) r1
            r0 = 2
            r1.set(r0)
            android.content.Context r14 = r11.A09
            java.lang.String r4 = r13.A03
            X.0wO r1 = r13.A02
            X.0vE r15 = r13.A00
            int r16 = r28.size()
            int r2 = r15.A00
            r0 = 0
            if (r2 > 0) goto L_0x0098
            r0 = 1
        L_0x0098:
            if (r0 == 0) goto L_0x012e
            java.lang.String r3 = ""
        L_0x009c:
            r1 = 2131833624(0x7f113318, float:1.9300335E38)
            int r0 = r6 + r9
            java.lang.Integer r15 = java.lang.Integer.valueOf(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)
            java.lang.Object[] r0 = new java.lang.Object[]{r15, r0}
            java.lang.String r1 = r14.getString(r1, r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            X.C35511rL.A06(r0, r4, r9)
            X.C35511rL.A06(r0, r3, r9)
            X.C35511rL.A06(r0, r1, r9)
            java.lang.String r0 = r0.toString()
            java.lang.Object r1 = r5.A02
            X.1I8 r1 = (X.AnonymousClass1I8) r1
            r1.A04 = r0
            X.1I9 r0 = X.AnonymousClass1I9.A0H
            r1.A03 = r0
            r0 = 1113587712(0x42600000, float:56.0)
            r5.A1p(r0)
            X.0wO r1 = r13.A02
            java.lang.Class<X.10e> r2 = X.C181010e.class
            java.lang.Object[] r1 = new java.lang.Object[]{r11, r1}
            r0 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            X.10N r0 = X.C17780zS.A0E(r2, r11, r0, r1)
            r5.A2y(r0)
            boolean r0 = r12.booleanValue()
            if (r0 == 0) goto L_0x012c
            X.0wO r1 = r13.A02
            java.lang.Object[] r1 = new java.lang.Object[]{r11, r1}
            r0 = 71235917(0x43ef94d, float:2.2448866E-36)
            X.10N r0 = X.C17780zS.A0E(r2, r11, r0, r1)
        L_0x00f7:
            r5.A2R(r0)
            r1 = 0
            r0 = r27
            if (r6 != r0) goto L_0x0100
            r1 = 1
        L_0x0100:
            java.lang.Object r0 = r5.A02
            X.1I8 r0 = (X.AnonymousClass1I8) r0
            r0.A06 = r1
            java.lang.String r1 = "tab_"
            X.0wO r0 = r13.A02
            java.lang.String r0 = r0.name()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r5.A2p(r0)
            java.lang.Object r2 = r5.A00
            java.util.BitSet r2 = (java.util.BitSet) r2
            java.lang.Object r1 = r5.A01
            java.lang.String[] r1 = (java.lang.String[]) r1
            r0 = 3
            X.AnonymousClass11F.A0C(r0, r2, r1)
            java.lang.Object r0 = r5.A02
            X.1I8 r0 = (X.AnonymousClass1I8) r0
            r7.add(r0)
            int r6 = r6 + 1
            goto L_0x0023
        L_0x012c:
            r0 = 0
            goto L_0x00f7
        L_0x012e:
            X.10O r0 = r15.A01
            int r0 = r0.ordinal()
            switch(r0) {
                case 2: goto L_0x0159;
                case 3: goto L_0x0137;
                case 4: goto L_0x0159;
                default: goto L_0x0137;
            }
        L_0x0137:
            int r1 = r1.ordinal()
            r0 = 0
            android.content.res.Resources r3 = r14.getResources()
            if (r1 == r0) goto L_0x0155
            r2 = 2131689711(0x7f0f00ef, float:1.9008445E38)
        L_0x0145:
            int r1 = r15.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r3 = r3.getQuantityString(r2, r1, r0)
            goto L_0x009c
        L_0x0155:
            r2 = 2131689712(0x7f0f00f0, float:1.9008447E38)
            goto L_0x0145
        L_0x0159:
            android.content.res.Resources r3 = r14.getResources()
            r2 = 2131689710(0x7f0f00ee, float:1.9008443E38)
            goto L_0x0145
        L_0x0161:
            com.google.common.collect.ImmutableList r0 = r7.build()
            X.0zR r0 = A04(r11, r0)
            r8.A3A(r0)
            X.1kz r7 = X.C21841Ix.A00(r11)
            com.google.common.collect.ImmutableList$Builder r17 = new com.google.common.collect.ImmutableList$Builder
            r17.<init>()
            r6 = 0
        L_0x0176:
            int r0 = r28.size()
            if (r6 >= r0) goto L_0x034a
            r16 = 0
            r0 = r27
            if (r6 != r0) goto L_0x0184
            r16 = 1
        L_0x0184:
            r0 = r28
            java.lang.Object r5 = r0.get(r6)
            X.10v r5 = (X.C182310v) r5
            X.1we r4 = X.AnonymousClass11D.A00(r11)
            X.0uO r0 = X.C14940uO.CENTER
            r4.A3D(r0)
            X.0uP r0 = X.C14950uP.CENTER
            r4.A3E(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r3 = X.C22291Kt.A00(r11)
            android.content.Context r12 = r11.A09
            X.10Z r13 = r5.A01
            X.0vE r2 = r5.A00
            int r1 = r2.A00
            r0 = 0
            if (r1 > 0) goto L_0x01aa
            r0 = 1
        L_0x01aa:
            if (r0 != 0) goto L_0x01c5
            X.10O r0 = r2.A01
            boolean r0 = r0.A00()
            if (r0 == 0) goto L_0x0340
            int r0 = r13.A00
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r1) goto L_0x01bf
            int r0 = r13.A01
        L_0x01bc:
            r14 = 0
            if (r0 == r1) goto L_0x01c0
        L_0x01bf:
            r14 = 1
        L_0x01c0:
            java.lang.String r0 = "Trying to badge an icon but the badge position is not specified"
            com.google.common.base.Preconditions.checkArgument(r14, r0)
        L_0x01c5:
            android.graphics.drawable.Drawable r1 = r13.A04
            X.10O r14 = r2.A01
            boolean r0 = r14.A00()
            if (r0 == 0) goto L_0x033a
            int r0 = r13.A00
            r24 = r0
        L_0x01d3:
            boolean r0 = r14.A00()
            if (r0 == 0) goto L_0x0336
            int r15 = r13.A01
        L_0x01db:
            boolean r0 = r1 instanceof android.graphics.drawable.BitmapDrawable
            com.google.common.base.Preconditions.checkArgument(r0)
            r0 = r1
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            android.graphics.Bitmap r14 = r0.getBitmap()
            int r13 = r2.A00
            r0 = 0
            if (r13 > 0) goto L_0x01ed
            r0 = 1
        L_0x01ed:
            if (r0 == 0) goto L_0x02b6
            android.graphics.drawable.BitmapDrawable r2 = new android.graphics.drawable.BitmapDrawable
            android.content.res.Resources r0 = r12.getResources()
            r2.<init>(r0, r14)
        L_0x01f8:
            if (r2 != 0) goto L_0x01fb
            r2 = r1
        L_0x01fb:
            r3.A37(r2)
            if (r16 == 0) goto L_0x02ae
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A00
            int r0 = r0.AzK()
        L_0x0206:
            r3.A33(r0)
            android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.CENTER_INSIDE
            java.lang.Object r0 = r3.A00
            X.1Kt r0 = (X.C22291Kt) r0
            r0.A02 = r1
            java.lang.String r1 = r5.A04
            r0 = 32
            if (r1 != 0) goto L_0x0219
            r0 = 56
        L_0x0219:
            float r0 = (float) r0
            r3.A1z(r0)
            java.lang.String r1 = r5.A04
            r0 = 32
            if (r1 != 0) goto L_0x0225
            r0 = 56
        L_0x0225:
            float r0 = (float) r0
            r3.A1p(r0)
            X.0wO r0 = r5.A02
            java.lang.String r1 = "tooltip_tab_"
            java.lang.String r0 = r0.name()
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r3.A2o(r0)
            java.lang.Object r0 = r3.A00
            X.1Kt r0 = (X.C22291Kt) r0
            r4.A3A(r0)
            java.lang.String r0 = r5.A04
            if (r0 != 0) goto L_0x0252
            r0 = 0
        L_0x0244:
            r4.A3A(r0)
            X.11D r1 = r4.A00
            r0 = r17
            r0.add(r1)
            int r6 = r6 + 1
            goto L_0x0176
        L_0x0252:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r3 = X.C22331Kx.A00(r11)
            X.10G r1 = X.AnonymousClass10G.TOP
            r0 = -1065353216(0xffffffffc0800000, float:-4.0)
            r3.A2X(r1, r0)
            java.lang.String r0 = r5.A04
            r3.A3i(r0)
            X.1I7 r0 = X.AnonymousClass1I7.MEDIUM
            int r0 = r0.B5k()
            float r2 = (float) r0
            java.lang.Object r1 = r3.A02
            X.1Kx r1 = (X.C22331Kx) r1
            X.0z4 r0 = r3.A02
            int r0 = r0.A01(r2)
            r1.A0I = r0
            X.10M r1 = X.AnonymousClass10M.ROBOTO_REGULAR
            android.content.Context r0 = r11.A09
            android.graphics.Typeface r0 = r1.A00(r0)
            java.lang.Object r1 = r3.A02
            X.1Kx r1 = (X.C22331Kx) r1
            r1.A0L = r0
            r1.A0V = r9
            android.text.TextUtils$TruncateAt r0 = android.text.TextUtils.TruncateAt.END
            r1.A0N = r0
            if (r16 == 0) goto L_0x02a7
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A00
            int r1 = r0.AzK()
        L_0x0291:
            java.lang.Object r0 = r3.A02
            X.1Kx r0 = (X.C22331Kx) r0
            r0.A0H = r1
            java.lang.Object r2 = r3.A00
            java.util.BitSet r2 = (java.util.BitSet) r2
            java.lang.Object r1 = r3.A01
            java.lang.String[] r1 = (java.lang.String[]) r1
            X.AnonymousClass11F.A0C(r9, r2, r1)
            java.lang.Object r0 = r3.A02
            X.1Kx r0 = (X.C22331Kx) r0
            goto L_0x0244
        L_0x02a7:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A00
            int r1 = r0.B7f()
            goto L_0x0291
        L_0x02ae:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A00
            int r0 = r0.B7f()
            goto L_0x0206
        L_0x02b6:
            int r0 = r14.getDensity()
            if (r0 != 0) goto L_0x02cd
            r0 = 1065353216(0x3f800000, float:1.0)
        L_0x02be:
            X.10O r13 = r2.A01
            int r13 = r13.ordinal()
            switch(r13) {
                case 0: goto L_0x0318;
                case 1: goto L_0x0309;
                case 2: goto L_0x02fe;
                case 3: goto L_0x02e6;
                case 4: goto L_0x02df;
                default: goto L_0x02c7;
            }
        L_0x02c7:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x02cd:
            android.content.res.Resources r0 = r12.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r0 = r0.densityDpi
            float r0 = (float) r0
            int r13 = r14.getDensity()
            float r13 = (float) r13
            float r0 = r0 / r13
            goto L_0x02be
        L_0x02df:
            com.facebook.mig.scheme.interfaces.MigColorScheme r13 = r10.A00
            int r21 = r13.AoD()
            goto L_0x02ec
        L_0x02e6:
            com.facebook.mig.scheme.interfaces.MigColorScheme r13 = r10.A00
            int r21 = r13.B0I()
        L_0x02ec:
            r18 = r12
            r19 = r14
            r20 = r0
            r22 = r24
            r23 = r15
            r24 = r2
            android.graphics.drawable.Drawable r2 = A02(r18, r19, r20, r21, r22, r23, r24)
            goto L_0x01f8
        L_0x02fe:
            com.facebook.mig.scheme.interfaces.MigColorScheme r13 = r10.A00
            int r21 = r13.Abs()
            int r22 = r13.AoD()
            goto L_0x0320
        L_0x0309:
            com.facebook.mig.scheme.interfaces.MigColorScheme r13 = r10.A00
            int r21 = r13.B0I()
            r22 = -1
            int r13 = r2.A00
            java.lang.String r23 = X.C75183jN.A01(r12, r13)
            goto L_0x0326
        L_0x0318:
            com.facebook.mig.scheme.interfaces.MigColorScheme r13 = r10.A00
            int r21 = r13.B0I()
            r22 = -1
        L_0x0320:
            int r13 = r2.A00
            java.lang.String r23 = X.C75183jN.A00(r12, r13)
        L_0x0326:
            r18 = r12
            r19 = r14
            r20 = r0
            r25 = r15
            r26 = r2
            android.graphics.drawable.Drawable r2 = A03(r18, r19, r20, r21, r22, r23, r24, r25, r26)
            goto L_0x01f8
        L_0x0336:
            int r15 = r13.A03
            goto L_0x01db
        L_0x033a:
            int r0 = r13.A02
            r24 = r0
            goto L_0x01d3
        L_0x0340:
            int r0 = r13.A02
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r1) goto L_0x01bf
            int r0 = r13.A03
            goto L_0x01bc
        L_0x034a:
            com.google.common.collect.ImmutableList r0 = r17.build()
            X.0zR r0 = A04(r11, r0)
            r7.A34(r0)
            X.1LC r0 = X.AnonymousClass1LC.A00
            r7.A2k(r0)
            X.10G r1 = X.AnonymousClass10G.ALL
            r0 = 0
            r7.A2b(r1, r0)
            java.lang.String r0 = "wrapper"
            r7.A2o(r0)
            X.1Ix r0 = r7.A31()
            r8.A3A(r0)
            r0 = 11
            java.lang.String r0 = X.ECX.$const$string(r0)
            r8.A2o(r0)
            X.11D r2 = r8.A00
            r0 = 8
            X.1we r1 = X.AnonymousClass11D.A00(r11)
            X.AnonymousClass1IA.A01(r11, r1, r0)
            r1.A3A(r2)
            java.lang.String r0 = "shadow_container"
            r1.A2o(r0)
            X.11D r0 = r1.A00
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C181010e.A0O(X.0p4):X.0zR");
    }

    public static C17770zR A04(AnonymousClass0p4 r5, ImmutableList immutableList) {
        C37941wd A002 = C16980y8.A00(r5);
        A002.A2o("row");
        A002.A3E(C14950uP.CENTER);
        A002.A3D(C14940uO.CENTER);
        A002.A1p(56.0f);
        float size = 100.0f / ((float) immutableList.size());
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            C32021kz A003 = C21841Ix.A00(r5);
            A003.A20(size);
            A003.A34((C17770zR) it.next());
            A002.A34(A003.A31());
        }
        return A002.A31();
    }
}
