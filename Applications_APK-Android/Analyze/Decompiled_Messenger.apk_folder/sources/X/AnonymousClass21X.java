package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.payments.p2p.service.model.transactions.SendPaymentMessageParams;

/* renamed from: X.21X  reason: invalid class name */
public final class AnonymousClass21X implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new SendPaymentMessageParams(parcel);
    }

    public Object[] newArray(int i) {
        return new SendPaymentMessageParams[i];
    }
}
