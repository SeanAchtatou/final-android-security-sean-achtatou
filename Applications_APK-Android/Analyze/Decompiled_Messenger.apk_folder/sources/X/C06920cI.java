package X;

import com.facebook.analytics.AnalyticsClientModule;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0cI  reason: invalid class name and case insensitive filesystem */
public final class C06920cI extends AnonymousClass0UV {
    public static final DeprecatedAnalyticsLogger A00(AnonymousClass1XY r0) {
        return AnalyticsClientModule.A02(r0);
    }
}
