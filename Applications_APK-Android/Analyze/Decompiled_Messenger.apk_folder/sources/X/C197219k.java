package X;

import android.graphics.Rect;
import android.view.View;

/* renamed from: X.19k  reason: invalid class name and case insensitive filesystem */
public abstract class C197219k {
    public int A00 = Integer.MIN_VALUE;
    public final Rect A01 = new Rect();
    public final AnonymousClass19T A02;

    public int A01() {
        return ((C197319l) this).A02.A01;
    }

    public int A02() {
        AnonymousClass19T r0 = ((C197319l) this).A02;
        return r0.A01 - r0.A0e();
    }

    public int A03() {
        return ((C197319l) this).A02.A0e();
    }

    public int A04() {
        return ((C197319l) this).A02.A02;
    }

    public int A05() {
        return ((C197319l) this).A02.A05;
    }

    public int A06() {
        return ((C197319l) this).A02.A0h();
    }

    public int A07() {
        C197319l r2 = (C197319l) this;
        AnonymousClass19T r0 = r2.A02;
        return (r0.A01 - r0.A0h()) - r2.A02.A0e();
    }

    public int A08(View view) {
        return ((C197319l) this).A02.A0i(view) + ((AnonymousClass1R7) view.getLayoutParams()).bottomMargin;
    }

    public int A09(View view) {
        AnonymousClass1R7 r2 = (AnonymousClass1R7) view.getLayoutParams();
        return AnonymousClass19T.A0J(view) + r2.topMargin + r2.bottomMargin;
    }

    public int A0A(View view) {
        AnonymousClass1R7 r2 = (AnonymousClass1R7) view.getLayoutParams();
        return AnonymousClass19T.A0K(view) + r2.leftMargin + r2.rightMargin;
    }

    public int A0B(View view) {
        return ((C197319l) this).A02.A0l(view) - ((AnonymousClass1R7) view.getLayoutParams()).topMargin;
    }

    public int A0C(View view) {
        C197319l r3 = (C197319l) this;
        r3.A02.A1D(view, true, r3.A01);
        return r3.A01.bottom;
    }

    public int A0D(View view) {
        C197319l r3 = (C197319l) this;
        r3.A02.A1D(view, true, r3.A01);
        return r3.A01.top;
    }

    public void A0E(int i) {
        ((C197319l) this).A02.A1i(i);
    }

    public static C197219k A00(AnonymousClass19T r1, int i) {
        if (i == 0) {
            return new AnonymousClass3D8(r1);
        }
        if (i == 1) {
            return new C197319l(r1);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    public C197219k(AnonymousClass19T r2) {
        this.A02 = r2;
    }
}
