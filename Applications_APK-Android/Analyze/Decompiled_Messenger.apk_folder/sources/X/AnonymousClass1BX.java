package X;

/* renamed from: X.1BX  reason: invalid class name */
public final class AnonymousClass1BX {
    private AnonymousClass0UN A00;

    public static final AnonymousClass1BX A00(AnonymousClass1XY r1) {
        return new AnonymousClass1BX(r1);
    }

    public boolean A01() {
        int i = AnonymousClass1Y3.BAo;
        AnonymousClass0UN r1 = this.A00;
        if (!((Boolean) AnonymousClass1XX.A02(0, i, r1)).booleanValue() || !((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r1)).Aer(283497906310122L, AnonymousClass0XE.A07)) {
            return false;
        }
        return true;
    }

    public AnonymousClass1BX(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
