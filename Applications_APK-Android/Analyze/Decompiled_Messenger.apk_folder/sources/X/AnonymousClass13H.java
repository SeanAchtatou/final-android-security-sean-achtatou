package X;

import java.util.Map;

/* renamed from: X.13H  reason: invalid class name */
public final class AnonymousClass13H implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.navigation.home.SurfacePreloadChoreographer$1";
    public final /* synthetic */ AnonymousClass13G A00;
    public final /* synthetic */ C13260r5 A01;

    public AnonymousClass13H(C13260r5 r1, AnonymousClass13G r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void run() {
        AnonymousClass13G r0 = this.A00;
        r0.B53();
        r0.AXh();
        this.A01.A01.remove(this.A00.B53());
        C13260r5 r7 = this.A01;
        AnonymousClass13G r2 = this.A00;
        String B53 = r2.B53();
        if (!r7.A02.containsKey(B53)) {
            Map map = r7.A02;
            String B532 = r2.B53();
            map.put(B53, ((AnonymousClass0WP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8a, r7.A00)).CIG(AnonymousClass08S.A0J(B532, "_prerender"), new C855744a(r7, B532, r2), AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, AnonymousClass07B.A00));
        }
    }
}
