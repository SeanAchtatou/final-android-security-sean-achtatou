package X;

/* renamed from: X.0nu  reason: invalid class name and case insensitive filesystem */
public class C11760nu extends C11770nv {
    private final String NAME = "GuavaModule";

    public String getModuleName() {
        return "GuavaModule";
    }

    public void setupModule(C11800ny r2) {
        r2.addDeserializers(new C11810nz());
        r2.addSerializers(new C11840o2());
        r2.addTypeModifier(new C11870o5());
        r2.addBeanSerializerModifier(new C11900o8());
    }

    public C11780nw version() {
        return C1926290l.instance._version;
    }
}
