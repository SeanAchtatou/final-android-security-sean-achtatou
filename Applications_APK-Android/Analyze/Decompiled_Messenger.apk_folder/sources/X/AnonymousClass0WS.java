package X;

/* renamed from: X.0WS  reason: invalid class name */
public final class AnonymousClass0WS {
    public int A00 = -1;
    public final C001500z A01;
    public final AnonymousClass1YI A02;
    public final C001500z[] A03;
    public final AnonymousClass0US[] A04;
    public final Integer[] A05;
    public final boolean[] A06;
    public final boolean[] A07;

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0047, code lost:
        if (r2 == null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004c, code lost:
        if (r1 == false) goto L_0x004e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.AnonymousClass0US A00() {
        /*
            r9 = this;
            monitor-enter(r9)
            r8 = r9
            monitor-enter(r8)     // Catch:{ all -> 0x0070 }
            X.0US[] r0 = r9.A04     // Catch:{ all -> 0x006d }
            int r4 = r0.length     // Catch:{ all -> 0x006d }
            int r0 = r9.A00     // Catch:{ all -> 0x006d }
            if (r0 < r4) goto L_0x000b
            goto L_0x005c
        L_0x000b:
            int r0 = r9.A00     // Catch:{ all -> 0x006d }
            int r0 = r0 + 1
            r9.A00 = r0     // Catch:{ all -> 0x006d }
            if (r0 >= r4) goto L_0x005c
            r7 = r9
            monitor-enter(r7)     // Catch:{ all -> 0x006d }
            r6 = r9
            monitor-enter(r6)     // Catch:{ all -> 0x0059 }
            java.lang.Integer[] r0 = r9.A05     // Catch:{ all -> 0x0053 }
            int r5 = r9.A00     // Catch:{ all -> 0x0053 }
            r0 = r0[r5]     // Catch:{ all -> 0x0053 }
            r3 = 1
            if (r0 == 0) goto L_0x0037
            X.1YI r2 = r9.A02     // Catch:{ all -> 0x0053 }
            int r1 = r0.intValue()     // Catch:{ all -> 0x0053 }
            boolean[] r0 = r9.A06     // Catch:{ all -> 0x0053 }
            boolean r0 = r0[r5]     // Catch:{ all -> 0x0053 }
            boolean r2 = r2.AbO(r1, r0)     // Catch:{ all -> 0x0053 }
            boolean[] r1 = r9.A07     // Catch:{ all -> 0x0053 }
            int r0 = r9.A00     // Catch:{ all -> 0x0053 }
            boolean r0 = r1[r0]     // Catch:{ all -> 0x0053 }
            if (r0 != r2) goto L_0x0037
            r3 = 0
        L_0x0037:
            monitor-exit(r6)     // Catch:{ all -> 0x0059 }
            if (r3 == 0) goto L_0x004e
            r3 = r9
            monitor-enter(r3)     // Catch:{ all -> 0x0059 }
            X.00z[] r1 = r9.A03     // Catch:{ all -> 0x0056 }
            int r0 = r9.A00     // Catch:{ all -> 0x0056 }
            r2 = r1[r0]     // Catch:{ all -> 0x0056 }
            X.00z r0 = r9.A01     // Catch:{ all -> 0x0056 }
            if (r2 == r0) goto L_0x0049
            r1 = 0
            if (r2 != 0) goto L_0x004a
        L_0x0049:
            r1 = 1
        L_0x004a:
            monitor-exit(r3)     // Catch:{ all -> 0x0059 }
            r0 = 0
            if (r1 != 0) goto L_0x004f
        L_0x004e:
            r0 = 1
        L_0x004f:
            monitor-exit(r7)     // Catch:{ all -> 0x006d }
            if (r0 == 0) goto L_0x005c
            goto L_0x000b
        L_0x0053:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0059 }
            goto L_0x0058
        L_0x0056:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0059 }
        L_0x0058:
            throw r0     // Catch:{ all -> 0x0059 }
        L_0x0059:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x006d }
            throw r0     // Catch:{ all -> 0x006d }
        L_0x005c:
            monitor-exit(r8)     // Catch:{ all -> 0x0070 }
            int r3 = r9.A00     // Catch:{ all -> 0x0070 }
            X.0US[] r2 = r9.A04     // Catch:{ all -> 0x0070 }
            int r0 = r2.length     // Catch:{ all -> 0x0070 }
            r1 = 0
            if (r3 < r0) goto L_0x0067
            monitor-exit(r9)
            return r1
        L_0x0067:
            r0 = r2[r3]     // Catch:{ all -> 0x0070 }
            r2[r3] = r1     // Catch:{ all -> 0x0070 }
            monitor-exit(r9)
            return r0
        L_0x006d:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0070 }
            throw r0     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WS.A00():X.0US");
    }

    public AnonymousClass0WS(AnonymousClass0US[] r3, C001500z[] r4, C001500z r5, Integer[] numArr, boolean[] zArr, boolean[] zArr2, AnonymousClass1YI r9) {
        this.A04 = r3;
        this.A03 = r4;
        this.A01 = r5;
        this.A05 = numArr;
        this.A06 = zArr;
        this.A07 = zArr2;
        int length = r3.length;
        if (length == numArr.length && length == zArr.length && length == zArr2.length && length == r4.length) {
            this.A02 = r9;
            return;
        }
        throw new RuntimeException("length of arrays does not match up!");
    }
}
