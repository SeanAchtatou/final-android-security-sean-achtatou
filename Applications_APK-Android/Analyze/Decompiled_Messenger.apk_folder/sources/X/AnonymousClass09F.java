package X;

import com.facebook.profilo.ipc.TraceContext;

/* renamed from: X.09F  reason: invalid class name */
public abstract class AnonymousClass09F extends C000900o {
    public void A09(TraceContext traceContext, C004505k r2) {
    }

    public int getSupportedProviders() {
        return -1;
    }

    public int getTracingProviders() {
        return 0;
    }

    public void logOnTraceEnd(TraceContext traceContext, C004505k r2) {
    }

    public void A07(TraceContext traceContext, C004505k r4) {
        if (traceContext.A00 != 2) {
            logOnTraceEnd(traceContext, r4);
        }
    }

    public void disable() {
        C000700l.A09(-1885709404, C000700l.A03(-507039351));
    }

    public void enable() {
        C000700l.A09(286608778, C000700l.A03(1361811259));
    }

    public void onTraceStarted(TraceContext traceContext, C004505k r2) {
        A09(traceContext, r2);
    }

    public AnonymousClass09F() {
        this(null);
    }

    public AnonymousClass09F(String str) {
        super(str);
    }
}
