package X;

import com.google.common.base.Preconditions;

/* renamed from: X.1zz  reason: invalid class name and case insensitive filesystem */
public final class C40021zz {
    public AnonymousClass0UN A00;
    public final AnonymousClass1Y7 A01;
    public final AnonymousClass1Y7 A02;

    public C40021zz(AnonymousClass1XY r5, AnonymousClass1Y7 r6, int i, long j) {
        this.A00 = new AnonymousClass0UN(2, r5);
        Preconditions.checkNotNull(r6);
        this.A01 = (AnonymousClass1Y7) r6.A09("impression_count");
        this.A02 = (AnonymousClass1Y7) r6.A09("last_impression_time");
        boolean z = true;
        Preconditions.checkArgument(i >= 0);
        Preconditions.checkArgument(j < 0 ? false : z);
    }
}
