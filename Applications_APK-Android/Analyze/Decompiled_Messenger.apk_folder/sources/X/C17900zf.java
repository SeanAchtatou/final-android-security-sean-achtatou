package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0zf  reason: invalid class name and case insensitive filesystem */
public final class C17900zf extends AnonymousClass0UV {
    private static volatile C17910zg A00;

    public static final C17910zg A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C17910zg.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C17910zg();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
