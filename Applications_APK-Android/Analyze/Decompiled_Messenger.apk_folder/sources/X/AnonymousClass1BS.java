package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threads.ThreadSummary;

@UserScoped
/* renamed from: X.1BS  reason: invalid class name */
public final class AnonymousClass1BS {
    private static C05540Zi A03;
    public final C17270yd A00;
    public final C04310Tq A01;
    private final C26681bq A02;

    public static final AnonymousClass1BS A00(AnonymousClass1XY r4) {
        AnonymousClass1BS r0;
        synchronized (AnonymousClass1BS.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new AnonymousClass1BS((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (AnonymousClass1BS) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public boolean A01(ThreadSummary threadSummary) {
        ThreadSummary A08 = this.A02.A08(threadSummary.A0S);
        if (A08 == null) {
            return threadSummary.A0B();
        }
        return A08.A0B();
    }

    private AnonymousClass1BS(AnonymousClass1XY r2) {
        this.A02 = C26681bq.A00(r2);
        this.A00 = C17270yd.A00(r2);
        this.A01 = AnonymousClass0XJ.A0I(r2);
    }
}
