package X;

/* renamed from: X.1xA  reason: invalid class name and case insensitive filesystem */
public final class C38271xA {
    public final long A00;
    public final AnonymousClass9FN A01;
    public final AnonymousClass9GS A02;

    public C38271xA() {
        this.A01 = null;
        this.A02 = null;
        this.A00 = -1;
    }

    public C38271xA(AnonymousClass9FN r3) {
        this.A01 = r3;
        this.A02 = null;
        this.A00 = -1;
    }

    public C38271xA(AnonymousClass9FN r1, AnonymousClass9GS r2, long j) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = j;
    }
}
