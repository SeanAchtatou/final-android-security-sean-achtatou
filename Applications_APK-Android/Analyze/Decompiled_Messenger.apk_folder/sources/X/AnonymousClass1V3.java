package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.1V3  reason: invalid class name */
public final class AnonymousClass1V3 implements FilenameFilter {
    public final /* synthetic */ File A00;

    public AnonymousClass1V3(File file) {
        this.A00 = file;
    }

    public boolean accept(File file, String str) {
        return str.startsWith(this.A00.getName());
    }
}
