package X;

import java.util.Comparator;

/* renamed from: X.178  reason: invalid class name */
public final class AnonymousClass178 implements Comparator {
    public int compare(Object obj, Object obj2) {
        AnonymousClass191 r4 = (AnonymousClass191) obj;
        AnonymousClass191 r5 = (AnonymousClass191) obj2;
        if (!r4.hasNext() && !r5.hasNext()) {
            return 0;
        }
        if (!r4.hasNext()) {
            return 1;
        }
        if (!r5.hasNext()) {
            return -1;
        }
        return AnonymousClass170.A00.compare(r4.peek(), r5.peek());
    }
}
