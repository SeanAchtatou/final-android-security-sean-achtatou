package X;

/* renamed from: X.0Z7  reason: invalid class name */
public final class AnonymousClass0Z7 implements AnonymousClass0Z1 {
    private AnonymousClass0UN A00;

    public String Amj() {
        return AnonymousClass24B.$const$string(AnonymousClass1Y3.A7t);
    }

    public static final AnonymousClass0Z7 A00(AnonymousClass1XY r1) {
        return new AnonymousClass0Z7(r1);
    }

    public String getCustomData(Throwable th) {
        return String.valueOf(((AnonymousClass0Z9) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AAd, this.A00)).A06());
    }

    private AnonymousClass0Z7(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
