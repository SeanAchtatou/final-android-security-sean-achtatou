package X;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.LithoView;

/* renamed from: X.10d  reason: invalid class name and case insensitive filesystem */
public final class C180910d implements AnonymousClass1M8 {
    public String getName() {
        return "y";
    }

    public float Ab1(C17730zN r2) {
        return (float) r2.A08.top;
    }

    public float Ab2(Object obj) {
        if (obj instanceof LithoView) {
            return ((LithoView) obj).getY();
        }
        if (obj instanceof View) {
            return C17700zK.A00((View) obj, false);
        }
        if (obj instanceof Drawable) {
            Drawable drawable = (Drawable) obj;
            return C17700zK.A00(C17700zK.A01(drawable), false) + ((float) drawable.getBounds().top);
        }
        throw new UnsupportedOperationException("Getting Y from unsupported mount content: " + obj);
    }

    public void C3Y(Object obj) {
        if (obj instanceof View) {
            ((View) obj).setTranslationY(0.0f);
        }
    }

    public void C5f(Object obj, float f) {
        if (obj instanceof LithoView) {
            ((View) obj).setY(f);
        } else if (obj instanceof View) {
            View view = (View) obj;
            view.setY(f - C17700zK.A00((View) view.getParent(), false));
        } else if (obj instanceof Drawable) {
            Drawable drawable = (Drawable) obj;
            float A00 = C17700zK.A00(C17700zK.A01(drawable), false);
            int i = drawable.getBounds().left;
            int i2 = (int) (f - A00);
            Rect bounds = drawable.getBounds();
            drawable.setBounds(i, i2, bounds.width() + i, bounds.height() + i2);
        } else {
            throw new UnsupportedOperationException("Setting Y on unsupported mount content: " + obj);
        }
    }
}
