package X;

import android.util.Pair;
import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.085  reason: invalid class name */
public final class AnonymousClass085 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$3";
    public final /* synthetic */ Pair A00;
    public final /* synthetic */ AnonymousClass0AH A01;
    public final /* synthetic */ Boolean A02;
    public final /* synthetic */ Integer A03;

    public AnonymousClass085(AnonymousClass0AH r1, Pair pair, Boolean bool, Integer num) {
        this.A01 = r1;
        this.A00 = pair;
        this.A02 = bool;
        this.A03 = num;
    }

    public void run() {
        List list;
        try {
            Pair pair = this.A00;
            List<SubscribeTopic> list2 = null;
            if (pair != null) {
                list = (List) pair.first;
            } else {
                list = null;
            }
            if (pair != null) {
                list2 = (List) pair.second;
            }
            ArrayList arrayList = new ArrayList();
            if (list2 != null) {
                for (SubscribeTopic subscribeTopic : list2) {
                    arrayList.add(subscribeTopic.A01);
                }
            }
            AnonymousClass0AH r1 = this.A01;
            byte[] AUR = r1.A0O.AUR(r1.A0V(this.A02), this.A02, this.A03, list, arrayList);
            if (AUR != null) {
                int A08 = this.A01.A08("/t_fs", AUR, AnonymousClass07B.A01, new AnonymousClass081(this, list, list2));
                if (A08 >= 0) {
                    if (list != null) {
                        this.A01.A0p.BsK(list, A08);
                    }
                    if (list2 != null) {
                        this.A01.A0p.BsL(list2, A08);
                        return;
                    }
                    return;
                }
                this.A01.A0X(this.A02);
            }
        } catch (AnonymousClass0CI unused) {
        }
    }
}
