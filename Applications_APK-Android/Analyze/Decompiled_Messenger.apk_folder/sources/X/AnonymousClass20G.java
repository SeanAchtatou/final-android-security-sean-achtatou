package X;

import com.facebook.messaging.integrity.frx.model.FeedbackSubmissionResult;

/* renamed from: X.20G  reason: invalid class name */
public final class AnonymousClass20G {
    public final /* synthetic */ FeedbackSubmissionResult A00;
    public final /* synthetic */ C140176gI A01;

    public AnonymousClass20G(C140176gI r1, FeedbackSubmissionResult feedbackSubmissionResult) {
        this.A01 = r1;
        this.A00 = feedbackSubmissionResult;
    }
}
