package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1vI  reason: invalid class name */
public final class AnonymousClass1vI extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C55672oT A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 3)
    public boolean A03;

    public AnonymousClass1vI(Context context) {
        super("M4DataSaverPreferenceLayout");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
