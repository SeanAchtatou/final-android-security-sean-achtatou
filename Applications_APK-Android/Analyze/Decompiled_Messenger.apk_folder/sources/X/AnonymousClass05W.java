package X;

import android.content.Context;
import android.view.WindowManager;
import java.util.concurrent.TimeUnit;

/* renamed from: X.05W  reason: invalid class name */
public final class AnonymousClass05W {
    public static AnonymousClass05W A01;
    private static final long A02 = TimeUnit.SECONDS.toNanos(1);
    private long A00 = 0;

    public long A00(Context context) {
        long j = this.A00;
        if (j > 0) {
            return j;
        }
        double refreshRate = (double) ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRefreshRate();
        if (refreshRate < 0.0d) {
            refreshRate = 60.0d;
        } else if (refreshRate < 30.0d) {
            refreshRate = 30.0d;
        } else if (refreshRate > 240.0d) {
            refreshRate = 240.0d;
        }
        long round = Math.round(((double) A02) / refreshRate);
        this.A00 = round;
        return round;
    }
}
