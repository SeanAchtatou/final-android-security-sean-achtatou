package X;

import android.os.SystemClock;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.anr.ANRDataProvider;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.common.dextricks.DexLibLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.0ic  reason: invalid class name and case insensitive filesystem */
public final class C09740ic extends ANRDataProvider implements C09750id {
    private long A00;
    private long A01;
    private AnonymousClass0UN A02;
    private boolean A03;
    private final AnonymousClass01F A04 = AnonymousClass01F.A00();
    private final AnonymousClass09P A05;
    private final AnonymousClass1YI A06;
    private final C25051Yd A07;
    private final Object A08 = new Object();
    private final List A09 = new ArrayList();
    private final List A0A = new ArrayList();

    public boolean shouldUploadSystemANRTraces() {
        return true;
    }

    public static final C09740ic A00(AnonymousClass1XY r1) {
        return new C09740ic(r1);
    }

    public void Be0(C189988sO r10) {
        synchronized (this.A08) {
            this.A03 = true;
            this.A09.add(r10);
            long At0 = this.A07.At0(563650033353228L);
            long uptimeMillis = SystemClock.uptimeMillis() - At0;
            if (At0 != -1 && uptimeMillis > this.A01) {
                ArrayList arrayList = new ArrayList();
                for (C189988sO r3 : this.A09) {
                    if (r3.A00 >= uptimeMillis) {
                        break;
                    }
                    arrayList.add(r3);
                }
                this.A09.removeAll(arrayList);
                ArrayList arrayList2 = new ArrayList();
                for (C189968sM r32 : this.A0A) {
                    if (r32.A00 >= uptimeMillis) {
                        break;
                    }
                    arrayList2.add(r32);
                }
                this.A0A.removeAll(arrayList2);
                this.A01 = SystemClock.uptimeMillis();
            }
        }
    }

    public void Be3(C189968sM r6) {
        synchronized (this.A08) {
            this.A03 = true;
            this.A0A.add(r6);
            this.A00 = Math.max(r6.A00, this.A00);
        }
    }

    public int detectionIntervalTimeMs() {
        return this.A07.AqL(566690870658793L, 500);
    }

    public int detectorToUse() {
        return (int) this.A07.At0(566690870003427L);
    }

    public int getExpirationTimeoutOnMainThreadUnblocked() {
        return (int) this.A07.At0(566690870265573L);
    }

    public int getForegroundCheckPeriod() {
        return (int) this.A07.At0(566690870331110L);
    }

    public int getMaxNumberOfProcessMonitorChecksAfterError() {
        return (int) this.A07.At0(566690870527719L);
    }

    public int getMaxNumberOfProcessMonitorChecksBeforeError() {
        return (int) this.A07.At0(566690870593256L);
    }

    public int getRecoveryTimeout() {
        return (int) this.A07.At0(566690870068964L);
    }

    public void provideLooperMonitorInfo() {
        ArrayList<BZY> arrayList;
        boolean Aem = this.A07.Aem(282978215463018L);
        C23164BZa bZa = C23164BZa.A08;
        boolean z = false;
        if (bZa != null) {
            z = true;
        }
        if (z && Aem) {
            if (bZa == null) {
                C23164BZa.A08 = new C23164BZa();
            }
            C23164BZa bZa2 = C23164BZa.A08;
            synchronized (bZa2.A03) {
                arrayList = new ArrayList<>(bZa2.A03);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(AnonymousClass08S.A09("num_looper_monitor_stack_traces: ", arrayList.size()));
            for (BZY bzy : arrayList) {
                sb.append(bzy.toString());
                sb.append("\n");
                sb.append("\n");
            }
            ErrorReporter.putCustomData(ErrorReportingConstants.ANR_LOOPER_MONITOR_STACKS, sb.toString());
        }
    }

    public void provideLooperProfileInfo() {
        synchronized (this.A08) {
            if (this.A03) {
                long At0 = this.A07.At0(563650033353228L);
                long uptimeMillis = SystemClock.uptimeMillis();
                long j = uptimeMillis - At0;
                StringBuilder sb = new StringBuilder();
                sb.append("Long running messages:\n");
                for (C189988sO r6 : this.A09) {
                    if (At0 == -1 || r6.A00 > j) {
                        sb.append(r6.A01);
                        sb.append("\n");
                    }
                }
                sb.append("Stall traces\n");
                for (C189968sM r62 : this.A0A) {
                    if (At0 == -1 || r62.A00 > j) {
                        sb.append("Message: ");
                        sb.append(r62.A01);
                        sb.append(AnonymousClass08S.A0Y("\nHAS_RECOVERED: ", r62.A03, "\n"));
                        sb.append("\nTRACE:\n");
                        String A002 = r62.A00();
                        if (A002 != null) {
                            sb.append(A002);
                        }
                    }
                }
                ErrorReporter.putCustomData(ErrorReportingConstants.ANR_LOOPER_PROFILER_DATA, sb.toString());
                ErrorReporter.putCustomData(ErrorReportingConstants.ANR_LOOPER_PROFILER_TIME_SINCE_LAST_STALL, String.valueOf(uptimeMillis - this.A00));
                this.A09.clear();
                this.A0A.clear();
                this.A01 = uptimeMillis;
            }
        }
    }

    public void provideStats() {
        String[] A022 = AnonymousClass8TZ.A02("/proc/self/stat");
        String str = "N/A";
        String str2 = str;
        if (A022 != null) {
            str2 = A022[2];
        }
        ErrorReporter.putCustomData(ErrorReportingConstants.ANR_PROC_STAT_STATE_TAG, str2);
        if (A022 != null) {
            str = Arrays.toString(A022);
        }
        ErrorReporter.putCustomData(ErrorReportingConstants.ANR_PROC_STAT_TAG, str);
    }

    public void reportSoftError(String str, Throwable th) {
        this.A05.softReport(str, th);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001d, code lost:
        if (r0 != false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldANRDetectorRun() {
        /*
            r3 = this;
            int r1 = X.AnonymousClass1Y3.A6S
            X.0UN r0 = r3.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            android.content.Context r2 = (android.content.Context) r2
            boolean r0 = r3.shouldCollectAndUploadANRReports()
            if (r0 != 0) goto L_0x0023
            X.01F r1 = r3.A04
            boolean r0 = r1.A0P(r2)
            if (r0 != 0) goto L_0x001f
            boolean r0 = r1.A0Q(r2)
            r1 = 0
            if (r0 == 0) goto L_0x0020
        L_0x001f:
            r1 = 1
        L_0x0020:
            r0 = 0
            if (r1 == 0) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09740ic.shouldANRDetectorRun():boolean");
    }

    public boolean shouldAvoidMutexOnSignalHandler() {
        return this.A07.Aem(285215893427715L);
    }

    public boolean shouldCleanupStateOnASLThread() {
        return this.A07.Aem(285215893493252L);
    }

    public boolean shouldCollectAndUploadANRReports() {
        return this.A06.AbO(589, true);
    }

    public boolean shouldLogOnSignalHandler() {
        return this.A07.Aem(285215893689861L);
    }

    public boolean shouldLogProcessPositionInAnrTraceFile() {
        return this.A07.Aem(285215893755398L);
    }

    public boolean shouldRecordSignalTime() {
        return this.A07.Aem(285215894017543L);
    }

    public boolean shouldReportSoftErrors() {
        return this.A07.Aem(285215894083080L);
    }

    public boolean shouldRunANRDetectorOnBrowserProcess() {
        return this.A07.Aem(285215894148617L);
    }

    public boolean shouldUseFgStateFromDetectorInASL() {
        return this.A07.Aem(285215894279690L);
    }

    private C09740ic(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(0, r3);
        this.A07 = AnonymousClass0WT.A00(r3);
        this.A06 = AnonymousClass0WA.A00(r3);
        this.A05 = C04750Wa.A01(r3);
        AnonymousClass1YA.A00(r3);
    }

    public void provideDexStatus() {
        String str;
        try {
            str = DexLibLoader.getMainDexStoreLoadInformation().toString();
        } catch (Throwable th) {
            str = th.toString();
        }
        ErrorReporter.putCustomData("mainDexStore", str);
    }
}
