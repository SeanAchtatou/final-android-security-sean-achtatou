package X;

import com.facebook.tigon.iface.TigonRequest;
import com.google.common.base.Preconditions;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import javax.inject.Singleton;
import org.apache.http.message.BasicNameValuePair;

@Singleton
/* renamed from: X.0DP  reason: invalid class name */
public final class AnonymousClass0DP implements C10810kt {
    private static volatile AnonymousClass0DP A01 = null;
    public static final String __redex_internal_original_name = "com.facebook.redex.dynamicanalysis.support.MethodStatsUploadMethod";
    private final C25921ac A00;

    public static final AnonymousClass0DP A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0DP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0DP(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public C47702Xm B0t(Object obj) {
        String str;
        AnonymousClass0DR r6 = (AnonymousClass0DR) obj;
        boolean z = false;
        if (r6 != null) {
            z = true;
        }
        Preconditions.checkArgument(z);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("device_id", this.A00.B7Z()));
        String str2 = r6.A08;
        if (str2 != null) {
            arrayList.add(new BasicNameValuePair("process_name", str2));
        }
        arrayList.add(new BasicNameValuePair("method_count", Integer.toString(r6.A02)));
        arrayList.add(new BasicNameValuePair("num_stats_per_method", Integer.toString(r6.A03)));
        arrayList.add(new BasicNameValuePair("coldstart_duration", Integer.toString(r6.A01)));
        String str3 = r6.A06;
        if (str3 != null) {
            arrayList.add(new BasicNameValuePair("coldstart_result", str3));
        }
        arrayList.add(new BasicNameValuePair("coldstart_cut_order", Integer.toString(r6.A00)));
        arrayList.add(new BasicNameValuePair("coldstart_extra", r6.A04));
        arrayList.add(new BasicNameValuePair("interaction", r6.A05));
        try {
            FileInputStream openFileInput = C02470Ey.A00.openFileInput(r6.A07);
            if (r6.A09) {
                str = "application/zstd";
            } else {
                str = "application/gzip";
            }
            C183208ec r3 = new C183208ec(openFileInput, str, r6.A07);
            AnonymousClass2U7 A002 = C47702Xm.A00();
            A002.A0B = "uploadMethodStats";
            A002.A0D = "me/dynamic_analysis_method_stats";
            A002.A0C = TigonRequest.POST;
            A002.A0H = arrayList;
            Integer num = AnonymousClass07B.A01;
            A002.A07 = num;
            A002.A05 = num;
            A002.A0G = Collections.singletonList(new AnonymousClass2U5("methodstats", r3));
            return A002.A01();
        } catch (Exception e) {
            C010708t.A0R("DYNA|MethodStatsUploadMethod", e, "Failed to open method stats file ... Nothing to upload then!");
            throw e;
        }
    }

    private AnonymousClass0DP(AnonymousClass1XY r2) {
        this.A00 = C25901aa.A00(r2);
    }

    public static final AnonymousClass0DP A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0014, code lost:
        if (r1 == false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object B1B(java.lang.Object r3, X.AnonymousClass2U8 r4) {
        /*
            r2 = this;
            r4.A04()
            com.fasterxml.jackson.databind.JsonNode r1 = r4.A01()
            java.lang.String r0 = "success"
            com.fasterxml.jackson.databind.JsonNode r0 = r1.get(r0)
            if (r0 == 0) goto L_0x0016
            boolean r1 = r0.asBoolean()
            r0 = 1
            if (r1 != 0) goto L_0x0017
        L_0x0016:
            r0 = 0
        L_0x0017:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DP.B1B(java.lang.Object, X.2U8):java.lang.Object");
    }
}
