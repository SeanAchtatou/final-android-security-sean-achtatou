package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1hq  reason: invalid class name and case insensitive filesystem */
public final class C30311hq {
    private static volatile C30311hq A01;
    public AnonymousClass0UN A00;

    public static final C30311hq A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C30311hq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C30311hq(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C30311hq(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
