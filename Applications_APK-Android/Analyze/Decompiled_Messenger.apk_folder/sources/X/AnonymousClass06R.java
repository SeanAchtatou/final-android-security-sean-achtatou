package X;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Process;
import android.widget.Toast;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.profilo.ipc.TraceContext;
import java.util.ArrayList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.06R  reason: invalid class name */
public final class AnonymousClass06R implements AnonymousClass1YQ, AnonymousClass0ZM {
    public static final String A08 = AnonymousClass08S.A09("com.facebook.profilo.CONTROL_TOGGLE.", Process.myPid());
    private static final String A09 = AnonymousClass08S.A09("com.facebook.profilo.DISMISS.", Process.myPid());
    private static volatile AnonymousClass06R A0A;
    public boolean A00 = false;
    public boolean A01 = false;
    private AnonymousClass0ZV A02;
    private AnonymousClass0ZN A03;
    private C06870cD A04;
    private AnonymousClass0UN A05;
    private boolean A06;
    private boolean A07 = true;

    private synchronized void A04() {
        AnonymousClass054 r0;
        if (this.A01) {
            C06870cD r3 = this.A04;
            if (r3 != null) {
                ((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, this.A05)).unregisterReceiver(r3);
            }
            A07(AnonymousClass1Y3.A1c);
            if (this.A00 && (r0 = AnonymousClass054.A07) != null) {
                AnonymousClass054.A05(r0, AnonymousClass04i.A00, null, 0, 0, 2);
            }
        }
    }

    private synchronized void A05() {
        if (!this.A06) {
            PendingIntent A012 = C64633Cq.A01((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, this.A05), 0, new Intent(A08), 134217728);
            PendingIntent A013 = C64633Cq.A01((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, this.A05), 0, new Intent(A09), 134217728);
            AnonymousClass0ZN r3 = new AnonymousClass0ZN((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, this.A05), null);
            r3.A0D("Performance tracing ready");
            r3.A09 = 1;
            r3.A0H = "profilo";
            r3.A0M = true;
            r3.A08(A00());
            r3.A05(17301593);
            r3.A0C = A012;
            AnonymousClass0ZN.A01(r3, 2, true);
            r3.A0G = "profilo_channel";
            r3.A0I.add(new AnonymousClass06S(this, 0, null, A012));
            r3.A0I.add(new C06010ah(17301560, "Dismiss", A013));
            this.A03 = r3;
            this.A02 = new AnonymousClass0ZV(r3);
            this.A04 = new C06870cD(A08, new AnonymousClass06T(this), A09, new AnonymousClass06V());
            A03();
            this.A06 = true;
        }
    }

    private synchronized void A06() {
        IntentFilter intentFilter = new IntentFilter(A08);
        intentFilter.addAction(A09);
        ((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, this.A05)).registerReceiver(this.A04, intentFilter);
    }

    private void A07(int i) {
        try {
            ((NotificationManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJ0, this.A05)).cancel(i);
        } catch (NullPointerException unused) {
        }
    }

    private synchronized void A0B(boolean z) {
        if (!this.A06 || this.A01 != z) {
            if (this.A01) {
                A04();
            }
            if (z) {
                synchronized (this) {
                    A05();
                    A06();
                }
                A0A(this.A00);
            }
            this.A01 = z;
        }
    }

    public synchronized void A0C(C005606a r6) {
        int i = AnonymousClass1Y3.B6q;
        AnonymousClass1Y7 r3 = C005706b.A01;
        boolean z = false;
        boolean Aep = ((FbSharedPreferences) AnonymousClass1XX.A02(1, i, this.A05)).Aep(r3, false);
        if (r6 != null) {
            z = true;
        }
        if (Aep != z) {
            C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(1, i, this.A05)).edit();
            edit.putBoolean(r3, z);
            edit.commit();
        }
    }

    public synchronized void A0D(Integer num) {
        String str;
        String str2;
        if (this.A01) {
            switch (num.intValue()) {
                case 0:
                    str = "Uploading Profilo trace";
                    str2 = "Uploading trace";
                    break;
                case 1:
                    str = "Trace upload was successful";
                    str2 = "Upload successful";
                    break;
                case 2:
                    str = "Trace upload failed";
                    str2 = "Upload failed";
                    break;
                default:
                    str = "Trace upload status unknown";
                    str2 = str;
                    break;
            }
            AnonymousClass0ZN r2 = new AnonymousClass0ZN((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, this.A05), null);
            r2.A09 = -1;
            r2.A0H = "profilo";
            r2.A05(17301589);
            r2.A0C(str);
            r2.A0G = "profilo_channel";
            r2.A0D(str2);
            A08(AnonymousClass1Y3.A1d, r2.A02());
        }
    }

    public synchronized void A0E(boolean z, TraceContext traceContext) {
        if (traceContext.A01 == AnonymousClass04i.A00 && this.A01 && this.A00 != z) {
            this.A00 = z;
            A0A(z);
        }
    }

    public String getSimpleName() {
        return "NotificationControls";
    }

    public synchronized void init() {
        ArrayList arrayList;
        int A032 = C000700l.A03(2021013962);
        C005606a r6 = AnonymousClass06Z.A00().A00;
        boolean z = false;
        boolean Aep = ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, this.A05)).Aep(C005706b.A01, false);
        if (r6 != null) {
            A0C(r6);
        } else if (Aep) {
            onSharedPreferenceChanged((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, this.A05), C005706b.A01);
        }
        if (Aep || r6 != null) {
            z = true;
        }
        A0B(z);
        AnonymousClass06Z A002 = AnonymousClass06Z.A00();
        synchronized (A002) {
            if (A002.A01 == null) {
                arrayList = new ArrayList(1);
            } else {
                arrayList = new ArrayList(A002.A01.size() + 1);
            }
            if (A002.A01 != null) {
                arrayList.addAll(A002.A01);
            }
            arrayList.add(this);
            A002.A01 = arrayList;
        }
        ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, this.A05)).C0f(C005706b.A01, this);
        C000700l.A09(1748863212, A032);
    }

    public synchronized void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r7) {
        AnonymousClass1Y7 r1 = C005706b.A01;
        if (r7.equals(r1)) {
            boolean Aep = fbSharedPreferences.Aep(r1, false);
            boolean z = false;
            if (AnonymousClass06Z.A00().A00 != null) {
                z = true;
            }
            if (Aep != z) {
                if (Aep) {
                    AnonymousClass06Z A002 = AnonymousClass06Z.A00();
                    synchronized (A002) {
                        try {
                            A002.A00 = new C005606a(false, null);
                            AnonymousClass06Z.A01(A002, A002.A00);
                        } catch (Throwable th) {
                            th = th;
                            throw th;
                        }
                    }
                } else {
                    AnonymousClass06Z A003 = AnonymousClass06Z.A00();
                    synchronized (A003) {
                        try {
                            A003.A00 = null;
                            AnonymousClass06Z.A01(A003, null);
                        } catch (Throwable th2) {
                            th = th2;
                            throw th;
                        }
                    }
                }
            }
            A0B(Aep);
        }
    }

    private Bitmap A00() {
        Drawable A002;
        int i = AnonymousClass1Y3.A1G;
        AnonymousClass0UN r1 = this.A05;
        C14290sy r4 = (C14290sy) AnonymousClass1XX.A02(3, i, r1);
        String packageName = ((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, r1)).getPackageName();
        if (packageName.startsWith("com.facebook.")) {
            ApplicationInfo A032 = r4.A03(packageName, 0);
            A002 = AnonymousClass01W.A00(r4, packageName);
            if (A032 == null || A002 == null || !AnonymousClass01W.A01(r4, A032)) {
                A002 = null;
            }
        } else {
            A002 = AnonymousClass01W.A00(r4, packageName);
        }
        if (A002 == null) {
            return BitmapFactory.decodeResource(((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, this.A05)).getResources(), 17301593);
        }
        if (A002 instanceof BitmapDrawable) {
            return ((BitmapDrawable) A002).getBitmap();
        }
        Bitmap createBitmap = Bitmap.createBitmap(A002.getIntrinsicWidth(), A002.getIntrinsicHeight(), Bitmap.Config.ARGB_4444);
        A002.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    public static final AnonymousClass0US A01(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.AUk, r1);
    }

    public static final AnonymousClass06R A02(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (AnonymousClass06R.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new AnonymousClass06R(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    private void A03() {
        if (Build.VERSION.SDK_INT >= 26) {
            ((NotificationManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJ0, this.A05)).createNotificationChannel(new NotificationChannel("profilo_channel", "Profilo", 2));
        }
    }

    private void A08(int i, Notification notification) {
        if (notification != null) {
            try {
                ((NotificationManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJ0, this.A05)).notify(i, notification);
            } catch (NullPointerException e) {
                throw e;
            }
        } else {
            throw new IllegalArgumentException("notification can't be null");
        }
    }

    public static void A09(AnonymousClass06R r10) {
        C005606a r0;
        AnonymousClass054 r4 = AnonymousClass054.A07;
        if (r4 != null && (r0 = AnonymousClass06Z.A00().A00) != null) {
            int i = AnonymousClass04i.A00;
            boolean z = r0.A01;
            int i2 = 0;
            if (z) {
                i2 = 2;
            }
            if (!r4.A0A(i, i2 | 1, null, 0)) {
                Toast.makeText((Context) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BCt, r10.A05), "Unable to start the Trace. Try again.", 0).show();
            }
        }
    }

    private void A0A(boolean z) {
        String str;
        String str2;
        String str3;
        String format;
        String str4;
        AnonymousClass054 r0 = AnonymousClass054.A07;
        if (r0 != null) {
            String[] A0C = r0.A0C();
            if (A0C != null) {
                str = A0C[A0C.length - 1];
            } else {
                str = "Not tracing";
            }
            StringBuilder sb = new StringBuilder();
            if (z) {
                str3 = "Tap to stop and upload trace";
                format = String.format("%s\n\nTrace ID: %s\n\n%s", str3, str, sb.toString());
                str2 = "Performance tracing started";
                str4 = "Profilo is weaving";
            } else {
                if (this.A07) {
                    str2 = "Performance tracing ready";
                } else {
                    str2 = "Performance tracing stopped";
                }
                str3 = "Tap to start tracing";
                format = String.format("%s\n\n%s", str3, sb.toString());
                str4 = "Profilo is primed and ready";
            }
            AnonymousClass0ZN r1 = this.A03;
            r1.A0D(str2);
            r1.A0C(str4);
            r1.A0B(str3);
            AnonymousClass0ZV r02 = this.A02;
            r02.A01(format);
            r1.A0A(r02);
            Notification A022 = r1.A02();
            this.A07 = false;
            A08(AnonymousClass1Y3.A1c, A022);
            return;
        }
        throw new IllegalStateException("TraceControl is null and we're showing a notification");
    }

    private AnonymousClass06R(AnonymousClass1XY r3) {
        this.A05 = new AnonymousClass0UN(4, r3);
    }
}
