package X;

import com.facebook.acra.ACRA;
import com.facebook.acra.PerformanceMarker;
import com.facebook.acra.anr.ANRDetectorListener;
import com.facebook.breakpad.BreakpadManager;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0Zs  reason: invalid class name and case insensitive filesystem */
public final class C05620Zs extends AnonymousClass0Wl implements ANRDetectorListener, PerformanceMarker {
    public AnonymousClass0UN A00;
    public boolean A01;
    public boolean A02;
    public final C25051Yd A03;
    private final AnonymousClass0iZ A04;
    private final C09610iB A05;
    private final QuickPerformanceLogger A06;

    public String getSimpleName() {
        return "ANRDetectorController";
    }

    public static final C05620Zs A00(AnonymousClass1XY r1) {
        return new C05620Zs(r1);
    }

    public String getLongStallTraceId() {
        AnonymousClass0iZ r5 = this.A04;
        if (!r5.A02) {
            return null;
        }
        String A002 = C004004z.A00(21364741);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r5.A00)).markerEnd(21364741, 2);
        r5.A02 = false;
        return A002;
    }

    public void markerEnd(short s) {
        this.A06.markerEnd(8192002, s);
    }

    public void markerStart() {
        this.A06.markerStart(8192002);
    }

    public void onEndANRDataCapture() {
        if (this.A06.isMarkerOn(15859714)) {
            this.A06.markerEnd(15859714, 2);
        }
        if (this.A01) {
            BreakpadManager.simulateSignalDelivery(6, "ANR Detector Controller");
        }
    }

    public void onStartANRDataCapture() {
        if (!this.A06.isMarkerOn(15859714)) {
            this.A06.markerStart(15859714);
        }
    }

    private C05620Zs(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A03 = AnonymousClass0WT.A00(r3);
        this.A06 = AnonymousClass0ZD.A03(r3);
        this.A05 = C09610iB.A00(r3);
        this.A04 = AnonymousClass0iZ.A00(r3);
    }

    public String getBlackBoxTraceId() {
        return AnonymousClass050.A01(15859713);
    }

    public void init() {
        int A032 = C000700l.A03(517366855);
        if (!ACRA.sInitialized) {
            C000700l.A09(1984878791, A032);
            return;
        }
        new C26711bt(this).start();
        ACRA.setPerformanceMarker(this);
        ACRA.setANRDetectorListener(this);
        int i = AnonymousClass1Y3.AJc;
        ACRA.setANRDataProvider((C09740ic) AnonymousClass1XX.A02(0, i, this.A00));
        C09610iB r2 = this.A05;
        if (r2.A0C) {
            r2.A0B.add((C09740ic) AnonymousClass1XX.A02(0, i, this.A00));
        }
        this.A01 = this.A03.Aem(282978215528555L);
        C000700l.A09(842449377, A032);
    }
}
