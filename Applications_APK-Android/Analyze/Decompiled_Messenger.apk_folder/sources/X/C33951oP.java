package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxTrackableItem;

/* renamed from: X.1oP  reason: invalid class name and case insensitive filesystem */
public final class C33951oP implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxTrackableItem(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxTrackableItem[i];
    }
}
