package X;

import android.os.Process;
import android.util.Log;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.ipc.TraceContext;
import java.io.File;
import java.util.Set;

/* renamed from: X.05K  reason: invalid class name */
public final class AnonymousClass05K extends C000900o {
    public static AnonymousClass05K A01;
    public static String A02;
    public static final int A03 = ProvidersRegistry.A00.A02("transient_network_data");
    public String A00 = "UNKNOWN_TRACEID";

    private AnonymousClass05K() {
        super(null);
    }

    public synchronized void A07(TraceContext traceContext, C004505k r7) {
        TraceContext traceContext2 = this.A00;
        if (traceContext2 != null && traceContext.A05 == traceContext2.A05) {
            this.A00 = "UNKNOWN_TRACEID";
        }
    }

    public synchronized void enable() {
        String str;
        int A032 = C000700l.A03(-1366642477);
        TraceContext traceContext = this.A00;
        if (traceContext != null) {
            str = AnonymousClass08S.A0P(traceContext.A09, "-", A02);
        } else {
            str = "UNKNOWN_TRACEID";
        }
        this.A00 = str;
        C000700l.A09(-1545402783, A032);
    }

    public static synchronized AnonymousClass05K A00() {
        AnonymousClass05K r0;
        synchronized (AnonymousClass05K.class) {
            if (A01 == null) {
                A01 = new AnonymousClass05K();
                A02 = AnonymousClass00M.A00().A03();
            }
            r0 = A01;
        }
        return r0;
    }

    public int getTracingProviders() {
        File file;
        TraceContext traceContext = this.A00;
        if (traceContext == null) {
            return 0;
        }
        boolean z = false;
        if (C004505k.A0F.get() != null) {
            z = true;
        }
        if (!z) {
            return 0;
        }
        C004505k A002 = C004505k.A00();
        if ((traceContext.A03 & 2) != 0) {
            return 0;
        }
        Set A032 = ProvidersRegistry.A00.A03(getSupportedProviders());
        if (A032.isEmpty()) {
            return 0;
        }
        synchronized (A002) {
            file = A002.A03.A03;
        }
        File file2 = new File(new File(file, traceContext.A09.replaceAll("[^a-zA-Z0-9\\-_.]", "_")), "extra");
        if (!file2.isDirectory() && !file2.mkdirs()) {
            Log.w("Profilo/TraceOrchestrator", "Failed to create extra data file! This could be because another process created it");
            if (!file2.exists() || !file2.isDirectory()) {
                return 0;
            }
        }
        new File(file2, A002.A0A + "-" + Process.myPid() + "-" + ((String) A032.iterator().next()));
        return 0;
    }

    public void disable() {
        C000700l.A09(408544169, C000700l.A03(-1367152439));
    }

    public int getSupportedProviders() {
        return A03;
    }
}
