package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0SB  reason: invalid class name */
public final class AnonymousClass0SB extends Enum {
    private static final /* synthetic */ AnonymousClass0SB[] A00;
    public static final AnonymousClass0SB A01;
    public static final AnonymousClass0SB A02;
    public static final AnonymousClass0SB A03;
    public static final AnonymousClass0SB A04;
    public static final AnonymousClass0SB A05;
    public static final AnonymousClass0SB A06;
    public static final AnonymousClass0SB A07;
    public static final AnonymousClass0SB A08;
    public static final AnonymousClass0SB A09;
    public static final AnonymousClass0SB A0A;
    public static final AnonymousClass0SB A0B;
    public static final AnonymousClass0SB A0C;
    public static final AnonymousClass0SB A0D;
    public static final AnonymousClass0SB A0E;
    public static final AnonymousClass0SB A0F;
    public static final AnonymousClass0SB A0G;
    public static final AnonymousClass0SB A0H;

    static {
        AnonymousClass0SB r0 = new AnonymousClass0SB("NETWORK_ERROR", 0);
        AnonymousClass0SB r02 = new AnonymousClass0SB("MQTT_ERROR", 1);
        AnonymousClass0SB r03 = new AnonymousClass0SB("FAILED_SOCKET_ERROR", 2);
        AnonymousClass0SB r04 = new AnonymousClass0SB("FAILED_SOCKET_CONNECT_ERROR", 3);
        A0D = r04;
        AnonymousClass0SB r15 = new AnonymousClass0SB("FAILED_SOCKET_CONNECT_TIMEOUT", 4);
        A0F = r15;
        AnonymousClass0SB r14 = new AnonymousClass0SB("FAILED_DNS_RESOLVE_TIMEOUT", 5);
        A09 = r14;
        AnonymousClass0SB r13 = new AnonymousClass0SB("FAILED_MQTT_CONACK_TIMEOUT", 6);
        A0C = r13;
        AnonymousClass0SB r12 = new AnonymousClass0SB("FAILED_CONNECT_MESSAGE", 7);
        A07 = r12;
        AnonymousClass0SB r11 = new AnonymousClass0SB("FAILED_CONNACK_READ", 8);
        A01 = r11;
        AnonymousClass0SB r10 = new AnonymousClass0SB("FAILED_INVALID_CONACK", 9);
        A0B = r10;
        AnonymousClass0SB r9 = new AnonymousClass0SB("FAILED_DNS_UNRESOLVED", 10);
        A0A = r9;
        AnonymousClass0SB r8 = new AnonymousClass0SB("FAILED_CREATE_IOSTREAM", 11);
        A08 = r8;
        AnonymousClass0SB r7 = new AnonymousClass0SB("FAILED_CONNECTION_REFUSED", 12);
        A02 = r7;
        AnonymousClass0SB r6 = new AnonymousClass0SB("FAILED_CONNECTION_REFUSED_SERVER_SHEDDING_LOAD", 13);
        A05 = r6;
        AnonymousClass0SB r5 = new AnonymousClass0SB("FAILED_UNEXPECTED_DISCONNECT", 14);
        A0H = r5;
        AnonymousClass0SB r4 = new AnonymousClass0SB("FAILED_CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD", 15);
        A03 = r4;
        AnonymousClass0SB r3 = new AnonymousClass0SB("FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED", 16);
        A04 = r3;
        AnonymousClass0SB r2 = new AnonymousClass0SB("FAILED_CONNECTION_UNKNOWN_CONNECT_HASH", 17);
        A06 = r2;
        AnonymousClass0SB r22 = new AnonymousClass0SB("FAILED_SOCKET_CONNECT_ERROR_SSL_CLOCK_SKEW", 18);
        A0E = r22;
        AnonymousClass0SB r222 = new AnonymousClass0SB("FAILED_STOPPED_BEFORE_SSL", 19);
        A0G = r222;
        AnonymousClass0SB r39 = r22;
        AnonymousClass0SB r40 = r222;
        AnonymousClass0SB r23 = r03;
        AnonymousClass0SB r24 = r04;
        AnonymousClass0SB r25 = r15;
        AnonymousClass0SB r26 = r14;
        AnonymousClass0SB r27 = r13;
        AnonymousClass0SB r28 = r12;
        AnonymousClass0SB r223 = r02;
        A00 = new AnonymousClass0SB[]{r0, r223, r23, r24, r25, r26, r27, r28, r11, r10, r9, r8, r7, r6, r5, r4, r3, r2, r39, r40};
    }

    public static AnonymousClass0SB valueOf(String str) {
        return (AnonymousClass0SB) Enum.valueOf(AnonymousClass0SB.class, str);
    }

    public static AnonymousClass0SB[] values() {
        return (AnonymousClass0SB[]) A00.clone();
    }

    private AnonymousClass0SB(String str, int i) {
    }
}
