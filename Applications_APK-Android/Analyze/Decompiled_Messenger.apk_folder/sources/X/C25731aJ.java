package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1aJ  reason: invalid class name and case insensitive filesystem */
public final class C25731aJ extends AnonymousClass0UV {
    private static volatile C25741aK A00;

    public static final C25741aK A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C25741aK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new C25741aK(AnonymousClass0XP.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
