package X;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import java.io.File;
import java.util.Collection;

/* renamed from: X.01s  reason: invalid class name and case insensitive filesystem */
public final class C002701s extends AnonymousClass025 {
    public int A00;
    public Context A01;
    public C002801t A02;

    public File A05(String str) {
        return this.A02.A05(str);
    }

    public String A06(String str) {
        return this.A02.A06(str);
    }

    public void A07(Collection collection) {
        this.A02.A07(collection);
    }

    public String[] A08(String str) {
        return this.A02.A08(str);
    }

    public int A09(String str, int i, StrictMode.ThreadPolicy threadPolicy) {
        return this.A02.A09(str, i, threadPolicy);
    }

    public void A0A(int i) {
        this.A02.A0A(i);
    }

    public String toString() {
        return this.A02.toString();
    }

    public C002701s(Context context, int i) {
        Context applicationContext = context.getApplicationContext();
        this.A01 = applicationContext;
        if (applicationContext == null) {
            Log.w("SoLoader", "context.getApplicationContext returned null, holding reference to original context.");
            this.A01 = context;
        }
        this.A00 = i;
        this.A02 = new C002801t(new File(this.A01.getApplicationInfo().nativeLibraryDir), i);
    }
}
