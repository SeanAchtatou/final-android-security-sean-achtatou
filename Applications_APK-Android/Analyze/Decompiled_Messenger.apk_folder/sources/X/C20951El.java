package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1El  reason: invalid class name and case insensitive filesystem */
public final class C20951El {
    private final C10970lA A00;
    private final FbSharedPreferences A01;

    public static final C20951El A00(AnonymousClass1XY r1) {
        return new C20951El(r1);
    }

    public boolean A01() {
        if (this.A01.BBh(C10990lD.A0E) || this.A01.Aep(C10990lD.A0Q, false) || this.A01.Aep(C10990lD.A0P, false) || this.A01.Aep(C10990lD.A00, false)) {
            return true;
        }
        return false;
    }

    public boolean A02() {
        return this.A00.A00.AbO(AnonymousClass1Y3.A5r, false);
    }

    public C20951El(AnonymousClass1XY r2) {
        this.A01 = FbSharedPreferencesModule.A00(r2);
        this.A00 = C10970lA.A00(r2);
    }
}
