package X;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.0dL  reason: invalid class name and case insensitive filesystem */
public final class C07390dL extends AnonymousClass1ZK {
    private static C04470Uu A00;

    public static final C07390dL A00(AnonymousClass1XY r5) {
        C07390dL r0;
        synchronized (C07390dL.class) {
            C04470Uu A002 = C04470Uu.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C07390dL(AnonymousClass1YA.A00(r02), C04430Uq.A00(r02));
                }
                C04470Uu r1 = A00;
                r0 = (C07390dL) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public void C4x(Intent intent) {
        super.C4x(new Intent(intent).setPackage(this.A00.getPackageName()));
    }

    private C07390dL(Context context, Handler handler) {
        super(context, AnonymousClass08S.A0J(context.getPackageName(), ".permission.CROSS_PROCESS_BROADCAST_MANAGER"), handler);
    }
}
