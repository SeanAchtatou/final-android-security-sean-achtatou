package X;

/* renamed from: X.1Ux  reason: invalid class name */
public final class AnonymousClass1Ux implements AnonymousClass1Uw {
    public final /* synthetic */ AnonymousClass1Ut A00;

    public AnonymousClass1Ux(AnonymousClass1Ut r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005e, code lost:
        X.C010708t.A08(X.AnonymousClass1Ut.A0E, "Exception processing messaging collection delta", r2);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BAZ(java.util.List r9) {
        /*
            r8 = this;
            java.util.Iterator r7 = r9.iterator()
        L_0x0004:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0066
            java.lang.Object r2 = r7.next()
            com.facebook.omnistore.Delta r2 = (com.facebook.omnistore.Delta) r2
            int r1 = r2.getType()     // Catch:{ RuntimeException -> 0x005d }
            r0 = 1
            if (r1 != r0) goto L_0x0004
            java.lang.String r1 = r2.getPrimaryKey()     // Catch:{ RuntimeException -> 0x005d }
            X.1Ut r0 = r8.A00     // Catch:{ RuntimeException -> 0x005d }
            X.0Tq r0 = r0.A09     // Catch:{ RuntimeException -> 0x005d }
            java.lang.Object r5 = r0.get()     // Catch:{ RuntimeException -> 0x005d }
            X.2y6 r5 = (X.C60952y6) r5     // Catch:{ RuntimeException -> 0x005d }
            com.facebook.messaging.tincan.type.TincanMessage r6 = new com.facebook.messaging.tincan.type.TincanMessage     // Catch:{ RuntimeException -> 0x005d }
            java.nio.ByteBuffer r0 = r2.getBlob()     // Catch:{ RuntimeException -> 0x005d }
            r6.<init>(r1, r0)     // Catch:{ RuntimeException -> 0x005d }
            monitor-enter(r5)     // Catch:{ RuntimeException -> 0x005d }
            com.facebook.fbservice.ops.BlueServiceOperationFactory r4 = r5.A01     // Catch:{ all -> 0x005a }
            java.lang.String r3 = "TincanNewMessage"
            android.os.Bundle r2 = new android.os.Bundle     // Catch:{ all -> 0x005a }
            r2.<init>()     // Catch:{ all -> 0x005a }
            java.lang.String r1 = r6.A00     // Catch:{ all -> 0x005a }
            java.lang.String r0 = "packet_key"
            r2.putString(r0, r1)     // Catch:{ all -> 0x005a }
            java.nio.ByteBuffer r0 = r6.A01     // Catch:{ all -> 0x005a }
            byte[] r1 = r0.array()     // Catch:{ all -> 0x005a }
            java.lang.String r0 = "message_data"
            r2.putByteArray(r0, r1)     // Catch:{ all -> 0x005a }
            r1 = 1
            java.lang.Class<X.2y6> r0 = X.C60952y6.class
            com.facebook.common.callercontext.CallerContext r0 = com.facebook.common.callercontext.CallerContext.A04(r0)     // Catch:{ all -> 0x005a }
            X.0lL r0 = r4.newInstance(r3, r2, r1, r0)     // Catch:{ all -> 0x005a }
            r0.CGe()     // Catch:{ all -> 0x005a }
            monitor-exit(r5)     // Catch:{ RuntimeException -> 0x005d }
            goto L_0x0004
        L_0x005a:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ RuntimeException -> 0x005d }
            throw r0     // Catch:{ RuntimeException -> 0x005d }
        L_0x005d:
            r2 = move-exception
            java.lang.Class r1 = X.AnonymousClass1Ut.A0E
            java.lang.String r0 = "Exception processing messaging collection delta"
            X.C010708t.A08(r1, r0, r2)
            goto L_0x0004
        L_0x0066:
            boolean r0 = r9.isEmpty()
            if (r0 != 0) goto L_0x00f1
            X.1Ut r0 = r8.A00     // Catch:{ Exception -> 0x00e9 }
            X.1Yd r2 = r0.A06     // Catch:{ Exception -> 0x00e9 }
            r0 = 283326109190486(0x101af00180956, double:1.39981697120885E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ Exception -> 0x00e9 }
            if (r0 != 0) goto L_0x00f1
            X.1Ut r0 = r8.A00     // Catch:{ Exception -> 0x00e9 }
            X.0Tq r0 = r0.A09     // Catch:{ Exception -> 0x00e9 }
            java.lang.Object r5 = r0.get()     // Catch:{ Exception -> 0x00e9 }
            X.2y6 r5 = (X.C60952y6) r5     // Catch:{ Exception -> 0x00e9 }
            X.1Ut r2 = r8.A00     // Catch:{ Exception -> 0x00e9 }
            com.facebook.omnistore.Omnistore r1 = r2.A02     // Catch:{ Exception -> 0x00e9 }
            if (r1 == 0) goto L_0x00e1
            com.facebook.omnistore.CollectionName r0 = r2.A01     // Catch:{ Exception -> 0x00e9 }
            if (r0 == 0) goto L_0x00e1
            java.lang.String r0 = r1.getDebugInfo()     // Catch:{ Exception -> 0x00e9 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00e9 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r0 = "subscription_info"
            org.json.JSONObject r1 = r1.getJSONObject(r0)     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r0 = "subscriptions"
            org.json.JSONArray r4 = r1.getJSONArray(r0)     // Catch:{ Exception -> 0x00e9 }
            com.facebook.omnistore.CollectionName r0 = r2.A01     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x00e9 }
            r2 = 0
        L_0x00ab:
            int r0 = r4.length()     // Catch:{ Exception -> 0x00e9 }
            if (r2 >= r0) goto L_0x00d9
            org.json.JSONObject r1 = r4.getJSONObject(r2)     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r0 = "collectionName"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x00e9 }
            boolean r0 = r0.equals(r3)     // Catch:{ Exception -> 0x00e9 }
            if (r0 == 0) goto L_0x00d5
            java.lang.String r0 = "globalVersionId"
            long r3 = r1.getLong(r0)     // Catch:{ Exception -> 0x00e9 }
            X.0VL r2 = r5.A02     // Catch:{ Exception -> 0x00e9 }
            X.AnV r1 = new X.AnV     // Catch:{ Exception -> 0x00e9 }
            r1.<init>(r5, r3)     // Catch:{ Exception -> 0x00e9 }
            r0 = 197674928(0xbc847b0, float:7.714506E-32)
            X.AnonymousClass07A.A04(r2, r1, r0)     // Catch:{ Exception -> 0x00e9 }
            goto L_0x00d8
        L_0x00d5:
            int r2 = r2 + 1
            goto L_0x00ab
        L_0x00d8:
            return
        L_0x00d9:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r0 = "No Omnistore collection subscription found"
            r1.<init>(r0)     // Catch:{ Exception -> 0x00e9 }
            goto L_0x00e8
        L_0x00e1:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ Exception -> 0x00e9 }
            java.lang.String r0 = "No Omnistore available"
            r1.<init>(r0)     // Catch:{ Exception -> 0x00e9 }
        L_0x00e8:
            throw r1     // Catch:{ Exception -> 0x00e9 }
        L_0x00e9:
            r2 = move-exception
            java.lang.Class r1 = X.AnonymousClass1Ut.A0E
            java.lang.String r0 = "Failed to update tincan_msg global version id:"
            X.C010708t.A08(r1, r0, r2)
        L_0x00f1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Ux.BAZ(java.util.List):void");
    }
}
