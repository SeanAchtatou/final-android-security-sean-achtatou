package X;

import com.facebook.acra.ACRA;
import com.facebook.litho.annotations.Comparable;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1Jr  reason: invalid class name and case insensitive filesystem */
public final class C22041Jr extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = ACRA.MULTI_SIGNAL_ANR_DETECTOR)
    public List A01 = Collections.EMPTY_LIST;

    public C22041Jr() {
        super("SpacedRow");
    }
}
