package X;

import com.facebook.litho.ComponentTree;

/* renamed from: X.16I  reason: invalid class name */
public final class AnonymousClass16I extends AnonymousClass16J {
    public static final String __redex_internal_original_name = "com.facebook.litho.ComponentTree$CalculateLayoutRunnable";
    public final int A00;
    public final AnonymousClass1KE A01;
    public final String A02;
    public final boolean A03;
    public final /* synthetic */ ComponentTree A04;

    public AnonymousClass16I(ComponentTree componentTree, int i, AnonymousClass1KE r3, String str, boolean z) {
        this.A04 = componentTree;
        this.A00 = i;
        this.A01 = r3;
        this.A02 = str;
        this.A03 = z;
    }
}
