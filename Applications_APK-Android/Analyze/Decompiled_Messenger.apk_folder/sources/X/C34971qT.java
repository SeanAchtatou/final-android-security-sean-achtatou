package X;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.ImmutableMap;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.1qT  reason: invalid class name and case insensitive filesystem */
public abstract class C34971qT implements C08210er, C04240Tb {
    public String A00;
    private int A01;
    private EvictingQueue A02;
    private StringBuilder A03;
    public final Object A04 = new Object();
    public final String A05;
    public final SimpleDateFormat A06 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS", Locale.US);
    private final Set A07 = new HashSet();

    public int A03() {
        return AnonymousClass1Y3.A1c;
    }

    public AnonymousClass879 A04() {
        AnonymousClass879 r0;
        C34961qS r2 = (C34961qS) this;
        synchronized (r2) {
            if (r2.A01 == null) {
                AnonymousClass1XX.A03(AnonymousClass1Y3.B2M, r2.A00);
                r2.A01 = Optional.absent();
            }
            r0 = (AnonymousClass879) r2.A01.orNull();
        }
        return r0;
    }

    public String A05() {
        return "RtcAppBugReportLogger";
    }

    public ImmutableMap AmJ() {
        return null;
    }

    public void prepareDataForWriting() {
    }

    public boolean shouldSendAsync() {
        if (!(this instanceof C34961qS)) {
            return false;
        }
        return ((C34961qS) this).A02.Aem(281651072991527L);
    }

    private Collection A01() {
        synchronized (this.A04) {
            if (this.A02 == null) {
                this.A02 = new EvictingQueue(A03());
            }
        }
        return this.A02;
    }

    public static void A02(C34971qT r9, String str) {
        synchronized (r9.A04) {
            r9.A01().add(str);
            AnonymousClass879 A042 = r9.A04();
            if (A042 != null) {
                StringBuilder sb = r9.A03;
                sb.append(str);
                sb.append(10);
                int i = r9.A01 + 1;
                r9.A01 = i;
                if (i >= 0) {
                    AnonymousClass07A.A04((ScheduledExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ARn, null), new AnonymousClass878(A042, sb.toString(), ((AnonymousClass06A) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AYB, null)).now()), -687721511);
                    r9.A03 = new StringBuilder();
                    r9.A01 = 0;
                }
            }
        }
    }

    public void A08(String str) {
        A02(this, AnonymousClass08S.A0P(this.A06.format(new Date()), "> ", str));
    }

    public ImmutableMap AmI() {
        HashSet hashSet;
        A02(this, "--------- BUG REPORT INVOKED HERE ---------------");
        StringBuilder sb = new StringBuilder();
        synchronized (this.A04) {
            try {
                hashSet = new HashSet(this.A07);
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            Map Bxv = ((C32651m6) it.next()).Bxv();
            if (Bxv != null) {
                for (String str : Bxv.keySet()) {
                    sb.append(str);
                    sb.append(": ");
                    sb.append((String) Bxv.get(str));
                    sb.append("\n");
                }
            }
        }
        synchronized (this.A04) {
            try {
                this.A00 = sb.toString();
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003d, code lost:
        if (r1 == false) goto L_0x0040;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x011c A[SYNTHETIC, Splitter:B:101:0x011c] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0126 A[SYNTHETIC, Splitter:B:107:0x0126] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x01a1 A[SYNTHETIC, Splitter:B:162:0x01a1] */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0095 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0100 A[SYNTHETIC, Splitter:B:84:0x0100] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map getExtraFileFromWorkerThread(java.io.File r18) {
        /*
            r17 = this;
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            r3 = r17
            java.lang.Object r5 = r3.A04     // Catch:{ IOException -> 0x01a5 }
            monitor-enter(r5)     // Catch:{ IOException -> 0x01a5 }
            java.lang.String r0 = r3.A00     // Catch:{ all -> 0x019e }
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)     // Catch:{ all -> 0x019e }
            if (r0 == 0) goto L_0x003f
            java.lang.Object r2 = r3.A04     // Catch:{ all -> 0x019e }
            monitor-enter(r2)     // Catch:{ all -> 0x019e }
            java.util.Collection r0 = r3.A01()     // Catch:{ all -> 0x0038 }
            boolean r1 = r0.isEmpty()     // Catch:{ all -> 0x0038 }
            r0 = 0
            if (r1 != 0) goto L_0x0021
            r0 = 1
        L_0x0021:
            monitor-exit(r2)     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x003f
            X.879 r0 = r3.A04()     // Catch:{ all -> 0x019e }
            if (r0 == 0) goto L_0x003b
            java.util.List r0 = X.AnonymousClass879.A00()     // Catch:{ all -> 0x019e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x019e }
            r0 = r0 ^ 1
            r1 = 1
            if (r0 != 0) goto L_0x003c
            goto L_0x003b
        L_0x0038:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0038 }
            throw r0     // Catch:{ all -> 0x019e }
        L_0x003b:
            r1 = 0
        L_0x003c:
            r0 = 0
            if (r1 == 0) goto L_0x0040
        L_0x003f:
            r0 = 1
        L_0x0040:
            monitor-exit(r5)     // Catch:{ all -> 0x019e }
            if (r0 == 0) goto L_0x019d
            r16 = r3
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x01a5 }
            java.lang.String r0 = r3.A05     // Catch:{ IOException -> 0x01a5 }
            r2 = r18
            r1.<init>(r2, r0)     // Catch:{ IOException -> 0x01a5 }
            android.net.Uri r15 = android.net.Uri.fromFile(r1)     // Catch:{ IOException -> 0x01a5 }
            r14 = 0
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x017c }
            r5.<init>(r1)     // Catch:{ FileNotFoundException -> 0x017c }
            java.io.PrintWriter r6 = new java.io.PrintWriter     // Catch:{ all -> 0x016e }
            java.io.OutputStreamWriter r0 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x016e }
            r0.<init>(r5)     // Catch:{ all -> 0x016e }
            r6.<init>(r0)     // Catch:{ all -> 0x016e }
            java.lang.Object r2 = r3.A04     // Catch:{ all -> 0x016c }
            monitor-enter(r2)     // Catch:{ all -> 0x016c }
            java.lang.String r1 = r3.A00     // Catch:{ all -> 0x0169 }
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)     // Catch:{ all -> 0x0169 }
            if (r0 != 0) goto L_0x0075
            r6.println(r1)     // Catch:{ all -> 0x0169 }
            java.lang.String r0 = "------------------------"
            r6.println(r0)     // Catch:{ all -> 0x0169 }
        L_0x0075:
            monitor-exit(r2)     // Catch:{ all -> 0x0169 }
            X.879 r0 = r3.A04()     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x0146
            int r2 = X.AnonymousClass1Y3.APr     // Catch:{ all -> 0x016c }
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r14)     // Catch:{ all -> 0x016c }
            X.1Y6 r0 = (X.AnonymousClass1Y6) r0     // Catch:{ all -> 0x016c }
            r0.AOx()     // Catch:{ all -> 0x016c }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x016c }
            r10.<init>()     // Catch:{ all -> 0x016c }
            java.util.List r0 = X.AnonymousClass879.A00()     // Catch:{ all -> 0x016c }
            java.util.Iterator r13 = r0.iterator()     // Catch:{ all -> 0x016c }
        L_0x0095:
            boolean r0 = r13.hasNext()     // Catch:{ all -> 0x016c }
            if (r0 == 0) goto L_0x012f
            java.lang.Object r0 = r13.next()     // Catch:{ all -> 0x016c }
            X.87B r0 = (X.AnonymousClass87B) r0     // Catch:{ all -> 0x016c }
            java.io.File r11 = r0.A01     // Catch:{ all -> 0x016c }
            java.lang.String r7 = "IOException while closing stream"
            java.lang.String r9 = "IOException while closing reader"
            r0 = 224(0xe0, float:3.14E-43)
            java.lang.String r2 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ all -> 0x016c }
            int r8 = X.AnonymousClass1Y3.APr     // Catch:{ all -> 0x016c }
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r8, r14)     // Catch:{ all -> 0x016c }
            X.1Y6 r0 = (X.AnonymousClass1Y6) r0     // Catch:{ all -> 0x016c }
            r0.AOx()     // Catch:{ all -> 0x016c }
            r12 = 0
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00ef, all -> 0x0113 }
            r8.<init>(r11)     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00ef, all -> 0x0113 }
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x00ed, IOException -> 0x00eb }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x00ed, IOException -> 0x00eb }
            r0.<init>(r8)     // Catch:{ FileNotFoundException -> 0x00ed, IOException -> 0x00eb }
            r11.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00ed, IOException -> 0x00eb }
            java.lang.String r0 = r11.readLine()     // Catch:{ FileNotFoundException -> 0x00e8, IOException -> 0x00e5, all -> 0x0118 }
        L_0x00cd:
            if (r0 == 0) goto L_0x00dc
            r10.append(r0)     // Catch:{ FileNotFoundException -> 0x00e8, IOException -> 0x00e5, all -> 0x0118 }
            java.lang.String r0 = "\n"
            r10.append(r0)     // Catch:{ FileNotFoundException -> 0x00e8, IOException -> 0x00e5, all -> 0x0118 }
            java.lang.String r0 = r11.readLine()     // Catch:{ FileNotFoundException -> 0x00e8, IOException -> 0x00e5, all -> 0x0118 }
            goto L_0x00cd
        L_0x00dc:
            r11.close()     // Catch:{ IOException -> 0x00e0 }
            goto L_0x010a
        L_0x00e0:
            r0 = move-exception
            X.C010708t.A0L(r2, r9, r0)     // Catch:{ all -> 0x016c }
            goto L_0x010a
        L_0x00e5:
            r1 = move-exception
            r12 = r11
            goto L_0x00f1
        L_0x00e8:
            r1 = move-exception
            r12 = r11
            goto L_0x00f9
        L_0x00eb:
            r1 = move-exception
            goto L_0x00f1
        L_0x00ed:
            r1 = move-exception
            goto L_0x00f9
        L_0x00ef:
            r1 = move-exception
            r8 = r14
        L_0x00f1:
            java.lang.String r0 = "IOException while reading file"
            X.C010708t.A0L(r2, r0, r1)     // Catch:{ all -> 0x0116 }
            goto L_0x00fe
        L_0x00f7:
            r1 = move-exception
            r8 = r14
        L_0x00f9:
            java.lang.String r0 = "Unable to open file for reading"
            X.C010708t.A0L(r2, r0, r1)     // Catch:{ all -> 0x0116 }
        L_0x00fe:
            if (r12 == 0) goto L_0x0108
            r12.close()     // Catch:{ IOException -> 0x0104 }
            goto L_0x0108
        L_0x0104:
            r0 = move-exception
            X.C010708t.A0L(r2, r9, r0)     // Catch:{ all -> 0x016c }
        L_0x0108:
            if (r8 == 0) goto L_0x0095
        L_0x010a:
            r8.close()     // Catch:{ IOException -> 0x010e }
            goto L_0x0095
        L_0x010e:
            r0 = move-exception
            X.C010708t.A0L(r2, r7, r0)     // Catch:{ all -> 0x016c }
            goto L_0x0095
        L_0x0113:
            r1 = move-exception
            r8 = r14
            goto L_0x011a
        L_0x0116:
            r1 = move-exception
            goto L_0x011a
        L_0x0118:
            r1 = move-exception
            r12 = r11
        L_0x011a:
            if (r12 == 0) goto L_0x0124
            r12.close()     // Catch:{ IOException -> 0x0120 }
            goto L_0x0124
        L_0x0120:
            r0 = move-exception
            X.C010708t.A0L(r2, r9, r0)     // Catch:{ all -> 0x016c }
        L_0x0124:
            if (r8 == 0) goto L_0x012e
            r8.close()     // Catch:{ IOException -> 0x012a }
            goto L_0x012e
        L_0x012a:
            r0 = move-exception
            X.C010708t.A0L(r2, r7, r0)     // Catch:{ all -> 0x016c }
        L_0x012e:
            throw r1     // Catch:{ all -> 0x016c }
        L_0x012f:
            java.lang.String r1 = r10.toString()     // Catch:{ all -> 0x016c }
            boolean r0 = X.C06850cB.A0B(r1)     // Catch:{ all -> 0x016c }
            if (r0 != 0) goto L_0x0146
            java.lang.String r0 = "---------Cached Data Start (previous app launch - MAY BE MISSING DATA) ---------------"
            r6.println(r0)     // Catch:{ all -> 0x016c }
            r6.println(r1)     // Catch:{ all -> 0x016c }
            java.lang.String r0 = "---------Cached Data End (previous app launch - MAY BE MISSING DATA) ---------------"
            r6.println(r0)     // Catch:{ all -> 0x016c }
        L_0x0146:
            java.lang.Object r2 = r3.A04     // Catch:{ all -> 0x016c }
            monitor-enter(r2)     // Catch:{ all -> 0x016c }
            java.util.Collection r0 = r3.A01()     // Catch:{ all -> 0x0166 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0166 }
        L_0x0151:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0166 }
            if (r0 == 0) goto L_0x0161
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0166 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0166 }
            r6.println(r0)     // Catch:{ all -> 0x0166 }
            goto L_0x0151
        L_0x0161:
            monitor-exit(r2)     // Catch:{ all -> 0x0166 }
            r6.close()     // Catch:{ FileNotFoundException -> 0x0179, all -> 0x0176 }
            goto L_0x018c
        L_0x0166:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0166 }
            goto L_0x016b
        L_0x0169:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0169 }
        L_0x016b:
            throw r0     // Catch:{ all -> 0x016c }
        L_0x016c:
            r0 = move-exception
            goto L_0x0170
        L_0x016e:
            r0 = move-exception
            r6 = r14
        L_0x0170:
            if (r6 == 0) goto L_0x0175
            r6.close()     // Catch:{ FileNotFoundException -> 0x0179, all -> 0x0176 }
        L_0x0175:
            throw r0     // Catch:{ FileNotFoundException -> 0x0179, all -> 0x0176 }
        L_0x0176:
            r0 = move-exception
            r14 = r5
            goto L_0x019a
        L_0x0179:
            r2 = move-exception
            r14 = r5
            goto L_0x017d
        L_0x017c:
            r2 = move-exception
        L_0x017d:
            java.lang.String r1 = r16.A05()     // Catch:{ all -> 0x0199 }
            java.lang.String r0 = "Cannot create/open trace file"
            X.C010708t.A0L(r1, r0, r2)     // Catch:{ all -> 0x0199 }
            if (r14 == 0) goto L_0x018f
            r14.close()     // Catch:{ IOException -> 0x01a5 }
            goto L_0x018f
        L_0x018c:
            r5.close()     // Catch:{ IOException -> 0x01a5 }
        L_0x018f:
            java.lang.String r1 = r3.A05     // Catch:{ IOException -> 0x01a5 }
            java.lang.String r0 = r15.toString()     // Catch:{ IOException -> 0x01a5 }
            r4.put(r1, r0)     // Catch:{ IOException -> 0x01a5 }
            return r4
        L_0x0199:
            r0 = move-exception
        L_0x019a:
            if (r14 == 0) goto L_0x01a4
            goto L_0x01a1
        L_0x019d:
            return r4
        L_0x019e:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x019e }
            goto L_0x01a4
        L_0x01a1:
            r14.close()     // Catch:{ IOException -> 0x01a5 }
        L_0x01a4:
            throw r0     // Catch:{ IOException -> 0x01a5 }
        L_0x01a5:
            r2 = move-exception
            java.lang.String r1 = r3.A05()
            java.lang.String r0 = "Exception saving rtc trace"
            X.C010708t.A0M(r1, r0, r2)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34971qT.getExtraFileFromWorkerThread(java.io.File):java.util.Map");
    }

    public C34971qT(String str) {
        this.A05 = str;
        this.A03 = new StringBuilder();
    }

    public void A06(C32651m6 r3) {
        Preconditions.checkNotNull(r3);
        synchronized (this.A04) {
            this.A07.add(r3);
        }
    }

    public void A07(C32651m6 r3) {
        Preconditions.checkNotNull(r3);
        synchronized (this.A04) {
            this.A07.remove(r3);
        }
    }
}
