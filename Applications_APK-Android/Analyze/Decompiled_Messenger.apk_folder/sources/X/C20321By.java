package X;

import android.util.SparseArray;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1By  reason: invalid class name and case insensitive filesystem */
public final class C20321By {
    private static final List A02 = new ArrayList();
    public AnonymousClass1CW A00;
    public List A01 = new ArrayList();

    private static final String A02(C16070wR r2, String str) {
        if (r2 != null && r2.A02 == null) {
            return r2.getClass().getSimpleName();
        }
        if (r2 != null) {
            return AnonymousClass08S.A0P(str, "->", r2.getClass().getSimpleName());
        }
        return BuildConfig.FLAVOR;
    }

    private static final int A00(List list, String str) {
        Iterator it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            C16070wR r1 = (C16070wR) it.next();
            if (r1.A04.equals(str)) {
                break;
            }
            i += r1.A00;
        }
        return i;
    }

    public static AnonymousClass1CW A01(AnonymousClass1GA r25, C16070wR r26, C16070wR r27, List list, AnonymousClass1AE r29, String str, String str2, String str3, String str4, boolean z) {
        AnonymousClass1CW r6;
        List arrayList;
        int i;
        int i2;
        String str5;
        boolean z2;
        boolean z3 = false;
        C16070wR r2 = r26;
        if (r26 == null) {
            z3 = true;
        }
        boolean z4 = false;
        C16070wR r10 = r27;
        if (r27 == null) {
            z4 = true;
        }
        if (!z3 || !z4) {
            boolean z5 = z;
            if (z4) {
                int i3 = r2.A00;
                list.add(r2);
                r6 = AnonymousClass1CW.A00(r2.A00, r10, z5);
                for (int i4 = 0; i4 < i3; i4++) {
                    r6.A04(AnonymousClass1IH.A00(3, 0, C21621Ib.A01(), null, null));
                }
            } else {
                String A022 = A02(r2, str2);
                String A023 = A02(r10, str3);
                String str6 = str;
                String str7 = str4;
                if (!z3) {
                    boolean z6 = false;
                    if (r27 != null) {
                        z2 = r10.A08 | false;
                    } else {
                        z2 = false;
                    }
                    if (z2 || r10.A0K(r2, r10)) {
                        z6 = true;
                    }
                    if (!z6) {
                        AnonymousClass1CW A002 = AnonymousClass1CW.A00(r2.A00, r10, z5);
                        r10.A00 = A002.A00;
                        for (AnonymousClass1AE r0 : r29.A00) {
                            for (AnonymousClass1AE r17 : r0.A00) {
                                r17.A05(str6, r2, r10, A022, A023, false, str7);
                            }
                        }
                        return A002;
                    }
                }
                for (AnonymousClass1AE r02 : r29.A00) {
                    for (AnonymousClass1AE A05 : r02.A00) {
                        A05.A05(str6, r2, r10, A022, A023, true, str7);
                    }
                }
                if (r10.A0H()) {
                    boolean A024 = C27041cY.A02();
                    if (A024) {
                        C09040gQ APo = C27041cY.A00.APo("generateChangeSet");
                        if (z3) {
                            str5 = "<null>";
                        } else {
                            str5 = r2.A05;
                        }
                        APo.AOo("current_root", str5);
                        APo.AOo("update_prefix", A022);
                        APo.flush();
                    }
                    if (z3) {
                        i2 = 0;
                    } else {
                        i2 = r2.A00;
                    }
                    r6 = AnonymousClass1CW.A00(i2, r10, z5);
                    r10.A0F(r10.A03, r6, r2, r10);
                    r10.A00 = r6.A00;
                    if (A024) {
                        C27041cY.A00();
                    }
                } else {
                    AnonymousClass1CW A003 = AnonymousClass1CW.A00(0, r10, z5);
                    Map A004 = C16070wR.A00(r2);
                    Map A005 = C16070wR.A00(r10);
                    if (r26 == null) {
                        arrayList = A02;
                    } else {
                        arrayList = new ArrayList(r2.A06);
                    }
                    List list2 = r10.A06;
                    int i5 = -1;
                    int i6 = -1;
                    for (int i7 = 0; i7 < list2.size(); i7++) {
                        String str8 = ((C16070wR) list2.get(i7)).A04;
                        if (A004.containsKey(str8)) {
                            C12300p2 r1 = (C12300p2) A004.get(str8);
                            C16070wR r5 = (C16070wR) r1.A00;
                            int intValue = ((Integer) r1.A01).intValue();
                            if (i5 > intValue) {
                                for (int i8 = 0; i8 < r5.A00; i8++) {
                                    A003.A04(AnonymousClass1IH.A01(A00(arrayList, str8), i6, null));
                                }
                                arrayList.remove(intValue);
                                arrayList.add(i5, r5);
                                int size = arrayList.size();
                                for (int i9 = 0; i9 < size; i9++) {
                                    C16070wR r3 = (C16070wR) arrayList.get(i9);
                                    C12300p2 r12 = (C12300p2) A004.get(r3.A04);
                                    if (((Integer) r12.A01).intValue() != i9) {
                                        A004.put(r3.A04, new C12300p2(r12.A00, Integer.valueOf(i9)));
                                    }
                                }
                            } else if (intValue > i5) {
                                i6 = (A00(arrayList, str8) + ((C16070wR) arrayList.get(intValue)).A00) - 1;
                                i5 = intValue;
                            }
                        }
                    }
                    SparseArray sparseArray = new SparseArray();
                    for (int i10 = 0; i10 < arrayList.size(); i10++) {
                        String str9 = ((C16070wR) arrayList.get(i10)).A04;
                        C16070wR r13 = (C16070wR) arrayList.get(i10);
                        if (A005.get(str9) == null) {
                            sparseArray.put(i10, A01(r25, r13, null, list, r29, str, A022, A023, str4, z));
                        }
                    }
                    int i11 = 0;
                    for (int i12 = 0; i12 < list2.size(); i12++) {
                        C16070wR r14 = (C16070wR) list2.get(i12);
                        C12300p2 r03 = (C12300p2) A004.get(r14.A04);
                        if (r03 != null) {
                            i = ((Integer) r03.A01).intValue();
                        } else {
                            i = -1;
                        }
                        if (i < 0) {
                            AnonymousClass1CW r22 = (AnonymousClass1CW) sparseArray.get(i11);
                            AnonymousClass1CW A012 = A01(r25, null, r14, list, r29, str, A022, A023, str4, z);
                            sparseArray.put(i11, AnonymousClass1CW.A01(r22, A012));
                            if (r22 != null) {
                                r22.A03();
                            }
                            A012.A03();
                        } else {
                            AnonymousClass1CW r23 = (AnonymousClass1CW) sparseArray.get(i);
                            AnonymousClass1CW A013 = A01(r25, (C16070wR) arrayList.get(i), r14, list, r29, str, A022, A023, str4, z);
                            sparseArray.put(i, AnonymousClass1CW.A01(r23, A013));
                            if (r23 != null) {
                                r23.A03();
                            }
                            A013.A03();
                            i11 = i;
                        }
                    }
                    int size2 = sparseArray.size();
                    for (int i13 = 0; i13 < size2; i13++) {
                        AnonymousClass1CW r04 = (AnonymousClass1CW) sparseArray.valueAt(i13);
                        A003 = AnonymousClass1CW.A01(A003, r04);
                        if (r04 != null) {
                            r04.A03();
                        }
                    }
                    r10.A00 = A003.A00;
                    return A003;
                }
            }
            return r6;
        }
        throw new IllegalStateException("Both currentRoot and newRoot are null.");
    }
}
