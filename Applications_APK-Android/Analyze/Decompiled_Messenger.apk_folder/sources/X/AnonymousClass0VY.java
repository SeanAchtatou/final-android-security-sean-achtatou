package X;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/* renamed from: X.0VY  reason: invalid class name */
public final class AnonymousClass0VY extends AnonymousClass0VZ {
    public int A00;
    public ArrayList A01;
    public final C04540Vc A02;
    public final Map A03;
    public final PriorityQueue A04 = new PriorityQueue(64, C04530Vb.A00);
    public final PriorityQueue A05 = new PriorityQueue(16, C04520Va.A00);

    public C07820eD A0H(long j, Integer num) {
        C07820eD r3;
        while (true) {
            C07690e0 r32 = (C07690e0) this.A05.peek();
            if (r32 != null && j >= r32.A00()) {
                this.A05.poll();
                A0I(r32);
            }
        }
        while (true) {
            r3 = (C07820eD) this.A04.peek();
            if (r3 != null) {
                C04610Vk AZE = r3.AZE();
                Preconditions.checkState(AZE instanceof C04610Vk);
                if (!AZE.A04) {
                    break;
                }
                this.A04.poll();
                if (this.A01 == null) {
                    this.A01 = new ArrayList();
                }
                this.A01.add(r3);
            } else {
                r3 = null;
                break;
            }
        }
        if (r3 != null) {
            C04540Vc r0 = this.A02;
            int i = r0.A00;
            int i2 = r0.A01;
            boolean z = false;
            if (i < i2) {
                z = true;
            }
            if (z) {
                if (num == AnonymousClass07B.A01) {
                    C07820eD r2 = (C07820eD) this.A04.poll();
                    boolean z2 = false;
                    if (r3 == r2) {
                        z2 = true;
                    }
                    Preconditions.checkState(z2);
                    C04610Vk AZE2 = r2.AZE();
                    Preconditions.checkState(AZE2 instanceof C04610Vk);
                    AZE2.A0C(r2);
                }
                return r3;
            }
        }
        return null;
    }

    public void A0I(C07820eD r3) {
        this.A00++;
        C04610Vk AZE = r3.AZE();
        Preconditions.checkState(AZE instanceof C04610Vk);
        AZE.A0A(r3);
    }

    public AnonymousClass0VY(int i) {
        this.A02 = new C04540Vc(i);
        this.A03 = new HashMap(i);
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("active", this.A02.A00);
        stringHelper.add("pending", this.A00);
        stringHelper.add("ready", this.A04.size());
        stringHelper.add("timer", this.A05.size());
        return stringHelper.toString();
    }
}
