package X;

import android.net.Uri;
import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.business.attachments.generic.model.LogoImage;
import com.facebook.messaging.business.commerce.model.retail.CommerceBubbleModel;
import com.facebook.messaging.business.commerce.model.retail.CommerceData;
import com.facebook.messaging.business.commerce.model.retail.Receipt;
import com.facebook.messaging.business.commerce.model.retail.ReceiptCancellation;
import com.facebook.messaging.business.commerce.model.retail.Shipment;
import com.facebook.messaging.business.commerce.model.retail.ShipmentTrackingEvent;
import com.facebook.messaging.model.share.Share;
import com.facebook.messaging.model.share.ShareMedia;
import com.facebook.messaging.model.share.ShareProperty;
import com.facebook.platform.opengraph.model.OpenGraphActionRobotext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: X.0pQ  reason: invalid class name and case insensitive filesystem */
public final class C12470pQ {
    private final C11990oM A00;

    public static final C12470pQ A00() {
        return new C12470pQ(new C11990oM());
    }

    public Share A01(JsonNode jsonNode) {
        OpenGraphActionRobotext openGraphActionRobotext;
        ShareMedia.Type type;
        JsonNode jsonNode2;
        C29907ElN elN = new C29907ElN();
        elN.A06 = JSONUtil.A0N(jsonNode.get("fbid"));
        elN.A08 = JSONUtil.A0N(jsonNode.get("name"));
        elN.A09 = JSONUtil.A0N(jsonNode.get("shareableid"));
        elN.A03 = JSONUtil.A0N(jsonNode.get("caption"));
        elN.A05 = JSONUtil.A0N(jsonNode.get("description"));
        JsonNode jsonNode3 = jsonNode.get("media");
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = jsonNode3.iterator();
        while (it.hasNext()) {
            JsonNode jsonNode4 = (JsonNode) it.next();
            C29633Een een = new C29633Een();
            een.A01 = JSONUtil.A0N(jsonNode4.get("href"));
            try {
                type = ShareMedia.Type.valueOf(JSONUtil.A0N(jsonNode4.get("type")).toUpperCase(Locale.US));
            } catch (IllegalArgumentException | NullPointerException unused) {
                type = ShareMedia.Type.UNKNOWN;
            }
            een.A00 = type;
            een.A03 = JSONUtil.A0N(jsonNode4.get("src"));
            if (jsonNode4.has("playable_src")) {
                jsonNode2 = jsonNode4.get("playable_src");
            } else if (jsonNode4.has("video")) {
                jsonNode2 = jsonNode4.get("video").get("source_url");
            } else {
                builder.add((Object) new ShareMedia(een));
            }
            een.A02 = JSONUtil.A0N(jsonNode2);
            builder.add((Object) new ShareMedia(een));
        }
        elN.A0A = builder.build();
        elN.A07 = JSONUtil.A0N(jsonNode.get("href"));
        JsonNode jsonNode5 = jsonNode.get("properties");
        ArrayList A002 = C04300To.A00();
        for (int i = 0; i < jsonNode5.size(); i++) {
            JsonNode jsonNode6 = jsonNode5.get(i);
            if (jsonNode6.has("name") && jsonNode6.has("text")) {
                C29944Elz elz = new C29944Elz();
                elz.A01 = JSONUtil.A0N(jsonNode6.get("name"));
                elz.A02 = JSONUtil.A0N(jsonNode6.get("text"));
                elz.A00 = JSONUtil.A0N(jsonNode6.get("href"));
                A002.add(new ShareProperty(elz));
            }
        }
        elN.A0B = A002;
        JsonNode jsonNode7 = jsonNode.get("robotext");
        if (jsonNode7 == null || (jsonNode7 instanceof NullNode)) {
            openGraphActionRobotext = null;
        } else {
            String A0N = JSONUtil.A0N(jsonNode7.get("robotext"));
            ArrayList A003 = C04300To.A00();
            JsonNode jsonNode8 = jsonNode7.get("spans");
            for (int i2 = 0; i2 < jsonNode8.size(); i2++) {
                JsonNode jsonNode9 = jsonNode8.get(i2);
                int A04 = JSONUtil.A04(jsonNode9.get("start"));
                A003.add(new OpenGraphActionRobotext.Span(A04, JSONUtil.A04(jsonNode9.get("end")) - A04));
            }
            openGraphActionRobotext = new OpenGraphActionRobotext(A0N, A003);
        }
        elN.A01 = openGraphActionRobotext;
        elN.A02 = JSONUtil.A0N(jsonNode.get("attribution"));
        elN.A04 = JSONUtil.A0N(jsonNode.get("deep_link_url"));
        elN.A00 = this.A00.A09(jsonNode.get("commerce_data"));
        return new Share(elN);
    }

    public ObjectNode A02(Share share) {
        ObjectNode objectNode;
        ObjectNode objectNode2;
        String str;
        String str2;
        ImmutableList immutableList;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        if (share == null) {
            return null;
        }
        ObjectNode objectNode3 = new ObjectNode(JsonNodeFactory.instance);
        objectNode3.put("fbid", share.A08);
        objectNode3.put("name", share.A0A);
        objectNode3.put("shareableid", share.A0B);
        objectNode3.put("caption", share.A05);
        objectNode3.put("description", share.A07);
        objectNode3.put("href", share.A09);
        ImmutableList<ShareMedia> immutableList2 = share.A02;
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        for (ShareMedia shareMedia : immutableList2) {
            ObjectNode objectNode4 = new ObjectNode(JsonNodeFactory.instance);
            objectNode4.put("type", shareMedia.A00.toString());
            objectNode4.put("src", shareMedia.A03);
            objectNode4.put("href", shareMedia.A01);
            if (ShareMedia.Type.VIDEO.equals(shareMedia.A00) && (str7 = shareMedia.A02) != null) {
                objectNode4.put("playable_src", str7);
            }
            arrayNode.add(objectNode4);
        }
        objectNode3.put("media", arrayNode);
        ImmutableList<ShareProperty> immutableList3 = share.A03;
        ArrayNode arrayNode2 = new ArrayNode(JsonNodeFactory.instance);
        for (ShareProperty shareProperty : immutableList3) {
            ObjectNode objectNode5 = new ObjectNode(JsonNodeFactory.instance);
            objectNode5.put("name", shareProperty.A01);
            objectNode5.put("text", shareProperty.A02);
            objectNode5.put("href", shareProperty.A00);
            arrayNode2.add(objectNode5);
        }
        objectNode3.put("properties", arrayNode2);
        OpenGraphActionRobotext openGraphActionRobotext = share.A01;
        if (openGraphActionRobotext == null) {
            objectNode = null;
        } else {
            objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("robotext", openGraphActionRobotext.A00);
            ArrayNode arrayNode3 = new ArrayNode(JsonNodeFactory.instance);
            for (OpenGraphActionRobotext.Span span : openGraphActionRobotext.A01) {
                ObjectNode objectNode6 = new ObjectNode(JsonNodeFactory.instance);
                objectNode6.put("start", span.mOffset);
                objectNode6.put("end", span.mOffset + span.mLength);
                arrayNode3.add(objectNode6);
            }
            objectNode.put("spans", arrayNode3);
        }
        objectNode3.put("robotext", objectNode);
        objectNode3.put("attribution", share.A04);
        objectNode3.put("deep_link_url", share.A06);
        CommerceData commerceData = share.A00;
        ObjectNode objectNode7 = null;
        if (commerceData != null) {
            CommerceBubbleModel commerceBubbleModel = commerceData.A00;
            CommerceBubbleModel commerceBubbleModel2 = commerceBubbleModel;
            if (commerceBubbleModel != null) {
                Integer B7Q = commerceBubbleModel.B7Q();
                if (B7Q == AnonymousClass07B.A01) {
                    Receipt receipt = (Receipt) commerceBubbleModel2;
                    objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                    objectNode2.put("receipt_id", receipt.A0D);
                    objectNode2.put("order_id", receipt.A0G);
                    objectNode2.put("shipping_method", receipt.A0J);
                    objectNode2.put("payment_method", receipt.A0H);
                    Uri uri = receipt.A02;
                    String str8 = BuildConfig.FLAVOR;
                    if (uri != null) {
                        str6 = uri.toString();
                    } else {
                        str6 = str8;
                    }
                    objectNode2.put("order_url", str6);
                    Uri uri2 = receipt.A01;
                    if (uri2 != null) {
                        str8 = uri2.toString();
                    }
                    objectNode2.put("cancellation_url", str8);
                    objectNode2.put("structured_address", C11990oM.A06(receipt.A04));
                    objectNode2.put("status", receipt.A0K);
                    objectNode2.put("total_cost", receipt.A0B);
                    objectNode2.put("total_tax", receipt.A0C);
                    objectNode2.put("shipping_cost", receipt.A09);
                    objectNode2.put("subtotal", receipt.A0A);
                    objectNode2.put("order_time", receipt.A0F);
                    objectNode2.put("partner_logo", C11990oM.A05(receipt.A03));
                    objectNode2.put("items", C11990oM.A07(receipt.A05));
                    objectNode2.put("recipient_name", receipt.A0I);
                    objectNode2.put("account_holder_name", receipt.A08);
                } else {
                    if (B7Q == AnonymousClass07B.A0C) {
                        ReceiptCancellation receiptCancellation = (ReceiptCancellation) commerceBubbleModel2;
                        objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                        objectNode2.put("cancellation_id", receiptCancellation.A03);
                        Receipt receipt2 = receiptCancellation.A01;
                        LogoImage logoImage = null;
                        if (receipt2 != null) {
                            str4 = receipt2.A0D;
                        } else {
                            str4 = null;
                        }
                        objectNode2.put("receipt_id", str4);
                        Receipt receipt3 = receiptCancellation.A01;
                        if (receipt3 != null) {
                            str5 = receipt3.A0G;
                        } else {
                            str5 = null;
                        }
                        objectNode2.put("order_id", str5);
                        Receipt receipt4 = receiptCancellation.A01;
                        if (receipt4 != null) {
                            logoImage = receipt4.A03;
                        }
                        objectNode2.put("partner_logo", C11990oM.A05(logoImage));
                        immutableList = receiptCancellation.A02;
                    } else if (B7Q == AnonymousClass07B.A0N || B7Q == AnonymousClass07B.A0q) {
                        Shipment shipment = (Shipment) commerceBubbleModel2;
                        objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                        objectNode2.put("shipment_id", shipment.A0D);
                        objectNode2.put("receipt_id", shipment.A0E);
                        objectNode2.put("tracking_number", shipment.A0G);
                        objectNode2.put("carrier", shipment.A07.A03);
                        Uri uri3 = shipment.A03;
                        String str9 = BuildConfig.FLAVOR;
                        if (uri3 != null) {
                            str = uri3.toString();
                        } else {
                            str = str9;
                        }
                        objectNode2.put("carrier_tracking_url", str);
                        objectNode2.put("ship_date", Long.toString(shipment.A02 / 1000));
                        objectNode2.put("display_ship_date", shipment.A0C);
                        objectNode2.put("origin", C11990oM.A06(shipment.A06));
                        objectNode2.put("destination", C11990oM.A06(shipment.A05));
                        long j = shipment.A01;
                        if (j != 0) {
                            str2 = Long.toString(j / 1000);
                        } else {
                            str2 = str9;
                        }
                        objectNode2.put("estimated_delivery_time", str2);
                        objectNode2.put("estimated_delivery_display_time", shipment.A0B);
                        long j2 = shipment.A00;
                        if (j2 != 0) {
                            str9 = Long.toString(j2 / 1000);
                        }
                        objectNode2.put("delayed_delivery_time", str9);
                        objectNode2.put("delayed_delivery_display_time", shipment.A0A);
                        objectNode2.put("service_type", shipment.A0F);
                        objectNode2.put("carrier_logo", C11990oM.A05(shipment.A04));
                        immutableList = shipment.A08;
                    } else if (B7Q == AnonymousClass07B.A0Y || B7Q == AnonymousClass07B.A0i || B7Q == AnonymousClass07B.A0n || B7Q == AnonymousClass07B.A0o || B7Q == AnonymousClass07B.A0p || B7Q == AnonymousClass07B.A02) {
                        ShipmentTrackingEvent shipmentTrackingEvent = (ShipmentTrackingEvent) commerceBubbleModel2;
                        objectNode2 = new ObjectNode(JsonNodeFactory.instance);
                        objectNode2.put("id", shipmentTrackingEvent.A05);
                        objectNode2.put("tracking_number", shipmentTrackingEvent.A02.A0G);
                        objectNode2.put("timestamp", Long.toString(shipmentTrackingEvent.A00 / 1000));
                        objectNode2.put("display_time", shipmentTrackingEvent.A04);
                        objectNode2.put("tracking_event_location", C11990oM.A06(shipmentTrackingEvent.A01));
                        Shipment shipment2 = shipmentTrackingEvent.A02;
                        if (shipment2 != null) {
                            objectNode2.put("shipment_id", shipment2.A0D);
                            objectNode2.put("carrier", shipmentTrackingEvent.A02.A07.A03);
                            Uri uri4 = shipmentTrackingEvent.A02.A07.A00;
                            if (uri4 != null) {
                                str3 = uri4.toString();
                            } else {
                                str3 = BuildConfig.FLAVOR;
                            }
                            objectNode2.put("carrier_tracking_url", str3);
                            objectNode2.put("carrier_logo", C11990oM.A05(shipmentTrackingEvent.A02.A07.A01));
                            objectNode2.put("service_type", shipmentTrackingEvent.A02.A0F);
                            immutableList = shipmentTrackingEvent.A02.A08;
                        }
                    }
                    objectNode2.put("items", C11990oM.A07(immutableList));
                }
                objectNode2.put("messenger_commerce_bubble_type", C22327AtE.A00(B7Q));
                objectNode7 = new ObjectNode(JsonNodeFactory.instance);
                objectNode7.put("fb_object_contents", objectNode2);
            }
        }
        objectNode3.put("commerce_data", objectNode7);
        return objectNode3;
    }

    public C12470pQ(C11990oM r1) {
        this.A00 = r1;
    }
}
