package X;

import com.facebook.acra.ACRA;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0uO  reason: invalid class name and case insensitive filesystem */
public enum C14940uO {
    AUTO(0),
    FLEX_START(1),
    CENTER(2),
    FLEX_END(3),
    STRETCH(4),
    BASELINE(5),
    SPACE_BETWEEN(6),
    SPACE_AROUND(7);
    
    public final int mIntValue;

    private C14940uO(int i) {
        this.mIntValue = i;
    }

    public static C14940uO A00(int i) {
        switch (i) {
            case 0:
                return AUTO;
            case 1:
                return FLEX_START;
            case 2:
                return CENTER;
            case 3:
                return FLEX_END;
            case 4:
                return STRETCH;
            case 5:
                return BASELINE;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return SPACE_BETWEEN;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return SPACE_AROUND;
            default:
                throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", i));
        }
    }
}
