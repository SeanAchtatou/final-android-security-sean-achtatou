package X;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1aX  reason: invalid class name and case insensitive filesystem */
public final class C25871aX {
    @JsonProperty("Marker Points")
    public Map<Long, String> mMarkerPoints = new HashMap();
    @JsonProperty("PersistId")
    public String mPersistId = BuildConfig.FLAVOR;
}
