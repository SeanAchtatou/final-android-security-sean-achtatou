package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.profilo.logger.Logger;
import com.facebook.profilo.logger.api.ProfiloLogger;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: X.009  reason: invalid class name */
public class AnonymousClass009 {
    public static Field A03;
    public static Field A04;
    public static Field A05;
    public static Field A06;
    public static Method A07;
    public static boolean A08;
    public boolean A00;
    private Message A01;
    private final Handler A02;

    public synchronized void A05() {
        int i;
        if (this.A01 != null) {
            if (0 != 0) {
                this.A02.removeMessages(1);
            }
            Message message = this.A01;
            if (ProfiloLogger.sHasProfilo) {
                i = Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 16, 0, 0, -1594103271, 0, 0);
            } else {
                i = -1;
            }
            message.arg1 = i;
            this.A02.sendMessageAtFrontOfQueue(this.A01);
            this.A01 = null;
        }
    }

    public boolean A07() {
        return false;
    }

    public static Handler A00(Message message) {
        try {
            return (Handler) A06.get(message);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    public static void A01(Message message) {
        AnonymousClass00C.A01(32, "manually pumping one message", 1072283922);
        try {
            if (A08) {
                A00(message).dispatchMessage(message);
                A03(message, 0);
                message.recycle();
                return;
            }
            throw new AssertionError("init not called");
        } finally {
            AnonymousClass00C.A00(32, -30928465);
        }
    }

    public static void A02(Message message) {
        try {
            A03(message, ((Integer) A04.get(message)).intValue() & -2);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    private static void A03(Message message, int i) {
        try {
            A04.set(message, Integer.valueOf(i));
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX WARN: Type inference failed for: r2v10, types: [java.lang.Throwable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
            r15 = this;
            r0 = 32
            java.lang.String r3 = "pumping messages"
            r2 = 106185917(0x65444bd, float:3.992327E-35)
            X.AnonymousClass00C.A01(r0, r3, r2)
            android.os.Handler r2 = r15.A02     // Catch:{ all -> 0x00a9 }
            android.os.Looper r2 = r2.getLooper()     // Catch:{ all -> 0x00a9 }
            java.lang.Thread r3 = r2.getThread()     // Catch:{ all -> 0x00a9 }
            java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x00a9 }
            if (r3 != r2) goto L_0x009d
            boolean r2 = r15.A07()     // Catch:{ all -> 0x00a9 }
            if (r2 == 0) goto L_0x0023
            r15.A05()     // Catch:{ all -> 0x00a9 }
        L_0x0023:
            android.os.MessageQueue r3 = android.os.Looper.myQueue()     // Catch:{ all -> 0x00a9 }
        L_0x0027:
            r4 = 0
            boolean r2 = com.facebook.profilo.logger.api.ProfiloLogger.sHasProfilo     // Catch:{ all -> 0x00a9 }
            if (r2 == 0) goto L_0x003d
            int r5 = X.AnonymousClass00n.A01     // Catch:{ all -> 0x00a9 }
            r6 = 6
            r7 = 53
            r8 = 0
            r10 = 0
            r13 = 0
            r11 = -659545307(0xffffffffd8b02325, float:-1.54931993E15)
            r12 = 0
            com.facebook.profilo.logger.Logger.writeStandardEntry(r5, r6, r7, r8, r10, r11, r12, r13)     // Catch:{ all -> 0x00a9 }
        L_0x003d:
            java.lang.reflect.Method r5 = X.AnonymousClass009.A07     // Catch:{ IllegalAccessException -> 0x0091, InvocationTargetException -> 0x007c }
            java.lang.Object[] r2 = new java.lang.Object[r4]     // Catch:{ IllegalAccessException -> 0x0091, InvocationTargetException -> 0x007c }
            java.lang.Object r5 = r5.invoke(r3, r2)     // Catch:{ IllegalAccessException -> 0x0091, InvocationTargetException -> 0x007c }
            android.os.Message r5 = (android.os.Message) r5     // Catch:{ IllegalAccessException -> 0x0091, InvocationTargetException -> 0x007c }
            r2 = 933116224(0x379e3940, float:1.8861727E-5)
            com.facebook.profilo.logger.api.ProfiloLogger.waitEnd(r4, r2)     // Catch:{ all -> 0x00a9 }
            android.os.Handler r4 = A00(r5)     // Catch:{ all -> 0x00a9 }
            android.os.Handler r2 = r15.A02     // Catch:{ all -> 0x00a9 }
            if (r4 != r2) goto L_0x0071
            r4.dispatchMessage(r5)     // Catch:{ all -> 0x00a9 }
            r2 = 0
            A03(r5, r2)     // Catch:{ all -> 0x00a9 }
            r5.recycle()     // Catch:{ all -> 0x00a9 }
        L_0x005f:
            boolean r2 = r15.A00     // Catch:{ all -> 0x00a9 }
            if (r2 != 0) goto L_0x006c
            boolean r2 = r15.A07()     // Catch:{ all -> 0x00a9 }
            if (r2 == 0) goto L_0x006c
            r15.A05()     // Catch:{ all -> 0x00a9 }
        L_0x006c:
            boolean r2 = r15.A00     // Catch:{ all -> 0x00a9 }
            if (r2 == 0) goto L_0x0027
            goto L_0x0075
        L_0x0071:
            r15.A06(r4, r5)     // Catch:{ all -> 0x00a9 }
            goto L_0x005f
        L_0x0075:
            r2 = -904747883(0xffffffffca12a495, float:-2402597.2)
            X.AnonymousClass00C.A00(r0, r2)
            return
        L_0x007c:
            r3 = move-exception
            java.lang.Throwable r2 = r3.getCause()     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x0084
            r3 = r2
        L_0x0084:
            boolean r2 = r3 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x008b
            java.lang.RuntimeException r3 = (java.lang.RuntimeException) r3     // Catch:{ all -> 0x0098 }
            throw r3     // Catch:{ all -> 0x0098 }
        L_0x008b:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x0098 }
            r2.<init>(r3)     // Catch:{ all -> 0x0098 }
            throw r2     // Catch:{ all -> 0x0098 }
        L_0x0091:
            r3 = move-exception
            java.lang.AssertionError r2 = new java.lang.AssertionError     // Catch:{ all -> 0x0098 }
            r2.<init>(r3)     // Catch:{ all -> 0x0098 }
            throw r2     // Catch:{ all -> 0x0098 }
        L_0x0098:
            r3 = move-exception
            r2 = -441152943(0xffffffffe5b48a51, float:-1.0657218E23)
            goto L_0x00a5
        L_0x009d:
            java.lang.AssertionError r3 = new java.lang.AssertionError     // Catch:{ all -> 0x00a9 }
            java.lang.String r2 = "MessagePumper has thread affinity"
            r3.<init>(r2)     // Catch:{ all -> 0x00a9 }
            goto L_0x00a8
        L_0x00a5:
            com.facebook.profilo.logger.api.ProfiloLogger.waitEnd(r4, r2)     // Catch:{ all -> 0x00a9 }
        L_0x00a8:
            throw r3     // Catch:{ all -> 0x00a9 }
        L_0x00a9:
            r3 = move-exception
            r2 = 1050586442(0x3e9ead4a, float:0.30991584)
            X.AnonymousClass00C.A00(r0, r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass009.A04():void");
    }

    public void A06(Handler handler, Message message) {
        handler.dispatchMessage(message);
        A03(message, 0);
        message.recycle();
    }

    public AnonymousClass009() {
        this(Looper.myLooper());
    }

    private AnonymousClass009(Looper looper) {
        if (A08) {
            C000500j r0 = new C000500j(this, looper);
            this.A02 = r0;
            this.A01 = r0.obtainMessage();
            return;
        }
        throw new AssertionError("init not called");
    }
}
