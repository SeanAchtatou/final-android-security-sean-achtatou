package X;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import com.facebook.common.callercontext.CallerContext;

/* renamed from: X.20u  reason: invalid class name and case insensitive filesystem */
public final class C402320u extends C13470rU implements AnonymousClass09C {
    public static final String __redex_internal_original_name = "com.facebook.fbservice.ops.BlueServiceFragment";
    public Bundle A00;
    public CallerContext A01;
    public C402520w A02;
    public C402620x A03;
    public String A04;
    public boolean A05;
    private C156107Jr A06;

    public void A2K(String str, Bundle bundle) {
        A2L(str, bundle, null);
    }

    public void A2L(String str, Bundle bundle, CallerContext callerContext) {
        if (this.A05) {
            this.A03.A08(str, false, bundle, callerContext);
            return;
        }
        this.A04 = str;
        this.A00 = bundle;
        this.A01 = callerContext;
    }

    public void A2J(C156107Jr r2) {
        if (this.A05) {
            this.A03.A07(r2);
        } else {
            this.A06 = r2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r2 == X.AnonymousClass2G1.A01) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A2M() {
        /*
            r3 = this;
            boolean r0 = r3.A05
            if (r0 == 0) goto L_0x0015
            X.20x r0 = r3.A03
            X.2G1 r2 = r0.A05
            X.2G1 r0 = X.AnonymousClass2G1.INIT
            if (r2 == r0) goto L_0x0011
            X.2G1 r0 = X.AnonymousClass2G1.COMPLETED
            r1 = 1
            if (r2 != r0) goto L_0x0012
        L_0x0011:
            r1 = 0
        L_0x0012:
            r0 = 1
            if (r1 != 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C402320u.A2M():boolean");
    }

    public static C402320u A00(Fragment fragment, String str) {
        return A04(fragment.A17(), str);
    }

    public static C402320u A04(C13060qW r2, String str) {
        C402320u r1 = (C402320u) r2.A0Q(str);
        if (r1 != null) {
            return r1;
        }
        C402320u r12 = new C402320u();
        C16290wo A0T = r2.A0T();
        A0T.A0C(r12, str);
        A0T.A02();
        return r12;
    }

    public void A1l() {
        int A022 = C000700l.A02(-871677533);
        super.A1l();
        C402620x r2 = this.A03;
        r2.A0E = true;
        C402620x.A05(r2);
        r2.A08 = null;
        r2.A04 = null;
        r2.A03 = null;
        C156107Jr r0 = r2.A06;
        if (r0 != null) {
            r0.CI0();
        }
        this.A02 = null;
        C000700l.A08(446654863, A022);
    }

    public void A1s(Context context) {
        super.A1s(context);
        this.A03 = C402620x.A00(AnonymousClass1XX.get(A1j()));
    }

    public void A1t(Bundle bundle) {
        int A022 = C000700l.A02(726671251);
        super.A1t(bundle);
        C402620x r3 = this.A03;
        r3.A0C = true;
        r3.A03 = new AnonymousClass2SJ(this);
        r3.A04 = new AnonymousClass2G3(this);
        if (bundle != null && this.A04 == null) {
            r3.A05 = (AnonymousClass2G1) bundle.getSerializable("operationState");
            r3.A0B = bundle.getString("type");
            boolean z = false;
            if (bundle.getInt("useExceptionResult") != 0) {
                z = true;
            }
            r3.A0H = z;
            r3.A00 = (Bundle) bundle.getParcelable("param");
            r3.A02 = (CallerContext) bundle.getParcelable(C99084oO.$const$string(3));
            r3.A0A = bundle.getString("operationId");
            if (Looper.myLooper() != null) {
                r3.A01 = new Handler();
            }
            AnonymousClass2G1 r1 = r3.A05;
            if (r1 != AnonymousClass2G1.INIT && (r1 == AnonymousClass2G1.A04 || r1 == AnonymousClass2G1.A03)) {
                C156107Jr r0 = r3.A06;
                if (r0 != null) {
                    r0.APp();
                }
                C402620x.A02(r3);
            }
        }
        this.A03.A07(this.A06);
        this.A06 = null;
        this.A05 = true;
        String str = this.A04;
        if (str != null) {
            this.A03.A08(str, false, this.A00, this.A01);
            this.A04 = null;
            this.A00 = null;
            this.A01 = null;
        }
        C000700l.A08(-229593358, A022);
    }

    public void A1u(Bundle bundle) {
        super.A1u(bundle);
        C402620x r2 = this.A03;
        bundle.putSerializable("operationState", r2.A05);
        bundle.putString("type", r2.A0B);
        bundle.putInt("useExceptionResult", r2.A0H ? 1 : 0);
        bundle.putParcelable("param", r2.A00);
        bundle.putParcelable(C99084oO.$const$string(3), r2.A02);
        bundle.putString("operationId", r2.A0A);
    }
}
