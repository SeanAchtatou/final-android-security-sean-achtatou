package X;

/* renamed from: X.0CI  reason: invalid class name */
public final class AnonymousClass0CI extends Exception {
    public AnonymousClass0CI() {
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CI(Integer num) {
        super(1 - num.intValue() != 0 ? "NOT_CONNECTED" : "REF_CODE_EXPIRED");
    }

    public AnonymousClass0CI(String str, Throwable th) {
        super(str, th);
    }
}
