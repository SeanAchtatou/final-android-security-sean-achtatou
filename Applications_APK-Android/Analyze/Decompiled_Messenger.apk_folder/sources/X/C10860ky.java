package X;

import com.google.common.base.Preconditions;
import java.util.Iterator;

/* renamed from: X.0ky  reason: invalid class name and case insensitive filesystem */
public final class C10860ky implements AnonymousClass191 {
    private Object A00;
    private boolean A01;
    private final Iterator A02;

    public boolean hasNext() {
        if (this.A01 || this.A02.hasNext()) {
            return true;
        }
        return false;
    }

    public Object next() {
        if (!this.A01) {
            return this.A02.next();
        }
        Object obj = this.A00;
        this.A01 = false;
        this.A00 = null;
        return obj;
    }

    public Object peek() {
        if (!this.A01) {
            this.A00 = this.A02.next();
            this.A01 = true;
        }
        return this.A00;
    }

    public void remove() {
        Preconditions.checkState(!this.A01, "Can't remove after you've peeked at next");
        this.A02.remove();
    }

    public C10860ky(Iterator it) {
        Preconditions.checkNotNull(it);
        this.A02 = it;
    }
}
