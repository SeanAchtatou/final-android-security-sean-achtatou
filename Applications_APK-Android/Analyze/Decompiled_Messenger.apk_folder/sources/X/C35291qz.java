package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1qz  reason: invalid class name and case insensitive filesystem */
public final class C35291qz extends C16070wR {
    public static final MigColorScheme A0A = C17190yT.A00();
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C15340v8 A01;
    @Comparable(type = 13)
    public C10700ki A02;
    @Comparable(type = 13)
    public C15460vK A03;
    @Comparable(type = 13)
    public MigColorScheme A04 = A0A;
    @Comparable(type = 13)
    public C191917d A05;
    @Comparable(type = 13)
    public C16450x5 A06;
    @Comparable(type = 13)
    public C15560vU A07;
    @Comparable(type = 5)
    public ImmutableList A08;
    @Comparable(type = 13)
    public Integer A09;

    public C35291qz(Context context) {
        super("ThreadListSection");
        this.A00 = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }
}
