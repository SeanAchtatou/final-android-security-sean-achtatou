package X;

import java.util.Arrays;

/* renamed from: X.1z8  reason: invalid class name and case insensitive filesystem */
public final class C39491z8 extends C28248DrN {
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals((Object[]) this.A00, (Object[]) ((C39491z8) obj).A00);
    }

    public String toString() {
        Object[] objArr = (Object[]) this.A00;
        StringBuilder sb = new StringBuilder("[");
        int i = 0;
        while (true) {
            int length = objArr.length;
            if (i < length) {
                Object obj = objArr[i];
                if (obj instanceof String) {
                    sb.append("\"");
                    sb.append(obj);
                    sb.append("\"");
                } else {
                    sb.append(obj);
                }
                if (i != length - 1) {
                    sb.append(", ");
                }
                i++;
            } else {
                sb.append("]");
                return sb.toString();
            }
        }
    }

    public C39491z8(Object[] objArr) {
        super(objArr);
    }
}
