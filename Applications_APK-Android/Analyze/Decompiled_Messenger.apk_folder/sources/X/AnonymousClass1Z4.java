package X;

import android.view.View;
import com.facebook.acra.ACRA;
import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.forker.Process;
import java.util.LinkedList;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.1Z4  reason: invalid class name */
public final class AnonymousClass1Z4 {
    private static final Object A03 = new Object();
    private static volatile AnonymousClass1Z4 A04;
    public AnonymousClass0UN A00;
    private final Object A01 = new Object();
    private final WeakHashMap A02 = new WeakHashMap();

    public static final AnonymousClass1Z4 A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass1Z4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass1Z4(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void A01() {
        if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A04()) {
            C28461eq r5 = (C28461eq) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aq4, this.A00);
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(2, AnonymousClass1Y3.APr, r5.A00)).AOx();
            Semaphore semaphore = new Semaphore(0);
            C190958ub r4 = new C190958ub(semaphore);
            r5.A04(r4);
            ((AnonymousClass069) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BBa, r5.A00)).now();
            try {
                semaphore.tryAcquire(StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS, TimeUnit.MILLISECONDS);
            } catch (InterruptedException unused) {
            } catch (Throwable th) {
                ((AnonymousClass069) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BBa, r5.A00)).now();
                C28461eq.A01(r5, r4);
                throw th;
            }
            ((AnonymousClass069) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BBa, r5.A00)).now();
            C28461eq.A01(r5, r4);
            return;
        }
        AnonymousClass0YT r52 = (AnonymousClass0YT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A01, this.A00);
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.APr, r52.A00)).AOx();
        Semaphore semaphore2 = new Semaphore(0);
        C190948ua r42 = new C190948ua(semaphore2);
        try {
            System.currentTimeMillis();
            r52.A05(r42);
            if (!semaphore2.tryAcquire(StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS, TimeUnit.MILLISECONDS)) {
                long now = ((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, r52.A00)).now();
                WeakHashMap weakHashMap = new WeakHashMap();
                synchronized (r52.A01) {
                    try {
                        weakHashMap.putAll(r52.A06);
                    } catch (Throwable th2) {
                        while (true) {
                            th = th2;
                            break;
                        }
                    }
                }
                LinkedList<View> linkedList = null;
                for (Map.Entry entry : weakHashMap.entrySet()) {
                    if (now - ((Long) entry.getValue()).longValue() >= 60000) {
                        if (linkedList == null) {
                            linkedList = new LinkedList<>();
                        }
                        linkedList.add(entry.getKey());
                        ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, r52.A00)).CGS("DefaultUserInteractionController", AnonymousClass08S.A0P("View ", ((View) entry.getKey()).getClass().getName(), " may not have ended it's user interaction event"));
                    }
                }
                if (linkedList != null) {
                    synchronized (r52.A01) {
                        try {
                            for (View remove : linkedList) {
                                r52.A06.remove(remove);
                            }
                            r52.A06.size();
                        } catch (Throwable th3) {
                            while (true) {
                                th = th3;
                                break;
                            }
                        }
                    }
                    AnonymousClass0YT.A02(r52);
                }
            }
            System.currentTimeMillis();
            synchronized (r52.A02) {
                try {
                    r52.A07.remove(r42);
                } catch (Throwable th4) {
                    th = th4;
                    throw th;
                }
            }
        } catch (InterruptedException e) {
            C010708t.A08(AnonymousClass0YT.A08, "Exception when the user interaction to be finished.", e);
        }
    }

    public void A02(View view) {
        Object obj;
        if (view == null) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Amr, this.A00)).CGS("BusySignalHandlerMigrationHelper", "Null key is always considered same with other null key, which is highly not recommended.");
        }
        if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A03()) {
            C28461eq r1 = (C28461eq) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aq4, this.A00);
            if (view == null) {
                obj = A03;
            } else {
                obj = view;
            }
            r1.A08(obj);
            if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A04()) {
                return;
            }
        }
        ((AnonymousClass0YT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A01, this.A00)).A03(view);
    }

    public void A03(C05130Xt r6) {
        if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A04()) {
            synchronized (this.A01) {
                C190928uY r3 = new C190928uY(this, r6);
                ((C28461eq) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aq4, this.A00)).A04(r3);
                this.A02.put(r6, r3);
            }
            return;
        }
        ((AnonymousClass0YT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A01, this.A00)).A05(r6);
    }

    public void A04(Integer num, View view) {
        Object obj;
        String str;
        if (view == null) {
            AnonymousClass09P r2 = (AnonymousClass09P) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Amr, this.A00);
            switch (num.intValue()) {
                case 1:
                    str = "ACTIVITY_LAUNCH";
                    break;
                case 2:
                    str = "TTRC_QPL";
                    break;
                case 3:
                    str = "TTRC_QPL_MOBILE_BOOST";
                    break;
                case 4:
                    str = "TOUCH";
                    break;
                case 5:
                    str = "SCROLLING";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    str = "SILENT_LOGIN";
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                    str = "FB_REACT_TTI";
                    break;
                case 8:
                    str = "VIDEO_CHAT_LINK_TAP";
                    break;
                case Process.SIGKILL:
                    str = "WEB_RTC_IN_CALL";
                    break;
                case AnonymousClass1Y3.A01:
                    str = "PAGE_ABOUT_HEADER_LOAD";
                    break;
                case AnonymousClass1Y3.A02:
                    str = "PAGE_HEADER_FETCH";
                    break;
                case AnonymousClass1Y3.A03:
                    str = "PAGES_TIMELINE_FETCH";
                    break;
                case 13:
                    str = "SIMPLE_PICKER_DATA_LOAD";
                    break;
                case 14:
                    str = "STICKER_KEYBOARD_METADATA_LOAD";
                    break;
                case 15:
                    str = "STICKER_GRID_LOAD";
                    break;
                case 16:
                    str = "MAP_CAMERA_MOVE";
                    break;
                case 17:
                    str = "STORY_VIEWER_ON";
                    break;
                case Process.SIGCONT:
                    str = "MESSAGE_CHAT_HEAD_POP";
                    break;
                case Process.SIGSTOP:
                    str = "MESSAGE_MONTAGE_THREAD_LOAD";
                    break;
                case 20:
                    str = "ORCA_THREAD_VIEW_LOAD";
                    break;
                case AnonymousClass1Y3.A05:
                    str = "ORCA_THREAD_LIST_LOAD";
                    break;
                default:
                    str = "START_UP";
                    break;
            }
            r2.CGS("BusySignalHandlerMigrationHelper", AnonymousClass08S.A0J("Null key is always considered same with other null key, which is highly not recommended. signalType = ", str));
        }
        if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A03()) {
            C28461eq r1 = (C28461eq) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aq4, this.A00);
            if (view == null) {
                obj = A03;
            } else {
                obj = view;
            }
            r1.A09(obj);
            if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A04()) {
                return;
            }
        }
        ((AnonymousClass0YT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A01, this.A00)).A04(view);
    }

    public boolean A05() {
        if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A04()) {
            return ((C28461eq) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Aq4, this.A00)).A0A();
        }
        return ((AnonymousClass0YT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A01, this.A00)).A06();
    }

    private AnonymousClass1Z4(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
