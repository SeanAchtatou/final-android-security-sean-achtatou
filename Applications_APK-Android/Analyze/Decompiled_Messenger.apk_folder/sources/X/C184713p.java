package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.13p  reason: invalid class name and case insensitive filesystem */
public final class C184713p {
    private static volatile C184713p A01;
    public final C25051Yd A00;

    public static final C184713p A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C184713p.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C184713p(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C184713p(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
