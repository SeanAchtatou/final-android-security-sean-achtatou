package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import com.google.common.base.Preconditions;

/* renamed from: X.0Us  reason: invalid class name and case insensitive filesystem */
public abstract class C04450Us implements C04460Ut {
    public final Handler A00;

    public void A01(BroadcastReceiver broadcastReceiver) {
        Context context;
        if (this instanceof AnonymousClass1ZK) {
            context = ((AnonymousClass1ZK) this).A00;
        } else if (!(this instanceof C04440Ur)) {
            context = ((C07510dg) this).A00;
        } else {
            ((C04440Ur) this).A00.A01(broadcastReceiver);
            return;
        }
        context.unregisterReceiver(broadcastReceiver);
    }

    public void A02(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter, Handler handler) {
        if (this instanceof AnonymousClass1ZK) {
            AnonymousClass1ZK r0 = (AnonymousClass1ZK) this;
            r0.A00.registerReceiver(broadcastReceiver, intentFilter, r0.A01, handler);
        } else if (!(this instanceof C04440Ur)) {
            ((C07510dg) this).A00.registerReceiver(broadcastReceiver, intentFilter, null, handler);
        } else {
            ((C04440Ur) this).A00.A03(broadcastReceiver, intentFilter, handler != null ? handler.getLooper() : null);
        }
    }

    public C06600bl BMm() {
        return new C06600bl(this);
    }

    public C04450Us(Handler handler) {
        this.A00 = handler;
    }

    public void C4y(String str) {
        Preconditions.checkNotNull(str);
        C4x(new Intent(str));
    }
}
