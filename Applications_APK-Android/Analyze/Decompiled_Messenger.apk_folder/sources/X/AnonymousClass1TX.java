package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.attachment.AttachmentImageMap;

/* renamed from: X.1TX  reason: invalid class name */
public final class AnonymousClass1TX implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new AttachmentImageMap(parcel);
    }

    public Object[] newArray(int i) {
        return new AttachmentImageMap[i];
    }
}
