package X;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;

/* renamed from: X.0tF  reason: invalid class name and case insensitive filesystem */
public final class C14400tF extends Fragment {
    public AnonymousClass1XP A00;

    public static void A00(Activity activity) {
        if (Build.VERSION.SDK_INT >= 29) {
            activity.registerActivityLifecycleCallbacks(new C27991e5());
        }
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new C14400tF(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    public static void A01(Activity activity, C14820uB r2) {
        if (activity instanceof C14830uC) {
            ((C14830uC) activity).getLifecycle().A08(r2);
        } else if (activity instanceof C11410n6) {
            AnonymousClass0qM AsK = ((C11410n6) activity).AsK();
            if (AsK instanceof C12990qL) {
                ((C12990qL) AsK).A08(r2);
            }
        }
    }

    private void A02(C14820uB r3) {
        if (Build.VERSION.SDK_INT < 29) {
            A01(getActivity(), r3);
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        AnonymousClass1XP r0 = this.A00;
        if (r0 != null) {
            r0.BUd();
        }
        A02(C14820uB.ON_CREATE);
    }

    public void onDestroy() {
        super.onDestroy();
        A02(C14820uB.ON_DESTROY);
        this.A00 = null;
    }

    public void onPause() {
        super.onPause();
        A02(C14820uB.ON_PAUSE);
    }

    public void onResume() {
        super.onResume();
        AnonymousClass1XP r0 = this.A00;
        if (r0 != null) {
            r0.onResume();
        }
        A02(C14820uB.ON_RESUME);
    }

    public void onStart() {
        super.onStart();
        AnonymousClass1XP r0 = this.A00;
        if (r0 != null) {
            r0.onStart();
        }
        A02(C14820uB.ON_START);
    }

    public void onStop() {
        super.onStop();
        A02(C14820uB.ON_STOP);
    }
}
