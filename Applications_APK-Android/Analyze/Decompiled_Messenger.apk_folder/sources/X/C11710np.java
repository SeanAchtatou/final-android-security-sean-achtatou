package X;

import java.io.Closeable;
import java.io.Flushable;
import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.0np  reason: invalid class name and case insensitive filesystem */
public abstract class C11710np implements Closeable, AnonymousClass0jN, Flushable {
    public C10210jj _cfgPrettyPrinter;

    public abstract void close();

    public abstract void flush();

    public abstract C09980jK getCodec();

    public abstract C11710np setCodec(C09980jK r1);

    public abstract C11710np useDefaultPrettyPrinter();

    public abstract C11780nw version();

    public abstract void writeBinary(C10350jx r1, byte[] bArr, int i, int i2);

    public abstract void writeBoolean(boolean z);

    public abstract void writeEndArray();

    public abstract void writeEndObject();

    public abstract void writeFieldName(C10240jm r1);

    public abstract void writeFieldName(String str);

    public abstract void writeNull();

    public abstract void writeNumber(double d);

    public abstract void writeNumber(float f);

    public abstract void writeNumber(int i);

    public abstract void writeNumber(long j);

    public abstract void writeNumber(String str);

    public abstract void writeNumber(BigDecimal bigDecimal);

    public abstract void writeNumber(BigInteger bigInteger);

    public abstract void writeObject(Object obj);

    public abstract void writeRaw(char c);

    public abstract void writeRaw(String str);

    public abstract void writeRaw(char[] cArr, int i, int i2);

    public abstract void writeRawValue(String str);

    public abstract void writeStartArray();

    public abstract void writeStartObject();

    public abstract void writeString(C10240jm r1);

    public abstract void writeString(String str);

    public abstract void writeString(char[] cArr, int i, int i2);

    public final void writeObjectField(String str, Object obj) {
        writeFieldName(str);
        writeObject(obj);
    }

    public void writeStringField(String str, String str2) {
        writeFieldName(str);
        writeString(str2);
    }

    public void writeNumber(short s) {
        writeNumber((int) s);
    }

    public void writeRaw(C10240jm r2) {
        writeRaw(r2.getValue());
    }
}
