package X;

import android.util.Log;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0Yz  reason: invalid class name and case insensitive filesystem */
public final class C05450Yz extends AnonymousClass0Wl {
    public final FbSharedPreferences A00;
    private final AnonymousClass0US A01;

    public String getSimpleName() {
        return "FbAppInitializerInternal";
    }

    public C05450Yz(FbSharedPreferences fbSharedPreferences, AnonymousClass0US r2) {
        this.A00 = fbSharedPreferences;
        this.A01 = r2;
    }

    public void init() {
        int A03 = C000700l.A03(837124414);
        this.A00.BCy();
        if (C006006f.A01()) {
            Map map = C006006f.A00;
            if (map == null) {
                C006006f.A00 = new HashMap();
                String A002 = C006006f.A00("sharedprefs", false);
                if (A002 != null) {
                    try {
                        JSONObject jSONObject = new JSONObject(A002);
                        Iterator<String> keys = jSONObject.keys();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            C006006f.A00.put(next, (JSONObject) jSONObject.get(next));
                        }
                    } catch (JSONException e) {
                        throw new RuntimeException("Malformed json for shared preferences", e);
                    }
                }
                map = C006006f.A00;
            }
            if (map != null) {
                C30281hn edit = this.A00.edit();
                for (String str : map.keySet()) {
                    JSONObject jSONObject2 = (JSONObject) map.get(str);
                    try {
                        String string = jSONObject2.getString("type");
                        String string2 = jSONObject2.getString("value");
                        AnonymousClass1Y7 r2 = (AnonymousClass1Y7) C04350Ue.A05.A09(str.substring(1, str.length()));
                        if (string.equals("Boolean")) {
                            edit.putBoolean(r2, Boolean.valueOf(string2).booleanValue());
                        } else if (string.equals("Long")) {
                            edit.BzA(r2, Long.valueOf(string2).longValue());
                        } else if (string.equals("Integer")) {
                            edit.Bz6(r2, Integer.valueOf(string2).intValue());
                        } else if (string.equals("String")) {
                            edit.BzC(r2, string2);
                        }
                    } catch (JSONException e2) {
                        C010708t.A0E(AnonymousClass0Y7.A0D, e2, "Failed to override shared preference for E2E tests", new Object[0]);
                    }
                }
                edit.commit();
            }
            String A003 = C006006f.A00("fb.e2e.sandbox_override", true);
            if (A003 != null && !A003.equals(BuildConfig.FLAVOR)) {
                String A05 = AnonymousClass0OY.A02.A05();
                AnonymousClass1Y7 r1 = C04350Ue.A05;
                String A052 = AnonymousClass0OY.A00.A05();
                Log.w(TurboLoader.Locator.$const$string(111), String.format("Using domain override: %s", A003));
                C30281hn edit2 = this.A00.edit();
                edit2.BzC((AnonymousClass1Y7) r1.A09(A05.substring(1, A05.length())), A003);
                edit2.putBoolean((AnonymousClass1Y7) r1.A09(A052.substring(1, A052.length())), false);
                edit2.commit();
            }
        }
        ((C05600Zq) this.A01.get()).A02();
        C000700l.A09(-796833171, A03);
    }
}
