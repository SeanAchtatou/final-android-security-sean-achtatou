package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0xn  reason: invalid class name and case insensitive filesystem */
public final class C16800xn extends LinkedHashMap<String, String> {
    public static final C16800xn instance = new C16800xn();

    public synchronized String intern(String str) {
        String str2;
        str2 = (String) get(str);
        if (str2 == null) {
            str2 = str.intern();
            put(str2, str2);
        }
        return str2;
    }

    private C16800xn() {
        super(100, 0.8f, true);
    }

    public boolean removeEldestEntry(Map.Entry entry) {
        if (size() > 100) {
            return true;
        }
        return false;
    }
}
