package X;

import java.util.List;

/* renamed from: X.1Om  reason: invalid class name and case insensitive filesystem */
public final class C23171Om extends C30771ic {
    private final C30151EqV A00;
    private final C23201Op A01;
    private final C23211Oq A02;
    private final C23191Oo[] A03;

    public C23171Om(C23121Oh r3, C23141Oj r4, C23091Oe r5, AnonymousClass1N6 r6, C23051Oa r7) {
        super(r3, r4, 1, r7);
        C23201Op A012 = C23181On.A01(r4.A01);
        this.A01 = A012;
        C23191Oo[] A022 = C23181On.A02(r4.A02, A012);
        this.A03 = A022;
        this.A02 = new C23211Oq(this, A022);
        List list = r4.A00;
        if (list != null && !list.isEmpty()) {
            this.A00 = C23181On.A00(r4.A00, r5, r6);
        }
    }

    public C30152EqW[] AiU() {
        return null;
    }

    public C30152EqW[] Auv() {
        C30151EqV eqV = this.A00;
        if (eqV != null) {
            return eqV.A02;
        }
        return null;
    }

    public int Ax8(String str) {
        return this.A01.A00(str);
    }

    public C23211Oq C3k(AnonymousClass1P6 r8) {
        C23191Oo[] r4;
        C30151EqV eqV = this.A00;
        if (eqV != null) {
            r4 = C30146EqQ.A00(eqV, r8);
        } else {
            r4 = null;
        }
        this.A06.C07(this, null, null, r4, this.A03, Acu());
        return this.A02;
    }
}
