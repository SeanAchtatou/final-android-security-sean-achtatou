package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1vT  reason: invalid class name */
public final class AnonymousClass1vT extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C200319bn A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 13)
    public CharSequence A03;
    @Comparable(type = 13)
    public CharSequence A04;
    @Comparable(type = 13)
    public CharSequence A05;
    @Comparable(type = 13)
    public CharSequence A06;
    @Comparable(type = 13)
    public CharSequence A07;
    @Comparable(type = 3)
    public boolean A08;
    @Comparable(type = 3)
    public boolean A09;
    @Comparable(type = 3)
    public boolean A0A;
    @Comparable(type = 3)
    public boolean A0B;

    public AnonymousClass1vT(Context context) {
        super("M4AutoDownloadSettingPreferenceLayout");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
