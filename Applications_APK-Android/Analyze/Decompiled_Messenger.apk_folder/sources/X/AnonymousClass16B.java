package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.16B  reason: invalid class name */
public final class AnonymousClass16B extends AnonymousClass169 {
    public static final String __redex_internal_original_name = "com.google.common.util.concurrent.AbstractTransformFuture$AsyncTransformFuture";

    public AnonymousClass16B(ListenableFuture listenableFuture, AnonymousClass0YC r2) {
        super(listenableFuture, r2);
    }
}
