package X;

/* renamed from: X.06b  reason: invalid class name and case insensitive filesystem */
public final class C005706b {
    public static final AnonymousClass1Y7 A00 = ((AnonymousClass1Y7) A03.A09("last_remaining_bytes_update_time"));
    public static final AnonymousClass1Y7 A01 = ((AnonymousClass1Y7) ((AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("profilo/")).A09("manual_tracing/")).A09("enabled"));
    public static final AnonymousClass1Y7 A02;
    private static final AnonymousClass1Y7 A03;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A06.A09("dextr/");
        A03 = r1;
        A02 = (AnonymousClass1Y7) r1.A09("remaining_bytes");
    }
}
