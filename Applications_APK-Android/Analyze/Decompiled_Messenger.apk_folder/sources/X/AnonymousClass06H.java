package X;

import com.facebook.acra.anr.AppStateUpdater;

/* renamed from: X.06H  reason: invalid class name */
public final class AnonymousClass06H extends AppStateUpdater {
    private boolean A00 = false;
    private final C010608s A01 = new C010508r(this);

    public void addForegroundTransitionListener(C010608s r6) {
        super.addForegroundTransitionListener(r6);
        synchronized (this.A01) {
            if (!this.A00) {
                C010608s r3 = this.A01;
                synchronized (AnonymousClass01P.A0Z) {
                    try {
                        if (AnonymousClass01P.A0Y == null) {
                            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
                        } else {
                            synchronized (AnonymousClass01P.A0Y.A0H) {
                                try {
                                    AnonymousClass01P r1 = AnonymousClass01P.A0Y;
                                    if (r1.A02 == null) {
                                        r1.A02 = r3;
                                    } else {
                                        throw new IllegalStateException("Only one listener at a time supported");
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    throw th;
                                }
                            }
                        }
                    } catch (Throwable th2) {
                        while (true) {
                            th = th2;
                        }
                        throw th;
                    }
                }
                this.A00 = true;
            }
        }
    }

    public boolean isAppInForegroundV1() {
        return AnonymousClass01P.A0D();
    }

    public boolean isAppInForegroundV2() {
        return AnonymousClass01P.A0E();
    }

    public void removeForegroundTransitionListener(C010608s r5) {
        super.removeForegroundTransitionListener(r5);
        synchronized (this.A01) {
            if (this.A00 && getForegroundTransitionListenerCount() == 0) {
                synchronized (AnonymousClass01P.A0Z) {
                    try {
                        if (AnonymousClass01P.A0Y == null) {
                            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
                        } else {
                            synchronized (AnonymousClass01P.A0Y.A0H) {
                                try {
                                    AnonymousClass01P.A0Y.A02 = null;
                                } catch (Throwable th) {
                                    th = th;
                                    throw th;
                                }
                            }
                        }
                    } catch (Throwable th2) {
                        while (true) {
                            th = th2;
                        }
                        throw th;
                    }
                }
                this.A00 = false;
            }
        }
    }

    public void updateAnrState(AnonymousClass04f r4, Runnable runnable) {
        synchronized (AnonymousClass01P.A0Z) {
            if (AnonymousClass01P.A0Y == null) {
                C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            } else {
                C001901i.A04(AnonymousClass01P.A0Y.A0B, r4, runnable, false, false);
            }
        }
    }

    public void updateAnrState(AnonymousClass04f r4, Runnable runnable, boolean z) {
        synchronized (AnonymousClass01P.A0Z) {
            if (AnonymousClass01P.A0Y == null) {
                C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            } else {
                C001901i.A04(AnonymousClass01P.A0Y.A0B, r4, runnable, true, z);
            }
        }
    }
}
