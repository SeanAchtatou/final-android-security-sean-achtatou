package X;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.os.Build;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: X.22V  reason: invalid class name */
public final class AnonymousClass22V implements AnonymousClass7e4 {
    public final /* synthetic */ C159867az A00;

    public AnonymousClass22V(C159867az r1) {
        this.A00 = r1;
    }

    public void APA() {
        Map map;
        String str;
        BluetoothHeadset bluetoothHeadset;
        int i;
        if (C159867az.A0o(this.A00)) {
            ((C161607dx) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AGl, this.A00.A08)).A04();
        }
        C161607dx r1 = (C161607dx) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AGl, this.A00.A08);
        C162537fX r4 = r1.A01;
        if (r4 != C162537fX.BLUETOOTH || (bluetoothHeadset = r1.A09.A02.A02) == null) {
            map = null;
        } else {
            ArrayList arrayList = new ArrayList(1);
            for (BluetoothDevice next : bluetoothHeadset.getConnectedDevices()) {
                String address = next.getAddress();
                String name = next.getName();
                String bluetoothClass = next.getBluetoothClass().toString();
                if (Build.VERSION.SDK_INT >= 18) {
                    i = next.getType();
                } else {
                    i = -1;
                }
                arrayList.add(C162957gF.A00("address", address, "name", name, "class", bluetoothClass, "type", Integer.toString(i)));
            }
            map = C162957gF.A00("bluetooth_devices", arrayList.toString());
        }
        C163497hM r2 = this.A00.A0n;
        switch (r4.ordinal()) {
            case 0:
                str = "EARPIECE";
                break;
            case 1:
                str = "SPEAKERPHONE";
                break;
            case 2:
                str = AnonymousClass80H.$const$string(27);
                break;
            case 3:
                str = "HEADSET";
                break;
            default:
                str = null;
                break;
        }
        Preconditions.checkNotNull(str);
        C163497hM.A07(r2, AnonymousClass80H.$const$string(81), str, map);
        C159867az.A0T(this.A00);
    }
}
