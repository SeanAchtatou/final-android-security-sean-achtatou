package X;

import androidx.fragment.app.Fragment;

/* renamed from: X.1gT  reason: invalid class name and case insensitive filesystem */
public final class C29471gT {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public Fragment A05;
    public AnonymousClass1XL A06;
    public AnonymousClass1XL A07;

    public C29471gT() {
    }

    public C29471gT(int i, Fragment fragment) {
        this.A00 = i;
        this.A05 = fragment;
        AnonymousClass1XL r0 = AnonymousClass1XL.RESUMED;
        this.A07 = r0;
        this.A06 = r0;
    }

    public C29471gT(int i, Fragment fragment, AnonymousClass1XL r4) {
        this.A00 = i;
        this.A05 = fragment;
        this.A07 = fragment.A02;
        this.A06 = r4;
    }
}
