package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* renamed from: X.0ac  reason: invalid class name and case insensitive filesystem */
public final class C05960ac extends C07370dJ implements AnonymousClass06U {
    private final IntentFilter A00;
    private final AnonymousClass0US A01;
    private final AnonymousClass1ZJ A02;

    public final boolean A08(String str) {
        return false;
    }

    public final AnonymousClass06U A02(Context context, String str) {
        if (this.A00.matchAction(str)) {
            return this;
        }
        return null;
    }

    public C05960ac(AnonymousClass0US r1, IntentFilter intentFilter, AnonymousClass1ZJ r3) {
        this.A01 = r1;
        this.A00 = intentFilter;
        this.A02 = r3;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r5) {
        int A002 = AnonymousClass09Y.A00(2036043051);
        ((AnonymousClass06U) this.A01.get()).Bl1(context, intent, r5);
        this.A02.A01.getAndIncrement();
        AnonymousClass09Y.A01(524568941, A002);
    }
}
