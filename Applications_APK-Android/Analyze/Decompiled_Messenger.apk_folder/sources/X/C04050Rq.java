package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0Rq  reason: invalid class name and case insensitive filesystem */
public final class C04050Rq {
    public final List A00;

    public String toString() {
        return TextUtils.join(",", this.A00.toArray());
    }

    public C04050Rq(List list) {
        this.A00 = Collections.unmodifiableList(new ArrayList(list));
    }
}
