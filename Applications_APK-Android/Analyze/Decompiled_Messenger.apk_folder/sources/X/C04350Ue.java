package X;

/* renamed from: X.0Ue  reason: invalid class name and case insensitive filesystem */
public final class C04350Ue {
    public static final AnonymousClass1Y7 A00 = ((AnonymousClass1Y7) A05.A09("config/"));
    public static final AnonymousClass1Y7 A01 = ((AnonymousClass1Y7) A05.A09("dash/"));
    public static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) A05.A09("fb_android/"));
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04 = ((AnonymousClass1Y7) A05.A09("prefs_user_id"));
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) A05.A09("settings/"));
    public static final AnonymousClass1Y7 A07 = ((AnonymousClass1Y7) A05.A09("shared/"));
    public static final AnonymousClass1Y8 A08;
    public static final AnonymousClass1Y8 A09;
    public static final AnonymousClass1Y8 A0A;
    public static final AnonymousClass1Y8 A0B;
    public static final AnonymousClass1Y8 A0C;

    static {
        AnonymousClass1Y7 r0 = new AnonymousClass1Y7("/");
        A05 = r0;
        A03 = (AnonymousClass1Y7) r0.A09("prefs/");
        AnonymousClass1Y8 r2 = new AnonymousClass1Y8("/", false);
        A0A = r2;
        A09 = new AnonymousClass1Y8(r2, "prefs/", r2.A00);
        AnonymousClass1Y8 r22 = A0A;
        A0B = new AnonymousClass1Y8(r22, "settings/", r22.A00);
        A08 = new AnonymousClass1Y8(r22, "config/", r22.A00);
        A0C = new AnonymousClass1Y8(r22, "shared/", r22.A00);
        r22.A09("fb_android/");
    }
}
