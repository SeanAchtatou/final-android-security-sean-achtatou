package X;

import android.net.Uri;
import com.facebook.graphql.enums.GraphQLMessengerAdProductType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.user.model.User;
import com.facebook.user.profilepic.PicSquare;
import com.facebook.user.profilepic.PicSquareUrlWithSize;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

/* renamed from: X.1ru  reason: invalid class name and case insensitive filesystem */
public final class C35841ru {
    public final C14500tS A00;
    public final C35881ry A01;

    public static User A03(GSTModelShape1S0000000 gSTModelShape1S0000000, ImmutableList.Builder builder) {
        boolean z;
        boolean z2 = true;
        if (gSTModelShape1S0000000 == null) {
            builder.add((Object) "adPage");
        } else {
            if (Platform.stringIsNullOrEmpty(gSTModelShape1S0000000.A3r())) {
                builder.add((Object) "adPage.name");
                z = true;
            } else {
                z = false;
            }
            if (Platform.stringIsNullOrEmpty(gSTModelShape1S0000000.A3m())) {
                builder.add((Object) "adPage.id");
                z = true;
            }
            GSTModelShape1S0000000 gSTModelShape1S00000002 = (GSTModelShape1S0000000) gSTModelShape1S0000000.A0J(1782764648, GSTModelShape1S0000000.class, -69917005);
            if (gSTModelShape1S00000002 == null) {
                builder.add((Object) "adPage.profilePicture");
            } else if (Platform.stringIsNullOrEmpty(gSTModelShape1S00000002.A41())) {
                builder.add((Object) "adPage.profilePicture.uri");
            } else {
                z2 = z;
            }
        }
        if (z2) {
            return null;
        }
        C07220cv r5 = new C07220cv();
        r5.A0g = gSTModelShape1S0000000.A3r();
        r5.A03(C25651aB.A03, gSTModelShape1S0000000.A3m());
        C25661aC r0 = C25661aC.PAGE;
        Preconditions.checkNotNull(r0);
        r5.A0G = r0;
        GSTModelShape1S0000000 gSTModelShape1S00000003 = (GSTModelShape1S0000000) gSTModelShape1S0000000.A0J(1782764648, GSTModelShape1S0000000.class, -69917005);
        r5.A0P = new PicSquare(ImmutableList.of(new PicSquareUrlWithSize(gSTModelShape1S00000003.A0e(), gSTModelShape1S00000003.A41())));
        return r5.A02();
    }

    public static Uri A00(GSTModelShape1S0000000 gSTModelShape1S0000000, User user, ImmutableList.Builder builder) {
        String A0D;
        if (gSTModelShape1S0000000 != null) {
            A0D = gSTModelShape1S0000000.A41();
        } else if (user != null) {
            A0D = user.A0D();
        } else {
            builder.add((Object) "adIcon");
            return null;
        }
        return A01(A0D, "adIcon", builder);
    }

    public static final C35841ru A02(AnonymousClass1XY r1) {
        return new C35841ru(r1);
    }

    public static ImmutableSet A05(ImmutableList immutableList, ImmutableList.Builder builder) {
        C35951s5 r1;
        if (immutableList == null) {
            builder.add((Object) "adTypes");
            return null;
        }
        C07410dQ r7 = new C07410dQ();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            String name = ((GraphQLMessengerAdProductType) it.next()).name();
            C35951s5[] values = C35951s5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    r1 = C35951s5.A05;
                    break;
                }
                r1 = values[i];
                if (r1.name().equals(name)) {
                    break;
                }
                i++;
            }
            r7.A01(r1);
        }
        return r7.build();
    }

    private C35841ru(AnonymousClass1XY r2) {
        this.A00 = C14500tS.A00(r2);
        this.A01 = new C35881ry(r2);
    }

    public static Uri A01(String str, String str2, ImmutableList.Builder builder) {
        if (!Platform.stringIsNullOrEmpty(str)) {
            return Uri.parse(str);
        }
        builder.add((Object) str2);
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:71:0x02ec  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x039f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.common.collect.ImmutableList A04(java.lang.String r30, com.facebook.user.model.User r31, java.lang.String r32, java.lang.String r33, java.lang.String r34, com.google.common.collect.ImmutableSet r35, boolean r36, com.google.common.collect.ImmutableList r37, com.google.common.collect.ImmutableList.Builder r38) {
        /*
            r10 = r38
            if (r37 == 0) goto L_0x03eb
            int r0 = r37.size()
            if (r0 == 0) goto L_0x03eb
            com.google.common.collect.ImmutableList$Builder r24 = new com.google.common.collect.ImmutableList$Builder
            r24.<init>()
            X.1Xv r23 = r37.iterator()
        L_0x0013:
            boolean r0 = r23.hasNext()
            if (r0 == 0) goto L_0x03e6
            java.lang.Object r22 = r23.next()
            r0 = r22
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            r22 = r0
            r29 = r35
            r3 = r35
            r1 = r0
            r0 = 2116204999(0x7e22b9c7, float:5.4074887E37)
            java.lang.String r13 = r1.A0P(r0)
            if (r13 != 0) goto L_0x03e2
            java.lang.String r0 = "displayInfo.itemId"
            r10.add(r0)
            r21 = 1
        L_0x0038:
            r0 = r31
            if (r31 == 0) goto L_0x03de
            java.lang.String r0 = r0.A0j
            r20 = r0
        L_0x0040:
            java.lang.String r12 = r22.A3x()
            if (r12 != 0) goto L_0x004d
            java.lang.String r0 = "displayInfo.subtitle"
            r10.add(r0)
            r21 = 1
        L_0x004d:
            r1 = -115006108(0xfffffffff9252564, float:-5.359296E34)
            r0 = r22
            double r4 = r0.getDoubleValue(r1)
            r1 = 0
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0063
            java.lang.String r0 = "displayInfo.aspectRatio"
            r10.add(r0)
            r21 = 1
        L_0x0063:
            com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType r1 = com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            r0 = 994159242(0x3b41aa8a, float:0.0029551112)
            r6 = r22
            r7 = r0
            com.google.common.collect.ImmutableList r0 = r6.A0N(r0, r1)
            if (r0 != 0) goto L_0x03b6
            java.lang.String r0 = "displayInfo.adCardTypes"
            r10.add(r0)
            r19 = 0
        L_0x0078:
            if (r19 != 0) goto L_0x007c
            r21 = 1
        L_0x007c:
            r8 = 0
            r15 = r34
            if (r35 == 0) goto L_0x03b2
            X.1s5 r0 = X.C35951s5.A03
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x00a1
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape3S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape3S0000000.class
            r1 = -178465831(0xfffffffff55cd3d9, float:-2.7993215E32)
            r0 = 1781980779(0x6a36de6b, float:5.526871E25)
            r25 = r22
            r26 = r1
            r27 = r2
            r28 = r0
            com.facebook.graphservice.tree.TreeJNI r0 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape3S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape3S0000000) r0
            if (r0 == 0) goto L_0x03b2
        L_0x00a1:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape3S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape3S0000000.class
            r1 = -178465831(0xfffffffff55cd3d9, float:-2.7993215E32)
            r0 = 1781980779(0x6a36de6b, float:5.526871E25)
            r25 = r22
            r26 = r1
            r27 = r2
            r28 = r0
            com.facebook.graphservice.tree.TreeJNI r2 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape3S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape3S0000000) r2
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = X.C35971s7.A00(r2)
            if (r2 == 0) goto L_0x03a7
            if (r0 == 0) goto L_0x03a7
            X.1s8 r1 = new X.1s8
            r1.<init>(r0)
            r1.A00 = r15
            r0 = -1393943294(0xffffffffacea1d02, float:-6.6539005E-12)
            java.lang.String r0 = r2.A0P(r0)
            java.lang.String r0 = com.google.common.base.Strings.nullToEmpty(r0)
            r1.A01 = r0
            com.facebook.messaging.business.common.calltoaction.model.AdCallToAction r18 = new com.facebook.messaging.business.common.calltoaction.model.AdCallToAction
            r0 = r18
            r0.<init>(r1)
        L_0x00da:
            com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType r1 = com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            r25 = r22
            r26 = r7
            r27 = r1
            com.google.common.collect.ImmutableList r1 = r25.A0N(r26, r27)
            com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType.IMAGE
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x0351
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 100313435(0x5faa95b, float:2.3572098E-35)
            r0 = -888632815(0xffffffffcb088a11, float:-8948241.0)
            r26 = r1
            r27 = r2
            r28 = r0
            com.facebook.graphservice.tree.TreeJNI r0 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 != 0) goto L_0x0351
            r0 = r8
        L_0x0105:
            com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType r2 = com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            r1 = 994159242(0x3b41aa8a, float:0.0029551112)
            r26 = r1
            r27 = r2
            com.google.common.collect.ImmutableList r2 = r25.A0N(r26, r27)
            com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType r1 = com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType.VIDEO
            boolean r1 = r2.contains(r1)
            if (r1 != 0) goto L_0x01e0
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 112202875(0x6b0147b, float:6.6233935E-35)
            r1 = -1327322351(0xffffffffb0e2ab11, float:-1.6492284E-9)
            r26 = r2
            r27 = r3
            r28 = r1
            com.facebook.graphservice.tree.TreeJNI r1 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            if (r1 != 0) goto L_0x01e0
            r1 = r8
        L_0x0131:
            if (r21 == 0) goto L_0x0145
            java.lang.String r0 = "displayInfo."
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r13)
            r10.add(r0)
        L_0x013c:
            if (r8 == 0) goto L_0x0013
            r0 = r24
            r0.add(r8)
            goto L_0x0013
        L_0x0145:
            X.1sC r3 = new X.1sC
            r3.<init>()
            com.google.common.collect.ImmutableList r2 = r19.asList()
            r3.A07 = r2
            java.lang.String r6 = "adCardTypes"
            X.C28931fb.A06(r2, r6)
            java.util.Set r2 = r3.A0J
            r2.add(r6)
            r6 = r30
            r3.A0A = r6
            java.lang.String r2 = "adId"
            X.C28931fb.A06(r6, r2)
            r3.A0D = r15
            java.lang.String r2 = "adToken"
            X.C28931fb.A06(r15, r2)
            r3.A0B = r13
            java.lang.String r2 = "adItemId"
            X.C28931fb.A06(r13, r2)
            com.google.common.collect.ImmutableList r2 = r29.asList()
            r3.A08 = r2
            java.lang.String r6 = "adTypes"
            X.C28931fb.A06(r2, r6)
            java.util.Set r2 = r3.A0J
            r2.add(r6)
            r3.A00 = r4
            r2 = r18
            r3.A04 = r2
            r4 = 2074101069(0x7ba0454d, float:1.6643462E36)
            r2 = r22
            java.lang.String r2 = r2.A0P(r4)
            java.lang.String r4 = com.google.common.base.Strings.nullToEmpty(r2)
            r3.A0E = r4
            java.lang.String r2 = "cardDescription"
            X.C28931fb.A06(r4, r2)
            r4 = r33
            r3.A0F = r4
            java.lang.String r2 = "description"
            X.C28931fb.A06(r4, r2)
            r3.A05 = r0
            r0 = r36
            r3.A0K = r0
            r0 = r20
            r3.A0G = r0
            java.lang.String r2 = "pageId"
            X.C28931fb.A06(r0, r2)
            r2 = 455566703(0x1b27656f, float:1.3846691E-22)
            r0 = r22
            java.lang.String r0 = r0.A0P(r2)
            java.lang.String r2 = com.google.common.base.Strings.nullToEmpty(r0)
            r3.A0H = r2
            java.lang.String r0 = "photoDescription"
            X.C28931fb.A06(r2, r0)
            r2 = r32
            r3.A0C = r2
            java.lang.String r0 = "adTitle"
            X.C28931fb.A06(r2, r0)
            r3.A0I = r12
            java.lang.String r0 = "title"
            X.C28931fb.A06(r12, r0)
            r3.A06 = r1
            com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo r8 = new com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo
            r8.<init>(r3)
            goto L_0x013c
        L_0x01e0:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 112202875(0x6b0147b, float:6.6233935E-35)
            r1 = -1327322351(0xffffffffb0e2ab11, float:-1.6492284E-9)
            r26 = r2
            r27 = r3
            r28 = r1
            com.facebook.graphservice.tree.TreeJNI r2 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            r3 = -918737644(0xffffffffc93d2d14, float:-774865.25)
            r1 = -2041548631(0xffffffff865070a9, float:-3.9203222E-35)
            r26 = r3
            r28 = r1
            com.facebook.graphservice.tree.TreeJNI r11 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r11 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r11
            r25 = r2
            r1 = 0
            java.lang.String r9 = "displayInfo.video"
            if (r2 == 0) goto L_0x034d
            X.5T4 r17 = new X.5T4
            r2 = r17
            r2.<init>()
            r16 = 0
            r2 = r25
            java.lang.String r3 = r2.A3m()
            if (r3 != 0) goto L_0x0342
            java.lang.String r2 = "displayInfo.video.id"
            r10.add(r2)
            r16 = 1
        L_0x0223:
            r2 = r25
            java.lang.String r3 = r2.A3t()
            java.lang.String r2 = "displayInfo.video.playableUrl"
            android.net.Uri r2 = A01(r3, r2, r10)
            if (r2 != 0) goto L_0x0330
            r16 = 1
        L_0x0233:
            r2 = 1799469729(0x6b41baa1, float:2.3420401E26)
            r6 = r25
            java.lang.String r2 = r6.A0P(r2)
            if (r2 == 0) goto L_0x0246
            android.net.Uri r3 = android.net.Uri.parse(r2)
            r2 = r17
            r2.A05 = r3
        L_0x0246:
            r2 = 1128191036(0x433ed43c, float:190.82904)
            java.lang.String r3 = r6.A0P(r2)
            r2 = r17
            r2.A0A = r3
            r3 = 100313435(0x5faa95b, float:2.3572098E-35)
            r2 = -1334712303(0xffffffffb071e811, float:-8.8005075E-10)
            r26 = r3
            r28 = r2
            com.facebook.graphservice.tree.TreeJNI r3 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            r6 = r3
            r2 = 0
            if (r3 == 0) goto L_0x0291
            java.lang.String r3 = r3.A41()
            if (r3 == 0) goto L_0x0291
            int r2 = r6.A0Z()
            int r7 = r6.A0e()
            X.1sA r6 = new X.1sA
            r6.<init>()
            r6.A00 = r2
            android.net.Uri r2 = android.net.Uri.parse(r3)
            r6.A02 = r2
            java.lang.String r3 = "uri"
            X.C28931fb.A06(r2, r3)
            java.util.Set r2 = r6.A03
            r2.add(r3)
            r6.A01 = r7
            com.facebook.messaging.business.inboxads.common.InboxAdsImage r2 = new com.facebook.messaging.business.inboxads.common.InboxAdsImage
            r2.<init>(r6)
        L_0x0291:
            r3 = r17
            r3.A07 = r2
            r2 = r25
            int r3 = r2.A0c()
            r2 = r17
            r2.A02 = r3
            r2 = -281351633(0xffffffffef3aea2f, float:-5.7847322E28)
            r6 = r25
            int r3 = r6.getIntValue(r2)
            r2 = r17
            r2.A00 = r3
            r2 = -102270099(0xfffffffff9e77b6d, float:-1.5024049E35)
            int r3 = r6.getIntValue(r2)
            r2 = r17
            r2.A01 = r3
            r2 = 753054417(0x2ce2b2d1, float:6.443159E-12)
            int r3 = r6.getIntValue(r2)
            r2 = r17
            r2.A03 = r3
            r2 = 1850247337(0x6e4888a9, float:1.5515553E28)
            boolean r3 = r6.getBooleanValue(r2)
            r2 = r17
            r2.A0F = r3
            r2 = 801632180(0x2fc7efb4, float:3.6368208E-10)
            int r3 = r6.getIntValue(r2)
            r2 = r17
            r2.A04 = r3
            if (r11 != 0) goto L_0x02f0
            java.lang.String r2 = "displayInfo.videoAutoplayStyleInfo"
            r10.add(r2)
            r16 = 1
        L_0x02e1:
            if (r16 != 0) goto L_0x034d
            com.facebook.messaging.business.inboxads.common.InboxAdsVideo r1 = new com.facebook.messaging.business.inboxads.common.InboxAdsVideo
            r2 = r17
            r1.<init>(r2)
        L_0x02ea:
            if (r1 != 0) goto L_0x0131
            r21 = 1
            goto L_0x0131
        L_0x02f0:
            r2 = 1555928294(0x5cbd94e6, float:4.26899889E17)
            boolean r3 = r11.getBooleanValue(r2)
            r2 = r17
            r2.A0D = r3
            r2 = 1556527769(0x5cc6ba99, float:4.47497693E17)
            boolean r3 = r11.getBooleanValue(r2)
            r2 = r17
            r2.A0E = r3
            r2 = -1262285389(0xffffffffb4c30db3, float:-3.6331548E-7)
            java.lang.String r2 = r11.A0P(r2)
            java.lang.String r3 = ""
            r6 = r3
            if (r2 == 0) goto L_0x0313
            r6 = r2
        L_0x0313:
            r2 = r17
            r2.A08 = r6
            java.lang.String r2 = "autoplayCellGatingResult"
            X.C28931fb.A06(r6, r2)
            r2 = -70074842(0xfffffffffbd2be26, float:-2.188478E36)
            java.lang.String r2 = r11.A0P(r2)
            if (r2 == 0) goto L_0x0326
            r3 = r2
        L_0x0326:
            r2 = r17
            r2.A09 = r3
            java.lang.String r2 = "autoplayWifiGatingResult"
            X.C28931fb.A06(r3, r2)
            goto L_0x02e1
        L_0x0330:
            r3 = r17
            r3.A06 = r2
            java.lang.String r3 = "uri"
            X.C28931fb.A06(r2, r3)
            r2 = r17
            java.util.Set r2 = r2.A0C
            r2.add(r3)
            goto L_0x0233
        L_0x0342:
            r2 = r17
            r2.A0B = r3
            java.lang.String r2 = "id"
            X.C28931fb.A06(r3, r2)
            goto L_0x0223
        L_0x034d:
            r10.add(r9)
            goto L_0x02ea
        L_0x0351:
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 100313435(0x5faa95b, float:2.3572098E-35)
            r0 = -888632815(0xffffffffcb088a11, float:-8948241.0)
            r26 = r1
            r27 = r2
            r28 = r0
            com.facebook.graphservice.tree.TreeJNI r1 = r25.A0J(r26, r27, r28)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r3 = r1
            r0 = 0
            java.lang.String r7 = "displayInfo.image"
            if (r1 == 0) goto L_0x03a3
            r11 = 0
            java.lang.String r2 = r1.A41()
            java.lang.String r1 = "displayInfo.image.uri"
            android.net.Uri r6 = A01(r2, r1, r10)
            if (r6 != 0) goto L_0x0379
            r11 = 1
        L_0x0379:
            int r1 = r3.A0Z()
            int r3 = r3.A0e()
            if (r11 != 0) goto L_0x03a3
            X.1sA r2 = new X.1sA
            r2.<init>()
            r2.A00 = r1
            r2.A02 = r6
            java.lang.String r1 = "uri"
            X.C28931fb.A06(r6, r1)
            java.util.Set r0 = r2.A03
            r0.add(r1)
            r2.A01 = r3
            com.facebook.messaging.business.inboxads.common.InboxAdsImage r0 = new com.facebook.messaging.business.inboxads.common.InboxAdsImage
            r0.<init>(r2)
        L_0x039d:
            if (r0 != 0) goto L_0x0105
            r21 = 1
            goto L_0x0105
        L_0x03a3:
            r10.add(r7)
            goto L_0x039d
        L_0x03a7:
            java.lang.String r0 = "displayInfo.callToAction"
            r10.add(r0)
            r18 = r8
            r21 = 1
            goto L_0x00da
        L_0x03b2:
            r18 = r8
            goto L_0x00da
        L_0x03b6:
            X.0dQ r6 = new X.0dQ
            r6.<init>()
            X.1Xv r2 = r0.iterator()
        L_0x03bf:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x03d8
            java.lang.Object r1 = r2.next()
            com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType r1 = (com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType) r1
            com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxAdsCardType.END_CARD
            if (r1 != r0) goto L_0x03d5
            X.1rw r0 = X.C35861rw.END_CARD
        L_0x03d1:
            r6.A01(r0)
            goto L_0x03bf
        L_0x03d5:
            X.1rw r0 = X.C35861rw.DEFAULT
            goto L_0x03d1
        L_0x03d8:
            com.google.common.collect.ImmutableSet r19 = r6.build()
            goto L_0x0078
        L_0x03de:
            java.lang.String r20 = ""
            goto L_0x0040
        L_0x03e2:
            r21 = 0
            goto L_0x0038
        L_0x03e6:
            com.google.common.collect.ImmutableList r0 = r24.build()
            return r0
        L_0x03eb:
            java.lang.String r0 = "adDisplayInfo"
            r10.add(r0)
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35841ru.A04(java.lang.String, com.facebook.user.model.User, java.lang.String, java.lang.String, java.lang.String, com.google.common.collect.ImmutableSet, boolean, com.google.common.collect.ImmutableList, com.google.common.collect.ImmutableList$Builder):com.google.common.collect.ImmutableList");
    }
}
