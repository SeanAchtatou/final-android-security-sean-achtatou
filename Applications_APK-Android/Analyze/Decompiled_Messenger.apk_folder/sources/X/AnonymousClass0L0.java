package X;

import com.facebook.acra.ACRA;
import com.facebook.acra.constants.ErrorReportingConstants;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0L0  reason: invalid class name */
public final class AnonymousClass0L0 {
    public int A00 = 0;
    public long A01;
    public C01690Bg A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public List A07 = new ArrayList(50);
    public UUID A08;

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("seq", this.A00);
            jSONObject.put("time", String.format(null, "%.3f", Double.valueOf(((double) this.A01) / 1000.0d)));
            jSONObject.putOpt("app_id", this.A03);
            jSONObject.putOpt("app_ver", this.A04);
            jSONObject.putOpt("build_num", this.A05);
            jSONObject.putOpt("device_id", this.A02.get());
            jSONObject.putOpt(ACRA.SESSION_ID_KEY, this.A08);
            jSONObject.putOpt(ErrorReportingConstants.USER_ID_KEY, this.A06);
            if (this.A07 != null) {
                JSONArray jSONArray = new JSONArray();
                for (C01750Bm A002 : this.A07) {
                    jSONArray.put(A002.A00());
                }
                jSONObject.put("data", jSONArray);
            }
            jSONObject.put("log_type", "client_event");
            return jSONObject.toString();
        } catch (JSONException e) {
            C010708t.A0S("AnalyticsSession", e, "Failed to serialize");
            return BuildConfig.FLAVOR;
        }
    }
}
