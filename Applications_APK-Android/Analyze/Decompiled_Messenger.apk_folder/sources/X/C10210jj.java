package X;

/* renamed from: X.0jj  reason: invalid class name and case insensitive filesystem */
public interface C10210jj {
    void beforeArrayValues(C11710np r1);

    void beforeObjectEntries(C11710np r1);

    void writeArrayValueSeparator(C11710np r1);

    void writeEndArray(C11710np r1, int i);

    void writeEndObject(C11710np r1, int i);

    void writeObjectEntrySeparator(C11710np r1);

    void writeObjectFieldValueSeparator(C11710np r1);

    void writeRootValueSeparator(C11710np r1);

    void writeStartArray(C11710np r1);

    void writeStartObject(C11710np r1);
}
