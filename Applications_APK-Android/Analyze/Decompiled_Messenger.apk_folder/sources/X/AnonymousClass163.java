package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.ThreadsCollection;

/* renamed from: X.163  reason: invalid class name */
public final class AnonymousClass163 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadsCollection(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadsCollection[i];
    }
}
