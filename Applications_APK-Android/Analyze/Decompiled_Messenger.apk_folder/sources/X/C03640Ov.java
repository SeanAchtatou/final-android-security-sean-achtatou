package X;

import android.content.pm.ComponentInfo;
import java.util.Comparator;

/* renamed from: X.0Ov  reason: invalid class name and case insensitive filesystem */
public final class C03640Ov implements Comparator {
    public int compare(Object obj, Object obj2) {
        return ((ComponentInfo) obj).name.compareTo(((ComponentInfo) obj2).name);
    }
}
