package X;

import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1cv  reason: invalid class name and case insensitive filesystem */
public final class C27271cv {
    public static final Object A00 = new Object();
    public static final Object A01 = new Object();
    public static final AtomicInteger A02 = new AtomicInteger();

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002e, code lost:
        if (r9.equals("com_facebook_orca_threadview_item_plugins_interfaces_messagecomponentbinder_MessageBinderProviderSpec") == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        if (r9.equals("com_facebook_msys_config_plugins_interfaces_flipper_FlipperPluginInterfaceSpec") == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r9.equals("com_facebook_messaging_threadsettings_plugins_interfaces_row_RowInterfaceSpec") == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004c, code lost:
        if (r9.equals("com_facebook_messaging_capability_thread_plugins_interfaces_threadcapabilitycomputation_ThreadCapabilityComputationSpec") == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        if (r9.equals("com_facebook_messenger_msys_plugins_interfaces_msysbootstrap_MessengerMsysBootstrapInterfaceSpec") == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0060, code lost:
        if (r9.equals("com_facebook_messaging_threadlist_plugins_interfaces_inboxviewbinder_InboxViewBinderInterfaceSpec") == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006a, code lost:
        if (r9.equals("com_facebook_orca_threadview_plugins_interfaces_addfriend_FriendAdderInterfaceSpec") == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0024, code lost:
        if (r9.equals("com_facebook_messenger_msys_plugins_interfaces_messengermsys_MessengerSecureMessageInterfaceSpec") == false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object A00(java.lang.String r9, java.lang.String r10, android.content.Context r11, java.lang.Object[] r12) {
        /*
            int r0 = r9.hashCode()
            r5 = 3
            r2 = 2
            r4 = 1
            r3 = 0
            switch(r0) {
                case -2101850339: goto L_0x0063;
                case -1972264818: goto L_0x0059;
                case -1798421254: goto L_0x004f;
                case -1486887484: goto L_0x0045;
                case -1281622135: goto L_0x003b;
                case -577314835: goto L_0x0031;
                case -482819643: goto L_0x0027;
                case 380978653: goto L_0x001d;
                default: goto L_0x000b;
            }
        L_0x000b:
            r1 = -1
        L_0x000c:
            r7 = r10
            r8 = r11
            switch(r1) {
                case 0: goto L_0x00b3;
                case 1: goto L_0x00a1;
                case 2: goto L_0x008b;
                case 3: goto L_0x0085;
                case 4: goto L_0x007f;
                case 5: goto L_0x0079;
                case 6: goto L_0x0073;
                case 7: goto L_0x006d;
                default: goto L_0x0011;
            }
        L_0x0011:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = " is not a known interface name"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r9, r0)
            r1.<init>(r0)
            throw r1
        L_0x001d:
            java.lang.String r0 = "com_facebook_messenger_msys_plugins_interfaces_messengermsys_MessengerSecureMessageInterfaceSpec"
            boolean r0 = r9.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0027:
            java.lang.String r0 = "com_facebook_orca_threadview_item_plugins_interfaces_messagecomponentbinder_MessageBinderProviderSpec"
            boolean r0 = r9.equals(r0)
            r1 = 5
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0031:
            java.lang.String r0 = "com_facebook_msys_config_plugins_interfaces_flipper_FlipperPluginInterfaceSpec"
            boolean r0 = r9.equals(r0)
            r1 = 7
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x003b:
            java.lang.String r0 = "com_facebook_messaging_threadsettings_plugins_interfaces_row_RowInterfaceSpec"
            boolean r0 = r9.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0045:
            java.lang.String r0 = "com_facebook_messaging_capability_thread_plugins_interfaces_threadcapabilitycomputation_ThreadCapabilityComputationSpec"
            boolean r0 = r9.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x004f:
            java.lang.String r0 = "com_facebook_messenger_msys_plugins_interfaces_msysbootstrap_MessengerMsysBootstrapInterfaceSpec"
            boolean r0 = r9.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0059:
            java.lang.String r0 = "com_facebook_messaging_threadlist_plugins_interfaces_inboxviewbinder_InboxViewBinderInterfaceSpec"
            boolean r0 = r9.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x0063:
            java.lang.String r0 = "com_facebook_orca_threadview_plugins_interfaces_addfriend_FriendAdderInterfaceSpec"
            boolean r0 = r9.equals(r0)
            r1 = 6
            if (r0 != 0) goto L_0x000c
            goto L_0x000b
        L_0x006d:
            X.EJT r0 = new X.EJT
            r0.<init>(r10)
            return r0
        L_0x0073:
            X.3M9 r0 = new X.3M9
            r0.<init>(r10, r11)
            return r0
        L_0x0079:
            X.3Eg r0 = new X.3Eg
            r0.<init>(r10, r11)
            return r0
        L_0x007f:
            X.1cw r0 = new X.1cw
            r0.<init>(r10, r11)
            return r0
        L_0x0085:
            X.1hw r0 = new X.1hw
            r0.<init>(r10, r11)
            return r0
        L_0x008b:
            X.6Xo r6 = new X.6Xo
            r9 = r12[r3]
            android.content.Context r9 = (android.content.Context) r9
            r10 = r12[r4]
            X.2sU r10 = (X.C57712sU) r10
            r11 = r12[r2]
            com.facebook.messaging.model.threads.ThreadSummary r11 = (com.facebook.messaging.model.threads.ThreadSummary) r11
            r12 = r12[r5]
            com.facebook.user.model.User r12 = (com.facebook.user.model.User) r12
            r6.<init>(r7, r8, r9, r10, r11, r12)
            return r6
        L_0x00a1:
            X.1qw r6 = new X.1qw
            r9 = r12[r3]
            X.0p4 r9 = (X.AnonymousClass0p4) r9
            r10 = r12[r4]
            X.1BC r10 = (X.AnonymousClass1BC) r10
            r11 = r12[r2]
            X.1BY r11 = (X.AnonymousClass1BY) r11
            r6.<init>(r7, r8, r9, r10, r11)
            return r6
        L_0x00b3:
            X.3Sx r2 = new X.3Sx
            r1 = r12[r3]
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1
            r0 = r12[r4]
            X.3Sw r0 = (X.C68453Sw) r0
            r2.<init>(r10, r11, r1, r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27271cv.A00(java.lang.String, java.lang.String, android.content.Context, java.lang.Object[]):java.lang.Object");
    }
}
