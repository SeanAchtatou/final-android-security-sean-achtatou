package X;

/* renamed from: X.0tG  reason: invalid class name and case insensitive filesystem */
public final class C14410tG implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.navigation.home.tabs.M4TabNavigationFragment$9";
    public final /* synthetic */ C13450rS A00;

    public C14410tG(C13450rS r1) {
        this.A00 = r1;
    }

    public void run() {
        C13450rS r3 = this.A00;
        C16130wX r4 = r3.A03;
        C24971Xv it = r4.A00.A00.iterator();
        while (it.hasNext()) {
            r4.A03.A01((C16040wO) it.next()).Ae6().CIL(r4.A02);
        }
        C16540xI r1 = r3.A05;
        r1.A01 = true;
        C16540xI.A00(r1, r1.A00);
        ((C15390vD) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AIz, r3.A02)).CIL(r3.A0A);
        boolean z = false;
        if (r3.A04 != null) {
            z = true;
        }
        if (z) {
            C13450rS.A06(r3);
            C13450rS.A05(r3);
        }
    }
}
