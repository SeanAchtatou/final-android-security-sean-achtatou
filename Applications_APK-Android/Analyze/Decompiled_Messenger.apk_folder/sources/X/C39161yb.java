package X;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;

/* renamed from: X.1yb  reason: invalid class name and case insensitive filesystem */
public final class C39161yb {
    public final ActivityInfo A00;
    public final Drawable A01;
    public final CharSequence A02;

    public C39161yb(Drawable drawable, CharSequence charSequence, ActivityInfo activityInfo) {
        this.A01 = drawable;
        this.A02 = charSequence;
        this.A00 = activityInfo;
    }
}
