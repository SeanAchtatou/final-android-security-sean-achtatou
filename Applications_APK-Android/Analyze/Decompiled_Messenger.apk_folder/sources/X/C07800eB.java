package X;

import android.os.Handler;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eB  reason: invalid class name and case insensitive filesystem */
public final class C07800eB extends C08190ep {
    private static volatile C07800eB A08;
    public final C07830eE A00;
    public final C07830eE A01;
    public final C07830eE A02;
    public final C07830eE A03;
    public final C07830eE A04;
    public final AtomicReference A05 = new AtomicReference(null);
    private final Handler A06 = new Handler(C07840eF.A00);
    private final int[] A07 = {7864351};

    public static final C07800eB A00(AnonymousClass1XY r3) {
        if (A08 == null) {
            synchronized (C07800eB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A08 = new C07800eB();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    public void A01(C25881aY r6) {
        this.A05.set(r6);
        if (r6 != null) {
            AnonymousClass00S.A05(this.A06, new C73083fT(this, r6), (long) (r6.A00 * AnonymousClass1Y3.A87), -911329827);
        }
    }

    public AnonymousClass0Ti AsW() {
        return AnonymousClass0Ti.A00(this.A07);
    }

    public C07800eB() {
        C07830eE r1 = new C07830eE();
        this.A04 = r1;
        r1.A00 = 0;
        C07830eE r12 = new C07830eE();
        this.A01 = r12;
        r12.A00 = 1;
        C07830eE r13 = new C07830eE();
        this.A03 = r13;
        r13.A00 = 5;
        C07830eE r14 = new C07830eE();
        this.A00 = r14;
        r14.A00 = 3;
        C07830eE r15 = new C07830eE();
        this.A02 = r15;
        r15.A00 = 4;
    }
}
