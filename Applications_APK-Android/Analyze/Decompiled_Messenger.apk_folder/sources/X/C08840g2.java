package X;

import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0g2  reason: invalid class name and case insensitive filesystem */
public final class C08840g2 implements C08850g3 {
    private static volatile C08840g2 A01;
    public Set A00;

    public static final C08840g2 A00(AnonymousClass1XY r6) {
        if (A01 == null) {
            synchronized (C08840g2.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r6);
                if (A002 != null) {
                    try {
                        A01 = new C08840g2(new AnonymousClass0X5(r6.getApplicationInjector(), AnonymousClass0X6.A0U));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public Set Ain() {
        return this.A00;
    }

    private C08840g2(Set set) {
        this.A00 = set;
    }
}
