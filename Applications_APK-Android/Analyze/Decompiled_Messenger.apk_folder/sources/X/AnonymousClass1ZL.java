package X;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1ZL  reason: invalid class name */
public class AnonymousClass1ZL extends AnonymousClass1ZM implements C04770Wc {
    public C04310Tq A00;
    private AnonymousClass1ZO A01;
    private AnonymousClass1ZO A02;
    public final AnonymousClass1ZN A03;
    private final AnonymousClass0WV A04;

    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[ExcHandler: IndexOutOfBoundsException | BufferUnderflowException (unused java.lang.Throwable), SYNTHETIC, Splitter:B:15:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A01(int r5, X.AnonymousClass10F r6) {
        /*
            r4 = this;
            X.0WV r0 = r4.A04
            if (r0 == 0) goto L_0x0082
            X.1ZN r0 = r4.A03
            if (r0 == 0) goto L_0x0082
            r1 = r5 & 6
            r0 = 0
            if (r1 == 0) goto L_0x000e
            r0 = 1
        L_0x000e:
            if (r0 == 0) goto L_0x0082
            int r3 = r5 >>> 8
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r3 = r3 & r0
            X.10F r0 = X.AnonymousClass10F.AUTO_EXPOSURE
            if (r6 != r0) goto L_0x001d
            X.1ZO r0 = r4.A01
        L_0x001c:
            goto L_0x0020
        L_0x001d:
            X.1ZO r0 = r4.A02
            goto L_0x001c
        L_0x0020:
            java.util.concurrent.atomic.AtomicIntegerArray r2 = r0.A00     // Catch:{  }
            r1 = 0
            r0 = 1
            boolean r0 = r2.compareAndSet(r3, r1, r0)     // Catch:{  }
            if (r0 == 0) goto L_0x0050
            X.1ZN r2 = r4.A03     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082 }
            int r0 = r2.A04     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082 }
            if (r0 == 0) goto L_0x003c
            int r1 = r2.A03(r0)     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082 }
            int r0 = r3 << 2
            int r1 = r1 + r0
            java.lang.String r3 = r2.A05(r1)     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082, IndexOutOfBoundsException | BufferUnderflowException -> 0x0082 }
            goto L_0x003d
        L_0x003c:
            r3 = 0
        L_0x003d:
            if (r3 == 0) goto L_0x0082
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0082
            java.lang.Integer r0 = X.C31801kP.A00(r5)
            int r0 = r0.intValue()
            switch(r0) {
                case 1: goto L_0x0056;
                case 2: goto L_0x0051;
                default: goto L_0x0050;
            }
        L_0x0050:
            return
        L_0x0051:
            X.10F r0 = X.AnonymousClass10F.AUTO_EXPOSURE
            if (r6 != r0) goto L_0x0056
            return
        L_0x0056:
            r1 = r5 & 8
            r0 = 0
            if (r1 == 0) goto L_0x005c
            r0 = 1
        L_0x005c:
            if (r0 == 0) goto L_0x007f
            java.lang.Throwable r2 = new java.lang.Throwable
            r2.<init>()
            java.io.StringWriter r1 = new java.io.StringWriter
            r1.<init>()
            java.io.PrintWriter r0 = new java.io.PrintWriter
            r0.<init>(r1)
            r2.printStackTrace(r0)
            r0.close()
            java.lang.String r2 = r1.toString()
        L_0x0077:
            X.0WV r1 = r4.A04
            java.lang.String r0 = r6.mValue
            r1.logExposure(r3, r0, r2)
            return
        L_0x007f:
            java.lang.String r2 = ""
            goto L_0x0077
        L_0x0082:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ZL.A01(int, X.10F):void");
    }

    public double A03(long j, double d, boolean z) {
        int i;
        AnonymousClass1ZN r4 = this.A03;
        if (r4 != null && AnonymousClass0XG.A00(j) == 4) {
            int A012 = AnonymousClass0XG.A01(j);
            try {
                int i2 = r4.A02;
                if (i2 != 0) {
                    i = r4.A01.getInt(r4.A03(i2) + (A012 << 2));
                } else {
                    i = 0;
                }
                if (!z) {
                    boolean z2 = false;
                    if ((i & 6) != 0) {
                        z2 = true;
                    }
                    if (z2) {
                        A01(i, AnonymousClass10F.AUTO_EXPOSURE);
                    }
                }
                boolean z3 = true;
                if ((i & 1) == 0) {
                    z3 = false;
                }
                if (z3) {
                    return d;
                }
                AnonymousClass1ZN r1 = this.A03;
                int i3 = r1.A03;
                if (i3 != 0) {
                    return r1.A01.getDouble(r1.A03(i3) + (A012 << 3));
                }
                return 0.0d;
            } catch (IndexOutOfBoundsException | BufferUnderflowException unused) {
            }
        }
        return d;
    }

    public long A04(long j, long j2, boolean z) {
        int i;
        AnonymousClass1ZN r4 = this.A03;
        if (r4 != null && AnonymousClass0XG.A00(j) == 2) {
            int A012 = AnonymousClass0XG.A01(j);
            try {
                int i2 = r4.A05;
                if (i2 != 0) {
                    i = r4.A01.getInt(r4.A03(i2) + (A012 << 2));
                } else {
                    i = 0;
                }
                if (!z) {
                    boolean z2 = false;
                    if ((i & 6) != 0) {
                        z2 = true;
                    }
                    if (z2) {
                        A01(i, AnonymousClass10F.AUTO_EXPOSURE);
                    }
                }
                boolean z3 = true;
                if ((i & 1) == 0) {
                    z3 = false;
                }
                if (z3) {
                    return j2;
                }
                AnonymousClass1ZN r1 = this.A03;
                int i3 = r1.A06;
                if (i3 != 0) {
                    return r1.A01.getLong(r1.A03(i3) + (A012 << 3));
                }
                return 0;
            } catch (IndexOutOfBoundsException | BufferUnderflowException unused) {
            }
        }
        return j2;
    }

    public Integer A05(long j) {
        if (this.A03 == null) {
            return AnonymousClass07B.A00;
        }
        int A002 = A00(this, j);
        Integer A003 = C31801kP.A00(A002);
        switch (A003.intValue()) {
            case 1:
                boolean z = false;
                if ((A002 & 8) != 0) {
                    z = true;
                }
                if (z) {
                    return AnonymousClass07B.A0N;
                }
                return AnonymousClass07B.A01;
            case 2:
                boolean z2 = false;
                if ((A002 & 8) != 0) {
                    z2 = true;
                }
                if (z2) {
                    return AnonymousClass07B.A0Y;
                }
                return AnonymousClass07B.A0C;
            default:
                return A003;
        }
    }

    public String A06(long j) {
        int i;
        if (this.A03 == null) {
            return null;
        }
        int A002 = A00(this, j);
        boolean z = false;
        if ((A002 & 6) != 0) {
            z = true;
        }
        if (z) {
            i = (A002 >>> 8) & C15320v6.MEASURED_SIZE_MASK;
        } else {
            i = -1;
        }
        if (i < 0) {
            return null;
        }
        try {
            AnonymousClass1ZN r3 = this.A03;
            int i2 = r3.A04;
            if (i2 != 0) {
                return r3.A05(r3.A03(i2) + (i << 2));
            }
            return null;
        } catch (IndexOutOfBoundsException | NegativeArraySizeException | OutOfMemoryError | BufferUnderflowException unused) {
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[ExcHandler: IndexOutOfBoundsException | BufferUnderflowException (unused java.lang.Throwable), SYNTHETIC, Splitter:B:21:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A07(long r6, java.lang.String r8, boolean r9) {
        /*
            r5 = this;
            X.1ZN r4 = r5.A03
            if (r4 == 0) goto L_0x004f
            int r1 = X.AnonymousClass0XG.A00(r6)
            r0 = 3
            if (r1 != r0) goto L_0x004f
            int r3 = X.AnonymousClass0XG.A01(r6)
            int r0 = r4.A07     // Catch:{  }
            if (r0 == 0) goto L_0x0021
            java.nio.ByteBuffer r2 = r4.A01     // Catch:{  }
            int r1 = r4.A03(r0)     // Catch:{  }
            int r0 = r3 << 2
            int r1 = r1 + r0
            int r2 = r2.getInt(r1)     // Catch:{  }
            goto L_0x0022
        L_0x0021:
            r2 = 0
        L_0x0022:
            if (r9 != 0) goto L_0x0031
            r1 = r2 & 6
            r0 = 0
            if (r1 == 0) goto L_0x002a
            r0 = 1
        L_0x002a:
            if (r0 == 0) goto L_0x0031
            X.10F r0 = X.AnonymousClass10F.AUTO_EXPOSURE
            r5.A01(r2, r0)
        L_0x0031:
            r0 = 1
            r2 = r2 & r0
            if (r2 != 0) goto L_0x0036
            r0 = 0
        L_0x0036:
            if (r0 == 0) goto L_0x0039
            return r8
        L_0x0039:
            X.1ZN r2 = r5.A03     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f }
            int r0 = r2.A08     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f }
            if (r0 == 0) goto L_0x004b
            int r1 = r2.A03(r0)     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f }
            int r0 = r3 << 2
            int r1 = r1 + r0
            java.lang.String r0 = r2.A05(r1)     // Catch:{ IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f, IndexOutOfBoundsException | BufferUnderflowException -> 0x004f }
            goto L_0x004c
        L_0x004b:
            r0 = 0
        L_0x004c:
            if (r0 == 0) goto L_0x004f
            return r0
        L_0x004f:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ZL.A07(long, java.lang.String, boolean):java.lang.String");
    }

    public void A08(long j, AnonymousClass10F r6) {
        if (this.A03 != null) {
            int A002 = A00(this, j);
            boolean z = false;
            if ((A002 & 6) != 0) {
                z = true;
            }
            if (z) {
                A01(A002, r6);
            }
        }
    }

    public boolean A0C(long j, boolean z, boolean z2) {
        byte b;
        AnonymousClass1ZN r2 = this.A03;
        if (r2 == null || AnonymousClass0XG.A00(j) != 1) {
            return z;
        }
        int A012 = AnonymousClass0XG.A01(j);
        try {
            int i = r2.A01;
            if (i != 0) {
                b = r2.A01.get(r2.A03(i) + A012);
            } else {
                b = 0;
            }
            if (!z2) {
                boolean z3 = false;
                if ((b & 6) != 0) {
                    z3 = true;
                }
                if (z3) {
                    int i2 = C31801kP.A00;
                    try {
                        int i3 = r2.A00;
                        if (i3 != 0) {
                            i2 = r2.A01.getInt(r2.A03(i3) + (A012 << 2));
                        } else {
                            i2 = 0;
                        }
                    } catch (IndexOutOfBoundsException | BufferUnderflowException unused) {
                    }
                    A01(i2, AnonymousClass10F.AUTO_EXPOSURE);
                }
            }
            boolean z4 = true;
            if ((b & 1) == 0) {
                z4 = false;
            }
            if (z4) {
                return z;
            }
            if ((b >>> 7) != 0) {
                return true;
            }
            return false;
        } catch (IndexOutOfBoundsException | BufferUnderflowException unused2) {
            return z;
        }
    }

    public Map Al7() {
        AnonymousClass1ZN r0 = this.A03;
        if (r0 == null) {
            return new HashMap();
        }
        return r0.A09;
    }

    public AnonymousClass1ZL(ByteBuffer byteBuffer, AnonymousClass0WV r12, C25851aV r13, C04310Tq r14) {
        super(r13);
        AnonymousClass1ZN r4;
        AnonymousClass1ZO r0;
        int i;
        boolean z;
        int i2;
        int i3;
        this.A04 = r12;
        this.A00 = r14;
        if (byteBuffer == null || byteBuffer.capacity() <= 0) {
            r4 = null;
        } else {
            r4 = new AnonymousClass1ZN();
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            r4.A00 = byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position();
            r4.A01 = byteBuffer;
            if (r4.A06() == 123456 && (r4.A07() == 123456 || r4.A07() == 0)) {
                r4.A01 = r4.A02(6);
                r4.A00 = r4.A02(8);
                r4.A06 = r4.A02(10);
                r4.A05 = r4.A02(12);
                r4.A02(14);
                r4.A02(16);
                r4.A03 = r4.A02(18);
                r4.A02 = r4.A02(20);
                r4.A08 = r4.A02(22);
                r4.A07 = r4.A02(24);
                r4.A04 = r4.A02(26);
                int A022 = r4.A02(32);
                if (A022 != 0) {
                    i = r4.A04(A022);
                } else {
                    i = 0;
                }
                int i4 = 0;
                if (i > 50) {
                    z = false;
                } else {
                    while (i4 < i) {
                        try {
                            AnonymousClass7WF r2 = new AnonymousClass7WF();
                            int A023 = r4.A02(32);
                            if (A023 != 0) {
                                int A012 = r4.A01(r4.A03(A023) + (i4 << 2));
                                ByteBuffer byteBuffer2 = r4.A01;
                                r2.A00 = A012;
                                r2.A01 = byteBuffer2;
                            } else {
                                r2 = null;
                            }
                            AnonymousClass7WF r8 = r2;
                            if (r2 != null) {
                                Map map = r4.A09;
                                int A024 = r2.A02(4);
                                if (A024 != 0) {
                                    i2 = r8.A01.getInt(A024 + r8.A00);
                                } else {
                                    i2 = 0;
                                }
                                Integer valueOf = Integer.valueOf(i2);
                                int A025 = r8.A02(6);
                                if (A025 != 0) {
                                    i3 = r8.A01.getInt(A025 + r8.A00);
                                } else {
                                    i3 = 0;
                                }
                                map.put(valueOf, Integer.valueOf(i3));
                            }
                            i4++;
                        } catch (IndexOutOfBoundsException unused) {
                            r4.A09.clear();
                        }
                    }
                    z = true;
                }
                if (!z) {
                    r4 = null;
                }
            } else {
                r4.A06();
                r4.A07();
                r4 = null;
            }
        }
        this.A03 = r4;
        int i5 = 0;
        if (r4 != null) {
            try {
                int A026 = r4.A02(26);
                if (A026 != 0) {
                    i5 = r4.A04(A026);
                }
            } catch (IndexOutOfBoundsException | BufferUnderflowException unused2) {
                this.A01 = new AnonymousClass1ZO(0);
                r0 = new AnonymousClass1ZO(0);
            } catch (Throwable th) {
                this.A01 = new AnonymousClass1ZO(0);
                this.A02 = new AnonymousClass1ZO(0);
                throw th;
            }
        }
        this.A01 = new AnonymousClass1ZO(i5);
        r0 = new AnonymousClass1ZO(i5);
        this.A02 = r0;
    }

    public static int A00(AnonymousClass1ZL r4, long j) {
        AnonymousClass09P r1;
        int A002 = AnonymousClass0XG.A00(j);
        int A012 = AnonymousClass0XG.A01(j);
        if (A002 == 1) {
            AnonymousClass1ZN r12 = r4.A03;
            int i = r12.A00;
            if (i != 0) {
                return r12.A01.getInt(r12.A03(i) + (A012 << 2));
            }
            return 0;
        } else if (A002 == 2) {
            AnonymousClass1ZN r13 = r4.A03;
            int i2 = r13.A05;
            if (i2 != 0) {
                return r13.A01.getInt(r13.A03(i2) + (A012 << 2));
            }
            return 0;
        } else if (A002 != 3) {
            if (A002 == 4) {
                try {
                    AnonymousClass1ZN r14 = r4.A03;
                    int i3 = r14.A02;
                    if (i3 != 0) {
                        return r14.A01.getInt(r14.A03(i3) + (A012 << 2));
                    }
                    return 0;
                } catch (IndexOutOfBoundsException | BufferUnderflowException unused) {
                }
            }
            String format = String.format("Null type specifier is given: %d", Long.valueOf(j));
            C010708t.A0I("MobileConfigContextV2Impl", format);
            C04310Tq r0 = r4.A00;
            if (r0 != null) {
                r1 = (AnonymousClass09P) r0.get();
            } else {
                r1 = null;
            }
            if (r1 != null) {
                r1.CGT("MobileConfigContextV2Impl", format, 100000);
            }
            return C31801kP.A00;
        } else {
            AnonymousClass1ZN r15 = r4.A03;
            int i4 = r15.A07;
            if (i4 != 0) {
                return r15.A01.getInt(r15.A03(i4) + (A012 << 2));
            }
            return 0;
        }
    }
}
