package X;

/* renamed from: X.0uA  reason: invalid class name and case insensitive filesystem */
public final class C14810uA {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A05.A09("tincan/");
        A04 = r1;
        A02 = (AnonymousClass1Y7) r1.A09("has_shown_nux");
        AnonymousClass1Y7 r12 = A04;
        A03 = (AnonymousClass1Y7) r12.A09("retry_thread_creation");
        A00 = (AnonymousClass1Y7) r12.A09("registration_state");
        A01 = (AnonymousClass1Y7) r12.A09("device_status_last_sync");
    }
}
