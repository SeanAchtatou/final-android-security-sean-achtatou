package X;

import java.util.ArrayList;
import java.util.WeakHashMap;

/* renamed from: X.07d  reason: invalid class name and case insensitive filesystem */
public final class C007807d extends C007907e {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public boolean A04 = true;
    public final AnonymousClass04b A05 = new AnonymousClass04b();
    public final ArrayList A06 = new ArrayList();
    public final WeakHashMap A07 = new WeakHashMap();

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004d, code lost:
        if (r0 != false) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A00(X.C007807d r15) {
        /*
            monitor-enter(r15)
            r6 = 0
            r13 = -1
            java.util.ArrayList r0 = r15.A06     // Catch:{ all -> 0x00b1 }
            java.util.Iterator r12 = r0.iterator()     // Catch:{ all -> 0x00b1 }
        L_0x000a:
            boolean r0 = r12.hasNext()     // Catch:{ all -> 0x00b1 }
            if (r0 == 0) goto L_0x009f
            java.lang.Object r11 = r12.next()     // Catch:{ all -> 0x00b1 }
            X.07f r11 = (X.AnonymousClass07f) r11     // Catch:{ all -> 0x00b1 }
            long r3 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x00b1 }
            long r1 = r11.A04     // Catch:{ all -> 0x00b1 }
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x009d
            boolean r3 = X.AnonymousClass07f.A00(r11, r1)     // Catch:{ all -> 0x00b1 }
        L_0x0024:
            boolean r0 = r11.A05     // Catch:{ all -> 0x00b1 }
            if (r0 == 0) goto L_0x0049
            java.lang.ref.WeakReference r0 = r11.A08     // Catch:{ all -> 0x00b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00b1 }
            if (r0 != 0) goto L_0x0049
            java.lang.String r2 = "The wakelock "
            java.lang.String r1 = r11.A07     // Catch:{ all -> 0x00b1 }
            java.lang.String r0 = " was garbage collected before being released."
            java.lang.String r2 = X.AnonymousClass08S.A0P(r2, r1, r0)     // Catch:{ all -> 0x00b1 }
            java.lang.String r1 = "WakeLockMetricsCollector"
            r0 = 0
            X.AnonymousClass0KZ.A00(r1, r2, r0)     // Catch:{ all -> 0x00b1 }
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x00b1 }
            boolean r0 = X.AnonymousClass07f.A00(r11, r0)     // Catch:{ all -> 0x00b1 }
            goto L_0x004a
        L_0x0049:
            r0 = 0
        L_0x004a:
            if (r3 != 0) goto L_0x004f
            r1 = 0
            if (r0 == 0) goto L_0x0050
        L_0x004f:
            r1 = 1
        L_0x0050:
            boolean r0 = r11.A05     // Catch:{ all -> 0x00b1 }
            if (r0 == 0) goto L_0x0057
            int r6 = r6 + 1
            goto L_0x0060
        L_0x0057:
            if (r1 == 0) goto L_0x0060
            long r1 = r11.A03     // Catch:{ all -> 0x00b1 }
            int r0 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r0 <= 0) goto L_0x0060
            r13 = r1
        L_0x0060:
            java.lang.ref.WeakReference r0 = r11.A08     // Catch:{ all -> 0x00b1 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00b1 }
            if (r0 != 0) goto L_0x000a
            X.04b r1 = r15.A05     // Catch:{ all -> 0x00b1 }
            java.lang.String r0 = r11.A07     // Catch:{ all -> 0x00b1 }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x00b1 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x00b1 }
            X.04b r10 = r15.A05     // Catch:{ all -> 0x00b1 }
            java.lang.String r9 = r11.A07     // Catch:{ all -> 0x00b1 }
            if (r0 != 0) goto L_0x007b
            r7 = 0
            goto L_0x007f
        L_0x007b:
            long r7 = r0.longValue()     // Catch:{ all -> 0x00b1 }
        L_0x007f:
            long r4 = r11.A02     // Catch:{ all -> 0x00b1 }
            boolean r0 = r11.A05     // Catch:{ all -> 0x00b1 }
            if (r0 == 0) goto L_0x009a
            long r2 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x00b1 }
            long r0 = r11.A01     // Catch:{ all -> 0x00b1 }
            long r2 = r2 - r0
        L_0x008c:
            long r4 = r4 + r2
            long r7 = r7 + r4
            java.lang.Long r0 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x00b1 }
            r10.put(r9, r0)     // Catch:{ all -> 0x00b1 }
            r12.remove()     // Catch:{ all -> 0x00b1 }
            goto L_0x000a
        L_0x009a:
            r2 = 0
            goto L_0x008c
        L_0x009d:
            r3 = 0
            goto L_0x0024
        L_0x009f:
            int r0 = r15.A00     // Catch:{ all -> 0x00b1 }
            if (r0 == 0) goto L_0x00ad
            if (r6 != 0) goto L_0x00ad
            long r2 = r15.A03     // Catch:{ all -> 0x00b1 }
            long r0 = r15.A02     // Catch:{ all -> 0x00b1 }
            long r13 = r13 - r0
            long r2 = r2 + r13
            r15.A03 = r2     // Catch:{ all -> 0x00b1 }
        L_0x00ad:
            r15.A00 = r6     // Catch:{ all -> 0x00b1 }
            monitor-exit(r15)
            return
        L_0x00b1:
            r0 = move-exception
            monitor-exit(r15)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C007807d.A00(X.07d):void");
    }

    public AnonymousClass0FM A03() {
        return new C02590Fn(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b1, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(X.AnonymousClass0FM r12) {
        /*
            r11 = this;
            X.0Fn r12 = (X.C02590Fn) r12
            monitor-enter(r11)
            java.lang.String r0 = "Null value passed to getSnapshot!"
            X.C02740Gd.A00(r12, r0)     // Catch:{ all -> 0x00b3 }
            boolean r0 = r11.A04     // Catch:{ all -> 0x00b3 }
            r7 = 0
            if (r0 != 0) goto L_0x000f
            monitor-exit(r11)
            return r7
        L_0x000f:
            A00(r11)     // Catch:{ all -> 0x00b3 }
            long r0 = r11.A01     // Catch:{ all -> 0x00b3 }
            r12.acquiredCount = r0     // Catch:{ all -> 0x00b3 }
            long r4 = r11.A03     // Catch:{ all -> 0x00b3 }
            int r0 = r11.A00     // Catch:{ all -> 0x00b3 }
            if (r0 <= 0) goto L_0x0037
            long r2 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x00b3 }
            long r0 = r11.A02     // Catch:{ all -> 0x00b3 }
            long r2 = r2 - r0
        L_0x0023:
            long r4 = r4 + r2
            r12.heldTimeMs = r4     // Catch:{ all -> 0x00b3 }
            boolean r0 = r12.isAttributionEnabled     // Catch:{ all -> 0x00b3 }
            if (r0 == 0) goto L_0x00b0
            X.04b r0 = r12.tagTimeMs     // Catch:{ all -> 0x00b3 }
            r0.clear()     // Catch:{ all -> 0x00b3 }
            java.util.ArrayList r0 = r11.A06     // Catch:{ all -> 0x00b3 }
            int r8 = r0.size()     // Catch:{ all -> 0x00b3 }
            r6 = 0
            goto L_0x003a
        L_0x0037:
            r2 = 0
            goto L_0x0023
        L_0x003a:
            if (r6 >= r8) goto L_0x0076
            java.util.ArrayList r0 = r11.A06     // Catch:{ all -> 0x00b3 }
            java.lang.Object r2 = r0.get(r6)     // Catch:{ all -> 0x00b3 }
            X.07f r2 = (X.AnonymousClass07f) r2     // Catch:{ all -> 0x00b3 }
            long r3 = r2.A02     // Catch:{ all -> 0x00b3 }
            boolean r0 = r2.A05     // Catch:{ all -> 0x00b3 }
            if (r0 == 0) goto L_0x0061
            long r9 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x00b3 }
            long r0 = r2.A01     // Catch:{ all -> 0x00b3 }
            long r9 = r9 - r0
        L_0x0051:
            long r3 = r3 + r9
            java.lang.String r5 = r2.A07     // Catch:{ all -> 0x00b3 }
            X.04b r0 = r12.tagTimeMs     // Catch:{ all -> 0x00b3 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x00b3 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x00b3 }
            X.04b r2 = r12.tagTimeMs     // Catch:{ all -> 0x00b3 }
            if (r0 != 0) goto L_0x0067
            goto L_0x0064
        L_0x0061:
            r9 = 0
            goto L_0x0051
        L_0x0064:
            r0 = 0
            goto L_0x006b
        L_0x0067:
            long r0 = r0.longValue()     // Catch:{ all -> 0x00b3 }
        L_0x006b:
            long r0 = r0 + r3
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x00b3 }
            r2.put(r5, r0)     // Catch:{ all -> 0x00b3 }
            int r6 = r6 + 1
            goto L_0x003a
        L_0x0076:
            X.04b r0 = r11.A05     // Catch:{ all -> 0x00b3 }
            int r6 = r0.size()     // Catch:{ all -> 0x00b3 }
        L_0x007c:
            if (r7 >= r6) goto L_0x00b0
            X.04b r0 = r11.A05     // Catch:{ all -> 0x00b3 }
            java.lang.Object r5 = r0.A07(r7)     // Catch:{ all -> 0x00b3 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x00b3 }
            X.04b r0 = r12.tagTimeMs     // Catch:{ all -> 0x00b3 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x00b3 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x00b3 }
            X.04b r4 = r12.tagTimeMs     // Catch:{ all -> 0x00b3 }
            if (r0 != 0) goto L_0x0095
            r2 = 0
            goto L_0x0099
        L_0x0095:
            long r2 = r0.longValue()     // Catch:{ all -> 0x00b3 }
        L_0x0099:
            X.04b r0 = r11.A05     // Catch:{ all -> 0x00b3 }
            java.lang.Object r0 = r0.A09(r7)     // Catch:{ all -> 0x00b3 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x00b3 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x00b3 }
            long r2 = r2 + r0
            java.lang.Long r0 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x00b3 }
            r4.put(r5, r0)     // Catch:{ all -> 0x00b3 }
            int r7 = r7 + 1
            goto L_0x007c
        L_0x00b0:
            monitor-exit(r11)
            r0 = 1
            return r0
        L_0x00b3:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C007807d.A04(X.0FM):boolean");
    }
}
