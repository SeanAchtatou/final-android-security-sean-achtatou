package X;

import com.facebook.cameracore.mediapipeline.asyncscripting.IScriptingClient;
import com.facebook.cameracore.mediapipeline.asyncscripting.JsVm;
import java.util.concurrent.Callable;

/* renamed from: X.0Kt  reason: invalid class name and case insensitive filesystem */
public final class C03270Kt implements Callable {
    public final /* synthetic */ IScriptingClient A00;

    public C03270Kt(IScriptingClient iScriptingClient) {
        this.A00 = iScriptingClient;
    }

    public Object call() {
        return new JsVm(this.A00);
    }
}
