package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.collect.ImmutableList;

/* renamed from: X.17V  reason: invalid class name */
public final class AnonymousClass17V implements C04810Wg {
    public final /* synthetic */ AnonymousClass16R A00;

    public AnonymousClass17V(AnonymousClass16R r1) {
        this.A00 = r1;
    }

    public void BYh(Throwable th) {
        AnonymousClass16R.A05(this.A00, th);
    }

    public void Bqf(Object obj) {
        C74363hi r10 = (C74363hi) obj;
        if (r10 != null) {
            AnonymousClass16R r3 = this.A00;
            AnonymousClass16S r1 = r3.A0A;
            r1.A06 = r10.A00;
            r1.A05 = r10.A01;
            C20921Ei r32 = (C20921Ei) AnonymousClass1XX.A02(17, AnonymousClass1Y3.BMw, r3.A08);
            if (C20921Ei.A03(r32)) {
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r32.A00)).markerPoint(5505198, "merging_success");
            }
            AnonymousClass16R.A04(this.A00, C74373hj.A00(r10.A04), r10.A03);
            AnonymousClass16R r33 = this.A00;
            if (r33.A0O) {
                C33891oJ r34 = (C33891oJ) AnonymousClass1XX.A02(35, AnonymousClass1Y3.AoV, r33.A08);
                ImmutableList immutableList = r10.A02;
                if (!immutableList.isEmpty()) {
                    ImmutableList.Builder builder = ImmutableList.builder();
                    int At0 = (int) ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C35221qs) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AP0, r34.A00)).A00)).At0(564058055246515L);
                    synchronized (r34.A03) {
                        C24971Xv it = immutableList.iterator();
                        while (it.hasNext()) {
                            ThreadKey threadKey = (ThreadKey) it.next();
                            if (!r34.A03.contains(threadKey) && !C26681bq.A02((C26681bq) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AuB, r34.A00), threadKey).A0k(threadKey, At0)) {
                                builder.add((Object) threadKey);
                                r34.A03.add(threadKey);
                            }
                        }
                    }
                    ImmutableList build = builder.build();
                    if (!build.isEmpty()) {
                        C33891oJ.A01(r34, build, At0, C99084oO.$const$string(252));
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        AnonymousClass16R.A05(this.A00, null);
    }
}
