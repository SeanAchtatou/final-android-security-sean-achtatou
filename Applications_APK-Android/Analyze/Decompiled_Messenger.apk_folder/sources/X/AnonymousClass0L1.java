package X;

import android.view.Choreographer;

/* renamed from: X.0L1  reason: invalid class name */
public final class AnonymousClass0L1 implements Choreographer.FrameCallback {
    public final /* synthetic */ C009408b A00;

    public AnonymousClass0L1(C009408b r1) {
        this.A00 = r1;
    }

    public void doFrame(long j) {
        synchronized (this.A00) {
            this.A00.A0O(j);
        }
    }
}
