package X;

/* renamed from: X.0bM  reason: invalid class name and case insensitive filesystem */
public final class C06350bM implements C06320bJ, C07300dC {
    public int A00 = -1;
    public int A01 = -1;
    public C06360bN A02;
    public C06360bN A03;
    public boolean A04;
    private int A05 = -1;
    private int A06 = -1;
    private boolean A07;
    public final AnonymousClass0US A08;
    public final AnonymousClass0US A09;

    public boolean AlE() {
        return A03(true);
    }

    public boolean AlF() {
        return A03(false);
    }

    private void A00() {
        if (((AnonymousClass0UW) this.A08.get()).A05()) {
            AnonymousClass06K.A01("readAnalyticsBatchingConfig", 1650786985);
            try {
                C25721aI r1 = (C25721aI) this.A09.get();
                this.A07 = r1.AlE();
                this.A00 = Math.max(r1.Aco(), 1);
            } finally {
                AnonymousClass06K.A00(1626511971);
            }
        } else {
            throw new IllegalStateException("Cannot read params now");
        }
    }

    private void A01() {
        int B0Y;
        int B0X;
        A02((AnonymousClass0UW) this.A08.get());
        AnonymousClass06K.A01("readMaxEventsPerBatchQE", -1274246547);
        try {
            C25721aI r1 = (C25721aI) this.A09.get();
            if (this.A04) {
                B0Y = r1.Aot();
            } else {
                B0Y = r1.B0Y();
            }
            this.A06 = B0Y;
            if (this.A04) {
                B0X = r1.Aos();
            } else {
                B0X = r1.B0X();
            }
            this.A05 = B0X;
        } finally {
            AnonymousClass06K.A00(1756199061);
        }
    }

    private static void A02(AnonymousClass0UW r2) {
        AnonymousClass06K.A01("waitForInitialization", 749818);
        try {
            r2.A03();
        } finally {
            AnonymousClass06K.A00(1025244325);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (r0 != false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A03(boolean r5) {
        /*
            r4 = this;
            X.0US r0 = r4.A08
            java.lang.Object r0 = r0.get()
            X.0UW r0 = (X.AnonymousClass0UW) r0
            boolean r0 = r0.A05()
            r3 = 1
            if (r0 == 0) goto L_0x001a
            if (r5 != 0) goto L_0x001e
            int r2 = r4.A00
            r1 = -1
            r0 = 0
            if (r2 != r1) goto L_0x0018
            r0 = 1
        L_0x0018:
            if (r0 == 0) goto L_0x001e
        L_0x001a:
            if (r3 == 0) goto L_0x0020
            r0 = 0
            return r0
        L_0x001e:
            r3 = 0
            goto L_0x001a
        L_0x0020:
            int r2 = r4.A00
            r1 = -1
            r0 = 0
            if (r2 != r1) goto L_0x0027
            r0 = 1
        L_0x0027:
            if (r0 == 0) goto L_0x002c
            r4.A00()
        L_0x002c:
            boolean r0 = r4.A07
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06350bM.A03(boolean):boolean");
    }

    public int Aco() {
        if (!((AnonymousClass0UW) this.A08.get()).A05()) {
            return 1;
        }
        boolean z = false;
        if (this.A00 == -1) {
            z = true;
        }
        if (z) {
            A00();
        }
        return this.A00;
    }

    public C06360bN Ae0() {
        C06360bN r2;
        if (this.A02 == null) {
            A02((AnonymousClass0UW) this.A08.get());
            AnonymousClass06K.A01("readBackgroundParamsHiPriData", 29682377);
            try {
                C25721aI r1 = (C25721aI) this.A09.get();
                if (this.A04) {
                    r2 = new C06360bN(r1.Aon(), r1.Aol(), r1.Aom(), r1.Aok());
                } else {
                    r2 = new C06360bN(r1.B0S(), r1.B0Q(), r1.B0R(), r1.B0P());
                }
                this.A02 = r2;
            } finally {
                AnonymousClass06K.A00(2051442935);
            }
        }
        C06360bN r0 = this.A02;
        C000300h.A01(r0);
        return r0;
    }

    public int AfE() {
        if (this.A01 == -1) {
            A02((AnonymousClass0UW) this.A08.get());
            AnonymousClass06K.A01("readBufferSize", -910923147);
            try {
                this.A01 = ((C25721aI) this.A09.get()).AfE();
            } finally {
                AnonymousClass06K.A00(-1553807384);
            }
        }
        return this.A01;
    }

    public C06360bN AnO() {
        C06360bN r2;
        if (this.A03 == null) {
            A02((AnonymousClass0UW) this.A08.get());
            AnonymousClass06K.A01("readForegroundParamsHiPriData", 1588515448);
            try {
                C25721aI r1 = (C25721aI) this.A09.get();
                if (this.A04) {
                    r2 = new C06360bN(r1.Aor(), r1.Aop(), r1.Aoq(), r1.Aoo());
                } else {
                    r2 = new C06360bN(r1.B0W(), r1.B0U(), r1.B0V(), r1.B0T());
                }
                this.A03 = r2;
            } finally {
                AnonymousClass06K.A00(-416048736);
            }
        }
        C06360bN r0 = this.A03;
        C000300h.A01(r0);
        return r0;
    }

    public int AoP() {
        if (this.A05 == -1) {
            A01();
        }
        return this.A05;
    }

    public int B3S() {
        if (this.A06 == -1) {
            A01();
        }
        return this.A06;
    }

    public C06350bM(AnonymousClass0US r2, AnonymousClass0US r3, boolean z) {
        this.A08 = r2;
        this.A09 = r3;
        this.A04 = z;
    }
}
