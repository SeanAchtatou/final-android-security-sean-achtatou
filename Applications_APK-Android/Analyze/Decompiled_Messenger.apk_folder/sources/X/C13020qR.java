package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import androidx.fragment.app.Fragment;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: X.0qR  reason: invalid class name and case insensitive filesystem */
public abstract class C13020qR extends C27711dd {
    public final Activity A00;
    public final Context A01;
    public final Handler A02;
    public final C13060qW A03 = new C13050qV();

    public Object A03() {
        return !(this instanceof AnonymousClass14S) ? !(this instanceof AnonymousClass14V) ? ((C13010qQ) this).A00 : ((AnonymousClass14V) this).A00 : ((AnonymousClass14S) this).A00;
    }

    public void A04() {
        if (this instanceof C13010qQ) {
            ((C13010qQ) this).A00.CIW();
        }
    }

    public void A05(Fragment fragment) {
        AnonymousClass6b3 r0;
        if (this instanceof C13010qQ) {
            ((C13010qQ) this).A00.A11(fragment);
        } else if (this instanceof AnonymousClass14U) {
            AnonymousClass14U r1 = (AnonymousClass14U) this;
            if ((r1 instanceof AnonymousClass14T) && (r0 = ((AnonymousClass14T) r1).A00) != null) {
                r0.BOX(fragment);
            }
        }
    }

    public void A07(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this instanceof C13010qQ) {
            ((C13010qQ) this).A00.dump(str, fileDescriptor, printWriter, strArr);
        }
    }

    public boolean A08(Fragment fragment) {
        boolean z;
        if (this instanceof AnonymousClass14S) {
            return false;
        }
        if (this instanceof AnonymousClass14V) {
            z = ((AnonymousClass14V) this).A00.isFinishing();
        } else if (!(this instanceof C13010qQ)) {
            return true;
        } else {
            z = ((C13010qQ) this).A00.isFinishing();
        }
        return !z;
    }

    public LayoutInflater A02() {
        if (this instanceof AnonymousClass14T) {
            return ((AnonymousClass14T) this).A02;
        }
        if (!(this instanceof C13010qQ)) {
            return LayoutInflater.from(this.A01);
        }
        C13010qQ r2 = (C13010qQ) this;
        return r2.A00.getLayoutInflater().cloneInContext(r2.A00);
    }

    public void A06(Fragment fragment, Intent intent, int i, Bundle bundle) {
        Context context;
        if (this instanceof AnonymousClass14T) {
            context = ((AnonymousClass14T) this).A01;
        } else if (this instanceof C13010qQ) {
            ((C13010qQ) this).A00.A12(fragment, intent, i, bundle);
            return;
        } else if (i == -1) {
            context = this.A01;
        } else {
            throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
        context.startActivity(intent);
    }

    public C13020qR(Activity activity, Context context, Handler handler) {
        this.A00 = activity;
        AnonymousClass0qX.A01(context, "context == null");
        this.A01 = context;
        AnonymousClass0qX.A01(handler, "handler == null");
        this.A02 = handler;
    }
}
