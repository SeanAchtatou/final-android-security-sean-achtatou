package X;

import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.res.XmlResourceParser;
import android.net.Uri;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: X.1XS  reason: invalid class name */
public final class AnonymousClass1XS {
    public static final HashSet A09 = new HashSet(Arrays.asList(AnonymousClass0TQ.FILES_PATH, AnonymousClass0TQ.CACHE_PATH, AnonymousClass0TQ.EXTERNAL_FILES_PATH, AnonymousClass0TQ.EXTERNAL_CACHE_PATH));
    private static final HashMap A0A = new HashMap();
    public boolean A00 = false;
    private boolean A01 = false;
    public final AnonymousClass04e A02;
    public final HashMap A03 = new HashMap();
    public final HashMap A04 = new HashMap();
    public final boolean A05;
    private final Context A06;
    private final String A07;
    private final HashSet A08;

    private Uri A00(String str, String str2, String str3, boolean z) {
        String substring;
        boolean endsWith = str.endsWith("/");
        int length = str.length();
        if (endsWith) {
            substring = str3.substring(length);
        } else {
            substring = str3.substring(length + 1);
        }
        if (z) {
            str2 = AnonymousClass08S.A0J("secure_shared_", str2);
        }
        return new Uri.Builder().scheme("content").authority(this.A07).encodedPath(AnonymousClass08S.A07(Uri.encode(str2), '/', Uri.encode(substring, "/"))).build();
    }

    public static AnonymousClass1XS A01(Context context, ProviderInfo providerInfo, AnonymousClass04e r6) {
        String str;
        AnonymousClass1XS r0;
        if (providerInfo == null) {
            str = AnonymousClass08S.A0J(context.getApplicationContext().getPackageName(), ".securefileprovider");
        } else {
            str = providerInfo.authority;
        }
        HashMap hashMap = A0A;
        synchronized (hashMap) {
            r0 = (AnonymousClass1XS) hashMap.get(str);
            if (r0 == null) {
                try {
                    r0 = new AnonymousClass1XS(context, providerInfo, r6);
                    hashMap.put(str, r0);
                } catch (IOException | XmlPullParserException e) {
                    String format = String.format("Failed to parse %s meta-data.", "com.facebook.secure.fileprovider.SECURE_FILE_PROVIDER_PATHS");
                    r6.C2T("SecurePathStrategy", format, e);
                    throw new IllegalArgumentException(format);
                }
            }
        }
        return r0;
    }

    public static C182848e0 A02(AnonymousClass1XS r5, AnonymousClass0TQ r6) {
        C182848e0 r4;
        synchronized (r5.A04) {
            r4 = (C182848e0) r5.A04.get(r6);
            if (r4 == null) {
                if (A09.contains(r6)) {
                    r4 = new C182848e0(new File(r6.A00(r5.A06), "secure_shared").getCanonicalFile(), new AnonymousClass1XR());
                    r5.A04.put(r6, r4);
                } else {
                    throw new IllegalArgumentException("No directory manager defined for " + r6);
                }
            }
        }
        return r4;
    }

    private void A04() {
        if (!this.A01) {
            synchronized (this.A03) {
                if (!this.A01) {
                    Iterator it = this.A08.iterator();
                    while (it.hasNext()) {
                        AnonymousClass0TS r2 = (AnonymousClass0TS) it.next();
                        String str = r2.A01;
                        File A002 = r2.A00.A00(this.A06);
                        String[] strArr = {r2.A02};
                        for (int i = 0; i < 1; i++) {
                            String str2 = strArr[i];
                            if (str2 != null) {
                                String trim = str2.trim();
                                if (trim.trim().length() != 0) {
                                    A002 = new File(A002, trim);
                                }
                            }
                        }
                        if (str == null || str.trim().length() == 0) {
                            this.A02.C2T("SecurePathStrategy", "Path names may not be empty", null);
                        } else {
                            this.A03.put(str, A002.getCanonicalFile());
                        }
                    }
                    this.A01 = true;
                }
            }
        }
    }

    private AnonymousClass1XS(Context context, ProviderInfo providerInfo, AnonymousClass04e r11) {
        AnonymousClass04e r3;
        Object[] objArr;
        String str;
        this.A02 = r11;
        this.A06 = context;
        if (providerInfo == null || providerInfo.metaData == null) {
            this.A07 = AnonymousClass08S.A0J(context.getApplicationContext().getPackageName(), ".securefileprovider");
            providerInfo = context.getPackageManager().resolveContentProvider(this.A07, 2176);
        } else {
            this.A07 = providerInfo.authority;
        }
        if (providerInfo == null) {
            r3 = this.A02;
            objArr = new Object[]{this.A07};
            str = "Could not retrieve provider info for %s";
        } else {
            this.A05 = providerInfo.grantUriPermissions;
            XmlResourceParser loadXmlMetaData = providerInfo.loadXmlMetaData(context.getPackageManager(), "com.facebook.secure.fileprovider.SECURE_FILE_PROVIDER_PATHS");
            if (loadXmlMetaData == null) {
                r3 = this.A02;
                objArr = new Object[]{"com.facebook.secure.fileprovider.SECURE_FILE_PROVIDER_PATHS"};
                str = "Could not read %s meta-data";
            } else {
                LinkedList linkedList = new LinkedList();
                while (true) {
                    int next = loadXmlMetaData.next();
                    if (next == 1) {
                        this.A08 = new HashSet(linkedList);
                        return;
                    } else if (next == 2) {
                        String name = loadXmlMetaData.getName();
                        if ("paths".equals(name)) {
                            continue;
                        } else {
                            AnonymousClass0TQ r32 = (AnonymousClass0TQ) AnonymousClass0TQ.A00.get(name);
                            if (r32 != null) {
                                linkedList.add(new AnonymousClass0TS(loadXmlMetaData.getAttributeValue(null, "name"), r32, loadXmlMetaData.getAttributeValue(null, "path")));
                            } else {
                                throw new IllegalArgumentException(AnonymousClass08S.A0J("Unrecognized storage root ", name));
                            }
                        }
                    }
                }
            }
        }
        r3.C2T("SecurePathStrategy", String.format(str, objArr), null);
        this.A08 = new HashSet();
    }

    public static Map.Entry A03(AnonymousClass1XS r4, File file) {
        String canonicalPath = file.getCanonicalPath();
        if (!r4.A00) {
            synchronized (r4.A04) {
                if (!r4.A00) {
                    Iterator it = A09.iterator();
                    while (it.hasNext()) {
                        A02(r4, (AnonymousClass0TQ) it.next());
                    }
                    r4.A00 = true;
                }
            }
        }
        for (Map.Entry entry : r4.A04.entrySet()) {
            if (canonicalPath.startsWith(((C182848e0) entry.getValue()).A00().getPath())) {
                return entry;
            }
        }
        return null;
    }

    public Uri A05(File file) {
        int length;
        Map.Entry A032 = A03(this, file);
        if (A032 != null) {
            return A00(((C182848e0) A032.getValue()).A00().getPath(), ((AnonymousClass0TQ) A032.getKey()).mTagName, file.getCanonicalPath(), true);
        } else if (this.A05) {
            String canonicalPath = file.getCanonicalPath();
            A04();
            Map.Entry entry = null;
            int i = 0;
            for (Map.Entry entry2 : this.A03.entrySet()) {
                String canonicalPath2 = ((File) entry2.getValue()).getCanonicalPath();
                if (canonicalPath.startsWith(canonicalPath2) && (length = canonicalPath2.length()) > i) {
                    i = length;
                    entry = entry2;
                }
            }
            if (entry != null) {
                return A00(((File) entry.getValue()).getPath(), (String) entry.getKey(), file.getCanonicalPath(), false);
            }
            StringBuilder sb = new StringBuilder(file.getCanonicalPath());
            for (Map.Entry value : this.A03.entrySet()) {
                sb.append(", ");
                sb.append(((File) value.getValue()).getCanonicalPath());
            }
            throw new SecurityException(AnonymousClass08S.A0J("Resolved path jumped beyond configured direct roots: ", sb.toString()));
        } else {
            throw new SecurityException(AnonymousClass08S.A0J("Resolved path jumped beyond configured temporary roots: ", file.getPath()));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x009f, code lost:
        if (r3.getPath().startsWith(r2.getPath()) != false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x005e, code lost:
        if (r3.getPath().startsWith(r4.getPath()) != false) goto L_0x0060;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File A06(android.net.Uri r8) {
        /*
            r7 = this;
            java.lang.String r6 = r8.getEncodedPath()
            r5 = 1
            r0 = 47
            int r3 = r6.indexOf(r0, r5)
            java.lang.String r0 = r6.substring(r5, r3)
            java.lang.String r1 = android.net.Uri.decode(r0)
            java.lang.String r0 = "secure_shared"
            boolean r0 = r1.startsWith(r0)
            if (r0 != 0) goto L_0x0067
            boolean r0 = r7.A05
            if (r0 == 0) goto L_0x00bc
            r7.A04()
            java.lang.String r3 = r8.getEncodedPath()
            r0 = 47
            int r1 = r3.indexOf(r0, r5)
            java.lang.String r0 = r3.substring(r5, r1)
            java.lang.String r2 = android.net.Uri.decode(r0)
            int r1 = r1 + r5
            java.lang.String r0 = r3.substring(r1)
            java.lang.String r1 = android.net.Uri.decode(r0)
            java.util.HashMap r0 = r7.A03
            java.lang.Object r4 = r0.get(r2)
            java.io.File r4 = (java.io.File) r4
            java.lang.String r2 = "Resolved path jumped beyond configured roots"
            if (r4 == 0) goto L_0x00b6
            java.io.File r0 = new java.io.File
            r0.<init>(r4, r1)
            java.io.File r3 = r0.getCanonicalFile()
            java.lang.String r1 = r3.getPath()
            java.lang.String r0 = r4.getPath()
            boolean r0 = r1.startsWith(r0)
            if (r0 == 0) goto L_0x00b6
        L_0x0060:
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x00a2
            return r3
        L_0x0067:
            r0 = 14
            java.lang.String r1 = r1.substring(r0)
            java.util.HashMap r0 = X.AnonymousClass0TQ.A00
            java.lang.Object r0 = r0.get(r1)
            X.0TQ r0 = (X.AnonymousClass0TQ) r0
            X.8e0 r0 = A02(r7, r0)
            java.lang.String r4 = "Resolved path jumped beyond configured roots"
            if (r0 == 0) goto L_0x00c4
            java.io.File r2 = r0.A00()
            int r3 = r3 + r5
            java.lang.String r0 = r6.substring(r3)
            java.lang.String r1 = android.net.Uri.decode(r0)
            java.io.File r0 = new java.io.File
            r0.<init>(r2, r1)
            java.io.File r3 = r0.getCanonicalFile()
            java.lang.String r1 = r3.getPath()
            java.lang.String r0 = r2.getPath()
            boolean r0 = r1.startsWith(r0)
            if (r0 == 0) goto L_0x00c4
            goto L_0x0060
        L_0x00a2:
            java.io.FileNotFoundException r2 = new java.io.FileNotFoundException
            java.lang.String r0 = r3.getPath()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "File %s not found"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r2.<init>(r0)
            throw r2
        L_0x00b6:
            java.lang.SecurityException r0 = new java.lang.SecurityException
            r0.<init>(r2)
            throw r0
        L_0x00bc:
            java.lang.SecurityException r1 = new java.lang.SecurityException
            java.lang.String r0 = "Direct access to shared files is not enabled."
            r1.<init>(r0)
            throw r1
        L_0x00c4:
            java.lang.SecurityException r0 = new java.lang.SecurityException
            r0.<init>(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1XS.A06(android.net.Uri):java.io.File");
    }
}
