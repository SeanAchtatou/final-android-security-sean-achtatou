package X;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1U8  reason: invalid class name */
public final class AnonymousClass1U8 implements AnonymousClass1U7 {
    private static volatile AnonymousClass1U8 A01;
    private ImmutableMap A00;

    public static final AnonymousClass1U8 A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass1U8.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new AnonymousClass1U8();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private ImmutableMap A01() {
        if (this.A00 == null) {
            ImmutableMap.Builder builder = ImmutableMap.builder();
            builder.put("profile_image_small_size", Integer.valueOf(C26901cK.A01()));
            builder.put("profile_image_big_size", Integer.valueOf(C26901cK.A00()));
            builder.put("scale", C26901cK.A03());
            this.A00 = builder.build();
        }
        return this.A00;
    }

    public ImmutableSet Ax9() {
        return A01().keySet();
    }

    public Object AxA(String str, C10880l0 r3) {
        return A01().get(str);
    }
}
