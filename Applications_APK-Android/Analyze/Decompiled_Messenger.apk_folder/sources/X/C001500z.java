package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.00z  reason: invalid class name and case insensitive filesystem */
public final class C001500z extends Enum {
    private static final /* synthetic */ C001500z[] A00;
    public static final C001500z A01;
    public static final C001500z A02;
    public static final C001500z A03;
    public static final C001500z A04;
    public static final C001500z A05;
    public static final C001500z A06;
    public static final C001500z A07;
    public static final C001500z A08;
    public static final C001500z A09;
    public static final C001500z A0A;
    public static final C001500z A0B;
    public static final C001500z A0C;
    public static final C001500z A0D;

    static {
        C001500z r7 = new C001500z("MESSENGER", 0);
        A07 = r7;
        C001500z r3 = new C001500z("FB4A", 1);
        A03 = r3;
        C001500z r2 = new C001500z("PAA", 2);
        A08 = r2;
        C001500z r1 = new C001500z("SAMPLE", 3);
        A0B = r1;
        C001500z r0 = new C001500z("GIFTS", 4);
        C001500z r02 = new C001500z("NATIVEMSITE", 5);
        C001500z r03 = new C001500z("HOME", 6);
        C001500z r04 = new C001500z("GAMESPORTAL", 7);
        C001500z r11 = new C001500z("PHONE", 8);
        A0A = r11;
        C001500z r05 = new C001500z("PETOPE", 9);
        C001500z r12 = new C001500z("GROUPS", 10);
        A06 = r12;
        C001500z r06 = new C001500z("ME", 11);
        C001500z r07 = new C001500z("MOMENTS", 12);
        C001500z r08 = new C001500z("APPMANAGER", 13);
        C001500z r8 = new C001500z("FACEBOOK_SERVICES", 14);
        C001500z r82 = new C001500z("GETAPPS", 15);
        C001500z r83 = new C001500z("FBCAMERA", 16);
        C001500z r15 = new C001500z("ALOHA", 17);
        A01 = r15;
        C001500z r14 = new C001500z("PARTIES", 18);
        A09 = r14;
        C001500z r84 = new C001500z("ARSTUDIO_PLAYER", 19);
        C001500z r13 = new C001500z("FBCREATORS", 20);
        A04 = r13;
        C001500z r10 = new C001500z("TALK", 21);
        A0C = r10;
        C001500z r16 = new C001500z("SPACESHIP", 22);
        C001500z r162 = new C001500z("LYNX", 23);
        C001500z r163 = new C001500z("ANNA", 24);
        C001500z r164 = new C001500z("STUDY", 25);
        C001500z r9 = new C001500z("GAMES", 26);
        A05 = r9;
        C001500z r165 = new C001500z("VIDEOS", 27);
        C001500z r166 = new C001500z("KOTOTORO", 28);
        C001500z r37 = new C001500z("CRS", 29);
        C001500z r372 = new C001500z("WORKSPEED", 30);
        C001500z r373 = new C001500z("SOCAL", 31);
        C001500z r85 = new C001500z("CREATOR_STUDIO", 32);
        A02 = r85;
        C001500z r6 = new C001500z("OCULUS_TWILIGHT", 33);
        C001500z r5 = new C001500z("UNKNOWN", 34);
        A0D = r5;
        C001500z[] r4 = new C001500z[35];
        System.arraycopy(new C001500z[]{r7, r3, r2, r1, r0, r02, r03, r04, r11, r05, r12, r06, r07, r08, r8, r82, r83, r15, r14, r84, r13, r10, r16, r162, r163, r164, r9}, 0, r4, 0, 27);
        System.arraycopy(new C001500z[]{r165, r166, r37, r372, r373, r85, r6, r5}, 0, r4, 27, 8);
        A00 = r4;
    }

    public static C001500z valueOf(String str) {
        return (C001500z) Enum.valueOf(C001500z.class, str);
    }

    public static C001500z[] values() {
        return (C001500z[]) A00.clone();
    }

    private C001500z(String str, int i) {
    }
}
