package X;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0i9  reason: invalid class name */
public final class AnonymousClass0i9 {
    public final AtomicReference A00 = new AtomicReference(false);
    private final C06940cL A01;

    public static boolean A00(AnonymousClass0i9 r1, Integer num, Locale locale) {
        Supplier supplier;
        C06940cL r12 = r1.A01;
        switch (num.intValue()) {
            case 0:
                supplier = r12.A00;
                break;
            case 1:
                supplier = r12.A01;
                break;
            default:
                throw new IllegalArgumentException("Unhandled language set type");
        }
        return ((ImmutableSet) supplier.get()).contains(locale.toString());
    }

    public boolean A01(Locale locale) {
        if (!A00(this, AnonymousClass07B.A01, locale) || ((Boolean) this.A00.get()).booleanValue()) {
            return false;
        }
        return true;
    }

    public AnonymousClass0i9(C06940cL r3) {
        this.A01 = r3;
    }
}
