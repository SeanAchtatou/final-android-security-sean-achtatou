package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1NL  reason: invalid class name */
public final class AnonymousClass1NL extends C17770zR {
    public static final MigColorScheme A03 = C17190yT.A00();
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public MigColorScheme A01 = A03;
    @Comparable(type = 3)
    public boolean A02 = true;

    public AnonymousClass1NL(Context context) {
        super("M4SwipeDeleteButton");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
