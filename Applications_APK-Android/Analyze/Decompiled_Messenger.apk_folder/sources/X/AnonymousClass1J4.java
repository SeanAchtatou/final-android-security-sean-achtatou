package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.ui.name.ThreadNameViewData;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1J4  reason: invalid class name */
public final class AnonymousClass1J4 extends C17770zR {
    public static final MigColorScheme A04 = C17190yT.A00();
    @Comparable(type = 13)
    public ThreadNameViewData A00;
    @Comparable(type = 13)
    public MigColorScheme A01 = A04;
    @Comparable(type = 13)
    public AnonymousClass10J A02;
    @Comparable(type = 3)
    public boolean A03;

    public AnonymousClass1J4() {
        super("M4ThreadNameComponent");
    }
}
