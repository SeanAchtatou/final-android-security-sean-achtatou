package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Rz  reason: invalid class name */
public final class AnonymousClass1Rz extends C17770zR {
    @Comparable(type = 13)
    public MigColorScheme A00;
    @Comparable(type = 13)
    public AnonymousClass10J A01;
    @Comparable(type = 13)
    public CharSequence A02;

    public AnonymousClass1Rz() {
        super("M4ThreadItemLastMessageSnippetComponent");
    }
}
