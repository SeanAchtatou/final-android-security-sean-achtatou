package X;

import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.attachment.AudioData;
import com.facebook.messaging.model.attachment.ImageData;
import com.facebook.messaging.model.attachment.VideoData;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0w6  reason: invalid class name and case insensitive filesystem */
public final class C15860w6 {
    public int A00;
    public long A01;
    public AudioData A02;
    public ImageData A03;
    public VideoData A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public Map A0A;
    public byte[] A0B;
    public final String A0C;
    public final String A0D;

    public C15860w6(Attachment attachment) {
        HashMap hashMap;
        String str = attachment.A09;
        Preconditions.checkNotNull(str);
        this.A0C = str;
        String str2 = attachment.A0B;
        Preconditions.checkNotNull(str2);
        this.A0D = str2;
        this.A06 = attachment.A07;
        this.A09 = attachment.A0C;
        this.A07 = attachment.A08;
        this.A00 = attachment.A00;
        this.A03 = attachment.A03;
        this.A04 = attachment.A04;
        this.A02 = attachment.A02;
        this.A05 = attachment.A06;
        this.A0B = attachment.A0D;
        this.A08 = attachment.A0A;
        ImmutableMap immutableMap = attachment.A05;
        if (immutableMap == null) {
            hashMap = new HashMap();
        } else {
            hashMap = new HashMap(immutableMap);
        }
        this.A0A = hashMap;
        this.A01 = attachment.A01;
    }

    public C15860w6(String str, String str2) {
        Preconditions.checkNotNull(str);
        this.A0C = str;
        Preconditions.checkNotNull(str2);
        this.A0D = str2;
    }
}
