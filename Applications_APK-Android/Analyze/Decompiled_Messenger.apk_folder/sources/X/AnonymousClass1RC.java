package X;

import android.content.Context;
import android.content.Intent;
import java.lang.ref.WeakReference;

/* renamed from: X.1RC  reason: invalid class name */
public final class AnonymousClass1RC implements AnonymousClass06U {
    private WeakReference A00;

    public AnonymousClass1RC(C21901Jd r2) {
        this.A00 = new WeakReference(r2);
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r7) {
        int A002 = AnonymousClass09Y.A00(-482485388);
        C21901Jd r2 = (C21901Jd) this.A00.get();
        if (!(r2 == null || r2.A0D == null || !intent.getParcelableArrayListExtra(C99084oO.$const$string(8)).contains(r2.A0D.A03))) {
            C21901Jd.A04(r2);
        }
        AnonymousClass09Y.A01(-612210884, A002);
    }
}
