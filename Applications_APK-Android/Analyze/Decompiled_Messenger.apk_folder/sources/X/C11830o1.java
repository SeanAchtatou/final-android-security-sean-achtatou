package X;

import java.lang.reflect.Array;
import java.util.HashSet;

/* renamed from: X.0o1  reason: invalid class name and case insensitive filesystem */
public final class C11830o1 {
    public CZ5 _booleanBuilder = null;
    public CZ6 _byteBuilder = null;
    public CZ7 _doubleBuilder = null;
    public CZ8 _floatBuilder = null;
    public CZ9 _intBuilder = null;
    public CZA _longBuilder = null;
    public CZB _shortBuilder = null;

    public static Object[] insertInListNoDup(Object[] objArr, Object obj) {
        int length = objArr.length;
        int i = 0;
        while (i < length) {
            if (objArr[i] != obj) {
                i++;
            } else if (i == 0) {
                return objArr;
            } else {
                Object[] objArr2 = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), length);
                System.arraycopy(objArr, 0, objArr2, 1, i);
                objArr2[0] = obj;
                int i2 = i + 1;
                int i3 = length - i2;
                if (i3 > 0) {
                    System.arraycopy(objArr, i2, objArr2, i2, i3);
                }
                return objArr2;
            }
        }
        Object[] objArr3 = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), length + 1);
        if (length > 0) {
            System.arraycopy(objArr, 0, objArr3, 1, length);
        }
        objArr3[0] = obj;
        return objArr3;
    }

    public static HashSet arrayToSet(Object[] objArr) {
        HashSet hashSet = new HashSet();
        if (objArr != null) {
            for (Object add : objArr) {
                hashSet.add(add);
            }
        }
        return hashSet;
    }
}
