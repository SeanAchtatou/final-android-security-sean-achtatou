package X;

import android.os.PowerManager;
import com.google.common.base.Preconditions;

/* renamed from: X.0tE  reason: invalid class name and case insensitive filesystem */
public final class C14390tE {
    public int A00 = 0;
    public int A01;
    public long A02 = 0;
    public long A03 = 0;
    public boolean A04 = false;
    public boolean A05;
    public boolean A06 = true;
    public final PowerManager.WakeLock A07;
    public final String A08;
    public final /* synthetic */ C14350tA A09;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        if (r1 == 0) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A00() {
        /*
            r4 = this;
            monitor-enter(r4)
            r3 = r4
            monitor-enter(r3)     // Catch:{ all -> 0x0043 }
            boolean r0 = r4.A04     // Catch:{ all -> 0x0040 }
            if (r0 != 0) goto L_0x0038
            boolean r0 = r4.A06     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x0013
            int r1 = r4.A01     // Catch:{ all -> 0x0040 }
            int r0 = r1 + 1
            r4.A01 = r0     // Catch:{ all -> 0x0040 }
            if (r1 != 0) goto L_0x002b
        L_0x0013:
            int r0 = r4.A00     // Catch:{ all -> 0x0040 }
            r2 = 1
            int r0 = r0 + r2
            r4.A00 = r0     // Catch:{ all -> 0x0040 }
            boolean r0 = r4.A04()     // Catch:{ all -> 0x0040 }
            if (r0 != 0) goto L_0x0029
            X.0tA r0 = r4.A09     // Catch:{ all -> 0x0040 }
            X.069 r0 = r0.A02     // Catch:{ all -> 0x0040 }
            long r0 = r0.now()     // Catch:{ all -> 0x0040 }
            r4.A02 = r0     // Catch:{ all -> 0x0040 }
        L_0x0029:
            r4.A05 = r2     // Catch:{ all -> 0x0040 }
        L_0x002b:
            android.os.PowerManager$WakeLock r2 = r4.A07     // Catch:{ all -> 0x0040 }
            r2.acquire()     // Catch:{ all -> 0x0040 }
            r0 = -1
            X.C009107w.A01(r2, r0)     // Catch:{ all -> 0x0040 }
            monitor-exit(r3)     // Catch:{ all -> 0x0043 }
            monitor-exit(r4)
            return
        L_0x0038:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0040 }
            java.lang.String r0 = "WakeLock already disposed"
            r1.<init>(r0)     // Catch:{ all -> 0x0040 }
            throw r1     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0043 }
            throw r0     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14390tE.A00():void");
    }

    public synchronized void A01() {
        A02(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0029, code lost:
        if (r0 == 0) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A02(int r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r0 = r6.A04     // Catch:{ all -> 0x0046 }
            if (r0 != 0) goto L_0x0044
            boolean r0 = r6.A06     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x000e
            int r0 = r6.A01     // Catch:{ all -> 0x0046 }
            if (r0 != 0) goto L_0x000e
            goto L_0x0044
        L_0x000e:
            if (r7 != 0) goto L_0x0011
            goto L_0x001a
        L_0x0011:
            android.os.PowerManager$WakeLock r0 = r6.A07     // Catch:{ all -> 0x0046 }
            r0.release(r7)     // Catch:{ all -> 0x0046 }
            X.C009107w.A00(r0)     // Catch:{ all -> 0x0046 }
            goto L_0x001f
        L_0x001a:
            android.os.PowerManager$WakeLock r0 = r6.A07     // Catch:{ all -> 0x0046 }
            X.C009007v.A01(r0)     // Catch:{ all -> 0x0046 }
        L_0x001f:
            boolean r0 = r6.A06     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x002b
            int r0 = r6.A01     // Catch:{ all -> 0x0046 }
            int r0 = r0 + -1
            r6.A01 = r0     // Catch:{ all -> 0x0046 }
            if (r0 != 0) goto L_0x0044
        L_0x002b:
            boolean r0 = r6.A04()     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0041
            long r4 = r6.A03     // Catch:{ all -> 0x0046 }
            X.0tA r0 = r6.A09     // Catch:{ all -> 0x0046 }
            X.069 r0 = r0.A02     // Catch:{ all -> 0x0046 }
            long r2 = r0.now()     // Catch:{ all -> 0x0046 }
            long r0 = r6.A02     // Catch:{ all -> 0x0046 }
            long r2 = r2 - r0
            long r4 = r4 + r2
            r6.A03 = r4     // Catch:{ all -> 0x0046 }
        L_0x0041:
            r0 = 0
            r6.A05 = r0     // Catch:{ all -> 0x0046 }
        L_0x0044:
            monitor-exit(r6)
            return
        L_0x0046:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14390tE.A02(int):void");
    }

    public synchronized void A03(boolean z) {
        if (z) {
            Preconditions.checkState(this.A06, "Wake lock cannot go from non-refcounted to refcounted");
        }
        this.A06 = z;
        C009007v.A03(this.A07, z);
    }

    public synchronized boolean A04() {
        return this.A05;
    }

    public C14390tE(C14350tA r5, PowerManager.WakeLock wakeLock, String str) {
        this.A09 = r5;
        this.A07 = wakeLock;
        this.A08 = AnonymousClass08S.A09(str, (int) (Math.random() * 1000.0d));
    }
}
