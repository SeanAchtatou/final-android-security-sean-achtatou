package X;

import com.facebook.common.util.TriState;

/* renamed from: X.0Wy  reason: invalid class name and case insensitive filesystem */
public final class C04940Wy implements C04310Tq {
    public final /* synthetic */ AnonymousClass1YI A00;

    public C04940Wy(AnonymousClass1YI r1) {
        this.A00 = r1;
    }

    public Object get() {
        boolean z = false;
        if (this.A00.AbO(655, false) || this.A00.AbO(656, false)) {
            z = true;
        }
        if (z) {
            return TriState.YES;
        }
        return TriState.NO;
    }
}
