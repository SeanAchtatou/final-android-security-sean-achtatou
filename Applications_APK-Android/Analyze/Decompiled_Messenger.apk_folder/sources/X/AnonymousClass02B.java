package X;

import android.os.Trace;

/* renamed from: X.02B  reason: invalid class name */
public final class AnonymousClass02B {
    public static void A00() {
        Trace.endSection();
    }

    public static void A01(String str, String str2, String str3) {
        String A0P = AnonymousClass08S.A0P(str, str2, str3);
        if (A0P.length() > 127 && str2 != null) {
            A0P = AnonymousClass08S.A0P(str, str2.substring(0, (127 - str.length()) - str3.length()), str3);
        }
        Trace.beginSection(A0P);
    }
}
