package X;

import android.util.LruCache;

/* renamed from: X.0Yt  reason: invalid class name and case insensitive filesystem */
public final class C05390Yt extends LruCache {
    public void entryRemoved(boolean z, Object obj, Object obj2, Object obj3) {
        AnonymousClass15E r6 = (AnonymousClass15E) obj2;
        if (!r6.A00) {
            try {
                AnonymousClass0YX.A0E.invoke(r6.A01, new Object[0]);
            } catch (Exception unused) {
            }
            r6.A00 = true;
        }
    }

    public C05390Yt(int i) {
        super(i);
    }
}
