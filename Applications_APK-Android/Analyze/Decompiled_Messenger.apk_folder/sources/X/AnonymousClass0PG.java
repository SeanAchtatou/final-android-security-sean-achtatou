package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0PG  reason: invalid class name */
public final class AnonymousClass0PG extends C03160Kg {
    public long A00() {
        return -2479634339626480691L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        C02520Fg r32 = (C02520Fg) r3;
        dataOutput.writeLong(r32.mobileBytesRx);
        dataOutput.writeLong(r32.mobileBytesTx);
        dataOutput.writeLong(r32.wifiBytesRx);
        dataOutput.writeLong(r32.wifiBytesTx);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        C02520Fg r32 = (C02520Fg) r3;
        r32.mobileBytesRx = dataInput.readLong();
        r32.mobileBytesTx = dataInput.readLong();
        r32.wifiBytesRx = dataInput.readLong();
        r32.wifiBytesTx = dataInput.readLong();
        return true;
    }
}
