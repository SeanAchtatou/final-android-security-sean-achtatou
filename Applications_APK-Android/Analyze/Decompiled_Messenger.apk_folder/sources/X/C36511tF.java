package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.acra.LogCatCollector;
import com.facebook.auth.userscope.UserScoped;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;

@UserScoped
/* renamed from: X.1tF  reason: invalid class name and case insensitive filesystem */
public final class C36511tF {
    private static C05540Zi A06;
    public static final String[] A07 = {C197619o.A02.A00, C197619o.A01.A00};
    public final AnonymousClass18N A00;
    public final ExecutorService A01;
    public final AtomicReference A02 = new AtomicReference(null);
    public final C04310Tq A03;
    private final AnonymousClass187 A04;
    private final C194918j A05;

    public synchronized void A04(List list) {
        if (list == null) {
            C010708t.A0J("OtherDevicesDAO", "Received request to add a null list of devices");
        } else {
            SQLiteDatabase A012 = ((AnonymousClass183) this.A03.get()).A01();
            if (A012 != null) {
                C007406x.A01(A012, 584354223);
                try {
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        AwE awE = (AwE) it.next();
                        C22338Aw9 aw9 = awE.address;
                        if (aw9 == null) {
                            C010708t.A0J("OtherDevicesDAO", "Received a personal device info packet without an address");
                        } else {
                            A01(this, A012, aw9.instance_id, awE.device_type);
                        }
                    }
                    A012.setTransactionSuccessful();
                    C007406x.A02(A012, 31499386);
                } catch (C37911wY | C37921wZ | IOException e) {
                    C010708t.A0S("OtherDevicesDAO", e, "There was a problem storing the information about the users devices");
                } catch (Throwable th) {
                    C007406x.A02(A012, -1451900054);
                    throw th;
                }
            }
        }
    }

    public static final C36511tF A00(AnonymousClass1XY r4) {
        C36511tF r0;
        synchronized (C36511tF.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r4)) {
                    A06.A00 = new C36511tF((AnonymousClass1XY) A06.A01());
                }
                C05540Zi r1 = A06;
                r0 = (C36511tF) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public static boolean A01(C36511tF r7, SQLiteDatabase sQLiteDatabase, String str, String str2) {
        boolean z;
        AnonymousClass0W6 r2 = C197619o.A00;
        C06140av A032 = C06160ax.A03(r2.A00, str);
        int A012 = r7.A05.A01();
        ContentValues contentValues = new ContentValues();
        contentValues.put(C197619o.A02.A00, r7.A04.A03(str2.getBytes(LogCatCollector.UTF_8_ENCODING), A012));
        contentValues.put(C197619o.A01.A00, Integer.valueOf(A012));
        if (sQLiteDatabase.update("other_devices", contentValues, A032.A02(), A032.A04()) == 0) {
            contentValues.put(r2.A00, str);
            C007406x.A00(-280820723);
            sQLiteDatabase.insertWithOnConflict("other_devices", null, contentValues, 5);
            C007406x.A00(1061432789);
            z = true;
        } else {
            z = false;
        }
        if (r7.A00.A02().equals(str)) {
            r7.A02.set(true);
        }
        return z;
    }

    public static byte[] A02(C36511tF r4, Cursor cursor) {
        int i;
        if (!C197619o.A01.A06(cursor)) {
            i = C197619o.A01.A00(cursor);
        } else {
            i = -1;
        }
        return r4.A04.A04(C197619o.A02.A07(cursor), "DeviceName", i);
    }

    public void A03() {
        SQLiteDatabase A012 = ((AnonymousClass183) this.A03.get()).A01();
        if (A012 != null) {
            A012.delete("other_devices", null, null);
            this.A02.set(false);
        }
    }

    public boolean A05(String str) {
        C06140av A032 = C06160ax.A03(C197619o.A00.A00, str);
        SQLiteDatabase A012 = ((AnonymousClass183) this.A03.get()).A01();
        if (A012 == null) {
            return false;
        }
        Cursor query = A012.query("other_devices", C197019i.A01, A032.A02(), A032.A04(), null, null, null, "1");
        try {
            return query.moveToNext();
        } finally {
            query.close();
        }
    }

    private C36511tF(AnonymousClass1XY r3) {
        this.A03 = AnonymousClass0VB.A00(AnonymousClass1Y3.BTf, r3);
        this.A04 = AnonymousClass187.A01(r3);
        this.A00 = AnonymousClass18N.A00(r3);
        this.A01 = AnonymousClass0UX.A0L(r3);
        this.A05 = C194918j.A00(r3);
    }
}
