package X;

import android.util.SparseArray;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1RE  reason: invalid class name */
public final class AnonymousClass1RE {
    public Map A00;
    public Map A01;

    public synchronized void A00() {
        Map map = this.A00;
        if (map != null) {
            map.clear();
        }
        Map map2 = this.A01;
        if (map2 != null) {
            map2.clear();
        }
    }

    public void A01(C61302yf r4) {
        if (r4 != null) {
            synchronized (this) {
                if (this.A00 == null) {
                    this.A00 = new HashMap();
                }
                if (this.A01 == null) {
                    this.A01 = new HashMap();
                }
                String str = r4.A03;
                if (str != null) {
                    this.A00.put(str, r4);
                }
                C128015zX r1 = r4.A02;
                if (r1 != null) {
                    SparseArray sparseArray = (SparseArray) this.A01.get(r1);
                    if (sparseArray == null) {
                        sparseArray = new SparseArray();
                    }
                    sparseArray.put(r4.A01, r4);
                    this.A01.put(r4.A02, sparseArray);
                }
            }
        }
    }
}
