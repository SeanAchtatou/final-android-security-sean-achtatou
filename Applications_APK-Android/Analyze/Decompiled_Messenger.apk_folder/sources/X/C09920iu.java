package X;

import android.graphics.Typeface;
import android.os.Build;
import android.util.LongSparseArray;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0iu  reason: invalid class name and case insensitive filesystem */
public final class C09920iu {
    private static final AtomicBoolean A00 = new AtomicBoolean(false);

    public static void A00() {
        if (!A00.getAndSet(true) && Build.VERSION.SDK_INT >= 16) {
            try {
                Field declaredField = Typeface.class.getDeclaredField("sTypefaceCache");
                declaredField.setAccessible(true);
                Object obj = new Object();
                synchronized (obj) {
                    int i = 0;
                    if (Build.VERSION.SDK_INT >= 21) {
                        LongSparseArray longSparseArray = (LongSparseArray) declaredField.get(null);
                        AnonymousClass38E r5 = new AnonymousClass38E(obj, longSparseArray.size());
                        declaredField.set(null, r5);
                        int size = longSparseArray.size();
                        while (i < size) {
                            r5.append(longSparseArray.keyAt(i), new AnonymousClass38F((SparseArray) longSparseArray.valueAt(i)));
                            i++;
                        }
                    } else {
                        SparseArray sparseArray = (SparseArray) declaredField.get(null);
                        C09930iv r4 = new C09930iv(obj, sparseArray.size());
                        declaredField.set(null, r4);
                        int size2 = sparseArray.size();
                        while (i < size2) {
                            r4.append(sparseArray.keyAt(i), new AnonymousClass38F((SparseArray) sparseArray.valueAt(i)));
                            i++;
                        }
                    }
                }
            } catch (Exception unused) {
            }
        }
    }
}
