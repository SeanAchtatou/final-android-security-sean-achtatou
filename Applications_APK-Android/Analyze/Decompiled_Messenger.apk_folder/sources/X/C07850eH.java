package X;

import android.os.Process;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eH  reason: invalid class name and case insensitive filesystem */
public final class C07850eH extends C07880eK {
    private static volatile C07850eH A03;
    public int A00 = -1;
    public AnonymousClass0UN A01;
    public Integer A02 = null;

    public static final C07850eH A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C07850eH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C07850eH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static boolean A01(C07850eH r6) {
        if (r6.A00 == -1) {
            r6.A00 = C70903bT.A00("(SynchronizedDat)");
        }
        try {
            Process.getThreadPriority(r6.A00);
            return false;
        } catch (IllegalArgumentException e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r6.A01)).softReport("SurfaceThreadBoost", "SurfaceDataFetch thread unexpectedly died", e);
            r6.A00 = -1;
            return true;
        }
    }

    private C07850eH(AnonymousClass1XY r5) {
        AnonymousClass0UN r3 = new AnonymousClass0UN(2, r5);
        this.A01 = r3;
        A06((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r3), (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r3));
    }
}
