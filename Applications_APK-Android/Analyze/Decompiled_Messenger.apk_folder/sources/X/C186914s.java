package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.14s  reason: invalid class name and case insensitive filesystem */
public final class C186914s implements C30281hn {
    public Set A00;
    private final Map A01 = new HashMap(4);
    public final /* synthetic */ AnonymousClass0VD A02;

    public synchronized C30281hn Bz4(AnonymousClass1Y7 r2, double d) {
        A00(r2, Double.valueOf(d));
        return this;
    }

    public synchronized C30281hn Bz5(AnonymousClass1Y7 r2, float f) {
        A00(r2, Float.valueOf(f));
        return this;
    }

    public synchronized C30281hn Bz6(AnonymousClass1Y7 r2, int i) {
        A00(r2, Integer.valueOf(i));
        return this;
    }

    public synchronized C30281hn BzA(AnonymousClass1Y7 r2, long j) {
        A00(r2, Long.valueOf(j));
        return this;
    }

    public synchronized C30281hn BzC(AnonymousClass1Y7 r2, String str) {
        if (str == null) {
            C1B(r2);
        } else {
            A00(r2, str);
        }
        return this;
    }

    public synchronized C30281hn C1B(AnonymousClass1Y7 r4) {
        if (this.A00 == null) {
            this.A00 = new HashSet(4);
        }
        this.A00.add(r4);
        this.A01.remove(r4);
        AnonymousClass1Y7 A012 = AnonymousClass0VD.A01(this.A02, r4);
        if (this.A00 == null) {
            this.A00 = new HashSet(4);
        }
        this.A00.add(A012);
        this.A01.remove(A012);
        return this;
    }

    public synchronized C30281hn C24(AnonymousClass1Y7 r3) {
        for (AnonymousClass1Y7 C1B : this.A02.Arq(r3)) {
            C1B(C1B);
        }
        return this;
    }

    public synchronized void commit() {
        Set hashSet;
        HashMap hashMap = new HashMap(this.A01);
        Set set = this.A00;
        if (set == null) {
            hashSet = Collections.emptySet();
        } else {
            hashSet = new HashSet(set);
        }
        AnonymousClass0VD.A03(this.A02, hashMap, hashSet, null);
    }

    public synchronized void commitImmediately() {
        Set hashSet;
        HashMap hashMap = new HashMap(this.A01);
        Set set = this.A00;
        if (set == null) {
            hashSet = Collections.emptySet();
        } else {
            hashSet = new HashSet(set);
        }
        AnonymousClass0VD.A03(this.A02, hashMap, hashSet, 0L);
    }

    public synchronized C30281hn putBoolean(AnonymousClass1Y7 r2, boolean z) {
        A00(r2, Boolean.valueOf(z));
        return this;
    }

    public C186914s(AnonymousClass0VD r3) {
        this.A02 = r3;
    }

    private void A00(AnonymousClass1Y7 r3, Object obj) {
        this.A01.put(r3, obj);
        this.A01.put(AnonymousClass0VD.A01(this.A02, r3), obj);
        Set set = this.A00;
        if (set != null) {
            set.remove(r3);
            this.A00.remove(AnonymousClass0VD.A01(this.A02, r3));
        }
    }
}
