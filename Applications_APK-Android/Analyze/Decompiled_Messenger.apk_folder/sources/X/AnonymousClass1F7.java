package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.Collection;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1F7  reason: invalid class name */
public final class AnonymousClass1F7 extends C31511jo {
    public AnonymousClass0V0 A00 = null;
    public Collection A01 = null;
    public Collection A02 = null;
    private AnonymousClass0UN A03;
    private final AnonymousClass04a A04 = new AnonymousClass04a();

    public static final AnonymousClass1F7 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1F7(r1);
    }

    public C32821mO A04(String str) {
        C32821mO r0;
        C32821mO r2;
        synchronized (this.A04) {
            try {
                r0 = (C32821mO) this.A04.get(str);
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (r0 != null) {
            return r0;
        }
        char c = 65535;
        switch (str.hashCode()) {
            case 1515173:
                if (str.equals("1820")) {
                    c = 'Q';
                    break;
                }
                break;
            case 1541094:
                if (str.equals("2415")) {
                    c = ',';
                    break;
                }
                break;
            case 1571937:
                if (str.equals("3543")) {
                    c = '/';
                    break;
                }
                break;
            case 1571939:
                if (str.equals("3545")) {
                    c = '0';
                    break;
                }
                break;
            case 1575748:
                if (str.equals("3931")) {
                    c = 14;
                    break;
                }
                break;
            case 1598787:
                if (str.equals("4227")) {
                    c = '-';
                    break;
                }
                break;
            case 1599741:
                if (str.equals("4320")) {
                    c = 'R';
                    break;
                }
                break;
            case 1599811:
                if (str.equals("4348")) {
                    c = 'C';
                    break;
                }
                break;
            case 1600648:
                if (str.equals("4408")) {
                    c = '.';
                    break;
                }
                break;
            case 1601726:
                if (str.equals("4541")) {
                    c = 'M';
                    break;
                }
                break;
            case 1603650:
                if (str.equals("4743")) {
                    c = '+';
                    break;
                }
                break;
            case 1603651:
                if (str.equals("4744")) {
                    c = '*';
                    break;
                }
                break;
            case 1603652:
                if (str.equals("4745")) {
                    c = ')';
                    break;
                }
                break;
            case 1603685:
                if (str.equals("4757")) {
                    c = 'U';
                    break;
                }
                break;
            case 1604554:
                if (str.equals("4828")) {
                    c = '&';
                    break;
                }
                break;
            case 1627642:
                if (str.equals("5131")) {
                    c = 'V';
                    break;
                }
                break;
            case 1628788:
                if (str.equals("5290")) {
                    c = '#';
                    break;
                }
                break;
            case 1628789:
                if (str.equals("5291")) {
                    c = '%';
                    break;
                }
                break;
            case 1628790:
                if (str.equals("5292")) {
                    c = '$';
                    break;
                }
                break;
            case 1629758:
                if (str.equals("5399")) {
                    c = 'O';
                    break;
                }
                break;
            case 1630463:
                if (str.equals("5411")) {
                    c = '2';
                    break;
                }
                break;
            case 1631578:
                if (str.equals("5560")) {
                    c = 28;
                    break;
                }
                break;
            case 1631583:
                if (str.equals("5565")) {
                    c = 31;
                    break;
                }
                break;
            case 1631584:
                if (str.equals("5566")) {
                    c = 29;
                    break;
                }
                break;
            case 1631618:
                if (str.equals("5579")) {
                    c = '1';
                    break;
                }
                break;
            case 1632424:
                if (str.equals("5629")) {
                    c = 30;
                    break;
                }
                break;
            case 1632508:
                if (str.equals("5650")) {
                    c = 19;
                    break;
                }
                break;
            case 1632544:
                if (str.equals("5665")) {
                    c = 20;
                    break;
                }
                break;
            case 1632577:
                if (str.equals("5677")) {
                    c = 21;
                    break;
                }
                break;
            case 1633601:
                if (str.equals("5798")) {
                    c = '\"';
                    break;
                }
                break;
            case 1634276:
                if (str.equals("5801")) {
                    c = 'L';
                    break;
                }
                break;
            case 1634499:
                if (str.equals("5877")) {
                    c = 'K';
                    break;
                }
                break;
            case 1635332:
                if (str.equals("5933")) {
                    c = 22;
                    break;
                }
                break;
            case 1656383:
                if (str.equals("6005")) {
                    c = 26;
                    break;
                }
                break;
            case 1656384:
                if (str.equals("6006")) {
                    c = 27;
                    break;
                }
                break;
            case 1656504:
                if (str.equals("6042")) {
                    c = ':';
                    break;
                }
                break;
            case 1657403:
                if (str.equals("6122")) {
                    c = 'J';
                    break;
                }
                break;
            case 1657467:
                if (str.equals("6144")) {
                    c = '>';
                    break;
                }
                break;
            case 1657494:
                if (str.equals("6150")) {
                    c = 'Y';
                    break;
                }
                break;
            case 1657627:
                if (str.equals("6199")) {
                    c = 0;
                    break;
                }
                break;
            case 1659267:
                if (str.equals("6306")) {
                    c = '?';
                    break;
                }
                break;
            case 1659454:
                if (str.equals("6367")) {
                    c = '9';
                    break;
                }
                break;
            case 1659512:
                if (str.equals("6383")) {
                    c = '4';
                    break;
                }
                break;
            case 1659518:
                if (str.equals("6389")) {
                    c = 'T';
                    break;
                }
                break;
            case 1660322:
                if (str.equals("6437")) {
                    c = '<';
                    break;
                }
                break;
            case 1660442:
                if (str.equals("6473")) {
                    c = 'N';
                    break;
                }
                break;
            case 1661187:
                if (str.equals("6504")) {
                    c = 1;
                    break;
                }
                break;
            case 1661217:
                if (str.equals("6513")) {
                    c = 'A';
                    break;
                }
                break;
            case 1661247:
                if (str.equals("6522")) {
                    c = 24;
                    break;
                }
                break;
            case 1661311:
                if (str.equals("6544")) {
                    c = 'S';
                    break;
                }
                break;
            case 1661341:
                if (str.equals("6553")) {
                    c = 23;
                    break;
                }
                break;
            case 1661372:
                if (str.equals("6563")) {
                    c = 25;
                    break;
                }
                break;
            case 1661400:
                if (str.equals("6570")) {
                    c = ';';
                    break;
                }
                break;
            case 1662395:
                if (str.equals("6683")) {
                    c = 10;
                    break;
                }
                break;
            case 1662432:
                if (str.equals("6699")) {
                    c = 'W';
                    break;
                }
                break;
            case 1663200:
                if (str.equals("6732")) {
                    c = 8;
                    break;
                }
                break;
            case 1663207:
                if (str.equals("6739")) {
                    c = 18;
                    break;
                }
                break;
            case 1663266:
                if (str.equals("6756")) {
                    c = 'B';
                    break;
                }
                break;
            case 1663293:
                if (str.equals("6762")) {
                    c = 17;
                    break;
                }
                break;
            case 1664099:
                if (str.equals("6812")) {
                    c = 12;
                    break;
                }
                break;
            case 1664199:
                if (str.equals("6849")) {
                    c = 13;
                    break;
                }
                break;
            case 1665125:
                if (str.equals("6935")) {
                    c = '6';
                    break;
                }
                break;
            case 1665128:
                if (str.equals("6938")) {
                    c = '!';
                    break;
                }
                break;
            case 1686173:
                if (str.equals("7004")) {
                    c = '\'';
                    break;
                }
                break;
            case 1686325:
                if (str.equals("7051")) {
                    c = 9;
                    break;
                }
                break;
            case 1686332:
                if (str.equals("7058")) {
                    c = '8';
                    break;
                }
                break;
            case 1686361:
                if (str.equals("7066")) {
                    c = 15;
                    break;
                }
                break;
            case 1686388:
                if (str.equals("7072")) {
                    c = 11;
                    break;
                }
                break;
            case 1687198:
                if (str.equals("7126")) {
                    c = '7';
                    break;
                }
                break;
            case 1687260:
                if (str.equals("7146")) {
                    c = '@';
                    break;
                }
                break;
            case 1687263:
                if (str.equals("7149")) {
                    c = '3';
                    break;
                }
                break;
            case 1687321:
                if (str.equals("7165")) {
                    c = 'D';
                    break;
                }
                break;
            case 1687409:
                if (str.equals("7190")) {
                    c = 'G';
                    break;
                }
                break;
            case 1687417:
                if (str.equals("7198")) {
                    c = 'E';
                    break;
                }
                break;
            case 1687418:
                if (str.equals("7199")) {
                    c = 'F';
                    break;
                }
                break;
            case 1688160:
                if (str.equals("7227")) {
                    c = 7;
                    break;
                }
                break;
            case 1688221:
                if (str.equals("7246")) {
                    c = '5';
                    break;
                }
                break;
            case 1688317:
                if (str.equals("7279")) {
                    c = '=';
                    break;
                }
                break;
            case 1690111:
                if (str.equals("7435")) {
                    c = 'P';
                    break;
                }
                break;
            case 1690171:
                if (str.equals("7453")) {
                    c = 3;
                    break;
                }
                break;
            case 1690203:
                if (str.equals("7464")) {
                    c = ' ';
                    break;
                }
                break;
            case 1691101:
                if (str.equals("7543")) {
                    c = 'H';
                    break;
                }
                break;
            case 1691258:
                if (str.equals("7595")) {
                    c = 16;
                    break;
                }
                break;
            case 1691971:
                if (str.equals("7615")) {
                    c = '(';
                    break;
                }
                break;
            case 1692006:
                if (str.equals("7629")) {
                    c = 5;
                    break;
                }
                break;
            case 1692033:
                if (str.equals("7635")) {
                    c = 'X';
                    break;
                }
                break;
            case 1692037:
                if (str.equals("7639")) {
                    c = 2;
                    break;
                }
                break;
            case 1692059:
                if (str.equals("7640")) {
                    c = 6;
                    break;
                }
                break;
            case 1692094:
                if (str.equals("7654")) {
                    c = 'I';
                    break;
                }
                break;
            case 1692097:
                if (str.equals("7657")) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                r2 = (C132156Gm) AnonymousClass1XX.A03(AnonymousClass1Y3.A6C, this.A03);
                break;
            case 1:
                r2 = (AnonymousClass42Y) AnonymousClass1XX.A03(AnonymousClass1Y3.BQl, this.A03);
                break;
            case 2:
                r2 = (AnonymousClass635) AnonymousClass1XX.A03(AnonymousClass1Y3.AZu, this.A03);
                break;
            case 3:
                r2 = (AnonymousClass63B) AnonymousClass1XX.A03(AnonymousClass1Y3.ATz, this.A03);
                break;
            case 4:
                r2 = (AnonymousClass636) AnonymousClass1XX.A03(AnonymousClass1Y3.AzK, this.A03);
                break;
            case 5:
                r2 = (AnonymousClass637) AnonymousClass1XX.A03(AnonymousClass1Y3.AsI, this.A03);
                break;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                r2 = (AnonymousClass41H) AnonymousClass1XX.A03(AnonymousClass1Y3.BTT, this.A03);
                break;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                r2 = (AnonymousClass41I) AnonymousClass1XX.A03(AnonymousClass1Y3.AQz, this.A03);
                break;
            case 8:
                r2 = (AnonymousClass41G) AnonymousClass1XX.A03(AnonymousClass1Y3.Ald, this.A03);
                break;
            case Process.SIGKILL:
                r2 = (AnonymousClass41F) AnonymousClass1XX.A03(AnonymousClass1Y3.Ajt, this.A03);
                break;
            case AnonymousClass1Y3.A01 /*10*/:
                r2 = (AnonymousClass42S) AnonymousClass1XX.A03(AnonymousClass1Y3.AWV, this.A03);
                break;
            case AnonymousClass1Y3.A02 /*11*/:
                r2 = (AnonymousClass42T) AnonymousClass1XX.A03(AnonymousClass1Y3.AdT, this.A03);
                break;
            case AnonymousClass1Y3.A03 /*12*/:
                r2 = (AnonymousClass41D) AnonymousClass1XX.A03(AnonymousClass1Y3.Anl, this.A03);
                break;
            case 13:
                r2 = (C134916Ss) AnonymousClass1XX.A03(AnonymousClass1Y3.A12, this.A03);
                break;
            case 14:
                r2 = (C134906Sr) AnonymousClass1XX.A03(AnonymousClass1Y3.BJh, this.A03);
                break;
            case 15:
                r2 = (AnonymousClass42H) AnonymousClass1XX.A03(AnonymousClass1Y3.A5i, this.A03);
                break;
            case 16:
                r2 = (AnonymousClass3OV) AnonymousClass1XX.A03(AnonymousClass1Y3.ABT, this.A03);
                break;
            case 17:
                r2 = (C1060955j) AnonymousClass1XX.A03(AnonymousClass1Y3.BGV, this.A03);
                break;
            case Process.SIGCONT:
                r2 = (C104264ya) AnonymousClass1XX.A03(AnonymousClass1Y3.AKK, this.A03);
                break;
            case Process.SIGSTOP:
                r2 = (C68423St) AnonymousClass1XX.A03(AnonymousClass1Y3.AhY, this.A03);
                break;
            case 20:
                r2 = (C38431xQ) AnonymousClass1XX.A03(AnonymousClass1Y3.BJB, this.A03);
                break;
            case AnonymousClass1Y3.A05 /*21*/:
                r2 = (C68433Su) AnonymousClass1XX.A03(AnonymousClass1Y3.ARD, this.A03);
                break;
            case AnonymousClass1Y3.A06 /*22*/:
                r2 = (C81953vA) AnonymousClass1XX.A03(AnonymousClass1Y3.A0n, this.A03);
                break;
            case 23:
                r2 = (C93304cU) AnonymousClass1XX.A03(AnonymousClass1Y3.AFe, this.A03);
                break;
            case AnonymousClass1Y3.A07 /*24*/:
                r2 = (AnonymousClass4FY) AnonymousClass1XX.A03(AnonymousClass1Y3.Aku, this.A03);
                break;
            case 25:
                r2 = (AnonymousClass856) AnonymousClass1XX.A03(AnonymousClass1Y3.Ag4, this.A03);
                break;
            case AnonymousClass1Y3.A08 /*26*/:
                r2 = (AnonymousClass4A9) AnonymousClass1XX.A03(AnonymousClass1Y3.BA0, this.A03);
                break;
            case AnonymousClass1Y3.A09 /*27*/:
                r2 = (AnonymousClass4AA) AnonymousClass1XX.A03(AnonymousClass1Y3.ApH, this.A03);
                break;
            case 28:
                r2 = (C862649v) AnonymousClass1XX.A03(AnonymousClass1Y3.AGN, this.A03);
                break;
            case 29:
                r2 = (AnonymousClass4A8) AnonymousClass1XX.A03(AnonymousClass1Y3.Atw, this.A03);
                break;
            case AnonymousClass1Y3.A0A /*30*/:
                r2 = (AnonymousClass4A7) AnonymousClass1XX.A03(AnonymousClass1Y3.Alh, this.A03);
                break;
            case AnonymousClass1Y3.A0B /*31*/:
                r2 = (AnonymousClass4D4) AnonymousClass1XX.A03(AnonymousClass1Y3.BO8, this.A03);
                break;
            case ' ':
                r2 = (C81913v6) AnonymousClass1XX.A03(AnonymousClass1Y3.AQY, this.A03);
                break;
            case '!':
                r2 = (C70463ag) AnonymousClass1XX.A03(AnonymousClass1Y3.BSO, this.A03);
                break;
            case AnonymousClass1Y3.A0C /*34*/:
                r2 = (C197669Rg) AnonymousClass1XX.A03(AnonymousClass1Y3.B5X, this.A03);
                break;
            case '#':
                r2 = (C96514it) AnonymousClass1XX.A03(AnonymousClass1Y3.BLk, this.A03);
                break;
            case '$':
                r2 = (C96524iu) AnonymousClass1XX.A03(AnonymousClass1Y3.A3a, this.A03);
                break;
            case AnonymousClass1Y3.A0D /*37*/:
                r2 = (C96504is) AnonymousClass1XX.A03(AnonymousClass1Y3.BU7, this.A03);
                break;
            case AnonymousClass1Y3.A0E /*38*/:
                r2 = (C29366EWv) AnonymousClass1XX.A03(AnonymousClass1Y3.Akz, this.A03);
                break;
            case AnonymousClass1Y3.A0F /*39*/:
                r2 = (C29365EWu) AnonymousClass1XX.A03(AnonymousClass1Y3.BP3, this.A03);
                break;
            case AnonymousClass1Y3.A0G /*40*/:
                r2 = (EWp) AnonymousClass1XX.A03(AnonymousClass1Y3.BMK, this.A03);
                break;
            case AnonymousClass1Y3.A0H /*41*/:
                r2 = (C29361EWo) AnonymousClass1XX.A03(AnonymousClass1Y3.AOg, this.A03);
                break;
            case '*':
                r2 = (C29360EWn) AnonymousClass1XX.A03(AnonymousClass1Y3.BI9, this.A03);
                break;
            case '+':
                r2 = (C29359EWm) AnonymousClass1XX.A03(AnonymousClass1Y3.Asp, this.A03);
                break;
            case AnonymousClass1Y3.A0I /*44*/:
                r2 = (C29358EWl) AnonymousClass1XX.A03(AnonymousClass1Y3.ANi, this.A03);
                break;
            case AnonymousClass1Y3.A0J /*45*/:
                r2 = (C29368EWx) AnonymousClass1XX.A03(AnonymousClass1Y3.BMf, this.A03);
                break;
            case AnonymousClass1Y3.A0K /*46*/:
                r2 = (C29364EWt) AnonymousClass1XX.A03(AnonymousClass1Y3.BF8, this.A03);
                break;
            case AnonymousClass1Y3.A0L /*47*/:
                r2 = (C29362EWq) AnonymousClass1XX.A03(AnonymousClass1Y3.BG3, this.A03);
                break;
            case '0':
                r2 = (C70673b5) AnonymousClass1XX.A03(AnonymousClass1Y3.Aql, this.A03);
                break;
            case '1':
                r2 = (C29363EWs) AnonymousClass1XX.A03(AnonymousClass1Y3.AS1, this.A03);
                break;
            case '2':
                r2 = (EWr) AnonymousClass1XX.A03(AnonymousClass1Y3.Avp, this.A03);
                break;
            case AnonymousClass1Y3.A0M /*51*/:
                r2 = (AnonymousClass43Z) AnonymousClass1XX.A03(AnonymousClass1Y3.Akq, this.A03);
                break;
            case AnonymousClass1Y3.A0N /*52*/:
                r2 = (AnonymousClass7u6) AnonymousClass1XX.A03(AnonymousClass1Y3.ArL, this.A03);
                break;
            case AnonymousClass1Y3.A0O /*53*/:
                r2 = (AnonymousClass7u5) AnonymousClass1XX.A03(AnonymousClass1Y3.AsY, this.A03);
                break;
            case '6':
                r2 = (AnonymousClass7u4) AnonymousClass1XX.A03(AnonymousClass1Y3.AA2, this.A03);
                break;
            case '7':
                r2 = (C170777tz) AnonymousClass1XX.A03(AnonymousClass1Y3.BEr, this.A03);
                break;
            case AnonymousClass1Y3.A0P /*56*/:
                r2 = (C170847uC) AnonymousClass1XX.A03(AnonymousClass1Y3.An1, this.A03);
                break;
            case '9':
                r2 = (AnonymousClass7u7) AnonymousClass1XX.A03(AnonymousClass1Y3.Ap9, this.A03);
                break;
            case AnonymousClass1Y3.A0Q /*58*/:
                r2 = (C170787u0) AnonymousClass1XX.A03(AnonymousClass1Y3.Ati, this.A03);
                break;
            case ';':
                r2 = (C170827uA) AnonymousClass1XX.A03(AnonymousClass1Y3.B7X, this.A03);
                break;
            case AnonymousClass1Y3.A0R /*60*/:
                r2 = (AnonymousClass7u3) AnonymousClass1XX.A03(AnonymousClass1Y3.Asc, this.A03);
                break;
            case '=':
                r2 = (C170807u2) AnonymousClass1XX.A03(AnonymousClass1Y3.BLZ, this.A03);
                break;
            case '>':
                r2 = (C170797u1) AnonymousClass1XX.A03(AnonymousClass1Y3.AiO, this.A03);
                break;
            case AnonymousClass1Y3.A0S /*63*/:
                r2 = (AnonymousClass7u9) AnonymousClass1XX.A03(AnonymousClass1Y3.B3z, this.A03);
                break;
            case '@':
                r2 = (C170857uD) AnonymousClass1XX.A03(AnonymousClass1Y3.AkT, this.A03);
                break;
            case AnonymousClass1Y3.A0T /*65*/:
                r2 = (C170837uB) AnonymousClass1XX.A03(AnonymousClass1Y3.AUS, this.A03);
                break;
            case 'B':
                r2 = (C170817u8) AnonymousClass1XX.A03(AnonymousClass1Y3.Ai1, this.A03);
                break;
            case 'C':
                r2 = (C91594Yy) AnonymousClass1XX.A03(AnonymousClass1Y3.BLf, this.A03);
                break;
            case AnonymousClass1Y3.A0U /*68*/:
                r2 = (AnonymousClass1BB) AnonymousClass1XX.A03(AnonymousClass1Y3.A2Y, this.A03);
                break;
            case 'E':
                r2 = (AnonymousClass3J6) AnonymousClass1XX.A03(AnonymousClass1Y3.AA6, this.A03);
                break;
            case 'F':
                r2 = (C124585th) AnonymousClass1XX.A03(AnonymousClass1Y3.Aka, this.A03);
                break;
            case AnonymousClass1Y3.A0V /*71*/:
                r2 = (C29367EWw) AnonymousClass1XX.A03(AnonymousClass1Y3.A7Z, this.A03);
                break;
            case 'H':
                r2 = (C853543c) AnonymousClass1XX.A03(AnonymousClass1Y3.AwV, this.A03);
                break;
            case AnonymousClass1Y3.A0W /*73*/:
                r2 = (AnonymousClass42U) AnonymousClass1XX.A03(AnonymousClass1Y3.Aq7, this.A03);
                break;
            case 'J':
                r2 = (C81963vB) AnonymousClass1XX.A03(AnonymousClass1Y3.AVX, this.A03);
                break;
            case AnonymousClass1Y3.A0X /*75*/:
                r2 = (C91544Yt) AnonymousClass1XX.A03(AnonymousClass1Y3.BOH, this.A03);
                break;
            case AnonymousClass1Y3.A0Y /*76*/:
                r2 = (C91564Yv) AnonymousClass1XX.A03(AnonymousClass1Y3.BTj, this.A03);
                break;
            case AnonymousClass1Y3.A0Z /*77*/:
                r2 = (AnonymousClass42V) AnonymousClass1XX.A03(AnonymousClass1Y3.AUD, this.A03);
                break;
            case 'N':
                r2 = (C851442a) AnonymousClass1XX.A03(AnonymousClass1Y3.Ajo, this.A03);
                break;
            case 'O':
                r2 = (AnonymousClass41E) AnonymousClass1XX.A03(AnonymousClass1Y3.A4q, this.A03);
                break;
            case AnonymousClass1Y3.A0a /*80*/:
                r2 = (C2J) AnonymousClass1XX.A03(AnonymousClass1Y3.ApX, this.A03);
                break;
            case AnonymousClass1Y3.A0b /*81*/:
                r2 = (C29357EWk) AnonymousClass1XX.A03(AnonymousClass1Y3.A9e, this.A03);
                break;
            case 'R':
                r2 = (AnonymousClass4Z3) AnonymousClass1XX.A03(AnonymousClass1Y3.Adr, this.A03);
                break;
            case 'S':
                r2 = (C39971zu) AnonymousClass1XX.A03(AnonymousClass1Y3.AlN, this.A03);
                break;
            case AnonymousClass1Y3.A0c /*84*/:
                r2 = (C96464io) AnonymousClass1XX.A03(AnonymousClass1Y3.AoF, this.A03);
                break;
            case 'U':
                r2 = (C96384ig) AnonymousClass1XX.A03(AnonymousClass1Y3.AHt, this.A03);
                break;
            case 'V':
                r2 = (C142606kb) AnonymousClass1XX.A03(AnonymousClass1Y3.AIt, this.A03);
                break;
            case 'W':
                r2 = (C86824Cd) AnonymousClass1XX.A03(AnonymousClass1Y3.B27, this.A03);
                break;
            case 'X':
                r2 = (C142596ka) AnonymousClass1XX.A03(AnonymousClass1Y3.ACJ, this.A03);
                break;
            case AnonymousClass1Y3.A0d /*89*/:
                r2 = (AnonymousClass41C) AnonymousClass1XX.A03(AnonymousClass1Y3.A1f, this.A03);
                break;
            default:
                return null;
        }
        synchronized (this.A04) {
            try {
                this.A04.put(str, r2);
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
        return r2;
    }

    public AnonymousClass1F7(AnonymousClass1XY r3) {
        this.A03 = new AnonymousClass0UN(0, r3);
    }
}
