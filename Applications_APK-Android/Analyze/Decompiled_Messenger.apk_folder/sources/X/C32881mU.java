package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.1mU  reason: invalid class name and case insensitive filesystem */
public final class C32881mU implements AnonymousClass0ZM {
    public final /* synthetic */ AnonymousClass1CV A00;

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r5) {
        if (fbSharedPreferences.Aep(r5, true)) {
            AnonymousClass1CV.A07(this.A00, C149836x6.UNREAD_PILL_FEATURE_ENABLED, null, null, null);
            AnonymousClass1CV.A02(this.A00);
            return;
        }
        AnonymousClass1CV.A07(this.A00, C149836x6.UNREAD_PILL_FEATURE_DISABLED, null, null, null);
        AnonymousClass1CV.A04(this.A00);
    }

    public C32881mU(AnonymousClass1CV r1) {
        this.A00 = r1;
    }
}
