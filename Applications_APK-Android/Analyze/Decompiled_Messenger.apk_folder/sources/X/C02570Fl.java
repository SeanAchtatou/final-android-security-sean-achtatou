package X;

/* renamed from: X.0Fl  reason: invalid class name and case insensitive filesystem */
public final class C02570Fl extends AnonymousClass0FM {
    public long realtimeMs;
    public long uptimeMs;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C02570Fl r7 = (C02570Fl) obj;
            if (!(this.uptimeMs == r7.uptimeMs && this.realtimeMs == r7.realtimeMs)) {
                return false;
            }
        }
        return true;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r3) {
        C02570Fl r32 = (C02570Fl) r3;
        this.uptimeMs = r32.uptimeMs;
        this.realtimeMs = r32.realtimeMs;
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02570Fl r52 = (C02570Fl) r5;
        C02570Fl r62 = (C02570Fl) r6;
        if (r62 == null) {
            r62 = new C02570Fl();
        }
        if (r52 == null) {
            r62.uptimeMs = this.uptimeMs;
            r62.realtimeMs = this.realtimeMs;
            return r62;
        }
        r62.uptimeMs = this.uptimeMs - r52.uptimeMs;
        r62.realtimeMs = this.realtimeMs - r52.realtimeMs;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02570Fl r52 = (C02570Fl) r5;
        C02570Fl r62 = (C02570Fl) r6;
        if (r62 == null) {
            r62 = new C02570Fl();
        }
        if (r52 == null) {
            r62.uptimeMs = this.uptimeMs;
            r62.realtimeMs = this.realtimeMs;
            return r62;
        }
        r62.uptimeMs = this.uptimeMs + r52.uptimeMs;
        r62.realtimeMs = this.realtimeMs + r52.realtimeMs;
        return r62;
    }

    public int hashCode() {
        long j = this.uptimeMs;
        long j2 = this.realtimeMs;
        return (((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "TimeMetrics{uptimeMs=" + this.uptimeMs + ", realtimeMs=" + this.realtimeMs + '}';
    }
}
