package X;

import com.facebook.stash.core.Stash;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.1j7  reason: invalid class name and case insensitive filesystem */
public final class C31081j7 extends C31091j8 implements Stash {
    public final Set A00 = Collections.synchronizedSet(new HashSet());
    public volatile boolean A01 = false;

    public Set getAllKeys() {
        LinkedHashSet linkedHashSet;
        if (!this.A01) {
            this.A00.addAll(super.getAllKeys());
            this.A01 = true;
        }
        synchronized (this.A00) {
            linkedHashSet = new LinkedHashSet(this.A00);
        }
        return linkedHashSet;
    }

    public File insert(String str) {
        this.A00.add(str);
        return super.insert(str);
    }

    public boolean remove(String str) {
        this.A00.remove(str);
        return super.remove(str);
    }

    public boolean removeAll() {
        this.A00.clear();
        return super.removeAll();
    }

    public C31081j7(Stash stash) {
        super(stash);
    }
}
