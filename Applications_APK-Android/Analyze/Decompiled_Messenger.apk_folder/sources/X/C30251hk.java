package X;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.SecureRandomSpi;

/* renamed from: X.1hk  reason: invalid class name and case insensitive filesystem */
public final class C30251hk extends SecureRandomSpi {
    public static DataInputStream A00;
    public static OutputStream A01;
    public static final File A02 = new File("/dev/urandom");
    public static final Object A03 = new Object();
    private boolean mSeedAttempted;

    public void engineSetSeed(byte[] bArr) {
        OutputStream outputStream;
        try {
            synchronized (A03) {
                synchronized (A03) {
                    if (A01 == null) {
                        try {
                            A01 = new FileOutputStream(A02);
                        } catch (IOException e) {
                            throw new SecurityException("Failed to open " + A02 + " for writing", e);
                        }
                    }
                    outputStream = A01;
                }
            }
            outputStream.write(bArr);
            outputStream.flush();
        } catch (Throwable unused) {
        }
        this.mSeedAttempted = true;
    }

    public byte[] engineGenerateSeed(int i) {
        byte[] bArr = new byte[i];
        engineNextBytes(bArr);
        return bArr;
    }

    public void engineNextBytes(byte[] bArr) {
        DataInputStream dataInputStream;
        if (!this.mSeedAttempted) {
            engineSetSeed(C30241hj.A01());
        }
        try {
            synchronized (A03) {
                try {
                    synchronized (A03) {
                        if (A00 == null) {
                            try {
                                A00 = new DataInputStream(new FileInputStream(A02));
                            } catch (IOException e) {
                                throw new SecurityException("Failed to open " + A02 + " for reading", e);
                            }
                        }
                        dataInputStream = A00;
                    }
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                    throw th;
                }
            }
            synchronized (dataInputStream) {
                try {
                    dataInputStream.readFully(bArr);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        } catch (IOException e2) {
            throw new SecurityException("Failed to read from " + A02, e2);
        }
    }
}
