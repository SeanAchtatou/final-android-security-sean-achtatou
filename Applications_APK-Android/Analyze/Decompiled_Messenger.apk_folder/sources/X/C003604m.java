package X;

import com.facebook.profilo.core.TriggerRegistry;
import com.facebook.profilo.ipc.TraceContext;
import java.util.Random;

/* renamed from: X.04m  reason: invalid class name and case insensitive filesystem */
public final class C003604m implements AnonymousClass04j, C003504l {
    public static final int A01 = TriggerRegistry.A00.A02("cold_start");
    private final ThreadLocal A00 = new AnonymousClass058();

    public boolean AUJ(long j, Object obj, long j2, Object obj2) {
        return ((int) j) == ((int) j2);
    }

    public boolean BE8() {
        return true;
    }

    public boolean BFS(long j, Object obj, int i) {
        if (((int) j) == i) {
            return true;
        }
        return false;
    }

    public int AYs(long j, Object obj, AnonymousClass057 r6) {
        AnonymousClass056 r62 = (AnonymousClass056) r6;
        if (((Random) this.A00.get()).nextInt(r62.A04) == 0) {
            return r62.A00;
        }
        return 0;
    }

    public TraceContext.TraceConfigExtras B6o(long j, Object obj, AnonymousClass057 r8) {
        AnonymousClass056 r82 = (AnonymousClass056) r8;
        return new TraceContext.TraceConfigExtras(r82.A03, r82.A01, r82.A02);
    }
}
