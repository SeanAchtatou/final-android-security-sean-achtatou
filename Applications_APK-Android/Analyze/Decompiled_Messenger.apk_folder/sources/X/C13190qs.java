package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import androidx.fragment.app.Fragment;
import com.google.common.base.Optional;

/* renamed from: X.0qs  reason: invalid class name and case insensitive filesystem */
public class C13190qs extends C13170qq implements C13200qt {
    public void AZv(Activity activity) {
    }

    public void BN7(Activity activity) {
    }

    public void BNJ(Activity activity, int i, int i2, Intent intent) {
    }

    public void BON(Activity activity, Resources.Theme theme, int i, boolean z) {
    }

    public void BOW(Activity activity, Fragment fragment) {
    }

    public boolean BPC(Activity activity) {
        return false;
    }

    public void BPY(Activity activity, Bundle bundle) {
    }

    public void BPo(Activity activity, Bundle bundle) {
    }

    public void BTK(Activity activity, Configuration configuration) {
    }

    public void BTy(Activity activity) {
    }

    public Dialog BUl(Activity activity, int i) {
        return null;
    }

    public void BUs(Menu menu) {
    }

    public void BgX(Activity activity, Intent intent) {
    }

    public boolean BhL(MenuItem menuItem) {
        return false;
    }

    public void Bif(Activity activity, boolean z, Configuration configuration) {
    }

    public void BjC(Activity activity, Bundle bundle) {
    }

    public boolean BjS(Activity activity, int i, Dialog dialog) {
        return false;
    }

    public void BjX(Menu menu) {
    }

    public void BmT(Activity activity) {
    }

    public void Bmx(Bundle bundle) {
    }

    public boolean Bo7(Activity activity, Throwable th) {
        return false;
    }

    public void BsC(CharSequence charSequence, int i) {
    }

    public void Bt3(Activity activity, int i) {
    }

    public void Btv(Activity activity) {
    }

    public void Bty(Activity activity) {
    }

    public void Bv9(Activity activity, boolean z) {
    }

    public Optional BcM(Activity activity, int i, KeyEvent keyEvent) {
        return Optional.absent();
    }

    public Optional BcN(Activity activity, int i, KeyEvent keyEvent) {
        return Optional.absent();
    }

    public Optional BnW(Activity activity) {
        return Optional.absent();
    }
}
