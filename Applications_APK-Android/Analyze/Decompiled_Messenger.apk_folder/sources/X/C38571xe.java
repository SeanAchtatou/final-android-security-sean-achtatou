package X;

import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1xe  reason: invalid class name and case insensitive filesystem */
public final class C38571xe {
    public DataFetchDisposition A00;
    public MessagesCollection A01;
    public ThreadSummary A02;
    public C52192if A03;
    public User A04;
    public ImmutableList A05;
    public boolean A06;

    public void A00(C61152yQ r2) {
        this.A02 = r2.A02;
        this.A04 = r2.A04;
        this.A01 = r2.A01;
        this.A05 = r2.A05;
        this.A06 = r2.A06;
        this.A00 = r2.A00;
        this.A03 = r2.A03;
    }
}
