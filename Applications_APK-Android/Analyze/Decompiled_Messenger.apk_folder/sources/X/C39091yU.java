package X;

import com.facebook.proxygen.LigerSamplePolicy;
import com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo;

/* renamed from: X.1yU  reason: invalid class name and case insensitive filesystem */
public final class C39091yU {
    public final int A00;
    public final FbWebrtcConferenceParticipantInfo A01;
    public final String A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C39091yU) {
                C39091yU r5 = (C39091yU) obj;
                if (!C28931fb.A07(this.A01, r5.A01) || this.A00 != r5.A00 || !C28931fb.A07(this.A02, r5.A02)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C28931fb.A02(C28931fb.A03((C28931fb.A03(1, this.A01) * 31) + this.A00, this.A02), LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT);
    }

    public C39091yU(C124005si r3) {
        this.A01 = r3.A01;
        this.A00 = r3.A00;
        String str = r3.A02;
        C28931fb.A06(str, "intentActionName");
        this.A02 = str;
    }
}
