package X;

import android.os.PowerManager;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tA  reason: invalid class name and case insensitive filesystem */
public final class C14350tA {
    private static volatile C14350tA A05;
    private int A00 = 0;
    private long A01 = 0;
    public final AnonymousClass069 A02;
    private final PowerManager A03;
    private final Map A04 = new HashMap();

    public synchronized C14390tE A01(int i, String str) {
        C14390tE r2;
        r2 = new C14390tE(this, C009007v.A00(this.A03, i, str), str);
        this.A04.put(r2.A08, r2);
        return r2;
    }

    public synchronized void A02(C14390tE r7) {
        long j;
        if (this.A04.containsKey(r7.A08)) {
            this.A04.remove(r7.A08);
            synchronized (r7) {
                if (r7.A04()) {
                    if (!r7.A06) {
                        r7.A01();
                    } else {
                        while (r7.A01 > 0) {
                            r7.A01();
                        }
                    }
                }
                r7.A04 = true;
            }
            long j2 = this.A01;
            synchronized (r7) {
                if (r7.A04()) {
                    j = (r7.A03 + r7.A09.A02.now()) - r7.A02;
                } else {
                    j = r7.A03;
                }
            }
            this.A01 = j2 + j;
            this.A00++;
        }
    }

    public static final C14350tA A00(AnonymousClass1XY r6) {
        if (A05 == null) {
            synchronized (C14350tA.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        PowerManager A0Y = C04490Ux.A0Y(applicationInjector);
                        AnonymousClass069 A032 = AnonymousClass067.A03(applicationInjector);
                        AnonymousClass0UX.A0e(applicationInjector);
                        A05 = new C14350tA(A0Y, A032);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private C14350tA(PowerManager powerManager, AnonymousClass069 r4) {
        this.A03 = powerManager;
        this.A02 = r4;
    }
}
