package X;

import com.facebook.orca.threadview.ThreadViewActivity;
import com.facebook.orca.threadview.ThreadViewFragment;
import com.facebook.orca.threadview.ThreadViewMessagesFragment;
import java.util.Set;

/* renamed from: X.0ZL  reason: invalid class name */
public final class AnonymousClass0ZL implements AnonymousClass0ZJ {
    public static final AnonymousClass0ZL A00() {
        return new AnonymousClass0ZL();
    }

    public Set AhG() {
        return C25011Xz.A05(ThreadViewMessagesFragment.class, ThreadViewFragment.class, ThreadViewActivity.class);
    }
}
