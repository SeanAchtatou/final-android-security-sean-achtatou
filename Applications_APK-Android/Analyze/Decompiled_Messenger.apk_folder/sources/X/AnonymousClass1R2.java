package X;

/* renamed from: X.1R2  reason: invalid class name */
public final class AnonymousClass1R2 extends C23711Qo {
    public static final String __redex_internal_original_name = "com.facebook.imagepipeline.producers.WebpTranscodeProducer$1";
    public final /* synthetic */ AnonymousClass1NY A00;
    public final /* synthetic */ C55992p5 A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass1R2(C55992p5 r1, C23581Qb r2, AnonymousClass1MN r3, AnonymousClass1QK r4, String str, AnonymousClass1NY r6) {
        super(r2, r3, r4, str);
        this.A01 = r1;
        this.A00 = r6;
    }

    public void A02() {
        AnonymousClass1NY.A04(this.A00);
        super.A02();
    }

    public void A03(Exception exc) {
        AnonymousClass1NY.A04(this.A00);
        super.A03(exc);
    }

    public void A05(Object obj) {
        AnonymousClass1NY.A04(this.A00);
        super.A05((AnonymousClass1NY) obj);
    }
}
