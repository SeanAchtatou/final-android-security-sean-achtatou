package X;

import android.net.Uri;

/* renamed from: X.13Q  reason: invalid class name */
public interface AnonymousClass13Q {
    String Ad9();

    Uri.Builder AdB();

    String AdC();

    Uri.Builder AoB();

    Uri.Builder AoC();

    Uri.Builder AoJ();

    Uri.Builder Auq();

    Uri.Builder B2G();

    Uri.Builder B2H();

    String getDomain();
}
