package X;

import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.google.common.base.MoreObjects;
import java.util.Arrays;

/* renamed from: X.0ln  reason: invalid class name and case insensitive filesystem */
public final class C11060ln {
    public final Bundle A00;
    public final CallerContext A01;
    public final C11030ld A02;
    public final C16890xw A03;
    public final String A04;
    public final String A05;

    public boolean equals(Object obj) {
        if (!(obj instanceof C11060ln)) {
            return false;
        }
        C11060ln r4 = (C11060ln) obj;
        if (!r4.A05.equals(this.A05) || !r4.A00.equals(this.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A05, this.A00});
    }

    public String toString() {
        MoreObjects.ToStringHelper stringHelper = MoreObjects.toStringHelper(this);
        stringHelper.add("id", this.A04);
        stringHelper.add("type", this.A05);
        stringHelper.add("bundle", this.A00);
        stringHelper.add("caller_context", this.A01);
        return stringHelper.toString();
    }

    public C11060ln(String str, Bundle bundle) {
        this(str, bundle, null, null, null, null);
    }

    public C11060ln(String str, Bundle bundle, String str2, C16890xw r4, CallerContext callerContext, C11030ld r6) {
        this.A05 = str;
        this.A00 = bundle;
        this.A03 = r4;
        this.A01 = callerContext;
        this.A02 = r6;
        this.A04 = str2;
    }
}
