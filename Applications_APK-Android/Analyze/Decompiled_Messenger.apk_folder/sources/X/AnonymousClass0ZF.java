package X;

import android.content.Context;
import android.os.HandlerThread;
import java.util.Map;
import java.util.Stack;

/* renamed from: X.0ZF  reason: invalid class name */
public class AnonymousClass0ZF {
    public long A00 = -1;
    public Boolean A01;
    public String A02;
    public String A03;
    public long A04;
    public long A05;
    public long A06 = 0;
    public C06230bA A07;
    public C12240os A08;
    public C12240os A09;
    public Integer A0A;
    public String A0B;
    public String A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public volatile boolean A0H;

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r2 != 0) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A00() {
        /*
            r5 = this;
            java.lang.String r0 = r5.A03
            if (r0 != 0) goto L_0x0033
            X.0bA r0 = r5.A07
            if (r0 == 0) goto L_0x0033
            X.0bG r0 = r0.A06
            if (r0 == 0) goto L_0x0033
            X.00M r0 = r0.A00
            java.lang.String r3 = r0.A04()
            if (r3 == 0) goto L_0x002a
            r2 = -1
            int r1 = r3.hashCode()
            r0 = 3075986(0x2eef92, float:4.310374E-39)
            if (r1 != r0) goto L_0x0027
            java.lang.String r0 = "dash"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0027
            r2 = 0
        L_0x0027:
            r0 = 1
            if (r2 == 0) goto L_0x002b
        L_0x002a:
            r0 = 0
        L_0x002b:
            if (r0 == 0) goto L_0x0033
            java.lang.String r0 = X.AnonymousClass0XR.A00()
            r5.A03 = r0
        L_0x0033:
            java.lang.Boolean r0 = r5.A01
            if (r0 != 0) goto L_0x0051
            X.0bA r0 = r5.A07
            if (r0 == 0) goto L_0x0051
            X.0dF r0 = r0.A09
            if (r0 == 0) goto L_0x0051
            X.0Tq r0 = r0.A00
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r5.A01 = r0
        L_0x0051:
            long r3 = r5.A00
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x005f
            long r0 = java.lang.System.currentTimeMillis()
            r5.A00 = r0
        L_0x005f:
            X.0bA r0 = r5.A07
            if (r0 == 0) goto L_0x0075
            X.0ch r0 = r0.A08
            if (r0 == 0) goto L_0x0075
            X.0US r0 = r0.A00
            java.lang.Object r0 = r0.get()
            X.0h3 r0 = (X.C09340h3) r0
            java.lang.String r0 = r0.A0I()
            r5.A02 = r0
        L_0x0075:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0ZF.A00():void");
    }

    private void A01() {
        String str;
        String str2;
        if (this.A03 != null) {
            A0B().A0K("process", this.A03);
        }
        if (this.A02 != null) {
            A0B().A0K("radio_type", this.A02);
        }
        C12240os r2 = this.A08;
        Integer num = this.A0A;
        if (num != null) {
            if (1 - num.intValue() != 0) {
                str2 = "client_event";
            } else {
                str2 = "experiment";
            }
            r2.A0K("log_type", str2);
        }
        Boolean bool = this.A01;
        if (bool != null) {
            C12240os r22 = this.A08;
            if (bool.booleanValue()) {
                str = "true";
            } else {
                str = "false";
            }
            r22.A0K("bg", str);
        }
        this.A08.A0J("time", Double.valueOf(((double) this.A00) / 1000.0d));
        String str3 = this.A0C;
        if (str3 != null) {
            this.A08.A0K("module", str3);
        }
        this.A08.A0K("name", this.A0B);
        this.A08.A0J("tags", Long.valueOf(this.A06));
    }

    private void A02() {
        C06380bP r3;
        if (this.A0H) {
            throw new IllegalStateException("Expected immutability");
        } else if (this.A08 == null && this.A09 == null) {
            C06230bA r0 = this.A07;
            if (r0 == null) {
                r3 = null;
            } else {
                r3 = r0.A05;
            }
            this.A0C = null;
            this.A0B = null;
            this.A0A = null;
            this.A0G = false;
            this.A03 = null;
            this.A01 = null;
            this.A00 = -1;
            this.A07 = null;
            this.A02 = null;
            this.A0F = false;
            this.A0D = false;
            this.A04 = 0;
            this.A06 = 0;
            this.A0E = false;
            if (r3 != null) {
                r3.C0w(this);
            }
        } else {
            throw new IllegalStateException("Must call ejectBaseParameters before release");
        }
    }

    private void A03() {
        if (!this.A0F) {
            throw new IllegalStateException("isSampled was not invoked, how can you have known?");
        } else if (!this.A0H) {
            throw new IllegalStateException("Expected mutability");
        }
    }

    private boolean A04() {
        long j;
        C06230bA r0 = this.A07;
        if (r0 != null) {
            C07310dD r02 = r0.A04;
            if (r02 == null) {
                j = Long.MAX_VALUE;
            } else {
                j = r02.A00;
            }
        } else {
            j = 0;
        }
        if (this.A04 < j) {
            return true;
        }
        return false;
    }

    public AnonymousClass0ZF A05() {
        if (this instanceof C06370bO) {
            return (C06370bO) this;
        }
        this.A0D = true;
        return this;
    }

    public AnonymousClass0ZF A06(long j) {
        if (this instanceof C06370bO) {
            return (C06370bO) this;
        }
        A03();
        this.A00 = j;
        return this;
    }

    public AnonymousClass0ZF A07(String str) {
        if (this instanceof C06370bO) {
            return (C06370bO) this;
        }
        A03();
        if (str != null) {
            this.A03 = str;
            return this;
        }
        throw new IllegalArgumentException("processName cannot be null if specified explicitly");
    }

    public AnonymousClass0ZF A08(String str, Boolean bool) {
        if (this instanceof C06370bO) {
            return (C06370bO) this;
        }
        A03();
        C12240os.A01(A0B(), str, bool);
        synchronized (this) {
            if (bool != null) {
                this.A04 += 4;
            }
        }
        return this;
    }

    public AnonymousClass0ZF A09(String str, Number number) {
        if (this instanceof C06370bO) {
            return (C06370bO) this;
        }
        A03();
        A0B().A0J(str, number);
        synchronized (this) {
            if (number != null) {
                this.A04 += 4;
            }
        }
        return this;
    }

    public AnonymousClass0ZF A0A(String str, String str2) {
        if (this instanceof C06370bO) {
            return (C06370bO) this;
        }
        A03();
        A0B().A0K(str, str2);
        synchronized (this) {
            if (str2 != null) {
                this.A04 += (long) str2.length();
            }
        }
        return this;
    }

    public C12240os A0B() {
        Integer num;
        String str;
        if (!(this instanceof C06370bO)) {
            A03();
            C06230bA r1 = this.A07;
            if (this.A09 == null) {
                C12240os A022 = r1.A0D.A02();
                this.A09 = A022;
                C12240os r12 = this.A08;
                if (!(r12 == null || (num = this.A0A) == null)) {
                    if (1 - num.intValue() != 0) {
                        str = "extra";
                    } else {
                        str = "result";
                    }
                    r12.A0I(str, A022);
                }
            }
            return this.A09;
        }
        C06370bO r13 = (C06370bO) this;
        if (r13.A00 == null) {
            r13.A00 = C06370bO.A01.A0D.A02();
        }
        return r13.A00;
    }

    public String A0C() {
        if (this instanceof C06370bO) {
            return "SampledOutEventName";
        }
        if (this.A0F) {
            return this.A0B;
        }
        throw new IllegalStateException("isSampled was not invoked, how can you have known?");
    }

    public String A0D() {
        if (!(this instanceof C06370bO)) {
            return this.A0C;
        }
        return "SampledOutEventModule";
    }

    public void A0E() {
        AnonymousClass15B A032;
        boolean z;
        AnonymousClass969 r1;
        AnonymousClass966 r6;
        long j;
        boolean z2;
        AnonymousClass968 r12;
        if (!(this instanceof C06370bO)) {
            C06230bA r0 = this.A07;
            AnonymousClass064.A01(r0, "builder was not acquired or was acquired without config");
            if (r0.A01.BFp()) {
                A03();
                A00();
                C06230bA r02 = this.A07;
                AnonymousClass064.A01(r02, "builder was not acquired or was acquired without config");
                C06400bR r4 = r02.A0C;
                synchronized (r4) {
                    if (r4.A02 == null) {
                        Class cls = r4.A0K;
                        Class cls2 = r4.A0G;
                        Class cls3 = r4.A0I;
                        Class cls4 = cls3;
                        AnonymousClass15A r13 = new AnonymousClass15A(cls, cls2, cls4, r4.A0H, r4.A0J, AnonymousClass07B.A00, "micro_batch");
                        HandlerThread A002 = C06400bR.A00(r4, "Analytics-MicroBatch-Proc", 10);
                        C07290dB r122 = r4.A09;
                        C07290dB r11 = r4.A08;
                        Context context = r4.A04;
                        C06320bJ r10 = r4.A0A;
                        C06410bS r8 = new C06410bS(r4.A07, r4.A05);
                        C06390bQ r7 = r4.A0F;
                        Class cls5 = r4.A0H;
                        C29591Edn edn = new C29591Edn(r4.A0E, r4.A0D, r4.A0C);
                        C06840cA r03 = r4.A0B;
                        AnonymousClass966 r24 = new AnonymousClass966(A002, r122, r11, new AnonymousClass967(context, 2131298546, "micro_batch", r10, r8, r7, r13, cls5, edn, r03), r4, r4.A06, r10, r03);
                        r4.A02 = r24;
                        C29671gn A012 = C06400bR.A01(r4);
                        AnonymousClass965 r14 = r24.A08;
                        AnonymousClass965.A05(r14);
                        r14.sendMessage(r14.obtainMessage(2, A012));
                    }
                    r6 = r4.A02;
                }
                AnonymousClass064.A01(this.A07, "builder was not acquired or was acquired without config");
                A01();
                this.A08.A05 = false;
                C12240os r15 = this.A09;
                if (r15 != null) {
                    r15.A05 = false;
                }
                this.A0H = false;
                if (this.A0G) {
                    j = -2;
                } else {
                    j = -1;
                }
                if (this.A0E) {
                    j = this.A05;
                }
                C12240os r2 = this.A08;
                C12240os r112 = null;
                if (r2 != null) {
                    r2.A05 = false;
                    C12240os r16 = this.A09;
                    if (r16 != null) {
                        r16.A05 = false;
                    }
                    this.A09 = null;
                    this.A08 = null;
                    r112 = r2;
                }
                if (r112 != null && A04()) {
                    if (this.A0D) {
                        AnonymousClass965 r22 = r6.A08;
                        AnonymousClass964 r62 = new AnonymousClass964(r112, j);
                        if (r22.A06.A05.AlE()) {
                            synchronized (r22.A03) {
                                try {
                                    if (r22.A02 == null) {
                                        r22.A02 = new Stack();
                                    }
                                    r22.A02.push(r62);
                                } catch (Throwable th) {
                                    while (true) {
                                        th = th;
                                        break;
                                    }
                                }
                            }
                            r22.sendMessageAtFrontOfQueue(r22.obtainMessage(8));
                        } else {
                            r22.sendMessageAtFrontOfQueue(r22.obtainMessage(1, r62));
                        }
                    } else {
                        AnonymousClass965 r63 = r6.A08;
                        if (r63.A06.A05.AlE()) {
                            synchronized (r63.A04) {
                                try {
                                    AnonymousClass968 r04 = r63.A01;
                                    if (r04 == null || AnonymousClass968.A00(r04)) {
                                        int AO3 = r63.A06.A06.AO3();
                                        synchronized (AnonymousClass968.A07) {
                                            r12 = AnonymousClass968.A06;
                                            if (r12 != null) {
                                                AnonymousClass968.A06 = r12.A02;
                                                r12.A02 = null;
                                            } else {
                                                r12 = new AnonymousClass968(AO3);
                                            }
                                        }
                                        r63.A01 = r12;
                                        z2 = true;
                                    } else {
                                        z2 = false;
                                    }
                                    AnonymousClass968 r23 = r63.A01;
                                    if (!AnonymousClass968.A00(r23)) {
                                        C12240os[] r05 = r23.A05;
                                        int i = r23.A01;
                                        r05[i] = r112;
                                        r23.A04[i] = j;
                                        r23.A01 = i + 1;
                                        if (z2) {
                                            r63.sendMessage(r63.obtainMessage(1, 2, 0, r23));
                                        }
                                    } else {
                                        throw new IllegalStateException("Batch cannot accept more events");
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    throw th;
                                }
                            }
                        } else {
                            r63.sendMessage(r63.obtainMessage(1, 1, 0, new AnonymousClass964(r112, j)));
                        }
                    }
                }
            } else {
                A03();
                A00();
                C06230bA r17 = this.A07;
                if (this.A0G) {
                    A032 = r17.A0C.A02();
                } else {
                    A032 = r17.A0C.A03();
                }
                AnonymousClass064.A01(this.A07, "builder was not acquired or was acquired without config");
                A01();
                this.A08.A05 = false;
                C12240os r18 = this.A09;
                if (r18 != null) {
                    r18.A05 = false;
                }
                this.A0H = false;
                C12240os r25 = this.A08;
                C12240os r9 = null;
                if (r25 != null) {
                    r25.A05 = false;
                    C12240os r19 = this.A09;
                    if (r19 != null) {
                        r19.A05 = false;
                    }
                    this.A09 = null;
                    this.A08 = null;
                    r9 = r25;
                }
                if (r9 != null && A04()) {
                    if (this.A0D) {
                        AnonymousClass15K r26 = A032.A04;
                        if (r26.A07.A06.AlE()) {
                            synchronized (r26.A03) {
                                try {
                                    if (r26.A02 == null) {
                                        r26.A02 = new Stack();
                                    }
                                    r26.A02.push(r9);
                                } catch (Throwable th3) {
                                    while (true) {
                                        th = th3;
                                        break;
                                    }
                                }
                            }
                            r26.sendMessageAtFrontOfQueue(r26.obtainMessage(8));
                        } else {
                            r26.sendMessageAtFrontOfQueue(r26.obtainMessage(1, r9));
                        }
                    } else {
                        AnonymousClass15K r64 = A032.A04;
                        if (r64.A07.A06.AlE()) {
                            synchronized (r64.A04) {
                                try {
                                    AnonymousClass969 r06 = r64.A01;
                                    if (r06 == null || AnonymousClass969.A00(r06)) {
                                        int Aco = r64.A07.A06.Aco();
                                        synchronized (AnonymousClass969.A06) {
                                            r1 = AnonymousClass969.A05;
                                            if (r1 != null) {
                                                AnonymousClass969.A05 = r1.A02;
                                                r1.A02 = null;
                                            } else {
                                                r1 = new AnonymousClass969(Aco);
                                            }
                                        }
                                        r64.A01 = r1;
                                        z = true;
                                    } else {
                                        z = false;
                                    }
                                    AnonymousClass969 r27 = r64.A01;
                                    if (!AnonymousClass969.A00(r27)) {
                                        C12240os[] r110 = r27.A04;
                                        int i2 = r27.A01;
                                        r110[i2] = r9;
                                        r27.A01 = i2 + 1;
                                        if (z) {
                                            r64.sendMessage(r64.obtainMessage(1, 2, 0, r27));
                                        }
                                    } else {
                                        throw new IllegalStateException("Batch cannot accept more events");
                                    }
                                } catch (Throwable th4) {
                                    th = th4;
                                    throw th;
                                }
                            }
                        } else {
                            r64.sendMessage(r64.obtainMessage(1, 1, 0, r9));
                        }
                    }
                }
            }
            A02();
            return;
        }
        C06370bO r65 = (C06370bO) this;
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement stackTraceElement : stackTrace) {
            sb.append("SampledOutEvent is logged: ");
            sb.append(stackTraceElement.toString());
            sb.append("\n");
        }
        C010708t.A0K("SampledOutEventBuilder", sb.toString());
        r65.A00 = null;
    }

    public void A0F(Map map) {
        if (!(this instanceof C06370bO) && map != null) {
            for (Map.Entry entry : map.entrySet()) {
                A0A((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    public boolean A0G() {
        if (this instanceof C06370bO) {
            return false;
        }
        this.A0F = true;
        return true;
    }
}
