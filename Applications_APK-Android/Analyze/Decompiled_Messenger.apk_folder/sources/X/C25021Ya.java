package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1Ya  reason: invalid class name and case insensitive filesystem */
public final class C25021Ya extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C25031Yb A01;
    private static volatile C25031Yb A02;

    public static final AnonymousClass95Q A00(AnonymousClass1XY r5) {
        AnonymousClass0VG A002 = AnonymousClass0VG.A00(AnonymousClass1Y3.AOJ, r5);
        AnonymousClass0VG A003 = AnonymousClass0VG.A00(AnonymousClass1Y3.AWK, r5);
        int i = AnonymousClass1Y3.BHG;
        return new AnonymousClass95Q(A002, A003, AnonymousClass0VG.A00(i, r5), AnonymousClass0VG.A00(i, r5));
    }

    public static final C25031Yb A01(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (C25031Yb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new C25031Yb();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C25031Yb A02(AnonymousClass1XY r3) {
        if (A02 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A02 = new C25031Yb();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final AnonymousClass95U A03(AnonymousClass1XY r4) {
        return new AnonymousClass95U(A01(r4), AnonymousClass0WT.A00(r4), AnonymousClass0WT.A01(r4));
    }
}
