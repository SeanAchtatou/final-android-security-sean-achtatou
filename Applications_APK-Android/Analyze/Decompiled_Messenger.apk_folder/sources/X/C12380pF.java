package X;

import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.service.model.FetchMessageAfterTimestampParams;
import com.facebook.messaging.service.model.FetchMessageResult;
import com.facebook.messaging.service.model.FetchMoreRecentMessagesParams;
import com.facebook.messaging.service.model.FetchMoreRecentMessagesResult;

/* renamed from: X.0pF  reason: invalid class name and case insensitive filesystem */
public final class C12380pF extends AnonymousClass0mF implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.protocol.WebServiceHandler";
    public AnonymousClass0UN A00;
    public final C04310Tq A01;
    private final C04310Tq A02;

    public static C47692Xl A00(C12380pF r4) {
        C47692Xl r3 = new C47692Xl();
        r3.A00(((C16900xx) AnonymousClass1XX.A02(31, AnonymousClass1Y3.AoI, r4.A00)).A01());
        return r3;
    }

    public static final C12380pF A01(AnonymousClass1XY r4) {
        return new C12380pF(r4, AnonymousClass0VB.A00(AnonymousClass1Y3.AKh, r4), AnonymousClass0VG.A00(AnonymousClass1Y3.Apk, r4), AnonymousClass0VG.A00(AnonymousClass1Y3.BBF, r4));
    }

    /* JADX WARNING: Removed duplicated region for block: B:86:0x0345 A[Catch:{ Exception -> 0x03bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x03b5 A[Catch:{ Exception -> 0x03bd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0w(X.C11060ln r33, X.C27311cz r34) {
        /*
            r32 = this;
            r0 = r33
            android.os.Bundle r1 = r0.A00
            java.lang.String r0 = "CreateCustomizableGroupParams"
            android.os.Parcelable r1 = r1.getParcelable(r0)
            com.facebook.messaging.groups.create.model.CreateCustomizableGroupParams r1 = (com.facebook.messaging.groups.create.model.CreateCustomizableGroupParams) r1
            int r2 = X.AnonymousClass1Y3.A4f
            r13 = r32
            X.0UN r0 = r13.A00
            r3 = 30
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.B5m r0 = (X.B5m) r0
            X.B5f r2 = r0.A03
            X.B5j r0 = r0.A02
            java.lang.Object r6 = r2.A01(r1, r0)
            com.facebook.messaging.service.model.FetchThreadResult r6 = (com.facebook.messaging.service.model.FetchThreadResult) r6
            if (r6 != 0) goto L_0x04a4
            int r2 = X.AnonymousClass1Y3.A4f
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.B5m r0 = (X.B5m) r0
            long r26 = X.C50402dv.A00()
            int r3 = X.AnonymousClass1Y3.B2E
            X.0UN r2 = r0.A00
            r5 = 8
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r3, r2)
            X.9oy r4 = (X.C206829oy) r4
            X.0h3 r2 = r0.A01
            boolean r3 = r2.A0Q()
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            r4.A03(r1, r3, r2)
            int r3 = X.AnonymousClass1Y3.AO3
            X.0UN r2 = r0.A00
            r8 = 7
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r8, r3, r2)
            com.facebook.messaging.groups.create.logging.CreateGroupAggregatedLatencyLogger r4 = (com.facebook.messaging.groups.create.logging.CreateGroupAggregatedLatencyLogger) r4
            long r2 = r1.A00
            X.B4D r6 = r4.A03
            X.B4S r4 = X.B4S.PRE_REQUEST
            java.lang.String r2 = java.lang.String.valueOf(r2)
            X.B4W r2 = X.B4W.A00(r2)
            r6.A03(r4, r2)
            int r3 = X.AnonymousClass1Y3.Afc
            X.0UN r2 = r0.A00
            r4 = 6
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r4, r3, r2)
            com.facebook.messaging.groups.create.logging.CreateGroupAggregatedReliabilityLogger r7 = (com.facebook.messaging.groups.create.logging.CreateGroupAggregatedReliabilityLogger) r7
            long r2 = r1.A00
            java.lang.Integer r6 = X.AnonymousClass07B.A01
            r7.A02(r2, r6)
            int r3 = X.AnonymousClass1Y3.AgK
            X.0UN r2 = r0.A00
            r7 = 10
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r3, r2)
            X.06B r2 = (X.AnonymousClass06B) r2
            long r14 = r2.now()
            X.0h3 r2 = r0.A01
            boolean r23 = r2.A0Q()
            java.lang.String r3 = r1.A0C     // Catch:{ Exception -> 0x03bd }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ Exception -> 0x03bd }
            if (r2 != 0) goto L_0x00ed
            boolean r2 = r1.A0J     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x00ed
            int r9 = X.AnonymousClass1Y3.BMW     // Catch:{ Exception -> 0x03bd }
            X.0UN r6 = r0.A00     // Catch:{ Exception -> 0x03bd }
            r2 = 0
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r9, r6)     // Catch:{ Exception -> 0x03bd }
            X.BCO r6 = (X.BCO) r6     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000 r9 = new com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000     // Catch:{ Exception -> 0x03bd }
            r2 = 66
            r9.<init>(r2)     // Catch:{ Exception -> 0x03bd }
            r2 = 251(0xfb, float:3.52E-43)
            java.lang.String r2 = X.AnonymousClass24B.$const$string(r2)     // Catch:{ Exception -> 0x03bd }
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = r1.A0B     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "entry_point"
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            X.43L r3 = new X.43L     // Catch:{ Exception -> 0x03bd }
            r3.<init>()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "data"
            r3.A04(r2, r9)     // Catch:{ Exception -> 0x03bd }
            X.AkD r3 = X.C26931cN.A01(r3)     // Catch:{ Exception -> 0x03bd }
            X.0mD r2 = r6.A01     // Catch:{ Exception -> 0x03bd }
            com.google.common.util.concurrent.ListenableFuture r2 = r2.A05(r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r2 = r2.get()     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphql.executor.GraphQLResult r2 = (com.facebook.graphql.executor.GraphQLResult) r2     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r9 = r2.A03     // Catch:{ Exception -> 0x03bd }
            if (r9 == 0) goto L_0x0341
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r9     // Catch:{ Exception -> 0x03bd }
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r6 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r3 = 1484816850(0x588081d2, float:1.1303605E15)
            r2 = 1517090099(0x5a6cf533, float:1.66744235E16)
            com.facebook.graphservice.tree.TreeJNI r10 = r9.A0J(r3, r6, r2)     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r10     // Catch:{ Exception -> 0x03bd }
            goto L_0x0342
        L_0x00ed:
            r6 = 0
            int r3 = X.AnonymousClass1Y3.BMW     // Catch:{ Exception -> 0x03bd }
            X.0UN r2 = r0.A00     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ Exception -> 0x03bd }
            X.BCO r10 = (X.BCO) r10     // Catch:{ Exception -> 0x03bd }
            r19 = r1
            r17 = r26
            com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000 r9 = new com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000     // Catch:{ Exception -> 0x03bd }
            r2 = 150(0x96, float:2.1E-43)
            r9.<init>(r2)     // Catch:{ Exception -> 0x03bd }
            X.0Tq r2 = r10.A05     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r2 = r2.get()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x03bd }
            r9.A0B(r2)     // Catch:{ Exception -> 0x03bd }
            com.google.common.collect.ImmutableList r16 = r1.A05()     // Catch:{ Exception -> 0x03bd }
            java.util.HashSet r12 = new java.util.HashSet     // Catch:{ Exception -> 0x03bd }
            r12.<init>()     // Catch:{ Exception -> 0x03bd }
            com.google.common.collect.ImmutableList$Builder r11 = com.google.common.collect.ImmutableList.builder()     // Catch:{ Exception -> 0x03bd }
            X.0Tq r2 = r10.A05     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r6 = r2.get()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x03bd }
            r12.add(r6)     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r3 = new com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000     // Catch:{ Exception -> 0x03bd }
            r2 = 104(0x68, float:1.46E-43)
            r3.<init>(r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "fbid"
            r3.A09(r2, r6)     // Catch:{ Exception -> 0x03bd }
            r11.add(r3)     // Catch:{ Exception -> 0x03bd }
            java.util.Iterator r16 = r16.iterator()     // Catch:{ Exception -> 0x03bd }
        L_0x0139:
            boolean r2 = r16.hasNext()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x0160
            java.lang.Object r2 = r16.next()     // Catch:{ Exception -> 0x03bd }
            com.facebook.user.model.User r2 = (com.facebook.user.model.User) r2     // Catch:{ Exception -> 0x03bd }
            java.lang.String r6 = r2.A0j     // Catch:{ Exception -> 0x03bd }
            boolean r2 = r12.contains(r6)     // Catch:{ Exception -> 0x03bd }
            if (r2 != 0) goto L_0x0139
            r12.add(r6)     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r3 = new com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000     // Catch:{ Exception -> 0x03bd }
            r2 = 104(0x68, float:1.46E-43)
            r3.<init>(r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "fbid"
            r3.A09(r2, r6)     // Catch:{ Exception -> 0x03bd }
            r11.add(r3)     // Catch:{ Exception -> 0x03bd }
            goto L_0x0139
        L_0x0160:
            com.google.common.collect.ImmutableList r3 = r11.build()     // Catch:{ Exception -> 0x03bd }
            r2 = 77
            java.lang.String r2 = X.C99084oO.$const$string(r2)     // Catch:{ Exception -> 0x03bd }
            r9.A0A(r2, r3)     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r6 = new com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000     // Catch:{ Exception -> 0x03bd }
            r2 = 126(0x7e, float:1.77E-43)
            r6.<init>(r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = r1.A0E     // Catch:{ Exception -> 0x03bd }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ Exception -> 0x03bd }
            if (r2 != 0) goto L_0x0181
            java.lang.String r2 = "name"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x0181:
            java.lang.String r3 = r1.A0D     // Catch:{ Exception -> 0x03bd }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ Exception -> 0x03bd }
            if (r2 != 0) goto L_0x018e
            java.lang.String r2 = "description"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x018e:
            com.facebook.common.util.TriState r3 = r1.A03()     // Catch:{ Exception -> 0x03bd }
            boolean r2 = r3.isSet()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x01a9
            boolean r2 = r3.asBoolean()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x0214
            r2 = 95
            java.lang.String r3 = X.C99084oO.$const$string(r2)     // Catch:{ Exception -> 0x03bd }
        L_0x01a4:
            java.lang.String r2 = "approval_mode"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x01a9:
            com.facebook.common.util.TriState r3 = r1.A01()     // Catch:{ Exception -> 0x03bd }
            boolean r2 = r3.isSet()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x01c0
            boolean r2 = r3.asBoolean()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x0211
            java.lang.String r3 = "JOINABLE"
        L_0x01bb:
            java.lang.String r2 = "joinable_mode"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x01c0:
            com.facebook.common.util.TriState r3 = r1.A00()     // Catch:{ Exception -> 0x03bd }
            boolean r2 = r3.isSet()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x01db
            boolean r2 = r3.asBoolean()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x020e
            r2 = 127(0x7f, float:1.78E-43)
            java.lang.String r3 = X.C22298Ase.$const$string(r2)     // Catch:{ Exception -> 0x03bd }
        L_0x01d6:
            java.lang.String r2 = "discoverable_mode"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x01db:
            com.facebook.common.util.TriState r3 = r1.A02()     // Catch:{ Exception -> 0x03bd }
            boolean r2 = r3.isSet()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x01f6
            boolean r2 = r3.asBoolean()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x020b
            r2 = 172(0xac, float:2.41E-43)
            java.lang.String r3 = X.AnonymousClass24B.$const$string(r2)     // Catch:{ Exception -> 0x03bd }
        L_0x01f1:
            java.lang.String r2 = "video_room_mode"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x01f6:
            com.facebook.ui.media.attachments.model.MediaResource r11 = r1.A08     // Catch:{ Exception -> 0x03bd }
            if (r11 == 0) goto L_0x0230
            java.util.concurrent.ExecutorService r2 = r10.A04     // Catch:{ Exception -> 0x03bd }
            X.0VL r3 = X.AnonymousClass0WJ.A00(r2)     // Catch:{ Exception -> 0x03bd }
            X.BDI r2 = new X.BDI     // Catch:{ Exception -> 0x03bd }
            r2.<init>(r10, r11)     // Catch:{ Exception -> 0x03bd }
            com.google.common.util.concurrent.ListenableFuture r2 = r3.CIF(r2)     // Catch:{ Exception -> 0x03bd }
            r12 = 0
            goto L_0x0217
        L_0x020b:
            java.lang.String r3 = "NON_VIDEO_ROOM"
            goto L_0x01f1
        L_0x020e:
            java.lang.String r3 = "HIDDEN"
            goto L_0x01d6
        L_0x0211:
            java.lang.String r3 = "PRIVATE"
            goto L_0x01bb
        L_0x0214:
            java.lang.String r3 = "OPEN"
            goto L_0x01a4
        L_0x0217:
            java.lang.Object r2 = r2.get()     // Catch:{ InterruptedException | ExecutionException -> 0x021e }
            X.94Q r2 = (X.AnonymousClass94Q) r2     // Catch:{ InterruptedException | ExecutionException -> 0x021e }
            goto L_0x0227
        L_0x021e:
            r11 = move-exception
            java.lang.String r3 = "CreateGroupGraphQLMutator"
            java.lang.String r2 = "Unable to upload messenger thread photo."
            X.C010708t.A0T(r3, r11, r2)     // Catch:{ Exception -> 0x03bd }
            r2 = r12
        L_0x0227:
            if (r2 == 0) goto L_0x0230
            java.lang.String r3 = r2.A04     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "thread_image_file_handle"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x0230:
            java.lang.String r2 = "thread_settings"
            r9.A05(r2, r6)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = r1.A0C     // Catch:{ Exception -> 0x03bd }
            r2 = 68
            java.lang.String r2 = X.AnonymousClass24B.$const$string(r2)     // Catch:{ Exception -> 0x03bd }
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = r1.A0F     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "thread_suggestion_source"
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = r1.A0H     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "thread_suggestion_source_name"
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = r1.A0G     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "thread_suggestion_source_ent_id"
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            com.google.common.collect.ImmutableList r2 = r19.A04()     // Catch:{ Exception -> 0x03bd }
            com.google.common.collect.ImmutableList$Builder r11 = com.google.common.collect.ImmutableList.builder()     // Catch:{ Exception -> 0x03bd }
            java.util.Iterator r12 = r2.iterator()     // Catch:{ Exception -> 0x03bd }
        L_0x0261:
            boolean r2 = r12.hasNext()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x0281
            java.lang.Object r3 = r12.next()     // Catch:{ Exception -> 0x03bd }
            com.facebook.user.model.User r3 = (com.facebook.user.model.User) r3     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r6 = new com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000     // Catch:{ Exception -> 0x03bd }
            r2 = 17
            r6.<init>(r2)     // Catch:{ Exception -> 0x03bd }
            com.facebook.user.model.UserKey r2 = r3.A0Q     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = r2.id     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "fbid"
            r6.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            r11.add(r6)     // Catch:{ Exception -> 0x03bd }
            goto L_0x0261
        L_0x0281:
            com.google.common.collect.ImmutableList r3 = r11.build()     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "room_co_creators"
            r9.A0A(r2, r3)     // Catch:{ Exception -> 0x03bd }
            boolean r2 = r1.A0K     // Catch:{ Exception -> 0x03bd }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "use_existing_group"
            r9.A06(r2, r3)     // Catch:{ Exception -> 0x03bd }
            r2 = 1
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "return_blocked_participants"
            r9.A06(r2, r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r3 = java.lang.String.valueOf(r17)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "attempt_id"
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
            long r2 = r1.A00     // Catch:{ Exception -> 0x03bd }
            r11 = 0
            int r6 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
            if (r6 == 0) goto L_0x02b9
            java.lang.String r3 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "offline_threading_id"
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x02b9:
            java.lang.String r3 = r1.A0B     // Catch:{ Exception -> 0x03bd }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ Exception -> 0x03bd }
            if (r2 != 0) goto L_0x02c6
            java.lang.String r2 = "entry_point"
            r9.A09(r2, r3)     // Catch:{ Exception -> 0x03bd }
        L_0x02c6:
            X.4E5 r12 = new X.4E5     // Catch:{ Exception -> 0x03bd }
            r12.<init>()     // Catch:{ Exception -> 0x03bd }
            X.7PV r11 = r10.A02     // Catch:{ Exception -> 0x03bd }
            com.google.common.collect.ImmutableList r6 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ Exception -> 0x03bd }
            java.lang.Boolean r3 = java.lang.Boolean.TRUE     // Catch:{ Exception -> 0x03bd }
            r2 = 20
            r11.A0N(r12, r6, r2, r3)     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "input"
            r12.A04(r2, r9)     // Catch:{ Exception -> 0x03bd }
            X.0mD r3 = r10.A01     // Catch:{ Exception -> 0x03bd }
            X.AkD r2 = X.C26931cN.A01(r12)     // Catch:{ Exception -> 0x03bd }
            com.google.common.util.concurrent.ListenableFuture r2 = r3.A05(r2)     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r2 = r2.get()     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphql.executor.GraphQLResult r2 = (com.facebook.graphql.executor.GraphQLResult) r2     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r6 = r2.A03     // Catch:{ Exception -> 0x03bd }
            if (r6 == 0) goto L_0x0341
            X.4E4 r6 = (X.AnonymousClass4E4) r6     // Catch:{ Exception -> 0x03bd }
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r9 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r3 = -544080109(0xffffffffdf91ff13, float:-2.1040296E19)
            r2 = -753409481(0xffffffffd317e237, float:-6.5233532E11)
            com.google.common.collect.ImmutableList r3 = r6.A0M(r3, r9, r2)     // Catch:{ Exception -> 0x03bd }
            boolean r2 = X.C013509w.A01(r3)     // Catch:{ Exception -> 0x03bd }
            if (r2 != 0) goto L_0x0308
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = r6.A0Q()     // Catch:{ Exception -> 0x03bd }
            goto L_0x0342
        L_0x0308:
            X.71w r6 = new X.71w     // Catch:{ Exception -> 0x03bd }
            com.google.common.collect.ImmutableList$Builder r7 = com.google.common.collect.ImmutableList.builder()     // Catch:{ Exception -> 0x03bd }
            X.1Xv r10 = r3.iterator()     // Catch:{ Exception -> 0x03bd }
        L_0x0312:
            boolean r2 = r10.hasNext()     // Catch:{ Exception -> 0x03bd }
            if (r2 == 0) goto L_0x0339
            java.lang.Object r9 = r10.next()     // Catch:{ Exception -> 0x03bd }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r9     // Catch:{ Exception -> 0x03bd }
            r8 = 3
            int r3 = X.AnonymousClass1Y3.AxE     // Catch:{ Exception -> 0x03bd }
            X.0UN r2 = r0.A00     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r8, r3, r2)     // Catch:{ Exception -> 0x03bd }
            X.0t0 r3 = (X.C14300t0) r3     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = r9.A3m()     // Catch:{ Exception -> 0x03bd }
            com.facebook.user.model.UserKey r2 = com.facebook.user.model.UserKey.A01(r2)     // Catch:{ Exception -> 0x03bd }
            com.facebook.user.model.User r2 = r3.A03(r2)     // Catch:{ Exception -> 0x03bd }
            r7.add(r2)     // Catch:{ Exception -> 0x03bd }
            goto L_0x0312
        L_0x0339:
            com.google.common.collect.ImmutableList r2 = r7.build()     // Catch:{ Exception -> 0x03bd }
            r6.<init>(r2)     // Catch:{ Exception -> 0x03bd }
            throw r6     // Catch:{ Exception -> 0x03bd }
        L_0x0341:
            r10 = 0
        L_0x0342:
            r9 = 0
            if (r10 == 0) goto L_0x03b5
            X.0Tq r2 = r0.A04     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r6 = r2.get()     // Catch:{ Exception -> 0x03bd }
            com.facebook.user.model.User r6 = (com.facebook.user.model.User) r6     // Catch:{ Exception -> 0x03bd }
            int r11 = X.AnonymousClass1Y3.AnS     // Catch:{ Exception -> 0x03bd }
            X.0UN r3 = r0.A00     // Catch:{ Exception -> 0x03bd }
            r2 = 2
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r11, r3)     // Catch:{ Exception -> 0x03bd }
            X.7PU r3 = (X.AnonymousClass7PU) r3     // Catch:{ Exception -> 0x03bd }
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r3.A09(r10, r6)     // Catch:{ Exception -> 0x03bd }
            com.facebook.messaging.service.model.FetchThreadResult r6 = r3.A0B(r2, r10, r6, r9)     // Catch:{ Exception -> 0x03bd }
            int r3 = X.AnonymousClass1Y3.Afc     // Catch:{ Exception -> 0x03bd }
            X.0UN r2 = r0.A00     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ Exception -> 0x03bd }
            com.facebook.messaging.groups.create.logging.CreateGroupAggregatedReliabilityLogger r10 = (com.facebook.messaging.groups.create.logging.CreateGroupAggregatedReliabilityLogger) r10     // Catch:{ Exception -> 0x03bd }
            long r2 = r1.A00     // Catch:{ Exception -> 0x03bd }
            java.lang.Integer r9 = X.AnonymousClass07B.A01     // Catch:{ Exception -> 0x03bd }
            r10.A03(r2, r9)     // Catch:{ Exception -> 0x03bd }
            int r3 = X.AnonymousClass1Y3.AO3     // Catch:{ Exception -> 0x03bd }
            X.0UN r2 = r0.A00     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r8, r3, r2)     // Catch:{ Exception -> 0x03bd }
            com.facebook.messaging.groups.create.logging.CreateGroupAggregatedLatencyLogger r8 = (com.facebook.messaging.groups.create.logging.CreateGroupAggregatedLatencyLogger) r8     // Catch:{ Exception -> 0x03bd }
            long r2 = r1.A00     // Catch:{ Exception -> 0x03bd }
            X.B4D r9 = r8.A03     // Catch:{ Exception -> 0x03bd }
            X.B4S r8 = X.B4S.REQUEST_RESPONSE     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x03bd }
            X.B4W r2 = X.B4W.A00(r2)     // Catch:{ Exception -> 0x03bd }
            r9.A03(r8, r2)     // Catch:{ Exception -> 0x03bd }
            int r2 = X.AnonymousClass1Y3.B2E     // Catch:{ Exception -> 0x03bd }
            X.0UN r8 = r0.A00     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r2, r8)     // Catch:{ Exception -> 0x03bd }
            X.9oy r2 = (X.C206829oy) r2     // Catch:{ Exception -> 0x03bd }
            int r3 = X.AnonymousClass1Y3.AgK     // Catch:{ Exception -> 0x03bd }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r7, r3, r8)     // Catch:{ Exception -> 0x03bd }
            X.06B r3 = (X.AnonymousClass06B) r3     // Catch:{ Exception -> 0x03bd }
            long r18 = r3.now()     // Catch:{ Exception -> 0x03bd }
            long r18 = r18 - r14
            r21 = 0
            java.lang.Integer r24 = X.AnonymousClass07B.A01     // Catch:{ Exception -> 0x03bd }
            r20 = 0
            r22 = r21
            r17 = r1
            r16 = r2
            r16.A01(r17, r18, r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x03bd }
            goto L_0x04a4
        L_0x03b5:
            X.2FM r3 = new X.2FM     // Catch:{ Exception -> 0x03bd }
            java.lang.String r2 = "ThreadInfo was null"
            r3.<init>(r2, r9, r1)     // Catch:{ Exception -> 0x03bd }
            throw r3     // Catch:{ Exception -> 0x03bd }
        L_0x03bd:
            r6 = move-exception
            int r3 = X.AnonymousClass1Y3.Afc
            X.0UN r2 = r0.A00
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r4, r3, r2)
            com.facebook.messaging.groups.create.logging.CreateGroupAggregatedReliabilityLogger r8 = (com.facebook.messaging.groups.create.logging.CreateGroupAggregatedReliabilityLogger) r8
            long r2 = r1.A00
            r7 = -1
            int r4 = X.B5m.A00(r6, r7)
            r8.A01(r2, r4)
            r4 = 9
            int r3 = X.AnonymousClass1Y3.AhC
            X.0UN r2 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.7Yp r4 = (X.C159087Yp) r4
            long r2 = r1.A00
            java.lang.String r25 = java.lang.String.valueOf(r2)
            int r28 = X.B5m.A00(r6, r7)
            r30 = 0
            java.lang.String r29 = ""
            r24 = r4
            r31 = r6
            r24.A02(r25, r26, r28, r29, r30, r31)
            int r2 = X.AnonymousClass1Y3.B2E
            X.0UN r4 = r0.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r2, r4)
            X.9oy r0 = (X.C206829oy) r0
            int r3 = X.AnonymousClass1Y3.AgK
            r2 = 10
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r3, r4)
            X.06B r2 = (X.AnonymousClass06B) r2
            long r3 = r2.now()
            long r3 = r3 - r14
            java.lang.String r7 = "offline_threading_id"
            long r1 = r1.A00
            java.lang.String r8 = java.lang.Long.toString(r1)
            java.lang.String r9 = "send_time_delta"
            java.lang.String r10 = java.lang.Long.toString(r3)
            java.lang.String r11 = "error_class"
            java.lang.Class r1 = r6.getClass()
            java.lang.String r12 = r1.toString()
            java.lang.String r13 = "error_message"
            java.lang.String r14 = r6.getMessage()
            java.lang.String r15 = "current_time"
            X.06B r1 = r0.A02
            long r1 = r1.now()
            java.lang.String r16 = java.lang.Long.toString(r1)
            r1 = 1246(0x4de, float:1.746E-42)
            java.lang.String r17 = X.AnonymousClass24B.$const$string(r1)
            java.lang.String r18 = java.lang.Boolean.toString(r23)
            r1 = 548(0x224, float:7.68E-43)
            java.lang.String r19 = X.AnonymousClass80H.$const$string(r1)
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            java.lang.String r20 = X.C206839oz.A00(r1)
            java.lang.String r21 = "netcheck_state"
            com.facebook.common.netchecker.NetChecker r1 = r0.A01
            X.2rX r1 = r1.A0B
            java.lang.String r22 = r1.toString()
            java.lang.String r23 = "last_netcheck_time"
            com.facebook.common.netchecker.NetChecker r1 = r0.A01
            long r1 = r1.A0A
            java.lang.String r24 = java.lang.Long.toString(r1)
            java.lang.String[] r1 = new java.lang.String[]{r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24}
            java.util.Map r9 = X.C57172rY.A01(r1)
            X.00x r1 = r0.A03
            X.00y r4 = r1.A01
            X.00y r3 = X.C001400y.PUBLIC
            r1 = 13
            java.lang.String r2 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r1)
            if (r4 == r3) goto L_0x0490
            java.lang.String r1 = X.AnonymousClass0D4.A01(r6)
            r9.put(r2, r1)
        L_0x047d:
            X.2qj r0 = r0.A00
            r10 = 0
            r11 = 0
            r12 = 0
            java.lang.String r7 = "create_group_request"
            java.lang.String r8 = "failed"
            X.0nb r1 = X.C56722qj.A01(r7, r8, r9, r10, r11, r12)
            com.facebook.analytics.DeprecatedAnalyticsLogger r0 = r0.A01
            r0.A07(r1)
            throw r6
        L_0x0490:
            java.lang.Throwable r1 = r6.getCause()
            boolean r1 = r1 instanceof java.lang.NullPointerException
            if (r1 == 0) goto L_0x047d
            java.lang.Throwable r1 = r6.getCause()
            java.lang.String r1 = X.AnonymousClass0D4.A01(r1)
            r9.put(r2, r1)
            goto L_0x047d
        L_0x04a4:
            r3 = 34
            int r2 = X.AnonymousClass1Y3.AO3
            X.0UN r0 = r13.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r2, r0)
            com.facebook.messaging.groups.create.logging.CreateGroupAggregatedLatencyLogger r0 = (com.facebook.messaging.groups.create.logging.CreateGroupAggregatedLatencyLogger) r0
            long r3 = r1.A00
            X.B4D r2 = r0.A03
            X.B4S r1 = X.B4S.WEB_SUCCESS
            java.lang.String r0 = java.lang.String.valueOf(r3)
            X.B4W r0 = X.B4W.A00(r0)
            r2.A03(r1, r0)
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12380pF.A0w(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0z(C11060ln r13, C27311cz r14) {
        FetchMessageAfterTimestampParams fetchMessageAfterTimestampParams = (FetchMessageAfterTimestampParams) r13.A00.getParcelable("fetchMessageAfterTimestampParams");
        ThreadKey threadKey = fetchMessageAfterTimestampParams.A01;
        long j = fetchMessageAfterTimestampParams.A00;
        FetchMoreRecentMessagesResult A0E = ((AnonymousClass7PV) AnonymousClass1XX.A02(24, AnonymousClass1Y3.ARS, this.A00)).A0E(new FetchMoreRecentMessagesParams(threadKey, j, null, null, 2, false), r13.A01);
        MessagesCollection messagesCollection = A0E.A02;
        if (messagesCollection != null && messagesCollection.A04() == 2 && A0E.A02.A07(1).A03 == j) {
            OperationResult A04 = OperationResult.A04(new FetchMessageResult(AnonymousClass102.FROM_SERVER, A0E.A02.A07(0), A0E.A00));
            C180878aL.A00(A04);
            return A04;
        }
        C000300h.A00();
        return OperationResult.A00(C14880uI.A05);
    }

    public C12380pF(AnonymousClass1XY r3, C04310Tq r4, C04310Tq r5, C04310Tq r6) {
        super("WebServiceHandler", (C12400pH) r6.get());
        this.A00 = new AnonymousClass0UN(35, r3);
        this.A02 = r4;
        this.A01 = r5;
    }

    public OperationResult BB0(C11060ln r3, C27311cz r4) {
        try {
            return super.BB0(r3, r4);
        } catch (Exception e) {
            ((AnonymousClass9KC) this.A02.get()).A02(e);
            throw e;
        }
    }
}
