package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1rf  reason: invalid class name and case insensitive filesystem */
public final class C35711rf {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 0:
                return "hits";
            case 1:
                return "misses";
            case 2:
                return "bytes";
            case 3:
                return "entries";
            case 4:
                return "eviction_on_cache_full_call";
            case 5:
                return "eviction_on_cache_full_item";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return "eviction_on_cache_full_size";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "eviction_on_stale_call";
            case 8:
                return "eviction_on_stale_item";
            case Process.SIGKILL:
                return "eviction_on_stale_size";
            case AnonymousClass1Y3.A01:
                return "eviction_on_user_forced_call";
            case AnonymousClass1Y3.A02:
                return "eviction_on_user_forced_item";
            case AnonymousClass1Y3.A03:
                return "eviction_on_user_forced_size";
            case 13:
                return "eviction_on_cache_manager_trimmed_call";
            case 14:
                return "eviction_on_cache_manager_trimmed_item";
            case 15:
                return "eviction_on_cache_manager_trimmed_size";
            case 16:
                return "insertion_item";
            case 17:
                return "insertion_size";
            case Process.SIGCONT:
                return "write_exceptions";
            case Process.SIGSTOP:
                return "read_exceptions";
            case 20:
                return "write_attempts";
            case AnonymousClass1Y3.A05:
                return "hit_time_ms";
            case AnonymousClass1Y3.A06:
                return "insertion_time_ms";
            default:
                throw new RuntimeException("CacheCounterType doesn't have a mapped string");
        }
    }
}
