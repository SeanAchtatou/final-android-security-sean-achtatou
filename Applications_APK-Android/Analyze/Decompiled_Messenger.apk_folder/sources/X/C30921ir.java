package X;

import java.io.InputStream;
import java.util.List;

/* renamed from: X.1ir  reason: invalid class name and case insensitive filesystem */
public final class C30921ir implements C23361Pf {
    private final AnonymousClass1OL A00;
    private final C31111jA A01;

    public long ASs(long j) {
        return 0;
    }

    public void trimToMinimum() {
    }

    public void trimToNothing() {
    }

    public void ASU() {
        this.A01.removeAll();
        this.A00.BSA();
    }

    public long getSize() {
        return this.A01.getSizeBytes();
    }

    public C30921ir(C31111jA r1, AnonymousClass1OL r2) {
        this.A00 = r2;
        this.A01 = r1;
    }

    private boolean A00(C23601Qd r4) {
        for (String hasKey : AnonymousClass1SG.A02(r4)) {
            if (this.A01.hasKey(hasKey)) {
                return true;
            }
        }
        return false;
    }

    public C24011Rv B17(C23601Qd r6) {
        List<String> A02 = AnonymousClass1SG.A02(r6);
        C33211nD A002 = C33211nD.A00();
        for (String str : A02) {
            InputStream A003 = this.A01.A00(str);
            if (A003 != null) {
                A002.A02 = str;
                this.A00.Baj(A002);
                return new C35721rg(A003, this.A01.getResourcePath(str).length());
            }
        }
        this.A00.BfG(A002);
        return null;
    }

    public boolean BBU(C23601Qd r2) {
        return A00(r2);
    }

    public boolean BBV(C23601Qd r2) {
        return A00(r2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r1 != null) goto L_0x001e;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0021 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C24011Rv BD7(X.C23601Qd r4, X.AnonymousClass95J r5) {
        /*
            r3 = this;
            java.lang.String r1 = X.AnonymousClass1SG.A00(r4)
            X.1nD r2 = X.C33211nD.A00()
            r2.A02 = r1
            X.1OL r0 = r3.A00
            r0.BvC(r2)
            X.1jA r0 = r3.A01     // Catch:{ IOException -> 0x002e }
            java.io.OutputStream r1 = r0.A01(r1)     // Catch:{ IOException -> 0x002e }
            r5.CNZ(r1)     // Catch:{ all -> 0x0019 }
            goto L_0x0022
        L_0x0019:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001b }
        L_0x001b:
            r0 = move-exception
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ all -> 0x0021 }
        L_0x0021:
            throw r0     // Catch:{ IOException -> 0x002e }
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x002e }
        L_0x0027:
            X.1OL r0 = r3.A00
            r0.BvE(r2)
            r0 = 0
            return r0
        L_0x002e:
            r1 = move-exception
            X.1OL r0 = r3.A00
            r0.BvD(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30921ir.BD7(X.1Qd, X.95J):X.1Rv");
    }

    public void C1E(C23601Qd r4) {
        for (String remove : AnonymousClass1SG.A02(r4)) {
            this.A01.remove(remove);
        }
        String A002 = AnonymousClass1SG.A00(r4);
        C33211nD A003 = C33211nD.A00();
        A003.A02 = A002;
        A003.A01 = AnonymousClass07B.A0C;
        this.A00.BY8(A003);
    }
}
