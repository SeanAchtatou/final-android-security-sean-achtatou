package X;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import com.facebook.analytics.appstatelogger.AppState;
import com.facebook.analytics.appstatelogger.AppStateLoggerCore$ForegroundAM;
import com.facebook.analytics.appstatelogger.AppStateLoggerCore$ForegroundInit;
import com.facebook.analytics.appstatelogger.AppStateLoggerCore$LaunchProxy;
import io.card.payment.BuildConfig;
import java.io.File;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/* renamed from: X.01P  reason: invalid class name */
public final class AnonymousClass01P {
    public static AnonymousClass01P A0Y;
    public static final Object A0Z = new Object();
    public AnonymousClass0KB A00;
    public AnonymousClass02G A01;
    public C010608s A02;
    public boolean A03;
    private boolean A04;
    public final ActivityManager.MemoryInfo A05 = new ActivityManager.MemoryInfo();
    public final ActivityManager A06;
    public final BroadcastReceiver A07 = new AnonymousClass01X(this);
    public final Context A08;
    public final AppState A09;
    public final C009708f A0A = new C009708f();
    public final C001901i A0B;
    public final AnonymousClass01G A0C;
    public final C009608e A0D = new C009608e();
    public final File A0E;
    public final Object A0F = new AppStateLoggerCore$ForegroundAM();
    public final Object A0G = new AppStateLoggerCore$ForegroundInit();
    public final Object A0H = new Object();
    public final Object A0I = new Object();
    public final Object A0J = new AppStateLoggerCore$LaunchProxy();
    public final StringBuilder A0K;
    public final HashSet A0L;
    public final Queue A0M = new ArrayDeque();
    public final boolean A0N;
    public final boolean A0O;
    public final boolean A0P;
    private final Handler A0Q;
    private final C001801h A0R;
    private final C03330Nh A0S;
    private final C001701e A0T;
    private final Object A0U = new Object();
    private final String A0V;
    private final List A0W;
    private volatile boolean A0X = false;

    public static C002001j A00() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0Y.A0B.A0U;
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return null;
        }
    }

    public static C001801h A01() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0Y.A0R;
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return null;
        }
    }

    public static String A02() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0Y.A09.A00();
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return BuildConfig.FLAVOR;
        }
    }

    public static String A03() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0Y.A09.A0K;
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return BuildConfig.FLAVOR;
        }
    }

    public static String A04() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0Y.A09.A0N;
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return BuildConfig.FLAVOR;
        }
    }

    public static String A05() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0Y.A0V;
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return BuildConfig.FLAVOR;
        }
    }

    private void A06() {
        synchronized (this.A0I) {
            if (!this.A03) {
                this.A08.registerReceiver(this.A07, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"), null, this.A0Q);
                this.A03 = true;
            }
        }
    }

    public static void A07(ActivityManager activityManager, ActivityManager.MemoryInfo memoryInfo, AppState appState) {
        if (activityManager != null) {
            activityManager.getMemoryInfo(memoryInfo);
            appState.A06 = memoryInfo.totalMem;
        }
    }

    public static void A08(AnonymousClass01P r3, int i, int i2) {
        int i3 = 32;
        if (i2 == 3) {
            i3 = 64;
        } else if (i2 == 9) {
            i3 = 96;
        }
        if (i > 30) {
            i = 30;
        }
        char c = (char) (i3 + i);
        C001901i r1 = r3.A0B;
        if (r1.A0R) {
            r1.A09.updatePendingStopTracking(c);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.A0A = r3;
        r1.A0E = r4;
        X.C001901i.A03(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        X.C001901i.A02(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        if (com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited == false) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0028, code lost:
        com.facebook.analytics.appstatelogger.AppStateLoggerNative.disableSelfSigkillHandlers();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        X.AnonymousClass01P.A0Y.A0B.join();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0034, code lost:
        X.C010708t.A0R("AppStateLoggerCore", r2, "Interrupted joining worker thread");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        r1 = X.AnonymousClass01P.A0Y;
        r1.A0X = true;
        r1 = r1.A0B;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0A(X.C002101k r3, java.lang.Throwable r4) {
        /*
            java.lang.Object r2 = X.AnonymousClass01P.A0Z
            monitor-enter(r2)
            X.01P r0 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x0010
            java.lang.String r1 = "AppStateLoggerCore"
            java.lang.String r0 = "No application has been registered with AppStateLogger"
            X.C010708t.A0J(r1, r0)     // Catch:{ all -> 0x003f }
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
            return
        L_0x0010:
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
            X.01P r1 = X.AnonymousClass01P.A0Y
            r0 = 1
            r1.A0X = r0
            X.01i r1 = r1.A0B
            monitor-enter(r1)
            r1.A0A = r3     // Catch:{ all -> 0x003c }
            r1.A0E = r4     // Catch:{ all -> 0x003c }
            X.C001901i.A03(r1)     // Catch:{ all -> 0x003c }
            monitor-exit(r1)     // Catch:{ all -> 0x003c }
            X.C001901i.A02(r1)
            boolean r0 = com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited
            if (r0 == 0) goto L_0x002b
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.disableSelfSigkillHandlers()
        L_0x002b:
            X.01P r0 = X.AnonymousClass01P.A0Y     // Catch:{ InterruptedException -> 0x0033 }
            X.01i r0 = r0.A0B     // Catch:{ InterruptedException -> 0x0033 }
            r0.join()     // Catch:{ InterruptedException -> 0x0033 }
            return
        L_0x0033:
            r2 = move-exception
            java.lang.String r1 = "AppStateLoggerCore"
            java.lang.String r0 = "Interrupted joining worker thread"
            X.C010708t.A0R(r1, r2, r0)
            return
        L_0x003c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003c }
            goto L_0x0041
        L_0x003f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003f }
        L_0x0041:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01P.A0A(X.01k, java.lang.Throwable):void");
    }

    public static void A0B(Object obj) {
        synchronized (A0Z) {
            if (A0Y == null) {
                C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            } else {
                A09(A0Y, obj, AnonymousClass01Y.IN_FOREGROUND);
            }
        }
    }

    public static void A0C(Object obj) {
        synchronized (A0Z) {
            if (A0Y == null) {
                C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            } else {
                A09(A0Y, obj, AnonymousClass01Y.IN_BACKGROUND);
            }
        }
    }

    public static boolean A0D() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0F(A0Y);
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return false;
        }
    }

    public static boolean A0E() {
        synchronized (A0Z) {
            if (A0Y != null) {
                return A0G(A0Y);
            }
            C010708t.A0J("AppStateLoggerCore", "AppStateLogger is not ready yet");
            return false;
        }
    }

    public static boolean A0F(AnonymousClass01P r3) {
        C009608e r32 = r3.A0D;
        synchronized (r32) {
            for (Map.Entry value : r32.A02.entrySet()) {
                if (value.getValue() == AnonymousClass01Y.ACTIVITY_RESUMED) {
                    return true;
                }
            }
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0011, code lost:
        if (r1 != X.AnonymousClass04f.A03) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004a, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0G(X.AnonymousClass01P r5) {
        /*
            boolean r0 = r5.A0X
            if (r0 != 0) goto L_0x0013
            X.01i r2 = r5.A0B
            monitor-enter(r2)
            X.04f r1 = r2.A0B     // Catch:{ all -> 0x000a }
            goto L_0x000d
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x000d:
            monitor-exit(r2)
            X.04f r0 = X.AnonymousClass04f.NO_ANR_DETECTED
            r4 = 1
            if (r1 == r0) goto L_0x0014
        L_0x0013:
            r4 = 0
        L_0x0014:
            X.08e r3 = r5.A0D
            monitor-enter(r3)
            java.util.WeakHashMap r0 = r3.A02     // Catch:{ all -> 0x004f }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x004f }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x004f }
        L_0x0021:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x004c
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x004f }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x004f }
            java.lang.Object r1 = r0.getValue()     // Catch:{ all -> 0x004f }
            X.01Y r1 = (X.AnonymousClass01Y) r1     // Catch:{ all -> 0x004f }
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_RESUMED     // Catch:{ all -> 0x004f }
            if (r1 == r0) goto L_0x0049
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_STARTED     // Catch:{ all -> 0x004f }
            if (r1 == r0) goto L_0x0049
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_CREATED     // Catch:{ all -> 0x004f }
            if (r1 == r0) goto L_0x0049
            X.01Y r0 = X.AnonymousClass01Y.IN_FOREGROUND     // Catch:{ all -> 0x004f }
            if (r1 == r0) goto L_0x0049
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_PAUSED     // Catch:{ all -> 0x004f }
            if (r1 != r0) goto L_0x0021
            if (r4 != 0) goto L_0x0021
        L_0x0049:
            monitor-exit(r3)
            r0 = 1
            return r0
        L_0x004c:
            monitor-exit(r3)
            r0 = 0
            return r0
        L_0x004f:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01P.A0G(X.01P):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x018c, code lost:
        if (r6 == 1) goto L_0x018e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01ec, code lost:
        if (r1 == X.AnonymousClass07B.A0C) goto L_0x018e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x00d5, code lost:
        if (r13.contains(":browser") != false) goto L_0x00d7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass01P(int r28, java.lang.String r29, int r30, java.lang.String r31, boolean r32, long r33, long r35, X.AnonymousClass01G r37, java.io.File r38, android.app.ActivityManager r39, android.content.Context r40, X.AnonymousClass01T r41, java.lang.String r42, java.lang.String r43, boolean r44, java.util.List r45, X.AnonymousClass0KB r46) {
        /*
            r27 = this;
            r3 = r27
            r27.<init>()
            X.08e r0 = new X.08e
            r0.<init>()
            r3.A0D = r0
            java.util.ArrayDeque r0 = new java.util.ArrayDeque
            r0.<init>()
            r3.A0M = r0
            android.app.ActivityManager$MemoryInfo r0 = new android.app.ActivityManager$MemoryInfo
            r0.<init>()
            r3.A05 = r0
            X.01X r0 = new X.01X
            r0.<init>(r3)
            r3.A07 = r0
            r0 = 0
            r3.A0X = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r3.A0H = r0
            com.facebook.analytics.appstatelogger.AppStateLoggerCore$LaunchProxy r0 = new com.facebook.analytics.appstatelogger.AppStateLoggerCore$LaunchProxy
            r0.<init>()
            r3.A0J = r0
            com.facebook.analytics.appstatelogger.AppStateLoggerCore$ForegroundInit r0 = new com.facebook.analytics.appstatelogger.AppStateLoggerCore$ForegroundInit
            r0.<init>()
            r3.A0G = r0
            com.facebook.analytics.appstatelogger.AppStateLoggerCore$ForegroundAM r0 = new com.facebook.analytics.appstatelogger.AppStateLoggerCore$ForegroundAM
            r0.<init>()
            r3.A0F = r0
            X.08f r0 = new X.08f
            r0.<init>()
            r3.A0A = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r3.A0I = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r3.A0U = r0
            r2 = r40
            r3.A08 = r2
            r8 = r38
            r3.A0E = r8
            r0 = r42
            r3.A0V = r0
            long r19 = java.lang.System.currentTimeMillis()
            r0 = 1000(0x3e8, double:4.94E-321)
            long r19 = r19 / r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r3.A0K = r0
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r3.A0L = r0
            r0 = r37
            r3.A0C = r0
            X.01e r0 = r0.A07(r2)
            r3.A0T = r0
            com.facebook.analytics.appstatelogger.AppState r12 = new com.facebook.analytics.appstatelogger.AppState
            r25 = 0
            r26 = 0
            r21 = r33
            r4 = r32
            r17 = r31
            r16 = r30
            r14 = r28
            r15 = r29
            r23 = r35
            r13 = r43
            r18 = r4
            r12.<init>(r13, r14, r15, r16, r17, r18, r19, r21, r23, r25)
            r3.A09 = r12
            r2 = r39
            r3.A06 = r2
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 16
            if (r1 < r0) goto L_0x00ad
            android.app.ActivityManager$MemoryInfo r0 = r3.A05
            A07(r2, r0, r12)
        L_0x00ad:
            X.01h r0 = new X.01h
            r0.<init>(r4)
            r3.A0R = r0
            java.util.concurrent.CopyOnWriteArrayList r0 = new java.util.concurrent.CopyOnWriteArrayList
            r1 = r45
            r0.<init>(r1)
            r3.A0W = r0
            X.01G r1 = r3.A0C
            android.content.Context r0 = r3.A08
            int r6 = r1.A01(r0)
            java.lang.String r5 = ":"
            boolean r0 = r13.contains(r5)
            java.lang.String r4 = ":browser"
            r1 = 1
            if (r0 == 0) goto L_0x00d7
            boolean r0 = r13.contains(r4)
            r2 = 0
            if (r0 == 0) goto L_0x00d8
        L_0x00d7:
            r2 = 1
        L_0x00d8:
            if (r2 == 0) goto L_0x0208
            if (r6 == r1) goto L_0x00e2
            r0 = 2
            if (r6 == r0) goto L_0x00e2
            r0 = 3
            if (r6 != r0) goto L_0x0208
        L_0x00e2:
            r3.A0N = r1
            X.01G r1 = r3.A0C
            android.content.Context r0 = r3.A08
            boolean r0 = r1.A0E(r0)
            r3.A0P = r0
            if (r2 == 0) goto L_0x00f8
            X.01G r1 = r3.A0C
            android.content.Context r0 = r3.A08
            int r26 = r1.A02(r0)
        L_0x00f8:
            boolean r0 = r3.A0N
            if (r0 == 0) goto L_0x0106
            X.01G r1 = r3.A0C
            android.content.Context r0 = r3.A08
            boolean r0 = r1.A0D(r0)
            if (r0 != 0) goto L_0x01ff
        L_0x0106:
            boolean r0 = r3.A0P
            if (r0 != 0) goto L_0x01ff
            r0 = 0
            r3.A0S = r0
        L_0x010d:
            X.01i r12 = new X.01i
            X.01G r11 = r3.A0C
            X.01h r10 = r3.A0R
            com.facebook.analytics.appstatelogger.AppState r9 = r3.A09
            X.08e r7 = r3.A0D
            android.content.Context r2 = r3.A08
            java.util.List r1 = r3.A0W
            X.0Nh r0 = r3.A0S
            r24 = r44
            r15 = r12
            r16 = r11
            r17 = r8
            r18 = r10
            r19 = r9
            r20 = r7
            r21 = r2
            r22 = r13
            r23 = r1
            r25 = r0
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
            r3.A0B = r12
            r0 = r46
            r3.A00 = r0
            boolean r0 = r3.A0N
            if (r0 == 0) goto L_0x0197
            android.app.ActivityManager$RunningAppProcessInfo r1 = new android.app.ActivityManager$RunningAppProcessInfo
            r1.<init>()
            X.01i r0 = r3.A0B
            X.08i r0 = r0.A0Y
            boolean r0 = r0.A03(r1)
            if (r0 == 0) goto L_0x01fc
            int r9 = r1.importance
        L_0x0150:
            X.01i r2 = r3.A0B
            boolean r1 = r3.A0P
            X.08h r0 = r2.A0W
            android.app.ActivityManager$RunningAppProcessInfo r0 = r0.A01
            r0.importance = r9
            if (r1 == 0) goto L_0x0174
            char r7 = X.AnonymousClass0KD.A00(r9)
            boolean r0 = r2.A0R
            if (r0 == 0) goto L_0x0174
            com.facebook.analytics.appstatelogger.AppStateLogFile r1 = r2.A09
            boolean r0 = r1.mIsEnabled
            if (r0 == 0) goto L_0x0174
            com.facebook.analytics.appstatelogger.AppStateLogFile.assertIsAscii(r7)
            java.nio.MappedByteBuffer r2 = r1.mMappedByteBuffer
            r1 = 4
            byte r0 = (byte) r7
            r2.put(r1, r0)
        L_0x0174:
            r8 = 1
            r7 = 0
            if (r6 == r8) goto L_0x017b
            r0 = 2
            if (r6 != r0) goto L_0x01ef
        L_0x017b:
            X.0KB r2 = r3.A00
            if (r2 == 0) goto L_0x01ef
            java.lang.Integer r1 = r2.getColdStartMode$REDEX$PsZwo0wQWz2()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x01ea
            X.08f r0 = r3.A0A
            r2.setColdStartModeChangeListener(r0)
            if (r6 != r8) goto L_0x0190
        L_0x018e:
            java.lang.Object r7 = r3.A0G
        L_0x0190:
            if (r7 == 0) goto L_0x0197
            X.01Y r0 = X.AnonymousClass01Y.IN_FOREGROUND
            A09(r3, r7, r0)
        L_0x0197:
            X.01i r0 = r3.A0B
            r0.start()
            java.lang.Thread r1 = new java.lang.Thread
            X.08j r0 = new X.08j
            r0.<init>()
            r1.<init>(r0)
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()
            r0.addShutdownHook(r1)
            X.01G r1 = r3.A0C
            android.content.Context r0 = r3.A08
            boolean r0 = r1.A0B(r0)
            r3.A0O = r0
            boolean r0 = r3.A0N
            if (r0 == 0) goto L_0x020b
            X.01G r1 = r3.A0C
            android.content.Context r0 = r3.A08
            boolean r0 = r1.A0C(r0)
            if (r0 == 0) goto L_0x020b
            boolean r0 = r13.contains(r5)
            if (r0 == 0) goto L_0x01d1
            boolean r0 = r13.contains(r4)
            if (r0 == 0) goto L_0x020b
        L_0x01d1:
            android.os.HandlerThread r2 = new android.os.HandlerThread
            java.lang.String r0 = "HomeTaskSwitcher Receiver thread"
            r2.<init>(r0)
            r2.start()
            android.os.Handler r1 = new android.os.Handler
            android.os.Looper r0 = r2.getLooper()
            r1.<init>(r0)
            r3.A0Q = r1
            r27.A06()
            return
        L_0x01ea:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            if (r1 != r0) goto L_0x0190
            goto L_0x018e
        L_0x01ef:
            r0 = 3
            if (r6 != r0) goto L_0x0190
            r0 = -1
            if (r9 == r0) goto L_0x0190
            r0 = 100
            if (r9 > r0) goto L_0x0190
            java.lang.Object r7 = r3.A0F
            goto L_0x0190
        L_0x01fc:
            r9 = -1
            goto L_0x0150
        L_0x01ff:
            X.0Nh r0 = new X.0Nh
            r0.<init>(r3)
            r3.A0S = r0
            goto L_0x010d
        L_0x0208:
            r1 = 0
            goto L_0x00e2
        L_0x020b:
            r0 = 0
            r3.A0Q = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01P.<init>(int, java.lang.String, int, java.lang.String, boolean, long, long, X.01G, java.io.File, android.app.ActivityManager, android.content.Context, X.01T, java.lang.String, java.lang.String, boolean, java.util.List, X.0KB):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:130|131|132|133) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001b, code lost:
        if (r14.A04 != false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0212, code lost:
        if (r11 != false) goto L_0x0214;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005c, code lost:
        if (A0F(r14) != false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        if (r15 == r14.A0F) goto L_0x0011;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:132:0x0192 */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0171 A[Catch:{ all -> 0x02b5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0181 A[Catch:{ all -> 0x02b5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0184 A[Catch:{ all -> 0x02b5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0198 A[SYNTHETIC, Splitter:B:138:0x0198] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A09(X.AnonymousClass01P r14, java.lang.Object r15, X.AnonymousClass01Y r16) {
        /*
            boolean r0 = r14.A0X
            if (r0 != 0) goto L_0x02c7
            java.lang.Object r0 = r14.A0J
            if (r15 == r0) goto L_0x0011
            java.lang.Object r0 = r14.A0G
            if (r15 == r0) goto L_0x0011
            java.lang.Object r0 = r14.A0F
            r3 = 0
            if (r15 != r0) goto L_0x0012
        L_0x0011:
            r3 = 1
        L_0x0012:
            java.lang.Object r2 = r14.A0U
            monitor-enter(r2)
            if (r15 == 0) goto L_0x0019
            if (r3 == 0) goto L_0x0020
        L_0x0019:
            boolean r0 = r14.A04     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x0020
        L_0x001d:
            monitor-exit(r2)     // Catch:{ all -> 0x02c4 }
            goto L_0x029f
        L_0x0020:
            if (r15 != 0) goto L_0x0028
            r0 = 0
        L_0x0023:
            r8 = r16
            if (r0 != 0) goto L_0x0049
            goto L_0x0031
        L_0x0028:
            java.lang.Class r0 = r15.getClass()     // Catch:{ all -> 0x02c4 }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x02c4 }
            goto L_0x0023
        L_0x0031:
            X.01Y r0 = X.AnonymousClass01Y.IN_BACKGROUND     // Catch:{ all -> 0x02c4 }
            boolean r0 = r0.equals(r8)     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x0049
            X.01Y r0 = X.AnonymousClass01Y.IN_BACKGROUND_DUE_TO_LOW_IMPORTANCE     // Catch:{ all -> 0x02c4 }
            boolean r0 = r0.equals(r8)     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x0049
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x02c4 }
            java.lang.String r0 = "State is expected to be IN_BACKGROUND or IN_BACKGROUND_DUE_TO_LOW_IMPORTANCE when entity is null"
            r1.<init>(r0)     // Catch:{ all -> 0x02c4 }
            throw r1     // Catch:{ all -> 0x02c4 }
        L_0x0049:
            X.08e r1 = r14.A0D     // Catch:{ all -> 0x02c4 }
            monitor-enter(r1)     // Catch:{ all -> 0x02c4 }
            int r7 = r1.A00     // Catch:{ all -> 0x02be }
            monitor-exit(r1)     // Catch:{ all -> 0x02c4 }
            boolean r0 = A0G(r14)     // Catch:{ all -> 0x02c4 }
            r6 = 1
            r9 = 0
            if (r0 != 0) goto L_0x005e
            boolean r0 = A0F(r14)     // Catch:{ all -> 0x02c4 }
            r13 = 0
            if (r0 == 0) goto L_0x005f
        L_0x005e:
            r13 = 1
        L_0x005f:
            if (r15 == 0) goto L_0x0068
            X.08e r0 = r14.A0D     // Catch:{ all -> 0x02c4 }
            int r5 = r0.A01(r15, r8)     // Catch:{ all -> 0x02c4 }
            goto L_0x0069
        L_0x0068:
            r5 = 0
        L_0x0069:
            if (r15 == 0) goto L_0x0071
            if (r3 != 0) goto L_0x009c
            boolean r0 = r14.A04     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x009c
        L_0x0071:
            if (r15 == 0) goto L_0x0074
            goto L_0x0076
        L_0x0074:
            r3 = r8
            goto L_0x0078
        L_0x0076:
            X.01Y r3 = X.AnonymousClass01Y.IN_BACKGROUND     // Catch:{ all -> 0x02c4 }
        L_0x0078:
            X.08e r1 = r14.A0D     // Catch:{ all -> 0x02c4 }
            java.lang.Object r0 = r14.A0J     // Catch:{ all -> 0x02c4 }
            r1.A01(r0, r3)     // Catch:{ all -> 0x02c4 }
            X.08e r1 = r14.A0D     // Catch:{ all -> 0x02c4 }
            java.lang.Object r0 = r14.A0G     // Catch:{ all -> 0x02c4 }
            r1.A01(r0, r3)     // Catch:{ all -> 0x02c4 }
            X.08e r1 = r14.A0D     // Catch:{ all -> 0x02c4 }
            java.lang.Object r0 = r14.A0F     // Catch:{ all -> 0x02c4 }
            r1.A01(r0, r3)     // Catch:{ all -> 0x02c4 }
            if (r15 == 0) goto L_0x009c
            r14.A04 = r6     // Catch:{ all -> 0x02c4 }
            com.facebook.analytics.appstatelogger.AppState r10 = r14.A09     // Catch:{ all -> 0x02c4 }
            long r3 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x02c4 }
            long r0 = r10.A05     // Catch:{ all -> 0x02c4 }
            long r3 = r3 - r0
            r10.A0C = r3     // Catch:{ all -> 0x02c4 }
        L_0x009c:
            boolean r0 = r14.A04     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x00b3
            boolean r0 = r14.A0P     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x00b3
            X.01i r1 = r14.A0B     // Catch:{ all -> 0x02c4 }
            X.08i r0 = r1.A0Y     // Catch:{ all -> 0x02c4 }
            r0.A06 = r6     // Catch:{ all -> 0x02c4 }
            X.08i r3 = r1.A0Y     // Catch:{ all -> 0x02c4 }
            r0 = 0
            java.lang.Object r1 = r3.A01     // Catch:{ all -> 0x02c4 }
            monitor-enter(r1)     // Catch:{ all -> 0x02c4 }
            r3.A00 = r0     // Catch:{ all -> 0x02a0 }
            monitor-exit(r1)     // Catch:{ all -> 0x02a0 }
        L_0x00b3:
            X.08e r1 = r14.A0D     // Catch:{ all -> 0x02c4 }
            monitor-enter(r1)     // Catch:{ all -> 0x02c4 }
            X.097 r10 = new X.097     // Catch:{ all -> 0x02bb }
            boolean r0 = r1.A01     // Catch:{ all -> 0x02bb }
            if (r0 == 0) goto L_0x00fd
            X.01Y r3 = X.AnonymousClass01Y.ACTIVITY_DESTROYED     // Catch:{ all -> 0x02bb }
        L_0x00be:
            r0 = 0
            r10.<init>(r3, r0)     // Catch:{ all -> 0x02bb }
            java.util.WeakHashMap r0 = r1.A02     // Catch:{ all -> 0x02bb }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x02bb }
            java.util.Iterator r11 = r0.iterator()     // Catch:{ all -> 0x02bb }
        L_0x00cc:
            boolean r0 = r11.hasNext()     // Catch:{ all -> 0x02bb }
            if (r0 == 0) goto L_0x0100
            java.lang.Object r4 = r11.next()     // Catch:{ all -> 0x02bb }
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4     // Catch:{ all -> 0x02bb }
            java.lang.Object r3 = r4.getValue()     // Catch:{ all -> 0x02bb }
            X.01Y r3 = (X.AnonymousClass01Y) r3     // Catch:{ all -> 0x02bb }
            X.01Y r0 = r10.A00     // Catch:{ all -> 0x02bb }
            int r0 = r3.compareTo(r0)     // Catch:{ all -> 0x02bb }
            if (r0 >= 0) goto L_0x00cc
            java.lang.Object r0 = r4.getValue()     // Catch:{ all -> 0x02bb }
            X.01Y r0 = (X.AnonymousClass01Y) r0     // Catch:{ all -> 0x02bb }
            r10.A00 = r0     // Catch:{ all -> 0x02bb }
            java.lang.Object r0 = r4.getKey()     // Catch:{ all -> 0x02bb }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x02bb }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x02bb }
            r10.A01 = r0     // Catch:{ all -> 0x02bb }
            goto L_0x00cc
        L_0x00fd:
            X.01Y r3 = X.AnonymousClass01Y.INITIAL_STATE     // Catch:{ all -> 0x02bb }
            goto L_0x00be
        L_0x0100:
            monitor-exit(r1)     // Catch:{ all -> 0x02c4 }
            X.01Y r1 = X.AnonymousClass01Y.ACTIVITY_DESTROYED     // Catch:{ all -> 0x02c4 }
            X.01Y r0 = r10.A00     // Catch:{ all -> 0x02c4 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x0111
            boolean r0 = r14.A04     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x0111
            r10.A00 = r8     // Catch:{ all -> 0x02c4 }
        L_0x0111:
            X.01i r11 = r14.A0B     // Catch:{ all -> 0x02c4 }
            X.01Y r4 = r10.A00     // Catch:{ all -> 0x02c4 }
            java.lang.String r12 = r10.A01     // Catch:{ all -> 0x02c4 }
            if (r12 == 0) goto L_0x0169
            X.02G r0 = r14.A01     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x0127
            X.01G r1 = r14.A0C     // Catch:{ all -> 0x02c4 }
            java.io.File r0 = r14.A0E     // Catch:{ all -> 0x02c4 }
            X.02G r0 = r1.A08(r0)     // Catch:{ all -> 0x02c4 }
            r14.A01 = r0     // Catch:{ all -> 0x02c4 }
        L_0x0127:
            X.02G r3 = r14.A01     // Catch:{ all -> 0x02c4 }
            if (r3 == 0) goto L_0x0169
            monitor-enter(r3)     // Catch:{ all -> 0x02c4 }
            java.util.Properties r0 = X.AnonymousClass02G.A03     // Catch:{ all -> 0x02c1 }
            java.lang.String r1 = r0.getProperty(r12)     // Catch:{ all -> 0x02c1 }
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x02c1 }
            if (r0 == 0) goto L_0x0164
            monitor-enter(r3)     // Catch:{ all -> 0x02c1 }
            char r1 = r3.A00     // Catch:{ all -> 0x02a6 }
            r0 = 126(0x7e, float:1.77E-43)
            if (r1 != r0) goto L_0x0143
            monitor-exit(r3)     // Catch:{ all -> 0x02c1 }
            r1 = 33
            goto L_0x016c
        L_0x0143:
            int r0 = r1 + 1
            char r0 = (char) r0     // Catch:{ all -> 0x02a6 }
            r3.A00 = r0     // Catch:{ all -> 0x02a6 }
            java.util.Properties r1 = X.AnonymousClass02G.A03     // Catch:{ all -> 0x02a6 }
            java.lang.String r0 = java.lang.Character.toString(r0)     // Catch:{ all -> 0x02a6 }
            r1.setProperty(r12, r0)     // Catch:{ all -> 0x02a6 }
            monitor-enter(r3)     // Catch:{ all -> 0x02a6 }
            android.os.Handler r12 = r3.A01     // Catch:{ all -> 0x02a3 }
            X.098 r1 = new X.098     // Catch:{ all -> 0x02a3 }
            r1.<init>(r3)     // Catch:{ all -> 0x02a3 }
            r0 = -2048351379(0xffffffff85e8a36d, float:-2.1877208E-35)
            X.AnonymousClass00S.A04(r12, r1, r0)     // Catch:{ all -> 0x02a3 }
            monitor-exit(r3)     // Catch:{ all -> 0x02a6 }
            char r1 = r3.A00     // Catch:{ all -> 0x02a6 }
            monitor-exit(r3)     // Catch:{ all -> 0x02c1 }
            goto L_0x016c
        L_0x0164:
            char r1 = r1.charAt(r9)     // Catch:{ all -> 0x02c1 }
            goto L_0x016c
        L_0x0169:
            r1 = 32
            goto L_0x016d
        L_0x016c:
            monitor-exit(r3)     // Catch:{ all -> 0x02c4 }
        L_0x016d:
            boolean r0 = r11.A0R     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x0176
            com.facebook.analytics.appstatelogger.AppStateLogFile r0 = r11.A09     // Catch:{ all -> 0x02c4 }
            r0.updateForegroundEntityInfo(r4, r1)     // Catch:{ all -> 0x02c4 }
        L_0x0176:
            X.01Y r1 = r10.A00     // Catch:{ all -> 0x02c4 }
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_STOPPED     // Catch:{ all -> 0x02c4 }
            int r1 = r1.compareTo(r0)     // Catch:{ all -> 0x02c4 }
            r0 = 0
            if (r1 >= 0) goto L_0x0182
            r0 = 1
        L_0x0182:
            if (r0 != 0) goto L_0x0195
            java.lang.Object r3 = r14.A0I     // Catch:{ all -> 0x02c4 }
            monitor-enter(r3)     // Catch:{ all -> 0x02c4 }
            boolean r0 = r14.A03     // Catch:{ all -> 0x02a9 }
            if (r0 == 0) goto L_0x0194
            android.content.Context r1 = r14.A08     // Catch:{ IllegalArgumentException -> 0x0192 }
            android.content.BroadcastReceiver r0 = r14.A07     // Catch:{ IllegalArgumentException -> 0x0192 }
            r1.unregisterReceiver(r0)     // Catch:{ IllegalArgumentException -> 0x0192 }
        L_0x0192:
            r14.A03 = r9     // Catch:{ all -> 0x02a9 }
        L_0x0194:
            monitor-exit(r3)     // Catch:{ all -> 0x02a9 }
        L_0x0195:
            java.util.Queue r3 = r14.A0M     // Catch:{ all -> 0x02c4 }
            monitor-enter(r3)     // Catch:{ all -> 0x02c4 }
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_STOPPED     // Catch:{ all -> 0x02b8 }
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x02b8 }
            if (r0 != 0) goto L_0x01a8
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_PAUSED     // Catch:{ all -> 0x02b8 }
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x02b8 }
            if (r0 == 0) goto L_0x01ad
        L_0x01a8:
            java.util.Queue r0 = r14.A0M     // Catch:{ all -> 0x02b8 }
            r0.poll()     // Catch:{ all -> 0x02b8 }
        L_0x01ad:
            java.util.Queue r0 = r14.A0M     // Catch:{ all -> 0x02b8 }
            int r1 = r0.size()     // Catch:{ all -> 0x02b8 }
            if (r1 <= 0) goto L_0x01c3
            java.util.Queue r0 = r14.A0M     // Catch:{ all -> 0x02b8 }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x02b8 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x02b8 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x02b8 }
        L_0x01c1:
            monitor-exit(r3)     // Catch:{ all -> 0x02b8 }
            goto L_0x01c5
        L_0x01c3:
            r0 = 0
            goto L_0x01c1
        L_0x01c5:
            A08(r14, r1, r0)     // Catch:{ all -> 0x02c4 }
            boolean r0 = r14.A0N     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x01ef
            android.os.Handler r0 = r14.A0Q     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x01ef
            boolean r0 = r14.A03     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x01ef
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_CREATED     // Catch:{ all -> 0x02c4 }
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x01ec
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_STARTED     // Catch:{ all -> 0x02c4 }
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x01ec
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_RESUMED     // Catch:{ all -> 0x02c4 }
            boolean r0 = r8.equals(r0)     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x01ef
        L_0x01ec:
            r14.A06()     // Catch:{ all -> 0x02c4 }
        L_0x01ef:
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_STARTED     // Catch:{ all -> 0x02c4 }
            if (r8 != r0) goto L_0x0203
            X.01h r0 = r14.A0R     // Catch:{ all -> 0x02c4 }
            r0.A02(r9)     // Catch:{ all -> 0x02c4 }
        L_0x01f8:
            boolean r11 = A0F(r14)     // Catch:{ all -> 0x02c4 }
            boolean r4 = A0G(r14)     // Catch:{ all -> 0x02c4 }
            if (r4 != 0) goto L_0x0214
            goto L_0x0211
        L_0x0203:
            X.01Y r0 = X.AnonymousClass01Y.ACTIVITY_STOPPED     // Catch:{ all -> 0x02c4 }
            if (r8 != r0) goto L_0x01f8
            X.01h r1 = r14.A0R     // Catch:{ all -> 0x02c4 }
            r0 = 0
            if (r5 != 0) goto L_0x020d
            r0 = 1
        L_0x020d:
            r1.A02(r0)     // Catch:{ all -> 0x02c4 }
            goto L_0x01f8
        L_0x0211:
            r10 = 0
            if (r11 == 0) goto L_0x0215
        L_0x0214:
            r10 = 1
        L_0x0215:
            java.lang.Class<com.facebook.analytics.appstatelogger.AppStateLoggerNative> r1 = com.facebook.analytics.appstatelogger.AppStateLoggerNative.class
            monitor-enter(r1)     // Catch:{ all -> 0x02c4 }
            boolean r0 = com.facebook.analytics.appstatelogger.AppStateLoggerNative.sAppStateLoggerNativeInited     // Catch:{ all -> 0x02b5 }
            if (r0 == 0) goto L_0x021f
            com.facebook.analytics.appstatelogger.AppStateLoggerNative.appInForeground(r11, r4)     // Catch:{ all -> 0x02b5 }
        L_0x021f:
            monitor-exit(r1)     // Catch:{ all -> 0x02c4 }
            boolean r0 = com.facebook.breakpad.BreakpadManager.isActive()     // Catch:{ all -> 0x02c4 }
            if (r0 == 0) goto L_0x023c
            java.lang.String r3 = "asl_app_in_foreground"
            java.lang.String r1 = java.lang.Boolean.toString(r11)     // Catch:{ all -> 0x02c4 }
            java.lang.Object[] r0 = new java.lang.Object[r9]     // Catch:{ all -> 0x02c4 }
            com.facebook.breakpad.BreakpadManager.setCustomData(r3, r1, r0)     // Catch:{ all -> 0x02c4 }
            java.lang.String r3 = "asl_app_in_foreground_v2"
            java.lang.String r1 = java.lang.Boolean.toString(r4)     // Catch:{ all -> 0x02c4 }
            java.lang.Object[] r0 = new java.lang.Object[r9]     // Catch:{ all -> 0x02c4 }
            com.facebook.breakpad.BreakpadManager.setCustomData(r3, r1, r0)     // Catch:{ all -> 0x02c4 }
        L_0x023c:
            X.01e r0 = r14.A0T     // Catch:{ all -> 0x02c4 }
            boolean r1 = r0.CDj(r5, r7, r8)     // Catch:{ all -> 0x02c4 }
            if (r1 != 0) goto L_0x024b
            boolean r0 = r0.CE8(r5, r7, r8)     // Catch:{ all -> 0x02c4 }
            if (r0 != 0) goto L_0x024b
            r6 = 0
        L_0x024b:
            X.01i r0 = r14.A0B     // Catch:{ all -> 0x02c4 }
            r0.A07(r6, r1)     // Catch:{ all -> 0x02c4 }
            if (r1 == 0) goto L_0x027d
            X.01i r6 = r14.A0B     // Catch:{ all -> 0x02c4 }
            java.lang.Object r5 = r6.A0b     // Catch:{ all -> 0x02c4 }
            monitor-enter(r5)     // Catch:{ all -> 0x02c4 }
            long r7 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x02ac }
            r0 = 400(0x190, double:1.976E-321)
            long r7 = r7 + r0
        L_0x025e:
            boolean r0 = r6.A0L     // Catch:{ all -> 0x02ac }
            if (r0 == 0) goto L_0x027c
            long r3 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x02ac }
            int r0 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r0 >= 0) goto L_0x027c
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ InterruptedException -> 0x025e }
            long r3 = r7 - r0
            r0 = 1
            long r3 = java.lang.Math.max(r3, r0)     // Catch:{ InterruptedException -> 0x025e }
            java.lang.Object r0 = r6.A0b     // Catch:{ InterruptedException -> 0x025e }
            r0.wait(r3)     // Catch:{ InterruptedException -> 0x025e }
            goto L_0x025e
        L_0x027c:
            monitor-exit(r5)     // Catch:{ all -> 0x02ac }
        L_0x027d:
            if (r10 == 0) goto L_0x028e
            if (r13 != 0) goto L_0x028e
            java.lang.Object r1 = r14.A0H     // Catch:{ all -> 0x02c4 }
            monitor-enter(r1)     // Catch:{ all -> 0x02c4 }
            X.08s r0 = r14.A02     // Catch:{ all -> 0x02af }
            if (r0 == 0) goto L_0x028b
            r0.onForeground()     // Catch:{ all -> 0x02af }
        L_0x028b:
            monitor-exit(r1)     // Catch:{ all -> 0x02af }
            goto L_0x001d
        L_0x028e:
            if (r10 != 0) goto L_0x001d
            if (r13 == 0) goto L_0x001d
            java.lang.Object r1 = r14.A0H     // Catch:{ all -> 0x02c4 }
            monitor-enter(r1)     // Catch:{ all -> 0x02c4 }
            X.08s r0 = r14.A02     // Catch:{ all -> 0x02b2 }
            if (r0 == 0) goto L_0x029c
            r0.onBackground()     // Catch:{ all -> 0x02b2 }
        L_0x029c:
            monitor-exit(r1)     // Catch:{ all -> 0x02b2 }
            goto L_0x001d
        L_0x029f:
            return
        L_0x02a0:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x02a0 }
            goto L_0x02c3
        L_0x02a3:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02a6 }
            throw r0     // Catch:{ all -> 0x02a6 }
        L_0x02a6:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02c1 }
            throw r0     // Catch:{ all -> 0x02c1 }
        L_0x02a9:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02a9 }
            goto L_0x02c3
        L_0x02ac:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x02ac }
            goto L_0x02c3
        L_0x02af:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x02af }
            goto L_0x02c3
        L_0x02b2:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x02b2 }
            goto L_0x02c3
        L_0x02b5:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x02c4 }
            goto L_0x02c3
        L_0x02b8:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02b8 }
            goto L_0x02c3
        L_0x02bb:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x02c4 }
            goto L_0x02c3
        L_0x02be:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x02c4 }
            goto L_0x02c3
        L_0x02c1:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x02c4 }
        L_0x02c3:
            throw r0     // Catch:{ all -> 0x02c4 }
        L_0x02c4:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x02c4 }
            throw r0
        L_0x02c7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01P.A09(X.01P, java.lang.Object, X.01Y):void");
    }
}
