package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1t1  reason: invalid class name and case insensitive filesystem */
public final class C36411t1 extends AnonymousClass1ZS {
    private static volatile C36411t1 A01;
    public AnonymousClass0UN A00;

    public static final C36411t1 A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (C36411t1.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A01 = new C36411t1(applicationInjector, AnonymousClass0VB.A00(AnonymousClass1Y3.B4u, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C36411t1(AnonymousClass1XY r3, AnonymousClass0US r4) {
        super(C006506l.A01(), r4);
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
