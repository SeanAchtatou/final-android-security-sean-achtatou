package X;

import java.util.ConcurrentModificationException;
import java.util.Map;
import java.util.NoSuchElementException;

/* renamed from: X.1Tx  reason: invalid class name and case insensitive filesystem */
public class C24511Tx implements Map.Entry {
    public int A00 = -1;
    private int A01;
    private int A02;
    public final /* synthetic */ C22141Kb A03;

    public C24511Tx(C22141Kb r2) {
        this.A03 = r2;
        this.A01 = r2.A00;
        this.A02 = r2.A06(-1);
    }

    public void A00() {
        int i = this.A01;
        C22141Kb r1 = this.A03;
        if (i == r1.A00) {
            int i2 = this.A02;
            if (i2 >= 0) {
                this.A00 = i2;
                this.A02 = r1.A06(i2);
                return;
            }
            throw new NoSuchElementException();
        }
        throw new ConcurrentModificationException();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (!C22141Kb.A05(entry.getKey(), getKey()) || !C22141Kb.A05(entry.getValue(), getValue())) {
            return false;
        }
        return true;
    }

    public Object getKey() {
        int i = this.A00;
        if (i >= 0) {
            return this.A03.A07(i);
        }
        throw new IllegalStateException("Iterator not pointing to any element.");
    }

    public Object getValue() {
        int i = this.A00;
        if (i >= 0) {
            return this.A03.A08(i);
        }
        throw new IllegalStateException("Iterator not pointing to any element.");
    }

    public boolean hasNext() {
        if (this.A02 >= 0) {
            return true;
        }
        return false;
    }

    public void remove() {
        int i = this.A00;
        if (i >= 0) {
            int i2 = this.A01;
            C22141Kb r1 = this.A03;
            if (i2 == r1.A00) {
                this.A01 = i2 + 1;
                r1.remove(r1.A07(i));
                Object[] objArr = this.A03.A03;
                int i3 = this.A00;
                if (objArr[i3 << 1] != null) {
                    this.A02 = i3;
                }
                this.A00 = -1;
                return;
            }
            throw new ConcurrentModificationException();
        }
        throw new IllegalStateException("Iterator not pointing to any element.");
    }

    public Object setValue(Object obj) {
        int i = this.A00;
        if (i >= 0) {
            int i2 = (i << 1) + 1;
            Object[] objArr = this.A03.A03;
            Object obj2 = objArr[i2];
            objArr[i2] = obj;
            return obj2;
        }
        throw new IllegalStateException("Iterator not pointing to any element.");
    }

    public int hashCode() {
        int hashCode;
        Object key = getKey();
        Object value = getValue();
        int i = 0;
        if (key == null) {
            hashCode = 0;
        } else {
            hashCode = key.hashCode();
        }
        if (value != null) {
            i = value.hashCode();
        }
        return hashCode ^ i;
    }
}
