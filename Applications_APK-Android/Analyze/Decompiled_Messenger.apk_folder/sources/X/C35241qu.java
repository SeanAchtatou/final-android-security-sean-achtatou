package X;

import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.1qu  reason: invalid class name and case insensitive filesystem */
public final class C35241qu implements C32691mA {
    public final /* synthetic */ ThreadListFragment A00;

    public boolean CF6(C21681Ih r2, C21681Ih r3) {
        return false;
    }

    public C35241qu(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public C31551js AVd(C21681Ih r3) {
        if (Boolean.TRUE.equals((Boolean) r3.AjM("THREAD_SAFE"))) {
            return null;
        }
        return new C35501rK(this.A00.A0I);
    }
}
