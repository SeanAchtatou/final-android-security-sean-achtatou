package X;

import com.facebook.messaging.accountswitch.model.MessengerAccountInfo;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1cb  reason: invalid class name and case insensitive filesystem */
public final class C27071cb implements C27081cc {
    private static volatile C27071cb A01;
    private AnonymousClass0UN A00;

    public static final C27071cb A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C27071cb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C27071cb(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public MessengerAccountInfo Aba(String str) {
        MessengerAccountInfo Aba = ((C27091cd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGj, this.A00)).Aba(str);
        if (Aba == null) {
            return ((C27101ce) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCT, this.A00)).Aba(str);
        }
        return Aba;
    }

    public List Abd() {
        ArrayList A03 = ((C27091cd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGj, this.A00)).Abd();
        ArrayList A02 = ((C27101ce) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCT, this.A00)).Abd();
        ArrayList arrayList = new ArrayList(A03.size() + A02.size());
        arrayList.addAll(A03);
        arrayList.addAll(A02);
        return arrayList;
    }

    public int Aw9() {
        return ((C27091cd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGj, this.A00)).Aw9() + ((C27101ce) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCT, this.A00)).Aw9();
    }

    public boolean BBk() {
        return ((C27091cd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGj, this.A00)).BBk();
    }

    public MessengerAccountInfo C0K() {
        if (((Boolean) AnonymousClass1XX.A03(AnonymousClass1Y3.AUm, this.A00)).booleanValue()) {
            return ((C27101ce) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCT, this.A00)).C0K();
        }
        return ((C27091cd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGj, this.A00)).C0K();
    }

    public void C1K(String str) {
        ((C27091cd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGj, this.A00)).C1K(str);
        ((C27101ce) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCT, this.A00)).C1K(str);
    }

    public void C4Q(MessengerAccountInfo messengerAccountInfo) {
        if (messengerAccountInfo.A05) {
            ((C27101ce) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCT, this.A00)).C4Q(messengerAccountInfo);
        } else {
            ((C27091cd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGj, this.A00)).C4Q(messengerAccountInfo);
        }
    }

    public void C91(C49132bh r4) {
        int i = AnonymousClass1Y3.AGj;
        AnonymousClass0UN r2 = this.A00;
        ((C27091cd) AnonymousClass1XX.A02(0, i, r2)).C91(r4);
        ((C27101ce) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCT, r2)).C91(r4);
    }

    private C27071cb(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
