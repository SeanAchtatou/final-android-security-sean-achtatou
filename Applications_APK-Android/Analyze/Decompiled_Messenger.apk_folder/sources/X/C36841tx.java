package X;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: X.1tx  reason: invalid class name and case insensitive filesystem */
public final class C36841tx extends C06220b5 {
    public String A0D() {
        int A02 = A02(4);
        if (A02 != 0) {
            return A05(A02 + this.A00);
        }
        return null;
    }

    public static C36841tx A00(ByteBuffer byteBuffer) {
        C36841tx r2 = new C36841tx();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        r2.A00 = byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position();
        r2.A01 = byteBuffer;
        return r2;
    }

    public C47332Uk A06() {
        C47332Uk r2 = new C47332Uk();
        int A02 = A02(48);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public AnonymousClass2UQ A07() {
        AnonymousClass2UQ r2 = new AnonymousClass2UQ();
        int A02 = A02(42);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public C47372Uo A08() {
        C47372Uo r2 = new C47372Uo();
        int A02 = A02(26);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public C47322Uj A09() {
        C47322Uj r2 = new C47322Uj();
        int A02 = A02(20);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public AnonymousClass1T9 A0A() {
        AnonymousClass1T9 r2 = new AnonymousClass1T9();
        int A02 = A02(6);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public AnonymousClass2UR A0B() {
        AnonymousClass2UR r2 = new AnonymousClass2UR();
        int A02 = A02(54);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public C47312Ui A0C() {
        C47312Ui r2 = new C47312Ui();
        int A02 = A02(56);
        if (A02 == 0) {
            return null;
        }
        int A01 = A01(A02 + this.A00);
        ByteBuffer byteBuffer = this.A01;
        r2.A00 = A01;
        r2.A01 = byteBuffer;
        return r2;
    }

    public String A0E() {
        int A02 = A02(12);
        if (A02 != 0) {
            return A05(A02 + this.A00);
        }
        return null;
    }
}
