package X;

import android.net.Uri;
import java.io.IOException;

/* renamed from: X.1x4  reason: invalid class name and case insensitive filesystem */
public interface C38211x4 {
    void BWv(int i, EjZ ejZ, C38031wm r3);

    void Bck(Uri uri, String str);

    void Bco(int i, EjZ ejZ, C21422ADo aDo, C38031wm r4);

    void Bcs(int i, EjZ ejZ, C21422ADo aDo, C38031wm r4, Object obj);

    void Bcv(int i, EjZ ejZ, C21422ADo aDo, C38031wm r4, IOException iOException, boolean z);

    void BdD(int i, EjZ ejZ, C21422ADo aDo, C38031wm r4);

    void Beb(int i, EjZ ejZ);

    void Bec(int i, EjZ ejZ);

    void Bky(int i, EjZ ejZ);

    void Bti(int i, EjZ ejZ, C38031wm r3);
}
