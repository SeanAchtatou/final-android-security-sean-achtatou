package X;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps$EntryFunction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1Fl  reason: invalid class name and case insensitive filesystem */
public final class C21161Fl extends C33131n4<K, V> {
    public final /* synthetic */ C21111Fg A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C21161Fl(C21111Fg r1) {
        super(r1);
        this.A00 = r1;
    }

    private boolean A00(Predicate predicate) {
        return AnonymousClass0j4.A0B(this.A00.A01.entrySet(), Predicates.and(this.A00.A00, new Predicates.CompositionPredicate(predicate, Maps$EntryFunction.KEY)));
    }

    public boolean remove(Object obj) {
        if (!this.A00.containsKey(obj)) {
            return false;
        }
        this.A00.A01.remove(obj);
        return true;
    }

    public boolean removeAll(Collection collection) {
        return A00(Predicates.in(collection));
    }

    public boolean retainAll(Collection collection) {
        return A00(new Predicates.NotPredicate(Predicates.in(collection)));
    }

    public Object[] toArray() {
        Iterator it = iterator();
        ArrayList A002 = C04300To.A00();
        C24931Xr.A09(A002, it);
        return A002.toArray();
    }

    public Object[] toArray(Object[] objArr) {
        Iterator it = iterator();
        ArrayList A002 = C04300To.A00();
        C24931Xr.A09(A002, it);
        return A002.toArray(objArr);
    }
}
