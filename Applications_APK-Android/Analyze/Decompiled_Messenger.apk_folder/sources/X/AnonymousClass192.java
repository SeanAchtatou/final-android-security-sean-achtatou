package X;

import com.facebook.cipher.jni.CipherHybrid;
import com.facebook.cipher.jni.DecryptHybrid;
import com.facebook.cipher.jni.EncryptHybrid;
import com.facebook.crypto.keychain.KeyChain;
import java.util.Arrays;

/* renamed from: X.192  reason: invalid class name */
public final class AnonymousClass192 {
    public final AnonymousClass193 A00;
    public final AnonymousClass18C A01;

    public byte[] A00(byte[] bArr, C194418e r12) {
        AnonymousClass193 r5 = this.A00;
        r5.A01.A04();
        int length = bArr.length;
        Integer num = r5.A02;
        int A02 = length - ((AnonymousClass189.A02(num) + 2) + AnonymousClass189.A01(num));
        byte[] bArr2 = new byte[A02];
        CipherHybrid cipherHybrid = new CipherHybrid(AnonymousClass189.A00(num), r5.A00);
        byte[] bArr3 = r12.A00;
        DecryptHybrid createDecrypt = cipherHybrid.createDecrypt(bArr3, 0, bArr3.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, AnonymousClass189.A02(r5.A02) + 2);
        createDecrypt.start(copyOfRange);
        int length2 = copyOfRange.length;
        createDecrypt.read(bArr, length2, bArr2, 0, A02);
        if (createDecrypt.end(Arrays.copyOfRange(bArr, length2 + A02, length))) {
            return bArr2;
        }
        throw new AnonymousClass9WT();
    }

    public byte[] A01(byte[] bArr, C194418e r13) {
        AnonymousClass193 r5 = this.A00;
        r5.A01.A04();
        int length = bArr.length;
        Integer num = r5.A02;
        int A02 = length + AnonymousClass189.A02(num) + 2 + AnonymousClass189.A01(num);
        byte[] bArr2 = new byte[A02];
        CipherHybrid cipherHybrid = new CipherHybrid(AnonymousClass189.A00(num), r5.A00);
        byte[] bArr3 = r13.A00;
        EncryptHybrid createEncrypt = cipherHybrid.createEncrypt(bArr3, 0, bArr3.length);
        byte[] start = createEncrypt.start();
        int length2 = start.length;
        System.arraycopy(start, 0, bArr2, 0, length2);
        createEncrypt.write(bArr, 0, bArr2, length2, length);
        byte[] end = createEncrypt.end();
        int length3 = end.length;
        System.arraycopy(end, 0, bArr2, A02 - length3, length3);
        return bArr2;
    }

    public AnonymousClass192(KeyChain keyChain, AnonymousClass18C r3, Integer num) {
        this.A00 = new AnonymousClass193(r3, num, keyChain);
        this.A01 = r3;
    }
}
