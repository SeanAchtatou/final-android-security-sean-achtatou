package X;

/* renamed from: X.0Zg  reason: invalid class name and case insensitive filesystem */
public final class C05520Zg {
    public static void A01(int i, int i2) {
        Object[] objArr;
        String str;
        if (i < 0 || i >= i2) {
            if (i < 0) {
                objArr = new Object[]{"index", Integer.valueOf(i)};
                str = "%s (%s) must not be negative";
            } else if (i2 >= 0) {
                objArr = new Object[]{"index", Integer.valueOf(i), Integer.valueOf(i2)};
                str = "%s (%s) must be less than size (%s)";
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A09("negative size: ", i2));
            }
            throw new IndexOutOfBoundsException(A00(str, objArr));
        }
    }

    public static void A02(Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
    }

    public static void A03(Object obj, Object obj2) {
        if (obj == null) {
            throw new NullPointerException(String.valueOf(obj2));
        }
    }

    public static void A04(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    public static void A05(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    public static void A06(boolean z, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    public static void A07(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static String A00(String str, Object... objArr) {
        String valueOf = String.valueOf(str);
        int length = valueOf.length();
        int length2 = objArr.length;
        StringBuilder sb = new StringBuilder(length + (length2 << 4));
        int i = 0;
        int i2 = 0;
        while (i < length2) {
            int indexOf = valueOf.indexOf("%s", i2);
            if (indexOf == -1) {
                break;
            }
            sb.append(valueOf.substring(i2, indexOf));
            sb.append(objArr[i]);
            i2 = indexOf + 2;
            i++;
        }
        sb.append(valueOf.substring(i2));
        if (i < length2) {
            sb.append(" [");
            sb.append(objArr[i]);
            for (int i3 = i + 1; i3 < length2; i3++) {
                sb.append(", ");
                sb.append(objArr[i3]);
            }
            sb.append(']');
        }
        return sb.toString();
    }
}
