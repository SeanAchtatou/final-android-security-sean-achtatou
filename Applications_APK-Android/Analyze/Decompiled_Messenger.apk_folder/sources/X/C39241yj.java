package X;

import com.facebook.messaging.model.messages.Message;
import com.facebook.user.model.User;
import java.util.Map;

/* renamed from: X.1yj  reason: invalid class name and case insensitive filesystem */
public final class C39241yj {
    public final long A00;
    public final Message A01;
    public final Message A02;
    public final Message A03;
    public final Message A04;
    public final Message A05;
    public final User A06;
    public final Map A07;
    public final boolean A08;
    public final boolean A09;

    public C39241yj(User user, Message message, Message message2, Message message3, Message message4, Message message5, long j, Map map, boolean z, boolean z2) {
        this.A06 = user;
        this.A03 = message;
        this.A04 = message2;
        this.A05 = message5;
        this.A02 = message3;
        this.A01 = message4;
        this.A00 = j;
        this.A07 = map;
        this.A08 = z;
        this.A09 = z2;
    }
}
