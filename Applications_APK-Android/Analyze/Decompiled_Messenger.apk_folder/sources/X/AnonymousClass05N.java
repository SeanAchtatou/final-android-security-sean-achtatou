package X;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Process;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;
import java.util.List;

/* renamed from: X.05N  reason: invalid class name */
public final class AnonymousClass05N extends AnonymousClass09F {
    private Context A00;

    public AnonymousClass05N(Context context) {
        super(null);
        this.A00 = context;
    }

    private void A00() {
        try {
            ActivityManager activityManager = (ActivityManager) this.A00.getSystemService("activity");
            if (activityManager != null) {
                List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
                String str = null;
                if (runningAppProcesses != null) {
                    StringBuilder sb = new StringBuilder();
                    for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                        if (next.uid == Process.myUid()) {
                            sb.append(AnonymousClass08S.A0M(next.processName, "(", next.pid, "),"));
                        }
                    }
                    if (sb.length() != 0) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    str = sb.toString();
                }
                if (str == null || str.isEmpty()) {
                    str = "PROCESS_METADATA_PROVIDER_FAILED_TO_GET_PROCESS_LIST";
                }
                Logger.writeBytesEntry(0, 1, 57, Logger.writeBytesEntry(0, 1, 56, Logger.writeStandardEntry(0, 7, 76, 0, 0, 0, 0, 0), "processes"), str);
            }
        } catch (Throwable unused) {
        }
    }

    public void A09(TraceContext traceContext, C004505k r2) {
        A00();
    }

    public void logOnTraceEnd(TraceContext traceContext, C004505k r2) {
        A00();
    }
}
