package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1r2  reason: invalid class name and case insensitive filesystem */
public final class C35321r2 {
    private static volatile C35321r2 A01;
    public final C25051Yd A00;

    public static final C35321r2 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C35321r2.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C35321r2(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C35321r2(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }
}
