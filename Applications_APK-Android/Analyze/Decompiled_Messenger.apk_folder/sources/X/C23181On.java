package X;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1On  reason: invalid class name and case insensitive filesystem */
public final class C23181On {
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0159, code lost:
        if (r4 != 3) goto L_0x015b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x01d5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x012c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C30151EqV A00(java.util.List r19, X.C23091Oe r20, X.AnonymousClass1N6 r21) {
        /*
            if (r19 == 0) goto L_0x0204
            boolean r0 = r19.isEmpty()
            if (r0 != 0) goto L_0x0204
            int r0 = r19.size()
            X.EqW[] r13 = new X.C30152EqW[r0]
            r12 = 0
            if (r20 == 0) goto L_0x0013
            X.1zQ[] r12 = new X.C39671zQ[r0]
        L_0x0013:
            r18 = 0
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.util.Iterator r17 = r19.iterator()
        L_0x001e:
            boolean r0 = r17.hasNext()
            if (r0 == 0) goto L_0x01fb
            java.lang.Object r14 = r17.next()
            X.9zh r14 = (X.C211779zh) r14
            java.lang.String r0 = r14.A02
            java.lang.String r10 = X.C23191Oo.A00(r0)
            java.lang.String r2 = r14.A00
            if (r2 == 0) goto L_0x01f3
            if (r10 == 0) goto L_0x01f3
            if (r20 == 0) goto L_0x01a1
            X.1zQ r9 = new X.1zQ
            r9.<init>()
            java.util.List r0 = r14.A03
            if (r0 == 0) goto L_0x019f
            java.util.Iterator r16 = r0.iterator()
        L_0x0045:
            boolean r0 = r16.hasNext()
            if (r0 == 0) goto L_0x019f
            java.lang.Object r0 = r16.next()
            X.9zi r0 = (X.C211789zi) r0
            java.lang.String r2 = r0.A00
            java.lang.String r8 = r0.A01
            java.util.List r7 = r0.A02
            if (r2 == 0) goto L_0x01eb
            if (r8 == 0) goto L_0x01e3
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r6 = r2.toLowerCase(r0)
            r15 = 0
            if (r8 == 0) goto L_0x0089
            r3 = -1
            int r5 = r8.hashCode()
            r0 = 3392903(0x33c587, float:4.75447E-39)
            r2 = 2
            r1 = 1
            if (r5 == r0) goto L_0x0194
            r0 = 104980213(0x641def5, float:3.6463069E-35)
            if (r5 == r0) goto L_0x0189
            r0 = 1509016093(0x59f1c21d, float:8.5061124E15)
            if (r5 != r0) goto L_0x0083
            java.lang.String r0 = "catch_all"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0083
            r3 = 0
        L_0x0083:
            if (r3 == 0) goto L_0x0183
            if (r3 == r1) goto L_0x0183
            if (r3 == r2) goto L_0x0183
        L_0x0089:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r15)
        L_0x008d:
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x009e
            X.BCC r1 = new X.BCC
            r1.<init>(r6, r8, r7)
        L_0x0098:
            java.util.List r0 = r9.A00
            r0.add(r1)
            goto L_0x0045
        L_0x009e:
            java.lang.String r0 = "same"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00ac
            X.BCA r1 = new X.BCA
            r1.<init>(r6, r7)
            goto L_0x0098
        L_0x00ac:
            r1 = r8
            if (r8 == 0) goto L_0x00ba
            r2 = -1
            int r0 = r8.hashCode()
            switch(r0) {
                case 3244: goto L_0x0122;
                case 3309: goto L_0x0117;
                case 3464: goto L_0x010d;
                case 98322: goto L_0x0103;
                case 98694: goto L_0x00f9;
                case 102680: goto L_0x00ee;
                case 107485: goto L_0x00e4;
                case 108954: goto L_0x00da;
                case 109854: goto L_0x00d0;
                case 110226: goto L_0x00c6;
                default: goto L_0x00b7;
            }
        L_0x00b7:
            switch(r2) {
                case 0: goto L_0x00c4;
                case 1: goto L_0x00c4;
                case 2: goto L_0x00c4;
                case 3: goto L_0x00c4;
                case 4: goto L_0x00c4;
                case 5: goto L_0x00c4;
                case 6: goto L_0x00c4;
                case 7: goto L_0x00c4;
                case 8: goto L_0x00c4;
                case 9: goto L_0x00c4;
                default: goto L_0x00ba;
            }
        L_0x00ba:
            r1 = 0
            r0 = r1
        L_0x00bc:
            if (r1 == 0) goto L_0x012c
            X.BCB r1 = new X.BCB
            r1.<init>(r6, r0, r7)
            goto L_0x0098
        L_0x00c4:
            r0 = r8
            goto L_0x00bc
        L_0x00c6:
            java.lang.String r0 = "oor"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 0
            goto L_0x00b7
        L_0x00d0:
            java.lang.String r0 = "ocr"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 2
            goto L_0x00b7
        L_0x00da:
            java.lang.String r0 = "neq"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 7
            goto L_0x00b7
        L_0x00e4:
            java.lang.String r0 = "lte"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 5
            goto L_0x00b7
        L_0x00ee:
            java.lang.String r0 = "gte"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 9
            goto L_0x00b7
        L_0x00f9:
            java.lang.String r0 = "cor"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 3
            goto L_0x00b7
        L_0x0103:
            java.lang.String r0 = "ccr"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 1
            goto L_0x00b7
        L_0x010d:
            java.lang.String r0 = "lt"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 4
            goto L_0x00b7
        L_0x0117:
            java.lang.String r0 = "gt"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 8
            goto L_0x00b7
        L_0x0122:
            java.lang.String r0 = "eq"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x00b7
            r2 = 6
            goto L_0x00b7
        L_0x012c:
            r5 = r8
            if (r8 == 0) goto L_0x015b
            r4 = -1
            int r3 = r8.hashCode()
            r0 = -567445985(0xffffffffde2d761f, float:-3.12480298E18)
            r15 = 3
            r2 = 2
            r1 = 1
            if (r3 == r0) goto L_0x0179
            r0 = 3365(0xd25, float:4.715E-42)
            if (r3 == r0) goto L_0x016f
            r0 = 109075(0x1aa13, float:1.52847E-40)
            if (r3 == r0) goto L_0x0165
            r0 = 108392519(0x675f047, float:4.6255906E-35)
            if (r3 != r0) goto L_0x0153
            java.lang.String r0 = "regex"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0153
            r4 = 3
        L_0x0153:
            if (r4 == 0) goto L_0x015c
            if (r4 == r1) goto L_0x015c
            if (r4 == r2) goto L_0x015c
            if (r4 == r15) goto L_0x015c
        L_0x015b:
            r5 = 0
        L_0x015c:
            if (r5 == 0) goto L_0x01d5
            X.BCy r1 = new X.BCy
            r1.<init>(r6, r5, r7)
            goto L_0x0098
        L_0x0165:
            java.lang.String r0 = "nin"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0153
            r4 = 1
            goto L_0x0153
        L_0x016f:
            java.lang.String r0 = "in"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0153
            r4 = 0
            goto L_0x0153
        L_0x0179:
            java.lang.String r0 = "contains"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0153
            r4 = 2
            goto L_0x0153
        L_0x0183:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            goto L_0x008d
        L_0x0189:
            java.lang.String r0 = "nnull"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0083
            r3 = 2
            goto L_0x0083
        L_0x0194:
            java.lang.String r0 = "null"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x0083
            r3 = 1
            goto L_0x0083
        L_0x019f:
            r12[r18] = r9
        L_0x01a1:
            java.lang.String r3 = r14.A01
            if (r3 == 0) goto L_0x01c9
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x01c9
            X.EqW r2 = new X.EqW
            java.lang.String r1 = r14.A00
            X.1Oo r0 = new X.1Oo
            r0.<init>(r10, r3)
            r2.<init>(r1, r0)
            r13[r18] = r2
        L_0x01b9:
            if (r18 <= 0) goto L_0x01c0
            java.lang.String r0 = ", "
            r11.append(r0)
        L_0x01c0:
            java.lang.String r0 = r14.A00
            r11.append(r0)
            int r18 = r18 + 1
            goto L_0x001e
        L_0x01c9:
            X.EqW r1 = new X.EqW
            java.lang.String r0 = r14.A00
            r2 = r21
            r1.<init>(r0, r2)
            r13[r18] = r1
            goto L_0x01b9
        L_0x01d5:
            X.1wT r2 = new X.1wT
            java.lang.String r0 = ","
            java.lang.String r1 = X.AnonymousClass08S.A0P(r6, r0, r8)
            java.lang.String r0 = "Unknown bucket definition"
            r2.<init>(r0, r1)
            throw r2
        L_0x01e3:
            X.1wT r1 = new X.1wT
            java.lang.String r0 = "Missing bucket strategy"
            r1.<init>(r0, r2)
            throw r1
        L_0x01eb:
            X.1wT r1 = new X.1wT
            java.lang.String r0 = "Missing bucket name"
            r1.<init>(r0)
            throw r1
        L_0x01f3:
            X.1wT r1 = new X.1wT
            java.lang.String r0 = "Bad context identifier"
            r1.<init>(r0, r2)
            throw r1
        L_0x01fb:
            X.EqV r0 = new X.EqV
            r11.toString()
            r0.<init>(r13, r12)
            return r0
        L_0x0204:
            X.1wT r1 = new X.1wT
            java.lang.String r0 = "Missing context in config"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C23181On.A00(java.util.List, X.1Oe, X.1N6):X.EqV");
    }

    public static C23201Op A01(List list) {
        String str;
        if (list == null || list.size() == 0) {
            throw new C37861wT("Missing outputs field definition");
        }
        int size = list.size();
        String[] strArr = new String[size];
        HashMap hashMap = new HashMap(size);
        int i = 0;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C23151Ok r3 = (C23151Ok) it.next();
            String str2 = r3.A01;
            if (str2 == null || (str = r3.A00) == null) {
                throw new C37861wT("Missing output field", r3.A00);
            }
            strArr[i] = C23191Oo.A00(str2);
            hashMap.put(str, Integer.valueOf(i));
            if (strArr[i] != null) {
                i++;
            } else {
                throw new C37861wT("Bad output type", r3.A01);
            }
        }
        return new C23201Op(strArr, hashMap);
    }

    public static C23191Oo[] A02(List list, C23201Op r8) {
        int i;
        int intValue;
        if (list != null && list.size() == (i = r8.A00)) {
            C23191Oo[] r5 = new C23191Oo[i];
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C23161Ol r6 = (C23161Ol) it.next();
                Integer num = (Integer) r8.A01.get(r6.A00);
                if (num == null || (intValue = num.intValue()) >= r8.A00) {
                    throw new C37861wT("Undeclared output param", r6.A00);
                }
                r5[intValue] = new C23191Oo(r8.A02[intValue], r6.A01);
            }
            int i2 = 0;
            while (i2 < r8.A00) {
                if (r5[i2] != null) {
                    i2++;
                }
            }
            return r5;
        }
        throw new C37861wT("Missing default value");
    }
}
