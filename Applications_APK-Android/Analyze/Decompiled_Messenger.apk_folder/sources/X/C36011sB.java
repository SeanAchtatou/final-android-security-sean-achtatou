package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.inboxads.common.InboxAdsImage;

/* renamed from: X.1sB  reason: invalid class name and case insensitive filesystem */
public final class C36011sB implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxAdsImage(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxAdsImage[i];
    }
}
