package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.1af  reason: invalid class name and case insensitive filesystem */
public final class C25951af {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) A03.A09("ip"));
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F;
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y7 A0H;
    public static final AnonymousClass1Y7 A0I;
    public static final AnonymousClass1Y7 A0J = ((AnonymousClass1Y7) A0F.A09("sandbox"));
    public static final AnonymousClass1Y7 A0K;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A06.A09("http/");
        A0E = r1;
        A08 = (AnonymousClass1Y7) r1.A09("fbtrace");
        AnonymousClass1Y7 r12 = A0E;
        A00 = (AnonymousClass1Y7) r12.A09("artillery");
        r12.A09("log_http_queue_events");
        r12.A09("carrier_id");
        A0B = (AnonymousClass1Y7) r12.A09("check_certs");
        A0A = (AnonymousClass1Y7) r12.A09("user_certs");
        A0C = (AnonymousClass1Y7) r12.A09("disable_fizz");
        A0D = (AnonymousClass1Y7) r12.A09("http_proxy");
        A07 = (AnonymousClass1Y7) r12.A09("empathy");
        A0G = (AnonymousClass1Y7) r12.A09("liger_trace_event");
        r12.A09("ssl_key_material_logger");
        A06 = (AnonymousClass1Y7) r12.A09("cdn_override");
        AnonymousClass1Y7 r13 = (AnonymousClass1Y7) C04350Ue.A06.A09("sandbox/");
        A0H = r13;
        AnonymousClass1Y7 r14 = (AnonymousClass1Y7) r13.A09("web/");
        A0F = r14;
        A0K = (AnonymousClass1Y7) r14.A09("server_tier");
        AnonymousClass1Y7 r15 = A0F;
        r15.A09("weinre");
        A0I = (AnonymousClass1Y7) r15.A09(TurboLoader.Locator.$const$string(88));
        AnonymousClass1Y7 r16 = (AnonymousClass1Y7) C04350Ue.A06.A09("fetch_alerts/");
        A09 = r16;
        r16.A09("fetch_thread_list");
        AnonymousClass1Y7 r17 = A09;
        r17.A09("fetch_more_threads");
        r17.A09("fetch_thread");
        r17.A09("fetch_multiple_threads");
        r17.A09("fetch_group_threads");
        r17.A09("fetch_more_messages");
        AnonymousClass1Y7 r18 = (AnonymousClass1Y7) ((AnonymousClass1Y7) A0H.A09("skywalker/")).A09("assimilator/");
        A03 = r18;
        A01 = (AnonymousClass1Y7) r18.A09("hostname");
        AnonymousClass1Y7 r0 = (AnonymousClass1Y7) C04350Ue.A06.A09("bladerunner/");
        A05 = r0;
        A04 = (AnonymousClass1Y7) r0.A09("sandbox");
        A05.A09("examples");
    }
}
