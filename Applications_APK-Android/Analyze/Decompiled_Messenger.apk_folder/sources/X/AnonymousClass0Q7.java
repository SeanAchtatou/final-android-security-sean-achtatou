package X;

import java.util.concurrent.Executor;

/* renamed from: X.0Q7  reason: invalid class name */
public final class AnonymousClass0Q7 {
    public final AnonymousClass0Q6 A00;
    public final Executor A01;

    public AnonymousClass0Q7(Executor executor, AnonymousClass0Q6 r2) {
        this.A01 = executor;
        this.A00 = r2;
    }
}
