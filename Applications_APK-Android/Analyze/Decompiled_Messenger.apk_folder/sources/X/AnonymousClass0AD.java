package X;

import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.SystemClock;
import com.facebook.rti.common.time.RealtimeSinceBootClock;
import io.card.payment.BuildConfig;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;

/* renamed from: X.0AD  reason: invalid class name */
public abstract class AnonymousClass0AD extends AnonymousClass0AE {
    public long A00;
    public AnonymousClass07g A01;
    public AnonymousClass0AW A02;
    public RealtimeSinceBootClock A03;
    public C01440Ag A04;
    public AnonymousClass0B7 A05;
    public C01570At A06;
    public C01470Aj A07;
    public AnonymousClass0BU A08;
    public AnonymousClass0AH A09;
    public AnonymousClass0AX A0A;
    public AnonymousClass089 A0B = AnonymousClass089.DISCONNECTED;
    public AtomicBoolean A0C = new AtomicBoolean(false);
    public final AnonymousClass0AG A0D = new AnonymousClass0AF(this);
    public volatile AnonymousClass0B6 A0E;

    public Looper A0B() {
        return null;
    }

    public AnonymousClass09P A0G() {
        return null;
    }

    public abstract AnonymousClass0AX A0H();

    public abstract Integer A0I();

    public String A0J() {
        return "N/A";
    }

    public void A0N() {
    }

    public void A0O() {
    }

    public void A0P() {
    }

    public void A0Q(int i) {
    }

    public void A0R(Intent intent, AnonymousClass08E r2) {
    }

    public void A0T(AnonymousClass0SB r1) {
    }

    public void A0U(AnonymousClass089 r1) {
    }

    public void A0V(C02020Cn r1) {
    }

    public void A0W(String str, long j, boolean z) {
    }

    public void A0X(String str, String str2, Throwable th) {
    }

    public void A0Y(String str, byte[] bArr, int i, long j, AnonymousClass0CU r6) {
    }

    public boolean A0a(Intent intent) {
        return true;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public static String A00(AnonymousClass0AD r4) {
        C04140Sd A062 = r4.A05.A06(r4.A09.A09(), false);
        try {
            return C04140Sd.A00(A062, A062.A00).toString(2);
        } catch (JSONException unused) {
            return BuildConfig.FLAVOR;
        }
    }

    private void A01(AnonymousClass0CE r4) {
        if (!this.A0C.getAndSet(false)) {
            C010708t.A0J("MqttPushService", "service/stop/inactive_connection");
            return;
        }
        A0P();
        this.A09.A0G();
        this.A09.A0K(r4);
        A02(this);
    }

    public static void A02(AnonymousClass0AD r5) {
        AnonymousClass089 r0;
        AnonymousClass089 A0A2 = r5.A09.A0A();
        if (A0A2 != null && A0A2 != (r0 = r5.A0B)) {
            r5.A01.BIo(AnonymousClass08S.A0S("[state_machine] ", r0.toString(), " -> ", A0A2.toString()));
            r5.A0B = A0A2;
            r5.A04.A01(A0A2.name());
            r5.A0U(A0A2);
        }
    }

    private void A03(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", str);
        hashMap.put("pid", String.valueOf(Process.myPid()));
        this.A01.BIq("life_cycle", hashMap);
    }

    public void A0C() {
        boolean z = false;
        if (this.A0A == null) {
            z = true;
        }
        AnonymousClass0A1.A02(z);
        this.A0A = A0H();
        A0M();
        A0L();
        this.A01.CCF(new C01640Ba(this));
        A03("doCreate");
        AnonymousClass0B6 r2 = this.A0E;
        String A0J = AnonymousClass08S.A0J(C01430Ae.A00(A0I()), ".SERVICE_CREATE");
        String A0J2 = A0J();
        C01660Bc r6 = C01660Bc.A00;
        r2.A06(A0J, A0J2, null, r6, r6, this.A0C.get(), -1, this.A06.A02(), this.A06.A04());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00fd, code lost:
        if ("Orca.PERSISTENT_KICK_SKIP_PING".equals(r1.getAction()) != false) goto L_0x00ff;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0D(android.content.Intent r21, int r22, int r23) {
        /*
            r20 = this;
            r1 = r21
            r12 = 0
            r13 = 0
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            if (r21 != 0) goto L_0x0162
            java.lang.String r0 = "intent"
            r4.put(r0, r12)
        L_0x0010:
            r0 = r20
            X.07g r3 = r0.A01
            java.lang.String r2 = "start_command"
            r3.BIq(r2, r4)
            boolean r2 = r0.A0a(r1)
            if (r2 != 0) goto L_0x0020
            r1 = 0
        L_0x0020:
            r7 = r0
            X.0AW r3 = r0.A02
            java.lang.Integer r2 = X.AnonymousClass07B.A04
            X.0B0 r4 = r3.AbP(r2)
            java.lang.String r3 = "DELIVERY_RETRY_INTERVAL"
            boolean r2 = r4.contains(r3)
            if (r2 == 0) goto L_0x003b
            r2 = 300(0x12c, float:4.2E-43)
            int r2 = r4.getInt(r3, r2)
            java.lang.Integer r13 = java.lang.Integer.valueOf(r2)
        L_0x003b:
            X.08E r8 = new X.08E
            r9 = 0
            r10 = 0
            r8.<init>(r9, r10, r12, r13)
            if (r1 == 0) goto L_0x015e
            if (r1 == 0) goto L_0x015b
            java.lang.String r3 = "caller"
            boolean r2 = r1.hasExtra(r3)
            if (r2 == 0) goto L_0x0055
            java.lang.String r2 = r1.getStringExtra(r3)
            r8.A03 = r2
        L_0x0055:
            java.lang.String r4 = "EXPIRED_SESSION"
            boolean r2 = r1.hasExtra(r4)
            if (r2 == 0) goto L_0x0063
            long r2 = r1.getLongExtra(r4, r10)
            r8.A00 = r2
        L_0x0063:
            java.lang.String r3 = "DELIVERY_RETRY_INTERVAL"
            boolean r2 = r1.hasExtra(r3)
            if (r2 == 0) goto L_0x015b
            r2 = 300(0x12c, float:4.2E-43)
            int r2 = r1.getIntExtra(r3, r2)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.Integer r2 = r8.A02
            boolean r2 = r3.equals(r2)
            if (r2 != 0) goto L_0x015b
            r8.A02 = r3
            r2 = 1
        L_0x0080:
            if (r2 == 0) goto L_0x00a3
            X.0AX r2 = r0.A0A
            X.0AW r3 = r2.A03
            java.lang.Integer r2 = X.AnonymousClass07B.A04
            X.0B0 r2 = r3.AbP(r2)
            X.0DD r4 = r2.AY8()
            java.lang.Integer r2 = r8.A02
            if (r2 == 0) goto L_0x0158
            int r3 = r2.intValue()
            java.lang.String r2 = "DELIVERY_RETRY_INTERVAL"
            r4.Bz7(r2, r3)
            r2 = 1
        L_0x009e:
            if (r2 == 0) goto L_0x00a3
            r4.commit()
        L_0x00a3:
            java.lang.String r6 = r1.getAction()
        L_0x00a7:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r22)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r23)
            X.0B6 r9 = r0.A0E
            java.lang.Integer r2 = r0.A0I()
            java.lang.String r3 = X.C01430Ae.A00(r2)
            r2 = 46
            java.lang.String r10 = X.AnonymousClass08S.A07(r3, r2, r6)
            java.lang.String r11 = r0.A0J()
            java.lang.String r12 = r8.A03
            X.0Aq r13 = X.C01540Aq.A00(r5)
            X.0Aq r14 = X.C01540Aq.A00(r4)
            java.util.concurrent.atomic.AtomicBoolean r2 = r0.A0C
            boolean r15 = r2.get()
            r16 = -1
            X.0At r2 = r0.A06
            long r17 = r2.A02()
            X.0At r2 = r0.A06
            android.net.NetworkInfo r19 = r2.A04()
            r9.A06(r10, r11, r12, r13, r14, r15, r16, r17, r19)
            if (r1 == 0) goto L_0x00ff
            java.lang.String r3 = r1.getAction()
            java.lang.String r2 = "Orca.PERSISTENT_KICK"
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x00ff
            java.lang.String r3 = r1.getAction()
            java.lang.String r2 = "Orca.PERSISTENT_KICK_SKIP_PING"
            boolean r3 = r2.equals(r3)
            r2 = 0
            if (r3 == 0) goto L_0x0100
        L_0x00ff:
            r2 = 1
        L_0x0100:
            if (r2 == 0) goto L_0x0183
            java.util.concurrent.atomic.AtomicBoolean r2 = r0.A0C
            boolean r2 = r2.get()
            if (r2 != 0) goto L_0x0115
            if (r1 != 0) goto L_0x0112
            X.0Bo r0 = X.C01770Bo.A0D
        L_0x010e:
            r7.A0S(r0, r8)
        L_0x0111:
            return
        L_0x0112:
            X.0Bo r0 = X.C01770Bo.A0C
            goto L_0x010e
        L_0x0115:
            boolean r2 = r0.A0Z()
            if (r2 == 0) goto L_0x017b
            X.0AH r2 = r0.A09
            boolean r2 = r2.A0Q()
            if (r2 == 0) goto L_0x017b
            if (r1 == 0) goto L_0x0111
            java.lang.String r2 = r1.getAction()
            java.lang.String r1 = "Orca.PERSISTENT_KICK"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0111
            X.0AH r7 = r0.A09
            java.lang.String r6 = r8.A03
            X.07h r0 = r7.A0B
            X.07i r0 = r0.A03()
            int r3 = r0.A0D
            if (r3 < 0) goto L_0x0111
            X.0C8 r2 = r7.A0n
            if (r2 == 0) goto L_0x0111
            long r4 = android.os.SystemClock.elapsedRealtime()
            monitor-enter(r2)
            long r0 = r2.A0S     // Catch:{ all -> 0x0178 }
            monitor-exit(r2)
            long r4 = r4 - r0
            long r2 = (long) r3
            r0 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r0
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0111
            r7.A0N(r6)
            return
        L_0x0158:
            r2 = 0
            goto L_0x009e
        L_0x015b:
            r2 = 0
            goto L_0x0080
        L_0x015e:
            java.lang.String r6 = "NULL"
            goto L_0x00a7
        L_0x0162:
            java.lang.String r2 = r1.getAction()
            java.lang.String r0 = "action"
            r4.put(r0, r2)
            java.lang.String r2 = "caller"
            java.lang.String r0 = r1.getStringExtra(r2)
            if (r0 == 0) goto L_0x0010
            r4.put(r2, r0)
            goto L_0x0010
        L_0x0178:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x017b:
            X.0Bo r1 = X.C01770Bo.A0C
            X.0AH r0 = r0.A09
            r0.A0J(r1)
            return
        L_0x0183:
            java.lang.String r3 = r1.getAction()
            java.lang.String r2 = "Orca.STOP"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0198
            X.0CE r1 = X.AnonymousClass0CE.A0L
            r0.A01(r1)
            r0.stopSelf()
            return
        L_0x0198:
            java.lang.String r2 = "Orca.START"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x01a6
            X.0Bo r1 = X.C01770Bo.A0E
            r0.A0S(r1, r8)
            return
        L_0x01a6:
            java.lang.String r2 = "Orca.EXPIRE_CONNECTION"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x01d1
            X.0AH r6 = r0.A09
            long r4 = r8.A00
            X.0C8 r3 = r6.A0n
            X.0B7 r1 = r6.A0A
            X.0Bo r0 = X.C01770Bo.A07
            r1.A0E = r0
            if (r3 == 0) goto L_0x01cd
            long r1 = r3.A0W
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x01cd
            X.0FG r1 = X.AnonymousClass0FG.STALED_CONNECTION
            X.0CE r0 = X.AnonymousClass0CE.A04
            r6.A0L(r3, r0, r1)
            X.AnonymousClass0AH.A02(r6)
            return
        L_0x01cd:
            r6.A0D()
            return
        L_0x01d1:
            r0.A0R(r1, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0AD.A0D(android.content.Intent, int, int):void");
    }

    public void A0E() {
        AnonymousClass0B6 r2 = this.A0E;
        String A0J = AnonymousClass08S.A0J(C01430Ae.A00(A0I()), ".SERVICE_DESTROY");
        String A0J2 = A0J();
        C01660Bc r6 = C01660Bc.A00;
        r2.A06(A0J, A0J2, null, r6, r6, this.A0C.get(), -1, this.A06.A02(), this.A06.A04());
        A03("doDestroy");
        this.A01.CCF(null);
        A0K();
    }

    public void A0F(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String valueOf;
        try {
            printWriter.println("[ MqttPushService ]");
            printWriter.println(AnonymousClass08S.A0J("persistence=", A0J()));
            long j = this.A09.A03;
            if (j > 0) {
                valueOf = new Date(j).toString();
            } else {
                valueOf = String.valueOf(j);
            }
            printWriter.println(AnonymousClass08S.A0J("networkChangedTime=", valueOf));
            printWriter.println("subscribedTopics=" + this.A09.A0C());
            if (!(!this.A0A.A06.A02)) {
                this.A09.A0M(printWriter);
                printWriter.println("[ MqttHealthStats ]");
                printWriter.println(A00(this));
            }
        } catch (Exception unused) {
        }
    }

    public void A0K() {
        if (this.A0C.get()) {
            A01(AnonymousClass0CE.A0K);
        }
        AnonymousClass0AH r1 = this.A09;
        if (r1 != null) {
            r1.A0K(AnonymousClass0CE.A0K);
        }
        AnonymousClass0AX r4 = this.A0A;
        if (r4 != null && !r4.A0U) {
            r4.A0U = true;
            AnonymousClass0BW r3 = r4.A0L;
            if (r3 != null) {
                synchronized (r3) {
                    r3.A00();
                    if (r3.A01) {
                        boolean z = false;
                        if (!r3.A08.A07(r3.A06, r3.A05)) {
                            z = true;
                        }
                        r3.A01 = z;
                    }
                }
            }
            C01570At r32 = r4.A0H;
            if (r32 != null) {
                synchronized (r32) {
                    try {
                        r32.A05.unregisterReceiver(r32.A04);
                    } catch (IllegalArgumentException e) {
                        C010708t.A0S("MqttNetworkManager", e, "Failed to unregister broadcast receiver");
                    }
                }
            }
            C01530Ap r0 = r4.A0F;
            if (r0 != null) {
                r0.shutdown();
            }
            AnonymousClass0BO r33 = r4.A0K;
            if (r33 != null) {
                synchronized (r33) {
                    r33.A03();
                    if (r33.A0L != null) {
                        r33.A0E.A07(r33.A0C, r33.A0A);
                        r33.A0E.A07(r33.A0C, r33.A0B);
                        r33.A0E.A07(r33.A0C, r33.A09);
                    }
                }
                return;
            }
            return;
        }
        return;
    }

    public void A0L() {
        AnonymousClass0B7 r3 = this.A05;
        C01650Bb r0 = C01650Bb.ServiceCreatedTimestamp;
        AnonymousClass0B7.A04(r3, r0).set(SystemClock.elapsedRealtime());
    }

    public void A0M() {
        AnonymousClass0AX r0 = this.A0A;
        AnonymousClass0AH r9 = r0.A0N;
        C01570At r8 = r0.A0H;
        AnonymousClass0BT r7 = r0.A0J;
        RealtimeSinceBootClock realtimeSinceBootClock = r0.A04;
        AnonymousClass0B6 r5 = r0.A0A;
        AnonymousClass0B7 r4 = r0.A0C;
        C01470Aj r3 = r0.A0I;
        C01440Ag r2 = r0.A0B;
        AnonymousClass07g r1 = r0.A02;
        AnonymousClass0AW r02 = r0.A03;
        this.A09 = r9;
        this.A06 = r8;
        this.A08 = r7;
        this.A03 = realtimeSinceBootClock;
        this.A0E = r5;
        this.A05 = r4;
        this.A07 = r3;
        this.A04 = r2;
        this.A01 = r1;
        this.A02 = r02;
    }

    public void A0S(C01770Bo r5, AnonymousClass08E r6) {
        Integer num;
        if (!this.A0C.getAndSet(true)) {
            if (!(r6 == null || (num = r6.A02) == null)) {
                A0Q(num.intValue());
            }
            AnonymousClass0B7 r0 = this.A05;
            String name = r5.name();
            AnonymousClass0B9 r3 = r0.A00;
            if (r3.A05 == null) {
                r3.A05 = name;
                r3.A04.set(SystemClock.elapsedRealtime());
                r3.A02.set(SystemClock.elapsedRealtime());
            }
            A0O();
            this.A09.A0F();
        }
        this.A09.A0J(r5);
    }

    public boolean A0Z() {
        if (!this.A0C.get()) {
            this.A01.BIo("MqttPushService/not_started");
            return false;
        }
        HashMap hashMap = new HashMap();
        if (this.A08.CDi(hashMap)) {
            return true;
        }
        this.A01.BIq("MqttPushService/should_not_connect", hashMap);
        return false;
    }

    public void onDestroy() {
        int A042 = C000700l.A04(-729803162);
        if (this.A0E != null) {
            AnonymousClass0B6 r3 = this.A0E;
            String A0J = AnonymousClass08S.A0J(C01430Ae.A00(A0I()), ".SERVICE_ON_DESTROY");
            String A0J2 = A0J();
            C01660Bc r7 = C01660Bc.A00;
            r3.A06(A0J, A0J2, null, r7, r7, this.A0C.get(), -1, 0, null);
        }
        super.onDestroy();
        C000700l.A0A(-392270703, A042);
    }
}
