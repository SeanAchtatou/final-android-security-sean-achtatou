package X;

import com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent;

/* renamed from: X.15q  reason: invalid class name and case insensitive filesystem */
public final class C188715q implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent$2";
    public final /* synthetic */ MontageOmnistoreComponent A00;

    public C188715q(MontageOmnistoreComponent montageOmnistoreComponent) {
        this.A00 = montageOmnistoreComponent;
    }

    public void run() {
        C005505z.A03("MontageOmnistoreComponent:tryToGetMontageCollection", -1585559527);
        try {
            if (MontageOmnistoreComponent.A02(this.A00)) {
                ((C20921Ei) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BMw, this.A00.A00)).A08("omni_try_to_get_montage_collection");
                int i = AnonymousClass1Y3.ASv;
                MontageOmnistoreComponent montageOmnistoreComponent = this.A00;
                ((C24341Tf) AnonymousClass1XX.A02(4, i, montageOmnistoreComponent.A00)).A02(montageOmnistoreComponent);
            }
            this.A00.A04.set(false);
        } finally {
            C005505z.A00(-997624236);
        }
    }
}
