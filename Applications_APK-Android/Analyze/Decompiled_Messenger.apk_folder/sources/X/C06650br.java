package X;

import android.content.Context;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0br  reason: invalid class name and case insensitive filesystem */
public final class C06650br extends C06660bs implements AnonymousClass1YQ, C06670bt {
    private static volatile C06650br A02;
    public final C25051Yd A00;
    private final AnonymousClass1YI A01;

    private void A01(String str, String str2, String str3) {
        synchronized (this) {
            this.A01 = AnonymousClass09U.A00(str, this.A03);
        }
        synchronized (this) {
            this.A02 = AnonymousClass1ZT.A00(str2);
        }
        synchronized (this) {
            this.A00 = C24461Ts.A00(str3);
        }
        Context context = this.A03;
        AnonymousClass09U[] AqR = AqR();
        AnonymousClass1ZT[] AjO = AjO();
        Map Ajo = Ajo();
        synchronized (C006506l.class) {
            C006506l.A00 = new AnonymousClass08O(AqR, AjO, Ajo);
            context.getSharedPreferences("com.facebook.secure.switchoff", 0).edit().putString("last_criteria", str).putString("last_custom_config", str2).putString("last_deeplink_config", str3).apply();
        }
    }

    public String getSimpleName() {
        return "IntentSwitchOffMobileConfigDI";
    }

    public static final C06650br A00(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (C06650br.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = new C06650br(applicationInjector, AnonymousClass1YA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public int Ai5() {
        return AnonymousClass1Y3.A3M;
    }

    public void BTE(int i) {
        C25051Yd r3 = this.A00;
        AnonymousClass0XE r0 = AnonymousClass0XE.A05;
        A01(r3.B4J(846177276854513L, r0), this.A00.B4J(846177276788976L, r0), this.A00.B4J(846164391952619L, r0));
    }

    public boolean CDm() {
        return this.A00.Aem(2306126223652161782L);
    }

    public boolean CDp() {
        return this.A01.AbO(AnonymousClass1Y3.A2u, false);
    }

    private C06650br(AnonymousClass1XY r2, Context context) {
        super(context);
        this.A01 = AnonymousClass0WA.A00(r2);
        this.A00 = AnonymousClass0WT.A00(r2);
    }

    public void init() {
        int A03 = C000700l.A03(2061442975);
        A01(A02(), A03(), A04());
        C000700l.A09(-812329539, A03);
    }
}
