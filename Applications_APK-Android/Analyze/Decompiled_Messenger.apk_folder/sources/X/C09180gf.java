package X;

import android.os.Build;

/* renamed from: X.0gf  reason: invalid class name and case insensitive filesystem */
public abstract class C09180gf {
    public long A00;
    public long A01;
    public final C09210gj A02;
    public final AnonymousClass0Td[] A03;

    public int[] A03(AnonymousClass0Td r2) {
        if (this instanceof C09220gk) {
            AnonymousClass0Ti AsW = r2.AsW();
            if (AsW != null) {
                return AsW.A01;
            }
            return null;
        } else if (!(this instanceof C26601bi)) {
            AnonymousClass0Ti AsW2 = r2.AsW();
            if (AsW2 != null) {
                return AsW2.A02;
            }
            return null;
        } else {
            AnonymousClass0Ti AsW3 = r2.AsW();
            if (AsW3 != null) {
                return AsW3.A00;
            }
            return null;
        }
    }

    public C09180gf(AnonymousClass0Td[] r19, C39851zi r20) {
        int length;
        AnonymousClass0Td[] r12 = r19;
        this.A03 = r12;
        if (r19 == null || (length = r12.length) == 0) {
            this.A02 = null;
            this.A00 = 0;
            this.A01 = 0;
        } else if (length <= 64) {
            if (length > 58) {
                C39851zi r4 = r20;
                if (r20 != null) {
                    r4.A01.A00.A00.softReport("qpl", "error", new IllegalArgumentException(AnonymousClass08S.A0A("We are reaching limit of listeners: ", length, " registered")));
                }
            }
            if (Build.VERSION.SDK_INT >= 18) {
                this.A02 = A01(30);
            } else {
                this.A02 = new C37081ue(30);
            }
            C09210gj r10 = this.A02;
            long j = 0;
            long j2 = 0;
            long j3 = 1;
            int i = 0;
            long j4 = 0;
            while (i < length) {
                int[] A032 = A03(r19[i]);
                if (A032 != null) {
                    int length2 = A032.length;
                    int i2 = 0;
                    while (i2 < length2) {
                        int i3 = A032[i2];
                        if (i3 == -1) {
                            j4 |= j3;
                        } else if (i3 != -2) {
                            r10.A01(i3, r10.A00(i3, j) | j3);
                            i2++;
                            j = 0;
                        }
                        j2 |= j3;
                        i2++;
                        j = 0;
                    }
                }
                i++;
                j3 <<= 1;
            }
            this.A00 = j4;
            this.A01 = j2;
        } else {
            throw new UnsupportedOperationException("We support up to 64 listeners");
        }
    }

    public static C09210gj A01(int i) {
        return new C09200gi(i);
    }

    public long A02(int i) {
        C09210gj r1 = this.A02;
        if (r1 == null || this.A03 == null) {
            return 0;
        }
        return r1.A00(i, 0) | this.A00;
    }
}
