package X;

/* renamed from: X.0dn  reason: invalid class name and case insensitive filesystem */
public abstract class C07580dn {
    public final int _hashCode;
    public final String _name;

    public abstract boolean equals(int i);

    public abstract boolean equals(int i, int i2);

    public boolean equals(Object obj) {
        return obj == this;
    }

    public abstract boolean equals(int[] iArr, int i);

    public final int hashCode() {
        return this._hashCode;
    }

    public String toString() {
        return this._name;
    }

    public C07580dn(String str, int i) {
        this._name = str;
        this._hashCode = i;
    }
}
