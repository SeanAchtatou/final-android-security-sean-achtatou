package X;

import android.content.res.ColorStateList;
import android.util.SparseIntArray;

/* renamed from: X.0sK  reason: invalid class name and case insensitive filesystem */
public final class C13940sK {
    public static final int[] A01 = {16842919, -16842910, 16842910};
    public SparseIntArray A00 = new SparseIntArray();

    public static ColorStateList A00(ColorStateList colorStateList, int i) {
        C13940sK r3 = new C13940sK();
        r3.A02(C15970wH.A02(AnonymousClass1KA.A00(colorStateList), i));
        r3.A03(C15970wH.A02(AnonymousClass1KA.A01(colorStateList), i));
        int[] iArr = AnonymousClass1KA.A00;
        int i2 = 0;
        if (colorStateList != null) {
            i2 = colorStateList.getColorForState(iArr, 0);
        }
        r3.A00.put(-16842910, C15970wH.A02(i2, i));
        return r3.A01();
    }

    public ColorStateList A01() {
        if (this.A00.size() <= A01.length) {
            int size = this.A00.size();
            int[][] iArr = new int[size][];
            int size2 = this.A00.size();
            int[] iArr2 = new int[size2];
            SparseIntArray sparseIntArray = this.A00;
            if (size < sparseIntArray.size() || size2 < sparseIntArray.size() || size != size2) {
                throw new IllegalArgumentException("Provided states or color array not large enough or size of states and colors not same.");
            }
            int i = 0;
            for (int i2 : A01) {
                boolean z = false;
                if (sparseIntArray.indexOfKey(i2) >= 0) {
                    z = true;
                }
                if (z) {
                    iArr[i] = new int[]{i2};
                    iArr2[i] = sparseIntArray.get(i2);
                    i++;
                }
            }
            return new ColorStateList(iArr, iArr2);
        }
        throw new IllegalStateException("More states have been set than specified by build order.");
    }

    public void A02(int i) {
        this.A00.put(16842910, i);
    }

    public void A03(int i) {
        this.A00.put(16842919, i);
    }
}
