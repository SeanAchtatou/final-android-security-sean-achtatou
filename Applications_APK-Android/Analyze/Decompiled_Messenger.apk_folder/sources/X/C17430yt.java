package X;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.0yt  reason: invalid class name and case insensitive filesystem */
public final class C17430yt {
    public static Account A00(Context context, String str) {
        Account[] A01 = A01(context, str);
        int length = A01.length;
        if (length > 1) {
            C010708t.A0O("AccountManagerUtils", "Only a single account of type %s is expected, but %d account found", str, Integer.valueOf(length));
        }
        if (length == 0) {
            return null;
        }
        return A01[0];
    }

    public static Account[] A01(Context context, String str) {
        int checkPermission = context.checkPermission(TurboLoader.Locator.$const$string(6), Process.myPid(), Process.myUid());
        if (Build.VERSION.SDK_INT > 22 || checkPermission == 0) {
            return AccountManager.get(context).getAccountsByType(str);
        }
        return new Account[0];
    }
}
