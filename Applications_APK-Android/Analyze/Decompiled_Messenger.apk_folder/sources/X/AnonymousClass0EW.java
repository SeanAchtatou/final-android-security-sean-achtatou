package X;

import java.io.InputStream;

/* renamed from: X.0EW  reason: invalid class name */
public final class AnonymousClass0EW extends AnonymousClass0EM {
    private int A00;
    public final /* synthetic */ AnonymousClass0ES A01;

    public AnonymousClass0EW(AnonymousClass0ES r1) {
        this.A01 = r1;
    }

    public C03930Qq A00() {
        this.A01.A03();
        AnonymousClass0ES r3 = this.A01;
        AnonymousClass0EU[] r2 = r3.A00;
        int i = this.A00;
        this.A00 = i + 1;
        AnonymousClass0EU r22 = r2[i];
        InputStream inputStream = r3.A01.getInputStream(r22.A01);
        try {
            return new C03930Qq(r22, inputStream);
        } catch (Throwable th) {
            if (inputStream != null) {
                inputStream.close();
            }
            throw th;
        }
    }

    public boolean A01() {
        this.A01.A03();
        if (this.A00 < this.A01.A00.length) {
            return true;
        }
        return false;
    }
}
