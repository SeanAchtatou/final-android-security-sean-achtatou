package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.facebook.nobreak.CrashLoop$LastState;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.0Ox  reason: invalid class name and case insensitive filesystem */
public final class C03650Ox {
    public final Context A00;
    public final PackageManager A01;
    private final ComponentName A02 = new ComponentName(this.A00, CrashLoop$LastState.class);

    private List A00(ComponentInfo[] componentInfoArr) {
        boolean z;
        LinkedList linkedList = new LinkedList();
        if (componentInfoArr != null) {
            String A0P = AnonymousClass08S.A0P(this.A00.getPackageName(), ":", "nodex");
            for (ComponentInfo componentInfo : componentInfoArr) {
                Bundle bundle = componentInfo.metaData;
                if (bundle != null) {
                    z = bundle.getBoolean("crash.loop.exclude", false);
                } else {
                    z = false;
                }
                if (A0P.equals(componentInfo.processName)) {
                    z = true;
                }
                if (!z) {
                    linkedList.add(componentInfo);
                }
            }
        }
        return linkedList;
    }

    public static void A01(C03650Ox r5, int i) {
        ArrayList<ComponentInfo> arrayList = new ArrayList<>();
        arrayList.addAll(r5.A00(r5.A01.getPackageInfo(r5.A00.getPackageName(), 642).receivers));
        arrayList.addAll(r5.A00(r5.A01.getPackageInfo(r5.A00.getPackageName(), 641).activities));
        arrayList.addAll(r5.A00(r5.A01.getPackageInfo(r5.A00.getPackageName(), (int) AnonymousClass1Y3.A5F).providers));
        arrayList.addAll(r5.A00(r5.A01.getPackageInfo(r5.A00.getPackageName(), 644).services));
        Collections.sort(arrayList, new C03640Ov());
        ArrayList<ComponentName> arrayList2 = new ArrayList<>();
        for (ComponentInfo componentInfo : arrayList) {
            arrayList2.add(new ComponentName(componentInfo.packageName, componentInfo.name));
        }
        for (ComponentName componentName : arrayList2) {
            componentName.getClassName();
            r5.A01.setComponentEnabledSetting(componentName, i, 1);
        }
        r5.A01.setComponentEnabledSetting(r5.A02, i, 1);
    }

    public C03650Ox(Context context) {
        this.A00 = context;
        this.A01 = context.getPackageManager();
    }
}
