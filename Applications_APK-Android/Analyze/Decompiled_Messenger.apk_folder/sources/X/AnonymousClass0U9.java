package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.0U9  reason: invalid class name */
public final class AnonymousClass0U9 {
    public final FbSharedPreferences A00;

    public static final AnonymousClass0U9 A00(AnonymousClass1XY r1) {
        return new AnonymousClass0U9(r1);
    }

    public AnonymousClass0U9(AnonymousClass1XY r2) {
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }
}
