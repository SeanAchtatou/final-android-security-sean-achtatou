package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Ph  reason: invalid class name and case insensitive filesystem */
public final class C23381Ph {
    public static final Class A01 = C23381Ph.class;
    public Map A00 = new HashMap();

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0048, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.AnonymousClass1NY A00(X.C23601Qd r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            X.C05520Zg.A02(r7)     // Catch:{ all -> 0x0049 }
            java.util.Map r0 = r6.A00     // Catch:{ all -> 0x0049 }
            java.lang.Object r5 = r0.get(r7)     // Catch:{ all -> 0x0049 }
            X.1NY r5 = (X.AnonymousClass1NY) r5     // Catch:{ all -> 0x0049 }
            if (r5 == 0) goto L_0x0046
            monitor-enter(r5)     // Catch:{ all -> 0x0049 }
            boolean r0 = X.AnonymousClass1NY.A07(r5)     // Catch:{ all -> 0x0043 }
            if (r0 != 0) goto L_0x003d
            java.util.Map r0 = r6.A00     // Catch:{ all -> 0x0043 }
            r0.remove(r7)     // Catch:{ all -> 0x0043 }
            java.lang.Class r4 = X.C23381Ph.A01     // Catch:{ all -> 0x0043 }
            java.lang.String r3 = "Found closed reference %d for key %s (%d)"
            int r0 = java.lang.System.identityHashCode(r5)     // Catch:{ all -> 0x0043 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0043 }
            java.lang.String r1 = r7.B7w()     // Catch:{ all -> 0x0043 }
            int r0 = java.lang.System.identityHashCode(r7)     // Catch:{ all -> 0x0043 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0043 }
            java.lang.Object[] r0 = new java.lang.Object[]{r2, r1, r0}     // Catch:{ all -> 0x0043 }
            X.AnonymousClass02w.A07(r4, r3, r0)     // Catch:{ all -> 0x0043 }
            r0 = 0
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            monitor-exit(r6)
            return r0
        L_0x003d:
            X.1NY r0 = X.AnonymousClass1NY.A03(r5)     // Catch:{ all -> 0x0043 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0047
        L_0x0043:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0046:
            r0 = r5
        L_0x0047:
            monitor-exit(r6)
            return r0
        L_0x0049:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C23381Ph.A00(X.1Qd):X.1NY");
    }

    public void A01() {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList(this.A00.values());
            this.A00.clear();
        }
        for (int i = 0; i < arrayList.size(); i++) {
            AnonymousClass1NY r0 = (AnonymousClass1NY) arrayList.get(i);
            if (r0 != null) {
                r0.close();
            }
        }
    }

    public synchronized void A03(C23601Qd r6, AnonymousClass1NY r7) {
        C05520Zg.A02(r6);
        C05520Zg.A02(r7);
        C05520Zg.A04(AnonymousClass1NY.A07(r7));
        AnonymousClass1NY r4 = (AnonymousClass1NY) this.A00.get(r6);
        if (r4 != null) {
            AnonymousClass1PS A002 = AnonymousClass1PS.A00(r4.A0A);
            AnonymousClass1PS A003 = AnonymousClass1PS.A00(r7.A0A);
            if (!(A002 == null || A003 == null)) {
                try {
                    if (A002.A0A() == A003.A0A()) {
                        this.A00.remove(r6);
                        AnonymousClass1PS.A05(A003);
                        AnonymousClass1PS.A05(A002);
                        AnonymousClass1NY.A04(r4);
                        synchronized (this) {
                            try {
                                this.A00.size();
                            } catch (Throwable th) {
                                th = th;
                                throw th;
                            }
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    AnonymousClass1PS.A05(A003);
                    AnonymousClass1PS.A05(A002);
                    AnonymousClass1NY.A04(r4);
                    throw th;
                }
            }
            AnonymousClass1PS.A05(A003);
            AnonymousClass1PS.A05(A002);
            AnonymousClass1NY.A04(r4);
        }
    }

    public void A02(C23601Qd r3) {
        AnonymousClass1NY r1;
        C05520Zg.A02(r3);
        synchronized (this) {
            r1 = (AnonymousClass1NY) this.A00.remove(r3);
        }
        if (r1 != null) {
            try {
                r1.A0C();
            } finally {
                r1.close();
            }
        }
    }
}
