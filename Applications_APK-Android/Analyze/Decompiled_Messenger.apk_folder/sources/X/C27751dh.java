package X;

import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1dh  reason: invalid class name and case insensitive filesystem */
public abstract class C27751dh {
    public CopyOnWriteArrayList A00 = new CopyOnWriteArrayList();
    public boolean A01;

    public void A00() {
        C13060qW r1 = ((C27741dg) this).A00;
        r1.A12(true);
        if (r1.A0N.A01) {
            r1.A14();
        } else {
            r1.A02.A00();
        }
    }

    public C27751dh(boolean z) {
        this.A01 = z;
    }
}
