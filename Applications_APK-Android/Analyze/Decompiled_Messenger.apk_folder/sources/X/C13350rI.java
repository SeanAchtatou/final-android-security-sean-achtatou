package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0rI  reason: invalid class name and case insensitive filesystem */
public final class C13350rI extends C28141eK {
    public static Intent A00(C13350rI r10, Intent intent, Context context, List list) {
        ArrayList<ComponentInfo> arrayList = new ArrayList<>(list.size());
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null) {
            r10.A00.C2T("SameKeyIntentScope", "Current app info is null.", null);
        } else {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ComponentInfo componentInfo = (ComponentInfo) it.next();
                ApplicationInfo applicationInfo2 = componentInfo.applicationInfo;
                if (applicationInfo2 == null) {
                    r10.A00.C2T("SameKeyIntentScope", "Target app info is null.", null);
                } else if (A01(r10, context, applicationInfo, applicationInfo2)) {
                    arrayList.add(componentInfo);
                } else if (r10.A0H()) {
                    r10.A00.C2T("SameKeyIntentScope", String.format("Different signature of the component but fail-open: current app=%s, target app=%s.", applicationInfo.packageName, applicationInfo2.packageName), null);
                    arrayList.add(componentInfo);
                } else {
                    r10.A00.C2T("SameKeyIntentScope", String.format("Different signature component blocked: current app=%s, target app=%s.", applicationInfo.packageName, applicationInfo2.packageName), null);
                }
            }
        }
        if (arrayList.isEmpty()) {
            r10.A00.C2T("SameKeyIntentScope", "No matching same-key components.", null);
            return null;
        } else if (r10.A02 && arrayList.size() > 1) {
            return C28141eK.A03(C28141eK.A08(arrayList, intent));
        } else {
            ComponentInfo componentInfo2 = (ComponentInfo) arrayList.get(0);
            if (arrayList.size() > 1) {
                for (ComponentInfo componentInfo3 : arrayList) {
                    if (!context.getPackageName().equals(componentInfo3.packageName)) {
                        componentInfo2 = componentInfo3;
                    }
                }
            }
            intent.setComponent(new ComponentName(componentInfo2.packageName, componentInfo2.name));
            return intent;
        }
    }

    public static boolean A01(C13350rI r4, Context context, ApplicationInfo applicationInfo, ApplicationInfo applicationInfo2) {
        try {
            return C28061eC.A04(context, applicationInfo.uid, applicationInfo2.uid);
        } catch (SecurityException e) {
            r4.A00.C2T("SameKeyIntentScope", AnonymousClass08S.A0J("Unexpected exception in verifying signature for: ", applicationInfo2.packageName), e);
            return r4.A0H();
        }
    }

    public C13350rI(C008807t r1, AnonymousClass04e r2, boolean z) {
        super(r1, r2, z);
    }
}
