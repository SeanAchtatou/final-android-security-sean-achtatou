package X;

import android.content.Context;
import android.graphics.Point;
import android.view.WindowManager;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tr  reason: invalid class name and case insensitive filesystem */
public final class C36821tr {
    private static volatile C36821tr A0B;
    public final WindowManager A00;
    private final Context A01;
    private final C17350yl A02;
    private final C25051Yd A03;
    private final C30301hp A04;
    public volatile Point A05;
    private volatile int A06;
    private volatile int A07;
    private volatile int A08;
    private volatile int A09;
    private volatile int A0A;

    public static int A02(Integer num) {
        switch (num.intValue()) {
            case 1:
                return AnonymousClass1Y3.A0y;
            case 2:
                return 280;
            case 3:
            case 4:
                return AnonymousClass1Y3.A2Y;
            case 5:
                return 112;
            default:
                return AnonymousClass1Y3.A1N;
        }
    }

    public synchronized int A0B() {
        return A07();
    }

    public synchronized int A0C() {
        Double valueOf;
        int A092 = A09();
        C17350yl r3 = this.A02;
        if (!((C16510xE) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADT, r3.A00)).A0B()) {
            valueOf = Double.valueOf(1.0d);
        } else {
            valueOf = Double.valueOf(((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r3.A00)).Aki(1126926413332547L));
        }
        if (valueOf.doubleValue() <= 0.0d) {
            return A092;
        }
        double d = (double) A092;
        double doubleValue = valueOf.doubleValue();
        Double.isNaN(d);
        return (int) (d * doubleValue);
    }

    public synchronized int A0D() {
        return A09();
    }

    public synchronized int A0E() {
        int A092 = A09();
        Double valueOf = Double.valueOf(((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, this.A02.A00)).Aki(1126926413463620L));
        if (valueOf.doubleValue() <= 0.0d) {
            return A092;
        }
        double d = (double) A092;
        double doubleValue = valueOf.doubleValue();
        Double.isNaN(d);
        return (int) (d * doubleValue);
    }

    private int A00(int i) {
        return Math.min(i, this.A01.getResources().getDisplayMetrics().widthPixels - C007106r.A00(this.A01, 120.0f));
    }

    public static int A01(C36821tr r2) {
        if (r2.A04.A00.A07("messenger_photo_size_limit")) {
            return AnonymousClass1Y3.A7l;
        }
        return (int) r2.A03.At0(566484711966402L);
    }

    public static final C36821tr A03(AnonymousClass1XY r4) {
        if (A0B == null) {
            synchronized (C36821tr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0B, r4);
                if (A002 != null) {
                    try {
                        A0B = new C36821tr(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0B;
    }

    public static String A04(int i, int i2) {
        return AnonymousClass08S.A0B(BuildConfig.FLAVOR, i, "x", i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0022, code lost:
        if (r1 > r0) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A05() {
        /*
            r5 = this;
            android.graphics.Point r0 = r5.A05
            if (r0 != 0) goto L_0x0038
            r4 = r5
            monitor-enter(r4)
            android.graphics.Point r0 = r5.A05     // Catch:{ all -> 0x0034 }
            if (r0 != 0) goto L_0x0037
            android.view.WindowManager r0 = r5.A00     // Catch:{ all -> 0x0034 }
            android.view.Display r3 = r0.getDefaultDisplay()     // Catch:{ all -> 0x0034 }
            android.graphics.Point r2 = new android.graphics.Point     // Catch:{ all -> 0x0034 }
            r2.<init>()     // Catch:{ all -> 0x0034 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0034 }
            r0 = 17
            if (r1 < r0) goto L_0x002a
            r3.getRealSize(r2)     // Catch:{ all -> 0x0034 }
            int r1 = r2.x     // Catch:{ all -> 0x0034 }
            int r0 = r2.y     // Catch:{ all -> 0x0034 }
            if (r1 <= r0) goto L_0x0027
        L_0x0024:
            r2.set(r0, r1)     // Catch:{ all -> 0x0034 }
        L_0x0027:
            r5.A05 = r2     // Catch:{ all -> 0x0034 }
            goto L_0x0037
        L_0x002a:
            r3.getSize(r2)     // Catch:{ all -> 0x0034 }
            int r1 = r2.x     // Catch:{ all -> 0x0034 }
            int r0 = r2.y     // Catch:{ all -> 0x0034 }
            if (r1 <= r0) goto L_0x0027
            goto L_0x0024
        L_0x0034:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0037:
            monitor-exit(r4)
        L_0x0038:
            android.graphics.Point r0 = r5.A05
            int r1 = r0.x
            android.graphics.Point r0 = r5.A05
            int r0 = r0.y
            int r1 = java.lang.Math.max(r1, r0)
            X.0yl r0 = r5.A02
            int r0 = r0.A02()
            if (r0 == 0) goto L_0x0057
            X.0yl r0 = r5.A02
            int r0 = r0.A02()
            int r0 = java.lang.Math.min(r1, r0)
            return r0
        L_0x0057:
            r0 = 960(0x3c0, float:1.345E-42)
            if (r1 <= r0) goto L_0x005d
            r0 = 2048(0x800, float:2.87E-42)
        L_0x005d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36821tr.A05():int");
    }

    public int A06() {
        if (this.A06 == 0) {
            this.A06 = C007106r.A00(this.A01, (float) A02(AnonymousClass07B.A0C));
        }
        return this.A06;
    }

    public int A07() {
        if (this.A07 == 0) {
            this.A07 = C007106r.A00(this.A01, (float) A02(AnonymousClass07B.A0Y));
            this.A07 = A00(this.A07);
        }
        return this.A07;
    }

    public int A08() {
        if (this.A08 == 0) {
            this.A08 = C007106r.A00(this.A01, (float) A02(AnonymousClass07B.A0N));
            this.A08 = A00(this.A08);
        }
        return this.A08;
    }

    public int A09() {
        if (this.A09 == 0) {
            this.A09 = C007106r.A00(this.A01, (float) A02(AnonymousClass07B.A01));
        }
        return this.A09;
    }

    public int A0A() {
        if (this.A0A == 0) {
            this.A0A = C007106r.A00(this.A01, (float) A02(AnonymousClass07B.A00));
        }
        return this.A0A;
    }

    private C36821tr(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass1YA.A00(r2);
        this.A04 = new C30301hp(r2);
        this.A03 = AnonymousClass0WT.A00(r2);
        this.A00 = C04490Ux.A0g(r2);
        this.A02 = C17350yl.A00(r2);
    }
}
