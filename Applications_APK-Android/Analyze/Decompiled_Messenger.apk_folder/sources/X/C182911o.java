package X;

import com.facebook.graphservice.tree.TreeBuilderJNI;
import com.google.common.collect.ImmutableList;

/* renamed from: X.11o  reason: invalid class name and case insensitive filesystem */
public abstract class C182911o extends TreeBuilderJNI {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(java.lang.String, java.lang.Iterable):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(int, java.lang.Iterable):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(java.lang.String, java.lang.Iterable):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(int, java.lang.Iterable):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setStringList(int, java.lang.Iterable):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(java.lang.String, java.lang.Iterable):com.facebook.graphservice.tree.TreeBuilderJNI */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(java.lang.String, java.lang.Iterable):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, com.google.common.collect.ImmutableList]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(int, java.lang.Iterable):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(java.lang.String, java.lang.Iterable):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(int, java.lang.Iterable):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setStringList(int, java.lang.Iterable):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setStringList(java.lang.String, java.lang.Iterable):com.facebook.graphservice.tree.TreeBuilderJNI */
    public final void A00(String str, ImmutableList immutableList) {
        String name;
        if (immutableList == null) {
            setStringList(str, (Iterable) null);
            return;
        }
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            Enum enumR = (Enum) it.next();
            if (enumR == null) {
                name = "UNSET_OR_UNRECOGNIZED_ENUM_VALUE";
            } else {
                name = enumR.name();
            }
            builder.add((Object) name);
        }
        setStringList(str, (Iterable) builder.build());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(int, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setString(int, java.lang.String):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setString(java.lang.String, java.lang.String):com.facebook.graphservice.tree.TreeBuilderJNI */
    public final void A01(String str, Enum enumR) {
        String name;
        if (enumR == null) {
            name = "UNSET_OR_UNRECOGNIZED_ENUM_VALUE";
        } else {
            name = enumR.name();
        }
        setString(str, name);
    }

    public C182911o(int i) {
        super(i);
    }
}
