package X;

import java.security.MessageDigest;

/* renamed from: X.0cj  reason: invalid class name and case insensitive filesystem */
public abstract class C07160cj {
    public C07180co A01() {
        if (this instanceof AnonymousClass1HT) {
            return new C33961oQ(((AnonymousClass1HT) this).seed);
        }
        C08060ec r3 = (C08060ec) this;
        if (r3.supportsClone) {
            try {
                return new C25431Zp((MessageDigest) r3.prototype.clone(), r3.bytes);
            } catch (CloneNotSupportedException unused) {
            }
        }
        return new C25431Zp(C08060ec.A00(r3.prototype.getAlgorithm()), r3.bytes);
    }
}
