package X;

import android.util.Patterns;
import com.facebook.user.model.User;
import com.google.common.base.Platform;
import java.util.Locale;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16b  reason: invalid class name and case insensitive filesystem */
public final class C189116b {
    private static final String A01;
    private static final String A02;
    private static final String A03;
    private static volatile C189116b A04;
    private final AnonymousClass0ZS A00;

    public static int A00(String str) {
        int i = 0;
        while (i < str.length()) {
            int codePointAt = str.codePointAt(i);
            if (Character.isLetterOrDigit(codePointAt)) {
                return codePointAt;
            }
            i += Character.charCount(codePointAt);
        }
        return str.codePointAt(0);
    }

    static {
        String language = Locale.JAPANESE.getLanguage();
        Locale locale = Locale.US;
        A02 = language.toLowerCase(locale);
        A03 = Locale.KOREAN.getLanguage().toLowerCase(locale);
        A01 = Locale.CHINESE.getLanguage().toLowerCase(locale);
    }

    public static final C189116b A01(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C189116b.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C189116b(AnonymousClass0ZS.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        if (r2.lastName == null) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02(com.facebook.user.model.Name r2) {
        /*
            java.lang.String r1 = r2.firstName
            r0 = 0
            if (r1 == 0) goto L_0x0006
            r0 = 1
        L_0x0006:
            if (r0 != 0) goto L_0x0021
            if (r1 == 0) goto L_0x000f
            java.lang.String r1 = r2.lastName
            r0 = 1
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = r2.A01()
            return r0
        L_0x0017:
            java.lang.String r1 = r2.displayName
            r0 = 0
            if (r1 == 0) goto L_0x001d
            r0 = 1
        L_0x001d:
            if (r0 != 0) goto L_0x0021
            r0 = 0
            return r0
        L_0x0021:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C189116b.A02(com.facebook.user.model.Name):java.lang.String");
    }

    public static String A03(User user) {
        String A022 = A02(user.A0L);
        if (!Platform.stringIsNullOrEmpty(A022)) {
            return A022;
        }
        if (!user.A0T.isEmpty()) {
            return user.A0E();
        }
        return null;
    }

    public static boolean A05(C189116b r1) {
        String language = r1.A00.A06().getLanguage();
        if (A02.equalsIgnoreCase(language) || A03.equalsIgnoreCase(language) || A01.equalsIgnoreCase(language)) {
            return true;
        }
        return false;
    }

    public String A06(User user) {
        String A09;
        if (user == null) {
            return null;
        }
        if (!A05(this) || (A09 = user.A09()) == null) {
            return A03(user);
        }
        return A09;
    }

    private C189116b(AnonymousClass0ZS r1) {
        this.A00 = r1;
    }

    public static String A04(String str) {
        if (!C06850cB.A0B(str)) {
            String trim = str.trim();
            if (!C06850cB.A0B(trim) && !Patterns.PHONE.matcher(trim).matches()) {
                StringBuilder sb = new StringBuilder(1);
                sb.appendCodePoint(A00(trim));
                return sb.toString();
            }
        }
        return null;
    }
}
