package X;

import com.google.common.collect.ImmutableBiMap;

/* renamed from: X.0NZ  reason: invalid class name */
public final class AnonymousClass0NZ {
    private static final ImmutableBiMap A00;
    private static final ImmutableBiMap A01;

    static {
        C1061655r r2 = new C1061655r();
        r2.A01(C02310Dz.A02, 0);
        r2.A01(C02310Dz.A05, 90);
        r2.A01(C02310Dz.A03, Integer.valueOf((int) AnonymousClass1Y3.A1Q));
        r2.A01(C02310Dz.A04, Integer.valueOf((int) AnonymousClass1Y3.A26));
        ImmutableBiMap A002 = r2.build();
        A00 = A002;
        A01 = A002.A00();
    }

    public static int A00(C02310Dz r1, int i) {
        Integer num = (Integer) A00.get(r1);
        if (num != null) {
            return num.intValue();
        }
        return i;
    }

    public static C02310Dz A01(int i, C02310Dz r3) {
        C02310Dz r0 = (C02310Dz) A01.get(Integer.valueOf(i));
        if (r0 == null) {
            return r3;
        }
        return r0;
    }
}
