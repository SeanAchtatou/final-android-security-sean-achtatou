package X;

import android.view.View;

/* renamed from: X.19Y  reason: invalid class name */
public final class AnonymousClass19Y implements AnonymousClass19Z {
    public final /* synthetic */ AnonymousClass19T A00;

    public AnonymousClass19Y(AnonymousClass19T r1) {
        this.A00 = r1;
    }

    public View Ah1(int i) {
        return this.A00.A0u(i);
    }

    public int AxE() {
        AnonymousClass19T r0 = this.A00;
        return r0.A04 - r0.A0g();
    }

    public int AxG() {
        return this.A00.A0f();
    }

    public int Ah5(View view) {
        return this.A00.A0k(view) + ((AnonymousClass1R7) view.getLayoutParams()).rightMargin;
    }

    public int Ah8(View view) {
        return this.A00.A0j(view) - ((AnonymousClass1R7) view.getLayoutParams()).leftMargin;
    }
}
