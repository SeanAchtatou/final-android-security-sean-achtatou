package X;

import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.user.model.UserKey;

/* renamed from: X.1fR  reason: invalid class name and case insensitive filesystem */
public final class C28831fR {
    public int A00 = -1;
    public long A01;
    public long A02;
    public long A03;
    public ParticipantInfo A04;
    public AnonymousClass114 A05 = AnonymousClass114.NON_ADMIN;
    public UserKey A06;
    public String A07;
    public boolean A08 = true;
    public boolean A09;

    public void A00(ThreadParticipant threadParticipant) {
        this.A04 = threadParticipant.A04;
        this.A02 = threadParticipant.A02;
        this.A03 = threadParticipant.A03;
        this.A07 = threadParticipant.A07;
        this.A01 = threadParticipant.A01;
        this.A09 = threadParticipant.A09;
        this.A05 = threadParticipant.A05;
        this.A08 = threadParticipant.A08;
        this.A06 = threadParticipant.A06;
        this.A00 = threadParticipant.A00;
    }
}
