package X;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeId;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;
import com.fasterxml.jackson.databind.annotation.JsonTypeResolver;
import com.fasterxml.jackson.databind.annotation.JsonValueInstantiator;
import com.fasterxml.jackson.databind.cfg.PackageVersion;
import com.fasterxml.jackson.databind.jsontype.impl.StdTypeResolverBuilder;
import com.fasterxml.jackson.databind.ser.std.RawSerializer;
import io.card.payment.BuildConfig;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0jb  reason: invalid class name and case insensitive filesystem */
public final class C10130jb extends C10140jc implements Serializable {
    private static final long serialVersionUID = 1;

    private CXQ _findTypeResolver(C10470kA r6, C10080jW r7, C10030jR r8) {
        CXQ stdTypeResolverBuilder;
        JsonTypeInfo jsonTypeInfo = (JsonTypeInfo) r7.getAnnotation(JsonTypeInfo.class);
        JsonTypeResolver jsonTypeResolver = (JsonTypeResolver) r7.getAnnotation(JsonTypeResolver.class);
        CXX cxx = null;
        if (jsonTypeResolver != null) {
            if (jsonTypeInfo != null) {
                Class value = jsonTypeResolver.value();
                CXG cxg = r6._base._handlerInstantiator;
                if (cxg == null || (stdTypeResolverBuilder = cxg.typeResolverBuilderInstance(r6, r7, value)) == null) {
                    stdTypeResolverBuilder = (CXQ) C29081fq.createInstance(value, r6.canOverrideAccessModifiers());
                }
            }
            return null;
        }
        if (jsonTypeInfo != null) {
            if (jsonTypeInfo.use() == BlQ.NONE) {
                return StdTypeResolverBuilder.noTypeInfoBuilder();
            }
            stdTypeResolverBuilder = new StdTypeResolverBuilder();
        }
        return null;
        JsonTypeIdResolver jsonTypeIdResolver = (JsonTypeIdResolver) r7.getAnnotation(JsonTypeIdResolver.class);
        if (jsonTypeIdResolver != null) {
            Class value2 = jsonTypeIdResolver.value();
            CXG cxg2 = r6._base._handlerInstantiator;
            if (cxg2 == null || (cxx = cxg2.typeIdResolverInstance(r6, r7, value2)) == null) {
                cxx = (CXX) C29081fq.createInstance(value2, r6.canOverrideAccessModifiers());
            }
        }
        if (cxx != null) {
            cxx.init(r8);
        }
        stdTypeResolverBuilder.init(jsonTypeInfo.use(), cxx);
        CXW include = jsonTypeInfo.include();
        if (include == CXW.EXTERNAL_PROPERTY && (r7 instanceof C10070jV)) {
            include = CXW.PROPERTY;
        }
        stdTypeResolverBuilder.inclusion(include);
        stdTypeResolverBuilder.typeProperty(jsonTypeInfo.property());
        Class<C30361Eum> defaultImpl = jsonTypeInfo.defaultImpl();
        if (defaultImpl != C30361Eum.class) {
            stdTypeResolverBuilder.defaultImpl(defaultImpl);
        }
        stdTypeResolverBuilder.typeIdVisibility(jsonTypeInfo.visible());
        return stdTypeResolverBuilder;
    }

    public C10160je findAutoDetectVisibility(C10070jV r2, C10160je r3) {
        JsonAutoDetect jsonAutoDetect = (JsonAutoDetect) r2.getAnnotation(JsonAutoDetect.class);
        if (jsonAutoDetect != null) {
            return r3.with(jsonAutoDetect);
        }
        return r3;
    }

    public /* bridge */ /* synthetic */ Object findContentDeserializer(C10080jW r3) {
        Class<JsonDeserializer.None> contentUsing;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (contentUsing = jsonDeserialize.contentUsing()) == JsonDeserializer.None.class) {
            return null;
        }
        return contentUsing;
    }

    public /* bridge */ /* synthetic */ Object findContentSerializer(C10080jW r3) {
        Class<JsonSerializer.None> contentUsing;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || (contentUsing = jsonSerialize.contentUsing()) == JsonSerializer.None.class) {
            return null;
        }
        return contentUsing;
    }

    public Object findDeserializationContentConverter(C183512m r3) {
        Class<C422328u> contentConverter;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (contentConverter = jsonDeserialize.contentConverter()) == C422328u.class) {
            return null;
        }
        return contentConverter;
    }

    public Class findDeserializationContentType(C10080jW r3, C10030jR r4) {
        Class<C422228t> contentAs;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (contentAs = jsonDeserialize.contentAs()) == C422228t.class) {
            return null;
        }
        return contentAs;
    }

    public Object findDeserializationConverter(C10080jW r3) {
        Class<C422328u> converter;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (converter = jsonDeserialize.converter()) == C422328u.class) {
            return null;
        }
        return converter;
    }

    public Class findDeserializationKeyType(C10080jW r3, C10030jR r4) {
        Class<C422228t> keyAs;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (keyAs = jsonDeserialize.keyAs()) == C422228t.class) {
            return null;
        }
        return keyAs;
    }

    public Class findDeserializationType(C10080jW r3, C10030jR r4) {
        Class<C422228t> as;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (as = jsonDeserialize.as()) == C422228t.class) {
            return null;
        }
        return as;
    }

    public /* bridge */ /* synthetic */ Object findDeserializer(C10080jW r3) {
        Class<JsonDeserializer.None> using;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (using = jsonDeserialize.using()) == JsonDeserializer.None.class) {
            return null;
        }
        return using;
    }

    public Object findFilterId(C10070jV r3) {
        JsonFilter jsonFilter = (JsonFilter) r3.getAnnotation(JsonFilter.class);
        if (jsonFilter == null) {
            return null;
        }
        String value = jsonFilter.value();
        if (value.length() > 0) {
            return value;
        }
        return null;
    }

    public Boolean findIgnoreUnknownProperties(C10070jV r2) {
        JsonIgnoreProperties jsonIgnoreProperties = (JsonIgnoreProperties) r2.getAnnotation(JsonIgnoreProperties.class);
        if (jsonIgnoreProperties == null) {
            return null;
        }
        return Boolean.valueOf(jsonIgnoreProperties.ignoreUnknown());
    }

    public Object findInjectableValueId(C183512m r3) {
        Class rawType;
        JacksonInject jacksonInject = (JacksonInject) r3.getAnnotation(JacksonInject.class);
        if (jacksonInject == null) {
            return null;
        }
        String value = jacksonInject.value();
        if (value.length() != 0) {
            return value;
        }
        if (r3 instanceof C29141fw) {
            C29141fw r1 = (C29141fw) r3;
            if (r1.getParameterCount() != 0) {
                rawType = r1.getRawParameterType(0);
                return rawType.getName();
            }
        }
        rawType = r3.getRawType();
        return rawType.getName();
    }

    public /* bridge */ /* synthetic */ Object findKeyDeserializer(C10080jW r3) {
        Class<C422528x> keyUsing;
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r3.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || (keyUsing = jsonDeserialize.keyUsing()) == C422528x.class) {
            return null;
        }
        return keyUsing;
    }

    public /* bridge */ /* synthetic */ Object findKeySerializer(C10080jW r3) {
        Class<JsonSerializer.None> keyUsing;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || (keyUsing = jsonSerialize.keyUsing()) == JsonSerializer.None.class) {
            return null;
        }
        return keyUsing;
    }

    public C87974Hw findNameForDeserialization(C10080jW r4) {
        String str;
        if (r4 instanceof C29091fr) {
            str = findDeserializationName((C29091fr) r4);
        } else if (r4 instanceof C29141fw) {
            str = findDeserializationName((C29141fw) r4);
        } else if (r4 instanceof AnonymousClass137) {
            str = findDeserializationName((AnonymousClass137) r4);
        } else {
            str = null;
        }
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return C87974Hw.USE_DEFAULT;
        }
        return new C87974Hw(str, null);
    }

    public C87974Hw findNameForSerialization(C10080jW r4) {
        String str;
        if (r4 instanceof C29091fr) {
            str = findSerializationName((C29091fr) r4);
        } else if (r4 instanceof C29141fw) {
            str = findSerializationName((C29141fw) r4);
        } else {
            str = null;
        }
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return C87974Hw.USE_DEFAULT;
        }
        return new C87974Hw(str, null);
    }

    public Object findNamingStrategy(C10070jV r2) {
        JsonNaming jsonNaming = (JsonNaming) r2.getAnnotation(JsonNaming.class);
        if (jsonNaming == null) {
            return null;
        }
        return jsonNaming.value();
    }

    public CZj findObjectIdInfo(C10080jW r7) {
        JsonIdentityInfo jsonIdentityInfo = (JsonIdentityInfo) r7.getAnnotation(JsonIdentityInfo.class);
        if (jsonIdentityInfo == null || jsonIdentityInfo.generator() == C25139CaY.class) {
            return null;
        }
        return new CZj(jsonIdentityInfo.property(), jsonIdentityInfo.scope(), jsonIdentityInfo.generator(), false);
    }

    public CZj findObjectReferenceInfo(C10080jW r6, CZj cZj) {
        boolean alwaysAsId;
        JsonIdentityReference jsonIdentityReference = (JsonIdentityReference) r6.getAnnotation(JsonIdentityReference.class);
        if (jsonIdentityReference == null || cZj._alwaysAsId == (alwaysAsId = jsonIdentityReference.alwaysAsId())) {
            return cZj;
        }
        return new CZj(cZj._propertyName, cZj._scope, cZj._generator, alwaysAsId);
    }

    public Class findPOJOBuilder(C10070jV r4) {
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r4.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || jsonDeserialize.builder() == C422228t.class) {
            return null;
        }
        return jsonDeserialize.builder();
    }

    public BM3 findPOJOBuilderConfig(C10070jV r3) {
        JsonPOJOBuilder jsonPOJOBuilder = (JsonPOJOBuilder) r3.getAnnotation(JsonPOJOBuilder.class);
        if (jsonPOJOBuilder == null) {
            return null;
        }
        return new BM3(jsonPOJOBuilder);
    }

    public String[] findPropertiesToIgnore(C10080jW r2) {
        JsonIgnoreProperties jsonIgnoreProperties = (JsonIgnoreProperties) r2.getAnnotation(JsonIgnoreProperties.class);
        if (jsonIgnoreProperties == null) {
            return null;
        }
        return jsonIgnoreProperties.value();
    }

    public C860046h findReferenceType(C183512m r4) {
        JsonManagedReference jsonManagedReference = (JsonManagedReference) r4.getAnnotation(JsonManagedReference.class);
        if (jsonManagedReference != null) {
            return new C860046h(AnonymousClass46g.MANAGED_REFERENCE, jsonManagedReference.value());
        }
        JsonBackReference jsonBackReference = (JsonBackReference) r4.getAnnotation(JsonBackReference.class);
        if (jsonBackReference == null) {
            return null;
        }
        return new C860046h(AnonymousClass46g.BACK_REFERENCE, jsonBackReference.value());
    }

    public C87974Hw findRootName(C10070jV r4) {
        JsonRootName jsonRootName = (JsonRootName) r4.getAnnotation(JsonRootName.class);
        if (jsonRootName == null) {
            return null;
        }
        return new C87974Hw(jsonRootName.value(), null);
    }

    public Object findSerializationContentConverter(C183512m r3) {
        Class<C422328u> contentConverter;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || (contentConverter = jsonSerialize.contentConverter()) == C422328u.class) {
            return null;
        }
        return contentConverter;
    }

    public Class findSerializationContentType(C10080jW r3, C10030jR r4) {
        Class<C422228t> contentAs;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || (contentAs = jsonSerialize.contentAs()) == C422228t.class) {
            return null;
        }
        return contentAs;
    }

    public Object findSerializationConverter(C10080jW r3) {
        Class<C422328u> converter;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || (converter = jsonSerialize.converter()) == C422328u.class) {
            return null;
        }
        return converter;
    }

    public AnonymousClass0oC findSerializationInclusion(C10080jW r2, AnonymousClass0oC r3) {
        JsonInclude jsonInclude = (JsonInclude) r2.getAnnotation(JsonInclude.class);
        if (jsonInclude != null) {
            return jsonInclude.value();
        }
        JsonSerialize jsonSerialize = (JsonSerialize) r2.getAnnotation(JsonSerialize.class);
        if (jsonSerialize != null) {
            switch (jsonSerialize.include().ordinal()) {
                case 0:
                    return AnonymousClass0oC.ALWAYS;
                case 1:
                    return AnonymousClass0oC.NON_NULL;
                case 2:
                    return AnonymousClass0oC.NON_DEFAULT;
                case 3:
                    return AnonymousClass0oC.NON_EMPTY;
            }
        }
        return r3;
    }

    public Class findSerializationKeyType(C10080jW r3, C10030jR r4) {
        Class<C422228t> keyAs;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || (keyAs = jsonSerialize.keyAs()) == C422228t.class) {
            return null;
        }
        return keyAs;
    }

    public String[] findSerializationPropertyOrder(C10070jV r2) {
        JsonPropertyOrder jsonPropertyOrder = (JsonPropertyOrder) r2.getAnnotation(JsonPropertyOrder.class);
        if (jsonPropertyOrder == null) {
            return null;
        }
        return jsonPropertyOrder.value();
    }

    public Boolean findSerializationSortAlphabetically(C10070jV r2) {
        JsonPropertyOrder jsonPropertyOrder = (JsonPropertyOrder) r2.getAnnotation(JsonPropertyOrder.class);
        if (jsonPropertyOrder == null) {
            return null;
        }
        return Boolean.valueOf(jsonPropertyOrder.alphabetic());
    }

    public Class findSerializationType(C10080jW r3) {
        Class<C422228t> as;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || (as = jsonSerialize.as()) == C422228t.class) {
            return null;
        }
        return as;
    }

    public AnonymousClass290 findSerializationTyping(C10080jW r2) {
        JsonSerialize jsonSerialize = (JsonSerialize) r2.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null) {
            return null;
        }
        return jsonSerialize.typing();
    }

    public Object findSerializer(C10080jW r3) {
        Class<JsonSerializer.None> using;
        JsonSerialize jsonSerialize = (JsonSerialize) r3.getAnnotation(JsonSerialize.class);
        if (jsonSerialize != null && (using = jsonSerialize.using()) != JsonSerializer.None.class) {
            return using;
        }
        JsonRawValue jsonRawValue = (JsonRawValue) r3.getAnnotation(JsonRawValue.class);
        if (jsonRawValue == null || !jsonRawValue.value()) {
            return null;
        }
        return new RawSerializer(r3.getRawType());
    }

    public List findSubtypes(C10080jW r8) {
        JsonSubTypes jsonSubTypes = (JsonSubTypes) r8.getAnnotation(JsonSubTypes.class);
        if (jsonSubTypes == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(r4);
        for (JsonSubTypes.Type type : jsonSubTypes.value()) {
            arrayList.add(new AnonymousClass8ZW(type.value(), type.name()));
        }
        return arrayList;
    }

    public String findTypeName(C10070jV r2) {
        JsonTypeName jsonTypeName = (JsonTypeName) r2.getAnnotation(JsonTypeName.class);
        if (jsonTypeName == null) {
            return null;
        }
        return jsonTypeName.value();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        if (r4.length() <= 0) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C25133CaS findUnwrappingNameTransformer(X.C183512m r6) {
        /*
            r5 = this;
            java.lang.Class<com.fasterxml.jackson.annotation.JsonUnwrapped> r0 = com.fasterxml.jackson.annotation.JsonUnwrapped.class
            java.lang.annotation.Annotation r1 = r6.getAnnotation(r0)
            com.fasterxml.jackson.annotation.JsonUnwrapped r1 = (com.fasterxml.jackson.annotation.JsonUnwrapped) r1
            if (r1 == 0) goto L_0x0048
            boolean r0 = r1.enabled()
            if (r0 == 0) goto L_0x0048
            java.lang.String r4 = r1.prefix()
            java.lang.String r3 = r1.suffix()
            r2 = 1
            if (r4 == 0) goto L_0x0022
            int r0 = r4.length()
            r1 = 1
            if (r0 > 0) goto L_0x0023
        L_0x0022:
            r1 = 0
        L_0x0023:
            if (r3 == 0) goto L_0x0035
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x0035
        L_0x002b:
            if (r1 == 0) goto L_0x003d
            if (r2 == 0) goto L_0x0037
            X.CaU r0 = new X.CaU
            r0.<init>(r4, r3)
            return r0
        L_0x0035:
            r2 = 0
            goto L_0x002b
        L_0x0037:
            X.CaV r0 = new X.CaV
            r0.<init>(r4)
            return r0
        L_0x003d:
            if (r2 == 0) goto L_0x0045
            X.CaW r0 = new X.CaW
            r0.<init>(r3)
            return r0
        L_0x0045:
            X.CaS r0 = X.C25133CaS.NOP
            return r0
        L_0x0048:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10130jb.findUnwrappingNameTransformer(X.12m):X.CaS");
    }

    public Object findValueInstantiator(C10070jV r2) {
        JsonValueInstantiator jsonValueInstantiator = (JsonValueInstantiator) r2.getAnnotation(JsonValueInstantiator.class);
        if (jsonValueInstantiator == null) {
            return null;
        }
        return jsonValueInstantiator.value();
    }

    public Class[] findViews(C10080jW r2) {
        JsonView jsonView = (JsonView) r2.getAnnotation(JsonView.class);
        if (jsonView == null) {
            return null;
        }
        return jsonView.value();
    }

    public boolean hasAnyGetterAnnotation(C29141fw r2) {
        return r2.hasAnnotation(JsonAnyGetter.class);
    }

    public boolean hasAnySetterAnnotation(C29141fw r2) {
        return r2.hasAnnotation(JsonAnySetter.class);
    }

    public boolean hasAsValueAnnotation(C29141fw r3) {
        JsonValue jsonValue = (JsonValue) r3.getAnnotation(JsonValue.class);
        if (jsonValue == null || !jsonValue.value()) {
            return false;
        }
        return true;
    }

    public boolean hasCreatorAnnotation(C10080jW r2) {
        return r2.hasAnnotation(JsonCreator.class);
    }

    public boolean hasIgnoreMarker(C183512m r3) {
        JsonIgnore jsonIgnore = (JsonIgnore) r3.getAnnotation(JsonIgnore.class);
        if (jsonIgnore == null || !jsonIgnore.value()) {
            return false;
        }
        return true;
    }

    public Boolean hasRequiredMarker(C183512m r2) {
        JsonProperty jsonProperty = (JsonProperty) r2.getAnnotation(JsonProperty.class);
        if (jsonProperty != null) {
            return Boolean.valueOf(jsonProperty.required());
        }
        return null;
    }

    public Boolean isIgnorableType(C10070jV r2) {
        JsonIgnoreType jsonIgnoreType = (JsonIgnoreType) r2.getAnnotation(JsonIgnoreType.class);
        if (jsonIgnoreType == null) {
            return null;
        }
        return Boolean.valueOf(jsonIgnoreType.value());
    }

    public Boolean isTypeId(C183512m r2) {
        return Boolean.valueOf(r2.hasAnnotation(JsonTypeId.class));
    }

    public C11780nw version() {
        return PackageVersion.VERSION;
    }

    public CXQ findPropertyContentTypeResolver(C10470kA r4, C183512m r5, C10030jR r6) {
        if (r6.isContainerType()) {
            return _findTypeResolver(r4, r5, r6);
        }
        throw new IllegalArgumentException("Must call method with a container type (got " + r6 + ")");
    }

    public CXQ findPropertyTypeResolver(C10470kA r2, C183512m r3, C10030jR r4) {
        if (r4.isContainerType()) {
            return null;
        }
        return _findTypeResolver(r2, r3, r4);
    }

    public CXQ findTypeResolver(C10470kA r2, C10070jV r3, C10030jR r4) {
        return _findTypeResolver(r2, r3, r4);
    }

    public boolean isAnnotationBundle(Annotation annotation) {
        if (annotation.annotationType().getAnnotation(JacksonAnnotationsInside.class) != null) {
            return true;
        }
        return false;
    }

    public String findDeserializationName(AnonymousClass137 r2) {
        JsonProperty jsonProperty;
        if (r2 == null || (jsonProperty = (JsonProperty) r2.getAnnotation(JsonProperty.class)) == null) {
            return null;
        }
        return jsonProperty.value();
    }

    public String findDeserializationName(C29091fr r2) {
        JsonProperty jsonProperty = (JsonProperty) r2.getAnnotation(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        if (r2.hasAnnotation(JsonDeserialize.class) || r2.hasAnnotation(JsonView.class) || r2.hasAnnotation(JsonBackReference.class) || r2.hasAnnotation(JsonManagedReference.class)) {
            return BuildConfig.FLAVOR;
        }
        return null;
    }

    public String findDeserializationName(C29141fw r2) {
        JsonSetter jsonSetter = (JsonSetter) r2.getAnnotation(JsonSetter.class);
        if (jsonSetter != null) {
            return jsonSetter.value();
        }
        JsonProperty jsonProperty = (JsonProperty) r2.getAnnotation(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        if (r2.hasAnnotation(JsonDeserialize.class) || r2.hasAnnotation(JsonView.class) || r2.hasAnnotation(JsonBackReference.class) || r2.hasAnnotation(JsonManagedReference.class)) {
            return BuildConfig.FLAVOR;
        }
        return null;
    }

    public CX6 findFormat(C10080jW r6) {
        JsonFormat jsonFormat = (JsonFormat) r6.getAnnotation(JsonFormat.class);
        if (jsonFormat == null) {
            return null;
        }
        return new CX6(jsonFormat.pattern(), jsonFormat.shape(), jsonFormat.locale(), jsonFormat.timezone());
    }

    public CX6 findFormat(C183512m r2) {
        return findFormat(r2);
    }

    public String findSerializationName(C29091fr r2) {
        JsonProperty jsonProperty = (JsonProperty) r2.getAnnotation(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        if (r2.hasAnnotation(JsonSerialize.class) || r2.hasAnnotation(JsonView.class)) {
            return BuildConfig.FLAVOR;
        }
        return null;
    }

    public String findSerializationName(C29141fw r2) {
        JsonGetter jsonGetter = (JsonGetter) r2.getAnnotation(JsonGetter.class);
        if (jsonGetter != null) {
            return jsonGetter.value();
        }
        JsonProperty jsonProperty = (JsonProperty) r2.getAnnotation(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        }
        if (r2.hasAnnotation(JsonSerialize.class) || r2.hasAnnotation(JsonView.class)) {
            return BuildConfig.FLAVOR;
        }
        return null;
    }
}
