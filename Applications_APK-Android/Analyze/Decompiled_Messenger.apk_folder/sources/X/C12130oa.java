package X;

import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.graphql.query.GQSQStringShape0S0000000_I0;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.User;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0oa  reason: invalid class name and case insensitive filesystem */
public final class C12130oa {
    private static volatile C12130oa A02;
    private final C26871cH A00;
    @LoggedInUser
    private final C04310Tq A01;

    public boolean A03(long j) {
        return j == 972215336239651L || A01().A03 == j;
    }

    public static final C12130oa A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C12130oa.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C12130oa(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public ThreadKey A01() {
        long j;
        ThreadKey A022 = A02((User) this.A01.get());
        if (A022 != null) {
            return A022;
        }
        C26871cH r3 = this.A00;
        if (r3.A02.A09() == null) {
            j = 0;
        } else {
            j = r3.A02.A09().A0D;
        }
        if (j == 0 && !r3.A04.A00.Aep(AnonymousClass129.A0Q, false) && !r3.A01) {
            C26931cN A002 = C26931cN.A00(new GQSQStringShape0S0000000_I0(4));
            A002.A09(0);
            C10910l3 A04 = ((C11170mD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ASp, r3.A00)).A04(A002);
            r3.A01 = true;
            C05350Yp.A08(AnonymousClass169.A01(A04, new AnonymousClass4AV(), r3.A05), new AnonymousClass43W(r3), r3.A06);
        }
        return ThreadKey.A01(972215336239651L);
    }

    public ThreadKey A02(User user) {
        if (user == null) {
            return null;
        }
        long j = user.A0D;
        if (j == 0) {
            return null;
        }
        return C12150od.A01(j);
    }

    public boolean A04(ThreadKey threadKey) {
        if (threadKey != null && threadKey.A03 == 972215336239651L) {
            return true;
        }
        if (!C12150od.A02(threadKey)) {
            return false;
        }
        return A01().equals(threadKey);
    }

    private C12130oa(AnonymousClass1XY r2) {
        this.A00 = C26871cH.A00(r2);
        this.A01 = AnonymousClass0WY.A01(r2);
        C12150od.A00(r2);
    }
}
