package X;

import com.facebook.graphservice.interfaces.Tree;

/* renamed from: X.11R  reason: invalid class name */
public interface AnonymousClass11R {
    Tree getResult(Class cls, int i);

    AnonymousClass11R setBoolean(int i, Boolean bool);

    AnonymousClass11R setDouble(int i, Double d);

    AnonymousClass11R setDoubleList(int i, Iterable iterable);

    AnonymousClass11R setInt(int i, Integer num);

    AnonymousClass11R setIntList(int i, Iterable iterable);

    AnonymousClass11R setString(int i, String str);

    AnonymousClass11R setStringList(int i, Iterable iterable);

    AnonymousClass11R setTime(int i, Long l);

    AnonymousClass11R setTree(int i, Tree tree);

    AnonymousClass11R setTreeList(int i, Iterable iterable);
}
