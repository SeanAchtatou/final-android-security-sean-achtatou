package X;

import android.text.format.DateFormat;
import com.google.common.collect.ImmutableList;
import java.text.SimpleDateFormat;
import java.util.Locale;

/* renamed from: X.1vi  reason: invalid class name and case insensitive filesystem */
public final class C37461vi extends C20831Dz {
    public ImmutableList A00;
    public SimpleDateFormat A01;
    public Locale A02;
    public final /* synthetic */ C54742mv A03;

    public C37461vi(C54742mv r4) {
        this.A03 = r4;
        this.A02 = r4.A12().getConfiguration().locale;
        if (DateFormat.is24HourFormat(r4.A1j())) {
            this.A01 = new SimpleDateFormat("HH:mm", this.A02);
        } else {
            this.A01 = new SimpleDateFormat("h:mm a", this.A02);
        }
        this.A01.setTimeZone(r4.A05);
    }

    public int ArU() {
        ImmutableList immutableList = this.A00;
        if (immutableList == null || immutableList.isEmpty()) {
            return 0;
        }
        return this.A00.size() + 1;
    }
}
