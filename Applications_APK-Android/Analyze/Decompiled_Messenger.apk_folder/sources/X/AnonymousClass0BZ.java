package X;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0BZ  reason: invalid class name */
public final class AnonymousClass0BZ implements Executor {
    public boolean A00 = false;
    private final Queue A01 = new ConcurrentLinkedQueue();
    private final Executor A02;

    public static void A01(AnonymousClass0BZ r3) {
        AnonymousClass086 r2;
        synchronized (r3) {
            if (!r3.A00 && (r2 = (AnonymousClass086) r3.A01.poll()) != null) {
                r3.A00 = true;
                AnonymousClass07A.A04(r3.A02, r2, 1909585907);
            }
        }
    }

    public static AnonymousClass0BZ A00(String str) {
        if (AnonymousClass0CX.A03 == null) {
            synchronized (AnonymousClass0CX.class) {
                if (AnonymousClass0CX.A03 == null) {
                    AnonymousClass0CX.A03 = new ThreadPoolExecutor(AnonymousClass0CX.A00, 128, 1, TimeUnit.SECONDS, AnonymousClass0CX.A01, AnonymousClass0CX.A02);
                }
            }
        }
        AnonymousClass0BY r2 = new AnonymousClass0BY(AnonymousClass0CX.A03);
        r2.A00 = str;
        return new AnonymousClass0BZ(r2);
    }

    public void execute(Runnable runnable) {
        this.A01.add(new AnonymousClass086(this, runnable));
        A01(this);
    }

    public AnonymousClass0BZ(AnonymousClass0BY r2) {
        this.A02 = r2.A01;
    }
}
