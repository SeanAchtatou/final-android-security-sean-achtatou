package X;

import com.facebook.acra.util.ProcFileReader;
import java.io.Writer;

/* renamed from: X.01N  reason: invalid class name */
public final class AnonymousClass01N implements AnonymousClass01J {
    public boolean ANg(Writer writer, AnonymousClass0HM r6) {
        try {
            int openFDCount = ProcFileReader.getProcFileReader().getOpenFDCount();
            ProcFileReader.OpenFDLimits openFDLimits = ProcFileReader.getProcFileReader().getOpenFDLimits();
            writer.append((CharSequence) "\"openFDCount\":");
            writer.append((CharSequence) Integer.toString(openFDCount));
            if (openFDLimits != null) {
                writer.append((CharSequence) ",");
                writer.append((CharSequence) "\"softFDLimit\":");
                writer.append((CharSequence) Integer.toString(openFDLimits.softLimit));
                writer.append((CharSequence) ",");
                writer.append((CharSequence) "\"hardFDLimit\":");
                writer.append((CharSequence) Integer.toString(openFDLimits.hardLimit));
            }
            return true;
        } catch (Exception unused) {
            writer.append((CharSequence) "\"FDReportError\":1");
            return true;
        }
    }
}
