package X;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Wu  reason: invalid class name and case insensitive filesystem */
public final class C24711Wu {
    public static final Executor A00 = AnonymousClass0T5.A00;

    public static Executor A00() {
        return new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), AnonymousClass15U.A00);
    }
}
