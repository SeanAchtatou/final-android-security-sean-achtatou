package X;

import android.content.Context;
import java.io.File;
import java.io.IOException;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0IB  reason: invalid class name */
public final class AnonymousClass0IB implements AnonymousClass1YQ {
    private static volatile AnonymousClass0IB A02;
    private Context A00;
    private AnonymousClass1YI A01;

    public String getSimpleName() {
        return "MessengerSplashScreenExperimentController";
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BKk, r1);
    }

    public static final AnonymousClass0IB A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0IB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass0IB(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private void A02(int i, String str) {
        boolean AbO = this.A01.AbO(i, false);
        Context context = this.A00;
        if (context != null) {
            File fileStreamPath = context.getFileStreamPath(str);
            if (AbO && !fileStreamPath.exists()) {
                try {
                    fileStreamPath.createNewFile();
                } catch (IOException unused) {
                }
            } else if (!AbO && fileStreamPath.exists()) {
                fileStreamPath.delete();
            }
        }
    }

    private AnonymousClass0IB(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A02(r2);
        this.A01 = AnonymousClass0WA.A00(r2);
    }

    public void init() {
        int A03 = C000700l.A03(-303118564);
        A02(268, "orca_splash_screen2.enabled");
        A02(AnonymousClass1Y3.A24, "orca_splash_screen.enabled");
        C000700l.A09(562024860, A03);
    }
}
