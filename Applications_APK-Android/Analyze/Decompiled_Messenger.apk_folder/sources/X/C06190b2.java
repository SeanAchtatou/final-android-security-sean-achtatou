package X;

import java.nio.ByteBuffer;

/* renamed from: X.0b2  reason: invalid class name and case insensitive filesystem */
public abstract class C06190b2 {
    public abstract ByteBuffer getJavaByteBuffer();

    /* JADX WARNING: Can't wrap try/catch for region: R(4:15|(2:17|18)|19|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        if (r2 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0029 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0030 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.nio.ByteBuffer A00(java.lang.String r7) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r7)
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0031 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0031 }
            java.nio.channels.FileChannel r2 = r1.getChannel()     // Catch:{ all -> 0x002a }
            java.nio.channels.FileChannel$MapMode r3 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ all -> 0x0021 }
            r4 = 0
            long r6 = r2.size()     // Catch:{ all -> 0x0021 }
            java.nio.MappedByteBuffer r0 = r2.map(r3, r4, r6)     // Catch:{ all -> 0x0021 }
            r2.close()     // Catch:{ all -> 0x002a }
            r1.close()     // Catch:{ IOException -> 0x0031 }
            return r0
        L_0x0021:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            if (r2 == 0) goto L_0x0029
            r2.close()     // Catch:{ all -> 0x0029 }
        L_0x0029:
            throw r0     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002c }
        L_0x002c:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0030 }
        L_0x0030:
            throw r0     // Catch:{ IOException -> 0x0031 }
        L_0x0031:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06190b2.A00(java.lang.String):java.nio.ByteBuffer");
    }
}
