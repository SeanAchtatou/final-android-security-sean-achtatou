package X;

import android.text.TextUtils;
import io.card.payment.BuildConfig;
import java.util.Collections;
import java.util.Set;

/* renamed from: X.0Xn  reason: invalid class name and case insensitive filesystem */
public abstract class C05070Xn {
    public int A00(String str, int i) {
        return ((C05060Xm) this).A00.A04(str, i);
    }

    public String A02(String str, String str2) {
        return ((C05060Xm) this).A00.A07(str, str2);
    }

    public Set A03(String str, Set set) {
        return ((C05060Xm) this).A00.A09(str, set);
    }

    public int A01(String str, String str2, String str3, int i) {
        int intValue;
        int intValue2;
        if (!(!TextUtils.isEmpty(A02("__fs_policy_config_checksum__", BuildConfig.FLAVOR)))) {
            return -1;
        }
        if (A03("__fs_policy_blacklisted_events__", Collections.emptySet()).contains(str)) {
            return 0;
        }
        if (str2 == null || str3 == null || (intValue = Integer.valueOf(A00(AnonymousClass08S.A0T(str, ":", str2, ":", str3), -2)).intValue()) == -2) {
            if (str2 != null && (intValue2 = Integer.valueOf(A00(AnonymousClass08S.A0P(str, ":", str2), -2)).intValue()) != -2) {
                return intValue2;
            }
            intValue = Integer.valueOf(A00(str, -2)).intValue();
            if (intValue == -2) {
                return i;
            }
        }
        return intValue;
    }
}
