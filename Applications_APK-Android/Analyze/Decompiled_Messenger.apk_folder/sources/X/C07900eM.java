package X;

import android.content.res.Resources;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import java.util.LinkedList;
import java.util.List;

@UserScoped
/* renamed from: X.0eM  reason: invalid class name and case insensitive filesystem */
public final class C07900eM {
    private static C05540Zi A06;
    public AnonymousClass0UN A00;
    public final Resources A01;
    public final C25921ac A02;
    public final C26681bq A03;
    public final C26691br A04;
    @LoggedInUser
    private final User A05;

    private ThreadKey A01(C16820xp r7) {
        GSTModelShape1S0000000 gSTModelShape1S0000000;
        if (r7 == null) {
            gSTModelShape1S0000000 = null;
        } else {
            gSTModelShape1S0000000 = (GSTModelShape1S0000000) r7.A0J(-364542794, GSTModelShape1S0000000.class, -1407803892);
        }
        if (gSTModelShape1S0000000 != null) {
            Class<GSTModelShape1S0000000> cls = GSTModelShape1S0000000.class;
            if (((GSTModelShape1S0000000) gSTModelShape1S0000000.A0J(-1184643414, cls, 526968138)) != null) {
                return A03(gSTModelShape1S0000000.getBooleanValue(-914085697), ((GSTModelShape1S0000000) gSTModelShape1S0000000.A0J(-1184643414, cls, 526968138)).A3z(), ((GSTModelShape1S0000000) gSTModelShape1S0000000.A0J(-1184643414, cls, 526968138)).A3s());
            }
        }
        return null;
    }

    public static final C07900eM A00(AnonymousClass1XY r4) {
        C07900eM r0;
        synchronized (C07900eM.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r4)) {
                    A06.A00 = new C07900eM((AnonymousClass1XY) A06.A01());
                }
                C05540Zi r1 = A06;
                r0 = (C07900eM) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    private ThreadKey A02(C16820xp r6) {
        GSTModelShape1S0000000 gSTModelShape1S0000000;
        Class<GSTModelShape1S0000000> cls = GSTModelShape1S0000000.class;
        GSTModelShape1S0000000 gSTModelShape1S00000002 = (GSTModelShape1S0000000) r6.A0J(-874443254, cls, -2050173883);
        if (gSTModelShape1S00000002 == null || (gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSTModelShape1S00000002.A0J(-1184643414, cls, -655871095)) == null) {
            return null;
        }
        return A03(gSTModelShape1S00000002.getBooleanValue(-914085697), gSTModelShape1S0000000.A3z(), gSTModelShape1S0000000.A3s());
    }

    private ThreadKey A03(boolean z, String str, String str2) {
        if (z) {
            return ThreadKey.A00(Long.parseLong(str));
        }
        return ThreadKey.A04(Long.parseLong(str2), Long.parseLong(this.A05.A0j));
    }

    public static List A05(C27161ck r6) {
        ImmutableList A0T;
        LinkedList linkedList = new LinkedList();
        if (!(r6 == null || (A0T = r6.A0T()) == null)) {
            C24971Xv it = A0T.iterator();
            while (it.hasNext()) {
                C16820xp A36 = ((GSTModelShape1S0000000) it.next()).A36();
                if (A36 != null) {
                    ImmutableList A0M = A36.A0M(1023019699, GSTModelShape1S0000000.class, -415005572);
                    if (!C013509w.A02(A0M)) {
                        C24971Xv it2 = A0M.iterator();
                        while (it2.hasNext()) {
                            linkedList.add(((GSTModelShape1S0000000) it2.next()).A3m());
                        }
                    }
                }
            }
        }
        return linkedList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:122:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:289:0x0679  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x067d  */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x0686  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:357:0x011e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:372:0x023a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0186  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.common.collect.ImmutableList A07(com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r42) {
        /*
            r41 = this;
            if (r42 == 0) goto L_0x090e
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r42.A1n()
            if (r3 == 0) goto L_0x090e
            java.lang.Class<X.1ck> r2 = X.C27161ck.class
            r1 = 104993457(0x64212b1, float:3.6501077E-35)
            r0 = 738428414(0x2c0385fe, float:1.86906E-12)
            com.google.common.collect.ImmutableList r0 = r3.A0M(r1, r2, r0)
            if (r0 == 0) goto L_0x090e
            com.google.common.collect.ImmutableList$Builder r25 = com.google.common.collect.ImmutableList.builder()
            X.1Xv r24 = r0.iterator()
        L_0x001e:
            boolean r0 = r24.hasNext()
            if (r0 == 0) goto L_0x0909
            java.lang.Object r8 = r24.next()
            X.1ck r8 = (X.C27161ck) r8
            java.lang.String r0 = r8.A0U()
            if (r0 == 0) goto L_0x001e
            r0 = -23571790(0xfffffffffe9852b2, float:-1.0123602E38)
            int r0 = r8.getIntValue(r0)
            if (r0 == 0) goto L_0x001e
            r4 = 0
            if (r8 == 0) goto L_0x0051
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r6 = r8.A0Q()
            if (r6 == 0) goto L_0x0051
            int r0 = r6.ordinal()
            r5 = r41
            switch(r0) {
                case 9: goto L_0x0059;
                case 10: goto L_0x00bd;
                case 11: goto L_0x0108;
                case 13: goto L_0x019c;
                case 17: goto L_0x0224;
                case 18: goto L_0x028f;
                case 19: goto L_0x02af;
                case 25: goto L_0x02bb;
                case 26: goto L_0x036b;
                case 27: goto L_0x0798;
                case 28: goto L_0x07f5;
                case 38: goto L_0x08d1;
                case 49: goto L_0x08fd;
                case 50: goto L_0x02af;
                default: goto L_0x004b;
            }
        L_0x004b:
            X.12V r4 = new X.12V
            r0 = 0
            r4.<init>(r8, r0)
        L_0x0051:
            if (r4 == 0) goto L_0x001e
            r0 = r25
            r0.add(r4)
            goto L_0x001e
        L_0x0059:
            X.12V r4 = new X.12V
            if (r8 == 0) goto L_0x00bb
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = r8.A0R()
            if (r6 == 0) goto L_0x00bb
            com.google.common.collect.ImmutableList r7 = r8.A0T()
            boolean r0 = X.C013509w.A02(r7)
            if (r0 != 0) goto L_0x00bb
            com.facebook.graphql.enums.GraphQLMessengerInbox2BotsYMMLayoutType r1 = com.facebook.graphql.enums.GraphQLMessengerInbox2BotsYMMLayoutType.A01
            r0 = 2011608879(0x77e6b72f, float:9.358935E33)
            java.lang.Enum r0 = r6.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLMessengerInbox2BotsYMMLayoutType r0 = (com.facebook.graphql.enums.GraphQLMessengerInbox2BotsYMMLayoutType) r0
            if (r0 == 0) goto L_0x0081
            int r0 = r0.ordinal()
            switch(r0) {
                case 3: goto L_0x00b8;
                case 4: goto L_0x00b8;
                case 5: goto L_0x00b5;
                default: goto L_0x0081;
            }
        L_0x0081:
            X.72v r3 = X.C1522572v.A04
        L_0x0083:
            int r2 = r7.size()
            int[] r1 = X.C1522672w.A00
            int r0 = r3.ordinal()
            r1 = r1[r0]
            r0 = 1
            if (r1 == r0) goto L_0x00a7
            r0 = 2
            if (r1 == r0) goto L_0x00a4
            X.72u r1 = X.C1522472u.HORIZONTAL_ITEM
        L_0x0097:
            com.google.common.collect.ImmutableList r0 = X.C29596Ee0.A00(r8, r1, r2)
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r1 = new com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit
            r1.<init>(r8, r3, r0)
        L_0x00a0:
            r4.<init>(r8, r1)
            goto L_0x0051
        L_0x00a4:
            X.72u r1 = X.C1522472u.VERTICAL_CARD
            goto L_0x0097
        L_0x00a7:
            X.72u r1 = X.C1522472u.VERTICAL_ITEM
            r0 = -929496119(0xffffffffc89903c9, float:-313374.28)
            int r0 = r6.getIntValue(r0)
            int r2 = java.lang.Math.min(r2, r0)
            goto L_0x0097
        L_0x00b5:
            X.72v r3 = X.C1522572v.CARD_VERTICAL
            goto L_0x0083
        L_0x00b8:
            X.72v r3 = X.C1522572v.VERTICAL
            goto L_0x0083
        L_0x00bb:
            r1 = 0
            goto L_0x00a0
        L_0x00bd:
            int r2 = X.AnonymousClass1Y3.AWJ
            X.0UN r1 = r5.A00
            r0 = 6
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0pP r1 = (X.C12460pP) r1
            r0 = 1
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            boolean r1 = r1.A08(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0051
            r3 = 3
            int r2 = X.AnonymousClass1Y3.AFL
            X.0UN r1 = r5.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.2fT r6 = (X.C51112fT) r6
            X.CuI r5 = new X.CuI
            r5.<init>()
            r4 = 0
            r5.A04 = r4
            r3 = 1
            java.lang.String r1 = "You must provide a positive Max item count."
            com.google.common.base.Preconditions.checkArgument(r3, r1)
            r5.A00 = r3
            com.facebook.messaging.media.loader.LocalMediaLoaderParams r1 = new com.facebook.messaging.media.loader.LocalMediaLoaderParams
            r1.<init>(r5)
            com.google.common.collect.ImmutableList r2 = r6.A02(r1)
            if (r2 == 0) goto L_0x0791
            boolean r1 = r2.isEmpty()
            if (r1 != 0) goto L_0x0791
            java.lang.Object r0 = r2.get(r4)
            com.facebook.ui.media.attachments.model.MediaResource r0 = (com.facebook.ui.media.attachments.model.MediaResource) r0
            goto L_0x0791
        L_0x0108:
            X.12V r4 = new X.12V
            com.google.common.collect.ImmutableList$Builder r7 = com.google.common.collect.ImmutableList.builder()
            if (r8 == 0) goto L_0x0191
            com.google.common.collect.ImmutableList r1 = r8.A0T()
            boolean r0 = X.C013509w.A02(r1)
            if (r0 != 0) goto L_0x0191
            X.1Xv r10 = r1.iterator()
        L_0x011e:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x0193
            java.lang.Object r6 = r10.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r6
            r0 = 1050673705(0x3ea00229, float:0.31251648)
            int r0 = r6.getIntValue(r0)
            if (r0 == 0) goto L_0x011e
            r0 = -1374423487(0xffffffffae13f641, float:-3.3642648E-11)
            int r0 = r6.getIntValue(r0)
            if (r0 == 0) goto L_0x011e
            X.0xp r1 = r6.A36()
            if (r1 == 0) goto L_0x018f
            X.1s6 r9 = r1.Afi()
            if (r9 == 0) goto L_0x018f
            r0 = 3373707(0x337a8b, float:4.72757E-39)
            java.lang.String r5 = r1.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 != 0) goto L_0x018f
            r0 = 3355(0xd1b, float:4.701E-42)
            java.lang.String r3 = r1.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)
            if (r0 != 0) goto L_0x018f
            r0 = -724044987(0xffffffffd4d7f345, float:-7.4199948E12)
            java.lang.String r2 = r1.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 != 0) goto L_0x018f
            X.EXs r1 = new X.EXs
            r1.<init>()
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = X.C35971s7.A00(r9)
            r1.A00 = r0
            r1.A01 = r2
            r1.A02 = r3
            r1.A03 = r5
            com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData r0 = new com.facebook.messaging.inbox2.platformextensions.InboxPlatformExtensionsBasicData
            r0.<init>(r1)
        L_0x0184:
            if (r0 == 0) goto L_0x011e
            com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem r1 = new com.facebook.messaging.inbox2.platformextensions.PlatformExtensionsVerticalInboxItem
            r1.<init>(r8, r6, r0)
            r7.add(r1)
            goto L_0x011e
        L_0x018f:
            r0 = 0
            goto L_0x0184
        L_0x0191:
            r0 = 0
            goto L_0x0197
        L_0x0193:
            com.google.common.collect.ImmutableList r0 = r7.build()
        L_0x0197:
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x019c:
            r7 = r5
            X.0dQ r3 = com.google.common.collect.ImmutableSet.A01()
            com.google.common.collect.ImmutableList r0 = r8.A0T()
            X.1Xv r2 = r0.iterator()
        L_0x01a9:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x01ca
            java.lang.Object r1 = r2.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r0 = 0
            boolean r0 = A06(r1, r0)
            if (r0 != 0) goto L_0x01a9
            X.0xp r0 = r1.A36()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A01(r0)
            if (r0 == 0) goto L_0x01a9
            r3.A01(r0)
            goto L_0x01a9
        L_0x01ca:
            com.google.common.collect.ImmutableSet r1 = r3.build()
            boolean r0 = r1.isEmpty()
            r10 = 0
            r6 = 1
            if (r0 == 0) goto L_0x0212
            r9 = r10
        L_0x01d7:
            com.google.common.collect.ImmutableList$Builder r5 = com.google.common.collect.ImmutableList.builder()
            com.google.common.collect.ImmutableList r0 = r8.A0T()
            X.1Xv r4 = r0.iterator()
        L_0x01e3:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0219
            java.lang.Object r3 = r4.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3
            boolean r0 = A06(r3, r6)
            if (r0 != 0) goto L_0x01e3
            X.0xp r2 = r3.A36()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A01(r2)
            if (r9 == 0) goto L_0x0210
            if (r0 == 0) goto L_0x0210
            java.lang.Object r0 = r9.get(r0)
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0
        L_0x0207:
            com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem r1 = new com.facebook.messaging.conversationstarters.InboxUnitConversationStarterItem
            r1.<init>(r8, r3, r0, r2)
            r5.add(r1)
            goto L_0x01e3
        L_0x0210:
            r0 = r10
            goto L_0x0207
        L_0x0212:
            java.lang.String r0 = "createConversationStartersInbox2Unit"
            com.google.common.collect.ImmutableMap r9 = r5.A04(r1, r6, r0)
            goto L_0x01d7
        L_0x0219:
            X.12V r4 = new X.12V
            com.google.common.collect.ImmutableList r0 = r5.build()
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x0224:
            X.12V r4 = new X.12V
            if (r8 == 0) goto L_0x0284
            com.google.common.collect.ImmutableList r1 = r8.A0T()
            boolean r0 = X.C013509w.A02(r1)
            if (r0 != 0) goto L_0x0284
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()
            X.1Xv r7 = r1.iterator()
        L_0x023a:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0286
            java.lang.Object r5 = r7.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r5
            r0 = -1374423487(0xffffffffae13f641, float:-3.3642648E-11)
            int r0 = r5.getIntValue(r0)
            if (r0 == 0) goto L_0x023a
            r0 = 1050673705(0x3ea00229, float:0.31251648)
            int r0 = r5.getIntValue(r0)
            if (r0 == 0) goto L_0x023a
            X.0xp r1 = r5.A36()
            if (r1 == 0) goto L_0x0282
            r0 = 3355(0xd1b, float:4.701E-42)
            java.lang.String r2 = r1.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 != 0) goto L_0x0282
            r0 = -979370063(0xffffffffc59fffb1, float:-5119.9614)
            java.lang.String r1 = r1.A0P(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 != 0) goto L_0x0282
            com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem r0 = new com.facebook.messaging.discovery.model.MessengerDiscoveryCategoryInboxItem
            r0.<init>(r8, r5, r2, r1)
        L_0x027c:
            if (r0 == 0) goto L_0x023a
            r6.add(r0)
            goto L_0x023a
        L_0x0282:
            r0 = 0
            goto L_0x027c
        L_0x0284:
            r0 = 0
            goto L_0x028a
        L_0x0286:
            com.google.common.collect.ImmutableList r0 = r6.build()
        L_0x028a:
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x028f:
            int r2 = X.AnonymousClass1Y3.AjZ
            X.0UN r1 = r5.A00
            r0 = 9
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.6ob r0 = (X.C144836ob) r0
            X.3UR r0 = r0.A02
            boolean r0 = r0.A03()
            if (r0 != 0) goto L_0x0051
            X.12V r4 = new X.12V
            com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem r0 = new com.facebook.messaging.discovery.model.DiscoverLocationUpsellItem
            r0.<init>(r8)
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x02af:
            X.12V r4 = new X.12V
            r0 = 0
            com.facebook.messaging.discovery.model.DiscoverTabAttachmentUnit r0 = X.C29595Edz.A02(r8, r0)
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x02bb:
            X.0dQ r6 = com.google.common.collect.ImmutableSet.A01()
            com.google.common.collect.ImmutableList r9 = r8.A0T()
            X.1Xv r3 = r9.iterator()
        L_0x02c7:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x02ec
            java.lang.Object r2 = r3.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2
            X.0xp r1 = r2.A36()
            if (r1 == 0) goto L_0x02c7
            r0 = -1374423487(0xffffffffae13f641, float:-3.3642648E-11)
            int r0 = r2.getIntValue(r0)
            if (r0 == 0) goto L_0x02c7
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A02(r1)
            if (r0 == 0) goto L_0x02c7
            r6.A01(r0)
            goto L_0x02c7
        L_0x02ec:
            com.google.common.collect.ImmutableSet r2 = r6.build()
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0051
            r1 = 1
            java.lang.String r0 = "createMessageThreadsInbox2Unit"
            com.google.common.collect.ImmutableMap r7 = r5.A04(r2, r1, r0)
            if (r7 == 0) goto L_0x0051
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()
            X.1Xv r9 = r9.iterator()
        L_0x0307:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x0360
            java.lang.Object r1 = r9.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            X.0xp r2 = r1.A36()
            if (r2 == 0) goto L_0x0307
            r0 = -1374423487(0xffffffffae13f641, float:-3.3642648E-11)
            int r0 = r1.getIntValue(r0)
            if (r0 == 0) goto L_0x0307
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A02(r2)
            if (r0 == 0) goto L_0x0307
            java.lang.Object r4 = r7.get(r0)
            com.google.common.base.Preconditions.checkNotNull(r4)
            com.facebook.messaging.model.threads.ThreadSummary r4 = (com.facebook.messaging.model.threads.ThreadSummary) r4
            com.facebook.graphql.enums.GraphQLMessengerInbox2MessageThreadReason r1 = com.facebook.graphql.enums.GraphQLMessengerInbox2MessageThreadReason.A02
            r0 = -934964668(0xffffffffc8459244, float:-202313.06)
            java.lang.Enum r1 = r2.A0O(r0, r1)
            com.facebook.graphql.enums.GraphQLMessengerInbox2MessageThreadReason r1 = (com.facebook.graphql.enums.GraphQLMessengerInbox2MessageThreadReason) r1
            com.facebook.graphql.enums.GraphQLMessengerInbox2MessageThreadReason r0 = com.facebook.graphql.enums.GraphQLMessengerInbox2MessageThreadReason.A01
            r3 = 1
            if (r1 != r0) goto L_0x0358
            r2 = 5
            int r1 = X.AnonymousClass1Y3.AFU
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1BS r0 = (X.AnonymousClass1BS) r0
            boolean r0 = r0.A01(r4)
            if (r0 != 0) goto L_0x0358
        L_0x0352:
            if (r3 != 0) goto L_0x0307
            r6.add(r4)
            goto L_0x0307
        L_0x0358:
            X.0l8 r1 = r4.A0O
            X.0l8 r0 = X.C10950l8.A04
            if (r1 == r0) goto L_0x0352
            r3 = 0
            goto L_0x0352
        L_0x0360:
            X.12V r4 = new X.12V
            com.google.common.collect.ImmutableList r0 = r6.build()
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x036b:
            int r2 = X.AnonymousClass1Y3.AB1
            X.0UN r1 = r5.A00
            r0 = 8
            java.lang.Object r30 = X.AnonymousClass1XX.A02(r0, r2, r1)
            r0 = r30
            X.1ru r0 = (X.C35841ru) r0
            r30 = r0
            if (r8 == 0) goto L_0x078b
            com.google.common.collect.ImmutableList r0 = r8.A0T()
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x078b
            com.google.common.collect.ImmutableList$Builder r15 = new com.google.common.collect.ImmutableList$Builder
            r15.<init>()
            r0 = r30
            X.1ry r0 = r0.A01
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r3 = (X.C25051Yd) r3
            r0 = 563903436816999(0x200de000a0267, double:2.78605315703089E-309)
            r2 = 9
            int r29 = r3.AqL(r0, r2)
            com.google.common.collect.ImmutableList r0 = r8.A0T()
            X.1Xv r28 = r0.iterator()
            r13 = 0
        L_0x03af:
            boolean r0 = r28.hasNext()
            if (r0 == 0) goto L_0x078d
            java.lang.Object r7 = r28.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r7 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r7
            X.0xp r27 = r7.A36()
            r0 = -1374423487(0xffffffffae13f641, float:-3.3642648E-11)
            int r0 = r7.getIntValue(r0)
            if (r0 == 0) goto L_0x0772
            r0 = 1050673705(0x3ea00229, float:0.31251648)
            int r0 = r7.getIntValue(r0)
            if (r0 == 0) goto L_0x0772
            r0 = 0
        L_0x03d2:
            if (r0 != 0) goto L_0x03af
            com.google.common.collect.ImmutableList$Builder r26 = new com.google.common.collect.ImmutableList$Builder
            r26.<init>()
            r6 = r26
            r2 = 0
            if (r27 != 0) goto L_0x04c3
            java.lang.String r0 = "attachment"
            r6.add(r0)
        L_0x03e3:
            r0 = r30
            X.1ry r6 = r0.A01
            if (r2 == 0) goto L_0x0442
            com.facebook.messaging.business.inboxads.common.InboxAdsData r0 = r2.A00
            if (r0 == 0) goto L_0x0442
            com.google.common.collect.ImmutableList r0 = r0.A07()
            int r0 = r0.size()
            if (r0 == 0) goto L_0x0442
            com.facebook.messaging.business.inboxads.common.InboxAdsData r4 = r2.A00
            java.lang.Integer r0 = r4.A0D
            int r1 = r0.intValue()
            r0 = -1
            if (r1 == r0) goto L_0x0442
            r3 = r26
            com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo r5 = X.C36081sI.A00(r4)
            com.facebook.messaging.business.inboxads.common.InboxAdsData r0 = r2.A00
            com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo r0 = X.C36081sI.A00(r0)
            com.facebook.messaging.business.common.calltoaction.model.AdCallToAction r0 = r0.A04
            if (r0 != 0) goto L_0x0417
            java.lang.String r0 = "native_format_no_cta"
            r3.add(r0)
        L_0x0417:
            r4 = 0
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r6.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1Yd r4 = (X.C25051Yd) r4
            r0 = 282428461221262(0x100de001b058e, double:1.395382000972323E-309)
            boolean r0 = r4.Aem(r0)
            if (r0 == 0) goto L_0x04b5
            com.facebook.messaging.business.inboxads.common.InboxAdsVideo r1 = r5.A06
            r0 = 0
            if (r1 == 0) goto L_0x0433
            r0 = 1
        L_0x0433:
            if (r0 != 0) goto L_0x0442
            com.facebook.messaging.business.inboxads.common.InboxAdsImage r1 = r5.A05
            r0 = 0
            if (r1 == 0) goto L_0x043b
            r0 = 1
        L_0x043b:
            if (r0 != 0) goto L_0x0442
            java.lang.String r0 = "native_format_no_media"
            r3.add(r0)
        L_0x0442:
            com.google.common.collect.ImmutableList r6 = r26.build()
            boolean r0 = r6.isEmpty()
            if (r0 == 0) goto L_0x045c
            r0 = 0
        L_0x044d:
            if (r0 != 0) goto L_0x03af
            if (r2 == 0) goto L_0x03af
            int r0 = r13 + 1
            r2.A0C(r13)
            r15.add(r2)
            r13 = r0
            goto L_0x03af
        L_0x045c:
            if (r27 == 0) goto L_0x04b2
            java.lang.String r5 = r27.Aby()
            if (r5 == 0) goto L_0x04b2
        L_0x0464:
            r0 = r30
            X.0tS r0 = r0.A00
            java.lang.String r4 = "Inbox ad row missing required fields"
            int r3 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.1ZE r1 = (X.AnonymousClass1ZE) r1
            java.lang.String r0 = "inbox_ad_error"
            X.0bW r1 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r3 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 266(0x10a, float:3.73E-43)
            r3.<init>(r1, r0)
            boolean r0 = r3.A0G()
            if (r0 == 0) goto L_0x04b0
            r3.A0I(r5)
            java.lang.String r0 = "error_message"
            r3.A0D(r0, r4)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = X.C111495Ss.A00(r0)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "error_type"
            r3.A0B(r0, r1)
            java.lang.String r1 = r6.toString()
            java.lang.String r0 = "invalid_fields"
            r3.A0D(r0, r1)
            java.lang.String r0 = "messenger_inbox_ads"
            r3.A0O(r0)
            r3.A06()
        L_0x04b0:
            r0 = 1
            goto L_0x044d
        L_0x04b2:
            java.lang.String r5 = ""
            goto L_0x0464
        L_0x04b5:
            com.facebook.messaging.business.inboxads.common.InboxAdsImage r1 = r5.A05
            r0 = 0
            if (r1 == 0) goto L_0x04bb
            r0 = 1
        L_0x04bb:
            if (r0 != 0) goto L_0x0442
            java.lang.String r0 = "native_format_not_image"
            r3.add(r0)
            goto L_0x0442
        L_0x04c3:
            java.lang.String r31 = r27.Ac6()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r31)
            if (r0 == 0) goto L_0x061c
            java.lang.String r0 = "adId"
            r6.add(r0)
            r23 = 1
        L_0x04d4:
            java.lang.String r33 = r27.AcE()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r33)
            if (r0 == 0) goto L_0x04e5
            java.lang.String r0 = "adTitle"
            r6.add(r0)
            r23 = 1
        L_0x04e5:
            java.lang.String r0 = r27.Ac1()
            java.lang.String r34 = com.google.common.base.Strings.nullToEmpty(r0)
            java.lang.String r35 = r27.Aby()
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r35)
            if (r0 == 0) goto L_0x04fe
            java.lang.String r0 = "adClientToken"
            r6.add(r0)
            r23 = 1
        L_0x04fe:
            java.lang.String r5 = r27.B3R()
            if (r5 != 0) goto L_0x0506
            java.lang.String r5 = ""
        L_0x0506:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r27.Ac8()
            com.facebook.user.model.User r12 = X.C35841ru.A03(r0, r6)
            if (r12 != 0) goto L_0x0512
            r23 = 1
        L_0x0512:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r27.Ac5()
            android.net.Uri r4 = X.C35841ru.A00(r0, r12, r6)
            if (r4 != 0) goto L_0x051e
            r23 = 1
        L_0x051e:
            java.lang.String r1 = r27.AcA()
            java.lang.String r0 = "adPreferenceUri"
            android.net.Uri r3 = X.C35841ru.A01(r1, r0, r6)
            if (r3 != 0) goto L_0x052c
            r23 = 1
        L_0x052c:
            java.lang.String r1 = r27.AcC()
            java.lang.String r0 = "adReportUri"
            android.net.Uri r2 = X.C35841ru.A01(r1, r0, r6)
            if (r2 != 0) goto L_0x053a
            r23 = 1
        L_0x053a:
            java.lang.String r1 = r27.Ac4()
            java.lang.String r0 = "adHideUri"
            android.net.Uri r1 = X.C35841ru.A01(r1, r0, r6)
            if (r1 != 0) goto L_0x0548
            r23 = 1
        L_0x0548:
            java.lang.String r9 = r27.Ac3()
            java.lang.String r0 = "adHelpUri"
            android.net.Uri r0 = X.C35841ru.A01(r9, r0, r6)
            if (r0 != 0) goto L_0x0556
            r23 = 1
        L_0x0556:
            com.google.common.collect.ImmutableList r9 = r27.AcB()
            com.google.common.collect.ImmutableSet r36 = X.C35841ru.A05(r9, r6)
            if (r36 != 0) goto L_0x0562
            r23 = 1
        L_0x0562:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = r27.Abx()
            if (r9 == 0) goto L_0x0618
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = r27.Abx()
            r9 = 1797083984(0x6b1d5350, float:1.9019479E26)
            boolean r22 = r10.getBooleanValue(r9)
        L_0x0573:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = r27.Abx()
            if (r9 == 0) goto L_0x0614
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = r27.Abx()
            r9 = -605065374(0xffffffffdbef6f62, float:-1.34789972E17)
            boolean r21 = r10.getBooleanValue(r9)
        L_0x0584:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = r27.Abx()
            if (r9 == 0) goto L_0x0611
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = r27.Abx()
            r9 = -4850153(0xffffffffffb5fe17, float:NaN)
            boolean r20 = r10.getBooleanValue(r9)
        L_0x0595:
            com.google.common.collect.ImmutableList$Builder r14 = com.google.common.collect.ImmutableList.builder()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = r27.Abx()
            if (r9 == 0) goto L_0x0620
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r16 = r27.Abx()
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r11 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r10 = 2146026906(0x7fe9c59a, float:NaN)
            r9 = -73209712(0xfffffffffba2e890, float:-1.691738E36)
            r17 = r10
            r18 = r11
            r19 = r9
            com.google.common.collect.ImmutableList r9 = r16.A0M(r17, r18, r19)
            X.1Xv r17 = r9.iterator()
        L_0x05b9:
            boolean r9 = r17.hasNext()
            if (r9 == 0) goto L_0x0620
            java.lang.Object r10 = r17.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r10 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r10
            r9 = 954925063(0x38eb0007, float:1.1205678E-4)
            java.lang.String r9 = r10.A0P(r9)
            boolean r9 = com.google.common.base.Platform.stringIsNullOrEmpty(r9)
            if (r9 != 0) goto L_0x05b9
            r9 = 831846208(0x3194f740, float:4.335476E-9)
            java.lang.String r9 = r10.A0P(r9)
            boolean r9 = com.google.common.base.Platform.stringIsNullOrEmpty(r9)
            if (r9 != 0) goto L_0x05b9
            X.2bk r16 = new X.2bk
            r9 = r16
            r9.<init>()
            r9 = 954925063(0x38eb0007, float:1.1205678E-4)
            java.lang.String r11 = r10.A0P(r9)
            r9 = r16
            r9.A01 = r11
            java.lang.String r9 = "text"
            X.C28931fb.A06(r11, r9)
            r9 = 831846208(0x3194f740, float:4.335476E-9)
            java.lang.String r10 = r10.A0P(r9)
            r9 = r16
            r9.A00 = r10
            java.lang.String r9 = "contentType"
            X.C28931fb.A06(r10, r9)
            com.facebook.messaging.business.inboxads.common.InboxAdsQuickReply r9 = new com.facebook.messaging.business.inboxads.common.InboxAdsQuickReply
            r10 = r16
            r9.<init>(r10)
            r14.add(r9)
            goto L_0x05b9
        L_0x0611:
            r20 = 0
            goto L_0x0595
        L_0x0614:
            r21 = 0
            goto L_0x0584
        L_0x0618:
            r22 = 0
            goto L_0x0573
        L_0x061c:
            r23 = 0
            goto L_0x04d4
        L_0x0620:
            boolean r37 = r27.ArD()
            com.google.common.collect.ImmutableList r38 = r27.Ac2()
            r40 = r12
            r39 = r6
            r32 = r12
            com.google.common.collect.ImmutableList r19 = X.C35841ru.A04(r31, r32, r33, r34, r35, r36, r37, r38, r39)
            if (r19 != 0) goto L_0x0636
            r23 = 1
        L_0x0636:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r18 = r27.AcD()
            r9 = 0
            java.lang.String r17 = "adThumbnail"
            if (r18 == 0) goto L_0x0680
            r16 = 0
            java.lang.String r11 = r18.A41()
            java.lang.String r10 = "adThumbnail.uri"
            android.net.Uri r12 = X.C35841ru.A01(r11, r10, r6)
            if (r12 != 0) goto L_0x064f
            r16 = 1
        L_0x064f:
            int r11 = r18.A0Z()
            int r10 = r18.A0e()
            if (r16 != 0) goto L_0x0680
            X.1sA r6 = new X.1sA
            r6.<init>()
            r6.A00 = r11
            r6.A02 = r12
            java.lang.String r11 = "uri"
            r16 = r12
            r17 = r11
            X.C28931fb.A06(r16, r17)
            java.util.Set r9 = r6.A03
            r9.add(r11)
            r6.A01 = r10
            com.facebook.messaging.business.inboxads.common.InboxAdsImage r9 = new com.facebook.messaging.business.inboxads.common.InboxAdsImage
            r9.<init>(r6)
        L_0x0677:
            if (r9 != 0) goto L_0x067b
            r23 = 1
        L_0x067b:
            if (r23 == 0) goto L_0x0686
            r2 = 0
            goto L_0x03e3
        L_0x0680:
            r10 = r17
            r6.add(r10)
            goto L_0x0677
        L_0x0686:
            X.1sF r6 = new X.1sF
            r6.<init>()
            r6.A01 = r1
            java.lang.String r10 = "adHideUri"
            X.C28931fb.A06(r1, r10)
            java.util.Set r1 = r6.A0J
            r1.add(r10)
            r6.A04 = r2
            java.lang.String r10 = "adReportUri"
            X.C28931fb.A06(r2, r10)
            java.util.Set r1 = r6.A0J
            r1.add(r10)
            r6.A03 = r3
            java.lang.String r2 = "adPreferenceUri"
            X.C28931fb.A06(r3, r2)
            java.util.Set r1 = r6.A0J
            r1.add(r2)
            r6.A00 = r0
            java.lang.String r1 = "adHelpUri"
            X.C28931fb.A06(r0, r1)
            java.util.Set r0 = r6.A0J
            r0.add(r1)
            r6.A02 = r4
            java.lang.String r1 = "adIconUri"
            X.C28931fb.A06(r4, r1)
            java.util.Set r0 = r6.A0J
            r0.add(r1)
            r0 = r31
            r6.A0E = r0
            java.lang.String r1 = "adId"
            X.C28931fb.A06(r0, r1)
            r0 = r19
            r6.A07 = r0
            java.lang.String r1 = "adMediaInfos"
            X.C28931fb.A06(r0, r1)
            java.util.Set r0 = r6.A0J
            r0.add(r1)
            com.google.common.collect.ImmutableList r0 = r36.asList()
            r6.A08 = r0
            java.lang.String r1 = "adTypes"
            X.C28931fb.A06(r0, r1)
            java.util.Set r0 = r6.A0J
            r0.add(r1)
            r0 = r35
            r6.A0F = r0
            java.lang.String r1 = "clientToken"
            X.C28931fb.A06(r0, r1)
            r0 = r34
            r6.A0G = r0
            java.lang.String r1 = "description"
            X.C28931fb.A06(r0, r1)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r22)
            r6.A0A = r1
            java.lang.String r0 = "isBusinessActive"
            X.C28931fb.A06(r1, r0)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r20)
            r6.A0B = r1
            java.lang.String r0 = "isDefaultWelcomeMessage"
            X.C28931fb.A06(r1, r0)
            r0 = r40
            r6.A06 = r0
            java.lang.String r1 = "page"
            X.C28931fb.A06(r0, r1)
            java.util.Set r0 = r6.A0J
            r0.add(r1)
            r6.A0H = r5
            java.lang.String r0 = "postclickSocialContext"
            X.C28931fb.A06(r5, r0)
            com.google.common.collect.ImmutableList r0 = r14.build()
            r6.A09 = r0
            java.lang.String r1 = "quickReplies"
            X.C28931fb.A06(r0, r1)
            java.util.Set r0 = r6.A0J
            r0.add(r1)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r21)
            r6.A0C = r1
            java.lang.String r0 = "shouldShowGetStarted"
            X.C28931fb.A06(r1, r0)
            r6.A05 = r9
            java.lang.String r1 = "thumbnailImage"
            X.C28931fb.A06(r9, r1)
            java.util.Set r0 = r6.A0J
            r0.add(r1)
            r0 = r33
            r6.A0I = r0
            java.lang.String r1 = "title"
            X.C28931fb.A06(r0, r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r29)
            r6.A0D = r1
            java.lang.String r0 = "uiFormat"
            X.C28931fb.A06(r1, r0)
            com.facebook.messaging.business.inboxads.common.InboxAdsData r1 = new com.facebook.messaging.business.inboxads.common.InboxAdsData
            r1.<init>(r6)
            com.facebook.messaging.business.inboxads.common.InboxAdsItem r2 = new com.facebook.messaging.business.inboxads.common.InboxAdsItem
            r2.<init>(r8, r7, r1)
            goto L_0x03e3
        L_0x0772:
            if (r27 == 0) goto L_0x0788
            java.lang.String r3 = r27.Ac6()
            if (r3 == 0) goto L_0x0788
        L_0x077a:
            r0 = r30
            X.0tS r2 = r0.A00
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            java.lang.String r0 = "Inbox ad hidden by the user"
            r2.A0C(r3, r0, r1)
            r0 = 1
            goto L_0x03d2
        L_0x0788:
            java.lang.String r3 = ""
            goto L_0x077a
        L_0x078b:
            r0 = 0
            goto L_0x0791
        L_0x078d:
            com.google.common.collect.ImmutableList r0 = r15.build()
        L_0x0791:
            X.12V r4 = new X.12V
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x0798:
            java.util.List r4 = A05(r8)
            int r2 = X.AnonymousClass1Y3.AD9
            X.0UN r1 = r5.A00
            r0 = 11
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.113 r5 = (X.AnonymousClass113) r5
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.SECONDS
            r0 = 815119367(0x3095bc07, float:1.0894617E-9)
            long r0 = r8.getTimeValue(r0)
            long r6 = r2.toMillis(r0)
            int r2 = X.AnonymousClass1Y3.B6q
            X.0UN r1 = r5.A00
            r0 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            com.facebook.prefs.shared.FbSharedPreferences r3 = (com.facebook.prefs.shared.FbSharedPreferences) r3
            X.1Y8 r2 = X.AnonymousClass129.A0W
            r0 = -2147483648(0xffffffff80000000, double:NaN)
            long r1 = r3.At2(r2, r0)
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x07db
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AJ2
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2UH r0 = (X.AnonymousClass2UH) r0
            r0.A01(r4)
        L_0x07db:
            X.12V r4 = new X.12V
            if (r8 == 0) goto L_0x07ee
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r8.A0R()
            if (r1 == 0) goto L_0x07ee
            r0 = 1205803611(0x47df1a5b, float:114228.71)
            com.google.common.collect.ImmutableList r0 = r1.A0K(r0)
            if (r0 != 0) goto L_0x07f0
        L_0x07ee:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
        L_0x07f0:
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x07f5:
            r5 = 0
            if (r8 == 0) goto L_0x08bb
            com.google.common.collect.ImmutableList r0 = r8.A0T()
            if (r0 == 0) goto L_0x08bb
            X.1Xv r9 = r0.iterator()
        L_0x0802:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x08bb
            java.lang.Object r0 = r9.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            X.0xp r3 = r0.A36()
            r4 = 0
            if (r3 == 0) goto L_0x08a3
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -954074566(0xffffffffc721fa3a, float:-41466.227)
            r0 = 14312237(0xda632d, float:2.0055716E-38)
            com.google.common.collect.ImmutableList r1 = r3.A0M(r1, r2, r0)
            boolean r0 = X.C013509w.A02(r1)
            if (r0 != 0) goto L_0x08a3
            com.google.common.collect.ImmutableList$Builder r3 = new com.google.common.collect.ImmutableList$Builder
            r3.<init>()
            java.util.Iterator r7 = r1.iterator()
        L_0x0830:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x089f
            java.lang.Object r6 = r7.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r6
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 1742542581(0x67dd16f5, float:2.088133E24)
            r0 = -592691609(0xffffffffdcac3e67, float:-3.87858465E17)
            com.facebook.graphservice.tree.TreeJNI r0 = r6.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 != 0) goto L_0x089a
            r0 = r4
        L_0x084d:
            if (r0 == 0) goto L_0x0830
            X.2Qw r1 = com.facebook.ui.media.attachments.model.MediaResource.A00()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r1.A0D = r0
            X.28p r0 = X.C421828p.PHOTO
            r1.A0L = r0
            X.2Qx r0 = X.C46652Qx.A03
            r1.A0J = r0
            com.facebook.ui.media.attachments.model.MediaResource r0 = r1.A00()
            X.1TG r1 = com.facebook.messaging.model.messages.Message.A00()
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r0)
            r1.A09(r0)
            com.facebook.messaging.model.messages.Message r6 = r1.A00()
            X.3uj r2 = new X.3uj
            r2.<init>()
            X.2Ul r0 = X.C47342Ul.VIDEO
            r2.A03 = r0
            r2.A02 = r6
            java.lang.String r0 = r6.A0q
            r2.A07 = r0
            long r0 = r6.A03
            r2.A01 = r0
            com.google.common.collect.ImmutableMultimap r0 = r6.A0j
            r2.A06 = r0
            r0 = 0
            r2.A00 = r0
            r0 = 1
            r2.A09 = r0
            com.facebook.messaging.montage.model.MontageCard r0 = r2.A00()
            r3.add(r0)
            goto L_0x0830
        L_0x089a:
            java.lang.String r0 = r0.A41()
            goto L_0x084d
        L_0x089f:
            com.google.common.collect.ImmutableList r4 = r3.build()
        L_0x08a3:
            boolean r0 = X.C013509w.A01(r4)
            if (r0 == 0) goto L_0x0802
            if (r4 != 0) goto L_0x08cb
            X.3ys r2 = new X.3ys
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            r2.<init>(r0)
        L_0x08b2:
            com.facebook.messaging.montage.model.MontageInboxNuxItem r5 = new com.facebook.messaging.montage.model.MontageInboxNuxItem
            com.google.common.collect.ImmutableList r1 = r2.A00
            boolean r0 = r2.A01
            r5.<init>(r1, r0)
        L_0x08bb:
            X.12V r4 = new X.12V
            java.util.List r1 = A05(r8)
            X.0p2 r0 = new X.0p2
            r0.<init>(r5, r1)
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x08cb:
            X.3ys r2 = new X.3ys
            r2.<init>(r4)
            goto L_0x08b2
        L_0x08d1:
            java.lang.Object[] r2 = new java.lang.Object[]{r6}
            java.lang.String r1 = "InboxUnitFetcherHelper"
            java.lang.String r0 = "Deprecated inbox unit type %s"
            X.C010708t.A0Q(r1, r0, r2)
            r2 = 10
            int r1 = X.AnonymousClass1Y3.BQ3
            X.0UN r0 = r5.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0dK r3 = (X.C07380dK) r3
            java.lang.String r2 = "android_messenger_inbox_deprecated_unit_type_"
            java.lang.String r1 = r6.name()
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r0 = r1.toLowerCase(r0)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r2, r0)
            r3.A02(r0)
            goto L_0x0051
        L_0x08fd:
            X.12V r4 = new X.12V
            com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem r0 = new com.facebook.messaging.discovery.model.DiscoverTabGameNuxFooterItem
            r0.<init>(r8)
            r4.<init>(r8, r0)
            goto L_0x0051
        L_0x0909:
            com.google.common.collect.ImmutableList r0 = r25.build()
            return r0
        L_0x090e:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07900eM.A07(com.facebook.graphservice.modelutil.GSTModelShape1S0000000):com.google.common.collect.ImmutableList");
    }

    private C07900eM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(12, r3);
        this.A05 = AnonymousClass0WY.A00(r3);
        this.A02 = C25901aa.A00(r3);
        this.A03 = C26681bq.A00(r3);
        this.A04 = C26691br.A02(r3);
        this.A01 = C04490Ux.A0L(r3);
    }

    private ImmutableMap A04(ImmutableSet immutableSet, boolean z, String str) {
        ImmutableMap.Builder builder = ImmutableMap.builder();
        C07410dQ A012 = ImmutableSet.A01();
        C24971Xv it = immutableSet.iterator();
        while (it.hasNext()) {
            ThreadKey threadKey = (ThreadKey) it.next();
            ThreadSummary A08 = this.A03.A08(threadKey);
            if (A08 == null) {
                FetchThreadResult A0H = this.A04.A0H(ThreadCriteria.A00(threadKey), 0);
                if (A0H == null || A0H == FetchThreadResult.A09) {
                    A08 = null;
                } else {
                    A08 = A0H.A05;
                }
            }
            if (A08 == null) {
                A012.A01(threadKey);
            } else {
                builder.put(threadKey, A08);
            }
        }
        ImmutableSet A042 = A012.build();
        if (z && !A042.isEmpty()) {
            try {
                builder.putAll(((C158777Xh) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BNg, this.A00)).A03(A042, RegularImmutableSet.A05, -1, str).A00);
            } catch (Exception e) {
                ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, this.A00)).softReport("InboxUnitFetcherHelper", e);
                return builder.build();
            }
        }
        return builder.build();
    }

    private static boolean A06(GSTModelShape1S0000000 gSTModelShape1S0000000, boolean z) {
        if (gSTModelShape1S0000000.getIntValue(-1374423487) == 0 || gSTModelShape1S0000000.getIntValue(1050673705) == 0) {
            if (z) {
                gSTModelShape1S0000000.A3m();
            }
        } else if (gSTModelShape1S0000000.A36() != null) {
            return false;
        }
        return true;
    }
}
