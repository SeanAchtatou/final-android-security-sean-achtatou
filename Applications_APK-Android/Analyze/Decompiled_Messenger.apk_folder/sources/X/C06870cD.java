package X;

import java.util.Iterator;

/* renamed from: X.0cD  reason: invalid class name and case insensitive filesystem */
public class C06870cD extends AnonymousClass06W {
    public C06870cD(String str, AnonymousClass06U r2) {
        super(str, r2);
    }

    public C06870cD(String str, AnonymousClass06U r2, String str2, AnonymousClass06U r4) {
        super(str, r2, str2, r4);
    }

    public C06870cD(String str, AnonymousClass06U r2, String str2, AnonymousClass06U r4, String str3, AnonymousClass06U r6) {
        super(str, r2, str2, r4, str3, r6);
    }

    public C06870cD(String str, AnonymousClass06U r2, String str2, AnonymousClass06U r4, String str3, AnonymousClass06U r6, String str4, AnonymousClass06U r8, String str5, AnonymousClass06U r10) {
        super(str, r2, str2, r4, str3, r6, str4, r8, str5, r10);
    }

    public C06870cD(Iterator it) {
        super(it);
    }
}
