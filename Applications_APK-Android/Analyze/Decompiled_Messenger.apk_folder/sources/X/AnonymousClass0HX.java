package X;

import android.os.Process;
import com.facebook.profilo.writer.NativeTraceWriter;

/* renamed from: X.0HX  reason: invalid class name */
public final class AnonymousClass0HX extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.profilo.logger.LoggerWorkerThread";
    private final NativeTraceWriter A00;

    public void run() {
        Process.setThreadPriority(5);
        this.A00.loop();
    }

    public AnonymousClass0HX(NativeTraceWriter nativeTraceWriter) {
        super("Prflo:Logger");
        this.A00 = nativeTraceWriter;
    }
}
