package X;

/* renamed from: X.18d  reason: invalid class name */
public final class AnonymousClass18d {
    public int[] A00 = new int[10];
    public int A01 = 0;

    public void A00(int i) {
        int i2 = this.A01;
        int[] iArr = this.A00;
        int length = iArr.length;
        if (i2 >= length) {
            int[] iArr2 = new int[(length << 1)];
            System.arraycopy(iArr, 0, iArr2, 0, length);
            this.A00 = iArr2;
        }
        int[] iArr3 = this.A00;
        int i3 = this.A01;
        this.A01 = i3 + 1;
        iArr3[i3] = i;
    }
}
