package X;

import com.google.common.collect.ImmutableMap;
import java.nio.ByteBuffer;

/* renamed from: X.0fk  reason: invalid class name and case insensitive filesystem */
public abstract class C08670fk {
    public C75423jp A01(int i, int i2) {
        C75433jq r5;
        int A01;
        C08660fj r1 = (C08660fj) this;
        int i3 = i - r1.A00;
        if (i3 < 0 || i3 >= r1.A01) {
            return null;
        }
        C08680fl r3 = r1.A06;
        C75433jq r2 = new C75433jq();
        int A02 = r3.A02(16);
        if (A02 == 0 || (A01 = r3.A01(r3.A03(A02) + (i3 << 2))) == r3.A00) {
            r2 = null;
            r5 = null;
        } else {
            ByteBuffer byteBuffer = r3.A01;
            r2.A00 = A01;
            r2.A01 = byteBuffer;
            r5 = r2;
        }
        if (r2 == null) {
            return null;
        }
        ImmutableMap.Builder builder = ImmutableMap.builder();
        C15130um r32 = new C15130um();
        int A022 = r5.A02(4);
        if (A022 != 0) {
            int A012 = r5.A01(A022 + r5.A00);
            ByteBuffer byteBuffer2 = r5.A01;
            r32.A00 = A012;
            r32.A01 = byteBuffer2;
        } else {
            r32 = null;
        }
        if (r32 != null) {
            builder.put(Integer.valueOf(AnonymousClass1ZB.ZERO.ordinal()), C08660fj.A00(r32, i2));
        }
        C15130um r33 = new C15130um();
        int A023 = r5.A02(6);
        if (A023 != 0) {
            int A013 = r5.A01(A023 + r5.A00);
            ByteBuffer byteBuffer3 = r5.A01;
            r33.A00 = A013;
            r33.A01 = byteBuffer3;
        } else {
            r33 = null;
        }
        if (r33 != null) {
            builder.put(Integer.valueOf(AnonymousClass1ZB.ONE.ordinal()), C08660fj.A00(r33, i2));
        }
        C15130um r34 = new C15130um();
        int A024 = r5.A02(8);
        if (A024 != 0) {
            int A014 = r5.A01(A024 + r5.A00);
            ByteBuffer byteBuffer4 = r5.A01;
            r34.A00 = A014;
            r34.A01 = byteBuffer4;
        } else {
            r34 = null;
        }
        if (r34 != null) {
            builder.put(Integer.valueOf(AnonymousClass1ZB.TWO.ordinal()), C08660fj.A00(r34, i2));
        }
        C15130um r35 = new C15130um();
        int A025 = r5.A02(10);
        if (A025 != 0) {
            int A015 = r5.A01(A025 + r5.A00);
            ByteBuffer byteBuffer5 = r5.A01;
            r35.A00 = A015;
            r35.A01 = byteBuffer5;
        } else {
            r35 = null;
        }
        if (r35 != null) {
            builder.put(Integer.valueOf(AnonymousClass1ZB.FEW.ordinal()), C08660fj.A00(r35, i2));
        }
        C15130um r36 = new C15130um();
        int A026 = r5.A02(12);
        if (A026 != 0) {
            int A016 = r5.A01(A026 + r5.A00);
            ByteBuffer byteBuffer6 = r5.A01;
            r36.A00 = A016;
            r36.A01 = byteBuffer6;
        } else {
            r36 = null;
        }
        if (r36 != null) {
            builder.put(Integer.valueOf(AnonymousClass1ZB.MANY.ordinal()), C08660fj.A00(r36, i2));
        }
        C15130um r37 = new C15130um();
        int A027 = r5.A02(14);
        if (A027 != 0) {
            int A017 = r5.A01(A027 + r5.A00);
            ByteBuffer byteBuffer7 = r5.A01;
            r37.A00 = A017;
            r37.A01 = byteBuffer7;
        } else {
            r37 = null;
        }
        if (r37 != null) {
            builder.put(Integer.valueOf(AnonymousClass1ZB.OTHER.ordinal()), C08660fj.A00(r37, i2));
        }
        return new C75423jp(builder.build());
    }

    public String A02(int i, int i2) {
        int A01;
        C08660fj r1 = (C08660fj) this;
        int i3 = i - r1.A04;
        if (i3 < 0 || i3 >= r1.A05) {
            return null;
        }
        C08680fl r3 = r1.A06;
        C15130um r2 = new C15130um();
        int A02 = r3.A02(10);
        if (A02 == 0 || (A01 = r3.A01(r3.A03(A02) + (i3 << 2))) == r3.A02) {
            r2 = null;
        } else {
            ByteBuffer byteBuffer = r3.A01;
            r2.A00 = A01;
            r2.A01 = byteBuffer;
        }
        if (r2 != null) {
            return C08660fj.A00(r2, i2);
        }
        return null;
    }

    public String[] A03(int i, int i2) {
        AnonymousClass7WO r5;
        int A01;
        C08660fj r1 = (C08660fj) this;
        int i3 = i - r1.A02;
        String[] strArr = null;
        if (i3 >= 0 && i3 < r1.A03) {
            C08680fl r3 = r1.A06;
            AnonymousClass7WO r2 = new AnonymousClass7WO();
            int A02 = r3.A02(22);
            if (A02 == 0 || (A01 = r3.A01(r3.A03(A02) + (i3 << 2))) == r3.A01) {
                r2 = null;
                r5 = null;
            } else {
                ByteBuffer byteBuffer = r3.A01;
                r2.A00 = A01;
                r2.A01 = byteBuffer;
                r5 = r2;
            }
            if (r2 != null) {
                int A022 = r5.A02(4);
                strArr = new String[(A022 != 0 ? r5.A04(A022) : 0)];
                int i4 = 0;
                while (true) {
                    int A023 = r5.A02(4);
                    if (i4 >= (A023 != 0 ? r5.A04(A023) : 0)) {
                        break;
                    }
                    C15130um r22 = new C15130um();
                    int A024 = r5.A02(4);
                    if (A024 != 0) {
                        int A012 = r5.A01(r5.A03(A024) + (i4 << 2));
                        ByteBuffer byteBuffer2 = r5.A01;
                        r22.A00 = A012;
                        r22.A01 = byteBuffer2;
                    } else {
                        r22 = null;
                    }
                    strArr[i4] = C08660fj.A00(r22, i2);
                    i4++;
                }
            }
        }
        return strArr;
    }
}
