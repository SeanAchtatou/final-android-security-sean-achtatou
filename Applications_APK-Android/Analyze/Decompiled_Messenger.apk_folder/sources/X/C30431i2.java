package X;

import android.content.Context;
import android.util.Pair;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1i2  reason: invalid class name and case insensitive filesystem */
public abstract class C30431i2 implements AnonymousClass062 {
    public AnonymousClass0UN A00;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v9, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v10, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r29v0, types: [java.lang.String] */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0292, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0293, code lost:
        if (r6 != null) goto L_0x0295;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0298, code lost:
        throw r0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.util.Pair A02(com.facebook.messaging.model.threadkey.ThreadKey r31, long r32, int r34) {
        /*
            r30 = this;
            r4 = r30
            X.17y r4 = (X.C193917y) r4
            X.0Tq r0 = r4.A08
            java.lang.Object r21 = r0.get()
            r0 = r21
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0
            r21 = r0
            boolean r0 = r4.A01
            if (r0 != 0) goto L_0x02b0
            if (r21 == 0) goto L_0x02b0
            r5 = 0
            r0 = 0
            r15 = 0
            r11 = 1
            int r2 = (r32 > r5 ? 1 : (r32 == r5 ? 0 : -1))
            r7 = r34
            if (r31 == 0) goto L_0x0054
            r1 = 0
            if (r2 > 0) goto L_0x0025
            r1 = 1
        L_0x0025:
            com.google.common.base.Preconditions.checkArgument(r1)
            r1 = 0
            if (r34 > 0) goto L_0x002c
            r1 = 1
        L_0x002c:
            com.google.common.base.Preconditions.checkArgument(r1)
            X.0W6 r1 = X.C193717w.A0F
            java.lang.String r2 = r1.A00
            java.lang.String r1 = r31.toString()
            java.lang.String[] r1 = new java.lang.String[]{r1}
            java.util.List r1 = java.util.Arrays.asList(r1)
            X.0av r6 = X.C06160ax.A04(r2, r1)
        L_0x0043:
            java.lang.String r5 = "threads LEFT JOIN thread_participants ON "
            X.0W6 r1 = X.C193717w.A09
            java.lang.String r3 = r1.A00
            java.lang.String r2 = " = "
            X.0W6 r1 = X.AnonymousClass182.A05
            java.lang.String r1 = r1.A00
            java.lang.String r23 = X.AnonymousClass08S.A0S(r5, r3, r2, r1)
            goto L_0x0073
        L_0x0054:
            if (r2 <= 0) goto L_0x0063
            X.0W6 r0 = X.C193717w.A0H
            java.lang.String r2 = r0.A00
            java.lang.String r1 = java.lang.String.valueOf(r32)
            X.2jZ r0 = new X.2jZ
            r0.<init>(r2, r1)
        L_0x0063:
            r6 = r0
            X.0W6 r0 = X.C193717w.A0H
            java.lang.String r0 = r0.A04()
            if (r34 <= 0) goto L_0x0043
            java.lang.String r1 = " LIMIT "
            java.lang.String r0 = X.AnonymousClass08S.A0L(r0, r1, r7)
            goto L_0x0043
        L_0x0073:
            X.187 r1 = r4.A04     // Catch:{ 1wY -> 0x0299 }
            X.0Tq r1 = r1.A01     // Catch:{ 1wY -> 0x0299 }
            java.lang.Object r1 = r1.get()     // Catch:{ 1wY -> 0x0299 }
            X.18F r1 = (X.AnonymousClass18F) r1     // Catch:{ 1wY -> 0x0299 }
            r1.getCipherKey()     // Catch:{ 1wY -> 0x0299 }
            X.0Tq r1 = r4.A0A
            java.lang.Object r1 = r1.get()
            X.183 r1 = (X.AnonymousClass183) r1
            android.database.sqlite.SQLiteDatabase r22 = r1.A01()
            if (r22 == 0) goto L_0x02b0
            X.0W6 r1 = X.C193717w.A0C
            java.lang.String r1 = r1.A00
            X.1CG r5 = new X.1CG
            r5.<init>(r1)
            X.0W6 r1 = X.C193717w.A00
            java.lang.String r1 = r1.A00
            X.1CG r3 = new X.1CG
            r3.<init>(r1)
            X.0W6 r1 = X.C193717w.A05
            java.lang.String r2 = r1.A00
            X.1CG r1 = new X.1CG
            r1.<init>(r2)
            X.0av[] r1 = new X.C06140av[]{r5, r3, r1}
            X.1a6 r1 = X.C06160ax.A02(r1)
            if (r6 == 0) goto L_0x00b4
            r1 = r6
        L_0x00b4:
            java.lang.String[] r24 = X.C193917y.A0D
            java.lang.String r25 = r1.A02()
            java.lang.String[] r26 = r1.A04()
            r27 = 0
            r28 = 0
            r29 = r0
            android.database.Cursor r6 = r22.query(r23, r24, r25, r26, r27, r28, r29)
            com.google.common.collect.ImmutableList$Builder r20 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x0290 }
            r20.<init>()     // Catch:{ all -> 0x0290 }
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ all -> 0x0290 }
            r9.<init>()     // Catch:{ all -> 0x0290 }
        L_0x00d2:
            boolean r0 = r6.moveToNext()     // Catch:{ all -> 0x0290 }
            if (r0 == 0) goto L_0x026d
            X.0W6 r0 = X.C193717w.A0F     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r0 = r0.A05(r6)     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.threadkey.ThreadKey r10 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)     // Catch:{ RuntimeException -> 0x0263 }
            com.google.common.base.Preconditions.checkNotNull(r10)     // Catch:{ RuntimeException -> 0x0263 }
            boolean r0 = r10.A0K()     // Catch:{ RuntimeException -> 0x0263 }
            com.google.common.base.Preconditions.checkState(r0)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.C193717w.A0G     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r19 = r0.A05(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.C193717w.A0H     // Catch:{ RuntimeException -> 0x0263 }
            long r2 = r0.A02(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.C193717w.A05     // Catch:{ RuntimeException -> 0x0263 }
            long r0 = r0.A02(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r5 = X.C193717w.A0A     // Catch:{ RuntimeException -> 0x0263 }
            int r18 = r5.A00(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r8 = X.C193717w.A0E     // Catch:{ RuntimeException -> 0x0263 }
            boolean r5 = r8.A06(r6)     // Catch:{ RuntimeException -> 0x0263 }
            if (r5 != 0) goto L_0x014b
            int r8 = r8.A00(r6)     // Catch:{ RuntimeException -> 0x0263 }
        L_0x0110:
            X.0Tq r5 = r4.A09     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.Object r5 = r5.get()     // Catch:{ RuntimeException -> 0x0263 }
            X.23J r5 = (X.AnonymousClass23J) r5     // Catch:{ RuntimeException -> 0x0263 }
            byte[] r13 = r5.A02(r10, r8)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r5 = X.C193717w.A0C     // Catch:{ RuntimeException -> 0x0263 }
            byte[] r8 = r5.A07(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r5 = X.C193717w.A00     // Catch:{ RuntimeException -> 0x0263 }
            byte[] r12 = r5.A07(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.187 r5 = r4.A04     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r17 = r5.A02(r13, r8)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r5 = X.C193717w.A0D     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r8 = r5.A05(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.187 r5 = r4.A04     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r16 = r5.A02(r13, r12)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r5 = X.C193717w.A02     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r14 = r5.A05(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r5 = X.C193717w.A01     // Catch:{ RuntimeException -> 0x0263 }
            int r12 = r5.A00(r6)     // Catch:{ RuntimeException -> 0x0263 }
            r5 = 1
            r13 = 0
            if (r12 != r5) goto L_0x014e
            goto L_0x014d
        L_0x014b:
            r8 = -1
            goto L_0x0110
        L_0x014d:
            r13 = 1
        L_0x014e:
            X.0zh r5 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ RuntimeException -> 0x0263 }
            X.0l8 r12 = X.C10950l8.A05     // Catch:{ RuntimeException -> 0x0263 }
            r5.A0N = r12     // Catch:{ RuntimeException -> 0x0263 }
            r5.A0R = r10     // Catch:{ RuntimeException -> 0x0263 }
            r5.A15 = r11     // Catch:{ RuntimeException -> 0x0263 }
            r12 = r19
            r5.A0p = r12     // Catch:{ RuntimeException -> 0x0263 }
            r5.A0A = r2     // Catch:{ RuntimeException -> 0x0263 }
            r5.A06 = r0     // Catch:{ RuntimeException -> 0x0263 }
            r0 = r18
            r5.A01 = r0     // Catch:{ RuntimeException -> 0x0263 }
            X.0rt r0 = r4.A03     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.messages.MessageDraft r0 = r0.A01(r14)     // Catch:{ RuntimeException -> 0x0263 }
            r5.A0O = r0     // Catch:{ RuntimeException -> 0x0263 }
            r5.A0x = r13     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.C193717w.A07     // Catch:{ RuntimeException -> 0x0263 }
            boolean r0 = r0.A06(r6)     // Catch:{ RuntimeException -> 0x0263 }
            r12 = 0
            r1 = 1
            if (r0 == 0) goto L_0x0215
            long r2 = r10.A01     // Catch:{ RuntimeException -> 0x0263 }
            long r0 = r10.A04     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = com.facebook.messaging.model.threadkey.ThreadKey.A04(r2, r0)     // Catch:{ RuntimeException -> 0x0263 }
            X.0Tq r0 = r4.A07     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.Object r0 = r0.get()     // Catch:{ RuntimeException -> 0x0263 }
            X.1br r0 = (X.C26691br) r0     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.threads.ThreadSummary r13 = r0.A0D(r1)     // Catch:{ RuntimeException -> 0x0263 }
            r3 = 0
            if (r13 != 0) goto L_0x01fe
            int r1 = X.AnonymousClass1Y3.AkG     // Catch:{ RuntimeException -> 0x0263 }
            X.0UN r0 = r4.A00     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r15, r1, r0)     // Catch:{ RuntimeException -> 0x0263 }
            X.1tG r0 = (X.C36521tG) r0     // Catch:{ RuntimeException -> 0x0263 }
            r0.A0K(r10, r15)     // Catch:{ RuntimeException -> 0x0263 }
        L_0x019e:
            r5.A0G = r12     // Catch:{ RuntimeException -> 0x0263 }
            r0 = r16
            r5.A0l = r0     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.C193717w.A09     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r10 = r0.A05(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.AnonymousClass182.A03     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r13 = r0.A05(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.C193717w.A03     // Catch:{ RuntimeException -> 0x0263 }
            long r2 = r0.A02(r6)     // Catch:{ RuntimeException -> 0x0263 }
            X.0W6 r0 = X.C193717w.A04     // Catch:{ RuntimeException -> 0x0263 }
            long r0 = r0.A02(r6)     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.messages.ParticipantInfo r12 = new com.facebook.messaging.model.messages.ParticipantInfo     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.user.model.UserKey r10 = com.facebook.user.model.UserKey.A01(r10)     // Catch:{ RuntimeException -> 0x0263 }
            r12.<init>(r10, r13)     // Catch:{ RuntimeException -> 0x0263 }
            X.1fR r10 = new X.1fR     // Catch:{ RuntimeException -> 0x0263 }
            r10.<init>()     // Catch:{ RuntimeException -> 0x0263 }
            r10.A04 = r12     // Catch:{ RuntimeException -> 0x0263 }
            r10.A01 = r2     // Catch:{ RuntimeException -> 0x0263 }
            r10.A02 = r0     // Catch:{ RuntimeException -> 0x0263 }
            r10.A03 = r0     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.threads.ThreadParticipant r2 = new com.facebook.messaging.model.threads.ThreadParticipant     // Catch:{ RuntimeException -> 0x0263 }
            r2.<init>(r10)     // Catch:{ RuntimeException -> 0x0263 }
            X.0Tq r0 = r4.A08     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.Object r0 = r0.get()     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.messages.ParticipantInfo r3 = new com.facebook.messaging.model.messages.ParticipantInfo     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.user.model.UserKey r1 = r0.A0Q     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r0 = r0.A08()     // Catch:{ RuntimeException -> 0x0263 }
            r3.<init>(r1, r0)     // Catch:{ RuntimeException -> 0x0263 }
            X.1fR r0 = new X.1fR     // Catch:{ RuntimeException -> 0x0263 }
            r0.<init>()     // Catch:{ RuntimeException -> 0x0263 }
            r0.A04 = r3     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.messaging.model.threads.ThreadParticipant r1 = new com.facebook.messaging.model.threads.ThreadParticipant     // Catch:{ RuntimeException -> 0x0263 }
            r1.<init>(r0)     // Catch:{ RuntimeException -> 0x0263 }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r2, r1)     // Catch:{ RuntimeException -> 0x0263 }
            r5.A03(r0)     // Catch:{ RuntimeException -> 0x0263 }
            goto L_0x0221
        L_0x01fe:
            int r1 = X.AnonymousClass1Y3.AkG     // Catch:{ RuntimeException -> 0x0263 }
            X.0UN r0 = r4.A00     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r15, r1, r0)     // Catch:{ RuntimeException -> 0x0263 }
            X.1tG r2 = (X.C36521tG) r2     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r1 = r13.A0H     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r0 = com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason.A01     // Catch:{ RuntimeException -> 0x0263 }
            if (r1 != r0) goto L_0x020f
            r3 = 1
        L_0x020f:
            r2.A0K(r10, r3)     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r12 = r13.A0H     // Catch:{ RuntimeException -> 0x0263 }
            goto L_0x019e
        L_0x0215:
            X.0W6 r0 = X.C193717w.A07     // Catch:{ RuntimeException -> 0x0263 }
            int r0 = r0.A00(r6)     // Catch:{ RuntimeException -> 0x0263 }
            if (r0 != r1) goto L_0x019e
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r12 = com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason.A01     // Catch:{ RuntimeException -> 0x0263 }
            goto L_0x019e
        L_0x0221:
            if (r8 == 0) goto L_0x0237
            r0 = r17
            r5.A0s = r0     // Catch:{ RuntimeException -> 0x0263 }
            com.facebook.user.model.UserKey r0 = r1.A00()     // Catch:{ RuntimeException -> 0x0263 }
            java.lang.String r0 = r0.id     // Catch:{ RuntimeException -> 0x0263 }
            boolean r0 = r8.equals(r0)     // Catch:{ RuntimeException -> 0x0263 }
            if (r0 == 0) goto L_0x0243
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r1.A04     // Catch:{ RuntimeException -> 0x0263 }
            r5.A0P = r0     // Catch:{ RuntimeException -> 0x0263 }
        L_0x0237:
            com.facebook.messaging.model.threads.ThreadSummary r1 = r5.A00()     // Catch:{ RuntimeException -> 0x0263 }
            if (r1 == 0) goto L_0x00d2
            r0 = r20
            r0.add(r1)     // Catch:{ RuntimeException -> 0x0263 }
            goto L_0x0248
        L_0x0243:
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r2.A04     // Catch:{ RuntimeException -> 0x0263 }
            r5.A0P = r0     // Catch:{ RuntimeException -> 0x0263 }
            goto L_0x0237
        L_0x0248:
            X.0W6 r0 = X.C193717w.A0F     // Catch:{ all -> 0x0290 }
            java.lang.String r0 = r0.A05(r6)     // Catch:{ all -> 0x0290 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)     // Catch:{ all -> 0x0290 }
            if (r0 == 0) goto L_0x00d2
            X.18l r2 = r4.A05     // Catch:{ all -> 0x0290 }
            long r0 = r0.A01     // Catch:{ all -> 0x0290 }
            com.facebook.user.model.User r0 = r2.A01(r0)     // Catch:{ all -> 0x0290 }
            if (r0 == 0) goto L_0x00d2
            r9.add(r0)     // Catch:{ all -> 0x0290 }
            goto L_0x00d2
        L_0x0263:
            r2 = move-exception
            java.lang.String r1 = "TincanDbThreadsFetcher"
            java.lang.String r0 = "Unable to convert database entry into a row"
            X.C010708t.A0S(r1, r2, r0)     // Catch:{ all -> 0x0290 }
            goto L_0x00d2
        L_0x026d:
            r0 = r21
            r9.add(r0)     // Catch:{ all -> 0x0290 }
            com.google.common.collect.ImmutableList r2 = r20.build()     // Catch:{ all -> 0x0290 }
            if (r34 <= 0) goto L_0x027e
            int r0 = r2.size()     // Catch:{ all -> 0x0290 }
            if (r0 >= r7) goto L_0x027f
        L_0x027e:
            r15 = 1
        L_0x027f:
            com.facebook.messaging.model.threads.ThreadsCollection r1 = new com.facebook.messaging.model.threads.ThreadsCollection     // Catch:{ all -> 0x0290 }
            r1.<init>(r2, r15)     // Catch:{ all -> 0x0290 }
            com.google.common.collect.ImmutableList r0 = r9.build()     // Catch:{ all -> 0x0290 }
            android.util.Pair r0 = android.util.Pair.create(r1, r0)     // Catch:{ all -> 0x0290 }
            r6.close()
            return r0
        L_0x0290:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0292 }
        L_0x0292:
            r0 = move-exception
            if (r6 == 0) goto L_0x0298
            r6.close()     // Catch:{ all -> 0x0298 }
        L_0x0298:
            throw r0
        L_0x0299:
            r4.A01 = r11
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r0 = X.C06680bu.A14
            r1.<init>(r0)
            X.0Ut r0 = r4.A02
            r0.C4x(r1)
            com.facebook.messaging.model.threads.ThreadsCollection r1 = com.facebook.messaging.model.threads.ThreadsCollection.A02
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            android.util.Pair r0 = android.util.Pair.create(r1, r0)
            return r0
        L_0x02b0:
            com.facebook.messaging.model.threads.ThreadsCollection r1 = com.facebook.messaging.model.threads.ThreadsCollection.A02
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            android.util.Pair r0 = android.util.Pair.create(r1, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30431i2.A02(com.facebook.messaging.model.threadkey.ThreadKey, long, int):android.util.Pair");
    }

    public MessagesCollection A03(ThreadKey threadKey, int i) {
        C193917y r0 = (C193917y) this;
        if (i > 0) {
            return r0.A06.A0A(threadKey, -1, i);
        }
        return null;
    }

    public FetchThreadResult A01(ThreadKey threadKey, int i) {
        Pair A02 = A02(threadKey, -1, -1);
        ThreadsCollection threadsCollection = (ThreadsCollection) A02.first;
        if (threadsCollection.A00.isEmpty()) {
            return FetchThreadResult.A09;
        }
        boolean z = true;
        if (threadsCollection.A02() != 1) {
            z = false;
        }
        Preconditions.checkState(z);
        ThreadSummary A03 = threadsCollection.A03(0);
        C61222yX A002 = FetchThreadResult.A00();
        MessagesCollection A032 = A03(threadKey, i);
        if (A032 != null) {
            A002.A02 = A032;
        }
        A002.A05 = C52192if.TINCAN;
        A002.A00 = ((AnonymousClass06A) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AYB, this.A00)).now();
        A002.A01 = DataFetchDisposition.A0E;
        A002.A04 = A03;
        A002.A06 = (ImmutableList) A02.second;
        return A002.A00();
    }

    public C30431i2(Context context) {
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
