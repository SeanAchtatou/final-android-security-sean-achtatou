package X;

/* renamed from: X.0GL  reason: invalid class name */
public final class AnonymousClass0GL {
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        if (r2 >= 10) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A01(int r2) {
        /*
            r0 = -2
            if (r2 == r0) goto L_0x001b
            r0 = -1
            if (r2 != r0) goto L_0x0009
            java.lang.String r0 = "MODULE_NAME_ROOT"
            return r0
        L_0x0009:
            if (r2 < 0) goto L_0x0010
            r1 = 10
            r0 = 1
            if (r2 < r1) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = X.C02420Er.A02(r2)
            return r0
        L_0x0018:
            java.lang.String r0 = "MODULE_NAME_ERROR"
            return r0
        L_0x001b:
            java.lang.String r0 = "MODULE_NAME_NON_MODULAR_BUILD"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GL.A01(int):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r2 >= 10) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.String r2) {
        /*
            if (r2 != 0) goto L_0x0004
            r0 = -1
            return r0
        L_0x0004:
            int r2 = X.C02420Er.A00(r2)
            if (r2 < 0) goto L_0x000f
            r1 = 10
            r0 = 1
            if (r2 < r1) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 != 0) goto L_0x0013
            r2 = -3
        L_0x0013:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GL.A00(java.lang.String):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        if (r5 >= 10) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A02(int r5) {
        /*
            if (r5 < 0) goto L_0x0007
            r0 = 10
            r4 = 1
            if (r5 < r0) goto L_0x0008
        L_0x0007:
            r4 = 0
        L_0x0008:
            if (r4 != 0) goto L_0x0023
            java.lang.String r1 = A01(r5)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            java.lang.Object[] r3 = new java.lang.Object[]{r1, r0}
            java.lang.String r2 = "AppModuleIndexUtil"
            java.lang.String r1 = "Checking index for %s (%d)"
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r0 = java.lang.String.format(r0, r1, r3)
            X.C010708t.A0K(r2, r0)
        L_0x0023:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GL.A02(int):boolean");
    }
}
