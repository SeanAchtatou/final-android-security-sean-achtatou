package X;

import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import java.util.concurrent.Executor;

/* renamed from: X.1T8  reason: invalid class name */
public final class AnonymousClass1T8 extends C23501Pt {
    public static final C06380bP A0B = new AnonymousClass0ZH(15);
    public int A00;
    public int A01;
    public Uri A02;
    public CallerContext A03;
    public AnonymousClass1QO A04;
    public AnonymousClass0UN A05;
    public AnonymousClass5XQ A06;
    public boolean A07;
    public final AnonymousClass1ZE A08;
    public final AnonymousClass1OM A09;
    public final Executor A0A;

    public static final AnonymousClass1T8 A00(AnonymousClass1XY r2) {
        return new AnonymousClass1T8(r2, AnonymousClass0UU.A02(r2));
    }

    private AnonymousClass1T8(AnonymousClass1XY r3, C001300x r4) {
        AnonymousClass1OM r0;
        this.A05 = new AnonymousClass0UN(1, r3);
        this.A08 = AnonymousClass1ZD.A00(r3);
        this.A0A = AnonymousClass0UX.A0U(r3);
        if (r4.A02 == C001500z.A07) {
            r0 = AnonymousClass1OM.SMALL;
        } else {
            r0 = AnonymousClass1OM.DEFAULT;
        }
        this.A09 = r0;
    }
}
