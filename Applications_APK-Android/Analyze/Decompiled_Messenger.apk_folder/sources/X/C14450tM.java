package X;

import com.facebook.auth.userscope.UserScoped;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

@UserScoped
/* renamed from: X.0tM  reason: invalid class name and case insensitive filesystem */
public final class C14450tM {
    private static C05540Zi A02;
    public final C14480tP A00;
    public final C14490tQ A01;

    public static final C14450tM A00(AnonymousClass1XY r5) {
        C14450tM r0;
        synchronized (C14450tM.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A02.A01();
                    A02.A00 = new C14450tM(C14480tP.A00(r02), C14490tQ.A00(r02));
                }
                C05540Zi r1 = A02;
                r0 = (C14450tM) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public ImmutableList A02(ImmutableList immutableList) {
        if (!this.A01.A00.isEmpty()) {
            immutableList = A01(immutableList, new AnonymousClass4CM(this));
        }
        if (!this.A00.A02.isEmpty()) {
            return A01(immutableList, new AnonymousClass4CN(this));
        }
        return immutableList;
    }

    private C14450tM(C14480tP r1, C14490tQ r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    private static ImmutableList A01(ImmutableList immutableList, Predicate predicate) {
        ImmutableList.Builder builder = ImmutableList.builder();
        for (int i = 0; i < immutableList.size(); i++) {
            Object obj = immutableList.get(i);
            if (predicate.apply(obj)) {
                builder.add(obj);
            }
        }
        return builder.build();
    }
}
