package X;

/* renamed from: X.1CA  reason: invalid class name */
public class AnonymousClass1CA extends C26701bs {
    private static final long serialVersionUID = 4611641304150899138L;
    public final C10030jR _elementType;

    public int containedTypeCount() {
        return 1;
    }

    public String containedTypeName(int i) {
        if (i == 0) {
            return "E";
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj == null || obj.getClass() != getClass()) {
                return false;
            }
            AnonymousClass1CA r5 = (AnonymousClass1CA) obj;
            if (this._class != r5._class || !this._elementType.equals(r5._elementType)) {
                return false;
            }
        }
        return true;
    }

    public boolean isCollectionLikeType() {
        return true;
    }

    public boolean isContainerType() {
        return true;
    }

    public C10030jR _narrow(Class cls) {
        return new AnonymousClass1CA(cls, this._elementType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    public String buildCanonicalName() {
        StringBuilder sb = new StringBuilder();
        sb.append(this._class.getName());
        C10030jR r1 = this._elementType;
        if (r1 != null) {
            sb.append('<');
            sb.append(r1.toCanonical());
            sb.append('>');
        }
        return sb.toString();
    }

    public C10030jR containedType(int i) {
        if (i == 0) {
            return this._elementType;
        }
        return null;
    }

    public C10030jR getContentType() {
        return this._elementType;
    }

    public C10030jR narrowContentsBy(Class cls) {
        C10030jR r1 = this._elementType;
        if (cls == r1._class) {
            return this;
        }
        return new AnonymousClass1CA(this._class, r1.narrowBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public String toString() {
        return "[collection-like type; class " + this._class.getName() + ", contains " + this._elementType + "]";
    }

    public C10030jR widenContentsBy(Class cls) {
        C10030jR r1 = this._elementType;
        if (cls == r1._class) {
            return this;
        }
        return new AnonymousClass1CA(this._class, r1.widenBy(cls), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public AnonymousClass1CA(Class cls, C10030jR r8, Object obj, Object obj2, boolean z) {
        super(cls, r8.hashCode(), obj, obj2, z);
        this._elementType = r8;
    }

    public AnonymousClass1CA withContentTypeHandler(Object obj) {
        return new AnonymousClass1CA(this._class, this._elementType.withTypeHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public AnonymousClass1CA withContentValueHandler(Object obj) {
        return new AnonymousClass1CA(this._class, this._elementType.withValueHandler(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public AnonymousClass1CA withStaticTyping() {
        if (this._asStatic) {
            return this;
        }
        return new AnonymousClass1CA(this._class, this._elementType.withStaticTyping(), this._valueHandler, this._typeHandler, true);
    }

    public AnonymousClass1CA withTypeHandler(Object obj) {
        return new AnonymousClass1CA(this._class, this._elementType, this._valueHandler, obj, this._asStatic);
    }

    public AnonymousClass1CA withValueHandler(Object obj) {
        return new AnonymousClass1CA(this._class, this._elementType, obj, this._typeHandler, this._asStatic);
    }
}
