package X;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Looper;
import com.facebook.litho.ComponentTree;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0p7  reason: invalid class name and case insensitive filesystem */
public final class C12320p7 implements AnonymousClass0p8 {
    public int A00;
    public int A01;
    public ComponentTree A02;
    public boolean A03 = true;
    public final int A04;
    public final C73183fd A05;
    public final C50422dy A06;
    public final C31571ju A07;
    public final C12310p3 A08;
    public final AnonymousClass2FG A09;
    public final C27581dQ A0A;
    public final Object A0B = new Object();
    public final Runnable A0C = new EIL(this);
    public final List A0D;
    public final AtomicBoolean A0E = new AtomicBoolean(false);
    public final AtomicBoolean A0F = new AtomicBoolean(false);
    public final AtomicBoolean A0G = new AtomicBoolean(false);
    public final AtomicReference A0H;
    public final AtomicReference A0I = new AtomicReference();
    public final AtomicReference A0J = new AtomicReference();
    private final C128025zY A0K;
    public volatile EIN A0L;
    public volatile boolean A0M;
    public volatile boolean A0N;
    private volatile boolean A0O;
    private volatile boolean A0P = true;

    public static C17770zR A01(C12320p7 r7, AnonymousClass0p4 r8, C17770zR r9) {
        C17770zR A16;
        String[] strArr = {"changeSetThreadLooper", "component", "lifecycleEventAdapter", "props"};
        BitSet bitSet = new BitSet(4);
        AnonymousClass5zQ r4 = new AnonymousClass5zQ();
        C17770zR r1 = r8.A04;
        if (r1 != null) {
            r4.A07 = r1.A06;
        }
        bitSet.clear();
        if (r9 == null) {
            A16 = null;
        } else {
            A16 = r9.A16();
        }
        r4.A00 = A16;
        bitSet.set(1);
        bitSet.set(0);
        r4.A02 = r7.A0K;
        bitSet.set(2);
        r4.A01 = r7.A0A;
        bitSet.set(3);
        AnonymousClass11F.A0C(4, bitSet, strArr);
        return r4;
    }

    public static boolean A04(C12320p7 r4) {
        if (0 == 0 && r4.A0P && (r4.A0G.get() || !r4.A0F.get() || r4.A0I.get() != r4.A0J.get())) {
            return false;
        }
        r4.A0E.set(false);
        return true;
    }

    public static Context A00(Context context) {
        while ((context instanceof ContextWrapper) && !(context instanceof Activity) && !(context instanceof Application) && !(context instanceof Service)) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        return context;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        if (r3.A0H.get() != null) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A03(java.lang.Object r4, java.lang.String r5) {
        /*
            r3 = this;
            if (r4 == 0) goto L_0x001c
            java.util.concurrent.atomic.AtomicReference r0 = r3.A0J
            java.lang.Object r0 = r0.get()
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x001c
            X.2dy r2 = r3.A06
            java.lang.String r0 = "end_render_early_from_"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r5)
            int r0 = r3.A04
            r2.AYD(r1, r0)
            return
        L_0x001c:
            java.util.concurrent.atomic.AtomicReference r0 = r3.A0J
            r0.set(r4)
            boolean r0 = r3.A0P
            if (r0 == 0) goto L_0x004a
            if (r4 == 0) goto L_0x004a
            boolean r0 = r3.A0N
            boolean r0 = X.EIN.A00(r0, r4)
            if (r0 == 0) goto L_0x004a
            r0 = 0
            if (r0 != 0) goto L_0x004a
            java.util.concurrent.atomic.AtomicBoolean r0 = r3.A0F
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0043
            java.util.concurrent.atomic.AtomicReference r0 = r3.A0H
            java.lang.Object r1 = r0.get()
            r0 = 1
            if (r1 == 0) goto L_0x0044
        L_0x0043:
            r0 = 0
        L_0x0044:
            if (r0 != 0) goto L_0x004b
            boolean r0 = r3.A0N
            if (r0 == 0) goto L_0x004b
        L_0x004a:
            return
        L_0x004b:
            X.2dy r2 = r3.A06
            java.lang.String r0 = "render_from_"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r5)
            int r0 = r3.A04
            r2.AYD(r1, r0)
            X.1ju r0 = r3.A07
            boolean r0 = r0.BG0()
            if (r0 != 0) goto L_0x0072
            java.util.concurrent.atomic.AtomicBoolean r2 = r3.A0E
            r1 = 0
            r0 = 1
            boolean r0 = r2.compareAndSet(r1, r0)
            if (r0 == 0) goto L_0x004a
            X.1ju r1 = r3.A07
            java.lang.Runnable r0 = r3.A0C
            r1.BxN(r0, r5)
            return
        L_0x0072:
            java.util.concurrent.atomic.AtomicBoolean r1 = r3.A0E
            r0 = 0
            r1.set(r0)
            X.1ju r1 = r3.A07
            java.lang.Runnable r0 = r3.A0C
            r1.C1H(r0)
            java.util.concurrent.atomic.AtomicBoolean r1 = r3.A0E
            r0 = 1
            r1.set(r0)
            java.lang.Runnable r0 = r3.A0C
            r0.run()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12320p7.A03(java.lang.Object, java.lang.String):void");
    }

    public void Bzl(Object obj) {
        this.A06.Bzm(BuildConfig.FLAVOR, this.A04);
        A03(obj, "SurfaceManager_receiveAdditional");
    }

    public void Bzo(Object obj) {
        this.A06.Bzp(BuildConfig.FLAVOR, this.A04);
        A03(obj, "SurfaceManager_receiveInitial");
    }

    public C12320p7(C27601dS r6) {
        C12310p3 r0 = new C12310p3(r6.A08);
        r0.A00 = this;
        this.A08 = r0;
        this.A07 = C07840eF.A00();
        this.A0H = new AtomicReference();
        this.A0D = new ArrayList();
        this.A0K = new C128025zY();
        this.A0L = r6.A07;
        EIN ein = this.A0L;
        C12310p3 r3 = this.A08;
        C12310p3 r1 = new C12310p3(r3.A09, r3.A01);
        r1.A00 = r3.A00;
        ein.A00 = r1;
        this.A01 = r6.A01;
        this.A00 = r6.A00;
        this.A0A = r6.A09;
        this.A04 = r6.A05;
        this.A06 = r6.A02;
        this.A0N = r6.A04;
        this.A09 = r6.A03;
        this.A05 = r6.A06;
        this.A02 = A02(this, r6.A01, r6.A00);
        this.A05.C9s(this);
    }

    public static ComponentTree A02(C12320p7 r4, int i, int i2) {
        C73153fa.A00();
        AnonymousClass0p4 r3 = new AnonymousClass0p4(r4.A08);
        C80443sS r2 = new C80443sS();
        C17770zR r1 = r3.A04;
        if (r1 != null) {
            r2.A07 = r1.A06;
        }
        AnonymousClass11J A022 = ComponentTree.A02(r3, A01(r4, r3, r2));
        Looper looper = C07840eF.A02;
        Looper looper2 = looper;
        if (looper != null) {
            A022.A03 = new AnonymousClass11N(looper2);
        }
        A022.A0C = false;
        ComponentTree A002 = A022.A00();
        if (!(i == -1 || i2 == -1)) {
            A002.A0N(i, i2);
        }
        C73153fa.A01();
        return A002;
    }
}
