package X;

/* renamed from: X.1Ma  reason: invalid class name and case insensitive filesystem */
public final class C22581Ma {
    public static C22581Ma A0J;
    private static final Class A0K = C22581Ma.class;
    public C23361Pf A00;
    public AnonymousClass1PZ A01;
    public AnonymousClass1PZ A02;
    public C23341Pd A03;
    public AnonymousClass1PW A04;
    public C22761Ms A05;
    public C30591iK A06;
    public C23351Pe A07;
    private C23361Pf A08;
    private C22791Mv A09;
    private C22831Mz A0A;
    private C30911iq A0B;
    private C30911iq A0C;
    private C30881in A0D;
    private C30881in A0E;
    private AnonymousClass1MZ A0F;
    public final C22971No A0G;
    public final AnonymousClass1PL A0H;
    public final AnonymousClass1PR A0I;

    public static C22791Mv A00(C22581Ma r10) {
        if (r10.A09 == null) {
            C22831Mz A072 = r10.A07();
            C22641Mg r8 = r10.A0H.A0D;
            AnonymousClass1PZ A082 = r10.A08();
            if (!C27508Ddr.A01) {
                try {
                    C27508Ddr.A00 = (C22791Mv) Class.forName("com.facebook.fresco.animation.factory.AnimatedFactoryV2Impl").getConstructor(C22831Mz.class, C22641Mg.class, AnonymousClass1PZ.class, Boolean.TYPE).newInstance(A072, r8, A082, false);
                } catch (Throwable unused) {
                }
                if (C27508Ddr.A00 != null) {
                    C27508Ddr.A01 = true;
                }
            }
            r10.A09 = C27508Ddr.A00;
        }
        return r10.A09;
    }

    private C30911iq A01() {
        if (this.A0B == null) {
            if (this.A00 == null) {
                AnonymousClass1PL r2 = this.A0H;
                this.A00 = r2.A0E.Ab7(r2.A03);
            }
            C23361Pf r4 = this.A00;
            AnonymousClass1PL r0 = this.A0H;
            C30661iR A052 = r0.A0J.A05(r0.A00);
            C23011Nw A012 = this.A0H.A0J.A01();
            AnonymousClass1PL r1 = this.A0H;
            C22641Mg r02 = r1.A0D;
            this.A0B = new C30911iq(r4, A052, A012, r02.AaQ(), r02.AaR(), r1.A0B);
        }
        return this.A0B;
    }

    private C30911iq A02() {
        if (this.A0C == null) {
            C23361Pf A062 = A06();
            AnonymousClass1PL r0 = this.A0H;
            C30661iR A052 = r0.A0J.A05(r0.A00);
            C23011Nw A012 = this.A0H.A0J.A01();
            AnonymousClass1PL r1 = this.A0H;
            C22641Mg r02 = r1.A0D;
            this.A0C = new C30911iq(A062, A052, A012, r02.AaQ(), r02.AaR(), r1.A0B);
        }
        return this.A0C;
    }

    private C30881in A03() {
        if (this.A0E == null) {
            if (this.A02 == null) {
                AnonymousClass1PL r0 = this.A0H;
                C23111Og r4 = r0.A07;
                C14320t5 r3 = r0.A09;
                AnonymousClass1PZ r02 = new AnonymousClass1PZ(new C30891io(), new C30901ip(), r4);
                r3.C0d(r02);
                this.A02 = r02;
            }
            AnonymousClass1PZ r2 = this.A02;
            C22701Mm r03 = this.A0H.A0B;
            r03.C0W(r2);
            this.A0E = new C30881in(r2, new C23331Pc(r03));
        }
        return this.A0E;
    }

    public static C22581Ma A04() {
        C22581Ma r1 = A0J;
        C05520Zg.A03(r1, "ImagePipelineFactory was not initialized!");
        return r1;
    }

    public static synchronized void A05(AnonymousClass1PL r3) {
        synchronized (C22581Ma.class) {
            if (A0J != null) {
                AnonymousClass02w.A01(A0K, "ImagePipelineFactory has already been initialized! `ImagePipelineFactory.initialize(...)` should only be called once to avoid unexpected behavior.");
            }
            A0J = new C22581Ma(r3);
        }
    }

    public C23361Pf A06() {
        if (this.A08 == null) {
            AnonymousClass1PL r0 = this.A0H;
            this.A08 = r0.A0E.Ab7(r0.A04);
        }
        return this.A08;
    }

    public C22831Mz A07() {
        if (this.A0A == null) {
            AnonymousClass1PL r1 = this.A0H;
            AnonymousClass1N0 r2 = r1.A0J;
            if (this.A06 == null) {
                this.A06 = AnonymousClass1NU.A00(r2, r1.A0F.A0G);
            }
            this.A0A = C30651iQ.A00(r2, this.A06, this.A0G);
        }
        return this.A0A;
    }

    public AnonymousClass1PZ A08() {
        if (this.A01 == null) {
            AnonymousClass1PL r0 = this.A0H;
            C23111Og r4 = r0.A06;
            C14320t5 r3 = r0.A09;
            AnonymousClass1PZ r02 = new AnonymousClass1PZ(new AnonymousClass1PX(), r0.A0C, r4);
            r3.C0d(r02);
            this.A01 = r02;
        }
        return this.A01;
    }

    public C30881in A09() {
        if (this.A0D == null) {
            AnonymousClass1PZ A082 = A08();
            C22701Mm r0 = this.A0H.A0B;
            r0.C0P(A082);
            this.A0D = new C30881in(A082, new C30861il(r0));
        }
        return this.A0D;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r0.A0H.A0F.A0I == false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1MZ A0A() {
        /*
            r48 = this;
            r0 = r48
            X.1MZ r1 = r0.A0F
            if (r1 != 0) goto L_0x0133
            X.1MZ r14 = new X.1MZ
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 24
            if (r2 < r1) goto L_0x0018
            X.1PL r1 = r0.A0H
            X.1PU r1 = r1.A0F
            boolean r1 = r1.A0I
            r44 = 1
            if (r1 != 0) goto L_0x001a
        L_0x0018:
            r44 = 0
        L_0x001a:
            X.1PW r1 = r0.A04
            if (r1 != 0) goto L_0x00ef
            X.1PW r5 = new X.1PW
            X.1PL r1 = r0.A0H
            android.content.Context r1 = r1.A01
            android.content.Context r1 = r1.getApplicationContext()
            android.content.ContentResolver r37 = r1.getContentResolver()
            X.1Pd r1 = r0.A03
            if (r1 != 0) goto L_0x00b0
            X.1PL r2 = r0.A0H
            X.1PU r1 = r2.A0F
            X.1PO r1 = r1.A0B
            r38 = r1
            android.content.Context r1 = r2.A01
            r16 = r1
            X.1N0 r1 = r2.A0J
            X.1Nc r17 = r1.A04()
            X.1Ms r1 = r0.A05
            if (r1 != 0) goto L_0x004e
            X.1PL r1 = r0.A0H
            X.1Ms r1 = r1.A0H
            if (r1 == 0) goto L_0x0136
            r0.A05 = r1
        L_0x004e:
            X.1Ms r15 = r0.A05
            X.1PL r3 = r0.A0H
            X.1Oz r13 = r3.A0I
            boolean r12 = r3.A0O
            boolean r11 = r3.A0P
            X.1PU r1 = r3.A0F
            boolean r10 = r1.A0E
            X.1Mg r9 = r3.A0D
            X.1N0 r2 = r3.A0J
            int r1 = r3.A00
            X.1iR r24 = r2.A05(r1)
            X.1in r25 = r0.A09()
            X.1in r26 = r0.A03()
            X.1iq r27 = r0.A01()
            X.1iq r28 = r0.A02()
            X.1PL r1 = r0.A0H
            X.1Mc r8 = r1.A0A
            X.1Mz r30 = r0.A07()
            X.1PL r1 = r0.A0H
            X.1PU r1 = r1.A0F
            int r7 = r1.A06
            int r6 = r1.A05
            boolean r4 = r1.A00
            int r3 = r1.A07
            X.1No r2 = r0.A0G
            boolean r1 = r1.A03
            r29 = r8
            r31 = r7
            r32 = r6
            r33 = r4
            r34 = r3
            r35 = r2
            r36 = r1
            r18 = r15
            r19 = r13
            r20 = r12
            r21 = r11
            r22 = r10
            r23 = r9
            r15 = r38
            X.1Pd r1 = r15.AVy(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36)
            r0.A03 = r1
        L_0x00b0:
            X.1Pd r13 = r0.A03
            X.1PL r3 = r0.A0H
            X.1Ox r12 = r3.A0K
            boolean r11 = r3.A0P
            X.1PU r1 = r3.A0F
            boolean r10 = r1.A0K
            X.1PR r9 = r0.A0I
            boolean r8 = r3.A0O
            boolean r7 = r1.A0H
            r46 = 1
            X.1Pe r2 = r0.A07
            if (r2 != 0) goto L_0x00d6
            X.1Pe r6 = r3.A0L
            X.1Pg r4 = new X.1Pg
            int r3 = r1.A07
            boolean r2 = r1.A0J
            r1 = 0
            r4.<init>(r3, r2, r6, r1)
            r0.A07 = r4
        L_0x00d6:
            X.1Pe r1 = r0.A07
            r36 = r5
            r38 = r13
            r39 = r12
            r40 = r11
            r41 = r10
            r42 = r9
            r43 = r8
            r45 = r7
            r47 = r1
            r36.<init>(r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47)
            r0.A04 = r5
        L_0x00ef:
            X.1PW r7 = r0.A04
            X.1PL r1 = r0.A0H
            java.util.Set r1 = r1.A0N
            java.util.Set r16 = java.util.Collections.unmodifiableSet(r1)
            X.1PL r1 = r0.A0H
            java.util.Set r1 = r1.A0M
            java.util.Set r17 = java.util.Collections.unmodifiableSet(r1)
            X.1PL r1 = r0.A0H
            X.1Og r6 = r1.A08
            X.1in r19 = r48.A09()
            X.1in r20 = r48.A03()
            X.1iq r21 = r48.A01()
            X.1iq r22 = r48.A02()
            X.1PL r5 = r0.A0H
            X.1Mc r4 = r5.A0A
            X.1PU r1 = r5.A0F
            X.1Og r3 = r1.A0A
            X.1Og r2 = r1.A09
            X.1PD r1 = r5.A05
            r15 = r7
            r18 = r6
            r23 = r4
            r24 = r3
            r25 = r2
            r26 = r1
            r27 = r5
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            r0.A0F = r14
        L_0x0133:
            X.1MZ r0 = r0.A0F
            return r0
        L_0x0136:
            X.1Mv r2 = A00(r0)
            r6 = 0
            if (r2 == 0) goto L_0x0167
            X.1PL r1 = r0.A0H
            android.graphics.Bitmap$Config r1 = r1.A02
            X.1Ms r6 = r2.Anz(r1)
            X.1Ms r4 = r2.B9o(r1)
        L_0x0149:
            X.1Mu r3 = new X.1Mu
            X.1iK r1 = r0.A06
            if (r1 != 0) goto L_0x015d
            X.1PL r1 = r0.A0H
            X.1N0 r2 = r1.A0J
            X.1PU r1 = r1.A0F
            boolean r1 = r1.A0G
            X.1iK r1 = X.AnonymousClass1NU.A00(r2, r1)
            r0.A06 = r1
        L_0x015d:
            X.1iK r2 = r0.A06
            r1 = 0
            r3.<init>(r6, r4, r2, r1)
            r0.A05 = r3
            goto L_0x004e
        L_0x0167:
            r4 = r6
            goto L_0x0149
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22581Ma.A0A():X.1MZ");
    }

    public C22581Ma(AnonymousClass1PL r3) {
        AnonymousClass1PR r1;
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("ImagePipelineConfig()");
        }
        C05520Zg.A02(r3);
        this.A0H = r3;
        if (r3.A0F.A0F) {
            r1 = new AnonymousClass5TY(r3.A0D.AaP());
        } else {
            r1 = new AnonymousClass1PQ(r3.A0D.AaP());
        }
        this.A0I = r1;
        AnonymousClass1PS.A04 = r3.A0F.A04;
        this.A0G = new C22971No(r3.A0G);
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
    }
}
