package X;

import java.util.Comparator;

/* renamed from: X.0dH  reason: invalid class name and case insensitive filesystem */
public final class C07350dH implements Comparator {
    public int compare(Object obj, Object obj2) {
        long startTime = ((C34431pZ) obj).getStartTime();
        long startTime2 = ((C34431pZ) obj2).getStartTime();
        if (startTime < startTime2) {
            return -1;
        }
        if (startTime == startTime2) {
            return 0;
        }
        return 1;
    }
}
