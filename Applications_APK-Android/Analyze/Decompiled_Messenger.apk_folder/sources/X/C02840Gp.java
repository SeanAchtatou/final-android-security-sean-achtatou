package X;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0Gp  reason: invalid class name and case insensitive filesystem */
public final class C02840Gp implements C02780Gi {
    public boolean A00 = true;

    public void C2x(AnonymousClass0FM r11, C02910Gx r12) {
        C02500Fe r112 = (C02500Fe) r11;
        long j = r112.coarseTimeMs;
        if (j != 0) {
            r12.AMV("coarse_time_ms", j);
        }
        long j2 = r112.mediumTimeMs;
        if (j2 != 0) {
            r12.AMV("medium_time_ms", j2);
        }
        long j3 = r112.fineTimeMs;
        if (j3 != 0) {
            r12.AMV("fine_time_ms", j3);
        }
        long j4 = r112.wifiScanCount;
        if (j4 != 0) {
            r12.AMV("wifi_scan_count", j4);
        }
        boolean z = this.A00;
        if (z) {
            JSONObject jSONObject = null;
            if (z && r112.isAttributionEnabled && !r112.tagLocationDetails.isEmpty()) {
                try {
                    JSONObject jSONObject2 = new JSONObject();
                    int size = r112.tagLocationDetails.size();
                    for (int i = 0; i < size; i++) {
                        AnonymousClass04b r0 = r112.tagLocationDetails;
                        C03130Kd r8 = (C03130Kd) r0.A09(i);
                        JSONObject jSONObject3 = new JSONObject();
                        jSONObject3.put("coarse_time_ms", r8.A00);
                        jSONObject3.put("medium_time_ms", r8.A02);
                        jSONObject3.put("fine_time_ms", r8.A01);
                        jSONObject2.put((String) r0.A07(i), jSONObject3);
                    }
                    jSONObject = jSONObject2;
                } catch (JSONException e) {
                    AnonymousClass0KZ.A00("LocationMetrics", "Failed to serialize attribution data", e);
                }
            }
            if (jSONObject != null) {
                r12.AMW("location_tag_time_ms", jSONObject.toString());
            }
        }
    }
}
