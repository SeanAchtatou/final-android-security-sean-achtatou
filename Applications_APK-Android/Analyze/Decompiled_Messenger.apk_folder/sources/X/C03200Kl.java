package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0Kl  reason: invalid class name and case insensitive filesystem */
public final class C03200Kl extends BroadcastReceiver {
    public final /* synthetic */ C03210Km A00;

    public C03200Kl(C03210Km r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(869533612);
        this.A00.A00();
        C000700l.A0D(intent, -424912613, A01);
    }
}
