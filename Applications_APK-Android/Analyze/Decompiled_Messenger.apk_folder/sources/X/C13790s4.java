package X;

import com.facebook.messaging.notify.MessageRequestNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0s4  reason: invalid class name and case insensitive filesystem */
public final class C13790s4 extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$19";
    public final /* synthetic */ MessageRequestNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C13790s4(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, MessageRequestNotification messageRequestNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = messageRequestNotification;
    }
}
