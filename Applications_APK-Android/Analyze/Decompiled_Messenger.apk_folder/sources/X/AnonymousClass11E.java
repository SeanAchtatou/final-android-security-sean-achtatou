package X;

import com.google.common.collect.AbstractMapBasedMultimap;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.11E  reason: invalid class name */
public class AnonymousClass11E implements Iterator {
    public final Collection A00;
    public final Iterator A01;
    public final /* synthetic */ AnonymousClass11B A02;

    public static void A00(AnonymousClass11E r2) {
        r2.A02.A02();
        if (r2.A02.A00 != r2.A00) {
            throw new ConcurrentModificationException();
        }
    }

    public void remove() {
        this.A01.remove();
        AnonymousClass11B r2 = this.A02;
        AbstractMapBasedMultimap abstractMapBasedMultimap = r2.A04;
        abstractMapBasedMultimap.A00--;
        r2.A03();
    }

    public boolean hasNext() {
        A00(this);
        return this.A01.hasNext();
    }

    public Object next() {
        A00(this);
        return this.A01.next();
    }

    public AnonymousClass11E(AnonymousClass11B r3) {
        Iterator it;
        this.A02 = r3;
        Collection collection = r3.A00;
        this.A00 = collection;
        if (collection instanceof List) {
            it = ((List) collection).listIterator();
        } else {
            it = collection.iterator();
        }
        this.A01 = it;
    }

    public AnonymousClass11E(AnonymousClass11B r2, Iterator it) {
        this.A02 = r2;
        this.A00 = r2.A00;
        this.A01 = it;
    }
}
