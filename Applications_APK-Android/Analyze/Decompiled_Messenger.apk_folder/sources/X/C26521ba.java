package X;

import java.lang.reflect.Field;

/* renamed from: X.1ba  reason: invalid class name and case insensitive filesystem */
public final class C26521ba {
    public final Class A00;
    public final Field A01;
    public final Field A02;

    public C26521ba(Object obj) {
        Class<?> cls = obj.getClass();
        this.A00 = cls;
        Field declaredField = cls.getDeclaredField("paused");
        this.A02 = declaredField;
        declaredField.setAccessible(true);
        Field declaredField2 = this.A00.getDeclaredField("activity");
        this.A01 = declaredField2;
        declaredField2.setAccessible(true);
    }
}
