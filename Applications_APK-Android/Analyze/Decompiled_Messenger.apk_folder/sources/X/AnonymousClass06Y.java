package X;

import android.os.Bundle;

/* renamed from: X.06Y  reason: invalid class name */
public interface AnonymousClass06Y {
    void abortBroadcast();

    int getResultCode();

    String getResultData();

    Bundle getResultExtras(boolean z);

    boolean isInitialStickyBroadcast();

    boolean isOrderedBroadcast();

    void setResult(int i, String str, Bundle bundle);

    void setResultCode(int i);

    void setResultData(String str);

    void setResultExtras(Bundle bundle);
}
