package X;

import com.facebook.common.util.TriState;
import com.facebook.inject.InjectorModule;
import com.facebook.user.model.User;

@InjectorModule
/* renamed from: X.13a  reason: invalid class name and case insensitive filesystem */
public final class C184313a extends AnonymousClass0UV {
    private static C04470Uu A00;

    public static final Boolean A01() {
        return true;
    }

    public static final C37791wG A00(AnonymousClass1XY r5) {
        C37791wG r0;
        synchronized (C37791wG.class) {
            C04470Uu A002 = C04470Uu.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r5)) {
                    AnonymousClass1XY r3 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new FNS(r3, C73253fm.A00(r3));
                }
                C04470Uu r1 = A00;
                r0 = (C37791wG) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final Boolean A02(AnonymousClass1XY r5) {
        TriState triState;
        boolean z;
        User A002 = AnonymousClass0WY.A00(r5);
        C001300x A02 = AnonymousClass0UU.A02(r5);
        AnonymousClass1YI A003 = AnonymousClass0WA.A00(r5);
        if (A002 == null) {
            z = false;
        } else if (A02.A01 == C001400y.DEVELOPMENT || A003.Ab9(AnonymousClass1Y3.A30) == (triState = TriState.YES) || A003.Ab9(698) == triState) {
            return true;
        } else {
            z = A002.A14;
        }
        return Boolean.valueOf(z);
    }
}
