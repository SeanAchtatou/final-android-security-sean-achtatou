package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.rti.orca.MainService;

/* renamed from: X.0H1  reason: invalid class name */
public final class AnonymousClass0H1 {
    public final Context A00;
    public final Intent A01;

    public AnonymousClass0H1(Context context) {
        this.A00 = context;
        this.A01 = new Intent(context, MainService.class);
    }
}
