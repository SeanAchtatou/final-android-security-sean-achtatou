package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.10Z  reason: invalid class name */
public final class AnonymousClass10Z {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final Drawable A04;

    public static AnonymousClass10Z A00(Drawable drawable) {
        return new AnonymousClass10Z(drawable, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public AnonymousClass10Z(Drawable drawable, int i, int i2, int i3, int i4) {
        this.A04 = drawable;
        this.A02 = i;
        this.A03 = i2;
        this.A00 = i3;
        this.A01 = i4;
    }
}
