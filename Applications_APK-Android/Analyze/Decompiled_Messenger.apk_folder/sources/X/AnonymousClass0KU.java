package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.0KU  reason: invalid class name */
public final class AnonymousClass0KU {
    public final AnonymousClass0MT A00;
    public final File A01;

    public AnonymousClass0KU(Context context, String str, C01690Bg r6, String str2, String str3, String str4) {
        this.A00 = new AnonymousClass0MT(AnonymousClass08S.A0P(str3, "|", str4), new AnonymousClass0DT(r6), str2);
        this.A01 = new File(context.getFilesDir(), AnonymousClass08S.A0J("mqtt_analytics.", str));
    }
}
