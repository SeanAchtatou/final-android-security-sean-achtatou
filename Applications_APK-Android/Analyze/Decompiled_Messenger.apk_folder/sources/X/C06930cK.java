package X;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.google.common.base.Preconditions;

/* renamed from: X.0cK  reason: invalid class name and case insensitive filesystem */
public final class C06930cK {
    public boolean A00;
    public final C06810c7 A01;
    public final long A02;
    public final BroadcastReceiver A03;
    public final IntentFilter A04;

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("Receiver{");
        sb.append(this.A03);
        sb.append(" filter=");
        sb.append(this.A04);
        sb.append(" looperId=");
        sb.append(this.A02);
        sb.append("}");
        return sb.toString();
    }

    public C06930cK(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver, long j, C06810c7 r5) {
        this.A04 = intentFilter;
        this.A03 = broadcastReceiver;
        this.A02 = j;
        Preconditions.checkNotNull(r5);
        this.A01 = r5;
    }
}
