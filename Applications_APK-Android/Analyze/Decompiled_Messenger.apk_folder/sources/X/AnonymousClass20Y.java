package X;

import android.view.View;
import com.facebook.auth.login.ui.GenericPasswordCredentialsViewGroup;

/* renamed from: X.20Y  reason: invalid class name */
public final class AnonymousClass20Y implements View.OnClickListener {
    public final /* synthetic */ GenericPasswordCredentialsViewGroup A00;

    public AnonymousClass20Y(GenericPasswordCredentialsViewGroup genericPasswordCredentialsViewGroup) {
        this.A00 = genericPasswordCredentialsViewGroup;
    }

    public void onClick(View view) {
        int A05 = C000700l.A05(2134246017);
        GenericPasswordCredentialsViewGroup.clearUser(this.A00);
        C000700l.A0B(-1350199396, A05);
    }
}
