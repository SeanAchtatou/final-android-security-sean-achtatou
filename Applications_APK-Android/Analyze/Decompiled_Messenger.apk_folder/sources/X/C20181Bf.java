package X;

import com.facebook.messaging.inbox2.analytics.InboxSourceLoggingData;

/* renamed from: X.1Bf  reason: invalid class name and case insensitive filesystem */
public final class C20181Bf extends AnonymousClass0UT {
    public C32871mT A00(InboxSourceLoggingData inboxSourceLoggingData) {
        return new C32871mT(this, C06920cI.A00(this), C05040Xk.A00(), inboxSourceLoggingData);
    }

    public C20181Bf(AnonymousClass1XY r1) {
        super(r1);
    }
}
