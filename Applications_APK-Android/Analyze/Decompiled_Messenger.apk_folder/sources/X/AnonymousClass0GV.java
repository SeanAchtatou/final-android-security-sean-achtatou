package X;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.facebook.breakpad.BreakpadManager;

/* renamed from: X.0GV  reason: invalid class name */
public final class AnonymousClass0GV implements AnonymousClass1YQ, C06670bt {
    private final Context A00;
    private final C25051Yd A01;

    public String getSimpleName() {
        return "BreakpadFlagsController";
    }

    public static final AnonymousClass0GV A00(AnonymousClass1XY r1) {
        return new AnonymousClass0GV(r1);
    }

    public static final AnonymousClass0US A01(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.BMz, r1);
    }

    private void A02() {
        long availableBlocks;
        boolean z;
        C25051Yd r2 = this.A01;
        AnonymousClass0XE r4 = AnonymousClass0XE.A05;
        boolean Aer = r2.Aer(281638185468118L, r4);
        Context context = this.A00;
        if (Aer) {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            if (Build.VERSION.SDK_INT >= 18) {
                availableBlocks = statFs.getAvailableBytes();
            } else {
                availableBlocks = ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
            }
            if (availableBlocks > 2147483648L) {
                z = true;
            } else {
                z = false;
            }
            AnonymousClass08X.A05(context, "breakpad_coredump_enabled", z);
        } else {
            AnonymousClass08X.A05(context, "breakpad_coredump_enabled", false);
            if (BreakpadManager.mNativeLibraryName == null) {
                AnonymousClass01q.A08("breakpad");
                BreakpadManager.mNativeLibraryName = "breakpad";
            }
            BreakpadManager.disableCoreDumpingImpl();
        }
        AnonymousClass08X.A05(this.A00, "android_unified_custom_data", this.A01.Aer(281638185533655L, r4));
        AnonymousClass08X.A05(this.A00, "breakpad_write_only_crash_thread", this.A01.Aer(281638185599192L, r4));
        AnonymousClass08X.A04(this.A00, "breakpad_record_libs", (int) this.A01.At4(563113162375362L, r4));
        AnonymousClass08X.A04(this.A00, "breakpad_dump_maps", (int) this.A01.At4(563113162440899L, r4));
        AnonymousClass08X.A05(this.A00, "breakpad_all_maps_interesting", this.A01.Aer(281638185795801L, r4));
        AnonymousClass08X.A05(this.A00, "breakpad_libunwindstack_enabled", this.A01.Aer(281638185861338L, r4));
    }

    private AnonymousClass0GV(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0WT.A00(r2);
        this.A00 = AnonymousClass1YA.A00(r2);
    }

    public int Ai5() {
        return 38;
    }

    public void init() {
        int A03 = C000700l.A03(-1691220334);
        A02();
        C000700l.A09(-1277240493, A03);
    }

    public void BTE(int i) {
        A02();
    }
}
