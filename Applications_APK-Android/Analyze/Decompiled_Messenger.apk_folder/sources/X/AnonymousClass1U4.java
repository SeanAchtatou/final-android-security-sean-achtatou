package X;

import com.facebook.common.jniexecutors.AndroidAsyncExecutorFactory;
import com.facebook.omnistore.MqttProtocolProvider;
import com.facebook.omnistore.OmnistoreCollections;
import com.facebook.omnistore.OmnistoreCustomLogger;
import com.facebook.omnistore.OmnistoreDatabaseCreator;
import com.facebook.omnistore.OmnistoreErrorReporter;
import com.facebook.omnistore.OmnistoreSettings;
import com.facebook.omnistore.OmnistoreXAnalyticsOpener;
import com.facebook.omnistore.logger.FbOmnistoreErrorReporter;
import com.facebook.omnistore.sqlite.AndroidSqliteOmnistoreDatabaseCreator;
import com.facebook.omnistore.util.DeviceIdUtil;
import com.facebook.tigon.nativeservice.authed.NativeAuthedTigonServiceHolder;
import io.card.payment.BuildConfig;
import java.io.File;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1U4  reason: invalid class name */
public final class AnonymousClass1U4 {
    private static volatile AnonymousClass1U4 A0A;
    public final C29401gM A00;
    private final AndroidAsyncExecutorFactory A01;
    private final OmnistoreCustomLogger A02 = null;
    private final OmnistoreErrorReporter A03;
    private final AnonymousClass1UN A04;
    private final C29411gN A05;
    private final C25801aQ A06;
    private final C04310Tq A07;
    private final C04310Tq A08;
    private final C04310Tq A09;

    public static final AnonymousClass1U4 A00(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (AnonymousClass1U4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new AnonymousClass1U4(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public AnonymousClass1V4 A01(MqttProtocolProvider mqttProtocolProvider) {
        C29401gM r0 = this.A00;
        File databasePath = r0.A00.getDatabasePath(C29401gM.A01(r0));
        C29411gN r5 = this.A05;
        OmnistoreSettings omnistoreSettings = new OmnistoreSettings();
        omnistoreSettings.deleteDbOnOpenError = true;
        omnistoreSettings.enableReportChangedBlob = r5.A00.Aem(282540128994974L);
        omnistoreSettings.enableFlatbufferRuntimeVerifier = r5.A00.Aem(282540128798363L);
        omnistoreSettings.deleteObjectsBeforeResnapshot = !r5.A00.Aem(282540128667289L);
        omnistoreSettings.enableSelfCheck = r5.A00.Aem(2306125549342557852L);
        omnistoreSettings.enableDatabaseHealthTracker = true;
        omnistoreSettings.deleteDbIfDbIsCorrupt = true;
        omnistoreSettings.deleteDbIfDbHealthTrackerIsCorrupt = false;
        omnistoreSettings.shouldSkipConnectForPreviousSession = r5.A00.Aem(2306128925186594757L);
        omnistoreSettings.enableServerSideUnsubscribe = false;
        omnistoreSettings.enableIrisAckOptimization = true;
        omnistoreSettings.sendCollectionWithConnectSubscription = true;
        omnistoreSettings.enableApiEventLogger = r5.A00.Aem(282540128732826L);
        omnistoreSettings.dbVacuumInterval = r5.A00.At0(564015105311401L);
        omnistoreSettings.minDeleteDBSizeMB = r5.A00.At0(564015105770154L);
        if (!databasePath.exists()) {
            File parentFile = databasePath.getParentFile();
            if (parentFile == null) {
                C010708t.A0B(C30447Ewc.A00, "The provided database file path (%s) does not seem to have a parent directory", databasePath);
            } else if (!parentFile.exists()) {
                C010708t.A0B(C30447Ewc.A00, "Parent directory of Omnistore database file (%s) does not exist", parentFile);
            } else if (!parentFile.canWrite()) {
                C010708t.A0B(C30447Ewc.A00, "Don't have write access to Omnistore database file directory %s", parentFile);
            }
        } else if (!databasePath.isFile()) {
            C010708t.A0B(C30447Ewc.A00, "Omnistore database file %s exists but is not a regular file", databasePath.getAbsolutePath());
        } else if (!databasePath.canWrite()) {
            C010708t.A0B(C30447Ewc.A00, "Don't have write access to Omnistore database file %s", databasePath.getAbsolutePath());
        }
        databasePath.getAbsolutePath();
        databasePath.exists();
        databasePath.getUsableSpace();
        databasePath.getTotalSpace();
        for (File file : databasePath.getParentFile().listFiles(new AnonymousClass1V3(databasePath))) {
            file.getName();
            file.length();
            file.canRead();
            file.canWrite();
        }
        AnonymousClass1V4 r1 = new AnonymousClass1V4();
        r1.A01 = new OmnistoreCollections();
        OmnistoreDatabaseCreator makeDatabaseCreator = AndroidSqliteOmnistoreDatabaseCreator.makeDatabaseCreator(this.A04);
        C29401gM r02 = this.A00;
        String deviceId = DeviceIdUtil.getDeviceId(r02.A00, Long.valueOf(Long.parseLong(r02.A01.A04)));
        if (deviceId == null) {
            deviceId = BuildConfig.FLAVOR;
        }
        r1.A00 = OmnistoreXAnalyticsOpener.open(makeDatabaseCreator, deviceId, mqttProtocolProvider, this.A03, this.A06.BA9(), omnistoreSettings, (NativeAuthedTigonServiceHolder) this.A09.get(), ((AnonymousClass13Q) this.A08.get()).AoB().toString(), this.A01, (String) this.A07.get(), r1.A01.getFrontend(), this.A02);
        return r1;
    }

    private AnonymousClass1U4(AnonymousClass1XY r2) {
        this.A00 = new C29401gM(r2);
        this.A05 = C29411gN.A00(r2);
        this.A03 = FbOmnistoreErrorReporter.$ul_$xXXcom_facebook_omnistore_logger_FbOmnistoreErrorReporter$xXXFACTORY_METHOD(r2);
        this.A06 = C12230or.A00(r2);
        this.A08 = AnonymousClass0VG.A00(AnonymousClass1Y3.B9n, r2);
        this.A04 = new AnonymousClass1UN(r2);
        this.A09 = AnonymousClass0VG.A00(AnonymousClass1Y3.Af3, r2);
        this.A01 = C13640rn.A01(r2);
        this.A07 = AnonymousClass0XJ.A0K(r2);
    }
}
