package X;

/* renamed from: X.1aO  reason: invalid class name and case insensitive filesystem */
public final class C25781aO implements C25791aP {
    public int A00;
    public int[] A01 = new int[16];

    public void clear() {
        this.A00 = 0;
    }

    public void AMO(int i) {
        int i2 = this.A00;
        int[] iArr = this.A01;
        int length = iArr.length;
        if (i2 >= length) {
            do {
                length <<= 1;
            } while (length <= i2);
            int[] iArr2 = new int[length];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.A01 = iArr2;
        }
        int[] iArr3 = this.A01;
        int i3 = this.A00;
        this.A00 = i3 + 1;
        iArr3[i3] = i;
    }

    public int Ab3(int i) {
        int i2 = this.A00;
        if (i < i2 && i >= 0) {
            return this.A01[i];
        }
        throw new ArrayIndexOutOfBoundsException(AnonymousClass08S.A0B("index:", i, " size:", i2));
    }

    public void C5c(int i, int i2) {
        int i3 = this.A00;
        if (i >= i3 || i < 0) {
            throw new ArrayIndexOutOfBoundsException(AnonymousClass08S.A0B("index:", i, " size:", i3));
        }
        this.A01[i] = i2;
    }

    public int[] CJ1() {
        int i = this.A00;
        if (i == 0) {
            return C25791aP.A00;
        }
        int[] iArr = new int[i];
        System.arraycopy(this.A01, 0, iArr, 0, i);
        return iArr;
    }

    public boolean isEmpty() {
        if (this.A00 == 0) {
            return true;
        }
        return false;
    }

    public int size() {
        return this.A00;
    }
}
