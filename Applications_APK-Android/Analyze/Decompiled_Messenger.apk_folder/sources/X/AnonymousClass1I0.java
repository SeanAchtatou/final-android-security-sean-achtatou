package X;

import com.facebook.messaging.inbox2.items.InboxUnitItem;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1I0  reason: invalid class name */
public abstract class AnonymousClass1I0 {
    public List A00 = new ArrayList();
    public boolean A01;

    public C64663Ct A00() {
        return C64663Ct.A01;
    }

    public boolean A01() {
        return ((C20281Bu) this).A00.A00.Aem(282479999059383L);
    }

    public boolean A02() {
        return true;
    }

    public abstract boolean A03(InboxUnitItem inboxUnitItem);
}
