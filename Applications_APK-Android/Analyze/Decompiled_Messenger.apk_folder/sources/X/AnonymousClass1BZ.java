package X;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;
import java.util.Set;

/* renamed from: X.1BZ  reason: invalid class name */
public final class AnonymousClass1BZ implements Set {
    private static final Integer A01 = 0;
    private static final Object[] A02 = new Object[0];
    public final AnonymousClass04b A00 = new AnonymousClass04b();

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof Set) {
                Set set = (Set) obj;
                if (size() == set.size()) {
                    try {
                        int size = size();
                        int i = 0;
                        while (i < size) {
                            if (set.contains(this.A00.A07(i))) {
                                i++;
                            }
                        }
                    } catch (ClassCastException | NullPointerException unused) {
                    }
                }
                return false;
            }
            return false;
        }
        return true;
    }

    public boolean add(Object obj) {
        if (this.A00.put(obj, A01) == null) {
            return true;
        }
        return false;
    }

    public void clear() {
        this.A00.clear();
    }

    public boolean contains(Object obj) {
        return this.A00.containsKey(obj);
    }

    public boolean containsAll(Collection collection) {
        if (!(collection instanceof List) || !(collection instanceof RandomAccess)) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                }
            }
            return true;
        }
        List list = (List) collection;
        int size = list.size();
        int i = 0;
        while (i < size) {
            if (contains(list.get(i))) {
                i++;
            }
        }
        return true;
        return false;
    }

    public boolean isEmpty() {
        return this.A00.isEmpty();
    }

    public Iterator iterator() {
        return new C70763bF(this);
    }

    public boolean remove(Object obj) {
        int A06 = this.A00.A06(obj);
        if (A06 < 0) {
            return false;
        }
        this.A00.A08(A06);
        return true;
    }

    public boolean removeAll(Collection collection) {
        boolean z;
        if (!(collection instanceof List) || !(collection instanceof RandomAccess)) {
            boolean z2 = false;
            for (Object remove : collection) {
                z2 = z | remove(remove);
            }
        } else {
            List list = (List) collection;
            int size = list.size();
            z = false;
            for (int i = 0; i < size; i++) {
                z |= remove(list.get(i));
            }
        }
        return z;
    }

    public int size() {
        return this.A00.size();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [boolean] */
    /* JADX WARN: Type inference failed for: r3v3, types: [int] */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean addAll(java.util.Collection r5) {
        /*
            r4 = this;
            int r1 = r4.size()
            int r0 = r5.size()
            int r1 = r1 + r0
            X.04b r0 = r4.A00
            r0.A0A(r1)
            boolean r0 = r5 instanceof X.AnonymousClass1BZ
            r3 = 0
            if (r0 == 0) goto L_0x0028
            X.1BZ r5 = (X.AnonymousClass1BZ) r5
            int r2 = r4.size()
            X.04b r1 = r4.A00
            X.04b r0 = r5.A00
            r1.A0B(r0)
            int r0 = r4.size()
            if (r0 == r2) goto L_0x0027
            r3 = 1
        L_0x0027:
            return r3
        L_0x0028:
            boolean r0 = r5 instanceof java.util.List
            if (r0 == 0) goto L_0x0045
            boolean r0 = r5 instanceof java.util.RandomAccess
            if (r0 == 0) goto L_0x0045
            java.util.List r5 = (java.util.List) r5
            int r2 = r5.size()
            r1 = 0
        L_0x0037:
            if (r3 >= r2) goto L_0x0059
            java.lang.Object r0 = r5.get(r3)
            boolean r0 = r4.add(r0)
            r1 = r1 | r0
            int r3 = r3 + 1
            goto L_0x0037
        L_0x0045:
            java.util.Iterator r1 = r5.iterator()
        L_0x0049:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0027
            java.lang.Object r0 = r1.next()
            boolean r0 = r4.add(r0)
            r3 = r3 | r0
            goto L_0x0049
        L_0x0059:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BZ.addAll(java.util.Collection):boolean");
    }

    public int hashCode() {
        int size = size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            Object A07 = this.A00.A07(i2);
            if (A07 != null) {
                i += A07.hashCode();
            }
        }
        return i;
    }

    public boolean retainAll(Collection collection) {
        boolean z = false;
        for (int size = size() - 1; size >= 0; size--) {
            if (!collection.contains(this.A00.A07(size))) {
                this.A00.A08(size);
                z = true;
            }
        }
        return z;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        int size = size();
        StringBuilder sb = new StringBuilder(size * 14);
        sb.append('{');
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object A07 = this.A00.A07(i);
            if (A07 != this) {
                sb.append(A07);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public Object[] toArray() {
        AnonymousClass04b r4 = this.A00;
        int size = r4.size();
        if (size == 0) {
            return A02;
        }
        Object[] objArr = new Object[size];
        for (int i = 0; i < size; i++) {
            objArr[i] = r4.A07(i);
        }
        return objArr;
    }

    public Object[] toArray(Object[] objArr) {
        int size = size();
        if (objArr.length < size) {
            objArr = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), size);
        }
        for (int i = 0; i < size; i++) {
            objArr[i] = this.A00.A07(i);
        }
        if (objArr.length > size) {
            objArr[size] = null;
        }
        return objArr;
    }
}
