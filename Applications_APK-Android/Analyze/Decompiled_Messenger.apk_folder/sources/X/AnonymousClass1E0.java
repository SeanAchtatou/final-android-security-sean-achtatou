package X;

import com.facebook.messaging.livelocation.feature.LiveLocationForegroundService;
import java.util.concurrent.CancellationException;

/* renamed from: X.1E0  reason: invalid class name */
public final class AnonymousClass1E0 extends C06020ai {
    public final /* synthetic */ LiveLocationForegroundService A00;

    public AnonymousClass1E0(LiveLocationForegroundService liveLocationForegroundService) {
        this.A00 = liveLocationForegroundService;
    }

    public void A03(CancellationException cancellationException) {
        AnonymousClass3KD.A05((AnonymousClass3KD) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AvX, this.A00.A01), "messenger_live_location_did_cancel_location_request");
        LiveLocationForegroundService.A00(this.A00);
    }
}
