package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1pV  reason: invalid class name and case insensitive filesystem */
public final class C34391pV implements AnonymousClass0AV {
    private static volatile C34391pV A03;
    private final C001500z A00;
    private final C25051Yd A01;
    private final AnonymousClass13w A02;

    public static final C34391pV A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C34391pV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C34391pV(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public int Adx() {
        return this.A02.A03().A06;
    }

    public int AnM() {
        switch (this.A00.ordinal()) {
            case 1:
                break;
            case 17:
                return this.A02.A03().A0C;
            default:
                int AqL = this.A01.AqL(563589903548927L, this.A02.A03().A0C);
                if (AqL < 60 || AqL > 1800) {
                    return 300;
                }
                return AqL;
        }
        return 300;
    }

    public int Ay8() {
        return this.A02.A03().A0J;
    }

    private C34391pV(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass13w.A00(r2);
        this.A01 = AnonymousClass0WT.A00(r2);
        this.A00 = AnonymousClass0UU.A05(r2);
    }
}
