package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1CL  reason: invalid class name */
public final class AnonymousClass1CL {
    private static volatile AnonymousClass1CL A02;
    public AnonymousClass0UN A00;
    public final AnonymousClass1CR A01;

    public static final AnonymousClass1CL A00(AnonymousClass1XY r6) {
        if (A02 == null) {
            synchronized (AnonymousClass1CL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A02 = new AnonymousClass1CL(applicationInjector, AnonymousClass1CM.A00(applicationInjector), C31361jZ.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
        if (r3.equals("new_design") == false) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0043, code lost:
        if (r3.equals("old_design_refactored") == false) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0028, code lost:
        if (r3.equals("old_design") == false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer A01() {
        /*
            r4 = this;
            X.1CR r3 = r4.A01
            r1 = 845588868432031(0x3010f0021009f, double:4.177764103980402E-309)
            java.lang.String r0 = "android_messenger_search_h1_2019.null_state_render_type"
            java.lang.String r3 = r3.B4E(r1, r0)
            int r1 = r3.hashCode()
            r0 = -439105942(0xffffffffe5d3c66a, float:-1.2500993E23)
            r2 = 1
            if (r1 == r0) goto L_0x003c
            r0 = 303300925(0x1214013d, float:4.670209E-28)
            if (r1 == r0) goto L_0x0032
            r0 = 1585850070(0x5e8626d6, float:4.8333245E18)
            if (r1 != r0) goto L_0x002a
            java.lang.String r0 = "old_design"
            boolean r0 = r3.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x002b
        L_0x002a:
            r1 = -1
        L_0x002b:
            if (r1 == 0) goto L_0x0049
            if (r1 == r2) goto L_0x0046
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            return r0
        L_0x0032:
            java.lang.String r0 = "new_design"
            boolean r0 = r3.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x002b
            goto L_0x002a
        L_0x003c:
            java.lang.String r0 = "old_design_refactored"
            boolean r0 = r3.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x002b
            goto L_0x002a
        L_0x0046:
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            return r0
        L_0x0049:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1CL.A01():java.lang.Integer");
    }

    private AnonymousClass1CL(AnonymousClass1XY r5, AnonymousClass1CM r6, C31361jZ r7) {
        this.A00 = new AnonymousClass0UN(0, r5);
        if (r6.Aen(2306125656716674841L, "android_messenger_search_h2_2018.enable_critical_config_caching")) {
            this.A01 = r7;
            ((C07380dK) AnonymousClass1XX.A03(AnonymousClass1Y3.BQ3, this.A00)).A03(AnonymousClass08S.A0J("android_messenger_search:critical_config_init_state:", AnonymousClass1FL.A00(r7.Aj3())));
            this.A01.Aen(282634618078926L, "android_messenger_search_h1_2018.use_search_item_data_sources_in_serp");
            A01();
            this.A01.Aen(282638915602160L, "android_messenger_search_h1_2019.show_search_tabs");
            this.A01.AqM(564122480607966L, "android_messenger_search_h2_2018.param_1");
            this.A01.AqM(564122480607966L, "android_messenger_search_h2_2018.param_2");
            this.A01.AqM(564122480607966L, "android_messenger_search_h2_2018.param_3");
            this.A01.AqM(564122480607966L, "android_messenger_search_h2_2018.param_4");
            return;
        }
        this.A01 = r6;
    }
}
