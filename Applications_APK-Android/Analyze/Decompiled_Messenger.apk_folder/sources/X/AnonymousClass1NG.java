package X;

import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1NG  reason: invalid class name */
public final class AnonymousClass1NG implements AnonymousClass1NH {
    public final /* synthetic */ AnonymousClass1BE A00;

    public AnonymousClass1NG(AnonymousClass1BE r1) {
        this.A00 = r1;
    }

    public void Bri(InboxUnitThreadItem inboxUnitThreadItem, C21361Gm r3) {
        AnonymousClass1BE r0 = this.A00;
        if (r0 != null) {
            r0.Bfh(inboxUnitThreadItem);
        }
    }
}
