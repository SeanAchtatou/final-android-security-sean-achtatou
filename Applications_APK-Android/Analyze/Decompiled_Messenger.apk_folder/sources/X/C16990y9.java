package X;

import android.graphics.drawable.ColorDrawable;

/* renamed from: X.0y9  reason: invalid class name and case insensitive filesystem */
public final class C16990y9 extends ColorDrawable implements C17000yA {
    public boolean BEd(C17000yA r4) {
        if (this != r4) {
            return (r4 instanceof C16990y9) && getColor() == ((C16990y9) r4).getColor();
        }
        return true;
    }

    public static C16990y9 A00(int i) {
        return new C16990y9(i);
    }

    private C16990y9(int i) {
        super(i);
    }
}
