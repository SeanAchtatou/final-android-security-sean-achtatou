package X;

import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.user.model.UserKey;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0jC  reason: invalid class name */
public final class AnonymousClass0jC {
    private final AnonymousClass0jD A00;
    private final AnonymousClass0oE A01;

    public static final AnonymousClass0jC A00(AnonymousClass1XY r1) {
        return new AnonymousClass0jC(r1);
    }

    private ParticipantInfo A01(JsonNode jsonNode) {
        C25661aC valueOf;
        String str;
        UserKey A02 = UserKey.A02(JSONUtil.A0N(jsonNode.path("user_key")));
        String A0N = JSONUtil.A0N(jsonNode.path("messagingActorType"));
        if (A0N == null) {
            valueOf = C25661aC.UNSET;
        } else {
            valueOf = C25661aC.valueOf(A0N);
        }
        ParticipantInfo participantInfo = new ParticipantInfo(A02, JSONUtil.A0N(jsonNode.path("name")), JSONUtil.A0N(jsonNode.path("email")), JSONUtil.A0N(jsonNode.path("phone")), JSONUtil.A0N(jsonNode.path("smsParticipantFbid")), JSONUtil.A0S(jsonNode.path("is_commerce")), valueOf);
        if (participantInfo.A03 == null) {
            AnonymousClass0oE r2 = this.A01;
            if (A02 != null) {
                str = A02.id;
            } else {
                str = "null_key";
            }
            r2.A01("DbParticipantsSerialization.deserializeParticipantInfoInternal", str);
        }
        return participantInfo;
    }

    public static JsonNode A02(ParticipantInfo participantInfo) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        UserKey userKey = participantInfo.A01;
        if (userKey != null) {
            objectNode.put("user_key", userKey.A06());
        }
        objectNode.put("name", participantInfo.A03);
        objectNode.put("email", participantInfo.A02);
        objectNode.put("phone", participantInfo.A04);
        objectNode.put("smsParticipantFbid", participantInfo.A05);
        objectNode.put("is_commerce", participantInfo.A06);
        objectNode.put("messagingActorType", participantInfo.A00.name());
        return objectNode;
    }

    public ParticipantInfo A03(String str) {
        if (str == null) {
            return null;
        }
        return A01(this.A00.A02(str));
    }

    public ImmutableList A04(String str) {
        if (str == null || str.equals("[]")) {
            return RegularImmutableList.A02;
        }
        JsonNode A02 = this.A00.A02(str);
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = A02.iterator();
        while (it.hasNext()) {
            builder.add((Object) A01((JsonNode) it.next()));
        }
        return builder.build();
    }

    public String A05(List list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayNode.add(A02((ParticipantInfo) it.next()));
        }
        return arrayNode.toString();
    }

    public AnonymousClass0jC(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0jD.A00(r2);
        this.A01 = new AnonymousClass0oE(r2);
    }
}
