package X;

import com.facebook.omnistore.OmnistoreIOException;
import com.facebook.omnistore.sqlite.AndroidSqliteOmnistoreDatabaseCreator;
import com.facebook.omnistore.sqlite.Database;
import java.io.File;

/* renamed from: X.1UN  reason: invalid class name */
public final class AnonymousClass1UN implements AndroidSqliteOmnistoreDatabaseCreator.DatabaseOpener {
    private static final Class A01 = AnonymousClass1UN.class;
    private final C29401gM A00;

    public static final AnonymousClass1UN A00(AnonymousClass1XY r1) {
        return new AnonymousClass1UN(r1);
    }

    private Database A01() {
        C29401gM r7 = this.A00;
        if (!r7.A00.getDatabasePath(C29401gM.A01(r7)).exists()) {
            File[] listFiles = r7.A00.getDir("omnistore", 0).listFiles();
            if (listFiles == null) {
                listFiles = new File[0];
            }
            for (File file : listFiles) {
                file.renameTo(r7.A00.getDatabasePath(file.getName()));
            }
        }
        File file2 = new File(r7.A00.getDatabasePath(C29401gM.A01(r7)) + ".toreset");
        if (file2.exists()) {
            file2.delete();
            r7.A03();
        }
        return new Database(r7.A00.openOrCreateDatabase(C29401gM.A01(r7), 8, null));
    }

    public void deleteDatabaseFiles() {
        this.A00.A03();
    }

    public String getHealthTrackerAbsoluteFilename() {
        C29401gM r0 = this.A00;
        return r0.A00.getDatabasePath(AnonymousClass08S.A0J(C29401gM.A02(r0), "_status.dat")).getAbsolutePath();
    }

    public AnonymousClass1UN(AnonymousClass1XY r2) {
        this.A00 = new C29401gM(r2);
    }

    public AndroidSqliteOmnistoreDatabaseCreator.PreparedDatabase openDatabase(AndroidSqliteOmnistoreDatabaseCreator.SchemaUpdater schemaUpdater) {
        try {
            return schemaUpdater.ensureDbSchema(A01());
        } catch (C66633La | OmnistoreIOException e) {
            C010708t.A0E(A01, e, "Omnistore must delete database", new Object[0]);
            this.A00.A03();
            try {
                return schemaUpdater.ensureDbSchema(A01());
            } catch (Exception e2) {
                throw new RuntimeException(AnonymousClass08S.A0J("Failed to create DB after forced Delete: ", e.getMessage()), e2);
            }
        }
    }
}
