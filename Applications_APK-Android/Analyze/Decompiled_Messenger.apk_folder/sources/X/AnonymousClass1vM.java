package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1vM  reason: invalid class name */
public final class AnonymousClass1vM extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public AnonymousClass52X A01;
    @Comparable(type = 13)
    public MigColorScheme A02;
    @Comparable(type = 5)
    public ImmutableList A03;
    @Comparable(type = 13)
    public String A04;

    public AnonymousClass1vM(Context context) {
        super("M4TranslationPreferenceLayout");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
