package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.18V  reason: invalid class name */
public final class AnonymousClass18V implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18V(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r7) {
        int A002 = AnonymousClass09Y.A00(102500504);
        C193617v r0 = this.A00.A03;
        if (r0 != null) {
            r0.BtS(false, "ads changed");
        }
        AnonymousClass09Y.A01(-151658112, A002);
    }
}
