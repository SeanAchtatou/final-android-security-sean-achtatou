package X;

import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.base.Function;

/* renamed from: X.0ds  reason: invalid class name and case insensitive filesystem */
public final class C07630ds implements Function {
    public final /* synthetic */ MigColorScheme A00;

    public C07630ds(MigColorScheme migColorScheme) {
        this.A00 = migColorScheme;
    }

    public Object apply(Object obj) {
        return new C21531Hr((InboxUnitItem) obj, this.A00);
    }
}
