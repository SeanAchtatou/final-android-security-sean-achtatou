package X;

import com.fasterxml.jackson.databind.JsonDeserializer;

/* renamed from: X.0o0  reason: invalid class name and case insensitive filesystem */
public class C11820o0 implements AnonymousClass1c7 {
    public JsonDeserializer findArrayDeserializer(BMA bma, C10490kF r3, C10120ja r4, C64433Bp r5, JsonDeserializer jsonDeserializer) {
        return null;
    }

    public JsonDeserializer findBeanDeserializer(C10030jR r2, C10490kF r3, C10120ja r4) {
        return null;
    }

    public JsonDeserializer findCollectionDeserializer(C28331ed r2, C10490kF r3, C10120ja r4, C64433Bp r5, JsonDeserializer jsonDeserializer) {
        return null;
    }

    public JsonDeserializer findCollectionLikeDeserializer(AnonymousClass1CA r2, C10490kF r3, C10120ja r4, C64433Bp r5, JsonDeserializer jsonDeserializer) {
        return null;
    }

    public JsonDeserializer findEnumDeserializer(Class cls, C10490kF r3, C10120ja r4) {
        return null;
    }

    public JsonDeserializer findMapDeserializer(C180610a r2, C10490kF r3, C10120ja r4, C422628y r5, C64433Bp r6, JsonDeserializer jsonDeserializer) {
        return null;
    }

    public JsonDeserializer findMapLikeDeserializer(C21991Jm r2, C10490kF r3, C10120ja r4, C422628y r5, C64433Bp r6, JsonDeserializer jsonDeserializer) {
        return null;
    }

    public JsonDeserializer findTreeNodeDeserializer(Class cls, C10490kF r3, C10120ja r4) {
        return null;
    }
}
