package X;

/* renamed from: X.0iY  reason: invalid class name */
public final class AnonymousClass0iY {
    public static final boolean A00 = Boolean.valueOf(AnonymousClass00I.A02("qpl_recorder_enabled")).booleanValue();
    public static final boolean A01;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0015, code lost:
        if ("1".equals(X.AnonymousClass00I.A02("persist.facebook.LogPerf")) != false) goto L_0x0017;
     */
    static {
        /*
            java.lang.String r0 = "is_perf_testing"
            boolean r0 = java.lang.Boolean.getBoolean(r0)
            if (r0 != 0) goto L_0x0017
            java.lang.String r0 = "persist.facebook.LogPerf"
            java.lang.String r1 = X.AnonymousClass00I.A02(r0)
            java.lang.String r0 = "1"
            boolean r1 = r0.equals(r1)
            r0 = 0
            if (r1 == 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            X.AnonymousClass0iY.A01 = r0
            java.lang.String r0 = "qpl_recorder_enabled"
            java.lang.String r0 = X.AnonymousClass00I.A02(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            boolean r0 = r0.booleanValue()
            X.AnonymousClass0iY.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0iY.<clinit>():void");
    }
}
