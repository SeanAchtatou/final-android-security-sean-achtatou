package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.profilepic.PicSquare;

/* renamed from: X.1gu  reason: invalid class name and case insensitive filesystem */
public final class C29741gu implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new PicSquare(parcel);
    }

    public Object[] newArray(int i) {
        return new PicSquare[i];
    }
}
