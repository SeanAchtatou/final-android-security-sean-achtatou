package X;

import com.facebook.secure.intentswitchoff.FbReceiverSwitchOffDI;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1t0  reason: invalid class name and case insensitive filesystem */
public final class C36401t0 extends AnonymousClass1ZS {
    public static final Class A02 = C36401t0.class;
    private static volatile C36401t0 A03;
    public AnonymousClass0UN A00;
    public volatile boolean A01 = false;

    public static final C36401t0 A00(AnonymousClass1XY r6) {
        if (A03 == null) {
            synchronized (C36401t0.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A03 = new C36401t0(applicationInjector, FbReceiverSwitchOffDI.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AFC, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C36401t0(AnonymousClass1XY r3, FbReceiverSwitchOffDI fbReceiverSwitchOffDI, AnonymousClass0US r5) {
        super(fbReceiverSwitchOffDI, r5);
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
