package X;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.05P  reason: invalid class name */
public final class AnonymousClass05P extends AnonymousClass09F {
    public int A00;
    public String A01;
    public final Context A02;

    public AnonymousClass05P(Context context) {
        super(null);
        Context applicationContext = context.getApplicationContext();
        this.A02 = applicationContext != null ? applicationContext : context;
    }

    public void logOnTraceEnd(TraceContext traceContext, C004505k r13) {
        PackageManager packageManager;
        if (this.A01 == null && (packageManager = this.A02.getPackageManager()) != null) {
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(this.A02.getPackageName(), 0);
                this.A01 = packageInfo.versionName;
                this.A00 = packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException | RuntimeException unused) {
            }
        }
        if (this.A01 != null) {
            Logger.writeBytesEntry(0, 1, 57, Logger.writeBytesEntry(0, 1, 56, Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126519, 0, 0), "App version"), this.A01);
            Logger.writeStandardEntry(0, 7, 52, 0, 0, 8126518, 0, (long) this.A00);
        }
    }
}
