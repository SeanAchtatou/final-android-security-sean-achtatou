package X;

import android.content.Context;
import com.google.common.base.Function;

/* renamed from: X.10f  reason: invalid class name and case insensitive filesystem */
public final class C181110f implements Function {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C180810c A01;

    public Object apply(Object obj) {
        String str;
        C16040wO r4 = (C16040wO) obj;
        AnonymousClass10Z B5D = this.A01.A01.A01(r4).B5D(this.A00);
        if (this.A01.A00.A00.Aem(282703337359183L)) {
            C180810c r1 = this.A01;
            if (!r1.A02.booleanValue()) {
                str = r1.A01.A01(r4).B6U(this.A00);
                return new C182310v(r4, B5D, str, this.A01.A01.A01(r4).B5E(this.A00), this.A01.A01.A01(r4).Ae6().Ae7());
            }
        }
        str = null;
        return new C182310v(r4, B5D, str, this.A01.A01.A01(r4).B5E(this.A00), this.A01.A01.A01(r4).Ae6().Ae7());
    }

    public C181110f(C180810c r1, Context context) {
        this.A01 = r1;
        this.A00 = context;
    }
}
