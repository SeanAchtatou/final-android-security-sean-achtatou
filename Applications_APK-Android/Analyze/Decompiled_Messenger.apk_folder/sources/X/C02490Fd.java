package X;

import java.nio.CharBuffer;

/* renamed from: X.0Fd  reason: invalid class name and case insensitive filesystem */
public final class C02490Fd extends C007907e {
    public boolean A00 = false;
    private final ThreadLocal A01 = new ThreadLocal();
    private final ThreadLocal A02 = new ThreadLocal();

    private static long A00(C02560Fk r2) {
        r2.A07(CharBuffer.allocate(32));
        r2.A06();
        long A03 = r2.A03();
        r2.A05();
        return A03;
    }

    public AnonymousClass0FM A03() {
        return new AnonymousClass0Fc();
    }

    public boolean A04(AnonymousClass0FM r6) {
        AnonymousClass0Fc r62 = (AnonymousClass0Fc) r6;
        synchronized (this) {
            C02740Gd.A00(r62, "Null value passed to getSnapshot!");
            if (this.A00) {
                try {
                    C02560Fk r2 = (C02560Fk) this.A01.get();
                    if (r2 == null) {
                        r2 = new C02560Fk("/proc/self/io", 512);
                        this.A01.set(r2);
                    }
                    r2.A04();
                    if (r2.A01) {
                        r62.rcharBytes = A00(r2);
                        r62.wcharBytes = A00(r2);
                        r62.syscrCount = A00(r2);
                        r62.syscwCount = A00(r2);
                        r62.readBytes = A00(r2);
                        r62.writeBytes = A00(r2);
                        r62.cancelledWriteBytes = A00(r2);
                        C02560Fk r3 = (C02560Fk) this.A02.get();
                        if (r3 == null) {
                            r3 = new C02560Fk("/proc/self/stat", 512);
                            this.A02.set(r3);
                        }
                        r3.A04();
                        if (r3.A01) {
                            int i = 0;
                            while (i < 11) {
                                r3.A06();
                                i++;
                            }
                            r62.majorFaults = r3.A03();
                            while (i < 41) {
                                r3.A06();
                                i++;
                            }
                            r62.blkIoTicks = r3.A03();
                            return true;
                        }
                    }
                } catch (AnonymousClass0KX e) {
                    AnonymousClass0KZ.A00("DiskMetricsCollector", "Unable to parse disk field", e);
                }
            }
        }
        return false;
    }
}
