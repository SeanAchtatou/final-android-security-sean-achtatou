package X;

/* renamed from: X.1RS  reason: invalid class name */
public final class AnonymousClass1RS implements C23961Rq {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass1QK A01;
    public final /* synthetic */ AnonymousClass1RQ A02;

    public AnonymousClass1RS(AnonymousClass1RQ r1, AnonymousClass1QK r2, int i) {
        this.A02 = r1;
        this.A01 = r2;
        this.A00 = i;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7, types: [X.1PS, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v8, types: [java.lang.Throwable] */
    /* JADX WARN: Type inference failed for: r1v22 */
    /* JADX WARN: Type inference failed for: r1v23 */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c2, code lost:
        if (r0 != false) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0016, code lost:
        if (r0 == false) goto L_0x0018;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void C47(X.AnonymousClass1NY r28, int r29) {
        /*
            r27 = this;
            r8 = r28
            if (r28 == 0) goto L_0x0082
            r5 = r27
            X.1RQ r0 = r5.A02
            X.1QC r3 = r0.A05
            boolean r0 = r3.A03
            if (r0 != 0) goto L_0x0018
            r2 = 16
            r1 = r29 & r2
            r0 = 0
            if (r1 != r2) goto L_0x0016
            r0 = 1
        L_0x0016:
            if (r0 != 0) goto L_0x0034
        L_0x0018:
            X.1QK r0 = r5.A01
            X.1Q0 r1 = r0.A09
            boolean r0 = r3.A04
            if (r0 != 0) goto L_0x0028
            android.net.Uri r0 = r1.A02
            boolean r0 = X.C006206h.A06(r0)
            if (r0 != 0) goto L_0x0034
        L_0x0028:
            X.1Q1 r2 = r1.A07
            X.36w r1 = r1.A06
            int r0 = r5.A00
            int r0 = X.C52932jv.A00(r2, r1, r8, r0)
            r8.A03 = r0
        L_0x0034:
            X.1QK r0 = r5.A01
            X.1PL r0 = r0.A06
            X.1PU r0 = r0.A0F
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x006b
            X.1RQ r2 = r5.A02
            X.AnonymousClass1NY.A05(r8)
            X.1O3 r1 = r8.A07
            X.1O3 r0 = X.AnonymousClass1SI.A06
            if (r1 != r0) goto L_0x006b
            X.1Px r0 = r2.A01
            android.graphics.Bitmap$Config r0 = r0.A02
            int r4 = X.AnonymousClass1SJ.A00(r0)
            r3 = 104857600(0x6400000, float:3.6111186E-35)
            int r2 = r8.A03
            X.AnonymousClass1NY.A05(r8)
            int r1 = r8.A05
            X.AnonymousClass1NY.A05(r8)
            int r0 = r8.A01
            int r1 = r1 * r0
            int r1 = r1 * r4
        L_0x0061:
            int r0 = r1 / r2
            int r0 = r0 / r2
            if (r0 <= r3) goto L_0x0069
            int r2 = r2 << 1
            goto L_0x0061
        L_0x0069:
            r8.A03 = r2
        L_0x006b:
            X.1RQ r7 = r5.A02
            r4 = r29
            java.lang.String r6 = "DecodeProducer"
            X.AnonymousClass1NY.A05(r8)
            X.1O3 r1 = r8.A07
            X.1O3 r0 = X.AnonymousClass1SI.A06
            if (r1 == r0) goto L_0x0083
            boolean r0 = X.AnonymousClass1QU.A03(r4)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0083
        L_0x0082:
            return
        L_0x0083:
            r1 = r7
            monitor-enter(r1)
            boolean r0 = r7.A00     // Catch:{ all -> 0x022c }
            monitor-exit(r1)
            if (r0 != 0) goto L_0x0082
            boolean r0 = X.AnonymousClass1NY.A07(r8)
            if (r0 == 0) goto L_0x0082
            X.AnonymousClass1NY.A05(r8)
            X.1O3 r0 = r8.A07
            java.lang.String r20 = "unknown"
            if (r0 == 0) goto L_0x00de
            java.lang.String r0 = r0.A01
            r26 = r0
        L_0x009d:
            X.AnonymousClass1NY.A05(r8)
            int r1 = r8.A05
            java.lang.String r2 = "x"
            X.AnonymousClass1NY.A05(r8)
            int r0 = r8.A01
            java.lang.String r19 = X.AnonymousClass08S.A02(r1, r2, r0)
            int r0 = r8.A03
            java.lang.String r21 = java.lang.String.valueOf(r0)
            boolean r17 = X.AnonymousClass1QU.A03(r29)
            if (r17 == 0) goto L_0x00c4
            r3 = 8
            r1 = r29 & r3
            r0 = 0
            if (r1 != r3) goto L_0x00c1
            r0 = 1
        L_0x00c1:
            r11 = 1
            if (r0 == 0) goto L_0x00c5
        L_0x00c4:
            r11 = 0
        L_0x00c5:
            r0 = 4
            r1 = 4
            r0 = r29 & r0
            r10 = 0
            if (r0 != r1) goto L_0x00cd
            r10 = 1
        L_0x00cd:
            X.1QK r0 = r7.A02
            X.1Q0 r0 = r0.A09
            X.36w r0 = r0.A06
            if (r0 == 0) goto L_0x00e1
            int r1 = r0.A03
            int r0 = r0.A02
            java.lang.String r20 = X.AnonymousClass08S.A02(r1, r2, r0)
            goto L_0x00e1
        L_0x00de:
            r26 = r20
            goto L_0x009d
        L_0x00e1:
            X.1RU r5 = r7.A03     // Catch:{ all -> 0x0227 }
            monitor-enter(r5)     // Catch:{ all -> 0x0227 }
            long r0 = r5.A01     // Catch:{ all -> 0x021f }
            long r2 = r5.A02     // Catch:{ all -> 0x021f }
            long r0 = r0 - r2
            monitor-exit(r5)     // Catch:{ all -> 0x0227 }
            X.1QK r2 = r7.A02     // Catch:{ all -> 0x0227 }
            X.1Q0 r2 = r2.A09     // Catch:{ all -> 0x0227 }
            android.net.Uri r2 = r2.A02     // Catch:{ all -> 0x0227 }
            java.lang.String r16 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0227 }
            if (r11 != 0) goto L_0x00fd
            if (r10 != 0) goto L_0x00fd
            int r9 = r7.A08(r8)     // Catch:{ all -> 0x0227 }
            goto L_0x0101
        L_0x00fd:
            int r9 = r8.A08()     // Catch:{ all -> 0x0227 }
        L_0x0101:
            if (r11 != 0) goto L_0x0106
            if (r10 != 0) goto L_0x0106
            goto L_0x0109
        L_0x0106:
            X.1nJ r10 = X.C33271nJ.A03     // Catch:{ all -> 0x0227 }
            goto L_0x010d
        L_0x0109:
            X.1nJ r10 = r7.A09()     // Catch:{ all -> 0x0227 }
        L_0x010d:
            X.1MN r3 = r7.A04     // Catch:{ all -> 0x0227 }
            X.1QK r2 = r7.A02     // Catch:{ all -> 0x0227 }
            r3.Bk8(r2, r6)     // Catch:{ all -> 0x0227 }
            X.1QC r2 = r7.A05     // Catch:{ 1wU -> 0x0182 }
            X.1Ms r5 = r2.A01     // Catch:{ 1wU -> 0x0182 }
            X.1Px r3 = r7.A01     // Catch:{ 1wU -> 0x0182 }
            X.1S9 r2 = r5.AWg(r8, r9, r10, r3)     // Catch:{ 1wU -> 0x0182 }
            int r5 = r8.A03     // Catch:{ Exception -> 0x0180 }
            r3 = 1
            if (r5 == r3) goto L_0x0125
            r4 = r29 | 16
        L_0x0125:
            r16 = r10
            r18 = r26
            r12 = r7
            r13 = r2
            r14 = r0
            java.util.Map r3 = X.AnonymousClass1RQ.A00(r12, r13, r14, r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x0227 }
            X.1MN r1 = r7.A04     // Catch:{ all -> 0x0227 }
            X.1QK r0 = r7.A02     // Catch:{ all -> 0x0227 }
            r1.Bk6(r0, r6, r3)     // Catch:{ all -> 0x0227 }
            if (r2 == 0) goto L_0x0155
            X.1rk r6 = new X.1rk     // Catch:{ all -> 0x0227 }
            X.1QK r1 = r7.A02     // Catch:{ all -> 0x0227 }
            X.1Q0 r0 = r1.A09     // Catch:{ all -> 0x0227 }
            android.net.Uri r5 = r0.A02     // Catch:{ all -> 0x0227 }
            java.lang.Object r3 = r1.A0A     // Catch:{ all -> 0x0227 }
            X.AnonymousClass1NY.A05(r8)     // Catch:{ all -> 0x0227 }
            int r1 = r8.A05     // Catch:{ all -> 0x0227 }
            X.AnonymousClass1NY.A05(r8)     // Catch:{ all -> 0x0227 }
            int r0 = r8.A01     // Catch:{ all -> 0x0227 }
            r8.A08()     // Catch:{ all -> 0x0227 }
            r6.<init>(r5, r3, r1, r0)     // Catch:{ all -> 0x0227 }
            r2.A00 = r6     // Catch:{ all -> 0x0227 }
        L_0x0155:
            X.1QC r0 = r7.A05     // Catch:{ all -> 0x0227 }
            X.1No r0 = r0.A00     // Catch:{ all -> 0x0227 }
            X.1Nq r3 = r0.A00     // Catch:{ all -> 0x0227 }
            r1 = 0
            if (r2 == 0) goto L_0x016f
            X.1Nf r2 = X.AnonymousClass1PS.A06     // Catch:{ all -> 0x0227 }
            boolean r0 = r3.C3V()     // Catch:{ all -> 0x0227 }
            if (r0 == 0) goto L_0x016b
            java.lang.Throwable r1 = new java.lang.Throwable     // Catch:{ all -> 0x0227 }
            r1.<init>()     // Catch:{ all -> 0x0227 }
        L_0x016b:
            X.1PS r1 = X.AnonymousClass1PS.A03(r13, r2, r3, r1)     // Catch:{ all -> 0x0227 }
        L_0x016f:
            boolean r0 = X.AnonymousClass1QU.A03(r4)     // Catch:{ all -> 0x0222 }
            X.AnonymousClass1RQ.A01(r7, r0)     // Catch:{ all -> 0x0222 }
            X.1Qb r0 = r7.A00     // Catch:{ all -> 0x0222 }
            r0.Bgg(r1, r4)     // Catch:{ all -> 0x0222 }
            X.AnonymousClass1PS.A05(r1)     // Catch:{ all -> 0x0227 }
            goto L_0x021b
        L_0x0180:
            r3 = move-exception
            goto L_0x0200
        L_0x0182:
            r11 = move-exception
            X.1NY r2 = r11.mEncodedImage     // Catch:{ Exception -> 0x01fe }
            r18 = r2
            java.lang.String r14 = "ProgressiveDecoder"
            java.lang.String r13 = "%s, {uri: %s, firstEncodedBytes: %s, length: %d}"
            java.lang.String r15 = r11.getMessage()     // Catch:{ Exception -> 0x01fe }
            r3 = 10
            X.1PS r2 = r2.A0A     // Catch:{ Exception -> 0x01fe }
            X.1PS r12 = X.AnonymousClass1PS.A00(r2)     // Catch:{ Exception -> 0x01fe }
            java.lang.String r4 = ""
            if (r12 == 0) goto L_0x01e4
            r2 = r18
            int r2 = r2.A08()     // Catch:{ Exception -> 0x01fe }
            int r9 = java.lang.Math.min(r2, r3)     // Catch:{ Exception -> 0x01fe }
            byte[] r5 = new byte[r9]     // Catch:{ Exception -> 0x01fe }
            java.lang.Object r3 = r12.A0A()     // Catch:{ all -> 0x01dc }
            X.1SS r3 = (X.AnonymousClass1SS) r3     // Catch:{ all -> 0x01dc }
            if (r3 != 0) goto L_0x01b0
            goto L_0x01e1
        L_0x01b0:
            r2 = 0
            r3.read(r2, r5, r2, r9)     // Catch:{ all -> 0x01dc }
            r12.close()     // Catch:{ Exception -> 0x01fe }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01fe }
            int r2 = r9 << 1
            r4.<init>(r2)     // Catch:{ Exception -> 0x01fe }
            r3 = 0
        L_0x01bf:
            if (r3 >= r9) goto L_0x01d7
            byte r2 = r5[r3]     // Catch:{ Exception -> 0x01fe }
            java.lang.Byte r2 = java.lang.Byte.valueOf(r2)     // Catch:{ Exception -> 0x01fe }
            java.lang.Object[] r12 = new java.lang.Object[]{r2}     // Catch:{ Exception -> 0x01fe }
            java.lang.String r2 = "%02X"
            java.lang.String r2 = java.lang.String.format(r2, r12)     // Catch:{ Exception -> 0x01fe }
            r4.append(r2)     // Catch:{ Exception -> 0x01fe }
            int r3 = r3 + 1
            goto L_0x01bf
        L_0x01d7:
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fe }
            goto L_0x01e4
        L_0x01dc:
            r2 = move-exception
            r12.close()     // Catch:{ Exception -> 0x01fe }
            throw r2     // Catch:{ Exception -> 0x01fe }
        L_0x01e1:
            r12.close()     // Catch:{ Exception -> 0x01fe }
        L_0x01e4:
            r2 = r18
            int r2 = r2.A08()     // Catch:{ Exception -> 0x01fe }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x01fe }
            r22 = r15
            r23 = r16
            r24 = r4
            r25 = r2
            java.lang.Object[] r2 = new java.lang.Object[]{r22, r23, r24, r25}     // Catch:{ Exception -> 0x01fe }
            X.AnonymousClass02w.A0E(r14, r13, r2)     // Catch:{ Exception -> 0x01fe }
            throw r11     // Catch:{ Exception -> 0x01fe }
        L_0x01fe:
            r3 = move-exception
            r2 = 0
        L_0x0200:
            r16 = r10
            r18 = r26
            r12 = r7
            r13 = r2
            r14 = r0
            java.util.Map r2 = X.AnonymousClass1RQ.A00(r12, r13, r14, r16, r17, r18, r19, r20, r21)     // Catch:{ all -> 0x0227 }
            X.1MN r1 = r7.A04     // Catch:{ all -> 0x0227 }
            X.1QK r0 = r7.A02     // Catch:{ all -> 0x0227 }
            r1.Bk4(r0, r6, r3, r2)     // Catch:{ all -> 0x0227 }
            r0 = 1
            X.AnonymousClass1RQ.A01(r7, r0)     // Catch:{ all -> 0x0227 }
            X.1Qb r0 = r7.A00     // Catch:{ all -> 0x0227 }
            r0.BYh(r3)     // Catch:{ all -> 0x0227 }
        L_0x021b:
            X.AnonymousClass1NY.A04(r8)
            return
        L_0x021f:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0227 }
            goto L_0x0226
        L_0x0222:
            r0 = move-exception
            X.AnonymousClass1PS.A05(r1)     // Catch:{ all -> 0x0227 }
        L_0x0226:
            throw r0     // Catch:{ all -> 0x0227 }
        L_0x0227:
            r0 = move-exception
            X.AnonymousClass1NY.A04(r8)
            throw r0
        L_0x022c:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1RS.C47(X.1NY, int):void");
    }
}
