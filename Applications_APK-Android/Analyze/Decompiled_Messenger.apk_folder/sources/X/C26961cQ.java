package X;

/* renamed from: X.1cQ  reason: invalid class name and case insensitive filesystem */
public final class C26961cQ {
    public int A00 = 1;

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000c, code lost:
        if (r4.getClass().isArray() == false) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(java.lang.Object r4) {
        /*
            r3 = this;
            r2 = 0
            if (r4 == 0) goto L_0x000e
            java.lang.Class r0 = r4.getClass()
            boolean r1 = r0.isArray()
            r0 = 0
            if (r1 != 0) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            X.AnonymousClass064.A04(r0)
            if (r4 == 0) goto L_0x0018
            int r2 = r4.hashCode()
        L_0x0018:
            int r0 = r3.A00
            int r0 = r0 * 31
            int r0 = r0 + r2
            r3.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26961cQ.A00(java.lang.Object):void");
    }

    public int hashCode() {
        return this.A00;
    }
}
