package X;

/* renamed from: X.1PA  reason: invalid class name */
public final class AnonymousClass1PA implements C23111Og {
    private final AnonymousClass1PB A00 = new AnonymousClass1PB();

    public static final AnonymousClass1PA A00(AnonymousClass1XY r1) {
        C04490Ux.A04(r1);
        AnonymousClass0WT.A00(r1);
        return new AnonymousClass1PA();
    }

    public Object get() {
        C30851ik A002 = this.A00.get();
        return new C30851ik((int) (((double) A002.A02) * 1.0d), A002.A00, A002.A04, A002.A03, A002.A01, A002.A05);
    }
}
