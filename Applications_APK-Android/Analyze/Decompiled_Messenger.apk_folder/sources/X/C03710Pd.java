package X;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/* renamed from: X.0Pd  reason: invalid class name and case insensitive filesystem */
public final class C03710Pd implements SensorEventListener {
    public Sensor A00;
    public SensorManager A01;
    private final AnonymousClass0Pq A02;
    private final C03720Pe A03 = new C03720Pe();

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void A00() {
        Sensor sensor = this.A00;
        if (sensor != null) {
            this.A01.unregisterListener(this, sensor);
            C02670Fv.A00.A06(this, sensor);
            this.A01 = null;
            this.A00 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a0, code lost:
        if (r3.A00 < ((r7 >> 1) + (r7 >> 2))) goto L_0x00a2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSensorChanged(android.hardware.SensorEvent r16) {
        /*
            r15 = this;
            r5 = r16
            float[] r3 = r5.values
            r8 = 0
            r2 = r3[r8]
            r0 = 1
            r1 = r3[r0]
            r0 = 2
            r0 = r3[r0]
            float r2 = r2 * r2
            float r1 = r1 * r1
            float r2 = r2 + r1
            float r0 = r0 * r0
            float r2 = r2 + r0
            double r0 = (double) r2
            double r3 = java.lang.Math.sqrt(r0)
            r1 = 4623507967449235456(0x402a000000000000, double:13.0)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x001e
            r8 = 1
        L_0x001e:
            long r4 = r5.timestamp
            X.0Pe r3 = r15.A03
            r0 = 500000000(0x1dcd6500, double:2.47032823E-315)
            long r13 = r4 - r0
        L_0x0027:
            int r7 = r3.A01
            r0 = 4
            if (r7 < r0) goto L_0x005a
            X.0DN r2 = r3.A03
            if (r2 == 0) goto L_0x005a
            long r0 = r2.A00
            long r11 = r13 - r0
            r9 = 0
            int r0 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r0 <= 0) goto L_0x005a
            boolean r0 = r2.A02
            if (r0 == 0) goto L_0x0044
            int r0 = r3.A00
            int r0 = r0 + -1
            r3.A00 = r0
        L_0x0044:
            int r0 = r7 + -1
            r3.A01 = r0
            X.0DN r0 = r2.A01
            r3.A03 = r0
            if (r0 != 0) goto L_0x0051
            r0 = 0
            r3.A02 = r0
        L_0x0051:
            X.0Pj r1 = r3.A04
            X.0DN r0 = r1.A00
            r2.A01 = r0
            r1.A00 = r2
            goto L_0x0027
        L_0x005a:
            X.0Pj r6 = r3.A04
            X.0DN r1 = r6.A00
            if (r1 != 0) goto L_0x00b4
            X.0DN r1 = new X.0DN
            r1.<init>()
        L_0x0065:
            r1.A00 = r4
            r1.A02 = r8
            r0 = 0
            r1.A01 = r0
            X.0DN r0 = r3.A02
            if (r0 == 0) goto L_0x0072
            r0.A01 = r1
        L_0x0072:
            r3.A02 = r1
            X.0DN r0 = r3.A03
            if (r0 != 0) goto L_0x007a
            r3.A03 = r1
        L_0x007a:
            int r7 = r7 + 1
            r3.A01 = r7
            if (r8 == 0) goto L_0x0086
            int r0 = r3.A00
            int r0 = r0 + 1
            r3.A00 = r0
        L_0x0086:
            if (r1 == 0) goto L_0x00a2
            X.0DN r0 = r3.A03
            if (r0 == 0) goto L_0x00a2
            long r4 = r1.A00
            long r0 = r0.A00
            long r4 = r4 - r0
            r1 = 250000000(0xee6b280, double:1.235164115E-315)
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x00a2
            int r2 = r3.A00
            int r1 = r7 >> 1
            int r0 = r7 >> 2
            int r1 = r1 + r0
            r0 = 1
            if (r2 >= r1) goto L_0x00a3
        L_0x00a2:
            r0 = 0
        L_0x00a3:
            if (r0 == 0) goto L_0x00c6
        L_0x00a5:
            X.0DN r2 = r3.A03
            if (r2 == 0) goto L_0x00b9
            X.0DN r0 = r2.A01
            r3.A03 = r0
            X.0DN r0 = r6.A00
            r2.A01 = r0
            r6.A00 = r2
            goto L_0x00a5
        L_0x00b4:
            X.0DN r0 = r1.A01
            r6.A00 = r0
            goto L_0x0065
        L_0x00b9:
            r0 = 0
            r3.A02 = r0
            r0 = 0
            r3.A01 = r0
            r3.A00 = r0
            X.0Pq r0 = r15.A02
            r0.BC0()
        L_0x00c6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03710Pd.onSensorChanged(android.hardware.SensorEvent):void");
    }

    public C03710Pd(AnonymousClass0Pq r2) {
        this.A02 = r2;
    }
}
