package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.1Tg  reason: invalid class name and case insensitive filesystem */
public final class C24351Tg {
    public static String A01(ImmutableList immutableList) {
        JSONArray jSONArray = new JSONArray();
        int size = immutableList.size();
        for (int i = 0; i < size; i++) {
            jSONArray.put(immutableList.get(i));
        }
        return jSONArray.toString();
    }

    public static ImmutableList A00(String str) {
        if (str.isEmpty()) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = new ImmutableList.Builder();
        try {
            JSONArray jSONArray = new JSONArray(str);
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                builder.add((Object) jSONArray.getString(i));
            }
            return builder.build();
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }
}
