package X;

import android.os.Looper;
import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1cW  reason: invalid class name and case insensitive filesystem */
public final class C27021cW implements C08470fP {
    private static volatile C27021cW A0C;
    public Runnable A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public final int A04;
    public final AnonymousClass1Y6 A05;
    public final C09020gN A06;
    public final boolean A07;
    private final C27031cX A08;
    private final C25051Yd A09;
    private final QuickPerformanceLogger A0A;
    private final Runnable A0B = new C09010gM();

    public void A02(int i) {
        boolean A022;
        synchronized (this) {
            A022 = C004004z.A02(i);
        }
        if (A022) {
            if (0 != 0 && !this.A01) {
                this.A0A.markerCancel(i);
            }
            if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
                this.A05.BxR(new C30211Erc(this, i));
            } else {
                A01(this);
            }
        }
    }

    public static final C27021cW A00(AnonymousClass1XY r4) {
        if (A0C == null) {
            synchronized (C27021cW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r4);
                if (A002 != null) {
                    try {
                        A0C = new C27021cW(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    public static void A01(C27021cW r3) {
        if (r3.A07 && r3.A02) {
            r3.A02 = false;
            r3.A01 = false;
            Runnable runnable = r3.A00;
            if (runnable != null) {
                r3.A05.C1d(runnable);
                r3.A00 = null;
            }
            if (r3.A03) {
                r3.A03 = false;
                r3.A06.A01();
            }
        }
    }

    public void onFrameRendered(int i) {
        if (this.A02) {
            if (AnonymousClass0OU.A04 == null) {
                AnonymousClass0OU.A04 = new AnonymousClass0OU();
            }
            AnonymousClass0OU.A04.A00(this.A0B);
            boolean z = this.A01;
            boolean z2 = false;
            if (i >= 200) {
                z2 = true;
            }
            this.A01 = z2 | z;
            C005505z.A03("ScrollPerf.FrameStarted", 1813266565);
            C005505z.A00(1861370782);
        }
    }

    private C27021cW(AnonymousClass1XY r5) {
        this.A08 = new C27031cX(r5);
        this.A05 = AnonymousClass0UX.A07(r5);
        this.A09 = AnonymousClass0WT.A00(r5);
        this.A0A = AnonymousClass0ZD.A03(r5);
        boolean z = !this.A09.Aem(286461433813833L);
        this.A07 = z;
        if (z) {
            C09020gN A002 = this.A08.A00(true);
            this.A06 = A002;
            A002.A01 = this;
            this.A04 = this.A09.AqL(567936410585234L, 60000);
        }
    }
}
