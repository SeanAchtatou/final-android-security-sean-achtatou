package X;

import android.content.Context;
import java.util.Set;

/* renamed from: X.1PI  reason: invalid class name */
public final class AnonymousClass1PI {
    public C23251Ou A00;
    public C23251Ou A01;
    public AnonymousClass1PD A02;
    public C23111Og A03;
    public C23111Og A04;
    public C23111Og A05;
    public C14320t5 A06;
    public C22831Mz A07;
    public C22601Mc A08;
    public C22701Mm A09;
    public C30831ii A0A;
    public C22641Mg A0B;
    public AnonymousClass1PF A0C;
    public C30641iP A0D = new C22961Nn();
    public C22761Ms A0E;
    public C23301Oz A0F;
    public AnonymousClass1N0 A0G;
    public C23281Ox A0H;
    public Set A0I;
    public Set A0J;
    public boolean A0K = false;
    public boolean A0L = true;
    public final Context A0M;
    public final AnonymousClass1PJ A0N = new AnonymousClass1PJ(this);

    public AnonymousClass1PI(Context context) {
        C05520Zg.A02(context);
        this.A0M = context;
    }
}
