package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.21D  reason: invalid class name */
public final class AnonymousClass21D {
    public final AnonymousClass230 A00;
    public final ImmutableList A01;
    public final String A02;

    public AnonymousClass21D(ImmutableList immutableList, AnonymousClass230 r2, String str) {
        this.A02 = str;
        this.A01 = immutableList;
        this.A00 = r2;
    }
}
