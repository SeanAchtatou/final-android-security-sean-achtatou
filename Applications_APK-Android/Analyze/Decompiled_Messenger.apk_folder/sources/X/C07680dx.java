package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0dx  reason: invalid class name and case insensitive filesystem */
public final class C07680dx implements C07820eD {
    public static final String __redex_internal_original_name = "com.facebook.common.combinedthreadpool.queue.CombinedSimpleTask";
    private C52952jx A00;
    private String A01 = null;
    private final long A02;
    private final AnonymousClass0VS A03;
    private final C04610Vk A04;
    private final Runnable A05;
    private final boolean A06;

    public synchronized C52952jx ATQ() {
        return this.A00;
    }

    public synchronized void C6z(C52952jx r2) {
        Preconditions.checkNotNull(r2);
        this.A00 = r2;
    }

    public C04610Vk AZE() {
        return this.A04;
    }

    public AnonymousClass0VS By8() {
        return this.A03;
    }

    public Object C4G() {
        return this.A05;
    }

    public String C4H() {
        String str;
        if (!this.A06) {
            return AnonymousClass0FD.A01(this.A05);
        }
        synchronized (this) {
            if (this.A01 == null) {
                this.A01 = AnonymousClass0FD.A01(this.A05);
            }
            str = this.A01;
        }
        return str;
    }

    public Integer C4I() {
        return AnonymousClass07B.A00;
    }

    public long C5W() {
        return this.A02;
    }

    public void run() {
        this.A05.run();
    }

    public C07680dx(Runnable runnable, AnonymousClass0VS r3, C04610Vk r4, long j, boolean z) {
        Preconditions.checkNotNull(runnable);
        this.A05 = runnable;
        this.A03 = r3;
        Preconditions.checkNotNull(r4);
        this.A04 = r4;
        this.A02 = j;
        this.A06 = z;
    }

    public String toString() {
        return AnonymousClass24S.A00(this);
    }
}
