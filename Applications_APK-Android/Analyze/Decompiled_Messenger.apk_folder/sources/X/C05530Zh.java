package X;

import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Set;

/* renamed from: X.0Zh  reason: invalid class name and case insensitive filesystem */
public final class C05530Zh {
    public AnonymousClass0j2 A00;
    public AnonymousClass100 A01;
    public Boolean A02;
    public boolean A03;
    public final C06790c5 A04;
    public final C05160Xw A05;
    public final C25511Zx A06;
    public final C25501Zw A07;
    private final C04460Ut A08;
    private final C09480hR A09 = new C09480hR(this);
    private final String A0A;

    public static synchronized void A00(C05530Zh r2) {
        synchronized (r2) {
            r2.A03 = true;
            A01(r2, false);
            String str = r2.A0A;
            if (str != null) {
                r2.A08.C4y(str);
            }
        }
    }

    public static synchronized void A01(C05530Zh r2, boolean z) {
        AnonymousClass100 r1;
        synchronized (r2) {
            if (z) {
                AnonymousClass100 r12 = r2.A01;
                if (r12 != null) {
                    r12.A00(r2.A09);
                }
            }
            if (!z && (r1 = r2.A01) != null) {
                r1.A00(null);
            }
        }
    }

    public synchronized ListenableFuture A03(C09500hT r5) {
        C09500hT r2;
        C25621a8 r1;
        AnonymousClass0j2 r0 = this.A00;
        if (r0 != null) {
            r2 = r0.A00;
        } else {
            r2 = null;
        }
        if (r2 == null) {
            r1 = C25621a8.START_NEW_FETCH_AND_TRACK_RESULT;
        } else {
            C25611a7 r02 = r5.A01;
            C25611a7 r12 = C25611a7.TOP;
            if (r02 == r12) {
                r1 = C25621a8.USE_CURRENT_FETCH;
            } else if (r2.A01 == r12) {
                r1 = C25621a8.START_NEW_FETCH_AND_TRACK_RESULT;
            } else {
                C09510hU r3 = r2.A00;
                switch (r5.A00.ordinal()) {
                    case 0:
                        if (r3 != C09510hU.CHECK_SERVER_FOR_NEW_DATA && r3 != C09510hU.PREFER_CACHE_IF_UP_TO_DATE) {
                            r1 = C25621a8.START_NEW_FETCH_AND_TRACK_RESULT;
                            break;
                        } else {
                            r1 = C25621a8.USE_CURRENT_FETCH;
                            break;
                        }
                        break;
                    case 1:
                        if (r3 != C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
                            r1 = C25621a8.START_NEW_FETCH_AND_TRACK_RESULT;
                            break;
                        } else {
                            r1 = C25621a8.USE_CURRENT_FETCH;
                            break;
                        }
                    case 2:
                        r1 = C25621a8.USE_CURRENT_FETCH;
                        break;
                    case 3:
                        if (r3 != C09510hU.CHECK_SERVER_FOR_NEW_DATA && r3 != C09510hU.STALE_DATA_OKAY && r3 != C09510hU.PREFER_CACHE_IF_UP_TO_DATE) {
                            r1 = C25621a8.START_NEW_FETCH_AND_TRACK_RESULT;
                            break;
                        } else {
                            r1 = C25621a8.USE_CURRENT_FETCH;
                            break;
                        }
                        break;
                    default:
                        throw new IllegalStateException("invalid data freshness param " + r3);
                }
            }
        }
        if (r1 == C25621a8.START_NEW_FETCH_AND_TRACK_RESULT) {
            synchronized (this) {
                if (this.A00 != null) {
                    this.A00 = null;
                }
                this.A00 = new AnonymousClass0j2(r5, this.A05.CIF(new C09520hV(this, r5)));
            }
        }
        return C05350Yp.A00(this.A00.A01);
    }

    public static boolean A02(C05530Zh r3, Set set) {
        Preconditions.checkNotNull(r3.A01);
        C24971Xv it = r3.A01.A01.A03.iterator();
        while (it.hasNext()) {
            AnonymousClass12V r2 = (AnonymousClass12V) it.next();
            if (r2.A00 == GraphQLMessengerInboxUnitType.A0E) {
                Preconditions.checkNotNull(r2.A02);
                C24971Xv it2 = ((ImmutableList) r2.A02).iterator();
                while (it2.hasNext()) {
                    if (set.contains(((ThreadSummary) it2.next()).A0S)) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    public C05530Zh(C25501Zw r4, C05160Xw r5, C25511Zx r6, C04460Ut r7, String str) {
        this.A07 = r4;
        this.A05 = r5;
        this.A06 = r6;
        this.A08 = r7;
        this.A0A = str;
        C09490hS r2 = new C09490hS(this);
        C06600bl BMm = r7.BMm();
        BMm.A02(C06680bu.A0e, r2);
        BMm.A02(C06680bu.A0c, r2);
        BMm.A02(C06680bu.A0b, r2);
        BMm.A02(C06680bu.A0F, r2);
        this.A04 = BMm.A00();
    }
}
