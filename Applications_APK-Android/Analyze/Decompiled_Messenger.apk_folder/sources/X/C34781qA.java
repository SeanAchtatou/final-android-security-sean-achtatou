package X;

import android.view.View;
import androidx.fragment.app.Fragment;
import com.facebook.messaging.montage.viewer.MontageViewerFragment;
import java.util.ArrayList;

/* renamed from: X.1qA  reason: invalid class name and case insensitive filesystem */
public final class C34781qA implements AnonymousClass13G {
    public final /* synthetic */ C32301lX A00;

    public String B53() {
        return "montage";
    }

    public C34781qA(C32301lX r1) {
        this.A00 = r1;
    }

    public void AXh() {
        C13150qn A002 = C32301lX.A00(this.A00);
        if (A002.A02 == null) {
            MontageViewerFragment A0B = MontageViewerFragment.A0B(new ArrayList(), C88984Nc.A0I);
            C16290wo r1 = A002.A05;
            r1.A08(2131299191, A0B);
            r1.A0H(A0B);
        }
        A002.A01();
        ((C89374Ou) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AEL, this.A00.A00)).A03.set(C89384Ov.PRELOADED);
    }

    public void AXi() {
        View view;
        Fragment A0O = this.A00.A01.A0O(2131300895);
        Fragment A0O2 = this.A00.A01.A0O(2131299191);
        if (A0O != null && A0O2 != null && (view = A0O.A0I) != null && A0O2.A0b) {
            int width = view.getWidth();
            int height = A0O.A0I.getHeight();
            MontageViewerFragment montageViewerFragment = (MontageViewerFragment) A0O2;
            montageViewerFragment.A01 = width;
            montageViewerFragment.A00 = height;
            View view2 = montageViewerFragment.A06;
            if (view2 != null && width > 0 && height > 0) {
                view2.measure(View.MeasureSpec.makeMeasureSpec(width, 1073741824), View.MeasureSpec.makeMeasureSpec(montageViewerFragment.A00, 1073741824));
                montageViewerFragment.A06.layout(0, 0, montageViewerFragment.A01, montageViewerFragment.A00);
            }
            ((C89374Ou) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AEL, this.A00.A00)).A03.set(C89384Ov.PRERENDERED);
        }
    }
}
