package X;

import android.graphics.ColorSpace;
import android.media.ExifInterface;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.1NY  reason: invalid class name */
public final class AnonymousClass1NY implements Closeable {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public ColorSpace A06;
    public AnonymousClass1O3 A07;
    public C196579Mo A08;
    public final C23111Og A09;
    public final AnonymousClass1PS A0A;

    public static int A02(InputStream inputStream, int i, boolean z) {
        int i2;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            int read = inputStream.read();
            if (read != -1) {
                if (z) {
                    i2 = (read & 255) << (i3 << 3);
                } else {
                    i4 <<= 8;
                    i2 = read & 255;
                }
                i4 |= i2;
                i3++;
            } else {
                throw new IOException("no more bytes");
            }
        }
        return i4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r2.A09 != null) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0C() {
        /*
            r2 = this;
            monitor-enter(r2)
            X.1PS r0 = r2.A0A     // Catch:{ all -> 0x0011 }
            boolean r0 = X.AnonymousClass1PS.A07(r0)     // Catch:{ all -> 0x0011 }
            if (r0 != 0) goto L_0x000e
            X.1Og r1 = r2.A09     // Catch:{ all -> 0x0011 }
            r0 = 0
            if (r1 == 0) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            monitor-exit(r2)
            return r0
        L_0x0011:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1NY.A0C():boolean");
    }

    public static int A00(InputStream inputStream) {
        try {
            return new ExifInterface(inputStream).getAttributeInt(AnonymousClass80H.$const$string(37), 1);
        } catch (IOException e) {
            AnonymousClass02v r1 = AnonymousClass02w.A00;
            if (!r1.BFj(3)) {
                return 0;
            }
            r1.AWX("HeifExifUtil", "Failed reading Heif Exif orientation -> ignoring", e);
            return 0;
        }
    }

    public static AnonymousClass1NY A03(AnonymousClass1NY r3) {
        AnonymousClass1NY r2;
        if (r3 == null) {
            return null;
        }
        C23111Og r1 = r3.A09;
        if (r1 != null) {
            r2 = new AnonymousClass1NY(r1, r3.A04);
        } else {
            AnonymousClass1PS A002 = AnonymousClass1PS.A00(r3.A0A);
            if (A002 == null) {
                r2 = null;
            } else {
                try {
                    r2 = new AnonymousClass1NY(A002);
                } catch (Throwable th) {
                    AnonymousClass1PS.A05(A002);
                    throw th;
                }
            }
            AnonymousClass1PS.A05(A002);
        }
        if (r2 != null) {
            r2.A0B(r3);
        }
        return r2;
    }

    public static void A04(AnonymousClass1NY r0) {
        if (r0 != null) {
            r0.close();
        }
    }

    public static void A05(AnonymousClass1NY r1) {
        if (r1.A05 < 0 || r1.A01 < 0) {
            r1.A0A();
        }
    }

    public static boolean A06(AnonymousClass1NY r1) {
        if (r1.A02 < 0 || r1.A05 < 0 || r1.A01 < 0) {
            return false;
        }
        return true;
    }

    public static boolean A07(AnonymousClass1NY r1) {
        if (r1 == null || !r1.A0C()) {
            return false;
        }
        return true;
    }

    public int A08() {
        AnonymousClass1PS r0 = this.A0A;
        if (r0 == null || r0.A0A() == null) {
            return this.A04;
        }
        return ((AnonymousClass1SS) this.A0A.A0A()).size();
    }

    public InputStream A09() {
        C23111Og r0 = this.A09;
        if (r0 != null) {
            return (InputStream) r0.get();
        }
        AnonymousClass1PS A002 = AnonymousClass1PS.A00(this.A0A);
        if (A002 == null) {
            return null;
        }
        try {
            return new AnonymousClass1SV((AnonymousClass1SS) A002.A0A());
        } finally {
            AnonymousClass1PS.A05(A002);
        }
    }

    public boolean A0D(int i) {
        AnonymousClass1O3 r1 = this.A07;
        if ((r1 != AnonymousClass1SI.A06 && r1 != AnonymousClass1SI.A02) || this.A09 != null) {
            return true;
        }
        C05520Zg.A02(this.A0A);
        AnonymousClass1SS r2 = (AnonymousClass1SS) this.A0A.A0A();
        if (r2.read(i - 2) == -1 && r2.read(i - 1) == -39) {
            return true;
        }
        return false;
    }

    public void close() {
        AnonymousClass1PS.A05(this.A0A);
    }

    public static int A01(InputStream inputStream) {
        byte read = (byte) (inputStream.read() & 255);
        return ((((byte) (inputStream.read() & 255)) << 16) & 16711680) | ((((byte) (inputStream.read() & 255)) << 8) & 65280) | (read & 255);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0225, code lost:
        if (r1 == 0) goto L_0x0228;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r2 == X.AnonymousClass1SI.A08) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x016a, code lost:
        if (r5 == null) goto L_0x01ac;
     */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x027d A[Catch:{ IOException -> 0x02bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x02fd A[SYNTHETIC, Splitter:B:169:0x02fd] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e A[Catch:{ IOException -> 0x0166, all -> 0x0170 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01ae  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A() {
        /*
            r10 = this;
            java.io.InputStream r0 = r10.A09()
            X.1O3 r2 = X.AnonymousClass1OG.A01(r0)
            r10.A07 = r2
            boolean r0 = X.AnonymousClass1SI.A00(r2)
            if (r0 != 0) goto L_0x0015
            X.1O3 r1 = X.AnonymousClass1SI.A08
            r0 = 0
            if (r2 != r1) goto L_0x0016
        L_0x0015:
            r0 = 1
        L_0x0016:
            if (r0 == 0) goto L_0x017c
            java.io.InputStream r5 = r10.A09()
            r8 = 4
            byte[] r7 = new byte[r8]
            r3 = 0
            r5.read(r7)     // Catch:{ IOException -> 0x0166 }
            java.lang.String r9 = "RIFF"
            int r0 = r9.length()     // Catch:{ IOException -> 0x0166 }
            if (r8 != r0) goto L_0x0039
            r4 = 0
        L_0x002c:
            if (r4 >= r8) goto L_0x003b
            char r1 = r9.charAt(r4)     // Catch:{ IOException -> 0x0166 }
            byte r0 = r7[r4]     // Catch:{ IOException -> 0x0166 }
            if (r1 != r0) goto L_0x0039
            int r4 = r4 + 1
            goto L_0x002c
        L_0x0039:
            r0 = 0
            goto L_0x003c
        L_0x003b:
            r0 = 1
        L_0x003c:
            if (r0 == 0) goto L_0x016c
            r5.read()     // Catch:{ IOException -> 0x0166 }
            r5.read()     // Catch:{ IOException -> 0x0166 }
            r5.read()     // Catch:{ IOException -> 0x0166 }
            r5.read()     // Catch:{ IOException -> 0x0166 }
            r5.read(r7)     // Catch:{ IOException -> 0x0166 }
            java.lang.String r9 = "WEBP"
            int r0 = r9.length()     // Catch:{ IOException -> 0x0166 }
            if (r8 != r0) goto L_0x0063
            r4 = 0
        L_0x0056:
            if (r4 >= r8) goto L_0x0065
            char r1 = r9.charAt(r4)     // Catch:{ IOException -> 0x0166 }
            byte r0 = r7[r4]     // Catch:{ IOException -> 0x0166 }
            if (r1 != r0) goto L_0x0063
            int r4 = r4 + 1
            goto L_0x0056
        L_0x0063:
            r0 = 0
            goto L_0x0066
        L_0x0065:
            r0 = 1
        L_0x0066:
            if (r0 == 0) goto L_0x016c
            r5.read(r7)     // Catch:{ IOException -> 0x0166 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0166 }
            r4.<init>()     // Catch:{ IOException -> 0x0166 }
            r1 = 0
        L_0x0071:
            if (r1 >= r8) goto L_0x007c
            byte r0 = r7[r1]     // Catch:{ IOException -> 0x0166 }
            char r0 = (char) r0     // Catch:{ IOException -> 0x0166 }
            r4.append(r0)     // Catch:{ IOException -> 0x0166 }
            int r1 = r1 + 1
            goto L_0x0071
        L_0x007c:
            java.lang.String r1 = r4.toString()     // Catch:{ IOException -> 0x0166 }
            java.lang.String r0 = "VP8 "
            boolean r0 = r0.equals(r1)     // Catch:{ IOException -> 0x0166 }
            if (r0 == 0) goto L_0x00e0
            r0 = 7
            r5.skip(r0)     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            short r6 = (short) r0     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            short r4 = (short) r0     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            short r1 = (short) r0     // Catch:{ IOException -> 0x0166 }
            r0 = 157(0x9d, float:2.2E-43)
            if (r6 != r0) goto L_0x016c
            r0 = 1
            if (r4 != r0) goto L_0x016c
            r0 = 42
            if (r1 != r0) goto L_0x016c
            android.util.Pair r8 = new android.util.Pair     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r4 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r0 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            int r1 = r0 << 8
            r7 = 65280(0xff00, float:9.1477E-41)
            r1 = r1 & r7
            r0 = r4 & 255(0xff, float:3.57E-43)
            r1 = r1 | r0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r4 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r0 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            int r1 = r0 << 8
            r1 = r1 & r7
            r0 = r4 & 255(0xff, float:3.57E-43)
            r1 = r1 | r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException -> 0x0166 }
            r8.<init>(r6, r0)     // Catch:{ IOException -> 0x0166 }
            r3 = r8
            goto L_0x016c
        L_0x00e0:
            java.lang.String r0 = "VP8L"
            boolean r0 = r0.equals(r1)     // Catch:{ IOException -> 0x0166 }
            if (r0 == 0) goto L_0x013e
            r5.read()     // Catch:{ IOException -> 0x0166 }
            r5.read()     // Catch:{ IOException -> 0x0166 }
            r5.read()     // Catch:{ IOException -> 0x0166 }
            r5.read()     // Catch:{ IOException -> 0x0166 }
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            r0 = r0 & 255(0xff, float:3.57E-43)
            byte r1 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            r0 = 47
            if (r1 != r0) goto L_0x016c
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r0 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            r6 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r0 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            r7 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r0 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            r4 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r5.read()     // Catch:{ IOException -> 0x0166 }
            byte r0 = (byte) r0     // Catch:{ IOException -> 0x0166 }
            r1 = r0 & 255(0xff, float:3.57E-43)
            r0 = r7 & 63
            int r0 = r0 << 8
            r6 = r6 | r0
            int r6 = r6 + 1
            r0 = r1 & 15
            int r1 = r0 << 10
            int r0 = r4 << 2
            r1 = r1 | r0
            r0 = r7 & 192(0xc0, float:2.69E-43)
            int r0 = r0 >> 6
            r1 = r1 | r0
            int r0 = r1 + 1
            android.util.Pair r4 = new android.util.Pair     // Catch:{ IOException -> 0x0166 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ IOException -> 0x0166 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0166 }
            r4.<init>(r1, r0)     // Catch:{ IOException -> 0x0166 }
            goto L_0x0164
        L_0x013e:
            java.lang.String r0 = "VP8X"
            boolean r0 = r0.equals(r1)     // Catch:{ IOException -> 0x0166 }
            if (r0 == 0) goto L_0x016c
            r0 = 8
            r5.skip(r0)     // Catch:{ IOException -> 0x0166 }
            android.util.Pair r4 = new android.util.Pair     // Catch:{ IOException -> 0x0166 }
            int r0 = A01(r5)     // Catch:{ IOException -> 0x0166 }
            int r0 = r0 + 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0166 }
            int r0 = A01(r5)     // Catch:{ IOException -> 0x0166 }
            int r0 = r0 + 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0166 }
            r4.<init>(r1, r0)     // Catch:{ IOException -> 0x0166 }
        L_0x0164:
            r3 = r4
            goto L_0x016c
        L_0x0166:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0170 }
            if (r5 == 0) goto L_0x01ac
        L_0x016c:
            r5.close()     // Catch:{ IOException -> 0x01a8 }
            goto L_0x01ac
        L_0x0170:
            r1 = move-exception
            if (r5 == 0) goto L_0x017b
            r5.close()     // Catch:{ IOException -> 0x0177 }
            goto L_0x017b
        L_0x0177:
            r0 = move-exception
            r0.printStackTrace()
        L_0x017b:
            throw r1
        L_0x017c:
            java.io.InputStream r4 = r10.A09()     // Catch:{ all -> 0x02f7 }
            X.1SL r3 = X.AnonymousClass1SJ.A02(r4)     // Catch:{ all -> 0x02fa }
            android.graphics.ColorSpace r0 = r3.A00     // Catch:{ all -> 0x02fa }
            r10.A06 = r0     // Catch:{ all -> 0x02fa }
            android.util.Pair r1 = r3.A01     // Catch:{ all -> 0x02fa }
            if (r1 == 0) goto L_0x01a0
            java.lang.Object r0 = r1.first     // Catch:{ all -> 0x02fa }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x02fa }
            int r0 = r0.intValue()     // Catch:{ all -> 0x02fa }
            r10.A05 = r0     // Catch:{ all -> 0x02fa }
            java.lang.Object r0 = r1.second     // Catch:{ all -> 0x02fa }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x02fa }
            int r0 = r0.intValue()     // Catch:{ all -> 0x02fa }
            r10.A01 = r0     // Catch:{ all -> 0x02fa }
        L_0x01a0:
            if (r4 == 0) goto L_0x01a5
            r4.close()     // Catch:{ IOException -> 0x01a5 }
        L_0x01a5:
            android.util.Pair r3 = r3.A01
            goto L_0x01c2
        L_0x01a8:
            r0 = move-exception
            r0.printStackTrace()
        L_0x01ac:
            if (r3 == 0) goto L_0x01c2
            java.lang.Object r0 = r3.first
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            r10.A05 = r0
            java.lang.Object r0 = r3.second
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            r10.A01 = r0
        L_0x01c2:
            X.1O3 r0 = X.AnonymousClass1SI.A06
            r1 = -1
            if (r2 != r0) goto L_0x02be
            int r0 = r10.A02
            if (r0 != r1) goto L_0x02be
            if (r3 == 0) goto L_0x02dc
            java.io.InputStream r4 = r10.A09()
            r5 = 225(0xe1, float:3.15E-43)
            X.C05520Zg.A02(r4)     // Catch:{ IOException -> 0x02bc }
        L_0x01d6:
            r3 = 0
            r2 = 1
            int r1 = A02(r4, r2, r3)     // Catch:{ IOException -> 0x02bc }
            r0 = 255(0xff, float:3.57E-43)
            if (r1 != r0) goto L_0x0206
            r1 = 255(0xff, float:3.57E-43)
        L_0x01e2:
            if (r1 != r0) goto L_0x01e9
            int r1 = A02(r4, r2, r3)     // Catch:{ IOException -> 0x02bc }
            goto L_0x01e2
        L_0x01e9:
            if (r1 != r5) goto L_0x01ed
            r0 = 1
            goto L_0x0207
        L_0x01ed:
            r0 = 216(0xd8, float:3.03E-43)
            if (r1 == r0) goto L_0x01d6
            if (r1 == r2) goto L_0x01d6
            r0 = 217(0xd9, float:3.04E-43)
            if (r1 == r0) goto L_0x0206
            r0 = 218(0xda, float:3.05E-43)
            if (r1 == r0) goto L_0x0206
            r1 = 2
            int r0 = A02(r4, r1, r3)     // Catch:{ IOException -> 0x02bc }
            int r0 = r0 - r1
            long r0 = (long) r0     // Catch:{ IOException -> 0x02bc }
            r4.skip(r0)     // Catch:{ IOException -> 0x02bc }
            goto L_0x01d6
        L_0x0206:
            r0 = 0
        L_0x0207:
            r5 = 0
            if (r0 == 0) goto L_0x0227
            r2 = 2
            int r1 = A02(r4, r2, r3)     // Catch:{ IOException -> 0x02bc }
            int r1 = r1 - r2
            r0 = 6
            if (r1 <= r0) goto L_0x0227
            r0 = 4
            int r3 = A02(r4, r0, r3)     // Catch:{ IOException -> 0x02bc }
            int r0 = r1 + -4
            int r1 = A02(r4, r2, r5)     // Catch:{ IOException -> 0x02bc }
            int r2 = r0 + -2
            r0 = 1165519206(0x45786966, float:3974.5874)
            if (r3 != r0) goto L_0x0227
            if (r1 == 0) goto L_0x0228
        L_0x0227:
            r2 = 0
        L_0x0228:
            if (r2 == 0) goto L_0x02ed
            X.3AL r8 = new X.3AL     // Catch:{ IOException -> 0x02bc }
            r8.<init>()     // Catch:{ IOException -> 0x02bc }
            r6 = 8
            r0 = 0
            if (r2 <= r6) goto L_0x0267
            r5 = 4
            int r3 = A02(r4, r5, r0)     // Catch:{ IOException -> 0x02bc }
            int r2 = r2 + -4
            r1 = 1229531648(0x49492a00, float:823968.0)
            if (r3 == r1) goto L_0x024d
            r0 = 1296891946(0x4d4d002a, float:2.14958752E8)
            if (r3 == r0) goto L_0x024d
            java.lang.Class r1 = X.C24171So.A00     // Catch:{ IOException -> 0x02bc }
            java.lang.String r0 = "Invalid TIFF header"
            X.AnonymousClass02w.A00(r1, r0)     // Catch:{ IOException -> 0x02bc }
            goto L_0x0267
        L_0x024d:
            r0 = 0
            if (r3 != r1) goto L_0x0251
            r0 = 1
        L_0x0251:
            r8.A01 = r0     // Catch:{ IOException -> 0x02bc }
            int r0 = A02(r4, r5, r0)     // Catch:{ IOException -> 0x02bc }
            r8.A00 = r0     // Catch:{ IOException -> 0x02bc }
            int r3 = r2 + -4
            if (r0 < r6) goto L_0x0260
            int r0 = r0 - r6
            if (r0 <= r3) goto L_0x0268
        L_0x0260:
            java.lang.Class r1 = X.C24171So.A00     // Catch:{ IOException -> 0x02bc }
            java.lang.String r0 = "Invalid offset"
            X.AnonymousClass02w.A00(r1, r0)     // Catch:{ IOException -> 0x02bc }
        L_0x0267:
            r3 = 0
        L_0x0268:
            int r0 = r8.A00     // Catch:{ IOException -> 0x02bc }
            int r2 = r0 + -8
            if (r3 == 0) goto L_0x02ed
            if (r2 > r3) goto L_0x02ed
            long r0 = (long) r2     // Catch:{ IOException -> 0x02bc }
            r4.skip(r0)     // Catch:{ IOException -> 0x02bc }
            int r3 = r3 - r2
            boolean r7 = r8.A01     // Catch:{ IOException -> 0x02bc }
            r6 = 274(0x112, float:3.84E-43)
            r0 = 14
            if (r3 < r0) goto L_0x029d
            r5 = 2
            int r0 = A02(r4, r5, r7)     // Catch:{ IOException -> 0x02bc }
            int r1 = r3 + -2
        L_0x0284:
            int r3 = r0 + -1
            if (r0 <= 0) goto L_0x029d
            r0 = 12
            if (r1 < r0) goto L_0x029d
            int r0 = A02(r4, r5, r7)     // Catch:{ IOException -> 0x02bc }
            int r2 = r1 + -2
            if (r0 == r6) goto L_0x029e
            r0 = 10
            r4.skip(r0)     // Catch:{ IOException -> 0x02bc }
            int r1 = r2 + -10
            r0 = r3
            goto L_0x0284
        L_0x029d:
            r2 = 0
        L_0x029e:
            boolean r3 = r8.A01     // Catch:{ IOException -> 0x02bc }
            r0 = 10
            if (r2 < r0) goto L_0x02ed
            r2 = 2
            int r1 = A02(r4, r2, r3)     // Catch:{ IOException -> 0x02bc }
            r0 = 3
            if (r1 != r0) goto L_0x02ed
            r0 = 4
            int r1 = A02(r4, r0, r3)     // Catch:{ IOException -> 0x02bc }
            r0 = 1
            if (r1 != r0) goto L_0x02ed
            int r0 = A02(r4, r2, r3)     // Catch:{ IOException -> 0x02bc }
            A02(r4, r2, r3)     // Catch:{ IOException -> 0x02bc }
            goto L_0x02d4
        L_0x02bc:
            r0 = 0
            goto L_0x02d4
        L_0x02be:
            X.1O3 r0 = X.AnonymousClass1SI.A04
            if (r2 != r0) goto L_0x02ef
            int r0 = r10.A02
            if (r0 != r1) goto L_0x02ef
            java.io.InputStream r2 = r10.A09()
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 24
            if (r1 < r0) goto L_0x02dd
            int r0 = A00(r2)
        L_0x02d4:
            r10.A00 = r0
            int r0 = X.C24171So.A00(r0)
            r10.A02 = r0
        L_0x02dc:
            return
        L_0x02dd:
            java.lang.String r3 = "HeifExifUtil"
            java.lang.String r2 = "Trying to read Heif Exif information before Android N -> ignoring"
            X.02v r1 = X.AnonymousClass02w.A00
            r0 = 3
            boolean r0 = r1.BFj(r0)
            if (r0 == 0) goto L_0x02ed
            r1.AWW(r3, r2)
        L_0x02ed:
            r0 = 0
            goto L_0x02d4
        L_0x02ef:
            int r0 = r10.A02
            if (r0 != r1) goto L_0x02dc
            r0 = 0
            r10.A02 = r0
            return
        L_0x02f7:
            r0 = move-exception
            r4 = 0
            goto L_0x02fb
        L_0x02fa:
            r0 = move-exception
        L_0x02fb:
            if (r4 == 0) goto L_0x0300
            r4.close()     // Catch:{ IOException -> 0x0300 }
        L_0x0300:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1NY.A0A():void");
    }

    public void A0B(AnonymousClass1NY r2) {
        A05(r2);
        this.A07 = r2.A07;
        A05(r2);
        this.A05 = r2.A05;
        A05(r2);
        this.A01 = r2.A01;
        A05(r2);
        this.A02 = r2.A02;
        A05(r2);
        this.A00 = r2.A00;
        this.A03 = r2.A03;
        this.A04 = r2.A08();
        this.A08 = r2.A08;
        A05(r2);
        this.A06 = r2.A06;
    }

    private AnonymousClass1NY(C23111Og r3) {
        this.A07 = AnonymousClass1O3.A02;
        this.A02 = -1;
        this.A00 = 0;
        this.A05 = -1;
        this.A01 = -1;
        this.A03 = 1;
        this.A04 = -1;
        C05520Zg.A02(r3);
        this.A0A = null;
        this.A09 = r3;
    }

    private AnonymousClass1NY(C23111Og r1, int i) {
        this(r1);
        this.A04 = i;
    }

    public AnonymousClass1NY(AnonymousClass1PS r3) {
        this.A07 = AnonymousClass1O3.A02;
        this.A02 = -1;
        this.A00 = 0;
        this.A05 = -1;
        this.A01 = -1;
        this.A03 = 1;
        this.A04 = -1;
        C05520Zg.A04(AnonymousClass1PS.A07(r3));
        this.A0A = r3.clone();
        this.A09 = null;
    }
}
