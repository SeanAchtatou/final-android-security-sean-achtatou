package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0Og  reason: invalid class name and case insensitive filesystem */
public final class C03510Og extends C03160Kg {
    public long A00() {
        return 2353414016265691865L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        AnonymousClass0FV r32 = (AnonymousClass0FV) r3;
        dataOutput.writeDouble(r32.userTimeS);
        dataOutput.writeDouble(r32.systemTimeS);
        dataOutput.writeDouble(r32.childUserTimeS);
        dataOutput.writeDouble(r32.childSystemTimeS);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        AnonymousClass0FV r32 = (AnonymousClass0FV) r3;
        r32.userTimeS = dataInput.readDouble();
        r32.systemTimeS = dataInput.readDouble();
        r32.childUserTimeS = dataInput.readDouble();
        r32.childSystemTimeS = dataInput.readDouble();
        return true;
    }
}
