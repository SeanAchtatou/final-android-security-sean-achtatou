package X;

import android.os.PowerManager;

/* renamed from: X.07v  reason: invalid class name and case insensitive filesystem */
public final class C009007v {
    public static PowerManager.WakeLock A00(PowerManager powerManager, int i, String str) {
        PowerManager.WakeLock newWakeLock = powerManager.newWakeLock(i, str);
        C007807d r2 = C009107w.A00;
        synchronized (r2) {
            if (r2.A04) {
                AnonymousClass07f r1 = new AnonymousClass07f(newWakeLock, str);
                r2.A07.put(newWakeLock, r1);
                r2.A06.add(r1);
            }
        }
        return newWakeLock;
    }

    public static void A01(PowerManager.WakeLock wakeLock) {
        wakeLock.release();
        C009107w.A00(wakeLock);
    }

    public static void A02(PowerManager.WakeLock wakeLock, long j) {
        wakeLock.acquire(j);
        C009107w.A01(wakeLock, j);
    }

    public static void A03(PowerManager.WakeLock wakeLock, boolean z) {
        wakeLock.setReferenceCounted(z);
        C007807d r3 = C009107w.A00;
        synchronized (r3) {
            if (r3.A04) {
                AnonymousClass07f r0 = (AnonymousClass07f) r3.A07.get(wakeLock);
                if (r0 == null) {
                    AnonymousClass0KZ.A00("WakeLockMetricsCollector", "Unknown wakelock modified", null);
                } else {
                    r0.A06 = z;
                }
            }
        }
    }
}
