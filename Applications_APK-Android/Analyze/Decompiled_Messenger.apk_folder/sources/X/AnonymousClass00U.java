package X;

import android.os.Process;

/* renamed from: X.00U  reason: invalid class name */
public final class AnonymousClass00U {
    private static final int[] A00 = {32, 32, 32, 32, 32, 32, 32, 32, 32, AnonymousClass1Y3.B7e, AnonymousClass1Y3.B7e, AnonymousClass1Y3.B7e, AnonymousClass1Y3.B7e, 32};

    public static final long[] A01(String str) {
        long[] jArr = {-1, -1, -1, -1};
        AnonymousClass00V.A01(str, A00, jArr);
        return jArr;
    }

    public static final long A00() {
        return A01(AnonymousClass08S.A0A("/proc/self/task/", Process.myTid(), "/stat"))[2];
    }
}
