package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Arrays;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0pm  reason: invalid class name and case insensitive filesystem */
public final class C12670pm {
    private static final String[] A07;
    private static final String[] A08;
    private static final String[] A09;
    private static volatile C12670pm A0A;
    public final C12720pr A00;
    public final C12710pq A01;
    public final C04310Tq A02;
    public final C04310Tq A03;
    private final C07050cY A04;
    private final C12700pp A05;
    private final C07040cX A06;

    static {
        String[] strArr = C12170og.A04;
        int length = strArr.length;
        Object[] copyOf = Arrays.copyOf(strArr, length + 1);
        copyOf[length] = "timestamp_in_folder_ms";
        A08 = (String[]) copyOf;
        String[] strArr2 = C12680pn.A04;
        int length2 = strArr2.length;
        Object[] copyOf2 = Arrays.copyOf(strArr2, length2 + 1);
        copyOf2[length2] = "timestamp_in_folder_ms";
        A09 = (String[]) copyOf2;
        A07 = (String[]) AnonymousClass0U8.A03(A08, strArr2, String.class);
    }

    public static C28021e8 A01(C12670pm r8, C25601a6 r9, String str, C10950l8 r11) {
        Cursor cursor = null;
        String str2 = str;
        if (r11 != C10950l8.A06) {
            SQLiteDatabase A062 = ((C25771aN) r8.A02.get()).A06();
            if (A062.isOpen()) {
                cursor = ((C28071eD) r8.A03.get()).A08(A062, A08, r9.A02(), r9.A04(), str2);
            }
            if (cursor != null) {
                return r8.A06.A00(cursor, true);
            }
        } else {
            SQLiteDatabase A063 = ((C25771aN) r8.A02.get()).A06();
            if (A063.isOpen()) {
                cursor = ((C28071eD) r8.A03.get()).A08(A063, A09, r9.A02(), r9.A04(), str2);
            }
            if (cursor != null) {
                C12700pp r4 = r8.A05;
                C13660rp r2 = new C13660rp(r4);
                C42722Bo r1 = new C42722Bo(r4);
                new C13690rs(r4);
                return new C12680pn(r2, r1, cursor, true);
            }
        }
        return C28021e8.A00;
    }

    public static C28021e8 A02(C12670pm r6, C06140av r7, String str) {
        Cursor cursor;
        SQLiteDatabase A062 = ((C25771aN) r6.A02.get()).A06();
        if (A062.isOpen()) {
            cursor = ((C28071eD) r6.A03.get()).A08(A062, C42712Bn.A05, r7.A02(), r7.A04(), str);
        } else {
            cursor = null;
        }
        if (cursor == null) {
            return C28021e8.A00;
        }
        return r6.A04.A00(cursor, false);
    }

    public static final C12670pm A03(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (C12670pm.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new C12670pm(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public C28021e8 A04(String str) {
        Cursor A012 = this.A00.A01(str, false, -1);
        if (A012 == null) {
            return C28021e8.A00;
        }
        C12710pq r2 = this.A01;
        new C13690rs(r2);
        return new AnonymousClass7VW(r2, A012);
    }

    private C12670pm(AnonymousClass1XY r2) {
        this.A02 = C25771aN.A02(r2);
        this.A06 = new C07040cX(r2);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.AnN, r2);
        this.A05 = new C12700pp(r2);
        this.A04 = new C07050cY(r2);
        this.A01 = new C12710pq(r2);
        this.A00 = C12720pr.A00(r2);
    }

    public static C25601a6 A00(C28711fF r4, C10950l8 r5) {
        C25601a6 A002 = C06160ax.A00();
        A002.A05(C06160ax.A03("folder", r5.dbName));
        A002.A05(new C42702Bm("thread_key", AnonymousClass08S.A0J(r4.name(), "%")));
        return A002;
    }

    public C28021e8 A05(String str) {
        Cursor cursor;
        C25601a6 A002 = C06160ax.A00();
        A002.A05(C06160ax.A03("folder", C10950l8.A05.dbName));
        A002.A05(C06160ax.A02(new C42702Bm("name", AnonymousClass08S.A0J(str, "%")), new C42702Bm("name", AnonymousClass08S.A0P("% ", str, "%"))));
        A002.A02();
        SQLiteDatabase A062 = ((C25771aN) this.A02.get()).A06();
        if (A062.isOpen()) {
            cursor = ((C28071eD) this.A03.get()).A08(A062, A07, A002.A02(), A002.A04(), "timestamp_in_folder_ms DESC");
        } else {
            cursor = null;
        }
        if (cursor == null) {
            return C28021e8.A00;
        }
        return this.A04.A00(cursor, true);
    }
}
