package X;

import android.graphics.Rect;

/* renamed from: X.0zN  reason: invalid class name and case insensitive filesystem */
public final class C17730zN implements Cloneable {
    public int A00;
    public int A01 = 0;
    public long A02;
    public AnonymousClass38U A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final long A07;
    public final Rect A08;
    public final C17770zR A09;
    public final C31401jd A0A;
    public final C17750zP A0B;
    public final C17650zF A0C;
    private final int A0D;
    private final int A0E;

    public void A00(Rect rect) {
        Rect rect2 = this.A08;
        int i = rect2.left;
        int i2 = this.A0D;
        rect.left = i - i2;
        int i3 = rect2.top;
        int i4 = this.A0E;
        rect.top = i3 - i4;
        rect.right = rect2.right - i2;
        rect.bottom = rect2.bottom - i4;
    }

    public C17730zN(C31401jd r3, C17650zF r4, C17770zR r5, Rect rect, int i, int i2, int i3, long j, int i4, int i5, C17750zP r14) {
        if (r5 != null) {
            this.A0A = r3;
            this.A0C = r4;
            this.A09 = r5;
            this.A08 = rect;
            this.A0D = i;
            this.A0E = i2;
            this.A04 = i3;
            this.A07 = j;
            this.A05 = i4;
            this.A06 = i5;
            this.A0B = r14;
            return;
        }
        throw new RuntimeException("Trying to set a null Component on a LayoutOutput!");
    }
}
