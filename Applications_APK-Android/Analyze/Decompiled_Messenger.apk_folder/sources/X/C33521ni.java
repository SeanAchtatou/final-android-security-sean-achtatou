package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Future;

/* renamed from: X.1ni  reason: invalid class name and case insensitive filesystem */
public final class C33521ni extends AnonymousClass1QN {
    public AnonymousClass1QO A00;
    public Future A01;

    public boolean AT6() {
        Future future;
        AnonymousClass1QO r1;
        synchronized (this) {
            future = this.A01;
            this.A01 = null;
            r1 = this.A00;
            this.A00 = null;
        }
        if (future != null) {
            future.cancel(true);
        }
        if (r1 != null) {
            r1.AT6();
        }
        return super.AT6();
    }

    public static C33521ni A00(ListenableFuture listenableFuture) {
        C33521ni r2 = new C33521ni(listenableFuture);
        C05350Yp.A08(listenableFuture, new C29723Egz(r2), C25141Ym.INSTANCE);
        return r2;
    }

    private C33521ni(Future future) {
        this.A01 = future;
    }

    public Object B1I() {
        return AnonymousClass1PS.A00((AnonymousClass1PS) super.B1I());
    }
}
