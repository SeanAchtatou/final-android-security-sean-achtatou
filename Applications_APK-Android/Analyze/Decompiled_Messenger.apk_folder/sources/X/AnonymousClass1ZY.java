package X;

import java.util.EnumSet;

/* renamed from: X.1ZY  reason: invalid class name */
public abstract class AnonymousClass1ZY {
    public AnonymousClass1XV A00;
    public EnumSet A01;
    public final int A02;

    public AnonymousClass1ZY(AnonymousClass1ZQ r2) {
        this.A01 = r2.A00;
        this.A00 = r2.A01;
        Integer num = r2.A02;
        if (num != null) {
            this.A02 = num.intValue();
        } else {
            this.A02 = C94374eP.A00(null);
        }
    }
}
