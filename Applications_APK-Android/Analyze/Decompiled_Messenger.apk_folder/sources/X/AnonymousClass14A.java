package X;

import com.facebook.messaging.service.model.FetchMoreMessagesParams;

/* renamed from: X.14A  reason: invalid class name */
public final class AnonymousClass14A extends AnonymousClass0lj {
    public final /* synthetic */ FetchMoreMessagesParams A00;
    public final /* synthetic */ C55462o7 A01;
    public final /* synthetic */ AnonymousClass3SU A02;

    public AnonymousClass14A(C55462o7 r1, AnonymousClass3SU r2, FetchMoreMessagesParams fetchMoreMessagesParams) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = fetchMoreMessagesParams;
    }
}
