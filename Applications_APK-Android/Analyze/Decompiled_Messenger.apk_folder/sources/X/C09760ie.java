package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;
import java.util.concurrent.ExecutorService;

@InjectorModule
/* renamed from: X.0ie  reason: invalid class name and case insensitive filesystem */
public final class C09760ie extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static final Object A01 = new Object();
    private static volatile AnonymousClass0i9 A02;
    private static volatile C08630fg A03;
    private static volatile ExecutorService A04;
    private static volatile ExecutorService A05;

    public static final C30458Ewq A00() {
        return null;
    }

    public static final C30460Ews A01() {
        return null;
    }

    public static final AnonymousClass0i9 A02(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0i9.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass0i9(C06820c8.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final C08630fg A03(AnonymousClass1XY r3) {
        if (A03 == null) {
            synchronized (C08630fg.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A03 = new C08630fg();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final ExecutorService A05(AnonymousClass1XY r7) {
        if (A04 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass0VM A003 = AnonymousClass0VM.A00(r7.getApplicationInjector());
                        A04 = (AnonymousClass1TI) ((AnonymousClass0VT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ALd, A003.A00)).A01(1, AnonymousClass0VS.BACKGROUND, "QTExposureLog", null);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final ExecutorService A06(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new C06480bZ(C06470bY.A00(applicationInjector).A00, A05(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final AnonymousClass29m A04(AnonymousClass1XY r9) {
        Context A022 = AnonymousClass1YA.A02(r9);
        AnonymousClass0Z4 A012 = AnonymousClass0Z4.A01(r9);
        AnonymousClass29g A002 = AnonymousClass29g.A00(r9);
        AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.BCX, r9);
        AnonymousClass29n A013 = AnonymousClass29n.A01(r9);
        AnonymousClass29o A004 = AnonymousClass29o.A00(r9);
        AnonymousClass29p.A00(r9);
        return new AnonymousClass29m(A022, A012, AnonymousClass80H.$const$string(160), A002, A003, A013, A004, AnonymousClass0WT.A00(r9));
    }
}
