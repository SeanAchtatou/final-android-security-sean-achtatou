package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.12M  reason: invalid class name */
public final class AnonymousClass12M extends C15380vC {
    public final C15960wG A00 = new AnonymousClass12N(this);
    public final ImmutableList A01;

    public void A06() {
        C24971Xv it = this.A01.iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((C15520vQ) it.next()).Ae6().Ae7().A00;
        }
        A03(new C15400vE(i, AnonymousClass10O.RED_WITH_TEXT));
    }

    public AnonymousClass12M(ImmutableList immutableList) {
        this.A01 = immutableList;
    }
}
