package X;

import java.io.File;
import java.io.InputStream;

/* renamed from: X.0iI  reason: invalid class name and case insensitive filesystem */
public final class C09680iI {
    public void A00(InputStream inputStream, File file) {
        try {
            new C192498zx(inputStream).A02(new C192448zs(file, new C192488zw[0]));
        } finally {
            inputStream.close();
        }
    }
}
