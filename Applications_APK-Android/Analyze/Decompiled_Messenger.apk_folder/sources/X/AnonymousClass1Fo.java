package X;

import com.google.common.base.Predicate;
import java.util.Set;

/* renamed from: X.1Fo  reason: invalid class name */
public class AnonymousClass1Fo<E> extends C33011mh<E> implements Set<E> {
    public AnonymousClass1Fo(Set set, Predicate predicate) {
        super(set, predicate);
    }

    public boolean equals(Object obj) {
        return C25011Xz.A0A(this, obj);
    }

    public int hashCode() {
        return C25011Xz.A00(this);
    }
}
