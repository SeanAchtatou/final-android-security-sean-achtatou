package X;

/* renamed from: X.0GP  reason: invalid class name */
public final class AnonymousClass0GP {
    private static boolean A00;

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        X.C010708t.A0T("DownloadableAppModuleMetadataReader", r2, "Error loading downloadable module metadata");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void A00(android.content.Context r8) {
        /*
            java.lang.Class<X.0GP> r7 = X.AnonymousClass0GP.class
            monitor-enter(r7)
            boolean r0 = X.AnonymousClass0GP.A00     // Catch:{ all -> 0x0099 }
            if (r0 != 0) goto L_0x0097
            boolean r0 = X.AnonymousClass0GM.A00(r8)     // Catch:{ all -> 0x0099 }
            r6 = 1
            if (r0 != 0) goto L_0x0012
            X.AnonymousClass0GP.A00 = r6     // Catch:{ all -> 0x0099 }
            goto L_0x0097
        L_0x0012:
            android.content.res.AssetManager r5 = r8.getAssets()     // Catch:{ IOException -> 0x008f }
            java.lang.String r0 = "app_modules.json"
            java.io.InputStream r3 = r5.open(r0)     // Catch:{ FileNotFoundException -> 0x0072 }
            X.0GQ r0 = new X.0GQ     // Catch:{ FileNotFoundException -> 0x0072 }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x0072 }
            android.util.JsonReader r2 = new android.util.JsonReader     // Catch:{ FileNotFoundException -> 0x0072 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x0072 }
            java.lang.String r0 = "UTF-8"
            r1.<init>(r3, r0)     // Catch:{ FileNotFoundException -> 0x0072 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0072 }
            java.util.List r0 = X.AnonymousClass0GQ.A00(r2)     // Catch:{ all -> 0x006d }
            r2.close()     // Catch:{ FileNotFoundException -> 0x0072 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ IOException -> 0x008f }
        L_0x0038:
            boolean r0 = r4.hasNext()     // Catch:{ IOException -> 0x008f }
            if (r0 == 0) goto L_0x006a
            java.lang.Object r1 = r4.next()     // Catch:{ IOException -> 0x008f }
            X.0GR r1 = (X.AnonymousClass0GR) r1     // Catch:{ IOException -> 0x008f }
            X.0Eq r3 = X.AnonymousClass0Eq.A00()     // Catch:{ IOException -> 0x008f }
            java.lang.String r0 = r1.A03     // Catch:{ IOException -> 0x008f }
            int r2 = X.AnonymousClass0GL.A00(r0)     // Catch:{ IOException -> 0x008f }
            java.lang.String r1 = r1.A01     // Catch:{ IOException -> 0x008f }
            monitor-enter(r3)     // Catch:{ IOException -> 0x008f }
            boolean r0 = X.AnonymousClass0GL.A02(r2)     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0065
            java.lang.String[] r0 = r3.A02     // Catch:{ all -> 0x0067 }
            r0[r2] = r1     // Catch:{ all -> 0x0067 }
            r0 = 3
            boolean r0 = X.C010708t.A0X(r0)     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0065
            X.AnonymousClass0GL.A01(r2)     // Catch:{ all -> 0x0067 }
        L_0x0065:
            monitor-exit(r3)     // Catch:{ IOException -> 0x008f }
            goto L_0x0038
        L_0x0067:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ IOException -> 0x008f }
            throw r0     // Catch:{ IOException -> 0x008f }
        L_0x006a:
            X.AnonymousClass0GP.A00 = r6     // Catch:{ IOException -> 0x008f }
            goto L_0x0097
        L_0x006d:
            r0 = move-exception
            r2.close()     // Catch:{ FileNotFoundException -> 0x0072 }
            throw r0     // Catch:{ FileNotFoundException -> 0x0072 }
        L_0x0072:
            r4 = move-exception
            java.io.IOException r3 = new java.io.IOException     // Catch:{ IOException -> 0x008f }
            java.util.Locale r2 = java.util.Locale.US     // Catch:{ IOException -> 0x008f }
            java.lang.String r0 = ""
            java.lang.String[] r0 = r5.list(r0)     // Catch:{ IOException -> 0x008f }
            java.lang.String r0 = java.util.Arrays.toString(r0)     // Catch:{ IOException -> 0x008f }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ IOException -> 0x008f }
            java.lang.String r0 = "app_modules.json not found, assets = %s"
            java.lang.String r0 = java.lang.String.format(r2, r0, r1)     // Catch:{ IOException -> 0x008f }
            r3.<init>(r0, r4)     // Catch:{ IOException -> 0x008f }
            throw r3     // Catch:{ IOException -> 0x008f }
        L_0x008f:
            r2 = move-exception
            java.lang.String r1 = "DownloadableAppModuleMetadataReader"
            java.lang.String r0 = "Error loading downloadable module metadata"
            X.C010708t.A0T(r1, r2, r0)     // Catch:{ all -> 0x0099 }
        L_0x0097:
            monitor-exit(r7)
            return
        L_0x0099:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GP.A00(android.content.Context):void");
    }
}
