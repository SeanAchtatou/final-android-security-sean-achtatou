package X;

import android.util.Log;
import com.facebook.litho.ComponentTree;

/* renamed from: X.16J  reason: invalid class name */
public abstract class AnonymousClass16J implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.litho.ThreadTracingRunnable";
    public final Throwable A00;

    public void A00(AnonymousClass16J r8) {
        if (this instanceof AnonymousClass16K) {
            AnonymousClass16K r4 = (AnonymousClass16K) this;
            C157047Nv r3 = r4.A01;
            synchronized (r4) {
                if (r4.A03) {
                    int i = r4.A00;
                    String str = r4.A02;
                    r4.A00 = -1;
                    r4.A02 = null;
                    r4.A03 = false;
                    try {
                        AnonymousClass1AC.A0E(r4.A05, i, str, r8, r3);
                    } catch (IndexOutOfBoundsException e) {
                        throw new RuntimeException(AnonymousClass08S.A0J(AnonymousClass1AC.A04(r4.A05), e.getMessage()), e);
                    }
                }
            }
        } else if (this instanceof AnonymousClass16L) {
            AnonymousClass16L r0 = (AnonymousClass16L) this;
            AnonymousClass1AC r1 = r0.A01;
            try {
                AnonymousClass1AC.A0F(r1, r0.A00);
            } catch (IndexOutOfBoundsException e2) {
                throw new RuntimeException(AnonymousClass08S.A0J(AnonymousClass1AC.A04(r1), e2.getMessage()), e2);
            }
        } else if (!(this instanceof AnonymousClass16M)) {
            AnonymousClass16I r02 = (AnonymousClass16I) this;
            ComponentTree.A0A(r02.A04, null, r02.A00, r02.A02, r02.A01, r02.A03);
        } else {
            AnonymousClass16M r03 = (AnonymousClass16M) this;
            r03.A02.A0W(false, r03.A00, r03.A01);
        }
    }

    public final void run() {
        try {
            A00(this);
        } catch (Throwable th) {
            if (this.A00 != null) {
                Log.w("LithoThreadTracing", "--- start debug trace");
                Log.w("LithoThreadTracing", "Thread tracing stacktrace", this.A00);
                Log.w("LithoThreadTracing", "--- end debug trace");
            }
            throw th;
        }
    }

    public AnonymousClass16J() {
        this(AnonymousClass07c.enableThreadTracingStacktrace);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass16J(X.AnonymousClass16J r3) {
        /*
            r2 = this;
            if (r3 == 0) goto L_0x0017
            java.lang.Throwable r1 = r3.A00
            r0 = 0
            if (r1 == 0) goto L_0x0008
            r0 = 1
        L_0x0008:
            r2.<init>(r0)
            java.lang.Throwable r1 = r2.A00
            if (r1 == 0) goto L_0x0016
            if (r3 == 0) goto L_0x0016
            java.lang.Throwable r0 = r3.A00
            r1.initCause(r0)
        L_0x0016:
            return
        L_0x0017:
            boolean r0 = X.AnonymousClass07c.enableThreadTracingStacktrace
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass16J.<init>(X.16J):void");
    }

    private AnonymousClass16J(boolean z) {
        if (z) {
            Thread currentThread = Thread.currentThread();
            this.A00 = new Throwable("Runnable instantiated on thread id: " + currentThread.getId() + ", name: " + currentThread.getName());
            return;
        }
        this.A00 = null;
    }
}
