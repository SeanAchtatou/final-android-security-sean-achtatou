package X;

import android.database.DataSetObserver;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.1Qe  reason: invalid class name and case insensitive filesystem */
public final class C23611Qe extends C20831Dz implements AnonymousClass062, C24161Sn {
    public RecyclerView A00;
    public AnonymousClass09P A01;
    public AnonymousClass1Y6 A02;
    public boolean A03;
    private int A04;
    private boolean A05;
    public final C27356DbG A06;
    private final DataSetObserver A07 = new AnonymousClass5AO(this);
    private final C15730vo A08 = new C21711Ik(this);

    public int ArU() {
        return this.A06.getCount();
    }

    public void C0O(C15730vo r4) {
        this.A04++;
        super.C0O(r4);
        if (!this.A05) {
            this.A06.registerDataSetObserver(this.A07);
            this.A05 = true;
        }
    }

    public void CJm(C15730vo r3) {
        this.A04--;
        super.CJm(r3);
        if (this.A05 && this.A04 == 0) {
            this.A06.unregisterDataSetObserver(this.A07);
            this.A05 = false;
        }
    }

    public Object getItem(int i) {
        return this.A06.getItem(i);
    }

    public C23611Qe(C27356DbG dbG, RecyclerView recyclerView, boolean z) {
        AnonymousClass1XX r1 = AnonymousClass1XX.get(recyclerView.getContext());
        this.A02 = AnonymousClass0UX.A07(r1);
        this.A01 = C04750Wa.A01(r1);
        A09(z);
        this.A06 = dbG;
        this.A00 = recyclerView;
        super.C0O(this.A08);
    }
}
