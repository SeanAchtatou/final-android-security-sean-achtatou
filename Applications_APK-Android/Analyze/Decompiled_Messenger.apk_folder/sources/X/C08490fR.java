package X;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.0fR  reason: invalid class name and case insensitive filesystem */
public final class C08490fR {
    public final ScheduledExecutorService A00;
    private final ExecutorService A01;

    public void A00(Runnable runnable) {
        AnonymousClass07A.A04(this.A01, runnable, -780376341);
    }

    public C08490fR(ExecutorService executorService, ScheduledExecutorService scheduledExecutorService) {
        this.A01 = executorService;
        this.A00 = scheduledExecutorService;
    }
}
