package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.1G9  reason: invalid class name */
public final class AnonymousClass1G9 {
    private static C05540Zi A03;
    public long A00 = -1;
    public final AnonymousClass069 A01;
    public final C25051Yd A02;

    public static final AnonymousClass1G9 A00(AnonymousClass1XY r4) {
        AnonymousClass1G9 r0;
        synchronized (AnonymousClass1G9.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new AnonymousClass1G9((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (AnonymousClass1G9) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass1G9(AnonymousClass1XY r3) {
        this.A01 = AnonymousClass067.A03(r3);
        this.A02 = AnonymousClass0WT.A00(r3);
    }
}
