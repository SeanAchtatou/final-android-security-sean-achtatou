package X;

import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.pillstub.PillViewStub;
import com.facebook.common.util.TriState;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.widget.recyclerview.BetterRecyclerView;
import com.facebook.widget.text.BetterTextView;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import java.util.concurrent.Future;

/* renamed from: X.1CV  reason: invalid class name */
public final class AnonymousClass1CV {
    public C06790c5 A00;
    public int A01;
    public int A02 = -1;
    public int A03 = -1;
    public int A04;
    public View A05;
    public PillViewStub A06;
    public AnonymousClass0UN A07;
    public AnonymousClass1Ri A08;
    public C10700ki A09 = C10700ki.ALL;
    public MigColorScheme A0A;
    public C20051As A0B;
    public C20061At A0C;
    public BetterRecyclerView A0D;
    public Future A0E;
    public boolean A0F;
    public final AnonymousClass1A9 A0G = new C35311r1(this);
    public final AnonymousClass06U A0H = new C32891mV(this);
    public final AnonymousClass0ZM A0I = new C32881mU(this);

    public static Pair A00(AnonymousClass1CV r5, ImmutableMap immutableMap, int i) {
        C149846x7 r2;
        Preconditions.checkNotNull(r5.A08, "Unread pill not initialized");
        Preconditions.checkNotNull(r5.A0C, "Unread pill not initialized");
        boolean z = false;
        try {
            InboxUnitItem inboxUnitItem = (InboxUnitItem) r5.A0C.A01.get(r5.A08.A0T(i).Ahm());
            if (inboxUnitItem instanceof InboxUnitThreadItem) {
                r2 = (C149846x7) immutableMap.get(((InboxUnitThreadItem) inboxUnitItem).A01.A0S);
                z = true;
            } else {
                r2 = null;
            }
            return new Pair(Boolean.valueOf(z), r2);
        } catch (IndexOutOfBoundsException e) {
            C010708t.A0V("UnreadThreadsPillController", e, "IndexOutOfBoundsException at position:%d", Integer.valueOf(i));
            return null;
        }
    }

    public static final AnonymousClass1CV A01(AnonymousClass1XY r1) {
        return new AnonymousClass1CV(r1);
    }

    public static void A02(AnonymousClass1CV r3) {
        Preconditions.checkNotNull(r3.A0D, "loading feature without initialization");
        r3.A0D.A12(r3.A0G);
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AKb, r3.A07)).BMm();
        BMm.A02(C06680bu.A0I, r3.A0H);
        C06790c5 A002 = BMm.A00();
        r3.A00 = A002;
        A002.A00();
        int i = AnonymousClass1Y3.BA5;
        if (((AnonymousClass42Q) AnonymousClass1XX.A02(4, i, r3.A07)).A03()) {
            A05(r3, ((AnonymousClass42Q) AnonymousClass1XX.A02(4, i, r3.A07)).A01());
        } else {
            ((C189216c) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B7s, r3.A07)).A08();
        }
    }

    public static void A03(AnonymousClass1CV r4) {
        if (r4.A06 != null) {
            MigColorScheme migColorScheme = r4.A0A;
            if (migColorScheme == null) {
                migColorScheme = C17190yT.A00();
            }
            PillViewStub pillViewStub = r4.A06;
            if (pillViewStub.A01 == null) {
                PillViewStub.A01(pillViewStub);
            }
            View view = pillViewStub.A01;
            C15980wI.A01(view, AnonymousClass6AJ.A00(view.getContext(), migColorScheme));
            ((BetterTextView) view.findViewById(2131299814)).setTextColor(migColorScheme.Aeb().AhV());
        }
    }

    public static void A04(AnonymousClass1CV r4) {
        AnonymousClass3E6 r2;
        BetterRecyclerView betterRecyclerView = r4.A0D;
        if (betterRecyclerView != null) {
            betterRecyclerView.A13(r4.A0G);
        }
        C06790c5 r0 = r4.A00;
        if (r0 != null) {
            r0.A01();
            r4.A00 = null;
        }
        PillViewStub pillViewStub = r4.A06;
        if (!(pillViewStub == null || (r2 = pillViewStub.A05) == null)) {
            r2.A04(0.0d);
            pillViewStub.A06 = false;
        }
        r4.A04 = 0;
        r4.A0F = false;
    }

    public static void A05(AnonymousClass1CV r3, int i) {
        TriState triState;
        ((AnonymousClass4LX) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BHq, r3.A07)).A01(i, "unreadpill", null);
        int i2 = r3.A04;
        if (i > i2) {
            triState = TriState.YES;
        } else if (i == 0) {
            triState = TriState.NO;
        } else {
            triState = TriState.UNSET;
        }
        if (i2 != i) {
            r3.A04 = i;
            r3.A0D();
        }
        A08(r3, triState);
    }

    public static void A06(AnonymousClass1CV r6, int i) {
        Preconditions.checkNotNull(r6.A0D, "Unread pill not initialized");
        int A032 = RecyclerView.A03(r6.A0D.getChildAt(0));
        BetterRecyclerView betterRecyclerView = r6.A0D;
        int A033 = RecyclerView.A03(betterRecyclerView.getChildAt(betterRecyclerView.getChildCount() - 1));
        if (i < A032) {
            r6.A0D.A0m(i);
        } else if (i <= A033) {
            View childAt = r6.A0D.getChildAt(i - A032);
            if (childAt != null) {
                r6.A0D.A0q(0, childAt.getTop());
            } else {
                C010708t.A0P("UnreadThreadsPillController", "Can't find target view: %d, %d, %d", Integer.valueOf(A032), Integer.valueOf(i), Integer.valueOf(A033));
            }
        } else {
            DD2 dd2 = new DD2(r6.A0D.getContext());
            dd2.A00 = i;
            r6.A0D.A0L.A1I(dd2);
        }
    }

    public static void A07(AnonymousClass1CV r3, C149836x6 r4, Long l, Long l2, Long l3) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(9, AnonymousClass1Y3.Ap7, r3.A07)).A01("msgr_unread_pill"), AnonymousClass1Y3.A3o);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A00.A03("event", r4);
            uSLEBaseShape0S0000000.A0C("unread_pill_count", l);
            uSLEBaseShape0S0000000.A0C("current_position", l2);
            uSLEBaseShape0S0000000.A0C("destination_position", l3);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public static void A08(AnonymousClass1CV r4, TriState triState) {
        AnonymousClass108 r1 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, r4.A07);
        r1.A02(new AnonymousClass6x8(r4, triState));
        r1.A02 = "UnreadPill";
        r1.A03("ForUiThread");
        r1.A04 = triState.toString();
        ((AnonymousClass0g4) AnonymousClass1XX.A02(11, AnonymousClass1Y3.ASI, r4.A07)).A04(r1.A01(), "ReplaceExisting");
    }

    private boolean A09() {
        if (this.A04 == 0) {
            return true;
        }
        Preconditions.checkNotNull(this.A08, "Unread pill not initialized");
        int AZr = this.A08.AZr();
        ImmutableMap A022 = ((AnonymousClass42Q) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BA5, this.A07)).A02();
        int i = 0;
        for (int AZo = this.A08.AZo(); AZo <= AZr; AZo++) {
            Pair A002 = A00(this, A022, AZo);
            if (A002 == null) {
                C010708t.A0P("UnreadThreadsPillController", "areAllUnreadItemsFullyVisible - curr:%d - null at position", Integer.valueOf(AZo));
                return false;
            }
            if (((Boolean) A002.first).booleanValue()) {
                Object obj = A002.second;
                if (obj == null) {
                    break;
                } else if (((C149846x7) obj).A00 > 0 && (i = i + 1) == this.A04) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean A0A(AnonymousClass1CV r4) {
        if (!((C35321r2) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Avj, r4.A07)).A00.Aem(282282430694678L) || !((FbSharedPreferences) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B6q, r4.A07)).Aep(C05690aA.A18, true)) {
            return false;
        }
        return true;
    }

    public static boolean A0B(AnonymousClass1CV r4) {
        Preconditions.checkNotNull(r4.A08, "Unread pill not initialized");
        Preconditions.checkNotNull(r4.A0C, "Unread pill not initialized");
        InboxUnitItem inboxUnitItem = null;
        if (r4.A08.ArU() == 0) {
            return false;
        }
        try {
            C20061At r2 = r4.A0C;
            AnonymousClass1Ri r1 = r4.A08;
            inboxUnitItem = (InboxUnitItem) r2.A01.get(r1.A0T(r1.ArU() - 1).Ahm());
        } catch (IndexOutOfBoundsException unused) {
        }
        return inboxUnitItem instanceof InboxMoreThreadsItem;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        if (r2 == -1) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0C(X.AnonymousClass1CV r8, com.facebook.common.util.TriState r9) {
        /*
            X.1Ri r0 = r8.A08
            if (r0 == 0) goto L_0x000c
            int r2 = r0.AZo()
            r1 = -1
            r0 = 1
            if (r2 != r1) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            r4 = 0
            if (r0 != 0) goto L_0x0024
            com.facebook.common.pillstub.PillViewStub r3 = r8.A06
            if (r3 == 0) goto L_0x0023
            boolean r0 = r3.A06
            if (r0 == 0) goto L_0x0023
            X.3E6 r2 = r3.A05
            if (r2 == 0) goto L_0x0023
            r0 = 0
            r2.A04(r0)
            r3.A06 = r4
        L_0x0023:
            return r4
        L_0x0024:
            com.facebook.common.pillstub.PillViewStub r0 = r8.A06
            if (r0 != 0) goto L_0x00ae
            android.view.View r1 = r8.A05
            java.lang.String r0 = "Failed to inflate unread_threads_pill"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            android.view.View r1 = r8.A05
            r0 = 2131299813(0x7f090de5, float:1.8217638E38)
            android.view.View r1 = X.C012809p.A01(r1, r0)
            com.facebook.common.pillstub.PillViewStub r1 = (com.facebook.common.pillstub.PillViewStub) r1
            r8.A06 = r1
            r1.A04 = r8
            android.view.View r0 = r1.A01
            if (r0 != 0) goto L_0x0045
            com.facebook.common.pillstub.PillViewStub.A01(r1)
        L_0x0045:
            android.view.View r2 = r1.A01
            X.6x2 r0 = new X.6x2
            r0.<init>(r8)
            r2.setOnTouchListener(r0)
            A03(r8)
            android.util.DisplayMetrics r2 = new android.util.DisplayMetrics
            r2.<init>()
            int r1 = X.AnonymousClass1Y3.AR1
            X.0UN r0 = r8.A07
            r7 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            android.view.Display r0 = r0.getDefaultDisplay()
            r0.getMetrics(r2)
            int r0 = r2.widthPixels
            r8.A01 = r0
            com.facebook.common.pillstub.PillViewStub r1 = r8.A06
            android.view.View r0 = r1.A01
            if (r0 != 0) goto L_0x0076
            com.facebook.common.pillstub.PillViewStub.A01(r1)
        L_0x0076:
            android.view.View r6 = r1.A01
            r0 = 2131299814(0x7f090de6, float:1.821764E38)
            android.view.View r5 = r6.findViewById(r0)
            android.content.res.Resources r2 = r6.getResources()
            r1 = 1092616192(0x41200000, float:10.0)
            android.util.DisplayMetrics r0 = r2.getDisplayMetrics()
            float r0 = android.util.TypedValue.applyDimension(r7, r1, r0)
            int r3 = java.lang.Math.round(r0)
            r1 = 1098907648(0x41800000, float:16.0)
            android.util.DisplayMetrics r0 = r2.getDisplayMetrics()
            float r0 = android.util.TypedValue.applyDimension(r7, r1, r0)
            int r2 = java.lang.Math.round(r0)
            android.view.ViewGroup$LayoutParams r1 = r6.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r1 = (android.widget.LinearLayout.LayoutParams) r1
            r1.setMargins(r4, r4, r4, r2)
            r6.requestLayout()
            r5.setPadding(r2, r3, r2, r3)
        L_0x00ae:
            com.facebook.common.pillstub.PillViewStub r1 = r8.A06
            java.lang.String r0 = "Failed to inflate pill_stub"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.UNSET
            com.facebook.common.pillstub.PillViewStub r0 = r8.A06
            boolean r0 = r0.A06
            if (r0 != 0) goto L_0x0118
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            if (r9 == r0) goto L_0x00c5
            boolean r0 = r8.A0F
            if (r0 != 0) goto L_0x0118
        L_0x00c5:
            boolean r0 = r8.A09()
            if (r0 != 0) goto L_0x0118
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.YES
        L_0x00cd:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r1 == r0) goto L_0x0023
            r9.toString()
            r1.toString()
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            if (r1 != r0) goto L_0x0108
            r8.A0F = r4
            com.facebook.common.pillstub.PillViewStub r3 = r8.A06
            com.facebook.common.pillstub.PillViewStub.A01(r3)
            X.1CV r2 = r3.A04
            int r1 = r2.A04
            r0 = 0
            if (r1 <= 0) goto L_0x00ea
            r0 = 1
        L_0x00ea:
            if (r0 == 0) goto L_0x00f9
            r2.A0D()
            X.3E6 r2 = r3.A05
            r0 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r2.A05(r0)
            r0 = 1
            r3.A06 = r0
        L_0x00f9:
            X.6x6 r2 = X.C149836x6.UNREAD_PILL_SHOWN
        L_0x00fb:
            int r0 = r8.A04
            long r0 = (long) r0
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            r0 = 0
            A07(r8, r2, r1, r0, r0)
            r0 = 1
            return r0
        L_0x0108:
            com.facebook.common.pillstub.PillViewStub r3 = r8.A06
            X.3E6 r2 = r3.A05
            if (r2 == 0) goto L_0x0115
            r0 = 0
            r2.A05(r0)
            r3.A06 = r4
        L_0x0115:
            X.6x6 r2 = X.C149836x6.UNREAD_PILL_HIDDEN
            goto L_0x00fb
        L_0x0118:
            com.facebook.common.pillstub.PillViewStub r0 = r8.A06
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x00cd
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO
            if (r9 == r0) goto L_0x0128
            boolean r0 = r8.A09()
            if (r0 == 0) goto L_0x00cd
        L_0x0128:
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.NO
            goto L_0x00cd
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1CV.A0C(X.1CV, com.facebook.common.util.TriState):boolean");
    }

    public void A0D() {
        PillViewStub pillViewStub = this.A06;
        if (pillViewStub != null) {
            Resources resources = pillViewStub.getResources();
            int i = this.A04;
            String quantityString = resources.getQuantityString(2131689768, i, Integer.valueOf(i));
            AnonymousClass4LX r7 = (AnonymousClass4LX) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BHq, this.A07);
            int i2 = this.A04;
            synchronized (r7) {
                r7.A02.put("unreadpill", new C861348y(i2, r7.A01.now(), null));
            }
            PillViewStub pillViewStub2 = this.A06;
            PillViewStub.A01(pillViewStub2);
            TextView textView = pillViewStub2.A02;
            if (textView != null) {
                textView.setText(quantityString);
                return;
            }
            throw new IllegalStateException("Pill layout must have a TextView with id 'pill_text'");
        }
    }

    private AnonymousClass1CV(AnonymousClass1XY r3) {
        this.A07 = new AnonymousClass0UN(12, r3);
    }

    public void A0E(boolean z, boolean z2) {
        boolean z3;
        TriState triState;
        PillViewStub pillViewStub;
        if (A0A(this) && this.A09 == C10700ki.ALL) {
            if (z) {
                z3 = false;
            } else {
                z3 = this.A0F;
            }
            this.A0F = z3;
            if (z) {
                triState = TriState.YES;
            } else {
                triState = TriState.UNSET;
            }
            if (!A0C(this, triState) && (pillViewStub = this.A06) != null && z2) {
                if (pillViewStub.A01 == null) {
                    PillViewStub.A01(pillViewStub);
                }
                ObjectAnimator.ofFloat(pillViewStub.A01, View.TRANSLATION_X, 0.0f, -25.0f, 0.0f, 25.0f, 0.0f).setDuration(500L).start();
            }
        }
    }
}
