package X;

import java.io.File;

/* renamed from: X.1ot  reason: invalid class name and case insensitive filesystem */
public final class C34201ot implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.storage.cask.plugins.scope.UserScopePluginControllerBase$1";
    public final /* synthetic */ C34161op A00;
    public final /* synthetic */ AnonymousClass13z A01;
    public final /* synthetic */ File A02;

    public C34201ot(C34161op r1, AnonymousClass13z r2, File file) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = file;
    }

    public void run() {
        this.A00.A03(this.A01, this.A02);
    }
}
