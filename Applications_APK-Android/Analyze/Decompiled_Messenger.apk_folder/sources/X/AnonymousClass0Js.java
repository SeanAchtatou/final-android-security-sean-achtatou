package X;

/* renamed from: X.0Js  reason: invalid class name */
public final class AnonymousClass0Js implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.appstatelogger.AppStateLoggerEnabler$FbErrorReporterAppStateErrorLogger$1";
    public final /* synthetic */ AnonymousClass0Nd A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ Throwable A02;

    public AnonymousClass0Js(AnonymousClass0Nd r1, Throwable th, String str) {
        this.A00 = r1;
        this.A02 = th;
        this.A01 = str;
    }

    public void run() {
        Throwable th = this.A02;
        if (th == null) {
            this.A00.A00.CGS("AppStateLogger", this.A01);
        } else {
            this.A00.A00.softReport("AppStateLogger", this.A01, th);
        }
    }
}
