package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;

/* renamed from: X.00n  reason: invalid class name */
public final class AnonymousClass00n extends C000900o {
    public static final int A00;
    public static final int A01;
    public static final int A02;
    public static final int A03 = ProvidersRegistry.A00.A02("fbsystrace");
    public static final int A04;
    public static final int A05 = ProvidersRegistry.A00.A02("liger");
    public static final int A06;
    public static final int A07;
    public static final int A08;
    public static final int A09;

    public AnonymousClass00n() {
        super(null);
    }

    public boolean A08() {
        return true;
    }

    static {
        AnonymousClass08Y r1 = ProvidersRegistry.A00;
        A01 = r1.A02("async");
        A04 = r1.A02("lifecycle");
        A08 = r1.A02("other");
        A09 = r1.A02("user_counters");
        A02 = r1.A02("class_load");
        A06 = r1.A02("main_thread_messages");
        int A022 = ProvidersRegistry.A00.A02("multiprocess");
        A07 = A022;
        A00 = A01 | A04 | A08 | A09 | A02 | A06 | A03 | A022;
    }

    public int getTracingProviders() {
        return A00 & TraceEvents.sProviders;
    }

    public void disable() {
        C000700l.A09(-1715218956, C000700l.A03(643833786));
    }

    public void enable() {
        C000700l.A09(-253787373, C000700l.A03(-1427059542));
    }

    public int getSupportedProviders() {
        return A00;
    }
}
