package X;

import android.content.Context;

/* renamed from: X.0dr  reason: invalid class name and case insensitive filesystem */
public final class C07620dr implements AnonymousClass062 {
    public AnonymousClass0UN A00;

    public static void A00(Context context) {
        ((AnonymousClass0UW) AnonymousClass1XX.A03(AnonymousClass1Y3.ALy, new C07620dr(context).A00)).A03();
    }

    private C07620dr(Context context) {
        this.A00 = new AnonymousClass0UN(0, AnonymousClass1XX.get(context));
    }
}
