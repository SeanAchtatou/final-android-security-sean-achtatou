package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.customthreads.model.ThreadThemeInfo;

/* renamed from: X.12T  reason: invalid class name */
public final class AnonymousClass12T implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadThemeInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadThemeInfo[i];
    }
}
