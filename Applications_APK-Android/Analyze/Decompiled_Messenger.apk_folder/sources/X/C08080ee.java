package X;

import android.util.SparseArray;

/* renamed from: X.0ee  reason: invalid class name and case insensitive filesystem */
public final class C08080ee {
    public static C08080ee A01 = new C08080ee();
    public final SparseArray A00 = new SparseArray();

    public synchronized AnonymousClass0f6 A00(int i) {
        AnonymousClass0f6 r1;
        r1 = (AnonymousClass0f6) this.A00.get(i);
        if (r1 == null) {
            r1 = C57282rj.A00;
            this.A00.put(i, r1);
        }
        return r1;
    }

    public synchronized AnonymousClass0f6 A01(int i) {
        return (AnonymousClass0f6) this.A00.get(i);
    }

    private C08080ee() {
    }
}
