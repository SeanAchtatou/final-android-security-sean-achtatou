package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ON  reason: invalid class name */
public final class AnonymousClass1ON extends AnonymousClass1OO implements C05460Za, AnonymousClass1OP, AnonymousClass1OQ {
    public static final long A06 = TimeUnit.DAYS.toMillis(5);
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D;
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F;
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y7 A0H;
    public static final AnonymousClass1Y7 A0I;
    public static final AnonymousClass1Y7 A0J;
    public static final AnonymousClass1Y7 A0K;
    public static final AnonymousClass1Y7 A0L;
    public static final AnonymousClass1Y7 A0M;
    public static final AnonymousClass1Y7 A0N;
    public static final AnonymousClass1Y7 A0O;
    public static final AnonymousClass1Y7 A0P;
    public static final AnonymousClass1Y7 A0Q;
    public static final AnonymousClass1Y7 A0R;
    public static final AnonymousClass1Y7 A0S;
    public static final AnonymousClass1Y7 A0T;
    private static volatile AnonymousClass1ON A0U;
    public final DeprecatedAnalyticsLogger A00;
    public final AnonymousClass06B A01 = AnonymousClass067.A02();
    public final FbSharedPreferences A02;
    private final C05920aY A03;
    private final C22601Mc A04;
    private final Random A05;

    public synchronized void clearUserData() {
        if (this.A02.BBh(A07) && !this.A02.BBh(A0L)) {
            C30281hn edit = this.A02.edit();
            edit.putBoolean(A09, true);
            edit.commit();
        }
    }

    public synchronized void trimToMinimum() {
        if (this.A02.BBh(A07)) {
            long At2 = this.A02.At2(A0E, 0);
            C30281hn edit = this.A02.edit();
            edit.BzA(A0S, this.A01.now() - At2);
            edit.commit();
        }
    }

    public synchronized void trimToNothing() {
        if (this.A02.BBh(A07)) {
            long At2 = this.A02.At2(A0E, 0);
            C30281hn edit = this.A02.edit();
            edit.BzA(A0T, this.A01.now() - At2);
            edit.commit();
        }
    }

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A05.A09("photos_eviction")).A09("tracking_state");
        A0R = r1;
        A07 = (AnonymousClass1Y7) r1.A09("cache_key");
        AnonymousClass1Y7 r12 = A0R;
        A0N = (AnonymousClass1Y7) r12.A09("resource_id");
        A0O = (AnonymousClass1Y7) r12.A09("size_bytes");
        A08 = (AnonymousClass1Y7) r12.A09("eviction_unix_time");
        A09 = (AnonymousClass1Y7) r12.A09("logout_detected");
        A0T = (AnonymousClass1Y7) r12.A09("trim_time");
        A0S = (AnonymousClass1Y7) r12.A09("min_trim_time");
        A0B = (AnonymousClass1Y7) r12.A09("o_calling_class");
        A0A = (AnonymousClass1Y7) r12.A09("o_analytics_tag");
        A0D = (AnonymousClass1Y7) r12.A09("o_is_prefetch");
        A0C = (AnonymousClass1Y7) r12.A09("o_cancel_req");
        A0F = (AnonymousClass1Y7) r12.A09("o_user_id");
        A0E = (AnonymousClass1Y7) r12.A09("o_unix_time");
        A0J = (AnonymousClass1Y7) r12.A09("r_count");
        A0H = (AnonymousClass1Y7) r12.A09("r_calling_class");
        A0G = (AnonymousClass1Y7) r12.A09("r_analytics_tag");
        A0K = (AnonymousClass1Y7) r12.A09("r_is_prefetch");
        A0I = (AnonymousClass1Y7) r12.A09("r_cancel_req");
        A0M = (AnonymousClass1Y7) r12.A09("r_user_id");
        A0L = (AnonymousClass1Y7) r12.A09("r_unix_time");
        A0P = (AnonymousClass1Y7) r12.A09("total_bytes");
        A0Q = (AnonymousClass1Y7) r12.A09("total_requests");
    }

    private int A00() {
        ViewerContext Asn = this.A03.Asn();
        if (Asn == null) {
            return -1;
        }
        String str = Asn.mUserId;
        if (!Platform.stringIsNullOrEmpty(str)) {
            return str.hashCode();
        }
        return -1;
    }

    public static final AnonymousClass1ON A01(AnonymousClass1XY r5) {
        if (A0U == null) {
            synchronized (AnonymousClass1ON.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0U, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0U = new AnonymousClass1ON(applicationInjector, C30721iX.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0U;
    }

    private void A02(long j) {
        Preconditions.checkState(this.A02.BBh(A07));
        C30281hn edit = this.A02.edit();
        edit.BzA(A0P, this.A02.At2(A0P, 0) + j);
        edit.BzA(A0Q, this.A02.At2(A0Q, 0) + 1);
        edit.commit();
    }

    private void A03(C23601Qd r5, CallerContext callerContext, long j, boolean z, boolean z2) {
        Preconditions.checkState(!this.A02.BBh(A07));
        if (this.A05.nextInt() % 30 == 0) {
            C30281hn edit = this.A02.edit();
            edit.BzC(A07, r5.toString());
            edit.BzC(A0N, AnonymousClass1SG.A00(r5));
            edit.BzA(A0O, j);
            edit.BzC(A0B, callerContext.A02);
            edit.BzC(A0A, callerContext.A0F());
            edit.putBoolean(A0D, z);
            edit.putBoolean(A0C, z2);
            edit.BzA(A0E, this.A01.now());
            edit.Bz6(A0F, A00());
            edit.commit();
            A02(j);
            Preconditions.checkState(this.A02.BBh(A07));
            A00();
        }
    }

    public synchronized void BgO(AnonymousClass1Q0 r23, CallerContext callerContext, int i, boolean z, boolean z2) {
        long j;
        CallerContext callerContext2 = callerContext;
        C23601Qd A052 = this.A04.A05(r23, callerContext2);
        int i2 = i;
        boolean z3 = z2;
        boolean z4 = z;
        if (this.A02.BBh(A07)) {
            if (this.A01.now() - this.A02.At2(A0E, 0) > A06) {
                Preconditions.checkState(this.A02.BBh(A07));
                C11670nb r6 = new C11670nb("photos_eviction_tracking");
                r6.A0A("bytes", this.A02.At2(A0O, -1));
                r6.A0D("original_calling_class", this.A02.B4F(A0B, null));
                r6.A0D("original_analytics_tag", this.A02.B4F(A0A, null));
                r6.A0E("original_is_prefetch", this.A02.Aep(A0D, false));
                r6.A0E("original_cancel_requested", this.A02.Aep(A0C, false));
                if (this.A02.BBh(A0L)) {
                    r6.A09("refetch_count", this.A02.AqN(A0J, 0));
                    r6.A0D("refetch_calling_class", this.A02.B4F(A0H, null));
                    r6.A0D("refetch_analytics_tag", this.A02.B4F(A0G, null));
                    r6.A0E("refetch_is_prefetch", this.A02.Aep(A0K, false));
                    r6.A0E("refetch_cancel_requested", this.A02.Aep(A0I, false));
                    r6.A0A("refetched_after", this.A02.At2(A0L, 0) - this.A02.At2(A0E, 0));
                    boolean z5 = false;
                    if (this.A02.AqN(A0F, -1) != this.A02.AqN(A0M, -1)) {
                        z5 = true;
                    }
                    r6.A0E("diferent_user", z5);
                }
                FbSharedPreferences fbSharedPreferences = this.A02;
                AnonymousClass1Y7 r1 = A08;
                if (fbSharedPreferences.BBh(r1)) {
                    j = this.A02.At2(r1, 0) - this.A02.At2(A0E, 0);
                } else {
                    j = 0;
                }
                r6.A0A("evicted_after", j);
                r6.A0E("logout_detected", this.A02.Aep(A09, false));
                r6.A0A("trim_to_nothing_time", this.A02.At2(A0T, -1));
                r6.A0A("trim_to_min_time", this.A02.At2(A0S, -1));
                r6.A0A("total_bytes", this.A02.At2(A0P, 0));
                r6.A0A("total_requests", this.A02.At2(A0Q, 0));
                this.A00.A09(r6);
                C30281hn edit = this.A02.edit();
                edit.C24(A0R);
                edit.commit();
                Preconditions.checkState(!this.A02.BBh(A07));
            } else {
                A02((long) i2);
                if (this.A02.B4F(A07, BuildConfig.FLAVOR).equals(A052.toString())) {
                    Preconditions.checkState(this.A02.BBh(A07));
                    FbSharedPreferences fbSharedPreferences2 = this.A02;
                    C30281hn edit2 = fbSharedPreferences2.edit();
                    AnonymousClass1Y7 r12 = A0J;
                    edit2.Bz6(r12, fbSharedPreferences2.AqN(r12, 0) + 1);
                    edit2.commit();
                    if (!this.A02.BBh(A0L)) {
                        C30281hn edit3 = this.A02.edit();
                        edit3.BzC(A0H, callerContext2.A02);
                        edit3.BzC(A0G, callerContext2.A0F());
                        edit3.putBoolean(A0K, z4);
                        edit3.putBoolean(A0I, z3);
                        edit3.BzA(A0L, this.A01.now());
                        edit3.Bz6(A0M, A00());
                        edit3.commit();
                        A00();
                    }
                }
            }
        }
        CallerContext callerContext3 = callerContext2;
        A03(A052, callerContext3, (long) i2, z4, z3);
    }

    private AnonymousClass1ON(AnonymousClass1XY r2, C30721iX r3) {
        this.A00 = C06920cI.A00(r2);
        this.A02 = FbSharedPreferencesModule.A00(r2);
        this.A03 = C10580kT.A01(r2);
        this.A04 = C22591Mb.A01(r2);
        this.A05 = AnonymousClass0W9.A01();
        r3.C0U(this);
    }
}
