package X;

import android.content.Context;
import com.facebook.common.file.FileModule;

/* renamed from: X.0iH  reason: invalid class name and case insensitive filesystem */
public final class C09670iH {
    public final Context A00;
    public final C09680iI A01;
    public final C09400hF A02;
    public final C09700iK A03;

    public static final C09670iH A00(AnonymousClass1XY r1) {
        return new C09670iH(r1);
    }

    private C09670iH(AnonymousClass1XY r2) {
        this.A01 = FileModule.A00(r2);
        this.A03 = C09700iK.A01(r2);
        this.A02 = C09450hO.A00(r2);
        this.A00 = AnonymousClass1YA.A00(r2);
    }
}
