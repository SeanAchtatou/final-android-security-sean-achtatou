package X;

import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.accountswitch.model.MessengerAccountInfo;
import com.facebook.user.model.User;
import java.util.ArrayList;
import java.util.Iterator;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ce  reason: invalid class name and case insensitive filesystem */
public final class C27101ce implements C27081cc {
    private static volatile C27101ce A05;
    public C49132bh A00;
    private AnonymousClass0UN A01;
    private ArrayList A02 = new ArrayList();
    @LoggedInUser
    private final C04310Tq A03;
    public volatile boolean A04;

    public static synchronized void A01(C27101ce r5) {
        synchronized (r5) {
            r5.A02.clear();
            if (((C27111cf) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BR2, r5.A01)).A01()) {
                int i = AnonymousClass1Y3.AJE;
                if (((AnonymousClass9VD) AnonymousClass1XX.A02(0, i, r5.A01)).A06()) {
                    r5.A04 = true;
                    C24971Xv it = ((AnonymousClass9VD) AnonymousClass1XX.A02(0, i, r5.A01)).A03().A2s().A1j().A3F().iterator();
                    while (it.hasNext()) {
                        GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) it.next();
                        String A0P = gSTModelShape1S0000000.A0P(1615086568);
                        if (A0P != null) {
                            C57382rt r2 = new C57382rt();
                            r2.A02 = A0P;
                            r2.A04 = gSTModelShape1S0000000.A3m();
                            r2.A05 = true;
                            r5.A02.add(new MessengerAccountInfo(r2));
                        }
                    }
                } else {
                    ((AnonymousClass9VQ) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AdS, r5.A01)).CHB(AnonymousClass9W4.FORCED_SERVER_FETCH);
                }
            }
        }
    }

    public boolean BBk() {
        return false;
    }

    public static final C27101ce A00(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (C27101ce.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new C27101ce(applicationInjector, C04440Ur.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    /* renamed from: A02 */
    public ArrayList Abd() {
        if (!this.A04 || ((AnonymousClass9VD) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJE, this.A01)).A04()) {
            A01(this);
        }
        return this.A02;
    }

    public MessengerAccountInfo Aba(String str) {
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            MessengerAccountInfo messengerAccountInfo = (MessengerAccountInfo) it.next();
            String str2 = messengerAccountInfo.A04;
            if (str2 != null && str2.equals(str)) {
                return messengerAccountInfo;
            }
        }
        return null;
    }

    public int Aw9() {
        return this.A02.size();
    }

    public MessengerAccountInfo C0K() {
        User user = (User) this.A03.get();
        if (user == null) {
            return null;
        }
        MessengerAccountInfo Aba = Aba(user.A0j);
        if (Aba != null && Aba.A02 != null) {
            return Aba;
        }
        C57382rt r1 = new C57382rt();
        r1.A02 = user.A08();
        r1.A04 = user.A0j;
        r1.A05 = user.A0F();
        MessengerAccountInfo messengerAccountInfo = new MessengerAccountInfo(r1);
        C4Q(messengerAccountInfo);
        return messengerAccountInfo;
    }

    public void C1K(String str) {
        this.A02.remove(Aba(str));
    }

    public void C4Q(MessengerAccountInfo messengerAccountInfo) {
        MessengerAccountInfo Aba = Aba(messengerAccountInfo.A04);
        if (Aba != null) {
            this.A02.remove(Aba);
        }
        this.A02.add(messengerAccountInfo);
    }

    private C27101ce(AnonymousClass1XY r4, C04440Ur r5) {
        this.A01 = new AnonymousClass0UN(3, r4);
        this.A03 = AnonymousClass0WY.A01(r4);
        A01(this);
        C27131ch r2 = new C27131ch(this);
        C06600bl BMm = r5.BMm();
        BMm.A02(C06680bu.A01, r2);
        BMm.A02(C06680bu.A02, r2);
        BMm.A02(C06680bu.A03, r2);
        BMm.A00().A00();
    }

    public void C91(C49132bh r1) {
        this.A00 = r1;
    }
}
