package X;

import com.mapbox.mapboxsdk.geometry.LatLng;
import java.util.Iterator;

/* renamed from: X.1z9  reason: invalid class name and case insensitive filesystem */
public final class C39501z9 implements C28385Du1 {
    public final /* synthetic */ C28394DuB A00;

    public C39501z9(C28394DuB duB) {
        this.A00 = duB;
    }

    public boolean BeA(LatLng latLng) {
        if (this.A00.A0V.isEmpty() || !this.A00.A0D.A0I(latLng)) {
            return false;
        }
        Iterator it = this.A00.A0V.iterator();
        while (it.hasNext()) {
            ((C30517Ey2) it.next()).onLocationComponentClick();
        }
        return true;
    }
}
