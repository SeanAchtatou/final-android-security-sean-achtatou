package X;

import android.os.Build;
import android.text.Layout;
import android.text.TextPaint;
import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1QX  reason: invalid class name */
public final class AnonymousClass1QX {
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public Layout.Alignment A06;
    public boolean A07;
    public boolean A08;
    public float A09 = Float.MAX_VALUE;
    public float A0A = 0.0f;
    public float A0B = 1.0f;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public TextPaint A0G = new TextPaint(1);
    public TextUtils.TruncateAt A0H;
    public C22311Kv A0I;
    public CharSequence A0J;
    public boolean A0K = true;
    public boolean A0L;

    public void A00() {
        if (this.A07) {
            TextPaint textPaint = new TextPaint(this.A0G);
            textPaint.set(this.A0G);
            this.A0G = textPaint;
            this.A07 = false;
        }
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int color = (((this.A0G.getColor() + 31) * 31) + Float.floatToIntBits(this.A0G.getTextSize())) * 31;
        int i5 = 0;
        if (this.A0G.getTypeface() != null) {
            i = this.A0G.getTypeface().hashCode();
        } else {
            i = 0;
        }
        TextPaint textPaint = this.A0G;
        int floatToIntBits = (((((((((((((((((((((((((((((color + i) * 31) + Float.floatToIntBits(this.A00)) * 31) + Float.floatToIntBits(this.A01)) * 31) + Float.floatToIntBits(this.A02)) * 31) + this.A05) * 31) + textPaint.linkColor) * 31) + Float.floatToIntBits(textPaint.density)) * 31) + Arrays.hashCode(textPaint.drawableState)) * 31) + this.A0F) * 31) + this.A0E) * 31) + Float.floatToIntBits(this.A0B)) * 31) + Float.floatToIntBits(this.A0A)) * 31) + Float.floatToIntBits(this.A09)) * 31) + (this.A0K ? 1 : 0)) * 31) + (this.A08 ? 1 : 0)) * 31;
        TextUtils.TruncateAt truncateAt = this.A0H;
        if (truncateAt != null) {
            i2 = truncateAt.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (((((floatToIntBits + i2) * 31) + (this.A0L ? 1 : 0)) * 31) + this.A0D) * 31;
        Layout.Alignment alignment = this.A06;
        if (alignment != null) {
            i3 = alignment.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 31;
        C22311Kv r0 = this.A0I;
        if (r0 != null) {
            i4 = r0.hashCode();
        } else {
            i4 = 0;
        }
        int hashCode = (((((((((i7 + i4) * 31) + this.A0C) * 31) + this.A03) * 31) + Arrays.hashCode((int[]) null)) * 31) + Arrays.hashCode((int[]) null)) * 31;
        CharSequence charSequence = this.A0J;
        if (charSequence != null) {
            i5 = charSequence.hashCode();
        }
        return hashCode + i5;
    }

    public AnonymousClass1QX() {
        boolean z = true;
        this.A08 = Build.VERSION.SDK_INT < 28 ? false : z;
        this.A0H = null;
        this.A0L = false;
        this.A0D = Integer.MAX_VALUE;
        this.A06 = Layout.Alignment.ALIGN_NORMAL;
        this.A0I = C22181Kf.A01;
        this.A0C = 0;
        this.A03 = 0;
        this.A04 = 0;
        this.A07 = false;
    }
}
