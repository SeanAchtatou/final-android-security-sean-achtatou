package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.ListView;

/* renamed from: X.1Ed  reason: invalid class name and case insensitive filesystem */
public class C20871Ed extends ViewGroup implements C15310v5, C15200ut, C15290v2, C15700vi, C15190us {
    private static final int[] A0a = {16842766};
    public int A00;
    public int A01;
    public boolean A02;
    public float A03;
    public int A04;
    public int A05;
    public Animation A06;
    public Animation A07;
    public Animation A08;
    public Animation A09;
    public C15140un A0A;
    public C15160up A0B;
    public AnonymousClass199 A0C;
    public boolean A0D = false;
    private float A0E;
    private float A0F;
    private float A0G = -1.0f;
    private float A0H;
    private int A0I = -1;
    private int A0J;
    private int A0K = -1;
    private int A0L;
    private View A0M;
    private Animation.AnimationListener A0N = new C16300wp(this);
    private Animation A0O;
    private boolean A0P;
    private boolean A0Q;
    public int A0R;
    public final Animation A0S = new C16350wu(this);
    public final Animation A0T = new C16370ww(this);
    public final DecelerateInterpolator A0U;
    public final C15270v0 A0V;
    private final C32531lu A0W;
    private final int[] A0X = new int[2];
    private final int[] A0Y = new int[2];
    private final int[] A0Z = new int[2];

    private boolean A07() {
        AnonymousClass6OV r1 = null;
        if (r1 != null) {
            return r1.canChildScrollUp(this, this.A0M);
        }
        View view = this.A0M;
        if (!(view instanceof ListView)) {
            return view.canScrollVertically(-1);
        }
        ListView listView = (ListView) view;
        if (Build.VERSION.SDK_INT >= 19) {
            return listView.canScrollList(-1);
        }
        int childCount = listView.getChildCount();
        if (childCount == 0) {
            return false;
        }
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        if (-1 > 0) {
            int bottom = listView.getChildAt(childCount - 1).getBottom();
            if (firstVisiblePosition + childCount < listView.getCount() || bottom > listView.getHeight() - listView.getListPaddingBottom()) {
                return true;
            }
            return false;
        }
        int top = listView.getChildAt(0).getTop();
        if (firstVisiblePosition > 0 || top < listView.getListPaddingTop()) {
            return true;
        }
        return false;
    }

    public void A0C(boolean z) {
        int i;
        if (!z || this.A0D == z) {
            A06(z, false);
            return;
        }
        this.A0D = z;
        if (0 == 0) {
            i = this.A01 + this.A0R;
        } else {
            i = this.A01;
        }
        A0A(i - this.A00);
        this.A02 = false;
        Animation.AnimationListener animationListener = this.A0N;
        this.A0A.setVisibility(0);
        this.A0B.setAlpha(255);
        C621530f r2 = new C621530f(this);
        this.A08 = r2;
        r2.setDuration((long) this.A05);
        if (animationListener != null) {
            this.A0A.A01 = animationListener;
        }
        this.A0A.clearAnimation();
        this.A0A.startAnimation(this.A08);
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        if (i2 > 0) {
            float f = this.A0H;
            if (f > 0.0f) {
                float f2 = (float) i2;
                if (f2 > f) {
                    iArr[1] = (int) f;
                    this.A0H = 0.0f;
                } else {
                    this.A0H = f - f2;
                    iArr[1] = i2;
                }
                A03(this.A0H);
            }
        }
        if (0 != 0 && i2 > 0 && this.A0H == 0.0f && Math.abs(i2 - iArr[1]) > 0) {
            this.A0A.setVisibility(8);
        }
        int[] iArr2 = this.A0Z;
        if (dispatchNestedPreScroll(i - iArr[0], i2 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr[1] + iArr2[1];
        }
    }

    private void A01() {
        if (this.A0M == null) {
            for (int i = 0; i < getChildCount(); i++) {
                View childAt = getChildAt(i);
                if (!childAt.equals(this.A0A)) {
                    this.A0M = childAt;
                    return;
                }
            }
        }
    }

    private void A02(float f) {
        if (f > this.A0G) {
            A06(true, true);
            return;
        }
        this.A0D = false;
        C15160up r2 = this.A0B;
        C15630vb r1 = r2.A05;
        r1.A04 = 0.0f;
        r1.A01 = 0.0f;
        r2.invalidateSelf();
        C60142we r4 = null;
        if (0 == 0) {
            r4 = new C60142we(this);
        }
        int i = this.A00;
        if (0 != 0) {
            this.A04 = i;
            this.A03 = this.A0A.getScaleX();
            C60152wf r22 = new C60152wf(this);
            this.A09 = r22;
            r22.setDuration(150);
            if (r4 != null) {
                this.A0A.A01 = r4;
            }
            this.A0A.clearAnimation();
            this.A0A.startAnimation(this.A09);
        } else {
            this.A04 = i;
            this.A0T.reset();
            this.A0T.setDuration(200);
            this.A0T.setInterpolator(this.A0U);
            if (r4 != null) {
                this.A0A.A01 = r4;
            }
            this.A0A.clearAnimation();
            this.A0A.startAnimation(this.A0T);
        }
        C15160up r23 = this.A0B;
        C15630vb r12 = r23.A05;
        if (r12.A0F) {
            r12.A0F = false;
        }
        r23.invalidateSelf();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ad, code lost:
        if (r1.hasEnded() != false) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x012d, code lost:
        if (r1.hasEnded() != false) goto L_0x012f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A03(float r12) {
        /*
            r11 = this;
            X.0up r3 = r11.A0B
            X.0vb r2 = r3.A05
            r1 = 1
            boolean r0 = r2.A0F
            if (r0 == r1) goto L_0x000b
            r2.A0F = r1
        L_0x000b:
            r3.invalidateSelf()
            float r7 = r11.A0G
            float r0 = r12 / r7
            float r0 = java.lang.Math.abs(r0)
            r5 = 1065353216(0x3f800000, float:1.0)
            float r6 = java.lang.Math.min(r5, r0)
            double r2 = (double) r6
            r0 = 4600877379321698714(0x3fd999999999999a, double:0.4)
            double r2 = r2 - r0
            r0 = 0
            double r0 = java.lang.Math.max(r2, r0)
            float r4 = (float) r0
            r0 = 1084227584(0x40a00000, float:5.0)
            float r4 = r4 * r0
            r0 = 1077936128(0x40400000, float:3.0)
            float r4 = r4 / r0
            float r2 = java.lang.Math.abs(r12)
            float r2 = r2 - r7
            r1 = 0
            if (r1 > 0) goto L_0x0040
            r0 = 0
            if (r0 == 0) goto L_0x0152
            int r1 = r11.A01
            int r0 = r11.A0R
            int r1 = r1 - r0
        L_0x0040:
            float r9 = (float) r1
            r10 = 1073741824(0x40000000, float:2.0)
            float r0 = r9 * r10
            float r0 = java.lang.Math.min(r2, r0)
            float r0 = r0 / r9
            r7 = 0
            float r1 = java.lang.Math.max(r7, r0)
            r0 = 1082130432(0x40800000, float:4.0)
            float r1 = r1 / r0
            double r2 = (double) r1
            r0 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r0 = java.lang.Math.pow(r2, r0)
            double r2 = r2 - r0
            float r8 = (float) r2
            float r8 = r8 * r10
            float r0 = r9 * r8
            float r0 = r0 * r10
            int r3 = r11.A0R
            float r9 = r9 * r6
            float r9 = r9 + r0
            int r0 = (int) r9
            int r3 = r3 + r0
            X.0un r0 = r11.A0A
            int r0 = r0.getVisibility()
            if (r0 == 0) goto L_0x0073
            X.0un r1 = r11.A0A
            r0 = 0
            r1.setVisibility(r0)
        L_0x0073:
            r0 = 0
            if (r0 != 0) goto L_0x0080
            X.0un r0 = r11.A0A
            r0.setScaleX(r5)
            X.0un r0 = r11.A0A
            r0.setScaleY(r5)
        L_0x0080:
            r0 = 0
            if (r0 == 0) goto L_0x008e
            float r0 = r11.A0G
            float r0 = r12 / r0
            float r0 = java.lang.Math.min(r5, r0)
            r11.setAnimationProgress(r0)
        L_0x008e:
            float r0 = r11.A0G
            int r0 = (r12 > r0 ? 1 : (r12 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x0114
            X.0up r0 = r11.A0B
            int r0 = r0.getAlpha()
            r2 = 76
            if (r0 <= r2) goto L_0x00d1
            android.view.animation.Animation r1 = r11.A07
            if (r1 == 0) goto L_0x00af
            boolean r0 = r1.hasStarted()
            if (r0 == 0) goto L_0x00af
            boolean r1 = r1.hasEnded()
            r0 = 1
            if (r1 == 0) goto L_0x00b0
        L_0x00af:
            r0 = 0
        L_0x00b0:
            if (r0 != 0) goto L_0x00d1
            X.0up r0 = r11.A0B
            int r1 = r0.getAlpha()
            X.2wg r6 = new X.2wg
            r6.<init>(r11, r1, r2)
            r0 = 300(0x12c, double:1.48E-321)
            r6.setDuration(r0)
            X.0un r2 = r11.A0A
            r1 = 0
            r2.A01 = r1
            r2.clearAnimation()
            X.0un r0 = r11.A0A
            r0.startAnimation(r6)
            r11.A07 = r6
        L_0x00d1:
            r1 = 1061997773(0x3f4ccccd, float:0.8)
            float r0 = r4 * r1
            X.0up r2 = r11.A0B
            float r1 = java.lang.Math.min(r1, r0)
            X.0vb r0 = r2.A05
            r0.A04 = r7
            r0.A01 = r1
            r2.invalidateSelf()
            X.0up r6 = r11.A0B
            float r2 = java.lang.Math.min(r5, r4)
            X.0vb r1 = r6.A05
            float r0 = r1.A00
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x00f5
            r1.A00 = r2
        L_0x00f5:
            r6.invalidateSelf()
            r1 = -1098907648(0xffffffffbe800000, float:-0.25)
            r0 = 1053609165(0x3ecccccd, float:0.4)
            float r4 = r4 * r0
            float r4 = r4 + r1
            float r8 = r8 * r10
            float r4 = r4 + r8
            r0 = 1056964608(0x3f000000, float:0.5)
            float r4 = r4 * r0
            X.0up r1 = r11.A0B
            X.0vb r0 = r1.A05
            r0.A03 = r4
            r1.invalidateSelf()
            int r0 = r11.A00
            int r3 = r3 - r0
            r11.A0A(r3)
            return
        L_0x0114:
            X.0up r0 = r11.A0B
            int r0 = r0.getAlpha()
            r2 = 255(0xff, float:3.57E-43)
            if (r0 >= r2) goto L_0x00d1
            android.view.animation.Animation r1 = r11.A06
            if (r1 == 0) goto L_0x012f
            boolean r0 = r1.hasStarted()
            if (r0 == 0) goto L_0x012f
            boolean r1 = r1.hasEnded()
            r0 = 1
            if (r1 == 0) goto L_0x0130
        L_0x012f:
            r0 = 0
        L_0x0130:
            if (r0 != 0) goto L_0x00d1
            X.0up r0 = r11.A0B
            int r1 = r0.getAlpha()
            X.2wg r6 = new X.2wg
            r6.<init>(r11, r1, r2)
            r0 = 300(0x12c, double:1.48E-321)
            r6.setDuration(r0)
            X.0un r2 = r11.A0A
            r1 = 0
            r2.A01 = r1
            r2.clearAnimation()
            X.0un r0 = r11.A0A
            r0.startAnimation(r6)
            r11.A06 = r6
            goto L_0x00d1
        L_0x0152:
            int r1 = r11.A01
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20871Ed.A03(float):void");
    }

    private void A04(float f) {
        float f2 = this.A0E;
        float f3 = (float) this.A0L;
        if (f - f2 > f3 && !this.A0P) {
            this.A0F = f2 + f3;
            this.A0P = true;
            this.A0B.setAlpha(76);
        }
    }

    private void A06(boolean z, boolean z2) {
        if (this.A0D != z) {
            this.A02 = z2;
            A01();
            this.A0D = z;
            if (z) {
                int i = this.A00;
                Animation.AnimationListener animationListener = this.A0N;
                this.A04 = i;
                this.A0S.reset();
                this.A0S.setDuration(200);
                this.A0S.setInterpolator(this.A0U);
                if (animationListener != null) {
                    this.A0A.A01 = animationListener;
                }
                this.A0A.clearAnimation();
                this.A0A.startAnimation(this.A0S);
                return;
            }
            A0B(this.A0N);
        }
    }

    public void A08() {
        this.A0A.clearAnimation();
        this.A0B.stop();
        this.A0A.setVisibility(8);
        this.A0A.getBackground().setAlpha(255);
        this.A0B.setAlpha(255);
        if (0 != 0) {
            setAnimationProgress(0.0f);
        } else {
            A0A(this.A0R - this.A00);
        }
        this.A00 = this.A0A.getTop();
    }

    public void A09(float f) {
        int i = this.A04;
        A0A((i + ((int) (((float) (this.A0R - i)) * f))) - this.A0A.getTop());
    }

    public void A0A(int i) {
        this.A0A.bringToFront();
        C15320v6.offsetTopAndBottom(this.A0A, i);
        this.A00 = this.A0A.getTop();
    }

    public void A0B(Animation.AnimationListener animationListener) {
        C60162wh r2 = new C60162wh(this);
        this.A0O = r2;
        r2.setDuration(150);
        C15140un r0 = this.A0A;
        r0.A01 = animationListener;
        r0.clearAnimation();
        this.A0A.startAnimation(this.A0O);
    }

    public void BgJ(View view, int i, int i2, int[] iArr, int i3) {
        if (i3 == 0) {
            onNestedPreScroll(view, i, i2, iArr);
        }
    }

    public void BgK(View view, int i, int i2, int i3, int i4, int i5) {
        View view2 = view;
        int i6 = i4;
        BgL(view2, i, i2, i3, i6, i5, this.A0X);
    }

    public void BgL(View view, int i, int i2, int i3, int i4, int i5, int[] iArr) {
        int i6;
        if (i5 == 0) {
            int i7 = iArr[1];
            int[] iArr2 = this.A0Y;
            if (0 == 0) {
                int i8 = i;
                C15270v0.A01(this.A0V, i8, i2, i3, i4, iArr2, 0, iArr);
            }
            int i9 = i4 - (iArr[1] - i7);
            if (i9 == 0) {
                i6 = i4 + this.A0Y[1];
            } else {
                i6 = i9;
            }
            if (i6 < 0 && !A07()) {
                float abs = this.A0H + ((float) Math.abs(i6));
                this.A0H = abs;
                A03(abs);
                iArr[1] = iArr[1] + i9;
            }
        }
    }

    public void BgM(View view, View view2, int i, int i2) {
        if (i2 == 0) {
            onNestedScrollAccepted(view, view2, i);
        }
    }

    public boolean Bph(View view, View view2, int i, int i2) {
        if (i2 == 0) {
            return onStartNestedScroll(view, view2, i);
        }
        return false;
    }

    public void Bq8(View view, int i) {
        if (i == 0) {
            onStopNestedScroll(view);
        }
    }

    public void CHt(int i) {
        if (i == 0) {
            stopNestedScroll();
        }
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.A0V.A04(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.A0V.A03(f, f2);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.A0V.A06(i, i2, iArr, iArr2, 0);
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return C15270v0.A01(this.A0V, i, i2, i3, i4, iArr, 0, null);
    }

    public int getChildDrawingOrder(int i, int i2) {
        int i3 = this.A0K;
        if (i3 < 0) {
            return i2;
        }
        if (i2 == i - 1) {
            return i3;
        }
        if (i2 >= i3) {
            return i2 + 1;
        }
        return i2;
    }

    public int getNestedScrollAxes() {
        C32531lu r0 = this.A0W;
        return r0.A01 | r0.A00;
    }

    public boolean hasNestedScrollingParent() {
        if (C15270v0.A00(this.A0V, 0) != null) {
            return true;
        }
        return false;
    }

    public boolean isNestedScrollingEnabled() {
        return this.A0V.A02;
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        View view2 = view;
        int i5 = i3;
        BgL(view2, i, i2, i5, i4, 0, this.A0X);
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.A0W.A01(i);
        startNestedScroll(i & 2);
        this.A0H = 0.0f;
        this.A0Q = true;
    }

    public void onStopNestedScroll(View view) {
        this.A0W.A00();
        this.A0Q = false;
        float f = this.A0H;
        if (f > 0.0f) {
            A02(f);
            this.A0H = 0.0f;
        }
        stopNestedScroll();
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (Build.VERSION.SDK_INT >= 21 || !(this.A0M instanceof AbsListView)) {
            View view = this.A0M;
            if (view == null || C15320v6.isNestedScrollingEnabled(view)) {
                super.requestDisallowInterceptTouchEvent(z);
            }
        }
    }

    public void setAnimationProgress(float f) {
        this.A0A.setScaleX(f);
        this.A0A.setScaleY(f);
    }

    public void setNestedScrollingEnabled(boolean z) {
        C15270v0 r1 = this.A0V;
        if (r1.A02) {
            C15320v6.stopNestedScroll(r1.A04);
        }
        r1.A02 = z;
    }

    public void setSize(int i) {
        if (i == 0 || i == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            float f = 40.0f;
            if (i == 0) {
                f = 56.0f;
            }
            this.A0J = (int) (displayMetrics.density * f);
            this.A0A.setImageDrawable(null);
            this.A0B.A03(i);
            this.A0A.setImageDrawable(this.A0B);
        }
    }

    public boolean startNestedScroll(int i) {
        return this.A0V.A05(i, 0);
    }

    public void stopNestedScroll() {
        this.A0V.A02(0);
    }

    public C20871Ed(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A0L = ViewConfiguration.get(context).getScaledTouchSlop();
        this.A05 = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.A0U = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.A0J = (int) (displayMetrics.density * 40.0f);
        this.A0A = new C15140un(getContext(), -328966);
        C15160up r1 = new C15160up(getContext());
        this.A0B = r1;
        r1.A03(1);
        this.A0A.setImageDrawable(this.A0B);
        this.A0A.setVisibility(8);
        addView(this.A0A);
        setChildrenDrawingOrderEnabled(true);
        int i = (int) (displayMetrics.density * 64.0f);
        this.A01 = i;
        this.A0G = (float) i;
        this.A0W = new C32531lu();
        this.A0V = new C15270v0(this);
        setNestedScrollingEnabled(true);
        int i2 = -this.A0J;
        this.A00 = i2;
        this.A0R = i2;
        A09(1.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, A0a);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    private void A05(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.A0I) {
            int i = 0;
            if (actionIndex == 0) {
                i = 1;
            }
            this.A0I = motionEvent.getPointerId(i);
        }
    }

    public void A0D(int... iArr) {
        A01();
        C15160up r3 = this.A0B;
        C15630vb r2 = r3.A05;
        r2.A0G = iArr;
        r2.A0C = 0;
        int i = iArr[0];
        r2.A0D = i;
        r2.A0C = 0;
        r2.A0D = i;
        r3.invalidateSelf();
    }

    public void onDetachedFromWindow() {
        int A062 = C000700l.A06(1403626149);
        super.onDetachedFromWindow();
        A08();
        C000700l.A0C(-928074462, A062);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        A01();
        int actionMasked = motionEvent.getActionMasked();
        if (isEnabled() && 0 == 0 && !A07() && !this.A0D && !this.A0Q) {
            if (actionMasked != 0) {
                if (actionMasked != 1) {
                    if (actionMasked == 2) {
                        int i = this.A0I;
                        if (i == -1) {
                            Log.e("SwipeRefreshLayout", "Got ACTION_MOVE event but don't have an active pointer id.");
                            return false;
                        }
                        int findPointerIndex = motionEvent.findPointerIndex(i);
                        if (findPointerIndex >= 0) {
                            A04(motionEvent.getY(findPointerIndex));
                        }
                    } else if (actionMasked != 3) {
                        if (actionMasked == 6) {
                            A05(motionEvent);
                        }
                    }
                }
                this.A0P = false;
                this.A0I = -1;
            } else {
                A0A(this.A0R - this.A0A.getTop());
                int pointerId = motionEvent.getPointerId(0);
                this.A0I = pointerId;
                this.A0P = false;
                int findPointerIndex2 = motionEvent.findPointerIndex(pointerId);
                if (findPointerIndex2 >= 0) {
                    this.A0E = motionEvent.getY(findPointerIndex2);
                }
            }
            return this.A0P;
        }
        return false;
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.A0M == null) {
                A01();
            }
            View view = this.A0M;
            if (view != null) {
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                int measuredWidth2 = this.A0A.getMeasuredWidth();
                int measuredHeight2 = this.A0A.getMeasuredHeight();
                int i5 = measuredWidth >> 1;
                int i6 = measuredWidth2 >> 1;
                int i7 = this.A00;
                this.A0A.layout(i5 - i6, i7, i5 + i6, measuredHeight2 + i7);
            }
        }
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A0M == null) {
            A01();
        }
        View view = this.A0M;
        if (view != null) {
            view.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.A0A.measure(View.MeasureSpec.makeMeasureSpec(this.A0J, 1073741824), View.MeasureSpec.makeMeasureSpec(this.A0J, 1073741824));
            this.A0K = -1;
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                if (getChildAt(i3) == this.A0A) {
                    this.A0K = i3;
                    return;
                }
            }
        }
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        return dispatchNestedFling(f, f2, z);
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return dispatchNestedPreFling(f, f2);
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        if (!isEnabled() || 0 != 0 || this.A0D || (i & 2) == 0) {
            return false;
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int A052 = C000700l.A05(-1478828141);
        int actionMasked = motionEvent.getActionMasked();
        if (!isEnabled() || 0 != 0 || A07() || this.A0D || this.A0Q) {
            C000700l.A0B(-1211853393, A052);
            return false;
        }
        if (actionMasked == 0) {
            this.A0I = motionEvent.getPointerId(0);
            this.A0P = false;
        } else if (actionMasked == 1) {
            int findPointerIndex = motionEvent.findPointerIndex(this.A0I);
            if (findPointerIndex < 0) {
                Log.e("SwipeRefreshLayout", "Got ACTION_UP event but don't have an active pointer id.");
                C000700l.A0B(1834935125, A052);
                return false;
            }
            if (this.A0P) {
                this.A0P = false;
                A02((motionEvent.getY(findPointerIndex) - this.A0F) * 0.5f);
            }
            this.A0I = -1;
            C000700l.A0B(586354475, A052);
            return false;
        } else if (actionMasked == 2) {
            int findPointerIndex2 = motionEvent.findPointerIndex(this.A0I);
            if (findPointerIndex2 < 0) {
                Log.e("SwipeRefreshLayout", "Got ACTION_MOVE event but have an invalid active pointer id.");
                C000700l.A0B(461041156, A052);
                return false;
            }
            float y = motionEvent.getY(findPointerIndex2);
            A04(y);
            if (this.A0P) {
                float f = (y - this.A0F) * 0.5f;
                if (f > 0.0f) {
                    A03(f);
                } else {
                    C000700l.A0B(-1901394204, A052);
                    return false;
                }
            }
        } else if (actionMasked == 3) {
            C000700l.A0B(1282040054, A052);
            return false;
        } else if (actionMasked == 5) {
            int actionIndex = motionEvent.getActionIndex();
            if (actionIndex < 0) {
                Log.e("SwipeRefreshLayout", "Got ACTION_POINTER_DOWN event but have an invalid action index.");
                C000700l.A0B(-1429100633, A052);
                return false;
            }
            this.A0I = motionEvent.getPointerId(actionIndex);
        } else if (actionMasked == 6) {
            A05(motionEvent);
        }
        C000700l.A0B(1011597257, A052);
        return true;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (!z) {
            A08();
        }
    }
}
