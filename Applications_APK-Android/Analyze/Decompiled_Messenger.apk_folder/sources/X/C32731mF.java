package X;

import com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000;
import com.facebook.graphql.query.GQSQStringShape0S0000000_I0;
import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1mF  reason: invalid class name and case insensitive filesystem */
public final class C32731mF {
    public final C11170mD A00;

    public static final C32731mF A00(AnonymousClass1XY r1) {
        return new C32731mF(r1);
    }

    public ListenableFuture A01(String str, String str2, String str3) {
        C09290gx r5 = C09290gx.NETWORK_ONLY;
        GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = new GQLCallInputCInputShape0S0000000(AnonymousClass1Y3.A0m);
        gQLCallInputCInputShape0S0000000.A09(AnonymousClass80H.$const$string(576), str);
        gQLCallInputCInputShape0S0000000.A09("origin", str2);
        if (str3 != null) {
            gQLCallInputCInputShape0S0000000.A09("thread_id", str3);
        }
        GQSQStringShape0S0000000_I0 gQSQStringShape0S0000000_I0 = new GQSQStringShape0S0000000_I0(5);
        gQSQStringShape0S0000000_I0.A04("params", gQLCallInputCInputShape0S0000000);
        C26931cN A002 = C26931cN.A00(gQSQStringShape0S0000000_I0);
        A002.A0B(r5);
        A002.A09(0);
        return AnonymousClass169.A01(this.A00.A04(A002), new AnonymousClass4AH(), C25141Ym.INSTANCE);
    }

    public C32731mF(AnonymousClass1XY r2) {
        this.A00 = C11170mD.A00(r2);
    }
}
