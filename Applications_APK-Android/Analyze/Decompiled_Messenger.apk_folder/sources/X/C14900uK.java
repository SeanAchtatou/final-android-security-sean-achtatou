package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.user.model.UserKey;
import java.util.ArrayList;
import java.util.List;

@UserScoped
/* renamed from: X.0uK  reason: invalid class name and case insensitive filesystem */
public final class C14900uK {
    private static C05540Zi A01;
    public AnonymousClass0UN A00;

    public static final C14900uK A00(AnonymousClass1XY r4) {
        C14900uK r0;
        synchronized (C14900uK.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C14900uK((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C14900uK) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public static boolean A01(C14900uK r5, BasicMontageThreadInfo basicMontageThreadInfo) {
        if (!(basicMontageThreadInfo == null || basicMontageThreadInfo.A01 == null || basicMontageThreadInfo.A02 == null || !((AnonymousClass1GE) AnonymousClass1XX.A02(11, AnonymousClass1Y3.BHw, r5.A00)).A02())) {
            if (((AnonymousClass113) AnonymousClass1XX.A02(12, AnonymousClass1Y3.AD9, r5.A00)).A03(Long.parseLong(basicMontageThreadInfo.A03.id))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public List A02(ThreadSummary threadSummary) {
        C17610zB r4 = new C17610zB(threadSummary.A0m.size());
        C24971Xv it = threadSummary.A0m.iterator();
        while (it.hasNext()) {
            ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
            if (threadParticipant.A03 >= threadSummary.A0B && !threadParticipant.A00().equals((UserKey) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B7T, this.A00))) {
                r4.A0D(threadParticipant.A02, threadParticipant.A00());
            }
        }
        int A012 = r4.A01();
        ArrayList arrayList = new ArrayList(A012);
        for (int i = 0; i < A012; i++) {
            arrayList.add(r4.A06(i));
        }
        return arrayList;
    }

    private C14900uK(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(13, r3);
    }
}
