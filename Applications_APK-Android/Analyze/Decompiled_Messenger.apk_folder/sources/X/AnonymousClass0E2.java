package X;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0E2  reason: invalid class name */
public final class AnonymousClass0E2 extends AnonymousClass0E3 {
    private final JobInfo A00;
    private final JobScheduler A01;

    public void A04(Intent intent) {
        this.A01.enqueue(this.A00, new JobWorkItem(intent));
    }

    public AnonymousClass0E2(Context context, ComponentName componentName, int i) {
        super(componentName);
        A03(i);
        this.A00 = new JobInfo.Builder(i, this.A02).setOverrideDeadline(0).build();
        this.A01 = (JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
    }
}
