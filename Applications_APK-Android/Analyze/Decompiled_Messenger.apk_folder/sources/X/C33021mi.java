package X;

import java.util.Queue;

/* renamed from: X.1mi  reason: invalid class name and case insensitive filesystem */
public abstract class C33021mi<E> extends C21221Fs<E> implements Queue<E> {
    public abstract Queue A06();

    public Object element() {
        return A06().element();
    }

    public boolean offer(Object obj) {
        return A06().offer(obj);
    }

    public Object peek() {
        return A06().peek();
    }

    public Object poll() {
        return A06().poll();
    }

    public Object remove() {
        return A06().remove();
    }
}
