package X;

import com.facebook.flexiblesampling.SamplingResult;
import com.facebook.xanalytics.XAnalyticsAdapter;
import io.card.payment.BuildConfig;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0gz  reason: invalid class name and case insensitive filesystem */
public final class C09310gz implements XAnalyticsAdapter {
    public final /* synthetic */ C12230or A00;

    public void cleanup() {
    }

    public void flush() {
    }

    public C09310gz(C12230or r1) {
        this.A00 = r1;
    }

    public String getStructureSamplingConfig(String str) {
        int length;
        Map A08 = this.A00.A01.A01.A00.A08();
        try {
            JSONObject jSONObject = new JSONObject();
            for (Map.Entry entry : A08.entrySet()) {
                if (((String) entry.getKey()).equals(str)) {
                    jSONObject.put("*", ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).startsWith(AnonymousClass08S.A0J(str, ":"))) {
                    int intValue = ((Integer) entry.getValue()).intValue();
                    String substring = ((String) entry.getKey()).substring(str.length() + 1);
                    if (!substring.contains(":")) {
                        if (jSONObject.optJSONObject(substring) == null) {
                            jSONObject.put(substring, new JSONObject());
                        }
                        jSONObject.optJSONObject(substring).put("*", intValue);
                    } else {
                        String[] split = substring.split(":");
                        int i = 0;
                        JSONObject jSONObject2 = jSONObject;
                        while (true) {
                            length = split.length - 1;
                            if (i >= length) {
                                break;
                            }
                            if (jSONObject2.optJSONObject(split[i]) == null) {
                                jSONObject2 = jSONObject2.put(split[i], new JSONObject());
                            }
                            jSONObject2 = jSONObject2.optJSONObject(split[i]);
                            i++;
                        }
                        jSONObject2.put(split[length], intValue);
                    }
                }
            }
            if (jSONObject.length() != 0) {
                return jSONObject.toString();
            }
            return BuildConfig.FLAVOR;
        } catch (JSONException unused) {
            return BuildConfig.FLAVOR;
        }
    }

    public void logCounter(String str, double d) {
        this.A00.A00.A04(str, (long) d, "core_counters");
    }

    public void logEvent(String str, String str2, String str3, boolean z, double d) {
        AnonymousClass0ZF A05 = this.A00.A02.A05(str, false, AnonymousClass07B.A00, z);
        if (A05.A0G()) {
            A05.A06 = AnonymousClass16C.A00(AnonymousClass07B.A0Y) | A05.A06;
            if (str2 != null) {
                C12230or.A01(str2, A05);
            }
            A05.A0E();
        }
    }

    public void logEventBypassSampling(String str, String str2) {
        C06230bA r4 = this.A00.A02;
        SamplingResult A02 = ((C04980Xe) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BC6, r4.A0B.A00)).A02(str, false);
        AnonymousClass0ZF A002 = C06230bA.A00(r4, null, str, AnonymousClass07B.A00, false);
        A002.A0G();
        C06230bA.A02(A002, A02);
        A002.A06 = AnonymousClass16C.A00(AnonymousClass07B.A0Y) | A002.A06;
        if (str2 != null) {
            C12230or.A01(str2, A002);
        }
        A002.A0E();
    }

    public boolean shouldLog(String str) {
        Boolean bool = false;
        C06300bH r0 = this.A00.A02.A0B;
        return ((C04980Xe) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BC6, r0.A00)).A02(str, bool.booleanValue()).A01();
    }
}
