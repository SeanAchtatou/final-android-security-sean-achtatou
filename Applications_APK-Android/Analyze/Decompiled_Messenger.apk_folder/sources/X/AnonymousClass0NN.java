package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0NN  reason: invalid class name */
public final class AnonymousClass0NN extends AnonymousClass0QR {
    private static volatile AnonymousClass0NN A02;
    private final AnonymousClass0jI A00 = new AnonymousClass0QT(this);
    private final C09730iO A01;

    public static final AnonymousClass0NN A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0NN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass0NN(AnonymousClass0iN.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public void A01() {
        this.A01.A02(this.A00);
    }

    public void A02() {
        this.A01.A01(this.A00);
    }

    private AnonymousClass0NN(C09730iO r2) {
        this.A01 = r2;
    }
}
