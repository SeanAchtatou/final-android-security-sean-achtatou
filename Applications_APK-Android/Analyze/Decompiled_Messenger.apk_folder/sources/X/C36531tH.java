package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tH  reason: invalid class name and case insensitive filesystem */
public final class C36531tH {
    private static volatile C36531tH A01;
    public final DeprecatedAnalyticsLogger A00;

    public static final C36531tH A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C36531tH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C36531tH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C36531tH(AnonymousClass1XY r2) {
        this.A00 = C06920cI.A00(r2);
    }
}
