package X;

import android.os.SystemClock;
import android.util.Log;
import com.google.common.collect.ImmutableMap;

/* renamed from: X.09L  reason: invalid class name */
public final class AnonymousClass09L implements C04240Tb {
    public static AnonymousClass09L A03;
    private long A00;
    private String A01;
    private volatile long A02;

    public String getName() {
        return "BlackBox";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    private ImmutableMap A00() {
        String str;
        if (0 == 0 || (str = this.A01) == null || SystemClock.uptimeMillis() - this.A00 >= 0) {
            String str2 = null;
            if (AnonymousClass050.A00) {
                Log.w("Profilo/BlackBoxApi", "STOP AND RESTART");
                AnonymousClass09N A002 = AnonymousClass050.A00(30539777, "bugreport");
                if (A002 == null) {
                    AnonymousClass050.A03();
                } else {
                    new Thread(new AnonymousClass09M(A002)).start();
                    str2 = A002.A02;
                }
            }
            this.A00 = SystemClock.uptimeMillis();
            this.A01 = str2;
            ImmutableMap.Builder builder = new ImmutableMap.Builder();
            if (str2 == null) {
                return builder.build();
            }
            builder.put("black_box_trace_id", str2);
            return builder.build();
        }
        ImmutableMap.Builder builder2 = new ImmutableMap.Builder();
        builder2.put("black_box_trace_id", str);
        return builder2.build();
    }

    public static void A01() {
        AnonymousClass09L r0;
        synchronized (AnonymousClass09L.class) {
            if (A03 == null) {
                A03 = new AnonymousClass09L();
            }
            r0 = A03;
        }
        AnonymousClass0Tc.A04 = r0;
    }

    private AnonymousClass09L() {
    }

    public ImmutableMap AmI() {
        return A00();
    }

    public ImmutableMap AmJ() {
        return A00();
    }
}
