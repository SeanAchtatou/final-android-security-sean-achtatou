package X;

/* renamed from: X.0tl  reason: invalid class name and case insensitive filesystem */
public final class C14660tl implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.init.impl.FbAppInitializerInternal$5";
    public final /* synthetic */ AnonymousClass0Y7 A00;

    public C14660tl(AnonymousClass0Y7 r1) {
        this.A00 = r1;
    }

    public void run() {
        int i = AnonymousClass1Y3.BEN;
        AnonymousClass0Y7 r2 = this.A00;
        r2.A08(new AnonymousClass0Z2((AnonymousClass0WB) AnonymousClass1XX.A02(14, i, r2.A01), r2.A08), "INeedInit.HighPriorityInitOnBackgroundThread");
    }
}
