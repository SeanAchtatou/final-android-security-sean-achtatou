package X;

import java.nio.ByteBuffer;

/* renamed from: X.0b1  reason: invalid class name and case insensitive filesystem */
public final class C06180b1 extends C06190b2 {
    public final String A00;

    public ByteBuffer getJavaByteBuffer() {
        return C06190b2.A00(this.A00);
    }

    public C06180b1(String str) {
        this.A00 = str;
    }
}
