package X;

import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0UF  reason: invalid class name */
public final class AnonymousClass0UF extends AnonymousClass0UB<E> {
    public final /* synthetic */ Set A00;
    public final /* synthetic */ Set A01;
    public final /* synthetic */ Set A02;

    public AnonymousClass0UF(Set set, Set set2, Set set3) {
        this.A01 = set;
        this.A02 = set2;
        this.A00 = set3;
    }

    public boolean contains(Object obj) {
        if (this.A01.contains(obj) || this.A00.contains(obj)) {
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        if (!this.A01.isEmpty() || !this.A00.isEmpty()) {
            return false;
        }
        return true;
    }

    public int size() {
        return C06950cM.A01(((long) this.A01.size()) + ((long) this.A02.size()));
    }

    public /* bridge */ /* synthetic */ Iterator iterator() {
        return iterator();
    }
}
