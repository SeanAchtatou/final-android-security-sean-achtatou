package X;

import com.facebook.messaging.notify.UriNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0s5  reason: invalid class name and case insensitive filesystem */
public final class C13800s5 extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$17";
    public final /* synthetic */ UriNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C13800s5(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, UriNotification uriNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = uriNotification;
    }
}
