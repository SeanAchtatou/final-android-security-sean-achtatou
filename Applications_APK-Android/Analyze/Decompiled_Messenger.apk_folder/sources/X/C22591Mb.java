package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Mb  reason: invalid class name and case insensitive filesystem */
public final class C22591Mb extends C22601Mc {
    private static volatile C22591Mb A02;
    public final Boolean A00;
    private final C22611Md A01;

    public static C23601Qd A00(C22591Mb r9, AnonymousClass1Q0 r10, Object obj, C23601Qd r12, String str) {
        String uri = r9.A02(r10.A02).toString();
        int i = r10.A01;
        AnonymousClass36w r4 = r10.A06;
        if (r9.A00.booleanValue() && i == 0) {
            r4 = null;
        }
        C23591Qc r2 = new C23591Qc(uri, r4, r10.A07, r10.A04, r12, str);
        C22611Md r0 = r9.A01;
        if (r0 != null) {
            r0.CJH(r10, r2);
        }
        return r2;
    }

    public static final C22591Mb A01(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (C22591Mb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = new C22591Mb(AnonymousClass1MY.A0e(applicationInjector), AnonymousClass1MY.A0M(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C22591Mb(Boolean bool, C22611Md r3) {
        this.A00 = Boolean.valueOf(!bool.booleanValue());
        this.A01 = r3;
    }
}
