package X;

import com.facebook.user.model.User;

/* renamed from: X.1HW  reason: invalid class name */
public final class AnonymousClass1HW {
    private AnonymousClass0UN A00;

    public static final AnonymousClass1HW A00(AnonymousClass1XY r1) {
        return new AnonymousClass1HW(r1);
    }

    public boolean A01(User user) {
        String A03;
        C32721mD r2 = (C32721mD) AnonymousClass1XX.A03(AnonymousClass1Y3.A2z, this.A00);
        if (!user.A0H() || (A03 = user.A0Q.A03()) == null || !r2.A08(A03, true)) {
            return false;
        }
        return true;
    }

    private AnonymousClass1HW(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
        if (r5.A0H != com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason.A01) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(com.facebook.user.model.User r4, com.facebook.messaging.model.threads.ThreadSummary r5) {
        /*
            r3 = this;
            java.lang.Integer r1 = r4.A06()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x001f
            boolean r0 = r3.A01(r4)
            if (r0 != 0) goto L_0x001f
            if (r5 == 0) goto L_0x001b
            boolean r0 = r5.A0y
            if (r0 != 0) goto L_0x001b
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r2 = r5.A0H
            com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason r0 = com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason.A01
            r1 = 1
            if (r2 == r0) goto L_0x001c
        L_0x001b:
            r1 = 0
        L_0x001c:
            r0 = 0
            if (r1 == 0) goto L_0x0020
        L_0x001f:
            r0 = 1
        L_0x0020:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1HW.A02(com.facebook.user.model.User, com.facebook.messaging.model.threads.ThreadSummary):boolean");
    }
}
