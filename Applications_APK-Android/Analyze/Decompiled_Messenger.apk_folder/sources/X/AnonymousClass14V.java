package X;

import android.app.Activity;
import android.content.ContextWrapper;

/* renamed from: X.14V  reason: invalid class name */
public final class AnonymousClass14V extends AnonymousClass14T {
    public final Activity A00;

    public AnonymousClass14V(Activity activity) {
        super(new ContextWrapper(activity));
        this.A00 = activity;
    }
}
