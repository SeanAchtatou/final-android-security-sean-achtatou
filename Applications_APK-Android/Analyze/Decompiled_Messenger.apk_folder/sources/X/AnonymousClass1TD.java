package X;

import com.facebook.auth.userscope.UserScoped;
import com.google.common.base.Function;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.ExecutorService;

@UserScoped
/* renamed from: X.1TD  reason: invalid class name */
public final class AnonymousClass1TD {
    private static C05540Zi A0A;
    public AnonymousClass1G0 A00 = null;
    public Function A01;
    public Function A02;
    private boolean A03 = false;
    public final C11170mD A04;
    public final C22251Ko A05;
    public final ExecutorService A06;
    private final C04460Ut A07;
    private final C11330mk A08;
    private final AnonymousClass1CU A09;

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0050, code lost:
        return r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet A01() {
        /*
            r8 = this;
            monitor-enter(r8)
            X.1G0 r1 = r8.A00     // Catch:{ all -> 0x0055 }
            r0 = 0
            if (r1 == 0) goto L_0x0007
            r0 = 1
        L_0x0007:
            r7 = 0
            if (r0 == 0) goto L_0x0053
            com.google.common.util.concurrent.ListenableFuture r1 = r1.A01     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            boolean r0 = r1.isDone()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            if (r0 == 0) goto L_0x0053
            java.lang.Object r6 = r1.get()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet r6 = (com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet) r6     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            boolean r0 = r8.A03     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            if (r0 != 0) goto L_0x004f
            X.0mk r5 = r8.A08     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            r4.<init>()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.lang.String r0 = "Badging - MessageRequestsSnippetFetcher - getCachedMRSnnipet: "
            r4.append(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            int r0 = r6.A01     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            int r0 = r6.A02     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            r1 = 1
            int r0 = r6.A00     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.lang.Integer[] r0 = new java.lang.Integer[]{r3, r2, r0}     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            r4.append(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            java.lang.String r0 = r4.toString()     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            r5.BIo(r0)     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
            r8.A03 = r1     // Catch:{ InterruptedException | CancellationException | ExecutionException -> 0x0051 }
        L_0x004f:
            monitor-exit(r8)
            return r6
        L_0x0051:
            monitor-exit(r8)
            return r7
        L_0x0053:
            monitor-exit(r8)
            return r7
        L_0x0055:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TD.A01():com.facebook.messaging.messagerequests.snippet.MessageRequestsSnippet");
    }

    public synchronized ListenableFuture A02() {
        AnonymousClass1G0 r1 = this.A00;
        boolean z = false;
        if (r1 != null) {
            z = true;
        }
        if (z) {
            return C05350Yp.A00(r1.A01);
        }
        ListenableFuture A022 = AnonymousClass169.A02(AnonymousClass0WJ.A00(this.A06).CIF(new C22271Kr()), new C33191nB(this));
        C35941s4 r2 = new C35941s4(this);
        this.A00 = AnonymousClass1G0.A00(A022, r2);
        this.A08.BIo("Badging - MessageRequestsSnippetFetcher - updatingMRSnippetFromServer");
        this.A03 = false;
        C05350Yp.A07(A022, r2);
        return C05350Yp.A00(A022);
    }

    public static final AnonymousClass1TD A00(AnonymousClass1XY r4) {
        AnonymousClass1TD r0;
        synchronized (AnonymousClass1TD.class) {
            C05540Zi A002 = C05540Zi.A00(A0A);
            A0A = A002;
            try {
                if (A002.A03(r4)) {
                    A0A.A00 = new AnonymousClass1TD((AnonymousClass1XY) A0A.A01());
                }
                C05540Zi r1 = A0A;
                r0 = (AnonymousClass1TD) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A0A.A02();
                throw th;
            }
        }
        return r0;
    }

    private AnonymousClass1TD(AnonymousClass1XY r4) {
        this.A06 = AnonymousClass0UX.A0Z(r4);
        this.A07 = C04430Uq.A02(r4);
        this.A04 = C11170mD.A00(r4);
        this.A05 = new C22251Ko(r4);
        this.A09 = AnonymousClass1CU.A00(r4);
        C06600bl BMm = this.A07.BMm();
        BMm.A02(C99084oO.$const$string(438), new C33811oB(this));
        BMm.A00().A00();
        this.A08 = this.A09.A01(AnonymousClass24B.$const$string(27));
    }
}
