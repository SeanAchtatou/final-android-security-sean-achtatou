package X;

import com.facebook.acra.asyncbroadcastreceiver.AsyncBroadcastReceiverObserver;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0KP  reason: invalid class name */
public final class AnonymousClass0KP {
    public final Set A00;

    private void A03(AnonymousClass0KO r5, List list) {
        String str;
        int i = 0;
        while (i < list.size()) {
            String str2 = (String) list.get(i);
            if (str2 == null || str2.length() == 0) {
                str = "Empty component name.";
            } else {
                String str3 = r5.A02;
                if (str3 == null || str3.length() == 0) {
                    str = "Package name is empty.";
                } else {
                    int indexOf = str2.indexOf(46);
                    if (indexOf == 0) {
                        str2 = AnonymousClass08S.A0J(str3, str2);
                    } else if (indexOf < 0) {
                        str2 = AnonymousClass08S.A0P(str3, ".", str2);
                    }
                    list.set(i, str2);
                    i++;
                }
            }
            throw A00(r5, str);
        }
    }

    private static AnonymousClass0KQ A00(AnonymousClass0KO r3, String str) {
        throw new AnonymousClass0KQ("Failed to parse AndroidManifest.xml in " + r3.A00 + ": " + str);
    }

    private static String A01(AnonymousClass0KO r1, int i) {
        String str = (String) r1.A09.get(Integer.valueOf(i));
        if (str != null) {
            return str;
        }
        throw new IllegalStateException(AnonymousClass08S.A09("String not found: ", i));
    }

    private Map A02(AnonymousClass0KM r10, AnonymousClass0KO r11, int i) {
        String A01;
        HashMap hashMap = new HashMap();
        for (int i2 = 0; i2 < i; i2++) {
            int A02 = r10.A02();
            int A022 = r10.A02();
            int A023 = r10.A02();
            int i3 = r10.A00;
            short A03 = r10.A03();
            r10.A01();
            r10.A01();
            int A024 = r10.A02();
            int i4 = A03 - (r10.A00 - i3);
            if (i4 > 0) {
                r10.A04(i4);
            }
            if (A02 >= 0) {
                A01(r11, A02);
            }
            String A012 = A01(r11, A022);
            if (A023 < 0) {
                A01 = null;
            } else {
                A01 = A01(r11, A023);
            }
            if (A01 == null) {
                A01 = Integer.toString(A024);
            }
            if (A01 != null) {
                hashMap.put(A012, A01);
            }
        }
        return hashMap;
    }

    public AnonymousClass0KP() {
        HashSet hashSet = new HashSet();
        hashSet.add("activity");
        hashSet.add("activity-alias");
        hashSet.add(AsyncBroadcastReceiverObserver.RECEIVER);
        hashSet.add("service");
        hashSet.add("provider");
        this.A00 = Collections.unmodifiableSet(hashSet);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.util.jar.JarFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: java.util.jar.JarFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: java.util.jar.JarFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: java.util.jar.JarFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: java.util.jar.JarFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v6, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v10, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v11, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: java.util.jar.JarFile} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x03a4  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x03a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0KN A04(java.io.File r21) {
        /*
            r20 = this;
            X.0KO r7 = new X.0KO
            r6 = 0
            r7.<init>()
            r8 = r21
            r7.A00 = r8
            java.util.jar.JarFile r5 = new java.util.jar.JarFile     // Catch:{ 0KQ -> 0x039a, IOException -> 0x037d, all -> 0x037a }
            r5.<init>(r8)     // Catch:{ 0KQ -> 0x039a, IOException -> 0x037d, all -> 0x037a }
            java.lang.String r0 = "AndroidManifest.xml"
            java.util.zip.ZipEntry r0 = r5.getEntry(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x035b
            java.io.InputStream r6 = r5.getInputStream(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r15 = r20
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.<init>(r6)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0KM r14 = new X.0KM     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r14.<init>(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r14.A01 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 0
            r14.A00 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0MV r0 = new X.0MV     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.<init>()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            short r3 = r14.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            short r2 = r14.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r14.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.A02 = r3     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.A01 = r2     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.A00 = r1     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 3
            if (r3 != r0) goto L_0x0354
            int r0 = r1 + -8
            r14.A01 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 0
            r14.A00 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r2 + -8
            if (r0 <= 0) goto L_0x0056
            r14.A04(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0056:
            X.0MV r19 = new X.0MV     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r19.<init>()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0KM r13 = new X.0KM     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.<init>(r14)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0060:
            int r2 = r14.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r14.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 0
            if (r2 >= r1) goto L_0x0068
            r0 = 1
        L_0x0068:
            if (r0 == 0) goto L_0x0322
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r13.A01 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 0
            r13.A00 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            short r2 = r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            short r3 = r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = r19
            r0.A02 = r2     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.A01 = r3     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.A00 = r1     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r1 + -8
            r13.A01 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 0
            r13.A00 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r3 = 1
            if (r2 == r3) goto L_0x01b3
            r0 = 258(0x102, float:3.62E-43)
            if (r2 == r0) goto L_0x00bd
            r0 = 259(0x103, float:3.63E-43)
            if (r2 != r0) goto L_0x02b9
            int r0 = r7.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r0 - r3
            r7.A01 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r0 + 8
            r0 = r19
            short r0 = r0.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r0 - r1
            if (r0 <= 0) goto L_0x00b0
            r13.A04(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x00b0:
            int r1 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r2 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r1 >= 0) goto L_0x029a
            r1 = 0
            goto L_0x029e
        L_0x00bd:
            r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r0 + 8
            r0 = r19
            short r0 = r0.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r0 - r1
            if (r0 <= 0) goto L_0x00d1
            r13.A04(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x00d1:
            int r11 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r4 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            short r1 = r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            short r9 = r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r0 - r11
            int r1 = r1 - r0
            if (r1 <= 0) goto L_0x00f7
            r13.A04(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x00f7:
            java.lang.String r4 = A01(r7, r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = "manifest"
            boolean r0 = r0.equals(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0133
            int r0 = r7.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 != 0) goto L_0x0133
            r7.A0B = r3     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.Map r1 = r15.A02(r13, r7, r9)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = "package"
            java.lang.Object r0 = r1.get(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r7.A02 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x02e3
            java.lang.String r0 = "versionName"
            java.lang.Object r0 = r1.get(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r7.A04 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x02dc
            java.lang.String r0 = "versionCode"
            java.lang.Object r0 = r1.get(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r7.A03 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 != 0) goto L_0x0145
            goto L_0x02d5
        L_0x0133:
            java.lang.String r0 = "application"
            boolean r0 = r0.equals(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0154
            boolean r0 = r7.A0B     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0154
            int r0 = r7.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 != r3) goto L_0x0154
            r7.A0A = r3     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0145:
            int r1 = r13.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r1 - r0
            r13.A04(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r7.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r0 + r3
            r7.A01 = r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x02b9
        L_0x0154:
            java.util.Set r0 = r15.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            boolean r0 = r0.contains(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0145
            boolean r0 = r7.A0A     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0145
            int r1 = r7.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 2
            if (r1 != r0) goto L_0x0145
            java.util.Map r1 = r15.A02(r13, r7, r9)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = "name"
            java.lang.Object r1 = r1.get(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r1 == 0) goto L_0x02ea
            java.lang.String r0 = "activity"
            boolean r0 = r0.equals(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 != 0) goto L_0x01ad
            java.lang.String r0 = "activity-alias"
            boolean r0 = r0.equals(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 != 0) goto L_0x01ad
            java.lang.String r0 = "receiver"
            boolean r0 = r0.equals(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0191
            java.util.List r0 = r7.A07     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.add(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x0145
        L_0x0191:
            java.lang.String r0 = "service"
            boolean r0 = r0.equals(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x019f
            java.util.List r0 = r7.A08     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.add(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x0145
        L_0x019f:
            java.lang.String r0 = "provider"
            boolean r0 = r0.equals(r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0145
            java.util.List r0 = r7.A06     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.add(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x0145
        L_0x01ad:
            java.util.List r0 = r7.A05     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0.add(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x0145
        L_0x01b3:
            int r12 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r4 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r2 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r18 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r3 = r0 + 8
            r0 = r19
            short r1 = r0.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r1 - r3
            if (r1 < 0) goto L_0x0317
            if (r1 <= 0) goto L_0x01d6
            r13.A04(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x01d6:
            java.util.HashMap r11 = new java.util.HashMap     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r11.<init>()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r3 = 0
        L_0x01dc:
            if (r3 >= r12) goto L_0x01f0
            int r0 = r13.A02()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r11.put(r1, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r3 = r3 + 1
            goto L_0x01dc
        L_0x01f0:
            int r0 = r4 << 2
            r13.A04(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r18 + -8
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r1 - r0
            if (r1 < 0) goto L_0x030c
            if (r1 <= 0) goto L_0x0201
            r13.A04(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0201:
            r0 = r2 & 256(0x100, float:3.59E-43)
            r17 = 0
            if (r0 == 0) goto L_0x0209
            r17 = 1
        L_0x0209:
            if (r17 == 0) goto L_0x020c
            goto L_0x020f
        L_0x020c:
            java.lang.String r0 = "UTF-16LE"
            goto L_0x0211
        L_0x020f:
            java.lang.String r0 = "UTF-8"
        L_0x0211:
            java.nio.charset.Charset r10 = java.nio.charset.Charset.forName(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r9 = 0
        L_0x0216:
            if (r9 >= r12) goto L_0x0291
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r0 + 8
            int r1 = r1 - r18
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            boolean r0 = r11.containsKey(r2)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x0301
            java.lang.Object r0 = r11.get(r2)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r16 = r0.intValue()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r4 = "each string is expected to end with \\0 terminator."
            r3 = 2
            if (r17 == 0) goto L_0x024f
            r2 = 0
            r0 = 0
        L_0x0239:
            if (r2 >= r3) goto L_0x0275
            byte r0 = r13.A01()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x024c
            byte r1 = r13.A01()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = r0 & 127(0x7f, float:1.78E-43)
            int r0 = r0 << 8
            r0 = r0 | r1
        L_0x024c:
            int r2 = r2 + 1
            goto L_0x0239
        L_0x024f:
            short r0 = r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r1 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0262
            short r1 = r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = r0 & 32767(0x7fff, float:4.5916E-41)
            int r0 = r0 << 16
            r0 = r0 | r1
        L_0x0262:
            int r0 = r0 << 1
            byte[] r0 = new byte[r0]     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A05(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            short r1 = r13.A03()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r1 != 0) goto L_0x02fc
            java.lang.String r2 = new java.lang.String     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r2.<init>(r0, r10)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x0285
        L_0x0275:
            byte[] r0 = new byte[r0]     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r13.A05(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            byte r1 = r13.A01()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r1 != 0) goto L_0x02f7
            java.lang.String r2 = new java.lang.String     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r2.<init>(r0, r10)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0285:
            java.util.Map r1 = r7.A09     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r1.put(r0, r2)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r9 = r9 + 1
            goto L_0x0216
        L_0x0291:
            int r1 = r13.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r1 - r0
            r13.A04(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x02b9
        L_0x029a:
            java.lang.String r1 = A01(r7, r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x029e:
            java.lang.String r3 = A01(r7, r2)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = "http://schemas.android.com/apk/res/android"
            boolean r1 = r0.equals(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r2 = 0
            if (r1 == 0) goto L_0x02c3
            java.lang.String r0 = "manifest"
            boolean r0 = r0.equals(r3)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x02c3
            int r0 = r7.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 != 0) goto L_0x02c3
            r7.A0B = r2     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x02b9:
            int r1 = r13.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r0 = r13.A00     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            int r1 = r1 - r0
            r13.A04(r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x0060
        L_0x02c3:
            if (r1 == 0) goto L_0x02b9
            java.lang.String r0 = "application"
            boolean r0 = r0.equals(r3)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r0 == 0) goto L_0x02b9
            int r1 = r7.A01     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r0 = 1
            if (r1 != r0) goto L_0x02b9
            r7.A0A = r2     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            goto L_0x02b9
        L_0x02d5:
            java.lang.String r0 = "manifest does not have version code specified."
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x02dc:
            java.lang.String r0 = "manifest does not have version name specified."
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x02e3:
            java.lang.String r0 = "manifest does not have package name specified."
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x02ea:
            java.lang.String r1 = "component tag "
            java.lang.String r0 = " did not include name attribute."
            java.lang.String r0 = X.AnonymousClass08S.A0P(r1, r4, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x02f7:
            X.0KQ r0 = A00(r7, r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x02fc:
            X.0KQ r0 = A00(r7, r4)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0301:
            java.lang.String r0 = "offset doesn't match string index: offset="
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x030c:
            java.lang.String r0 = "consumed too much data from string index table: "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0317:
            java.lang.String r0 = "consumed too much data from string header: "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r1)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0322:
            java.util.List r0 = r7.A05     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r15.A03(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.List r0 = r7.A07     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r15.A03(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.List r0 = r7.A06     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r15.A03(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.List r0 = r7.A08     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r15.A03(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            X.0KN r9 = new X.0KN     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r10 = r7.A02     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r11 = r7.A03     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r12 = r7.A04     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.List r13 = r7.A05     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.List r14 = r7.A07     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.List r15 = r7.A06     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.util.List r0 = r7.A08     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r16 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            if (r6 == 0) goto L_0x0350
            r6.close()
        L_0x0350:
            r5.close()
            return r9
        L_0x0354:
            java.lang.String r0 = "stream is not an xml resource."
            X.0KQ r0 = A00(r7, r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r0     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x035b:
            java.io.FileNotFoundException r2 = new java.io.FileNotFoundException     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r1.<init>()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = "No manifest found in "
            r1.append(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r1.append(r8)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            java.lang.String r0 = r1.toString()     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            r2.<init>(r0)     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
            throw r2     // Catch:{ 0KQ -> 0x0376, IOException -> 0x0372, all -> 0x03a1 }
        L_0x0372:
            r3 = move-exception
            r4 = r6
            r6 = r5
            goto L_0x037f
        L_0x0376:
            r0 = move-exception
            r1 = r6
            r6 = r5
            goto L_0x039c
        L_0x037a:
            r0 = move-exception
            r5 = r6
            goto L_0x03a2
        L_0x037d:
            r3 = move-exception
            r4 = r6
        L_0x037f:
            X.0KQ r2 = new X.0KQ     // Catch:{ all -> 0x0396 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0396 }
            r1.<init>()     // Catch:{ all -> 0x0396 }
            java.lang.String r0 = "Failed to parse manifest from "
            r1.append(r0)     // Catch:{ all -> 0x0396 }
            r1.append(r8)     // Catch:{ all -> 0x0396 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0396 }
            r2.<init>(r0, r3)     // Catch:{ all -> 0x0396 }
            throw r2     // Catch:{ all -> 0x0396 }
        L_0x0396:
            r0 = move-exception
            r5 = r6
            r6 = r4
            goto L_0x03a2
        L_0x039a:
            r0 = move-exception
            r1 = r6
        L_0x039c:
            throw r0     // Catch:{ all -> 0x039d }
        L_0x039d:
            r0 = move-exception
            r5 = r6
            r6 = r1
            goto L_0x03a2
        L_0x03a1:
            r0 = move-exception
        L_0x03a2:
            if (r6 == 0) goto L_0x03a7
            r6.close()
        L_0x03a7:
            if (r5 == 0) goto L_0x03ac
            r5.close()
        L_0x03ac:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0KP.A04(java.io.File):X.0KN");
    }
}
