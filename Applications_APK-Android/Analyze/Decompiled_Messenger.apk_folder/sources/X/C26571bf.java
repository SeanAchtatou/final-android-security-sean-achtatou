package X;

import android.content.Context;
import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1bf  reason: invalid class name and case insensitive filesystem */
public final class C26571bf extends AnonymousClass0f8 {
    private static volatile C26571bf A03;
    private AnonymousClass8Y0 A00 = null;
    private C08120ei A01 = null;
    private final Context A02;

    public String Azg() {
        return "thermal_stats";
    }

    public static final C26571bf A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C26571bf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C26571bf(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        C30404Evf evf = (C30404Evf) obj;
        C30404Evf evf2 = (C30404Evf) obj2;
        if (evf != null && evf2 != null) {
            if (this.A01 == null) {
                this.A01 = new C08120ei();
            }
            performanceLoggingEvent.A0B("cpu_vendor", this.A01.A01);
            performanceLoggingEvent.A0B("cpu_name", this.A01.A00);
            int i = evf.A00;
            if (i != Integer.MIN_VALUE) {
                performanceLoggingEvent.A04("battery_temp", i);
                performanceLoggingEvent.A04("battery_temp_inc", evf2.A00 - evf.A00);
            }
            int i2 = evf.A01;
            if (i2 != Integer.MIN_VALUE) {
                performanceLoggingEvent.A04("cpu_temp", i2);
                performanceLoggingEvent.A04("cpu_temp_inc", evf2.A01 - evf.A01);
            }
            int i3 = evf.A02;
            if (i3 != Integer.MIN_VALUE) {
                performanceLoggingEvent.A04("gpu_temp", i3);
                performanceLoggingEvent.A04("gpu_temp_inc", evf2.A02 - evf.A02);
            }
        }
    }

    public long Azh() {
        return C08350fD.A0G;
    }

    public Class B3O() {
        return C30404Evf.class;
    }

    public Object CGO() {
        C30404Evf evf = new C30404Evf();
        if (this.A00 == null) {
            this.A00 = new AnonymousClass8Y0();
        }
        evf.A00 = this.A00.A07(this.A02);
        evf.A01 = this.A00.A05();
        evf.A02 = this.A00.A06();
        return evf;
    }

    private C26571bf(AnonymousClass1XY r2) {
        this.A02 = AnonymousClass1YA.A02(r2);
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A0C;
    }
}
