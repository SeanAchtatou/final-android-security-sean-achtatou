package X;

import java.util.concurrent.ConcurrentLinkedQueue;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0so  reason: invalid class name and case insensitive filesystem */
public final class C14220so implements C08210er {
    private static volatile C14220so A04;
    public final AnonymousClass06B A00;
    public final ConcurrentLinkedQueue A01 = new ConcurrentLinkedQueue();
    private final AnonymousClass09P A02;
    private final C25051Yd A03;

    public String getName() {
        return "BugReportOperationLogger";
    }

    public boolean isMemoryIntensive() {
        return false;
    }

    public void prepareDataForWriting() {
    }

    public static final C14220so A00(AnonymousClass1XY r6) {
        if (A04 == null) {
            synchronized (C14220so.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A04 = new C14220so(AnonymousClass067.A02(), C04750Wa.A01(applicationInjector), AnonymousClass0WT.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void A01(String str, Integer num) {
        this.A01.add(new C30436EwM(this.A00.now(), str, num));
        if (this.A01.size() > 50) {
            this.A01.remove();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:50:0x00db */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map getExtraFileFromWorkerThread(java.io.File r18) {
        /*
            r17 = this;
            java.lang.String r3 = "BugReportOperationLogger"
            r4 = r17
            java.io.File r7 = new java.io.File     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            java.lang.String r0 = "bugreport_operation_json.txt"
            r1 = r18
            r7.<init>(r1, r0)     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            java.io.PrintWriter r5 = new java.io.PrintWriter     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            r5.<init>(r7)     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ all -> 0x00d5 }
            r8.<init>()     // Catch:{ all -> 0x00d5 }
            java.util.concurrent.ConcurrentLinkedQueue r0 = r4.A01     // Catch:{ all -> 0x00d5 }
            java.util.Iterator r16 = r0.iterator()     // Catch:{ all -> 0x00d5 }
            r15 = 1
        L_0x001e:
            boolean r0 = r16.hasNext()     // Catch:{ all -> 0x00d5 }
            if (r0 == 0) goto L_0x00b8
            java.lang.Object r10 = r16.next()     // Catch:{ all -> 0x00d5 }
            X.EwM r10 = (X.C30436EwM) r10     // Catch:{ all -> 0x00d5 }
            X.06B r0 = r4.A00     // Catch:{ all -> 0x00d5 }
            long r13 = r0.now()     // Catch:{ all -> 0x00d5 }
            long r0 = r10.A00     // Catch:{ all -> 0x00d5 }
            long r13 = r13 - r0
            r11 = 900000(0xdbba0, double:4.44659E-318)
            int r1 = (r13 > r11 ? 1 : (r13 == r11 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x003c
            r0 = 1
        L_0x003c:
            if (r0 != 0) goto L_0x001e
            java.lang.Integer r0 = r10.A01     // Catch:{ all -> 0x00d5 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x00d5 }
            switch(r0) {
                case 1: goto L_0x00b1;
                case 2: goto L_0x00ae;
                case 3: goto L_0x00ab;
                case 4: goto L_0x00a8;
                case 5: goto L_0x00a5;
                case 6: goto L_0x00a2;
                case 7: goto L_0x009f;
                case 8: goto L_0x009c;
                case 9: goto L_0x0099;
                case 10: goto L_0x0096;
                case 11: goto L_0x0093;
                case 12: goto L_0x0090;
                case 13: goto L_0x008d;
                case 14: goto L_0x008a;
                case 15: goto L_0x0087;
                case 16: goto L_0x0084;
                case 17: goto L_0x0081;
                case 18: goto L_0x007e;
                case 19: goto L_0x007b;
                case 20: goto L_0x0078;
                case 21: goto L_0x0075;
                case 22: goto L_0x0072;
                case 23: goto L_0x006f;
                case 24: goto L_0x006c;
                default: goto L_0x0047;
            }     // Catch:{ all -> 0x00d5 }
        L_0x0047:
            java.lang.String r9 = "Not Inspected"
        L_0x0049:
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ all -> 0x00d5 }
            r6.<init>()     // Catch:{ all -> 0x00d5 }
            long r1 = r10.A00     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = "recordTime"
            org.json.JSONObject r1 = r6.put(r0, r1)     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = "category"
            org.json.JSONObject r2 = r1.put(r0, r9)     // Catch:{ all -> 0x00d5 }
            java.lang.String r1 = r10.A02     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = "operation"
            org.json.JSONObject r1 = r2.put(r0, r1)     // Catch:{ all -> 0x00d5 }
            java.lang.String r0 = java.lang.String.valueOf(r15)     // Catch:{ all -> 0x00d5 }
            r8.put(r0, r1)     // Catch:{ all -> 0x00d5 }
            goto L_0x00b4
        L_0x006c:
            java.lang.String r9 = "Search"
            goto L_0x0049
        L_0x006f:
            java.lang.String r9 = "Add Contact"
            goto L_0x0049
        L_0x0072:
            java.lang.String r9 = "Mute Action"
            goto L_0x0049
        L_0x0075:
            java.lang.String r9 = "Thread Settings"
            goto L_0x0049
        L_0x0078:
            java.lang.String r9 = "VoIP Call"
            goto L_0x0049
        L_0x007b:
            java.lang.String r9 = "Like Button"
            goto L_0x0049
        L_0x007e:
            java.lang.String r9 = "Platform"
            goto L_0x0049
        L_0x0081:
            java.lang.String r9 = "P2P"
            goto L_0x0049
        L_0x0084:
            java.lang.String r9 = "Voice Clips"
            goto L_0x0049
        L_0x0087:
            java.lang.String r9 = "Lightweight Actions"
            goto L_0x0049
        L_0x008a:
            java.lang.String r9 = "Composer Long Press"
            goto L_0x0049
        L_0x008d:
            java.lang.String r9 = "Stickers"
            goto L_0x0049
        L_0x0090:
            java.lang.String r9 = "Media Picker"
            goto L_0x0049
        L_0x0093:
            java.lang.String r9 = "Media Tray"
            goto L_0x0049
        L_0x0096:
            java.lang.String r9 = "QuickCam"
            goto L_0x0049
        L_0x0099:
            java.lang.String r9 = "Invite Flow"
            goto L_0x0049
        L_0x009c:
            java.lang.String r9 = "Create Group Flow"
            goto L_0x0049
        L_0x009f:
            java.lang.String r9 = "Compose Message Flow"
            goto L_0x0049
        L_0x00a2:
            java.lang.String r9 = "Discover Tab"
            goto L_0x0049
        L_0x00a5:
            java.lang.String r9 = "Settings Tab"
            goto L_0x0049
        L_0x00a8:
            java.lang.String r9 = "People Tab"
            goto L_0x0049
        L_0x00ab:
            java.lang.String r9 = "Groups Tab"
            goto L_0x0049
        L_0x00ae:
            java.lang.String r9 = "Call Tab"
            goto L_0x0049
        L_0x00b1:
            java.lang.String r9 = "Recents Tab"
            goto L_0x0049
        L_0x00b4:
            int r15 = r15 + 1
            goto L_0x001e
        L_0x00b8:
            java.lang.String r0 = r8.toString()     // Catch:{ all -> 0x00d5 }
            r5.write(r0)     // Catch:{ all -> 0x00d5 }
            r5.close()     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            android.net.Uri r0 = android.net.Uri.fromFile(r7)     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            r2.<init>()     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            java.lang.String r1 = "bugreport_operation_json.txt"
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            r2.put(r1, r0)     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
            return r2
        L_0x00d5:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00d7 }
        L_0x00d7:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x00db }
        L_0x00db:
            throw r0     // Catch:{ IOException -> 0x00e4, Exception -> 0x00dc }
        L_0x00dc:
            r1 = move-exception
            X.09P r0 = r4.A02
            r0.softReport(r3, r1)
            r0 = 0
            return r0
        L_0x00e4:
            r1 = move-exception
            X.09P r0 = r4.A02
            r0.softReport(r3, r1)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14220so.getExtraFileFromWorkerThread(java.io.File):java.util.Map");
    }

    public boolean shouldSendAsync() {
        return this.A03.Aeo(2306124660285178128L, false);
    }

    private C14220so(AnonymousClass06B r2, AnonymousClass09P r3, C25051Yd r4) {
        this.A00 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }
}
