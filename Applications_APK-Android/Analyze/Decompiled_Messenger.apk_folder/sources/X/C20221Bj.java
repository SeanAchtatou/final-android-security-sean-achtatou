package X;

import android.animation.AnimatorSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Bj  reason: invalid class name and case insensitive filesystem */
public abstract class C20221Bj {
    public long A00 = 120;
    public long A01 = 250;
    public long A02 = 120;
    public C16090wT A03 = null;
    public ArrayList A04 = new ArrayList();

    public void A07() {
        if (!(this instanceof C15660ve) && !(this instanceof C15670vf)) {
            C32491lq r5 = (C32491lq) this;
            for (int size = r5.A08.size() - 1; size >= 0; size--) {
                C29695EgC egC = (C29695EgC) r5.A08.get(size);
                View view = egC.A04.A0H;
                view.setTranslationY(0.0f);
                view.setTranslationX(0.0f);
                C33781o8 r0 = egC.A04;
                r5.A0L(r0);
                r5.A0B(r0);
                r5.A08.remove(size);
            }
            for (int size2 = r5.A09.size() - 1; size2 >= 0; size2--) {
                C33781o8 r02 = (C33781o8) r5.A09.get(size2);
                r5.A0N(r02);
                r5.A0B(r02);
                r5.A09.remove(size2);
            }
            for (int size3 = r5.A06.size() - 1; size3 >= 0; size3--) {
                C33781o8 r1 = (C33781o8) r5.A06.get(size3);
                r1.A0H.setAlpha(1.0f);
                r5.A0J(r1);
                r5.A0B(r1);
                r5.A06.remove(size3);
            }
            for (int size4 = r5.A07.size() - 1; size4 >= 0; size4--) {
                C29686Eg3 eg3 = (C29686Eg3) r5.A07.get(size4);
                C33781o8 r03 = eg3.A05;
                if (r03 != null) {
                    C32491lq.A04(r5, eg3, r03);
                }
                C33781o8 r04 = eg3.A04;
                if (r04 != null) {
                    C32491lq.A04(r5, eg3, r04);
                }
            }
            r5.A07.clear();
            if (r5.A0C()) {
                for (int size5 = r5.A05.size() - 1; size5 >= 0; size5--) {
                    ArrayList arrayList = (ArrayList) r5.A05.get(size5);
                    for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                        C29695EgC egC2 = (C29695EgC) arrayList.get(size6);
                        View view2 = egC2.A04.A0H;
                        view2.setTranslationY(0.0f);
                        view2.setTranslationX(0.0f);
                        C33781o8 r05 = egC2.A04;
                        r5.A0L(r05);
                        r5.A0B(r05);
                        arrayList.remove(size6);
                        if (arrayList.isEmpty()) {
                            r5.A05.remove(arrayList);
                        }
                    }
                }
                for (int size7 = r5.A01.size() - 1; size7 >= 0; size7--) {
                    ArrayList arrayList2 = (ArrayList) r5.A01.get(size7);
                    for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                        C33781o8 r12 = (C33781o8) arrayList2.get(size8);
                        r12.A0H.setAlpha(1.0f);
                        r5.A0J(r12);
                        r5.A0B(r12);
                        arrayList2.remove(size8);
                        if (arrayList2.isEmpty()) {
                            r5.A01.remove(arrayList2);
                        }
                    }
                }
                for (int size9 = r5.A03.size() - 1; size9 >= 0; size9--) {
                    ArrayList arrayList3 = (ArrayList) r5.A03.get(size9);
                    for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                        C29686Eg3 eg32 = (C29686Eg3) arrayList3.get(size10);
                        C33781o8 r06 = eg32.A05;
                        if (r06 != null) {
                            C32491lq.A04(r5, eg32, r06);
                        }
                        C33781o8 r07 = eg32.A04;
                        if (r07 != null) {
                            C32491lq.A04(r5, eg32, r07);
                        }
                        if (arrayList3.isEmpty()) {
                            r5.A03.remove(arrayList3);
                        }
                    }
                }
                C32491lq.A03(r5.A0A);
                C32491lq.A03(r5.A04);
                C32491lq.A03(r5.A00);
                C32491lq.A03(r5.A02);
                r5.A09();
            }
        }
    }

    public void A08() {
        if (this instanceof C15660ve) {
            C15660ve r3 = (C15660ve) this;
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setInterpolator(new LinearInterpolator());
            animatorSet.setDuration(100L);
            animatorSet.playTogether(r3.A02);
            animatorSet.start();
            r3.A02.clear();
        } else if (!(this instanceof C15670vf)) {
            C32491lq r9 = (C32491lq) this;
            boolean z = !r9.A09.isEmpty();
            boolean z2 = !r9.A08.isEmpty();
            boolean z3 = !r9.A07.isEmpty();
            boolean z4 = !r9.A06.isEmpty();
            if (z || z2 || z4 || z3) {
                Iterator it = r9.A09.iterator();
                while (it.hasNext()) {
                    C33781o8 r4 = (C33781o8) it.next();
                    View view = r4.A0H;
                    ViewPropertyAnimator animate = view.animate();
                    r9.A0A.add(r4);
                    animate.setDuration(r9.A02).alpha(0.0f).setListener(new C80643sq(r9, r4, animate, view)).start();
                }
                r9.A09.clear();
                if (z2) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(r9.A08);
                    r9.A05.add(arrayList);
                    r9.A08.clear();
                    C29688Eg5 eg5 = new C29688Eg5(r9, arrayList);
                    if (z) {
                        C15320v6.postOnAnimationDelayed(((C29695EgC) arrayList.get(0)).A04.A0H, eg5, r9.A02);
                    } else {
                        eg5.run();
                    }
                }
                if (z3) {
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.addAll(r9.A07);
                    r9.A03.add(arrayList2);
                    r9.A07.clear();
                    C29687Eg4 eg4 = new C29687Eg4(r9, arrayList2);
                    if (z) {
                        C15320v6.postOnAnimationDelayed(((C29686Eg3) arrayList2.get(0)).A05.A0H, eg4, r9.A02);
                    } else {
                        eg4.run();
                    }
                }
                if (z4) {
                    ArrayList arrayList3 = new ArrayList();
                    arrayList3.addAll(r9.A06);
                    r9.A01.add(arrayList3);
                    r9.A06.clear();
                    C70923bW r6 = new C70923bW(r9, arrayList3);
                    if (z || z2 || z3) {
                        long j = 0;
                        long j2 = z ? r9.A02 : 0;
                        long j3 = z2 ? r9.A01 : 0;
                        if (z3) {
                            j = 250;
                        }
                        C15320v6.postOnAnimationDelayed(((C33781o8) arrayList3.get(0)).A0H, r6, j2 + Math.max(j3, j));
                        return;
                    }
                    r6.run();
                }
            }
        }
    }

    public void A0A(C33781o8 r9) {
        if (!(this instanceof C15660ve) && !(this instanceof C15670vf)) {
            C32491lq r3 = (C32491lq) this;
            View view = r9.A0H;
            view.animate().cancel();
            for (int size = r3.A08.size() - 1; size >= 0; size--) {
                if (((C29695EgC) r3.A08.get(size)).A04 == r9) {
                    view.setTranslationY(0.0f);
                    view.setTranslationX(0.0f);
                    r3.A0L(r9);
                    r3.A0B(r9);
                    r3.A08.remove(size);
                }
            }
            C32491lq.A01(r3, r3.A07, r9);
            if (r3.A09.remove(r9)) {
                view.setAlpha(1.0f);
                r3.A0N(r9);
                r3.A0B(r9);
            }
            if (r3.A06.remove(r9)) {
                view.setAlpha(1.0f);
                r3.A0J(r9);
                r3.A0B(r9);
            }
            for (int size2 = r3.A03.size() - 1; size2 >= 0; size2--) {
                ArrayList arrayList = (ArrayList) r3.A03.get(size2);
                C32491lq.A01(r3, arrayList, r9);
                if (arrayList.isEmpty()) {
                    r3.A03.remove(size2);
                }
            }
            for (int size3 = r3.A05.size() - 1; size3 >= 0; size3--) {
                ArrayList arrayList2 = (ArrayList) r3.A05.get(size3);
                int size4 = arrayList2.size() - 1;
                while (true) {
                    if (size4 < 0) {
                        break;
                    } else if (((C29695EgC) arrayList2.get(size4)).A04 == r9) {
                        view.setTranslationY(0.0f);
                        view.setTranslationX(0.0f);
                        r3.A0L(r9);
                        r3.A0B(r9);
                        arrayList2.remove(size4);
                        if (arrayList2.isEmpty()) {
                            r3.A05.remove(size3);
                        }
                    } else {
                        size4--;
                    }
                }
            }
            for (int size5 = r3.A01.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList3 = (ArrayList) r3.A01.get(size5);
                if (arrayList3.remove(r9)) {
                    view.setAlpha(1.0f);
                    r3.A0J(r9);
                    r3.A0B(r9);
                    if (arrayList3.isEmpty()) {
                        r3.A01.remove(size5);
                    }
                }
            }
            r3.A0A.remove(r9);
            r3.A00.remove(r9);
            r3.A02.remove(r9);
            r3.A04.remove(r9);
            r3.A0T();
        }
    }

    public boolean A0C() {
        if ((this instanceof C15660ve) || (this instanceof C15670vf)) {
            return false;
        }
        C32491lq r1 = (C32491lq) this;
        return !r1.A06.isEmpty() || !r1.A07.isEmpty() || !r1.A08.isEmpty() || !r1.A09.isEmpty() || !r1.A04.isEmpty() || !r1.A0A.isEmpty() || !r1.A00.isEmpty() || !r1.A02.isEmpty() || !r1.A05.isEmpty() || !r1.A01.isEmpty() || !r1.A03.isEmpty();
    }

    public boolean A0D(C33781o8 r3) {
        return !(this instanceof C191817c) || !((C191817c) this).A00 || r3.A0D();
    }

    public boolean A0E(C33781o8 r9, C60862xx r10, C60862xx r11) {
        int i;
        int i2;
        C191817c r2 = (C191817c) this;
        return (r10 == null || ((i = r10.A01) == (i2 = r11.A01) && r10.A02 == r11.A02)) ? r2.A0P(r9) : r2.A0R(r9, i, r10.A02, i2, r11.A02);
    }

    public boolean A0F(C33781o8 r10, C60862xx r11, C60862xx r12) {
        C191817c r3 = (C191817c) this;
        int i = r11.A01;
        int i2 = r11.A02;
        C33781o8 r4 = r10;
        View view = r10.A0H;
        int left = r12 == null ? view.getLeft() : r12.A01;
        int top = r12 == null ? view.getTop() : r12.A02;
        if (r10.A0E() || (i == left && i2 == top)) {
            return r3.A0Q(r10);
        }
        view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
        return r3.A0R(r4, i, i2, left, top);
    }

    public boolean A0G(C33781o8 r9, C60862xx r10, C60862xx r11) {
        C191817c r2 = (C191817c) this;
        int i = r10.A01;
        int i2 = r11.A01;
        C33781o8 r3 = r9;
        if (i != i2 || r10.A02 != r11.A02) {
            return r2.A0R(r3, i, r10.A02, i2, r11.A02);
        }
        r2.A0L(r9);
        r2.A0B(r9);
        return false;
    }

    public boolean A0I(C33781o8 r9, C33781o8 r10, C60862xx r11, C60862xx r12) {
        int i;
        int i2;
        C191817c r1 = (C191817c) this;
        int i3 = r11.A01;
        int i4 = r11.A02;
        C33781o8 r3 = r10;
        if (r10.A0F()) {
            i2 = i4;
            i = i3;
        } else {
            i = r12.A01;
            i2 = r12.A02;
        }
        return r1.A0S(r9, r3, i3, i4, i, i2);
    }

    public static void A00(C33781o8 r3) {
        int i = r3.A00 & 14;
        if (!r3.A0D() && (i & 4) == 0) {
            int i2 = r3.A02;
            r3.A04();
        }
    }

    public final void A09() {
        int size = this.A04.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass3TW) this.A04.get(i)).BOK();
        }
        this.A04.clear();
    }

    public final void A0B(C33781o8 r2) {
        C16090wT r0 = this.A03;
        if (r0 != null) {
            r0.BOE(r2);
        }
    }

    public boolean A0H(C33781o8 r2, List list) {
        return A0D(r2);
    }
}
