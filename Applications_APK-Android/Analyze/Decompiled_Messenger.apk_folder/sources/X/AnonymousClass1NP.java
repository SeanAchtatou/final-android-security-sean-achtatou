package X;

import android.graphics.drawable.Drawable;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1NP  reason: invalid class name */
public final class AnonymousClass1NP extends C17770zR {
    public static final int A08 = C17190yT.A00().AkX();
    public static final int A09 = C17190yT.A00().AzK();
    public static final int A0A = AnonymousClass1KA.A00(C17190yT.A00().AfS());
    public static final int A0B = AnonymousClass1KA.A01(C17190yT.A00().AfS());
    @Comparable(type = 3)
    public int A00 = A08;
    @Comparable(type = 3)
    public int A01 = A09;
    @Comparable(type = 3)
    public int A02 = A0A;
    @Comparable(type = 3)
    public int A03 = A0B;
    @Comparable(type = 3)
    public int A04;
    @Comparable(type = 13)
    public Drawable A05;
    @Comparable(type = 13)
    public String A06;
    @Comparable(type = 3)
    public boolean A07 = true;

    public AnonymousClass1NP() {
        super("BaseM4MigIconButton");
    }
}
