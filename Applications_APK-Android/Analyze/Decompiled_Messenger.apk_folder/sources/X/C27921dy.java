package X;

/* renamed from: X.1dy  reason: invalid class name and case insensitive filesystem */
public final class C27921dy {
    public final C27961e2 A00;
    public final C27911dx A01;

    public C27921dy(C27911dx r1, C27961e2 r2) {
        this.A00 = r2;
        this.A01 = r1;
    }

    public C27941e0 A00(Class cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            String A0J = AnonymousClass08S.A0J("androidx.lifecycle.ViewModelProvider.DefaultKey:", canonicalName);
            C27941e0 r1 = (C27941e0) this.A01.A00.get(A0J);
            if (!cls.isInstance(r1)) {
                C27961e2 r12 = this.A00;
                if (r12 instanceof C27971e3) {
                    r1 = ((C27971e3) r12).A00(A0J, cls);
                } else {
                    r1 = r12.AUd(cls);
                }
                C27941e0 r0 = (C27941e0) this.A01.A00.put(A0J, r1);
                if (r0 != null) {
                    r0.A02();
                }
            }
            return r1;
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
}
