package X;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1OW  reason: invalid class name */
public final class AnonymousClass1OW {
    public final Map A00 = new HashMap();
    private final AnonymousClass1OX A01;
    private final AnonymousClass1OY A02;

    public C30781id A00(long j) {
        Long valueOf;
        C30781id r2;
        synchronized (this.A00) {
            try {
                Map map = this.A00;
                valueOf = Long.valueOf(j);
                r2 = (C30781id) map.get(valueOf);
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (r2 != null) {
            return r2;
        }
        AnonymousClass1OX r6 = this.A01;
        int i = (int) ((j >>> 32) & 65535);
        String B4J = r6.A01.B4J(j, AnonymousClass0XE.A05);
        r6.A00.now();
        C23121Oh r5 = new C23121Oh(B4J);
        r5.A00.put("conduit", "mobile_config");
        r5.A00.put("conduit_info", AnonymousClass08S.A02(i, ":", (int) ((j >>> 16) & 65535)));
        synchronized (r6.A02) {
            try {
                Map map2 = r6.A02;
                Integer valueOf2 = Integer.valueOf(i);
                if (map2.get(valueOf2) == null) {
                    r6.A02.put(valueOf2, new HashSet());
                }
                ((Set) r6.A02.get(valueOf2)).add(valueOf);
            } catch (Throwable th2) {
                while (true) {
                    th = th2;
                    break;
                }
            }
        }
        C30781id A06 = this.A02.A06(r5);
        synchronized (this.A00) {
            try {
                this.A00.put(valueOf, A06);
            } catch (Throwable th3) {
                th = th3;
                throw th;
            }
        }
        return A06;
    }

    public void A01() {
        synchronized (this.A00) {
            this.A00.clear();
        }
    }

    public AnonymousClass1OW(AnonymousClass1OX r2, AnonymousClass1OY r3) {
        this.A01 = r2;
        this.A02 = r3;
        r2.A03 = this;
    }
}
