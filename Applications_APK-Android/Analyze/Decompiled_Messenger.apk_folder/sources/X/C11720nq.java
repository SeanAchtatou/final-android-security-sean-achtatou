package X;

import java.io.Serializable;

/* renamed from: X.0nq  reason: invalid class name and case insensitive filesystem */
public final class C11720nq implements Serializable {
    public static final C11740ns[] NO_MODIFIERS = new C11740ns[0];
    public static final C11730nr[] NO_SERIALIZERS = new C11730nr[0];
    private static final long serialVersionUID = 1;
    public final C11730nr[] _additionalKeySerializers;
    public final C11730nr[] _additionalSerializers;
    public final C11740ns[] _modifiers;

    public boolean hasSerializerModifiers() {
        if (this._modifiers.length > 0) {
            return true;
        }
        return false;
    }

    public C11720nq() {
        this(null, null, null);
    }

    public C11720nq(C11730nr[] r1, C11730nr[] r2, C11740ns[] r3) {
        this._additionalSerializers = r1 == null ? NO_SERIALIZERS : r1;
        this._additionalKeySerializers = r2 == null ? NO_SERIALIZERS : r2;
        this._modifiers = r3 == null ? NO_MODIFIERS : r3;
    }
}
