package X;

import java.io.UnsupportedEncodingException;

/* renamed from: X.1O5  reason: invalid class name */
public final class AnonymousClass1O5 {
    public static boolean A00(byte[] bArr, byte[] bArr2, int i) {
        C05520Zg.A02(bArr);
        C05520Zg.A02(bArr2);
        int length = bArr2.length;
        if (length + i <= bArr.length) {
            int i2 = 0;
            while (i2 < length) {
                if (bArr[i + i2] == bArr2[i2]) {
                    i2++;
                }
            }
            return true;
        }
        return false;
    }

    public static byte[] A01(String str) {
        C05520Zg.A02(str);
        try {
            return str.getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("ASCII not found!", e);
        }
    }
}
