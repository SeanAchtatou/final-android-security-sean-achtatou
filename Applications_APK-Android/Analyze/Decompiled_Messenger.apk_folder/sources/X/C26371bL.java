package X;

import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.1bL  reason: invalid class name and case insensitive filesystem */
public final class C26371bL implements C26381bM {
    public AnonymousClass0f7 AUy(C26401bO r2, C26501bY r3) {
        return null;
    }

    public int AyN() {
        return 0;
    }

    public int AyO() {
        return 0;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", "none");
            jSONObject.put("framework", BuildConfig.FLAVOR);
            jSONObject.put("extra", BuildConfig.FLAVOR);
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }
}
