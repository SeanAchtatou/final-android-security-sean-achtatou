package X;

import android.content.Context;
import android.content.Intent;
import java.util.Locale;

/* renamed from: X.1S8  reason: invalid class name */
public final class AnonymousClass1S8 implements AnonymousClass06U {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass1HK A01;
    public final /* synthetic */ Locale A02;

    public AnonymousClass1S8(AnonymousClass1HK r1, Locale locale, Context context) {
        this.A01 = r1;
        this.A02 = locale;
        this.A00 = context;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r8) {
        int A002 = AnonymousClass09Y.A00(-1603579381);
        this.A01.A00 = new AnonymousClass1S7(this.A02, this.A00);
        AnonymousClass09Y.A01(59728942, A002);
    }
}
