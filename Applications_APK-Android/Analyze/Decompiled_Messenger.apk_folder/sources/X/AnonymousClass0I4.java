package X;

import android.content.Context;
import android.os.Build;
import com.facebook.profilo.provider.stacktrace.ArtCompatibility;
import java.io.File;

/* renamed from: X.0I4  reason: invalid class name */
public final class AnonymousClass0I4 implements AnonymousClass1YQ {
    private AnonymousClass0UN A00;

    public String getSimpleName() {
        return "ArtProfilerUpgradeListener";
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.AvM, r1);
    }

    public static final AnonymousClass0I4 A01(AnonymousClass1XY r1) {
        return new AnonymousClass0I4(r1);
    }

    private AnonymousClass0I4(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public void init() {
        int A03 = C000700l.A03(-144997729);
        Context context = (Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, this.A00);
        File file = new File(context.getFilesDir(), AnonymousClass08S.A0J("ProfiloArtUnwindcCompat_", Build.VERSION.RELEASE));
        if (file.exists() && !file.delete()) {
            file.deleteOnExit();
        }
        ArtCompatibility.isCompatible(context);
        C000700l.A09(934217774, A03);
    }
}
