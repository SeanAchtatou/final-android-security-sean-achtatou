package X;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.internal.zace;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/* renamed from: X.1yW  reason: invalid class name and case insensitive filesystem */
public class C39111yW {
    public final int A00;
    public final Context A01;
    public final Looper A02;
    public final C28859E7l A03;
    public final EBR A04;
    public final C28910EAy A05;
    private final E5b A06;
    private final E7D A07;

    private E6P A00() {
        Account account;
        Collection emptySet;
        GoogleSignInAccount googleSignInAccount;
        GoogleSignInAccount googleSignInAccount2;
        E6P e6p = new E6P();
        E5b e5b = this.A06;
        if (!(e5b instanceof C28841E5w) || (googleSignInAccount2 = ((C28841E5w) e5b).getGoogleSignInAccount()) == null) {
            if (e5b instanceof E6L) {
                account = ((E6L) e5b).getAccount();
            }
            account = null;
        } else {
            String str = googleSignInAccount2.A04;
            if (str != null) {
                account = new Account(str, "com.google");
            }
            account = null;
        }
        e6p.A00 = account;
        E5b e5b2 = this.A06;
        if (!(e5b2 instanceof C28841E5w) || (googleSignInAccount = ((C28841E5w) e5b2).getGoogleSignInAccount()) == null) {
            emptySet = Collections.emptySet();
        } else {
            emptySet = new HashSet(googleSignInAccount.A0A);
            emptySet.addAll(googleSignInAccount.A0B);
        }
        if (e6p.A01 == null) {
            e6p.A01 = new C21321Gd();
        }
        e6p.A01.addAll(emptySet);
        Context context = this.A01;
        e6p.A03 = context.getClass().getName();
        e6p.A02 = context.getPackageName();
        return e6p;
    }

    public static final C53112kD A01(C39111yW r6, int i, E75 e75) {
        C53122kE r5 = new C53122kE();
        EBR ebr = r6.A04;
        C28867E7y e7y = new C28867E7y(e75, r5, r6.A07);
        Handler handler = ebr.A03;
        handler.sendMessage(handler.obtainMessage(4, new E7s(e7y, ebr.A09.get(), r6)));
        return r5.A00;
    }

    public zace A03(Context context, Handler handler) {
        zace zace = new zace(context, handler, A00().A00(), zace.A07);
        C000700l.A09(408431305, C000700l.A03(1818609551));
        return zace;
    }

    public C39111yW(Context context, C28859E7l e7l, E5b e5b, E7C e7c) {
        AnonymousClass09E.A02(context, "Null context is not permitted.");
        AnonymousClass09E.A02(e7l, "Api must not be null.");
        AnonymousClass09E.A02(e7c, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.A01 = context.getApplicationContext();
        this.A03 = e7l;
        this.A06 = e5b;
        this.A02 = e7c.A00;
        this.A05 = new C28910EAy(e7l, e5b);
        EBR A002 = EBR.A00(this.A01);
        this.A04 = A002;
        this.A00 = A002.A08.getAndIncrement();
        this.A07 = e7c.A01;
        Handler handler = this.A04.A03;
        handler.sendMessage(handler.obtainMessage(7, this));
    }

    public C28917EBi A02(Looper looper, EBQ ebq) {
        E6Q A002 = A00().A00();
        C28859E7l e7l = this.A03;
        boolean z = false;
        if (e7l.A00 != null) {
            z = true;
        }
        AnonymousClass09E.A09(z, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return e7l.A00.A01(this.A01, looper, A002, this.A06, ebq, ebq);
    }
}
