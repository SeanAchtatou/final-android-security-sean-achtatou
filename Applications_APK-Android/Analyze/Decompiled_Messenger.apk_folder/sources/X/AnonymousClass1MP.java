package X;

import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1MP  reason: invalid class name */
public final class AnonymousClass1MP extends AnonymousClass1NF {
    private AnonymousClass1BY A00;

    public AnonymousClass1MP(AnonymousClass1NH r1, InboxUnitThreadItem inboxUnitThreadItem, C21361Gm r3, AnonymousClass1BY r4) {
        super(r1, inboxUnitThreadItem, r3);
        this.A00 = r4;
    }

    public void A00() {
        super.A00();
        AnonymousClass1BY r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
    }
}
