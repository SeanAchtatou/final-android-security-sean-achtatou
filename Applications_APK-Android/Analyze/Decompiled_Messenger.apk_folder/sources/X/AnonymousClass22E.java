package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.facebook.bugreporter.activity.categorylist.CategoryInfo;
import com.facebook.common.util.TriState;
import com.google.common.collect.ImmutableList;

/* renamed from: X.22E  reason: invalid class name */
public final class AnonymousClass22E extends BaseAdapter {
    public ImmutableList A00;
    private final Context A01;
    private final TriState A02;

    public boolean hasStableIds() {
        return true;
    }

    public static final AnonymousClass22E A00(AnonymousClass1XY r1) {
        return new AnonymousClass22E(r1);
    }

    /* renamed from: A01 */
    public CategoryInfo getItem(int i) {
        if (i < this.A00.size()) {
            return (CategoryInfo) this.A00.get(i);
        }
        return null;
    }

    public int getCount() {
        return this.A00.size();
    }

    public long getItemId(int i) {
        if (i < this.A00.size()) {
            return ((CategoryInfo) this.A00.get(i)).A00;
        }
        return -1;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        String str;
        C104914zo r6 = (C104914zo) view;
        boolean z = false;
        if (i <= this.A00.size()) {
            z = true;
        }
        C12870q8.A00(z, AnonymousClass80H.$const$string(514));
        if (r6 == null) {
            r6 = new C104914zo(this.A01, this.A02);
        }
        CategoryInfo A012 = getItem(i);
        TextView textView = r6.A00;
        if (TriState.YES.equals(r6.A01)) {
            str = A012.A01;
        } else {
            str = A012.A02;
        }
        textView.setText(str);
        return r6;
    }

    public AnonymousClass22E(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass1YA.A00(r2);
        this.A02 = AnonymousClass0XJ.A04(r2);
    }
}
