package X;

/* renamed from: X.0HJ  reason: invalid class name */
public final class AnonymousClass0HJ {
    private C07870eJ A00;

    public void A00() {
        this.A00.A06();
    }

    public void A01(int i) {
        this.A00.A09(i, this);
    }

    public int[] A02() {
        C07870eJ r4 = this.A00;
        int A01 = r4.A01();
        int[] iArr = new int[A01];
        for (int i = 0; i < A01; i++) {
            iArr[i] = r4.A02(i);
        }
        return iArr;
    }

    public AnonymousClass0HJ() {
        this(new C07870eJ());
    }

    public AnonymousClass0HJ(int i) {
        this(new C07870eJ(i));
    }

    private AnonymousClass0HJ(C07870eJ r1) {
        this.A00 = r1;
    }
}
