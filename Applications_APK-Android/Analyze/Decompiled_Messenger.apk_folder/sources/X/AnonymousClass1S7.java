package X;

import android.content.Context;
import android.os.Build;
import android.text.format.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/* renamed from: X.1S7  reason: invalid class name */
public final class AnonymousClass1S7 {
    private static final boolean A0H;
    public final ThreadLocal A00;
    public final ThreadLocal A01;
    public final ThreadLocal A02;
    public final ThreadLocal A03;
    public final ThreadLocal A04;
    public final ThreadLocal A05;
    public final ThreadLocal A06;
    public final ThreadLocal A07;
    public final ThreadLocal A08;
    public final ThreadLocal A09;
    public final ThreadLocal A0A;
    public final ThreadLocal A0B;
    public final Locale A0C;
    private final Context A0D;
    private final ThreadLocal A0E = new ThreadLocal();
    private final ThreadLocal A0F = new ThreadLocal();
    private final ThreadLocal A0G = new ThreadLocal();

    static {
        boolean z;
        int i = Build.VERSION.SDK_INT;
        if (i < 18 || (i == 18 && Build.MANUFACTURER.toUpperCase(Locale.getDefault()).equals("SAMSUNG") && Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SM-N900"))) {
            z = false;
        } else {
            z = true;
        }
        A0H = z;
    }

    public static void A00(SimpleDateFormat simpleDateFormat, String str, Locale locale) {
        if (A0H) {
            simpleDateFormat.applyPattern(DateFormat.getBestDateTimePattern(locale, str));
        }
    }

    public java.text.DateFormat A01() {
        java.text.DateFormat dateFormat = (java.text.DateFormat) this.A0E.get();
        if (dateFormat != null) {
            return dateFormat;
        }
        java.text.DateFormat dateInstance = java.text.DateFormat.getDateInstance(2, this.A0C);
        this.A0E.set(dateInstance);
        return dateInstance;
    }

    public java.text.DateFormat A02() {
        String str;
        java.text.DateFormat dateFormat = (java.text.DateFormat) this.A0F.get();
        if (dateFormat == null) {
            Context context = this.A0D;
            if (context == null || Build.VERSION.SDK_INT < 18) {
                dateFormat = java.text.DateFormat.getTimeInstance(3, this.A0C);
            } else {
                if (DateFormat.is24HourFormat(context)) {
                    str = "Hm";
                } else {
                    str = "hm";
                }
                dateFormat = new SimpleDateFormat(DateFormat.getBestDateTimePattern(this.A0C, str), this.A0C);
            }
            this.A0F.set(dateFormat);
        }
        return dateFormat;
    }

    public java.text.DateFormat A03() {
        String str;
        java.text.DateFormat dateFormat = (java.text.DateFormat) this.A0G.get();
        if (dateFormat == null) {
            Context context = this.A0D;
            if (context == null || Build.VERSION.SDK_INT < 18) {
                dateFormat = java.text.DateFormat.getTimeInstance(3, this.A0C);
            } else {
                if (DateFormat.is24HourFormat(context)) {
                    str = "Hm";
                } else {
                    str = "h";
                }
                dateFormat = new SimpleDateFormat(DateFormat.getBestDateTimePattern(this.A0C, str), this.A0C);
            }
            this.A0G.set(dateFormat);
        }
        return dateFormat;
    }

    public AnonymousClass1S7(Locale locale, Context context) {
        new ThreadLocal();
        this.A02 = new ThreadLocal();
        this.A08 = new ThreadLocal();
        new ThreadLocal();
        this.A03 = new ThreadLocal();
        this.A05 = new ThreadLocal();
        this.A00 = new ThreadLocal();
        this.A0A = new ThreadLocal();
        this.A09 = new ThreadLocal();
        this.A0B = new ThreadLocal();
        this.A04 = new ThreadLocal();
        this.A06 = new ThreadLocal();
        this.A01 = new ThreadLocal();
        this.A07 = new ThreadLocal();
        this.A0C = locale;
        this.A0D = context;
    }
}
