package X;

import android.content.Context;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1CU  reason: invalid class name */
public final class AnonymousClass1CU {
    private static volatile AnonymousClass1CU A05;
    private final Context A00;
    private final AnonymousClass06B A01;
    private final FbSharedPreferences A02;
    private final HashMap A03 = new HashMap();
    private final ExecutorService A04;

    public synchronized C11330mk A01(String str) {
        C11330mk r1;
        String str2 = str;
        if (str.equals("mqtt_instance") || str.equals(AnonymousClass24B.$const$string(27))) {
            r1 = (C11330mk) this.A03.get(str);
            if (r1 == null) {
                r1 = new C11330mk(str2, this.A00, this.A02, this.A01, this.A04);
                this.A03.put(str, r1);
            }
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0J("Unknown log type: ", str));
        }
        return r1;
    }

    public static final AnonymousClass1CU A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (AnonymousClass1CU.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new AnonymousClass1CU(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private AnonymousClass1CU(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass1YA.A00(r2);
        this.A02 = FbSharedPreferencesModule.A00(r2);
        this.A01 = AnonymousClass067.A02();
        this.A04 = AnonymousClass0UX.A0b(r2);
    }
}
