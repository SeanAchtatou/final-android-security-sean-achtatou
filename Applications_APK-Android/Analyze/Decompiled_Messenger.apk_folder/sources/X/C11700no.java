package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import com.fasterxml.jackson.databind.cfg.PackageVersion;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0no  reason: invalid class name and case insensitive filesystem */
public final class C11700no extends C11710np {
    public static final int DEFAULT_PARSER_FEATURES;
    public int _appendOffset;
    public boolean _closed;
    public AnonymousClass2Rd _first;
    public int _generatorFeatures = DEFAULT_PARSER_FEATURES;
    public AnonymousClass2Rd _last;
    public C09980jK _objectCodec;
    public C48752az _writeContext = new C48752az(0, null);

    public void close() {
        this._closed = true;
    }

    public void flush() {
    }

    public C11710np useDefaultPrettyPrinter() {
        return this;
    }

    public static void copyCurrentEvent(C11700no r3, C28271eX r4) {
        switch (CXm.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r4.getCurrentToken().ordinal()]) {
            case 1:
                r3.writeStartObject();
                return;
            case 2:
                r3.writeEndObject();
                return;
            case 3:
                r3.writeStartArray();
                return;
            case 4:
                r3.writeEndArray();
                return;
            case 5:
                r3.writeFieldName(r4.getCurrentName());
                return;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                if (r4.hasTextCharacters()) {
                    r3.writeString(r4.getTextCharacters(), r4.getTextOffset(), r4.getTextLength());
                    return;
                } else {
                    r3.writeString(r4.getText());
                    return;
                }
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                switch (r4.getNumberType().ordinal()) {
                    case 0:
                        r3.writeNumber(r4.getIntValue());
                        return;
                    case 1:
                    default:
                        r3.writeNumber(r4.getLongValue());
                        return;
                    case 2:
                        r3.writeNumber(r4.getBigIntegerValue());
                        return;
                }
            case 8:
                switch (r4.getNumberType().ordinal()) {
                    case 3:
                        r3.writeNumber(r4.getFloatValue());
                        return;
                    case 4:
                    default:
                        r3.writeNumber(r4.getDoubleValue());
                        return;
                    case 5:
                        r3.writeNumber(r4.getDecimalValue());
                        return;
                }
            case Process.SIGKILL:
                r3.writeBoolean(true);
                return;
            case AnonymousClass1Y3.A01 /*10*/:
                r3.writeBoolean(false);
                return;
            case AnonymousClass1Y3.A02 /*11*/:
                r3.writeNull();
                return;
            case AnonymousClass1Y3.A03 /*12*/:
                r3.writeObject(r4.getEmbeddedObject());
                return;
            default:
                throw new RuntimeException("Internal error: should never end up through this code path");
        }
    }

    public C09980jK getCodec() {
        return this._objectCodec;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[TokenBuffer: ");
        C28271eX asParser = asParser();
        int i = 0;
        while (true) {
            try {
                C182811d nextToken = asParser.nextToken();
                if (nextToken == null) {
                    break;
                }
                if (i < 100) {
                    if (i > 0) {
                        sb.append(", ");
                    }
                    sb.append(nextToken.toString());
                    if (nextToken == C182811d.FIELD_NAME) {
                        sb.append('(');
                        sb.append(asParser.getCurrentName());
                        sb.append(')');
                    }
                }
                i++;
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        if (i >= 100) {
            sb.append(" ... (truncated ");
            sb.append(i - 100);
            sb.append(" entries)");
        }
        sb.append(']');
        return sb.toString();
    }

    public C11780nw version() {
        return PackageVersion.VERSION;
    }

    public void writeBinary(C10350jx r3, byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        writeObject(bArr2);
    }

    public void writeBoolean(boolean z) {
        C182811d r0;
        if (z) {
            r0 = C182811d.VALUE_TRUE;
        } else {
            r0 = C182811d.VALUE_FALSE;
        }
        _append(r0);
    }

    public final void writeEndArray() {
        _append(C182811d.END_ARRAY);
        C48752az r0 = this._writeContext._parent;
        if (r0 != null) {
            this._writeContext = r0;
        }
    }

    public final void writeEndObject() {
        _append(C182811d.END_OBJECT);
        C48752az r0 = this._writeContext._parent;
        if (r0 != null) {
            this._writeContext = r0;
        }
    }

    public void writeNull() {
        _append(C182811d.VALUE_NULL);
    }

    public void writeObject(Object obj) {
        _append(C182811d.VALUE_EMBEDDED_OBJECT, obj);
    }

    public void writeRawValue(String str) {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    public final void writeStartArray() {
        _append(C182811d.START_ARRAY);
        this._writeContext = this._writeContext.createChildArrayContext();
    }

    public final void writeStartObject() {
        _append(C182811d.START_OBJECT);
        this._writeContext = this._writeContext.createChildObjectContext();
    }

    static {
        int i = 0;
        for (C10390k1 r1 : C10390k1.values()) {
            if (r1._defaultState) {
                i |= 1 << r1.ordinal();
            }
        }
        DEFAULT_PARSER_FEATURES = i;
    }

    public C11700no(C09980jK r4) {
        this._objectCodec = r4;
        AnonymousClass2Rd r0 = new AnonymousClass2Rd();
        this._last = r0;
        this._first = r0;
        this._appendOffset = 0;
    }

    public void copyCurrentStructure(C28271eX r4) {
        C182811d currentToken = r4.getCurrentToken();
        if (currentToken == C182811d.FIELD_NAME) {
            writeFieldName(r4.getCurrentName());
            currentToken = r4.nextToken();
        }
        int i = CXm.$SwitchMap$com$fasterxml$jackson$core$JsonToken[currentToken.ordinal()];
        if (i == 1) {
            writeStartObject();
            while (r4.nextToken() != C182811d.END_OBJECT) {
                copyCurrentStructure(r4);
            }
            writeEndObject();
        } else if (i != 3) {
            copyCurrentEvent(this, r4);
        } else {
            writeStartArray();
            while (r4.nextToken() != C182811d.END_ARRAY) {
                copyCurrentStructure(r4);
            }
            writeEndArray();
        }
    }

    public C11710np setCodec(C09980jK r1) {
        this._objectCodec = r1;
        return this;
    }

    private final void _append(C182811d r8) {
        AnonymousClass2Rd r4;
        AnonymousClass2Rd r6 = this._last;
        int i = this._appendOffset;
        if (i < 16) {
            long ordinal = (long) r8.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            r6._tokenTypes |= ordinal;
            r4 = null;
        } else {
            r4 = new AnonymousClass2Rd();
            r6._next = r4;
            r4._tokenTypes |= (long) r8.ordinal();
        }
        if (r4 == null) {
            this._appendOffset = i + 1;
            return;
        }
        this._last = r4;
        this._appendOffset = 1;
    }

    private final void _append(C182811d r8, Object obj) {
        AnonymousClass2Rd r4;
        AnonymousClass2Rd r6 = this._last;
        int i = this._appendOffset;
        if (i < 16) {
            r6._tokens[i] = obj;
            long ordinal = (long) r8.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            r6._tokenTypes = ordinal | r6._tokenTypes;
            r4 = null;
        } else {
            r4 = new AnonymousClass2Rd();
            r6._next = r4;
            r4._tokens[0] = obj;
            r4._tokenTypes = ((long) r8.ordinal()) | r4._tokenTypes;
        }
        if (r4 == null) {
            this._appendOffset = i + 1;
            return;
        }
        this._last = r4;
        this._appendOffset = 1;
    }

    public C28271eX asParser() {
        return new C25098CXn(this._first, this._objectCodec);
    }

    public C28271eX asParser(C28271eX r4) {
        C25098CXn cXn = new C25098CXn(this._first, r4.getCodec());
        cXn._location = r4.getTokenLocation();
        return cXn;
    }

    public void writeFieldName(C10240jm r3) {
        _append(C182811d.FIELD_NAME, r3);
        this._writeContext.writeFieldName(r3.getValue());
    }

    public final void writeFieldName(String str) {
        _append(C182811d.FIELD_NAME, str);
        this._writeContext.writeFieldName(str);
    }

    public void writeNumber(double d) {
        _append(C182811d.VALUE_NUMBER_FLOAT, Double.valueOf(d));
    }

    public void writeNumber(float f) {
        _append(C182811d.VALUE_NUMBER_FLOAT, Float.valueOf(f));
    }

    public void writeNumber(int i) {
        _append(C182811d.VALUE_NUMBER_INT, Integer.valueOf(i));
    }

    public void writeNumber(long j) {
        _append(C182811d.VALUE_NUMBER_INT, Long.valueOf(j));
    }

    public void writeNumber(String str) {
        _append(C182811d.VALUE_NUMBER_FLOAT, str);
    }

    public void writeNumber(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            writeNull();
        } else {
            _append(C182811d.VALUE_NUMBER_FLOAT, bigDecimal);
        }
    }

    public void writeNumber(BigInteger bigInteger) {
        if (bigInteger == null) {
            writeNull();
        } else {
            _append(C182811d.VALUE_NUMBER_INT, bigInteger);
        }
    }

    public void writeNumber(short s) {
        _append(C182811d.VALUE_NUMBER_INT, Short.valueOf(s));
    }

    public void writeRaw(char c) {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    public void writeRaw(C10240jm r3) {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    public void writeRaw(String str) {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    public void writeRaw(char[] cArr, int i, int i2) {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    public void writeString(C10240jm r2) {
        if (r2 == null) {
            writeNull();
        } else {
            _append(C182811d.VALUE_STRING, r2);
        }
    }

    public void writeString(String str) {
        if (str == null) {
            writeNull();
        } else {
            _append(C182811d.VALUE_STRING, str);
        }
    }

    public void writeString(char[] cArr, int i, int i2) {
        writeString(new String(cArr, i, i2));
    }
}
