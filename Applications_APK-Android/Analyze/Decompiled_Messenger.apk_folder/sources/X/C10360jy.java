package X;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/* renamed from: X.0jy  reason: invalid class name and case insensitive filesystem */
public final class C10360jy extends C10050jT {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jT.forDeserialization(X.0kF, X.0jR, X.1by):X.0jZ
     arg types: [X.0kF, X.0jR, X.1by]
     candidates:
      X.0jy.forDeserialization(X.0kF, X.0jR, X.1by):X.0ja
      X.0jT.forDeserialization(X.0kF, X.0jR, X.1by):X.0ja
      X.0jU.forDeserialization(X.0kF, X.0jR, X.1by):X.0ja
      X.0jT.forDeserialization(X.0kF, X.0jR, X.1by):X.0jZ */
    public C10110jZ forDeserialization(C10490kF r3, C10030jR r4, C26761by r5) {
        C10110jZ _findCachedDesc = C10050jT._findCachedDesc(r4);
        if (_findCachedDesc != null) {
            return _findCachedDesc;
        }
        JsonDeserialize jsonDeserialize = (JsonDeserialize) r4._class.getAnnotation(JsonDeserialize.class);
        if (jsonDeserialize == null || jsonDeserialize.using() == null) {
            return super.forDeserialization(r3, r4, r5);
        }
        return C10050jT.forDirectClassAnnotations(r3, r4, r5);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0jT.forSerialization(X.0k8, X.0jR, X.1by):X.0jZ
     arg types: [X.0k8, X.0jR, X.1by]
     candidates:
      X.0jy.forSerialization(X.0k8, X.0jR, X.1by):X.0ja
      X.0jT.forSerialization(X.0k8, X.0jR, X.1by):X.0ja
      X.0jU.forSerialization(X.0k8, X.0jR, X.1by):X.0ja
      X.0jT.forSerialization(X.0k8, X.0jR, X.1by):X.0jZ */
    public C10110jZ forSerialization(C10450k8 r3, C10030jR r4, C26761by r5) {
        C10110jZ _findCachedDesc = C10050jT._findCachedDesc(r4);
        if (_findCachedDesc != null) {
            return _findCachedDesc;
        }
        JsonSerialize jsonSerialize = (JsonSerialize) r4._class.getAnnotation(JsonSerialize.class);
        if (jsonSerialize == null || jsonSerialize.using() == null) {
            return super.forSerialization(r3, r4, r5);
        }
        return C10050jT.forDirectClassAnnotations(r3, r4, r5);
    }
}
