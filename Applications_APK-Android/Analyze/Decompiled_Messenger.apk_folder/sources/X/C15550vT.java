package X;

import android.content.Context;
import com.google.common.collect.RegularImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0vT  reason: invalid class name and case insensitive filesystem */
public final class C15550vT implements C15520vQ {
    private static volatile C15550vT A02;
    private AnonymousClass0UN A00;
    private final AnonymousClass1MT A01;

    public String B5H() {
        return "ComposerButton";
    }

    public static final C15550vT A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C15550vT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C15550vT(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public C15390vD Ae6() {
        return C32521lt.A00;
    }

    public AnonymousClass10Z Ap6(Context context) {
        return AnonymousClass10Z.A00(AnonymousClass01R.A03(context, this.A01.A02(C34891qL.A0w, AnonymousClass07B.A01)));
    }

    public void BSD(Context context, C33691nz r4, C13060qW r5) {
        ((C13410rO) AnonymousClass1XX.A03(AnonymousClass1Y3.Ajr, this.A00)).A01.A01.A0D("topRightOmnipicker");
        r4.BvV(context, RegularImmutableList.A02);
    }

    private C15550vT(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = AnonymousClass1MT.A00(r3);
    }

    public String AiM(Context context) {
        return context.getString(2131823042);
    }
}
