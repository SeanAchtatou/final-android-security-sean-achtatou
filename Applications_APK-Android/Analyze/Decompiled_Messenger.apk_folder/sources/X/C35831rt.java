package X;

/* renamed from: X.1rt  reason: invalid class name and case insensitive filesystem */
public final class C35831rt implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appchoreographer.BusySignalHandler$8";
    public final /* synthetic */ C28461eq A00;

    public C35831rt(C28461eq r1) {
        this.A00 = r1;
    }

    public void run() {
        synchronized (this.A00.A05) {
            for (AnonymousClass22U A07 : this.A00.A08.keySet()) {
                A07.A07();
            }
            this.A00.A08.clear();
        }
    }
}
