package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1l9  reason: invalid class name and case insensitive filesystem */
public final class C32101l9 extends C17770zR {
    public static final int A07 = AnonymousClass1JQ.XLARGE.B3A();
    public static final Integer A08 = AnonymousClass07B.A0C;
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public C33691nz A01;
    @Comparable(type = 13)
    public C15400vE A02;
    @Comparable(type = 13)
    public C15350v9 A03;
    @Comparable(type = 5)
    public ImmutableList A04;
    @Comparable(type = 13)
    public String A05;
    @Comparable(type = 3)
    public boolean A06;

    public C32101l9(Context context) {
        super("TabToolbar");
        this.A00 = new AnonymousClass0UN(6, AnonymousClass1XX.get(context));
    }
}
