package X;

import android.content.Context;
import com.facebook.inject.ContextScoped;

/* renamed from: X.0Uu  reason: invalid class name and case insensitive filesystem */
public final class C04470Uu {
    public Object A00;
    private C04500Uy A01;
    private C24911Xp A02;
    private C24811Xe A03;
    private AnonymousClass0UO A04;
    private Byte A05;
    private Object A06;

    public final void A02() {
        try {
            C24811Xe r0 = this.A03;
            if (r0 != null) {
                r0.A03();
            }
            C04500Uy r2 = this.A01;
            if (r2 != null) {
                Object obj = this.A00;
                if (!(obj == null || this.A03 == null)) {
                    r2.CB0(this, obj);
                }
            } else if (this.A06 == null) {
                this.A06 = this.A00;
            }
            Byte b = this.A05;
            if (b != null) {
                this.A04.A00 = b.byteValue();
            }
        } finally {
            this.A03 = null;
            this.A02 = null;
            this.A01 = null;
            this.A05 = null;
            this.A04 = null;
            this.A00 = null;
        }
    }

    public static C04470Uu A00(C04470Uu r1) {
        if (r1 == null) {
            r1 = new C04470Uu();
        }
        if (r1.A04 == null) {
            return r1;
        }
        throw new IllegalStateException("reentrant injection or failed cleanup detected");
    }

    public final C24851Xi A01() {
        return this.A03.A01();
    }

    public final boolean A03(AnonymousClass1XY r4) {
        AnonymousClass0UO A002 = AnonymousClass0UO.A00();
        this.A04 = A002;
        byte b = A002.A00;
        A002.A00 = (byte) (8 | b);
        this.A05 = Byte.valueOf(b);
        Context Aq3 = r4.getScopeAwareInjector().Aq3();
        if (Aq3 != null) {
            this.A02 = (C24911Xp) r4.getScope(ContextScoped.class);
            C04500Uy r0 = (C04500Uy) AnonymousClass065.A00(Aq3, C04500Uy.class);
            this.A01 = r0;
            if (r0 != null) {
                this.A00 = r0.Aze(this);
            } else {
                this.A00 = this.A06;
            }
            if (this.A00 != null) {
                return false;
            }
            C24811Xe injectorThreadStack = r4.getInjectorThreadStack();
            this.A03 = injectorThreadStack;
            injectorThreadStack.A02.add((C24851Xi) this.A02.A01.A00(Aq3));
            return true;
        }
        throw new C38471xU("Called context scoped provider outside of context scope");
    }
}
