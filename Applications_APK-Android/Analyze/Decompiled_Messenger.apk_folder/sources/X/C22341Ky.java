package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1Ky  reason: invalid class name and case insensitive filesystem */
public final class C22341Ky extends AnonymousClass0UV {
    private static volatile C22351Kz A00;

    public static final Boolean A02() {
        return false;
    }

    public static final Boolean A03() {
        return false;
    }

    public static final C22351Kz A01(AnonymousClass1XY r5) {
        AnonymousClass1L4 r0;
        if (A00 == null) {
            synchronized (C22351Kz.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        AnonymousClass1L1 A003 = AnonymousClass1L0.A00(applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.A4m, applicationInjector);
                        if (A003 == null) {
                            r0 = null;
                        } else {
                            r0 = (AnonymousClass1L4) A004.get();
                        }
                        A00 = r0;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C22351Kz A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
