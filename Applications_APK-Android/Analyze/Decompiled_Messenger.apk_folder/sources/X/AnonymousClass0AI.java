package X;

import android.util.SparseArray;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0AI  reason: invalid class name */
public final class AnonymousClass0AI {
    public static final SparseArray A00 = new AnonymousClass0AJ();
    private static final Map A01 = new HashMap();

    static {
        for (int i = 0; i < A00.size(); i++) {
            Map map = A01;
            SparseArray sparseArray = A00;
            map.put(sparseArray.valueAt(i), Integer.valueOf(sparseArray.keyAt(i)));
        }
    }

    public static Integer A00(String str) {
        return (Integer) A01.get(str);
    }

    public static String A01(String str) {
        if (str.startsWith("/")) {
            return str;
        }
        try {
            return (String) A00.get(Integer.parseInt(str));
        } catch (NumberFormatException unused) {
            return null;
        }
    }
}
