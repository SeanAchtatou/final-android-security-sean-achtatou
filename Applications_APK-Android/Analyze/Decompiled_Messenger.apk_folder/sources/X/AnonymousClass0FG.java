package X;

/* renamed from: X.0FG  reason: invalid class name */
public enum AnonymousClass0FG {
    CONNECT_FAILED,
    CONNECTION_LOST,
    BY_REQUEST,
    DISCONNECTED,
    STALED_CONNECTION,
    PREEMPTIVE_RECONNECT_SUCCESS
}
