package X;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.FrameLayout;
import androidx.fragment.app.Fragment;
import io.card.payment.BuildConfig;
import java.util.ArrayList;

/* renamed from: X.1fl  reason: invalid class name and case insensitive filesystem */
public final class C29031fl extends FrameLayout {
    public boolean A00 = true;
    private ArrayList A01;
    private ArrayList A02;

    public WindowInsets onApplyWindowInsets(WindowInsets windowInsets) {
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).dispatchApplyWindowInsets(new WindowInsets(windowInsets));
        }
        return windowInsets;
    }

    public void removeViews(int i, int i2) {
        for (int i3 = i; i3 < i + i2; i3++) {
            A00(getChildAt(i3));
        }
        super.removeViews(i, i2);
    }

    public void removeViewsInLayout(int i, int i2) {
        for (int i3 = i; i3 < i + i2; i3++) {
            A00(getChildAt(i3));
        }
        super.removeViewsInLayout(i, i2);
    }

    public void dispatchDraw(Canvas canvas) {
        if (this.A00 && this.A01 != null) {
            for (int i = 0; i < this.A01.size(); i++) {
                super.drawChild(canvas, (View) this.A01.get(i), getDrawingTime());
            }
        }
        super.dispatchDraw(canvas);
    }

    public boolean drawChild(Canvas canvas, View view, long j) {
        ArrayList arrayList;
        if (!this.A00 || (arrayList = this.A01) == null || arrayList.size() <= 0 || !this.A01.contains(view)) {
            return super.drawChild(canvas, view, j);
        }
        return false;
    }

    public void endViewTransition(View view) {
        ArrayList arrayList = this.A02;
        if (arrayList != null) {
            arrayList.remove(view);
            ArrayList arrayList2 = this.A01;
            if (arrayList2 != null && arrayList2.remove(view)) {
                this.A00 = true;
            }
        }
        super.endViewTransition(view);
    }

    public void removeDetachedView(View view, boolean z) {
        if (z) {
            A00(view);
        }
        super.removeDetachedView(view, z);
    }

    public void setLayoutTransition(LayoutTransition layoutTransition) {
        if (Build.VERSION.SDK_INT < 18) {
            super.setLayoutTransition(layoutTransition);
            return;
        }
        throw new UnsupportedOperationException("FragmentContainerView does not support Layout Transitions or animateLayoutChanges=\"true\".");
    }

    public C29031fl(Context context, AttributeSet attributeSet, C13060qW r9) {
        super(context, attributeSet);
        String str;
        String classAttribute = attributeSet.getClassAttribute();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass3D3.A01);
        classAttribute = classAttribute == null ? obtainStyledAttributes.getString(0) : classAttribute;
        String string = obtainStyledAttributes.getString(1);
        obtainStyledAttributes.recycle();
        int id = getId();
        Fragment A0O = r9.A0O(id);
        if (classAttribute != null && A0O == null) {
            if (id <= 0) {
                if (string != null) {
                    str = AnonymousClass08S.A0J(" with tag ", string);
                } else {
                    str = BuildConfig.FLAVOR;
                }
                throw new IllegalStateException(AnonymousClass08S.A0P("FragmentContainerView must have an android:id to add Fragment ", classAttribute, str));
            }
            Fragment A012 = r9.A0S().A01(context.getClassLoader(), classAttribute);
            A012.A1K(context, attributeSet, null);
            C16290wo A0T = r9.A0T();
            A0T.A0F = true;
            A012.A0J = this;
            A0T.A0A(getId(), A012, string);
            A0T.A05();
        }
    }

    private void A00(View view) {
        ArrayList arrayList;
        if (view.getAnimation() != null || ((arrayList = this.A02) != null && arrayList.contains(view))) {
            if (this.A01 == null) {
                this.A01 = new ArrayList();
            }
            this.A01.add(view);
        }
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        Fragment fragment;
        Object tag = view.getTag(2131298152);
        if (tag instanceof Fragment) {
            fragment = (Fragment) tag;
        } else {
            fragment = null;
        }
        if (fragment != null) {
            super.addView(view, i, layoutParams);
            return;
        }
        throw new IllegalStateException("Views added to a FragmentContainerView must be associated with a Fragment. View " + view + " is not associated with a Fragment.");
    }

    public boolean addViewInLayout(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        Fragment fragment;
        Object tag = view.getTag(2131298152);
        if (tag instanceof Fragment) {
            fragment = (Fragment) tag;
        } else {
            fragment = null;
        }
        if (fragment != null) {
            return super.addViewInLayout(view, i, layoutParams, z);
        }
        throw new IllegalStateException("Views added to a FragmentContainerView must be associated with a Fragment. View " + view + " is not associated with a Fragment.");
    }

    public void removeAllViewsInLayout() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            A00(getChildAt(childCount));
        }
        super.removeAllViewsInLayout();
    }

    public void removeView(View view) {
        A00(view);
        super.removeView(view);
    }

    public void removeViewAt(int i) {
        A00(getChildAt(i));
        super.removeViewAt(i);
    }

    public void removeViewInLayout(View view) {
        A00(view);
        super.removeViewInLayout(view);
    }

    public void startViewTransition(View view) {
        if (view.getParent() == this) {
            if (this.A02 == null) {
                this.A02 = new ArrayList();
            }
            this.A02.add(view);
        }
        super.startViewTransition(view);
    }
}
