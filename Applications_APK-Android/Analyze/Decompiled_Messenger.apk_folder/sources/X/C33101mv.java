package X;

import com.google.common.base.Preconditions;
import java.util.Comparator;

/* renamed from: X.1mv  reason: invalid class name and case insensitive filesystem */
public final class C33101mv {
    public final Comparator A00;

    public C33101mv(Comparator comparator) {
        Preconditions.checkNotNull(comparator);
        this.A00 = comparator;
    }
}
