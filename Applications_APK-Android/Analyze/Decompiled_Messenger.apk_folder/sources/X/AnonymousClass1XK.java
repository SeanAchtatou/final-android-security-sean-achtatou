package X;

import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.1XK  reason: invalid class name */
public class AnonymousClass1XK implements Iterable {
    public int A00 = 0;
    public C27701dc A01;
    public C27701dc A02;
    public WeakHashMap A03 = new WeakHashMap();

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AnonymousClass1XK) {
            AnonymousClass1XK r7 = (AnonymousClass1XK) obj;
            if (this.A00 == r7.A00) {
                Iterator it = iterator();
                Iterator it2 = r7.iterator();
                while (it.hasNext() && it2.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    Object next = it2.next();
                    if ((entry != null || next == null) && (entry == null || entry.equals(next))) {
                    }
                }
                return !it.hasNext() && !it2.hasNext();
            }
        }
        return false;
    }

    public C27701dc A00(Object obj) {
        if (this instanceof C13000qN) {
            return (C27701dc) ((C13000qN) this).A00.get(obj);
        }
        C27701dc r1 = this.A02;
        while (r1 != null && !r1.A02.equals(obj)) {
            r1 = r1.A00;
        }
        return r1;
    }

    public C27701dc A01(Object obj, Object obj2) {
        C27701dc r1 = new C27701dc(obj, obj2);
        this.A00++;
        C27701dc r0 = this.A01;
        if (r0 == null) {
            this.A02 = r1;
            this.A01 = r1;
            return r1;
        }
        r0.A00 = r1;
        r1.A01 = r0;
        this.A01 = r1;
        return r1;
    }

    public Object A03(Object obj, Object obj2) {
        if (!(this instanceof C13000qN)) {
            C27701dc A002 = A00(obj);
            if (A002 != null) {
                return A002.A03;
            }
            A01(obj, obj2);
            return null;
        }
        C13000qN r2 = (C13000qN) this;
        C27701dc A003 = r2.A00(obj);
        if (A003 != null) {
            return A003.A03;
        }
        r2.A00.put(obj, r2.A01(obj, obj2));
        return null;
    }

    public Iterator iterator() {
        C30363Eup eup = new C30363Eup(this.A02, this.A01);
        this.A03.put(eup, false);
        return eup;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(((Map.Entry) it.next()).toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public Object A02(Object obj) {
        C27701dc A002 = A00(obj);
        if (A002 == null) {
            return null;
        }
        this.A00--;
        if (!this.A03.isEmpty()) {
            for (C17400yq CIX : this.A03.keySet()) {
                CIX.CIX(A002);
            }
        }
        C27701dc r1 = A002.A01;
        if (r1 != null) {
            r1.A00 = A002.A00;
        } else {
            this.A02 = A002.A00;
        }
        C27701dc r0 = A002.A00;
        if (r0 != null) {
            r0.A01 = r1;
        } else {
            this.A01 = r1;
        }
        A002.A00 = null;
        A002.A01 = null;
        return A002.A03;
    }

    public int hashCode() {
        Iterator it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((Map.Entry) it.next()).hashCode();
        }
        return i;
    }
}
