package X;

/* renamed from: X.0hU  reason: invalid class name and case insensitive filesystem */
public enum C09510hU {
    PREFER_CACHE_IF_UP_TO_DATE,
    CHECK_SERVER_FOR_NEW_DATA,
    DO_NOT_CHECK_SERVER,
    STALE_DATA_OKAY
}
