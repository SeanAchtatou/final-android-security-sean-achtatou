package X;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.fragment.app.Fragment;
import com.facebook.acra.ACRA;
import com.facebook.common.util.TriState;
import com.facebook.forker.Process;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0wL  reason: invalid class name and case insensitive filesystem */
public final class C16010wL extends C13460rT {
    public static final String __redex_internal_original_name = "com.facebook.messaging.search.M4SearchHostFragment";
    public AnonymousClass0UN A00;
    public C34791qB A01;
    public C72573eW A02;
    public C138146cd A03;
    public C15750vt A04;
    private C73493gD A05;
    private boolean A06;
    private boolean A07;
    private final C147146sb A08 = new C147146sb(this);
    private final C17100yK A09 = new C131316Cw(this);

    public static C73493gD A00(C16010wL r2) {
        if (r2.A05 == null) {
            r2.A05 = (C73493gD) AnonymousClass1XX.A03(AnonymousClass1Y3.Arm, r2.A00);
        }
        return r2.A05;
    }

    public static /* synthetic */ String A04() {
        switch (AnonymousClass07B.A00.intValue()) {
            case 1:
                return "home_nested";
            case 2:
                return "groups_tab";
            case 3:
                return "montage_tab";
            case 4:
                return "people";
            case 5:
                return "settings";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return C99084oO.$const$string(AnonymousClass1Y3.A1F);
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "games";
            case 8:
                return "active_now";
            case Process.SIGKILL:
                return C99084oO.$const$string(AnonymousClass1Y3.A19);
            case AnonymousClass1Y3.A01 /*10*/:
                return "workchat_bot";
            default:
                return "thread_list";
        }
    }

    private void A05() {
        C72573eW r2 = this.A02;
        if (r2 != null && this.A04 != null) {
            r2.A0h.A00 = new C138086cX(this);
            r2.A0B = new C131326Cx(this);
            r2.A0C = new AnonymousClass2JC(this);
            C83803yE r4 = (C83803yE) AnonymousClass1XX.A03(AnonymousClass1Y3.BFS, this.A00);
            C15750vt r9 = this.A04;
            C138206ck r10 = new C138206ck(this, r2);
            InputMethodManager A0j = C04490Ux.A0j(r4);
            AnonymousClass0UU.A08(r4);
            this.A03 = new C138146cd(r4, A0j, AnonymousClass0UQ.A00(AnonymousClass1Y3.BJS, r4), C14220so.A00(r4), AnonymousClass067.A02(), r9, r10);
            r2.A0D = new C138156ce(this);
            r2.A0E = new C131336Cy(this);
            ((C17070yH) AnonymousClass1XX.A03(AnonymousClass1Y3.AqE, this.A00)).A01(this, this.A09);
        }
    }

    public static void A06(C16010wL r3) {
        View view = r3.A0I;
        if (view != null) {
            C15980wI.A00(view, ((MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, r3.A00)).B9m());
        }
    }

    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int A022 = C000700l.A02(982167066);
        Context context = layoutInflater.getContext();
        ViewStubCompat viewStubCompat = new ViewStubCompat(context, null);
        viewStubCompat.A02 = layoutInflater;
        this.A04 = C15750vt.A00(viewStubCompat);
        AnonymousClass16W A012 = C13950sM.A01(context);
        C13520ra A002 = C13950sM.A00(context);
        A002.A07(1);
        A002.A04(this.A04.A02());
        AnonymousClass16W A013 = C13950sM.A01(context);
        A013.A02(2131300410);
        A013.A03(-1, -1);
        A002.A06(A013);
        A012.A06(A002);
        AnonymousClass16W A014 = C13950sM.A01(context);
        A014.A02(2131297751);
        A014.A03(-1, -1);
        A012.A06(A014);
        AnonymousClass16W A015 = C13950sM.A01(context);
        A015.A02(2131296988);
        A015.A03(-1, -1);
        A012.A06(A015);
        View view = A012.A00;
        C000700l.A08(-193160368, A022);
        return view;
    }

    public void A1o() {
        int A022 = C000700l.A02(-948783415);
        super.A1o();
        C138146cd r2 = this.A03;
        r2.A05 = false;
        C138146cd.A02(r2);
        if (!r2.A04) {
            r2.A0A.A01.A2U(r2.A03);
        }
        C000700l.A08(57489687, A022);
    }

    public void A1p() {
        int A022 = C000700l.A02(1706354522);
        super.A1p();
        C138146cd r1 = this.A03;
        r1.A05 = true;
        C138146cd.A03(r1);
        C000700l.A08(-1866007604, A022);
    }

    public void A1q() {
        int A022 = C000700l.A02(687137628);
        super.A1q();
        if (!this.A06) {
            this.A03.A05(this.A07);
            this.A06 = true;
            this.A07 = true;
        }
        C000700l.A08(86115323, A022);
    }

    public void A1u(Bundle bundle) {
        super.A1u(bundle);
        C138146cd r3 = this.A03;
        bundle.putLong("search_pt", r3.A00);
        bundle.putCharSequence("search_request", r3.A03);
        bundle.putSerializable("search_last_open", TriState.valueOf(true));
        bundle.putBoolean("is_reopen", this.A07);
    }

    public void A1v(View view, Bundle bundle) {
        super.A1v(view, bundle);
        if (A17().A0O(2131300410) == null) {
            C16290wo A0T = A17().A0T();
            A0T.A0A(2131300410, new C72573eW(), "tabbed_search_fragment_tag");
            A0T.A02();
        }
        A06(this);
        A05();
        C138146cd r2 = this.A03;
        if (r2 != null && bundle != null) {
            r2.A00 = bundle.getLong("search_pt");
            r2.A03 = bundle.getCharSequence("search_request");
            TriState triState = (TriState) bundle.getSerializable("search_last_open");
            r2.A01 = triState;
            if (triState == null) {
                r2.A01 = TriState.UNSET;
            }
        }
    }

    public void A1w(Fragment fragment) {
        super.A1w(fragment);
        if (fragment instanceof C72573eW) {
            this.A02 = (C72573eW) fragment;
            A05();
        } else if (fragment instanceof C146666rp) {
            ((C146666rp) fragment).A01 = this.A08;
        }
    }

    public void A1x(boolean z) {
        super.A1x(z);
        this.A03.A04(z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0021, code lost:
        if (r4.getBoolean("is_reopen") == false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2M(android.os.Bundle r4) {
        /*
            r3 = this;
            super.A2M(r4)
            android.content.Context r0 = r3.A1j()
            X.1XX r2 = X.AnonymousClass1XX.get(r0)
            X.0UN r1 = new X.0UN
            r0 = 0
            r1.<init>(r0, r2)
            r3.A00 = r1
            X.6cl r0 = new X.6cl
            r0.<init>(r2)
            if (r4 == 0) goto L_0x0023
            java.lang.String r0 = "is_reopen"
            boolean r1 = r4.getBoolean(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            r3.A07 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16010wL.A2M(android.os.Bundle):void");
    }
}
