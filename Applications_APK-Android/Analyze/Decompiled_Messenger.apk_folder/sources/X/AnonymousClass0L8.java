package X;

/* renamed from: X.0L8  reason: invalid class name */
public final class AnonymousClass0L8 {
    private C17610zB A00;

    public void A00() {
        this.A00.A09();
    }

    public void A01() {
        this.A00.A01();
    }

    public void A02(long j) {
        this.A00.A0D(j, this);
    }

    public boolean A03() {
        if (this.A00.A01() == 0) {
            return true;
        }
        return false;
    }

    public boolean A04(long j) {
        if (this.A00.A02(j) >= 0) {
            return true;
        }
        return false;
    }

    public long[] A05() {
        C17610zB r5 = this.A00;
        int A01 = r5.A01();
        long[] jArr = new long[A01];
        for (int i = 0; i < A01; i++) {
            jArr[i] = r5.A04(i);
        }
        return jArr;
    }

    public AnonymousClass0L8() {
        this(new C17610zB());
    }

    public AnonymousClass0L8(int i) {
        this(new C17610zB(i));
    }

    private AnonymousClass0L8(C17610zB r1) {
        this.A00 = r1;
    }
}
