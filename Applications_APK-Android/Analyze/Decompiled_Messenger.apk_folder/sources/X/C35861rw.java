package X;

/* renamed from: X.1rw  reason: invalid class name and case insensitive filesystem */
public enum C35861rw {
    DEFAULT(0),
    END_CARD(1);
    
    public final int value;

    private C35861rw(int i) {
        this.value = i;
    }
}
