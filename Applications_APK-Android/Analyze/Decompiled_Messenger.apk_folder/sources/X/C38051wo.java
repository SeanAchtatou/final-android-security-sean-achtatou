package X;

/* renamed from: X.1wo  reason: invalid class name and case insensitive filesystem */
public final class C38051wo extends Exception {
    public final int rendererIndex;
    public final int type;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C38051wo(int r1, java.lang.String r2, java.lang.Throwable r3, int r4) {
        /*
            r0 = this;
            if (r2 != 0) goto L_0x0005
            if (r3 != 0) goto L_0x000d
            r2 = 0
        L_0x0005:
            r0.<init>(r2, r3)
            r0.type = r1
            r0.rendererIndex = r4
            return
        L_0x000d:
            java.lang.String r2 = r3.getMessage()
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38051wo.<init>(int, java.lang.String, java.lang.Throwable, int):void");
    }

    public static C38051wo A00(Exception exc, int i) {
        return new C38051wo(1, null, exc, i);
    }
}
