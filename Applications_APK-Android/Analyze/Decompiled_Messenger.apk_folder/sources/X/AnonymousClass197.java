package X;

/* renamed from: X.197  reason: invalid class name */
public final class AnonymousClass197 {
    public static final AnonymousClass19A A00 = new AnonymousClass19A("master_key_version");
    public static final AnonymousClass19A A01 = new AnonymousClass19A("master_key_entity_probe");
    public static final AnonymousClass19A A02 = new AnonymousClass19A("master_key_probe");
    public static final AnonymousClass19A A03 = new AnonymousClass19A("master_key_v1");
    public static final AnonymousClass19A A04 = new AnonymousClass19A("master_key_v2");
    public static final AnonymousClass19A A05 = new AnonymousClass19A("master_key_v3");
    public static final AnonymousClass19A A06 = new AnonymousClass19A("next_pkid");
    public static final AnonymousClass19A A07 = new AnonymousClass19A("next_spkid");

    static {
        new AnonymousClass19A("account_key_user_id");
    }
}
