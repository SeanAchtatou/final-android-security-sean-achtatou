package X;

import android.database.Cursor;

/* renamed from: X.142  reason: invalid class name */
public final class AnonymousClass142 implements C28381ei {
    private final String A00;

    public /* bridge */ /* synthetic */ C28411el AV7(Cursor cursor) {
        return new C28391ej(cursor);
    }

    public Object[] AxB() {
        return new Object[]{C22298Ase.$const$string(AnonymousClass1Y3.A2b), new String[]{"_id", "hash"}, "table_name = ?", new String[]{String.valueOf(this.A00)}, null, null, null};
    }

    public AnonymousClass142(String str) {
        this.A00 = str;
    }
}
