package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0YN  reason: invalid class name */
public final class AnonymousClass0YN implements AnonymousClass0Z1 {
    private static final Comparator A02 = new AnonymousClass0Z3();
    private static volatile AnonymousClass0YN A03;
    private AnonymousClass0UN A00;
    private final C04310Tq A01;

    public String Amj() {
        return "network_log";
    }

    public static final AnonymousClass0YN A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass0YN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass0YN(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public String getCustomData(Throwable th) {
        StringBuilder sb = new StringBuilder();
        ArrayList A002 = C04300To.A00();
        for (C09320h0 AlV : (Set) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BHx, this.A00)) {
            A002.addAll(AlV.AlV());
        }
        Collections.sort(A002, A02);
        Iterator it = A002.iterator();
        while (it.hasNext()) {
            C13330rF r3 = (C13330rF) it.next();
            sb.append(StringFormatUtil.formatStrLocaleSafe("[%s] %s%n", Long.valueOf(r3.getStartTime()), r3.Aae()));
        }
        sb.append(AnonymousClass0Z4.A00((AnonymousClass0Z4) this.A01.get()).Aqa());
        return sb.toString();
    }

    private AnonymousClass0YN(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = AnonymousClass0VB.A00(AnonymousClass1Y3.AAv, r3);
    }
}
