package X;

import com.google.common.base.Objects;
import com.google.common.base.Platform;
import io.card.payment.BuildConfig;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.0kP  reason: invalid class name and case insensitive filesystem */
public final class C10560kP {
    public static final Pattern A03;
    public boolean A00;
    public final String A01;
    public final Matcher A02;

    static {
        StringBuilder sb = new StringBuilder(BuildConfig.FLAVOR);
        boolean z = true;
        for (C09640iE r1 : C09640iE.values()) {
            if (z) {
                z = false;
            } else {
                sb.append("|");
            }
            sb.append(r1.mValue);
        }
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder(BuildConfig.FLAVOR);
        boolean z2 = true;
        for (Integer num : AnonymousClass07B.A00(3)) {
            if (num != AnonymousClass07B.A00) {
                if (z2) {
                    z2 = false;
                } else {
                    sb3.append("|");
                }
                sb3.append(C10570kQ.A00(num));
            }
        }
        String sb4 = sb3.toString();
        String str = "(?:\\.(";
        StringBuilder sb5 = new StringBuilder(BuildConfig.FLAVOR);
        boolean z3 = true;
        for (Integer num2 : AnonymousClass07B.A00(4)) {
            if (num2 != AnonymousClass07B.A00) {
                if (z3) {
                    z3 = false;
                } else {
                    sb5.append("|");
                }
                sb5.append(AnonymousClass15J.A00(num2));
            }
        }
        A03 = Pattern.compile(AnonymousClass08S.A0W("([a-z]+(?:_[A-Z]+)?)-([0-9]+)(?:-(\\w+))?\\.(", sb2, ")", "(?:\\.(", sb4, "))?", str, sb5.toString(), "))?"));
    }

    public static String A01(String str, long j, String str2, Integer num) {
        String str3 = str + "-" + j + "-" + str2 + "." + C09640iE.LANGPACK.mValue;
        if (num != AnonymousClass07B.A00) {
            return AnonymousClass08S.A0P(str3, ".", C10570kQ.A00(num));
        }
        return str3;
    }

    public int A03() {
        if (!this.A02.matches()) {
            return 0;
        }
        return Integer.parseInt(this.A02.group(2));
    }

    public C09640iE A04() {
        if (!this.A02.matches()) {
            return null;
        }
        String group = this.A02.group(4);
        if (group != null) {
            for (C09640iE r1 : C09640iE.values()) {
                if (r1.mValue.equals(group)) {
                    return r1;
                }
            }
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Unrecognized language file format : ", group));
    }

    public Integer A08() {
        if (!this.A02.matches()) {
            return null;
        }
        String group = this.A02.group(5);
        if (Platform.stringIsNullOrEmpty(group)) {
            return AnonymousClass07B.A00;
        }
        for (Integer num : AnonymousClass07B.A00(3)) {
            if (C10570kQ.A00(num).equals(group)) {
                return num;
            }
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Unrecognized language file annotation : ", group));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equal(this.A01, ((C10560kP) obj).A01);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        if (r4 != null) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C10560kP(java.lang.String r7) {
        /*
            r6 = this;
            r6.<init>()
            r6.A01 = r7
            java.util.regex.Pattern r0 = X.C10560kP.A03
            java.util.regex.Matcher r0 = r0.matcher(r7)
            r6.A02 = r0
            r0.matches()
            r5 = 0
            X.0iE r2 = r6.A04()     // Catch:{ Exception -> 0x0052 }
            X.0iE r1 = X.C09640iE.LANGPACK     // Catch:{ Exception -> 0x0052 }
            r0 = 0
            if (r2 != r1) goto L_0x001b
            r0 = 1
        L_0x001b:
            if (r0 == 0) goto L_0x004e
            boolean r0 = r6.A0A()     // Catch:{ Exception -> 0x0052 }
            if (r0 == 0) goto L_0x004e
            java.lang.Integer r0 = r6.A07()     // Catch:{ Exception -> 0x0052 }
            if (r0 == 0) goto L_0x004e
            java.lang.Integer r1 = r6.A07()     // Catch:{ Exception -> 0x0052 }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ Exception -> 0x0052 }
            if (r1 == r0) goto L_0x004e
            java.lang.String[] r4 = A02(r6)     // Catch:{ Exception -> 0x0052 }
            r3 = 0
            if (r4 == 0) goto L_0x004a
            int r2 = r4.length     // Catch:{ Exception -> 0x0052 }
            r0 = 2
            if (r2 != r0) goto L_0x004a
            r1 = 0
        L_0x003d:
            if (r1 >= r2) goto L_0x004b
            r0 = r4[r1]     // Catch:{ Exception -> 0x0052 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x0052 }
            if (r0 == 0) goto L_0x004a
            int r1 = r1 + 1
            goto L_0x003d
        L_0x004a:
            r4 = r3
        L_0x004b:
            r0 = 1
            if (r4 != 0) goto L_0x004f
        L_0x004e:
            r0 = 0
        L_0x004f:
            r6.A00 = r0     // Catch:{ Exception -> 0x0052 }
            return
        L_0x0052:
            r6.A00 = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10560kP.<init>(java.lang.String):void");
    }

    public static C10560kP A00(C10560kP r7, Integer num) {
        if (!r7.A0A()) {
            return null;
        }
        String str = r7.A01;
        Integer num2 = AnonymousClass07B.A0C;
        return new C10560kP(str.replaceAll(AnonymousClass08S.A0P("\\.", C10570kQ.A00(num2), ".*$"), AnonymousClass08S.A0S(".", C10570kQ.A00(num2), ".", AnonymousClass15J.A00(num))));
    }

    public static String[] A02(C10560kP r2) {
        if (!r2.A0A() || r2.A02.group(3) == null) {
            return null;
        }
        return r2.A02.group(3).split("_");
    }

    public C10560kP A05() {
        String str;
        String replaceAll;
        String[] A022 = A02(this);
        if (!this.A00 || A022 == null) {
            str = null;
        } else {
            str = A022[1];
        }
        if (str == null) {
            replaceAll = null;
        } else {
            replaceAll = this.A01.replaceAll(this.A02.group(3), str).replaceAll(AnonymousClass08S.A0P("\\.", C10570kQ.A00(AnonymousClass07B.A0C), ".*$"), AnonymousClass08S.A0J(".", C10570kQ.A00(AnonymousClass07B.A01)));
        }
        if (replaceAll == null) {
            return null;
        }
        return new C10560kP(replaceAll);
    }

    public C10560kP A06() {
        String str;
        String replaceAll;
        String[] A022 = A02(this);
        if (!this.A00 || A022 == null) {
            str = null;
        } else {
            str = A022[0];
        }
        if (str == null) {
            replaceAll = null;
        } else {
            replaceAll = this.A01.replaceAll(this.A02.group(3), str).replaceAll(AnonymousClass08S.A0P("\\.", C10570kQ.A00(AnonymousClass07B.A0C), ".*$"), BuildConfig.FLAVOR);
        }
        if (replaceAll == null) {
            return null;
        }
        return new C10560kP(replaceAll);
    }

    public Integer A07() {
        if (!A0A()) {
            return null;
        }
        String group = this.A02.group(6);
        if (Platform.stringIsNullOrEmpty(group)) {
            return AnonymousClass07B.A00;
        }
        for (Integer num : AnonymousClass07B.A00(4)) {
            if (AnonymousClass15J.A00(num).equals(group)) {
                return num;
            }
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0J("Unrecognized delta file annotation : ", group));
    }

    public String A09() {
        if (A08() != AnonymousClass07B.A0C) {
            return this.A02.group(3);
        }
        return null;
    }

    public boolean A0A() {
        if (A08() == AnonymousClass07B.A0C) {
            return true;
        }
        return false;
    }
}
