package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.ui.name.ThreadNameViewData;

/* renamed from: X.1Kh  reason: invalid class name and case insensitive filesystem */
public final class C22201Kh implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadNameViewData(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadNameViewData[i];
    }
}
