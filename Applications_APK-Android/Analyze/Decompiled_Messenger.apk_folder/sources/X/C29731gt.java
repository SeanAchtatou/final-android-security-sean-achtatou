package X;

import com.facebook.user.profilepic.PicSquareUrlWithSize;
import java.util.Comparator;

/* renamed from: X.1gt  reason: invalid class name and case insensitive filesystem */
public final class C29731gt implements Comparator {
    public int compare(Object obj, Object obj2) {
        return ((PicSquareUrlWithSize) obj).size - ((PicSquareUrlWithSize) obj2).size;
    }
}
