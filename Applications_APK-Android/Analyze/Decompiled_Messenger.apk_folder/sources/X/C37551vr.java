package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.customthreads.model.ThreadThemeReactionAssetInfo;

/* renamed from: X.1vr  reason: invalid class name and case insensitive filesystem */
public final class C37551vr implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadThemeReactionAssetInfo(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadThemeReactionAssetInfo[i];
    }
}
