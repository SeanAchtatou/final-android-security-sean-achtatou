package X;

/* renamed from: X.0uP  reason: invalid class name and case insensitive filesystem */
public enum C14950uP {
    FLEX_START(0),
    CENTER(1),
    FLEX_END(2),
    SPACE_BETWEEN(3),
    SPACE_AROUND(4),
    SPACE_EVENLY(5);
    
    public final int mIntValue;

    private C14950uP(int i) {
        this.mIntValue = i;
    }
}
