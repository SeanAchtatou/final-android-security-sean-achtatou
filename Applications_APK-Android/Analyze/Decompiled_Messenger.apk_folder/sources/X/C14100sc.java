package X;

import android.net.Uri;
import java.io.Serializable;

/* renamed from: X.0sc  reason: invalid class name and case insensitive filesystem */
public final class C14100sc implements Serializable {
    private static final long serialVersionUID = 1;
    public String mChannelId;
    public String mGroupId;
    public int mImportance;
    public C15120ul mLight;
    public String mName;
    public AnonymousClass16F mNotifyVibrate;
    public boolean mShouldVibrate;
    public boolean mShowBadge = true;
    public String mSoundUri;

    public C14100sc(String str, String str2, int i, C15120ul r5, boolean z, AnonymousClass16F r7, Uri uri, String str3) {
        String str4;
        this.mChannelId = str;
        this.mName = str2;
        this.mImportance = i;
        this.mLight = r5;
        this.mShouldVibrate = z;
        this.mNotifyVibrate = r7;
        if (uri != null) {
            str4 = uri.toString();
        } else {
            str4 = null;
        }
        this.mSoundUri = str4;
        this.mGroupId = str3;
    }
}
