package X;

import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1To  reason: invalid class name and case insensitive filesystem */
public final class C24421To {
    private static volatile C24421To A02;
    public Map A00 = AnonymousClass0TG.A04();
    public final AnonymousClass0lQ A01;

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001b  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C24421To r4, android.content.ComponentName r5, android.os.IBinder r6) {
        /*
            monitor-enter(r4)
            monitor-enter(r4)     // Catch:{ all -> 0x0046 }
            java.util.Map r0 = r4.A00     // Catch:{ all -> 0x0043 }
            java.lang.Object r3 = r0.get(r5)     // Catch:{ all -> 0x0043 }
            X.1nW r3 = (X.C33401nW) r3     // Catch:{ all -> 0x0043 }
            monitor-exit(r4)     // Catch:{ all -> 0x0046 }
            if (r3 == 0) goto L_0x000e
            goto L_0x0010
        L_0x000e:
            r0 = 0
            goto L_0x0018
        L_0x0010:
            r3.A00 = r6     // Catch:{ all -> 0x0046 }
            java.util.Set r0 = r3.A04     // Catch:{ all -> 0x0046 }
            com.google.common.collect.ImmutableSet r0 = com.google.common.collect.ImmutableSet.A0A(r0)     // Catch:{ all -> 0x0046 }
        L_0x0018:
            monitor-exit(r4)     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0042
            java.util.Iterator r2 = r0.iterator()
        L_0x001f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0042
            java.lang.Object r1 = r2.next()
            android.content.ServiceConnection r1 = (android.content.ServiceConnection) r1
            monitor-enter(r4)
            java.util.Set r0 = r3.A04     // Catch:{ all -> 0x003f }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x003f }
            monitor-exit(r4)
            if (r0 == 0) goto L_0x001f
            if (r6 == 0) goto L_0x003b
            r1.onServiceConnected(r5, r6)
            goto L_0x001f
        L_0x003b:
            r1.onServiceDisconnected(r5)
            goto L_0x001f
        L_0x003f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0042:
            return
        L_0x0043:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0046 }
            throw r0     // Catch:{ all -> 0x0046 }
        L_0x0046:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0046 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24421To.A01(X.1To, android.content.ComponentName, android.os.IBinder):void");
    }

    public static final C24421To A00(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (C24421To.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        A02 = new C24421To(new AnonymousClass0lQ(AnonymousClass1YA.A00(r5.getApplicationInjector())));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C24421To(AnonymousClass0lQ r2) {
        this.A01 = r2;
    }
}
