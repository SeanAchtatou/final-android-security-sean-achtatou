package X;

import com.facebook.acra.ActionId;

/* renamed from: X.0gE  reason: invalid class name and case insensitive filesystem */
public final class C08940gE implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.analytics.perf.MessagingPerformanceLogger$3";
    public final /* synthetic */ C08770fv A00;

    public C08940gE(C08770fv r1) {
        this.A00 = r1;
    }

    public void run() {
        if (this.A00.A05.isMarkerOn(5505203)) {
            this.A00.A05.endAllInstancesOfMarker(5505203, ActionId.TIMEOUT);
        }
    }
}
