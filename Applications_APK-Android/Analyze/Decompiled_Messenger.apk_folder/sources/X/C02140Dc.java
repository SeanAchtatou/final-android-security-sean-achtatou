package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0Dc  reason: invalid class name and case insensitive filesystem */
public final class C02140Dc extends C03160Kg {
    public long A00() {
        return 4345974300167284411L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        C02570Fl r32 = (C02570Fl) r3;
        dataOutput.writeLong(r32.realtimeMs);
        dataOutput.writeLong(r32.uptimeMs);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        C02570Fl r32 = (C02570Fl) r3;
        r32.realtimeMs = dataInput.readLong();
        r32.uptimeMs = dataInput.readLong();
        return true;
    }
}
