package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.DeadObjectException;
import android.text.TextUtils;

/* renamed from: X.0Aw  reason: invalid class name and case insensitive filesystem */
public final class C01600Aw {
    private static C01600Aw A03;
    public final String A00;
    public final String A01;
    public final boolean A02;

    public static synchronized C01600Aw A00(Context context) {
        C01600Aw r0;
        synchronized (C01600Aw.class) {
            if (A03 == null) {
                A03 = new C01600Aw(context);
            }
            r0 = A03;
        }
        return r0;
    }

    private C01600Aw(Context context) {
        ApplicationInfo applicationInfo;
        boolean z = false;
        AnonymousClass0AC A002 = AnonymousClass0AB.A00(context, context.getPackageName(), 0, C009207y.A01);
        String str = "1";
        String str2 = "unknown";
        if (A002.A01 != null) {
            this.A01 = !TextUtils.isEmpty(A002.A01.versionName) ? A002.A01.versionName : str2;
            this.A00 = A002.A01.versionCode > 0 ? String.valueOf(A002.A01.versionCode) : str;
        } else {
            this.A01 = str2;
            this.A00 = str;
        }
        C009207y r1 = C009207y.A01;
        try {
            applicationInfo = context.getApplicationInfo();
        } catch (RuntimeException e) {
            C010708t.A0R("RtiGracefulSystemMethodHelper", e, "Failed to getApplicationInfo");
            AnonymousClass09P r12 = r1.A00;
            if (r12 != null) {
                r12.softReport("RtiGracefulSystemMethodHelper", "getApplicationInfo", e);
            }
            if (e.getCause() instanceof DeadObjectException) {
                applicationInfo = null;
            } else {
                throw e;
            }
        }
        if (!(applicationInfo == null || (applicationInfo.flags & 2) == 0)) {
            z = true;
        }
        this.A02 = z;
    }
}
