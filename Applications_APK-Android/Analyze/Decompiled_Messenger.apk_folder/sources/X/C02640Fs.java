package X;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0Fs  reason: invalid class name and case insensitive filesystem */
public final class C02640Fs extends C007907e {
    private static C02640Fs A03;
    public C02650Ft A00;
    public boolean A01 = false;
    private final AtomicLong A02 = new AtomicLong();

    public static synchronized C02640Fs A00() {
        C02640Fs r0;
        synchronized (C02640Fs.class) {
            if (A03 == null) {
                A03 = new C02640Fs();
            }
            r0 = A03;
        }
        return r0;
    }

    public AnonymousClass0FM A03() {
        return new C02630Fr();
    }

    public boolean A04(AnonymousClass0FM r5) {
        C02630Fr r52 = (C02630Fr) r5;
        synchronized (this) {
            if (r52 == null) {
                throw new IllegalArgumentException("Null value passed to getSnapshot!");
            } else if (!this.A01) {
                return false;
            } else {
                r52.acraActiveRadioTimeS = (int) ((long) ((int) ((this.A00.A02.get() & 4294901760L) >> 16)));
                r52.acraTailRadioTimeS = (int) ((long) ((int) (this.A00.A02.get() & 65535)));
                r52.acraRadioWakeupCount = (int) ((long) this.A00.A01.get());
                r52.acraTxBytes = this.A02.get();
                return true;
            }
        }
    }

    public void A05(long j, long j2, int i) {
        long j3;
        long j4;
        long j5;
        C05520Zg.A02(this.A00);
        C02650Ft r12 = this.A00;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long seconds = timeUnit.toSeconds(j);
        long seconds2 = timeUnit.toSeconds(j2);
        boolean z = false;
        do {
            long j6 = r12.A02.get();
            long j7 = seconds;
            long j8 = seconds2;
            j3 = j6 >> 32;
            long j9 = (long) r12.A00;
            long j10 = j3 - j9;
            if (seconds2 > j10) {
                j4 = 1;
            } else {
                j4 = 0;
            }
            long max = Math.max(j7, j10);
            long max2 = Math.max(j8, j10);
            long j11 = (long) ((int) ((j6 & 4294901760L) >> 16));
            long j12 = (long) ((int) (j6 & 65535));
            long max3 = Math.max(max2 - max, j4);
            if (max >= j3) {
                j5 = j9;
            } else if (max2 < j3) {
                j5 = (j9 - max3) - (j3 - max2);
            } else {
                j5 = j9 - (j3 - max);
            }
            long j13 = ((max2 + j9) << 32) | ((j11 + max3) << 16) | (j12 + j5);
            long j14 = j13;
            if (j3 >= (j13 >> 32)) {
                break;
            }
            z = r12.A02.compareAndSet(j6, j14);
        } while (!z);
        if (z && j3 <= seconds) {
            r12.A01.getAndIncrement();
        }
        this.A02.getAndAdd((long) i);
    }
}
