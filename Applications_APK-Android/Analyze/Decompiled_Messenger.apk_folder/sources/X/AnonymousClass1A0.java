package X;

import com.facebook.litho.LithoView;

/* renamed from: X.1A0  reason: invalid class name */
public final class AnonymousClass1A0 extends C20831Dz {
    public final /* synthetic */ AnonymousClass1Ri A00;

    public AnonymousClass1A0(AnonymousClass1Ri r2) {
        this.A00 = r2;
        A09(r2.A0k);
    }

    public void A0D(C33781o8 r4) {
        AnonymousClass1Qz r42 = (AnonymousClass1Qz) r4;
        if (r42.A01) {
            LithoView lithoView = (LithoView) r42.A0H;
            lithoView.A0V();
            lithoView.A0b(null);
            lithoView.A0d(null);
            lithoView.A05 = null;
            return;
        }
        C78893ph r1 = r42.A00;
        if (r1 != null) {
            r1.CJY(r42.A0H);
            r42.A00 = null;
        }
    }

    public int ArU() {
        int size = this.A00.A0h.size();
        if (!this.A00.A0n || size <= 0) {
            return size;
        }
        return Integer.MAX_VALUE;
    }
}
