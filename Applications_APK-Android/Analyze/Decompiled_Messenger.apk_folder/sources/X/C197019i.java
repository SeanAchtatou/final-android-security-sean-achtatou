package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.19i  reason: invalid class name and case insensitive filesystem */
public final class C197019i extends AnonymousClass0W4 {
    public static final ImmutableList A00;
    public static final String[] A01;
    private static final C06060am A02;

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    static {
        AnonymousClass0W6 r5 = C197619o.A00;
        String str = r5.A00;
        AnonymousClass0W6 r3 = C197619o.A02;
        String str2 = r3.A00;
        AnonymousClass0W6 r2 = C197619o.A01;
        A01 = new String[]{str, str2, r2.A00};
        A02 = new C06050al(ImmutableList.of(r5));
        A00 = ImmutableList.of(r5, r3, r2);
    }

    public C197019i() {
        super("other_devices", A00, A02);
    }
}
