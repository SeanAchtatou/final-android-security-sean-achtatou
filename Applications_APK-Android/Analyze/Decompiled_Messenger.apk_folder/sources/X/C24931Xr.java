package X;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1Xr  reason: invalid class name and case insensitive filesystem */
public final class C24931Xr {
    public static C24961Xu A06(Object[] objArr, int i, int i2, int i3) {
        boolean z = false;
        if (i2 >= 0) {
            z = true;
        }
        Preconditions.checkArgument(z);
        Preconditions.checkPositionIndexes(i, i + i2, objArr.length);
        Preconditions.checkPositionIndex(i3, i2);
        if (i2 == 0) {
            return C24941Xs.A02;
        }
        return new C24941Xs(objArr, i, i2, i3);
    }

    public static int A00(Iterator it) {
        long j = 0;
        while (it.hasNext()) {
            it.next();
            j++;
        }
        return C06950cM.A01(j);
    }

    public static int A02(Iterator it, Predicate predicate) {
        Preconditions.checkNotNull(predicate, "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (predicate.apply(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static AnonymousClass191 A03(Iterator it) {
        if (it instanceof C10860ky) {
            return (C10860ky) it;
        }
        return new C10860ky(it);
    }

    public static int A01(Iterator it, int i) {
        Preconditions.checkNotNull(it);
        int i2 = 0;
        boolean z = false;
        if (i >= 0) {
            z = true;
        }
        Preconditions.checkArgument(z, "numberToAdvance must be nonnegative");
        while (i2 < i && it.hasNext()) {
            it.next();
            i2++;
        }
        return i2;
    }

    public static C24971Xv A04(Iterator it) {
        Preconditions.checkNotNull(it);
        if (it instanceof C24971Xv) {
            return (C24971Xv) it;
        }
        return new C21271Fx(it);
    }

    public static C24971Xv A05(Iterator it, Predicate predicate) {
        Preconditions.checkNotNull(it);
        Preconditions.checkNotNull(predicate);
        return new AnonymousClass1Y0(it, predicate);
    }

    public static Object A07(Iterator it) {
        if (!it.hasNext()) {
            return null;
        }
        Object next = it.next();
        it.remove();
        return next;
    }

    public static void A08(Iterator it) {
        Preconditions.checkNotNull(it);
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    public static boolean A09(Collection collection, Iterator it) {
        Preconditions.checkNotNull(collection);
        Preconditions.checkNotNull(it);
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0006  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0A(java.util.Iterator r3, java.util.Iterator r4) {
        /*
        L_0x0000:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x001c
            boolean r0 = r4.hasNext()
            r2 = 0
            if (r0 == 0) goto L_0x001b
            java.lang.Object r1 = r3.next()
            java.lang.Object r0 = r4.next()
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)
            if (r0 != 0) goto L_0x0000
        L_0x001b:
            return r2
        L_0x001c:
            boolean r0 = r4.hasNext()
            r0 = r0 ^ 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24931Xr.A0A(java.util.Iterator, java.util.Iterator):boolean");
    }
}
