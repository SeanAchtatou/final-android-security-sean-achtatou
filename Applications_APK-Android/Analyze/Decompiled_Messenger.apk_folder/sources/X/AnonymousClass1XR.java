package X;

import android.util.Log;

/* renamed from: X.1XR  reason: invalid class name */
public final class AnonymousClass1XR implements AnonymousClass04e {
    public void C2S(String str) {
        Log.e("Security-LocalReporter", str);
    }

    public void C2T(String str, String str2, Throwable th) {
        StringBuilder sb = new StringBuilder("category=");
        sb.append(str);
        sb.append(", message=");
        sb.append(str2);
        if (th != null) {
            sb.append(", cause=");
            sb.append(th.toString());
        }
        C2S(sb.toString());
    }
}
