package X;

import android.content.UriMatcher;
import android.net.Uri;
import java.util.Map;

/* renamed from: X.0TF  reason: invalid class name */
public final class AnonymousClass0TF {
    private int A00 = 1;
    private final UriMatcher A01 = new UriMatcher(-1);
    private final Map A02 = AnonymousClass0TG.A04();

    public AnonymousClass0j5 A00(Uri uri) {
        int match = this.A01.match(uri);
        if (match != -1) {
            AnonymousClass0j5 r0 = (AnonymousClass0j5) this.A02.get(Integer.valueOf(match));
            if (r0 != null) {
                return r0;
            }
            throw new IllegalStateException("Table is null?");
        }
        throw new IllegalArgumentException("Unknown URI " + uri);
    }

    public void A01(String str, String str2, AnonymousClass0j5 r6) {
        int i = this.A00;
        this.A00 = i + 1;
        this.A01.addURI(str, str2, i);
        this.A02.put(Integer.valueOf(i), r6);
    }
}
