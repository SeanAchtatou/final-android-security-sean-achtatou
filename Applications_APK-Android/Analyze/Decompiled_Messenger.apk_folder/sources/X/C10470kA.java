package X;

import java.io.Serializable;

/* renamed from: X.0kA  reason: invalid class name and case insensitive filesystem */
public abstract class C10470kA implements C26761by, Serializable {
    private static final long serialVersionUID = 8891625428805876137L;
    public final C10290jr _base;
    public final int _mapperFeatures;

    public abstract C10120ja introspectClassAnnotations(C10030jR r1);

    public final boolean canOverrideAccessModifiers() {
        return isEnabled(C26771bz.CAN_OVERRIDE_ACCESS_MODIFIERS);
    }

    public final C10030jR constructType(Class cls) {
        return this._base._typeFactory._constructType(cls, null);
    }

    public C10140jc getAnnotationIntrospector() {
        return this._base._annotationIntrospector;
    }

    public C10160je getDefaultVisibilityChecker() {
        return this._base._visibilityChecker;
    }

    public final boolean isEnabled(C26771bz r3) {
        if ((r3.getMask() & this._mapperFeatures) != 0) {
            return true;
        }
        return false;
    }

    public static int collectFeatureDefaults(Class cls) {
        int i = 0;
        for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
            C26781c0 r1 = (C26781c0) enumR;
            if (r1.enabledByDefault()) {
                i |= r1.getMask();
            }
        }
        return i;
    }

    public C10470kA(C10290jr r1, int i) {
        this._base = r1;
        this._mapperFeatures = i;
    }

    public C10470kA(C10470kA r2) {
        this._base = r2._base;
        this._mapperFeatures = r2._mapperFeatures;
    }
}
