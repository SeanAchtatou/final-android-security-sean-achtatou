package X;

import com.facebook.acra.ACRA;
import com.facebook.acra.util.StatFsUtil;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.16C  reason: invalid class name */
public final class AnonymousClass16C {
    public static long A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return 2;
            case 2:
                return 4;
            case 3:
                return 8;
            case 4:
                return 16;
            case 5:
                return 32;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return 64;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return 128;
            case 8:
                return 256;
            case Process.SIGKILL:
                return 512;
            case AnonymousClass1Y3.A01 /*10*/:
                return StatFsUtil.IN_KILO_BYTE;
            default:
                return 1;
        }
    }
}
