package X;

/* renamed from: X.06P  reason: invalid class name */
public final class AnonymousClass06P implements AnonymousClass0Z1 {
    private AnonymousClass0US A00;

    public String Amj() {
        return "loom_config";
    }

    public static final AnonymousClass06P A00(AnonymousClass1XY r2) {
        return new AnonymousClass06P(AnonymousClass0UQ.A00(AnonymousClass1Y3.AOJ, r2));
    }

    public String getCustomData(Throwable th) {
        return ((C25051Yd) this.A00.get()).B4E(846456450056495L, "<no config>");
    }

    private AnonymousClass06P(AnonymousClass0US r1) {
        this.A00 = r1;
    }
}
