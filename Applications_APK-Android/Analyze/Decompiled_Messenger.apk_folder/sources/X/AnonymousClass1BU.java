package X;

import com.facebook.graphservice.interfaces.GraphQLQuery;
import com.facebook.graphservice.interfaces.GraphQLService;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

/* renamed from: X.1BU  reason: invalid class name */
public final class AnonymousClass1BU {
    public AnonymousClass0UN A00;

    public static ListenableFuture A01(AnonymousClass1BU r9, C26941cO r10, C10880l0 r11, C33431nZ r12, C09290gx r13, SettableFuture settableFuture) {
        SettableFuture settableFuture2 = settableFuture;
        AnonymousClass1BU r4 = r9;
        GraphQLService A02 = ((C13560re) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B2J, r9.A00)).A02(r10);
        C10880l0 r7 = r11;
        GraphQLQuery A01 = ((C13320rE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BE2, r9.A00)).A01(r11, r12);
        if (settableFuture == null) {
            settableFuture2 = SettableFuture.create();
        }
        AnonymousClass108 r1 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, r9.A00);
        r1.A02(new AnonymousClass109(r4, A02, A01, r7, settableFuture2, r13));
        r1.A02 = r11.A06;
        r1.A03 = "GraphQL";
        r1.A00 = new AnonymousClass0WK();
        ((AnonymousClass0g4) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ASI, r4.A00)).A04(r1.A01(), "None");
        return settableFuture2;
    }

    public static final AnonymousClass1BU A00(AnonymousClass1XY r1) {
        return new AnonymousClass1BU(r1);
    }

    private AnonymousClass1BU(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(8, r3);
    }
}
