package X;

/* renamed from: X.0p2  reason: invalid class name and case insensitive filesystem */
public final class C12300p2 {
    public final Object A00;
    public final Object A01;

    public boolean equals(Object obj) {
        if (!(obj instanceof C12300p2)) {
            return false;
        }
        C12300p2 r4 = (C12300p2) obj;
        if (!C1756388o.A00(r4.A00, this.A00) || !C1756388o.A00(r4.A01, this.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode;
        Object obj = this.A00;
        int i = 0;
        if (obj == null) {
            hashCode = 0;
        } else {
            hashCode = obj.hashCode();
        }
        Object obj2 = this.A01;
        if (obj2 != null) {
            i = obj2.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return AnonymousClass08S.A0T("Pair{", String.valueOf(this.A00), " ", String.valueOf(this.A01), "}");
    }

    public C12300p2(Object obj, Object obj2) {
        this.A00 = obj;
        this.A01 = obj2;
    }
}
