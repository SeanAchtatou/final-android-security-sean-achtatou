package X;

import com.facebook.gk.sessionless.GkSessionlessModule;

/* renamed from: X.0WQ  reason: invalid class name */
public final class AnonymousClass0WQ implements AnonymousClass0WR {
    private final AnonymousClass0WS A00;
    private final AnonymousClass0US[] A01;

    public static final AnonymousClass0WQ A00(AnonymousClass1XY r5) {
        C001500z A05 = AnonymousClass0UU.A05(r5);
        AnonymousClass1YI A002 = AnonymousClass0WA.A00(r5);
        GkSessionlessModule.A00(r5);
        return new AnonymousClass0WQ(A05, A002, AnonymousClass0VB.A00(AnonymousClass1Y3.AV9, r5), AnonymousClass0UQ.A00(AnonymousClass1Y3.A59, r5));
    }

    public int BKI() {
        return this.A00.A04.length;
    }

    public AnonymousClass1YQ BLp() {
        AnonymousClass0US A002 = this.A00.A00();
        if (A002 != null) {
            return (AnonymousClass1YQ) A002.get();
        }
        return null;
    }

    private AnonymousClass0WQ(C001500z r9, AnonymousClass1YI r10, AnonymousClass0US r11, AnonymousClass0US r12) {
        this.A01 = new AnonymousClass0US[]{r11, r12};
        AnonymousClass0US[] r1 = this.A01;
        this.A00 = new AnonymousClass0WS(r1, new C001500z[]{null, null}, r9, new Integer[]{null, null}, new boolean[]{true, true}, new boolean[]{false, false}, r10);
    }
}
