package X;

import android.animation.StateListAnimator;
import android.content.res.TypedArray;
import android.graphics.PathEffect;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0yw  reason: invalid class name and case insensitive filesystem */
public final class C17460yw implements C17470yx {
    public void AMg(C22571Lz r1) {
    }

    public void ANp(C17760zQ r1) {
    }

    public void ANs(List list) {
    }

    public C17470yx ANw(C14940uO r2) {
        return null;
    }

    public C17470yx ANx(C14940uO r2) {
        return null;
    }

    public void ANz(C14940uO r1) {
    }

    public void AOS(C17770zR r1) {
    }

    public void AOU(C17770zR r1) {
    }

    public void AOc(TypedArray typedArray) {
    }

    public boolean AOl() {
        return false;
    }

    public void AOt(float f) {
    }

    public void AOw() {
    }

    public C17470yx API(Drawable drawable) {
        return null;
    }

    public C17470yx AQW(AnonymousClass38W r2) {
        return null;
    }

    public void AQX(AnonymousClass1IF r1, int[] iArr, float[] fArr) {
    }

    public void AR7(float f, float f2) {
    }

    public C17470yx ASH(C17770zR r2) {
        return null;
    }

    public C17470yx AY1(boolean z) {
        return null;
    }

    public void Aa6(float f) {
    }

    public void Aa7(float f) {
    }

    public void Aa8(int i) {
    }

    public C17470yx Aa9(AnonymousClass10X r2) {
        return null;
    }

    public void AaB(float f) {
    }

    public void AaD(float f) {
    }

    public C17470yx AaM(AnonymousClass10N r2) {
        return null;
    }

    public C17470yx Aac(Drawable drawable) {
        return null;
    }

    public C17470yx Aaj(AnonymousClass10N r2) {
        return null;
    }

    public Drawable Ads() {
        return null;
    }

    public C17730zN Adz() {
        return null;
    }

    public int[] Aeu() {
        return new int[0];
    }

    public C17730zN Aev() {
        return null;
    }

    public PathEffect Aew() {
        return null;
    }

    public float[] Aex() {
        return new float[0];
    }

    /* renamed from: Ah3 */
    public C17470yx Ah2(int i) {
        return null;
    }

    public int Ah4() {
        return 0;
    }

    public C17770zR Ahm() {
        return null;
    }

    public List Ahs() {
        return null;
    }

    public ArrayList Ahu() {
        return null;
    }

    public C17730zN AiP() {
        return null;
    }

    public AnonymousClass0p4 AiS() {
        return null;
    }

    public C22571Lz AkV() {
        return null;
    }

    public AnonymousClass10N AnI() {
        return null;
    }

    public Drawable AnL() {
        return null;
    }

    public C17730zN AnN() {
        return null;
    }

    public AnonymousClass10N Anq() {
        return null;
    }

    public C17770zR AoX() {
        return null;
    }

    public int Apg() {
        return 0;
    }

    public AnonymousClass10N Aqk() {
        return null;
    }

    public int Ary() {
        return 0;
    }

    public float As1() {
        return 0.0f;
    }

    public float As2() {
        return 0.0f;
    }

    public int AsD() {
        return 0;
    }

    public int AsF(AnonymousClass10G r2) {
        return 0;
    }

    public C17470yx AvV() {
        return null;
    }

    public C31401jd Avp() {
        return null;
    }

    public AnonymousClass14K AwP() {
        return null;
    }

    public C31401jd AwQ() {
        return null;
    }

    public int Aws() {
        return 0;
    }

    public int Awt() {
        return 0;
    }

    public int Awu() {
        return 0;
    }

    public int Awv() {
        return 0;
    }

    public C17470yx AxD() {
        return null;
    }

    public AnonymousClass1KE Axp() {
        return null;
    }

    public C17660zG B16() {
        return null;
    }

    public StateListAnimator B3m() {
        return null;
    }

    public int B3n() {
        return 0;
    }

    public C17660zG B4P() {
        return null;
    }

    public float B4Q() {
        return 0.0f;
    }

    public float B4V() {
        return 0.0f;
    }

    public C17770zR B5K() {
        return null;
    }

    public String B5X() {
        return null;
    }

    public int B6k() {
        return 0;
    }

    public int B6l() {
        return 0;
    }

    public int B6m() {
        return 0;
    }

    public int B6n() {
        return 0;
    }

    public String B73() {
        return null;
    }

    public Integer B74() {
        return null;
    }

    public String B75() {
        return null;
    }

    public ArrayList B76() {
        return null;
    }

    public AnonymousClass10N B7X() {
        return null;
    }

    public List B7d() {
        return null;
    }

    public AnonymousClass10N B9W() {
        return null;
    }

    public AnonymousClass10N B9Y() {
        return null;
    }

    public float B9Z() {
        return 0.0f;
    }

    public float B9b() {
        return 0.0f;
    }

    public ArrayList BA2() {
        return null;
    }

    public int BA8() {
        return 0;
    }

    public int BAE() {
        return 0;
    }

    public AnonymousClass10W BAF() {
        return null;
    }

    public boolean BBY() {
        return false;
    }

    public boolean BBZ() {
        return false;
    }

    public boolean BBs() {
        return false;
    }

    public boolean BBu() {
        return false;
    }

    public boolean BBv() {
        return false;
    }

    public boolean BBx() {
        return false;
    }

    public void BC1(float f) {
    }

    public void BC2(int i) {
    }

    public C17470yx BCN(int i) {
        return null;
    }

    public C17470yx BDS(AnonymousClass10N r2) {
        return null;
    }

    public boolean BER() {
        return false;
    }

    public boolean BF5() {
        return false;
    }

    public boolean BFL() {
        return false;
    }

    public boolean BFQ() {
        return false;
    }

    public boolean BFZ() {
        return false;
    }

    public boolean BFx() {
        return false;
    }

    public boolean BG6() {
        return false;
    }

    public void BGL(boolean z) {
    }

    public C17470yx BHb(C14950uP r2) {
        return null;
    }

    public void BHv(C17660zG r1) {
    }

    public void BJt(AnonymousClass10G r1) {
    }

    public void BJv(AnonymousClass10G r1, float f) {
    }

    public void BJx(AnonymousClass10G r1, int i) {
    }

    public void BJz(AnonymousClass1KE r1) {
    }

    public void BK0() {
    }

    public void BKG(float f) {
    }

    public void BKH(int i) {
    }

    public void BKK(float f) {
    }

    public void BKL(int i) {
    }

    public void BL7(float f) {
    }

    public void BL8(int i) {
    }

    public void BL9(float f) {
    }

    public void BLA(int i) {
    }

    public void BwL(AnonymousClass10G r1, float f) {
    }

    public void BwM(AnonymousClass10G r1, int i) {
    }

    public void BxH(AnonymousClass10G r1, float f) {
    }

    public void BxI(AnonymousClass10G r1, int i) {
    }

    public void BxJ(AnonymousClass1LC r1) {
    }

    public C17470yx Bzu(AnonymousClass0p4 r1, C17770zR r2) {
        return this;
    }

    public C17660zG C0A() {
        return null;
    }

    public void C0S(C49912d4 r1) {
    }

    public void C6H(C17730zN r1) {
    }

    public void C6O(C17730zN r1) {
    }

    public void C6X(boolean z) {
    }

    public void C70(C17770zR r1) {
    }

    public void C78(C17730zN r1) {
    }

    public void C7h(C22571Lz r1) {
    }

    public void C84(C17730zN r1) {
    }

    public void C8J(C17730zN r1) {
    }

    public void C8l(int i) {
    }

    public void C8m(float f) {
    }

    public void C8n(float f) {
    }

    public void C8r(int i) {
    }

    public void C9P(AnonymousClass119 r1) {
    }

    public void C9n(C17470yx r1) {
    }

    public void C9r(C31401jd r1) {
    }

    public void CC0(int i) {
    }

    public void CC1(int i) {
    }

    public void CDF(AnonymousClass117 r1) {
    }

    public boolean CDz() {
        return false;
    }

    public C17470yx CHg(StateListAnimator stateListAnimator) {
        return null;
    }

    public C17470yx CHh(int i) {
        return null;
    }

    public C17470yx CIv(String str) {
        return null;
    }

    public C17470yx CJD(AnonymousClass10G r2, int i) {
        return null;
    }

    public C17470yx CJN(String str, String str2) {
        return null;
    }

    public C17470yx CJO(Integer num) {
        return null;
    }

    public C17470yx CJe(AnonymousClass10N r2) {
        return null;
    }

    public void CLJ(boolean z) {
    }

    public C17470yx CLh(AnonymousClass10N r2) {
        return null;
    }

    public C17470yx CLi(AnonymousClass10N r2) {
        return null;
    }

    public C17470yx CLj(float f) {
        return null;
    }

    public C17470yx CLk(float f) {
        return null;
    }

    public void CNJ(float f) {
    }

    public void CNK(int i) {
    }

    public C17470yx CNU(AnonymousClass6I0 r2) {
        return null;
    }

    public C17470yx CNW() {
        return null;
    }

    public int getHeight() {
        return 0;
    }

    public String getSimpleName() {
        return "NoOpInternalNode";
    }

    public int getWidth() {
        return 0;
    }

    public C17470yx AWl() {
        throw new UnsupportedOperationException("NoOpInternalNode.deepClone not implemented.");
    }

    public void AUX(Object obj) {
    }
}
