package X;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import com.facebook.content.SecureContextHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1e6  reason: invalid class name and case insensitive filesystem */
public final class C28001e6 implements SecureContextHelper {
    private static volatile C28001e6 A05;
    public AnonymousClass0UN A00;
    private C28131eJ A01;
    private C28131eJ A02;
    private C28131eJ A03;
    public final String A04;

    public synchronized C28131eJ BLv() {
        if (this.A02 == null) {
            this.A02 = A02(false);
        }
        return this.A02;
    }

    public synchronized C28131eJ BLw() {
        if (this.A03 == null) {
            this.A03 = A02(true);
        }
        return this.A03;
    }

    public static Intent A00(C28001e6 r5, Intent intent) {
        boolean A032;
        String str = r5.A04;
        int i = AnonymousClass1Y3.Aii;
        AnonymousClass0UN r2 = r5.A00;
        AnonymousClass26F r3 = (AnonymousClass26F) AnonymousClass1XX.A02(0, i, r2);
        AnonymousClass09P r22 = (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, r2);
        if (intent == null) {
            A032 = false;
        } else {
            ComponentName component = intent.getComponent();
            if (component == null || !component.getPackageName().equals(str)) {
                A032 = AnonymousClass3DX.A03(str, r22, intent, AnonymousClass26F.A01(intent, r3.A01, r3.A00));
            } else {
                A032 = true;
            }
        }
        if (!A032) {
            return null;
        }
        return intent;
    }

    public static final C28001e6 A01(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C28001e6.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C28001e6(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private C28131eJ A02(boolean z) {
        C28131eJ r2;
        ArrayList arrayList = new ArrayList((Set) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Axv, this.A00));
        Collections.sort(arrayList, new AnonymousClass226());
        ArrayList arrayList2 = new ArrayList(arrayList.size() + 1);
        arrayList2.addAll(arrayList);
        if (z) {
            r2 = (C44082Gv) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AEs, this.A00);
        } else {
            r2 = (AnonymousClass227) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BOr, this.A00);
        }
        arrayList2.add(new AnonymousClass26G(r2, (AnonymousClass09P) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Amr, this.A00)));
        return new AnonymousClass26E(new AnonymousClass26A(new C44072Gu(this), new AnonymousClass26D(arrayList2)));
    }

    public C28131eJ AZR() {
        if (this.A01 == null) {
            Set set = (Set) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AVp, this.A00);
            ArrayList arrayList = new ArrayList(set.size() + 1);
            arrayList.addAll(set);
            arrayList.add((AnonymousClass267) AnonymousClass1XX.A02(8, AnonymousClass1Y3.Au4, this.A00));
            AnonymousClass268 r4 = new AnonymousClass268(this, new AnonymousClass269(this, new AnonymousClass26A(new AnonymousClass26B(this), new AnonymousClass26D(arrayList))));
            this.A01 = r4;
            this.A01 = new AnonymousClass26E(r4);
        }
        return this.A01;
    }

    private C28001e6(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(11, r3);
        this.A04 = C04490Ux.A0t(r3);
    }

    public void CGs(Intent intent, int i, Activity activity) {
        AZR().A09(intent, i, activity);
    }

    public void CGt(Intent intent, int i, Fragment fragment) {
        AZR().A0A(intent, i, fragment);
    }

    public void CHH(Intent intent, Context context) {
        BLv().A0B(intent, context);
    }

    public void CHJ(Intent intent, int i, Activity activity) {
        BLv().A09(intent, i, activity);
    }

    public void CHK(Intent intent, int i, Fragment fragment) {
        BLv().A0A(intent, i, fragment);
    }

    public void startFacebookActivity(Intent intent, Context context) {
        AZR().A0B(intent, context);
    }
}
