package X;

import java.util.concurrent.ExecutorService;

/* renamed from: X.0bZ  reason: invalid class name and case insensitive filesystem */
public final class C06480bZ extends C06490ba implements AnonymousClass0VL {
    private final AnonymousClass0WP A00;
    private final ExecutorService A01;

    public void execute(Runnable runnable) {
        this.A00.CIB("DefaultProcessIdleExecutor", runnable, AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, this.A01);
    }

    public C06480bZ(AnonymousClass0WP r1, ExecutorService executorService) {
        this.A00 = r1;
        this.A01 = executorService;
    }
}
