package X;

/* renamed from: X.1Rp  reason: invalid class name and case insensitive filesystem */
public final class C23951Rp {
    public final C23901Rk A00 = new C23901Rk();

    public void A00() {
        if (!this.A00.A05()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    public void A01(Exception exc) {
        boolean z;
        C23901Rk r3 = this.A00;
        synchronized (r3.A07) {
            if (r3.A05) {
                z = false;
            } else {
                r3.A05 = true;
                r3.A01 = exc;
                r3.A06 = false;
                r3.A07.notifyAll();
                C23901Rk.A01(r3);
                if (!r3.A06 && 0 != 0) {
                    r3.A00 = new AnonymousClass9N2(r3);
                }
                z = true;
            }
        }
        if (!z) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }

    public void A02(Object obj) {
        if (!this.A00.A06(obj)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }
}
