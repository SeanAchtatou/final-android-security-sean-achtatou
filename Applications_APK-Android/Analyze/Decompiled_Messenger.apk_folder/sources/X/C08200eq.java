package X;

import java.util.HashSet;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0eq  reason: invalid class name and case insensitive filesystem */
public final class C08200eq extends C08190ep {
    private static volatile C08200eq A04;
    public AnonymousClass0UN A00;
    public volatile HashSet A01 = null;
    private volatile int A02 = 0;
    private volatile int[] A03 = null;

    public static final C08200eq A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C08200eq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C08200eq(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public AnonymousClass0Ti AsW() {
        if (this.A02 == 0) {
            synchronized (this) {
                if (this.A02 == 0) {
                    this.A03 = ((C07030cV) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A3l, this.A00)).A00.Aik().Azv();
                    HashSet hashSet = new HashSet();
                    for (int valueOf : this.A03) {
                        hashSet.add(Integer.valueOf(valueOf));
                    }
                    this.A01 = hashSet;
                    this.A02 = this.A03.length;
                }
            }
        }
        return new AnonymousClass0Ti(null, this.A03, null);
    }

    private C08200eq(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = new HashSet();
    }
}
