package X;

/* renamed from: X.102  reason: invalid class name */
public enum AnonymousClass102 {
    FROM_SERVER,
    FROM_CACHE_UP_TO_DATE,
    FROM_CACHE_STALE,
    NO_DATA,
    FROM_CACHE_INCOMPLETE,
    FROM_CACHE_HAD_SERVER_ERROR,
    FROM_DB_NEED_INITIAL_FETCH
}
