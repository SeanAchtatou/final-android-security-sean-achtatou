package X;

/* renamed from: X.21E  reason: invalid class name */
public final class AnonymousClass21E implements AnonymousClass04e {
    private final AnonymousClass09P A00;
    private final String A01;

    public void C2S(String str) {
        this.A00.CGS(this.A01, str);
    }

    public void C2T(String str, String str2, Throwable th) {
        this.A00.softReport(str, str2, th);
    }

    public AnonymousClass21E(AnonymousClass09P r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }
}
