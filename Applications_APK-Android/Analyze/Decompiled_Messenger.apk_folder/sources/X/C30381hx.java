package X;

import com.facebook.messaging.notify.EventReminderNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1hx  reason: invalid class name and case insensitive filesystem */
public final class C30381hx extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$22";
    public final /* synthetic */ EventReminderNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C30381hx(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, EventReminderNotification eventReminderNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = eventReminderNotification;
    }
}
