package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.mig.scheme.schemes.DarkColorScheme;

/* renamed from: X.1uL  reason: invalid class name and case insensitive filesystem */
public final class C36931uL implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return DarkColorScheme.A00();
    }

    public Object[] newArray(int i) {
        return new DarkColorScheme[i];
    }
}
