package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0K1  reason: invalid class name */
public final class AnonymousClass0K1 implements FilenameFilter {
    public final /* synthetic */ String A00;

    public AnonymousClass0K1(String str) {
        this.A00 = str;
    }

    public boolean accept(File file, String str) {
        String str2 = this.A00;
        if (str.indexOf(str2) == 0 && str.indexOf(95, str2.length() + 1) == -1 && str.endsWith(".txt")) {
            return true;
        }
        return false;
    }
}
