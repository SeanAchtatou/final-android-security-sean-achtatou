package X;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/* renamed from: X.1T0  reason: invalid class name */
public final class AnonymousClass1T0 implements C08470fP {
    public int A00;
    public int A01 = 100;
    public long A02;
    public C30440EwU A03;
    public C22531Ls A04;
    public AnonymousClass0UN A05;
    public C26322Cw1 A06;
    public WeakReference A07;
    public Map A08;
    public Map A09;
    public Map A0A;
    public Map A0B;
    public Map A0C;
    public Future A0D;
    private boolean A0E = false;
    public final C09390hE A0F;
    public final AnonymousClass15Q A0G;
    public final AnonymousClass1T2 A0H;
    public final C22481Ln A0I;
    public final C22521Lr A0J;
    public final C22521Lr A0K;
    public final C22511Lq A0L;
    public final C194017z A0M;
    public final C194017z A0N;
    public final AnonymousClass1T4 A0O;
    public final AnonymousClass1T3 A0P;
    public final AnonymousClass0US A0Q;
    public final AnonymousClass0US A0R;
    public final ExecutorService A0S;
    private final C09020gN A0T;
    private final C28781fM A0U;

    public static void A01(AnonymousClass1T0 r4, C194017z r5, int i, int i2, boolean z) {
        int i3;
        int i4;
        r5.A06.A00(z, i);
        AnonymousClass184 r3 = r5.A04;
        AnonymousClass15Q r0 = r4.A0G;
        AnonymousClass15Q.A02(r0);
        int A002 = AnonymousClass15Q.A00(r0, i, z);
        boolean z2 = false;
        if (A002 >= 4) {
            z2 = true;
        }
        if (!z2) {
            A002 = 0;
        }
        r3.A00(z, A002);
        r5.A05.A00(z, i);
        AnonymousClass15Q r02 = r4.A0G;
        AnonymousClass15Q.A02(r02);
        if (z) {
            i3 = r02.A00;
        } else {
            i3 = 1;
        }
        int i5 = i + i3;
        r5.A07.A00(z, i5);
        AnonymousClass15Q r03 = r4.A0O.A00;
        if (z) {
            i4 = r03.A00;
        } else {
            i4 = 1;
        }
        r5.A08.A00(z, (i5 * i2) / i4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0059, code lost:
        if (r1 != false) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008f, code lost:
        if (r1 != false) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a7, code lost:
        if (r1.A01 != 0) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b7, code lost:
        if (r0 != 0) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00bb, code lost:
        if (r1 != false) goto L_0x00bd;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r5 = this;
            int r2 = X.AnonymousClass1Y3.Aue
            X.0UN r1 = r5.A05
            r0 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4Lb r3 = (X.C88604Lb) r3
            monitor-enter(r3)
            java.util.HashSet r1 = r3.A01     // Catch:{ all -> 0x00f3 }
            int r0 = java.lang.System.identityHashCode(r5)     // Catch:{ all -> 0x00f3 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00f3 }
            boolean r0 = r1.remove(r0)     // Catch:{ all -> 0x00f3 }
            if (r0 == 0) goto L_0x0036
            java.util.HashSet r0 = r3.A01     // Catch:{ all -> 0x00f3 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00f3 }
            if (r0 == 0) goto L_0x0036
            r2 = 0
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x00f3 }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x00f3 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00f3 }
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2     // Catch:{ all -> 0x00f3 }
            r1 = 44826625(0x2ac0001, float:2.527313E-37)
            r0 = 2
            r2.markerEnd(r1, r0)     // Catch:{ all -> 0x00f3 }
        L_0x0036:
            monitor-exit(r3)
            X.1fM r2 = r5.A0U
            boolean r0 = r2.A03
            if (r0 == 0) goto L_0x004b
            r1 = 0
            r2.A03 = r1
            boolean r0 = r2.A05
            if (r0 == 0) goto L_0x004b
            X.0gN r0 = r2.A06
            r0.A01()
            r2.A05 = r1
        L_0x004b:
            boolean r0 = r5.A0E
            if (r0 != 0) goto L_0x0050
            return
        L_0x0050:
            java.util.concurrent.Future r0 = r5.A0D
            if (r0 == 0) goto L_0x005b
            boolean r1 = r0.isDone()
            r0 = 1
            if (r1 == 0) goto L_0x005c
        L_0x005b:
            r0 = 0
        L_0x005c:
            if (r0 != 0) goto L_0x006f
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r5.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            long r0 = r0.now()
            r5.A02 = r0
        L_0x006f:
            X.1Ln r1 = r5.A0I
            boolean r0 = r1.A09
            if (r0 == 0) goto L_0x0086
            X.5hk r2 = r1.A01
            r0 = 0
            r1.A01 = r0
            if (r2 == 0) goto L_0x0086
            X.1Lo r1 = r1.A05
            r0 = 1
            android.os.Message r0 = r1.obtainMessage(r0, r2)
            r1.sendMessage(r0)
        L_0x0086:
            java.util.concurrent.Future r0 = r5.A0D
            if (r0 == 0) goto L_0x0091
            boolean r1 = r0.isDone()
            r0 = 1
            if (r1 == 0) goto L_0x0092
        L_0x0091:
            r0 = 0
        L_0x0092:
            if (r0 != 0) goto L_0x00de
            X.1Ln r3 = r5.A0I
            boolean r4 = r3.A09
            r0 = r4 ^ 1
            if (r0 == 0) goto L_0x00bd
            X.17z r0 = r5.A0M
            X.184 r1 = r0.A08
            int r0 = r1.A00
            if (r0 != 0) goto L_0x00a9
            int r1 = r1.A01
            r0 = 0
            if (r1 == 0) goto L_0x00aa
        L_0x00a9:
            r0 = 1
        L_0x00aa:
            if (r0 != 0) goto L_0x00bd
            X.17z r0 = r5.A0N
            X.184 r1 = r0.A08
            int r0 = r1.A00
            if (r0 != 0) goto L_0x00b9
            int r0 = r1.A01
            r1 = 0
            if (r0 == 0) goto L_0x00ba
        L_0x00b9:
            r1 = 1
        L_0x00ba:
            r0 = 0
            if (r1 == 0) goto L_0x00be
        L_0x00bd:
            r0 = 1
        L_0x00be:
            if (r0 == 0) goto L_0x00de
            r0 = 0
            r5.A03 = r0
            X.1Lq r2 = r5.A0L
            java.util.concurrent.ExecutorService r1 = r5.A0S
            if (r4 == 0) goto L_0x00eb
            X.0VJ r0 = r3.A00
            if (r0 != 0) goto L_0x00d6
            X.0VI r1 = new X.0VI
            X.1Lo r0 = r3.A05
            r1.<init>(r0)
            r3.A00 = r1
        L_0x00d6:
            X.0VJ r0 = r3.A00
            com.google.common.util.concurrent.ListenableFuture r0 = r0.CIC(r2)
        L_0x00dc:
            r5.A0D = r0
        L_0x00de:
            X.0gN r0 = r5.A0T
            r0.A01()
            r0 = 0
            r5.A0E = r0
            X.1T2 r1 = r5.A0H
            r1.A01 = r0
            return
        L_0x00eb:
            r0 = -1115638610(0xffffffffbd80b4ae, float:-0.06284462)
            java.util.concurrent.Future r0 = X.AnonymousClass07A.A02(r1, r2, r0)
            goto L_0x00dc
        L_0x00f3:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1T0.A02():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0058, code lost:
        if (r1 != false) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
            r4 = this;
            int r2 = X.AnonymousClass1Y3.Aue
            X.0UN r1 = r4.A05
            r0 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4Lb r3 = (X.C88604Lb) r3
            monitor-enter(r3)
            java.util.HashSet r1 = r3.A01     // Catch:{ all -> 0x009a }
            int r0 = java.lang.System.identityHashCode(r4)     // Catch:{ all -> 0x009a }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x009a }
            boolean r0 = r1.add(r0)     // Catch:{ all -> 0x009a }
            if (r0 == 0) goto L_0x0036
            java.util.HashSet r0 = r3.A01     // Catch:{ all -> 0x009a }
            int r1 = r0.size()     // Catch:{ all -> 0x009a }
            r0 = 1
            if (r1 != r0) goto L_0x0036
            r2 = 0
            int r1 = X.AnonymousClass1Y3.BBd     // Catch:{ all -> 0x009a }
            X.0UN r0 = r3.A00     // Catch:{ all -> 0x009a }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x009a }
            com.facebook.quicklog.QuickPerformanceLogger r1 = (com.facebook.quicklog.QuickPerformanceLogger) r1     // Catch:{ all -> 0x009a }
            r0 = 44826625(0x2ac0001, float:2.527313E-37)
            r1.markerStart(r0)     // Catch:{ all -> 0x009a }
        L_0x0036:
            monitor-exit(r3)
            X.1fM r2 = r4.A0U
            boolean r0 = r2.A03
            if (r0 != 0) goto L_0x004b
            r1 = 1
            r2.A03 = r1
            boolean r0 = r2.A04
            if (r0 == 0) goto L_0x004b
            X.0gN r0 = r2.A06
            r0.A02()
            r2.A05 = r1
        L_0x004b:
            boolean r0 = r4.A0E
            if (r0 != 0) goto L_0x0099
            java.util.concurrent.Future r0 = r4.A0D
            if (r0 == 0) goto L_0x005a
            boolean r1 = r0.isDone()
            r0 = 1
            if (r1 == 0) goto L_0x005b
        L_0x005a:
            r0 = 0
        L_0x005b:
            if (r0 != 0) goto L_0x0099
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BBa
            X.0UN r0 = r4.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.069 r0 = (X.AnonymousClass069) r0
            r0.now()
            X.1Lr r0 = r4.A0K
            com.facebook.quicklog.QuickPerformanceLogger r2 = r0.A03
            int r1 = r0.A00
            int r0 = r0.A01
            r2.markerStart(r1, r0)
            X.1Lr r0 = r4.A0J
            com.facebook.quicklog.QuickPerformanceLogger r2 = r0.A03
            int r1 = r0.A00
            int r0 = r0.A01
            r2.markerStart(r1, r0)
            X.1T2 r1 = r4.A0H
            r0 = 1
            r1.A01 = r0
            r4.A0E = r0
            X.0gN r0 = r4.A0T
            r0.A02()
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r1.<init>(r0)
            r4.A07 = r1
        L_0x0099:
            return
        L_0x009a:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1T0.A03():void");
    }

    public void onFrameRendered(int i) {
        C117635hk r3;
        int i2;
        C005505z.A03("FrameRateLogger.onFrameRendered", 1207867516);
        try {
            if (this.A0E) {
                if (this.A07.get() == null) {
                    AnonymousClass1T2 r6 = this.A0H;
                    if (C30442EwX.A00 == null) {
                        C30442EwX.A00 = new C30442EwX();
                    }
                    C30442EwX ewX = C30442EwX.A00;
                    synchronized (r6.A05) {
                        try {
                            if (r6.A01 && !r6.A08.contains(ewX)) {
                                r6.A08.add(ewX);
                                int A002 = AnonymousClass1T2.A00(r6, ewX);
                                synchronized (r6.A04) {
                                    int i3 = r6.A02.get(A002);
                                    r6.A02.put(A002, i3 + 1);
                                    if (i3 == 0) {
                                        r6.A00 = ((long) (1 << A002)) | r6.A00;
                                    }
                                }
                            }
                        } catch (Throwable th) {
                            th = th;
                            throw th;
                        }
                    }
                    this.A07 = new WeakReference(new Object());
                }
                long j = this.A0H.A00;
                C22481Ln r7 = this.A0I;
                long j2 = j;
                int max = Math.max(1, i);
                if (!r7.A09) {
                    if (!r7.A08) {
                        j2 = 0;
                    }
                    AnonymousClass1T3 r0 = r7.A06;
                    AnonymousClass1T3.A01(r0);
                    if (0 != 0) {
                        i2 = r0.A02;
                    } else {
                        i2 = r0.A03;
                    }
                    r7.A00(max, i2, false, j2);
                } else {
                    if (r7.A01 == null) {
                        if (r7.A02 != null) {
                            synchronized (r7.A07) {
                                try {
                                    r3 = r7.A02;
                                    if (r3 != null) {
                                        r7.A02 = r3.A02;
                                        r3.A02 = null;
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    throw th;
                                }
                            }
                            r7.A01 = r3;
                        }
                        r3 = new C117635hk(r7.A08);
                        r7.A01 = r3;
                    }
                    C117635hk r5 = r7.A01;
                    int i4 = r5.A00;
                    int i5 = i4 + 1;
                    r5.A00 = i5;
                    r5.A03[i4] = max;
                    r5.A05[i4] = false;
                    long[] jArr = r5.A04;
                    if (jArr != null) {
                        jArr[i4] = j;
                    }
                    boolean z = false;
                    if (100 <= i5) {
                        z = true;
                    }
                    if (z) {
                        C117635hk r2 = r7.A01;
                        r7.A01 = null;
                        if (r2 != null) {
                            C22491Lo r1 = r7.A05;
                            r1.sendMessage(r1.obtainMessage(1, r2));
                        }
                    }
                }
                this.A0H.A02();
                C005505z.A00(-49059929);
            }
        } finally {
            this.A0H.A02();
            C005505z.A00(123430934);
        }
    }

    public static void A00(int i, boolean z, C30442EwX ewX, Map map) {
        AnonymousClass184 r0 = (AnonymousClass184) map.get(ewX);
        if (r0 == null) {
            r0 = new AnonymousClass184();
            map.put(ewX, r0);
        }
        r0.A00(z, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x013c, code lost:
        if ((r23.getPackageManager().getApplicationInfo(r23.getPackageName(), 0).flags & com.facebook.common.dextricks.DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET) != 0) goto L_0x013e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1T0(X.AnonymousClass1XY r12, java.util.concurrent.ExecutorService r13, X.C27031cX r14, X.C28771fL r15, X.AnonymousClass1T2 r16, X.AnonymousClass0US r17, X.AnonymousClass1T3 r18, X.C09390hE r19, X.AnonymousClass0US r20, X.AnonymousClass15Q r21, X.AnonymousClass1T4 r22, android.content.Context r23, android.os.Looper r24, java.lang.Boolean r25, java.lang.String r26, X.C25051Yd r27) {
        /*
            r11 = this;
            r11.<init>()
            r1 = 0
            r11.A0E = r1
            r0 = 100
            r11.A01 = r0
            X.0UN r2 = new X.0UN
            r0 = 5
            r2.<init>(r0, r12)
            r11.A05 = r2
            r9 = r26
            com.google.common.base.Preconditions.checkNotNull(r9)
            r2 = r25
            boolean r8 = r2.booleanValue()
            X.1fM r3 = new X.1fM
            X.1cX r4 = new X.1cX
            r4.<init>(r15)
            X.1T3 r5 = X.AnonymousClass1T3.A00(r15)
            X.1Yd r6 = X.AnonymousClass0WT.A00(r15)
            X.1T4 r7 = X.AnonymousClass1T4.A00(r15)
            r3.<init>(r4, r5, r6, r7, r8, r9)
            r11.A0U = r3
            r11.A0S = r13
            X.0gN r0 = r14.A00(r2)
            r11.A0T = r0
            r0.A01 = r11
            r0 = r16
            r11.A0H = r0
            r0 = r17
            r11.A0R = r0
            r6 = r18
            r11.A0P = r6
            r0 = r19
            r11.A0F = r0
            r0 = r20
            r11.A0Q = r0
            r7 = r21
            r11.A0G = r7
            r0 = r22
            r11.A0O = r0
            X.1Ln r4 = new X.1Ln
            int r2 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r11.A05
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r0)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r0 = 80
            boolean r10 = r2.AbO(r0, r1)
            r5 = r11
            r8 = r24
            r4.<init>(r5, r6, r7, r8, r9, r10)
            r11.A0I = r4
            X.17z r0 = new X.17z
            r0.<init>()
            r11.A0N = r0
            X.17z r0 = new X.17z
            r0.<init>()
            r11.A0M = r0
            r2 = 846993320575350(0x3025600000176, double:4.184703019532823E-309)
            r0 = r27
            java.lang.String r2 = r0.B4C(r2)
            r6 = 0
            if (r2 == 0) goto L_0x00d7
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x00d7
            X.04b r7 = new X.04b
            r7.<init>()
            java.lang.String r0 = ","
            java.lang.String[] r8 = r2.split(r0)
            int r5 = r8.length
            r4 = 0
        L_0x00a4:
            if (r4 >= r5) goto L_0x00c5
            r2 = r8[r4]
            java.lang.String r0 = ":"
            java.lang.String[] r3 = r2.split(r0)
            int r2 = r3.length
            r0 = 2
            if (r2 != r0) goto L_0x00c2
            r2 = r3[r1]     // Catch:{ NumberFormatException -> 0x00c2 }
            r0 = 1
            r0 = r3[r0]     // Catch:{ NumberFormatException -> 0x00c2 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x00c2 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ NumberFormatException -> 0x00c2 }
            r7.put(r2, r0)     // Catch:{ NumberFormatException -> 0x00c2 }
        L_0x00c2:
            int r4 = r4 + 1
            goto L_0x00a4
        L_0x00c5:
            boolean r0 = r7.containsKey(r9)
            if (r0 == 0) goto L_0x00d7
            java.lang.Object r0 = r7.get(r9)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x00d7
            int r6 = r0.intValue()
        L_0x00d7:
            r11.A00 = r6
            X.1Lq r0 = new X.1Lq
            r0.<init>(r11)
            r11.A0L = r0
            X.1Lr r6 = new X.1Lr
            int r2 = X.AnonymousClass1Y3.BBd
            X.0UN r0 = r11.A05
            r3 = 1
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r3, r2, r0)
            com.facebook.quicklog.QuickPerformanceLogger r5 = (com.facebook.quicklog.QuickPerformanceLogger) r5
            int r4 = r11.A00
            r0 = 15990790(0xf40006, float:2.240787E-38)
            r6.<init>(r5, r0, r9, r4)
            r11.A0K = r6
            X.1Lr r2 = new X.1Lr
            r0 = 15990793(0xf40009, float:2.2407874E-38)
            r2.<init>(r5, r0, r9, r4)
            r11.A0J = r2
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r11.A0A = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r11.A09 = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r11.A0B = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r11.A08 = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r11.A0C = r0
            X.1Ls r0 = new X.1Ls
            r0.<init>()
            r11.A04 = r0
            android.content.pm.PackageManager r2 = r23.getPackageManager()     // Catch:{ NameNotFoundException -> 0x013e }
            java.lang.String r0 = r23.getPackageName()     // Catch:{ NameNotFoundException -> 0x013e }
            android.content.pm.ApplicationInfo r0 = r2.getApplicationInfo(r0, r1)     // Catch:{ NameNotFoundException -> 0x013e }
            int r0 = r0.flags     // Catch:{ NameNotFoundException -> 0x013e }
            r0 = r0 & 16384(0x4000, float:2.2959E-41)
            r2 = 0
            if (r0 == 0) goto L_0x013f
        L_0x013e:
            r2 = 1
        L_0x013f:
            X.1Ls r0 = r11.A04
            r0.A01 = r2
            java.lang.String r0 = "dalvik.vm.usejit"
            java.lang.String r0 = X.AnonymousClass00I.A02(r0)
            java.lang.String r2 = "true"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0175
            X.1Ls r0 = r11.A04
            r0.A02 = r3
        L_0x0155:
            java.lang.String r0 = "dalvik.vm.usejitprofiles"
            java.lang.String r0 = X.AnonymousClass00I.A02(r0)
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0170
            X.1Ls r0 = r11.A04
            r0.A03 = r3
        L_0x0165:
            X.1Ls r1 = r11.A04
            java.lang.String r0 = "pm.dexopt.bg-dexopt"
            java.lang.String r0 = X.AnonymousClass00I.A02(r0)
            r1.A00 = r0
            return
        L_0x0170:
            X.1Ls r0 = r11.A04
            r0.A03 = r1
            goto L_0x0165
        L_0x0175:
            X.1Ls r0 = r11.A04
            r0.A02 = r1
            goto L_0x0155
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1T0.<init>(X.1XY, java.util.concurrent.ExecutorService, X.1cX, X.1fL, X.1T2, X.0US, X.1T3, X.0hE, X.0US, X.15Q, X.1T4, android.content.Context, android.os.Looper, java.lang.Boolean, java.lang.String, X.1Yd):void");
    }
}
