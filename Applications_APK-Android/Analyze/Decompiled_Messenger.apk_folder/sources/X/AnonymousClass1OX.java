package X;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1OX  reason: invalid class name */
public final class AnonymousClass1OX implements C06670bt {
    public final AnonymousClass069 A00;
    public final C25051Yd A01;
    public final Map A02 = new HashMap();
    public volatile AnonymousClass1OW A03;

    public int Ai5() {
        return -1;
    }

    public void BTE(int i) {
        synchronized (this.A02) {
            AnonymousClass1OW r6 = this.A03;
            Collection<Long> collection = (Collection) this.A02.get(Integer.valueOf(i));
            if (!(collection == null || r6 == null)) {
                synchronized (r6.A00) {
                    for (Long longValue : collection) {
                        r6.A00.remove(Long.valueOf(longValue.longValue()));
                    }
                }
            }
        }
    }

    public AnonymousClass1OX(C25051Yd r2, AnonymousClass069 r3) {
        this.A01 = r2;
        this.A00 = r3;
    }
}
