package X;

import java.util.Arrays;

/* renamed from: X.0Ss  reason: invalid class name and case insensitive filesystem */
public final class C04170Ss {
    public final String A00;
    public final String A01;
    private final String A02;
    private final String A03;
    private final String A04;
    private final String A05;
    private final String A06;

    public boolean equals(Object obj) {
        if (!(obj instanceof C04170Ss)) {
            return false;
        }
        C04170Ss r4 = (C04170Ss) obj;
        if (!C28858E7g.A00(this.A00, r4.A00) || !C28858E7g.A00(this.A02, r4.A02) || !C28858E7g.A00(this.A03, r4.A03) || !C28858E7g.A00(this.A04, r4.A04) || !C28858E7g.A00(this.A01, r4.A01) || !C28858E7g.A00(this.A06, r4.A06) || !C28858E7g.A00(this.A05, r4.A05)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A02, this.A03, this.A04, this.A01, this.A06, this.A05});
    }

    public String toString() {
        E8B e8b = new E8B(this);
        e8b.A00(AnonymousClass24B.$const$string(AnonymousClass1Y3.A6g), this.A00);
        e8b.A00("apiKey", this.A02);
        e8b.A00("databaseUrl", this.A03);
        e8b.A00("gcmSenderId", this.A01);
        e8b.A00("storageBucket", this.A06);
        e8b.A00("projectId", this.A05);
        return e8b.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000e, code lost:
        if (r3.trim().isEmpty() != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C04170Ss(java.lang.String r3, java.lang.String r4, java.lang.String r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, java.lang.String r9) {
        /*
            r2 = this;
            r2.<init>()
            if (r3 == 0) goto L_0x0010
            java.lang.String r0 = r3.trim()
            boolean r1 = r0.isEmpty()
            r0 = 0
            if (r1 == 0) goto L_0x0011
        L_0x0010:
            r0 = 1
        L_0x0011:
            r1 = r0 ^ 1
            java.lang.String r0 = "ApplicationId must be set."
            X.AnonymousClass09E.A09(r1, r0)
            r2.A00 = r3
            r2.A02 = r4
            r2.A03 = r5
            r2.A04 = r6
            r2.A01 = r7
            r2.A06 = r8
            r2.A05 = r9
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04170Ss.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }
}
