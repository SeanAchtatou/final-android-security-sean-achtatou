package X;

import com.facebook.http.interfaces.RequestPriority;
import com.google.common.base.Preconditions;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0xw  reason: invalid class name and case insensitive filesystem */
public final class C16890xw {
    public RequestPriority A00;
    public AtomicReference A01;
    public final Object A02 = new Object();
    public volatile AnonymousClass2Y5 A03;
    public volatile RequestPriority A04;

    public static void A00(C16890xw r2, RequestPriority requestPriority) {
        synchronized (r2.A02) {
            r2.A04 = requestPriority;
            r2.A00 = null;
            r2.A03.AS7(requestPriority);
        }
    }

    public RequestPriority A01() {
        if (this.A04 != null) {
            return this.A04;
        }
        RequestPriority requestPriority = (RequestPriority) this.A01.get();
        if (requestPriority == null) {
            return RequestPriority.A04;
        }
        return requestPriority;
    }

    public C16890xw(String str, RequestPriority requestPriority) {
        Preconditions.checkNotNull(str);
        this.A04 = requestPriority;
        this.A01 = new AtomicReference(null);
    }
}
