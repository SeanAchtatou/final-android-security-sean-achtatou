package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1GH  reason: invalid class name */
public final class AnonymousClass1GH extends Enum {
    private static final /* synthetic */ AnonymousClass1GH[] A00;
    public static final AnonymousClass1GH A01;
    public static final AnonymousClass1GH A02;
    public final String analyticsString;

    static {
        AnonymousClass1GH r3 = new AnonymousClass1GH("SECTION_HEADER", 0, "h");
        A02 = r3;
        AnonymousClass1GH r4 = new AnonymousClass1GH("MONTAGE_SECTION_HEADER", 1, "w");
        AnonymousClass1GH r5 = new AnonymousClass1GH("MORE_FOOTER", 2, "m");
        A01 = r5;
        A00 = new AnonymousClass1GH[]{r3, r4, r5, new AnonymousClass1GH("COLLAPSED_UNIT", 3, "c"), new AnonymousClass1GH("CREATE_ROOM", 4, "r"), new AnonymousClass1GH("INVITE_COWORKERS", 5, "i")};
    }

    public static AnonymousClass1GH valueOf(String str) {
        return (AnonymousClass1GH) Enum.valueOf(AnonymousClass1GH.class, str);
    }

    public static AnonymousClass1GH[] values() {
        return (AnonymousClass1GH[]) A00.clone();
    }

    private AnonymousClass1GH(String str, int i, String str2) {
        this.analyticsString = str2;
    }
}
