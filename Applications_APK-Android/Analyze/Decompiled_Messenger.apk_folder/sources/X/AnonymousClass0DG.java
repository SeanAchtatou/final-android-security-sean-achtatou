package X;

import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CaptureRequest;
import android.os.Handler;

/* renamed from: X.0DG  reason: invalid class name */
public final class AnonymousClass0DG {
    static {
        AnonymousClass0FQ.A03 = true;
    }

    public static void A00(CameraCaptureSession cameraCaptureSession, CaptureRequest captureRequest, CameraCaptureSession.CaptureCallback captureCallback, Handler handler) {
        cameraCaptureSession.setRepeatingRequest(captureRequest, captureCallback, handler);
        if (AnonymousClass0FQ.A03()) {
            CameraDevice device = cameraCaptureSession.getDevice();
            try {
                AnonymousClass0FQ.A02.readLock().lock();
                int size = AnonymousClass0FQ.A01.size();
                for (int i = 0; i < size; i++) {
                    ((AnonymousClass0FU) AnonymousClass0FQ.A01.get(i)).Bpi(device);
                }
            } finally {
                AnonymousClass0FQ.A02.readLock().unlock();
            }
        }
    }
}
