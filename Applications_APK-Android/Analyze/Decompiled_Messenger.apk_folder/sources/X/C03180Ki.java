package X;

import java.io.OutputStream;

/* renamed from: X.0Ki  reason: invalid class name and case insensitive filesystem */
public final class C03180Ki extends OutputStream {
    public final /* synthetic */ C03190Kj A00;

    public C03180Ki(C03190Kj r1) {
        this.A00 = r1;
    }

    public void write(int i) {
        this.A00.A00.write(i);
    }

    public void write(byte[] bArr) {
        this.A00.A00.write(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.A00.A00.write(bArr, i, i2);
    }
}
