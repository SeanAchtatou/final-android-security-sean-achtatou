package X;

import java.io.File;

/* renamed from: X.0Pc  reason: invalid class name and case insensitive filesystem */
public final class C03700Pc implements C05460Za {
    public static final C03700Pc A00(AnonymousClass1XY r1) {
        AnonymousClass1YA.A00(r1);
        return new C03700Pc();
    }

    private C03700Pc() {
    }

    public void clearUserData() {
        boolean z;
        C004505k A00 = C004505k.A00();
        synchronized (A00) {
            A00.A09(new C004105a());
            AnonymousClass05m r5 = A00.A03;
            z = true;
            for (File file : r5.A05()) {
                if (file.exists() && !file.delete()) {
                    z = false;
                    r5.A02.A02++;
                }
            }
        }
        if (!z) {
            C010708t.A0J("ProfiloTraceCleaner", "Could not clear config or traces!");
        }
    }
}
