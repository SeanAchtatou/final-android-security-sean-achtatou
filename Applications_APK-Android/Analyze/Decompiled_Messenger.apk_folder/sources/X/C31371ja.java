package X;

import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.http.interfaces.RequestPriority;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchMoreThreadsParams;
import com.facebook.messaging.service.model.FetchMoreThreadsResult;
import com.facebook.messaging.service.model.FetchThreadListParams;
import com.facebook.messaging.service.model.FetchThreadListResult;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1ja  reason: invalid class name and case insensitive filesystem */
public final class C31371ja implements AnonymousClass1EM, CallerContextable {
    public static final AtomicInteger A0D = new AtomicInteger();
    public static final String __redex_internal_original_name = "com.facebook.messaging.threadlist.loader.ThreadListLoader";
    public int A00;
    public AnonymousClass1G0 A01;
    public AnonymousClass1G0 A02;
    public AnonymousClass0UN A03;
    public C10950l8 A04;
    public C10700ki A05 = C10700ki.ALL;
    public C32971md A06;
    public C32971md A07;
    public ImmutableSet A08 = RegularImmutableSet.A05;
    private C20021Ap A09;
    private C09510hU A0A;
    public final Map A0B = new EnumMap(C10700ki.class);
    private final Map A0C = new AnonymousClass04a();

    public static void A0B(C31371ja r8, C32971md r9, Bundle bundle, FetchMoreThreadsResult fetchMoreThreadsResult, String str) {
        ThreadsCollection threadsCollection;
        C31371ja r4 = r8;
        C32971md r5 = r9;
        C13890sF r92 = (C13890sF) r8.A0B.get(r9.A01);
        if (r92 != null) {
            A0A(r8, r5);
            if (((C35391r9) AnonymousClass1XX.A02(16, AnonymousClass1Y3.ACN, r8.A03)).A01()) {
                threadsCollection = ((AnonymousClass1TR) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AgT, r8.A03)).A06(fetchMoreThreadsResult.A03, r92.A02.A02());
            } else {
                threadsCollection = fetchMoreThreadsResult.A03;
            }
            A0C(r4, r5, threadsCollection, bundle, fetchMoreThreadsResult.A01, r92);
        }
        ((C07380dK) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BQ3, ((C15500vO) AnonymousClass1XX.A02(8, AnonymousClass1Y3.ATj, r4.A03)).A00)).A03("android_messenger_thread_list_load_more_threads_success");
        ((C15500vO) AnonymousClass1XX.A02(8, AnonymousClass1Y3.ATj, r4.A03)).A03(str);
    }

    private C46952Sp A00(C10700ki r6) {
        C46952Sp r4 = (C46952Sp) this.A0C.get(r6);
        if (r4 != null) {
            return r4;
        }
        C46952Sp r42 = new C46952Sp(((C15460vK) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Aib, this.A03)).A00.AqL(567309345425284L, 10), 0);
        this.A0C.put(r6, r42);
        return r42;
    }

    public static final C31371ja A01(AnonymousClass1XY r1) {
        return new C31371ja(r1);
    }

    public static final C31371ja A02(AnonymousClass1XY r1) {
        return new C31371ja(r1);
    }

    public static String A03(OperationResult operationResult) {
        String string;
        Bundle bundle = operationResult.resultDataBundle;
        if (bundle == null) {
            string = null;
        } else {
            string = bundle.getString("source");
        }
        if (string == null) {
            return "unknown";
        }
        return string;
    }

    private void A04(C32971md r8, C09510hU r9) {
        CallerContext A072;
        ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03)).A01("loadThreads", "ThreadListLoader", r8, r9.name());
        if (this.A02 != null) {
            ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03)).A01("returnFromLoadThreads", "ThreadListLoader", r8, "loadAlreadyInProgress");
            return;
        }
        RequestPriority requestPriority = RequestPriority.A05;
        if (r8 != null && (r8.A03 || r8.A04)) {
            requestPriority = RequestPriority.A04;
        }
        if (r8 != null) {
            A072 = r8.A00;
        } else {
            A072 = CallerContext.A07(getClass(), "thread_list_loader_param_null");
        }
        Preconditions.checkNotNull(A072.A0G());
        this.A0A = r9;
        C10680kg r1 = new C10680kg();
        r1.A02 = r9;
        C10950l8 r6 = this.A04;
        r1.A04 = r6;
        r1.A05 = r8.A01;
        r1.A06 = this.A08;
        r1.A03 = requestPriority;
        FetchThreadListParams fetchThreadListParams = new FetchThreadListParams(r1);
        C12520pV r2 = C12500pT.A04;
        ((C12450pN) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AwM, this.A03)).A03(r2, "ThreadListLoader FETCH_THREAD_LIST (folder=" + r6 + ")");
        Bundle bundle = new Bundle();
        bundle.putParcelable("fetchThreadListParams", fetchThreadListParams);
        AnonymousClass0lL newInstance = ((BlueServiceOperationFactory) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B1W, this.A03)).newInstance("fetch_thread_list", bundle, 1, A072);
        if (r9 != C09510hU.STALE_DATA_OKAY) {
            AnonymousClass108 r12 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, this.A03);
            r12.A02(new AnonymousClass2TE(this, newInstance, r8));
            r12.A02 = "FetchThreadList";
            r12.A00 = new AnonymousClass0WK();
            ((AnonymousClass0g4) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ASI, this.A03)).A04(r12.A01(), "KeepExisting");
        } else {
            A08(this, newInstance, r8);
        }
        this.A06 = r8;
    }

    private void A05(C32971md r5, C13890sF r6) {
        if (this.A09 != null) {
            r6.A02.A00.size();
            ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03)).A01("notifyLoadSucceeded", "ThreadListLoader", r5, r6);
            this.A09.BdJ(r5, r6);
            return;
        }
        ((AnonymousClass09P) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Amr, this.A03)).CGS("ThreadListLoader", "callback is null");
    }

    private void A06(C32971md r5, C13890sF r6) {
        if (this.A09 != null) {
            r6.A02.A00.size();
            ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03)).A01("notifyNewResult", "ThreadListLoader", r5, r6);
            this.A09.Bgh(r5, r6);
            return;
        }
        ((AnonymousClass09P) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Amr, this.A03)).CGS("ThreadListLoader", "callback is null");
    }

    private void A07(C32971md r5, ListenableFuture listenableFuture) {
        C20021Ap r0 = this.A09;
        if (r0 != null) {
            r0.BdU(r5, listenableFuture);
        } else {
            ((AnonymousClass09P) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Amr, this.A03)).CGS("ThreadListLoader", "callback is null");
        }
    }

    public static void A08(C31371ja r5, AnonymousClass0lL r6, C32971md r7) {
        ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, r5.A03)).A01("startOperation", "ThreadListLoader", r7, r6.AwN());
        C27211cp CGe = r6.CGe();
        r5.A07(r7, CGe);
        C186214l r3 = new C186214l(r5, r7);
        r5.A02 = AnonymousClass1G0.A00(CGe, r3);
        C05350Yp.A08(CGe, r3, (AnonymousClass0VL) AnonymousClass1XX.A02(10, AnonymousClass1Y3.AKg, r5.A03));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0043, code lost:
        if (r0 != false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0040, code lost:
        if (r2 == X.AnonymousClass0u3.A04) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A09(X.C31371ja r5, com.facebook.fbservice.results.DataFetchDisposition r6, X.C13890sF r7, X.C32971md r8, X.C32971md r9) {
        /*
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r5.A03
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.06B r0 = (X.AnonymousClass06B) r0
            long r3 = r0.now()
            java.util.Map r1 = r5.A0B
            X.0ki r0 = r8.A01
            r1.put(r0, r7)
            int r2 = X.AnonymousClass1Y3.B9x
            X.0UN r1 = r5.A03
            r0 = 12
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0fv r1 = (X.C08770fv) r1
            java.lang.String r0 = "thread_list_loaded"
            r1.A0G(r0)
            X.1md r0 = r5.A06
            boolean r0 = r0.A05
            if (r0 == 0) goto L_0x0050
            X.0hU r1 = r5.A0A
            X.0hU r0 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA
            if (r1 == r0) goto L_0x0050
            X.0u3 r2 = r6.A07
            X.0u3 r0 = X.AnonymousClass0u3.IN_MEMORY_CACHE
            if (r2 == r0) goto L_0x0042
            X.0u3 r0 = X.AnonymousClass0u3.LOCAL_DISK_CACHE
            if (r2 == r0) goto L_0x0042
            X.0u3 r1 = X.AnonymousClass0u3.LOCAL_UNSPECIFIED_CACHE
            r0 = 0
            if (r2 != r1) goto L_0x0043
        L_0x0042:
            r0 = 1
        L_0x0043:
            if (r0 == 0) goto L_0x0050
        L_0x0045:
            r5.A06(r8, r7)
            X.1md r1 = r5.A06
            X.0hU r0 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA
            r5.A04(r1, r0)
        L_0x004f:
            return
        L_0x0050:
            com.facebook.common.util.TriState r1 = r6.A04
            r0 = 0
            boolean r0 = r1.asBoolean(r0)
            if (r0 != 0) goto L_0x0045
            X.0l8 r1 = r5.A04
            X.0l8 r0 = X.C10950l8.A0A
            if (r1 == r0) goto L_0x006a
            long r0 = r7.A00
            long r3 = r3 - r0
            r1 = 1800000(0x1b7740, double:8.89318E-318)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x006a
            goto L_0x0045
        L_0x006a:
            r5.A06(r8, r7)
            X.1md r0 = r5.A06
            r5.A05(r0, r7)
            if (r9 == 0) goto L_0x004f
            java.lang.String r0 = "onFetchThreadsSucceeded"
            A0F(r5, r9, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31371ja.A09(X.1ja, com.facebook.fbservice.results.DataFetchDisposition, X.0sF, X.1md, X.1md):void");
    }

    public static void A0A(C31371ja r8, C32971md r9) {
        C46952Sp A002 = r8.A00(r9.A01);
        int i = A002.A01;
        int i2 = A002.A00;
        A002.A01 = Math.min(((int) (((double) i) * ((C15460vK) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Aib, r8.A03)).A00.Aki(1130259298976056L))) + ((C15460vK) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Aib, r8.A03)).A00.AqL(567309345490821L, 0), 100);
        A002.A00 = i2 + 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public static void A0C(C31371ja r7, C32971md r8, ThreadsCollection threadsCollection, Bundle bundle, DataFetchDisposition dataFetchDisposition, C13890sF r12) {
        C13890sF r2 = new C13890sF(ThreadsCollection.A00(r12.A02, threadsCollection), r12.A00, dataFetchDisposition);
        r7.A0B.put(r8.A01, r2);
        r7.A06(r8, r2);
        r7.A05(r8, r2);
        if (bundle != null && bundle.containsKey("source")) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BBd, r7.A03)).markerAnnotate(5505136, r7.A00, bundle.getString("source"), -1);
        }
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BBd, r7.A03)).markerEnd(5505136, r7.A00, (short) 2);
    }

    public static void A0D(C31371ja r5, C32971md r6, FetchThreadListResult fetchThreadListResult, C32971md r8, String str) {
        ThreadsCollection threadsCollection;
        if (r5.A06 == null) {
            C010708t.A0J("ThreadListLoader", "mCurrentParams is null in onFetchThreadsSucceeded.");
        } else {
            if (((C35391r9) AnonymousClass1XX.A02(16, AnonymousClass1Y3.ACN, r5.A03)).A01()) {
                threadsCollection = ((AnonymousClass1TR) AnonymousClass1XX.A02(15, AnonymousClass1Y3.AgT, r5.A03)).A06(fetchThreadListResult.A06, 0);
            } else {
                threadsCollection = fetchThreadListResult.A06;
            }
            DataFetchDisposition dataFetchDisposition = fetchThreadListResult.A02;
            A09(r5, dataFetchDisposition, new C13890sF(threadsCollection, fetchThreadListResult.A00, dataFetchDisposition), r6, r8);
        }
        int i = AnonymousClass1Y3.ATj;
        ((C15500vO) AnonymousClass1XX.A02(8, i, r5.A03)).A01();
        ((C15500vO) AnonymousClass1XX.A02(8, i, r5.A03)).A04(str);
    }

    public static void A0E(C31371ja r4, C32971md r5, C55452o6 r6) {
        if (r4.A09 != null) {
            r6.A00.getMessage();
            ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, r4.A03)).A01("notifyLoadFailed", "ThreadListLoader", r5, r6.A00);
            r4.A09.Bd1(r5, r6);
        } else {
            ((AnonymousClass09P) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Amr, r4.A03)).CGS("ThreadListLoader", "callback is null");
        }
        ((C149056vp) AnonymousClass1XX.A02(11, AnonymousClass1Y3.ANo, r4.A03)).A01("ThreadListLoader", r5, r6.A00, new HashMap());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0338, code lost:
        if (r7.A05 != false) goto L_0x033a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0342, code lost:
        if (r7.A04 != false) goto L_0x0344;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0065, code lost:
        if (((X.AnonymousClass0m6) r0).A0i(r6) != false) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b1, code lost:
        if (((X.AnonymousClass0m6) r0).A0i(r6) != false) goto L_0x00b3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0313  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x03b5  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0406  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x02d9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0F(X.C31371ja r21, X.C32971md r22, java.lang.String r23) {
        /*
            java.lang.String r20 = "ThreadListLoader"
            int r2 = X.AnonymousClass1Y3.B9x
            r8 = r21
            X.0UN r1 = r8.A03
            r19 = 12
            r0 = r19
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0fv r1 = (X.C08770fv) r1
            java.lang.String r0 = "thread_list_loading"
            r1.A0G(r0)
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r8.A03
            r10 = 13
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r10, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            java.lang.String r1 = "startLoadThreadList"
            r7 = r22
            r3 = r23
            r0 = r20
            r2.A01(r1, r0, r7, r3)
            int r1 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r8.A03
            r9 = 4
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.1bq r11 = (X.C26681bq) r11
            X.0l8 r6 = r8.A04
            X.0ki r1 = r7.A01
            X.0l8 r0 = X.C10950l8.A06
            r5 = 1
            if (r6 == r0) goto L_0x00f2
            boolean r0 = r6.A01()
            if (r0 != 0) goto L_0x00f2
            X.0l8 r0 = X.C10950l8.A0B
            if (r6 == r0) goto L_0x00f2
            X.0ki r0 = X.C10700ki.SMS
            r4 = 2
            if (r1 != r0) goto L_0x009e
            boolean r0 = X.C26681bq.A06(r11)
            if (r0 == 0) goto L_0x00ef
            int r1 = X.AnonymousClass1Y3.Ad9
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
        L_0x005f:
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            boolean r0 = r0.A0i(r6)
            if (r0 == 0) goto L_0x00ef
        L_0x0067:
            r11 = 0
            r6 = 0
            if (r5 == 0) goto L_0x02d2
            int r1 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r8.A03
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.1bq r5 = (X.C26681bq) r5
            com.google.common.collect.ImmutableSet r0 = r8.A08
            X.1Xv r4 = r0.iterator()
        L_0x007b:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0105
            java.lang.Object r3 = r4.next()
            X.2e3 r3 = (X.C50452e3) r3
            r2 = 3
            int r1 = X.AnonymousClass1Y3.BPw
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2eN r1 = (X.C50652eN) r1
            monitor-enter(r1)
            X.2eP r0 = X.C50652eN.A00(r1, r3)     // Catch:{ all -> 0x0102 }
            boolean r0 = r0.A02     // Catch:{ all -> 0x0102 }
            monitor-exit(r1)
            if (r0 != 0) goto L_0x007b
            r0 = 0
            goto L_0x0106
        L_0x009e:
            X.0ki r0 = X.C10700ki.NON_SMS
            r2 = 6
            if (r1 != r0) goto L_0x00cc
            int r1 = X.AnonymousClass1Y3.ATy
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
        L_0x00ab:
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            boolean r0 = r0.A0i(r6)
            if (r0 == 0) goto L_0x00ef
        L_0x00b3:
            int r1 = X.AnonymousClass1Y3.Abq
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pJ r0 = (X.C12420pJ) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x0067
            int r1 = X.AnonymousClass1Y3.AL3
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)
            goto L_0x005f
        L_0x00cc:
            X.0ki r0 = X.C10700ki.ALL
            if (r1 != r0) goto L_0x00ef
            int r1 = X.AnonymousClass1Y3.ATy
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            boolean r0 = r0.A0i(r6)
            if (r0 == 0) goto L_0x00ef
            boolean r0 = X.C26681bq.A06(r11)
            if (r0 == 0) goto L_0x00b3
            int r1 = X.AnonymousClass1Y3.Ad9
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            goto L_0x00ab
        L_0x00ef:
            r5 = 0
            goto L_0x0067
        L_0x00f2:
            int r1 = X.AnonymousClass1Y3.ATy
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            boolean r5 = r0.A0i(r6)
            goto L_0x0067
        L_0x0102:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0105:
            r0 = 1
        L_0x0106:
            if (r0 == 0) goto L_0x02d2
            int r0 = X.AnonymousClass1Y3.AgT
            X.0UN r1 = r8.A03
            r2 = 15
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r2, r0, r1)
            X.1TR r12 = (X.AnonymousClass1TR) r12
            int r0 = X.AnonymousClass1Y3.AuB
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r9, r0, r1)
            X.1bq r4 = (X.C26681bq) r4
            X.0l8 r3 = r8.A04
            X.0ki r1 = r7.A01
            com.google.common.collect.ImmutableSet r0 = r8.A08
            com.facebook.messaging.model.threads.ThreadsCollection r5 = r4.A09(r3, r1, r0)
            int r1 = X.AnonymousClass1Y3.ACN
            X.0UN r0 = r12.A00
            r3 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1r9 r0 = (X.C35391r9) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x025c
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1r9 r0 = (X.C35391r9) r0
            X.1Yd r3 = r0.A00
            r0 = 282325383185737(0x100c6002d0549, double:1.394872727810386E-309)
            boolean r0 = r3.Aem(r0)
            if (r0 == 0) goto L_0x0152
            com.facebook.messaging.model.threads.ThreadsCollection r0 = X.AnonymousClass1TR.A00(r12, r5, r6)
            if (r0 != 0) goto L_0x025c
        L_0x0152:
            r0 = 1
        L_0x0153:
            if (r0 != 0) goto L_0x02d2
            int r1 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r8.A03
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.1bq r4 = (X.C26681bq) r4
            X.0l8 r3 = r8.A04
            X.0ki r1 = r7.A01
            com.google.common.collect.ImmutableSet r0 = r8.A08
            com.facebook.messaging.model.threads.ThreadsCollection r18 = r4.A09(r3, r1, r0)
            r3 = 17
            int r1 = X.AnonymousClass1Y3.A8f
            X.0UN r0 = r8.A03
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1rA r1 = (X.C35401rA) r1
            r0 = r18
            com.google.common.collect.ImmutableList r0 = r0.A00
            r1.A01(r0)
            r3 = 16
            int r1 = X.AnonymousClass1Y3.ACN
            X.0UN r0 = r8.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1r9 r0 = (X.C35391r9) r0
            boolean r0 = r0.A01()
            if (r0 == 0) goto L_0x01dd
            int r1 = X.AnonymousClass1Y3.AgT
            X.0UN r0 = r8.A03
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1TR r4 = (X.AnonymousClass1TR) r4
            int r1 = X.AnonymousClass1Y3.ACN
            X.0UN r0 = r4.A00
            r2 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1r9 r0 = (X.C35391r9) r0
            X.1Yd r12 = r0.A00
            r0 = 845275336671356(0x300c6002e007c, double:4.176215051262106E-309)
            java.lang.String r5 = ""
            java.lang.String r0 = r12.B4E(r0, r5)
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0259
            r0 = r18
            com.facebook.messaging.model.threads.ThreadsCollection r5 = X.AnonymousClass1TR.A00(r4, r0, r6)
            int r1 = X.AnonymousClass1Y3.ACN
            X.0UN r0 = r4.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1r9 r0 = (X.C35391r9) r0
            X.1Yd r2 = r0.A00
            r0 = 282325384299857(0x100c6003e0551, double:1.39487273331487E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x01d5
            r5 = r18
        L_0x01d5:
            if (r5 != 0) goto L_0x0256
            r0 = r18
            com.facebook.messaging.model.threads.ThreadsCollection r18 = r4.A06(r0, r6)
        L_0x01dd:
            int r1 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r8.A03
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.1bq r12 = (X.C26681bq) r12
            X.0l8 r0 = r8.A04
            r21 = r0
            X.0ki r13 = r7.A01
            com.google.common.collect.ImmutableSet r14 = r8.A08
            X.0ki r0 = X.C10700ki.SMS
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r13 == r0) goto L_0x020d
            r2 = 1
            int r1 = X.AnonymousClass1Y3.ATy
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            r1 = r21
            long r0 = r0.A0M(r1)
            long r4 = java.lang.Math.min(r4, r0)
        L_0x020d:
            X.0ki r0 = X.C10700ki.NON_SMS
            if (r13 == r0) goto L_0x0266
            boolean r0 = X.C26681bq.A06(r12)
            if (r0 == 0) goto L_0x0266
            r3 = r21
            int r2 = X.AnonymousClass1Y3.Ad9
            X.0UN r1 = r12.A00
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            long r2 = r0.A0M(r3)
            X.1Xv r17 = r14.iterator()
        L_0x022c:
            boolean r0 = r17.hasNext()
            if (r0 == 0) goto L_0x0262
            java.lang.Object r15 = r17.next()
            X.2e3 r15 = (X.C50452e3) r15
            r14 = 3
            int r1 = X.AnonymousClass1Y3.BPw
            X.0UN r0 = r12.A00
            java.lang.Object r14 = X.AnonymousClass1XX.A02(r14, r1, r0)
            X.2eN r14 = (X.C50652eN) r14
            monitor-enter(r14)
            X.2eP r0 = X.C50652eN.A00(r14, r15)     // Catch:{ all -> 0x025f }
            long r0 = r0.A00     // Catch:{ all -> 0x025f }
            monitor-exit(r14)
            r15 = -1
            int r14 = (r0 > r15 ? 1 : (r0 == r15 ? 0 : -1))
            if (r14 == 0) goto L_0x022c
            long r2 = java.lang.Math.min(r2, r0)
            goto L_0x022c
        L_0x0256:
            r18 = r5
            goto L_0x01dd
        L_0x0259:
            r5 = 0
            goto L_0x01d5
        L_0x025c:
            r0 = 0
            goto L_0x0153
        L_0x025f:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        L_0x0262:
            long r4 = java.lang.Math.min(r4, r2)
        L_0x0266:
            X.0ki r0 = X.C10700ki.SMS
            if (r13 == r0) goto L_0x028f
            r2 = 6
            int r1 = X.AnonymousClass1Y3.Abq
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pJ r0 = (X.C12420pJ) r0
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x028f
            int r1 = X.AnonymousClass1Y3.AL3
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            r1 = r21
            long r0 = r0.A0M(r1)
            long r4 = java.lang.Math.min(r4, r0)
        L_0x028f:
            int r1 = X.AnonymousClass1Y3.AuB
            X.0UN r0 = r8.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.1bq r0 = (X.C26681bq) r0
            X.0l8 r3 = r8.A04
            int r2 = X.AnonymousClass1Y3.ATy
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0m6 r0 = (X.AnonymousClass0m6) r0
            r0.A0N(r3)
            com.facebook.fbservice.results.DataFetchDisposition r0 = com.facebook.fbservice.results.DataFetchDisposition.A0C
            X.0sF r3 = new X.0sF
            r12 = r3
            r13 = r18
            r14 = r4
            r16 = r0
            r12.<init>(r13, r14, r16)
            java.util.Map r1 = r8.A0B
            X.0ki r0 = r7.A01
            r1.put(r0, r3)
            int r2 = X.AnonymousClass1Y3.B9x
            X.0UN r1 = r8.A03
            r0 = r19
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0fv r1 = (X.C08770fv) r1
            java.lang.String r0 = "thread_list_loaded_from_cache"
            r1.A0G(r0)
            r8.A06(r7, r3)
            goto L_0x02d3
        L_0x02d2:
            r3 = r11
        L_0x02d3:
            X.1G0 r1 = r8.A01
            java.lang.String r4 = "returnFromStartLoadThreadList"
            if (r1 == 0) goto L_0x030f
            X.1md r0 = r8.A06
            boolean r0 = r0.A04
            if (r0 == 0) goto L_0x02f5
            boolean r0 = r7.A04
            if (r0 != 0) goto L_0x02f5
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r8.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r10, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            java.lang.String r1 = "nonUserActionCannotPrioritizeUserAction"
        L_0x02ef:
            r0 = r20
            r2.A01(r4, r0, r7, r1)
            return
        L_0x02f5:
            r1.A01(r6)
            r8.A01 = r11
            r8.A06 = r11
            r2 = 6
            int r1 = X.AnonymousClass1Y3.BBd
            X.0UN r0 = r8.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2
            r1 = 5505136(0x540070, float:7.714339E-39)
            int r0 = r8.A00
            r2.markerEnd(r1, r0, r9)
        L_0x030f:
            X.1G0 r0 = r8.A02
            if (r0 == 0) goto L_0x03b3
            X.1md r11 = r8.A06
            if (r11 == 0) goto L_0x0376
            X.1r8 r9 = r11.A02
            X.1r8 r12 = r7.A02
            if (r9 != r12) goto L_0x0376
            X.0ki r5 = r11.A01
            X.0ki r2 = r7.A01
            if (r5 != r2) goto L_0x0376
            r0 = 0
            if (r9 != r12) goto L_0x0327
            r0 = 1
        L_0x0327:
            com.google.common.base.Preconditions.checkState(r0)
            r0 = 0
            if (r5 != r2) goto L_0x032e
            r0 = 1
        L_0x032e:
            com.google.common.base.Preconditions.checkState(r0)
            boolean r6 = r11.A05
            if (r6 != 0) goto L_0x033a
            boolean r0 = r7.A05
            r3 = 0
            if (r0 == 0) goto L_0x033b
        L_0x033a:
            r3 = 1
        L_0x033b:
            boolean r1 = r11.A04
            if (r1 != 0) goto L_0x0344
            boolean r0 = r7.A04
            r2 = 0
            if (r0 == 0) goto L_0x0345
        L_0x0344:
            r2 = 1
        L_0x0345:
            if (r6 != r3) goto L_0x0349
            if (r1 == r2) goto L_0x0360
        L_0x0349:
            X.1md r12 = new X.1md
            boolean r1 = r11.A03
            com.facebook.common.callercontext.CallerContext r0 = r7.A00
            if (r0 != 0) goto L_0x0353
            com.facebook.common.callercontext.CallerContext r0 = r11.A00
        L_0x0353:
            r13 = r3
            r14 = r2
            r15 = r9
            r16 = r5
            r17 = r1
            r18 = r0
            r12.<init>(r13, r14, r15, r16, r17, r18)
            r11 = r12
        L_0x0360:
            r8.A06 = r11
            boolean r0 = r7.A03
            if (r0 == 0) goto L_0x0368
            r8.A07 = r7
        L_0x0368:
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r8.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r10, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            java.lang.String r1 = "loadAlreadyInProgress"
            goto L_0x02ef
        L_0x0376:
            r2 = 14
            int r1 = X.AnonymousClass1Y3.AR0
            X.0UN r0 = r8.A03
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.analytics.DeprecatedAnalyticsLogger r1 = (com.facebook.analytics.DeprecatedAnalyticsLogger) r1
            java.lang.String r0 = "thread_list_loader_mismatched"
            X.1La r2 = r1.A04(r0, r6)
            boolean r0 = r2.A0B()
            if (r0 == 0) goto L_0x03a5
            X.1md r0 = r8.A06
            if (r0 != 0) goto L_0x03bf
            java.lang.String r1 = "null"
        L_0x0394:
            java.lang.String r0 = "current_params"
            r2.A06(r0, r1)
            java.lang.String r1 = r7.toString()
            java.lang.String r0 = "new_params"
            r2.A06(r0, r1)
            r2.A0A()
        L_0x03a5:
            X.1md r0 = r8.A06
            if (r0 == 0) goto L_0x03af
            X.1r8 r1 = r0.A02
            X.1r8 r0 = r7.A02
            if (r1 == r0) goto L_0x03b0
        L_0x03af:
            r6 = 1
        L_0x03b0:
            A0G(r8, r6)
        L_0x03b3:
            if (r3 == 0) goto L_0x0406
            boolean r0 = r7.A05
            if (r0 == 0) goto L_0x03c4
            X.0hU r0 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA
            r8.A04(r7, r0)
            return
        L_0x03bf:
            java.lang.String r1 = r0.toString()
            goto L_0x0394
        L_0x03c4:
            r2 = 3
            int r1 = X.AnonymousClass1Y3.BDk
            X.0UN r0 = r8.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0pX r2 = (X.C12540pX) r2
            X.0l8 r1 = r8.A04
            r0 = 0
            X.0pY r0 = r2.A01(r1, r0)
            X.0hU r1 = r0.A00
            r0 = 0
            if (r1 == 0) goto L_0x03dc
            r0 = 1
        L_0x03dc:
            if (r0 == 0) goto L_0x03e4
            X.0hU r0 = X.C09510hU.PREFER_CACHE_IF_UP_TO_DATE
            r8.A04(r7, r0)
            return
        L_0x03e4:
            r8.A06 = r7
            r8.A05(r7, r3)
            int r1 = X.AnonymousClass1Y3.ATj
            X.0UN r0 = r8.A03
            r2 = 8
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0vO r0 = (X.C15500vO) r0
            r0.A01()
            X.0UN r0 = r8.A03
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0vO r1 = (X.C15500vO) r1
            java.lang.String r0 = "direct_cache"
            r1.A04(r0)
            return
        L_0x0406:
            X.0hU r0 = X.C09510hU.STALE_DATA_OKAY
            r8.A04(r7, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31371ja.A0F(X.1ja, X.1md, java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public static void A0G(C31371ja r5, boolean z) {
        AnonymousClass1G0 r0 = r5.A02;
        if (r0 != null) {
            r0.A01(false);
            r5.A02 = null;
        }
        AnonymousClass1G0 r02 = r5.A01;
        if (r02 != null) {
            r02.A01(false);
            r5.A01 = null;
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BBd, r5.A03)).markerEnd(5505136, r5.A00, (short) 4);
        }
        r5.A06 = null;
        if (z) {
            r5.A0B.clear();
            r5.A0C.clear();
        }
    }

    public void A0J(C32971md r21, String str) {
        C20011Ao r3;
        String str2;
        C32971md r2 = r21;
        ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03)).A01("startLoadMoreThreads", "ThreadListLoader", r2, str);
        if (this.A02 != null) {
            r3 = (C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03);
            str2 = "stillLoadingInitialThread";
        } else if (this.A01 != null) {
            r3 = (C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03);
            str2 = "alreadyLoadingMore";
        } else {
            C13890sF r5 = (C13890sF) this.A0B.get(r2.A01);
            if (r5 == null || r5.A02.A00.isEmpty()) {
                r3 = (C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03);
                str2 = "noThreads";
            } else {
                ThreadsCollection threadsCollection = r5.A02;
                ThreadSummary A032 = threadsCollection.A03(threadsCollection.A02() - 1);
                C09510hU r18 = C09510hU.CHECK_SERVER_FOR_NEW_DATA;
                int i = A00(r2.A01).A01;
                long j = A032.A0B;
                C10950l8 r9 = this.A04;
                C10700ki r10 = r2.A01;
                long j2 = Long.MAX_VALUE;
                if (threadsCollection != null) {
                    C24971Xv it = threadsCollection.A00.iterator();
                    while (it.hasNext()) {
                        ThreadSummary threadSummary = (ThreadSummary) it.next();
                        if (ThreadKey.A0E(threadSummary.A0S)) {
                            long j3 = threadSummary.A08;
                            if (j3 < j2) {
                                j2 = j3;
                            }
                        }
                    }
                }
                FetchMoreThreadsParams fetchMoreThreadsParams = new FetchMoreThreadsParams(r9, r10, j, i, j2, this.A08, AnonymousClass2BV.NONE, r18, null);
                this.A00 = A0D.getAndIncrement();
                ((C12450pN) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AwM, this.A03)).A03(C12500pT.A04, "ThreadListLoader FETCH_MORE_THREADS");
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BBd, this.A03)).markerStart(5505136, this.A00);
                Bundle bundle = new Bundle();
                bundle.putParcelable("fetchMoreThreadsParams", fetchMoreThreadsParams);
                ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03)).A01("loadMoreThreadsInternal", "ThreadListLoader", r2, "fetch_more_threads");
                C27211cp CGe = ((BlueServiceOperationFactory) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B1W, this.A03)).newInstance("fetch_more_threads", bundle, 1, CallerContext.A04(getClass())).CGe();
                this.A06 = r2;
                A07(r2, CGe);
                C186014j r4 = new C186014j(this, r2);
                this.A01 = AnonymousClass1G0.A00(CGe, r4);
                C05350Yp.A08(CGe, r4, (AnonymousClass0VL) AnonymousClass1XX.A02(10, AnonymousClass1Y3.AKg, this.A03));
                return;
            }
        }
        r3.A01("returnFromStartLoadMoreThreads", "ThreadListLoader", r2, str2);
    }

    public void ARp() {
        ((C20011Ao) AnonymousClass1XX.A02(13, AnonymousClass1Y3.BSe, this.A03)).A01("cancelLoad", "ThreadListLoader", null, null);
        A0G(this, true);
    }

    private C31371ja(AnonymousClass1XY r3) {
        this.A03 = new AnonymousClass0UN(18, r3);
    }

    public void A0H(C10950l8 r2) {
        Preconditions.checkNotNull(r2);
        if (r2 != this.A04) {
            this.A04 = r2;
            A0G(this, true);
        }
    }

    /* renamed from: A0I */
    public void CHB(C32971md r4) {
        Preconditions.checkNotNull(r4);
        C35381r8 r2 = r4.A02;
        if (r2 == C35381r8.A02) {
            A0F(this, r4, "startLoad");
        } else if (r2 == C35381r8.MORE_THREADS) {
            A0J(r4, "startLoad");
        }
    }

    public void C6Z(C20021Ap r1) {
        this.A09 = r1;
    }
}
