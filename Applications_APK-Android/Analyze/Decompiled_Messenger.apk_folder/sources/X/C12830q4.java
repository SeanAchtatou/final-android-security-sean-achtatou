package X;

import com.google.common.base.Preconditions;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

/* renamed from: X.0q4  reason: invalid class name and case insensitive filesystem */
public abstract class C12830q4 {
    public InputStream A01() {
        return new FileInputStream(((C12820q3) this).A00);
    }

    public C12830q4 A00(long j, long j2) {
        return new C192518zz(this, j, j2);
    }

    public byte[] A04() {
        byte[] bArr;
        if (!(this instanceof C12820q3)) {
            C12840q5 A00 = C12840q5.A00();
            try {
                InputStream A01 = A01();
                A00.A02(A01);
                byte[] A03 = C76163l4.A03(A01);
                A00.close();
                return A03;
            } catch (Throwable th) {
                A00.close();
                throw th;
            }
        } else {
            C12820q3 r0 = (C12820q3) this;
            C12840q5 A002 = C12840q5.A00();
            try {
                FileInputStream fileInputStream = new FileInputStream(r0.A00);
                A002.A02(fileInputStream);
                long size = fileInputStream.getChannel().size();
                if (size <= 2147483647L) {
                    if (size == 0) {
                        bArr = C76163l4.A03(fileInputStream);
                    } else {
                        int i = (int) size;
                        int i2 = i;
                        bArr = new byte[i];
                        int i3 = i;
                        while (true) {
                            if (i3 > 0) {
                                int i4 = i - i3;
                                int read = fileInputStream.read(bArr, i4, i3);
                                if (read == -1) {
                                    bArr = Arrays.copyOf(bArr, i4);
                                    break;
                                }
                                i3 -= read;
                            } else {
                                int read2 = fileInputStream.read();
                                if (read2 != -1) {
                                    AnonymousClass8XI r2 = new AnonymousClass8XI();
                                    r2.write(read2);
                                    C76163l4.A01(fileInputStream, r2);
                                    byte[] bArr2 = new byte[(i + r2.size())];
                                    System.arraycopy(bArr, 0, bArr2, 0, i2);
                                    r2.A00(bArr2, i2);
                                    bArr = bArr2;
                                }
                            }
                        }
                    }
                    A002.close();
                    return bArr;
                }
                throw new OutOfMemoryError(AnonymousClass08S.A0H("file is too large to fit in a byte array: ", size, " bytes"));
            } catch (Throwable th2) {
                A002.close();
                throw th2;
            }
        }
    }

    public void A02(C192508zy r4) {
        Preconditions.checkNotNull(r4);
        C12840q5 A00 = C12840q5.A00();
        try {
            InputStream A01 = A01();
            A00.A02(A01);
            OutputStream A002 = r4.A00();
            A00.A02(A002);
            C76163l4.A01(A01, A002);
            A00.close();
        } catch (Throwable th) {
            A00.close();
            throw th;
        }
    }

    public void A03(OutputStream outputStream) {
        Preconditions.checkNotNull(outputStream);
        C12840q5 A00 = C12840q5.A00();
        try {
            InputStream A01 = A01();
            A00.A02(A01);
            C76163l4.A01(A01, outputStream);
            A00.close();
        } catch (Throwable th) {
            A00.close();
            throw th;
        }
    }
}
