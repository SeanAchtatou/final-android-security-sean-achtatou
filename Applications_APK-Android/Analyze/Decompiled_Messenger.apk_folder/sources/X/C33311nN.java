package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1nN  reason: invalid class name and case insensitive filesystem */
public final class C33311nN implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxUnitThreadItem(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxUnitThreadItem[i];
    }
}
