package X;

import com.google.common.base.Predicate;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Fg  reason: invalid class name and case insensitive filesystem */
public final class C21111Fg<K, V> extends C21091Fe<K, V> {
    public final Set A00;

    public C21111Fg(Map map, Predicate predicate) {
        super(map, predicate);
        this.A00 = C25011Xz.A09(map.entrySet(), this.A00);
    }
}
