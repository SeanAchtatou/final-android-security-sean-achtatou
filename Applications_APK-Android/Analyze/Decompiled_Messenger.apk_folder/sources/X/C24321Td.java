package X;

import com.facebook.omnistore.Collection;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Td  reason: invalid class name and case insensitive filesystem */
public final class C24321Td {
    private static volatile C24321Td A02;
    public Collection A00 = null;
    public Collection A01 = null;

    public synchronized Collection A01(Integer num) {
        Collection collection;
        synchronized (this) {
            try {
                switch (num.intValue()) {
                    case 0:
                        collection = this.A01;
                        break;
                    case 1:
                        collection = this.A00;
                        break;
                    default:
                        throw new IllegalArgumentException("Non supported CollectionType");
                }
            } catch (Throwable th) {
                th = th;
            }
        }
        if (collection == null) {
            th = new AnonymousClass4X9();
            throw th;
        }
        return collection;
    }

    public synchronized void A02(Collection collection, Integer num) {
        switch (num.intValue()) {
            case 0:
                this.A01 = collection;
                break;
            case 1:
                this.A00 = collection;
                break;
            default:
                throw new IllegalArgumentException("Non supported CollectionType");
        }
    }

    public synchronized void A03(Integer num) {
        switch (num.intValue()) {
            case 0:
                this.A01 = null;
                break;
            case 1:
                this.A00 = null;
                break;
            default:
                throw new IllegalArgumentException("Non supported CollectionType");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r2.A00 != null) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A04(java.lang.Integer r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            int r0 = r3.intValue()     // Catch:{ all -> 0x001d }
            r1 = 0
            switch(r0) {
                case 0: goto L_0x0011;
                case 1: goto L_0x0016;
                default: goto L_0x0009;
            }     // Catch:{ all -> 0x001d }
        L_0x0009:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x001d }
            java.lang.String r0 = "Non supported CollectionType"
            r1.<init>(r0)     // Catch:{ all -> 0x001d }
            throw r1     // Catch:{ all -> 0x001d }
        L_0x0011:
            com.facebook.omnistore.Collection r0 = r2.A01     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            goto L_0x001a
        L_0x0016:
            com.facebook.omnistore.Collection r0 = r2.A00     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
        L_0x001a:
            r1 = 1
        L_0x001b:
            monitor-exit(r2)
            return r1
        L_0x001d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24321Td.A04(java.lang.Integer):boolean");
    }

    public static final C24321Td A00(AnonymousClass1XY r3) {
        if (A02 == null) {
            synchronized (C24321Td.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A02 = new C24321Td();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }
}
