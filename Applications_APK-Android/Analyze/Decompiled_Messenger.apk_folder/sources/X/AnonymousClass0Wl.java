package X;

import com.facebook.mobileconfig.init.MobileConfigInit;
import com.facebook.mobileconfig.init.MobileConfigSessionlessInit;

/* renamed from: X.0Wl  reason: invalid class name */
public abstract class AnonymousClass0Wl implements AnonymousClass1Z5 {
    public AnonymousClass0WW AkC() {
        if (this instanceof C05290Yj) {
            return AnonymousClass0WW.A07;
        }
        if (this instanceof MobileConfigSessionlessInit) {
            return AnonymousClass0WW.A04;
        }
        if (this instanceof MobileConfigInit) {
            return AnonymousClass0WW.A03;
        }
        if (this instanceof C12650pk) {
            return AnonymousClass0WW.A02;
        }
        if (this instanceof C05830aP) {
            return AnonymousClass0WW.A08;
        }
        if (this instanceof AnonymousClass0Z2) {
            return AnonymousClass0WW.A01;
        }
        if (!(this instanceof C05450Yz)) {
            return AnonymousClass0WW.A05;
        }
        return AnonymousClass0WW.A06;
    }
}
