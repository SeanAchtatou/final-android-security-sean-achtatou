package X;

/* renamed from: X.1PV  reason: invalid class name */
public final class AnonymousClass1PV implements C22991Nq {
    public boolean C3V() {
        return false;
    }

    public void C2m(AnonymousClass1SA r5, Throwable th) {
        AnonymousClass02w.A07(AnonymousClass1PS.A05, "Finalized without closing: %x %x (type = %s)", Integer.valueOf(System.identityHashCode(this)), Integer.valueOf(System.identityHashCode(r5)), r5.A01().getClass().getName());
    }
}
