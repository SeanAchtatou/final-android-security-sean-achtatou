package X;

import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount;

/* renamed from: X.1JK  reason: invalid class name */
public final class AnonymousClass1JK extends C17770zR {
    @Comparable(type = 13)
    public C25998CqM A00;
    @Comparable(type = 13)
    public C203689j5 A01;
    @Comparable(type = 13)
    public C203729j9 A02;
    @Comparable(type = 14)
    public AnonymousClass5CL A03 = new AnonymousClass5CL();
    @Comparable(type = 13)
    public C121605oW A04;
    @Comparable(type = 13)
    public RecoveredAccount A05;
    @Comparable(type = 13)
    public String A06;
    @Comparable(type = 3)
    public boolean A07;

    public AnonymousClass1JK() {
        super("AccountRegSoftmatchLoginComponent");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x02a9, code lost:
        if (r9.A04 != false) goto L_0x02ab;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C17770zR A0P(X.AnonymousClass0p4 r19, int r20, int r21) {
        /*
            r18 = this;
            r1 = r18
            com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount r6 = r1.A05
            X.5oW r5 = r1.A04
            java.lang.String r3 = r1.A06
            boolean r2 = r1.A07
            r12 = 0
            X.9j9 r0 = r1.A02
            X.CqM r1 = r1.A00
            int r8 = android.view.View.MeasureSpec.getSize(r21)
            X.9jc r4 = new X.9jc
            r9 = r4
            r10 = r3
            r11 = r2
            r13 = r6
            r14 = r5
            r15 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15)
            r5 = r19
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r3 = X.C112145Vo.A00(r5)
            java.lang.Object r0 = r3.A00
            X.5Vo r0 = (X.C112145Vo) r0
            r0.A03 = r1
            X.5oW r0 = r4.A01
            r17 = r0
            X.1JQ r0 = X.AnonymousClass1JQ.LARGE
            int r10 = r0.B3A()
            X.1JQ r0 = X.AnonymousClass1JQ.MEDIUM
            int r9 = r0.B3A()
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r6 = r0.B3A()
            X.1JQ r0 = X.AnonymousClass1JQ.XSMALL
            int r7 = r0.B3A()
            X.1we r2 = X.AnonymousClass11D.A00(r5)
            X.10G r1 = X.AnonymousClass10G.TOP
            X.1JM r0 = r17.A08()
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2Z(r1, r0)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            X.1JM r0 = r17.A06()
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2Z(r1, r0)
            X.10G r1 = X.AnonymousClass10G.A04
            X.1JM r0 = r17.A07()
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2Z(r1, r0)
            r2.A2C(r8)
            X.0uO r0 = X.C14940uO.CENTER
            r2.A3D(r0)
            X.1we r1 = X.AnonymousClass11D.A00(r5)
            com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount r0 = r4.A02
            java.lang.String r0 = r0.A03
            r16 = r0
            r8 = 2
            java.lang.String r0 = "loginStyle"
            java.lang.String r11 = "text"
            java.lang.String[] r13 = new java.lang.String[]{r0, r11}
            java.util.BitSet r12 = new java.util.BitSet
            r12.<init>(r8)
            X.1vU r8 = new X.1vU
            r8.<init>()
            X.0z4 r14 = r5.A0B
            X.0zR r0 = r5.A04
            if (r0 == 0) goto L_0x00a3
            java.lang.String r15 = r0.A06
            r8.A07 = r15
        L_0x00a3:
            r12.clear()
            X.5oW r0 = r4.A01
            r8.A00 = r0
            r0 = 0
            r12.set(r0)
            java.lang.Object[] r15 = new java.lang.Object[]{r16}
            r0 = 2131829655(0x7f112397, float:1.9292285E38)
            android.content.res.Resources r14 = r14.A00
            java.lang.String r0 = r14.getString(r0, r15)
            r8.A02 = r0
            r0 = 1
            r12.set(r0)
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r12, r13)
            r1.A3A(r8)
            com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount r0 = r4.A02
            int r0 = r0.A00
            r14 = 2131829654(0x7f112396, float:1.9292283E38)
            if (r0 != 0) goto L_0x00d4
            r14 = 2131829653(0x7f112395, float:1.9292281E38)
        L_0x00d4:
            r8 = 2
            java.lang.String r0 = "loginStyle"
            java.lang.String[] r12 = new java.lang.String[]{r0, r11}
            java.util.BitSet r11 = new java.util.BitSet
            r11.<init>(r8)
            X.1vV r8 = new X.1vV
            r8.<init>()
            X.0z4 r13 = r5.A0B
            X.0zR r0 = r5.A04
            if (r0 == 0) goto L_0x00ef
            java.lang.String r0 = r0.A06
            r8.A07 = r0
        L_0x00ef:
            r11.clear()
            X.5oW r0 = r4.A01
            r8.A00 = r0
            r0 = 0
            r11.set(r0)
            java.lang.String r0 = r13.A09(r14)
            r8.A02 = r0
            r0 = 1
            r11.set(r0)
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r11, r12)
            r1.A3A(r8)
            float r11 = (float) r10
            float r8 = (float) r9
            com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount r0 = r4.A02
            java.lang.String r12 = r0.A03
            X.1wd r9 = X.C16980y8.A00(r5)
            X.0uO r0 = X.C14940uO.FLEX_START
            r9.A2V(r0)
            X.10G r10 = X.AnonymousClass10G.A04
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r9.A2Z(r10, r0)
            com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount r0 = r4.A02
            int r0 = r0.A00
            if (r0 != 0) goto L_0x03fd
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r13 = X.AnonymousClass10H.A00(r5)
            X.10G r0 = X.AnonymousClass10G.TOP
            r13.A2X(r0, r11)
            X.10G r0 = X.AnonymousClass10G.BOTTOM
            r13.A2X(r0, r8)
            r0 = 2132279478(0x7f1800b6, float:2.0204635E38)
            r13.A3H(r0)
            X.10G r10 = X.AnonymousClass10G.RIGHT
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r13.A2X(r10, r0)
            X.10H r0 = r13.A33()
        L_0x0150:
            r9.A3A(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r10 = X.C17030yD.A00(r5)
            X.10J r0 = X.AnonymousClass10J.A0J
            r10.A3f(r0)
            r10.A3l(r12)
            r0 = 0
            r10.A2v(r0)
            X.10G r0 = X.AnonymousClass10G.TOP
            r10.A2X(r0, r11)
            X.10G r0 = X.AnonymousClass10G.BOTTOM
            r10.A2X(r0, r8)
            X.0yD r0 = r10.A39()
            r9.A3A(r0)
            X.0y8 r0 = r9.A00
            r1.A3A(r0)
            r9 = 0
            r11 = 0
            X.1wd r10 = X.C16980y8.A00(r5)
            r0 = 1
            r10.A27(r0)
            X.0uP r0 = X.C14950uP.CENTER
            r10.A3E(r0)
            X.10J r0 = X.AnonymousClass10J.A03
            X.1JZ r0 = r0.mTextColor
            int r0 = r0.AhV()
            r10.A23(r0)
            X.10G r0 = X.AnonymousClass10G.TOP
            r10.A2X(r0, r9)
            X.10G r9 = X.AnonymousClass10G.A04
            X.5UN r0 = X.AnonymousClass5UN.XSMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            r10.A2X(r9, r0)
            X.10G r0 = X.AnonymousClass10G.BOTTOM
            r10.A2X(r0, r11)
            X.0y8 r0 = r10.A00
            r1.A3A(r0)
            float r9 = (float) r7
            r7 = 1
            java.lang.String r0 = "stateContainer"
            java.lang.String[] r13 = new java.lang.String[]{r0}
            java.util.BitSet r12 = new java.util.BitSet
            r12.<init>(r7)
            X.0yC r11 = new X.0yC
            r11.<init>()
            X.0z4 r14 = r5.A0B
            X.0zR r0 = r5.A04
            if (r0 == 0) goto L_0x01ca
            java.lang.String r7 = r0.A06
            r11.A07 = r7
        L_0x01ca:
            r12.clear()
            X.9j9 r0 = r4.A00
            X.2Nf r0 = r0.A02
            r11.A03 = r0
            r0 = 0
            r12.set(r0)
            java.lang.String r7 = "password_field"
            X.11G r0 = r11.A14()
            r0.A0S(r7)
            r0 = 2131830050(0x7f112522, float:1.9293086E38)
            java.lang.String r0 = r14.A09(r0)
            r11.A05 = r0
            r0 = 6
            r11.A00 = r0
            X.10G r10 = X.AnonymousClass10G.TOP
            int r7 = r14.A00(r9)
            X.11G r0 = r11.A14()
            r0.BJx(r10, r7)
            X.10G r10 = X.AnonymousClass10G.BOTTOM
            r0 = 0
            int r7 = r14.A00(r0)
            X.11G r0 = r11.A14()
            r0.BJx(r10, r7)
            java.lang.Class<X.1JK> r10 = X.AnonymousClass1JK.class
            java.lang.Object[] r7 = new java.lang.Object[]{r5}
            r0 = 1196116736(0x474b4b00, float:52043.0)
            X.10N r0 = X.C17780zS.A0E(r10, r5, r0, r7)
            r11.A02 = r0
            java.lang.Object[] r7 = new java.lang.Object[]{r5}
            r0 = 96515278(0x5c0b4ce, float:1.812201E-35)
            X.10N r0 = X.C17780zS.A0E(r10, r5, r0, r7)
            r11.A01 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r12, r13)
            r1.A3A(r11)
            r10 = 0
            java.lang.String r0 = r4.A03
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x03d5
            X.1we r0 = X.AnonymousClass11D.A00(r5)
            X.11D r0 = r0.A00
        L_0x0239:
            r1.A3A(r0)
            X.11D r0 = r1.A00
            r2.A3A(r0)
            X.1we r1 = X.AnonymousClass11D.A00(r5)
            float r6 = (float) r6
            boolean r0 = r4.A05
            if (r0 != 0) goto L_0x0351
            X.1we r0 = X.AnonymousClass11D.A00(r5)
            X.11D r0 = r0.A00
        L_0x0250:
            r1.A3A(r0)
            X.10G r7 = X.AnonymousClass10G.A04
            X.1JM r0 = r17.A07()
            int r0 = r0.B3A()
            float r0 = (float) r0
            r1.A2Z(r7, r0)
            X.11D r0 = r1.A00
            r2.A3A(r0)
            X.1we r1 = X.AnonymousClass11D.A00(r5)
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.A1n(r0)
            X.11D r0 = r1.A00
            r2.A3A(r0)
            X.1we r7 = X.AnonymousClass11D.A00(r5)
            X.5X7 r9 = new X.5X7
            android.content.Context r0 = r5.A09
            r9.<init>(r0)
            X.0z4 r10 = r5.A0B
            X.0zR r0 = r5.A04
            if (r0 == 0) goto L_0x0289
            java.lang.String r1 = r0.A06
            r9.A07 = r1
        L_0x0289:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            r9.A05 = r0
            r0 = 2131821092(0x7f110224, float:1.9274917E38)
            java.lang.String r0 = r10.A09(r0)
            r9.A03 = r0
            X.9j9 r0 = r4.A00
            X.2Nf r0 = r0.A02
            java.lang.String r0 = r0.A00
            java.lang.String r0 = r0.trim()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x02ab
            boolean r1 = r4.A04
            r0 = 1
            if (r1 == 0) goto L_0x02ac
        L_0x02ab:
            r0 = 0
        L_0x02ac:
            r9.A06 = r0
            java.lang.String r1 = "login_button"
            X.11G r0 = r9.A14()
            r0.A0S(r1)
            X.10G r4 = X.AnonymousClass10G.TOP
            int r1 = r10.A00(r8)
            X.11G r0 = r9.A14()
            r0.BJx(r4, r1)
            X.10G r4 = X.AnonymousClass10G.BOTTOM
            r0 = 0
            int r1 = r10.A00(r0)
            X.11G r0 = r9.A14()
            r0.BJx(r4, r1)
            java.lang.Class<X.1JK> r4 = X.AnonymousClass1JK.class
            java.lang.Object[] r1 = new java.lang.Object[]{r5}
            r0 = -855949748(0xffffffffccfb3e4c, float:-1.31723872E8)
            X.10N r1 = X.C17780zS.A0E(r4, r5, r0, r1)
            X.11G r0 = r9.A14()
            r0.A0G(r1)
            r7.A3A(r9)
            X.5X7 r8 = new X.5X7
            android.content.Context r0 = r5.A09
            r8.<init>(r0)
            X.0z4 r9 = r5.A0B
            X.0zR r0 = r5.A04
            if (r0 == 0) goto L_0x02fa
            java.lang.String r1 = r0.A06
            r8.A07 = r1
        L_0x02fa:
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r8.A05 = r0
            r0 = 2131821048(0x7f1101f8, float:1.9274828E38)
            java.lang.String r0 = r9.A09(r0)
            r8.A03 = r0
            java.lang.String r1 = "forgot_password_button"
            X.11G r0 = r8.A14()
            r0.A0S(r1)
            X.10G r4 = X.AnonymousClass10G.TOP
            int r1 = r9.A00(r6)
            X.11G r0 = r8.A14()
            r0.BJx(r4, r1)
            X.10G r4 = X.AnonymousClass10G.BOTTOM
            r0 = 0
            int r1 = r9.A00(r0)
            X.11G r0 = r8.A14()
            r0.BJx(r4, r1)
            java.lang.Class<X.1JK> r4 = X.AnonymousClass1JK.class
            java.lang.Object[] r1 = new java.lang.Object[]{r5}
            r0 = 1415173789(0x5459d69d, float:3.74243407E12)
            X.10N r1 = X.C17780zS.A0E(r4, r5, r0, r1)
            X.11G r0 = r8.A14()
            r0.A0G(r1)
            r7.A3A(r8)
            X.11D r0 = r7.A00
            r2.A3A(r0)
            X.11D r0 = r2.A00
            r3.A3Y(r0)
            X.5Vo r0 = r3.A37()
            return r0
        L_0x0351:
            X.5xM r11 = new X.5xM
            r11.<init>()
            X.0z4 r12 = r5.A0B
            X.0zR r0 = r5.A04
            if (r0 == 0) goto L_0x0360
            java.lang.String r7 = r0.A06
            r11.A07 = r7
        L_0x0360:
            java.lang.String r7 = "save_password_checkbox"
            X.11G r0 = r11.A14()
            r0.A0S(r7)
            java.lang.Class<X.1JK> r10 = X.AnonymousClass1JK.class
            java.lang.Object[] r7 = new java.lang.Object[]{r5}
            r0 = -952092468(0xffffffffc74038cc, float:-49208.797)
            X.10N r7 = X.C17780zS.A0E(r10, r5, r0, r7)
            X.11G r0 = r11.A14()
            r0.A0G(r7)
            X.9j9 r0 = r4.A00
            boolean r0 = r0.A00
            r0 = r0 ^ 1
            r11.A03 = r0
            r7 = 0
            X.11G r0 = r11.A14()
            r0.AaD(r7)
            X.10G r10 = X.AnonymousClass10G.RIGHT
            X.1JQ r0 = X.AnonymousClass1JQ.SMALL
            int r0 = r0.B3A()
            float r0 = (float) r0
            int r7 = r12.A00(r0)
            X.11G r0 = r11.A14()
            r0.BJx(r10, r7)
            X.1wd r7 = X.C16980y8.A00(r5)
            r7.A3A(r11)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r10 = X.C17030yD.A00(r5)
            r0 = 2131821111(0x7f110237, float:1.9274956E38)
            r10.A3P(r0)
            X.5oW r0 = r4.A01
            X.10K r0 = r0.A0E()
            r10.A3f(r0)
            X.0yD r0 = r10.A39()
            r7.A3A(r0)
            X.10G r0 = X.AnonymousClass10G.TOP
            r7.A2X(r0, r9)
            X.10G r0 = X.AnonymousClass10G.BOTTOM
            r7.A2X(r0, r6)
            X.0uO r0 = X.C14940uO.CENTER
            r7.A3D(r0)
            X.0y8 r0 = r7.A00
            goto L_0x0250
        L_0x03d5:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r7 = X.C17030yD.A00(r5)
            java.lang.String r0 = ""
            r7.A2n(r0)
            java.lang.String r0 = r4.A03
            r7.A3l(r0)
            X.10G r0 = X.AnonymousClass10G.TOP
            r7.A2X(r0, r10)
            X.10G r0 = X.AnonymousClass10G.BOTTOM
            r7.A2X(r0, r8)
            X.10J r0 = X.AnonymousClass10J.A0T
            r7.A3f(r0)
            android.text.Layout$Alignment r0 = android.text.Layout.Alignment.ALIGN_NORMAL
            r7.A3V(r0)
            X.0yD r0 = r7.A39()
            goto L_0x0239
        L_0x03fd:
            X.1we r0 = X.AnonymousClass11D.A00(r5)
            X.11D r0 = r0.A00
            goto L_0x0150
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1JK.A0P(X.0p4, int, int):X.0zR");
    }

    public C17770zR A16() {
        AnonymousClass1JK r1 = (AnonymousClass1JK) super.A16();
        r1.A03 = new AnonymousClass5CL();
        return r1;
    }
}
