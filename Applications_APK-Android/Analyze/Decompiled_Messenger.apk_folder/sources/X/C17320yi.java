package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0yi  reason: invalid class name and case insensitive filesystem */
public final class C17320yi extends BroadcastReceiver {
    public final /* synthetic */ C17310yh A00;

    public C17320yi(C17310yh r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(-2030859356);
        C17310yh r1 = this.A00;
        if (!r1.A00) {
            C010708t.A05(C17310yh.A05, "Called onReceive after it was unregistered.");
            C000700l.A0D(intent, 2000886822, A01);
            return;
        }
        r1.A01(context, intent);
        C000700l.A0D(intent, -465189111, A01);
    }
}
