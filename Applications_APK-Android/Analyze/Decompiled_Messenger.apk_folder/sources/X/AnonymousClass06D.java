package X;

import android.content.Context;
import com.facebook.acra.ErrorReporter;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.acra.info.ExternalProcessInfo;
import com.facebook.common.util.TriState;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;

/* renamed from: X.06D  reason: invalid class name */
public final class AnonymousClass06D extends AnonymousClass06E {
    public static final C04310Tq A0E = new AnonymousClass0X7();
    public static volatile boolean A0F = AnonymousClass0VW.A00();
    private Boolean A00;
    public final Context A01;
    public final AnonymousClass09Q A02;
    public final AnonymousClass069 A03;
    public final Map A04;
    public final C04310Tq A05;
    public final boolean A06;
    private final Random A07;
    private final ExecutorService A08;
    private final C04310Tq A09;
    private final C04310Tq A0A;
    private volatile C21879Aji A0B;
    private volatile C30353Eud A0C;
    private volatile C30354Eue A0D;

    public void CGQ(AnonymousClass06F r2) {
        CGR(r2, null);
    }

    private AnonymousClass06F A00(AnonymousClass06F r4) {
        if (r4.A00 != 1000 || 0 == 0) {
            return r4;
        }
        C30353Eud eud = null;
        int defaultReportingFrequency = eud.getDefaultReportingFrequency();
        AnonymousClass06G A022 = AnonymousClass06F.A02(r4.A01, r4.A02);
        A022.A04 = r4.A04;
        A022.A00 = defaultReportingFrequency;
        A022.A03 = r4.A03;
        return A022.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0030, code lost:
        if (r2 != com.facebook.common.util.TriState.YES) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (((java.lang.Boolean) r14.A09.get()).booleanValue() != false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Integer A01(X.AnonymousClass06F r15) {
        /*
            r14 = this;
            boolean r0 = X.AnonymousClass06D.A0F
            r13 = 0
            if (r0 != 0) goto L_0x0138
            X.0Tq r0 = r14.A0A
            java.lang.Object r1 = r0.get()
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            if (r1 == r0) goto L_0x001e
            X.0Tq r0 = r14.A09
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r12 = 0
            if (r0 == 0) goto L_0x001f
        L_0x001e:
            r12 = 1
        L_0x001f:
            boolean r0 = r15.A04
            if (r0 == 0) goto L_0x004f
            if (r12 == 0) goto L_0x004f
            X.09Q r0 = r14.A02
            if (r0 == 0) goto L_0x0032
            com.facebook.common.util.TriState r2 = r0.BEu()
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.YES
            r0 = 1
            if (r2 == r1) goto L_0x0033
        L_0x0032:
            r0 = 0
        L_0x0033:
            if (r0 == 0) goto L_0x0134
            android.content.Context r0 = r14.A01
            if (r0 == 0) goto L_0x004f
            android.os.Handler r2 = new android.os.Handler
            android.os.Looper r0 = android.os.Looper.getMainLooper()
            r2.<init>(r0)
            X.0X8 r1 = new X.0X8
            android.content.Context r0 = r14.A01
            r1.<init>(r15, r0)
            r0 = -987980757(0xffffffffc51c9c2b, float:-2505.7605)
            X.AnonymousClass00S.A04(r2, r1, r0)
        L_0x004f:
            X.069 r0 = r14.A03
            long r10 = r0.now()
            r0 = 1000(0x3e8, double:4.94E-321)
            long r10 = r10 / r0
            java.lang.String r7 = r15.A01
            java.util.Map r6 = r14.A04
            monitor-enter(r6)
            java.util.Map r1 = r14.A04     // Catch:{ all -> 0x0131 }
            java.lang.String r0 = r15.A01     // Catch:{ all -> 0x0131 }
            java.lang.Object r5 = r1.get(r0)     // Catch:{ all -> 0x0131 }
            java.util.List r5 = (java.util.List) r5     // Catch:{ all -> 0x0131 }
            r1 = 0
            r4 = 1
            if (r5 == 0) goto L_0x009c
            java.lang.Object r0 = r5.get(r4)     // Catch:{ all -> 0x0131 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x0131 }
            int r3 = r0.intValue()     // Catch:{ all -> 0x0131 }
            java.lang.Object r0 = r5.get(r1)     // Catch:{ all -> 0x0131 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x0131 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0131 }
            long r8 = r10 - r0
            r1 = 120(0x78, double:5.93E-322)
            int r0 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0099
            r0 = 100
            if (r3 >= r0) goto L_0x0099
            long r2 = (long) r3     // Catch:{ all -> 0x0131 }
            r0 = 1
            long r2 = r2 + r0
            java.lang.Long r0 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x0131 }
            r5.set(r4, r0)     // Catch:{ all -> 0x0131 }
            r0 = 0
            monitor-exit(r6)     // Catch:{ all -> 0x0131 }
            goto L_0x00c0
        L_0x0099:
            int r5 = r3 + 1
            goto L_0x009d
        L_0x009c:
            r5 = 1
        L_0x009d:
            java.util.Map r4 = r14.A04     // Catch:{ all -> 0x0131 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0131 }
            java.lang.Long r2 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0131 }
            r0 = 0
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0131 }
            java.lang.Long[] r0 = new java.lang.Long[]{r2, r0}     // Catch:{ all -> 0x0131 }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ all -> 0x0131 }
            r3.<init>(r0)     // Catch:{ all -> 0x0131 }
            r4.put(r7, r3)     // Catch:{ all -> 0x0131 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0131 }
            monitor-exit(r6)     // Catch:{ all -> 0x0131 }
            r6 = r0
            goto L_0x00c1
        L_0x00c0:
            r6 = r13
        L_0x00c1:
            if (r12 == 0) goto L_0x00c6
            if (r0 != 0) goto L_0x00c6
            return r13
        L_0x00c6:
            if (r12 != 0) goto L_0x00ce
            int r0 = r15.A00
            java.lang.Integer r6 = java.lang.Integer.valueOf(r0)
        L_0x00ce:
            r0 = 256(0x100, double:1.265E-321)
            boolean r0 = X.AnonymousClass08Z.A05(r0)
            if (r0 == 0) goto L_0x0100
            java.lang.String r3 = "softReport category: "
            java.lang.String r2 = r15.A01
            java.lang.String r1 = " message: "
            java.lang.String r0 = r15.A02
            java.lang.String r5 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            r0 = 256(0x100, double:1.265E-321)
            boolean r0 = X.AnonymousClass08Z.A05(r0)
            if (r0 == 0) goto L_0x0100
            int r0 = r2.intValue()
            switch(r0) {
                case 1: goto L_0x0101;
                case 2: goto L_0x0104;
                default: goto L_0x00f3;
            }
        L_0x00f3:
            r4 = 116(0x74, float:1.63E-43)
        L_0x00f5:
            java.lang.String r3 = ""
            boolean r0 = com.facebook.systrace.TraceDirect.checkNative()
            if (r0 == 0) goto L_0x0107
            com.facebook.systrace.TraceDirect.nativeTraceInstant(r3, r5, r4)
        L_0x0100:
            return r6
        L_0x0101:
            r4 = 112(0x70, float:1.57E-43)
            goto L_0x00f5
        L_0x0104:
            r4 = 103(0x67, float:1.44E-43)
            goto L_0x00f5
        L_0x0107:
            r0 = 73
            X.09R r2 = new X.09R
            r2.<init>(r0)
            int r0 = android.os.Process.myPid()
            r2.A01(r0)
            r2.A03(r5)
            java.lang.StringBuilder r1 = r2.A00
            r0 = 124(0x7c, float:1.74E-43)
            r1.append(r0)
            char r0 = X.AnonymousClass09R.A00(r4)
            r1.append(r0)
            r2.A03(r3)
            java.lang.String r0 = r2.toString()
            X.AnonymousClass0HL.A00(r0)
            return r6
        L_0x0131:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0131 }
            throw r0
        L_0x0134:
            r14.A04(r15)
            return r13
        L_0x0138:
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass06D.A01(X.06F):java.lang.Integer");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0018, code lost:
        if (r4.A0A.get() == com.facebook.common.util.TriState.YES) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String A02(java.lang.String r5, int r6, boolean r7) {
        /*
            r4 = this;
            X.0Tq r0 = r4.A09
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r3 = 1
            if (r0 != 0) goto L_0x001a
            X.0Tq r0 = r4.A0A
            java.lang.Object r2 = r0.get()
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.YES
            r0 = 0
            if (r2 != r1) goto L_0x001b
        L_0x001a:
            r0 = 1
        L_0x001b:
            if (r0 == 0) goto L_0x0027
            java.lang.Boolean r0 = r4.A00
            if (r0 == 0) goto L_0x003d
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x003d
        L_0x0027:
            r1 = 0
            if (r7 != 0) goto L_0x003e
            java.util.Random r0 = r4.A07
            int r0 = r0.nextInt()
            int r0 = r0 % r6
            if (r0 != 0) goto L_0x003e
            if (r6 == r3) goto L_0x003d
            java.lang.String r1 = " [freq="
            java.lang.String r0 = "]"
            java.lang.String r5 = X.AnonymousClass08S.A0M(r5, r1, r6, r0)
        L_0x003d:
            return r5
        L_0x003e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass06D.A02(java.lang.String, int, boolean):java.lang.String");
    }

    private void A03() {
        if (this.A00 == null) {
            Context context = this.A01;
            boolean z = false;
            if (context == null) {
                this.A00 = false;
                return;
            }
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = context.openFileInput("soft_errors_pref");
                if (fileInputStream.read() == 1) {
                    z = true;
                }
                this.A00 = Boolean.valueOf(z);
            } catch (IOException unused) {
                if (fileInputStream == null) {
                    return;
                }
            } catch (Throwable th) {
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException unused2) {
                    }
                }
                throw th;
            }
            try {
                fileInputStream.close();
            } catch (IOException unused3) {
            }
        }
    }

    private void A04(AnonymousClass06F r7) {
        ErrorReporter.putCustomData(ErrorReportingConstants.SOFT_ERROR_MESSAGE, r7.A02);
        C010708t.A0O("FbErrorReporterImpl", "category: %s message: %s", r7.A01, r7.A02);
        ((ErrorReporter) this.A05.get()).reportErrorAndTerminate(Thread.currentThread(), new RuntimeException(AnonymousClass08S.A0S("Soft error FAILING HARDER: ", r7.A01, ", ", r7.A02), r7.A03));
    }

    public void Bz1(String str) {
        ((ErrorReporter) this.A05.get()).setUserId(str);
    }

    public void Bz2(String str, String str2) {
        this.A05.get();
        ErrorReporter.putCustomData(str, str2);
    }

    public void Bz9(String str, AnonymousClass0Z1 r4) {
        ((ErrorReporter) this.A05.get()).putLazyCustomData(str, new AnonymousClass0XB(r4));
    }

    public void C1g(String str) {
        ((ErrorReporter) this.A05.get()).removeLazyCustomData(str);
    }

    public void C2k(Throwable th) {
        AnonymousClass07A.A04(this.A08, new AnonymousClass0XC(th), 1398221289);
    }

    public void C2l(String str) {
        ((ErrorReporter) this.A05.get()).registerActivity(str);
    }

    public void CGR(AnonymousClass06F r11, ExternalProcessInfo externalProcessInfo) {
        String A022;
        AnonymousClass09Q r0 = this.A02;
        if (r0 == null || r0.BGs() != TriState.YES) {
            A03();
            if (0 != 0) {
                C30354Eue eue = null;
                if (eue.isCategoryBlacklisted(r11.A01)) {
                    return;
                }
            }
            AnonymousClass06F A002 = A00(r11);
            if (this.A0B != null) {
                this.A0B.A01(A002.A01, A002.A02);
            }
            Integer A012 = A01(A002);
            if (A012 != null && (A022 = A02(A002.A01, A002.A00, A002.A05)) != null) {
                String str = A002.A02;
                AnonymousClass0XA r3 = new AnonymousClass0XA(this, A022, str, A012, externalProcessInfo, new AnonymousClass06I(AnonymousClass08S.A0P(A002.A01, " | ", str), A002.A03));
                if (0 != 0) {
                    r3.run();
                } else {
                    AnonymousClass07A.A04(this.A08, r3, 2038479193);
                }
            }
        }
    }

    public void removeCustomData(String str) {
        this.A05.get();
        ErrorReporter.removeCustomData(str);
    }

    public void CGW(AnonymousClass06F r8) {
        String A022;
        A03();
        if (0 != 0) {
            C30354Eue eue = null;
            if (eue.isCategoryBlacklisted(r8.A01)) {
                return;
            }
        }
        AnonymousClass06F A002 = A00(r8);
        if (this.A0B != null) {
            this.A0B.A01(A002.A01, A002.A02);
        }
        if (A01(A002) != null && (A022 = A02(A002.A01, A002.A00, A002.A05)) != null) {
            String str = A002.A02;
            AnonymousClass07A.A04(this.A08, new AnonymousClass0X9(this, A022, str, new AnonymousClass06I(str, A002.A03)), -468529030);
        }
    }

    public void A05(C21879Aji aji) {
        this.A0B = aji;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass06D(X.C04310Tq r11, X.C04310Tq r12, java.util.concurrent.ExecutorService r13, X.AnonymousClass069 r14, java.util.Random r15, android.content.Context r16, X.AnonymousClass09Q r17) {
        /*
            r10 = this;
            X.0Tq r8 = X.AnonymousClass06D.A0E
            r9 = 0
            r0 = r10
            r6 = r16
            r2 = r12
            r7 = r17
            r1 = r11
            r3 = r13
            r4 = r14
            r5 = r15
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass06D.<init>(X.0Tq, X.0Tq, java.util.concurrent.ExecutorService, X.069, java.util.Random, android.content.Context, X.09Q):void");
    }

    private AnonymousClass06D(C04310Tq r2, C04310Tq r3, ExecutorService executorService, AnonymousClass069 r5, Random random, Context context, AnonymousClass09Q r8, C04310Tq r9, boolean z) {
        this.A00 = null;
        this.A0A = r2;
        this.A09 = r3;
        this.A08 = executorService;
        this.A03 = r5;
        this.A07 = random;
        this.A01 = context;
        this.A05 = r9;
        this.A02 = r8;
        this.A06 = z;
        this.A04 = new HashMap();
    }
}
