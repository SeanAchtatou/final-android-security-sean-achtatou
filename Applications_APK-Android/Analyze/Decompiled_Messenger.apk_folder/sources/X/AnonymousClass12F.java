package X;

import java.util.HashSet;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.12F  reason: invalid class name */
public final class AnonymousClass12F implements C06670bt {
    private static volatile AnonymousClass12F A02;
    private AnonymousClass0UN A00;
    public final Set A01 = new HashSet();

    public static final AnonymousClass12F A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass12F.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass12F(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public int Ai5() {
        return 239;
    }

    public void BTE(int i) {
        ((AnonymousClass1GE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BHw, this.A00)).clearUserData();
        for (AnonymousClass12H r0 : this.A01) {
            if (r0 != null) {
                r0.BfC();
            }
        }
    }

    private AnonymousClass12F(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
