package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0PT  reason: invalid class name */
public final class AnonymousClass0PT extends C03160Kg {
    public long A00() {
        return -4040221479651313008L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        AnonymousClass0G0 r32 = (AnonymousClass0G0) r3;
        dataOutput.writeLong(r32.javaHeapMaxSizeKb);
        dataOutput.writeLong(r32.javaHeapAllocatedKb);
        dataOutput.writeLong(r32.nativeHeapSizeKb);
        dataOutput.writeLong(r32.nativeHeapAllocatedKb);
        dataOutput.writeLong(r32.vmSizeKb);
        dataOutput.writeLong(r32.vmRssKb);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        AnonymousClass0G0 r32 = (AnonymousClass0G0) r3;
        r32.javaHeapMaxSizeKb = dataInput.readLong();
        r32.javaHeapAllocatedKb = dataInput.readLong();
        r32.nativeHeapSizeKb = dataInput.readLong();
        r32.nativeHeapAllocatedKb = dataInput.readLong();
        r32.vmSizeKb = dataInput.readLong();
        r32.vmRssKb = dataInput.readLong();
        return true;
    }
}
