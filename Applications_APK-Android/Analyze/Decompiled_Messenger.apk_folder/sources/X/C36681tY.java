package X;

/* renamed from: X.1tY  reason: invalid class name and case insensitive filesystem */
public final class C36681tY extends C10370jz {
    private static final long serialVersionUID = -6744103724013275513L;

    public String getFormatName() {
        return "JSON";
    }

    public /* bridge */ /* synthetic */ C09980jK getCodec() {
        return (AnonymousClass0jJ) this._objectCodec;
    }

    public C36681tY() {
        this(null);
    }

    public C36681tY(AnonymousClass0jJ r3) {
        super(r3);
        if (r3 == null) {
            this._objectCodec = new AnonymousClass0jJ(this, null, null);
        }
    }
}
