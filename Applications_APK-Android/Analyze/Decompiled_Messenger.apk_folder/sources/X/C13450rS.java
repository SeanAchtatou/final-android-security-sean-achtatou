package X;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.fragment.app.Fragment;
import com.facebook.litho.LithoView;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.widget.CustomFrameLayout;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.BitSet;

/* renamed from: X.0rS  reason: invalid class name and case insensitive filesystem */
public final class C13450rS extends C13460rT {
    public static final String __redex_internal_original_name = "com.facebook.messaging.navigation.home.tabs.M4TabNavigationFragment";
    public int A00;
    public View A01;
    public AnonymousClass0UN A02;
    public C16130wX A03;
    public AnonymousClass12x A04;
    public C16540xI A05;
    public C16030wN A06;
    public ListenableFuture A07;
    public boolean A08 = true;
    public final C33691nz A09 = new C33681ny(this);
    public final C15960wG A0A = new C15950wF(this);
    private final C33761o6 A0B = new C33761o6(this);

    public static AnonymousClass13r A00(C13450rS r3) {
        return ((C13420rP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASO, r3.A02)).A01(r3.A2S());
    }

    private void A04(int i, Bundle bundle) {
        if (this.A00 != i || bundle != null) {
            this.A00 = i;
            A07(this, bundle);
            C16040wO A2S = A2S();
            C16540xI r2 = this.A05;
            C16040wO r1 = r2.A00;
            if (A2S != r1) {
                r2.A00 = A2S;
                if (r2.A01) {
                    C16540xI.A01(r2, r1);
                    C16540xI.A00(r2, A2S);
                }
            }
            C13400rN r12 = (C13400rN) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A7C, this.A02);
            String A012 = A2S.A01();
            C13400rN.A01(r12, A012);
            r12.A02 = A012;
        }
    }

    public static void A05(C13450rS r14) {
        String str;
        Preconditions.checkNotNull(r14.A04);
        C15350v9 r6 = new C15350v9((MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, r14.A02), AnonymousClass10J.A0Z);
        C180810c r10 = (C180810c) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AZV, r14.A02);
        LithoView lithoView = r14.A04.A02;
        C33691nz r8 = r14.A09;
        C16030wN r11 = r14.A06;
        int i = r14.A00;
        AnonymousClass0p4 r13 = lithoView.A0H;
        AnonymousClass0qX.A00(r6);
        String[] strArr = {"colorScheme", "currentTabIndex", C99084oO.$const$string(4), "tabs"};
        BitSet bitSet = new BitSet(4);
        C181010e r4 = new C181010e(r13.A09);
        C17770zR r2 = r13.A04;
        if (r2 != null) {
            r4.A07 = r2.A06;
        }
        bitSet.clear();
        String str2 = "tab_bar";
        if ("tab_bar" == 0) {
            if (r2 != null) {
                str = r2.A1A();
            } else {
                str = "unknown component";
            }
            C09070gU.A01(AnonymousClass07B.A01, "Component:NullKeySet", AnonymousClass08S.A0P("Setting a null key from ", str, C99084oO.$const$string(0)));
            str2 = "null";
        }
        r4.A1I(str2);
        r4.A14().A0S("tab_bar");
        r4.A04 = ImmutableList.copyOf(AnonymousClass0j4.A00(r11.A00, new C181110f(r10, r13.A09)));
        bitSet.set(3);
        r4.A00 = i;
        bitSet.set(1);
        r4.A03 = r6;
        bitSet.set(0);
        r4.A02 = r8;
        bitSet.set(2);
        AnonymousClass11F.A0C(4, bitSet, strArr);
        lithoView.A0a(r4);
        C15980wI.A00(lithoView, AnonymousClass1KA.A00(r6.A00.B9n()));
    }

    public static void A06(C13450rS r16) {
        C13450rS r3 = r16;
        Preconditions.checkNotNull(r3.A04);
        boolean A082 = A08(r3);
        r3.A08 = A082;
        C15350v9 r5 = new C15350v9((MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, r3.A02), AnonymousClass10J.A0Z);
        LithoView lithoView = r3.A04.A03;
        C33691nz r8 = r3.A09;
        AnonymousClass13r A002 = A00(r3);
        C13060qW r6 = r3.A0P;
        Preconditions.checkNotNull(r5);
        C15390vD r4 = (C15390vD) AnonymousClass1XX.A03(AnonymousClass1Y3.AIz, ((C15360vA) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BG1, r3.A02)).A00);
        AnonymousClass0p4 r1 = lithoView.A0H;
        String[] strArr = {"buttons", "colorScheme", C99084oO.$const$string(4), "profileButtonBadge", "shadow", "title"};
        BitSet bitSet = new BitSet(6);
        C32101l9 r12 = new C32101l9(r1.A09);
        C17770zR r0 = r1.A04;
        if (r0 != null) {
            r12.A07 = r0.A06;
        }
        bitSet.clear();
        r12.A05 = A002.B6U(r1.A09);
        bitSet.set(5);
        r12.A04 = ImmutableList.copyOf(AnonymousClass0j4.A00(A002.B6e(), new C15580vW(r1.A09, r8, r6)));
        bitSet.set(0);
        r12.A02 = r4.Ae7();
        bitSet.set(3);
        r12.A01 = r8;
        bitSet.set(2);
        r12.A03 = r5;
        boolean z = true;
        bitSet.set(1);
        if (A082 || A002.AiR() == C73953h2.class) {
            z = false;
        }
        r12.A06 = z;
        bitSet.set(4);
        r12.A14().A0S("tab_toolbar");
        AnonymousClass11F.A0C(6, bitSet, strArr);
        lithoView.A0Z(r12);
        C15980wI.A00(lithoView, AnonymousClass1KA.A00(r5.A00.B9n()));
        View view = r3.A01;
        if (view != null) {
            int i = 2;
            if (r3.A08) {
                i = 0;
            }
            AnonymousClass1IA.A00(view, i);
        }
    }

    public static void A07(C13450rS r12, Bundle bundle) {
        Fragment A0Q;
        AnonymousClass12x r1 = r12.A04;
        boolean z = false;
        if (r1 != null) {
            z = true;
        }
        if (z) {
            Preconditions.checkNotNull(r1);
            C184713p r8 = (C184713p) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ATS, r12.A02);
            C13060qW A17 = r12.A17();
            int i = r12.A04.A00;
            C16030wN r0 = r12.A06;
            int i2 = r12.A00;
            AnonymousClass13r A002 = A00(r12);
            C16040wO r4 = (C16040wO) r0.A00.get(i2);
            C16290wo A0T = A17.A0T();
            for (C16040wO r02 : C16040wO.values()) {
                if (!(r02 == r4 || (A0Q = A17.A0Q(r02.name())) == null || A0Q.A0b)) {
                    A0T.A0H(A0Q);
                    A0Q.A1W(false);
                }
            }
            String name = r4.name();
            Fragment A0Q2 = A17.A0Q(name);
            if (A0Q2 == null) {
                Fragment AV6 = A002.AV6();
                AV6.A1P(bundle);
                r4.name();
                A0T.A0A(i, AV6, name);
            } else {
                A0Q2.A1P(bundle);
                if (A0Q2.A0b) {
                    A0T.A0J(A0Q2);
                    if (!r8.A00.Aem(2306125244399813860L)) {
                        A0Q2.A1W(true);
                    }
                }
            }
            A0T.A02();
            A17.A0Y();
            if (r8.A00.Aem(2306125244399813860L)) {
                r4.name();
                Fragment A0Q3 = A17.A0Q(name);
                if (A0Q3 != null) {
                    A0Q3.A1W(true);
                }
            }
            A06(r12);
            A05(r12);
            A00(r12).Ae6().BUD();
        }
    }

    public static boolean A08(C13450rS r3) {
        Object A032;
        C13020qR r0 = r3.A0N;
        if (r0 == null) {
            A032 = null;
        } else {
            A032 = r0.A03();
        }
        if (A032 == null) {
            return true;
        }
        C13060qW A17 = r3.A17();
        C16040wO A2S = r3.A2S();
        AnonymousClass13r A002 = A00(r3);
        Fragment A0Q = A17.A0Q(A2S.name());
        if (A0Q == null) {
            return true;
        }
        return A002.BGe(A0Q);
    }

    public C16040wO A2S() {
        return (C16040wO) this.A06.A00.get(this.A00);
    }

    public boolean A2U() {
        boolean A13;
        boolean z;
        C13060qW r0 = this.A0P;
        if (r0 == null) {
            A13 = false;
        } else {
            A13 = r0.A13();
        }
        if (!A13) {
            Fragment A0Q = A17().A0Q(A2S().name());
            if (A0Q instanceof C13240qx) {
                z = ((C13240qx) A0Q).BPB();
            } else {
                z = false;
            }
            if (z) {
                A06(this);
                return true;
            } else if (this.A00 != 0) {
                A04(0, null);
                return true;
            }
        }
        return false;
    }

    public View A1k(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int A022 = C000700l.A02(1122890306);
        Context A1j = A1j();
        LithoView lithoView = new LithoView(A1j);
        LithoView lithoView2 = new LithoView(A1j);
        ViewStubCompat viewStubCompat = new ViewStubCompat(A1j, null);
        viewStubCompat.setId(2131296723);
        C13520ra A002 = C13950sM.A00(A1j);
        A002.A00.setBackgroundResource(2132082802);
        A002.A07(1);
        A002.A04(lithoView);
        A002.A04(viewStubCompat);
        C183912v r5 = new C183912v(new CustomFrameLayout(A1j));
        r5.A02(2131298339);
        r5.A05(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        A002.A06(r5);
        A002.A04(lithoView2);
        AnonymousClass12x r3 = new AnonymousClass12x((ViewGroup) A002.A00, lithoView, 2131298339, lithoView2);
        this.A04 = r3;
        ViewGroup viewGroup2 = r3.A01;
        ((C16580xM) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGs, this.A02)).A01 = viewGroup2;
        C000700l.A08(-1143976029, A022);
        return viewGroup2;
    }

    public void A1m() {
        int A022 = C000700l.A02(1141114892);
        super.A1m();
        this.A04 = null;
        C000700l.A08(1828619612, A022);
    }

    public void A1o() {
        Fragment A0Q;
        int A022 = C000700l.A02(873799893);
        super.A1o();
        C13060qW A17 = A17();
        C16030wN r2 = this.A06;
        int i = this.A00;
        if (C16000wK.A01(A17)) {
            C16040wO r5 = (C16040wO) r2.A00.get(i);
            C16290wo A0T = A17.A0T();
            for (C16040wO r0 : C16040wO.values()) {
                if (!(r0 == r5 || (A0Q = A17.A0Q(r0.name())) == null)) {
                    A0T.A0I(A0Q);
                }
            }
            A0T.A02();
            A17.A0Y();
        }
        ((C16580xM) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGs, this.A02)).A02();
        C000700l.A08(457759231, A022);
    }

    public void A1p() {
        int A022 = C000700l.A02(428849534);
        super.A1p();
        Fragment A0Q = A17().A0Q(A2S().name());
        if (A0Q != null) {
            A0Q.A1W(true);
        }
        ((C16580xM) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGs, this.A02)).A03();
        C000700l.A08(689010188, A022);
    }

    public void A1r() {
        int A022 = C000700l.A02(-648208918);
        super.A1r();
        C000700l.A08(1044193522, A022);
    }

    public void A1u(Bundle bundle) {
        super.A1u(bundle);
        bundle.putInt("current_tab_index", this.A00);
    }

    public void A1v(View view, Bundle bundle) {
        super.A1v(view, bundle);
        Bundle bundle2 = this.A0G;
        if (bundle2 == null || !bundle2.getBoolean("defer_init")) {
            A07(this, null);
        }
    }

    public void A1w(Fragment fragment) {
        super.A1w(fragment);
        if (this.A06 == null) {
            this.A06 = ((C16020wM) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Acq, this.A02)).A02();
        }
        C24971Xv it = this.A06.A00.iterator();
        while (it.hasNext()) {
            AnonymousClass13r A012 = ((C13420rP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASO, this.A02)).A01((C16040wO) it.next());
            if (A012.AiR() == fragment.getClass()) {
                A012.AP7(A1j(), fragment, this.A09);
                A012.CBh(fragment, this.A0B);
                return;
            }
        }
    }

    public void A1x(boolean z) {
        super.A1x(z);
        Fragment A0Q = A17().A0Q(A2S().name());
        if (A0Q != null) {
            A0Q.A1W(!z);
        }
    }

    public void A2E(Bundle bundle) {
        super.A2E(bundle);
        AnonymousClass0UN r2 = new AnonymousClass0UN(9, AnonymousClass1XX.get(A1j()));
        this.A02 = r2;
        this.A06 = ((C16020wM) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Acq, r2)).A02();
        if (bundle != null) {
            this.A00 = bundle.getInt("current_tab_index");
        }
    }

    public void A2M(Bundle bundle) {
        super.A2M(bundle);
        if (this.A06 == null) {
            this.A06 = ((C16020wM) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Acq, this.A02)).A02();
        }
        C16030wN r4 = this.A06;
        int i = AnonymousClass1Y3.ASO;
        AnonymousClass0UN r3 = this.A02;
        C13420rP r2 = (C13420rP) AnonymousClass1XX.A02(1, i, r3);
        this.A03 = new C16130wX(r4, r2, new C16500xD(this));
        this.A05 = new C16540xI(r2, new C16560xK(this));
        ((C16580xM) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BGs, r3)).A02 = new C16790xm(this);
        new C13990sQ(this, new C14380tD(this));
        ((C17070yH) AnonymousClass1XX.A03(AnonymousClass1Y3.AqE, this.A02)).A01(this, new C16940y4(this));
    }

    public void A2T(C16040wO r5, Bundle bundle) {
        if (C16000wK.A01(A17()) && this.A06.A00.contains(r5)) {
            String A002 = C16040wO.A00((C16040wO) this.A06.A00.get(this.A00));
            String A003 = C16040wO.A00(r5);
            AnonymousClass2GO r0 = (AnonymousClass2GO) AnonymousClass1XX.A03(AnonymousClass1Y3.AS3, this.A02);
            if (A002 != null) {
                r0.A02(A002);
            }
            if (A003 != null) {
                r0.A01(A003);
            }
            ((C13400rN) AnonymousClass1XX.A02(5, AnonymousClass1Y3.A7C, this.A02)).A01 = Integer.valueOf(((C13420rP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASO, this.A02)).A01(r5).Ae6().Ae7().A00);
            A04(this.A06.A00.indexOf(r5), bundle);
        }
    }
}
