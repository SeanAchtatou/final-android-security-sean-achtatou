package X;

import java.lang.ref.WeakReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.14x  reason: invalid class name and case insensitive filesystem */
public final class C187414x {
    private static volatile C187414x A02;
    public AnonymousClass0UN A00;
    public final C04310Tq A01;

    public static final C187414x A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C187414x.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C187414x(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static void A01(C187414x r4) {
        ((C31841kT) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Aum, r4.A00)).A00 = new WeakReference(null);
        C18010zv.A03((C18010zv) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ahm, r4.A00), true, null);
    }

    private C187414x(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.BTJ, r3);
    }
}
