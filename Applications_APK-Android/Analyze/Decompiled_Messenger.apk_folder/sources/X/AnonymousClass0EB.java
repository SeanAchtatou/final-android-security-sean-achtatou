package X;

/* renamed from: X.0EB  reason: invalid class name */
public final class AnonymousClass0EB extends AnonymousClass0EC {
    private int A00;
    private final int A01;
    private final Object A02;
    private final Object A03;
    private final Object[] A04;

    public boolean A00() {
        Object obj;
        int i = this.A00;
        if (i + 1 >= this.A01) {
            return false;
        }
        int i2 = i + 1;
        this.A00 = i2;
        if (i2 == 0) {
            this.A03 = this.A02;
            obj = this.A03;
        } else {
            int i3 = (i2 - 1) << 1;
            Object[] objArr = this.A04;
            this.A03 = objArr[i3];
            obj = objArr[i3 + 1];
        }
        this.A04 = obj;
        return true;
    }

    public AnonymousClass0EB(Object obj, Object obj2, Object[] objArr, int i) {
        int length;
        if (objArr == null || (objArr.length & 1) != 1) {
            this.A02 = obj;
            this.A03 = obj2;
            this.A04 = objArr;
            if (objArr == null) {
                length = 0;
            } else {
                length = objArr.length >> 1;
            }
            this.A01 = length + 1;
            this.A00 = i;
            return;
        }
        throw new IllegalArgumentException();
    }
}
