package X;

import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;

/* renamed from: X.0IM  reason: invalid class name */
public final class AnonymousClass0IM extends C01900Cb implements RunnableFuture {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.common.executors.WakingExecutorService$ListenableScheduledRunnableFuture";

    public AnonymousClass0IM(C01530Ap r1, Runnable runnable, Object obj) {
        super(r1, runnable, obj);
    }

    public AnonymousClass0IM(C01530Ap r1, Callable callable) {
        super(r1, callable);
    }
}
