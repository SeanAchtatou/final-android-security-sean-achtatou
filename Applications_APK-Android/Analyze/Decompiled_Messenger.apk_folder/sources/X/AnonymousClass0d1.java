package X;

/* renamed from: X.0d1  reason: invalid class name */
public final class AnonymousClass0d1 extends AnonymousClass1RO {
    public int A00 = 0;
    public AnonymousClass1PS A01 = null;
    public boolean A02;
    public boolean A03 = false;
    public boolean A04 = false;
    public final AnonymousClass1QK A05;
    public final AnonymousClass1MN A06;
    public final AnonymousClass32I A07;
    public final /* synthetic */ C635636z A08;

    public static boolean A01(AnonymousClass0d1 r2) {
        synchronized (r2) {
            if (r2.A02) {
                return false;
            }
            AnonymousClass1PS r1 = r2.A01;
            r2.A01 = null;
            r2.A02 = true;
            AnonymousClass1PS.A05(r1);
            return true;
        }
    }

    public static synchronized boolean A02(AnonymousClass0d1 r1) {
        synchronized (r1) {
            if (r1.A02 || !r1.A03 || r1.A04 || !AnonymousClass1PS.A07(r1.A01)) {
                return false;
            }
            r1.A04 = true;
            return true;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0d1(C635636z r2, C23581Qb r3, AnonymousClass1MN r4, AnonymousClass32I r5, AnonymousClass1QK r6) {
        super(r3);
        this.A08 = r2;
        this.A06 = r4;
        this.A07 = r5;
        this.A05 = r6;
        r6.A06(new C625932k(this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000e, code lost:
        if (r0 != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(X.AnonymousClass0d1 r2, X.AnonymousClass1PS r3, int r4) {
        /*
            boolean r1 = X.AnonymousClass1QU.A03(r4)
            if (r1 != 0) goto L_0x0010
            monitor-enter(r2)
            boolean r0 = r2.A02     // Catch:{ all -> 0x000a }
            goto L_0x000d
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x000d:
            monitor-exit(r2)
            if (r0 == 0) goto L_0x0018
        L_0x0010:
            if (r1 == 0) goto L_0x001d
            boolean r0 = A01(r2)
            if (r0 == 0) goto L_0x001d
        L_0x0018:
            X.1Qb r0 = r2.A00
            r0.Bgg(r3, r4)
        L_0x001d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0d1.A00(X.0d1, X.1PS, int):void");
    }
}
