package X;

import com.facebook.acra.config.DefaultAcraConfig;
import com.facebook.acra.config.StartupBlockingConfig;
import com.facebook.acra.constants.ReportField;
import com.facebook.acra.sender.FlexibleReportSender;
import com.facebook.acra.sender.HttpPostSender;

/* renamed from: X.02O  reason: invalid class name */
public class AnonymousClass02O extends DefaultAcraConfig {
    private StartupBlockingConfig A00 = null;
    private boolean A01 = false;
    private boolean A02 = false;
    private boolean A03 = false;
    private final int A04;
    private final boolean A05;
    private final boolean A06;
    private final boolean A07;
    private final boolean A08;
    private final boolean A09;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass02O(android.app.Application r8, java.lang.String r9, boolean r10, boolean r11, boolean r12, boolean r13, boolean r14, boolean r15, com.facebook.acra.config.StartupBlockingConfig r16, java.lang.String r17, int r18, boolean r19, boolean r20, boolean r21, boolean r22, boolean r23) {
        /*
            r7 = this;
            r0 = r7
            r4 = r11
            r3 = r10
            r5 = r15
            r1 = r8
            r2 = r9
            r6 = r17
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r0 = 0
            r7.A02 = r0
            r7.A03 = r0
            r7.A01 = r0
            r0 = 0
            r7.A00 = r0
            r7.A02 = r12
            r7.A01 = r13
            r7.A03 = r14
            r0 = r16
            r7.A00 = r0
            r0 = r18
            r7.A04 = r0
            r0 = r19
            r7.A05 = r0
            r0 = r20
            r7.A08 = r0
            r0 = r21
            r7.A07 = r0
            r0 = r22
            r7.A09 = r0
            r0 = r23
            r7.A06 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02O.<init>(android.app.Application, java.lang.String, boolean, boolean, boolean, boolean, boolean, boolean, com.facebook.acra.config.StartupBlockingConfig, java.lang.String, int, boolean, boolean, boolean, boolean, boolean):void");
    }

    public boolean shouldReportField(String str) {
        if (0 != 0 && str.equals(ReportField.DATA_FILE_LS_LR)) {
            return false;
        }
        if (0 != 0 && str.equals(ReportField.OPEN_FILE_DESCRIPTORS)) {
            return false;
        }
        if (str.equals(ReportField.LOGCAT_NATIVE)) {
            return this.A01;
        }
        if (str.equals(ReportField.COMPONENTS_TOTAL) || str.equals(ReportField.COMPONENTS_DEFAULT) || str.equals(ReportField.COMPONENTS_DISABLED) || str.equals(ReportField.COMPONENTS_ENABLED) || str.equals(ReportField.COMPONENTS_DEFAULT_NAMES) || str.equals(ReportField.COMPONENTS_DISABLED_NAMES) || str.equals(ReportField.COMPONENTS_FLAG_STATE)) {
            return true;
        }
        return super.shouldReportField(str);
    }

    public FlexibleReportSender createReportSender() {
        HttpPostSender httpPostSender = new HttpPostSender(this);
        httpPostSender.mUseMultipartPost = this.A02;
        httpPostSender.mUseZstd = this.A03;
        return httpPostSender;
    }

    public String getLogcatNumberOfLinesToCapture() {
        return String.valueOf(this.A04);
    }

    public boolean allowCollectionOfMaxNumberOfLinesInLogcat() {
        return this.A05;
    }

    public StartupBlockingConfig getStartupBlockingConfig() {
        return this.A00;
    }

    public boolean shouldLazyFieldsOverwriteExistingValues() {
        return false;
    }

    public boolean shouldOnlyWriteReport() {
        return this.A06;
    }

    public boolean shouldSkipReportOnSocketTimeout() {
        return this.A07;
    }

    public boolean shouldStopAnrDetectorOnErrorReporting() {
        return this.A08;
    }

    public boolean shouldUseUploadService() {
        return this.A09;
    }
}
