package X;

/* renamed from: X.1sz  reason: invalid class name and case insensitive filesystem */
public final class C36391sz implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.mqttlite.whistle.ThreadSafeMqttClient$1";
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ C36181se A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ boolean A05;
    public final /* synthetic */ byte[] A06;

    public C36391sz(C36181se r1, String str, int i, byte[] bArr, int i2, int i3, boolean z) {
        this.A03 = r1;
        this.A04 = str;
        this.A02 = i;
        this.A06 = bArr;
        this.A01 = i2;
        this.A00 = i3;
        this.A05 = z;
    }

    public void run() {
        if (!this.A03.A02) {
            this.A03.A00.connect(this.A04, this.A02, this.A06, this.A01, this.A00, this.A05);
        } else {
            C010708t.A0J(ECX.$const$string(27), "connect ignored as client has been closed");
        }
    }
}
