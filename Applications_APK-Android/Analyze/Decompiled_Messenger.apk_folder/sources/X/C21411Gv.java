package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.Name;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import io.card.payment.BuildConfig;

@UserScoped
/* renamed from: X.1Gv  reason: invalid class name and case insensitive filesystem */
public final class C21411Gv {
    private static C05540Zi A01;
    public AnonymousClass0UN A00;

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006b, code lost:
        if (r0 != false) goto L_0x006e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1JY A03(com.facebook.messaging.model.threadkey.ThreadKey r9, com.facebook.user.model.UserKey r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = this;
            r3 = 0
            if (r12 == 0) goto L_0x000c
            android.net.Uri r0 = android.net.Uri.parse(r12)
            X.1JY r0 = X.AnonymousClass1JY.A00(r0)
            return r0
        L_0x000c:
            int r1 = X.AnonymousClass1Y3.AxE
            X.0UN r0 = r8.A00
            r5 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0t0 r0 = (X.C14300t0) r0
            com.facebook.user.model.User r7 = r0.A03(r10)
            int r0 = X.AnonymousClass1Y3.BAo
            X.0UN r2 = r8.A00
            r6 = 7
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r0, r2)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0042
            if (r7 == 0) goto L_0x0042
            r1 = 5
            int r0 = X.AnonymousClass1Y3.AXq
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.1H0 r0 = (X.AnonymousClass1H0) r0
            boolean r0 = r0.A02(r7)
            if (r0 == 0) goto L_0x0042
        L_0x003d:
            X.1JY r0 = r8.A01(r10, r11, r9, r3)
            return r0
        L_0x0042:
            int r0 = X.AnonymousClass1Y3.BAo
            X.0UN r2 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r0, r2)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x006d
            r1 = 3
            int r0 = X.AnonymousClass1Y3.BQY
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.3Rf r1 = (X.C68053Rf) r1
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0B(r9)
            if (r0 == 0) goto L_0x0075
            X.3Rg r2 = X.C68053Rf.A04
        L_0x0063:
            long r0 = X.C68053Rf.A00(r1)
            boolean r0 = r2.A00(r0)
        L_0x006b:
            if (r0 != 0) goto L_0x006e
        L_0x006d:
            r5 = 1
        L_0x006e:
            if (r5 == 0) goto L_0x003d
            X.1JY r0 = X.AnonymousClass1JY.A04(r10)
            return r0
        L_0x0075:
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0A(r9)
            if (r0 == 0) goto L_0x007e
            X.3Rg r2 = X.C68053Rf.A03
            goto L_0x0063
        L_0x007e:
            r0 = 1
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21411Gv.A03(com.facebook.messaging.model.threadkey.ThreadKey, com.facebook.user.model.UserKey, java.lang.String, java.lang.String):X.1JY");
    }

    public AnonymousClass1JY A04(User user) {
        C21381Gs A02 = A02(this, user, false, 0, true);
        if (!User.A02(user.A0M)) {
            return AnonymousClass1JY.A02(user, A02);
        }
        User user2 = user.A0N;
        if (user2 == null) {
            return AnonymousClass1JY.A08(user.A07(), user.A0L, A02, 0);
        }
        return AnonymousClass1JY.A05(user2.A0Q, A02);
    }

    public static final C21411Gv A00(AnonymousClass1XY r4) {
        C21411Gv r0;
        synchronized (C21411Gv.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C21411Gv((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C21411Gv) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b5, code lost:
        if (((X.AnonymousClass2T4) X.AnonymousClass1XX.A02(8, X.AnonymousClass1Y3.BL7, r5.A00)).A00.Aem(282295315465502L) != false) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C21381Gs A02(X.C21411Gv r5, com.facebook.user.model.User r6, boolean r7, int r8, boolean r9) {
        /*
            java.lang.String r4 = "unknown"
            boolean r0 = r6.A17
            if (r0 != 0) goto L_0x00b8
            boolean r0 = r6.A0G()
            if (r0 != 0) goto L_0x00b8
            r2 = 5
            int r1 = X.AnonymousClass1Y3.AXq
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1H0 r0 = (X.AnonymousClass1H0) r0
            boolean r0 = r0.A02(r6)
            if (r0 == 0) goto L_0x0020
            X.1Gs r0 = X.C21381Gs.A0Y
            return r0
        L_0x0020:
            r2 = 6
            int r1 = X.AnonymousClass1Y3.BDi
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Gz r1 = (X.C21441Gz) r1
            java.lang.String r0 = r6.A0j
            boolean r0 = r1.A01(r0)
            if (r0 == 0) goto L_0x0036
            X.1Gs r0 = X.C21381Gs.A0X
            return r0
        L_0x0036:
            boolean r0 = r6.A0H()
            if (r0 == 0) goto L_0x003f
            X.1Gs r0 = X.C21381Gs.A0R
            return r0
        L_0x003f:
            int r1 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r5.A00
            r3 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0r6 r1 = (X.AnonymousClass0r6) r1
            com.facebook.user.model.UserKey r0 = r6.A0Q
            boolean r0 = r1.A0a(r0)
            if (r0 == 0) goto L_0x0055
            X.1Gs r0 = X.C21381Gs.A02
            return r0
        L_0x0055:
            if (r9 == 0) goto L_0x007d
            int r1 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0r6 r1 = (X.AnonymousClass0r6) r1
            com.facebook.user.model.UserKey r0 = r6.A0Q
            boolean r0 = r1.A0Z(r0)
            if (r0 == 0) goto L_0x007d
            r2 = 2
            int r1 = X.AnonymousClass1Y3.ADw
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1F8 r0 = (X.AnonymousClass1F8) r0
            boolean r0 = r0.A01(r4)
            if (r0 == 0) goto L_0x007d
        L_0x007a:
            X.1Gs r0 = X.C21381Gs.A01
            return r0
        L_0x007d:
            int r1 = X.AnonymousClass1Y3.Auh
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0r6 r1 = (X.AnonymousClass0r6) r1
            com.facebook.user.model.UserKey r0 = r6.A0Q
            boolean r0 = r1.A0b(r0, r8)
            if (r0 == 0) goto L_0x0094
            if (r7 == 0) goto L_0x0094
            X.1Gs r0 = X.C21381Gs.A0Q
            return r0
        L_0x0094:
            boolean r0 = r6.A0F()
            if (r0 == 0) goto L_0x00b8
            boolean r0 = r6.A1M
            if (r0 == 0) goto L_0x00b8
            r2 = 8
            int r1 = X.AnonymousClass1Y3.BL7
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2T4 r0 = (X.AnonymousClass2T4) r0
            X.1Yd r2 = r0.A00
            r0 = 282295315465502(0x100bf0000051e, double:1.394724173534217E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x00b8
            goto L_0x007a
        L_0x00b8:
            X.1Gs r0 = X.C21381Gs.A0L
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21411Gv.A02(X.1Gv, com.facebook.user.model.User, boolean, int, boolean):X.1Gs");
    }

    public AnonymousClass1JY A05(UserKey userKey) {
        return A06(userKey, BuildConfig.FLAVOR, 0);
    }

    public AnonymousClass1JY A06(UserKey userKey, String str, int i) {
        UserKey userKey2;
        C21381Gs A07;
        User user;
        User A03 = ((C14300t0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxE, this.A00)).A03(userKey);
        if (A03 == null || (user = A03.A0N) == null) {
            userKey2 = null;
        } else {
            userKey2 = user.A0Q;
        }
        if (userKey2 != null) {
            A07 = C21381Gs.A0R;
            userKey = userKey2;
        } else {
            A07 = A07(userKey, false, 0, true);
        }
        if (userKey.A08()) {
            return AnonymousClass1JY.A08(userKey.A04(), new Name(str), A07, i);
        }
        return AnonymousClass1JY.A05(userKey, A07);
    }

    public C21381Gs A07(UserKey userKey, boolean z, int i, boolean z2) {
        User A03 = ((C14300t0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxE, this.A00)).A03(userKey);
        if (A03 != null) {
            return A02(this, A03, z, i, z2);
        }
        if (userKey.A08()) {
            return C21381Gs.A0R;
        }
        int i2 = AnonymousClass1Y3.Auh;
        if (((AnonymousClass0r6) AnonymousClass1XX.A02(1, i2, this.A00)).A0a(userKey)) {
            return C21381Gs.A02;
        }
        if (((AnonymousClass0r6) AnonymousClass1XX.A02(1, i2, this.A00)).A0Z(userKey) && ((AnonymousClass1F8) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ADw, this.A00)).A01("unknown")) {
            return C21381Gs.A01;
        }
        if (!((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, this.A00)).A0b(userKey, i) || !z) {
            return C21381Gs.A0L;
        }
        return C21381Gs.A0Q;
    }

    private C21411Gv(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(9, r3);
    }

    private AnonymousClass1JY A01(UserKey userKey, String str, ThreadKey threadKey, int i) {
        C21381Gs r0;
        if (ThreadKey.A0F(threadKey)) {
            r0 = C21381Gs.A0T;
        } else if (!ThreadKey.A0E(threadKey) || userKey.A08()) {
            return A06(userKey, str, i);
        } else {
            r0 = C21381Gs.A0R;
        }
        return AnonymousClass1JY.A05(userKey, r0);
    }
}
