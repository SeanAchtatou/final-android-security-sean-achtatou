package X;

import java.util.ArrayList;

/* renamed from: X.0AA  reason: invalid class name */
public final class AnonymousClass0AA extends ArrayList<String> {
    public AnonymousClass0AA() {
        add("com.facebook.rti.fbnsdemo");
        add("com.facebook.katana");
        add("com.facebook.wakizashi");
        add("com.facebook.lite");
        add("com.facebook.orca");
        add("com.instagram.android");
        add("com.instagram.direct");
        add("com.instagram.android.preload");
        add("com.facebook.alohaservices.push");
        add("com.facebook.mlite");
        add("com.facebook.mlite_debug");
        add("com.facebook.mlite_inhouse");
        add("com.facebook.aloha.push");
        add("com.whatsapp");
        add("com.whatsapp.w4b");
    }
}
