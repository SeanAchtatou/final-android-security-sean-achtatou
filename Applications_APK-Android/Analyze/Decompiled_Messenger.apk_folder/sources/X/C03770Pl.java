package X;

import java.io.File;

/* renamed from: X.0Pl  reason: invalid class name and case insensitive filesystem */
public final class C03770Pl implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.profilo.upload.BackgroundUploadServiceImpl$1";
    public final /* synthetic */ C003804w A00;
    public final /* synthetic */ AnonymousClass0Qc A01;
    public final /* synthetic */ File A02;
    public final /* synthetic */ boolean A03;

    public C03770Pl(AnonymousClass0Qc r1, File file, C003804w r3, boolean z) {
        this.A01 = r1;
        this.A02 = file;
        this.A00 = r3;
        this.A03 = z;
    }

    public void run() {
        if (this.A02.exists()) {
            synchronized (this.A01.A01) {
                try {
                    this.A01.A01.add(this.A02);
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            AnonymousClass0Qc.A01(this.A01, this.A02, this.A00, this.A03);
            synchronized (this.A01.A01) {
                try {
                    this.A01.A01.remove(this.A02);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }
}
