package X;

/* renamed from: X.1IL  reason: invalid class name */
public final class AnonymousClass1IL extends C21631Ic {
    public int A00;
    public final C78893ph A01;
    public final C37481vk A02;

    public String getName() {
        return AnonymousClass08S.A0A("View (viewType=", this.A00, ")");
    }

    public AnonymousClass1IL(C21331Gg r2) {
        super(r2);
        this.A01 = r2.A00;
        this.A02 = r2.A01;
    }
}
