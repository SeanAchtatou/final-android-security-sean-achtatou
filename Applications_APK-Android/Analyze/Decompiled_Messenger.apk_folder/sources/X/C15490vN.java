package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0vN  reason: invalid class name and case insensitive filesystem */
public final class C15490vN extends AnonymousClass1A9 {
    public Map A00;
    public final AnonymousClass1ZE A01;
    private final AnonymousClass06B A02 = AnonymousClass067.A02();

    public void A08(RecyclerView recyclerView, int i, int i2) {
    }

    public static final C15490vN A00(AnonymousClass1XY r1) {
        return new C15490vN(r1);
    }

    private Map A01(RecyclerView recyclerView) {
        AnonymousClass19S r3 = (AnonymousClass19S) recyclerView.A0L;
        HashMap hashMap = new HashMap();
        hashMap.put("si", String.valueOf(r3.A1u()));
        hashMap.put("ei", String.valueOf(r3.AZs()));
        hashMap.put("o", String.valueOf(recyclerView.computeVerticalScrollOffset()));
        hashMap.put("t", String.valueOf(this.A02.now()));
        return hashMap;
    }

    public void A07(RecyclerView recyclerView, int i) {
        if (i != 0) {
            if (i == 1) {
                this.A00 = A01(recyclerView);
            }
        } else if (this.A00 != null) {
            Map A012 = A01(recyclerView);
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(this.A01.A01("thread_list_scrolling_snapshot"), 570);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0F("e", A012);
                uSLEBaseShape0S0000000.A0F("s", this.A00);
                uSLEBaseShape0S0000000.A09("vh", Double.valueOf((double) recyclerView.getHeight()));
                uSLEBaseShape0S0000000.A06();
            }
        }
    }

    public C15490vN(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass1ZD.A00(r2);
    }
}
