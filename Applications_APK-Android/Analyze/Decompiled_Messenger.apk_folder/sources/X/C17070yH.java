package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0yH  reason: invalid class name and case insensitive filesystem */
public final class C17070yH {
    private static volatile C17070yH A01;
    private AnonymousClass0UN A00;

    public void A01(C13460rT r2, C17100yK r3) {
        A02(r2, r3, true);
    }

    public static final C17070yH A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C17070yH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C17070yH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C17070yH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public void A02(C13460rT r4, C17100yK r5, boolean z) {
        AnonymousClass064.A00(r4);
        AnonymousClass064.A00(r5);
        if (z) {
            new C13990sQ(r4, new C17120yM(r5, (C13980sP) AnonymousClass1XX.A03(AnonymousClass1Y3.AXL, this.A00), new C17110yL(r5)));
        }
    }
}
