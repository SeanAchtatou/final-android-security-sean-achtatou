package X;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.util.HashMap;

/* renamed from: X.0TQ  reason: invalid class name */
public enum AnonymousClass0TQ {
    ROOT_PATH("root-path", false),
    FILES_PATH("files-path", true),
    CACHE_PATH("cache-path", true),
    EXTERNAL_PATH("external-path", false),
    EXTERNAL_FILES_PATH("external-files-path", false),
    EXTERNAL_CACHE_PATH("external-cache-path", false);
    
    public static final HashMap A00 = new HashMap();
    private static final File A01 = new File("/");
    public final boolean mIsPrivate;
    public final String mTagName;

    /* access modifiers changed from: public */
    static {
        for (AnonymousClass0TQ r2 : values()) {
            A00.put(r2.mTagName, r2);
        }
    }

    private AnonymousClass0TQ(String str, boolean z) {
        this.mTagName = str;
        this.mIsPrivate = z;
    }

    public File A00(Context context) {
        switch (ordinal()) {
            case 0:
                return A01;
            case 1:
                return context.getFilesDir();
            case 2:
                return context.getCacheDir();
            case 3:
                return Environment.getExternalStorageDirectory();
            case 4:
                return context.getExternalFilesDir(null);
            case 5:
                return context.getExternalCacheDir();
            default:
                return null;
        }
    }
}
