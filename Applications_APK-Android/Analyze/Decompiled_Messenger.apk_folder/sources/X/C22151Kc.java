package X;

import android.content.Context;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

/* renamed from: X.1Kc  reason: invalid class name and case insensitive filesystem */
public final class C22151Kc {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public Context A06;
    public Path A07;
    public Typeface A08;
    public Drawable A09;
    public AnonymousClass1JY A0A;
    public AnonymousClass1KZ A0B;
    public AnonymousClass1KU A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    private AnonymousClass0UN A0H;
    private C21901Jd A0I;

    public static final C22151Kc A00(AnonymousClass1XY r1) {
        return new C22151Kc(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x006b, code lost:
        if (r13.A0D != false) goto L_0x006d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C21901Jd A01() {
        /*
            r13 = this;
            boolean r0 = r13.A0F
            if (r0 == 0) goto L_0x0091
            java.lang.String r0 = "UserTileDrawableCachingBuilder#createDrawableController"
            X.C27041cY.A01(r0)
            X.1R8 r9 = new X.1R8
            r9.<init>()
            X.1KU r0 = r13.A0C
            r9.A04 = r0
            int r0 = r13.A05
            r9.A03(r0)
            float r0 = r13.A01
            r9.A02(r0)
            android.graphics.Path r0 = r13.A07
            r9.A03 = r0
            r9.invalidateSelf()
            android.graphics.Typeface r0 = r13.A08
            r9.A05(r0)
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0 = 1
            r9.A07 = r0
            r9.A01 = r1
            r9.A08 = r0
            X.1KZ r11 = r13.A0B
            if (r11 != 0) goto L_0x004a
            android.content.Context r3 = r13.A06
            int[] r2 = X.C008407o.A0A
            r1 = 0
            r0 = 0
            android.content.res.TypedArray r1 = r3.obtainStyledAttributes(r0, r2, r1, r1)
            X.1Ka r0 = X.AnonymousClass3DL.A00(r3, r1)
            r1.recycle()
            X.1KZ r11 = r0.A00()
        L_0x004a:
            java.lang.String r0 = "injectDrawableController"
            X.C27041cY.A01(r0)
            int r1 = X.AnonymousClass1Y3.BEs
            X.0UN r0 = r13.A0H
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            X.1Jd r2 = (X.C21901Jd) r2
            X.C27041cY.A00()
            java.lang.String r0 = "initDrawableController"
            X.C27041cY.A01(r0)
            android.content.Context r3 = r13.A06
            boolean r0 = r13.A0E
            r1 = 0
            if (r0 != 0) goto L_0x006d
            boolean r0 = r13.A0D
            r4 = 0
            if (r0 == 0) goto L_0x006e
        L_0x006d:
            r4 = 1
        L_0x006e:
            int r5 = r13.A04
            int r6 = r13.A03
            boolean r7 = r13.A0G
            android.graphics.drawable.Drawable r8 = r13.A09
            float r10 = r13.A00
            android.graphics.Path r12 = r13.A07
            r2.A0D(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            X.C27041cY.A00()
            X.1JY r0 = r13.A0A
            r2.A0E(r0)
            int r0 = r13.A02
            r2.A09(r0)
            r13.A0I = r2
            r13.A0F = r1
            X.C27041cY.A00()
        L_0x0091:
            X.1Jd r0 = r13.A0I
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22151Kc.A01():X.1Jd");
    }

    private C22151Kc(AnonymousClass1XY r3) {
        this.A0H = new AnonymousClass0UN(0, r3);
    }
}
