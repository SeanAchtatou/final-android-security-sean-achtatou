package X;

/* renamed from: X.21K  reason: invalid class name */
public final class AnonymousClass21K {
    public AnonymousClass0UN A00;

    public static final AnonymousClass21K A00(AnonymousClass1XY r1) {
        return new AnonymousClass21K(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0050, code lost:
        if (((X.C25051Yd) X.AnonymousClass1XX.A02(0, X.AnonymousClass1Y3.AOJ, r2)).Aem(287423506750914L) != false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0027, code lost:
        if (X.C06850cB.A0B(r1.A0A()) != false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01() {
        /*
            r6 = this;
            int r5 = X.AnonymousClass1Y3.AME
            X.0UN r0 = r6.A00
            r4 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r5, r0)
            X.0sv r1 = (X.C14260sv) r1
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A0B
            r3 = 0
            if (r0 == 0) goto L_0x0053
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            boolean r0 = r0.A0L()
            if (r0 == 0) goto L_0x0029
            boolean r0 = r1.A0p()
            if (r0 == 0) goto L_0x0029
            java.lang.String r0 = r1.A0A()
            boolean r1 = X.C06850cB.A0B(r0)
            r0 = 1
            if (r1 == 0) goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            if (r0 != 0) goto L_0x0052
            X.0UN r2 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r5, r2)
            X.0sv r0 = (X.C14260sv) r0
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A0B
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S
            boolean r0 = r0.A0N()
            if (r0 == 0) goto L_0x0053
            int r1 = X.AnonymousClass1Y3.AOJ
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r2)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 287423506750914(0x1056900051dc2, double:1.42006080492846E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0053
        L_0x0052:
            r3 = 1
        L_0x0053:
            if (r3 == 0) goto L_0x0079
            r1 = 2
            int r0 = X.AnonymousClass1Y3.Ayw
            X.0UN r2 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r2)
            X.1m3 r0 = (X.C32621m3) r0
            boolean r0 = r0.A05
            if (r0 != 0) goto L_0x0079
            int r1 = X.AnonymousClass1Y3.AOJ
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r2)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 287423506423230(0x1056900001dbe, double:1.42006080330949E-309)
            boolean r1 = r2.Aem(r0)
            r0 = 1
            if (r1 != 0) goto L_0x007a
        L_0x0079:
            r0 = 0
        L_0x007a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass21K.A01():boolean");
    }

    public AnonymousClass21K(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
