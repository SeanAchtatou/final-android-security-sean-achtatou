package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.10w  reason: invalid class name and case insensitive filesystem */
public final class C182410w extends C15380vC implements AnonymousClass1YQ {
    private static volatile C182410w A01;
    private AnonymousClass0UN A00;

    public String getSimpleName() {
        return "InboxBadgeController";
    }

    public static final C182410w A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C182410w.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C182410w(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C182410w(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
    }

    public void init() {
        int A03 = C000700l.A03(-1026713733);
        C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A03(AnonymousClass1Y3.AKb, this.A00)).BMm();
        BMm.A02(C06680bu.A0I, new C100414qo(this));
        BMm.A00().A00();
        C000700l.A09(-2105792668, A03);
    }
}
