package X;

import android.os.Handler;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.0BL  reason: invalid class name */
public final class AnonymousClass0BL {
    public int A00 = 0;
    public long A01;
    public AnonymousClass07g A02;
    public AnonymousClass0BK A03;
    public C01840Bv A04;
    public Runnable A05;
    public Future A06;
    public boolean A07;
    private C008007h A08;
    public final C01690Bg A09;
    public final ScheduledExecutorService A0A;
    private final Handler A0B;
    private final AnonymousClass0BM A0C;
    private final ExecutorService A0D;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r1 != false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A02(X.AnonymousClass0BL r2) {
        /*
            monitor-enter(r2)
            java.util.concurrent.Future r0 = r2.A06     // Catch:{ all -> 0x000f }
            if (r0 == 0) goto L_0x000c
            boolean r1 = r0.isDone()     // Catch:{ all -> 0x000f }
            r0 = 1
            if (r1 == 0) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            monitor-exit(r2)
            return r0
        L_0x000f:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BL.A02(X.0BL):boolean");
    }

    public synchronized void A04() {
        A00(this);
        A05();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        if (r0 != false) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A05() {
        /*
            r8 = this;
            monitor-enter(r8)
            r7 = r8
            monitor-enter(r7)     // Catch:{ all -> 0x00b5 }
            java.lang.String r4 = "ConnectionRetryManager"
            X.0Bv r0 = r8.A04     // Catch:{ all -> 0x00b2 }
            if (r0 != 0) goto L_0x0010
            java.lang.String r0 = "next is called before having a strategy."
            X.C010708t.A0J(r4, r0)     // Catch:{ all -> 0x00b2 }
            goto L_0x00ac
        L_0x0010:
            X.0BK r0 = r8.A03     // Catch:{ all -> 0x00b2 }
            boolean r0 = r0.ASA()     // Catch:{ all -> 0x00b2 }
            if (r0 == 0) goto L_0x00ac
            boolean r0 = A02(r8)     // Catch:{ all -> 0x00b2 }
            r6 = 1
            if (r0 != 0) goto L_0x00aa
            int r0 = r8.A00     // Catch:{ all -> 0x00b2 }
            if (r0 != 0) goto L_0x0029
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x00b2 }
            r8.A01 = r0     // Catch:{ all -> 0x00b2 }
        L_0x0029:
            X.0Bg r0 = r8.A09     // Catch:{ all -> 0x00b2 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00b2 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x00b2 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x00b2 }
            if (r0 == 0) goto L_0x0043
            r1 = r8
            monitor-enter(r1)     // Catch:{ all -> 0x00b2 }
            boolean r0 = r8.A07     // Catch:{ all -> 0x003d }
            monitor-exit(r1)     // Catch:{ all -> 0x00b2 }
            goto L_0x0040
        L_0x003d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00b2 }
            throw r0     // Catch:{ all -> 0x00b2 }
        L_0x0040:
            r3 = 1
            if (r0 == 0) goto L_0x0044
        L_0x0043:
            r3 = 0
        L_0x0044:
            X.0Bv r0 = r8.A04     // Catch:{ all -> 0x00b2 }
            boolean r2 = r0.BBa(r3)     // Catch:{ all -> 0x00b2 }
            if (r2 != 0) goto L_0x0067
            X.07l r1 = r0.B48()     // Catch:{ all -> 0x00b2 }
            X.07l r0 = X.C008107l.BACK_TO_BACK     // Catch:{ all -> 0x00b2 }
            if (r1 != r0) goto L_0x005f
            X.07l r0 = X.C008107l.BACK_OFF     // Catch:{ all -> 0x00b2 }
            r8.A01(r0)     // Catch:{ all -> 0x00b2 }
            X.0Bv r0 = r8.A04     // Catch:{ all -> 0x00b2 }
            boolean r2 = r0.BBa(r3)     // Catch:{ all -> 0x00b2 }
        L_0x005f:
            if (r2 != 0) goto L_0x0067
            java.lang.String r0 = "No more retry!"
            X.C010708t.A0J(r4, r0)     // Catch:{ all -> 0x00b2 }
            goto L_0x00ac
        L_0x0067:
            X.0Bv r0 = r8.A04     // Catch:{ all -> 0x00b2 }
            int r2 = r0.BLm(r3)     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.Future r1 = r8.A06     // Catch:{ all -> 0x00b2 }
            if (r1 == 0) goto L_0x0078
            r0 = 0
            r1.cancel(r0)     // Catch:{ all -> 0x00b2 }
            r0 = 0
            r8.A06 = r0     // Catch:{ all -> 0x00b2 }
        L_0x0078:
            if (r2 > 0) goto L_0x007b
            goto L_0x009f
        L_0x007b:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.ScheduledExecutorService r4 = r8.A0A     // Catch:{ all -> 0x00b2 }
            java.lang.Runnable r3 = r8.A05     // Catch:{ all -> 0x00b2 }
            long r1 = (long) r2     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00b2 }
            java.util.concurrent.ScheduledFuture r0 = r4.schedule(r3, r1, r0)     // Catch:{ all -> 0x00b2 }
            r8.A06 = r0     // Catch:{ all -> 0x00b2 }
            X.07g r3 = r8.A02     // Catch:{ all -> 0x00b2 }
            if (r3 == 0) goto L_0x00a5
            java.lang.String r2 = "retry in %d seconds"
            java.lang.Object[] r1 = new java.lang.Object[]{r5}     // Catch:{ all -> 0x00b2 }
            r0 = 0
            java.lang.String r0 = java.lang.String.format(r0, r2, r1)     // Catch:{ all -> 0x00b2 }
            r3.BIo(r0)     // Catch:{ all -> 0x00b2 }
            goto L_0x00a5
        L_0x009f:
            java.util.concurrent.Future r0 = r8.A03()     // Catch:{ all -> 0x00b2 }
            r8.A06 = r0     // Catch:{ all -> 0x00b2 }
        L_0x00a5:
            int r0 = r8.A00     // Catch:{ all -> 0x00b2 }
            int r0 = r0 + r6
            r8.A00 = r0     // Catch:{ all -> 0x00b2 }
        L_0x00aa:
            monitor-exit(r7)     // Catch:{ all -> 0x00b5 }
            goto L_0x00af
        L_0x00ac:
            monitor-exit(r7)     // Catch:{ all -> 0x00b5 }
            r0 = 0
            goto L_0x00b0
        L_0x00af:
            r0 = 1
        L_0x00b0:
            monitor-exit(r8)
            return r0
        L_0x00b2:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00b5 }
            throw r0     // Catch:{ all -> 0x00b5 }
        L_0x00b5:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BL.A05():boolean");
    }

    public static void A00(AnonymousClass0BL r2) {
        Future future = r2.A06;
        if (future != null) {
            future.cancel(false);
            r2.A06 = null;
        }
        r2.A01(C008107l.BACK_TO_BACK);
        AnonymousClass0BM r1 = r2.A0C;
        r1.A01 = -2;
        r1.A00 = r1.A02;
        r2.A00 = 0;
    }

    private void A01(C008107l r6) {
        Future future = this.A06;
        if (future != null) {
            future.cancel(false);
            this.A06 = null;
        }
        AnonymousClass07i A032 = this.A08.A03();
        if (r6 == C008107l.BACK_TO_BACK) {
            this.A04 = new C008207m(A032.A02, A032.A05, A032.A03);
        } else if (r6 == C008107l.BACK_OFF) {
            this.A04 = new C03520Oi(A032.A00, A032.A04, A032.A01);
        } else {
            throw new IllegalArgumentException(String.format(null, "Invalid strategy %s specified", r6));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0011, code lost:
        if (r2 != java.lang.Thread.currentThread()) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.concurrent.Future A03() {
        /*
            r3 = this;
            android.os.Handler r0 = r3.A0B
            if (r0 == 0) goto L_0x0013
            android.os.Looper r0 = r0.getLooper()
            java.lang.Thread r2 = r0.getThread()
            java.lang.Thread r1 = java.lang.Thread.currentThread()
            r0 = 1
            if (r2 == r1) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            if (r0 == 0) goto L_0x001e
            java.lang.Runnable r0 = r3.A05
            r0.run()
            X.0CO r0 = X.AnonymousClass0CO.A01
            return r0
        L_0x001e:
            java.util.concurrent.ExecutorService r2 = r3.A0D
            java.lang.Runnable r1 = r3.A05
            r0 = 831020264(0x31885ce8, float:3.968683E-9)
            java.util.concurrent.Future r0 = X.AnonymousClass07A.A02(r2, r1, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BL.A03():java.util.concurrent.Future");
    }

    public AnonymousClass0BL(C01690Bg r4, ExecutorService executorService, ScheduledExecutorService scheduledExecutorService, Handler handler, C008007h r8, AnonymousClass0BK r9, AnonymousClass07g r10) {
        this.A09 = r4;
        this.A0D = executorService;
        this.A0A = scheduledExecutorService;
        this.A0B = handler;
        this.A08 = r8;
        this.A03 = r9;
        this.A02 = r10;
        AnonymousClass07i A032 = r8.A03();
        this.A0C = new AnonymousClass0BM(A032.A00, A032.A01);
    }
}
