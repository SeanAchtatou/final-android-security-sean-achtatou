package X;

import java.io.StringWriter;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.06g  reason: invalid class name and case insensitive filesystem */
public final class C006106g extends C07280dA {
    private static volatile C006106g A00;

    public static final C006106g A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C006106g.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C006106g();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public void A01(C12240os r4) {
        if (C006006f.A01()) {
            StringWriter stringWriter = new StringWriter();
            try {
                C11910oA.A00().AYP(stringWriter, r4);
                C010708t.A0J("EndToEnd-AnalyticsEvent#reportEvent", stringWriter.toString());
            } catch (Exception unused) {
                C010708t.A0I("reportEvent", "Can't encode event data");
            }
        }
    }
}
