package X;

import com.facebook.common.dextricks.DexStore;

/* renamed from: X.01Z  reason: invalid class name */
public final class AnonymousClass01Z implements C001701e {
    private final int A00;

    private boolean A00(int i) {
        if ((i & this.A00) != 0) {
            return true;
        }
        return false;
    }

    public boolean CDj(int i, int i2, AnonymousClass01Y r5) {
        if (A00(65536) && i2 == 0 && i != 0) {
            return true;
        }
        if (A00(DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP) && i2 != 0 && i == 0) {
            return true;
        }
        if (A00(1024) && r5 == AnonymousClass01Y.ACTIVITY_CREATED) {
            return true;
        }
        if (A00(2048) && r5 == AnonymousClass01Y.ACTIVITY_STARTED) {
            return true;
        }
        if (A00(DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED) && r5 == AnonymousClass01Y.ACTIVITY_RESUMED) {
            return true;
        }
        if (A00(DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED) && r5 == AnonymousClass01Y.ACTIVITY_PAUSED) {
            return true;
        }
        if (A00(DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET) && r5 == AnonymousClass01Y.ACTIVITY_STOPPED) {
            return true;
        }
        if (!A00(DexStore.LOAD_RESULT_PGO) || r5 != AnonymousClass01Y.ACTIVITY_DESTROYED) {
            return false;
        }
        return true;
    }

    public boolean CE8(int i, int i2, AnonymousClass01Y r5) {
        if (A00(64) && i2 == 0 && i != 0) {
            return true;
        }
        if (A00(128) && i2 != 0 && i == 0) {
            return true;
        }
        if (A00(1) && r5 == AnonymousClass01Y.ACTIVITY_CREATED) {
            return true;
        }
        if (A00(2) && r5 == AnonymousClass01Y.ACTIVITY_STARTED) {
            return true;
        }
        if (A00(4) && r5 == AnonymousClass01Y.ACTIVITY_RESUMED) {
            return true;
        }
        if (A00(8) && r5 == AnonymousClass01Y.ACTIVITY_PAUSED) {
            return true;
        }
        if (A00(16) && r5 == AnonymousClass01Y.ACTIVITY_STOPPED) {
            return true;
        }
        if (!A00(32) || r5 != AnonymousClass01Y.ACTIVITY_DESTROYED) {
            return false;
        }
        return true;
    }

    public AnonymousClass01Z(int i) {
        this.A00 = i;
    }
}
