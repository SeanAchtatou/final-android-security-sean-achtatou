package X;

import android.graphics.Typeface;
import java.io.File;

/* renamed from: X.1bn  reason: invalid class name and case insensitive filesystem */
public final class C26651bn implements C09950ix {
    public Object AUP(File file) {
        if (file == null) {
            return null;
        }
        try {
            return Typeface.createFromFile(file);
        } catch (RuntimeException unused) {
            return null;
        }
    }
}
