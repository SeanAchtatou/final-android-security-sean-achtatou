package X;

import java.io.Serializable;

/* renamed from: X.1HT  reason: invalid class name */
public final class AnonymousClass1HT extends C07160cj implements Serializable {
    private static final long serialVersionUID = 0;
    public final int seed;

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass1HT) || this.seed != ((AnonymousClass1HT) obj).seed) {
            return false;
        }
        return true;
    }

    public String toString() {
        return AnonymousClass08S.A0A("Hashing.murmur3_128(", this.seed, ")");
    }

    public AnonymousClass1HT(int i) {
        this.seed = i;
    }

    public int hashCode() {
        return getClass().hashCode() ^ this.seed;
    }
}
