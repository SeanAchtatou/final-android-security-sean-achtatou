package X;

/* renamed from: X.0eI  reason: invalid class name and case insensitive filesystem */
public final class C07860eI implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.dialtone.DialtoneAsyncSignalFile$2";
    public final /* synthetic */ AnonymousClass0Z9 A00;

    public C07860eI(AnonymousClass0Z9 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006b, code lost:
        if (r3.A03.canWrite() == false) goto L_0x006d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r6 = this;
            X.0Z9 r3 = r6.A00
            boolean r5 = X.AnonymousClass0Z9.A03(r3)
            java.lang.String r2 = "enable_dialtone_mode"
            java.io.File r0 = r3.A02     // Catch:{ IOException -> 0x003f }
            boolean r0 = r0.exists()     // Catch:{ IOException -> 0x003f }
            if (r0 == 0) goto L_0x002a
            java.io.File r0 = r3.A02     // Catch:{ IOException -> 0x003f }
            boolean r0 = r0.isDirectory()     // Catch:{ IOException -> 0x003f }
            if (r0 == 0) goto L_0x0031
            java.io.File r0 = r3.A02     // Catch:{ IOException -> 0x003f }
            boolean r0 = r0.canRead()     // Catch:{ IOException -> 0x003f }
            if (r0 == 0) goto L_0x0031
            java.io.File r0 = r3.A02     // Catch:{ IOException -> 0x003f }
            boolean r1 = r0.canWrite()     // Catch:{ IOException -> 0x003f }
            r0 = 1
            if (r1 != 0) goto L_0x0032
            goto L_0x0031
        L_0x002a:
            java.io.File r0 = r3.A02     // Catch:{ IOException -> 0x003f }
            boolean r0 = r0.mkdirs()     // Catch:{ IOException -> 0x003f }
            goto L_0x0032
        L_0x0031:
            r0 = 0
        L_0x0032:
            if (r0 == 0) goto L_0x0047
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x003f }
            java.io.File r0 = r3.A02     // Catch:{ IOException -> 0x003f }
            r1.<init>(r0, r2)     // Catch:{ IOException -> 0x003f }
            r1.createNewFile()     // Catch:{ IOException -> 0x003f }
            goto L_0x0047
        L_0x003f:
            r4 = move-exception
            java.lang.String r1 = "DialtoneSignalFile"
            java.lang.String r0 = "Dialtone signal file could not be created"
            android.util.Log.w(r1, r0, r4)     // Catch:{ all -> 0x0095 }
        L_0x0047:
            X.C05230Yd.A00(r3)
            if (r5 == 0) goto L_0x0094
            java.io.File r0 = r3.A03
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0071
            java.io.File r0 = r3.A03
            boolean r0 = r0.isDirectory()
            if (r0 == 0) goto L_0x006d
            java.io.File r0 = r3.A03
            boolean r0 = r0.canRead()
            if (r0 == 0) goto L_0x006d
            java.io.File r0 = r3.A03
            boolean r1 = r0.canWrite()
            r0 = 1
            if (r1 != 0) goto L_0x006e
        L_0x006d:
            r0 = 0
        L_0x006e:
            if (r0 == 0) goto L_0x0094
            goto L_0x0078
        L_0x0071:
            java.io.File r0 = r3.A03
            boolean r0 = r0.mkdirs()
            goto L_0x006e
        L_0x0078:
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0083 }
            java.io.File r0 = r3.A03     // Catch:{ IOException -> 0x0083 }
            r1.<init>(r0, r2)     // Catch:{ IOException -> 0x0083 }
            r1.createNewFile()     // Catch:{ IOException -> 0x0083 }
            goto L_0x0091
        L_0x0083:
            r2 = move-exception
            java.lang.String r1 = "DialtoneSignalFile"
            java.lang.String r0 = "Dialtone internal signal file could not be created"
            android.util.Log.w(r1, r0, r2)     // Catch:{ all -> 0x008c }
            goto L_0x0091
        L_0x008c:
            r0 = move-exception
            X.C05230Yd.A01(r3)
            throw r0
        L_0x0091:
            X.C05230Yd.A01(r3)
        L_0x0094:
            return
        L_0x0095:
            r0 = move-exception
            X.C05230Yd.A00(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07860eI.run():void");
    }
}
