package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.0yC  reason: invalid class name and case insensitive filesystem */
public final class C17020yC extends C17770zR {
    public static final MigColorScheme A07 = C17190yT.A00();
    @Comparable(type = 3)
    public int A00 = 0;
    public AnonymousClass10N A01;
    public AnonymousClass10N A02;
    @Comparable(type = 13)
    public C45722Nf A03;
    @Comparable(type = 13)
    public MigColorScheme A04 = A07;
    @Comparable(type = 13)
    public CharSequence A05;
    @Comparable(type = 3)
    public boolean A06;

    public int A0M() {
        return 3;
    }

    public C17020yC() {
        super("PasswordEditComponent");
    }

    public static void A00(Context context, MigColorScheme migColorScheme, C29819EjC ejC, C29825EjI ejI) {
        C16930y3 B5d = AnonymousClass10J.A01.B5d(migColorScheme);
        if (C012609n.A02(B5d.AhV())) {
            B5d = AnonymousClass1JZ.A02;
        }
        int A002 = C007106r.A00(context, 5.0f);
        int A003 = C007106r.A00(context, (float) AnonymousClass1JQ.XSMALL.B3A());
        int A004 = C007106r.A00(context, (float) AnonymousClass1JQ.SMALL.B3A());
        ejC.setPadding(A003, A004, C007106r.A00(context, 28.0f) + A003, A004);
        ejC.setHintTextColor(AnonymousClass10J.A03.B5d(migColorScheme).AhV());
        ejC.setTextColor(AnonymousClass10J.A01.B5d(migColorScheme).AhV());
        ejC.setTextSize(2, (float) AnonymousClass10J.A01.mTextSize.B5k());
        ejI.setPadding(A002, A002, A002, A002);
        ejI.A00 = B5d.AhV();
        C29825EjI.A01(ejI);
        ejI.A01 = AnonymousClass10J.A03.B5d(migColorScheme).AhV();
        C29825EjI.A01(ejI);
        C29825EjI.A00(ejI);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007d, code lost:
        if (r1 == false) goto L_0x007f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0j(X.AnonymousClass0p4 r10, java.lang.Object r11) {
        /*
            r9 = this;
            android.widget.RelativeLayout r11 = (android.widget.RelativeLayout) r11
            X.2Nf r6 = r9.A03
            com.facebook.mig.scheme.interfaces.MigColorScheme r4 = r9.A04
            int r8 = r9.A00
            java.lang.CharSequence r7 = r9.A05
            boolean r1 = r9.A06
            r0 = 0
            android.view.View r3 = r11.getChildAt(r0)
            X.EjC r3 = (X.C29819EjC) r3
            r0 = 1
            android.view.View r5 = r11.getChildAt(r0)
            X.EjI r5 = (X.C29825EjI) r5
            X.10M r2 = X.AnonymousClass10M.ROBOTO_REGULAR
            android.content.Context r0 = r10.A09
            android.graphics.Typeface r0 = r2.A00(r0)
            r3.A00 = r0
            if (r0 == 0) goto L_0x0029
            r3.setTypeface(r0)
        L_0x0029:
            r3.setHint(r7)
            r3.setContentDescription(r7)
            r3.A05 = r1
            r3.A04 = r6
            r3.setImeOptions(r8)
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r2 <= r0) goto L_0x0049
            r0 = 1
            r3.setImportantForAutofill(r0)
            java.lang.String r0 = "password"
            java.lang.String[] r0 = new java.lang.String[]{r0}
            r3.setAutofillHints(r0)
        L_0x0049:
            android.text.Editable r0 = r3.getText()
            java.lang.String r2 = r0.toString()
            java.lang.String r0 = r6.A00
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x0074
            android.text.Editable r0 = r3.getText()
            java.lang.String r0 = r0.toString()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x006f
            java.lang.String r0 = r6.A00
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0074
        L_0x006f:
            java.lang.String r0 = r6.A00
            r3.setText(r0)
        L_0x0074:
            java.lang.String r0 = r6.A00
            if (r1 != 0) goto L_0x007f
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            r0 = 4
            if (r1 != 0) goto L_0x0080
        L_0x007f:
            r0 = 0
        L_0x0080:
            r5.setVisibility(r0)
            r5.A03 = r6
            boolean r2 = r5.A04
            boolean r1 = r6.A01
            if (r2 == r1) goto L_0x0093
            r5.A04 = r1
            X.C29825EjI.A01(r5)
            X.C29825EjI.A00(r5)
        L_0x0093:
            boolean r1 = r6.A01
            r0 = 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x009b
            r0 = 144(0x90, float:2.02E-43)
        L_0x009b:
            r0 = r0 | 1
            r3.setInputType(r0)
            r0 = 2132214783(0x7f1703ff, float:2.0073418E38)
            r3.setBackgroundResource(r0)
            android.content.Context r0 = r10.A09
            A00(r0, r4, r3, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17020yC.A0j(X.0p4, java.lang.Object):void");
    }
}
