package X;

/* renamed from: X.0v2  reason: invalid class name and case insensitive filesystem */
public interface C15290v2 {
    boolean isNestedScrollingEnabled();

    void setNestedScrollingEnabled(boolean z);

    void stopNestedScroll();
}
