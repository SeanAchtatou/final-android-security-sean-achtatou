package X;

/* renamed from: X.1MQ  reason: invalid class name */
public final class AnonymousClass1MQ implements AnonymousClass1M8 {
    public String getName() {
        return "rotation";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (r1 == false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public float Ab1(X.C17730zN r5) {
        /*
            r4 = this;
            X.1jd r3 = r5.A0A
            if (r3 == 0) goto L_0x0010
            int r2 = r3.A0A
            r0 = 2097152(0x200000, float:2.938736E-39)
            r2 = r2 & r0
            r1 = 0
            if (r2 == 0) goto L_0x000d
            r1 = 1
        L_0x000d:
            r0 = 1
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            if (r0 == 0) goto L_0x0018
            if (r3 == 0) goto L_0x0018
            float r0 = r3.A01
            return r0
        L_0x0018:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MQ.Ab1(X.0zN):float");
    }

    public float Ab2(Object obj) {
        return C17700zK.A02(obj, this).getRotation();
    }

    public void C3Y(Object obj) {
        C17700zK.A02(obj, this).setRotation(0.0f);
    }

    public void C5f(Object obj, float f) {
        C17700zK.A02(obj, this).setRotation(f);
    }
}
