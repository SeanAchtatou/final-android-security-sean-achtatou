package X;

import com.facebook.mobileconfig.MobileConfigCxxChangeListener;
import com.facebook.mobileconfig.MobileConfigEmergencyPushChangeListener;
import com.facebook.tigon.iface.TigonServiceHolder;
import io.card.payment.BuildConfig;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Z7  reason: invalid class name */
public final class AnonymousClass1Z7 implements AnonymousClass0WV {
    public C04310Tq A00;
    public long[][] A01 = null;
    private C06180b1 A02 = null;
    public final String A03;
    public final String A04;
    public final List A05 = new ArrayList();
    public final Set A06 = new HashSet();
    public final Set A07 = new HashSet();

    public static boolean A02(String str, String str2) {
        if (str == null || str.isEmpty() || str2 == null || str2.isEmpty()) {
            return false;
        }
        int indexOf = str.indexOf(58);
        if (indexOf != -1) {
            str = str.substring(0, indexOf);
        }
        int indexOf2 = str2.indexOf(58);
        if (indexOf2 != -1) {
            str2 = str2.substring(0, indexOf2);
        }
        return str.equals(str2);
    }

    public void clearAlternativeUpdater() {
    }

    public void clearOverrides() {
    }

    public void deleteOldUserData(int i) {
    }

    public String getConsistencyLoggingFlagsJSON() {
        return "Internal error: MobileConfig manager not yet initialized";
    }

    public String getFrameworkStatus() {
        return "UNINITIALIZED";
    }

    public C25851aV getNewOverridesTable() {
        return null;
    }

    public boolean isConsistencyLoggingNeeded(AnonymousClass8ZO r2) {
        return false;
    }

    public boolean isFetchNeeded() {
        return false;
    }

    public boolean isTigonServiceSet() {
        return false;
    }

    public boolean isValid() {
        return true;
    }

    public void logConfigs(String str, AnonymousClass8ZO r2, Map map) {
    }

    public void logStorageConsistency() {
    }

    public boolean registerConfigChangeListener(MobileConfigCxxChangeListener mobileConfigCxxChangeListener) {
        return false;
    }

    public boolean setEpHandler(MobileConfigEmergencyPushChangeListener mobileConfigEmergencyPushChangeListener) {
        return false;
    }

    public boolean setSandboxURL(String str) {
        return false;
    }

    public void setTigonService(TigonServiceHolder tigonServiceHolder, boolean z) {
    }

    public boolean tryUpdateConfigsSynchronously(int i) {
        return false;
    }

    public boolean updateConfigsSynchronouslyWithDefaultUpdater(int i) {
        return false;
    }

    public boolean updateEmergencyPushConfigs() {
        return false;
    }

    public boolean updateEmergencyPushConfigsSynchronously(int i) {
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x008d A[Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b3 A[Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(X.AnonymousClass1Z6 r8, X.C06190b2 r9) {
        /*
            if (r9 != 0) goto L_0x0035
            r2 = 0
        L_0x0003:
            java.lang.String r9 = ""
            if (r2 != 0) goto L_0x000e
            if (r8 == 0) goto L_0x000d
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r8.A00 = r0
        L_0x000d:
            return r9
        L_0x000e:
            X.0b3 r5 = new X.0b3     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r5.<init>()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            java.nio.ByteOrder r0 = java.nio.ByteOrder.LITTLE_ENDIAN     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r2.order(r0)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r0 = r2.position()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r1 = r2.getInt(r0)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r0 = r2.position()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r1 = r1 + r0
            r5.A00 = r1     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r5.A01 = r2     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r1 = r5.A06()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r0 = 123456(0x1e240, float:1.72999E-40)
            if (r1 == r0) goto L_0x003f
            if (r8 == 0) goto L_0x000d
            goto L_0x003a
        L_0x0035:
            java.nio.ByteBuffer r2 = r9.getJavaByteBuffer()
            goto L_0x0003
        L_0x003a:
            java.lang.Integer r0 = X.AnonymousClass07B.A0i     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r8.A00 = r0     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            return r9
        L_0x003f:
            r0 = 4
            r4 = 1
            int r3 = r5.A02(r0)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            if (r3 != 0) goto L_0x0049
            r2 = 0
            goto L_0x0063
        L_0x0049:
            java.nio.ByteBuffer r1 = r2.duplicate()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            java.nio.ByteOrder r0 = java.nio.ByteOrder.LITTLE_ENDIAN     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            java.nio.ByteBuffer r2 = r1.order(r0)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r1 = r5.A03(r3)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r2.position(r1)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r0 = r5.A04(r3)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r0 = r0 * r4
            int r1 = r1 + r0
            r2.limit(r1)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
        L_0x0063:
            r7 = 0
            java.lang.String r8 = "FBConfigUtils"
            if (r2 != 0) goto L_0x006e
            java.lang.String r0 = "Invalid ByteBuffer passed. Forcing C++ manager creation."
            X.C010708t.A0J(r8, r0)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            return r7
        L_0x006e:
            java.lang.String r0 = "UTF-8"
            java.nio.charset.Charset r6 = java.nio.charset.Charset.forName(r0)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r5 = r2.limit()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r0 = r2.position()     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            int r5 = r5 - r0
            byte[] r3 = new byte[r5]     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r2.get(r3)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r0 = 32
            if (r5 == r0) goto L_0x008e
            r0 = 65
            if (r5 == r0) goto L_0x008e
        L_0x008a:
            r0 = 0
        L_0x008b:
            if (r0 != 0) goto L_0x00b3
            goto L_0x00ad
        L_0x008e:
            r2 = 0
        L_0x008f:
            if (r2 >= r5) goto L_0x00ab
            byte r1 = r3[r2]     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r0 = 97
            if (r1 < r0) goto L_0x009b
            r0 = 102(0x66, float:1.43E-43)
            if (r1 <= r0) goto L_0x00a8
        L_0x009b:
            r0 = 48
            if (r1 < r0) goto L_0x00a3
            r0 = 57
            if (r1 <= r0) goto L_0x00a8
        L_0x00a3:
            r0 = 58
            if (r1 == r0) goto L_0x00a8
            goto L_0x008a
        L_0x00a8:
            int r2 = r2 + 1
            goto L_0x008f
        L_0x00ab:
            r0 = 1
            goto L_0x008b
        L_0x00ad:
            java.lang.String r0 = "Invalid schema hash in flatbuffer. Forcing C++ manager creation."
            X.C010708t.A0J(r8, r0)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            return r7
        L_0x00b3:
            java.lang.String r7 = new java.lang.String     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            r0 = 0
            r7.<init>(r3, r0, r5, r6)     // Catch:{ IndexOutOfBoundsException | OutOfMemoryError | BufferUnderflowException -> 0x00ba }
            return r7
        L_0x00ba:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Z7.A00(X.1Z6, X.0b2):java.lang.String");
    }

    public static String A01(ReadableByteChannel readableByteChannel) {
        try {
            ByteBuffer allocate = ByteBuffer.allocate(6);
            allocate.order(ByteOrder.BIG_ENDIAN);
            if (readableByteChannel.read(allocate) == 6) {
                allocate.flip();
                if (Short.valueOf(allocate.getShort()).shortValue() == Short.MAX_VALUE && Short.valueOf(allocate.getShort()).shortValue() == 2) {
                    short shortValue = Short.valueOf(allocate.getShort()).shortValue();
                    ByteBuffer allocate2 = ByteBuffer.allocate(shortValue);
                    allocate2.order(ByteOrder.BIG_ENDIAN);
                    if (readableByteChannel.read(allocate2) == shortValue) {
                        allocate2.flip();
                        return Charset.forName("US-ASCII").decode(allocate2).toString();
                    }
                }
            }
            return BuildConfig.FLAVOR;
        } catch (IOException e) {
            C010708t.A0T("MobileConfigJavaManager", e, "populateTranslationTableInternal: IOException");
            return BuildConfig.FLAVOR;
        }
    }

    public C06190b2 getLatestHandle() {
        String A0J;
        int i;
        if (this.A02 == null) {
            String str = this.A04;
            if (str == null || str.isEmpty() || str.equals("0")) {
                A0J = AnonymousClass08S.A0J(this.A03, "sessionless.data/");
            } else {
                A0J = AnonymousClass08S.A0P(this.A03, str, ".data/");
            }
            File[] listFiles = new File(A0J).listFiles(new AnonymousClass1Z8());
            String str2 = BuildConfig.FLAVOR;
            if (listFiles != null) {
                int i2 = -1;
                for (File file : listFiles) {
                    String name = file.getName();
                    try {
                        i = Integer.parseInt(name.substring(0, name.length() - 8));
                    } catch (NumberFormatException unused) {
                        i = -1;
                    }
                    if (i > i2) {
                        str2 = file.getAbsolutePath();
                        i2 = i;
                    }
                }
            }
            this.A02 = new C06180b1(str2);
        }
        return this.A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0062, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0066 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C25851aV getNewOverridesTableIfExists() {
        /*
            r7 = this;
            java.io.File r6 = new java.io.File
            java.lang.String r1 = r7.A03
            java.lang.String r0 = "mc_overrides.json"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r6.<init>(r0)
            X.0Tq r0 = r7.A00
            if (r0 == 0) goto L_0x0067
            boolean r0 = r6.exists()
            if (r0 == 0) goto L_0x0067
            X.0Tq r0 = r7.A00
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x0067
            X.95P r5 = new X.95P     // Catch:{ IOException -> 0x0067 }
            java.lang.String r0 = "UTF-8"
            java.nio.charset.Charset r2 = java.nio.charset.Charset.forName(r0)     // Catch:{ IOException -> 0x0067 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0067 }
            r4.<init>()     // Catch:{ IOException -> 0x0067 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0067 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0067 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0067 }
            r0.<init>(r6)     // Catch:{ IOException -> 0x0067 }
            r1.<init>(r0, r2)     // Catch:{ IOException -> 0x0067 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0067 }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r2 = new char[r0]     // Catch:{ all -> 0x0060 }
        L_0x003f:
            int r1 = r3.read(r2)     // Catch:{ all -> 0x0060 }
            r0 = -1
            if (r1 == r0) goto L_0x004b
            r0 = 0
            r4.append(r2, r0, r1)     // Catch:{ all -> 0x0060 }
            goto L_0x003f
        L_0x004b:
            r3.close()     // Catch:{ IOException -> 0x0067 }
            java.lang.String r2 = r4.toString()     // Catch:{ IOException -> 0x0067 }
            long[][] r1 = r7.A01     // Catch:{ IOException -> 0x0067 }
            X.0Tq r0 = r7.A00     // Catch:{ IOException -> 0x0067 }
            java.lang.Object r0 = r0.get()     // Catch:{ IOException -> 0x0067 }
            X.95R r0 = (X.AnonymousClass95R) r0     // Catch:{ IOException -> 0x0067 }
            r5.<init>(r2, r1, r0)     // Catch:{ IOException -> 0x0067 }
            return r5
        L_0x0060:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0062 }
        L_0x0062:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0066 }
        L_0x0066:
            throw r0     // Catch:{ IOException -> 0x0067 }
        L_0x0067:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Z7.getNewOverridesTableIfExists():X.1aV");
    }

    public void logExposure(String str, String str2, String str3) {
        this.A06.add(new C31811kQ(str, str2, str3));
    }

    public void logShadowResult(String str, String str2, String str3, String str4, String str5, String str6) {
        synchronized (this.A05) {
            this.A05.add(new C30358Eui(str, str2, str3, str4, str5, str6));
        }
    }

    public String syncFetchReason() {
        if (this.A01 != null) {
            return "MobileConfigJavaManager: Using translation table.";
        }
        return "MobileConfigJavaManager: No sync fetch was needed";
    }

    public boolean updateConfigs() {
        return false;
    }

    public AnonymousClass1Z7(File file, String str) {
        this.A03 = AnonymousClass08S.A0J(file.getAbsolutePath(), "/mobileconfig/");
        this.A04 = str;
    }
}
