package X;

/* renamed from: X.0dm  reason: invalid class name and case insensitive filesystem */
public final class C07570dm {
    public final int collCount;
    public final int collEnd;
    public final C56002p6[] collList;
    public final int count;
    public final int longestCollisionList;
    public final int[] mainHash;
    public final int mainHashMask;
    public final C07580dn[] mainNames;

    public C07570dm(int i, int i2, int[] iArr, C07580dn[] r4, C56002p6[] r5, int i3, int i4, int i5) {
        this.count = i;
        this.mainHashMask = i2;
        this.mainHash = iArr;
        this.mainNames = r4;
        this.collList = r5;
        this.collCount = i3;
        this.collEnd = i4;
        this.longestCollisionList = i5;
    }

    public C07570dm(C26721bu r2) {
        this.count = r2._count;
        this.mainHashMask = r2._mainHashMask;
        this.mainHash = r2._mainHash;
        this.mainNames = r2._mainNames;
        this.collList = r2._collList;
        this.collCount = r2._collCount;
        this.collEnd = r2._collEnd;
        this.longestCollisionList = r2._longestCollisionList;
    }
}
