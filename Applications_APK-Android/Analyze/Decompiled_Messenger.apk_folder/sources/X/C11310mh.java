package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.RegularImmutableList;
import java.util.Iterator;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0mh  reason: invalid class name and case insensitive filesystem */
public final class C11310mh extends AnonymousClass0W1 {
    private static volatile C11310mh A06;
    private AnonymousClass0UN A00;
    public final AnonymousClass1YI A01;
    private final C11350mm A02 = new C11350mm();
    private final C11340ml A03 = new C11320mi();
    private final C11360mn A04;
    private final C11370mo A05;

    private C11310mh(AnonymousClass1XY r4) {
        super("threads", AnonymousClass1Y3.A2R, RegularImmutableList.A02);
        this.A00 = new AnonymousClass0UN(1, r4);
        this.A04 = C11360mn.A00(r4);
        this.A05 = new C11370mo(r4);
        this.A01 = AnonymousClass0WA.A00(r4);
    }

    public static final C11310mh A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (C11310mh.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new C11310mh(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    private void A01(SQLiteDatabase sQLiteDatabase) {
        String A002 = AnonymousClass0W4.A00("users");
        C007406x.A00(-376937270);
        sQLiteDatabase.execSQL(A002);
        C007406x.A00(1714080385);
        String A003 = AnonymousClass0W4.A00(C99084oO.$const$string(AnonymousClass1Y3.A1M));
        C007406x.A00(1743021280);
        sQLiteDatabase.execSQL(A003);
        C007406x.A00(-884310112);
        String A004 = AnonymousClass0W4.A00("archived_sms_mms_threads");
        C007406x.A00(1580415193);
        sQLiteDatabase.execSQL(A004);
        C007406x.A00(-896882091);
        String A005 = AnonymousClass0W4.A00("unread_mms_sms_threads");
        C007406x.A00(1069002663);
        sQLiteDatabase.execSQL(A005);
        C007406x.A00(1440761948);
        String A006 = AnonymousClass0W4.A00("group_clusters");
        C007406x.A00(936254714);
        sQLiteDatabase.execSQL(A006);
        C007406x.A00(-769856287);
        String A007 = AnonymousClass0W4.A00("pinned_threads");
        C007406x.A00(1673914210);
        sQLiteDatabase.execSQL(A007);
        C007406x.A00(637972450);
        String A008 = AnonymousClass0W4.A00("event_reminder_members");
        C007406x.A00(1579520608);
        sQLiteDatabase.execSQL(A008);
        C007406x.A00(-696300691);
        String A009 = AnonymousClass0W4.A00("properties");
        C007406x.A00(-1206660113);
        sQLiteDatabase.execSQL(A009);
        C007406x.A00(2007523574);
        String A0010 = AnonymousClass0W4.A00("folder_counts");
        C007406x.A00(-1375198787);
        sQLiteDatabase.execSQL(A0010);
        C007406x.A00(293271976);
        String A0011 = AnonymousClass0W4.A00("folders");
        C007406x.A00(78911892);
        sQLiteDatabase.execSQL(A0011);
        C007406x.A00(-1238042616);
        String A0012 = AnonymousClass0W4.A00("threads");
        C007406x.A00(-1662653402);
        sQLiteDatabase.execSQL(A0012);
        C007406x.A00(873574391);
        String A0013 = AnonymousClass0W4.A00("threads_metadata");
        C007406x.A00(1956609009);
        sQLiteDatabase.execSQL(A0013);
        C007406x.A00(334616655);
        String A0014 = AnonymousClass0W4.A00("messages");
        C007406x.A00(-1382618046);
        sQLiteDatabase.execSQL(A0014);
        C007406x.A00(964025005);
        String A0015 = AnonymousClass0W4.A00("thread_users");
        C007406x.A00(-1436899384);
        sQLiteDatabase.execSQL(A0015);
        C007406x.A00(1453939525);
        String A0016 = AnonymousClass0W4.A00("group_conversations");
        C007406x.A00(-1595157805);
        sQLiteDatabase.execSQL(A0016);
        C007406x.A00(136574905);
        String A0017 = AnonymousClass0W4.A00("ranked_threads");
        C007406x.A00(146001222);
        sQLiteDatabase.execSQL(A0017);
        C007406x.A00(1702771000);
        String A0018 = AnonymousClass0W4.A00("thread_participants");
        C007406x.A00(-1976252192);
        sQLiteDatabase.execSQL(A0018);
        C007406x.A00(-1127497134);
        String A0019 = AnonymousClass0W4.A00(AnonymousClass24B.$const$string(76));
        C007406x.A00(-424255118);
        sQLiteDatabase.execSQL(A0019);
        C007406x.A00(1561036044);
        String A0020 = AnonymousClass0W4.A00("montage_message_reactions");
        C007406x.A00(-1177072298);
        sQLiteDatabase.execSQL(A0020);
        C007406x.A00(709094722);
        String A0021 = AnonymousClass0W4.A00("event_reminders");
        C007406x.A00(628645776);
        sQLiteDatabase.execSQL(A0021);
        C007406x.A00(521414117);
        String A0022 = AnonymousClass0W4.A00("fb_events");
        C007406x.A00(1475579175);
        sQLiteDatabase.execSQL(A0022);
        C007406x.A00(154565471);
        String A0023 = AnonymousClass0W4.A00("fb_event_members");
        C007406x.A00(1528553948);
        sQLiteDatabase.execSQL(A0023);
        C007406x.A00(-898783725);
        String A0024 = AnonymousClass0W4.A00("montage_directs");
        C007406x.A00(891451422);
        sQLiteDatabase.execSQL(A0024);
        C007406x.A00(-2066227308);
        String A0025 = AnonymousClass0W4.A00("montage_message_poll");
        C007406x.A00(1060334796);
        sQLiteDatabase.execSQL(A0025);
        C007406x.A00(-464474267);
        String A0026 = AnonymousClass0W4.A00("montage_message_poll_options");
        C007406x.A00(-24526526);
        sQLiteDatabase.execSQL(A0026);
        C007406x.A00(244960084);
        String A0027 = AnonymousClass0W4.A00("montage_message_interactive_overlays");
        C007406x.A00(568075477);
        sQLiteDatabase.execSQL(A0027);
        C007406x.A00(154531245);
        String A0028 = AnonymousClass0W4.A00("thread_themes");
        C007406x.A00(1833078627);
        sQLiteDatabase.execSQL(A0028);
        C007406x.A00(523696144);
        A04(sQLiteDatabase);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static void A02(SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("initial_fetch_complete", (Integer) 0);
        sQLiteDatabase.updateWithOnConflict("threads", contentValues, "thread_key LIKE ?", new String[]{"GROUP:%"}, 5);
    }

    public static void A03(C11310mh r3, SQLiteDatabase sQLiteDatabase, boolean z) {
        C13590rh r2 = new C13590rh(sQLiteDatabase, r3.A03, z, r3.A02);
        C11380mp r1 = r3.A05;
        AnonymousClass0r8 r0 = r2.A00;
        if (r1 == null) {
            r1 = new AnonymousClass2RU();
        }
        r0.A0D(r1);
    }

    /* JADX INFO: finally extract failed */
    public void A07(SQLiteDatabase sQLiteDatabase) {
        C007406x.A01(sQLiteDatabase, 751016211);
        try {
            A03(this, sQLiteDatabase, false);
            sQLiteDatabase.setTransactionSuccessful();
            C007406x.A02(sQLiteDatabase, -543056132);
            super.A07(sQLiteDatabase);
            if (!sQLiteDatabase.isReadOnly()) {
                C007406x.A00(-330618298);
                sQLiteDatabase.execSQL("PRAGMA foreign_keys=ON;");
                C007406x.A00(-1992165322);
                if (!this.A01.AbO(AnonymousClass1Y3.A1S, false)) {
                    C28431en r3 = (C28431en) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AGw, this.A00);
                    synchronized (r3) {
                        if (!r3.A00) {
                            C007406x.A01(sQLiteDatabase, 1293378621);
                            C16520xF r5 = new C16520xF(sQLiteDatabase);
                            try {
                                Iterator it = r3.A01.iterator();
                                while (it.hasNext()) {
                                    it.next();
                                    C28041eA r4 = (C28041eA) C28431en.A02.A09("montage_thread_type");
                                    String str = null;
                                    String A032 = r5.A03(r4);
                                    if (A032 != null) {
                                        str = A032;
                                    }
                                    boolean equals = "montage".equals(str);
                                    if (!equals) {
                                        C007406x.A00(1560258872);
                                        sQLiteDatabase.execSQL("DELETE FROM messages WHERE thread_key IN (SELECT thread_key FROM folders WHERE folder IN ('montage', 'montage_groups'))");
                                        C007406x.A00(-1782865625);
                                        sQLiteDatabase.delete("threads", "folder = ?", new String[]{C10950l8.A06.dbName});
                                        sQLiteDatabase.delete("folders", "folder IN ('montage', 'montage_groups')", null);
                                        new C16520xF(sQLiteDatabase).A04(C12740pt.A01(C10950l8.A06));
                                        String str2 = "montage";
                                        if (equals) {
                                            str2 = "inbox";
                                        }
                                        if (str2 != null) {
                                            r5.A06(r4, str2);
                                        }
                                    }
                                }
                                sQLiteDatabase.setTransactionSuccessful();
                                C007406x.A02(sQLiteDatabase, -1943226332);
                                r3.A00 = true;
                            } catch (Throwable th) {
                                C007406x.A02(sQLiteDatabase, 1859079560);
                                throw th;
                            }
                        }
                    }
                }
            }
        } catch (Throwable th2) {
            C007406x.A02(sQLiteDatabase, -923075294);
            throw th2;
        }
    }

    /* JADX WARN: Type inference failed for: r5v6, types: [java.lang.String[], android.database.Cursor] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:353:0x33d6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:354:0x33d7, code lost:
        if (r3 != null) goto L_0x33d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:356:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:357:0x33dc, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.database.sqlite.SQLiteDatabase r75, int r76, int r77) {
        /*
            r74 = this;
            r3 = r76
        L_0x0002:
            r0 = 300(0x12c, float:4.2E-43)
            r1 = r75
            r2 = r74
            if (r3 >= r0) goto L_0x33dd
            r7 = r77
            int r16 = r3 + 1
            X.1YI r5 = r2.A01
            r4 = 185(0xb9, float:2.59E-43)
            r0 = 0
            boolean r6 = r5.AbO(r4, r0)
            r4 = 233(0xe9, float:3.27E-43)
            r5 = 205(0xcd, float:2.87E-43)
            r0 = 205(0xcd, float:2.87E-43)
            if (r6 == 0) goto L_0x0021
            r0 = 233(0xe9, float:3.27E-43)
        L_0x0021:
            if (r3 >= r0) goto L_0x0028
            r2.A01(r1)
            r3 = r7
            goto L_0x0002
        L_0x0028:
            if (r3 != r5) goto L_0x003f
            r0 = -647987093(0xffffffffd960806b, float:-3.94947449E15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN video_room_mode INTEGER"
            r1.execSQL(r0)
            r0 = 1020226445(0x3ccf6b8d, float:0.025319839)
            X.C007406x.A00(r0)
        L_0x003b:
            r7 = r16
        L_0x003d:
            r3 = r7
            goto L_0x0002
        L_0x003f:
            r0 = 206(0xce, float:2.89E-43)
            if (r3 != r0) goto L_0x0055
            r0 = -1026363503(0xffffffffc2d2ef91, float:-105.4679)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN nested_menu_call_to_actions TEXT"
            r1.execSQL(r0)
            r0 = -1712905235(0xffffffff99e723ed, float:-2.389935E-23)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x0055:
            r0 = 207(0xcf, float:2.9E-43)
            if (r3 != r0) goto L_0x007c
            r0 = 1740010135(0x67b67297, float:1.723169E24)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN send_error_detail TEXT"
            r1.execSQL(r0)
            r0 = -757867148(0xffffffffd2d3dd74, float:-4.54976733E11)
            X.C007406x.A00(r0)
            r0 = -1032657549(0xffffffffc272e573, float:-60.72407)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN send_error_original_exception TEXT"
            r1.execSQL(r0)
            r0 = -52100475(0xfffffffffce50285, float:-9.5126967E36)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x007c:
            r0 = 208(0xd0, float:2.91E-43)
            if (r3 != r0) goto L_0x0092
            r0 = -485470076(0xffffffffe3105084, float:-2.662133E21)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_broadcast_recipient_holdout INTEGER"
            r1.execSQL(r0)
            r0 = 1899682144(0x713ad960, float:9.25232E29)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x0092:
            r0 = 209(0xd1, float:2.93E-43)
            if (r3 != r0) goto L_0x00a8
            r0 = -889406395(0xffffffffcafcbc45, float:-8281634.5)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN marketplace_data TEXT"
            r1.execSQL(r0)
            r0 = -1047654268(0xffffffffc18e1084, float:-17.758064)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x00a8:
            r0 = 210(0xd2, float:2.94E-43)
            if (r3 != r0) goto L_0x00bf
            r0 = 1851234307(0x6e579803, float:1.6680762E28)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN room_creation_time INTEGER"
            r1.execSQL(r0)
            r0 = -1160648659(0xffffffffbad1e82d, float:-0.0016014628)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x00bf:
            r0 = 211(0xd3, float:2.96E-43)
            if (r3 != r0) goto L_0x021b
            X.0W6 r11 = new X.0W6
            java.lang.String r2 = "TEXT"
            java.lang.String r0 = "thread_key"
            r11.<init>(r0, r2)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "user_key"
            r12.<init>(r0, r2)
            X.0W6 r10 = new X.0W6
            java.lang.String r3 = "type"
            r0 = 128(0x80, float:1.794E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r10.<init>(r3, r0)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "phone"
            r13.<init>(r0, r2)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "sms_participant_fbid"
            r9.<init>(r0, r2)
            X.0W6 r8 = new X.0W6
            java.lang.String r7 = "INTEGER"
            java.lang.String r0 = "is_admin"
            r8.<init>(r0, r7)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "last_read_receipt_time"
            r6.<init>(r0, r7)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "last_read_receipt_watermark_time"
            r5.<init>(r0, r7)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "last_delivered_receipt_time"
            r4.<init>(r0, r7)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "last_delivered_receipt_id"
            r3.<init>(r0, r2)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "request_timestamp_ms"
            r2.<init>(r0, r7)
            r24 = r5
            r25 = r4
            r26 = r3
            r27 = r2
            r17 = r11
            r18 = r12
            r19 = r13
            r20 = r9
            r21 = r10
            r22 = r8
            r23 = r6
            com.google.common.collect.ImmutableList r7 = com.google.common.collect.ImmutableList.of(r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r11, r12, r10)
            com.google.common.collect.ImmutableList r8 = com.google.common.collect.ImmutableList.of(r11)
            java.lang.String r6 = "thread_participants"
            java.lang.String r5 = "threads"
            java.lang.String r3 = "ON DELETE CASCADE"
            X.1Xv r9 = r8.iterator()
        L_0x0146:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x016a
            java.lang.Object r0 = r9.next()
            X.0W6 r0 = (X.AnonymousClass0W6) r0
            java.lang.String r2 = r0.A00
            java.lang.String r0 = "DELETE FROM %s where %s NOT IN (SELECT %s FROM %s)"
            java.lang.String r2 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r6, r2, r2, r5)
            r0 = 887488074(0x34e5fe4a, float:4.2839594E-7)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 424113740(0x1947764c, float:1.0311949E-23)
            X.C007406x.A00(r0)
            goto L_0x0146
        L_0x016a:
            X.3lk r2 = new X.3lk
            r2.<init>(r8, r5, r8, r3)
            com.google.common.collect.ImmutableMap r8 = com.google.common.collect.RegularImmutableMap.A03
            X.0al r0 = new X.0al
            r0.<init>(r4)
            com.google.common.collect.ImmutableList r5 = com.google.common.collect.ImmutableList.of(r0, r2)
            java.lang.String r0 = "_temp"
            java.lang.String r3 = X.AnonymousClass08S.A0J(r6, r0)
            java.lang.String r0 = ", "
            com.google.common.base.Joiner r2 = com.google.common.base.Joiner.on(r0)
            com.google.common.base.Function r0 = X.AnonymousClass0W6.A03
            java.util.Collection r0 = X.AnonymousClass0TH.A00(r7, r0)
            java.lang.String r4 = r2.join(r0)
            java.lang.String r0 = ", "
            com.google.common.base.Joiner r2 = com.google.common.base.Joiner.on(r0)
            X.5qf r0 = new X.5qf
            r0.<init>(r8)
            java.util.Collection r0 = X.AnonymousClass0TH.A00(r7, r0)
            java.lang.String r2 = r2.join(r0)
            java.lang.String r0 = "INSERT INTO %s SELECT %s FROM %s"
            java.lang.String r8 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r3, r4, r6)
            java.lang.String r4 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r6, r2, r3)
            java.lang.String r0 = "CREATE TEMPORARY TABLE"
            java.lang.String r2 = X.AnonymousClass0W4.A04(r3, r7, r5, r0)
            r0 = 473449026(0x1c384242, float:6.096613E-22)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = -1605506033(0xffffffffa04dec0f, float:-1.7442281E-19)
            X.C007406x.A00(r0)
            r0 = -979498907(0xffffffffc59e0865, float:-5057.0493)
            X.C007406x.A00(r0)
            r1.execSQL(r8)
            r0 = 1446436650(0x5636df2a, float:5.02674E13)
            X.C007406x.A00(r0)
            java.lang.String r2 = X.AnonymousClass0W4.A00(r6)
            r0 = -1085276998(0xffffffffbf4ffcba, float:-0.81245005)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = -53664751(0xfffffffffccd2411, float:-8.521219E36)
            X.C007406x.A00(r0)
            java.lang.String r2 = X.AnonymousClass0W4.A03(r6, r7, r5)
            r0 = 171421176(0xa37adf8, float:8.8438494E-33)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 731399037(0x2b98437d, float:1.0818981E-12)
            X.C007406x.A00(r0)
            r0 = 1619715909(0x608ae745, float:8.007236E19)
            X.C007406x.A00(r0)
            r1.execSQL(r4)
            r0 = 232419042(0xdda6ee2, float:1.3461981E-30)
            X.C007406x.A00(r0)
            java.lang.String r2 = X.AnonymousClass0W4.A00(r3)
            r0 = -1317361625(0xffffffffb17aa827, float:-3.6475372E-9)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 1744157989(0x67f5bd25, float:2.3209378E24)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x021b:
            r0 = 212(0xd4, float:2.97E-43)
            if (r3 != r0) goto L_0x0232
            r0 = -830631728(0xffffffffce7d90d0, float:-1.06353152E9)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN group_thread_category TEXT"
            r1.execSQL(r0)
            r0 = -935988931(0xffffffffc835f13d, float:-186308.95)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x0232:
            r0 = 213(0xd5, float:2.98E-43)
            if (r3 != r0) goto L_0x02a1
            r0 = -423853423(0xffffffffe6bc8291, float:-4.451067E23)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN are_admins_supported INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = -398984578(0xffffffffe837fa7e, float:-3.4752553E24)
            X.C007406x.A00(r0)
            r0 = -1844229552(0xffffffff92134a50, float:-4.647662E-28)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN group_thread_add_mode TEXT"
            r1.execSQL(r0)
            r0 = -817484379(0xffffffffcf462da5, float:-3.32488013E9)
            X.C007406x.A00(r0)
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            r0 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "are_admins_supported"
            r6.put(r0, r2)
            java.lang.String r3 = "group_thread_add_mode"
            java.lang.String r0 = "INVITE"
            r6.put(r3, r0)
            java.lang.String r2 = "group_thread_category"
            java.lang.String r0 = "ROOM"
            r6.put(r2, r0)
            java.lang.String r0 = "1"
            java.lang.String[] r8 = new java.lang.String[]{r0}
            java.lang.String r5 = "threads"
            java.lang.String r7 = "is_joinable = ?"
            r9 = 5
            r4 = r1
            r4.updateWithOnConflict(r5, r6, r7, r8, r9)
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r0 = "ADD"
            r6.put(r3, r0)
            java.lang.String r0 = "GROUP"
            r6.put(r2, r0)
            java.lang.String r0 = "0"
            java.lang.String[] r8 = new java.lang.String[]{r0}
            r4.updateWithOnConflict(r5, r6, r7, r8, r9)
            A02(r1)
            goto L_0x003b
        L_0x02a1:
            r0 = 214(0xd6, float:3.0E-43)
            if (r3 != r0) goto L_0x030d
            X.0W6 r10 = new X.0W6
            java.lang.String r2 = "TEXT"
            java.lang.String r0 = "msg_id"
            r10.<init>(r0, r2)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "user_key"
            r9.<init>(r0, r2)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "emoji"
            r8.<init>(r0, r2)
            X.0W6 r7 = new X.0W6
            java.lang.String r2 = "INTEGER"
            java.lang.String r0 = "offset"
            r7.<init>(r0, r2)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "timestamp"
            r6.<init>(r0, r2)
            X.3lk r5 = new X.3lk
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r10)
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r10)
            java.lang.String r2 = "messages"
            java.lang.String r0 = "ON DELETE CASCADE"
            r5.<init>(r4, r2, r3, r0)
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r10, r9, r8, r7, r6)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r5)
            java.lang.String r0 = "montage_message_reactions"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r0, r3, r2)
            r0 = 1207143015(0x47f38a67, float:124692.805)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 2106009772(0x7d8728ac, float:2.245712E37)
            X.C007406x.A00(r0)
            r0 = 1644812326(0x6209d826, float:6.356948E20)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE UNIQUE INDEX IF NOT EXISTS unique_reaction_on_message_by_user_at_offset ON montage_message_reactions(msg_id, user_key, emoji, offset)"
            r1.execSQL(r0)
            r0 = -546362744(0xffffffffdf6f2a88, float:-1.7233736E19)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x030d:
            r0 = 215(0xd7, float:3.01E-43)
            if (r3 != r0) goto L_0x05fc
            X.0W6 r43 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "msg_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r2 = "thread_key"
            r0 = r42
            r0.<init>(r2, r15)
            X.0W6 r41 = new X.0W6
            java.lang.String r7 = "INTEGER"
            java.lang.String r2 = "action_id"
            r0 = r41
            r0.<init>(r2, r7)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "text"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "sender"
            r0 = r39
            r0.<init>(r2, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "is_not_forwardable"
            r0 = r38
            r0.<init>(r2, r7)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r37
            r0.<init>(r2, r7)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "timestamp_sent_ms"
            r0 = r36
            r0.<init>(r2, r7)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "attachments"
            r0 = r35
            r0.<init>(r2, r15)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "shares"
            r0 = r34
            r0.<init>(r2, r15)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "sticker_id"
            r0 = r33
            r0.<init>(r2, r15)
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "msg_type"
            r0 = r32
            r0.<init>(r2, r7)
            r0 = 50
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r71 = r0
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "affected_users"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "coordinates"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "offline_threading_id"
            r0 = r29
            r0.<init>(r2, r15)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "source"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "channel_source"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "send_channel"
            r0 = r26
            r0.<init>(r2, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "is_non_authoritative"
            r0 = r25
            r0.<init>(r2, r7)
            X.0W6 r24 = new X.0W6
            java.lang.String r2 = "STRING"
            java.lang.String r0 = "pending_send_media_attachment"
            r3 = r24
            r3.<init>(r0, r2)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "sent_share_attachment"
            r3 = r23
            r3.<init>(r0, r2)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "client_tags"
            r3 = r22
            r3.<init>(r0, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "send_error"
            r3 = r21
            r3.<init>(r0, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "send_error_message"
            r3 = r20
            r3.<init>(r0, r2)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "send_error_number"
            r3 = r19
            r3.<init>(r0, r7)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "send_error_timestamp_ms"
            r3 = r18
            r3.<init>(r0, r7)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "send_error_error_url"
            r3 = r17
            r3.<init>(r0, r2)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "publicity"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "send_queue_type"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "payment_transaction"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "payment_request"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "has_unavailable_attachment"
            r10.<init>(r0, r7)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "app_attribution"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "content_app_attribution"
            r8.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "xma"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "admin_text_type"
            r5.<init>(r0, r7)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "admin_text_theme_color"
            r4.<init>(r0, r7)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "admin_text_thread_icon_emoji"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "admin_text_nickname"
            r2.<init>(r0, r15)
            r51 = r24
            r52 = r23
            r53 = r22
            r54 = r21
            r55 = r20
            r56 = r19
            r57 = r18
            r58 = r17
            r59 = r14
            r60 = r13
            r61 = r12
            r62 = r11
            r63 = r10
            r64 = r9
            r65 = r8
            r66 = r6
            r67 = r5
            r68 = r4
            r69 = r3
            r70 = r2
            r44 = r31
            r45 = r30
            r46 = r29
            r47 = r28
            r48 = r27
            r49 = r26
            r50 = r25
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70}
            r3 = 0
            r0 = 27
            r10 = r71
            java.lang.System.arraycopy(r4, r3, r10, r3, r0)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "admin_text_target_id"
            r0 = r26
            r0.<init>(r2, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "admin_text_thread_message_lifetime"
            r0 = r25
            r0.<init>(r2, r7)
            X.0W6 r24 = new X.0W6
            java.lang.String r2 = "message_lifetime"
            r0 = r24
            r0.<init>(r2, r7)
            X.0W6 r23 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_event"
            r0 = r23
            r0.<init>(r2, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_server_info_data"
            r0 = r22
            r0.<init>(r2, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_is_video_call"
            r0 = r21
            r0.<init>(r2, r7)
            X.0W6 r20 = new X.0W6
            java.lang.String r2 = "admin_text_thread_ride_provider_name"
            r0 = r20
            r0.<init>(r2, r15)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "is_sponsored"
            r2 = r19
            r2.<init>(r0, r7)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "admin_text_thread_ad_properties"
            r2 = r18
            r2.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "admin_text_game_score_data"
            r2 = r17
            r2.<init>(r0, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "admin_text_thread_event_reminder_properties"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "commerce_message_type"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "customizations"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "admin_text_joinable_event_type"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "metadata_at_text_ranges"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "platform_metadata"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "admin_text_is_joinable_promo"
            r8.<init>(r0, r7)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "admin_text_agent_intent_id"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "montage_reply_message_id"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "generic_admin_message_extensible_data"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "profile_ranges"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "send_error_detail"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "send_error_original_exception"
            r2.<init>(r0, r15)
            r51 = r19
            r52 = r18
            r53 = r17
            r54 = r14
            r55 = r13
            r56 = r12
            r57 = r11
            r58 = r10
            r59 = r9
            r60 = r8
            r61 = r7
            r62 = r6
            r63 = r5
            r64 = r4
            r65 = r3
            r66 = r2
            r44 = r26
            r45 = r25
            r46 = r24
            r47 = r23
            r48 = r22
            r49 = r21
            r50 = r20
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66}
            r3 = 0
            r2 = 27
            r0 = 23
            r6 = r71
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r43
            r3 = r42
            r4 = r41
            r5 = r40
            r6 = r39
            r7 = r38
            r8 = r37
            r9 = r36
            r10 = r35
            r11 = r34
            r12 = r33
            r13 = r32
            r14 = r71
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r43)
            r3.<init>(r0)
            java.lang.String r2 = "messages"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = -1165001223(0xffffffffba8f7df9, float:-0.0010947577)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_timestamp_index ON messages(thread_key, timestamp_ms DESC)"
            r1.execSQL(r0)
            r0 = -712955338(0xffffffffd5812a36, float:-1.77522868E13)
            X.C007406x.A00(r0)
            r0 = 1148543760(0x44756310, float:981.54785)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_offline_threading_id_index ON messages(offline_threading_id)"
            r1.execSQL(r0)
            r0 = -115796587(0xfffffffff9191595, float:-4.9678697E34)
            X.C007406x.A00(r0)
            r0 = -395551864(0xffffffffe86c5b88, float:-4.4646678E24)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_type_index ON messages(thread_key, msg_type, timestamp_ms)"
            r1.execSQL(r0)
            r0 = -1746565567(0xffffffff97e58641, float:-1.4832669E-24)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x05fc:
            r0 = 216(0xd8, float:3.03E-43)
            if (r3 != r0) goto L_0x08c8
            X.0W6 r44 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "thread_key"
            r0 = r44
            r0.<init>(r2, r15)
            X.0W6 r43 = new X.0W6
            java.lang.String r2 = "legacy_thread_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r4 = "INTEGER"
            java.lang.String r2 = "action_id"
            r0 = r42
            r0.<init>(r2, r4)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "refetch_action_id"
            r0 = r41
            r0.<init>(r2, r4)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "last_visible_action_id"
            r0 = r40
            r0.<init>(r2, r4)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "sequence_id"
            r0 = r39
            r0.<init>(r2, r4)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "name"
            r0 = r38
            r0.<init>(r2, r15)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "senders"
            r0 = r37
            r0.<init>(r2, r15)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "snippet"
            r0 = r36
            r0.<init>(r2, r15)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "snippet_sender"
            r0 = r35
            r0.<init>(r2, r15)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "admin_snippet"
            r0 = r34
            r0.<init>(r2, r15)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r33
            r0.<init>(r2, r4)
            r0 = 48
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r72 = r0
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "last_read_timestamp_ms"
            r0 = r32
            r0.<init>(r2, r4)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "approx_total_message_count"
            r0 = r31
            r0.<init>(r2, r4)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "unread_message_count"
            r0 = r30
            r0.<init>(r2, r4)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "last_fetch_time_ms"
            r0 = r29
            r0.<init>(r2, r4)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "pic_hash"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "pic"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "can_reply_to"
            r0 = r26
            r0.<init>(r2, r4)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "cannot_reply_reason"
            r5 = r25
            r5.<init>(r0, r15)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "last_message_admin_text_type"
            r5 = r24
            r5.<init>(r0, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "mute_until"
            r2 = r23
            r2.<init>(r0, r4)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "is_subscribed"
            r2 = r22
            r2.<init>(r0, r4)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "folder"
            r5 = r21
            r5.<init>(r0, r15)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "draft"
            r5 = r20
            r5.<init>(r0, r15)
            X.0W6 r19 = new X.0W6
            java.lang.String r2 = "INTEGER DEFAULT 0"
            java.lang.String r0 = "missed_call_status"
            r5 = r19
            r5.<init>(r0, r2)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "me_bubble_color"
            r5 = r18
            r5.<init>(r0, r4)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "other_bubble_color"
            r5 = r17
            r5.<init>(r0, r4)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "wallpaper_color"
            r14.<init>(r0, r4)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "initial_fetch_complete"
            r13.<init>(r0, r4)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "custom_like_emoji"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "outgoing_message_lifetime"
            r11.<init>(r0, r4)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "custom_nicknames"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "invite_uri"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "is_last_message_sponsored"
            r8.<init>(r0, r4)
            X.0W6 r7 = new X.0W6
            java.lang.String r3 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r7.<init>(r3, r0)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "game_data"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "is_joinable"
            r5.<init>(r0, r2)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "requires_approval"
            r3.<init>(r0, r2)
            r52 = r25
            r53 = r24
            r54 = r23
            r55 = r22
            r56 = r21
            r57 = r20
            r58 = r19
            r59 = r18
            r60 = r17
            r61 = r14
            r62 = r13
            r63 = r12
            r64 = r11
            r65 = r10
            r66 = r9
            r67 = r8
            r68 = r7
            r69 = r6
            r70 = r5
            r71 = r3
            r45 = r32
            r46 = r31
            r47 = r30
            r48 = r29
            r49 = r28
            r50 = r27
            r51 = r26
            X.0W6[] r6 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71}
            r5 = 0
            r0 = 27
            r8 = r72
            java.lang.System.arraycopy(r6, r5, r8, r5, r0)
            X.0W6 r24 = new X.0W6
            java.lang.String r3 = "rtc_call_info"
            r0 = r24
            r0.<init>(r3, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r3 = "last_message_commerce_message_type"
            r0 = r23
            r0.<init>(r3, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r3 = "is_thread_queue_enabled"
            r0 = r22
            r0.<init>(r3, r4)
            X.0W6 r21 = new X.0W6
            java.lang.String r3 = "group_description"
            r0 = r21
            r0.<init>(r3, r15)
            X.0W6 r20 = new X.0W6
            java.lang.String r3 = "media_preview"
            r0 = r20
            r0.<init>(r3, r15)
            X.0W6 r19 = new X.0W6
            java.lang.String r3 = "booking_requests"
            r0 = r19
            r0.<init>(r3, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r3 = "last_call_ms"
            r0 = r18
            r0.<init>(r3, r4)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "is_discoverable"
            r5 = r17
            r5.<init>(r0, r4)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "last_sponsored_message_call_to_action"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "montage_thread_key"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "room_privacy_mode"
            r12.<init>(r0, r4)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_id"
            r11.<init>(r0, r4)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_name"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "room_associated_photo_uri"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "approval_toggleable"
            r8.<init>(r0, r4)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "video_room_mode"
            r7.<init>(r0, r4)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "marketplace_data"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "room_creation_time"
            r5.<init>(r0, r4)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "group_thread_category"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "are_admins_supported"
            r3.<init>(r0, r2)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "group_thread_add_mode"
            r2.<init>(r0, r15)
            r52 = r17
            r53 = r14
            r54 = r13
            r55 = r12
            r56 = r11
            r57 = r10
            r58 = r9
            r59 = r8
            r60 = r7
            r61 = r6
            r62 = r5
            r63 = r4
            r64 = r3
            r65 = r2
            r45 = r24
            r46 = r23
            r47 = r22
            r48 = r21
            r49 = r20
            r50 = r19
            r51 = r18
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65}
            r3 = 0
            r2 = 27
            r0 = 21
            r6 = r72
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r44
            r3 = r43
            r4 = r42
            r5 = r41
            r6 = r40
            r7 = r39
            r8 = r38
            r9 = r37
            r10 = r36
            r11 = r35
            r12 = r34
            r13 = r33
            r14 = r72
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r44)
            r3.<init>(r0)
            java.lang.String r2 = "threads"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = -394081248(0xffffffffe882cc20, float:-4.9413847E24)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS threads_legacy_thread_id_index ON threads(legacy_thread_id)"
            r1.execSQL(r0)
            r0 = -245475089(0xfffffffff15e58ef, float:-1.101011E30)
            X.C007406x.A00(r0)
            r0 = 45108021(0x2b04b35, float:2.5904042E-37)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE UNIQUE INDEX IF NOT EXISTS threads_montage_thread_key_index ON threads(montage_thread_key)"
            r1.execSQL(r0)
            r0 = -684540888(0xffffffffd732bc28, float:-1.96521195E14)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x08c8:
            r0 = 217(0xd9, float:3.04E-43)
            if (r3 != r0) goto L_0x08df
            r0 = -245637844(0xfffffffff15bdd2c, float:-1.08871356E30)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN group_thread_offline_threading_id INTEGER"
            r1.execSQL(r0)
            r0 = -91170990(0xfffffffffa90d752, float:-3.76029E35)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x08df:
            r0 = 218(0xda, float:3.05E-43)
            if (r3 != r0) goto L_0x08f6
            r0 = 1024534005(0x3d1125f5, float:0.03543659)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_reply_action TEXT"
            r1.execSQL(r0)
            r0 = -1222083550(0xffffffffb7287c22, float:-1.0042482E-5)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x08f6:
            r0 = 219(0xdb, float:3.07E-43)
            if (r3 != r0) goto L_0x090d
            r0 = -1560309654(0xffffffffa2ff906a, float:-6.9270793E-18)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN had_messenger_call INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = -1639560008(0xffffffff9e464cb8, float:-1.0497898E-20)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x090d:
            r0 = 220(0xdc, float:3.08E-43)
            if (r3 != r0) goto L_0x0924
            r0 = -11797647(0xffffffffff4bfb71, float:-2.7113884E38)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN personal_group_invite_link TEXT"
            r1.execSQL(r0)
            r0 = 1770515986(0x6987ee12, float:2.0541155E25)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x0924:
            r0 = 221(0xdd, float:3.1E-43)
            if (r3 != r0) goto L_0x093b
            r0 = -1854758856(0xffffffff9172a038, float:-1.9139805E-28)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN extensible_message_data TEXT"
            r1.execSQL(r0)
            r0 = -618428979(0xffffffffdb2385cd, float:-4.6027537E16)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x093b:
            r0 = 222(0xde, float:3.11E-43)
            if (r3 != r0) goto L_0x0c1d
            X.0W6 r44 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "thread_key"
            r0 = r44
            r0.<init>(r2, r15)
            X.0W6 r43 = new X.0W6
            java.lang.String r2 = "legacy_thread_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r3 = "INTEGER"
            java.lang.String r2 = "action_id"
            r0 = r42
            r0.<init>(r2, r3)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "last_visible_action_id"
            r0 = r41
            r0.<init>(r2, r3)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "sequence_id"
            r0 = r40
            r0.<init>(r2, r3)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "name"
            r0 = r39
            r0.<init>(r2, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "senders"
            r0 = r38
            r0.<init>(r2, r15)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "snippet"
            r0 = r37
            r0.<init>(r2, r15)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "snippet_sender"
            r0 = r36
            r0.<init>(r2, r15)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "admin_snippet"
            r0 = r35
            r0.<init>(r2, r15)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r34
            r0.<init>(r2, r3)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "last_read_timestamp_ms"
            r0 = r33
            r0.<init>(r2, r3)
            r0 = 50
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r72 = r0
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "approx_total_message_count"
            r0 = r32
            r0.<init>(r2, r3)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "unread_message_count"
            r0 = r31
            r0.<init>(r2, r3)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "last_fetch_time_ms"
            r0 = r30
            r0.<init>(r2, r3)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "pic_hash"
            r0 = r29
            r0.<init>(r2, r15)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "pic"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "can_reply_to"
            r0 = r27
            r0.<init>(r2, r3)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "cannot_reply_reason"
            r0 = r26
            r0.<init>(r2, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "last_message_admin_text_type"
            r4 = r25
            r4.<init>(r0, r15)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "mute_until"
            r4 = r24
            r4.<init>(r0, r3)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "is_subscribed"
            r4 = r23
            r4.<init>(r0, r3)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "folder"
            r4 = r22
            r4.<init>(r0, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "draft"
            r4 = r21
            r4.<init>(r0, r15)
            X.0W6 r20 = new X.0W6
            java.lang.String r2 = "INTEGER DEFAULT 0"
            java.lang.String r0 = "missed_call_status"
            r4 = r20
            r4.<init>(r0, r2)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "me_bubble_color"
            r4 = r19
            r4.<init>(r0, r3)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "other_bubble_color"
            r4 = r18
            r4.<init>(r0, r3)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "wallpaper_color"
            r4 = r17
            r4.<init>(r0, r3)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "initial_fetch_complete"
            r14.<init>(r0, r3)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "custom_like_emoji"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "outgoing_message_lifetime"
            r12.<init>(r0, r3)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "custom_nicknames"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "invite_uri"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "is_last_message_sponsored"
            r9.<init>(r0, r3)
            X.0W6 r8 = new X.0W6
            java.lang.String r4 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r8.<init>(r4, r0)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "game_data"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "is_joinable"
            r6.<init>(r0, r2)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "requires_approval"
            r5.<init>(r0, r2)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "rtc_call_info"
            r4.<init>(r0, r15)
            r52 = r25
            r53 = r24
            r54 = r23
            r55 = r22
            r56 = r21
            r57 = r20
            r58 = r19
            r59 = r18
            r60 = r17
            r61 = r14
            r62 = r13
            r63 = r12
            r64 = r11
            r65 = r10
            r66 = r9
            r67 = r8
            r68 = r7
            r69 = r6
            r70 = r5
            r71 = r4
            r45 = r32
            r46 = r31
            r47 = r30
            r48 = r29
            r49 = r28
            r50 = r27
            r51 = r26
            X.0W6[] r6 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71}
            r5 = 0
            r0 = 27
            r8 = r72
            java.lang.System.arraycopy(r6, r5, r8, r5, r0)
            X.0W6 r26 = new X.0W6
            java.lang.String r4 = "last_message_commerce_message_type"
            r0 = r26
            r0.<init>(r4, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r4 = "is_thread_queue_enabled"
            r0 = r25
            r0.<init>(r4, r3)
            X.0W6 r24 = new X.0W6
            java.lang.String r4 = "group_description"
            r0 = r24
            r0.<init>(r4, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r4 = "media_preview"
            r0 = r23
            r0.<init>(r4, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r4 = "booking_requests"
            r0 = r22
            r0.<init>(r4, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r4 = "last_call_ms"
            r0 = r21
            r0.<init>(r4, r3)
            X.0W6 r20 = new X.0W6
            java.lang.String r4 = "is_discoverable"
            r0 = r20
            r0.<init>(r4, r3)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "last_sponsored_message_call_to_action"
            r4 = r19
            r4.<init>(r0, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "montage_thread_key"
            r4 = r18
            r4.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "room_privacy_mode"
            r4 = r17
            r4.<init>(r0, r3)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_id"
            r14.<init>(r0, r3)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_name"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "room_associated_photo_uri"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "approval_toggleable"
            r11.<init>(r0, r3)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "video_room_mode"
            r10.<init>(r0, r3)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "marketplace_data"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "room_creation_time"
            r8.<init>(r0, r3)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "group_thread_category"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "are_admins_supported"
            r6.<init>(r0, r2)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "group_thread_add_mode"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "group_thread_offline_threading_id"
            r4.<init>(r0, r3)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "had_messenger_call"
            r3.<init>(r0, r2)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "personal_group_invite_link"
            r2.<init>(r0, r15)
            r52 = r19
            r53 = r18
            r54 = r17
            r55 = r14
            r56 = r13
            r57 = r12
            r58 = r11
            r59 = r10
            r60 = r9
            r61 = r8
            r62 = r7
            r63 = r6
            r64 = r5
            r65 = r4
            r66 = r3
            r67 = r2
            r45 = r26
            r46 = r25
            r47 = r24
            r48 = r23
            r49 = r22
            r50 = r21
            r51 = r20
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67}
            r3 = 0
            r2 = 27
            r0 = 23
            r6 = r72
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r44
            r3 = r43
            r4 = r42
            r5 = r41
            r6 = r40
            r7 = r39
            r8 = r38
            r9 = r37
            r10 = r36
            r11 = r35
            r12 = r34
            r13 = r33
            r14 = r72
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r44)
            r3.<init>(r0)
            java.lang.String r2 = "threads"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = -996047012(0xffffffffc4a1875c, float:-1292.23)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS threads_legacy_thread_id_index ON threads(legacy_thread_id)"
            r1.execSQL(r0)
            r0 = 644921278(0x2670b7be, float:8.3515744E-16)
            X.C007406x.A00(r0)
            r0 = -1381172746(0xffffffffadacf9f6, float:-1.9665141E-11)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE UNIQUE INDEX IF NOT EXISTS threads_montage_thread_key_index ON threads(montage_thread_key)"
            r1.execSQL(r0)
            r0 = 942114638(0x3827874e, float:3.9941915E-5)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x0c1d:
            r0 = 223(0xdf, float:3.12E-43)
            if (r3 != r0) goto L_0x0c59
            X.0W6 r6 = new X.0W6
            java.lang.String r2 = "folder"
            java.lang.String r0 = "TEXT"
            r6.<init>(r2, r0)
            X.0W6 r5 = new X.0W6
            java.lang.String r4 = "INTEGER"
            java.lang.String r0 = "unread_count"
            r5.<init>(r0, r4)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "unseen_count"
            r3.<init>(r0, r4)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "last_seen_time"
            r2.<init>(r0, r4)
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r6, r5, r3, r2)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r6)
            r3.<init>(r0)
            java.lang.String r2 = "folder_counts"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            goto L_0x003b
        L_0x0c59:
            r0 = 224(0xe0, float:3.14E-43)
            if (r3 != r0) goto L_0x0f30
            X.0W6 r44 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "thread_key"
            r0 = r44
            r0.<init>(r2, r15)
            X.0W6 r43 = new X.0W6
            java.lang.String r2 = "legacy_thread_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r3 = "INTEGER"
            java.lang.String r2 = "action_id"
            r0 = r42
            r0.<init>(r2, r3)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "sequence_id"
            r0 = r41
            r0.<init>(r2, r3)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "name"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "senders"
            r0 = r39
            r0.<init>(r2, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "snippet"
            r0 = r38
            r0.<init>(r2, r15)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "snippet_sender"
            r0 = r37
            r0.<init>(r2, r15)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "admin_snippet"
            r0 = r36
            r0.<init>(r2, r15)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r35
            r0.<init>(r2, r3)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "last_read_timestamp_ms"
            r0 = r34
            r0.<init>(r2, r3)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "approx_total_message_count"
            r0 = r33
            r0.<init>(r2, r3)
            r0 = 49
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r72 = r0
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "unread_message_count"
            r0 = r32
            r0.<init>(r2, r3)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "last_fetch_time_ms"
            r0 = r31
            r0.<init>(r2, r3)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "pic_hash"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "pic"
            r0 = r29
            r0.<init>(r2, r15)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "can_reply_to"
            r0 = r28
            r0.<init>(r2, r3)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "cannot_reply_reason"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "last_message_admin_text_type"
            r0 = r26
            r0.<init>(r2, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "mute_until"
            r4 = r25
            r4.<init>(r0, r3)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "is_subscribed"
            r4 = r24
            r4.<init>(r0, r3)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "folder"
            r4 = r23
            r4.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "draft"
            r4 = r22
            r4.<init>(r0, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r2 = "INTEGER DEFAULT 0"
            java.lang.String r0 = "missed_call_status"
            r4 = r21
            r4.<init>(r0, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "me_bubble_color"
            r4 = r20
            r4.<init>(r0, r3)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "other_bubble_color"
            r4 = r19
            r4.<init>(r0, r3)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "wallpaper_color"
            r4 = r18
            r4.<init>(r0, r3)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "initial_fetch_complete"
            r4 = r17
            r4.<init>(r0, r3)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "custom_like_emoji"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "outgoing_message_lifetime"
            r13.<init>(r0, r3)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "custom_nicknames"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "invite_uri"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "is_last_message_sponsored"
            r10.<init>(r0, r3)
            X.0W6 r9 = new X.0W6
            java.lang.String r4 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r9.<init>(r4, r0)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "game_data"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "is_joinable"
            r7.<init>(r0, r2)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "requires_approval"
            r6.<init>(r0, r2)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "rtc_call_info"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "last_message_commerce_message_type"
            r4.<init>(r0, r15)
            r52 = r25
            r53 = r24
            r54 = r23
            r55 = r22
            r56 = r21
            r57 = r20
            r58 = r19
            r59 = r18
            r60 = r17
            r61 = r14
            r62 = r13
            r63 = r12
            r64 = r11
            r65 = r10
            r66 = r9
            r67 = r8
            r68 = r7
            r69 = r6
            r70 = r5
            r71 = r4
            r45 = r32
            r46 = r31
            r47 = r30
            r48 = r29
            r49 = r28
            r50 = r27
            r51 = r26
            X.0W6[] r6 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71}
            r5 = 0
            r0 = 27
            r8 = r72
            java.lang.System.arraycopy(r6, r5, r8, r5, r0)
            X.0W6 r25 = new X.0W6
            java.lang.String r4 = "is_thread_queue_enabled"
            r0 = r25
            r0.<init>(r4, r3)
            X.0W6 r24 = new X.0W6
            java.lang.String r4 = "group_description"
            r0 = r24
            r0.<init>(r4, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r4 = "media_preview"
            r0 = r23
            r0.<init>(r4, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r4 = "booking_requests"
            r0 = r22
            r0.<init>(r4, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r4 = "last_call_ms"
            r0 = r21
            r0.<init>(r4, r3)
            X.0W6 r20 = new X.0W6
            java.lang.String r4 = "is_discoverable"
            r0 = r20
            r0.<init>(r4, r3)
            X.0W6 r19 = new X.0W6
            java.lang.String r4 = "last_sponsored_message_call_to_action"
            r0 = r19
            r0.<init>(r4, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "montage_thread_key"
            r4 = r18
            r4.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "room_privacy_mode"
            r4 = r17
            r4.<init>(r0, r3)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_id"
            r14.<init>(r0, r3)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_name"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "room_associated_photo_uri"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "approval_toggleable"
            r11.<init>(r0, r3)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "video_room_mode"
            r10.<init>(r0, r3)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "marketplace_data"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "room_creation_time"
            r8.<init>(r0, r3)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "group_thread_category"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "are_admins_supported"
            r6.<init>(r0, r2)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "group_thread_add_mode"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "group_thread_offline_threading_id"
            r4.<init>(r0, r3)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "had_messenger_call"
            r3.<init>(r0, r2)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "personal_group_invite_link"
            r2.<init>(r0, r15)
            r52 = r18
            r53 = r17
            r54 = r14
            r55 = r13
            r56 = r12
            r57 = r11
            r58 = r10
            r59 = r9
            r60 = r8
            r61 = r7
            r62 = r6
            r63 = r5
            r64 = r4
            r65 = r3
            r66 = r2
            r45 = r25
            r46 = r24
            r47 = r23
            r48 = r22
            r49 = r21
            r50 = r20
            r51 = r19
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66}
            r3 = 0
            r2 = 27
            r0 = 22
            r6 = r72
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r44
            r3 = r43
            r4 = r42
            r5 = r41
            r6 = r40
            r7 = r39
            r8 = r38
            r9 = r37
            r10 = r36
            r11 = r35
            r12 = r34
            r13 = r33
            r14 = r72
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r44)
            r3.<init>(r0)
            java.lang.String r2 = "threads"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = -213871752(0xfffffffff3409378, float:-1.5257447E31)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS threads_legacy_thread_id_index ON threads(legacy_thread_id)"
            r1.execSQL(r0)
            r0 = -1228345207(0xffffffffb6c8f089, float:-5.9884665E-6)
            X.C007406x.A00(r0)
            r0 = 461551945(0x1b82b949, float:2.1626434E-22)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE UNIQUE INDEX IF NOT EXISTS threads_montage_thread_key_index ON threads(montage_thread_key)"
            r1.execSQL(r0)
            r0 = 1840601416(0x6db55948, float:7.01559E27)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x0f30:
            r0 = 225(0xe1, float:3.15E-43)
            if (r3 != r0) goto L_0x122a
            X.0W6 r43 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "msg_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r2 = "thread_key"
            r0 = r42
            r0.<init>(r2, r15)
            X.0W6 r41 = new X.0W6
            java.lang.String r9 = "INTEGER"
            java.lang.String r2 = "action_id"
            r0 = r41
            r0.<init>(r2, r9)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "text"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "sender"
            r0 = r39
            r0.<init>(r2, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "is_not_forwardable"
            r0 = r38
            r0.<init>(r2, r9)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r37
            r0.<init>(r2, r9)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "timestamp_sent_ms"
            r0 = r36
            r0.<init>(r2, r9)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "attachments"
            r0 = r35
            r0.<init>(r2, r15)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "shares"
            r0 = r34
            r0.<init>(r2, r15)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "sticker_id"
            r0 = r33
            r0.<init>(r2, r15)
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "msg_type"
            r0 = r32
            r0.<init>(r2, r9)
            r0 = 51
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r71 = r0
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "affected_users"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "coordinates"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "offline_threading_id"
            r0 = r29
            r0.<init>(r2, r15)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "source"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "channel_source"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "send_channel"
            r0 = r26
            r0.<init>(r2, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "is_non_authoritative"
            r0 = r25
            r0.<init>(r2, r9)
            X.0W6 r24 = new X.0W6
            java.lang.String r2 = "STRING"
            java.lang.String r0 = "pending_send_media_attachment"
            r3 = r24
            r3.<init>(r0, r2)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "sent_share_attachment"
            r3 = r23
            r3.<init>(r0, r2)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "client_tags"
            r3 = r22
            r3.<init>(r0, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "send_error"
            r3 = r21
            r3.<init>(r0, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "send_error_message"
            r3 = r20
            r3.<init>(r0, r2)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "send_error_number"
            r3 = r19
            r3.<init>(r0, r9)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "send_error_timestamp_ms"
            r3 = r18
            r3.<init>(r0, r9)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "send_error_error_url"
            r3 = r17
            r3.<init>(r0, r2)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "publicity"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "send_queue_type"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "payment_transaction"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "payment_request"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "has_unavailable_attachment"
            r10.<init>(r0, r9)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "app_attribution"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "content_app_attribution"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "xma"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "admin_text_type"
            r5.<init>(r0, r9)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "admin_text_theme_color"
            r4.<init>(r0, r9)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "admin_text_thread_icon_emoji"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "admin_text_nickname"
            r2.<init>(r0, r15)
            r51 = r24
            r52 = r23
            r53 = r22
            r54 = r21
            r55 = r20
            r56 = r19
            r57 = r18
            r58 = r17
            r59 = r14
            r60 = r13
            r61 = r12
            r62 = r11
            r63 = r10
            r64 = r8
            r65 = r7
            r66 = r6
            r67 = r5
            r68 = r4
            r69 = r3
            r70 = r2
            r44 = r31
            r45 = r30
            r46 = r29
            r47 = r28
            r48 = r27
            r49 = r26
            r50 = r25
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70}
            r3 = 0
            r0 = 27
            r6 = r71
            java.lang.System.arraycopy(r4, r3, r6, r3, r0)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "admin_text_target_id"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "admin_text_thread_message_lifetime"
            r0 = r26
            r0.<init>(r2, r9)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "message_lifetime"
            r0 = r25
            r0.<init>(r2, r9)
            X.0W6 r24 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_event"
            r0 = r24
            r0.<init>(r2, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_server_info_data"
            r0 = r23
            r0.<init>(r2, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_is_video_call"
            r0 = r22
            r0.<init>(r2, r9)
            X.0W6 r21 = new X.0W6
            java.lang.String r2 = "admin_text_thread_ride_provider_name"
            r0 = r21
            r0.<init>(r2, r15)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "is_sponsored"
            r2 = r20
            r2.<init>(r0, r9)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "admin_text_thread_ad_properties"
            r2 = r19
            r2.<init>(r0, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "admin_text_game_score_data"
            r2 = r18
            r2.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "admin_text_thread_event_reminder_properties"
            r2 = r17
            r2.<init>(r0, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "commerce_message_type"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "admin_text_joinable_event_type"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "metadata_at_text_ranges"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "platform_metadata"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "admin_text_is_joinable_promo"
            r10.<init>(r0, r9)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "admin_text_agent_intent_id"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "montage_reply_message_id"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "generic_admin_message_extensible_data"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "profile_ranges"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "send_error_detail"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "send_error_original_exception"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "montage_reply_action"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "extensible_message_data"
            r2.<init>(r0, r15)
            r51 = r20
            r52 = r19
            r53 = r18
            r54 = r17
            r55 = r14
            r56 = r13
            r57 = r12
            r58 = r11
            r59 = r10
            r60 = r9
            r61 = r8
            r62 = r7
            r63 = r6
            r64 = r5
            r65 = r4
            r66 = r3
            r67 = r2
            r44 = r27
            r45 = r26
            r46 = r25
            r47 = r24
            r48 = r23
            r49 = r22
            r50 = r21
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67}
            r3 = 0
            r2 = 27
            r0 = 24
            r6 = r71
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r43
            r3 = r42
            r4 = r41
            r5 = r40
            r6 = r39
            r7 = r38
            r8 = r37
            r9 = r36
            r10 = r35
            r11 = r34
            r12 = r33
            r13 = r32
            r14 = r71
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r43)
            r3.<init>(r0)
            java.lang.String r2 = "messages"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = 747282301(0x2c8a9f7d, float:3.9399027E-12)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_timestamp_index ON messages(thread_key, timestamp_ms DESC)"
            r1.execSQL(r0)
            r0 = -824556452(0xffffffffceda445c, float:-1.83095654E9)
            X.C007406x.A00(r0)
            r0 = 1895864621(0x7100992d, float:6.3678815E29)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_offline_threading_id_index ON messages(offline_threading_id)"
            r1.execSQL(r0)
            r0 = -96202015(0xfffffffffa4412e1, float:-2.5451827E35)
            X.C007406x.A00(r0)
            r0 = 49782462(0x2f79ebe, float:3.6384502E-37)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_type_index ON messages(thread_key, msg_type, timestamp_ms)"
            r1.execSQL(r0)
            r0 = -414624760(0xffffffffe7495408, float:-9.507458E23)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x122a:
            r0 = 226(0xe2, float:3.17E-43)
            if (r3 != r0) goto L_0x14f6
            X.0W6 r44 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "thread_key"
            r0 = r44
            r0.<init>(r2, r15)
            X.0W6 r43 = new X.0W6
            java.lang.String r2 = "legacy_thread_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r3 = "INTEGER"
            java.lang.String r2 = "sequence_id"
            r0 = r42
            r0.<init>(r2, r3)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "name"
            r0 = r41
            r0.<init>(r2, r15)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "senders"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "snippet"
            r0 = r39
            r0.<init>(r2, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "snippet_sender"
            r0 = r38
            r0.<init>(r2, r15)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "admin_snippet"
            r0 = r37
            r0.<init>(r2, r15)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r36
            r0.<init>(r2, r3)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "last_read_timestamp_ms"
            r0 = r35
            r0.<init>(r2, r3)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "approx_total_message_count"
            r0 = r34
            r0.<init>(r2, r3)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "unread_message_count"
            r0 = r33
            r0.<init>(r2, r3)
            r0 = 48
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r72 = r0
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "last_fetch_time_ms"
            r0 = r32
            r0.<init>(r2, r3)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "pic_hash"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "pic"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "can_reply_to"
            r0 = r29
            r0.<init>(r2, r3)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "cannot_reply_reason"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "last_message_admin_text_type"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "mute_until"
            r0 = r26
            r0.<init>(r2, r3)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "is_subscribed"
            r4 = r25
            r4.<init>(r0, r3)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "folder"
            r4 = r24
            r4.<init>(r0, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "draft"
            r4 = r23
            r4.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r2 = "INTEGER DEFAULT 0"
            java.lang.String r0 = "missed_call_status"
            r4 = r22
            r4.<init>(r0, r2)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "me_bubble_color"
            r4 = r21
            r4.<init>(r0, r3)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "other_bubble_color"
            r4 = r20
            r4.<init>(r0, r3)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "wallpaper_color"
            r4 = r19
            r4.<init>(r0, r3)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "initial_fetch_complete"
            r4 = r18
            r4.<init>(r0, r3)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "custom_like_emoji"
            r4 = r17
            r4.<init>(r0, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "outgoing_message_lifetime"
            r14.<init>(r0, r3)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "custom_nicknames"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "invite_uri"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "is_last_message_sponsored"
            r11.<init>(r0, r3)
            X.0W6 r10 = new X.0W6
            java.lang.String r4 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r10.<init>(r4, r0)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "game_data"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "is_joinable"
            r8.<init>(r0, r2)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "requires_approval"
            r7.<init>(r0, r2)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "rtc_call_info"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "last_message_commerce_message_type"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "is_thread_queue_enabled"
            r4.<init>(r0, r3)
            r52 = r25
            r53 = r24
            r54 = r23
            r55 = r22
            r56 = r21
            r57 = r20
            r58 = r19
            r59 = r18
            r60 = r17
            r61 = r14
            r62 = r13
            r63 = r12
            r64 = r11
            r65 = r10
            r66 = r9
            r67 = r8
            r68 = r7
            r69 = r6
            r70 = r5
            r71 = r4
            r45 = r32
            r46 = r31
            r47 = r30
            r48 = r29
            r49 = r28
            r50 = r27
            r51 = r26
            X.0W6[] r6 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71}
            r5 = 0
            r0 = 27
            r8 = r72
            java.lang.System.arraycopy(r6, r5, r8, r5, r0)
            X.0W6 r24 = new X.0W6
            java.lang.String r4 = "group_description"
            r0 = r24
            r0.<init>(r4, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r4 = "media_preview"
            r0 = r23
            r0.<init>(r4, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r4 = "booking_requests"
            r0 = r22
            r0.<init>(r4, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r4 = "last_call_ms"
            r0 = r21
            r0.<init>(r4, r3)
            X.0W6 r20 = new X.0W6
            java.lang.String r4 = "is_discoverable"
            r0 = r20
            r0.<init>(r4, r3)
            X.0W6 r19 = new X.0W6
            java.lang.String r4 = "last_sponsored_message_call_to_action"
            r0 = r19
            r0.<init>(r4, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r4 = "montage_thread_key"
            r0 = r18
            r0.<init>(r4, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "room_privacy_mode"
            r4 = r17
            r4.<init>(r0, r3)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_id"
            r14.<init>(r0, r3)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_name"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "room_associated_photo_uri"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "approval_toggleable"
            r11.<init>(r0, r3)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "video_room_mode"
            r10.<init>(r0, r3)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "marketplace_data"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "room_creation_time"
            r8.<init>(r0, r3)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "group_thread_category"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "are_admins_supported"
            r6.<init>(r0, r2)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "group_thread_add_mode"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "group_thread_offline_threading_id"
            r4.<init>(r0, r3)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "had_messenger_call"
            r3.<init>(r0, r2)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "personal_group_invite_link"
            r2.<init>(r0, r15)
            r52 = r17
            r53 = r14
            r54 = r13
            r55 = r12
            r56 = r11
            r57 = r10
            r58 = r9
            r59 = r8
            r60 = r7
            r61 = r6
            r62 = r5
            r63 = r4
            r64 = r3
            r65 = r2
            r45 = r24
            r46 = r23
            r47 = r22
            r48 = r21
            r49 = r20
            r50 = r19
            r51 = r18
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65}
            r3 = 0
            r2 = 27
            r0 = 21
            r6 = r72
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r44
            r3 = r43
            r4 = r42
            r5 = r41
            r6 = r40
            r7 = r39
            r8 = r38
            r9 = r37
            r10 = r36
            r11 = r35
            r12 = r34
            r13 = r33
            r14 = r72
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r44)
            r3.<init>(r0)
            java.lang.String r2 = "threads"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = 498625528(0x1db86bf8, float:4.881603E-21)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS threads_legacy_thread_id_index ON threads(legacy_thread_id)"
            r1.execSQL(r0)
            r0 = -1767696946(0xffffffff96a315ce, float:-2.634783E-25)
            X.C007406x.A00(r0)
            r0 = -1732520909(0xffffffff98bbd433, float:-4.8552634E-24)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE UNIQUE INDEX IF NOT EXISTS threads_montage_thread_key_index ON threads(montage_thread_key)"
            r1.execSQL(r0)
            r0 = -833347616(0xffffffffce541fe0, float:-8.8971469E8)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x14f6:
            r0 = 227(0xe3, float:3.18E-43)
            if (r3 != r0) goto L_0x150d
            r0 = 1850134634(0x6e46d06a, float:1.5382497E28)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE event_reminders ADD COLUMN event_reminder_creator_user_key TEXT"
            r1.execSQL(r0)
            r0 = 68108680(0x40f4188, float:1.683966E-36)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x150d:
            r0 = 228(0xe4, float:3.2E-43)
            if (r3 != r0) goto L_0x17fc
            X.0W6 r43 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "msg_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r2 = "thread_key"
            r0 = r42
            r0.<init>(r2, r15)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "text"
            r0 = r41
            r0.<init>(r2, r15)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "sender"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r9 = "INTEGER"
            java.lang.String r2 = "is_not_forwardable"
            r0 = r39
            r0.<init>(r2, r9)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r38
            r0.<init>(r2, r9)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "timestamp_sent_ms"
            r0 = r37
            r0.<init>(r2, r9)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "attachments"
            r0 = r36
            r0.<init>(r2, r15)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "shares"
            r0 = r35
            r0.<init>(r2, r15)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "sticker_id"
            r0 = r34
            r0.<init>(r2, r15)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "msg_type"
            r0 = r33
            r0.<init>(r2, r9)
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "affected_users"
            r0 = r32
            r0.<init>(r2, r15)
            r0 = 50
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r71 = r0
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "coordinates"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "offline_threading_id"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "source"
            r0 = r29
            r0.<init>(r2, r15)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "channel_source"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "send_channel"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "is_non_authoritative"
            r0 = r26
            r0.<init>(r2, r9)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "STRING"
            java.lang.String r3 = "pending_send_media_attachment"
            r0 = r25
            r0.<init>(r3, r2)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "sent_share_attachment"
            r3 = r24
            r3.<init>(r0, r2)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "client_tags"
            r3 = r23
            r3.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "send_error"
            r3 = r22
            r3.<init>(r0, r2)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "send_error_message"
            r3 = r21
            r3.<init>(r0, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "send_error_number"
            r3 = r20
            r3.<init>(r0, r9)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "send_error_timestamp_ms"
            r3 = r19
            r3.<init>(r0, r9)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "send_error_error_url"
            r3 = r18
            r3.<init>(r0, r2)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "publicity"
            r2 = r17
            r2.<init>(r0, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "send_queue_type"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "payment_transaction"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "payment_request"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "has_unavailable_attachment"
            r11.<init>(r0, r9)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "app_attribution"
            r10.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "content_app_attribution"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "xma"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "admin_text_type"
            r6.<init>(r0, r9)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "admin_text_theme_color"
            r5.<init>(r0, r9)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "admin_text_thread_icon_emoji"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "admin_text_nickname"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "admin_text_target_id"
            r2.<init>(r0, r15)
            r51 = r24
            r52 = r23
            r53 = r22
            r54 = r21
            r55 = r20
            r56 = r19
            r57 = r18
            r58 = r17
            r59 = r14
            r60 = r13
            r61 = r12
            r62 = r11
            r63 = r10
            r64 = r8
            r65 = r7
            r66 = r6
            r67 = r5
            r68 = r4
            r69 = r3
            r70 = r2
            r44 = r31
            r45 = r30
            r46 = r29
            r47 = r28
            r48 = r27
            r49 = r26
            r50 = r25
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70}
            r3 = 0
            r0 = 27
            r6 = r71
            java.lang.System.arraycopy(r4, r3, r6, r3, r0)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "admin_text_thread_message_lifetime"
            r0 = r26
            r0.<init>(r2, r9)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "message_lifetime"
            r0 = r25
            r0.<init>(r2, r9)
            X.0W6 r24 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_event"
            r0 = r24
            r0.<init>(r2, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_server_info_data"
            r0 = r23
            r0.<init>(r2, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_is_video_call"
            r0 = r22
            r0.<init>(r2, r9)
            X.0W6 r21 = new X.0W6
            java.lang.String r2 = "admin_text_thread_ride_provider_name"
            r0 = r21
            r0.<init>(r2, r15)
            X.0W6 r20 = new X.0W6
            java.lang.String r2 = "is_sponsored"
            r0 = r20
            r0.<init>(r2, r9)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "admin_text_thread_ad_properties"
            r2 = r19
            r2.<init>(r0, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "admin_text_game_score_data"
            r2 = r18
            r2.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "admin_text_thread_event_reminder_properties"
            r2 = r17
            r2.<init>(r0, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "commerce_message_type"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "admin_text_joinable_event_type"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "metadata_at_text_ranges"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "platform_metadata"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "admin_text_is_joinable_promo"
            r10.<init>(r0, r9)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "admin_text_agent_intent_id"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "montage_reply_message_id"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "generic_admin_message_extensible_data"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "profile_ranges"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "send_error_detail"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "send_error_original_exception"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "montage_reply_action"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "extensible_message_data"
            r2.<init>(r0, r15)
            r51 = r19
            r52 = r18
            r53 = r17
            r54 = r14
            r55 = r13
            r56 = r12
            r57 = r11
            r58 = r10
            r59 = r9
            r60 = r8
            r61 = r7
            r62 = r6
            r63 = r5
            r64 = r4
            r65 = r3
            r66 = r2
            r44 = r26
            r45 = r25
            r46 = r24
            r47 = r23
            r48 = r22
            r49 = r21
            r50 = r20
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66}
            r3 = 0
            r2 = 27
            r0 = 23
            r6 = r71
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r43
            r3 = r42
            r4 = r41
            r5 = r40
            r6 = r39
            r7 = r38
            r8 = r37
            r9 = r36
            r10 = r35
            r11 = r34
            r12 = r33
            r13 = r32
            r14 = r71
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r43)
            r3.<init>(r0)
            java.lang.String r2 = "messages"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = -107767482(0xfffffffff9939946, float:-9.579705E34)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_timestamp_index ON messages(thread_key, timestamp_ms DESC)"
            r1.execSQL(r0)
            r0 = -42742186(0xfffffffffd73ce56, float:-2.025461E37)
            X.C007406x.A00(r0)
            r0 = -905201750(0xffffffffca0bb7aa, float:-2289130.5)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_offline_threading_id_index ON messages(offline_threading_id)"
            r1.execSQL(r0)
            r0 = -176716081(0xfffffffff57786cf, float:-3.1377724E32)
            X.C007406x.A00(r0)
            r0 = 1661989663(0x630ff31f, float:2.655403E21)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_type_index ON messages(thread_key, msg_type, timestamp_ms)"
            r1.execSQL(r0)
            r0 = 1253976571(0x4abe29fb, float:6231293.5)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x17fc:
            r0 = 229(0xe5, float:3.21E-43)
            if (r3 != r0) goto L_0x1813
            r0 = 97660484(0x5d22e44, float:1.97653E-35)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN maximum_messenger_version TEXT"
            r1.execSQL(r0)
            r0 = 2119518429(0x7e5548dd, float:7.0875973E37)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1813:
            r0 = 230(0xe6, float:3.22E-43)
            if (r3 != r0) goto L_0x182a
            r0 = 731470508(0x2b995aac, float:1.089647E-12)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN managing_ps TEXT"
            r1.execSQL(r0)
            r0 = 1048174969(0x3e79e179, float:0.24402417)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x182a:
            r0 = 231(0xe7, float:3.24E-43)
            if (r3 != r0) goto L_0x1841
            r0 = 1868724676(0x6f6279c4, float:7.009082E28)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN is_draft INTEGER"
            r1.execSQL(r0)
            r0 = -490172537(0xffffffffe2c88f87, float:-1.8498455E21)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1841:
            r0 = 232(0xe8, float:3.25E-43)
            if (r3 != r0) goto L_0x1858
            r0 = 1585674834(0x5e837a52, float:4.7369875E18)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN ad_context_data TEXT"
            r1.execSQL(r0)
            r0 = -95597302(0xfffffffffa4d4d0a, float:-2.6649585E35)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1858:
            if (r3 != r4) goto L_0x186d
            r0 = -1488063501(0xffffffffa74df3f3, float:-2.858171E-15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN use_existing_group INTEGER"
            r1.execSQL(r0)
            r0 = -1102768379(0xffffffffbe451705, float:-0.19247063)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x186d:
            r0 = 234(0xea, float:3.28E-43)
            if (r3 != r0) goto L_0x1895
            r0 = -1033554571(0xffffffffc2653575, float:-57.302204)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN thread_associated_object_type TEXT"
            r1.execSQL(r0)
            r0 = -382689832(0xffffffffe9309dd8, float:-1.3344771E25)
            X.C007406x.A00(r0)
            r0 = 676400997(0x28510f65, float:1.1605169E-14)
            X.C007406x.A00(r0)
            java.lang.String r0 = "UPDATE threads SET thread_associated_object_type='Group' WHERE room_associated_fb_group_id != 0"
            r1.execSQL(r0)
            r0 = -1973784121(0xffffffff8a5a71c7, float:-1.0517717E-32)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1895:
            r0 = 235(0xeb, float:3.3E-43)
            if (r3 != r0) goto L_0x199e
            X.0W6 r15 = new X.0W6
            java.lang.String r14 = "fb_event_id"
            java.lang.String r13 = "INTEGER"
            r15.<init>(r14, r13)
            X.0W6 r12 = new X.0W6
            java.lang.String r11 = "TEXT"
            r0 = 228(0xe4, float:3.2E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r12.<init>(r0, r11)
            X.0W6 r10 = new X.0W6
            r0 = 230(0xe6, float:3.22E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r10.<init>(r0, r13)
            X.0W6 r9 = new X.0W6
            r0 = 223(0xdf, float:3.12E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r9.<init>(r0, r13)
            X.0W6 r8 = new X.0W6
            r0 = 222(0xde, float:3.11E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r8.<init>(r0, r11)
            X.0W6 r7 = new X.0W6
            r0 = 224(0xe0, float:3.14E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r7.<init>(r0, r11)
            X.0W6 r6 = new X.0W6
            java.lang.String r2 = "REAL"
            r0 = 226(0xe2, float:3.17E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r6.<init>(r0, r2)
            X.0W6 r5 = new X.0W6
            r0 = 227(0xe3, float:3.18E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r5.<init>(r0, r2)
            X.0W6 r4 = new X.0W6
            r0 = 225(0xe1, float:3.15E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r4.<init>(r0, r13)
            X.0W6 r3 = new X.0W6
            r0 = 231(0xe7, float:3.24E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r3.<init>(r0, r11)
            X.0W6 r2 = new X.0W6
            r0 = 229(0xe5, float:3.21E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r2.<init>(r0, r11)
            r23 = r6
            r24 = r5
            r25 = r4
            r26 = r3
            r27 = r2
            r17 = r15
            r18 = r12
            r19 = r10
            r20 = r9
            r21 = r8
            r22 = r7
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            X.0al r2 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r15)
            r2.<init>(r0)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r2)
            java.lang.String r7 = "fb_events"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r7, r3, r0)
            r0 = -1145807628(0xffffffffbbb45cf4, float:-0.005504245)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = -545211253(0xffffffffdf80bc8b, float:-1.8552884E19)
            X.C007406x.A00(r0)
            X.0W6 r6 = new X.0W6
            r6.<init>(r14, r13)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "user_key"
            r9.<init>(r0, r11)
            X.0W6 r8 = new X.0W6
            r0 = 267(0x10b, float:3.74E-43)
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r8.<init>(r0, r11)
            X.0al r5 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r6, r9)
            r5.<init>(r0)
            X.3lk r4 = new X.3lk
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r6)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r15)
            java.lang.String r0 = "ON DELETE CASCADE"
            r4.<init>(r3, r7, r2, r0)
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r6, r9, r8)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r5, r4)
            java.lang.String r0 = "fb_event_members"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r0, r3, r2)
            r0 = -1796264093(0xffffffff94ef2f63, float:-2.4151524E-26)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 1350659362(0x50816d22, float:1.73713039E10)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x199e:
            r0 = 236(0xec, float:3.31E-43)
            if (r3 != r0) goto L_0x19b5
            r0 = 2126938990(0x7ec6836e, float:1.3193478E38)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_aloha_proxy_confirmed INTEGER"
            r1.execSQL(r0)
            r0 = 1626651312(0x60f4bab0, float:1.4107681E20)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x19b5:
            r0 = 237(0xed, float:3.32E-43)
            if (r3 != r0) goto L_0x1cb6
            r0 = -641141332(0xffffffffd9c8f5ac, float:-7.0706393E15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN optimistic_group_state INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = -1656483856(0xffffffff9d440ff0, float:-2.5948624E-21)
            X.C007406x.A00(r0)
            r0 = 338201062(0x142889e6, float:8.509028E-27)
            X.C007406x.A00(r0)
            java.lang.String r0 = "UPDATE threads SET optimistic_group_state = 3 WHERE is_draft = 1"
            r1.execSQL(r0)
            r0 = 369102772(0x16000fb4, float:1.0344713E-25)
            X.C007406x.A00(r0)
            X.0W6 r45 = new X.0W6
            java.lang.String r44 = "thread_key"
            java.lang.String r15 = "TEXT"
            r2 = r45
            r0 = r44
            r2.<init>(r0, r15)
            X.0W6 r43 = new X.0W6
            java.lang.String r2 = "legacy_thread_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r2 = "INTEGER"
            java.lang.String r3 = "sequence_id"
            r0 = r42
            r0.<init>(r3, r2)
            X.0W6 r41 = new X.0W6
            java.lang.String r3 = "name"
            r0 = r41
            r0.<init>(r3, r15)
            X.0W6 r40 = new X.0W6
            java.lang.String r3 = "senders"
            r0 = r40
            r0.<init>(r3, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r3 = "snippet"
            r0 = r39
            r0.<init>(r3, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r3 = "snippet_sender"
            r0 = r38
            r0.<init>(r3, r15)
            X.0W6 r37 = new X.0W6
            java.lang.String r3 = "admin_snippet"
            r0 = r37
            r0.<init>(r3, r15)
            X.0W6 r36 = new X.0W6
            java.lang.String r3 = "timestamp_ms"
            r0 = r36
            r0.<init>(r3, r2)
            X.0W6 r35 = new X.0W6
            java.lang.String r3 = "last_read_timestamp_ms"
            r0 = r35
            r0.<init>(r3, r2)
            X.0W6 r34 = new X.0W6
            java.lang.String r3 = "approx_total_message_count"
            r0 = r34
            r0.<init>(r3, r2)
            X.0W6 r33 = new X.0W6
            java.lang.String r0 = "unread_message_count"
            r3 = r33
            r3.<init>(r0, r2)
            r0 = 52
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r73 = r0
            X.0W6 r32 = new X.0W6
            java.lang.String r3 = "last_fetch_time_ms"
            r0 = r32
            r0.<init>(r3, r2)
            X.0W6 r31 = new X.0W6
            java.lang.String r3 = "pic_hash"
            r0 = r31
            r0.<init>(r3, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r3 = "pic"
            r0 = r30
            r0.<init>(r3, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r3 = "can_reply_to"
            r0 = r29
            r0.<init>(r3, r2)
            X.0W6 r28 = new X.0W6
            java.lang.String r3 = "cannot_reply_reason"
            r0 = r28
            r0.<init>(r3, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r3 = "last_message_admin_text_type"
            r0 = r27
            r0.<init>(r3, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r3 = "mute_until"
            r0 = r26
            r0.<init>(r3, r2)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "is_subscribed"
            r3 = r25
            r3.<init>(r0, r2)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "folder"
            r3 = r24
            r3.<init>(r0, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "draft"
            r3 = r23
            r3.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "missed_call_status"
            r3 = r22
            r3.<init>(r0, r2)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "me_bubble_color"
            r3 = r21
            r3.<init>(r0, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "other_bubble_color"
            r3 = r20
            r3.<init>(r0, r2)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "wallpaper_color"
            r3 = r19
            r3.<init>(r0, r2)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "initial_fetch_complete"
            r3 = r18
            r3.<init>(r0, r2)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "custom_like_emoji"
            r3 = r17
            r3.<init>(r0, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "outgoing_message_lifetime"
            r14.<init>(r0, r2)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "custom_nicknames"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "invite_uri"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "is_last_message_sponsored"
            r11.<init>(r0, r2)
            X.0W6 r10 = new X.0W6
            java.lang.String r3 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r10.<init>(r3, r0)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "game_data"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r6 = "INTEGER DEFAULT 0"
            java.lang.String r0 = "is_joinable"
            r8.<init>(r0, r6)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "requires_approval"
            r7.<init>(r0, r6)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "rtc_call_info"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "last_message_commerce_message_type"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "is_thread_queue_enabled"
            r3.<init>(r0, r2)
            r53 = r25
            r54 = r24
            r55 = r23
            r56 = r22
            r57 = r21
            r58 = r20
            r59 = r19
            r60 = r18
            r61 = r17
            r62 = r14
            r63 = r13
            r64 = r12
            r65 = r11
            r66 = r10
            r67 = r9
            r68 = r8
            r69 = r7
            r70 = r5
            r71 = r4
            r72 = r3
            r46 = r32
            r47 = r31
            r48 = r30
            r49 = r29
            r50 = r28
            r51 = r27
            r52 = r26
            X.0W6[] r5 = new X.AnonymousClass0W6[]{r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72}
            r4 = 0
            r0 = 27
            r9 = r73
            java.lang.System.arraycopy(r5, r4, r9, r4, r0)
            X.0W6 r28 = new X.0W6
            java.lang.String r3 = "group_description"
            r0 = r28
            r0.<init>(r3, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r3 = "media_preview"
            r0 = r27
            r0.<init>(r3, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r3 = "booking_requests"
            r0 = r26
            r0.<init>(r3, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r3 = "last_call_ms"
            r0 = r25
            r0.<init>(r3, r2)
            X.0W6 r24 = new X.0W6
            java.lang.String r3 = "is_discoverable"
            r0 = r24
            r0.<init>(r3, r2)
            X.0W6 r23 = new X.0W6
            java.lang.String r3 = "last_sponsored_message_call_to_action"
            r0 = r23
            r0.<init>(r3, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r3 = "montage_thread_key"
            r0 = r22
            r0.<init>(r3, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "room_privacy_mode"
            r3 = r21
            r3.<init>(r0, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_id"
            r3 = r20
            r3.<init>(r0, r2)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_name"
            r3 = r19
            r3.<init>(r0, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "room_associated_photo_uri"
            r3 = r18
            r3.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "approval_toggleable"
            r3 = r17
            r3.<init>(r0, r2)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "video_room_mode"
            r14.<init>(r0, r2)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "marketplace_data"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "room_creation_time"
            r12.<init>(r0, r2)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "group_thread_category"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "are_admins_supported"
            r10.<init>(r0, r6)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "group_thread_add_mode"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "group_thread_offline_threading_id"
            r8.<init>(r0, r2)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "had_messenger_call"
            r7.<init>(r0, r6)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "personal_group_invite_link"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "optimistic_group_state"
            r5.<init>(r0, r2)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "ad_context_data"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "use_existing_group"
            r3.<init>(r0, r2)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "thread_associated_object_type"
            r2.<init>(r0, r15)
            r53 = r21
            r54 = r20
            r55 = r19
            r56 = r18
            r57 = r17
            r58 = r14
            r59 = r13
            r60 = r12
            r61 = r11
            r62 = r10
            r63 = r9
            r64 = r8
            r65 = r7
            r66 = r6
            r67 = r5
            r68 = r4
            r69 = r3
            r70 = r2
            r46 = r28
            r47 = r27
            r48 = r26
            r49 = r25
            r50 = r24
            r51 = r23
            r52 = r22
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70}
            r3 = 0
            r2 = 27
            r0 = 25
            r6 = r73
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r45
            r3 = r43
            r4 = r42
            r5 = r41
            r6 = r40
            r7 = r39
            r8 = r38
            r9 = r37
            r10 = r36
            r11 = r35
            r12 = r34
            r13 = r33
            r14 = r73
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            X.0W6 r2 = new X.0W6
            r0 = r44
            r2.<init>(r0, r15)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r2)
            r3.<init>(r0)
            java.lang.String r2 = "threads"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            goto L_0x003b
        L_0x1cb6:
            r0 = 238(0xee, float:3.34E-43)
            if (r3 != r0) goto L_0x1ccd
            r0 = -1617696037(0xffffffff9f93eadb, float:-6.264546E-20)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN last_aloha_call_conference_id TEXT"
            r1.execSQL(r0)
            r0 = -1680501818(0xffffffff9bd593c6, float:-3.533339E-22)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1ccd:
            r0 = 239(0xef, float:3.35E-43)
            if (r3 != r0) goto L_0x1ce4
            r0 = -313023093(0xffffffffed57a58b, float:-4.171213E27)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_participants ADD COLUMN can_viewer_message INTEGER DEFAULT 1"
            r1.execSQL(r0)
            r0 = -589035954(0xffffffffdce4064e, float:-5.13465814E17)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1ce4:
            r0 = 240(0xf0, float:3.36E-43)
            if (r3 != r0) goto L_0x1cfb
            r0 = -600263739(0xffffffffdc38b3c5, float:-2.07956218E17)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN thread_streak_data TEXT"
            r1.execSQL(r0)
            r0 = 927024832(0x374146c0, float:1.1520169E-5)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1cfb:
            r0 = 241(0xf1, float:3.38E-43)
            if (r3 != r0) goto L_0x1d12
            r0 = 1044990730(0x3e494b0a, float:0.19657531)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN aloha_proxy_user_owners TEXT"
            r1.execSQL(r0)
            r0 = 1732597586(0x67455752, float:9.31917E23)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1d12:
            r0 = 242(0xf2, float:3.39E-43)
            if (r3 != r0) goto L_0x1d29
            r0 = -2002194551(0xffffffff88a8ef89, float:-1.0167447E-33)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN page_comm_item_data TEXT"
            r1.execSQL(r0)
            r0 = -1484897222(0xffffffffa77e443a, float:-3.5286567E-15)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1d29:
            r0 = 243(0xf3, float:3.4E-43)
            if (r3 != r0) goto L_0x1d43
            r0 = 478312834(0x1c827982, float:8.634087E-22)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE fb_events ADD COLUMN fb_event_url TEXT"
            r1.execSQL(r0)
            r0 = 1539190069(0x5bbe2d35, float:1.07059902E17)
            X.C007406x.A00(r0)
            A02(r1)
            goto L_0x003b
        L_0x1d43:
            r0 = 244(0xf4, float:3.42E-43)
            if (r3 != r0) goto L_0x1d5a
            r0 = 916316624(0x369de1d0, float:4.7052526E-6)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN instant_game_channel TEXT"
            r1.execSQL(r0)
            r0 = 1324519214(0x4ef28f2e, float:2.03473485E9)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1d5a:
            r0 = 245(0xf5, float:3.43E-43)
            if (r3 != r0) goto L_0x1d71
            r0 = -1883791358(0xffffffff8fb7a002, float:-1.8106826E-29)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_messenger_welcome_page_cta_enabled INTEGER"
            r1.execSQL(r0)
            r0 = -968253515(0xffffffffc6499fb5, float:-12903.927)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1d71:
            r0 = 246(0xf6, float:3.45E-43)
            if (r3 != r0) goto L_0x1d88
            r0 = 1023059564(0x3cfaa66c, float:0.030596934)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_message_ignored_by_viewer INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 306534023(0x12455687, float:6.2268893E-28)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1d88:
            r0 = 247(0xf7, float:3.46E-43)
            if (r3 != r0) goto L_0x1d9f
            r0 = -1186698143(0xffffffffb9446c61, float:-1.8732391E-4)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN aloha_proxy_users_owned TEXT"
            r1.execSQL(r0)
            r0 = 1165413454(0x4576cc4e, float:3948.769)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1d9f:
            r0 = 248(0xf8, float:3.48E-43)
            if (r3 != r0) goto L_0x1db6
            r0 = -1474033030(0xffffffffa8240a7a, float:-9.1061005E-15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_viewer_subscribed_to_message_updates INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 1389626387(0x52d40413, float:4.5530071E11)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1db6:
            r0 = 249(0xf9, float:3.49E-43)
            if (r3 != r0) goto L_0x1dcd
            r0 = 729712045(0x2b7e85ad, float:9.042444E-13)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_conversation_ice_breaker_enabled INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 292413932(0x116de1ec, float:1.8765621E-28)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1dcd:
            r0 = 250(0xfa, float:3.5E-43)
            if (r3 != r0) goto L_0x1de4
            r0 = -489442859(0xffffffffe2d3b1d5, float:-1.9525386E21)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN games_push_notification_settings TEXT"
            r1.execSQL(r0)
            r0 = -615086870(0xffffffffdb5684ea, float:-6.0381786E16)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x1de4:
            r0 = 251(0xfb, float:3.52E-43)
            if (r3 != r0) goto L_0x210e
            X.0W6 r45 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "thread_key"
            r0 = r45
            r0.<init>(r2, r15)
            X.0W6 r44 = new X.0W6
            java.lang.String r2 = "legacy_thread_id"
            r0 = r44
            r0.<init>(r2, r15)
            X.0W6 r43 = new X.0W6
            java.lang.String r5 = "INTEGER"
            java.lang.String r2 = "sequence_id"
            r0 = r43
            r0.<init>(r2, r5)
            X.0W6 r42 = new X.0W6
            java.lang.String r2 = "name"
            r0 = r42
            r0.<init>(r2, r15)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "senders"
            r0 = r41
            r0.<init>(r2, r15)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "snippet"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "snippet_sender"
            r0 = r39
            r0.<init>(r2, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "admin_snippet"
            r0 = r38
            r0.<init>(r2, r15)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r37
            r0.<init>(r2, r5)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "last_read_timestamp_ms"
            r0 = r36
            r0.<init>(r2, r5)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "approx_total_message_count"
            r0 = r35
            r0.<init>(r2, r5)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "unread_message_count"
            r0 = r34
            r0.<init>(r2, r5)
            r0 = 55
            X.0W6[] r14 = new X.AnonymousClass0W6[r0]
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "last_fetch_time_ms"
            r0 = r33
            r0.<init>(r2, r5)
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "pic_hash"
            r0 = r32
            r0.<init>(r2, r15)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "pic"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "can_reply_to"
            r0 = r30
            r0.<init>(r2, r5)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "cannot_reply_reason"
            r0 = r29
            r0.<init>(r2, r15)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "last_message_admin_text_type"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "mute_until"
            r0 = r27
            r0.<init>(r2, r5)
            X.0W6 r26 = new X.0W6
            java.lang.String r0 = "is_subscribed"
            r2 = r26
            r2.<init>(r0, r5)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "folder"
            r2 = r25
            r2.<init>(r0, r15)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "draft"
            r2 = r24
            r2.<init>(r0, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r7 = "INTEGER DEFAULT 0"
            java.lang.String r0 = "missed_call_status"
            r2 = r23
            r2.<init>(r0, r7)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "me_bubble_color"
            r2 = r22
            r2.<init>(r0, r5)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "other_bubble_color"
            r2 = r21
            r2.<init>(r0, r5)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "wallpaper_color"
            r2 = r20
            r2.<init>(r0, r5)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "initial_fetch_complete"
            r2 = r19
            r2.<init>(r0, r5)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "custom_like_emoji"
            r2 = r18
            r2.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "outgoing_message_lifetime"
            r2 = r17
            r2.<init>(r0, r5)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "custom_nicknames"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "invite_uri"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r2 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r11.<init>(r2, r0)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "game_data"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "is_joinable"
            r9.<init>(r0, r7)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "requires_approval"
            r8.<init>(r0, r7)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "rtc_call_info"
            r6.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "last_message_commerce_message_type"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "is_thread_queue_enabled"
            r3.<init>(r0, r5)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "group_description"
            r2.<init>(r0, r15)
            r53 = r26
            r54 = r25
            r55 = r24
            r56 = r23
            r57 = r22
            r58 = r21
            r59 = r20
            r60 = r19
            r61 = r18
            r62 = r17
            r63 = r13
            r64 = r12
            r65 = r11
            r66 = r10
            r67 = r9
            r68 = r8
            r69 = r6
            r70 = r4
            r71 = r3
            r72 = r2
            r46 = r33
            r47 = r32
            r48 = r31
            r49 = r30
            r50 = r29
            r51 = r28
            r52 = r27
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72}
            r3 = 0
            r0 = 27
            java.lang.System.arraycopy(r4, r3, r14, r3, r0)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "media_preview"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "booking_requests"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "last_call_ms"
            r0 = r29
            r0.<init>(r2, r5)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "is_discoverable"
            r0 = r28
            r0.<init>(r2, r5)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "last_sponsored_message_call_to_action"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "montage_thread_key"
            r0 = r26
            r0.<init>(r2, r15)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "room_privacy_mode"
            r0 = r25
            r0.<init>(r2, r5)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_id"
            r2 = r24
            r2.<init>(r0, r5)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_name"
            r2 = r23
            r2.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "room_associated_photo_uri"
            r2 = r22
            r2.<init>(r0, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "approval_toggleable"
            r2 = r21
            r2.<init>(r0, r5)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "video_room_mode"
            r2 = r20
            r2.<init>(r0, r5)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "marketplace_data"
            r2 = r19
            r2.<init>(r0, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "room_creation_time"
            r2 = r18
            r2.<init>(r0, r5)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "group_thread_category"
            r2 = r17
            r2.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "are_admins_supported"
            r13.<init>(r0, r7)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "group_thread_add_mode"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "group_thread_offline_threading_id"
            r11.<init>(r0, r5)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "had_messenger_call"
            r10.<init>(r0, r7)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "personal_group_invite_link"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "optimistic_group_state"
            r8.<init>(r0, r7)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "ad_context_data"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "use_existing_group"
            r6.<init>(r0, r5)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "thread_associated_object_type"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "last_aloha_call_conference_id"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "thread_streak_data"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "page_comm_item_data"
            r2.<init>(r0, r15)
            r53 = r24
            r54 = r23
            r55 = r22
            r56 = r21
            r57 = r20
            r58 = r19
            r59 = r18
            r60 = r17
            r61 = r13
            r62 = r12
            r63 = r11
            r64 = r10
            r65 = r9
            r66 = r8
            r67 = r7
            r68 = r6
            r69 = r5
            r70 = r4
            r71 = r3
            r72 = r2
            r46 = r31
            r47 = r30
            r48 = r29
            r49 = r28
            r50 = r27
            r51 = r26
            r52 = r25
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72}
            r3 = 0
            r2 = 27
            java.lang.System.arraycopy(r4, r3, r14, r2, r2)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "games_push_notification_settings"
            r2.<init>(r0, r15)
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r2}
            r2 = 54
            r0 = 1
            java.lang.System.arraycopy(r4, r3, r14, r2, r0)
            r2 = r45
            r3 = r44
            r4 = r43
            r5 = r42
            r6 = r41
            r7 = r40
            r8 = r39
            r9 = r38
            r10 = r37
            r11 = r36
            r12 = r35
            r13 = r34
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r45)
            r3.<init>(r0)
            java.lang.String r2 = "threads"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = 166430916(0x9eb88c4, float:5.6702806E-33)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS threads_legacy_thread_id_index ON threads(legacy_thread_id)"
            r1.execSQL(r0)
            r0 = -1006586488(0xffffffffc400b588, float:-514.8364)
            X.C007406x.A00(r0)
            r0 = -318838858(0xffffffffecfee7b6, float:-2.465291E27)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE UNIQUE INDEX IF NOT EXISTS threads_montage_thread_key_index ON threads(montage_thread_key)"
            r1.execSQL(r0)
            r0 = -1376254479(0xffffffffadf805f1, float:-2.8196974E-11)
            X.C007406x.A00(r0)
            r0 = 1063297237(0x3f60a0d5, float:0.8774541)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN last_message_id_if_sponsored TEXT"
            r1.execSQL(r0)
            r0 = -1868876943(0xffffffff909b3371, float:-6.121598E-29)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x210e:
            r0 = 252(0xfc, float:3.53E-43)
            if (r3 != r0) goto L_0x2139
            r0 = 855241954(0x32f9f4e2, float:2.9098775E-8)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN can_participants_claim_admin INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 1066693607(0x3f9473e7, float:1.159787)
            X.C007406x.A00(r0)
            r0 = -131008957(0xfffffffff830f643, float:-1.435686E34)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN group_approval_mode INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 1867350488(0x6f4d81d8, float:6.36014E28)
            X.C007406x.A00(r0)
            A02(r1)
            goto L_0x003b
        L_0x2139:
            r0 = 253(0xfd, float:3.55E-43)
            if (r3 != r0) goto L_0x2150
            r0 = -1580518741(0xffffffffa1cb32ab, float:-1.3769227E-18)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN unopened_montage_directs TEXT"
            r1.execSQL(r0)
            r0 = 340696361(0x144e9d29, float:1.04313366E-26)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2150:
            r0 = 254(0xfe, float:3.56E-43)
            if (r3 != r0) goto L_0x2178
            r0 = 807869952(0x30271e00, float:6.079688E-10)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_participants ADD COLUMN inviter_user_key TEXT"
            r1.execSQL(r0)
            r0 = 536323594(0x1ff7a60a, float:1.0488326E-19)
            X.C007406x.A00(r0)
            r0 = 1530991622(0x5b411406, float:5.4346687E16)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_participants ADD COLUMN request_source INTEGER"
            r1.execSQL(r0)
            r0 = -1626568048(0xffffffff9f0c8a90, float:-2.976077E-20)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2178:
            r0 = 255(0xff, float:3.57E-43)
            if (r3 != r0) goto L_0x226c
            X.0W6 r23 = new X.0W6
            java.lang.String r2 = "msg_id"
            java.lang.String r15 = "TEXT"
            r0 = r23
            r0.<init>(r2, r15)
            X.0W6 r22 = new X.0W6
            r0 = r22
            r0.<init>(r2, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r2 = "thread_key"
            r0 = r21
            r0.<init>(r2, r15)
            X.0W6 r20 = new X.0W6
            java.lang.String r2 = "sender"
            r0 = r20
            r0.<init>(r2, r15)
            X.0W6 r19 = new X.0W6
            java.lang.String r2 = "INTEGER"
            java.lang.String r3 = "timestamp_ms"
            r0 = r19
            r0.<init>(r3, r2)
            X.0W6 r18 = new X.0W6
            java.lang.String r3 = "timestamp_sent_ms"
            r0 = r18
            r0.<init>(r3, r2)
            X.0W6 r17 = new X.0W6
            java.lang.String r3 = "attachments"
            r0 = r17
            r0.<init>(r3, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "msg_type"
            r14.<init>(r0, r2)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "offline_threading_id"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "source"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "channel_source"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "send_channel"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r3 = "STRING"
            java.lang.String r0 = "pending_send_media_attachment"
            r9.<init>(r0, r3)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "sent_share_attachment"
            r8.<init>(r0, r3)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "client_tags"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "extensible_message_data"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "xma"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "message_lifetime"
            r4.<init>(r0, r2)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "montage_reply_message_id"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "montage_reply_action"
            r2.<init>(r0, r15)
            r24 = r8
            r25 = r7
            r26 = r6
            r27 = r5
            r28 = r4
            r29 = r3
            r30 = r2
            X.0W6[] r36 = new X.AnonymousClass0W6[]{r24, r25, r26, r27, r28, r29, r30}
            r24 = r22
            r25 = r21
            r26 = r20
            r27 = r19
            r28 = r18
            r29 = r17
            r30 = r14
            r31 = r13
            r32 = r12
            r33 = r11
            r34 = r10
            r35 = r9
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36)
            X.0al r2 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r23)
            r2.<init>(r0)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r2)
            java.lang.String r0 = "montage_directs"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r0, r3, r2)
            r0 = -1214199763(0xffffffffb7a0c82d, float:-1.91667E-5)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = -1522208797(0xffffffffa544efe3, float:-1.7081567E-16)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x226c:
            r0 = 256(0x100, float:3.59E-43)
            if (r3 != r0) goto L_0x2294
            r0 = 54274449(0x33c2991, float:5.529595E-37)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN synced_fb_group_id INTEGER"
            r1.execSQL(r0)
            r0 = 446515875(0x1a9d4aa3, float:6.505426E-23)
            X.C007406x.A00(r0)
            r0 = 1899449885(0x71374e1d, float:9.0768304E29)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN synced_fb_group_status TEXT"
            r1.execSQL(r0)
            r0 = -1868092084(0xffffffff90a72d4c, float:-6.5939676E-29)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2294:
            r0 = 257(0x101, float:3.6E-43)
            if (r3 == r0) goto L_0x003b
            r0 = 258(0x102, float:3.62E-43)
            if (r3 != r0) goto L_0x22c0
            r0 = 2055874899(0x7a8a2953, float:3.5868756E35)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_work_user INTEGER"
            r1.execSQL(r0)
            r0 = 540626161(0x20394cf1, float:1.5695567E-19)
            X.C007406x.A00(r0)
            r0 = -14029981(0xffffffffff29eb63, float:-2.2586173E38)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_viewer_coworker INTEGER"
            r1.execSQL(r0)
            r0 = -1431795363(0xffffffffaaa8895d, float:-2.993811E-13)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x22c0:
            r0 = 259(0x103, float:3.63E-43)
            if (r3 != r0) goto L_0x230a
            r0 = 2135736215(0x7f4cbf97, float:2.721573E38)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_reply_message_media_type TEXT"
            r1.execSQL(r0)
            r0 = 1111484720(0x423fe930, float:47.977722)
            X.C007406x.A00(r0)
            r0 = 1542193324(0x5bec00ac, float:1.32857666E17)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_reply_story_media_type TEXT"
            r1.execSQL(r0)
            r0 = -1773164982(0xffffffff964fa64a, float:-1.6773798E-25)
            X.C007406x.A00(r0)
            r0 = 1576842958(0x5dfcb6ce, float:2.27624608E18)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_reply_story_name TEXT"
            r1.execSQL(r0)
            r0 = -1375375158(0xffffffffae0570ca, float:-3.0340876E-11)
            X.C007406x.A00(r0)
            r0 = 746031810(0x2c778ac2, float:3.5177837E-12)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_reply_story_type TEXT"
            r1.execSQL(r0)
            r0 = -462694445(0xffffffffe46bd7d3, float:-1.7402147E22)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x230a:
            r0 = 260(0x104, float:3.64E-43)
            if (r3 != r0) goto L_0x2313
            A02(r1)
            goto L_0x003b
        L_0x2313:
            r0 = 261(0x105, float:3.66E-43)
            if (r3 != r0) goto L_0x2326
            java.lang.String r0 = "3"
            java.lang.String[] r3 = new java.lang.String[]{r0}
            java.lang.String r2 = "threads"
            java.lang.String r0 = "optimistic_group_state=?"
            r1.delete(r2, r0, r3)
            goto L_0x003b
        L_0x2326:
            r0 = 262(0x106, float:3.67E-43)
            if (r3 != r0) goto L_0x233d
            r0 = 1487009543(0x58a1f707, float:1.42465877E15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN favorite_color TEXT"
            r1.execSQL(r0)
            r0 = 2055833029(0x7a8985c5, float:3.570289E35)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x233d:
            r0 = 263(0x107, float:3.69E-43)
            if (r3 != r0) goto L_0x26e9
            java.lang.String r12 = "INTEGER DEFAULT 0"
            java.lang.String r10 = "INTEGER"
            X.0W6 r11 = new X.0W6
            java.lang.String r3 = "thread_key"
            java.lang.String r9 = "TEXT"
            r11.<init>(r3, r9)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "game_data"
            r8.<init>(r0, r9)
            X.0al r7 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r11)
            r7.<init>(r0)
            X.3lk r6 = new X.3lk
            com.google.common.collect.ImmutableList r5 = com.google.common.collect.ImmutableList.of(r11)
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r11)
            java.lang.String r45 = "threads"
            java.lang.String r2 = "ON DELETE CASCADE"
            r0 = r45
            r6.<init>(r5, r0, r4, r2)
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r11, r8)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r7, r6)
            java.lang.String r0 = "threads_metadata"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r0, r4, r2)
            r0 = 975111070(0x3a1f039e, float:6.0659077E-4)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 1815005758(0x6c2eca3e, float:8.452326E26)
            X.C007406x.A00(r0)
            r0 = -735436618(0xffffffffd42a20b6, float:-2.92277296E12)
            X.C007406x.A01(r1, r0)     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "INSERT INTO threads_metadata (thread_key, game_data) SELECT thread_key, game_data FROM threads"
            r0 = -1306398531(0xffffffffb221f0bd, float:-9.426171E-9)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x33bf }
            r1.execSQL(r2)     // Catch:{ all -> 0x33bf }
            r0 = -1816416543(0xffffffff93bbaee1, float:-4.737788E-27)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x33bf }
            X.0W6 r44 = new X.0W6     // Catch:{ all -> 0x33bf }
            r0 = r44
            r0.<init>(r3, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r43 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "legacy_thread_id"
            r0 = r43
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r42 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "sequence_id"
            r0 = r42
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r41 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "name"
            r0 = r41
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r40 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "senders"
            r0 = r40
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r39 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "snippet"
            r0 = r39
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r38 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "snippet_sender"
            r0 = r38
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r37 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "admin_snippet"
            r0 = r37
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r36 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "timestamp_ms"
            r0 = r36
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r35 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "last_read_timestamp_ms"
            r0 = r35
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r34 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "approx_total_message_count"
            r2 = r34
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r33 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "unread_message_count"
            r2 = r33
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            r0 = 60
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]     // Catch:{ all -> 0x33bf }
            r73 = r0
            X.0W6 r32 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "last_fetch_time_ms"
            r0 = r32
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r31 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "pic_hash"
            r0 = r31
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r30 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "pic"
            r0 = r30
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r29 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "can_reply_to"
            r0 = r29
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r28 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "cannot_reply_reason"
            r0 = r28
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r27 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "last_message_admin_text_type"
            r0 = r27
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r26 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "mute_until"
            r0 = r26
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r25 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "is_subscribed"
            r2 = r25
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r24 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "folder"
            r2 = r24
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r23 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "draft"
            r2 = r23
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r22 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "missed_call_status"
            r2 = r22
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r21 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "me_bubble_color"
            r2 = r21
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r20 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "other_bubble_color"
            r2 = r20
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r19 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "wallpaper_color"
            r2 = r19
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r18 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "initial_fetch_complete"
            r2 = r18
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r17 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "custom_like_emoji"
            r2 = r17
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r15 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "outgoing_message_lifetime"
            r15.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r14 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "custom_nicknames"
            r14.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r13 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "invite_uri"
            r13.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r11 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "last_message_id_if_sponsored"
            r11.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r8 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r8.<init>(r2, r0)     // Catch:{ all -> 0x33bf }
            X.0W6 r7 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "is_joinable"
            r7.<init>(r0, r12)     // Catch:{ all -> 0x33bf }
            X.0W6 r6 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "requires_approval"
            r6.<init>(r0, r12)     // Catch:{ all -> 0x33bf }
            X.0W6 r5 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "rtc_call_info"
            r5.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r4 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "last_message_commerce_message_type"
            r4.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r3 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "is_thread_queue_enabled"
            r3.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r2 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "group_description"
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            r53 = r25
            r54 = r24
            r55 = r23
            r56 = r22
            r57 = r21
            r58 = r20
            r59 = r19
            r60 = r18
            r61 = r17
            r62 = r15
            r63 = r14
            r64 = r13
            r65 = r11
            r66 = r8
            r67 = r7
            r68 = r6
            r69 = r5
            r70 = r4
            r71 = r3
            r72 = r2
            r46 = r32
            r47 = r31
            r48 = r30
            r49 = r29
            r50 = r28
            r51 = r27
            r52 = r26
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72}     // Catch:{ all -> 0x33bf }
            r3 = 0
            r0 = 27
            r6 = r73
            java.lang.System.arraycopy(r4, r3, r6, r3, r0)
            X.0W6 r32 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "media_preview"
            r0 = r32
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r31 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "booking_requests"
            r0 = r31
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r30 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "last_call_ms"
            r0 = r30
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r29 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "is_discoverable"
            r0 = r29
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r28 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "last_sponsored_message_call_to_action"
            r0 = r28
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r27 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "montage_thread_key"
            r0 = r27
            r0.<init>(r2, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r26 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "room_privacy_mode"
            r0 = r26
            r0.<init>(r2, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r25 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "room_associated_fb_group_id"
            r2 = r25
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r24 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "room_associated_fb_group_name"
            r2 = r24
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r23 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "room_associated_photo_uri"
            r2 = r23
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r22 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "approval_toggleable"
            r2 = r22
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r21 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "video_room_mode"
            r2 = r21
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r20 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "marketplace_data"
            r2 = r20
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r19 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "room_creation_time"
            r2 = r19
            r2.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r18 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "group_thread_category"
            r2 = r18
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r17 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "are_admins_supported"
            r2 = r17
            r2.<init>(r0, r12)     // Catch:{ all -> 0x33bf }
            X.0W6 r15 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "group_thread_add_mode"
            r15.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r14 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "group_thread_offline_threading_id"
            r14.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r13 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "had_messenger_call"
            r13.<init>(r0, r12)     // Catch:{ all -> 0x33bf }
            X.0W6 r11 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "personal_group_invite_link"
            r11.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r8 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "optimistic_group_state"
            r8.<init>(r0, r12)     // Catch:{ all -> 0x33bf }
            X.0W6 r7 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "ad_context_data"
            r7.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r6 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "use_existing_group"
            r6.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r5 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "thread_associated_object_type"
            r5.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r4 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "last_aloha_call_conference_id"
            r4.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r3 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "thread_streak_data"
            r3.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r2 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "page_comm_item_data"
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            r53 = r25
            r54 = r24
            r55 = r23
            r56 = r22
            r57 = r21
            r58 = r20
            r59 = r19
            r60 = r18
            r61 = r17
            r62 = r15
            r63 = r14
            r64 = r13
            r65 = r11
            r66 = r8
            r67 = r7
            r68 = r6
            r69 = r5
            r70 = r4
            r71 = r3
            r72 = r2
            r46 = r32
            r47 = r31
            r48 = r30
            r49 = r29
            r50 = r28
            r51 = r27
            r52 = r26
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72}     // Catch:{ all -> 0x33bf }
            r3 = 0
            r2 = 27
            r6 = r73
            java.lang.System.arraycopy(r4, r3, r6, r2, r2)
            X.0W6 r2 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "games_push_notification_settings"
            r2.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r3 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "can_participants_claim_admin"
            r3.<init>(r0, r12)     // Catch:{ all -> 0x33bf }
            X.0W6 r4 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "group_approval_mode"
            r4.<init>(r0, r12)     // Catch:{ all -> 0x33bf }
            X.0W6 r5 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "unopened_montage_directs"
            r5.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6 r6 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "synced_fb_group_id"
            r6.<init>(r0, r10)     // Catch:{ all -> 0x33bf }
            X.0W6 r7 = new X.0W6     // Catch:{ all -> 0x33bf }
            java.lang.String r0 = "synced_fb_group_status"
            r7.<init>(r0, r9)     // Catch:{ all -> 0x33bf }
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r2, r3, r4, r5, r6, r7}     // Catch:{ all -> 0x33bf }
            r3 = 0
            r2 = 54
            r0 = 6
            r6 = r73
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r44
            r3 = r43
            r4 = r42
            r5 = r41
            r6 = r40
            r7 = r39
            r8 = r38
            r9 = r37
            r10 = r36
            r11 = r35
            r12 = r34
            r13 = r33
            r14 = r73
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)     // Catch:{ all -> 0x33bf }
            X.0al r2 = new X.0al     // Catch:{ all -> 0x33bf }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r44)     // Catch:{ all -> 0x33bf }
            r2.<init>(r0)     // Catch:{ all -> 0x33bf }
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r2)     // Catch:{ all -> 0x33bf }
            r0 = r45
            X.AnonymousClass0W4.A08(r1, r0, r4, r2)     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "CREATE INDEX IF NOT EXISTS threads_legacy_thread_id_index ON threads(legacy_thread_id)"
            r0 = -160040058(0xfffffffff675fb86, float:-1.2472795E33)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x33bf }
            r1.execSQL(r2)     // Catch:{ all -> 0x33bf }
            r0 = -215712470(0xfffffffff3247d2a, float:-1.3032155E31)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x33bf }
            java.lang.String r2 = "CREATE UNIQUE INDEX IF NOT EXISTS threads_montage_thread_key_index ON threads(montage_thread_key)"
            r0 = 1544627172(0x5c1123e4, float:1.63413335E17)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x33bf }
            r1.execSQL(r2)     // Catch:{ all -> 0x33bf }
            r0 = -1331185829(0xffffffffb0a7b75b, float:-1.2202962E-9)
            X.C007406x.A00(r0)     // Catch:{ all -> 0x33bf }
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x33bf }
            r0 = -1426031573(0xffffffffab007c2b, float:-4.5647053E-13)
            X.C007406x.A02(r1, r0)
            goto L_0x003b
        L_0x26e9:
            r0 = 264(0x108, float:3.7E-43)
            if (r3 != r0) goto L_0x2708
            java.lang.String r3 = "ALTER TABLE %s ADD COLUMN %s TEXT"
            java.lang.String r2 = "messages"
            java.lang.String r0 = "admin_relationship_message_title"
            java.lang.String r2 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r3, r2, r0)
            r0 = -1539015924(0xffffffffa4447b0c, float:-4.260495E-17)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 922849099(0x37018f4b, float:7.722362E-6)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2708:
            r0 = 265(0x109, float:3.71E-43)
            if (r3 != r0) goto L_0x2a31
            X.0W6 r43 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "msg_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r2 = "thread_key"
            r0 = r42
            r0.<init>(r2, r15)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "text"
            r0 = r41
            r0.<init>(r2, r15)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "sender"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r14 = "INTEGER"
            java.lang.String r2 = "is_not_forwardable"
            r0 = r39
            r0.<init>(r2, r14)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r38
            r0.<init>(r2, r14)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "timestamp_sent_ms"
            r0 = r37
            r0.<init>(r2, r14)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "attachments"
            r0 = r36
            r0.<init>(r2, r15)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "shares"
            r0 = r35
            r0.<init>(r2, r15)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "sticker_id"
            r0 = r34
            r0.<init>(r2, r15)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "msg_type"
            r0 = r33
            r0.<init>(r2, r14)
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "affected_users"
            r0 = r32
            r0.<init>(r2, r15)
            r0 = 54
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r71 = r0
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "coordinates"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "offline_threading_id"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "source"
            r0 = r29
            r0.<init>(r2, r15)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "channel_source"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "send_channel"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "is_non_authoritative"
            r0 = r26
            r0.<init>(r2, r14)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "STRING"
            java.lang.String r3 = "pending_send_media_attachment"
            r0 = r25
            r0.<init>(r3, r2)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "sent_share_attachment"
            r3 = r24
            r3.<init>(r0, r2)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "client_tags"
            r3 = r23
            r3.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "send_error"
            r3 = r22
            r3.<init>(r0, r2)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "send_error_message"
            r3 = r21
            r3.<init>(r0, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "send_error_number"
            r3 = r20
            r3.<init>(r0, r14)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "send_error_timestamp_ms"
            r3 = r19
            r3.<init>(r0, r14)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "send_error_error_url"
            r3 = r18
            r3.<init>(r0, r2)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "publicity"
            r2 = r17
            r2.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "send_queue_type"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "payment_transaction"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "payment_request"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "has_unavailable_attachment"
            r10.<init>(r0, r14)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "app_attribution"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "content_app_attribution"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "xma"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "admin_text_type"
            r6.<init>(r0, r14)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "admin_text_theme_color"
            r5.<init>(r0, r14)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "admin_text_thread_icon_emoji"
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "admin_text_nickname"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "admin_text_target_id"
            r2.<init>(r0, r15)
            r51 = r24
            r52 = r23
            r53 = r22
            r54 = r21
            r55 = r20
            r56 = r19
            r57 = r18
            r58 = r17
            r59 = r13
            r60 = r12
            r61 = r11
            r62 = r10
            r63 = r9
            r64 = r8
            r65 = r7
            r66 = r6
            r67 = r5
            r68 = r4
            r69 = r3
            r70 = r2
            r44 = r31
            r45 = r30
            r46 = r29
            r47 = r28
            r48 = r27
            r49 = r26
            r50 = r25
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70}
            r3 = 0
            r0 = 27
            r6 = r71
            java.lang.System.arraycopy(r4, r3, r6, r3, r0)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "admin_text_thread_message_lifetime"
            r0 = r30
            r0.<init>(r2, r14)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "message_lifetime"
            r0 = r29
            r0.<init>(r2, r14)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_event"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_server_info_data"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "admin_text_thread_rtc_is_video_call"
            r0 = r26
            r0.<init>(r2, r14)
            X.0W6 r25 = new X.0W6
            java.lang.String r2 = "is_sponsored"
            r0 = r25
            r0.<init>(r2, r14)
            X.0W6 r24 = new X.0W6
            java.lang.String r2 = "admin_text_thread_ad_properties"
            r0 = r24
            r0.<init>(r2, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "admin_text_game_score_data"
            r2 = r23
            r2.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "admin_text_thread_event_reminder_properties"
            r2 = r22
            r2.<init>(r0, r15)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "commerce_message_type"
            r2 = r21
            r2.<init>(r0, r15)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "admin_text_joinable_event_type"
            r2 = r20
            r2.<init>(r0, r15)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "metadata_at_text_ranges"
            r2 = r19
            r2.<init>(r0, r15)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "platform_metadata"
            r2 = r18
            r2.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "admin_text_is_joinable_promo"
            r2 = r17
            r2.<init>(r0, r14)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "admin_text_agent_intent_id"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "montage_reply_message_id"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "generic_admin_message_extensible_data"
            r12.<init>(r0, r15)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "profile_ranges"
            r11.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "send_error_detail"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "send_error_original_exception"
            r9.<init>(r0, r15)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "montage_reply_action"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            r0 = 23
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            r0 = 24
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            r0 = 25
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            r0 = 26
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r4.<init>(r0, r15)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "extensible_message_data"
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "admin_relationship_message_title"
            r2.<init>(r0, r15)
            r51 = r23
            r52 = r22
            r53 = r21
            r54 = r20
            r55 = r19
            r56 = r18
            r57 = r17
            r58 = r14
            r59 = r13
            r60 = r12
            r61 = r11
            r62 = r10
            r63 = r9
            r64 = r8
            r65 = r7
            r66 = r6
            r67 = r5
            r68 = r4
            r69 = r3
            r70 = r2
            r44 = r30
            r45 = r29
            r46 = r28
            r47 = r27
            r48 = r26
            r49 = r25
            r50 = r24
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70}
            r3 = 0
            r2 = 27
            r6 = r71
            java.lang.System.arraycopy(r4, r3, r6, r2, r2)
            r2 = r43
            r3 = r42
            r4 = r41
            r5 = r40
            r6 = r39
            r7 = r38
            r8 = r37
            r9 = r36
            r10 = r35
            r11 = r34
            r12 = r33
            r13 = r32
            r14 = r71
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r43)
            r3.<init>(r0)
            java.lang.String r2 = "messages"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = 1377516818(0x521b3d12, float:1.6668613E11)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_timestamp_index ON messages(thread_key, timestamp_ms DESC)"
            r1.execSQL(r0)
            r0 = -985682094(0xffffffffc53faf52, float:-3066.9575)
            X.C007406x.A00(r0)
            r0 = -1878166964(0xffffffff900d724c, float:-2.7895398E-29)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_offline_threading_id_index ON messages(offline_threading_id)"
            r1.execSQL(r0)
            r0 = -1075717079(0xffffffffbfe1dc29, float:-1.7645313)
            X.C007406x.A00(r0)
            r0 = -973329129(0xffffffffc5fc2d17, float:-8069.636)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS messages_type_index ON messages(thread_key, msg_type, timestamp_ms)"
            r1.execSQL(r0)
            r0 = 112188621(0x6afdccd, float:6.615212E-35)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2a31:
            r0 = 266(0x10a, float:3.73E-43)
            if (r3 != r0) goto L_0x2a48
            r0 = -1445848684(0xffffffffa9d21994, float:-9.3303105E-14)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN has_work_multi_company_associated_group INTEGER"
            r1.execSQL(r0)
            r0 = 864227743(0x3383119f, float:6.103368E-8)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2a48:
            r0 = 267(0x10b, float:3.74E-43)
            if (r3 != r0) goto L_0x2a5f
            r0 = -719625188(0xffffffffd51b641c, float:-1.06783918E13)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_story_type TEXT"
            r1.execSQL(r0)
            r0 = 364893009(0x15bfd351, float:7.7477684E-26)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2a5f:
            r0 = 268(0x10c, float:3.76E-43)
            if (r3 == r0) goto L_0x003b
            r0 = 269(0x10d, float:3.77E-43)
            if (r3 != r0) goto L_0x2a7d
            r0 = 1529168984(0x5b254458, float:4.6518516E16)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN synced_fb_group_is_work_multi_company_group INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 1095030284(0x4144d60c, float:12.302258)
            X.C007406x.A00(r0)
            A02(r1)
            goto L_0x003b
        L_0x2a7d:
            r0 = 270(0x10e, float:3.78E-43)
            if (r3 != r0) goto L_0x2a94
            r0 = -1144472378(0xffffffffbbc8bcc6, float:-0.006126019)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads_metadata ADD COLUMN mentorship_data TEXT"
            r1.execSQL(r0)
            r0 = 1932497894(0x732f93e6, float:1.3910701E31)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2a94:
            r0 = 271(0x10f, float:3.8E-43)
            if (r3 != r0) goto L_0x2b79
            X.0W6 r15 = new X.0W6
            java.lang.String r14 = "TEXT"
            java.lang.String r0 = "poll_id"
            r15.<init>(r0, r14)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "msg_id"
            r13.<init>(r0, r14)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "style"
            r12.<init>(r0, r14)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "question_text"
            r11.<init>(r0, r14)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "url"
            r10.<init>(r0, r14)
            X.0W6 r9 = new X.0W6
            java.lang.String r8 = "INTEGER"
            java.lang.String r0 = "can_viewer_vote"
            r9.<init>(r0, r8)
            X.0W6 r7 = new X.0W6
            java.lang.String r6 = "REAL"
            java.lang.String r0 = "bound_x"
            r7.<init>(r0, r6)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "bound_y"
            r5.<init>(r0, r6)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "bound_width"
            r4.<init>(r0, r6)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "bound_height"
            r3.<init>(r0, r6)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "bound_rotation"
            r2.<init>(r0, r6)
            r23 = r7
            r24 = r5
            r25 = r4
            r26 = r3
            r27 = r2
            r17 = r13
            r18 = r15
            r19 = r12
            r20 = r11
            r21 = r10
            r22 = r9
            com.google.common.collect.ImmutableList r7 = com.google.common.collect.ImmutableList.of(r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            X.0al r5 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r15)
            r5.<init>(r0)
            X.3lk r4 = new X.3lk
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r13)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r13)
            java.lang.String r6 = "ON DELETE CASCADE"
            java.lang.String r0 = "messages"
            r4.<init>(r3, r0, r2, r6)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r5, r4)
            java.lang.String r5 = "montage_message_poll"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r5, r7, r0)
            r0 = 3014262(0x2dfe76, float:4.223881E-39)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 1020842082(0x3cd8d062, float:0.026466552)
            X.C007406x.A00(r0)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "option_index"
            r4.<init>(r0, r8)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "option_text"
            r3.<init>(r0, r14)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "vote_count"
            r2.<init>(r0, r8)
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r15, r4, r3, r2)
            X.3lk r3 = new X.3lk
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r15)
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r15)
            r3.<init>(r2, r5, r0, r6)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r3)
            java.lang.String r0 = "montage_message_poll_options"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r0, r4, r2)
            r0 = -1550531418(0xffffffffa394c4a6, float:-1.6129476E-17)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = -2090154965(0xffffffff836ac42b, float:-6.899161E-37)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2b79:
            r0 = 272(0x110, float:3.81E-43)
            if (r3 != r0) goto L_0x2ba1
            r0 = -778434037(0xffffffffd19a0a0b, float:-8.2699182E10)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads_metadata ADD COLUMN room_associated_group_can_viewer_create_chats INTEGER"
            r1.execSQL(r0)
            r0 = -473936302(0xffffffffe3c04e52, float:-7.094837E21)
            X.C007406x.A00(r0)
            r0 = -601137504(0xffffffffdc2b5ea0, float:-1.92945049E17)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads_metadata ADD COLUMN room_associated_group_rooms_count INTEGER"
            r1.execSQL(r0)
            r0 = 186251780(0xb19fa04, float:2.965482E-32)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2ba1:
            r0 = 273(0x111, float:3.83E-43)
            if (r3 != r0) goto L_0x2bb8
            r0 = 648655528(0x26a9b2a8, float:1.1775156E-15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE montage_message_poll ADD COLUMN viewer_vote_index INTEGER"
            r1.execSQL(r0)
            r0 = -1372504043(0xffffffffae314015, float:-4.0302057E-11)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2bb8:
            r0 = 274(0x112, float:3.84E-43)
            if (r3 != r0) goto L_0x2bcf
            r0 = -181392351(0xfffffffff5302c21, float:-2.2332502E32)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_viewer_managing_parent INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = -1370838784(0xffffffffae4aa900, float:-4.6079585E-11)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2bcf:
            r0 = 275(0x113, float:3.85E-43)
            if (r3 != r0) goto L_0x2be6
            r0 = -1910609922(0xffffffff8e1e67fe, float:-1.9525074E-30)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN is_page_callable INTEGER"
            r1.execSQL(r0)
            r0 = 1082640851(0x4087c9d3, float:4.2433867)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2be6:
            r0 = 276(0x114, float:3.87E-43)
            if (r3 != r0) goto L_0x2bfd
            r0 = -2069817830(0xffffffff84a1161a, float:-3.7871215E-36)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_branded_camera_share_attribution TEXT"
            r1.execSQL(r0)
            r0 = 1136165275(0x43b8819b, float:369.01254)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2bfd:
            r0 = 277(0x115, float:3.88E-43)
            if (r3 != r0) goto L_0x2c14
            r0 = 1842427651(0x6dd13703, float:8.093609E27)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN message_replied_to_data TEXT"
            r1.execSQL(r0)
            r0 = -1247055855(0xffffffffb5ab7011, float:-1.2773108E-6)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2c14:
            r0 = 278(0x116, float:3.9E-43)
            if (r3 != r0) goto L_0x2c2b
            r0 = -2070524455(0xffffffff84964dd9, float:-3.5336322E-36)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN copy_message_id TEXT"
            r1.execSQL(r0)
            r0 = -1157611604(0xffffffffbb003fac, float:-0.0019569201)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2c2b:
            r0 = 279(0x117, float:3.91E-43)
            if (r3 != r0) goto L_0x2c76
            X.0W6 r7 = new X.0W6
            java.lang.String r6 = "TEXT"
            java.lang.String r0 = "msg_id"
            r7.<init>(r0, r6)
            X.3lk r5 = new X.3lk
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r7)
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r7)
            java.lang.String r2 = "messages"
            java.lang.String r0 = "ON DELETE CASCADE"
            r5.<init>(r4, r2, r3, r0)
            X.0W6 r3 = new X.0W6
            java.lang.String r0 = "overlay_type"
            r3.<init>(r0, r6)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "overlay_data"
            r2.<init>(r0, r6)
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r7, r3, r2)
            com.google.common.collect.ImmutableList r2 = com.google.common.collect.ImmutableList.of(r5)
            java.lang.String r0 = "montage_message_interactive_overlays"
            java.lang.String r2 = X.AnonymousClass0W4.A03(r0, r3, r2)
            r0 = -1452194130(0xffffffffa97146ae, float:-5.3574055E-14)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 313848844(0x12b4f40c, float:1.1419759E-27)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2c76:
            r0 = 280(0x118, float:3.92E-43)
            if (r3 != r0) goto L_0x2c8d
            r0 = 1640705317(0x61cb2d25, float:4.6849276E20)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN video_chat_link TEXT"
            r1.execSQL(r0)
            r0 = 1606216023(0x5fbce957, float:2.7225014E19)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2c8d:
            r0 = 281(0x119, float:3.94E-43)
            if (r3 != r0) goto L_0x2ca4
            r0 = -159074008(0xfffffffff684b928, float:-1.3459738E33)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_attribution TEXT"
            r1.execSQL(r0)
            r0 = 1376420700(0x520a835c, float:1.48727333E11)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2ca4:
            r0 = 282(0x11a, float:3.95E-43)
            if (r3 != r0) goto L_0x2cff
            r0 = -1033813586(0xffffffffc26141ae, float:-56.31414)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN theme_id LONG DEFAULT -1"
            r1.execSQL(r0)
            r0 = 389474640(0x1736e950, float:5.9101853E-25)
            X.C007406x.A00(r0)
            r0 = -763974466(0xffffffffd276acbe, float:-2.64865022E11)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN theme_fallback_color INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 2057377313(0x7aa11621, float:4.182043E35)
            X.C007406x.A00(r0)
            r0 = -732120720(0xffffffffd45cb970, float:-3.79201572E12)
            X.C007406x.A00(r0)
            java.lang.String r0 = "UPDATE threads SET theme_fallback_color = me_bubble_color"
            r1.execSQL(r0)
            r0 = 1044834942(0x3e46ea7e, float:0.19425389)
            X.C007406x.A00(r0)
            r0 = 913278816(0x366f8760, float:3.5692574E-6)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN theme_gradient_colors TEXT"
            r1.execSQL(r0)
            r0 = 1280923949(0x4c59592d, float:5.6976564E7)
            X.C007406x.A00(r0)
            r0 = -1705613834(0xffffffff9a5665f6, float:-4.4336526E-23)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN theme_accessibility_label TEXT"
            r1.execSQL(r0)
            r0 = -1933162320(0xffffffff8cc648b0, float:-3.0550478E-31)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2cff:
            r0 = 283(0x11b, float:3.97E-43)
            if (r3 != r0) goto L_0x2d16
            r0 = 347957017(0x14bd6719, float:1.9124788E-26)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN is_thread_pinned INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = -1752729643(0xffffffff978777d5, float:-8.75442E-25)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2d16:
            r0 = 284(0x11c, float:3.98E-43)
            if (r3 != r0) goto L_0x2d3e
            r0 = -908245118(0xffffffffc9dd4782, float:-1812720.2)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN inbox_profile_pic_uri TEXT"
            r1.execSQL(r0)
            r0 = -610580789(0xffffffffdb9b46cb, float:-8.7412918E16)
            X.C007406x.A00(r0)
            r0 = 666445895(0x27b92847, float:5.1391484E-15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN inbox_profile_pic_file_path TEXT"
            r1.execSQL(r0)
            r0 = 1470971220(0x57ad3d54, float:3.80957828E14)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2d3e:
            r0 = 285(0x11d, float:4.0E-43)
            if (r3 != r0) goto L_0x2d55
            r0 = -1781483601(0xffffffff95d0b7af, float:-8.430033E-26)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_users ADD COLUMN work_info TEXT"
            r1.execSQL(r0)
            r0 = -303808917(0xffffffffede43e6b, float:-8.829755E27)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2d55:
            r0 = 286(0x11e, float:4.01E-43)
            if (r3 != r0) goto L_0x2d6c
            r0 = 400753905(0x17e304f1, float:1.4670779E-24)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN admin_text_gradient_colors TEXT"
            r1.execSQL(r0)
            r0 = -931684658(0xffffffffc8779ece, float:-253563.22)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2d6c:
            r0 = 287(0x11f, float:4.02E-43)
            if (r3 != r0) goto L_0x2d83
            r0 = 941988566(0x38259ad6, float:3.9483268E-5)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE event_reminders ADD COLUMN event_reminder_end_time INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 1853518205(0x6e7a717d, float:1.9377113E28)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2d83:
            r0 = 288(0x120, float:4.04E-43)
            if (r3 != r0) goto L_0x2d9a
            r0 = 1594494923(0x5f0a0fcb, float:9.948393E18)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN thread_pin_timestamp INTEGER DEFAULT 0"
            r1.execSQL(r0)
            r0 = 902615447(0x35ccd197, float:1.5260183E-6)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2d9a:
            r0 = 289(0x121, float:4.05E-43)
            if (r3 != r0) goto L_0x2db1
            r0 = -2102964861(0xffffffff82a74d83, float:-2.4582934E-37)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN montage_metadata TEXT"
            r1.execSQL(r0)
            r0 = -1617970863(0xffffffff9f8fb951, float:-6.0869436E-20)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2db1:
            r0 = 290(0x122, float:4.06E-43)
            if (r3 != r0) goto L_0x2dc8
            r0 = 555054978(0x21157782, float:5.064133E-19)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN bonfire_user_id TEXT"
            r1.execSQL(r0)
            r0 = 2081277651(0x7c0dc6d3, float:2.944586E36)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2dc8:
            r0 = 291(0x123, float:4.08E-43)
            if (r3 != r0) goto L_0x2ddf
            r0 = 218063945(0xcff6449, float:3.9349328E-31)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN room_associated_group_type TEXT"
            r1.execSQL(r0)
            r0 = -818794644(0xffffffffcf322f6c, float:-2.98945229E9)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2ddf:
            r0 = 292(0x124, float:4.09E-43)
            if (r3 != r0) goto L_0x2df9
            r0 = 296392001(0x11aa9541, float:2.6913256E-28)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE thread_participants ADD COLUMN admin_type INTEGER DEFAULT -1"
            r1.execSQL(r0)
            r0 = -450322472(0xffffffffe5289fd8, float:-4.9769135E22)
            X.C007406x.A00(r0)
            A02(r1)
            goto L_0x003b
        L_0x2df9:
            r0 = 293(0x125, float:4.1E-43)
            if (r3 != r0) goto L_0x2e10
            r0 = 442584710(0x1a614e86, float:4.659234E-23)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN work_bot_should_show_get_started_cta INTEGER"
            r1.execSQL(r0)
            r0 = 1852433798(0x6e69e586, float:1.8096871E28)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2e10:
            r0 = 294(0x126, float:4.12E-43)
            if (r3 != r0) goto L_0x2e27
            r0 = -1279355676(0xffffffffb3be94e4, float:-8.8746475E-8)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE messages ADD COLUMN translations TEXT"
            r1.execSQL(r0)
            r0 = 610198048(0x245ee220, float:4.8330112E-17)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2e27:
            r0 = 295(0x127, float:4.13E-43)
            if (r3 != r0) goto L_0x2e3e
            r0 = 991330379(0x3b16804b, float:0.0022964652)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE threads ADD COLUMN animated_thread_activity_banner TEXT"
            r1.execSQL(r0)
            r0 = -402055253(0xffffffffe8091fab, float:-2.5901935E24)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x2e3e:
            r0 = 296(0x128, float:4.15E-43)
            if (r3 != r0) goto L_0x3203
            X.0W6 r44 = new X.0W6
            java.lang.String r15 = "TEXT"
            java.lang.String r2 = "thread_key"
            r0 = r44
            r0.<init>(r2, r15)
            X.0W6 r43 = new X.0W6
            java.lang.String r2 = "legacy_thread_id"
            r0 = r43
            r0.<init>(r2, r15)
            X.0W6 r42 = new X.0W6
            java.lang.String r11 = "INTEGER"
            java.lang.String r2 = "sequence_id"
            r0 = r42
            r0.<init>(r2, r11)
            X.0W6 r41 = new X.0W6
            java.lang.String r2 = "name"
            r0 = r41
            r0.<init>(r2, r15)
            X.0W6 r40 = new X.0W6
            java.lang.String r2 = "senders"
            r0 = r40
            r0.<init>(r2, r15)
            X.0W6 r39 = new X.0W6
            java.lang.String r2 = "snippet"
            r0 = r39
            r0.<init>(r2, r15)
            X.0W6 r38 = new X.0W6
            java.lang.String r2 = "snippet_sender"
            r0 = r38
            r0.<init>(r2, r15)
            X.0W6 r37 = new X.0W6
            java.lang.String r2 = "admin_snippet"
            r0 = r37
            r0.<init>(r2, r15)
            X.0W6 r36 = new X.0W6
            java.lang.String r2 = "timestamp_ms"
            r0 = r36
            r0.<init>(r2, r11)
            X.0W6 r35 = new X.0W6
            java.lang.String r2 = "last_read_timestamp_ms"
            r0 = r35
            r0.<init>(r2, r11)
            X.0W6 r34 = new X.0W6
            java.lang.String r2 = "approx_total_message_count"
            r0 = r34
            r0.<init>(r2, r11)
            X.0W6 r33 = new X.0W6
            java.lang.String r2 = "unread_message_count"
            r0 = r33
            r0.<init>(r2, r11)
            r0 = 71
            X.0W6[] r0 = new X.AnonymousClass0W6[r0]
            r72 = r0
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "last_fetch_time_ms"
            r0 = r32
            r0.<init>(r2, r11)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "pic_hash"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "pic"
            r0 = r30
            r0.<init>(r2, r15)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "can_reply_to"
            r0 = r29
            r0.<init>(r2, r11)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "cannot_reply_reason"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "last_message_admin_text_type"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "mute_until"
            r0 = r26
            r0.<init>(r2, r11)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "is_subscribed"
            r2 = r25
            r2.<init>(r0, r11)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "folder"
            r2 = r24
            r2.<init>(r0, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "draft"
            r2 = r23
            r2.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "missed_call_status"
            r2 = r22
            r2.<init>(r0, r11)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "me_bubble_color"
            r2 = r21
            r2.<init>(r0, r11)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "other_bubble_color"
            r2 = r20
            r2.<init>(r0, r11)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "wallpaper_color"
            r2 = r19
            r2.<init>(r0, r11)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "initial_fetch_complete"
            r2 = r18
            r2.<init>(r0, r11)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "custom_like_emoji"
            r2 = r17
            r2.<init>(r0, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "outgoing_message_lifetime"
            r14.<init>(r0, r11)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "custom_nicknames"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "invite_uri"
            r12.<init>(r0, r15)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "last_message_id_if_sponsored"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r2 = "group_chat_rank"
            java.lang.String r0 = "FLOAT"
            r9.<init>(r2, r0)
            X.0W6 r8 = new X.0W6
            java.lang.String r3 = "INTEGER DEFAULT 0"
            java.lang.String r0 = "is_joinable"
            r8.<init>(r0, r3)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "requires_approval"
            r7.<init>(r0, r3)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "rtc_call_info"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "last_message_commerce_message_type"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "is_thread_queue_enabled"
            r4.<init>(r0, r11)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "group_description"
            r2.<init>(r0, r15)
            r52 = r25
            r53 = r24
            r54 = r23
            r55 = r22
            r56 = r21
            r57 = r20
            r58 = r19
            r59 = r18
            r60 = r17
            r61 = r14
            r62 = r13
            r63 = r12
            r64 = r10
            r65 = r9
            r66 = r8
            r67 = r7
            r68 = r6
            r69 = r5
            r70 = r4
            r71 = r2
            r45 = r32
            r46 = r31
            r47 = r30
            r48 = r29
            r49 = r28
            r50 = r27
            r51 = r26
            X.0W6[] r5 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71}
            r4 = 0
            r0 = 27
            r7 = r72
            java.lang.System.arraycopy(r5, r4, r7, r4, r0)
            X.0W6 r32 = new X.0W6
            java.lang.String r2 = "media_preview"
            r0 = r32
            r0.<init>(r2, r15)
            X.0W6 r31 = new X.0W6
            java.lang.String r2 = "booking_requests"
            r0 = r31
            r0.<init>(r2, r15)
            X.0W6 r30 = new X.0W6
            java.lang.String r2 = "last_call_ms"
            r0 = r30
            r0.<init>(r2, r11)
            X.0W6 r29 = new X.0W6
            java.lang.String r2 = "is_discoverable"
            r0 = r29
            r0.<init>(r2, r11)
            X.0W6 r28 = new X.0W6
            java.lang.String r2 = "last_sponsored_message_call_to_action"
            r0 = r28
            r0.<init>(r2, r15)
            X.0W6 r27 = new X.0W6
            java.lang.String r2 = "montage_thread_key"
            r0 = r27
            r0.<init>(r2, r15)
            X.0W6 r26 = new X.0W6
            java.lang.String r2 = "room_privacy_mode"
            r0 = r26
            r0.<init>(r2, r11)
            X.0W6 r25 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_id"
            r4 = r25
            r4.<init>(r0, r11)
            X.0W6 r24 = new X.0W6
            java.lang.String r0 = "room_associated_fb_group_name"
            r4 = r24
            r4.<init>(r0, r15)
            X.0W6 r23 = new X.0W6
            java.lang.String r0 = "room_associated_photo_uri"
            r4 = r23
            r4.<init>(r0, r15)
            X.0W6 r22 = new X.0W6
            java.lang.String r0 = "approval_toggleable"
            r4 = r22
            r4.<init>(r0, r11)
            X.0W6 r21 = new X.0W6
            java.lang.String r0 = "video_room_mode"
            r4 = r21
            r4.<init>(r0, r11)
            X.0W6 r20 = new X.0W6
            java.lang.String r0 = "marketplace_data"
            r4 = r20
            r4.<init>(r0, r15)
            X.0W6 r19 = new X.0W6
            java.lang.String r0 = "room_creation_time"
            r4 = r19
            r4.<init>(r0, r11)
            X.0W6 r18 = new X.0W6
            java.lang.String r0 = "group_thread_category"
            r4 = r18
            r4.<init>(r0, r15)
            X.0W6 r17 = new X.0W6
            java.lang.String r0 = "are_admins_supported"
            r4 = r17
            r4.<init>(r0, r3)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "group_thread_add_mode"
            r14.<init>(r0, r15)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "group_thread_offline_threading_id"
            r13.<init>(r0, r11)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "had_messenger_call"
            r12.<init>(r0, r3)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "personal_group_invite_link"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r0 = "optimistic_group_state"
            r9.<init>(r0, r3)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "ad_context_data"
            r8.<init>(r0, r15)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "use_existing_group"
            r7.<init>(r0, r11)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "thread_associated_object_type"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "last_aloha_call_conference_id"
            r5.<init>(r0, r15)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "thread_streak_data"
            r4.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "page_comm_item_data"
            r2.<init>(r0, r15)
            r52 = r25
            r53 = r24
            r54 = r23
            r55 = r22
            r56 = r21
            r57 = r20
            r58 = r19
            r59 = r18
            r60 = r17
            r61 = r14
            r62 = r13
            r63 = r12
            r64 = r10
            r65 = r9
            r66 = r8
            r67 = r7
            r68 = r6
            r69 = r5
            r70 = r4
            r71 = r2
            r45 = r32
            r46 = r31
            r47 = r30
            r48 = r29
            r49 = r28
            r50 = r27
            r51 = r26
            X.0W6[] r5 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71}
            r4 = 0
            r2 = 27
            r7 = r72
            java.lang.System.arraycopy(r5, r4, r7, r2, r2)
            X.0W6 r20 = new X.0W6
            java.lang.String r2 = "games_push_notification_settings"
            r0 = r20
            r0.<init>(r2, r15)
            X.0W6 r19 = new X.0W6
            java.lang.String r2 = "can_participants_claim_admin"
            r0 = r19
            r0.<init>(r2, r3)
            X.0W6 r18 = new X.0W6
            java.lang.String r2 = "group_approval_mode"
            r0 = r18
            r0.<init>(r2, r3)
            X.0W6 r17 = new X.0W6
            java.lang.String r2 = "unopened_montage_directs"
            r0 = r17
            r0.<init>(r2, r15)
            X.0W6 r14 = new X.0W6
            java.lang.String r0 = "synced_fb_group_id"
            r14.<init>(r0, r11)
            X.0W6 r13 = new X.0W6
            java.lang.String r0 = "synced_fb_group_status"
            r13.<init>(r0, r15)
            X.0W6 r12 = new X.0W6
            java.lang.String r0 = "has_work_multi_company_associated_group"
            r12.<init>(r0, r11)
            X.0W6 r11 = new X.0W6
            java.lang.String r0 = "synced_fb_group_is_work_multi_company_group"
            r11.<init>(r0, r3)
            X.0W6 r10 = new X.0W6
            java.lang.String r0 = "video_chat_link"
            r10.<init>(r0, r15)
            X.0W6 r9 = new X.0W6
            java.lang.String r2 = "theme_id"
            java.lang.String r0 = "LONG DEFAULT -1"
            r9.<init>(r2, r0)
            X.0W6 r8 = new X.0W6
            java.lang.String r0 = "theme_fallback_color"
            r8.<init>(r0, r3)
            X.0W6 r7 = new X.0W6
            java.lang.String r0 = "theme_gradient_colors"
            r7.<init>(r0, r15)
            X.0W6 r6 = new X.0W6
            java.lang.String r0 = "theme_accessibility_label"
            r6.<init>(r0, r15)
            X.0W6 r5 = new X.0W6
            java.lang.String r0 = "is_thread_pinned"
            r5.<init>(r0, r3)
            X.0W6 r4 = new X.0W6
            java.lang.String r0 = "thread_pin_timestamp"
            r4.<init>(r0, r3)
            X.0W6 r3 = new X.0W6
            r0 = 128(0x80, float:1.794E-43)
            java.lang.String r0 = X.ECX.$const$string(r0)
            r3.<init>(r0, r15)
            X.0W6 r2 = new X.0W6
            java.lang.String r0 = "animated_thread_activity_banner"
            r2.<init>(r0, r15)
            r52 = r11
            r53 = r10
            r54 = r9
            r55 = r8
            r56 = r7
            r57 = r6
            r58 = r5
            r59 = r4
            r60 = r3
            r61 = r2
            r45 = r20
            r46 = r19
            r47 = r18
            r48 = r17
            r49 = r14
            r50 = r13
            r51 = r12
            X.0W6[] r4 = new X.AnonymousClass0W6[]{r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61}
            r3 = 0
            r2 = 54
            r0 = 17
            r6 = r72
            java.lang.System.arraycopy(r4, r3, r6, r2, r0)
            r2 = r44
            r3 = r43
            r4 = r42
            r5 = r41
            r6 = r40
            r7 = r39
            r8 = r38
            r9 = r37
            r10 = r36
            r11 = r35
            r12 = r34
            r13 = r33
            r14 = r72
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.0al r3 = new X.0al
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r44)
            r3.<init>(r0)
            java.lang.String r2 = "threads"
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r3)
            X.AnonymousClass0W4.A08(r1, r2, r4, r0)
            r0 = 733075704(0x2bb1d8f8, float:1.2636827E-12)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE INDEX IF NOT EXISTS threads_legacy_thread_id_index ON threads(legacy_thread_id)"
            r1.execSQL(r0)
            r0 = -937768233(0xffffffffc81acad7, float:-158507.36)
            X.C007406x.A00(r0)
            r0 = -1849340872(0xffffffff91c54c38, float:-3.1128093E-28)
            X.C007406x.A00(r0)
            java.lang.String r0 = "CREATE UNIQUE INDEX IF NOT EXISTS threads_montage_thread_key_index ON threads(montage_thread_key)"
            r1.execSQL(r0)
            r0 = 2113435174(0x7df87626, float:4.128275E37)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x3203:
            r0 = 297(0x129, float:4.16E-43)
            if (r3 != r0) goto L_0x32b1
            r0 = -1495296591(0xffffffffa6df95b1, float:-1.5514307E-15)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE event_reminders ADD COLUMN event_reminder_guest_rsvps TEXT"
            r1.execSQL(r0)
            r0 = 1684562384(0x646861d0, float:1.7146771E22)
            X.C007406x.A00(r0)
            r0 = -462459246(0xffffffffe46f6e92, float:-1.7666957E22)
            r5 = 0
            X.C007406x.A01(r1, r0)     // Catch:{ all -> 0x33c7 }
            java.lang.String r0 = "SELECT event_reminder_key, user_key, member_guest_status FROM event_reminder_members"
            android.database.Cursor r5 = r1.rawQuery(r0, r5)     // Catch:{ all -> 0x33c7 }
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ all -> 0x33c7 }
            r6.<init>()     // Catch:{ all -> 0x33c7 }
        L_0x322a:
            boolean r0 = r5.moveToNext()     // Catch:{ all -> 0x33c7 }
            if (r0 == 0) goto L_0x3262
            r0 = 0
            java.lang.String r4 = r5.getString(r0)     // Catch:{ all -> 0x33c7 }
            r0 = 1
            java.lang.String r3 = r5.getString(r0)     // Catch:{ all -> 0x33c7 }
            if (r4 == 0) goto L_0x322a
            if (r3 == 0) goto L_0x322a
            boolean r0 = r6.containsKey(r4)     // Catch:{ all -> 0x33c7 }
            if (r0 == 0) goto L_0x325c
            java.lang.Object r2 = r6.get(r4)     // Catch:{ all -> 0x33c7 }
            java.util.Map r2 = (java.util.Map) r2     // Catch:{ all -> 0x33c7 }
        L_0x324a:
            boolean r0 = r2.containsKey(r3)     // Catch:{ all -> 0x33c7 }
            if (r0 != 0) goto L_0x322a
            r0 = 2
            java.lang.String r0 = r5.getString(r0)     // Catch:{ all -> 0x33c7 }
            r2.put(r3, r0)     // Catch:{ all -> 0x33c7 }
            r6.put(r4, r2)     // Catch:{ all -> 0x33c7 }
            goto L_0x322a
        L_0x325c:
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x33c7 }
            r2.<init>()     // Catch:{ all -> 0x33c7 }
            goto L_0x324a
        L_0x3262:
            java.util.Set r0 = r6.keySet()     // Catch:{ all -> 0x33c7 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x33c7 }
        L_0x326a:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x33c7 }
            if (r0 == 0) goto L_0x32a3
            java.lang.Object r3 = r7.next()     // Catch:{ all -> 0x33c7 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x33c7 }
            java.lang.Object r2 = r6.get(r3)     // Catch:{ all -> 0x33c7 }
            java.util.Map r2 = (java.util.Map) r2     // Catch:{ all -> 0x33c7 }
            X.38l r0 = new X.38l     // Catch:{ all -> 0x33c7 }
            r0.<init>()     // Catch:{ all -> 0x33c7 }
            java.lang.String r2 = r0.A0A(r2)     // Catch:{ all -> 0x33c7 }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ all -> 0x33c7 }
            r4.<init>()     // Catch:{ all -> 0x33c7 }
            java.lang.String r0 = "event_reminder_guest_rsvps"
            r4.put(r0, r2)     // Catch:{ all -> 0x33c7 }
            java.lang.String r0 = "event_reminder_key"
            X.0av r0 = X.C06160ax.A03(r0, r3)     // Catch:{ all -> 0x33c7 }
            java.lang.String r3 = "event_reminders"
            java.lang.String r2 = r0.A02()     // Catch:{ all -> 0x33c7 }
            java.lang.String[] r0 = r0.A04()     // Catch:{ all -> 0x33c7 }
            r1.update(r3, r4, r2, r0)     // Catch:{ all -> 0x33c7 }
            goto L_0x326a
        L_0x32a3:
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x33c7 }
            r5.close()
            r0 = -923603061(0xffffffffc8f2ef8b, float:-497532.34)
            X.C007406x.A02(r1, r0)
            goto L_0x003b
        L_0x32b1:
            r0 = 298(0x12a, float:4.18E-43)
            if (r3 != r0) goto L_0x32cc
            java.lang.String r0 = "event_reminder_members"
            java.lang.String r2 = X.AnonymousClass0W4.A00(r0)
            r0 = -1194213362(0xffffffffb8d1c00e, float:-1.00016696E-4)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = -2146775804(0xffffffff800acd04, float:-9.91901E-40)
            X.C007406x.A00(r0)
            goto L_0x003b
        L_0x32cc:
            r0 = 299(0x12b, float:4.19E-43)
            if (r3 != r0) goto L_0x33ba
            X.BJR[] r3 = X.BJJ.A00
            X.BJH[] r2 = X.BJJ.A01
            java.lang.String r0 = "sqliteproc_metadata"
            X.AnonymousClass0r8.A05(r1, r0, r3, r2)
            X.BJR[] r3 = X.BKB.A00
            X.BJH[] r2 = X.BKB.A01
            java.lang.String r0 = "sqliteproc_schema"
            X.AnonymousClass0r8.A05(r1, r0, r3, r2)
            X.BJW r5 = new X.BJW
            r5.<init>()
            r4 = 0
        L_0x32e8:
            X.BJj[] r0 = r5.B5G()
            int r0 = r0.length
            if (r4 >= r0) goto L_0x3379
            X.BJj[] r0 = r5.B5G()
            r0 = r0[r4]
            java.lang.String r11 = r0.A01
            X.BJR[] r10 = r5.Ahc(r4)
            X.BJH[] r9 = r5.Apr(r4)
            java.lang.String r0 = "SELECT * FROM "
            java.lang.String r2 = X.AnonymousClass08S.A0J(r0, r11)
            r0 = 0
            android.database.Cursor r3 = r1.rawQuery(r2, r0)
            java.lang.String r2 = ","
            java.lang.String[] r0 = r3.getColumnNames()     // Catch:{ all -> 0x33d4 }
            java.lang.String r8 = android.text.TextUtils.join(r2, r0)     // Catch:{ all -> 0x33d4 }
            r3.close()
            if (r8 == 0) goto L_0x3375
            java.lang.String r0 = "_temp"
            java.lang.String r7 = X.AnonymousClass08S.A0J(r11, r0)
            X.AnonymousClass0r8.A05(r1, r7, r10, r9)
            java.lang.String r6 = "INSERT OR IGNORE INTO %s (%s) SELECT %s FROM %s"
            java.lang.String r2 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r6, r7, r8, r8, r11)
            r0 = 580801109(0x229e5255, float:4.291316E-18)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 1895683170(0x70fdd462, float:6.284517E29)
            X.C007406x.A00(r0)
            java.lang.String r3 = "DROP TABLE IF EXISTS %s"
            java.lang.String r2 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r3, r11)
            r0 = -736564127(0xffffffffd418ec61, float:-2.62720324E12)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 1841907823(0x6dc9486f, float:7.786757E27)
            X.C007406x.A00(r0)
            X.AnonymousClass0r8.A05(r1, r11, r10, r9)
            java.lang.String r2 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r6, r11, r8, r8, r7)
            r0 = 1316257371(0x4e747e5b, float:1.02548038E9)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = 71701821(0x446153d, float:2.328454E-36)
            X.C007406x.A00(r0)
            java.lang.String r2 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r3, r7)
            r0 = -1739476836(0xffffffff9851b09c, float:-2.7101782E-24)
            X.C007406x.A00(r0)
            r1.execSQL(r2)
            r0 = -1708156759(0xffffffff9a2f98a9, float:-3.631247E-23)
            X.C007406x.A00(r0)
        L_0x3375:
            int r4 = r4 + 1
            goto L_0x32e8
        L_0x3379:
            X.BJW r6 = new X.BJW
            r6.<init>()
            r2 = 0
            java.lang.String r0 = "sqliteproc_schema"
            r1.delete(r0, r2, r2)
            java.lang.String r0 = "sqliteproc_metadata"
            r1.delete(r0, r2, r2)
            r5 = 0
        L_0x338a:
            X.BJj[] r0 = r6.B5G()
            int r0 = r0.length
            if (r5 >= r0) goto L_0x33ac
            X.BJj[] r0 = r6.B5G()
            r4 = r0[r5]
            java.lang.String r2 = r4.A01
            X.BJR[] r0 = r6.Ahc(r5)
            X.C28371eh.A03(r1, r2, r0)
            java.lang.String r3 = r4.A01
            java.lang.String r2 = r4.A00
            java.lang.String r0 = r4.A00
            X.C28371eh.A02(r1, r3, r2, r0)
            int r5 = r5 + 1
            goto L_0x338a
        L_0x33ac:
            X.1em r0 = r6.Ajf()
            java.lang.String r3 = r0.A00
            java.lang.String r2 = "__database__"
            r0 = 0
            X.C28371eh.A02(r1, r2, r3, r0)
            goto L_0x003b
        L_0x33ba:
            r2.A01(r1)
            goto L_0x003d
        L_0x33bf:
            r2 = move-exception
            r0 = -1208856306(0xffffffffb7f2510e, float:-2.8886392E-5)
            X.C007406x.A02(r1, r0)
            throw r2
        L_0x33c7:
            r2 = move-exception
            if (r5 == 0) goto L_0x33cd
            r5.close()
        L_0x33cd:
            r0 = 671871536(0x280bf230, float:7.768566E-15)
            X.C007406x.A02(r1, r0)
            throw r2
        L_0x33d4:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x33d6 }
        L_0x33d6:
            r0 = move-exception
            if (r3 == 0) goto L_0x33dc
            r3.close()     // Catch:{ all -> 0x33dc }
        L_0x33dc:
            throw r0
        L_0x33dd:
            r0 = 0
            A03(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11310mh.A08(android.database.sqlite.SQLiteDatabase, int, int):void");
    }
}
