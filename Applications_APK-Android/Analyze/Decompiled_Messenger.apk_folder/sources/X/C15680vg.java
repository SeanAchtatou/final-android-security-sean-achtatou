package X;

import android.util.SparseArray;

/* renamed from: X.0vg  reason: invalid class name and case insensitive filesystem */
public final class C15680vg {
    public int A00 = 0;
    public SparseArray A01 = new SparseArray();

    public static AnonymousClass1R1 A00(C15680vg r2, int i) {
        AnonymousClass1R1 r1 = (AnonymousClass1R1) r2.A01.get(i);
        if (r1 != null) {
            return r1;
        }
        AnonymousClass1R1 r12 = new AnonymousClass1R1();
        r2.A01.put(i, r12);
        return r12;
    }
}
