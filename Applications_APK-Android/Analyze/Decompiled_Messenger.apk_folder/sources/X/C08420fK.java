package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import com.google.common.collect.ImmutableList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fK  reason: invalid class name and case insensitive filesystem */
public final class C08420fK implements C08230et {
    private static volatile C08420fK A03;
    private AnonymousClass0UN A00;
    private final C08430fL A01;
    private final AtomicInteger A02 = new AtomicInteger(1);

    public String Azg() {
        return "responsiveness";
    }

    public static final C08420fK A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C08420fK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C08420fK(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private static void A01(PerformanceLoggingEvent performanceLoggingEvent, String str, C08460fO r7, C08460fO r8) {
        performanceLoggingEvent.A04(AnonymousClass08S.A0J(str, ".count"), r8.A00 - r7.A00);
        performanceLoggingEvent.A06(AnonymousClass08S.A0J(str, ".sum"), r8.A01 - r7.A01);
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        BJ6 bj6 = (BJ6) obj;
        BJ4 bj4 = (BJ4) obj2;
        if (bj6 != null && bj4 != null) {
            A01(performanceLoggingEvent, "responsiveness.stall.66", bj6.A01.A04, bj4.A04);
            A01(performanceLoggingEvent, "responsiveness.stall.200", bj6.A01.A03, bj4.A03);
            A01(performanceLoggingEvent, "responsiveness.stall.1", bj6.A01.A02, bj4.A02);
            BJ4 bj42 = bj6.A01;
            ImmutableList immutableList = bj4.A06;
            ImmutableList immutableList2 = bj4.A05;
            long j = bj42.A01;
            int size = bj4.A06.size();
            if (size > immutableList2.size()) {
                size = immutableList2.size();
            }
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            boolean z = true;
            for (int size2 = bj42.A06.size(); size2 < size; size2++) {
                if (z) {
                    z = false;
                } else {
                    sb.append(",");
                    sb2.append(",");
                }
                sb.append(((Long) immutableList.get(size2)).longValue() - j);
                sb2.append(immutableList2.get(size2));
            }
            performanceLoggingEvent.A0B("responsiveness.stall.end_times", sb.toString());
            performanceLoggingEvent.A0B("responsiveness.stall.durations", sb2.toString());
            BJ3 bj3 = bj6.A01.A00;
            BJ3 bj32 = bj4.A00;
            if (bj3 != null && bj32 != null && ((int) TimeUnit.NANOSECONDS.toMillis(performanceLoggingEvent.A08)) >= bj32.A00) {
                if (bj32.A01 == null) {
                    bj32.A01 = bj3;
                    C73113fW.A02(bj32.A0G, bj3.A0G);
                    C73113fW.A02(bj32.A07, bj3.A07);
                    C73113fW.A02(bj32.A0F, bj3.A0F);
                    C73113fW.A02(bj32.A0M, bj3.A0M);
                    C73113fW.A02(bj32.A0K, bj3.A0K);
                    C73113fW.A02(bj32.A0L, bj3.A0L);
                    C73113fW.A02(bj32.A0H, bj3.A0H);
                    C73113fW.A02(bj32.A0D, bj3.A0D);
                    C73113fW.A02(bj32.A0I, bj3.A0I);
                    C73113fW.A02(bj32.A0A, bj3.A0A);
                    C73113fW.A02(bj32.A0E, bj3.A0E);
                    C73113fW.A02(bj32.A06, bj3.A06);
                    C73113fW.A02(bj32.A0J, bj3.A0J);
                    C73113fW.A02(bj32.A0B, bj3.A0B);
                    C73113fW.A02(bj32.A09, bj3.A09);
                    C73113fW.A02(bj32.A08, bj3.A08);
                    C73113fW.A02(bj32.A0C, bj3.A0C);
                    bj32.A03.A00(bj3.A03);
                    bj32.A02.A00(bj3.A02);
                    bj32.A04.A00(bj3.A04);
                    bj32.A05.A00(bj3.A05);
                }
                String[] strArr = C73123fX.A03;
                String[] strArr2 = C73123fX.A02;
                BJ3.A01(performanceLoggingEvent, "st", bj32.A0G, strArr);
                BJ3.A01(performanceLoggingEvent, "st_anim", bj32.A07, strArr);
                BJ3.A01(performanceLoggingEvent, "st_scroll", bj32.A0F, strArr);
                BJ3.A01(performanceLoggingEvent, "st_vid", bj32.A0M, strArr);
                BJ3.A01(performanceLoggingEvent, "st_typing", bj32.A0K, strArr);
                BJ3.A01(performanceLoggingEvent, "st_typingext", bj32.A0L, strArr);
                BJ3.A01(performanceLoggingEvent, "st_touchd", bj32.A0H, strArr);
                BJ3.A01(performanceLoggingEvent, "st_touchd_nav_caused", bj32.A0D, strArr);
                BJ3.A01(performanceLoggingEvent, "st_touchu", bj32.A0I, strArr);
                BJ3.A01(performanceLoggingEvent, "st_touchu_nav_caused", bj32.A0E, strArr);
                BJ3.A01(performanceLoggingEvent, "st_touch_deduped", bj32.A0A, strArr);
                BJ3.A01(performanceLoggingEvent, "st_actff", bj32.A06, strArr);
                BJ3.A01(performanceLoggingEvent, "st_ttrc", bj32.A0J, strArr2);
                BJ3.A01(performanceLoggingEvent, "st_ttff", bj32.A0B, strArr2);
                BJ3.A01(performanceLoggingEvent, "st_cu", bj32.A09, strArr);
                BJ3.A01(performanceLoggingEvent, "st_cuff", bj32.A08, strArr);
                BJ3.A01(performanceLoggingEvent, "st_frame", bj32.A0C, strArr);
                performanceLoggingEvent.A04("frames", C73113fW.A00(bj32.A0C));
                BJ3.A00(performanceLoggingEvent, "frameMs", C73113fW.A01(bj32.A0C, 0));
                BJ3.A00(performanceLoggingEvent, "cus", bj32.A03.A00);
                BJ3.A00(performanceLoggingEvent, "cuMs", bj32.A03.A01);
                BJ3.A00(performanceLoggingEvent, "anims", bj32.A02.A00);
                BJ3.A00(performanceLoggingEvent, "animMs", bj32.A02.A01);
                BJ3.A00(performanceLoggingEvent, "videos", bj32.A05.A00);
                BJ3.A00(performanceLoggingEvent, "videoMs", bj32.A05.A01);
                BJ3.A00(performanceLoggingEvent, "scrolls", bj32.A04.A00);
                BJ3.A00(performanceLoggingEvent, "scrollMs", bj32.A04.A01);
                BJ3.A00(performanceLoggingEvent, "ttrcs", C73113fW.A00(bj32.A0J));
                BJ3.A00(performanceLoggingEvent, "ttffs", C73113fW.A00(bj32.A0B));
                BJ3.A00(performanceLoggingEvent, "typing", C73113fW.A00(bj32.A0K));
                BJ3.A00(performanceLoggingEvent, "touchD", C73113fW.A00(bj32.A0H));
                BJ3.A00(performanceLoggingEvent, "touchD_nav_caused", C73113fW.A00(bj32.A0D));
                BJ3.A00(performanceLoggingEvent, "touchU", C73113fW.A00(bj32.A0I));
                BJ3.A00(performanceLoggingEvent, "touchU_nav_caused", C73113fW.A00(bj32.A0E));
                BJ3.A00(performanceLoggingEvent, "touch_deduped", C73113fW.A00(bj32.A0A));
                BJ3.A00(performanceLoggingEvent, "typingMs", C73113fW.A01(bj32.A0K, 0));
                BJ3.A00(performanceLoggingEvent, "typingExtMs", C73113fW.A01(bj32.A0L, 0));
                BJ3.A00(performanceLoggingEvent, "touchDMs", C73113fW.A01(bj32.A0H, 0));
                BJ3.A00(performanceLoggingEvent, "touchDMs_nav_caused", C73113fW.A01(bj32.A0D, 0));
                BJ3.A00(performanceLoggingEvent, "touchUMs", C73113fW.A01(bj32.A0I, 0));
                BJ3.A00(performanceLoggingEvent, "touchUMs_nav_caused", C73113fW.A01(bj32.A0E, 0));
                BJ3.A00(performanceLoggingEvent, "touchMs_deduped", C73113fW.A01(bj32.A0A, 0));
                BJ3.A00(performanceLoggingEvent, "st500Ms", C73113fW.A01(bj32.A0G, 4));
                BJ3.A00(performanceLoggingEvent, "st500Ms_typing", C73113fW.A01(bj32.A0K, 4));
                BJ3.A00(performanceLoggingEvent, "st500Ms_typingext", C73113fW.A01(bj32.A0L, 4));
                BJ3.A00(performanceLoggingEvent, "st500Ms_touchD", C73113fW.A01(bj32.A0H, 4));
                BJ3.A00(performanceLoggingEvent, "st500Ms_touchD_nav_caused", C73113fW.A01(bj32.A0D, 4));
                BJ3.A00(performanceLoggingEvent, "st500Ms_touchU", C73113fW.A01(bj32.A0I, 4));
                BJ3.A00(performanceLoggingEvent, "st500Ms_touchU_nav_caused", C73113fW.A01(bj32.A0E, 4));
                BJ3.A00(performanceLoggingEvent, "st500Ms_touch_deduped", C73113fW.A01(bj32.A0A, 4));
                BJ3.A00(performanceLoggingEvent, "st66Ms_anim", C73113fW.A01(bj32.A07, 2));
                BJ3.A00(performanceLoggingEvent, "st66Ms_cu", C73113fW.A01(bj32.A09, 2));
                BJ3.A00(performanceLoggingEvent, "st66Ms_cuff", C73113fW.A01(bj32.A08, 2));
                BJ3.A00(performanceLoggingEvent, "st66Ms_scroll", C73113fW.A01(bj32.A0F, 2));
                BJ3.A00(performanceLoggingEvent, "st66Ms_video", C73113fW.A01(bj32.A0M, 2));
            }
        }
    }

    public long Azh() {
        return C08350fD.A0E;
    }

    public Class B3O() {
        return BJ4.class;
    }

    public Class B3e() {
        return BJ6.class;
    }

    public Object CGO() {
        return ((C08450fN) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGo, this.A00)).A04();
    }

    public Object CGf() {
        int andIncrement = this.A02.getAndIncrement();
        C08450fN r2 = (C08450fN) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGo, this.A00);
        if (C08450fN.A0A != Thread.currentThread()) {
            C08430fL r1 = r2.A08;
            r1.sendMessage(r1.obtainMessage(0, andIncrement, 0));
        } else {
            C08450fN.A02(r2, andIncrement);
        }
        BJ4 A04 = r2.A04();
        if (A04 == null) {
            return null;
        }
        BJ6 bj6 = new BJ6(andIncrement, A04);
        C08430fL r4 = this.A01;
        r4.sendMessageDelayed(r4.obtainMessage(2, bj6), ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At0(564740954784680L));
        return bj6;
    }

    public void CHl(Object obj) {
        BJ6 bj6 = (BJ6) obj;
        if (bj6 != null) {
            int i = bj6.A00;
            this.A01.removeMessages(2, bj6);
            C08430fL r2 = ((C08450fN) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGo, this.A00)).A08;
            r2.sendMessage(r2.obtainMessage(1, i, 0));
        }
    }

    private C08420fK(AnonymousClass1XY r5) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(2, r5);
        this.A00 = r2;
        this.A01 = new C08430fL((C08450fN) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGo, r2));
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A0A;
    }
}
