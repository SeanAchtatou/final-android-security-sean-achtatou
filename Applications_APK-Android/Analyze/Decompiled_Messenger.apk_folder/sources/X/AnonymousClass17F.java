package X;

import com.facebook.messaging.notify.VideoChatLinkJoinAttemptNotification;
import java.util.concurrent.ExecutorService;

/* renamed from: X.17F  reason: invalid class name */
public final class AnonymousClass17F extends C191316x {
    public static final String __redex_internal_original_name = "com.facebook.orca.notify.AsyncNotificationClient$30";
    public final /* synthetic */ VideoChatLinkJoinAttemptNotification A00;
    public final /* synthetic */ C190716r A01;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass17F(C190716r r1, ExecutorService executorService, AnonymousClass0US r3, String str, VideoChatLinkJoinAttemptNotification videoChatLinkJoinAttemptNotification) {
        super(executorService, r3, str);
        this.A01 = r1;
        this.A00 = videoChatLinkJoinAttemptNotification;
    }
}
