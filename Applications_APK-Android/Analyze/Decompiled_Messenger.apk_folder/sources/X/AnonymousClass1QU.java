package X;

import android.util.Pair;
import java.util.Iterator;

/* renamed from: X.1QU  reason: invalid class name */
public abstract class AnonymousClass1QU implements C23581Qb {
    private boolean A00 = false;

    public static boolean A03(int i) {
        return (i & 1) == 1;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public void A04() {
        AnonymousClass0d1 r1;
        if (this instanceof AnonymousClass1RL) {
            AnonymousClass1RL r12 = (AnonymousClass1RL) this;
            try {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A02("MultiplexProducer#onCancellation");
                }
                AnonymousClass1R4 r2 = r12.A00;
                synchronized (r2) {
                    if (r2.A03 == r12) {
                        r2.A03 = null;
                        r2.A02 = null;
                        AnonymousClass1R4.A05(r2.A04);
                        r2.A04 = null;
                        AnonymousClass1R4.A04(r2);
                    }
                }
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
            } catch (Throwable th) {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
                throw th;
            }
        } else if (!(this instanceof AnonymousClass1RO)) {
            AnonymousClass1QM r13 = ((AnonymousClass1QQ) this).A00;
            synchronized (r13) {
                C05520Zg.A05(r13.isClosed());
            }
        } else {
            AnonymousClass1RO r14 = (AnonymousClass1RO) this;
            if (!(r14 instanceof AnonymousClass1n9)) {
                if (r14 instanceof AnonymousClass0d0) {
                    AnonymousClass0d0 r15 = (AnonymousClass0d0) r14;
                    boolean A002 = AnonymousClass0d0.A00(r15);
                    r1 = r15;
                    if (!A002) {
                        return;
                    }
                } else if (!(r14 instanceof AnonymousClass0d1)) {
                    boolean z = r14 instanceof AnonymousClass1RQ;
                    r1 = r14;
                    if (z) {
                        AnonymousClass1RQ r16 = (AnonymousClass1RQ) r14;
                        AnonymousClass1RQ.A01(r16, true);
                        r16.A00.onCancellation();
                        return;
                    }
                } else {
                    AnonymousClass0d1 r17 = (AnonymousClass0d1) r14;
                    boolean A01 = AnonymousClass0d1.A01(r17);
                    r1 = r17;
                    if (!A01) {
                        return;
                    }
                }
                r1.A00.onCancellation();
                return;
            }
            AnonymousClass1n9 r18 = (AnonymousClass1n9) r14;
            r18.A00.onCancellation();
            AnonymousClass1n9.A00(r18);
        }
    }

    public void A05(float f) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:272:0x03e0, code lost:
        X.AnonymousClass1PS.A05(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x03e3, code lost:
        if (r0 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x03e5, code lost:
        X.AnonymousClass07A.A04(r3.A08.A01, new X.AnonymousClass375(r3), 1696794265);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x03f4, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:300:?, code lost:
        r3.A00.Bgg(r2, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:358:0x04e2, code lost:
        if (r0 != false) goto L_0x04e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:362:0x04f0, code lost:
        if (r3.canResize(r10, r4.A07, r4.A06) != false) goto L_0x04f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:518:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:361:0x04e7  */
    /* JADX WARNING: Removed duplicated region for block: B:370:0x0506  */
    /* JADX WARNING: Removed duplicated region for block: B:396:0x0559  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:84:0x00f2=Splitter:B:84:0x00f2, B:178:0x0276=Splitter:B:178:0x0276, B:467:0x0687=Splitter:B:467:0x0687} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(java.lang.Object r10, int r11) {
        /*
            r9 = this;
            boolean r0 = r9 instanceof X.AnonymousClass1RL
            if (r0 != 0) goto L_0x063f
            boolean r0 = r9 instanceof X.AnonymousClass1SN
            if (r0 != 0) goto L_0x05d0
            boolean r0 = r9 instanceof X.AnonymousClass1SQ
            if (r0 != 0) goto L_0x0593
            boolean r0 = r9 instanceof X.AnonymousClass1n9
            if (r0 != 0) goto L_0x0581
            boolean r0 = r9 instanceof X.AnonymousClass1SR
            if (r0 != 0) goto L_0x0571
            boolean r0 = r9 instanceof X.AnonymousClass1SP
            if (r0 != 0) goto L_0x046e
            boolean r0 = r9 instanceof X.AnonymousClass1SY
            if (r0 != 0) goto L_0x044e
            boolean r0 = r9 instanceof X.AnonymousClass1SZ
            if (r0 != 0) goto L_0x043b
            boolean r0 = r9 instanceof X.AnonymousClass0d0
            if (r0 != 0) goto L_0x03f8
            boolean r0 = r9 instanceof X.AnonymousClass0d1
            if (r0 != 0) goto L_0x03b1
            boolean r0 = r9 instanceof X.C24091Se
            if (r0 != 0) goto L_0x0374
            boolean r0 = r9 instanceof X.C24101Sf
            if (r0 != 0) goto L_0x02b7
            boolean r0 = r9 instanceof X.C23851Re
            if (r0 != 0) goto L_0x0230
            boolean r0 = r9 instanceof X.C33261nI
            if (r0 != 0) goto L_0x01da
            boolean r0 = r9 instanceof X.AnonymousClass1RQ
            if (r0 != 0) goto L_0x0179
            boolean r0 = r9 instanceof X.C24221St
            if (r0 != 0) goto L_0x013a
            boolean r0 = r9 instanceof X.C24231Su
            if (r0 != 0) goto L_0x00f9
            boolean r0 = r9 instanceof X.AnonymousClass1RM
            if (r0 != 0) goto L_0x0055
            boolean r0 = r9 instanceof X.C23571Qa
            if (r0 != 0) goto L_0x06d6
            r0 = r9
            X.1QQ r0 = (X.AnonymousClass1QQ) r0
            X.1QM r0 = r0.A00
            r0.A0A(r10, r11)
            return
        L_0x0055:
            r3 = r9
            X.1RM r3 = (X.AnonymousClass1RM) r3
            X.1PS r10 = (X.AnonymousClass1PS) r10
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x0065
            java.lang.String r0 = "BitmapMemoryCacheProducer#onNewResultImpl"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x06fa }
        L_0x0065:
            boolean r6 = A03(r11)     // Catch:{ all -> 0x06fa }
            r4 = 0
            if (r10 != 0) goto L_0x0075
            if (r6 == 0) goto L_0x06cc
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x06fa }
            r0.Bgg(r4, r11)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x0075:
            java.lang.Object r0 = r10.A0A()     // Catch:{ all -> 0x06fa }
            X.1S9 r0 = (X.AnonymousClass1S9) r0     // Catch:{ all -> 0x06fa }
            boolean r0 = r0.A03()     // Catch:{ all -> 0x06fa }
            if (r0 != 0) goto L_0x00f2
            r2 = 8
            r1 = r11 & r2
            r0 = 0
            if (r1 != r2) goto L_0x0089
            r0 = 1
        L_0x0089:
            if (r0 != 0) goto L_0x00f2
            if (r6 != 0) goto L_0x00cb
            X.1QD r0 = r3.A01     // Catch:{ all -> 0x06fa }
            X.1Pa r1 = r0.A00     // Catch:{ all -> 0x06fa }
            X.1Qd r0 = r3.A00     // Catch:{ all -> 0x06fa }
            X.1PS r5 = r1.Ab8(r0)     // Catch:{ all -> 0x06fa }
            if (r5 == 0) goto L_0x00cb
            java.lang.Object r0 = r10.A0A()     // Catch:{ all -> 0x00c2 }
            X.1S9 r0 = (X.AnonymousClass1S9) r0     // Catch:{ all -> 0x00c2 }
            X.1nJ r2 = r0.Azo()     // Catch:{ all -> 0x00c2 }
            java.lang.Object r0 = r5.A0A()     // Catch:{ all -> 0x00c2 }
            X.1S9 r0 = (X.AnonymousClass1S9) r0     // Catch:{ all -> 0x00c2 }
            X.1nJ r1 = r0.Azo()     // Catch:{ all -> 0x00c2 }
            boolean r0 = r1.A01     // Catch:{ all -> 0x00c2 }
            if (r0 != 0) goto L_0x00b8
            int r1 = r1.A00     // Catch:{ all -> 0x00c2 }
            int r0 = r2.A00     // Catch:{ all -> 0x00c2 }
            if (r1 >= r0) goto L_0x00b8
            goto L_0x00c8
        L_0x00b8:
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x00c2 }
            r0.Bgg(r5, r11)     // Catch:{ all -> 0x00c2 }
            X.AnonymousClass1PS.A05(r5)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x00c2:
            r0 = move-exception
            X.AnonymousClass1PS.A05(r5)     // Catch:{ all -> 0x06fa }
            goto L_0x06f9
        L_0x00c8:
            X.AnonymousClass1PS.A05(r5)     // Catch:{ all -> 0x06fa }
        L_0x00cb:
            boolean r0 = r3.A02     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x00d9
            X.1QD r0 = r3.A01     // Catch:{ all -> 0x06fa }
            X.1Pa r1 = r0.A00     // Catch:{ all -> 0x06fa }
            X.1Qd r0 = r3.A00     // Catch:{ all -> 0x06fa }
            X.1PS r4 = r1.AR1(r0, r10)     // Catch:{ all -> 0x06fa }
        L_0x00d9:
            if (r6 == 0) goto L_0x00e2
            X.1Qb r1 = r3.A00     // Catch:{ all -> 0x00ef }
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.BkJ(r0)     // Catch:{ all -> 0x00ef }
        L_0x00e2:
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x00ef }
            if (r4 == 0) goto L_0x00e7
            r10 = r4
        L_0x00e7:
            r0.Bgg(r10, r11)     // Catch:{ all -> 0x00ef }
            X.AnonymousClass1PS.A05(r4)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x00ef:
            r0 = move-exception
            goto L_0x06f6
        L_0x00f2:
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x06fa }
            r0.Bgg(r10, r11)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x00f9:
            r3 = r9
            X.1Su r3 = (X.C24231Su) r3
            X.1PS r10 = (X.AnonymousClass1PS) r10
            if (r10 == 0) goto L_0x0134
            boolean r0 = r10.A0B()
            if (r0 == 0) goto L_0x0134
            java.lang.Object r1 = r10.A0A()
            X.1S9 r1 = (X.AnonymousClass1S9) r1
            if (r1 == 0) goto L_0x0134
            boolean r0 = r1.A02()
            if (r0 != 0) goto L_0x0134
            boolean r0 = r1 instanceof X.AnonymousClass1S5
            if (r0 == 0) goto L_0x0134
            X.1S5 r1 = (X.AnonymousClass1S5) r1
            android.graphics.Bitmap r2 = r1.A04()
            if (r2 == 0) goto L_0x0134
            int r1 = r2.getRowBytes()
            int r0 = r2.getHeight()
            int r1 = r1 * r0
            int r0 = r3.A01
            if (r1 < r0) goto L_0x0134
            int r0 = r3.A00
            if (r1 > r0) goto L_0x0134
            r2.prepareToDraw()
        L_0x0134:
            X.1Qb r0 = r3.A00
            r0.Bgg(r10, r11)
            return
        L_0x013a:
            r4 = r9
            X.1St r4 = (X.C24221St) r4
            X.1NY r10 = (X.AnonymousClass1NY) r10
            X.1QK r0 = r4.A00
            X.1Q0 r3 = r0.A09
            boolean r2 = A03(r11)
            X.36w r0 = r3.A06
            boolean r1 = X.C52892jr.A01(r10, r0)
            if (r10 == 0) goto L_0x015e
            if (r1 != 0) goto L_0x0155
            boolean r0 = r3.A0G
            if (r0 == 0) goto L_0x015e
        L_0x0155:
            if (r2 == 0) goto L_0x0171
            if (r1 == 0) goto L_0x0171
            X.1Qb r0 = r4.A00
            r0.Bgg(r10, r11)
        L_0x015e:
            if (r2 == 0) goto L_0x06e3
            if (r1 != 0) goto L_0x06e3
            X.AnonymousClass1NY.A04(r10)
            X.2jp r0 = r4.A01
            X.1Q3 r2 = r0.A00
            X.1Qb r1 = r4.A00
            X.1QK r0 = r4.A00
            r2.ByS(r1, r0)
            return
        L_0x0171:
            r0 = -2
            r11 = r11 & r0
            X.1Qb r0 = r4.A00
            r0.Bgg(r10, r11)
            goto L_0x015e
        L_0x0179:
            r2 = r9
            X.1RQ r2 = (X.AnonymousClass1RQ) r2
            X.1NY r10 = (X.AnonymousClass1NY) r10
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x0189
            java.lang.String r0 = "DecodeProducer#onNewResultImpl"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x06fa }
        L_0x0189:
            boolean r3 = A03(r11)     // Catch:{ all -> 0x06fa }
            if (r3 == 0) goto L_0x01bb
            if (r10 != 0) goto L_0x01a3
            X.0NY r1 = new X.0NY     // Catch:{ all -> 0x06fa }
            java.lang.String r0 = "Encoded image is null."
            r1.<init>(r0)     // Catch:{ all -> 0x06fa }
            r0 = 1
            X.AnonymousClass1RQ.A01(r2, r0)     // Catch:{ all -> 0x06fa }
            X.1Qb r0 = r2.A00     // Catch:{ all -> 0x06fa }
            r0.BYh(r1)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x01a3:
            boolean r0 = r10.A0C()     // Catch:{ all -> 0x06fa }
            if (r0 != 0) goto L_0x01bb
            X.0NY r1 = new X.0NY     // Catch:{ all -> 0x06fa }
            java.lang.String r0 = "Encoded image is not valid."
            r1.<init>(r0)     // Catch:{ all -> 0x06fa }
            r0 = 1
            X.AnonymousClass1RQ.A01(r2, r0)     // Catch:{ all -> 0x06fa }
            X.1Qb r0 = r2.A00     // Catch:{ all -> 0x06fa }
            r0.BYh(r1)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x01bb:
            boolean r0 = r2.A0A(r10, r11)     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x06cc
            r1 = 4
            r11 = r11 & r1
            r0 = 0
            if (r11 != r1) goto L_0x01c7
            r0 = 1
        L_0x01c7:
            if (r3 != 0) goto L_0x01d3
            if (r0 != 0) goto L_0x01d3
            X.1QK r0 = r2.A02     // Catch:{ all -> 0x06fa }
            boolean r0 = r0.A07()     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x06cc
        L_0x01d3:
            X.1RU r0 = r2.A03     // Catch:{ all -> 0x06fa }
            r0.A04()     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x01da:
            r5 = r9
            X.1nI r5 = (X.C33261nI) r5
            X.1NY r10 = (X.AnonymousClass1NY) r10
            X.1QK r1 = r5.A03
            X.1MN r0 = r1.A07
            java.lang.String r4 = "DiskCacheWriteProducer"
            r0.Bk8(r1, r4)
            boolean r0 = A03(r11)
            r0 = r0 ^ 1
            r3 = 0
            if (r0 != 0) goto L_0x021d
            if (r10 == 0) goto L_0x021d
            r0 = 10
            r1 = r11 & r0
            r0 = 0
            if (r1 == 0) goto L_0x01fb
            r0 = 1
        L_0x01fb:
            if (r0 != 0) goto L_0x021d
            X.AnonymousClass1NY.A05(r10)
            X.1O3 r1 = r10.A07
            X.1O3 r0 = X.AnonymousClass1O3.A02
            if (r1 == r0) goto L_0x021d
            X.1QK r2 = r5.A03
            X.1Q0 r6 = r2.A09
            X.1Mc r1 = r5.A02
            java.lang.Object r0 = r2.A0A
            X.1Qd r2 = r1.A05(r6, r0)
            X.1OM r1 = r6.A09
            X.1OM r0 = X.AnonymousClass1OM.SMALL
            if (r1 != r0) goto L_0x022a
            X.1iq r0 = r5.A01
            r0.A03(r2, r10)
        L_0x021d:
            X.1QK r1 = r5.A03
            X.1MN r0 = r1.A07
            r0.Bk6(r1, r4, r3)
            X.1Qb r0 = r5.A00
            r0.Bgg(r10, r11)
            return
        L_0x022a:
            X.1iq r0 = r5.A00
            r0.A03(r2, r10)
            goto L_0x021d
        L_0x0230:
            r3 = r9
            X.1Re r3 = (X.C23851Re) r3
            X.1NY r10 = (X.AnonymousClass1NY) r10
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x0240
            java.lang.String r0 = "EncodedMemoryCacheProducer#onNewResultImpl"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x06fa }
        L_0x0240:
            boolean r0 = A03(r11)     // Catch:{ all -> 0x06fa }
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x02b0
            if (r10 == 0) goto L_0x02b0
            r0 = 10
            r1 = r11 & r0
            r0 = 0
            if (r1 == 0) goto L_0x0252
            r0 = 1
        L_0x0252:
            if (r0 != 0) goto L_0x02b0
            X.AnonymousClass1NY.A05(r10)     // Catch:{ all -> 0x06fa }
            X.1O3 r1 = r10.A07     // Catch:{ all -> 0x06fa }
            X.1O3 r0 = X.AnonymousClass1O3.A02     // Catch:{ all -> 0x06fa }
            if (r1 == r0) goto L_0x02b0
            X.1PS r0 = r10.A0A     // Catch:{ all -> 0x06fa }
            X.1PS r2 = X.AnonymousClass1PS.A00(r0)     // Catch:{ all -> 0x06fa }
            if (r2 == 0) goto L_0x02a9
            r1 = 0
            boolean r0 = r3.A02     // Catch:{ all -> 0x02a3 }
            if (r0 == 0) goto L_0x0276
            boolean r0 = r3.A03     // Catch:{ all -> 0x02a3 }
            if (r0 == 0) goto L_0x0276
            X.1Pa r1 = r3.A01     // Catch:{ all -> 0x02a3 }
            X.1Qd r0 = r3.A00     // Catch:{ all -> 0x02a3 }
            X.1PS r1 = r1.AR1(r0, r2)     // Catch:{ all -> 0x02a3 }
        L_0x0276:
            X.AnonymousClass1PS.A05(r2)     // Catch:{ all -> 0x06fa }
            if (r1 == 0) goto L_0x02a9
            X.1NY r2 = new X.1NY     // Catch:{ all -> 0x029d }
            r2.<init>(r1)     // Catch:{ all -> 0x029d }
            r2.A0B(r10)     // Catch:{ all -> 0x029d }
            X.AnonymousClass1PS.A05(r1)     // Catch:{ all -> 0x06fa }
            X.1Qb r1 = r3.A00     // Catch:{ all -> 0x0297 }
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.BkJ(r0)     // Catch:{ all -> 0x0297 }
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x0297 }
            r0.Bgg(r2, r11)     // Catch:{ all -> 0x0297 }
            X.AnonymousClass1NY.A04(r2)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x0297:
            r0 = move-exception
            X.AnonymousClass1NY.A04(r2)     // Catch:{ all -> 0x06fa }
            goto L_0x06f9
        L_0x029d:
            r0 = move-exception
            X.AnonymousClass1PS.A05(r1)     // Catch:{ all -> 0x06fa }
            goto L_0x06f9
        L_0x02a3:
            r0 = move-exception
            X.AnonymousClass1PS.A05(r2)     // Catch:{ all -> 0x06fa }
            goto L_0x06f9
        L_0x02a9:
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x06fa }
            r0.Bgg(r10, r11)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x02b0:
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x06fa }
            r0.Bgg(r10, r11)     // Catch:{ all -> 0x06fa }
            goto L_0x06cc
        L_0x02b7:
            r3 = r9
            X.1Sf r3 = (X.C24101Sf) r3
            X.1NY r10 = (X.AnonymousClass1NY) r10
            boolean r0 = A03(r11)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x06e3
            X.1NY r4 = r3.A02
            if (r4 == 0) goto L_0x0348
            X.9Mo r0 = r10.A08
            if (r0 == 0) goto L_0x0348
            int r1 = r10.A08()     // Catch:{ IOException -> 0x031d }
            X.9Mo r0 = r10.A08     // Catch:{ IOException -> 0x031d }
            int r0 = r0.A00     // Catch:{ IOException -> 0x031d }
            int r1 = r1 + r0
            X.1iR r0 = r3.A03     // Catch:{ IOException -> 0x031d }
            X.1S2 r2 = new X.1S2     // Catch:{ IOException -> 0x031d }
            X.1Ni r0 = r0.A01     // Catch:{ IOException -> 0x031d }
            r2.<init>(r0, r1)     // Catch:{ IOException -> 0x031d }
            X.9Mo r0 = r10.A08     // Catch:{ IOException -> 0x031d }
            int r1 = r0.A00     // Catch:{ IOException -> 0x031d }
            java.io.InputStream r0 = r4.A09()     // Catch:{ IOException -> 0x031d }
            X.C24101Sf.A00(r3, r0, r2, r1)     // Catch:{ IOException -> 0x031d }
            java.io.InputStream r1 = r10.A09()     // Catch:{ IOException -> 0x031d }
            int r0 = r10.A08()     // Catch:{ IOException -> 0x031d }
            X.C24101Sf.A00(r3, r1, r2, r0)     // Catch:{ IOException -> 0x031d }
            X.1SS r0 = r2.A01()     // Catch:{ IOException -> 0x031d }
            X.1PS r4 = X.AnonymousClass1PS.A01(r0)     // Catch:{ IOException -> 0x031d }
            r1 = 0
            X.1NY r2 = new X.1NY     // Catch:{ all -> 0x0314 }
            r2.<init>(r4)     // Catch:{ all -> 0x0314 }
            r2.A0A()     // Catch:{ all -> 0x0312 }
            X.1Qb r1 = r3.A00     // Catch:{ all -> 0x0312 }
            r0 = 1
            r1.Bgg(r2, r0)     // Catch:{ all -> 0x0312 }
            X.AnonymousClass1NY.A04(r2)     // Catch:{ IOException -> 0x031d }
            X.AnonymousClass1PS.A05(r4)     // Catch:{ IOException -> 0x031d }
            goto L_0x032e
        L_0x0312:
            r0 = move-exception
            goto L_0x0316
        L_0x0314:
            r0 = move-exception
            r2 = r1
        L_0x0316:
            X.AnonymousClass1NY.A04(r2)     // Catch:{ IOException -> 0x031d }
            X.AnonymousClass1PS.A05(r4)     // Catch:{ IOException -> 0x031d }
            throw r0     // Catch:{ IOException -> 0x031d }
        L_0x031d:
            r2 = move-exception
            r0 = 110(0x6e, float:1.54E-43)
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ all -> 0x033e }
            java.lang.String r0 = "Error while merging image data"
            X.AnonymousClass02w.A0D(r1, r0, r2)     // Catch:{ all -> 0x033e }
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x033e }
            r0.BYh(r2)     // Catch:{ all -> 0x033e }
        L_0x032e:
            r10.close()
            X.1NY r0 = r3.A02
            r0.close()
            X.1iq r1 = r3.A01
            X.1Qd r0 = r3.A00
            r1.A02(r0)
            return
        L_0x033e:
            r1 = move-exception
            r10.close()
            X.1NY r0 = r3.A02
            r0.close()
            throw r1
        L_0x0348:
            r2 = 8
            r1 = r11 & r2
            r0 = 0
            if (r1 != r2) goto L_0x0350
            r0 = 1
        L_0x0350:
            if (r0 == 0) goto L_0x036e
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x036e
            X.AnonymousClass1NY.A05(r10)
            X.1O3 r1 = r10.A07
            X.1O3 r0 = X.AnonymousClass1O3.A02
            if (r1 == r0) goto L_0x036e
            X.1iq r1 = r3.A01
            X.1Qd r0 = r3.A00
            r1.A03(r0, r10)
            X.1Qb r0 = r3.A00
            r0.Bgg(r10, r11)
            return
        L_0x036e:
            X.1Qb r0 = r3.A00
            r0.Bgg(r10, r11)
            return
        L_0x0374:
            r3 = r9
            X.1Se r3 = (X.C24091Se) r3
            X.1PS r10 = (X.AnonymousClass1PS) r10
            r2 = 0
            if (r10 != 0) goto L_0x0388
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x06e3
            X.1Qb r0 = r3.A00
            r0.Bgg(r2, r11)
            return
        L_0x0388:
            boolean r0 = A03(r11)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0395
            boolean r0 = r3.A03
            if (r0 != 0) goto L_0x0395
            return
        L_0x0395:
            boolean r0 = r3.A02
            if (r0 == 0) goto L_0x03a1
            X.1Pa r1 = r3.A01
            X.1Qd r0 = r3.A00
            X.1PS r2 = r1.AR1(r0, r10)
        L_0x03a1:
            X.1Qb r1 = r3.A00     // Catch:{ all -> 0x0430 }
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.BkJ(r0)     // Catch:{ all -> 0x0430 }
            X.1Qb r0 = r3.A00     // Catch:{ all -> 0x0430 }
            if (r2 == 0) goto L_0x03ad
            r10 = r2
        L_0x03ad:
            r0.Bgg(r10, r11)     // Catch:{ all -> 0x0430 }
            goto L_0x042c
        L_0x03b1:
            r3 = r9
            X.0d1 r3 = (X.AnonymousClass0d1) r3
            X.1PS r10 = (X.AnonymousClass1PS) r10
            boolean r0 = X.AnonymousClass1PS.A07(r10)
            if (r0 != 0) goto L_0x03c7
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x06e3
            r0 = 0
            X.AnonymousClass0d1.A00(r3, r0, r11)
            return
        L_0x03c7:
            monitor-enter(r3)
            boolean r0 = r3.A02     // Catch:{ all -> 0x03f5 }
            if (r0 == 0) goto L_0x03ce
            monitor-exit(r3)     // Catch:{ all -> 0x03f5 }
            return
        L_0x03ce:
            X.1PS r1 = r3.A01     // Catch:{ all -> 0x03f5 }
            X.1PS r0 = X.AnonymousClass1PS.A00(r10)     // Catch:{ all -> 0x03f5 }
            r3.A01 = r0     // Catch:{ all -> 0x03f5 }
            r3.A00 = r11     // Catch:{ all -> 0x03f5 }
            r0 = 1
            r3.A03 = r0     // Catch:{ all -> 0x03f5 }
            boolean r0 = X.AnonymousClass0d1.A02(r3)     // Catch:{ all -> 0x03f5 }
            monitor-exit(r3)     // Catch:{ all -> 0x03f5 }
            X.AnonymousClass1PS.A05(r1)
            if (r0 == 0) goto L_0x06e3
            X.36z r0 = r3.A08
            java.util.concurrent.Executor r2 = r0.A01
            X.375 r1 = new X.375
            r1.<init>(r3)
            r0 = 1696794265(0x65230699, float:4.8116715E22)
            X.AnonymousClass07A.A04(r2, r1, r0)
            return
        L_0x03f5:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x03f5 }
            goto L_0x043a
        L_0x03f8:
            r3 = r9
            X.0d0 r3 = (X.AnonymousClass0d0) r3
            X.1PS r10 = (X.AnonymousClass1PS) r10
            boolean r0 = A03(r11)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x06e3
            monitor-enter(r3)
            boolean r0 = r3.A01     // Catch:{ all -> 0x0438 }
            if (r0 == 0) goto L_0x040c
            monitor-exit(r3)     // Catch:{ all -> 0x0438 }
            goto L_0x0418
        L_0x040c:
            X.1PS r1 = r3.A00     // Catch:{ all -> 0x0438 }
            X.1PS r0 = X.AnonymousClass1PS.A00(r10)     // Catch:{ all -> 0x0438 }
            r3.A00 = r0     // Catch:{ all -> 0x0438 }
            monitor-exit(r3)     // Catch:{ all -> 0x0438 }
            X.AnonymousClass1PS.A05(r1)
        L_0x0418:
            monitor-enter(r3)
            boolean r0 = r3.A01     // Catch:{ all -> 0x0435 }
            if (r0 == 0) goto L_0x041f
            monitor-exit(r3)     // Catch:{ all -> 0x0435 }
            return
        L_0x041f:
            X.1PS r0 = r3.A00     // Catch:{ all -> 0x0435 }
            X.1PS r2 = X.AnonymousClass1PS.A00(r0)     // Catch:{ all -> 0x0435 }
            monitor-exit(r3)     // Catch:{ all -> 0x0435 }
            X.1Qb r1 = r3.A00     // Catch:{ all -> 0x0430 }
            r0 = 0
            r1.Bgg(r2, r0)     // Catch:{ all -> 0x0430 }
        L_0x042c:
            X.AnonymousClass1PS.A05(r2)
            return
        L_0x0430:
            r0 = move-exception
            X.AnonymousClass1PS.A05(r2)
            throw r0
        L_0x0435:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0435 }
            goto L_0x043a
        L_0x0438:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0438 }
        L_0x043a:
            throw r0
        L_0x043b:
            r1 = r9
            X.1SZ r1 = (X.AnonymousClass1SZ) r1
            X.1PS r10 = (X.AnonymousClass1PS) r10
            boolean r0 = A03(r11)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x06e3
            X.1Qb r0 = r1.A00
            r0.Bgg(r10, r11)
            return
        L_0x044e:
            r2 = r9
            X.1SY r2 = (X.AnonymousClass1SY) r2
            X.1NY r10 = (X.AnonymousClass1NY) r10
            r1 = 0
            boolean r0 = X.AnonymousClass1NY.A07(r10)     // Catch:{ all -> 0x0469 }
            if (r0 == 0) goto L_0x0460
            X.1PS r0 = r10.A0A     // Catch:{ all -> 0x0469 }
            X.1PS r1 = X.AnonymousClass1PS.A00(r0)     // Catch:{ all -> 0x0469 }
        L_0x0460:
            X.1Qb r0 = r2.A00     // Catch:{ all -> 0x0469 }
            r0.Bgg(r1, r11)     // Catch:{ all -> 0x0469 }
            X.AnonymousClass1PS.A05(r1)
            return
        L_0x0469:
            r0 = move-exception
            X.AnonymousClass1PS.A05(r1)
            throw r0
        L_0x046e:
            r5 = r9
            X.1SP r5 = (X.AnonymousClass1SP) r5
            X.1NY r10 = (X.AnonymousClass1NY) r10
            boolean r0 = r5.A00
            if (r0 != 0) goto L_0x06e3
            boolean r8 = A03(r11)
            if (r10 != 0) goto L_0x0487
            if (r8 == 0) goto L_0x06e3
            X.1Qb r2 = r5.A00
            r1 = 0
            r0 = 1
            r2.Bgg(r1, r0)
            return
        L_0x0487:
            X.AnonymousClass1NY.A05(r10)
            X.1O3 r2 = r10.A07
            X.1QK r0 = r5.A01
            X.1Q0 r4 = r0.A09
            X.1Pe r1 = r5.A03
            boolean r0 = r5.A04
            X.1T5 r3 = r1.createImageTranscoder(r2, r0)
            X.C05520Zg.A02(r3)
            if (r10 == 0) goto L_0x04ff
            X.AnonymousClass1NY.A05(r10)
            X.1O3 r1 = r10.A07
            X.1O3 r0 = X.AnonymousClass1O3.A02
            if (r1 == r0) goto L_0x04ff
            X.AnonymousClass1NY.A05(r10)
            X.1O3 r0 = r10.A07
            boolean r0 = r3.canTranscode(r0)
            if (r0 != 0) goto L_0x04ba
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.NO
        L_0x04b3:
            if (r8 != 0) goto L_0x0502
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r1 != r0) goto L_0x0502
            return
        L_0x04ba:
            X.1Q1 r7 = r4.A07
            boolean r0 = r7.A01
            if (r0 != 0) goto L_0x04fd
            int r0 = X.AnonymousClass1T7.A02(r7, r10)
            if (r0 != 0) goto L_0x04e4
            int r6 = r7.A00
            r1 = -2
            r0 = 0
            if (r6 == r1) goto L_0x04cd
            r0 = 1
        L_0x04cd:
            if (r0 == 0) goto L_0x04f8
            boolean r0 = r7.A01
            if (r0 != 0) goto L_0x04f8
            X.0yS r1 = X.AnonymousClass1T7.A00
            X.AnonymousClass1NY.A05(r10)
            int r0 = r10.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r1.contains(r0)
        L_0x04e2:
            if (r0 == 0) goto L_0x04fd
        L_0x04e4:
            r0 = 1
        L_0x04e5:
            if (r0 != 0) goto L_0x04f2
            X.1Q1 r1 = r4.A07
            X.36w r0 = r4.A06
            boolean r1 = r3.canResize(r10, r1, r0)
            r0 = 0
            if (r1 == 0) goto L_0x04f3
        L_0x04f2:
            r0 = 1
        L_0x04f3:
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.valueOf(r0)
            goto L_0x04b3
        L_0x04f8:
            r0 = 0
            r10.A00 = r0
            r0 = 0
            goto L_0x04e2
        L_0x04fd:
            r0 = 0
            goto L_0x04e5
        L_0x04ff:
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.UNSET
            goto L_0x04b3
        L_0x0502:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            if (r1 == r0) goto L_0x0559
            X.1O3 r0 = X.AnonymousClass1SI.A06
            if (r2 == r0) goto L_0x0536
            X.1O3 r0 = X.AnonymousClass1SI.A04
            if (r2 == r0) goto L_0x0536
            X.1QK r0 = r5.A01
            X.1Q0 r0 = r0.A09
            X.1Q1 r4 = r0.A07
            int r3 = r4.A00
            r2 = -1
            r0 = 0
            if (r3 != r2) goto L_0x051b
            r0 = 1
        L_0x051b:
            if (r0 != 0) goto L_0x0530
            r1 = -2
            r0 = 0
            if (r3 == r1) goto L_0x0522
            r0 = 1
        L_0x0522:
            if (r0 == 0) goto L_0x0530
            int r0 = r4.A00()
            X.1NY r10 = X.AnonymousClass1NY.A03(r10)
            if (r10 == 0) goto L_0x0530
            r10.A02 = r0
        L_0x0530:
            X.1Qb r0 = r5.A00
            r0.Bgg(r10, r11)
            return
        L_0x0536:
            X.1QK r0 = r5.A01
            X.1Q0 r0 = r0.A09
            X.1Q1 r0 = r0.A07
            boolean r0 = r0.A01
            if (r0 != 0) goto L_0x0530
            X.AnonymousClass1NY.A05(r10)
            int r0 = r10.A02
            if (r0 == 0) goto L_0x0530
            X.AnonymousClass1NY.A05(r10)
            int r1 = r10.A02
            r0 = -1
            if (r1 == r0) goto L_0x0530
            X.1NY r10 = X.AnonymousClass1NY.A03(r10)
            if (r10 == 0) goto L_0x0530
            r0 = 0
            r10.A02 = r0
            goto L_0x0530
        L_0x0559:
            X.1RU r0 = r5.A02
            boolean r0 = r0.A05(r10, r11)
            if (r0 == 0) goto L_0x06e3
            if (r8 != 0) goto L_0x056b
            X.1QK r0 = r5.A01
            boolean r0 = r0.A07()
            if (r0 == 0) goto L_0x06e3
        L_0x056b:
            X.1RU r0 = r5.A02
            r0.A04()
            return
        L_0x0571:
            r1 = r9
            X.1SR r1 = (X.AnonymousClass1SR) r1
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x06e3
            X.1Qb r1 = r1.A00
            r0 = 0
            r1.Bgg(r0, r11)
            return
        L_0x0581:
            r1 = r9
            X.1n9 r1 = (X.AnonymousClass1n9) r1
            X.1Qb r0 = r1.A00
            r0.Bgg(r10, r11)
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x06e3
            X.AnonymousClass1n9.A00(r1)
            return
        L_0x0593:
            r5 = r9
            X.1SQ r5 = (X.AnonymousClass1SQ) r5
            X.1NY r10 = (X.AnonymousClass1NY) r10
            if (r10 == 0) goto L_0x05b0
            boolean r0 = A03(r11)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x05aa
            X.36w r0 = r5.A01
            boolean r0 = X.C52892jr.A01(r10, r0)
            if (r0 == 0) goto L_0x05b0
        L_0x05aa:
            X.1Qb r0 = r5.A00
            r0.Bgg(r10, r11)
            return
        L_0x05b0:
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x06e3
            X.AnonymousClass1NY.A04(r10)
            X.2jo r4 = r5.A03
            int r3 = r5.A00
            r2 = 1
            int r3 = r3 + r2
            X.1Qb r1 = r5.A00
            X.1QK r0 = r5.A02
            boolean r0 = X.C52862jo.A00(r4, r3, r1, r0)
            if (r0 != 0) goto L_0x06e3
            X.1Qb r1 = r5.A00
            r0 = 0
            r1.Bgg(r0, r2)
            return
        L_0x05d0:
            r2 = r9
            X.1SN r2 = (X.AnonymousClass1SN) r2
            X.1NY r10 = (X.AnonymousClass1NY) r10
            com.facebook.common.util.TriState r1 = r2.A00
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r1 != r0) goto L_0x05fe
            if (r10 == 0) goto L_0x05fe
            X.C05520Zg.A02(r10)
            java.io.InputStream r0 = r10.A09()
            X.1O3 r1 = X.AnonymousClass1OG.A01(r0)
            boolean r0 = X.AnonymousClass1SI.A00(r1)
            if (r0 == 0) goto L_0x062f
            com.facebook.imagepipeline.nativecode.WebpTranscoderImpl r0 = X.AnonymousClass95I.A00
            if (r0 == 0) goto L_0x0636
            boolean r0 = r0.isWebpNativelySupported(r1)
            r0 = r0 ^ 1
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)
        L_0x05fc:
            r2.A00 = r0
        L_0x05fe:
            com.facebook.common.util.TriState r1 = r2.A00
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO
            if (r1 == r0) goto L_0x0639
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x06e3
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            if (r1 != r0) goto L_0x0639
            if (r10 == 0) goto L_0x0639
            X.2p5 r3 = r2.A02
            X.1Qb r4 = r2.A00
            X.1QK r6 = r2.A01
            X.C05520Zg.A02(r10)
            X.1NY r8 = X.AnonymousClass1NY.A03(r10)
            X.1R2 r2 = new X.1R2
            X.1MN r5 = r6.A07
            java.lang.String r7 = "WebpTranscodeProducer"
            r2.<init>(r3, r4, r5, r6, r7, r8)
            java.util.concurrent.Executor r1 = r3.A01
            r0 = -1752170623(0xffffffff978fff81, float:-9.305657E-25)
            X.AnonymousClass07A.A04(r1, r2, r0)
            return
        L_0x062f:
            X.1O3 r0 = X.AnonymousClass1O3.A02
            if (r1 != r0) goto L_0x0636
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            goto L_0x05fc
        L_0x0636:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO
            goto L_0x05fc
        L_0x0639:
            X.1Qb r0 = r2.A00
            r0.Bgg(r10, r11)
            return
        L_0x063f:
            r1 = r9
            X.1RL r1 = (X.AnonymousClass1RL) r1
            java.io.Closeable r10 = (java.io.Closeable) r10
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x064f
            java.lang.String r0 = "MultiplexProducer#onNewResult"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x06fa }
        L_0x064f:
            X.1R4 r5 = r1.A00     // Catch:{ all -> 0x06fa }
            monitor-enter(r5)     // Catch:{ all -> 0x06fa }
            X.1RL r0 = r5.A03     // Catch:{ all -> 0x06f3 }
            if (r0 == r1) goto L_0x0658
            monitor-exit(r5)     // Catch:{ all -> 0x06f3 }
            goto L_0x06cc
        L_0x0658:
            java.io.Closeable r0 = r5.A04     // Catch:{ all -> 0x06f3 }
            X.AnonymousClass1R4.A05(r0)     // Catch:{ all -> 0x06f3 }
            r6 = 0
            r5.A04 = r6     // Catch:{ all -> 0x06f3 }
            java.util.concurrent.CopyOnWriteArraySet r0 = r5.A06     // Catch:{ all -> 0x06f3 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x06f3 }
            boolean r0 = A03(r11)     // Catch:{ all -> 0x06f3 }
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x067a
            X.1Q9 r0 = r5.A07     // Catch:{ all -> 0x06f3 }
            java.io.Closeable r0 = r0.A00(r10)     // Catch:{ all -> 0x06f3 }
            r5.A04 = r0     // Catch:{ all -> 0x06f3 }
            r5.A01 = r11     // Catch:{ all -> 0x06f3 }
        L_0x0678:
            monitor-exit(r5)     // Catch:{ all -> 0x06f3 }
            goto L_0x0687
        L_0x067a:
            java.util.concurrent.CopyOnWriteArraySet r0 = r5.A06     // Catch:{ all -> 0x06f3 }
            r0.clear()     // Catch:{ all -> 0x06f3 }
            X.1Q9 r1 = r5.A07     // Catch:{ all -> 0x06f3 }
            java.lang.Object r0 = r5.A05     // Catch:{ all -> 0x06f3 }
            r1.A02(r0, r5)     // Catch:{ all -> 0x06f3 }
            goto L_0x0678
        L_0x0687:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x06fa }
            if (r0 == 0) goto L_0x06cc
            java.lang.Object r4 = r7.next()     // Catch:{ all -> 0x06fa }
            android.util.Pair r4 = (android.util.Pair) r4     // Catch:{ all -> 0x06fa }
            monitor-enter(r4)     // Catch:{ all -> 0x06fa }
            boolean r0 = A03(r11)     // Catch:{ all -> 0x06c9 }
            if (r0 == 0) goto L_0x06c0
            java.lang.Object r2 = r4.second     // Catch:{ all -> 0x06c9 }
            X.1QK r2 = (X.AnonymousClass1QK) r2     // Catch:{ all -> 0x06c9 }
            X.1MN r1 = r2.A07     // Catch:{ all -> 0x06c9 }
            X.1Q9 r0 = r5.A07     // Catch:{ all -> 0x06c9 }
            java.lang.String r0 = r0.A01     // Catch:{ all -> 0x06c9 }
            r1.Bk6(r2, r0, r6)     // Catch:{ all -> 0x06c9 }
            X.1QK r0 = r5.A02     // Catch:{ all -> 0x06c9 }
            if (r0 == 0) goto L_0x06c0
            java.lang.Object r3 = r4.second     // Catch:{ all -> 0x06c9 }
            X.1QK r3 = (X.AnonymousClass1QK) r3     // Catch:{ all -> 0x06c9 }
            r2 = 1
            android.util.SparseArray r1 = r0.A05     // Catch:{ all -> 0x06c9 }
            java.lang.String r0 = ""
            java.lang.Object r2 = r1.get(r2, r0)     // Catch:{ all -> 0x06c9 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x06c9 }
            r1 = 1
            android.util.SparseArray r0 = r3.A05     // Catch:{ all -> 0x06c9 }
            r0.put(r1, r2)     // Catch:{ all -> 0x06c9 }
        L_0x06c0:
            java.lang.Object r0 = r4.first     // Catch:{ all -> 0x06c9 }
            X.1Qb r0 = (X.C23581Qb) r0     // Catch:{ all -> 0x06c9 }
            r0.Bgg(r10, r11)     // Catch:{ all -> 0x06c9 }
            monitor-exit(r4)     // Catch:{ all -> 0x06c9 }
            goto L_0x0687
        L_0x06c9:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x06c9 }
            goto L_0x06f9
        L_0x06cc:
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x06e3
            X.AnonymousClass1NB.A01()
            return
        L_0x06d6:
            r1 = r9
            X.1Qa r1 = (X.C23571Qa) r1
            X.1NY r10 = (X.AnonymousClass1NY) r10
            if (r10 != 0) goto L_0x06e4
            X.1Qb r1 = r1.A00
            r0 = 0
            r1.Bgg(r0, r11)
        L_0x06e3:
            return
        L_0x06e4:
            boolean r0 = X.AnonymousClass1NY.A06(r10)
            if (r0 != 0) goto L_0x06ed
            r10.A0A()
        L_0x06ed:
            X.1Qb r0 = r1.A00
            r0.Bgg(r10, r11)
            return
        L_0x06f3:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x06f3 }
            goto L_0x06f9
        L_0x06f6:
            X.AnonymousClass1PS.A05(r4)     // Catch:{ all -> 0x06fa }
        L_0x06f9:
            throw r0     // Catch:{ all -> 0x06fa }
        L_0x06fa:
            r1 = move-exception
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x0704
            X.AnonymousClass1NB.A01()
        L_0x0704:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QU.A06(java.lang.Object, int):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public void A07(Throwable th) {
        AnonymousClass0d1 r4;
        if (this instanceof AnonymousClass1RL) {
            AnonymousClass1RL r1 = (AnonymousClass1RL) this;
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("MultiplexProducer#onFailure");
            }
            AnonymousClass1R4 r5 = r1.A00;
            synchronized (r5) {
                try {
                    if (r5.A03 == r1) {
                        Iterator it = r5.A06.iterator();
                        r5.A06.clear();
                        r5.A07.A02(r5.A05, r5);
                        AnonymousClass1R4.A05(r5.A04);
                        r5.A04 = null;
                        while (it.hasNext()) {
                            try {
                                Pair pair = (Pair) it.next();
                                synchronized (pair) {
                                    try {
                                        AnonymousClass1QK r2 = (AnonymousClass1QK) pair.second;
                                        r2.A07.Bk4(r2, r5.A07.A01, th, null);
                                        ((C23581Qb) pair.first).BYh(th);
                                    } catch (Throwable th2) {
                                        th = th2;
                                        throw th;
                                    }
                                }
                            } catch (Throwable th3) {
                                if (AnonymousClass1NB.A03()) {
                                    AnonymousClass1NB.A01();
                                }
                                throw th3;
                            }
                        }
                    }
                } catch (Throwable th4) {
                    while (true) {
                        th = th4;
                    }
                    throw th;
                }
            }
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
        } else if (!(this instanceof AnonymousClass1RO)) {
            AnonymousClass1QM.A01(((AnonymousClass1QQ) this).A00, th);
        } else {
            AnonymousClass1RO r42 = (AnonymousClass1RO) this;
            if (r42 instanceof AnonymousClass1SQ) {
                AnonymousClass1SQ r43 = (AnonymousClass1SQ) r42;
                boolean A002 = C52862jo.A00(r43.A03, r43.A00 + 1, r43.A00, r43.A02);
                r4 = r43;
                if (A002) {
                    return;
                }
            } else if (r42 instanceof AnonymousClass1n9) {
                AnonymousClass1n9 r44 = (AnonymousClass1n9) r42;
                r44.A00.BYh(th);
                AnonymousClass1n9.A00(r44);
                return;
            } else if (r42 instanceof AnonymousClass0d0) {
                AnonymousClass0d0 r45 = (AnonymousClass0d0) r42;
                boolean A003 = AnonymousClass0d0.A00(r45);
                r4 = r45;
                if (!A003) {
                    return;
                }
            } else if (r42 instanceof AnonymousClass0d1) {
                AnonymousClass0d1 r46 = (AnonymousClass0d1) r42;
                boolean A01 = AnonymousClass0d1.A01(r46);
                r4 = r46;
                if (!A01) {
                    return;
                }
            } else if (!(r42 instanceof AnonymousClass1RQ)) {
                boolean z = r42 instanceof C24221St;
                r4 = r42;
                if (z) {
                    C24221St r47 = (C24221St) r42;
                    r47.A01.A00.ByS(r47.A00, r47.A00);
                    return;
                }
            } else {
                AnonymousClass1RQ r48 = (AnonymousClass1RQ) r42;
                AnonymousClass1RQ.A01(r48, true);
                r48.A00.BYh(th);
                return;
            }
            r4.A00.BYh(th);
        }
    }

    public synchronized void BYh(Throwable th) {
        if (!this.A00) {
            this.A00 = true;
            try {
                A07(th);
            } catch (Exception e) {
                AnonymousClass02w.A05(getClass(), "unhandled exception", e);
            }
        }
        return;
    }

    public synchronized void Bgg(Object obj, int i) {
        if (!this.A00) {
            this.A00 = A03(i);
            try {
                A06(obj, i);
            } catch (Exception e) {
                AnonymousClass02w.A05(getClass(), "unhandled exception", e);
            }
        }
        return;
    }

    public synchronized void BkJ(float f) {
        if (!this.A00) {
            try {
                A05(f);
            } catch (Exception e) {
                AnonymousClass02w.A05(getClass(), "unhandled exception", e);
            }
        }
        return;
    }

    public synchronized void onCancellation() {
        if (!this.A00) {
            this.A00 = true;
            try {
                A04();
            } catch (Exception e) {
                AnonymousClass02w.A05(getClass(), "unhandled exception", e);
            }
        }
        return;
    }
}
