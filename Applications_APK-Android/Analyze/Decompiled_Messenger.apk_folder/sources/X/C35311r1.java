package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.common.util.TriState;

/* renamed from: X.1r1  reason: invalid class name and case insensitive filesystem */
public final class C35311r1 extends AnonymousClass1A9 {
    public final /* synthetic */ AnonymousClass1CV A00;

    public C35311r1(AnonymousClass1CV r1) {
        this.A00 = r1;
    }

    public void A07(RecyclerView recyclerView, int i) {
        AnonymousClass1CV r3 = this.A00;
        if (r3.A06 != null) {
            if (i == 1) {
                boolean z = false;
                if (r3.A03 != -1) {
                    z = true;
                }
                if (z) {
                    r3.A03 = -1;
                    r3.A02 = -1;
                }
            }
            if (i == 0 && r3.A09 == C10700ki.ALL) {
                AnonymousClass1CV.A08(r3, TriState.UNSET);
            }
        }
    }
}
