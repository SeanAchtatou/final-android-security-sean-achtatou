package X;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.facebook.ui.media.attachments.model.MediaResource;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

/* renamed from: X.1Di  reason: invalid class name and case insensitive filesystem */
public final class C20661Di extends C06020ai {
    public final /* synthetic */ C53882lW A00;

    public C20661Di(C53882lW r1) {
        this.A00 = r1;
    }

    public static void A00(C20661Di r7, MediaResource mediaResource) {
        C421828p r1 = mediaResource.A0L;
        if (r1 == C421828p.PHOTO) {
            Uri uri = mediaResource.A0D;
            try {
                long now = r7.A00.A0C.now();
                Locale locale = Locale.US;
                Long valueOf = Long.valueOf(now);
                String format = String.format(locale, "messenger-quick-cam-%d", valueOf);
                MediaStore.Images.Media.insertImage(r7.A00.getContext().getContentResolver(), uri.getPath(), format, (String) null);
                ContentValues contentValues = new ContentValues();
                contentValues.put("datetaken", valueOf);
                r7.A00.getContext().getContentResolver().update(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues, "title=?", new String[]{format});
            } catch (IOException unused) {
                r7.A00.A0V.A04(new C51092fR(2131831082));
            }
        } else if (r1 == C421828p.VIDEO) {
            Uri uri2 = mediaResource.A0D;
            try {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
                file.mkdirs();
                File file2 = new File(file, AnonymousClass08S.A0P("QuickCam_", Long.valueOf(r7.A00.A0C.now()).toString(), ".mp4"));
                AnonymousClass0q2.A04(new File(uri2.getPath()), file2);
                Intent intent = new Intent(C99084oO.$const$string(132));
                intent.setData(Uri.fromFile(file2));
                r7.A00.getContext().sendBroadcast(intent);
            } catch (IOException unused2) {
                r7.A00.A0V.A04(new C51092fR(2131831083));
            }
        }
    }
}
