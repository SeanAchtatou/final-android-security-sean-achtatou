package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableSet;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.13w  reason: invalid class name */
public final class AnonymousClass13w extends C008007h {
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final Class A08 = AnonymousClass13w.class;
    private static final AnonymousClass1Y7 A09;
    private static volatile AnonymousClass13w A0A;
    public final FbSharedPreferences A00;
    private final Context A01;
    private final AnonymousClass0ZM A02 = new AnonymousClass140(this);
    private final C185213x A03;
    public volatile AnonymousClass07i A04;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) ((AnonymousClass1Y7) C04350Ue.A06.A09("sandbox/")).A09("mqtt/");
        A09 = r1;
        A07 = (AnonymousClass1Y7) r1.A09("server_tier");
        AnonymousClass1Y7 r12 = A09;
        A06 = (AnonymousClass1Y7) r12.A09("sandbox");
        A05 = (AnonymousClass1Y7) r12.A09("delivery_sandbox");
    }

    public static final AnonymousClass13w A00(AnonymousClass1XY r5) {
        if (A0A == null) {
            synchronized (AnonymousClass13w.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0A = new AnonymousClass13w(applicationInjector, FbSharedPreferencesModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static JSONObject A01(AnonymousClass13w r4, AnonymousClass1Y7 r5) {
        String B4F = r4.A00.B4F(r5, BuildConfig.FLAVOR);
        JSONObject jSONObject = new JSONObject();
        if (C06850cB.A0B(B4F)) {
            return jSONObject;
        }
        try {
            return new JSONObject(B4F);
        } catch (JSONException e) {
            C010708t.A0F(A08, e, BuildConfig.FLAVOR, new Object[0]);
            return jSONObject;
        }
    }

    public AnonymousClass07i A03() {
        return this.A04;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public void A04() {
        int i;
        JSONObject A012 = A01(this, C10930l6.A02);
        A02(A012);
        String B4F = this.A00.B4F(A07, "default");
        String B4F2 = this.A00.B4F(A05, null);
        if ("sandbox".equals(B4F) || !C06850cB.A0B(B4F2)) {
            String B4F3 = this.A00.B4F(A06, null);
            if (!C06850cB.A0B(B4F3) || !C06850cB.A0B(B4F2)) {
                try {
                    if (!TextUtils.isEmpty(B4F3)) {
                        if (B4F3.contains(":")) {
                            String[] split = B4F3.split(":", 2);
                            B4F3 = split[0];
                            i = Integer.parseInt(split[1]);
                        } else {
                            i = AnonymousClass1Y3.BDO;
                        }
                        if (!TextUtils.isEmpty(B4F3)) {
                            A012.put("host_name_v6", B4F3);
                            A012.put("default_port", i);
                            A012.put("backup_port", i);
                            A012.put("use_ssl", false);
                            A012.put("use_compression", false);
                        }
                    }
                    if (!TextUtils.isEmpty(B4F2)) {
                        A012.put("php_sandbox_host_name", B4F2);
                    }
                } catch (Throwable th) {
                    C010708t.A0M("ConnectionConfigManager", "Failed to parse mqtt sandbox URL", th);
                }
            }
        }
        AnonymousClass07i r1 = new AnonymousClass07i(A012);
        if (!r1.equals(this.A04)) {
            this.A04 = r1;
        }
    }

    public void A05() {
        C009207y.A01.A09(this.A01, new Intent("com.facebook.rti.mqtt.ACTION_MQTT_CONFIG_CHANGED").setPackage(this.A01.getPackageName()));
    }

    private AnonymousClass13w(AnonymousClass1XY r5, FbSharedPreferences fbSharedPreferences) {
        this.A01 = AnonymousClass1YA.A00(r5);
        this.A03 = C185213x.A00(r5);
        this.A00 = fbSharedPreferences;
        this.A00.C0h(ImmutableSet.A07(A05, A07, A06, C10930l6.A02), this.A02);
        this.A04 = new AnonymousClass07i(new JSONObject());
        this.A00.C0e(new AnonymousClass141(this));
        this.A03.A00 = new AnonymousClass1EU(this);
    }
}
