package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Cc  reason: invalid class name and case insensitive filesystem */
public abstract class C01910Cc extends C01920Cd {
    public final Handler A00;

    public C01910Cc(Handler handler) {
        this.A00 = handler;
    }

    public Object get() {
        boolean z = false;
        if (Looper.myLooper() == this.A00.getLooper()) {
            z = true;
        }
        if (!z || isDone()) {
            return super.get();
        }
        throw new IllegalStateException("Must not call get() function from this Handler thread. Will deadlock!");
    }

    public Object get(long j, TimeUnit timeUnit) {
        boolean z = false;
        if (Looper.myLooper() == this.A00.getLooper()) {
            z = true;
        }
        if (!z || isDone()) {
            return super.get(j, timeUnit);
        }
        throw new IllegalStateException("Must not call get() function from this Handler thread. Will deadlock!");
    }
}
