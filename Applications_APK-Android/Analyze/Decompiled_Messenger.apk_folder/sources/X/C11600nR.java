package X;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatViewInflater;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;

/* renamed from: X.0nR  reason: invalid class name and case insensitive filesystem */
public final class C11600nR extends C11610nS implements C11640nX, LayoutInflater.Factory2 {
    public static final boolean A0i;
    public static final int[] A0j = {16842836};
    public int A00;
    public int A01 = -100;
    public int A02;
    public Context A03;
    public ViewGroup A04;
    public PopupWindow A05;
    public TextView A06;
    public C24367ByW A07;
    public C27630DgE A08;
    public C27968Dm9 A09;
    public C27969DmA A0A;
    public AppCompatViewInflater A0B;
    public C27962Dm1 A0C;
    public ActionBarContextView A0D;
    public C27998Dme A0E;
    public AnonymousClass1L6 A0F = null;
    public CharSequence A0G;
    public Runnable A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    private Rect A0V;
    private Rect A0W;
    private View A0X;
    private C27979DmL A0Y;
    private boolean A0Z;
    private boolean A0a;
    private boolean A0b;
    private C27968Dm9[] A0c;
    public final Context A0d;
    public final Window.Callback A0e;
    public final Window A0f;
    public final C73143fZ A0g;
    public final Runnable A0h = new C1756688r(this);

    public void A0W(int i) {
        C27968Dm9 A0U2;
        C27968Dm9 A0U3 = A0U(i);
        if (A0U3.A0A != null) {
            Bundle bundle = new Bundle();
            A0U3.A0A.A0B(bundle);
            if (bundle.size() > 0) {
                A0U3.A00 = bundle;
            }
            C27933DlV dlV = A0U3.A0A;
            dlV.A09();
            dlV.clear();
        }
        A0U3.A0F = true;
        A0U3.A0E = true;
        if ((i == 108 || i == 0) && this.A0E != null && (A0U2 = A0U(0)) != null) {
            A0U2.A0D = false;
            A07(this, A0U2, null);
        }
    }

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT < 21) {
            z = true;
        }
        A0i = z;
        if (z) {
            Thread.setDefaultUncaughtExceptionHandler(new C1926990v(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }

    private void A00() {
        if (this.A08 == null) {
            Context A0T2 = A0T();
            if (C27147DUp.A03 == null) {
                Context applicationContext = A0T2.getApplicationContext();
                C27147DUp.A03 = new C27147DUp(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
            }
            this.A08 = new C27630DgE(this, C27147DUp.A03);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0067, code lost:
        if (r0.width != -1) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x013a, code lost:
        if (r3.A06.getCount() > 0) goto L_0x013c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0192, code lost:
        if (r0 != null) goto L_0x011c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A01(X.C27968Dm9 r13, android.view.KeyEvent r14) {
        /*
            r12 = this;
            boolean r0 = r13.A0C
            if (r0 != 0) goto L_0x0025
            boolean r0 = r12.A0O
            if (r0 != 0) goto L_0x0025
            int r0 = r13.A02
            r4 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0026
            android.content.Context r0 = r12.A0T()
            android.content.res.Resources r0 = r0.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r0 = r0.screenLayout
            r3 = r0 & 15
            r1 = 4
            r0 = 0
            if (r3 != r1) goto L_0x0023
            r0 = 1
        L_0x0023:
            if (r0 == 0) goto L_0x0026
        L_0x0025:
            return
        L_0x0026:
            android.view.Window r0 = r12.A0f
            android.view.Window$Callback r3 = r0.getCallback()
            if (r3 == 0) goto L_0x003c
            int r1 = r13.A02
            X.DlV r0 = r13.A0A
            boolean r0 = r3.onMenuOpened(r1, r0)
            if (r0 != 0) goto L_0x003c
            r12.A0Y(r13, r2)
            return
        L_0x003c:
            android.content.Context r1 = r12.A0T()
            java.lang.String r0 = "window"
            java.lang.Object r1 = r1.getSystemService(r0)
            android.view.WindowManager r1 = (android.view.WindowManager) r1
            if (r1 == 0) goto L_0x0025
            boolean r0 = A07(r12, r13, r14)
            if (r0 == 0) goto L_0x0025
            android.view.ViewGroup r5 = r13.A08
            r3 = -1
            r6 = -2
            if (r5 == 0) goto L_0x0088
            boolean r0 = r13.A0E
            if (r0 != 0) goto L_0x0088
            android.view.View r0 = r13.A06
            if (r0 == 0) goto L_0x0069
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            if (r0 == 0) goto L_0x0069
            int r0 = r0.width
            r5 = -1
            if (r0 == r3) goto L_0x006a
        L_0x0069:
            r5 = -2
        L_0x006a:
            r13.A0B = r4
            android.view.WindowManager$LayoutParams r4 = new android.view.WindowManager$LayoutParams
            r7 = 0
            r8 = 0
            r9 = 1002(0x3ea, float:1.404E-42)
            r10 = 8519680(0x820000, float:1.1938615E-38)
            r11 = -3
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
            int r0 = r13.A03
            r4.gravity = r0
            int r0 = r13.A04
            r4.windowAnimations = r0
            android.view.ViewGroup r0 = r13.A08
            r1.addView(r0, r4)
            r13.A0C = r2
            return
        L_0x0088:
            if (r5 != 0) goto L_0x0106
            X.ByW r0 = r12.A09()
            if (r0 == 0) goto L_0x0104
            android.content.Context r10 = r0.A03()
        L_0x0094:
            if (r10 != 0) goto L_0x009a
            android.content.Context r10 = r12.A0T()
        L_0x009a:
            android.util.TypedValue r3 = new android.util.TypedValue
            r3.<init>()
            android.content.res.Resources r0 = r10.getResources()
            android.content.res.Resources$Theme r9 = r0.newTheme()
            android.content.res.Resources$Theme r0 = r10.getTheme()
            r9.setTo(r0)
            r0 = 2130968601(0x7f040019, float:1.754586E38)
            r9.resolveAttribute(r0, r3, r2)
            int r0 = r3.resourceId
            if (r0 == 0) goto L_0x00bb
            r9.applyStyle(r0, r2)
        L_0x00bb:
            r0 = 2130970164(0x7f040634, float:1.754903E38)
            r9.resolveAttribute(r0, r3, r2)
            int r0 = r3.resourceId
            if (r0 == 0) goto L_0x00fd
            r9.applyStyle(r0, r2)
        L_0x00c8:
            X.3EJ r3 = new X.3EJ
            r3.<init>(r10, r4)
            android.content.res.Resources$Theme r0 = r3.getTheme()
            r0.setTo(r9)
            r13.A05 = r3
            int[] r0 = X.C22551Lw.A0C
            android.content.res.TypedArray r3 = r3.obtainStyledAttributes(r0)
            r0 = 82
            int r0 = r3.getResourceId(r0, r4)
            r13.A01 = r0
            int r0 = r3.getResourceId(r2, r4)
            r13.A04 = r0
            r3.recycle()
            X.Dm7 r3 = new X.Dm7
            android.content.Context r0 = r13.A05
            r3.<init>(r12, r0)
            r13.A08 = r3
            r0 = 81
            r13.A03 = r0
            if (r3 != 0) goto L_0x0115
            return
        L_0x00fd:
            r0 = 2132476904(0x7f1b03e8, float:2.0605062E38)
            r9.applyStyle(r0, r2)
            goto L_0x00c8
        L_0x0104:
            r10 = 0
            goto L_0x0094
        L_0x0106:
            boolean r0 = r13.A0E
            if (r0 == 0) goto L_0x0115
            int r0 = r5.getChildCount()
            if (r0 <= 0) goto L_0x0115
            android.view.ViewGroup r0 = r13.A08
            r0.removeAllViews()
        L_0x0115:
            android.view.View r0 = r13.A06
            r9 = 1
            if (r0 == 0) goto L_0x017a
            r13.A07 = r0
        L_0x011c:
            if (r9 == 0) goto L_0x0025
            android.view.View r0 = r13.A07
            r5 = 0
            if (r0 == 0) goto L_0x013d
            android.view.View r0 = r13.A06
            if (r0 != 0) goto L_0x013c
            X.DgI r3 = r13.A09
            X.DgK r0 = r3.A06
            if (r0 != 0) goto L_0x0134
            X.DgK r0 = new X.DgK
            r0.<init>(r3)
            r3.A06 = r0
        L_0x0134:
            X.DgK r0 = r3.A06
            int r0 = r0.getCount()
            if (r0 <= 0) goto L_0x013d
        L_0x013c:
            r5 = 1
        L_0x013d:
            if (r5 == 0) goto L_0x0025
            android.view.View r0 = r13.A07
            android.view.ViewGroup$LayoutParams r5 = r0.getLayoutParams()
            if (r5 != 0) goto L_0x014c
            android.view.ViewGroup$LayoutParams r5 = new android.view.ViewGroup$LayoutParams
            r5.<init>(r6, r6)
        L_0x014c:
            int r3 = r13.A01
            android.view.ViewGroup r0 = r13.A08
            r0.setBackgroundResource(r3)
            android.view.View r0 = r13.A07
            android.view.ViewParent r3 = r0.getParent()
            boolean r0 = r3 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x0164
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3
            android.view.View r0 = r13.A07
            r3.removeView(r0)
        L_0x0164:
            android.view.ViewGroup r3 = r13.A08
            android.view.View r0 = r13.A07
            r3.addView(r0, r5)
            android.view.View r0 = r13.A07
            boolean r0 = r0.hasFocus()
            if (r0 != 0) goto L_0x0069
            android.view.View r0 = r13.A07
            r0.requestFocus()
            goto L_0x0069
        L_0x017a:
            X.DlV r3 = r13.A0A
            if (r3 == 0) goto L_0x01de
            X.DmA r0 = r12.A0A
            if (r0 != 0) goto L_0x0189
            X.DmA r0 = new X.DmA
            r0.<init>(r12)
            r12.A0A = r0
        L_0x0189:
            X.DmA r7 = r12.A0A
            if (r3 != 0) goto L_0x0195
            r0 = 0
        L_0x018e:
            android.view.View r0 = (android.view.View) r0
            r13.A07 = r0
            if (r0 == 0) goto L_0x01de
            goto L_0x011c
        L_0x0195:
            X.DgI r0 = r13.A09
            if (r0 != 0) goto L_0x01af
            X.DgI r5 = new X.DgI
            android.content.Context r3 = r13.A05
            r0 = 2132410380(0x7f1a000c, float:2.0470135E38)
            r5.<init>(r3, r0)
            r13.A09 = r5
            r5.C6Y(r7)
            X.DlV r3 = r13.A0A
            android.content.Context r0 = r3.A0M
            r3.A0E(r5, r0)
        L_0x01af:
            X.DgI r7 = r13.A09
            android.view.ViewGroup r8 = r13.A08
            androidx.appcompat.view.menu.ExpandedMenuView r0 = r7.A05
            if (r0 != 0) goto L_0x01db
            android.view.LayoutInflater r5 = r7.A04
            r3 = 2132410377(0x7f1a0009, float:2.047013E38)
            android.view.View r0 = r5.inflate(r3, r8, r4)
            androidx.appcompat.view.menu.ExpandedMenuView r0 = (androidx.appcompat.view.menu.ExpandedMenuView) r0
            r7.A05 = r0
            X.DgK r0 = r7.A06
            if (r0 != 0) goto L_0x01cf
            X.DgK r0 = new X.DgK
            r0.<init>(r7)
            r7.A06 = r0
        L_0x01cf:
            androidx.appcompat.view.menu.ExpandedMenuView r3 = r7.A05
            X.DgK r0 = r7.A06
            r3.setAdapter(r0)
            androidx.appcompat.view.menu.ExpandedMenuView r0 = r7.A05
            r0.setOnItemClickListener(r7)
        L_0x01db:
            androidx.appcompat.view.menu.ExpandedMenuView r0 = r7.A05
            goto L_0x018e
        L_0x01de:
            r9 = 0
            goto L_0x011c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11600nR.A01(X.Dm9, android.view.KeyEvent):void");
    }

    public static void A02(C11600nR r8) {
        ViewGroup viewGroup;
        CharSequence charSequence;
        if (!r8.A0T) {
            Context A0T2 = r8.A0T();
            TypedArray obtainStyledAttributes = A0T2.obtainStyledAttributes(C22551Lw.A0C);
            if (obtainStyledAttributes.hasValue(AnonymousClass1Y3.A0q)) {
                if (obtainStyledAttributes.getBoolean(AnonymousClass1Y3.A0v, false)) {
                    r8.A0Q(1);
                } else if (obtainStyledAttributes.getBoolean(AnonymousClass1Y3.A0q, false)) {
                    r8.A0Q(108);
                }
                if (obtainStyledAttributes.getBoolean(114, false)) {
                    r8.A0Q(AnonymousClass1Y3.A0o);
                }
                if (obtainStyledAttributes.getBoolean(115, false)) {
                    r8.A0Q(10);
                }
                r8.A0P = obtainStyledAttributes.getBoolean(0, false);
                obtainStyledAttributes.recycle();
                r8.A0f.getDecorView();
                LayoutInflater cloneInContext = LayoutInflater.from(r8.A0d).cloneInContext(A0T2);
                if (r8.A0U) {
                    int i = 2132410385;
                    if (r8.A0S) {
                        i = 2132410386;
                    }
                    viewGroup = (ViewGroup) cloneInContext.inflate(i, (ViewGroup) null);
                    if (Build.VERSION.SDK_INT >= 21) {
                        C15320v6.setOnApplyWindowInsetsListener(viewGroup, new C1756588q(r8));
                    } else {
                        ((AnonymousClass88J) viewGroup).CA0(new C38091ws(r8));
                    }
                } else if (r8.A0P) {
                    viewGroup = (ViewGroup) cloneInContext.inflate(2132410376, (ViewGroup) null);
                    r8.A0R = false;
                    r8.A0M = false;
                } else if (r8.A0M) {
                    TypedValue typedValue = new TypedValue();
                    A0T2.getTheme().resolveAttribute(2130968608, typedValue, true);
                    int i2 = typedValue.resourceId;
                    if (i2 != 0) {
                        A0T2 = new AnonymousClass3EJ(A0T2, i2);
                    }
                    viewGroup = (ViewGroup) LayoutInflater.from(A0T2).inflate(2132410387, (ViewGroup) null);
                    C27998Dme dme = (C27998Dme) viewGroup.findViewById(2131297569);
                    r8.A0E = dme;
                    dme.CDL(r8.A0f.getCallback());
                    if (r8.A0R) {
                        r8.A0E.BCi(AnonymousClass1Y3.A0o);
                    }
                    if (r8.A0L) {
                        r8.A0E.BCi(2);
                    }
                    if (r8.A0K) {
                        r8.A0E.BCi(5);
                    }
                } else {
                    viewGroup = null;
                }
                if (viewGroup != null) {
                    if (r8.A0E == null) {
                        r8.A06 = (TextView) viewGroup.findViewById(2131301101);
                    }
                    C27761DiW.A00(viewGroup);
                    ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(2131296322);
                    ViewGroup viewGroup2 = (ViewGroup) r8.A0f.findViewById(16908290);
                    if (viewGroup2 != null) {
                        while (viewGroup2.getChildCount() > 0) {
                            View childAt = viewGroup2.getChildAt(0);
                            viewGroup2.removeViewAt(0);
                            contentFrameLayout.addView(childAt);
                        }
                        viewGroup2.setId(-1);
                        contentFrameLayout.setId(16908290);
                        if (viewGroup2 instanceof FrameLayout) {
                            ((FrameLayout) viewGroup2).setForeground(null);
                        }
                    }
                    r8.A0f.setContentView(viewGroup);
                    contentFrameLayout.A06 = new C27955Dlu(r8);
                    r8.A04 = viewGroup;
                    Window.Callback callback = r8.A0e;
                    if (callback instanceof Activity) {
                        charSequence = ((Activity) callback).getTitle();
                    } else {
                        charSequence = r8.A0G;
                    }
                    if (!TextUtils.isEmpty(charSequence)) {
                        C27998Dme dme2 = r8.A0E;
                        if (dme2 != null) {
                            dme2.CDM(charSequence);
                        } else {
                            C24367ByW byW = r8.A07;
                            if (byW != null) {
                                byW.A0I(charSequence);
                            } else {
                                TextView textView = r8.A06;
                                if (textView != null) {
                                    textView.setText(charSequence);
                                }
                            }
                        }
                    }
                    ContentFrameLayout contentFrameLayout2 = (ContentFrameLayout) r8.A04.findViewById(16908290);
                    View decorView = r8.A0f.getDecorView();
                    contentFrameLayout2.A07.set(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
                    if (C15320v6.isLaidOut(contentFrameLayout2)) {
                        contentFrameLayout2.requestLayout();
                    }
                    TypedArray obtainStyledAttributes2 = r8.A0T().obtainStyledAttributes(C22551Lw.A0C);
                    if (contentFrameLayout2.A04 == null) {
                        contentFrameLayout2.A04 = new TypedValue();
                    }
                    obtainStyledAttributes2.getValue(AnonymousClass1Y3.A0t, contentFrameLayout2.A04);
                    if (contentFrameLayout2.A05 == null) {
                        contentFrameLayout2.A05 = new TypedValue();
                    }
                    obtainStyledAttributes2.getValue(AnonymousClass1Y3.A0u, contentFrameLayout2.A05);
                    if (obtainStyledAttributes2.hasValue(118)) {
                        if (contentFrameLayout2.A02 == null) {
                            contentFrameLayout2.A02 = new TypedValue();
                        }
                        obtainStyledAttributes2.getValue(118, contentFrameLayout2.A02);
                    }
                    if (obtainStyledAttributes2.hasValue(AnonymousClass1Y3.A0s)) {
                        if (contentFrameLayout2.A03 == null) {
                            contentFrameLayout2.A03 = new TypedValue();
                        }
                        obtainStyledAttributes2.getValue(AnonymousClass1Y3.A0s, contentFrameLayout2.A03);
                    }
                    if (obtainStyledAttributes2.hasValue(116)) {
                        if (contentFrameLayout2.A00 == null) {
                            contentFrameLayout2.A00 = new TypedValue();
                        }
                        obtainStyledAttributes2.getValue(116, contentFrameLayout2.A00);
                    }
                    if (obtainStyledAttributes2.hasValue(AnonymousClass1Y3.A0r)) {
                        if (contentFrameLayout2.A01 == null) {
                            contentFrameLayout2.A01 = new TypedValue();
                        }
                        obtainStyledAttributes2.getValue(AnonymousClass1Y3.A0r, contentFrameLayout2.A01);
                    }
                    obtainStyledAttributes2.recycle();
                    contentFrameLayout2.requestLayout();
                    r8.A0T = true;
                    C27968Dm9 A0U2 = r8.A0U(0);
                    if (r8.A0O) {
                        return;
                    }
                    if (A0U2 == null || A0U2.A0A == null) {
                        A04(r8, 108);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + r8.A0M + ", windowActionBarOverlay: " + r8.A0R + ", android:windowIsFloating: " + r8.A0P + ", windowActionModeOverlay: " + r8.A0S + ", windowNoTitle: " + r8.A0U + " }");
            }
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
    }

    public static void A03(C11600nR r1) {
        if (r1.A0T) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    public static void A04(C11600nR r3, int i) {
        r3.A00 = (1 << i) | r3.A00;
        if (!r3.A0N) {
            C15320v6.postOnAnimation(r3.A0f.getDecorView(), r3.A0h);
            r3.A0N = true;
        }
    }

    private boolean A05() {
        if (!this.A0a) {
            Context context = this.A0d;
            if (context instanceof Activity) {
                PackageManager packageManager = context.getPackageManager();
                try {
                    Context context2 = this.A0d;
                    boolean z = false;
                    if ((packageManager.getActivityInfo(new ComponentName(context2, context2.getClass()), 0).configChanges & 512) != 0) {
                        z = true;
                    }
                    this.A0Z = z;
                } catch (PackageManager.NameNotFoundException unused) {
                    this.A0Z = false;
                }
            }
        }
        this.A0a = true;
        return this.A0Z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r1 == 108) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A07(X.C11600nR r11, X.C27968Dm9 r12, android.view.KeyEvent r13) {
        /*
            boolean r0 = r11.A0O
            r2 = 0
            if (r0 != 0) goto L_0x0148
            boolean r0 = r12.A0D
            r3 = 1
            if (r0 == 0) goto L_0x000b
            return r3
        L_0x000b:
            X.Dm9 r0 = r11.A09
            if (r0 == 0) goto L_0x0014
            if (r0 == r12) goto L_0x0014
            r11.A0Y(r0, r2)
        L_0x0014:
            android.view.Window r0 = r11.A0f
            android.view.Window$Callback r5 = r0.getCallback()
            if (r5 == 0) goto L_0x0024
            int r0 = r12.A02
            android.view.View r0 = r5.onCreatePanelView(r0)
            r12.A06 = r0
        L_0x0024:
            int r1 = r12.A02
            if (r1 == 0) goto L_0x002d
            r0 = 108(0x6c, float:1.51E-43)
            r10 = 0
            if (r1 != r0) goto L_0x002e
        L_0x002d:
            r10 = 1
        L_0x002e:
            if (r10 == 0) goto L_0x0037
            X.Dme r0 = r11.A0E
            if (r0 == 0) goto L_0x0037
            r0.C9U()
        L_0x0037:
            android.view.View r0 = r12.A06
            if (r0 != 0) goto L_0x013f
            if (r10 == 0) goto L_0x0043
            X.ByW r0 = r11.A07
            boolean r0 = r0 instanceof X.C24112BtU
            if (r0 != 0) goto L_0x013f
        L_0x0043:
            X.DlV r1 = r12.A0A
            r4 = 0
            if (r1 == 0) goto L_0x004c
            boolean r0 = r12.A0F
            if (r0 == 0) goto L_0x00fa
        L_0x004c:
            if (r1 != 0) goto L_0x00c4
            android.content.Context r6 = r11.A0T()
            int r1 = r12.A02
            if (r1 == 0) goto L_0x005a
            r0 = 108(0x6c, float:1.51E-43)
            if (r1 != r0) goto L_0x00ad
        L_0x005a:
            X.Dme r0 = r11.A0E
            if (r0 == 0) goto L_0x00ad
            android.util.TypedValue r9 = new android.util.TypedValue
            r9.<init>()
            android.content.res.Resources$Theme r1 = r6.getTheme()
            r0 = 2130968608(0x7f040020, float:1.7545874E38)
            r1.resolveAttribute(r0, r9, r3)
            r7 = 0
            int r0 = r9.resourceId
            if (r0 == 0) goto L_0x00bd
            android.content.res.Resources r0 = r6.getResources()
            android.content.res.Resources$Theme r7 = r0.newTheme()
            r7.setTo(r1)
            int r0 = r9.resourceId
            r7.applyStyle(r0, r3)
            r0 = 2130968609(0x7f040021, float:1.7545876E38)
            r7.resolveAttribute(r0, r9, r3)
        L_0x0088:
            int r0 = r9.resourceId
            if (r0 == 0) goto L_0x009e
            if (r7 != 0) goto L_0x0099
            android.content.res.Resources r0 = r6.getResources()
            android.content.res.Resources$Theme r7 = r0.newTheme()
            r7.setTo(r1)
        L_0x0099:
            int r0 = r9.resourceId
            r7.applyStyle(r0, r3)
        L_0x009e:
            if (r7 == 0) goto L_0x00ad
            X.3EJ r1 = new X.3EJ
            r1.<init>(r6, r2)
            android.content.res.Resources$Theme r0 = r1.getTheme()
            r0.setTo(r7)
            r6 = r1
        L_0x00ad:
            X.DlV r0 = new X.DlV
            r0.<init>(r6)
            r0.A0C(r11)
            r12.A00(r0)
            X.DlV r0 = r12.A0A
            if (r0 != 0) goto L_0x00c4
            return r2
        L_0x00bd:
            r0 = 2130968609(0x7f040021, float:1.7545876E38)
            r1.resolveAttribute(r0, r9, r3)
            goto L_0x0088
        L_0x00c4:
            if (r10 == 0) goto L_0x00dc
            X.Dme r6 = r11.A0E
            if (r6 == 0) goto L_0x00dc
            X.DmL r0 = r11.A0Y
            if (r0 != 0) goto L_0x00d5
            X.DmL r0 = new X.DmL
            r0.<init>(r11)
            r11.A0Y = r0
        L_0x00d5:
            X.DlV r1 = r12.A0A
            X.DmL r0 = r11.A0Y
            r6.C9T(r1, r0)
        L_0x00dc:
            X.DlV r1 = r12.A0A
            r1.A09()
            int r0 = r12.A02
            boolean r0 = r5.onCreatePanelMenu(r0, r1)
            if (r0 != 0) goto L_0x00f8
            r12.A00(r4)
            if (r10 == 0) goto L_0x0148
            X.Dme r1 = r11.A0E
            if (r1 == 0) goto L_0x0148
            X.DmL r0 = r11.A0Y
            r1.C9T(r4, r0)
            return r2
        L_0x00f8:
            r12.A0F = r2
        L_0x00fa:
            X.DlV r1 = r12.A0A
            r1.A09()
            android.os.Bundle r0 = r12.A00
            if (r0 == 0) goto L_0x0108
            r1.A0A(r0)
            r12.A00 = r4
        L_0x0108:
            android.view.View r1 = r12.A06
            X.DlV r0 = r12.A0A
            boolean r0 = r5.onPreparePanel(r2, r1, r0)
            if (r0 != 0) goto L_0x0123
            if (r10 == 0) goto L_0x011d
            X.Dme r1 = r11.A0E
            if (r1 == 0) goto L_0x011d
            X.DmL r0 = r11.A0Y
            r1.C9T(r4, r0)
        L_0x011d:
            X.DlV r0 = r12.A0A
            r0.A08()
            return r2
        L_0x0123:
            if (r13 == 0) goto L_0x0146
            int r0 = r13.getDeviceId()
        L_0x0129:
            android.view.KeyCharacterMap r0 = android.view.KeyCharacterMap.load(r0)
            int r0 = r0.getKeyboardType()
            r1 = 0
            if (r0 == r3) goto L_0x0135
            r1 = 1
        L_0x0135:
            X.DlV r0 = r12.A0A
            r0.setQwertyMode(r1)
            X.DlV r0 = r12.A0A
            r0.A08()
        L_0x013f:
            r12.A0D = r3
            r12.A0B = r2
            r11.A09 = r12
            return r3
        L_0x0146:
            r0 = -1
            goto L_0x0129
        L_0x0148:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11600nR.A07(X.0nR, X.Dm9, android.view.KeyEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0082, code lost:
        if (r0 != false) goto L_0x0084;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0135  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0R() {
        /*
            r10 = this;
            int r3 = r10.A01
            r0 = -100
            if (r3 != r0) goto L_0x0007
            r3 = -1
        L_0x0007:
            if (r3 == r0) goto L_0x014e
            if (r3 == 0) goto L_0x0138
            r0 = r3
        L_0x000c:
            r5 = 0
            r1 = -1
            if (r0 == r1) goto L_0x0135
            android.content.Context r1 = r10.A0d
            android.content.res.Resources r7 = r1.getResources()
            android.content.res.Configuration r9 = r7.getConfiguration()
            int r1 = r9.uiMode
            r4 = r1 & 48
            r1 = 2
            r8 = 16
            if (r0 != r1) goto L_0x0025
            r8 = 32
        L_0x0025:
            r2 = 0
            if (r4 == r8) goto L_0x0132
            boolean r6 = r10.A05()
            boolean r1 = r10.A0I
            if (r1 == 0) goto L_0x0039
            android.content.Context r1 = r10.A0d
            boolean r1 = r1 instanceof android.app.Activity
            if (r1 == 0) goto L_0x0039
            if (r6 != 0) goto L_0x0039
            r2 = 1
        L_0x0039:
            if (r2 == 0) goto L_0x010b
            android.content.Context r1 = r10.A0d
            android.app.Activity r1 = (android.app.Activity) r1
            r1.recreate()
        L_0x0042:
            r8 = 1
        L_0x0043:
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 17
            if (r2 < r1) goto L_0x0085
            android.content.Context r1 = r10.A03
            r7 = 0
            if (r1 == 0) goto L_0x0108
            android.content.res.Resources r1 = r1.getResources()
            android.content.res.Configuration r1 = r1.getConfiguration()
            int r1 = r1.uiMode
            r6 = r1 & 48
        L_0x005a:
            r1 = 2
            r4 = 16
            if (r0 != r1) goto L_0x0061
            r4 = 32
        L_0x0061:
            if (r6 == r4) goto L_0x0105
            boolean r1 = r10.A05()
            boolean r0 = r10.A0I
            if (r0 == 0) goto L_0x0074
            android.content.Context r0 = r10.A0d
            boolean r0 = r0 instanceof android.app.Activity
            if (r0 == 0) goto L_0x0074
            if (r1 != 0) goto L_0x0074
            r7 = 1
        L_0x0074:
            if (r7 == 0) goto L_0x00db
            android.content.Context r0 = r10.A0d
            android.app.Activity r0 = (android.app.Activity) r0
            r0.recreate()
        L_0x007d:
            r0 = 1
        L_0x007e:
            if (r0 != 0) goto L_0x0084
            r0 = r8
            r8 = 0
            if (r0 == 0) goto L_0x0085
        L_0x0084:
            r8 = 1
        L_0x0085:
            if (r3 != 0) goto L_0x00c3
            r10.A00()
            X.DgE r3 = r10.A08
            r3.A00()
            android.content.BroadcastReceiver r0 = r3.A00
            if (r0 != 0) goto L_0x009a
            X.DgC r0 = new X.DgC
            r0.<init>(r3)
            r3.A00 = r0
        L_0x009a:
            android.content.IntentFilter r0 = r3.A01
            if (r0 != 0) goto L_0x00b8
            android.content.IntentFilter r1 = new android.content.IntentFilter
            r1.<init>()
            r3.A01 = r1
            java.lang.String r0 = "android.intent.action.TIME_SET"
            r1.addAction(r0)
            android.content.IntentFilter r1 = r3.A01
            java.lang.String r0 = "android.intent.action.TIMEZONE_CHANGED"
            r1.addAction(r0)
            android.content.IntentFilter r1 = r3.A01
            java.lang.String r0 = "android.intent.action.TIME_TICK"
            r1.addAction(r0)
        L_0x00b8:
            X.0nR r0 = r3.A04
            android.content.Context r2 = r0.A0d
            android.content.BroadcastReceiver r1 = r3.A00
            android.content.IntentFilter r0 = r3.A01
            r2.registerReceiver(r1, r0)
        L_0x00c3:
            r0 = 1
            r10.A0I = r0
            if (r8 == 0) goto L_0x00da
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 < r0) goto L_0x00d3
            android.content.Context r0 = r10.A0d
            r0.setTheme(r5)
        L_0x00d3:
            android.content.Context r1 = r10.A0d
            int r0 = r10.A02
            r1.setTheme(r0)
        L_0x00da:
            return r8
        L_0x00db:
            android.content.res.Configuration r1 = new android.content.res.Configuration
            android.content.Context r0 = r10.A0d
            android.content.res.Resources r0 = r0.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            r1.<init>(r0)
            int r0 = r1.uiMode
            r0 = r0 & -49
            r4 = r4 | r0
            r1.uiMode = r4
            android.content.Context r0 = r10.A0d
            android.content.Context r1 = r0.createConfigurationContext(r1)
            int r0 = r10.A02
            r1.setTheme(r0)
            X.9HC r0 = new X.9HC
            r0.<init>(r10, r1)
            r10.A03 = r0
            goto L_0x007d
        L_0x0105:
            r0 = 0
            goto L_0x007e
        L_0x0108:
            r6 = 0
            goto L_0x005a
        L_0x010b:
            android.content.res.Configuration r4 = new android.content.res.Configuration
            r4.<init>(r9)
            int r1 = r4.uiMode
            r1 = r1 & -49
            r8 = r8 | r1
            r4.uiMode = r8
            android.util.DisplayMetrics r1 = r7.getDisplayMetrics()
            r7.updateConfiguration(r4, r1)
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 26
            if (r2 >= r1) goto L_0x0127
            androidx.appcompat.app.ResourcesFlusher.flush(r7)
        L_0x0127:
            if (r6 == 0) goto L_0x0042
            android.content.Context r1 = r10.A0d
            android.app.Activity r1 = (android.app.Activity) r1
            r1.onConfigurationChanged(r4)
            goto L_0x0042
        L_0x0132:
            r8 = 0
            goto L_0x0043
        L_0x0135:
            r8 = 0
            goto L_0x0085
        L_0x0138:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 < r0) goto L_0x0151
            android.content.Context r1 = r10.A0d
            java.lang.Class<android.app.UiModeManager> r0 = android.app.UiModeManager.class
            java.lang.Object r0 = r1.getSystemService(r0)
            android.app.UiModeManager r0 = (android.app.UiModeManager) r0
            int r0 = r0.getNightMode()
            if (r0 != 0) goto L_0x0151
        L_0x014e:
            r0 = -1
            goto L_0x000c
        L_0x0151:
            r10.A00()
            X.DgE r2 = r10.A08
            X.DUp r0 = r2.A02
            boolean r1 = r0.A01()
            r2.A03 = r1
            r0 = 1
            if (r1 == 0) goto L_0x000c
            r0 = 2
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11600nR.A0R():boolean");
    }

    public int A0S(int i) {
        boolean z;
        boolean z2;
        boolean z3;
        ActionBarContextView actionBarContextView = this.A0D;
        int i2 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A0D.getLayoutParams();
            z = true;
            if (this.A0D.isShown()) {
                if (this.A0V == null) {
                    this.A0V = new Rect();
                    this.A0W = new Rect();
                }
                Rect rect = this.A0V;
                Rect rect2 = this.A0W;
                rect.set(0, i, 0, 0);
                C27761DiW.A01(this.A04, rect, rect2);
                int i3 = rect2.top;
                int i4 = 0;
                if (i3 == 0) {
                    i4 = i;
                }
                if (marginLayoutParams.topMargin != i4) {
                    marginLayoutParams.topMargin = i;
                    View view = this.A0X;
                    if (view == null) {
                        Context A0T2 = A0T();
                        View view2 = new View(A0T2);
                        this.A0X = view2;
                        view2.setBackgroundColor(A0T2.getResources().getColor(2132082697));
                        this.A04.addView(this.A0X, -1, new ViewGroup.LayoutParams(-1, i));
                    } else {
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.A0X.setLayoutParams(layoutParams);
                        }
                    }
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (this.A0X == null) {
                    z = false;
                }
                if (!this.A0S && z) {
                    i = 0;
                }
            } else {
                if (marginLayoutParams.topMargin != 0) {
                    marginLayoutParams.topMargin = 0;
                    z2 = true;
                } else {
                    z2 = false;
                }
                z = false;
            }
            if (z3) {
                this.A0D.setLayoutParams(marginLayoutParams);
            }
        }
        View view3 = this.A0X;
        if (view3 != null) {
            if (!z) {
                i2 = 8;
            }
            view3.setVisibility(i2);
        }
        return i;
    }

    public Context A0T() {
        Context context = this.A03;
        if (context == null) {
            return this.A0d;
        }
        return context;
    }

    public C27968Dm9 A0U(int i) {
        C27968Dm9[] dm9Arr = this.A0c;
        if (dm9Arr == null || dm9Arr.length <= i) {
            C27968Dm9[] dm9Arr2 = new C27968Dm9[(i + 1)];
            if (dm9Arr != null) {
                System.arraycopy(dm9Arr, 0, dm9Arr2, 0, dm9Arr.length);
            }
            this.A0c = dm9Arr2;
            dm9Arr = dm9Arr2;
        }
        C27968Dm9 dm9 = dm9Arr[i];
        if (dm9 != null) {
            return dm9;
        }
        C27968Dm9 dm92 = new C27968Dm9(i);
        dm9Arr[i] = dm92;
        return dm92;
    }

    public C27968Dm9 A0V(Menu menu) {
        int i;
        C27968Dm9[] dm9Arr = this.A0c;
        if (dm9Arr != null) {
            i = dm9Arr.length;
        } else {
            i = 0;
        }
        for (int i2 = 0; i2 < i; i2++) {
            C27968Dm9 dm9 = dm9Arr[i2];
            if (dm9 != null && dm9.A0A == menu) {
                return dm9;
            }
        }
        return null;
    }

    public void A0X(int i, C27968Dm9 dm9, Menu menu) {
        if (menu == null) {
            if (dm9 == null && i >= 0) {
                C27968Dm9[] dm9Arr = this.A0c;
                if (i < dm9Arr.length) {
                    dm9 = dm9Arr[i];
                }
            }
            if (dm9 != null) {
                menu = dm9.A0A;
            }
        }
        if ((dm9 == null || dm9.A0C) && !this.A0O) {
            this.A0e.onPanelClosed(i, menu);
        }
    }

    public void A0Y(C27968Dm9 dm9, boolean z) {
        ViewGroup viewGroup;
        C27998Dme dme;
        if (!z || dm9.A02 != 0 || (dme = this.A0E) == null || !dme.BG5()) {
            WindowManager windowManager = (WindowManager) A0T().getSystemService("window");
            if (!(windowManager == null || !dm9.A0C || (viewGroup = dm9.A08) == null)) {
                windowManager.removeView(viewGroup);
                if (z) {
                    A0X(dm9.A02, dm9, null);
                }
            }
            dm9.A0D = false;
            dm9.A0B = false;
            dm9.A0C = false;
            dm9.A07 = null;
            dm9.A0E = true;
            if (this.A09 == dm9) {
                this.A09 = null;
                return;
            }
            return;
        }
        A0Z(dm9.A0A);
    }

    public void A0Z(C27933DlV dlV) {
        if (!this.A0b) {
            this.A0b = true;
            this.A0E.AXX();
            Window.Callback callback = this.A0f.getCallback();
            if (callback != null && !this.A0O) {
                callback.onPanelClosed(108, dlV);
            }
            this.A0b = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0109, code lost:
        if (r0.A0M() == false) goto L_0x010b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0a(android.view.KeyEvent r8) {
        /*
            r7 = this;
            android.view.Window$Callback r1 = r7.A0e
            boolean r0 = r1 instanceof X.AnonymousClass0n5
            r3 = 1
            if (r0 != 0) goto L_0x000b
            boolean r0 = r1 instanceof X.C27627DgA
            if (r0 == 0) goto L_0x001a
        L_0x000b:
            android.view.Window r0 = r7.A0f
            android.view.View r0 = r0.getDecorView()
            if (r0 == 0) goto L_0x001a
            boolean r0 = X.C15320v6.dispatchUnhandledKeyEventBeforeHierarchy(r0, r8)
            if (r0 == 0) goto L_0x001a
            return r3
        L_0x001a:
            int r1 = r8.getKeyCode()
            r0 = 82
            if (r1 != r0) goto L_0x002b
            android.view.Window$Callback r0 = r7.A0e
            boolean r0 = r0.dispatchKeyEvent(r8)
            if (r0 == 0) goto L_0x002b
            return r3
        L_0x002b:
            int r2 = r8.getKeyCode()
            int r0 = r8.getAction()
            if (r0 == 0) goto L_0x0036
            r3 = 0
        L_0x0036:
            if (r3 == 0) goto L_0x0054
            r0 = 4
            r1 = 1
            if (r2 == r0) goto L_0x010d
            r0 = 82
            if (r2 != r0) goto L_0x0118
            r1 = 0
            int r0 = r8.getRepeatCount()
            if (r0 != 0) goto L_0x0052
            X.Dm9 r1 = r7.A0U(r1)
            boolean r0 = r1.A0C
            if (r0 != 0) goto L_0x0052
            A07(r7, r1, r8)
        L_0x0052:
            r0 = 1
            return r0
        L_0x0054:
            r1 = 4
            r3 = 1
            r0 = 0
            if (r2 == r1) goto L_0x00dd
            r0 = 82
            if (r2 != r0) goto L_0x0118
            r1 = 0
            X.Dm1 r0 = r7.A0C
            r2 = 0
            if (r0 != 0) goto L_0x0052
            android.content.Context r3 = r7.A0T()
            r6 = 1
            X.Dm9 r4 = r7.A0U(r1)
            X.Dme r0 = r7.A0E
            if (r0 == 0) goto L_0x00af
            boolean r0 = r0.ARZ()
            if (r0 == 0) goto L_0x00af
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r3)
            boolean r0 = r0.hasPermanentMenuKey()
            if (r0 != 0) goto L_0x00af
            X.Dme r0 = r7.A0E
            boolean r0 = r0.BG5()
            if (r0 != 0) goto L_0x00a8
            boolean r0 = r7.A0O
            if (r0 != 0) goto L_0x00ce
            boolean r0 = A07(r7, r4, r8)
            if (r0 == 0) goto L_0x00ce
            X.Dme r0 = r7.A0E
            boolean r1 = r0.CFw()
        L_0x0098:
            if (r1 == 0) goto L_0x0052
            java.lang.String r0 = "audio"
            java.lang.Object r0 = r3.getSystemService(r0)
            android.media.AudioManager r0 = (android.media.AudioManager) r0
            if (r0 == 0) goto L_0x00d4
            r0.playSoundEffect(r2)
            goto L_0x0052
        L_0x00a8:
            X.Dme r0 = r7.A0E
            boolean r1 = r0.BCC()
            goto L_0x0098
        L_0x00af:
            boolean r1 = r4.A0C
            if (r1 != 0) goto L_0x00d0
            boolean r0 = r4.A0B
            if (r0 != 0) goto L_0x00d0
            boolean r0 = r4.A0D
            if (r0 == 0) goto L_0x00ce
            boolean r0 = r4.A0F
            if (r0 == 0) goto L_0x00cc
            r4.A0D = r2
            boolean r0 = A07(r7, r4, r8)
        L_0x00c5:
            if (r0 == 0) goto L_0x00ce
            r7.A01(r4, r8)
            r1 = 1
            goto L_0x0098
        L_0x00cc:
            r0 = 1
            goto L_0x00c5
        L_0x00ce:
            r1 = 0
            goto L_0x0098
        L_0x00d0:
            r7.A0Y(r4, r6)
            goto L_0x0098
        L_0x00d4:
            java.lang.String r1 = "AppCompatDelegate"
            java.lang.String r0 = "Couldn't get audio manager"
            android.util.Log.w(r1, r0)
            goto L_0x0052
        L_0x00dd:
            boolean r2 = r7.A0Q
            r7.A0Q = r0
            X.Dm9 r1 = r7.A0U(r0)
            if (r1 == 0) goto L_0x00f2
            boolean r0 = r1.A0C
            if (r0 == 0) goto L_0x00f2
            if (r2 != 0) goto L_0x0052
            r7.A0Y(r1, r3)
            goto L_0x0052
        L_0x00f2:
            X.Dm1 r0 = r7.A0C
            if (r0 == 0) goto L_0x00fe
            r0.A05()
            r1 = 1
        L_0x00fa:
            if (r1 == 0) goto L_0x0118
            goto L_0x0052
        L_0x00fe:
            X.ByW r0 = r7.A09()
            if (r0 == 0) goto L_0x010b
            boolean r0 = r0.A0M()
            r1 = 1
            if (r0 != 0) goto L_0x00fa
        L_0x010b:
            r1 = 0
            goto L_0x00fa
        L_0x010d:
            int r0 = r8.getFlags()
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 != 0) goto L_0x0116
            r1 = 0
        L_0x0116:
            r7.A0Q = r1
        L_0x0118:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11600nR.A0a(android.view.KeyEvent):boolean");
    }

    public boolean Bes(C27933DlV dlV, MenuItem menuItem) {
        C27968Dm9 A0V2;
        Window.Callback callback = this.A0f.getCallback();
        if (callback == null || this.A0O || (A0V2 = A0V(dlV.A04())) == null) {
            return false;
        }
        return callback.onMenuItemSelected(A0V2.A02, menuItem);
    }

    public void Bev(C27933DlV dlV) {
        C27998Dme dme = this.A0E;
        if (dme == null || !dme.ARZ() || (ViewConfiguration.get(A0T()).hasPermanentMenuKey() && !this.A0E.BG4())) {
            C27968Dm9 A0U2 = A0U(0);
            A0U2.A0E = true;
            A0Y(A0U2, false);
            A01(A0U2, null);
            return;
        }
        Window.Callback callback = this.A0f.getCallback();
        if (this.A0E.BG5()) {
            this.A0E.BCC();
            if (!this.A0O) {
                callback.onPanelClosed(108, A0U(0).A0A);
            }
        } else if (callback != null && !this.A0O) {
            if (this.A0N && (this.A00 & 1) != 0) {
                this.A0f.getDecorView().removeCallbacks(this.A0h);
                this.A0h.run();
            }
            C27968Dm9 A0U3 = A0U(0);
            C27933DlV dlV2 = A0U3.A0A;
            if (dlV2 != null && !A0U3.A0F && callback.onPreparePanel(0, A0U3.A06, dlV2)) {
                callback.onMenuOpened(108, A0U3.A0A);
                this.A0E.CFw();
            }
        }
    }

    public C11600nR(Context context, Window window, C73143fZ r6) {
        this.A0d = context;
        this.A0f = window;
        this.A0g = r6;
        Window.Callback callback = window.getCallback();
        this.A0e = callback;
        if (!(callback instanceof C27937Dlb)) {
            this.A0f.setCallback(new C27937Dlb(this, callback));
            return;
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    public static boolean A06(C11600nR r3, C27968Dm9 dm9, int i, KeyEvent keyEvent, int i2) {
        C27933DlV dlV;
        boolean z = false;
        if (!keyEvent.isSystem()) {
            if ((dm9.A0D || A07(r3, dm9, keyEvent)) && (dlV = dm9.A0A) != null) {
                z = dlV.performShortcut(i, keyEvent, i2);
            }
            if (z && (i2 & 1) == 0 && r3.A0E == null) {
                r3.A0Y(dm9, true);
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0059, code lost:
        if (((org.xmlpull.v1.XmlPullParser) r6).getDepth() > 1) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View onCreateView(android.view.View r12, java.lang.String r13, android.content.Context r14, android.util.AttributeSet r15) {
        /*
            r11 = this;
            androidx.appcompat.app.AppCompatViewInflater r0 = r11.A0B
            r7 = 0
            if (r0 != 0) goto L_0x0047
            android.content.Context r1 = r11.A0T()
            int[] r0 = X.C22551Lw.A0C
            android.content.res.TypedArray r1 = r1.obtainStyledAttributes(r0)
            r0 = 112(0x70, float:1.57E-43)
            java.lang.String r1 = r1.getString(r0)
            if (r1 == 0) goto L_0x0038
            java.lang.Class<androidx.appcompat.app.AppCompatViewInflater> r0 = androidx.appcompat.app.AppCompatViewInflater.class
            java.lang.String r0 = r0.getName()
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0038
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ all -> 0x0040 }
            java.lang.Class[] r0 = new java.lang.Class[r7]     // Catch:{ all -> 0x0040 }
            java.lang.reflect.Constructor r1 = r1.getDeclaredConstructor(r0)     // Catch:{ all -> 0x0040 }
            java.lang.Object[] r0 = new java.lang.Object[r7]     // Catch:{ all -> 0x0040 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ all -> 0x0040 }
            androidx.appcompat.app.AppCompatViewInflater r0 = (androidx.appcompat.app.AppCompatViewInflater) r0     // Catch:{ all -> 0x0040 }
            r11.A0B = r0     // Catch:{ all -> 0x0040 }
            goto L_0x0047
        L_0x0038:
            androidx.appcompat.app.AppCompatViewInflater r0 = new androidx.appcompat.app.AppCompatViewInflater
            r0.<init>()
            r11.A0B = r0
            goto L_0x0047
        L_0x0040:
            androidx.appcompat.app.AppCompatViewInflater r0 = new androidx.appcompat.app.AppCompatViewInflater
            r0.<init>()
            r11.A0B = r0
        L_0x0047:
            boolean r0 = X.C11600nR.A0i
            r3 = r12
            r6 = r15
            if (r0 == 0) goto L_0x005c
            boolean r0 = r15 instanceof org.xmlpull.v1.XmlPullParser
            r1 = 1
            if (r0 == 0) goto L_0x006c
            r0 = r6
            org.xmlpull.v1.XmlPullParser r0 = (org.xmlpull.v1.XmlPullParser) r0
            int r0 = r0.getDepth()
            if (r0 <= r1) goto L_0x005c
        L_0x005b:
            r7 = 1
        L_0x005c:
            androidx.appcompat.app.AppCompatViewInflater r2 = r11.A0B
            boolean r8 = X.C11600nR.A0i
            r9 = 1
            boolean r10 = X.C27740DiA.A00()
            r4 = r13
            r5 = r14
            android.view.View r0 = r2.A02(r3, r4, r5, r6, r7, r8, r9, r10)
            return r0
        L_0x006c:
            r2 = r3
            android.view.ViewParent r2 = (android.view.ViewParent) r2
            if (r2 == 0) goto L_0x005c
            android.view.Window r0 = r11.A0f
            android.view.View r1 = r0.getDecorView()
        L_0x0077:
            if (r2 == 0) goto L_0x005b
            if (r2 == r1) goto L_0x005c
            boolean r0 = r2 instanceof android.view.View
            if (r0 == 0) goto L_0x005c
            r0 = r2
            android.view.View r0 = (android.view.View) r0
            boolean r0 = X.C15320v6.isAttachedToWindow(r0)
            if (r0 != 0) goto L_0x005c
            android.view.ViewParent r2 = r2.getParent()
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11600nR.onCreateView(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }
}
