package X;

/* renamed from: X.0lj  reason: invalid class name */
public class AnonymousClass0lj extends C06020ai {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0325, code lost:
        if (r6 == X.AnonymousClass0u3.A04) goto L_0x0327;
     */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x03a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(com.facebook.fbservice.service.OperationResult r13) {
        /*
            r12 = this;
            boolean r0 = r12 instanceof X.AnonymousClass1EV
            if (r0 != 0) goto L_0x013b
            boolean r0 = r12 instanceof X.AnonymousClass1EW
            if (r0 != 0) goto L_0x0434
            boolean r0 = r12 instanceof X.AnonymousClass148
            if (r0 != 0) goto L_0x041a
            boolean r0 = r12 instanceof X.AnonymousClass149
            if (r0 != 0) goto L_0x03ed
            boolean r0 = r12 instanceof X.AnonymousClass14A
            if (r0 != 0) goto L_0x0023
            boolean r0 = r12 instanceof X.AnonymousClass14C
            if (r0 != 0) goto L_0x024b
            boolean r0 = r12 instanceof X.C186014j
            if (r0 != 0) goto L_0x01c9
            boolean r0 = r12 instanceof X.C186214l
            if (r0 != 0) goto L_0x0168
            boolean r0 = r12 instanceof X.AnonymousClass0li
        L_0x0022:
            return
        L_0x0023:
            r7 = r12
            X.14A r7 = (X.AnonymousClass14A) r7
            X.2o7 r6 = r7.A01
            r0 = 0
            r6.A00 = r0
            X.3SU r5 = r7.A02
            X.2yQ r8 = r6.A07
            java.lang.String r4 = "ThreadViewLoader"
            r3 = 4
            if (r8 != 0) goto L_0x00c7
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r6.A04
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            java.lang.String r1 = "returnFromOnFetchMoreMessagesSucceeded"
            java.lang.String r0 = "currentResultIsNull"
            r2.A01(r1, r4, r5, r0)
        L_0x0045:
            int r1 = X.AnonymousClass1Y3.BU3
            X.2o7 r0 = r7.A01
            X.0UN r0 = r0.A04
            r3 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.2oA r0 = (X.C55492oA) r0
            int r2 = X.AnonymousClass1Y3.BQ3
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0dK r1 = (X.C07380dK) r1
            java.lang.String r0 = "android_messenger_thread_view_load_more_messages_success"
            r1.A03(r0)
            int r1 = X.AnonymousClass1Y3.BU3
            X.2o7 r0 = r7.A01
            X.0UN r0 = r0.A04
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.2oA r0 = (X.C55492oA) r0
            java.lang.String r3 = X.C55462o7.A05(r13)
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0dK r1 = (X.C07380dK) r1
            java.lang.String r0 = "android_messenger_thread_view_load_more_messages_from_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.A03(r0)
            X.2o7 r1 = r7.A01
            com.facebook.messaging.service.model.FetchMoreMessagesParams r0 = r7.A00
            int r5 = r0.A00
            X.2yQ r0 = r1.A07
            com.facebook.messaging.model.messages.MessagesCollection r0 = r0.A01
            if (r0 == 0) goto L_0x0022
            boolean r0 = r0.A08()
            if (r0 != 0) goto L_0x0022
            X.2yQ r0 = r1.A07
            com.facebook.messaging.model.messages.MessagesCollection r0 = r0.A01
            int r3 = r0.A04()
            com.facebook.analytics.DeprecatedAnalyticsLogger r2 = r1.A0B
            r1 = 0
            java.lang.String r0 = "android_load_more_messages"
            X.1La r4 = r2.A04(r0, r1)
            boolean r0 = r4.A0B()
            if (r0 == 0) goto L_0x0022
            java.lang.String r0 = "num_msgs"
            r4.A02(r0, r3)
            double r2 = (double) r3
            double r0 = (double) r5
            double r2 = r2 / r0
            double r1 = java.lang.Math.ceil(r2)
            java.lang.String r0 = "num_page"
            r4.A01(r0, r1)
            java.lang.String r0 = "page_size"
            r4.A02(r0, r5)
            r4.A0A()
            return
        L_0x00c7:
            r0 = 0
            r6.A09 = r0
            com.facebook.messaging.model.threads.ThreadSummary r1 = r8.A02
            if (r1 == 0) goto L_0x0045
            com.facebook.messaging.model.messages.MessagesCollection r0 = r8.A01
            if (r0 == 0) goto L_0x0045
            if (r8 == 0) goto L_0x0111
            if (r1 == 0) goto L_0x0111
            if (r0 == 0) goto L_0x0111
            java.lang.Object r9 = r13.A08()
            com.facebook.messaging.service.model.FetchMoreMessagesResult r9 = (com.facebook.messaging.service.model.FetchMoreMessagesResult) r9
            if (r9 == 0) goto L_0x0111
            com.facebook.messaging.model.messages.MessagesCollection r8 = r9.A02
            if (r8 == 0) goto L_0x0111
            X.3SU r1 = r6.A05
            java.lang.String r0 = r1.A06
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 == 0) goto L_0x0101
            long r0 = r1.A01
            r10 = 0
            int r2 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r2 > 0) goto L_0x0101
            X.0mB r2 = r6.A0F
            X.2yQ r0 = r6.A07
            com.facebook.messaging.model.messages.MessagesCollection r1 = r0.A01
            r0 = 0
            com.facebook.messaging.model.messages.MessagesCollection r8 = X.AnonymousClass0mB.A00(r2, r1, r8, r0)
        L_0x0101:
            X.2yQ r0 = r6.A07
            com.facebook.messaging.model.threads.ThreadSummary r10 = r0.A02
            com.google.common.collect.ImmutableList r2 = r0.A05
            com.facebook.fbservice.results.DataFetchDisposition r1 = r9.A01
            boolean r0 = r9.A03
            X.2yQ r0 = X.C61152yQ.A00(r10, r8, r2, r1, r0)
            r6.A07 = r0
        L_0x0111:
            X.2yQ r0 = r6.A07
            X.C55462o7.A0B(r0)
            X.1Ap r1 = r6.A02
            if (r1 == 0) goto L_0x0045
            X.2yQ r0 = r6.A07
            r1.Bgh(r5, r0)
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r6.A04
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            X.2yQ r1 = r6.A07
            java.lang.String r0 = "notifyNewResult"
            r2.A01(r0, r4, r5, r1)
            X.1Ap r2 = r6.A02
            X.3SU r1 = r6.A05
            X.2yQ r0 = r6.A07
            r2.BdJ(r1, r0)
            goto L_0x0045
        L_0x013b:
            r2 = r12
            X.1EV r2 = (X.AnonymousClass1EV) r2
            X.4XV r0 = r2.A00
            X.4XW r0 = r0.A00
            if (r0 == 0) goto L_0x0022
            java.lang.Object r1 = r13.A07()
            com.facebook.contacts.server.AddContactResult r1 = (com.facebook.contacts.server.AddContactResult) r1
            X.4XV r0 = r2.A00
            X.4XW r0 = r0.A00
            com.facebook.contacts.graphql.Contact r1 = r1.A00
            X.3HY r0 = r0.A00
            r0.A00 = r1
            java.lang.String r1 = r1.mProfileFbid
            com.facebook.prefs.shared.FbSharedPreferences r0 = r0.A07
            X.1hn r2 = r0.edit()
            X.1Y7 r1 = X.AnonymousClass3HY.A00(r1)
            r0 = 1
            r2.putBoolean(r1, r0)
            r2.commit()
            return
        L_0x0168:
            r8 = r12
            X.14l r8 = (X.C186214l) r8
            X.1ja r0 = r8.A01
            r1 = 0
            r0.A02 = r1
            X.1md r7 = r0.A07
            java.lang.String r6 = X.C31371ja.A03(r13)
            X.1ja r0 = r8.A01
            r0.A07 = r1
            java.lang.Object r5 = r13.A08()
            com.facebook.messaging.service.model.FetchThreadListResult r5 = (com.facebook.messaging.service.model.FetchThreadListResult) r5
            if (r5 != 0) goto L_0x018a
            X.1ja r1 = r8.A01
            X.1md r0 = r8.A00
            X.C31371ja.A0D(r1, r0, r5, r7, r6)
            return
        L_0x018a:
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r5.A06
            if (r0 == 0) goto L_0x01a3
            com.google.common.collect.ImmutableList r3 = r0.A00
            if (r3 == 0) goto L_0x01a3
            r2 = 17
            int r1 = X.AnonymousClass1Y3.A8f
            X.1ja r0 = r8.A01
            X.0UN r0 = r0.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1rA r0 = (X.C35401rA) r0
            r0.A01(r3)
        L_0x01a3:
            X.1ja r4 = r8.A01
            X.1md r0 = r4.A06
            if (r0 == 0) goto L_0x01c3
            com.facebook.messaging.model.threads.ThreadsCollection r3 = r5.A06
            if (r3 == 0) goto L_0x01c3
            r2 = 15
            int r1 = X.AnonymousClass1Y3.AgT
            X.0UN r0 = r4.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1TR r2 = (X.AnonymousClass1TR) r2
            r1 = 0
            X.2TC r0 = new X.2TC
            r0.<init>(r8, r7, r5, r6)
            r2.A07(r3, r1, r0)
            return
        L_0x01c3:
            X.1md r0 = r8.A00
            X.C31371ja.A0D(r4, r0, r5, r7, r6)
            return
        L_0x01c9:
            r5 = r12
            X.14j r5 = (X.C186014j) r5
            X.1ja r1 = r5.A01
            r0 = 0
            r1.A01 = r0
            java.lang.Object r9 = r13.A08()
            com.facebook.messaging.service.model.FetchMoreThreadsResult r9 = (com.facebook.messaging.service.model.FetchMoreThreadsResult) r9
            if (r9 != 0) goto L_0x01e7
            X.1ja r3 = r5.A01
            X.1md r2 = r5.A00
            android.os.Bundle r1 = r13.resultDataBundle
            java.lang.String r0 = X.C31371ja.A03(r13)
            X.C31371ja.A0B(r3, r2, r1, r9, r0)
            return
        L_0x01e7:
            android.os.Bundle r6 = r13.resultDataBundle
            com.facebook.fbservice.results.DataFetchDisposition r7 = r9.A01
            java.lang.String r8 = X.C31371ja.A03(r13)
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r9.A03
            if (r0 == 0) goto L_0x0208
            com.google.common.collect.ImmutableList r3 = r0.A00
            if (r3 == 0) goto L_0x0208
            r2 = 17
            int r1 = X.AnonymousClass1Y3.A8f
            X.1ja r0 = r5.A01
            X.0UN r0 = r0.A03
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1rA r0 = (X.C35401rA) r0
            r0.A01(r3)
        L_0x0208:
            X.1ja r0 = r5.A01
            java.util.Map r1 = r0.A0B
            X.1md r0 = r5.A00
            X.0ki r0 = r0.A01
            java.lang.Object r0 = r1.get(r0)
            if (r0 == 0) goto L_0x0243
            com.facebook.messaging.model.threads.ThreadsCollection r3 = r9.A03
            if (r3 == 0) goto L_0x0243
            r4 = 15
            int r2 = X.AnonymousClass1Y3.AgT
            X.1ja r1 = r5.A01
            X.0UN r0 = r1.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r2, r0)
            X.1TR r2 = (X.AnonymousClass1TR) r2
            java.util.Map r1 = r1.A0B
            X.1md r0 = r5.A00
            X.0ki r0 = r0.A01
            java.lang.Object r0 = r1.get(r0)
            X.0sF r0 = (X.C13890sF) r0
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r0.A02
            int r0 = r0.A02()
            X.3h4 r4 = new X.3h4
            r4.<init>(r5, r6, r7, r8, r9)
            r2.A07(r3, r0, r4)
            return
        L_0x0243:
            X.1ja r1 = r5.A01
            X.1md r0 = r5.A00
            X.C31371ja.A0B(r1, r0, r6, r9, r8)
            return
        L_0x024b:
            r0 = r12
            X.14C r0 = (X.AnonymousClass14C) r0
            X.2o7 r4 = r0.A00
            r9 = 0
            r4.A01 = r9
            X.3SU r3 = r4.A06
            r4.A06 = r9
            X.3SU r6 = r0.A01
            java.lang.String r0 = r4.A08
            com.google.common.base.Preconditions.checkNotNull(r0)
            java.lang.String r1 = r4.A08
            java.lang.String r0 = "fetch_threads"
            boolean r0 = r0.equals(r1)
            r5 = 0
            if (r0 == 0) goto L_0x03a9
            java.util.ArrayList r1 = r13.A0A()
            if (r1 == 0) goto L_0x027c
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x027c
            r0 = 0
            java.lang.Object r9 = r1.get(r0)
        L_0x027a:
            com.facebook.messaging.service.model.FetchThreadResult r9 = (com.facebook.messaging.service.model.FetchThreadResult) r9
        L_0x027c:
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r4.A04
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            if (r9 == 0) goto L_0x028e
            com.facebook.messaging.model.messages.MessagesCollection r0 = r9.A03
            if (r0 == 0) goto L_0x028e
            r5 = r0
        L_0x028e:
            java.lang.String r1 = "onFetchThreadSucceeded"
            java.lang.String r0 = "ThreadViewLoader"
            r2.A01(r1, r0, r6, r5)
            java.lang.String r8 = X.C55462o7.A05(r13)
            r7 = 3
            if (r9 == 0) goto L_0x0351
            com.facebook.messaging.model.threads.ThreadSummary r10 = r9.A05
            if (r10 == 0) goto L_0x0351
            com.google.common.base.Preconditions.checkNotNull(r10)
            com.facebook.messaging.model.messages.MessagesCollection r1 = r9.A03
            java.lang.String r2 = "params = %s, delayedForceUpdate = %s"
            if (r1 == 0) goto L_0x03d6
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r6.A03
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r10.A0S
            boolean r0 = com.google.common.base.Objects.equal(r2, r0)
            if (r0 == 0) goto L_0x03af
            com.facebook.fbservice.results.DataFetchDisposition r10 = r9.A02
            X.2yQ r0 = X.C55462o7.A02(r4, r9)
            r4.A07 = r0
            X.C55462o7.A0B(r0)
            X.1Ap r2 = r4.A02
            r9 = 4
            java.lang.String r5 = "ThreadViewLoader"
            if (r2 == 0) goto L_0x02db
            X.2yQ r0 = r4.A07
            r2.Bgh(r6, r0)
            int r2 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r4.A04
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r9, r2, r0)
            X.1Ao r11 = (X.C20011Ao) r11
            X.2yQ r2 = r4.A07
            java.lang.String r0 = "notifyNewResult"
            r11.A01(r0, r5, r6, r2)
        L_0x02db:
            X.3SU r0 = r4.A05
            r11 = 0
            if (r0 == 0) goto L_0x0318
            boolean r0 = r0.A0B
            if (r0 == 0) goto L_0x0318
            X.0hU r0 = r4.A03
            X.0hU r2 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA
            if (r0 == r2) goto L_0x0318
            com.facebook.common.util.TriState r0 = r10.A02
            boolean r0 = r0.asBoolean(r11)
            if (r0 != 0) goto L_0x0318
            com.facebook.common.util.TriState r0 = r10.A01
            boolean r0 = r0.asBoolean(r11)
            if (r0 != 0) goto L_0x0318
            X.3SU r0 = r4.A05
            X.C55462o7.A08(r4, r0, r2)
        L_0x02ff:
            int r1 = X.AnonymousClass1Y3.BU3
            X.0UN r0 = r4.A04
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.2oA r0 = (X.C55492oA) r0
            r0.A01()
            X.0UN r0 = r4.A04
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.2oA r0 = (X.C55492oA) r0
            r0.A03(r8)
            return
        L_0x0318:
            X.0u3 r6 = r10.A07
            X.0u3 r0 = X.AnonymousClass0u3.IN_MEMORY_CACHE
            if (r6 == r0) goto L_0x0327
            X.0u3 r0 = X.AnonymousClass0u3.LOCAL_DISK_CACHE
            if (r6 == r0) goto L_0x0327
            X.0u3 r2 = X.AnonymousClass0u3.LOCAL_UNSPECIFIED_CACHE
            r0 = 0
            if (r6 != r2) goto L_0x0328
        L_0x0327:
            r0 = 1
        L_0x0328:
            java.lang.String r6 = "notifyLoadSucceeded"
            if (r0 == 0) goto L_0x0386
            com.facebook.common.util.TriState r0 = r10.A04
            boolean r0 = r0.asBoolean(r11)
            if (r0 == 0) goto L_0x0344
            com.facebook.common.util.TriState r0 = r10.A01
            boolean r0 = r0.asBoolean(r11)
            if (r0 != 0) goto L_0x0344
        L_0x033c:
            X.3SU r1 = r4.A05
            X.0hU r0 = X.C09510hU.PREFER_CACHE_IF_UP_TO_DATE
            X.C55462o7.A08(r4, r1, r0)
            goto L_0x02ff
        L_0x0344:
            X.3SU r0 = r4.A05
            if (r0 == 0) goto L_0x0386
            int r0 = r0.A00
            boolean r0 = r1.A09(r0)
            if (r0 != 0) goto L_0x0386
            goto L_0x033c
        L_0x0351:
            if (r9 == 0) goto L_0x03e4
            com.google.common.collect.ImmutableList r0 = r9.A07
            com.facebook.user.model.User r0 = X.C55462o7.A04(r4, r0)
            if (r0 == 0) goto L_0x03e4
            X.2yQ r1 = X.C55462o7.A02(r4, r9)
            r4.A07 = r1
            X.1Ap r0 = r4.A02
            java.lang.String r5 = "ThreadViewLoader"
            if (r0 == 0) goto L_0x03a2
            r0.Bgh(r6, r1)
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r4.A04
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            X.2yQ r1 = r4.A07
            java.lang.String r0 = "notifyNewResult"
            r2.A01(r0, r5, r6, r1)
            X.1Ap r2 = r4.A02
            X.3SU r1 = r4.A05
            X.2yQ r0 = r4.A07
            r2.BdJ(r1, r0)
            goto L_0x03a2
        L_0x0386:
            X.1Ap r2 = r4.A02
            if (r2 == 0) goto L_0x03a2
            X.3SU r1 = r4.A05
            X.2yQ r0 = r4.A07
            r2.BdJ(r1, r0)
            int r1 = X.AnonymousClass1Y3.BSe
            X.0UN r0 = r4.A04
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.1Ao r2 = (X.C20011Ao) r2
            X.3SU r1 = r4.A05
            X.2yQ r0 = r4.A07
            r2.A01(r6, r5, r1, r0)
        L_0x03a2:
            if (r3 == 0) goto L_0x02ff
            X.C55462o7.A07(r4, r3)
            goto L_0x02ff
        L_0x03a9:
            java.lang.Object r9 = r13.A08()
            goto L_0x027a
        L_0x03af:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r6.A03
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r10.A0S
            X.3ST r2 = r6.A04
            com.facebook.messaging.model.messages.MessagesCollection r0 = r9.A03
            if (r0 == 0) goto L_0x03d3
            int r0 = r0.A04()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
        L_0x03c3:
            com.facebook.fbservice.results.DataFetchDisposition r0 = r9.A02
            java.lang.Object[] r1 = new java.lang.Object[]{r4, r3, r2, r1, r0}
            java.lang.String r0 = "Invalid threadKey after thread fetch. mThreadKey=%s, threadSummary.threadKey=%s, loadType=%s, numMessages=%s, %s"
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r0, r1)
            r5.<init>(r0)
            throw r5
        L_0x03d3:
            java.lang.String r1 = "na"
            goto L_0x03c3
        L_0x03d6:
            java.lang.NullPointerException r1 = new java.lang.NullPointerException
            java.lang.Object[] r0 = new java.lang.Object[]{r6, r3}
            java.lang.String r0 = com.google.common.base.Preconditions.format(r2, r0)
            r1.<init>(r0)
            throw r1
        L_0x03e4:
            X.5kg r0 = new X.5kg
            r0.<init>()
            X.C55462o7.A0A(r4, r6, r0, r3)
            return
        L_0x03ed:
            r5 = r12
            X.149 r5 = (X.AnonymousClass149) r5
            java.lang.String r4 = "ThreadViewLoader"
            int r2 = X.AnonymousClass1Y3.B7s
            X.2o7 r0 = r5.A01
            X.0UN r1 = r0.A04
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.16c r1 = (X.C189216c) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A00
            r1.A0G(r0, r4)
            int r2 = X.AnonymousClass1Y3.BSe
            X.2o7 r0 = r5.A01
            X.0UN r1 = r0.A04
            r0 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Ao r3 = (X.C20011Ao) r3
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r5.A00
            java.lang.String r1 = "broadcastForPrefetchMore"
            r0 = 0
            r3.A01(r1, r4, r2, r0)
            return
        L_0x041a:
            r4 = r12
            X.148 r4 = (X.AnonymousClass148) r4
            int r2 = X.AnonymousClass1Y3.BBd
            X.2oB r0 = r4.A01
            X.0UN r1 = r0.A00
            r0 = 4
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            com.facebook.quicklog.QuickPerformanceLogger r3 = (com.facebook.quicklog.QuickPerformanceLogger) r3
            int r2 = r4.A00
            r1 = 5505173(0x540095, float:7.71439E-39)
            r0 = 2
            r3.markerEnd(r1, r2, r0)
            return
        L_0x0434:
            r0 = r12
            X.1EW r0 = (X.AnonymousClass1EW) r0
            com.facebook.orca.threadview.DownloadAttachmentDialogFragment r3 = r0.A00
            java.lang.Object r5 = r13.A07()
            android.net.Uri r5 = (android.net.Uri) r5
            com.google.common.base.Preconditions.checkNotNull(r5)
            android.content.Intent r4 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.VIEW"
            r4.<init>(r1)
            java.lang.String r0 = r3.A0A
            r4.setDataAndType(r5, r0)
            android.content.Intent r2 = new android.content.Intent
            r2.<init>(r1)
            r2.setData(r5)
            android.content.Context r0 = r3.A00
            boolean r0 = X.AnonymousClass3DX.A01(r0, r4)
            if (r0 == 0) goto L_0x046d
            com.facebook.content.SecureContextHelper r0 = r3.A03
            X.1eJ r1 = r0.BLv()
            android.content.Context r0 = r3.A00
            r1.A0B(r4, r0)
        L_0x0469:
            r3.A22()
            return
        L_0x046d:
            android.content.Context r0 = r3.A00
            boolean r0 = X.AnonymousClass3DX.A01(r0, r2)
            if (r0 == 0) goto L_0x0481
            com.facebook.content.SecureContextHelper r0 = r3.A03
            X.1eJ r1 = r0.BLv()
            android.content.Context r0 = r3.A00
            r1.A0B(r2, r0)
            goto L_0x0469
        L_0x0481:
            X.2p2 r2 = r3.A09
            android.content.res.Resources r0 = r3.A12()
            X.2oC r1 = new X.2oC
            r1.<init>(r0)
            android.content.res.Resources r0 = r3.A12()
            java.lang.String r0 = X.C121665oc.A05(r0)
            r1.A05 = r0
            r0 = 2131821675(0x7f11046b, float:1.92761E38)
            r1.A01(r0)
            X.4yK r0 = r1.A00()
            r2.A02(r0)
            goto L_0x0469
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0lj.A04(com.facebook.fbservice.service.OperationResult):void");
    }
}
