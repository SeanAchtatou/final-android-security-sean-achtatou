package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0fc  reason: invalid class name and case insensitive filesystem */
public final class C08600fc implements C08560fY {
    private AnonymousClass0UN A00;

    public String AWk() {
        return "startup_stats";
    }

    public static final C08600fc A00(AnonymousClass1XY r1) {
        return new C08600fc(r1);
    }

    public long BL4() {
        return C08350fD.A0F;
    }

    public void BjZ(PerformanceLoggingEvent performanceLoggingEvent) {
        String str;
        AnonymousClass0Ud r4 = (AnonymousClass0Ud) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B5j, this.A00);
        performanceLoggingEvent.A06("app_init_ms", r4.A0D);
        performanceLoggingEvent.A06("time_since_app_init_ms", TimeUnit.NANOSECONDS.toMillis(performanceLoggingEvent.A0B) - r4.A0D);
        performanceLoggingEvent.A06("first_foreground_ms", r4.A0F);
        performanceLoggingEvent.A06("last_foreground_ms", r4.A0I);
        int i = AnonymousClass0oD.A01.A00;
        if (i == 0) {
            str = "UNTRACKED";
        } else if (i == 1) {
            str = "COLD";
        } else if (i == 2) {
            str = "WARM";
        } else if (i != 3) {
            str = "INVALID";
        } else {
            str = "HOT";
        }
        performanceLoggingEvent.A0B("startup_status", str);
    }

    private C08600fc(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A0B;
    }
}
