package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.0wM  reason: invalid class name and case insensitive filesystem */
public final class C16020wM {
    public final C34811qD A00;
    private final C16510xE A01;
    private final C17350yl A02;

    public static final C16020wM A00(AnonymousClass1XY r1) {
        return new C16020wM(r1);
    }

    private boolean A01() {
        if (this.A02.A0A()) {
            if (!((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A01.A00)).Aem(282501481694741L)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public C16030wN A02() {
        C16040wO r0;
        C16040wO r02;
        ImmutableList.Builder builder = ImmutableList.builder();
        builder.add((Object) C16040wO.INBOX);
        if (A01()) {
            r0 = C16040wO.FRIENDS;
        } else {
            r0 = C16040wO.CONTACTS;
        }
        builder.add((Object) r0);
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00.A00)).Aem(2306125587997001386L)) {
            builder.add((Object) C16040wO.DISCOVER);
        }
        ImmutableList build = builder.build();
        if (A01()) {
            r02 = C16040wO.FRIENDS;
        } else {
            r02 = C16040wO.CONTACTS;
        }
        return new C16030wN(build, ImmutableList.of(r02));
    }

    private C16020wM(AnonymousClass1XY r2) {
        this.A01 = C16510xE.A00(r2);
        this.A02 = C17350yl.A00(r2);
        this.A00 = C34811qD.A00(r2);
    }
}
