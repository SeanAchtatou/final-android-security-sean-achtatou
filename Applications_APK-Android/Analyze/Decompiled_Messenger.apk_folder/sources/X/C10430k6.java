package X;

import java.util.Collection;

/* renamed from: X.0k6  reason: invalid class name and case insensitive filesystem */
public abstract class C10430k6 {
    public abstract Collection collectAndResolveSubtypes(C10070jV r1, C10470kA r2, C10140jc r3);

    public abstract Collection collectAndResolveSubtypes(C183512m r1, C10470kA r2, C10140jc r3, C10030jR r4);
}
