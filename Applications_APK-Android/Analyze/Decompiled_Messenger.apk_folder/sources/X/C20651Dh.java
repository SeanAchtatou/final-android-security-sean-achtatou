package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import java.util.concurrent.CancellationException;

/* renamed from: X.1Dh  reason: invalid class name and case insensitive filesystem */
public final class C20651Dh extends C06020ai {
    public final ThreadKey A00;
    public final /* synthetic */ C53882lW A01;

    public C20651Dh(C53882lW r1, ThreadKey threadKey) {
        this.A01 = r1;
        this.A00 = threadKey;
    }

    public void A03(CancellationException cancellationException) {
        super.A03(cancellationException);
        this.A01.A0e = false;
    }
}
