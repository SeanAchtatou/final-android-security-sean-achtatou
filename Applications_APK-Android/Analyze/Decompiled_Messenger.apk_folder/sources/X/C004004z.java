package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.core.TraceEvents;

/* renamed from: X.04z  reason: invalid class name and case insensitive filesystem */
public final class C004004z {
    public static volatile boolean A00;

    public static String A00(int i) {
        AnonymousClass054 r0;
        if (!A00 || (r0 = AnonymousClass054.A07) == null) {
            return null;
        }
        return r0.A07(i);
    }

    public static boolean A01(int i) {
        AnonymousClass054 r0;
        if (!A00 || (r0 = AnonymousClass054.A07) == null) {
            return false;
        }
        int i2 = r0.A02.get() & 65534;
        boolean z = false;
        if (i2 != 0) {
            z = true;
        }
        if (!z || !TraceEvents.isEnabled(i)) {
            return false;
        }
        return true;
    }

    public static boolean A02(int i) {
        AnonymousClass054 r0;
        if (!A00 || (r0 = AnonymousClass054.A07) == null) {
            return false;
        }
        String A07 = r0.A07(i);
        boolean z = false;
        if (A07 != null) {
            z = true;
        }
        if (z) {
            return true;
        }
        return false;
    }

    public static boolean A03(String str) {
        AnonymousClass054 r0;
        if (!A00 || (r0 = AnonymousClass054.A07) == null) {
            return false;
        }
        int i = r0.A02.get() & 65534;
        boolean z = false;
        if (i != 0) {
            z = true;
        }
        if (!z || !TraceEvents.isEnabled(ProvidersRegistry.getBitMaskFor(str))) {
            return false;
        }
        return true;
    }
}
