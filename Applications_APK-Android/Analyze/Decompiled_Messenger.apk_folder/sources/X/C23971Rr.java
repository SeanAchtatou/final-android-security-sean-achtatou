package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.1Rr  reason: invalid class name and case insensitive filesystem */
public final class C23971Rr implements C23981Rs {
    public final /* synthetic */ AnonymousClass1QK A00;
    public final /* synthetic */ C23581Qb A01;
    public final /* synthetic */ AnonymousClass1Q6 A02;
    public final /* synthetic */ AnonymousClass1MN A03;

    public C23971Rr(AnonymousClass1Q6 r1, AnonymousClass1MN r2, AnonymousClass1QK r3, C23581Qb r4) {
        this.A02 = r1;
        this.A03 = r2;
        this.A00 = r3;
        this.A01 = r4;
    }

    public Object CIx(C23901Rk r8) {
        boolean z;
        boolean z2;
        Object obj;
        synchronized (r8.A07) {
            try {
                z = r8.A04;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (z || (r8.A04() && (r8.A02() instanceof CancellationException))) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2) {
            this.A03.Bk2(this.A00, "DiskCacheProducer", null);
            this.A01.onCancellation();
            return null;
        }
        if (r8.A04()) {
            this.A03.Bk4(this.A00, "DiskCacheProducer", r8.A02(), null);
        } else {
            synchronized (r8.A07) {
                try {
                    obj = r8.A02;
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
            AnonymousClass1NY r5 = (AnonymousClass1NY) obj;
            if (r5 != null) {
                AnonymousClass1MN r2 = this.A03;
                AnonymousClass1QK r1 = this.A00;
                r2.Bk6(r1, "DiskCacheProducer", AnonymousClass1Q6.A00(r2, r1, true, r5.A08()));
                this.A03.Bt8(this.A00, "DiskCacheProducer", true);
                this.A00.A05.put(1, "disk");
                this.A01.BkJ(1.0f);
                this.A01.Bgg(r5, 1);
                r5.close();
                return null;
            }
            AnonymousClass1MN r22 = this.A03;
            AnonymousClass1QK r12 = this.A00;
            r22.Bk6(r12, "DiskCacheProducer", AnonymousClass1Q6.A00(r22, r12, false, 0));
        }
        this.A02.A00.ByS(this.A01, this.A00);
        return null;
        throw th;
    }
}
