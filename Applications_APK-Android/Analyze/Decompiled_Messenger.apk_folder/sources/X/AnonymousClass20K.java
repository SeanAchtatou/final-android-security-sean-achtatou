package X;

/* renamed from: X.20K  reason: invalid class name */
public final class AnonymousClass20K {
    public C30043Eoa A00 = C30043Eoa.CREATING_TEST;

    public synchronized void A00() {
        A01(this.A00.A01());
    }

    public synchronized void A01(C30043Eoa eoa) {
        this.A00 = eoa;
    }

    public synchronized boolean A02(C30043Eoa eoa) {
        boolean z;
        z = false;
        if (this.A00.A00() > eoa.A00()) {
            z = true;
        }
        return z;
    }
}
