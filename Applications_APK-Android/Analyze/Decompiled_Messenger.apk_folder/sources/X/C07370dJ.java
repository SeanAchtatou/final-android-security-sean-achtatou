package X;

import android.content.Context;

/* renamed from: X.0dJ  reason: invalid class name and case insensitive filesystem */
public abstract class C07370dJ extends AnonymousClass06X {
    public void A05(Context context, String str) {
        C852542s r0 = new C852542s(context);
        String A04 = A04();
        ((AnonymousClass09P) AnonymousClass1XX.A03(AnonymousClass1Y3.Amr, r0.A00)).CGS(A04, AnonymousClass08S.A0S("Rejected the intent for the receiver because it was not registered: ", str, ":", A04));
    }
}
