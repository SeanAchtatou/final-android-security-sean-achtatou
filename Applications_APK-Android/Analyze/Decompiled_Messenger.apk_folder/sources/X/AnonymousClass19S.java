package X;

import android.graphics.PointF;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.LinearLayoutManager$SavedState;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.19S  reason: invalid class name */
public class AnonymousClass19S extends AnonymousClass19T implements AnonymousClass19V, AnonymousClass19W {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public AnonymousClass1AA A04;
    public LinearLayoutManager$SavedState A05;
    public C197219k A06;
    public boolean A07;
    public boolean A08;
    private boolean A09;
    public final C196919h A0A;
    private final C197119j A0B;
    public boolean mLastStackFromEnd;
    public boolean mStackFromEnd;

    public static int A04(AnonymousClass19S r7, C15930wD r8) {
        AnonymousClass19S r5 = r7;
        if (r7.A0c() == 0) {
            return 0;
        }
        r7.A1y();
        return C52082iR.A01(r8, r7.A06, r7.A0A(!r7.A09, true), r7.A09(!r7.A09, true), r5, r7.A09, r7.A08);
    }

    public static int A05(AnonymousClass19S r6, C15930wD r7) {
        AnonymousClass19S r5 = r6;
        if (r6.A0c() == 0) {
            return 0;
        }
        r6.A1y();
        return C52082iR.A00(r7, r6.A06, r6.A0A(!r6.A09, true), r6.A09(!r6.A09, true), r5, r6.A09);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0033 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0046 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A1w(int r5) {
        /*
            r4 = this;
            r3 = -1
            r2 = 1
            if (r5 == r2) goto L_0x0037
            r0 = 2
            if (r5 == r0) goto L_0x0024
            r0 = 17
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r5 == r0) goto L_0x001f
            r0 = 33
            if (r5 == r0) goto L_0x0042
            r0 = 66
            if (r5 == r0) goto L_0x001a
            r0 = 130(0x82, float:1.82E-43)
            if (r5 == r0) goto L_0x002f
            return r1
        L_0x001a:
            int r0 = r4.A01
            if (r0 != 0) goto L_0x0034
            return r2
        L_0x001f:
            int r0 = r4.A01
            if (r0 != 0) goto L_0x0047
            return r3
        L_0x0024:
            int r0 = r4.A01
            if (r0 == r2) goto L_0x0033
            boolean r0 = r4.A25()
            if (r0 == 0) goto L_0x0033
            return r3
        L_0x002f:
            int r0 = r4.A01
            if (r0 != r2) goto L_0x0034
        L_0x0033:
            return r2
        L_0x0034:
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            return r2
        L_0x0037:
            int r0 = r4.A01
            if (r0 == r2) goto L_0x0046
            boolean r0 = r4.A25()
            if (r0 == 0) goto L_0x0046
            return r2
        L_0x0042:
            int r0 = r4.A01
            if (r0 != r2) goto L_0x0047
        L_0x0046:
            return r3
        L_0x0047:
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19S.A1w(int):int");
    }

    public void A20(C15740vp r1, C15930wD r2, C196919h r3, int i) {
    }

    public void A23(boolean z) {
        A1r(null);
        if (z != this.A07) {
            this.A07 = z;
            A0y();
        }
    }

    public void A24(boolean z) {
        A1r(null);
        if (this.mStackFromEnd != z) {
            this.mStackFromEnd = z;
            A0y();
        }
    }

    private int A01(int i, C15740vp r5, C15930wD r6, boolean z) {
        int A022;
        int A023 = this.A06.A02() - i;
        if (A023 <= 0) {
            return 0;
        }
        int i2 = -A00(-A023, r5, r6);
        int i3 = i + i2;
        if (!z || (A022 = this.A06.A02() - i3) <= 0) {
            return i2;
        }
        this.A06.A0E(A022);
        return A022 + i2;
    }

    private int A02(int i, C15740vp r5, C15930wD r6, boolean z) {
        int A062;
        int A063 = i - this.A06.A06();
        if (A063 <= 0) {
            return 0;
        }
        int i2 = -A00(A063, r5, r6);
        int i3 = i + i2;
        if (!z || (A062 = i3 - this.A06.A06()) <= 0) {
            return i2;
        }
        this.A06.A0E(-A062);
        return i2 - A062;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
        if (r6 >= r10.A00()) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int A06(X.C15740vp r8, X.AnonymousClass1AA r9, X.C15930wD r10, boolean r11) {
        /*
            r7 = this;
            int r5 = r9.A03
            int r0 = r9.A09
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 == r4) goto L_0x0010
            if (r5 >= 0) goto L_0x000d
            int r0 = r0 + r5
            r9.A09 = r0
        L_0x000d:
            r7.A0G(r8, r9)
        L_0x0010:
            int r3 = r9.A03
            int r0 = r9.A05
            int r3 = r3 + r0
            X.19j r2 = r7.A0B
        L_0x0017:
            boolean r0 = r9.A01
            if (r0 != 0) goto L_0x001d
            if (r3 <= 0) goto L_0x0070
        L_0x001d:
            int r6 = r9.A04
            if (r6 < 0) goto L_0x0028
            int r1 = r10.A00()
            r0 = 1
            if (r6 < r1) goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            if (r0 == 0) goto L_0x0070
            r0 = 0
            r2.A00 = r0
            r2.A01 = r0
            r2.A03 = r0
            r2.A02 = r0
            r7.A21(r8, r10, r9, r2)
            boolean r0 = r2.A01
            if (r0 != 0) goto L_0x0070
            int r1 = r9.A08
            int r6 = r2.A00
            int r0 = r9.A07
            int r0 = r0 * r6
            int r1 = r1 + r0
            r9.A08 = r1
            boolean r0 = r2.A03
            if (r0 == 0) goto L_0x0053
            X.1AA r0 = r7.A04
            java.util.List r0 = r0.A0A
            if (r0 != 0) goto L_0x0053
            boolean r0 = r10.A08
            if (r0 != 0) goto L_0x0059
        L_0x0053:
            int r0 = r9.A03
            int r0 = r0 - r6
            r9.A03 = r0
            int r3 = r3 - r6
        L_0x0059:
            int r1 = r9.A09
            if (r1 == r4) goto L_0x006a
            int r1 = r1 + r6
            r9.A09 = r1
            int r0 = r9.A03
            if (r0 >= 0) goto L_0x0067
            int r1 = r1 + r0
            r9.A09 = r1
        L_0x0067:
            r7.A0G(r8, r9)
        L_0x006a:
            if (r11 == 0) goto L_0x0017
            boolean r0 = r2.A02
            if (r0 == 0) goto L_0x0017
        L_0x0070:
            int r0 = r9.A03
            int r5 = r5 - r0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19S.A06(X.0vp, X.1AA, X.0wD, boolean):int");
    }

    private View A09(boolean z, boolean z2) {
        if (this.A08) {
            return A08(0, A0c(), z, z2);
        }
        return A08(A0c() - 1, -1, z, z2);
    }

    private View A0A(boolean z, boolean z2) {
        if (this.A08) {
            return A08(A0c() - 1, -1, z, z2);
        }
        return A08(0, A0c(), z, z2);
    }

    private void A0B(int i, int i2) {
        this.A04.A03 = this.A06.A02() - i2;
        AnonymousClass1AA r3 = this.A04;
        int i3 = 1;
        if (this.A08) {
            i3 = -1;
        }
        r3.A06 = i3;
        r3.A04 = i;
        r3.A07 = 1;
        r3.A08 = i2;
        r3.A09 = Integer.MIN_VALUE;
    }

    private void A0C(int i, int i2) {
        this.A04.A03 = i2 - this.A06.A06();
        AnonymousClass1AA r3 = this.A04;
        r3.A04 = i;
        int i3 = -1;
        if (this.A08) {
            i3 = 1;
        }
        r3.A06 = i3;
        r3.A07 = -1;
        r3.A08 = i2;
        r3.A09 = Integer.MIN_VALUE;
    }

    public static void A0D(AnonymousClass19S r2) {
        if (r2.A01 == 1 || !r2.A25()) {
            r2.A08 = r2.A07;
        } else {
            r2.A08 = !r2.A07;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000f, code lost:
        if (r3.A01() != 0) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0E(X.AnonymousClass19S r6, int r7, int r8, boolean r9, X.C15930wD r10) {
        /*
            X.1AA r4 = r6.A04
            X.19k r3 = r6.A06
            int r0 = r3.A04()
            if (r0 != 0) goto L_0x0011
            int r1 = r3.A01()
            r0 = 1
            if (r1 == 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            r4.A01 = r0
            int r2 = r10.A06
            r1 = -1
            r0 = 0
            if (r2 == r1) goto L_0x001b
            r0 = 1
        L_0x001b:
            if (r0 == 0) goto L_0x00c3
            int r0 = r3.A07()
        L_0x0021:
            r4.A05 = r0
            X.1AA r2 = r6.A04
            r2.A07 = r7
            r5 = -1
            r0 = 1
            if (r7 != r0) goto L_0x007b
            int r1 = r2.A05
            X.19k r0 = r6.A06
            int r0 = r0.A03()
            int r1 = r1 + r0
            r2.A05 = r1
            boolean r0 = r6.A08
            if (r0 == 0) goto L_0x0074
            r0 = 0
        L_0x003b:
            android.view.View r4 = r6.A0u(r0)
            X.1AA r3 = r6.A04
            boolean r0 = r6.A08
            if (r0 != 0) goto L_0x0046
            r5 = 1
        L_0x0046:
            r3.A06 = r5
            int r2 = X.AnonymousClass19T.A0L(r4)
            X.1AA r1 = r6.A04
            int r0 = r1.A06
            int r2 = r2 + r0
            r3.A04 = r2
            X.19k r0 = r6.A06
            int r0 = r0.A08(r4)
            r1.A08 = r0
            X.19k r0 = r6.A06
            int r1 = r0.A08(r4)
            X.19k r0 = r6.A06
            int r0 = r0.A02()
            int r1 = r1 - r0
        L_0x0068:
            X.1AA r0 = r6.A04
            r0.A03 = r8
            if (r9 == 0) goto L_0x0071
            int r8 = r8 - r1
            r0.A03 = r8
        L_0x0071:
            r0.A09 = r1
            return
        L_0x0074:
            int r0 = r6.A0c()
            int r0 = r0 + -1
            goto L_0x003b
        L_0x007b:
            boolean r0 = r6.A08
            if (r0 == 0) goto L_0x00c1
            int r0 = r6.A0c()
            int r0 = r0 + -1
        L_0x0085:
            android.view.View r4 = r6.A0u(r0)
            X.1AA r2 = r6.A04
            int r1 = r2.A05
            X.19k r0 = r6.A06
            int r0 = r0.A06()
            int r1 = r1 + r0
            r2.A05 = r1
            X.1AA r3 = r6.A04
            boolean r0 = r6.A08
            if (r0 == 0) goto L_0x009d
            r5 = 1
        L_0x009d:
            r3.A06 = r5
            int r2 = X.AnonymousClass19T.A0L(r4)
            X.1AA r1 = r6.A04
            int r0 = r1.A06
            int r2 = r2 + r0
            r3.A04 = r2
            X.19k r0 = r6.A06
            int r0 = r0.A0B(r4)
            r1.A08 = r0
            X.19k r0 = r6.A06
            int r0 = r0.A0B(r4)
            int r1 = -r0
            X.19k r0 = r6.A06
            int r0 = r0.A06()
            int r1 = r1 + r0
            goto L_0x0068
        L_0x00c1:
            r0 = 0
            goto L_0x0085
        L_0x00c3:
            r0 = 0
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19S.A0E(X.19S, int, int, boolean, X.0wD):void");
    }

    private void A0F(C15740vp r2, int i, int i2) {
        if (i == i2) {
            return;
        }
        if (i2 > i) {
            for (int i3 = i2 - 1; i3 >= i; i3--) {
                A14(i3, r2);
            }
            return;
        }
        while (i > i2) {
            A14(i, r2);
            i--;
        }
    }

    private void A0G(C15740vp r8, AnonymousClass1AA r9) {
        int i;
        int i2;
        if (r9.A02 && !r9.A01) {
            int i3 = r9.A07;
            int i4 = r9.A09;
            if (i3 == -1) {
                int A0c = A0c();
                if (i4 >= 0) {
                    int A012 = this.A06.A01() - i4;
                    if (this.A08) {
                        i = 0;
                        i2 = 0;
                        while (i2 < A0c) {
                            View A0u = A0u(i2);
                            if (this.A06.A0B(A0u) >= A012 && this.A06.A0D(A0u) >= A012) {
                                i2++;
                            }
                        }
                        return;
                    }
                    i = A0c - 1;
                    int i5 = i;
                    while (i2 >= 0) {
                        View A0u2 = A0u(i2);
                        if (this.A06.A0B(A0u2) >= A012 && this.A06.A0D(A0u2) >= A012) {
                            i5 = i2 - 1;
                        }
                    }
                    return;
                }
                return;
            } else if (i4 >= 0) {
                int A0c2 = A0c();
                if (this.A08) {
                    i = A0c2 - 1;
                    int i6 = i;
                    while (i2 >= 0) {
                        View A0u3 = A0u(i2);
                        if (this.A06.A08(A0u3) <= i4 && this.A06.A0C(A0u3) <= i4) {
                            i6 = i2 - 1;
                        }
                    }
                    return;
                }
                i = 0;
                int i7 = 0;
                while (i2 < A0c2) {
                    View A0u4 = A0u(i2);
                    if (this.A06.A08(A0u4) <= i4 && this.A06.A0C(A0u4) <= i4) {
                        i7 = i2 + 1;
                    }
                }
                return;
            } else {
                return;
            }
            A0F(r8, i, i2);
        }
    }

    public int A1b(int i, C15740vp r4, C15930wD r5) {
        if (this.A01 == 1) {
            return 0;
        }
        return A00(i, r4, r5);
    }

    public int A1c(int i, C15740vp r3, C15930wD r4) {
        if (this.A01 == 0) {
            return 0;
        }
        return A00(i, r3, r4);
    }

    public void A1j(int i) {
        this.A02 = i;
        this.A03 = Integer.MIN_VALUE;
        LinearLayoutManager$SavedState linearLayoutManager$SavedState = this.A05;
        if (linearLayoutManager$SavedState != null) {
            linearLayoutManager$SavedState.A01 = -1;
        }
        A0y();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:150:0x029c, code lost:
        if (r9.A05() >= r1.A00()) goto L_0x029e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00ed, code lost:
        if (r5.A01() != 0) goto L_0x00ef;
     */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x02ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1m(X.C15740vp r20, X.C15930wD r21) {
        /*
            r19 = this;
            r3 = r19
            androidx.recyclerview.widget.LinearLayoutManager$SavedState r4 = r3.A05
            r5 = -1
            r2 = r20
            r1 = r21
            if (r4 != 0) goto L_0x000f
            int r0 = r3.A02
            if (r0 == r5) goto L_0x0019
        L_0x000f:
            int r0 = r1.A00()
            if (r0 != 0) goto L_0x0019
            r3.A1F(r2)
            return
        L_0x0019:
            if (r4 == 0) goto L_0x0025
            int r4 = r4.A01
            r0 = 0
            if (r4 < 0) goto L_0x0021
            r0 = 1
        L_0x0021:
            if (r0 == 0) goto L_0x0025
            r3.A02 = r4
        L_0x0025:
            r3.A1y()
            X.1AA r0 = r3.A04
            r4 = 0
            r0.A02 = r4
            A0D(r3)
            androidx.recyclerview.widget.RecyclerView r0 = r3.A08
            r6 = 0
            if (r0 == 0) goto L_0x0045
            android.view.View r7 = r0.getFocusedChild()
            if (r7 == 0) goto L_0x0045
            X.0wh r0 = r3.A06
            java.util.List r0 = r0.A02
            boolean r0 = r0.contains(r7)
            if (r0 == 0) goto L_0x0046
        L_0x0045:
            r7 = r6
        L_0x0046:
            X.19h r0 = r3.A0A
            boolean r8 = r0.A01
            r6 = 1
            if (r8 == 0) goto L_0x021d
            int r8 = r3.A02
            if (r8 != r5) goto L_0x021d
            androidx.recyclerview.widget.LinearLayoutManager$SavedState r8 = r3.A05
            if (r8 != 0) goto L_0x021d
            if (r7 == 0) goto L_0x007c
            X.19k r0 = r3.A06
            int r8 = r0.A0B(r7)
            X.19k r0 = r3.A06
            int r0 = r0.A02()
            if (r8 >= r0) goto L_0x0073
            X.19k r0 = r3.A06
            int r8 = r0.A08(r7)
            X.19k r0 = r3.A06
            int r0 = r0.A06()
            if (r8 > r0) goto L_0x007c
        L_0x0073:
            X.19h r8 = r3.A0A
            int r0 = X.AnonymousClass19T.A0L(r7)
            r8.A03(r7, r0)
        L_0x007c:
            int r8 = r1.A06
            r7 = -1
            r0 = 0
            if (r8 == r7) goto L_0x0083
            r0 = 1
        L_0x0083:
            if (r0 == 0) goto L_0x021a
            X.19k r0 = r3.A06
            int r8 = r0.A07()
        L_0x008b:
            X.1AA r0 = r3.A04
            int r0 = r0.A00
            r9 = 0
            if (r0 < 0) goto L_0x0094
            r9 = r8
            r8 = 0
        L_0x0094:
            X.19k r0 = r3.A06
            int r0 = r0.A06()
            int r8 = r8 + r0
            X.19k r0 = r3.A06
            int r0 = r0.A03()
            int r9 = r9 + r0
            boolean r0 = r1.A08
            if (r0 == 0) goto L_0x00cd
            int r10 = r3.A02
            if (r10 == r5) goto L_0x00cd
            int r7 = r3.A03
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r7 == r0) goto L_0x00cd
            android.view.View r7 = r3.A0t(r10)
            if (r7 == 0) goto L_0x00cd
            boolean r0 = r3.A08
            if (r0 == 0) goto L_0x0209
            X.19k r0 = r3.A06
            int r10 = r0.A02()
            X.19k r0 = r3.A06
            int r0 = r0.A08(r7)
            int r10 = r10 - r0
            int r7 = r3.A03
        L_0x00c9:
            int r10 = r10 - r7
            if (r10 <= 0) goto L_0x0206
            int r8 = r8 + r10
        L_0x00cd:
            X.19h r7 = r3.A0A
            boolean r0 = r7.A04
            if (r0 == 0) goto L_0x0200
            boolean r0 = r3.A08
            if (r0 == 0) goto L_0x00d8
        L_0x00d7:
            r5 = 1
        L_0x00d8:
            r3.A20(r2, r1, r7, r5)
            r3.A1E(r2)
            X.1AA r7 = r3.A04
            X.19k r5 = r3.A06
            int r0 = r5.A04()
            if (r0 != 0) goto L_0x00ef
            int r5 = r5.A01()
            r0 = 1
            if (r5 == 0) goto L_0x00f0
        L_0x00ef:
            r0 = 0
        L_0x00f0:
            r7.A01 = r0
            X.19h r7 = r3.A0A
            boolean r0 = r7.A04
            if (r0 == 0) goto L_0x01b8
            int r5 = r7.A03
            int r0 = r7.A02
            r3.A0C(r5, r0)
            X.1AA r0 = r3.A04
            r0.A05 = r8
            r3.A06(r2, r0, r1, r4)
            X.1AA r0 = r3.A04
            int r14 = r0.A08
            int r8 = r0.A04
            int r0 = r0.A03
            if (r0 <= 0) goto L_0x0111
            int r9 = r9 + r0
        L_0x0111:
            X.19h r0 = r3.A0A
            int r5 = r0.A03
            int r0 = r0.A02
            r3.A0B(r5, r0)
            X.1AA r7 = r3.A04
            r7.A05 = r9
            int r5 = r7.A04
            int r0 = r7.A06
            int r5 = r5 + r0
            r7.A04 = r5
            r3.A06(r2, r7, r1, r4)
            X.1AA r0 = r3.A04
            int r13 = r0.A08
            int r5 = r0.A03
            if (r5 <= 0) goto L_0x013e
            r3.A0C(r8, r14)
            X.1AA r0 = r3.A04
            r0.A05 = r5
            r3.A06(r2, r0, r1, r4)
            X.1AA r0 = r3.A04
            int r14 = r0.A08
        L_0x013e:
            int r0 = r3.A0c()
            if (r0 <= 0) goto L_0x0157
            boolean r5 = r3.A08
            boolean r0 = r3.mStackFromEnd
            r5 = r5 ^ r0
            if (r5 == 0) goto L_0x01ad
            int r0 = r3.A01(r13, r2, r1, r6)
            int r14 = r14 + r0
            int r13 = r13 + r0
            int r0 = r3.A02(r14, r2, r1, r4)
        L_0x0155:
            int r14 = r14 + r0
            int r13 = r13 + r0
        L_0x0157:
            boolean r0 = r1.A0A
            if (r0 == 0) goto L_0x0481
            int r0 = r3.A0c()
            if (r0 == 0) goto L_0x0481
            boolean r0 = r1.A08
            if (r0 != 0) goto L_0x0481
            boolean r0 = r3.A1s()
            if (r0 == 0) goto L_0x0481
            java.util.List r15 = r2.A06
            int r17 = r15.size()
            android.view.View r0 = r3.A0u(r4)
            int r16 = X.AnonymousClass19T.A0L(r0)
            r11 = 0
            r10 = 0
            r9 = 0
        L_0x017c:
            r0 = r17
            if (r11 >= r0) goto L_0x0431
            java.lang.Object r8 = r15.get(r11)
            X.1o8 r8 = (X.C33781o8) r8
            boolean r0 = r8.A0E()
            if (r0 != 0) goto L_0x01a8
            int r5 = r8.A05()
            r7 = 1
            r6 = 0
            r0 = r16
            if (r5 >= r0) goto L_0x0197
            r6 = 1
        L_0x0197:
            boolean r5 = r3.A08
            r0 = -1
            if (r6 == r5) goto L_0x019d
            r7 = -1
        L_0x019d:
            X.19k r5 = r3.A06
            android.view.View r6 = r8.A0H
            int r5 = r5.A09(r6)
            if (r7 != r0) goto L_0x01ab
            int r10 = r10 + r5
        L_0x01a8:
            int r11 = r11 + 1
            goto L_0x017c
        L_0x01ab:
            int r9 = r9 + r5
            goto L_0x01a8
        L_0x01ad:
            int r0 = r3.A02(r14, r2, r1, r6)
            int r14 = r14 + r0
            int r13 = r13 + r0
            int r0 = r3.A01(r13, r2, r1, r4)
            goto L_0x0155
        L_0x01b8:
            int r5 = r7.A03
            int r0 = r7.A02
            r3.A0B(r5, r0)
            X.1AA r0 = r3.A04
            r0.A05 = r9
            r3.A06(r2, r0, r1, r4)
            X.1AA r0 = r3.A04
            int r13 = r0.A08
            int r9 = r0.A04
            int r0 = r0.A03
            if (r0 <= 0) goto L_0x01d1
            int r8 = r8 + r0
        L_0x01d1:
            X.19h r0 = r3.A0A
            int r5 = r0.A03
            int r0 = r0.A02
            r3.A0C(r5, r0)
            X.1AA r7 = r3.A04
            r7.A05 = r8
            int r5 = r7.A04
            int r0 = r7.A06
            int r5 = r5 + r0
            r7.A04 = r5
            r3.A06(r2, r7, r1, r4)
            X.1AA r0 = r3.A04
            int r14 = r0.A08
            int r5 = r0.A03
            if (r5 <= 0) goto L_0x013e
            r3.A0B(r9, r13)
            X.1AA r0 = r3.A04
            r0.A05 = r5
            r3.A06(r2, r0, r1, r4)
            X.1AA r0 = r3.A04
            int r13 = r0.A08
            goto L_0x013e
        L_0x0200:
            boolean r0 = r3.A08
            if (r0 == 0) goto L_0x00d7
            goto L_0x00d8
        L_0x0206:
            int r9 = r9 - r10
            goto L_0x00cd
        L_0x0209:
            X.19k r0 = r3.A06
            int r7 = r0.A0B(r7)
            X.19k r0 = r3.A06
            int r0 = r0.A06()
            int r7 = r7 - r0
            int r10 = r3.A03
            goto L_0x00c9
        L_0x021a:
            r8 = 0
            goto L_0x008b
        L_0x021d:
            r0.A00()
            boolean r11 = r3.A08
            boolean r7 = r3.mStackFromEnd
            r7 = r7 ^ r11
            r0.A04 = r7
            boolean r7 = r1.A08
            r10 = 0
            if (r7 != 0) goto L_0x042e
            int r12 = r3.A02
            if (r12 == r5) goto L_0x042e
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r12 < 0) goto L_0x042a
            int r7 = r1.A00()
            if (r12 >= r7) goto L_0x042a
            r0.A03 = r12
            androidx.recyclerview.widget.LinearLayoutManager$SavedState r13 = r3.A05
            if (r13 == 0) goto L_0x0374
            int r8 = r13.A01
            r7 = 0
            if (r8 < 0) goto L_0x0246
            r7 = 1
        L_0x0246:
            if (r7 == 0) goto L_0x0374
            boolean r7 = r13.A02
            r0.A04 = r7
            if (r7 == 0) goto L_0x0368
            X.19k r7 = r3.A06
            int r8 = r7.A02()
            androidx.recyclerview.widget.LinearLayoutManager$SavedState r7 = r3.A05
            int r7 = r7.A00
        L_0x0258:
            int r8 = r8 - r7
            r0.A02 = r8
        L_0x025b:
            r7 = 1
        L_0x025c:
            if (r7 != 0) goto L_0x02ba
            r18 = r3
            int r7 = r18.A0c()
            r11 = 0
            if (r7 == 0) goto L_0x0365
            androidx.recyclerview.widget.RecyclerView r7 = r3.A08
            r9 = 0
            if (r7 == 0) goto L_0x027c
            android.view.View r8 = r7.getFocusedChild()
            if (r8 == 0) goto L_0x027c
            X.0wh r7 = r3.A06
            java.util.List r7 = r7.A02
            boolean r7 = r7.contains(r8)
            if (r7 == 0) goto L_0x027d
        L_0x027c:
            r8 = r9
        L_0x027d:
            if (r8 == 0) goto L_0x02c2
            android.view.ViewGroup$LayoutParams r7 = r8.getLayoutParams()
            X.1R7 r7 = (X.AnonymousClass1R7) r7
            X.1o8 r9 = r7.mViewHolder
            boolean r7 = r9.A0E()
            if (r7 != 0) goto L_0x029e
            int r7 = r9.A05()
            if (r7 < 0) goto L_0x029e
            int r10 = r9.A05()
            int r9 = r1.A00()
            r7 = 1
            if (r10 < r9) goto L_0x029f
        L_0x029e:
            r7 = 0
        L_0x029f:
            if (r7 == 0) goto L_0x02c2
            int r7 = X.AnonymousClass19T.A0L(r8)
            r0.A03(r8, r7)
            r7 = 1
        L_0x02a9:
            if (r7 != 0) goto L_0x02ba
            r0.A01()
            boolean r7 = r3.mStackFromEnd
            if (r7 == 0) goto L_0x02c0
            int r7 = r1.A00()
            int r7 = r7 + -1
        L_0x02b8:
            r0.A03 = r7
        L_0x02ba:
            X.19h r0 = r3.A0A
            r0.A01 = r6
            goto L_0x007c
        L_0x02c0:
            r7 = 0
            goto L_0x02b8
        L_0x02c2:
            boolean r8 = r3.mLastStackFromEnd
            boolean r7 = r3.mStackFromEnd
            if (r8 != r7) goto L_0x0365
            boolean r7 = r0.A04
            if (r7 == 0) goto L_0x033b
            boolean r7 = r3.A08
            if (r7 == 0) goto L_0x0327
            int r16 = r3.A0c()
            int r17 = r1.A00()
            r15 = 0
            r12 = r3
            r14 = r1
            r13 = r2
            android.view.View r7 = r12.A1x(r13, r14, r15, r16, r17)
        L_0x02e0:
            if (r7 == 0) goto L_0x0365
            int r8 = X.AnonymousClass19T.A0L(r7)
            r0.A02(r7, r8)
            boolean r8 = r1.A08
            if (r8 != 0) goto L_0x031e
            boolean r8 = r18.A1s()
            if (r8 == 0) goto L_0x031e
            X.19k r8 = r3.A06
            int r9 = r8.A0B(r7)
            X.19k r8 = r3.A06
            int r8 = r8.A02()
            if (r9 >= r8) goto L_0x030f
            X.19k r8 = r3.A06
            int r8 = r8.A08(r7)
            X.19k r7 = r3.A06
            int r7 = r7.A06()
            if (r8 >= r7) goto L_0x0310
        L_0x030f:
            r11 = 1
        L_0x0310:
            if (r11 == 0) goto L_0x031e
            boolean r7 = r0.A04
            if (r7 == 0) goto L_0x0320
            X.19k r7 = r3.A06
            int r7 = r7.A02()
        L_0x031c:
            r0.A02 = r7
        L_0x031e:
            r7 = 1
            goto L_0x02a9
        L_0x0320:
            X.19k r7 = r3.A06
            int r7 = r7.A06()
            goto L_0x031c
        L_0x0327:
            int r7 = r3.A0c()
            int r15 = r7 + -1
            int r17 = r1.A00()
            r16 = -1
            r12 = r3
            r14 = r1
            r13 = r2
            android.view.View r7 = r12.A1x(r13, r14, r15, r16, r17)
            goto L_0x02e0
        L_0x033b:
            boolean r7 = r3.A08
            if (r7 == 0) goto L_0x0353
            int r7 = r3.A0c()
            int r15 = r7 + -1
            int r17 = r1.A00()
            r16 = -1
            r12 = r3
            r14 = r1
            r13 = r2
            android.view.View r7 = r12.A1x(r13, r14, r15, r16, r17)
            goto L_0x02e0
        L_0x0353:
            int r16 = r3.A0c()
            int r17 = r1.A00()
            r15 = 0
            r12 = r3
            r14 = r1
            r13 = r2
            android.view.View r7 = r12.A1x(r13, r14, r15, r16, r17)
            goto L_0x02e0
        L_0x0365:
            r7 = 0
            goto L_0x02a9
        L_0x0368:
            X.19k r7 = r3.A06
            int r8 = r7.A06()
            androidx.recyclerview.widget.LinearLayoutManager$SavedState r7 = r3.A05
            int r7 = r7.A00
            goto L_0x0425
        L_0x0374:
            int r7 = r3.A03
            if (r7 != r9) goto L_0x0411
            android.view.View r9 = r3.A0t(r12)
            if (r9 == 0) goto L_0x03f1
            X.19k r7 = r3.A06
            int r8 = r7.A09(r9)
            X.19k r7 = r3.A06
            int r7 = r7.A07()
            if (r8 <= r7) goto L_0x0392
            r0.A01()
            r7 = 1
            goto L_0x025c
        L_0x0392:
            X.19k r7 = r3.A06
            int r8 = r7.A0B(r9)
            X.19k r7 = r3.A06
            int r7 = r7.A06()
            int r8 = r8 - r7
            if (r8 >= 0) goto L_0x03ae
            X.19k r7 = r3.A06
            int r7 = r7.A06()
            r0.A02 = r7
            r0.A04 = r4
            r7 = 1
            goto L_0x025c
        L_0x03ae:
            X.19k r7 = r3.A06
            int r8 = r7.A02()
            X.19k r7 = r3.A06
            int r7 = r7.A08(r9)
            int r8 = r8 - r7
            if (r8 >= 0) goto L_0x03ca
            X.19k r7 = r3.A06
            int r7 = r7.A02()
            r0.A02 = r7
            r0.A04 = r6
            r7 = 1
            goto L_0x025c
        L_0x03ca:
            boolean r7 = r0.A04
            if (r7 == 0) goto L_0x03ea
            X.19k r7 = r3.A06
            int r10 = r7.A08(r9)
            X.19k r9 = r3.A06
            int r8 = r9.A00
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r7 != r8) goto L_0x03e2
            r8 = 0
        L_0x03dd:
            int r10 = r10 + r8
        L_0x03de:
            r0.A02 = r10
            goto L_0x025b
        L_0x03e2:
            int r8 = r9.A07()
            int r7 = r9.A00
            int r8 = r8 - r7
            goto L_0x03dd
        L_0x03ea:
            X.19k r7 = r3.A06
            int r10 = r7.A0B(r9)
            goto L_0x03de
        L_0x03f1:
            int r7 = r3.A0c()
            if (r7 <= 0) goto L_0x040c
            android.view.View r7 = r3.A0u(r4)
            int r9 = X.AnonymousClass19T.A0L(r7)
            int r7 = r3.A02
            r8 = 0
            if (r7 >= r9) goto L_0x0405
            r8 = 1
        L_0x0405:
            boolean r7 = r3.A08
            if (r8 != r7) goto L_0x040a
            r10 = 1
        L_0x040a:
            r0.A04 = r10
        L_0x040c:
            r0.A01()
            goto L_0x025b
        L_0x0411:
            r0.A04 = r11
            X.19k r7 = r3.A06
            if (r11 == 0) goto L_0x041f
            int r8 = r7.A02()
            int r7 = r3.A03
            goto L_0x0258
        L_0x041f:
            int r8 = r7.A06()
            int r7 = r3.A03
        L_0x0425:
            int r8 = r8 + r7
            r0.A02 = r8
            goto L_0x025b
        L_0x042a:
            r3.A02 = r5
            r3.A03 = r9
        L_0x042e:
            r7 = 0
            goto L_0x025c
        L_0x0431:
            X.1AA r0 = r3.A04
            r0.A0A = r15
            if (r10 <= 0) goto L_0x045b
            boolean r0 = r3.A08
            if (r0 == 0) goto L_0x049f
            int r0 = r3.A0c()
            int r0 = r0 + -1
        L_0x0441:
            android.view.View r0 = r3.A0u(r0)
            int r0 = X.AnonymousClass19T.A0L(r0)
            r3.A0C(r0, r14)
            X.1AA r5 = r3.A04
            r5.A05 = r10
            r5.A03 = r4
            r0 = 0
            X.AnonymousClass1AA.A00(r5, r0)
            X.1AA r0 = r3.A04
            r3.A06(r2, r0, r1, r4)
        L_0x045b:
            if (r9 <= 0) goto L_0x047c
            boolean r0 = r3.A08
            if (r0 == 0) goto L_0x0498
            r0 = 0
        L_0x0462:
            android.view.View r0 = r3.A0u(r0)
            int r0 = X.AnonymousClass19T.A0L(r0)
            r3.A0B(r0, r13)
            X.1AA r5 = r3.A04
            r5.A05 = r9
            r5.A03 = r4
            r0 = 0
            X.AnonymousClass1AA.A00(r5, r0)
            X.1AA r0 = r3.A04
            r3.A06(r2, r0, r1, r4)
        L_0x047c:
            X.1AA r2 = r3.A04
            r0 = 0
            r2.A0A = r0
        L_0x0481:
            boolean r0 = r1.A08
            if (r0 != 0) goto L_0x0492
            X.19k r1 = r3.A06
            int r0 = r1.A07()
            r1.A00 = r0
        L_0x048d:
            boolean r0 = r3.mStackFromEnd
            r3.mLastStackFromEnd = r0
            return
        L_0x0492:
            X.19h r0 = r3.A0A
            r0.A00()
            goto L_0x048d
        L_0x0498:
            int r0 = r3.A0c()
            int r0 = r0 + -1
            goto L_0x0462
        L_0x049f:
            r0 = 0
            goto L_0x0441
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19S.A1m(X.0vp, X.0wD):void");
    }

    public void A1q(RecyclerView recyclerView, C15930wD r4, int i) {
        C27360DbK dbK = new C27360DbK(recyclerView.getContext());
        dbK.A00 = i;
        A1I(dbK);
    }

    public void A1r(String str) {
        if (this.A05 == null) {
            super.A1r(str);
        }
    }

    public boolean A1s() {
        if (this.A05 == null && this.mLastStackFromEnd == this.mStackFromEnd) {
            return true;
        }
        return false;
    }

    public View A1x(C15740vp r9, C15930wD r10, int i, int i2, int i3) {
        View view;
        if (!(this instanceof C24131Sk)) {
            A1y();
            int A062 = this.A06.A06();
            int A022 = this.A06.A02();
            int i4 = -1;
            if (i2 > i) {
                i4 = 1;
            }
            View view2 = null;
            View view3 = null;
            while (i != i2) {
                view = A0u(i);
                int A0L = AnonymousClass19T.A0L(view);
                if (A0L >= 0 && A0L < i3) {
                    if (((AnonymousClass1R7) view.getLayoutParams()).mViewHolder.A0E()) {
                        if (view3 == null) {
                            view3 = view;
                        }
                    } else if (this.A06.A0B(view) >= A022 || this.A06.A08(view) < A062) {
                        if (view2 == null) {
                            view2 = view;
                        }
                    }
                }
                i += i4;
            }
            if (view2 == null) {
                return view3;
            }
            return view2;
        }
        C24131Sk r4 = (C24131Sk) this;
        r4.A1y();
        int A063 = r4.A06.A06();
        int A023 = r4.A06.A02();
        int i5 = -1;
        if (i2 > i) {
            i5 = 1;
        }
        View view4 = null;
        View view5 = null;
        while (i != i2) {
            view = r4.A0u(i);
            int A0L2 = AnonymousClass19T.A0L(view);
            if (A0L2 >= 0 && A0L2 < i3 && C24131Sk.A01(r4, r9, r10, A0L2) == 0) {
                if (((AnonymousClass1R7) view.getLayoutParams()).mViewHolder.A0E()) {
                    if (view5 == null) {
                        view5 = view;
                    }
                } else if (r4.A06.A0B(view) >= A023 || r4.A06.A08(view) < A063) {
                    if (view4 == null) {
                        view4 = view;
                    }
                }
            }
            i += i5;
        }
        if (view4 == null) {
            return view5;
        }
        return view4;
        return view;
    }

    public void A1y() {
        if (this.A04 == null) {
            this.A04 = new AnonymousClass1AA();
        }
    }

    public void A1z(int i) {
        if (i == 0 || i == 1) {
            A1r(null);
            if (i != this.A01 || this.A06 == null) {
                C197219k A002 = C197219k.A00(this, i);
                this.A06 = A002;
                this.A0A.A00 = A002;
                this.A01 = i;
                A0y();
                return;
            }
            return;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("invalid orientation:", i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x010d, code lost:
        if (r14 >= r8.A00()) goto L_0x010f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A21(X.C15740vp r20, X.C15930wD r21, X.AnonymousClass1AA r22, X.C197119j r23) {
        /*
            r19 = this;
            r10 = r19
            boolean r0 = r10 instanceof X.C24131Sk
            r12 = r20
            r7 = r23
            r2 = r22
            if (r0 != 0) goto L_0x00c0
            android.view.View r8 = r2.A01(r12)
            r6 = 1
            if (r8 != 0) goto L_0x0016
            r7.A01 = r6
            return
        L_0x0016:
            android.view.ViewGroup$LayoutParams r5 = r8.getLayoutParams()
            X.1R7 r5 = (X.AnonymousClass1R7) r5
            java.util.List r0 = r2.A0A
            r9 = -1
            r4 = 0
            if (r0 != 0) goto L_0x00ab
            boolean r3 = r10.A08
            int r1 = r2.A07
            r0 = 0
            if (r1 != r9) goto L_0x002a
            r0 = 1
        L_0x002a:
            if (r3 != r0) goto L_0x00a7
            r10.A16(r8)
        L_0x002f:
            r10.A19(r8, r4, r4)
            X.19k r0 = r10.A06
            int r0 = r0.A09(r8)
            r7.A00 = r0
            int r0 = r10.A01
            if (r0 != r6) goto L_0x008a
            boolean r0 = r10.A25()
            if (r0 == 0) goto L_0x007e
            int r11 = r10.A04
            int r0 = r10.A0g()
            int r11 = r11 - r0
            X.19k r0 = r10.A06
            int r0 = r0.A0A(r8)
            int r1 = r11 - r0
        L_0x0053:
            int r0 = r2.A07
            if (r0 != r9) goto L_0x0077
            int r3 = r2.A08
            int r0 = r7.A00
            int r4 = r3 - r0
        L_0x005d:
            X.AnonymousClass19T.A0N(r8, r1, r4, r11, r3)
            X.1o8 r0 = r5.mViewHolder
            boolean r0 = r0.A0E()
            if (r0 != 0) goto L_0x006e
            boolean r0 = r5.A00()
            if (r0 == 0) goto L_0x0070
        L_0x006e:
            r7.A03 = r6
        L_0x0070:
            boolean r0 = r8.hasFocusable()
            r7.A02 = r0
            return
        L_0x0077:
            int r4 = r2.A08
            int r0 = r7.A00
            int r3 = r4 + r0
            goto L_0x005d
        L_0x007e:
            int r1 = r10.A0f()
            X.19k r0 = r10.A06
            int r11 = r0.A0A(r8)
            int r11 = r11 + r1
            goto L_0x0053
        L_0x008a:
            int r4 = r10.A0h()
            X.19k r0 = r10.A06
            int r3 = r0.A0A(r8)
            int r3 = r3 + r4
            int r0 = r2.A07
            if (r0 != r9) goto L_0x00a0
            int r11 = r2.A08
            int r0 = r7.A00
            int r1 = r11 - r0
            goto L_0x005d
        L_0x00a0:
            int r1 = r2.A08
            int r0 = r7.A00
            int r11 = r1 + r0
            goto L_0x005d
        L_0x00a7:
            r10.A18(r8, r4)
            goto L_0x002f
        L_0x00ab:
            boolean r3 = r10.A08
            int r1 = r2.A07
            r0 = 0
            if (r1 != r9) goto L_0x00b3
            r0 = 1
        L_0x00b3:
            if (r3 != r0) goto L_0x00bb
            r1 = -1
            X.AnonymousClass19T.A0O(r10, r8, r1, r6)
            goto L_0x002f
        L_0x00bb:
            X.AnonymousClass19T.A0O(r10, r8, r4, r6)
            goto L_0x002f
        L_0x00c0:
            r6 = r10
            X.1Sk r6 = (X.C24131Sk) r6
            X.19k r0 = r6.A06
            int r11 = r0.A05()
            r0 = 1073741824(0x40000000, float:2.0)
            r5 = 1
            r10 = 0
            r18 = 0
            if (r11 == r0) goto L_0x00d3
            r18 = 1
        L_0x00d3:
            int r0 = r6.A0c()
            if (r0 <= 0) goto L_0x012c
            int[] r1 = r6.A03
            int r0 = r6.A01
            r9 = r1[r0]
        L_0x00df:
            if (r18 == 0) goto L_0x00e4
            X.C24131Sk.A09(r6)
        L_0x00e4:
            int r0 = r2.A06
            r17 = 0
            if (r0 != r5) goto L_0x00ec
            r17 = 1
        L_0x00ec:
            int r15 = r6.A01
            r8 = r21
            if (r17 != 0) goto L_0x00ff
            int r0 = r2.A04
            int r15 = X.C24131Sk.A01(r6, r12, r8, r0)
            int r0 = r2.A04
            int r0 = X.C24131Sk.A02(r6, r12, r8, r0)
            int r15 = r15 + r0
        L_0x00ff:
            r4 = 0
        L_0x0100:
            int r0 = r6.A01
            if (r4 >= r0) goto L_0x0157
            int r14 = r2.A04
            if (r14 < 0) goto L_0x010f
            int r1 = r8.A00()
            r0 = 1
            if (r14 < r1) goto L_0x0110
        L_0x010f:
            r0 = 0
        L_0x0110:
            if (r0 == 0) goto L_0x0157
            if (r15 <= 0) goto L_0x0157
            int r13 = X.C24131Sk.A02(r6, r12, r8, r14)
            int r3 = r6.A01
            if (r13 > r3) goto L_0x012e
            int r15 = r15 - r13
            if (r15 < 0) goto L_0x0157
            android.view.View r1 = r2.A01(r12)
            if (r1 == 0) goto L_0x0157
            android.view.View[] r0 = r6.A04
            r0[r4] = r1
            int r4 = r4 + 1
            goto L_0x0100
        L_0x012c:
            r9 = 0
            goto L_0x00df
        L_0x012e:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Item at position "
            r1.<init>(r0)
            r1.append(r14)
            java.lang.String r0 = " requires "
            r1.append(r0)
            r1.append(r13)
            java.lang.String r0 = " spans but GridLayoutManager has only "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " spans."
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0157:
            if (r4 != 0) goto L_0x015c
            r7.A01 = r5
            return
        L_0x015c:
            r16 = 0
            r14 = 0
            r13 = -1
            int r3 = r4 + -1
            r15 = -1
            if (r17 == 0) goto L_0x0168
            r13 = r4
            r3 = 0
            r15 = 1
        L_0x0168:
            if (r3 == r13) goto L_0x0183
            android.view.View[] r0 = r6.A04
            r0 = r0[r3]
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            X.Cau r1 = (X.C25158Cau) r1
            int r0 = X.AnonymousClass19T.A0L(r0)
            int r0 = X.C24131Sk.A02(r6, r12, r8, r0)
            r1.A01 = r0
            r1.A00 = r14
            int r14 = r14 + r0
            int r3 = r3 + r15
            goto L_0x0168
        L_0x0183:
            r13 = 0
            r8 = 0
        L_0x0185:
            if (r13 >= r4) goto L_0x01d2
            android.view.View[] r0 = r6.A04
            r12 = r0[r13]
            java.util.List r0 = r2.A0A
            if (r0 != 0) goto L_0x01c7
            if (r17 == 0) goto L_0x01c3
            r6.A16(r12)
        L_0x0194:
            android.graphics.Rect r0 = r6.A05
            r6.A1A(r12, r0)
            X.C24131Sk.A0C(r6, r12, r11, r10)
            X.19k r0 = r6.A06
            int r0 = r0.A09(r12)
            if (r0 <= r8) goto L_0x01a5
            r8 = r0
        L_0x01a5:
            android.view.ViewGroup$LayoutParams r10 = r12.getLayoutParams()
            X.Cau r10 = (X.C25158Cau) r10
            r3 = 1065353216(0x3f800000, float:1.0)
            X.19k r0 = r6.A06
            int r0 = r0.A0A(r12)
            float r1 = (float) r0
            float r1 = r1 * r3
            int r0 = r10.A01
            float r0 = (float) r0
            float r1 = r1 / r0
            int r0 = (r1 > r16 ? 1 : (r1 == r16 ? 0 : -1))
            if (r0 <= 0) goto L_0x01bf
            r16 = r1
        L_0x01bf:
            int r13 = r13 + 1
            r10 = 0
            goto L_0x0185
        L_0x01c3:
            r6.A18(r12, r10)
            goto L_0x0194
        L_0x01c7:
            if (r17 == 0) goto L_0x01ce
            r1 = -1
            X.AnonymousClass19T.A0O(r6, r12, r1, r5)
            goto L_0x0194
        L_0x01ce:
            X.AnonymousClass19T.A0O(r6, r12, r10, r5)
            goto L_0x0194
        L_0x01d2:
            if (r18 == 0) goto L_0x01fd
            int r0 = r6.A01
            float r0 = (float) r0
            float r16 = r16 * r0
            int r0 = java.lang.Math.round(r16)
            int r0 = java.lang.Math.max(r0, r9)
            X.C24131Sk.A0A(r6, r0)
            r3 = 0
            r8 = 0
        L_0x01e6:
            if (r3 >= r4) goto L_0x01fd
            android.view.View[] r0 = r6.A04
            r1 = r0[r3]
            r0 = 1073741824(0x40000000, float:2.0)
            X.C24131Sk.A0C(r6, r1, r0, r5)
            X.19k r0 = r6.A06
            int r0 = r0.A09(r1)
            if (r0 <= r8) goto L_0x01fa
            r8 = r0
        L_0x01fa:
            int r3 = r3 + 1
            goto L_0x01e6
        L_0x01fd:
            r11 = 0
        L_0x01fe:
            if (r11 >= r4) goto L_0x025b
            android.view.View[] r0 = r6.A04
            r10 = r0[r11]
            X.19k r0 = r6.A06
            int r0 = r0.A09(r10)
            if (r0 == r8) goto L_0x0248
            android.view.ViewGroup$LayoutParams r15 = r10.getLayoutParams()
            X.Cau r15 = (X.C25158Cau) r15
            android.graphics.Rect r1 = r15.A02
            int r13 = r1.top
            int r0 = r1.bottom
            int r13 = r13 + r0
            int r0 = r15.topMargin
            int r13 = r13 + r0
            int r0 = r15.bottomMargin
            int r13 = r13 + r0
            int r14 = r1.left
            int r0 = r1.right
            int r14 = r14 + r0
            int r0 = r15.leftMargin
            int r14 = r14 + r0
            int r0 = r15.rightMargin
            int r14 = r14 + r0
            int r1 = r15.A00
            int r0 = r15.A01
            int r12 = X.C24131Sk.A00(r6, r1, r0)
            int r0 = r6.A01
            if (r0 != r5) goto L_0x024b
            int r1 = r15.width
            r3 = 1073741824(0x40000000, float:2.0)
            r0 = 0
            int r1 = X.AnonymousClass19T.A0I(r12, r3, r14, r1, r0)
            int r0 = r8 - r13
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r3)
        L_0x0245:
            X.C24131Sk.A0B(r6, r10, r1, r0, r5)
        L_0x0248:
            int r11 = r11 + 1
            goto L_0x01fe
        L_0x024b:
            r9 = 1073741824(0x40000000, float:2.0)
            r3 = 0
            int r0 = r8 - r14
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            int r0 = r15.height
            int r0 = X.AnonymousClass19T.A0I(r12, r9, r13, r0, r3)
            goto L_0x0245
        L_0x025b:
            r9 = 0
            r7.A00 = r8
            int r0 = r6.A01
            r3 = -1
            if (r0 != r5) goto L_0x02e3
            int r1 = r2.A07
            int r0 = r2.A08
            if (r1 != r3) goto L_0x02df
            int r10 = r0 - r8
            r1 = r0
        L_0x026c:
            r3 = 0
            r2 = 0
        L_0x026e:
            if (r9 >= r4) goto L_0x02f6
            android.view.View[] r0 = r6.A04
            r11 = r0[r9]
            android.view.ViewGroup$LayoutParams r8 = r11.getLayoutParams()
            X.Cau r8 = (X.C25158Cau) r8
            int r0 = r6.A01
            if (r0 != r5) goto L_0x02cc
            boolean r0 = r6.A25()
            if (r0 == 0) goto L_0x02b9
            int r2 = r6.A0f()
            int[] r12 = r6.A03
            int r3 = r6.A01
            int r0 = r8.A00
            int r3 = r3 - r0
            r0 = r12[r3]
            int r2 = r2 + r0
            X.19k r0 = r6.A06
            int r0 = r0.A0A(r11)
            int r3 = r2 - r0
        L_0x029a:
            X.AnonymousClass19T.A0N(r11, r3, r10, r2, r1)
            X.1o8 r0 = r8.mViewHolder
            boolean r0 = r0.A0E()
            if (r0 != 0) goto L_0x02ab
            boolean r0 = r8.A00()
            if (r0 == 0) goto L_0x02ad
        L_0x02ab:
            r7.A03 = r5
        L_0x02ad:
            boolean r8 = r7.A02
            boolean r0 = r11.hasFocusable()
            r8 = r8 | r0
            r7.A02 = r8
            int r9 = r9 + 1
            goto L_0x026e
        L_0x02b9:
            int r3 = r6.A0f()
            int[] r2 = r6.A03
            int r0 = r8.A00
            r0 = r2[r0]
            int r3 = r3 + r0
            X.19k r0 = r6.A06
            int r2 = r0.A0A(r11)
            int r2 = r2 + r3
            goto L_0x029a
        L_0x02cc:
            int r10 = r6.A0h()
            int[] r1 = r6.A03
            int r0 = r8.A00
            r0 = r1[r0]
            int r10 = r10 + r0
            X.19k r0 = r6.A06
            int r1 = r0.A0A(r11)
            int r1 = r1 + r10
            goto L_0x029a
        L_0x02df:
            int r1 = r0 + r8
            r10 = r0
            goto L_0x026c
        L_0x02e3:
            int r0 = r2.A07
            int r2 = r2.A08
            if (r0 != r3) goto L_0x02ee
            int r3 = r2 - r8
            r10 = 0
            r1 = 0
            goto L_0x026e
        L_0x02ee:
            int r0 = r2 + r8
            r10 = 0
            r1 = 0
            r3 = r2
            r2 = r0
            goto L_0x026e
        L_0x02f6:
            android.view.View[] r1 = r6.A04
            r0 = 0
            java.util.Arrays.fill(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19S.A21(X.0vp, X.0wD, X.1AA, X.19j):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        if (r2 >= r8.A00()) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A22(X.C15930wD r8, X.AnonymousClass1AA r9, X.C37131uk r10) {
        /*
            r7 = this;
            boolean r0 = r7 instanceof X.C24131Sk
            if (r0 != 0) goto L_0x0019
            int r2 = r9.A04
            if (r2 < 0) goto L_0x0018
            int r0 = r8.A00()
            if (r2 >= r0) goto L_0x0018
            r1 = 0
            int r0 = r9.A09
            int r0 = java.lang.Math.max(r1, r0)
            r10.ANZ(r2, r0)
        L_0x0018:
            return
        L_0x0019:
            r6 = r7
            X.1Sk r6 = (X.C24131Sk) r6
            int r5 = r6.A01
            r4 = 0
            r3 = 0
        L_0x0020:
            int r0 = r6.A01
            if (r3 >= r0) goto L_0x0018
            int r2 = r9.A04
            if (r2 < 0) goto L_0x002f
            int r1 = r8.A00()
            r0 = 1
            if (r2 < r1) goto L_0x0030
        L_0x002f:
            r0 = 0
        L_0x0030:
            if (r0 == 0) goto L_0x0018
            if (r5 <= 0) goto L_0x0018
            int r0 = r9.A09
            int r0 = java.lang.Math.max(r4, r0)
            r10.ANZ(r2, r0)
            X.Cvd r0 = r6.A02
            int r0 = r0.A00(r2)
            int r5 = r5 - r0
            int r1 = r9.A04
            int r0 = r9.A06
            int r1 = r1 + r0
            r9.A04 = r1
            int r3 = r3 + 1
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19S.A22(X.0wD, X.1AA, X.1uk):void");
    }

    public boolean A25() {
        if (C15320v6.getLayoutDirection(this.A08) != 1) {
            return false;
        }
        return true;
    }

    public void By1(View view, View view2, int i, int i2) {
        int i3;
        int i4;
        A1r("Cannot drop a view during a scroll or layout calculation");
        A1y();
        A0D(this);
        int A0L = AnonymousClass19T.A0L(view);
        int A0L2 = AnonymousClass19T.A0L(view2);
        char c = 65535;
        if (A0L < A0L2) {
            c = 1;
        }
        if (this.A08) {
            if (c == 1) {
                C4c(A0L2, this.A06.A02() - (this.A06.A0B(view2) + this.A06.A09(view)));
                return;
            } else {
                i3 = this.A06.A02();
                i4 = this.A06.A08(view2);
            }
        } else if (c == 65535) {
            C4c(A0L2, this.A06.A0B(view2));
            return;
        } else {
            i3 = this.A06.A08(view2);
            i4 = this.A06.A09(view);
        }
        C4c(A0L2, i3 - i4);
    }

    public void C4c(int i, int i2) {
        this.A02 = i;
        this.A03 = i2;
        LinearLayoutManager$SavedState linearLayoutManager$SavedState = this.A05;
        if (linearLayoutManager$SavedState != null) {
            linearLayoutManager$SavedState.A01 = -1;
        }
        A0y();
    }

    private int A00(int i, C15740vp r7, C15930wD r8) {
        if (!(A0c() == 0 || i == 0)) {
            this.A04.A02 = true;
            A1y();
            int i2 = -1;
            if (i > 0) {
                i2 = 1;
            }
            int abs = Math.abs(i);
            A0E(this, i2, abs, true, r8);
            AnonymousClass1AA r0 = this.A04;
            int A062 = r0.A09 + A06(r7, r0, r8, false);
            if (A062 >= 0) {
                if (abs > A062) {
                    i = i2 * A062;
                }
                this.A06.A0E(-i);
                this.A04.A00 = i;
                return i;
            }
        }
        return 0;
    }

    public static int A03(AnonymousClass19S r5, C15930wD r6) {
        if (r5.A0c() == 0) {
            return 0;
        }
        r5.A1y();
        C197219k r4 = r5.A06;
        View A0A2 = r5.A0A(!r5.A09, true);
        View A092 = r5.A09(!r5.A09, true);
        boolean z = r5.A09;
        if (r5.A0c() == 0 || r6.A00() == 0 || A0A2 == null || A092 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(AnonymousClass19T.A0L(A0A2) - AnonymousClass19T.A0L(A092)) + 1;
        }
        return Math.min(r4.A07(), r4.A08(A092) - r4.A0B(A0A2));
    }

    private View A07(int i, int i2) {
        char c;
        C196719f r0;
        A1y();
        if (i2 > i) {
            c = 1;
        } else {
            c = 0;
            if (i2 < i) {
                c = 65535;
            }
        }
        if (c == 0) {
            return A0u(i);
        }
        int A0B2 = this.A06.A0B(A0u(i));
        int A062 = this.A06.A06();
        int i3 = AnonymousClass1Y3.AYS;
        int i4 = AnonymousClass1Y3.AXr;
        if (A0B2 < A062) {
            i3 = 16644;
            i4 = 16388;
        }
        if (this.A01 == 0) {
            r0 = this.A09;
        } else {
            r0 = this.A0A;
        }
        return r0.A00(i, i2, i3, i4);
    }

    private View A08(int i, int i2, boolean z, boolean z2) {
        C196719f r0;
        A1y();
        int i3 = AnonymousClass1Y3.A2Y;
        int i4 = AnonymousClass1Y3.A2Y;
        if (z) {
            i4 = 24579;
        }
        if (!z2) {
            i3 = 0;
        }
        if (this.A01 == 0) {
            r0 = this.A09;
        } else {
            r0 = this.A0A;
        }
        return r0.A00(i, i2, i4, i3);
    }

    public View A0t(int i) {
        int A0c = A0c();
        if (A0c == 0) {
            return null;
        }
        int A0L = i - AnonymousClass19T.A0L(A0u(0));
        if (A0L >= 0 && A0L < A0c) {
            View A0u = A0u(A0L);
            if (AnonymousClass19T.A0L(A0u) == i) {
                return A0u;
            }
        }
        return super.A0t(i);
    }

    public View A1f(View view, int i, C15740vp r9, C15930wD r10) {
        int A1w;
        View A072;
        int i2;
        A0D(this);
        if (!(A0c() == 0 || (A1w = A1w(i)) == Integer.MIN_VALUE)) {
            A1y();
            A1y();
            A0E(this, A1w, (int) (((float) this.A06.A07()) * 0.33333334f), false, r10);
            AnonymousClass1AA r1 = this.A04;
            r1.A09 = Integer.MIN_VALUE;
            r1.A02 = false;
            A06(r9, r1, r10, true);
            if (A1w == -1) {
                if (!this.A08) {
                    A072 = A07(0, A0c());
                }
                A072 = A07(A0c() - 1, -1);
            } else {
                if (this.A08) {
                    A072 = A07(0, A0c());
                }
                A072 = A07(A0c() - 1, -1);
            }
            if (A1w != -1 ? !this.A08 : this.A08) {
                i2 = A0c() - 1;
            } else {
                i2 = 0;
            }
            View A0u = A0u(i2);
            if (!A0u.hasFocusable()) {
                return A072;
            }
            if (A072 != null) {
                return A0u;
            }
        }
        return null;
    }

    public void A1l(AccessibilityEvent accessibilityEvent) {
        super.A1l(accessibilityEvent);
        if (A0c() > 0) {
            accessibilityEvent.setFromIndex(A1u());
            accessibilityEvent.setToIndex(AZs());
        }
    }

    public void A1o(C15930wD r2) {
        super.A1o(r2);
        this.A05 = null;
        this.A02 = -1;
        this.A03 = Integer.MIN_VALUE;
        this.A0A.A00();
    }

    public void A1p(RecyclerView recyclerView, C15740vp r3) {
        super.A1p(recyclerView, r3);
        if (0 != 0) {
            A1F(r3);
            r3.A04.clear();
            C15740vp.A01(r3);
        }
    }

    public int A1t() {
        View A082 = A08(0, A0c(), true, false);
        if (A082 == null) {
            return -1;
        }
        return AnonymousClass19T.A0L(A082);
    }

    public int A1u() {
        View A082 = A08(0, A0c(), false, true);
        if (A082 == null) {
            return -1;
        }
        return AnonymousClass19T.A0L(A082);
    }

    public int A1v() {
        View A082 = A08(A0c() - 1, -1, true, false);
        if (A082 != null) {
            return AnonymousClass19T.A0L(A082);
        }
        return -1;
    }

    public PointF ATk(int i) {
        if (A0c() == 0) {
            return null;
        }
        boolean z = false;
        int i2 = 1;
        if (i < AnonymousClass19T.A0L(A0u(0))) {
            z = true;
        }
        if (z != this.A08) {
            i2 = -1;
        }
        if (this.A01 == 0) {
            return new PointF((float) i2, 0.0f);
        }
        return new PointF(0.0f, (float) i2);
    }

    public int AZs() {
        View A082 = A08(A0c() - 1, -1, false, true);
        if (A082 != null) {
            return AnonymousClass19T.A0L(A082);
        }
        return -1;
    }

    public AnonymousClass19S() {
        this(1, false);
    }

    public AnonymousClass19S(int i, boolean z) {
        this.A01 = 1;
        this.A07 = false;
        this.A08 = false;
        this.mStackFromEnd = false;
        this.A09 = true;
        this.A02 = -1;
        this.A03 = Integer.MIN_VALUE;
        this.A05 = null;
        this.A0A = new C196919h();
        this.A0B = new C197119j();
        this.A00 = 2;
        A1z(i);
        A23(z);
    }
}
