package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0Pa  reason: invalid class name and case insensitive filesystem */
public final class C03680Pa extends C03160Kg {
    public long A00() {
        return -3940877017738808059L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        AnonymousClass0Fc r32 = (AnonymousClass0Fc) r3;
        dataOutput.writeLong(r32.rcharBytes);
        dataOutput.writeLong(r32.wcharBytes);
        dataOutput.writeLong(r32.syscrCount);
        dataOutput.writeLong(r32.syscwCount);
        dataOutput.writeLong(r32.readBytes);
        dataOutput.writeLong(r32.writeBytes);
        dataOutput.writeLong(r32.cancelledWriteBytes);
        dataOutput.writeLong(r32.majorFaults);
        dataOutput.writeLong(r32.blkIoTicks);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        AnonymousClass0Fc r32 = (AnonymousClass0Fc) r3;
        r32.rcharBytes = dataInput.readLong();
        r32.wcharBytes = dataInput.readLong();
        r32.syscrCount = dataInput.readLong();
        r32.syscwCount = dataInput.readLong();
        r32.readBytes = dataInput.readLong();
        r32.writeBytes = dataInput.readLong();
        r32.cancelledWriteBytes = dataInput.readLong();
        r32.majorFaults = dataInput.readLong();
        r32.blkIoTicks = dataInput.readLong();
        return true;
    }
}
