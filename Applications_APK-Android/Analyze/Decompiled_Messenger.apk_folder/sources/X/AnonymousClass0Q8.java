package X;

/* renamed from: X.0Q8  reason: invalid class name */
public final class AnonymousClass0Q8 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.voltron.runtime.AppModuleStateCache$ListenerSender$1";
    public final /* synthetic */ AnonymousClass0Q7 A00;
    public final /* synthetic */ Integer A01;
    public final /* synthetic */ String A02;

    public AnonymousClass0Q8(AnonymousClass0Q7 r1, String str, Integer num) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = num;
    }

    public void run() {
        this.A00.A00.Bpw(this.A02, this.A01);
    }
}
