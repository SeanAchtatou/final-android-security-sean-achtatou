package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

/* renamed from: X.0lQ  reason: invalid class name */
public final class AnonymousClass0lQ {
    public final Context A00;

    public static final AnonymousClass0lQ A00(AnonymousClass1XY r2) {
        return new AnonymousClass0lQ(AnonymousClass1YA.A00(r2));
    }

    public AnonymousClass0lQ(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.getParent() instanceof Activity) {
                context = activity.getParent();
            }
        }
        this.A00 = context;
    }

    public boolean A01(Intent intent, ServiceConnection serviceConnection, int i) {
        C005505z.A05("BindService(%s)", intent.getComponent().getClassName(), -784651853);
        try {
            return C006406k.A02(this.A00, intent, serviceConnection, i, -388183216);
        } finally {
            C005505z.A00(-442117917);
        }
    }
}
