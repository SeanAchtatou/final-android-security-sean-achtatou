package X;

import android.content.Intent;
import org.json.JSONObject;

/* renamed from: X.0PW  reason: invalid class name */
public final class AnonymousClass0PW extends AnonymousClass0SE {
    public long A00;
    public final long A01;

    public static AnonymousClass0PW A00(Object obj) {
        try {
            String str = (String) obj;
            if (str.length() > 50000) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            return new AnonymousClass0PW(Intent.parseUri(jSONObject.getString("key_intent"), 0), jSONObject.getString("key_notifid"), jSONObject.getLong("key_timestamp_received"), jSONObject.getLong("key_timestamp_last_retried"));
        } catch (Exception unused) {
            return null;
        }
    }

    public String A01() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("key_intent", this.A00.toUri(0));
            jSONObject.putOpt("key_notifid", this.A01);
            jSONObject.putOpt("key_timestamp_received", Long.valueOf(this.A01));
            jSONObject.putOpt("key_timestamp_last_retried", Long.valueOf(this.A00));
            return jSONObject.toString();
        } catch (Exception unused) {
            return null;
        }
    }

    public AnonymousClass0PW(Intent intent, String str, long j, long j2) {
        super(intent, str);
        this.A01 = j;
        this.A00 = j2;
    }
}
