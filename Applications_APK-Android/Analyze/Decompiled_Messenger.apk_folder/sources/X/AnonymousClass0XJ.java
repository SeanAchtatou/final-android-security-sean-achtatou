package X;

import com.facebook.auth.credentials.UserTokenCredentials;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.common.util.TriState;
import com.facebook.inject.InjectorModule;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.base.Objects;

@InjectorModule
/* renamed from: X.0XJ  reason: invalid class name */
public final class AnonymousClass0XJ extends AnonymousClass0UV {
    private static C04470Uu A00;
    private static final Object A01 = new Object();
    private static final Object A02 = new Object();
    private static volatile C05920aY A03;

    public static final C05920aY A02(AnonymousClass1XY r7) {
        if (A03 == null) {
            synchronized (A01) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A03 = new C05900aW(AnonymousClass0XN.A01(applicationInjector), AnonymousClass1YA.A00(applicationInjector), C04750Wa.A01(applicationInjector), AnonymousClass0WA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C05920aY A03(AnonymousClass1XY r7) {
        C05920aY r0;
        synchronized (A02) {
            C04470Uu A002 = C04470Uu.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r7)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C05900aW(AnonymousClass0XN.A01(r02), AnonymousClass1YA.A00(r02), C04750Wa.A01(r02), AnonymousClass0WA.A00(r02));
                }
                C04470Uu r1 = A00;
                r0 = (C05920aY) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C04310Tq A0H(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AZO, r1);
    }

    public static final C04310Tq A0I(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.B7T, r1);
    }

    public static final C04310Tq A0J(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.A20, r1);
    }

    public static final C04310Tq A0K(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.Awq, r1);
    }

    public static final C04310Tq A0L(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AjY, r1);
    }

    public static final UserTokenCredentials A00(AnonymousClass1XY r2) {
        ViewerContext A08 = AnonymousClass0XN.A01(r2).A08();
        if (A08 != null) {
            return new UserTokenCredentials(A08.mUserId, A08.mAuthToken);
        }
        return null;
    }

    public static final ViewerContext A01(AnonymousClass1XY r0) {
        return AnonymousClass0XL.A01(r0).B9R();
    }

    public static final TriState A04(AnonymousClass1XY r0) {
        User user = (User) AnonymousClass0WY.A01(r0).get();
        if (user == null) {
            return TriState.UNSET;
        }
        if (user.A14) {
            return TriState.YES;
        }
        return TriState.NO;
    }

    public static final TriState A05(AnonymousClass1XY r1) {
        AnonymousClass0XN A002 = AnonymousClass0XM.A00(r1);
        if (A002.A0I()) {
            return TriState.valueOf(A002.A09().A1c);
        }
        return TriState.UNSET;
    }

    public static final User A06(AnonymousClass1XY r5) {
        ViewerContext A002 = C10580kT.A00(r5);
        C04310Tq A012 = AnonymousClass0WY.A01(r5);
        C001500z A05 = AnonymousClass0UU.A05(r5);
        if (A05 != C001500z.A08) {
            User user = (User) A012.get();
            if (user == null) {
                return null;
            }
            if (Objects.equal(user.A0j, A002.mUserId)) {
                return user;
            }
            throw new IllegalStateException("viewer context id and logged in user id should always be the same in " + A05);
        }
        throw new IllegalStateException("cannot use default viewer context user provider for Page Manager");
    }

    public static final UserKey A07(AnonymousClass1XY r0) {
        User A002 = AnonymousClass0WY.A00(r0);
        if (A002 != null) {
            return A002.A0Q;
        }
        return null;
    }

    public static final UserKey A08(AnonymousClass1XY r0) {
        User A002 = AnonymousClass0WY.A00(r0);
        if (A002 != null) {
            return A002.A0Q;
        }
        return null;
    }

    public static final UserKey A09(AnonymousClass1XY r2) {
        ViewerContext A002 = C10580kT.A00(r2);
        if (A002 != null) {
            return new UserKey(C25651aB.A03, A002.mUserId);
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        if (r0 != false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A0A(X.AnonymousClass1XY r1) {
        /*
            com.facebook.user.model.User r1 = X.AnonymousClass0WY.A00(r1)
            if (r1 != 0) goto L_0x0011
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            boolean r1 = r0.booleanValue()
        L_0x000c:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            return r0
        L_0x0011:
            boolean r0 = r1.A1c
            if (r0 == 0) goto L_0x001a
            boolean r0 = r1.A1Q
            r1 = 1
            if (r0 == 0) goto L_0x000c
        L_0x001a:
            r1 = 0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XJ.A0A(X.1XY):java.lang.Boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        if (r0 == false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Boolean A0B(X.AnonymousClass1XY r1) {
        /*
            com.facebook.user.model.User r1 = X.AnonymousClass0WY.A00(r1)
            if (r1 != 0) goto L_0x0011
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            boolean r1 = r0.booleanValue()
        L_0x000c:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            return r0
        L_0x0011:
            boolean r0 = r1.A1c
            if (r0 == 0) goto L_0x001a
            boolean r0 = r1.A1Q
            r1 = 1
            if (r0 != 0) goto L_0x000c
        L_0x001a:
            r1 = 0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XJ.A0B(X.1XY):java.lang.Boolean");
    }

    public static final Boolean A0C(AnonymousClass1XY r0) {
        boolean z;
        User A002 = AnonymousClass0WY.A00(r0);
        if (A002 == null) {
            z = Boolean.FALSE.booleanValue();
        } else {
            z = A002.A1X;
        }
        return Boolean.valueOf(z);
    }

    public static final Boolean A0D(AnonymousClass1XY r1) {
        TriState triState;
        AnonymousClass0XN A002 = AnonymousClass0XM.A00(r1);
        if (A002.A0I()) {
            triState = TriState.valueOf(A002.A09().A1c);
        } else {
            triState = TriState.UNSET;
        }
        return Boolean.valueOf(triState.asBoolean(false));
    }

    public static final Integer A0E(AnonymousClass1XY r0) {
        int i;
        User user = (User) AnonymousClass0WY.A01(r0).get();
        if (user == null) {
            i = 0;
        } else {
            i = user.A08;
        }
        return Integer.valueOf(i);
    }

    public static final String A0F(AnonymousClass1XY r0) {
        ViewerContext Asn = AnonymousClass0XL.A01(r0).Asn();
        if (Asn == null) {
            return null;
        }
        return Asn.mUserId;
    }

    public static final String A0G(AnonymousClass1XY r0) {
        ViewerContext A002 = C10580kT.A00(r0);
        if (A002 != null) {
            return A002.mUserId;
        }
        return null;
    }
}
