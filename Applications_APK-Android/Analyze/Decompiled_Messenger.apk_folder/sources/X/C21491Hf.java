package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.collect.ImmutableSet;

/* renamed from: X.1Hf  reason: invalid class name and case insensitive filesystem */
public final class C21491Hf implements C05700aB {
    public AnonymousClass0UN A00;

    public static final C21491Hf A00(AnonymousClass1XY r1) {
        return new C21491Hf(r1);
    }

    public void A01(String str) {
        AnonymousClass1Y8 A0D = C21501Hg.A00.A09(str);
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A00)).edit();
        edit.C1B(A0D);
        edit.commit();
    }

    public ImmutableSet AzO() {
        return ImmutableSet.A0A(((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A00)).B87(C21501Hg.A00));
    }

    public C21491Hf(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
