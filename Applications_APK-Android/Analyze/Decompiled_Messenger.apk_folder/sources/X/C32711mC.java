package X;

import android.app.Activity;
import android.net.Uri;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.checkpoint.CheckpointMetadata;
import com.facebook.common.stringformat.StringFormatUtil;

@UserScoped
/* renamed from: X.1mC  reason: invalid class name and case insensitive filesystem */
public final class C32711mC {
    private static C05540Zi A03;
    public long A00;
    public AnonymousClass0UN A01;
    public boolean A02;

    public static final C32711mC A00(AnonymousClass1XY r4) {
        C32711mC r0;
        synchronized (C32711mC.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C32711mC((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (C32711mC) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(C32711mC r8, CheckpointMetadata checkpointMetadata, Activity activity) {
        ((C70643b0) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AZn, r8.A01)).A05(activity, StringFormatUtil.formatStrLocaleSafe(C27881du.A0I, Uri.encode(Uri.parse("/help/resources/3381190").buildUpon().appendQueryParameter("oid", checkpointMetadata.A00).build().toString())), null, null, 3888, activity);
        r8.A00 = ((AnonymousClass069) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BBa, r8.A01)).now();
        ((C93964dg) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AxH, r8.A01)).A01();
    }

    private C32711mC(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(7, r3);
    }
}
