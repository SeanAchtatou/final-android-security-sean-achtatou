package X;

import android.os.HandlerThread;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0aj  reason: invalid class name and case insensitive filesystem */
public final class C06030aj {
    private static volatile C06030aj A07;
    private final HandlerThread A00;
    private final C07380dK A01;
    private final C06890cF A02;
    private final AnonymousClass09P A03;
    private final C04910Wu A04;
    private final C04310Tq A05;
    private final C04310Tq A06;

    public static final C06030aj A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C06030aj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C06030aj(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public C07930eP A01(String str, C04460Ut r13, boolean z) {
        return new C07930eP(str, r13, this.A06, this.A04, this.A03, this.A02, this.A00, this.A05, z, this.A01);
    }

    private C06030aj(AnonymousClass1XY r2) {
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.A0W, r2);
        this.A04 = C04910Wu.A00(r2);
        this.A03 = C04750Wa.A01(r2);
        this.A00 = AnonymousClass0UX.A03(r2);
        this.A05 = AnonymousClass0WY.A02(r2);
        this.A02 = C06890cF.A00(r2);
        this.A01 = C07380dK.A00(r2);
    }
}
