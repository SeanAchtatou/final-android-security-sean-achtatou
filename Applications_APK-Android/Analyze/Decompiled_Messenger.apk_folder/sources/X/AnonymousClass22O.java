package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.22O  reason: invalid class name */
public final class AnonymousClass22O extends C15380vC {
    private static C05540Zi A06;
    private AnonymousClass0UN A00;
    public final AnonymousClass22R A01;
    public final AnonymousClass22S A02;
    private final C06790c5 A03;
    private final AnonymousClass06U A04 = new AnonymousClass22Q(this);
    private final AnonymousClass06U A05 = new AnonymousClass22P(this);

    public static final AnonymousClass22O A00(AnonymousClass1XY r4) {
        AnonymousClass22O r0;
        synchronized (AnonymousClass22O.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r4)) {
                    A06.A00 = new AnonymousClass22O((AnonymousClass1XY) A06.A01());
                }
                C05540Zi r1 = A06;
                r0 = (AnonymousClass22O) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A01(AnonymousClass22O r5) {
        int i;
        int A052 = r5.A01.A05(AnonymousClass2MG.A03, null);
        int A053 = r5.A01.A05(AnonymousClass2MG.A01, null);
        if (!((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C35471rH) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BOE, r5.A00)).A00)).Aem(284919543764159L) || (i = A052 + A053) <= 0) {
            r5.A03(C15400vE.A02);
        } else {
            r5.A03(new C15400vE(i, AnonymousClass10O.RED_DOT));
        }
    }

    public static void A02(AnonymousClass22O r3, String str) {
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C35471rH) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BOE, r3.A00)).A00)).Aem(284919542191283L)) {
            ((AnonymousClass17N) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BKd, r3.A00)).A01(str).CHB(new AnonymousClass1G5(AnonymousClass07B.A00, "BADGING"));
        }
    }

    public void A04() {
        if (!this.A03.A02()) {
            this.A03.A00();
        }
        A01(this);
    }

    public void A05() {
        if (this.A03.A02()) {
            this.A03.A01();
        }
    }

    private AnonymousClass22O(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(2, r4);
        this.A01 = AnonymousClass22R.A00(r4);
        this.A02 = AnonymousClass22S.A00(r4);
        C06600bl BMm = C04430Uq.A02(r4).BMm();
        BMm.A02(C06680bu.A06, this.A04);
        BMm.A02(C06680bu.A07, this.A05);
        this.A03 = BMm.A00();
    }

    public void BUD() {
        super.BUD();
        A03(C15400vE.A02);
        C202079fi r2 = new C202079fi();
        r2.A00 = AnonymousClass2MG.A03;
        r2.A03 = true;
        this.A01.A09(new C202069fh(r2));
        C202079fi r22 = new C202079fi();
        r22.A00 = AnonymousClass2MG.A01;
        r22.A03 = true;
        this.A01.A09(new C202069fh(r22));
    }
}
