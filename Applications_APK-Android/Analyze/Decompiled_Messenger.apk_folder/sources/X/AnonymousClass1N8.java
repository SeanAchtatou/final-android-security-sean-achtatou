package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1N8  reason: invalid class name */
public final class AnonymousClass1N8 implements AnonymousClass1YQ {
    public final FbSharedPreferences A00;
    public final AnonymousClass1Y7 A01 = ((AnonymousClass1Y7) this.A0H.A09("bitmap_max_size_percent"));
    public final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) this.A0H.A09("pool_max_size_percent"));
    public final AnonymousClass1Y7 A03 = ((AnonymousClass1Y7) this.A0H.A09("should_register_trimmable"));
    public final AnonymousClass1Y7 A04 = ((AnonymousClass1Y7) this.A0H.A09("bitmap_pool_type"));
    public final AnonymousClass1Y7 A05 = ((AnonymousClass1Y7) this.A0H.A09("pool_fix_buckets_reinit"));
    public final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) this.A0H.A09("pool_hard_limit_relative_to_max_memory"));
    public final AnonymousClass1Y7 A07 = ((AnonymousClass1Y7) this.A0H.A09("pool_max_bucket_size_relative_to_screen"));
    public final AnonymousClass1Y7 A08 = ((AnonymousClass1Y7) this.A0H.A09("pool_min_bucket_size_relative_to_screen"));
    public final AnonymousClass1Y7 A09 = ((AnonymousClass1Y7) this.A0H.A09("pool_soft_limit_relative_to_max_memory"));
    public final AnonymousClass1Y7 A0A = ((AnonymousClass1Y7) this.A0H.A09("prepare_bitmap_at_least_bytes"));
    public final AnonymousClass1Y7 A0B = ((AnonymousClass1Y7) this.A0H.A09("prepare_bitmap_for_prefetch"));
    public final AnonymousClass1Y7 A0C = ((AnonymousClass1Y7) this.A0H.A09("prepare_bitmap_not_more_than_bytes"));
    public final AnonymousClass1Y7 A0D = ((AnonymousClass1Y7) this.A0H.A09("prepare_to_draw_enabled"));
    public final AnonymousClass1Y7 A0E;
    public final AnonymousClass1Y7 A0F = ((AnonymousClass1Y7) this.A0H.A09("use_gingerbread_decoder"));
    private final C25051Yd A0G;
    private final AnonymousClass1Y7 A0H;

    public String getSimpleName() {
        return "ImagePipelineMobileConfigProvider";
    }

    public static final AnonymousClass1N8 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1N8(r1);
    }

    public AnonymousClass1N8(AnonymousClass1XY r3) {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A00.A09("image_pipeline_mc_provider");
        this.A0H = r1;
        this.A0E = (AnonymousClass1Y7) r1.A09("use_default_bitmap_pool_params");
        this.A0G = AnonymousClass0WT.A00(r3);
        this.A00 = FbSharedPreferencesModule.A00(r3);
    }

    public void init() {
        int A032 = C000700l.A03(-1159203126);
        C30281hn edit = this.A00.edit();
        edit.putBoolean(this.A0E, true);
        edit.Bz4(this.A09, this.A0G.Aki(1129855571984681L));
        edit.Bz4(this.A06, this.A0G.Aki(1129855571788070L));
        edit.Bz4(this.A08, 0.0d);
        edit.Bz4(this.A07, this.A0G.Aki(1129855571853607L));
        edit.putBoolean(this.A05, this.A0G.Aem(285430641596076L));
        edit.BzC(this.A04, this.A0G.B4C(848384890044986L));
        edit.Bz4(this.A02, this.A0G.Aki(1129859866820907L));
        edit.Bz4(this.A01, this.A0G.Aki(1129859866689834L));
        edit.putBoolean(this.A03, this.A0G.Aem(285434936759982L));
        edit.putBoolean(this.A0D, this.A0G.Aem(2306128457035355828L));
        edit.Bz6(this.A0A, (int) this.A0G.At0(566922798171936L));
        edit.Bz6(this.A0C, (int) this.A0G.At0(566922798237473L));
        edit.putBoolean(this.A0B, this.A0G.Aem(2306128457035290291L));
        edit.putBoolean(this.A0F, false);
        edit.commit();
        C000700l.A09(-352679046, A032);
    }
}
