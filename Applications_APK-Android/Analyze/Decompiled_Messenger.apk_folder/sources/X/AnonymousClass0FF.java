package X;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0FF  reason: invalid class name */
public final class AnonymousClass0FF implements ThreadFactory {
    private final String A00;
    private final AtomicInteger A01 = new AtomicInteger(1);

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, AnonymousClass08S.A09(this.A00, this.A01.getAndIncrement()));
    }

    public AnonymousClass0FF(String str) {
        this.A00 = AnonymousClass08S.A0J(str, "-");
    }
}
