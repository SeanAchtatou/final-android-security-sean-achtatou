package X;

import android.util.SparseArray;
import com.facebook.profilo.config.v2.Config;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/* renamed from: X.09J  reason: invalid class name */
public final class AnonymousClass09J implements AnonymousClass05d {
    private AnonymousClass05i A00;
    private C004405h A01;
    private Config A02;

    public AnonymousClass09J(Config config) {
        AnonymousClass056 r11;
        Config config2 = config;
        this.A02 = config2;
        long longValue = Long.valueOf((long) config2.getSystemConfigParamInt("system_config.upload_max_bytes")).longValue();
        long longValue2 = Long.valueOf((long) config2.getSystemConfigParamInt("system_config.upload_bytes_per_update")).longValue();
        long longValue3 = Long.valueOf((long) config2.getSystemConfigParamInt("system_config.upload_time_period_sec")).longValue();
        if (longValue == 0 || longValue2 == 0 || longValue3 == 0) {
            throw new IllegalArgumentException("Bad values for system control configuration");
        }
        this.A01 = new AnonymousClass083(longValue, longValue2, longValue3, config2.optSystemConfigParamInt("system_config.buffer_size", -1), config2.optSystemConfigParamBool("system_config.mmap_buffer", false));
        Config config3 = this.A02;
        ArrayList arrayList = new ArrayList();
        int[] traceConfigIdxs = config3.getTraceConfigIdxs("qpl", "start");
        int i = -1;
        for (int i2 : traceConfigIdxs) {
            if (i != -1 || !config3.optTraceConfigParamBool(i2, "trace_config.is_cold_start", false)) {
                arrayList.add(Integer.valueOf(i2));
            } else {
                i = i2;
            }
        }
        SparseArray sparseArray = new SparseArray();
        AnonymousClass05Z r10 = null;
        if (!arrayList.isEmpty()) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                int intValue = ((Integer) it.next()).intValue();
                sparseArray.put(config3.getTraceConfigTriggerParamInt(intValue, "qpl", "start", "trigger.qpl.marker"), new C008507p(config3.getTraceConfigParamInt(intValue, "trace_config.coinflip_sample_rate"), Arrays.asList(config3.getTraceConfigProviders(intValue)), AnonymousClass09K.A02(config3, intValue), AnonymousClass09K.A00(config3, intValue), AnonymousClass09K.A01(config3, intValue)));
            }
            if (sparseArray.size() > 0) {
                r10 = new AnonymousClass05Z(sparseArray);
            }
        }
        if (i == -1) {
            r11 = null;
        } else {
            r11 = new AnonymousClass056(config3.getTraceConfigParamInt(i, "trace_config.coinflip_sample_rate"), Arrays.asList(config3.getTraceConfigProviders(i)), config3.getTraceConfigTriggerParamInt(i, "qpl", "stop", "trigger.qpl.marker"), AnonymousClass09K.A02(config3, i), AnonymousClass09K.A00(config3, i), AnonymousClass09K.A01(config3, i));
        }
        int[] traceConfigIdxs2 = config3.getTraceConfigIdxs("startup", "start");
        int length = traceConfigIdxs2.length;
        AnonymousClass072 r12 = null;
        if (length != 0) {
            int i3 = 0;
            while (true) {
                if (i3 >= length) {
                    break;
                }
                int i4 = traceConfigIdxs2[i3];
                if (config3.optTraceConfigParamBool(i4, "trace_config.is_black_box", false)) {
                    r12 = new AnonymousClass072(config3.getTraceConfigParamInt(i4, "trace_config.coinflip_sample_rate"), Arrays.asList(config3.getTraceConfigProviders(i4)), config3.optTraceConfigTriggerParamInt(i4, "qpl", "stop", "trigger.qpl.marker", 0), config3.optTraceConfigParamBool(i4, "trace_config.should_pause_in_background", false), AnonymousClass09K.A02(config3, i4), AnonymousClass09K.A00(config3, i4), AnonymousClass09K.A01(config3, i4));
                    break;
                }
                i3++;
            }
        }
        this.A00 = new AnonymousClass09K(r10, r11, r12, config3.getSystemConfigParamInt("system_config.max_trace_timeout_ms"), config3.getSystemConfigParamInt("system_config.timed_out_upload_sample_rate"));
    }

    public long Ai2() {
        return this.A02.getID();
    }

    public AnonymousClass05i AiX() {
        return this.A00;
    }

    public C004405h B5B() {
        return this.A01;
    }
}
