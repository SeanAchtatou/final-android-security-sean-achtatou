package X;

import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fJ  reason: invalid class name and case insensitive filesystem */
public final class C08410fJ extends AnonymousClass0f8 {
    private static volatile C08410fJ A00;

    public String Azg() {
        return "litho_stats";
    }

    public static final C08410fJ A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C08410fJ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C08410fJ();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        C30402Evd evd = (C30402Evd) obj;
        C30402Evd evd2 = (C30402Evd) obj2;
        if (evd != null && evd2 != null) {
            performanceLoggingEvent.A06("litho_component_applied_state_update_counter", evd2.A00 - evd.A00);
            performanceLoggingEvent.A06("litho_component_triggered_sync_state_update_counter", evd2.A05 - evd.A05);
            performanceLoggingEvent.A06("litho_component_triggered_async_state_update_counter", evd2.A04 - evd.A04);
            performanceLoggingEvent.A06("litho_calculate_layout_counter", evd2.A01 - evd.A01);
            performanceLoggingEvent.A06("litho_calculate_layout_on_ui_counter", evd2.A02 - evd.A02);
            performanceLoggingEvent.A06("litho_mount_counter", evd2.A03 - evd.A03);
            performanceLoggingEvent.A06("litho_section_applied_state_update_counter", evd2.A06 - evd.A06);
            performanceLoggingEvent.A06("litho_section_triggered_sync_state_update_counter", evd2.A0A - evd.A0A);
            performanceLoggingEvent.A06("litho_section_triggered_async_state_update_counter", evd2.A09 - evd.A09);
            performanceLoggingEvent.A06("litho_section_calculate_new_changeset_counter", evd2.A07 - evd.A07);
            performanceLoggingEvent.A06("litho_section_calculate_new_changeset_on_ui_counter", evd2.A08 - evd.A08);
        }
    }

    public long Azh() {
        return C08350fD.A08;
    }

    public Class B3O() {
        return C30402Evd.class;
    }

    public Object CGO() {
        C30402Evd evd = new C30402Evd();
        evd.A00 = C32121lB.A00.get();
        evd.A05 = C32121lB.A05.get();
        evd.A04 = C32121lB.A04.get();
        evd.A01 = C32121lB.A01.get();
        evd.A02 = C32121lB.A02.get();
        evd.A03 = C32121lB.A03.get();
        evd.A06 = C32121lB.A06.get();
        evd.A0A = C32121lB.A0A.get();
        evd.A09 = C32121lB.A09.get();
        evd.A07 = C32121lB.A07.get();
        evd.A08 = C32121lB.A08.get();
        return evd;
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A06;
    }
}
