package X;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: X.04a  reason: invalid class name */
public class AnonymousClass04a<K, V> extends AnonymousClass04b<K, V> implements Map<K, V> {
    public C04180St A00;

    private C04180St A00() {
        if (this.A00 == null) {
            this.A00 = new AnonymousClass04c(this);
        }
        return this.A00;
    }

    public void putAll(Map map) {
        A0A(this.A00 + map.size());
        for (Map.Entry entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public Set entrySet() {
        C04180St A002 = A00();
        if (A002.A00 == null) {
            A002.A00 = new C04190Su(A002);
        }
        return A002.A00;
    }

    public Set keySet() {
        C04180St A002 = A00();
        if (A002.A01 == null) {
            A002.A01 = new AnonymousClass1WP(A002);
        }
        return A002.A01;
    }

    public Collection values() {
        C04180St A002 = A00();
        if (A002.A02 == null) {
            A002.A02 = new AnonymousClass1WQ(A002);
        }
        return A002.A02;
    }

    public void A0C(Collection collection) {
        C04180St.A00(this, collection);
    }

    public AnonymousClass04a() {
    }

    public AnonymousClass04a(int i) {
        super(i);
    }

    public AnonymousClass04a(AnonymousClass04b r1) {
        if (r1 != null) {
            A0B(r1);
        }
    }
}
