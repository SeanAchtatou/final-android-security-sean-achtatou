package X;

import com.facebook.messaging.integrity.frx.model.AdditionalAction;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1xN  reason: invalid class name and case insensitive filesystem */
public final class C38401xN {
    public AnonymousClass0UN A00;

    public static final C38401xN A00(AnonymousClass1XY r1) {
        return new C38401xN(r1);
    }

    public static ImmutableList A01(ImmutableList immutableList, String str) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            AdditionalAction additionalAction = (AdditionalAction) it.next();
            if (additionalAction.A01() == AnonymousClass07B.A01) {
                C401920q r1 = new C401920q(additionalAction);
                r1.A02 = str;
                r1.A08 = false;
                builder.add((Object) new AdditionalAction(r1));
            } else {
                builder.add((Object) additionalAction);
            }
        }
        return builder.build();
    }

    public static ImmutableList A02(ImmutableList immutableList, boolean z, Integer... numArr) {
        boolean z2;
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            AdditionalAction additionalAction = (AdditionalAction) it.next();
            Integer A01 = additionalAction.A01();
            int length = numArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    z2 = false;
                    break;
                } else if (A01 == numArr[i]) {
                    z2 = true;
                    break;
                } else {
                    i++;
                }
            }
            if (z2) {
                C401920q r1 = new C401920q(additionalAction);
                r1.A08 = z;
                builder.add((Object) new AdditionalAction(r1));
            } else {
                builder.add((Object) additionalAction);
            }
        }
        return builder.build();
    }

    private C38401xN(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
