package X;

/* renamed from: X.0A2  reason: invalid class name */
public final class AnonymousClass0A2 {
    public static boolean A00(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || !obj.equals(obj2)) {
            return false;
        }
        return true;
    }
}
