package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.ThreadJoinRequest;

/* renamed from: X.22I  reason: invalid class name */
public final class AnonymousClass22I implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ThreadJoinRequest(parcel);
    }

    public Object[] newArray(int i) {
        return new ThreadJoinRequest[i];
    }
}
