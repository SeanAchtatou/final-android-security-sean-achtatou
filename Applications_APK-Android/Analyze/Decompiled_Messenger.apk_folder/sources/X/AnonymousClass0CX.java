package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;

/* renamed from: X.0CX  reason: invalid class name */
public final class AnonymousClass0CX {
    public static int A00 = 5;
    public static final BlockingQueue A01 = new LinkedBlockingQueue(10);
    public static final ThreadFactory A02 = new C02080Cu();
    public static volatile Executor A03;
}
