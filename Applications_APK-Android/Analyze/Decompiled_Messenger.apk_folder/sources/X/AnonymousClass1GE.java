package X;

import com.facebook.common.util.TriState;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1GE  reason: invalid class name */
public final class AnonymousClass1GE implements C05460Za {
    private static volatile AnonymousClass1GE A05;
    public AnonymousClass0UN A00;
    private TriState A01 = TriState.UNSET;
    public final C001500z A02;
    public final C32191lM A03 = new C32191lM(this, 282501480908300L, false);
    private final Boolean A04;

    public static final AnonymousClass1GE A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (AnonymousClass1GE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new AnonymousClass1GE(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public boolean A01() {
        if (this.A02 == C001500z.A01) {
            return true;
        }
        if (!this.A01.isSet()) {
            this.A01 = TriState.valueOf(((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(192, false));
        }
        return this.A01.asBoolean(false);
    }

    public boolean A02() {
        if (!this.A04.booleanValue() && this.A02 == C001500z.A07) {
            C32191lM r4 = this.A03;
            if (!r4.A00.isSet()) {
                boolean z = r4.A02;
                int i = AnonymousClass1Y3.AOJ;
                AnonymousClass1GE r1 = r4.A03;
                C32191lM r0 = r1.A03;
                if (((C25051Yd) AnonymousClass1XX.A02(1, i, r1.A00)).Aes(r0.A01, r0.A02, AnonymousClass0XE.A07)) {
                    z = ((C25051Yd) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AOJ, r4.A03.A00)).Aeo(r4.A01, r4.A02);
                }
                r4.A00 = TriState.valueOf(z);
            }
            if (!r4.A00.asBoolean(r4.A02)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public void clearUserData() {
        C32191lM r1 = this.A03;
        TriState triState = TriState.UNSET;
        r1.A00 = triState;
        this.A01 = triState;
    }

    private AnonymousClass1GE(AnonymousClass1XY r5) {
        this.A00 = new AnonymousClass0UN(2, r5);
        this.A04 = AnonymousClass0UU.A08(r5);
        this.A02 = AnonymousClass0UU.A05(r5);
    }
}
