package X;

import java.net.Socket;
import java.util.concurrent.Callable;

/* renamed from: X.0S6  reason: invalid class name */
public final class AnonymousClass0S6 implements Callable {
    public final /* synthetic */ AnonymousClass0S5 A00;
    public final /* synthetic */ Socket A01;
    public final /* synthetic */ Socket A02;

    public AnonymousClass0S6(AnonymousClass0S5 r1, Socket socket, Socket socket2) {
        this.A00 = r1;
        this.A01 = socket;
        this.A02 = socket2;
    }

    public Object call() {
        AnonymousClass0S5 r3 = this.A00;
        AnonymousClass0S5.A00(r3, this.A01, r3.A01, this.A02);
        return null;
    }
}
