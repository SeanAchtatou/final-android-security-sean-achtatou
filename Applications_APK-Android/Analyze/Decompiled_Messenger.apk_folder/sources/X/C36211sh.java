package X;

import com.facebook.common.dextricks.DalvikInternals;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/* renamed from: X.1sh  reason: invalid class name and case insensitive filesystem */
public final class C36211sh implements C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("appSpecificInfo", DalvikInternals.IOPRIO_CLASS_SHIFT, 10);
    private static final C36241sk A01 = new C36241sk("clientIdentifier", (byte) 11, 1);
    private static final C36241sk A02 = new C36241sk("clientInfo", (byte) 12, 4);
    private static final C36241sk A03 = new C36241sk("combinedPublishes", (byte) 15, 8);
    private static final C36241sk A04 = new C36241sk("getDiffsRequests", (byte) 15, 6);
    private static final C36241sk A05 = new C36241sk("password", (byte) 11, 5);
    private static final C36241sk A06 = new C36241sk("phpOverride", (byte) 12, 11);
    private static final C36241sk A07 = new C36241sk("proxygenInfo", (byte) 15, 7);
    private static final C36241sk A08 = new C36241sk("willMessage", (byte) 11, 3);
    private static final C36241sk A09 = new C36241sk("willTopic", (byte) 11, 2);
    private static final C36241sk A0A = new C36241sk("zeroRatingTokenHash", (byte) 11, 9);
    private static final C36231sj A0B = new C36231sj("ConnectMessage");
    public final Map appSpecificInfo;
    public final String clientIdentifier;
    public final C36251sl clientInfo;
    public final List combinedPublishes;
    public final List getDiffsRequests;
    public final String password;
    public final C36271sn phpOverride;
    public final List proxygenInfo;
    public final String willMessage;
    public final String willTopic;
    public final String zeroRatingTokenHash;

    public boolean equals(Object obj) {
        C36211sh r5;
        if (obj == null || !(obj instanceof C36211sh) || (r5 = (C36211sh) obj) == null) {
            return false;
        }
        if (this == r5) {
            return true;
        }
        String str = this.clientIdentifier;
        boolean z = false;
        if (str != null) {
            z = true;
        }
        String str2 = r5.clientIdentifier;
        boolean z2 = false;
        if (str2 != null) {
            z2 = true;
        }
        if ((z || z2) && (!z || !z2 || !str.equals(str2))) {
            return false;
        }
        String str3 = this.willTopic;
        boolean z3 = false;
        if (str3 != null) {
            z3 = true;
        }
        String str4 = r5.willTopic;
        boolean z4 = false;
        if (str4 != null) {
            z4 = true;
        }
        if ((z3 || z4) && (!z3 || !z4 || !str3.equals(str4))) {
            return false;
        }
        String str5 = this.willMessage;
        boolean z5 = false;
        if (str5 != null) {
            z5 = true;
        }
        String str6 = r5.willMessage;
        boolean z6 = false;
        if (str6 != null) {
            z6 = true;
        }
        if ((z5 || z6) && (!z5 || !z6 || !str5.equals(str6))) {
            return false;
        }
        C36251sl r3 = this.clientInfo;
        boolean z7 = false;
        if (r3 != null) {
            z7 = true;
        }
        C36251sl r1 = r5.clientInfo;
        boolean z8 = false;
        if (r1 != null) {
            z8 = true;
        }
        if ((z7 || z8) && (!z7 || !z8 || !B36.A0B(r3, r1))) {
            return false;
        }
        String str7 = this.password;
        boolean z9 = false;
        if (str7 != null) {
            z9 = true;
        }
        String str8 = r5.password;
        boolean z10 = false;
        if (str8 != null) {
            z10 = true;
        }
        if ((z9 || z10) && (!z9 || !z10 || !str7.equals(str8))) {
            return false;
        }
        List list = this.getDiffsRequests;
        boolean z11 = false;
        if (list != null) {
            z11 = true;
        }
        List list2 = r5.getDiffsRequests;
        boolean z12 = false;
        if (list2 != null) {
            z12 = true;
        }
        if ((z11 || z12) && (!z11 || !z12 || !B36.A0G(list, list2))) {
            return false;
        }
        List list3 = this.proxygenInfo;
        boolean z13 = false;
        if (list3 != null) {
            z13 = true;
        }
        List list4 = r5.proxygenInfo;
        boolean z14 = false;
        if (list4 != null) {
            z14 = true;
        }
        if ((z13 || z14) && (!z13 || !z14 || !B36.A0F(list3, list4))) {
            return false;
        }
        List list5 = this.combinedPublishes;
        boolean z15 = false;
        if (list5 != null) {
            z15 = true;
        }
        List list6 = r5.combinedPublishes;
        boolean z16 = false;
        if (list6 != null) {
            z16 = true;
        }
        if ((z15 || z16) && (!z15 || !z16 || !B36.A0F(list5, list6))) {
            return false;
        }
        String str9 = this.zeroRatingTokenHash;
        boolean z17 = false;
        if (str9 != null) {
            z17 = true;
        }
        String str10 = r5.zeroRatingTokenHash;
        boolean z18 = false;
        if (str10 != null) {
            z18 = true;
        }
        if ((z17 || z18) && (!z17 || !z18 || !str9.equals(str10))) {
            return false;
        }
        Map map = this.appSpecificInfo;
        boolean z19 = false;
        if (map != null) {
            z19 = true;
        }
        Map map2 = r5.appSpecificInfo;
        boolean z20 = false;
        if (map2 != null) {
            z20 = true;
        }
        if ((z19 || z20) && (!z19 || !z20 || !map.equals(map2))) {
            return false;
        }
        C36271sn r32 = this.phpOverride;
        boolean z21 = false;
        if (r32 != null) {
            z21 = true;
        }
        C36271sn r12 = r5.phpOverride;
        boolean z22 = false;
        if (r12 != null) {
            z22 = true;
        }
        if (!z21 && !z22) {
            return true;
        }
        if (!z21 || !z22 || !B36.A0B(r32, r12)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    public void CNX(C35781ro r5) {
        r5.A0i(A0B);
        if (this.clientIdentifier != null) {
            r5.A0e(A01);
            r5.A0j(this.clientIdentifier);
            r5.A0S();
        }
        String str = this.willTopic;
        if (str != null) {
            boolean z = false;
            if (str != null) {
                z = true;
            }
            if (z) {
                r5.A0e(A09);
                r5.A0j(this.willTopic);
                r5.A0S();
            }
        }
        String str2 = this.willMessage;
        if (str2 != null) {
            boolean z2 = false;
            if (str2 != null) {
                z2 = true;
            }
            if (z2) {
                r5.A0e(A08);
                r5.A0j(this.willMessage);
                r5.A0S();
            }
        }
        if (this.clientInfo != null) {
            r5.A0e(A02);
            this.clientInfo.CNX(r5);
            r5.A0S();
        }
        String str3 = this.password;
        if (str3 != null) {
            boolean z3 = false;
            if (str3 != null) {
                z3 = true;
            }
            if (z3) {
                r5.A0e(A05);
                r5.A0j(this.password);
                r5.A0S();
            }
        }
        List list = this.getDiffsRequests;
        if (list != null) {
            boolean z4 = false;
            if (list != null) {
                z4 = true;
            }
            if (z4) {
                r5.A0e(A04);
                r5.A0f(new C36381sy((byte) 11, this.getDiffsRequests.size()));
                for (byte[] A0m : this.getDiffsRequests) {
                    r5.A0m(A0m);
                }
                r5.A0U();
                r5.A0S();
            }
        }
        List list2 = this.proxygenInfo;
        if (list2 != null) {
            boolean z5 = false;
            if (list2 != null) {
                z5 = true;
            }
            if (z5) {
                r5.A0e(A07);
                r5.A0f(new C36381sy((byte) 12, this.proxygenInfo.size()));
                for (C22366Awl CNX : this.proxygenInfo) {
                    CNX.CNX(r5);
                }
                r5.A0U();
                r5.A0S();
            }
        }
        List list3 = this.combinedPublishes;
        if (list3 != null) {
            boolean z6 = false;
            if (list3 != null) {
                z6 = true;
            }
            if (z6) {
                r5.A0e(A03);
                r5.A0f(new C36381sy((byte) 12, this.combinedPublishes.size()));
                for (AxV CNX2 : this.combinedPublishes) {
                    CNX2.CNX(r5);
                }
                r5.A0U();
                r5.A0S();
            }
        }
        String str4 = this.zeroRatingTokenHash;
        if (str4 != null) {
            boolean z7 = false;
            if (str4 != null) {
                z7 = true;
            }
            if (z7) {
                r5.A0e(A0A);
                r5.A0j(this.zeroRatingTokenHash);
                r5.A0S();
            }
        }
        Map map = this.appSpecificInfo;
        if (map != null) {
            boolean z8 = false;
            if (map != null) {
                z8 = true;
            }
            if (z8) {
                r5.A0e(A00);
                r5.A0g(new C22332Avx((byte) 11, (byte) 11, this.appSpecificInfo.size()));
                for (Map.Entry entry : this.appSpecificInfo.entrySet()) {
                    r5.A0j((String) entry.getKey());
                    r5.A0j((String) entry.getValue());
                }
                r5.A0V();
                r5.A0S();
            }
        }
        C36271sn r1 = this.phpOverride;
        if (r1 != null) {
            boolean z9 = false;
            if (r1 != null) {
                z9 = true;
            }
            if (z9) {
                r5.A0e(A06);
                this.phpOverride.CNX(r5);
                r5.A0S();
            }
        }
        r5.A0T();
        r5.A0X();
    }

    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{this.clientIdentifier, this.willTopic, this.willMessage, this.clientInfo, this.password, this.getDiffsRequests, this.proxygenInfo, this.combinedPublishes, this.zeroRatingTokenHash, this.appSpecificInfo, this.phpOverride});
    }

    public C36211sh(String str, String str2, String str3, C36251sl r4, String str4, List list, List list2, List list3, String str5, Map map, C36271sn r11) {
        this.clientIdentifier = str;
        this.willTopic = str2;
        this.willMessage = str3;
        this.clientInfo = r4;
        this.password = str4;
        this.getDiffsRequests = list;
        this.proxygenInfo = list2;
        this.combinedPublishes = list3;
        this.zeroRatingTokenHash = str5;
        this.appSpecificInfo = map;
        this.phpOverride = r11;
    }

    public String CJ9(int i, boolean z) {
        return B36.A06(this, i, z);
    }
}
