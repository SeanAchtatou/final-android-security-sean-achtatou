package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.05s  reason: invalid class name and case insensitive filesystem */
public final class C005105s implements FilenameFilter {
    public boolean accept(File file, String str) {
        if (str.startsWith("override-")) {
            return false;
        }
        if (str.endsWith(".log") || str.endsWith(".zip") || str.endsWith(".tmp")) {
            return true;
        }
        return false;
    }
}
