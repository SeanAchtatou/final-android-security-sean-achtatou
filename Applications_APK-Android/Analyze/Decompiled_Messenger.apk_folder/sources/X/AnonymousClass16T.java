package X;

import com.google.common.collect.ImmutableList;
import java.util.EnumSet;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16T  reason: invalid class name */
public final class AnonymousClass16T {
    private static volatile AnonymousClass16T A03;
    public final Boolean A00;
    public final C04310Tq A01;
    public final C04310Tq A02;

    public static C189016a A00(AnonymousClass16T r2, EnumSet enumSet, int i) {
        C192617l r1 = new C192617l(enumSet, i);
        C189016a r0 = (C189016a) r2.A01.get();
        r0.A03 = r1;
        return r0;
    }

    public static C189016a A01(AnonymousClass16T r2, EnumSet enumSet, ImmutableList immutableList, int i) {
        C192617l r1 = new C192617l(enumSet, i);
        C189016a r3 = (C189016a) r2.A01.get();
        r3.A03 = r1;
        ((AnonymousClass79M) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AnX, r3.A02)).A01 = immutableList;
        return r3;
    }

    public static final AnonymousClass16T A02(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass16T.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass16T(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public C189016a A03() {
        C192617l r1 = new C192617l(EnumSet.of(AnonymousClass16Y.A0I, AnonymousClass16Y.A0D), 60);
        C189016a r0 = (C189016a) this.A01.get();
        r0.A03 = r1;
        return r0;
    }

    public C189016a A04(int i) {
        C192617l r1 = new C192617l(EnumSet.of(AnonymousClass16Y.A0E), i);
        C189016a r0 = (C189016a) this.A01.get();
        r0.A03 = r1;
        return r0;
    }

    public C189016a A05(ImmutableList immutableList, int i) {
        return A01(this, EnumSet.of(AnonymousClass16Y.A0B), immutableList, i);
    }

    private AnonymousClass16T(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.Avg, r2);
        this.A02 = AnonymousClass0VG.A00(AnonymousClass1Y3.BDv, r2);
        this.A00 = AnonymousClass0UU.A08(r2);
    }
}
