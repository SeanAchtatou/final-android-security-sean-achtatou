package X;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/* renamed from: X.1eU  reason: invalid class name and case insensitive filesystem */
public final class C28241eU extends C28251eV {
    public final int _hashSeed;
    public char[] _inputBuffer;
    public C09980jK _objectCodec;
    public Reader _reader;
    public final C10410k3 _symbols;
    public boolean _tokenIncomplete = false;

    /* JADX WARN: Failed to insert an additional move for type inference into block B:412:0x0083 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:72:0x0129 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v80, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v81, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v85, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v86, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v87, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v88, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v89, resolved type: char} */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x04ae, code lost:
        if (r1 != '.') goto L_0x04c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x04b0, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x04b1, code lost:
        if (r2 >= r10) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:308:0x04b3, code lost:
        r14 = r2 + 1;
        r1 = r0[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x04b7, code lost:
        if (r1 < '0') goto L_0x04bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x04b9, code lost:
        if (r1 > '9') goto L_0x04bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:311:0x04bb, code lost:
        r3 = r3 + 1;
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:312:0x04bf, code lost:
        if (r3 != 0) goto L_0x04c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x04c1, code lost:
        reportUnexpectedNumberChar(r1, "Decimal point not followed by a digit");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:314:0x04c6, code lost:
        r2 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:315:0x04c8, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:317:0x04cb, code lost:
        if (r1 == 'e') goto L_0x04d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:319:0x04cf, code lost:
        if (r1 != 'E') goto L_0x04fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:320:0x04d1, code lost:
        if (r2 >= r10) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:321:0x04d3, code lost:
        r14 = r12._inputBuffer;
        r0 = r2 + 1;
        r1 = r14[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:322:0x04d9, code lost:
        if (r1 == '-') goto L_0x04ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:324:0x04dd, code lost:
        if (r1 == '+') goto L_0x04ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:325:0x04df, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:326:0x04e0, code lost:
        if (r1 > '9') goto L_0x04f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:327:0x04e2, code lost:
        if (r1 < '0') goto L_0x04f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:328:0x04e4, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:329:0x04e6, code lost:
        if (r2 >= r10) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:330:0x04e8, code lost:
        r13 = r2 + 1;
        r1 = r14[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:331:0x04ec, code lost:
        r2 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:332:0x04ee, code lost:
        if (r0 >= r10) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:333:0x04f0, code lost:
        r13 = r0 + 1;
        r1 = r14[r0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:334:0x04f5, code lost:
        if (r7 != 0) goto L_0x04fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:335:0x04f7, code lost:
        reportUnexpectedNumberChar(r1, "Exponent indicator not followed by a digit");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:336:0x04fc, code lost:
        r2 = r2 - 1;
        r12._inputPtr = r2;
        r12._textBuffer.resetWithShared(r12._inputBuffer, r8, r2 - r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:337:0x0508, code lost:
        if (r3 >= 1) goto L_0x0512;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x050a, code lost:
        if (r7 >= 1) goto L_0x0512;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:339:0x050c, code lost:
        r0 = resetInt(r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x0512, code lost:
        r12._numberNegative = r5;
        r12._intLength = r6;
        r12._fractLength = r3;
        r12._expLength = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:401:0x0630, code lost:
        if (r2 == '\"') goto L_0x0632;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:402:0x0632, code lost:
        r12._inputPtr = r8 + 1;
        r0 = r12._symbols.findSymbol(r3, r7, r8 - r7, r0);
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x02f1  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x0303  */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x030c  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x0318  */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x034f  */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x0362  */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x0388  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x039b  */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x03ac  */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x03bd  */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x03eb  */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x03f2  */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x0413  */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x0418  */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x043e  */
    /* JADX WARNING: Removed duplicated region for block: B:297:0x0491  */
    /* JADX WARNING: Removed duplicated region for block: B:298:0x0499  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C182811d nextToken() {
        /*
            r19 = this;
            r0 = 0
            r12 = r19
            r12._numTypesValid = r0
            X.11d r1 = r12._currToken
            X.11d r0 = X.C182811d.FIELD_NAME
            if (r1 != r0) goto L_0x0037
            r0 = 0
            r12._nameCopied = r0
            X.11d r3 = r12._nextToken
            r0 = 0
            r12._nextToken = r0
            X.11d r0 = X.C182811d.START_ARRAY
            if (r3 != r0) goto L_0x0026
            X.1eZ r2 = r12._parsingContext
            int r1 = r12._tokenInputRow
            int r0 = r12._tokenInputCol
            X.1eZ r0 = r2.createChildArrayContext(r1, r0)
            r12._parsingContext = r0
        L_0x0023:
            r12._currToken = r3
            return r3
        L_0x0026:
            X.11d r0 = X.C182811d.START_OBJECT
            if (r3 != r0) goto L_0x0023
            X.1eZ r2 = r12._parsingContext
            int r1 = r12._tokenInputRow
            int r0 = r12._tokenInputCol
            X.1eZ r0 = r2.createChildObjectContext(r1, r0)
            r12._parsingContext = r0
            goto L_0x0023
        L_0x0037:
            boolean r0 = r12._tokenIncomplete
            if (r0 == 0) goto L_0x0073
            r0 = 0
            r12._tokenIncomplete = r0
            int r0 = r12._inputPtr
            int r4 = r12._inputEnd
            char[] r3 = r12._inputBuffer
        L_0x0044:
            if (r0 < r4) goto L_0x0057
            r12._inputPtr = r0
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x0053
            java.lang.String r0 = ": was expecting closing quote for a string value"
            r12._reportInvalidEOF(r0)
        L_0x0053:
            int r0 = r12._inputPtr
            int r4 = r12._inputEnd
        L_0x0057:
            int r2 = r0 + 1
            char r1 = r3[r0]
            r0 = 92
            if (r1 > r0) goto L_0x00cc
            if (r1 != r0) goto L_0x006b
            r12._inputPtr = r2
            r12._decodeEscaped()
            int r0 = r12._inputPtr
            int r4 = r12._inputEnd
            goto L_0x0044
        L_0x006b:
            r0 = 34
            if (r1 > r0) goto L_0x00cc
            if (r1 != r0) goto L_0x00c1
            r12._inputPtr = r2
        L_0x0073:
            int r1 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r1 < r0) goto L_0x008c
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x008c
            r12._handleEOF()
            r1 = -1
        L_0x0083:
            r7 = 0
            if (r1 >= 0) goto L_0x00cf
            r12.close()
            r12._currToken = r7
            return r7
        L_0x008c:
            char[] r1 = r12._inputBuffer
            int r0 = r12._inputPtr
            int r2 = r0 + 1
            r12._inputPtr = r2
            char r1 = r1[r0]
            r0 = 32
            if (r1 <= r0) goto L_0x00a2
            r0 = 47
            if (r1 != r0) goto L_0x0083
            r12._skipComment()
            goto L_0x0073
        L_0x00a2:
            if (r1 == r0) goto L_0x0073
            r0 = 10
            if (r1 != r0) goto L_0x00b1
            int r0 = r12._currInputRow
            int r0 = r0 + 1
            r12._currInputRow = r0
            r12._currInputRowStart = r2
            goto L_0x0073
        L_0x00b1:
            r0 = 13
            if (r1 != r0) goto L_0x00b9
            r12._skipCR()
            goto L_0x0073
        L_0x00b9:
            r0 = 9
            if (r1 == r0) goto L_0x0073
            r12._throwInvalidSpace(r1)
            goto L_0x0073
        L_0x00c1:
            r0 = 32
            if (r1 >= r0) goto L_0x00cc
            r12._inputPtr = r2
            java.lang.String r0 = "string value"
            r12._throwUnquotedSpace(r1, r0)
        L_0x00cc:
            r0 = r2
            goto L_0x0044
        L_0x00cf:
            long r5 = r12._currInputProcessed
            int r4 = r12._inputPtr
            long r2 = (long) r4
            long r5 = r5 + r2
            r2 = 1
            long r5 = r5 - r2
            r12._tokenInputTotal = r5
            int r0 = r12._currInputRow
            r12._tokenInputRow = r0
            int r0 = r12._currInputRowStart
            int r4 = r4 - r0
            r11 = 1
            int r4 = r4 - r11
            r12._tokenInputCol = r4
            r12._binaryValue = r7
            r10 = 125(0x7d, float:1.75E-43)
            r9 = 93
            if (r1 != r9) goto L_0x0105
            X.1eZ r0 = r12._parsingContext
            int r2 = r0._type
            r0 = 1
            if (r2 == r11) goto L_0x00f5
            r0 = 0
        L_0x00f5:
            if (r0 != 0) goto L_0x00fa
            r12._reportMismatchedEndMarker(r1, r10)
        L_0x00fa:
            X.1eZ r0 = r12._parsingContext
            X.1eZ r0 = r0._parent
            r12._parsingContext = r0
            X.11d r0 = X.C182811d.END_ARRAY
        L_0x0102:
            r12._currToken = r0
            return r0
        L_0x0105:
            if (r1 != r10) goto L_0x011b
            X.1eZ r0 = r12._parsingContext
            boolean r0 = r0.inObject()
            if (r0 != 0) goto L_0x0112
            r12._reportMismatchedEndMarker(r1, r9)
        L_0x0112:
            X.1eZ r0 = r12._parsingContext
            X.1eZ r0 = r0._parent
            r12._parsingContext = r0
            X.11d r0 = X.C182811d.END_OBJECT
            goto L_0x0102
        L_0x011b:
            X.1eZ r4 = r12._parsingContext
            int r3 = r4._index
            r2 = 1
            int r3 = r3 + r11
            r4._index = r3
            int r0 = r4._type
            if (r0 == 0) goto L_0x064e
            if (r3 <= 0) goto L_0x064e
        L_0x0129:
            if (r2 == 0) goto L_0x0142
            r0 = 44
            if (r1 == r0) goto L_0x013e
            java.lang.String r3 = "was expecting comma to separate "
            java.lang.String r2 = r4.getTypeDesc()
            java.lang.String r0 = " entries"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r3, r2, r0)
            r12._reportUnexpectedChar(r1, r0)
        L_0x013e:
            int r1 = r12._skipWS()
        L_0x0142:
            X.1eZ r0 = r12._parsingContext
            boolean r17 = r0.inObject()
            if (r17 == 0) goto L_0x0193
            r13 = 34
            if (r1 == r13) goto L_0x061a
            r0 = 39
            if (r1 != r0) goto L_0x0541
            X.0k1 r0 = X.C10390k1.ALLOW_SINGLE_QUOTES
            boolean r0 = r12.isEnabled(r0)
            if (r0 == 0) goto L_0x0541
            int r8 = r12._inputPtr
            r7 = r8
            int r0 = r12._hashSeed
            int r13 = r12._inputEnd
            r6 = 39
            if (r8 >= r13) goto L_0x0174
            int[] r5 = X.AnonymousClass11z.sInputCodes
            int r4 = r5.length
        L_0x0168:
            char[] r3 = r12._inputBuffer
            char r2 = r3[r8]
            if (r2 == r6) goto L_0x0632
            if (r2 >= r4) goto L_0x0538
            r1 = r5[r2]
            if (r1 == 0) goto L_0x0538
        L_0x0174:
            r12._inputPtr = r8
            java.lang.String r0 = r12._parseFieldName2(r7, r0, r6)
        L_0x017a:
            X.1eZ r1 = r12._parsingContext
            r1._currentName = r0
            X.11d r0 = X.C182811d.FIELD_NAME
            r12._currToken = r0
            int r1 = r12._skipWS()
            r0 = 58
            if (r1 == r0) goto L_0x018f
            java.lang.String r0 = "was expecting a colon to separate field name and value"
            r12._reportUnexpectedChar(r1, r0)
        L_0x018f:
            int r1 = r12._skipWS()
        L_0x0193:
            r0 = 34
            if (r1 == r0) goto L_0x0532
            r0 = 45
            if (r1 == r0) goto L_0x02cc
            r0 = 91
            if (r1 == r0) goto L_0x02ba
            if (r1 == r9) goto L_0x02a8
            r0 = 102(0x66, float:1.43E-43)
            if (r1 == r0) goto L_0x029f
            r0 = 110(0x6e, float:1.54E-43)
            if (r1 == r0) goto L_0x0296
            r0 = 116(0x74, float:1.63E-43)
            if (r1 == r0) goto L_0x02b1
            r0 = 123(0x7b, float:1.72E-43)
            if (r1 == r0) goto L_0x0284
            if (r1 == r10) goto L_0x02a8
            switch(r1) {
                case 48: goto L_0x02cc;
                case 49: goto L_0x02cc;
                case 50: goto L_0x02cc;
                case 51: goto L_0x02cc;
                case 52: goto L_0x02cc;
                case 53: goto L_0x02cc;
                case 54: goto L_0x02cc;
                case 55: goto L_0x02cc;
                case 56: goto L_0x02cc;
                case 57: goto L_0x02cc;
                default: goto L_0x01b6;
            }
        L_0x01b6:
            r0 = 39
            if (r1 == r0) goto L_0x0218
            r0 = 43
            if (r1 == r0) goto L_0x01f7
            r0 = 73
            if (r1 == r0) goto L_0x01e0
            r0 = 78
            if (r1 != r0) goto L_0x027c
            java.lang.String r2 = "NaN"
            r12._matchToken(r2, r11)
            X.0k1 r0 = X.C10390k1.ALLOW_NON_NUMERIC_NUMBERS
            boolean r0 = r12.isEnabled(r0)
            if (r0 == 0) goto L_0x0277
            r0 = 9221120237041090560(0x7ff8000000000000, double:NaN)
        L_0x01d5:
            X.11d r0 = r12.resetAsNaN(r2, r0)
        L_0x01d9:
            if (r17 == 0) goto L_0x0102
            r12._nextToken = r0
            X.11d r0 = r12._currToken
            return r0
        L_0x01e0:
            java.lang.String r2 = "Infinity"
            r12._matchToken(r2, r11)
            X.0k1 r0 = X.C10390k1.ALLOW_NON_NUMERIC_NUMBERS
            boolean r0 = r12.isEnabled(r0)
            if (r0 == 0) goto L_0x01f0
            r0 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
            goto L_0x01d5
        L_0x01f0:
            java.lang.String r0 = "Non-standard token 'Infinity': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r12._reportError(r0)
            goto L_0x027c
        L_0x01f7:
            int r1 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r1 < r0) goto L_0x0208
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x0208
            java.lang.String r0 = " in a value"
            r12._reportInvalidEOF(r0)
        L_0x0208:
            char[] r2 = r12._inputBuffer
            int r1 = r12._inputPtr
            int r0 = r1 + 1
            r12._inputPtr = r0
            char r1 = r2[r1]
            r0 = 0
            X.11d r0 = r12._handleInvalidNumberStart(r1, r0)
            goto L_0x01d9
        L_0x0218:
            X.0k1 r0 = X.C10390k1.ALLOW_SINGLE_QUOTES
            boolean r0 = r12.isEnabled(r0)
            if (r0 == 0) goto L_0x027c
            X.1eY r0 = r12._textBuffer
            char[] r4 = r0.emptyAndGetCurrentSegment()
            X.1eY r0 = r12._textBuffer
            int r3 = r0._currentSize
        L_0x022a:
            int r1 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r1 < r0) goto L_0x023b
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x023b
            java.lang.String r0 = ": was expecting closing quote for a string value"
            r12._reportInvalidEOF(r0)
        L_0x023b:
            char[] r2 = r12._inputBuffer
            int r1 = r12._inputPtr
            int r0 = r1 + 1
            r12._inputPtr = r0
            char r1 = r2[r1]
            r0 = 92
            if (r1 > r0) goto L_0x024f
            if (r1 != r0) goto L_0x025f
            char r1 = r12._decodeEscaped()
        L_0x024f:
            int r0 = r4.length
            if (r3 < r0) goto L_0x0259
            X.1eY r0 = r12._textBuffer
            char[] r4 = r0.finishCurrentSegment()
            r3 = 0
        L_0x0259:
            int r0 = r3 + 1
            r4[r3] = r1
            r3 = r0
            goto L_0x022a
        L_0x025f:
            r0 = 39
            if (r1 > r0) goto L_0x024f
            if (r1 != r0) goto L_0x026d
            X.1eY r0 = r12._textBuffer
            r0._currentSize = r3
            X.11d r0 = X.C182811d.VALUE_STRING
            goto L_0x01d9
        L_0x026d:
            r0 = 32
            if (r1 >= r0) goto L_0x024f
            java.lang.String r0 = "string value"
            r12._throwUnquotedSpace(r1, r0)
            goto L_0x024f
        L_0x0277:
            java.lang.String r0 = "Non-standard token 'NaN': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r12._reportError(r0)
        L_0x027c:
            java.lang.String r0 = "expected a valid value (number, String, array, object, 'true', 'false' or 'null')"
            r12._reportUnexpectedChar(r1, r0)
            r0 = 0
            goto L_0x01d9
        L_0x0284:
            if (r17 != 0) goto L_0x0292
            X.1eZ r2 = r12._parsingContext
            int r1 = r12._tokenInputRow
            int r0 = r12._tokenInputCol
            X.1eZ r0 = r2.createChildObjectContext(r1, r0)
            r12._parsingContext = r0
        L_0x0292:
            X.11d r0 = X.C182811d.START_OBJECT
            goto L_0x01d9
        L_0x0296:
            java.lang.String r0 = "null"
            r12._matchToken(r0, r11)
            X.11d r0 = X.C182811d.VALUE_NULL
            goto L_0x01d9
        L_0x029f:
            java.lang.String r0 = "false"
            r12._matchToken(r0, r11)
            X.11d r0 = X.C182811d.VALUE_FALSE
            goto L_0x01d9
        L_0x02a8:
            r0 = 995(0x3e3, float:1.394E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r12._reportUnexpectedChar(r1, r0)
        L_0x02b1:
            java.lang.String r0 = "true"
            r12._matchToken(r0, r11)
            X.11d r0 = X.C182811d.VALUE_TRUE
            goto L_0x01d9
        L_0x02ba:
            if (r17 != 0) goto L_0x02c8
            X.1eZ r2 = r12._parsingContext
            int r1 = r12._tokenInputRow
            int r0 = r12._tokenInputCol
            X.1eZ r0 = r2.createChildArrayContext(r1, r0)
            r12._parsingContext = r0
        L_0x02c8:
            X.11d r0 = X.C182811d.START_ARRAY
            goto L_0x01d9
        L_0x02cc:
            r13 = 45
            r7 = 0
            r6 = 1
            r5 = 0
            if (r1 != r13) goto L_0x02d4
            r5 = 1
        L_0x02d4:
            int r3 = r12._inputPtr
            int r8 = r3 + -1
            int r10 = r12._inputEnd
            r9 = 57
            r4 = 48
            if (r5 == 0) goto L_0x02ed
            if (r3 >= r10) goto L_0x02ef
            char[] r1 = r12._inputBuffer
            int r0 = r3 + 1
            char r1 = r1[r3]
            if (r1 > r9) goto L_0x052a
            if (r1 < r4) goto L_0x052a
            r3 = r0
        L_0x02ed:
            if (r1 != r4) goto L_0x049c
        L_0x02ef:
            if (r5 == 0) goto L_0x02f3
            int r8 = r8 + 1
        L_0x02f3:
            r12._inputPtr = r8
            r18 = r12
            r8 = r5
            X.1eY r0 = r12._textBuffer
            char[] r7 = r0.emptyAndGetCurrentSegment()
            r6 = 45
            r4 = 0
            if (r5 == 0) goto L_0x0499
            r7[r4] = r6
            r3 = 1
        L_0x0306:
            int r2 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r2 >= r0) goto L_0x0491
            char[] r1 = r12._inputBuffer
            int r0 = r2 + 1
            r12._inputPtr = r0
            char r1 = r1[r2]
        L_0x0314:
            r10 = 48
            if (r1 != r10) goto L_0x0326
            int r1 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r1 < r0) goto L_0x0452
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x0452
        L_0x0324:
            r1 = 48
        L_0x0326:
            r5 = 0
        L_0x0327:
            r13 = 57
            if (r1 < r10) goto L_0x044e
            if (r1 > r13) goto L_0x044e
            int r5 = r5 + 1
            int r0 = r7.length
            if (r3 < r0) goto L_0x0339
            X.1eY r0 = r12._textBuffer
            char[] r7 = r0.finishCurrentSegment()
            r3 = 0
        L_0x0339:
            int r14 = r3 + 1
            r7[r3] = r1
            int r1 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r1 < r0) goto L_0x0441
            boolean r0 = r18.loadMore()
            if (r0 != 0) goto L_0x0441
            r3 = r14
            r1 = 0
            r16 = 1
        L_0x034d:
            if (r5 != 0) goto L_0x035e
            java.lang.String r14 = "Missing integer part (next char "
            java.lang.String r2 = X.C28261eW._getCharDesc(r1)
            java.lang.String r0 = ")"
            java.lang.String r0 = X.AnonymousClass08S.A0P(r14, r2, r0)
            r12.reportInvalidNumber(r0)
        L_0x035e:
            r0 = 46
            if (r1 != r0) goto L_0x043e
            int r14 = r3 + 1
            r7[r3] = r1
            r2 = 0
        L_0x0367:
            int r3 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r3 < r0) goto L_0x041d
            boolean r0 = r18.loadMore()
            if (r0 != 0) goto L_0x041d
            r16 = 1
        L_0x0375:
            if (r2 != 0) goto L_0x037c
            java.lang.String r0 = "Decimal point not followed by a digit"
            r12.reportUnexpectedNumberChar(r1, r0)
        L_0x037c:
            r3 = r14
        L_0x037d:
            r0 = 101(0x65, float:1.42E-43)
            if (r1 == r0) goto L_0x0385
            r0 = 69
            if (r1 != r0) goto L_0x03f0
        L_0x0385:
            int r0 = r7.length
            if (r3 < r0) goto L_0x038f
            X.1eY r0 = r12._textBuffer
            char[] r7 = r0.finishCurrentSegment()
            r3 = 0
        L_0x038f:
            int r14 = r3 + 1
            r7[r3] = r1
            int r3 = r12._inputPtr
            int r0 = r12._inputEnd
            java.lang.String r15 = "expected a digit for number exponent"
            if (r3 >= r0) goto L_0x0418
            char[] r1 = r12._inputBuffer
            int r0 = r3 + 1
            r12._inputPtr = r0
            char r1 = r1[r3]
        L_0x03a3:
            if (r1 == r6) goto L_0x03a9
            r0 = 43
            if (r1 != r0) goto L_0x03c6
        L_0x03a9:
            int r0 = r7.length
            if (r14 < r0) goto L_0x03b3
            X.1eY r0 = r12._textBuffer
            char[] r7 = r0.finishCurrentSegment()
            r14 = 0
        L_0x03b3:
            int r4 = r14 + 1
            r7[r14] = r1
            int r3 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r3 >= r0) goto L_0x0413
            char[] r1 = r12._inputBuffer
            int r0 = r3 + 1
            r12._inputPtr = r0
            char r1 = r1[r3]
        L_0x03c5:
            r14 = r4
        L_0x03c6:
            r4 = 0
        L_0x03c7:
            if (r1 > r13) goto L_0x0411
            if (r1 < r10) goto L_0x0411
            int r4 = r4 + 1
            int r0 = r7.length
            if (r14 < r0) goto L_0x03d7
            X.1eY r0 = r12._textBuffer
            char[] r7 = r0.finishCurrentSegment()
            r14 = 0
        L_0x03d7:
            int r3 = r14 + 1
            r7[r14] = r1
            int r6 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r6 < r0) goto L_0x0405
            boolean r0 = r18.loadMore()
            if (r0 != 0) goto L_0x0405
            r16 = 1
        L_0x03e9:
            if (r4 != 0) goto L_0x03f0
            java.lang.String r0 = "Exponent indicator not followed by a digit"
            r12.reportUnexpectedNumberChar(r1, r0)
        L_0x03f0:
            if (r16 != 0) goto L_0x03f7
            int r0 = r12._inputPtr
            int r0 = r0 - r11
            r12._inputPtr = r0
        L_0x03f7:
            X.1eY r0 = r12._textBuffer
            r0._currentSize = r3
            if (r2 >= r11) goto L_0x051b
            if (r4 >= r11) goto L_0x051b
            X.11d r0 = r12.resetInt(r8, r5)
            goto L_0x01d9
        L_0x0405:
            char[] r6 = r12._inputBuffer
            int r1 = r12._inputPtr
            int r0 = r1 + 1
            r12._inputPtr = r0
            char r1 = r6[r1]
            r14 = r3
            goto L_0x03c7
        L_0x0411:
            r3 = r14
            goto L_0x03e9
        L_0x0413:
            char r1 = r12.getNextChar(r15)
            goto L_0x03c5
        L_0x0418:
            char r1 = r12.getNextChar(r15)
            goto L_0x03a3
        L_0x041d:
            char[] r3 = r12._inputBuffer
            int r1 = r12._inputPtr
            int r0 = r1 + 1
            r12._inputPtr = r0
            char r1 = r3[r1]
            if (r1 < r10) goto L_0x0375
            if (r1 > r13) goto L_0x0375
            int r2 = r2 + 1
            int r0 = r7.length
            if (r14 < r0) goto L_0x0437
            X.1eY r0 = r12._textBuffer
            char[] r7 = r0.finishCurrentSegment()
            r14 = 0
        L_0x0437:
            int r0 = r14 + 1
            r7[r14] = r1
            r14 = r0
            goto L_0x0367
        L_0x043e:
            r2 = 0
            goto L_0x037d
        L_0x0441:
            char[] r2 = r12._inputBuffer
            int r1 = r12._inputPtr
            int r0 = r1 + 1
            r12._inputPtr = r0
            char r1 = r2[r1]
            r3 = r14
            goto L_0x0327
        L_0x044e:
            r16 = 0
            goto L_0x034d
        L_0x0452:
            char[] r1 = r12._inputBuffer
            int r0 = r12._inputPtr
            char r1 = r1[r0]
            if (r1 < r10) goto L_0x0324
            r2 = 57
            if (r1 > r2) goto L_0x0324
            X.0k1 r0 = X.C10390k1.ALLOW_NUMERIC_LEADING_ZEROS
            boolean r0 = r12.isEnabled(r0)
            if (r0 != 0) goto L_0x046b
            java.lang.String r0 = "Leading zeroes not allowed"
            r12.reportInvalidNumber(r0)
        L_0x046b:
            int r0 = r12._inputPtr
            int r0 = r0 + 1
            r12._inputPtr = r0
            if (r1 != r10) goto L_0x0326
        L_0x0473:
            int r13 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r13 < r0) goto L_0x047f
            boolean r0 = r12.loadMore()
            if (r0 == 0) goto L_0x0326
        L_0x047f:
            char[] r1 = r12._inputBuffer
            int r0 = r12._inputPtr
            char r1 = r1[r0]
            if (r1 < r10) goto L_0x0324
            if (r1 > r2) goto L_0x0324
            int r0 = r0 + 1
            r12._inputPtr = r0
            if (r1 == r10) goto L_0x0473
            goto L_0x0326
        L_0x0491:
            java.lang.String r0 = "No digit following minus sign"
            char r1 = r12.getNextChar(r0)
            goto L_0x0314
        L_0x0499:
            r3 = 0
            goto L_0x0306
        L_0x049c:
            if (r3 >= r10) goto L_0x02ef
            char[] r0 = r12._inputBuffer
            int r2 = r3 + 1
            char r1 = r0[r3]
            if (r1 < r4) goto L_0x04ac
            if (r1 > r9) goto L_0x04ac
            int r6 = r6 + 1
            r3 = r2
            goto L_0x049c
        L_0x04ac:
            r3 = 46
            if (r1 != r3) goto L_0x04c8
            r3 = 0
        L_0x04b1:
            if (r2 >= r10) goto L_0x02ef
            int r14 = r2 + 1
            char r1 = r0[r2]
            if (r1 < r4) goto L_0x04bf
            if (r1 > r9) goto L_0x04bf
            int r3 = r3 + 1
            r2 = r14
            goto L_0x04b1
        L_0x04bf:
            if (r3 != 0) goto L_0x04c6
            java.lang.String r0 = "Decimal point not followed by a digit"
            r12.reportUnexpectedNumberChar(r1, r0)
        L_0x04c6:
            r2 = r14
            goto L_0x04c9
        L_0x04c8:
            r3 = 0
        L_0x04c9:
            r0 = 101(0x65, float:1.42E-43)
            if (r1 == r0) goto L_0x04d1
            r0 = 69
            if (r1 != r0) goto L_0x04fc
        L_0x04d1:
            if (r2 >= r10) goto L_0x02ef
            char[] r14 = r12._inputBuffer
            int r0 = r2 + 1
            char r1 = r14[r2]
            if (r1 == r13) goto L_0x04ee
            r2 = 43
            if (r1 == r2) goto L_0x04ee
            r2 = r0
        L_0x04e0:
            if (r1 > r9) goto L_0x04f5
            if (r1 < r4) goto L_0x04f5
            int r7 = r7 + 1
            if (r2 >= r10) goto L_0x02ef
            int r13 = r2 + 1
            char r1 = r14[r2]
        L_0x04ec:
            r2 = r13
            goto L_0x04e0
        L_0x04ee:
            if (r0 >= r10) goto L_0x02ef
            int r13 = r0 + 1
            char r1 = r14[r0]
            goto L_0x04ec
        L_0x04f5:
            if (r7 != 0) goto L_0x04fc
            java.lang.String r0 = "Exponent indicator not followed by a digit"
            r12.reportUnexpectedNumberChar(r1, r0)
        L_0x04fc:
            int r2 = r2 + -1
            r12._inputPtr = r2
            int r2 = r2 - r8
            X.1eY r1 = r12._textBuffer
            char[] r0 = r12._inputBuffer
            r1.resetWithShared(r0, r8, r2)
            if (r3 >= r11) goto L_0x0512
            if (r7 >= r11) goto L_0x0512
            X.11d r0 = r12.resetInt(r5, r6)
            goto L_0x01d9
        L_0x0512:
            r12._numberNegative = r5
            r12._intLength = r6
            r12._fractLength = r3
            r12._expLength = r7
            goto L_0x0523
        L_0x051b:
            r12._numberNegative = r8
            r12._intLength = r5
            r12._fractLength = r2
            r12._expLength = r4
        L_0x0523:
            r0 = 0
            r12._numTypesValid = r0
            X.11d r0 = X.C182811d.VALUE_NUMBER_FLOAT
            goto L_0x01d9
        L_0x052a:
            r12._inputPtr = r0
            X.11d r0 = r12._handleInvalidNumberStart(r1, r11)
            goto L_0x01d9
        L_0x0532:
            r12._tokenIncomplete = r11
            X.11d r0 = X.C182811d.VALUE_STRING
            goto L_0x01d9
        L_0x0538:
            int r0 = r0 * 33
            int r0 = r0 + r2
            int r8 = r8 + 1
            if (r8 < r13) goto L_0x0168
            goto L_0x0174
        L_0x0541:
            X.0k1 r0 = X.C10390k1.ALLOW_UNQUOTED_FIELD_NAMES
            boolean r0 = r12.isEnabled(r0)
            if (r0 != 0) goto L_0x054e
            java.lang.String r0 = "was expecting double-quote to start field name"
            r12._reportUnexpectedChar(r1, r0)
        L_0x054e:
            int[] r4 = X.AnonymousClass11z.sInputCodesJsNames
            int r7 = r4.length
            if (r1 >= r7) goto L_0x0613
            r0 = r4[r1]
            if (r0 != 0) goto L_0x0610
            r0 = 48
            if (r1 < r0) goto L_0x055f
            r0 = 57
            if (r1 <= r0) goto L_0x0610
        L_0x055f:
            r0 = 1
        L_0x0560:
            if (r0 != 0) goto L_0x0567
            java.lang.String r0 = "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name"
            r12._reportUnexpectedChar(r1, r0)
        L_0x0567:
            int r6 = r12._inputPtr
            int r5 = r12._hashSeed
            int r3 = r12._inputEnd
            if (r6 >= r3) goto L_0x05a5
        L_0x056f:
            char[] r2 = r12._inputBuffer
            char r1 = r2[r6]
            if (r1 >= r7) goto L_0x0587
            r0 = r4[r1]
            if (r0 == 0) goto L_0x059e
            int r1 = r12._inputPtr
            int r1 = r1 - r11
            r12._inputPtr = r6
            X.0k3 r0 = r12._symbols
            int r6 = r6 - r1
            java.lang.String r0 = r0.findSymbol(r2, r1, r6, r5)
            goto L_0x017a
        L_0x0587:
            char r0 = (char) r1
            boolean r0 = java.lang.Character.isJavaIdentifierPart(r0)
            if (r0 != 0) goto L_0x059e
            int r2 = r12._inputPtr
            int r2 = r2 - r11
            r12._inputPtr = r6
            X.0k3 r1 = r12._symbols
            char[] r0 = r12._inputBuffer
            int r6 = r6 - r2
            java.lang.String r0 = r1.findSymbol(r0, r2, r6, r5)
            goto L_0x017a
        L_0x059e:
            int r5 = r5 * 33
            int r5 = r5 + r1
            int r6 = r6 + 1
            if (r6 < r3) goto L_0x056f
        L_0x05a5:
            int r3 = r12._inputPtr
            int r3 = r3 - r11
            r12._inputPtr = r6
            X.1eY r1 = r12._textBuffer
            char[] r0 = r12._inputBuffer
            int r6 = r6 - r3
            r1.resetWithShared(r0, r3, r6)
            X.1eY r0 = r12._textBuffer
            char[] r6 = r0.getCurrentSegment()
            X.1eY r0 = r12._textBuffer
            int r7 = r0._currentSize
            int r3 = r4.length
        L_0x05bd:
            int r1 = r12._inputPtr
            int r0 = r12._inputEnd
            if (r1 < r0) goto L_0x05e2
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x05e2
        L_0x05c9:
            X.1eY r0 = r12._textBuffer
            r0._currentSize = r7
            char[] r3 = r0.getTextBuffer()
            int r2 = r0._inputStart
            if (r2 >= 0) goto L_0x05d6
            r2 = 0
        L_0x05d6:
            int r1 = r0.size()
            X.0k3 r0 = r12._symbols
            java.lang.String r0 = r0.findSymbol(r3, r2, r1, r5)
            goto L_0x017a
        L_0x05e2:
            char[] r1 = r12._inputBuffer
            int r0 = r12._inputPtr
            char r2 = r1[r0]
            if (r2 > r3) goto L_0x05ef
            r0 = r4[r2]
            if (r0 == 0) goto L_0x05f6
            goto L_0x05c9
        L_0x05ef:
            boolean r0 = java.lang.Character.isJavaIdentifierPart(r2)
            if (r0 != 0) goto L_0x05f6
            goto L_0x05c9
        L_0x05f6:
            int r0 = r12._inputPtr
            int r0 = r0 + 1
            r12._inputPtr = r0
            int r5 = r5 * 33
            int r5 = r5 + r2
            int r1 = r7 + 1
            r6[r7] = r2
            int r0 = r6.length
            if (r1 < r0) goto L_0x060e
            X.1eY r0 = r12._textBuffer
            char[] r6 = r0.finishCurrentSegment()
            r7 = 0
            goto L_0x05bd
        L_0x060e:
            r7 = r1
            goto L_0x05bd
        L_0x0610:
            r0 = 0
            goto L_0x0560
        L_0x0613:
            char r0 = (char) r1
            boolean r0 = java.lang.Character.isJavaIdentifierPart(r0)
            goto L_0x0560
        L_0x061a:
            int r8 = r12._inputPtr
            r7 = r8
            int r0 = r12._hashSeed
            int r6 = r12._inputEnd
            if (r8 >= r6) goto L_0x0646
            int[] r5 = X.AnonymousClass11z.sInputCodes
            int r4 = r5.length
        L_0x0626:
            char[] r3 = r12._inputBuffer
            char r2 = r3[r8]
            if (r2 >= r4) goto L_0x063f
            r1 = r5[r2]
            if (r1 == 0) goto L_0x063f
            if (r2 != r13) goto L_0x0646
        L_0x0632:
            int r1 = r8 + 1
            r12._inputPtr = r1
            X.0k3 r1 = r12._symbols
            int r8 = r8 - r7
            java.lang.String r0 = r1.findSymbol(r3, r7, r8, r0)
            goto L_0x017a
        L_0x063f:
            int r0 = r0 * 33
            int r0 = r0 + r2
            int r8 = r8 + 1
            if (r8 < r6) goto L_0x0626
        L_0x0646:
            r12._inputPtr = r8
            java.lang.String r0 = r12._parseFieldName2(r7, r0, r13)
            goto L_0x017a
        L_0x064e:
            r2 = 0
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28241eU.nextToken():X.11d");
    }

    private void _finishString() {
        int i = this._inputPtr;
        int i2 = i;
        int i3 = this._inputEnd;
        if (i < i3) {
            int[] iArr = AnonymousClass11z.sInputCodes;
            int length = iArr.length;
            while (true) {
                char[] cArr = this._inputBuffer;
                char c = cArr[i];
                if (c >= length || iArr[c] == 0) {
                    i++;
                    if (i >= i3) {
                        break;
                    }
                } else if (c == '\"') {
                    this._textBuffer.resetWithShared(cArr, i2, i - i2);
                    this._inputPtr = i + 1;
                    return;
                }
            }
        }
        C28281eY r5 = this._textBuffer;
        char[] cArr2 = this._inputBuffer;
        int i4 = i - i2;
        r5._inputBuffer = null;
        r5._inputStart = -1;
        r5._inputLen = 0;
        r5._resultString = null;
        r5._resultArray = null;
        if (r5._hasSegments) {
            C28281eY.clearSegments(r5);
        } else if (r5._currentSegment == null) {
            r5._currentSegment = C28281eY.findBuffer(r5, i4);
        }
        r5._segmentSize = 0;
        r5._currentSize = 0;
        r5.append(cArr2, i2, i4);
        this._inputPtr = i;
        char[] currentSegment = this._textBuffer.getCurrentSegment();
        int i5 = this._textBuffer._currentSize;
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(": was expecting closing quote for a string value");
            }
            char[] cArr3 = this._inputBuffer;
            int i6 = this._inputPtr;
            this._inputPtr = i6 + 1;
            char c2 = cArr3[i6];
            if (c2 <= '\\') {
                if (c2 == '\\') {
                    c2 = _decodeEscaped();
                } else if (c2 <= '\"') {
                    if (c2 == '\"') {
                        this._textBuffer._currentSize = i5;
                        return;
                    } else if (c2 < ' ') {
                        _throwUnquotedSpace(c2, "string value");
                    }
                }
            }
            if (i5 >= currentSegment.length) {
                currentSegment = this._textBuffer.finishCurrentSegment();
                i5 = 0;
            }
            currentSegment[i5] = c2;
            i5++;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, char], vars: [r8v0 ?, r8v1 ?, r8v2 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:48)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    private X.C182811d _handleInvalidNumberStart(
/*
Method generation error in method: X.1eU._handleInvalidNumberStart(int, boolean):X.11d, dex: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r8v0 ?
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:189)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:161)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:133)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:317)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
    	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
    	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
    	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
    	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
    	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
    	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
    	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
    	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
    	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
    	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
    	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
    	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
    	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
    	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
    	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
    	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
    	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
    
*/

    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String _parseFieldName2(int r6, int r7, int r8) {
        /*
            r5 = this;
            X.1eY r2 = r5._textBuffer
            char[] r1 = r5._inputBuffer
            int r0 = r5._inputPtr
            int r0 = r0 - r6
            r2.resetWithShared(r1, r6, r0)
            X.1eY r0 = r5._textBuffer
            char[] r4 = r0.getCurrentSegment()
            X.1eY r0 = r5._textBuffer
            int r3 = r0._currentSize
        L_0x0014:
            int r1 = r5._inputPtr
            int r0 = r5._inputEnd
            if (r1 < r0) goto L_0x002c
            boolean r0 = r5.loadMore()
            if (r0 != 0) goto L_0x002c
            java.lang.String r2 = ": was expecting closing '"
            char r1 = (char) r8
            java.lang.String r0 = "' for name"
            java.lang.String r0 = X.AnonymousClass08S.A07(r2, r1, r0)
            r5._reportInvalidEOF(r0)
        L_0x002c:
            char[] r2 = r5._inputBuffer
            int r1 = r5._inputPtr
            int r0 = r1 + 1
            r5._inputPtr = r0
            char r1 = r2[r1]
            r0 = 92
            if (r1 > r0) goto L_0x0079
            if (r1 != r0) goto L_0x0054
            char r0 = r5._decodeEscaped()
        L_0x0040:
            int r7 = r7 * 33
            int r7 = r7 + r1
            int r1 = r3 + 1
            r4[r3] = r0
            int r0 = r4.length
            if (r1 < r0) goto L_0x0052
            X.1eY r0 = r5._textBuffer
            char[] r4 = r0.finishCurrentSegment()
            r3 = 0
            goto L_0x0014
        L_0x0052:
            r3 = r1
            goto L_0x0014
        L_0x0054:
            if (r1 > r8) goto L_0x0079
            if (r1 != r8) goto L_0x0070
            X.1eY r0 = r5._textBuffer
            r0._currentSize = r3
            char[] r3 = r0.getTextBuffer()
            int r2 = r0._inputStart
            if (r2 >= 0) goto L_0x0065
            r2 = 0
        L_0x0065:
            int r1 = r0.size()
            X.0k3 r0 = r5._symbols
            java.lang.String r0 = r0.findSymbol(r3, r2, r1, r7)
            return r0
        L_0x0070:
            r0 = 32
            if (r1 >= r0) goto L_0x0079
            java.lang.String r0 = "name"
            r5._throwUnquotedSpace(r1, r0)
        L_0x0079:
            r0 = r1
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28241eU._parseFieldName2(int, int, int):java.lang.String");
    }

    private void _reportInvalidToken(String str) {
        StringBuilder sb = new StringBuilder(str);
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                break;
            }
            char c = this._inputBuffer[this._inputPtr];
            if (!Character.isJavaIdentifierPart(c)) {
                break;
            }
            this._inputPtr++;
            sb.append(c);
        }
        _reportError(AnonymousClass08S.A0P("Unrecognized token '", sb.toString(), "': was expecting "));
    }

    private void _skipCR() {
        if (this._inputPtr < this._inputEnd || loadMore()) {
            char[] cArr = this._inputBuffer;
            int i = this._inputPtr;
            if (cArr[i] == 10) {
                this._inputPtr = i + 1;
            }
        }
        this._currInputRow++;
        this._currInputRowStart = this._inputPtr;
    }

    private void _skipComment() {
        if (!isEnabled(C10390k1.ALLOW_COMMENTS)) {
            _reportUnexpectedChar(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in a comment");
        }
        char[] cArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        char c = cArr[i];
        if (c == '/') {
            while (true) {
                if (this._inputPtr < this._inputEnd || loadMore()) {
                    char[] cArr2 = this._inputBuffer;
                    int i2 = this._inputPtr;
                    int i3 = i2 + 1;
                    this._inputPtr = i3;
                    char c2 = cArr2[i2];
                    if (c2 < ' ') {
                        if (c2 == 10) {
                            this._currInputRow++;
                            this._currInputRowStart = i3;
                            return;
                        } else if (c2 == 13) {
                            _skipCR();
                            return;
                        } else if (c2 != 9) {
                            _throwInvalidSpace(c2);
                        }
                    }
                } else {
                    return;
                }
            }
        } else if (c == '*') {
            while (true) {
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    break;
                }
                char[] cArr3 = this._inputBuffer;
                int i4 = this._inputPtr;
                int i5 = i4 + 1;
                this._inputPtr = i5;
                char c3 = cArr3[i4];
                if (c3 <= '*') {
                    if (c3 == '*') {
                        if (i5 >= this._inputEnd && !loadMore()) {
                            break;
                        }
                        char[] cArr4 = this._inputBuffer;
                        int i6 = this._inputPtr;
                        if (cArr4[i6] == '/') {
                            this._inputPtr = i6 + 1;
                            return;
                        }
                    } else if (c3 < ' ') {
                        if (c3 == 10) {
                            this._currInputRow++;
                            this._currInputRowStart = i5;
                        } else if (c3 == 13) {
                            _skipCR();
                        } else if (c3 != 9) {
                            _throwInvalidSpace(c3);
                        }
                    }
                }
            }
            _reportInvalidEOF(" in a comment");
        } else {
            _reportUnexpectedChar(c, "was expecting either '*' or '/' for a comment");
        }
    }

    private int _skipWS() {
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                char[] cArr = this._inputBuffer;
                int i = this._inputPtr;
                int i2 = i + 1;
                this._inputPtr = i2;
                char c = cArr[i];
                if (c > ' ') {
                    if (c != '/') {
                        return c;
                    }
                    _skipComment();
                } else if (c != ' ') {
                    if (c == 10) {
                        this._currInputRow++;
                        this._currInputRowStart = i2;
                    } else if (c == 13) {
                        _skipCR();
                    } else if (c != 9) {
                        _throwInvalidSpace(c);
                    }
                }
            } else {
                throw _constructError(AnonymousClass08S.A0P("Unexpected end-of-input within/between ", this._parsingContext.getTypeDesc(), " entries"));
            }
        }
    }

    private char getNextChar(String str) {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(str);
        }
        char[] cArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        return cArr[i];
    }

    public void _closeInput() {
        Reader reader = this._reader;
        if (reader != null) {
            if (this._ioContext._managedResource || isEnabled(C10390k1.AUTO_CLOSE_SOURCE)) {
                reader.close();
            }
            this._reader = null;
        }
    }

    public char _decodeEscaped() {
        int i;
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in character escape sequence");
        }
        char[] cArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        char c = cArr[i2];
        if (c == '\"' || c == '/' || c == '\\') {
            return c;
        }
        if (c == 'b') {
            return 8;
        }
        if (c == 'f') {
            return 12;
        }
        if (c == 'n') {
            return 10;
        }
        if (c == 'r') {
            return 13;
        }
        if (c == 't') {
            return 9;
        }
        if (c != 'u') {
            _handleUnrecognizedCharacterEscape(c);
            return c;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < 4; i4++) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(" in character escape sequence");
            }
            char[] cArr2 = this._inputBuffer;
            int i5 = this._inputPtr;
            this._inputPtr = i5 + 1;
            char c2 = cArr2[i5];
            if (c2 > 127) {
                i = -1;
            } else {
                i = AnonymousClass11z.sHexValues[c2];
            }
            if (i < 0) {
                _reportUnexpectedChar(c2, "expected a hex-digit for character escape sequence");
            }
            i3 = (i3 << 4) | i;
        }
        return (char) i3;
    }

    public byte[] getBinaryValue(C10350jx r10) {
        byte[] bArr;
        C182811d r2 = this._currToken;
        if (r2 != C182811d.VALUE_STRING && (r2 != C182811d.VALUE_EMBEDDED_OBJECT || this._binaryValue == null)) {
            _reportError(C22298Ase.$const$string(19) + r2 + ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        if (this._tokenIncomplete) {
            try {
                A21 _getByteArrayBuilder = _getByteArrayBuilder();
                while (true) {
                    if (this._inputPtr >= this._inputEnd) {
                        loadMoreGuaranteed();
                    }
                    char[] cArr = this._inputBuffer;
                    int i = this._inputPtr;
                    this._inputPtr = i + 1;
                    char c = cArr[i];
                    if (c > ' ') {
                        int decodeBase64Char = r10.decodeBase64Char(c);
                        if (decodeBase64Char < 0) {
                            if (c == '\"') {
                                bArr = _getByteArrayBuilder.toByteArray();
                                break;
                            }
                            decodeBase64Char = _decodeBase64Escape(r10, c, 0);
                            if (decodeBase64Char < 0) {
                            }
                        }
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        char[] cArr2 = this._inputBuffer;
                        int i2 = this._inputPtr;
                        this._inputPtr = i2 + 1;
                        char c2 = cArr2[i2];
                        int decodeBase64Char2 = r10.decodeBase64Char(c2);
                        if (decodeBase64Char2 < 0) {
                            decodeBase64Char2 = _decodeBase64Escape(r10, c2, 1);
                        }
                        int i3 = (decodeBase64Char << 6) | decodeBase64Char2;
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        char[] cArr3 = this._inputBuffer;
                        int i4 = this._inputPtr;
                        this._inputPtr = i4 + 1;
                        char c3 = cArr3[i4];
                        int decodeBase64Char3 = r10.decodeBase64Char(c3);
                        if (decodeBase64Char3 < 0) {
                            if (decodeBase64Char3 != -2) {
                                if (c3 == '\"' && !r10._usesPadding) {
                                    _getByteArrayBuilder.append(i3 >> 4);
                                    bArr = _getByteArrayBuilder.toByteArray();
                                    break;
                                }
                                decodeBase64Char3 = _decodeBase64Escape(r10, c3, 2);
                            }
                            if (decodeBase64Char3 == -2) {
                                if (this._inputPtr >= this._inputEnd) {
                                    loadMoreGuaranteed();
                                }
                                char[] cArr4 = this._inputBuffer;
                                int i5 = this._inputPtr;
                                this._inputPtr = i5 + 1;
                                char c4 = cArr4[i5];
                                char c5 = r10._paddingChar;
                                boolean z = false;
                                if (c4 == c5) {
                                    z = true;
                                }
                                if (z) {
                                    _getByteArrayBuilder.append(i3 >> 4);
                                } else {
                                    throw C28251eV.reportInvalidBase64Char(r10, c4, 3, AnonymousClass08S.A07("expected padding character '", c5, "'"));
                                }
                            }
                        }
                        int i6 = (i3 << 6) | decodeBase64Char3;
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        char[] cArr5 = this._inputBuffer;
                        int i7 = this._inputPtr;
                        this._inputPtr = i7 + 1;
                        char c6 = cArr5[i7];
                        int decodeBase64Char4 = r10.decodeBase64Char(c6);
                        if (decodeBase64Char4 < 0) {
                            if (decodeBase64Char4 != -2) {
                                if (c6 == '\"' && !r10._usesPadding) {
                                    _getByteArrayBuilder.appendTwoBytes(i6 >> 2);
                                    bArr = _getByteArrayBuilder.toByteArray();
                                    break;
                                }
                                decodeBase64Char4 = _decodeBase64Escape(r10, c6, 3);
                            }
                            if (decodeBase64Char4 == -2) {
                                _getByteArrayBuilder.appendTwoBytes(i6 >> 2);
                            }
                        }
                        _getByteArrayBuilder.appendThreeBytes((i6 << 6) | decodeBase64Char4);
                    }
                }
                this._binaryValue = bArr;
                this._tokenIncomplete = false;
            } catch (IllegalArgumentException e) {
                throw _constructError("Failed to decode VALUE_STRING as base64 (" + r10 + "): " + e.getMessage());
            }
        } else if (this._binaryValue == null) {
            A21 _getByteArrayBuilder2 = _getByteArrayBuilder();
            _decodeBase64(getText(), _getByteArrayBuilder2, r10);
            this._binaryValue = _getByteArrayBuilder2.toByteArray();
        }
        return this._binaryValue;
    }

    public C09980jK getCodec() {
        return this._objectCodec;
    }

    public Object getInputSource() {
        return this._reader;
    }

    public String getText() {
        C182811d r2 = this._currToken;
        if (r2 == C182811d.VALUE_STRING) {
            if (this._tokenIncomplete) {
                this._tokenIncomplete = false;
                _finishString();
            }
            return this._textBuffer.contentsAsString();
        } else if (r2 == null) {
            return null;
        } else {
            int i = C25102CXs.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r2.ordinal()];
            if (i == 1) {
                return this._parsingContext._currentName;
            }
            if (i == 2 || i == 3 || i == 4) {
                return this._textBuffer.contentsAsString();
            }
            return r2._serialized;
        }
    }

    public char[] getTextCharacters() {
        C182811d r2 = this._currToken;
        if (r2 == null) {
            return null;
        }
        int i = C25102CXs.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r2.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (!(i == 3 || i == 4)) {
                    return r2._serializedChars;
                }
            } else if (this._tokenIncomplete) {
                this._tokenIncomplete = false;
                _finishString();
            }
            return this._textBuffer.getTextBuffer();
        }
        if (!this._nameCopied) {
            String str = this._parsingContext._currentName;
            int length = str.length();
            char[] cArr = this._nameCopyBuffer;
            if (cArr == null) {
                this._nameCopyBuffer = this._ioContext.allocNameCopyBuffer(length);
            } else if (cArr.length < length) {
                this._nameCopyBuffer = new char[length];
            }
            str.getChars(0, length, this._nameCopyBuffer, 0);
            this._nameCopied = true;
        }
        return this._nameCopyBuffer;
    }

    public int getTextLength() {
        C182811d r3 = this._currToken;
        if (r3 == null) {
            return 0;
        }
        int i = C25102CXs.$SwitchMap$com$fasterxml$jackson$core$JsonToken[r3.ordinal()];
        if (i == 1) {
            return this._parsingContext._currentName.length();
        }
        if (i != 2) {
            if (!(i == 3 || i == 4)) {
                return r3._serializedChars.length;
            }
        } else if (this._tokenIncomplete) {
            this._tokenIncomplete = false;
            _finishString();
        }
        return this._textBuffer.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        if (r1 != 4) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getTextOffset() {
        /*
            r3 = this;
            X.11d r0 = r3._currToken
            r2 = 0
            if (r0 == 0) goto L_0x0019
            int[] r1 = X.C25102CXs.$SwitchMap$com$fasterxml$jackson$core$JsonToken
            int r0 = r0.ordinal()
            r1 = r1[r0]
            r0 = 1
            if (r1 == r0) goto L_0x0019
            r0 = 2
            if (r1 == r0) goto L_0x001a
            r0 = 3
            if (r1 == r0) goto L_0x0023
            r0 = 4
            if (r1 == r0) goto L_0x0023
        L_0x0019:
            return r2
        L_0x001a:
            boolean r0 = r3._tokenIncomplete
            if (r0 == 0) goto L_0x0023
            r3._tokenIncomplete = r2
            r3._finishString()
        L_0x0023:
            X.1eY r0 = r3._textBuffer
            int r0 = r0._inputStart
            if (r0 >= 0) goto L_0x002a
            r0 = 0
        L_0x002a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28241eU.getTextOffset():int");
    }

    public boolean loadMore() {
        long j = this._currInputProcessed;
        int i = this._inputEnd;
        this._currInputProcessed = j + ((long) i);
        this._currInputRowStart -= i;
        Reader reader = this._reader;
        if (reader != null) {
            char[] cArr = this._inputBuffer;
            int read = reader.read(cArr, 0, cArr.length);
            if (read > 0) {
                this._inputPtr = 0;
                this._inputEnd = read;
                return true;
            }
            _closeInput();
            if (read == 0) {
                throw new IOException(AnonymousClass08S.A09("Reader returned 0 characters when trying to read ", this._inputEnd));
            }
        }
        return false;
    }

    public int nextIntValue(int i) {
        if (this._currToken == C182811d.FIELD_NAME) {
            this._nameCopied = false;
            C182811d r1 = this._nextToken;
            this._nextToken = null;
            this._currToken = r1;
            if (r1 == C182811d.VALUE_NUMBER_INT) {
                return getIntValue();
            }
            if (r1 == C182811d.START_ARRAY) {
                this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
            } else if (r1 == C182811d.START_OBJECT) {
                this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                return i;
            }
        } else if (nextToken() == C182811d.VALUE_NUMBER_INT) {
            return getIntValue();
        }
        return i;
    }

    public String nextTextValue() {
        if (this._currToken == C182811d.FIELD_NAME) {
            this._nameCopied = false;
            C182811d r1 = this._nextToken;
            this._nextToken = null;
            this._currToken = r1;
            if (r1 == C182811d.VALUE_STRING) {
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                }
                return this._textBuffer.contentsAsString();
            } else if (r1 == C182811d.START_ARRAY) {
                this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
            } else if (r1 == C182811d.START_OBJECT) {
                this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                return null;
            }
        } else if (nextToken() == C182811d.VALUE_STRING) {
            return getText();
        }
        return null;
    }

    public int releaseBuffered(Writer writer) {
        int i = this._inputEnd;
        int i2 = this._inputPtr;
        int i3 = i - i2;
        if (i3 < 1) {
            return 0;
        }
        writer.write(this._inputBuffer, i2, i3);
        return i3;
    }

    public C28241eU(AnonymousClass11K r4, int i, Reader reader, C09980jK r7, C10410k3 r8) {
        super(r4, i);
        this._reader = reader;
        if (r4._tokenCBuffer == null) {
            char[] allocCharBuffer = r4._bufferRecycler.allocCharBuffer(AnonymousClass11P.TOKEN_BUFFER, 0);
            r4._tokenCBuffer = allocCharBuffer;
            this._inputBuffer = allocCharBuffer;
            this._objectCodec = r7;
            this._symbols = r8;
            this._hashSeed = r8._hashSeed;
            return;
        }
        throw new IllegalStateException("Trying to call same allocXxx() method second time");
    }

    private void _matchToken(String str, int i) {
        int i2;
        char c;
        int length = str.length();
        do {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidToken(str.substring(0, i));
            }
            if (this._inputBuffer[this._inputPtr] != str.charAt(i)) {
                _reportInvalidToken(str.substring(0, i));
            }
            i2 = this._inputPtr + 1;
            this._inputPtr = i2;
            i++;
        } while (i < length);
        if ((i2 < this._inputEnd || loadMore()) && (c = this._inputBuffer[this._inputPtr]) >= '0' && c != ']' && c != '}' && Character.isJavaIdentifierPart(c)) {
            _reportInvalidToken(str.substring(0, i));
        }
    }

    public void _releaseBuffers() {
        super._releaseBuffers();
        char[] cArr = this._inputBuffer;
        if (cArr != null) {
            this._inputBuffer = null;
            AnonymousClass11K r1 = this._ioContext;
            if (cArr == null) {
                return;
            }
            if (cArr == r1._tokenCBuffer) {
                r1._tokenCBuffer = null;
                r1._bufferRecycler._charBuffers[AnonymousClass11P.TOKEN_BUFFER.ordinal()] = cArr;
                return;
            }
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
    }

    public void close() {
        C10410k3 r4;
        super.close();
        C10410k3 r5 = this._symbols;
        if (r5._dirty && (r4 = r5._parent) != null) {
            int i = r5._size;
            if (i > 12000 || r5._longestCollisionList > 63) {
                synchronized (r4) {
                    try {
                        C10410k3.initTables(r4, 64);
                        r4._dirty = false;
                    } catch (Throwable th) {
                        while (true) {
                            th = th;
                        }
                    }
                }
            } else if (i > r4._size) {
                synchronized (r4) {
                    try {
                        r4._symbols = r5._symbols;
                        r4._buckets = r5._buckets;
                        r4._size = r5._size;
                        r4._sizeThreshold = r5._sizeThreshold;
                        r4._indexMask = r5._indexMask;
                        r4._longestCollisionList = r5._longestCollisionList;
                        r4._dirty = false;
                    } catch (Throwable th2) {
                        th = th2;
                        throw th;
                    }
                }
            }
            r5._dirty = false;
        }
    }

    public void setCodec(C09980jK r1) {
        this._objectCodec = r1;
    }

    public String getValueAsString() {
        if (this._currToken != C182811d.VALUE_STRING) {
            return super.getValueAsString(null);
        }
        if (this._tokenIncomplete) {
            this._tokenIncomplete = false;
            _finishString();
        }
        return this._textBuffer.contentsAsString();
    }

    public String getValueAsString(String str) {
        if (this._currToken != C182811d.VALUE_STRING) {
            return super.getValueAsString(str);
        }
        if (this._tokenIncomplete) {
            this._tokenIncomplete = false;
            _finishString();
        }
        return this._textBuffer.contentsAsString();
    }
}
