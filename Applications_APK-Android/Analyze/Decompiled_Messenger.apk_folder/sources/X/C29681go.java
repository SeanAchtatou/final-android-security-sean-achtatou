package X;

import android.view.View;
import android.view.ViewTreeObserver;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.1go  reason: invalid class name and case insensitive filesystem */
public final class C29681go implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener {
    public WeakHashMap A00 = new WeakHashMap();

    public void onViewDetachedFromWindow(View view) {
    }

    public void onGlobalLayout() {
        for (Map.Entry entry : this.A00.entrySet()) {
            View view = (View) entry.getKey();
            boolean booleanValue = ((Boolean) entry.getValue()).booleanValue();
            boolean z = false;
            if (view.getVisibility() == 0) {
                z = true;
            }
            if (booleanValue != z) {
                if (z) {
                    C15320v6.notifyViewAccessibilityStateChangedIfNeeded(view, 16);
                }
                this.A00.put(view, Boolean.valueOf(z));
            }
        }
    }

    public void onViewAttachedToWindow(View view) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(this);
    }
}
