package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import io.card.payment.BuildConfig;
import java.io.File;
import java.util.Map;

/* renamed from: X.1Wv  reason: invalid class name and case insensitive filesystem */
public final class C24721Wv {
    public final Context A00;
    public final SharedPreferences A01;
    public final C24731Ww A02;
    public final Map A03 = new AnonymousClass04a();

    public final synchronized String A02() {
        return this.A01.getString("topic_operaion_queue", BuildConfig.FLAVOR);
    }

    public final synchronized void A03() {
        this.A03.clear();
        Context context = this.A00;
        File A04 = AnonymousClass01R.A04(context);
        if (A04 == null || !A04.isDirectory()) {
            Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
            A04 = context.getFilesDir();
        }
        for (File file : A04.listFiles()) {
            if (file.getName().startsWith("com.google.InstanceId")) {
                file.delete();
            }
        }
        this.A01.edit().clear().commit();
    }

    public final synchronized void A04(String str) {
        this.A01.edit().putString("topic_operaion_queue", str).apply();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0053, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0059, code lost:
        if (android.util.Log.isLoggable("FirebaseInstanceId", 3) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006b, code lost:
        new java.lang.String("Error creating file in no backup dir: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C24721Wv(android.content.Context r5, X.C24731Ww r6) {
        /*
            r4 = this;
            java.lang.String r3 = "FirebaseInstanceId"
            r4.<init>()
            X.04a r0 = new X.04a
            r0.<init>()
            r4.A03 = r0
            r4.A00 = r5
            java.lang.String r1 = "com.google.android.gms.appid"
            r0 = 0
            android.content.SharedPreferences r0 = r5.getSharedPreferences(r1, r0)
            r4.A01 = r0
            r4.A02 = r6
            android.content.Context r0 = r4.A00
            java.io.File r2 = X.AnonymousClass01R.A04(r0)
            java.io.File r1 = new java.io.File
            java.lang.String r0 = "com.google.android.gms.appid-no-backup"
            r1.<init>(r2, r0)
            boolean r0 = r1.exists()
            if (r0 != 0) goto L_0x0070
            boolean r0 = r1.createNewFile()     // Catch:{ IOException -> 0x0053 }
            if (r0 == 0) goto L_0x0070
            r1 = r4
            monitor-enter(r1)     // Catch:{ IOException -> 0x0053 }
            android.content.SharedPreferences r0 = r4.A01     // Catch:{ all -> 0x0050 }
            java.util.Map r0 = r0.getAll()     // Catch:{ all -> 0x0050 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0050 }
            monitor-exit(r1)     // Catch:{ IOException -> 0x0053 }
            if (r0 != 0) goto L_0x0070
            r4.A03()     // Catch:{ IOException -> 0x0053 }
            X.1WN r0 = X.AnonymousClass1WN.A00()     // Catch:{ IOException -> 0x0053 }
            com.google.firebase.iid.FirebaseInstanceId r0 = com.google.firebase.iid.FirebaseInstanceId.getInstance(r0)     // Catch:{ IOException -> 0x0053 }
            r0.A08()     // Catch:{ IOException -> 0x0053 }
            return
        L_0x0050:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ IOException -> 0x0053 }
            throw r0     // Catch:{ IOException -> 0x0053 }
        L_0x0053:
            r2 = move-exception
            r0 = 3
            boolean r0 = android.util.Log.isLoggable(r3, r0)
            if (r0 == 0) goto L_0x0070
            java.lang.String r1 = "Error creating file in no backup dir: "
            java.lang.String r0 = r2.getMessage()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r0 = r0.length()
            if (r0 != 0) goto L_0x0070
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
        L_0x0070:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24721Wv.<init>(android.content.Context, X.1Ww):void");
    }

    public static String A00(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    public static String A01(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }
}
