package X;

import android.view.ViewTreeObserver;

/* renamed from: X.19p  reason: invalid class name and case insensitive filesystem */
public final class C197719p implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AnonymousClass1Ri A00;

    public C197719p(AnonymousClass1Ri r1) {
        this.A00 = r1;
    }

    public boolean onPreDraw() {
        AnonymousClass1Ri.A0H(this.A00);
        return true;
    }
}
