package X;

import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.0sI  reason: invalid class name and case insensitive filesystem */
public final class C13920sI implements C13970sO {
    public final /* synthetic */ ThreadListFragment A00;

    public void BZi() {
    }

    public C13920sI(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public void BZh() {
        AnonymousClass6LI r0;
        AnonymousClass1B2 r02 = this.A00.A12;
        if (!(r02 == null || (r0 = r02.A06) == null)) {
            r0.dismiss();
        }
        if (AnonymousClass10R.A01(this.A00.A0a, true)) {
            ((C74163hN) AnonymousClass1XX.A02(27, AnonymousClass1Y3.AoY, this.A00.A0O)).A05();
        }
    }
}
