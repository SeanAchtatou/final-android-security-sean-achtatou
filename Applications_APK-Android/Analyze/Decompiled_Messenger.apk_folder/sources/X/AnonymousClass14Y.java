package X;

import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.14Y  reason: invalid class name */
public class AnonymousClass14Y extends C08880g8 implements Runnable, AnonymousClass0Y0, ScheduledFuture {
    public static final String __redex_internal_original_name = "com.facebook.common.executors.WakingExecutorService$WakingListenableScheduledFuture";
    public final AnonymousClass0XX A00;
    public final /* synthetic */ C29371gJ A01;

    public void addListener(Runnable runnable, Executor executor) {
        this.A00.addListener(runnable, executor);
    }

    public boolean cancel(boolean z) {
        C72013dW r1;
        C29371gJ r3 = this.A01;
        synchronized (r3) {
            Iterator it = r3.A01.iterator();
            while (true) {
                if (!it.hasNext()) {
                    r1 = null;
                    break;
                }
                r1 = (C72013dW) it.next();
                if (r1.A01 == this) {
                    break;
                }
            }
            if (r1 != null) {
                r3.A01.remove(r1);
                C29371gJ.A02(r3);
            }
        }
        return this.A00.cancel(z);
    }

    public int compareTo(Object obj) {
        throw new UnsupportedOperationException();
    }

    public long getDelay(TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }

    public void run() {
        this.A00.run();
    }

    public AnonymousClass14Y(C29371gJ r2, Runnable runnable, Object obj) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0XX(runnable, obj);
    }

    public AnonymousClass14Y(C29371gJ r2, Callable callable) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0XX(callable);
    }
}
