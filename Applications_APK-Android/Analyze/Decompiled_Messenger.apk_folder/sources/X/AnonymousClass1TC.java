package X;

import java.util.concurrent.Executor;

/* renamed from: X.1TC  reason: invalid class name */
public final class AnonymousClass1TC extends AnonymousClass1EP {
    public final AnonymousClass1TD A00;

    public static final AnonymousClass1TC A00(AnonymousClass1XY r3) {
        return new AnonymousClass1TC(AnonymousClass1TD.A00(r3), AnonymousClass0UX.A0U(r3));
    }

    private AnonymousClass1TC(AnonymousClass1TD r1, Executor executor) {
        super(executor);
        this.A00 = r1;
    }
}
