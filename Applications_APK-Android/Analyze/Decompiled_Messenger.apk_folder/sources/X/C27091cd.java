package X;

import android.text.TextUtils;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.messaging.accountswitch.model.MessengerAccountInfo;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.user.model.User;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.Map;
import javax.inject.Singleton;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1cd  reason: invalid class name and case insensitive filesystem */
public final class C27091cd implements C27081cc {
    private static volatile C27091cd A05;
    public C49132bh A00;
    private final AnonymousClass09P A01;
    private final FbSharedPreferences A02;
    @LoggedInUser
    private final C04310Tq A03;
    public volatile int A04;

    public static synchronized int A00(C27091cd r2) {
        int size;
        synchronized (r2) {
            size = r2.A02.AlW(C10980lB.A05).entrySet().size();
        }
        return size;
    }

    private MessengerAccountInfo A01(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            C57382rt r2 = new C57382rt();
            if (jSONObject.has(ErrorReportingConstants.USER_ID_KEY)) {
                r2.A04 = jSONObject.getString(ErrorReportingConstants.USER_ID_KEY);
            }
            if (jSONObject.has("name")) {
                r2.A02 = jSONObject.getString("name");
            }
            if (jSONObject.has("last_logout_timestamp")) {
                r2.A00 = jSONObject.getLong("last_logout_timestamp");
            }
            if (jSONObject.has("unseen_count_access_token")) {
                r2.A03 = jSONObject.getString("unseen_count_access_token");
            }
            if (jSONObject.has("last_unseen_timestamp")) {
                r2.A01 = jSONObject.getLong("last_unseen_timestamp");
            }
            if (jSONObject.has("is_page_account")) {
                r2.A05 = jSONObject.getBoolean("is_page_account");
            }
            return new MessengerAccountInfo(r2);
        } catch (JSONException e) {
            this.A01.softReport("Corrupt MessengerAccountInfo Read", str, e);
            return null;
        }
    }

    public static final C27091cd A02(AnonymousClass1XY r5) {
        if (A05 == null) {
            synchronized (C27091cd.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A05 = new C27091cd(applicationInjector, FbSharedPreferencesModule.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    /* renamed from: A03 */
    public ArrayList Abd() {
        ArrayList arrayList = new ArrayList(5);
        ArrayList<AnonymousClass1Y7> arrayList2 = new ArrayList<>();
        synchronized (this) {
            for (Map.Entry entry : this.A02.AlW(C10980lB.A05).entrySet()) {
                MessengerAccountInfo A012 = A01((String) entry.getValue());
                if (A012 == null || TextUtils.isEmpty(A012.A02) || TextUtils.isEmpty(A012.A04)) {
                    arrayList2.add(entry.getKey());
                } else {
                    arrayList.add(A012);
                }
            }
            for (AnonymousClass1Y7 C1B : arrayList2) {
                C30281hn edit = this.A02.edit();
                edit.C1B(C1B);
                edit.commit();
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.accountswitch.model.MessengerAccountInfo Aba(java.lang.String r5) {
        /*
            r4 = this;
            X.1Y7 r0 = X.C10980lB.A05
            X.063 r3 = r0.A09(r5)
            X.1Y7 r3 = (X.AnonymousClass1Y7) r3
            monitor-enter(r4)
            com.facebook.prefs.shared.FbSharedPreferences r0 = r4.A02     // Catch:{ all -> 0x0038 }
            r2 = 0
            java.lang.String r0 = r0.B4F(r3, r2)     // Catch:{ all -> 0x0038 }
            if (r0 == 0) goto L_0x0036
            com.facebook.messaging.accountswitch.model.MessengerAccountInfo r1 = r4.A01(r0)     // Catch:{ all -> 0x0038 }
            if (r1 == 0) goto L_0x002a
            java.lang.String r0 = r1.A02     // Catch:{ all -> 0x0038 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x002a
            java.lang.String r0 = r1.A04     // Catch:{ all -> 0x0038 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x002a
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            return r1
        L_0x002a:
            com.facebook.prefs.shared.FbSharedPreferences r0 = r4.A02     // Catch:{ all -> 0x0038 }
            X.1hn r0 = r0.edit()     // Catch:{ all -> 0x0038 }
            r0.C1B(r3)     // Catch:{ all -> 0x0038 }
            r0.commit()     // Catch:{ all -> 0x0038 }
        L_0x0036:
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            return r2
        L_0x0038:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27091cd.Aba(java.lang.String):com.facebook.messaging.accountswitch.model.MessengerAccountInfo");
    }

    public int Aw9() {
        return this.A04;
    }

    public boolean BBk() {
        if (this.A04 >= 5) {
            return true;
        }
        return false;
    }

    public MessengerAccountInfo C0K() {
        User user = (User) this.A03.get();
        if (user == null) {
            return null;
        }
        MessengerAccountInfo Aba = Aba(user.A0j);
        if (Aba != null && Aba.A02 != null) {
            return Aba;
        }
        C57382rt r2 = new C57382rt();
        r2.A04 = user.A0j;
        r2.A02 = user.A09();
        r2.A00 = -1;
        r2.A03 = null;
        r2.A01 = 0;
        r2.A05 = false;
        MessengerAccountInfo messengerAccountInfo = new MessengerAccountInfo(r2);
        C4Q(messengerAccountInfo);
        return messengerAccountInfo;
    }

    public void C1K(String str) {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C10980lB.A05.A09(str);
        synchronized (this) {
            C30281hn edit = this.A02.edit();
            edit.C1B(r1);
            edit.commit();
        }
        this.A04 = A00(this);
        C49132bh r0 = this.A00;
        if (r0 != null) {
            C198369Up r12 = r0.A00;
            if (!r12.A0W) {
                C198369Up.A07(r12);
            }
        }
    }

    public void C4Q(MessengerAccountInfo messengerAccountInfo) {
        AnonymousClass1Y7 r3 = (AnonymousClass1Y7) C10980lB.A05.A09(messengerAccountInfo.A04);
        synchronized (this) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(ErrorReportingConstants.USER_ID_KEY, messengerAccountInfo.A04);
                jSONObject.put("name", messengerAccountInfo.A02);
                jSONObject.put("last_logout_timestamp", messengerAccountInfo.A00);
                jSONObject.put("unseen_count_access_token", messengerAccountInfo.A03);
                jSONObject.put("last_unseen_timestamp", messengerAccountInfo.A01);
                jSONObject.put("is_page_account", messengerAccountInfo.A05);
                String jSONObject2 = jSONObject.toString();
                C30281hn edit = this.A02.edit();
                edit.BzC(r3, jSONObject2);
                edit.commit();
            } catch (JSONException e) {
                this.A01.softReport("Corrupt MessengerAccountInfo Write", BuildConfig.FLAVOR, e);
            }
        }
        this.A04 = A00(this);
        C49132bh r0 = this.A00;
        if (r0 != null) {
            C198369Up r1 = r0.A00;
            if (!r1.A0W) {
                C198369Up.A07(r1);
            }
        }
    }

    private C27091cd(AnonymousClass1XY r3, FbSharedPreferences fbSharedPreferences) {
        this.A01 = C04750Wa.A01(r3);
        this.A03 = AnonymousClass0WY.A01(r3);
        this.A02 = fbSharedPreferences;
        if (fbSharedPreferences.BFQ()) {
            this.A04 = A00(this);
        } else {
            this.A02.C0e(new C49272bw(this));
        }
    }

    public void C91(C49132bh r1) {
        this.A00 = r1;
    }
}
