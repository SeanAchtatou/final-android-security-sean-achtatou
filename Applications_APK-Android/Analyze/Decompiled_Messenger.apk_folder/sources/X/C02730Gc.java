package X;

/* renamed from: X.0Gc  reason: invalid class name and case insensitive filesystem */
public final class C02730Gc {
    private AnonymousClass0FM A00;
    private AnonymousClass0FM A01;
    private boolean A02;
    private final AnonymousClass0FM A03;
    private final C007907e A04;

    public AnonymousClass0FM A00() {
        boolean A042 = this.A02 & this.A04.A04(this.A00);
        this.A02 = A042;
        if (!A042) {
            return null;
        }
        this.A00.A07(this.A01, this.A03);
        return this.A03;
    }

    public AnonymousClass0FM A01() {
        if (A00() == null) {
            return null;
        }
        AnonymousClass0FM r1 = this.A01;
        this.A01 = this.A00;
        this.A00 = r1;
        return this.A03;
    }

    public C02730Gc(C007907e r4) {
        this(r4, r4.A03(), r4.A03(), r4.A03());
        this.A02 = r4.A04(this.A01) & this.A02;
    }

    public C02730Gc(C007907e r2, AnonymousClass0FM r3, AnonymousClass0FM r4, AnonymousClass0FM r5) {
        this.A02 = true;
        this.A04 = r2;
        this.A00 = r3;
        this.A01 = r4;
        this.A03 = r5;
    }
}
