package X;

import android.os.AsyncTask;
import android.util.Log;

/* renamed from: X.0E5  reason: invalid class name */
public final class AnonymousClass0E5 extends AsyncTask {
    public final /* synthetic */ AnonymousClass0E1 A00;

    public AnonymousClass0E5(AnonymousClass0E1 r1) {
        this.A00 = r1;
    }

    public Object doInBackground(Object[] objArr) {
        while (true) {
            AnonymousClass0E7 dequeueWork = this.A00.dequeueWork();
            if (dequeueWork == null) {
                return null;
            }
            this.A00.onHandleWork(dequeueWork.getIntent());
            try {
                dequeueWork.ATZ();
            } catch (SecurityException e) {
                if (e.getMessage().contains("Caller no longer running")) {
                    Log.e(AnonymousClass0E1.TAG, "Captured a \"Caller no longer running\"", e);
                } else {
                    throw e;
                }
            }
        }
    }

    public void onCancelled(Object obj) {
        this.A00.processorFinished();
    }

    public void onPostExecute(Object obj) {
        this.A00.processorFinished();
    }
}
