package X;

/* renamed from: X.1Q1  reason: invalid class name */
public final class AnonymousClass1Q1 {
    public static final AnonymousClass1Q1 A02 = new AnonymousClass1Q1(-1, false);
    public static final AnonymousClass1Q1 A03 = new AnonymousClass1Q1(-1, true);
    public final int A00;
    public final boolean A01;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AnonymousClass1Q1)) {
            return false;
        }
        AnonymousClass1Q1 r4 = (AnonymousClass1Q1) obj;
        return this.A00 == r4.A00 && this.A01 == r4.A01;
    }

    public String toString() {
        return String.format(null, "%d defer:%b", Integer.valueOf(this.A00), Boolean.valueOf(this.A01));
    }

    public int A00() {
        int i = this.A00;
        boolean z = false;
        if (i == -1) {
            z = true;
        }
        if (!z) {
            return i;
        }
        throw new IllegalStateException("Rotation is set to use EXIF");
    }

    public int hashCode() {
        return AnonymousClass07K.A01(Integer.valueOf(this.A00), Boolean.valueOf(this.A01));
    }

    public AnonymousClass1Q1(int i, boolean z) {
        this.A00 = i;
        this.A01 = z;
    }
}
