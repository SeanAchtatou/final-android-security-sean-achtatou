package X;

import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/* renamed from: X.07A  reason: invalid class name */
public final class AnonymousClass07A {
    public static Runnable A00(Runnable runnable, int i) {
        if (runnable == null) {
            throw new NullPointerException("runnable for Executor call is null");
        } else if (!TraceEvents.isEnabled(AnonymousClass00n.A01)) {
            return runnable;
        } else {
            return new AnonymousClass0Hi(runnable, Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 16, 0, 0, i, 0, 0), i);
        }
    }

    public static Callable A01(Callable callable, int i) {
        if (callable == null) {
            throw new NullPointerException("callable for Executor call is null");
        } else if (!TraceEvents.isEnabled(AnonymousClass00n.A01)) {
            return callable;
        } else {
            return new AnonymousClass0NL(callable, Logger.writeStandardEntry(AnonymousClass00n.A01, 6, 16, 0, 0, i, 0, 0), i);
        }
    }

    public static Future A02(ExecutorService executorService, Runnable runnable, int i) {
        return executorService.submit(A00(runnable, i));
    }

    public static Future A03(ExecutorService executorService, Callable callable, int i) {
        return executorService.submit(A01(callable, i));
    }

    public static void A04(Executor executor, Runnable runnable, int i) {
        executor.execute(A00(runnable, i));
    }
}
