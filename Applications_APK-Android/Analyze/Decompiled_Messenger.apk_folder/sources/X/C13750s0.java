package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0s0  reason: invalid class name and case insensitive filesystem */
public final class C13750s0 {
    private static volatile C13750s0 A01;
    public final C25051Yd A00;

    public static final C13750s0 A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C13750s0.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C13750s0(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public boolean A02() {
        return this.A00.Aem(282471409845678L);
    }

    private C13750s0(AnonymousClass1XY r3) {
        new AnonymousClass0UN(1, r3);
        this.A00 = AnonymousClass0WT.A00(r3);
    }

    public static final C13750s0 A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
