package X;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/* renamed from: X.1N2  reason: invalid class name */
public final class AnonymousClass1N2 extends C22731Mp implements AnonymousClass1N3 {
    public List A00 = new LinkedList();
    private long A01;
    private C22871Nd A02;
    private Map A03;
    public final String A04;
    private final AnonymousClass06B A05;

    private void A01() {
        if (this.A05.now() - this.A01 > 300000) {
            for (Map.Entry entry : this.A03.entrySet()) {
                A05((String) entry.getKey(), (long) ((Integer) entry.getValue()).intValue());
            }
            this.A00.add(A02());
            A00();
        }
    }

    public void C6L(C22871Nd r1) {
        this.A02 = r1;
        A00();
    }

    public AnonymousClass1N2(C22751Mr r2, AnonymousClass06B r3, String str) {
        super(r2);
        C05520Zg.A02(r3);
        this.A05 = r3;
        C05520Zg.A02(str);
        this.A04 = str;
    }

    private void A00() {
        HashMap hashMap;
        A03();
        this.A01 = this.A05.now();
        C22871Nd r2 = this.A02;
        synchronized (r2) {
            hashMap = new HashMap();
            for (int i = 0; i < r2.A03.size(); i++) {
                int keyAt = r2.A03.keyAt(i);
                r2.A04(keyAt);
                hashMap.put(AnonymousClass08S.A09("buckets_used_", keyAt), Integer.valueOf(((C22911Nh) r2.A03.valueAt(i)).A00));
            }
            hashMap.put("soft_cap", Integer.valueOf(r2.A07.A03));
            hashMap.put("hard_cap", Integer.valueOf(r2.A07.A02));
            hashMap.put("used_count", Integer.valueOf(r2.A06.A00));
            hashMap.put("used_bytes", Integer.valueOf(r2.A06.A01));
            hashMap.put("free_count", Integer.valueOf(r2.A05.A00));
            hashMap.put("free_bytes", Integer.valueOf(r2.A05.A01));
        }
        this.A03 = hashMap;
    }

    public void BO0(int i) {
        A01();
        A04(AnonymousClass08S.A09("pool_alloc_", i), 1);
    }

    public void BaY() {
        A01();
        A04("hard_cap_exceeded", 1);
    }

    public void Bp0() {
        A01();
        A04("soft_cap_exceeded", 1);
    }

    public void BuD(int i) {
        A01();
        A04(AnonymousClass08S.A09("value_reuse_", i), 1);
    }

    public void BZs(int i) {
        A01();
    }

    public void BuA(int i) {
        A01();
    }
}
