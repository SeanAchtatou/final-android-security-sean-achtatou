package X;

import com.facebook.common.util.TriState;

/* renamed from: X.05g  reason: invalid class name and case insensitive filesystem */
public final class C004305g implements C004205c {
    public Object ATP(Object obj, Object obj2) {
        TriState triState = (TriState) obj;
        TriState triState2 = (TriState) obj2;
        if (!triState.isSet() || !triState2.isSet()) {
            if (!triState.isSet()) {
                return triState2;
            }
            return triState;
        } else if (triState.asBoolean() || triState2.asBoolean()) {
            return TriState.YES;
        } else {
            return TriState.NO;
        }
    }
}
