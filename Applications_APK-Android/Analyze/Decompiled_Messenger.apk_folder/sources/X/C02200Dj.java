package X;

import android.content.Context;
import android.content.IntentFilter;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0Dj  reason: invalid class name and case insensitive filesystem */
public final class C02200Dj {
    public static C02200Dj A0J;
    public static final C008807t A0K = new C008807t();
    private static final C417726x A0L = new C417726x();
    private static final C28151eL A0M = new C28151eL();
    private AnonymousClass15w A00 = null;
    private AnonymousClass15w A01 = null;
    private AnonymousClass15w A02 = null;
    private AnonymousClass15w A03 = null;
    private AnonymousClass15w A04 = null;
    private AnonymousClass15w A05 = null;
    private AnonymousClass15w A06 = null;
    private AnonymousClass15w A07 = null;
    private AnonymousClass15w A08 = null;
    private C14130sf A09 = null;
    private C28851fT A0A = null;
    private C28861fU A0B = null;
    private C15890w9 A0C = null;
    private C15890w9 A0D = null;
    private C14070sZ A0E = null;
    private C13350rI A0F = null;
    private C13350rI A0G = null;
    private C28881fW A0H = null;
    private final List A0I = new ArrayList();

    private synchronized C14130sf A01() {
        if (this.A09 == null) {
            this.A09 = new C14130sf(A0K, A0L, A0M);
        }
        return this.A09;
    }

    private synchronized C28851fT A02() {
        if (this.A0A == null) {
            this.A0A = new C28851fT(A0K, A0L, A0M);
        }
        return this.A0A;
    }

    private synchronized C28861fU A03() {
        if (this.A0B == null) {
            this.A0B = new C28861fU(A0K, A0L, A0M);
        }
        return this.A0B;
    }

    private synchronized C15890w9 A04() {
        if (this.A0C == null) {
            this.A0C = new C15890w9(A0K, A0L, A0M, false);
        }
        return this.A0C;
    }

    private synchronized C15890w9 A05() {
        if (this.A0D == null) {
            this.A0D = new C15890w9(A0K, A0L, A0M, true);
        }
        return this.A0D;
    }

    private synchronized C14070sZ A06() {
        if (this.A0E == null) {
            this.A0E = new C14070sZ(A0K, A0L, A0M);
        }
        return this.A0E;
    }

    private synchronized C13350rI A07() {
        if (this.A0F == null) {
            this.A0F = new C13350rI(A0K, A0L, false);
        }
        return this.A0F;
    }

    private synchronized C13350rI A08() {
        if (this.A0G == null) {
            this.A0G = new C13350rI(A0K, A0L, true);
        }
        return this.A0G;
    }

    private synchronized C28881fW A09() {
        if (this.A0H == null) {
            this.A0H = new C28881fW(A0K, A0L, A0M);
        }
        return this.A0H;
    }

    public synchronized AnonymousClass15w A0B() {
        if (this.A00 == null) {
            this.A00 = new AnonymousClass15w(A01(), this.A0I);
        }
        return this.A00;
    }

    public synchronized AnonymousClass15w A0C() {
        if (this.A01 == null) {
            this.A01 = new AnonymousClass15w(A02(), this.A0I);
        }
        return this.A01;
    }

    public synchronized AnonymousClass15w A0D() {
        if (this.A02 == null) {
            this.A02 = new AnonymousClass15w(A03(), this.A0I);
        }
        return this.A02;
    }

    public synchronized AnonymousClass15w A0E() {
        if (this.A03 == null) {
            this.A03 = new AnonymousClass15w(A04(), this.A0I);
        }
        return this.A03;
    }

    public synchronized AnonymousClass15w A0F() {
        if (this.A04 == null) {
            this.A04 = new AnonymousClass15w(A05(), this.A0I);
        }
        return this.A04;
    }

    public synchronized AnonymousClass15w A0G() {
        if (this.A05 == null) {
            this.A05 = new AnonymousClass15w(A06(), this.A0I);
        }
        return this.A05;
    }

    public synchronized AnonymousClass15w A0H() {
        if (this.A06 == null) {
            this.A06 = new AnonymousClass15w(A07(), this.A0I);
        }
        return this.A06;
    }

    public synchronized AnonymousClass15w A0I() {
        if (this.A07 == null) {
            this.A07 = new AnonymousClass15w(A08(), this.A0I);
        }
        return this.A07;
    }

    public synchronized AnonymousClass15w A0J() {
        if (this.A08 == null) {
            this.A08 = new AnonymousClass15w(A09(), this.A0I);
        }
        return this.A08;
    }

    public static synchronized C02200Dj A00() {
        C02200Dj r0;
        synchronized (C02200Dj.class) {
            synchronized (C02200Dj.class) {
                synchronized (C02200Dj.class) {
                    if (A0J == null) {
                        A0J = new C02200Dj();
                    }
                    if (0 != 0) {
                        C008807t r1 = A0K;
                        synchronized (r1) {
                            r1.A00 = null;
                        }
                    }
                    if (0 != 0) {
                        A0A(null);
                    }
                    r0 = A0J;
                }
            }
        }
        return r0;
    }

    private static void A0A(AnonymousClass04e r2) {
        C417726x r1 = A0L;
        synchronized (r1) {
            r1.A00 = r2;
        }
    }

    private C02200Dj() {
    }

    public void A0K(Context context, AnonymousClass06X r3, IntentFilter intentFilter) {
        r3.A06(A02());
        context.registerReceiver(r3, intentFilter);
    }
}
