package X;

import java.util.Set;

/* renamed from: X.1o7  reason: invalid class name and case insensitive filesystem */
public final class C33771o7 {
    public final AnonymousClass09P A00;
    public final C34261pE A01;
    public final C34311pM A02;
    public final AnonymousClass0B7 A03;
    public final Set A04;
    public volatile AnonymousClass0B6 A05;

    public C33771o7(C34261pE r1, C34311pM r2, AnonymousClass0B7 r3, Set set, AnonymousClass09P r5, AnonymousClass0B6 r6) {
        this.A01 = r1;
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = set;
        this.A00 = r5;
        this.A05 = r6;
    }
}
