package X;

import java.io.Serializable;
import java.util.Map;

/* renamed from: X.0k8  reason: invalid class name and case insensitive filesystem */
public final class C10450k8 extends C10460k9 implements Serializable {
    private static final long serialVersionUID = 8849092838541724233L;
    public final C25138CaX _filterProvider;
    public final int _serFeatures;
    public AnonymousClass0oC _serializationInclusion;

    public C10140jc getAnnotationIntrospector() {
        if (isEnabled(C26771bz.USE_ANNOTATIONS)) {
            return super.getAnnotationIntrospector();
        }
        return CYm.instance;
    }

    public C10120ja introspectClassAnnotations(C10030jR r2) {
        return this._base._classIntrospector.forClassAnnotations(this, r2, this);
    }

    public final boolean isEnabled(C10480kE r3) {
        if ((r3.getMask() & this._serFeatures) != 0) {
            return true;
        }
        return false;
    }

    public String toString() {
        return AnonymousClass08S.A0P("[SerializationConfig: flags=0x", Integer.toHexString(this._serFeatures), "]");
    }

    public C10160je getDefaultVisibilityChecker() {
        C10160je defaultVisibilityChecker = super.getDefaultVisibilityChecker();
        if (!isEnabled(C26771bz.AUTO_DETECT_GETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withGetterVisibility(C10180jg.NONE);
        }
        if (!isEnabled(C26771bz.AUTO_DETECT_IS_GETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withIsGetterVisibility(C10180jg.NONE);
        }
        if (!isEnabled(C26771bz.AUTO_DETECT_FIELDS)) {
            return defaultVisibilityChecker.withFieldVisibility(C10180jg.NONE);
        }
        return defaultVisibilityChecker;
    }

    public C10450k8(C10290jr r3, C10430k6 r4, Map map) {
        super(r3, r4, map);
        this._serializationInclusion = null;
        this._serFeatures = C10470kA.collectFeatureDefaults(C10480kE.class);
        this._filterProvider = null;
    }

    public C10450k8(C10450k8 r2, int i, int i2) {
        super(r2, i);
        this._serializationInclusion = null;
        this._serFeatures = i2;
        this._serializationInclusion = r2._serializationInclusion;
        this._filterProvider = r2._filterProvider;
    }

    public C10450k8(C10450k8 r2, C10290jr r3) {
        super(r2, r3);
        this._serializationInclusion = null;
        this._serFeatures = r2._serFeatures;
        this._serializationInclusion = r2._serializationInclusion;
        this._filterProvider = r2._filterProvider;
    }

    public C10450k8(C10450k8 r2, AnonymousClass0oC r3) {
        super(r2);
        this._serializationInclusion = null;
        this._serFeatures = r2._serFeatures;
        this._serializationInclusion = r3;
        this._filterProvider = r2._filterProvider;
    }
}
