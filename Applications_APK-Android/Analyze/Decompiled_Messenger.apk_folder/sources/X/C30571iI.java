package X;

import android.util.SparseIntArray;

/* renamed from: X.1iI  reason: invalid class name and case insensitive filesystem */
public final class C30571iI {
    private static final SparseIntArray A00 = new SparseIntArray(0);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public static AnonymousClass1N7 A00() {
        int i;
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min > 16777216) {
            i = (min >> 2) * 3;
        } else {
            i = min >> 1;
        }
        return new AnonymousClass1N7(0, i, A00, -1);
    }
}
