package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.facebook.litho.ComponentTree;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1NM  reason: invalid class name */
public final class AnonymousClass1NM extends C17770zR {
    public static final Drawable A0L = AnonymousClass1M2.A01(0.0f, -1, 520093696);
    public static final AnonymousClass0ZG A0M = new AnonymousClass0ZG(2);
    @Comparable(type = 13)
    public Drawable A00 = A0L;
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A01;
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A02;
    @Comparable(type = AnonymousClass1Y3.A01)
    public C17770zR A03;
    public ComponentTree A04;
    public ComponentTree A05;
    public ComponentTree A06;
    public ComponentTree A07;
    @Comparable(type = 13)
    public AnonymousClass1BY A08;
    @Comparable(type = 13)
    public AnonymousClass1MF A09;
    @Comparable(type = 13)
    public AnonymousClass1MD A0A;
    public Integer A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    public Integer A0I;
    public Integer A0J;
    @Comparable(type = 13)
    public String A0K;

    public static void A02(AnonymousClass0p4 r2, C17770zR r3, AnonymousClass10L r4) {
        if (r3 == null) {
            r4.A01 = 0;
            r4.A00 = 0;
            return;
        }
        r3.A1F(r2, View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0), r4);
    }

    public int A0M() {
        return 3;
    }

    public AnonymousClass1NM() {
        super("SwipeableListItemComponent");
    }

    public static ComponentTree A00(AnonymousClass0p4 r3, E76 e76, Drawable drawable) {
        Class<AnonymousClass1NM> cls = AnonymousClass1NM.class;
        e76.A2y(C17780zS.A0E(cls, r3, -1351902487, new Object[]{r3}));
        e76.A2R(C17780zS.A0E(cls, r3, 71235917, new Object[]{r3}));
        e76.A2H(drawable);
        AnonymousClass11J A022 = ComponentTree.A02(r3, e76.A31());
        A022.A0C = false;
        return A022.A00();
    }

    public static void A01(AnonymousClass0p4 r5, int i, C17770zR r7, Drawable drawable, C17770zR r9, C17770zR r10, AnonymousClass10L r11, AnonymousClass1JP r12, AnonymousClass1JP r13, AnonymousClass1JP r14, AnonymousClass1JP r15, AnonymousClass1JP r16, AnonymousClass1JP r17) {
        AnonymousClass10L r3 = (AnonymousClass10L) A0M.ALa();
        if (r3 == null) {
            r3 = new AnonymousClass10L();
        }
        A02(r5, r9, r3);
        r16.A00(Integer.valueOf(r3.A01));
        A02(r5, r10, r3);
        r17.A00(Integer.valueOf(r3.A01));
        C37941wd A002 = C16980y8.A00(r5);
        A002.A3A(r7);
        A002.A2F(View.MeasureSpec.getSize(i));
        ComponentTree A003 = A00(r5, A002, drawable);
        A003.A0O(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0), r3);
        r12.A00(A003);
        C37941wd A004 = C16980y8.A00(r5);
        A004.A3A(r9);
        C37941wd A005 = C16980y8.A00(r5);
        A005.A3A(r7);
        A005.A2F(View.MeasureSpec.getSize(i));
        A004.A39(A005);
        A004.A3A(r10);
        r13.A00(A00(r5, A004, drawable));
        int i2 = r3.A01;
        r14.A00(Integer.valueOf(i2));
        int i3 = r3.A00;
        r15.A00(Integer.valueOf(i3));
        if (r11 != null) {
            r11.A01 = i2;
            r11.A00 = i3;
        }
        A0M.C0w(r3);
    }

    public C17770zR A16() {
        C17770zR r0;
        C17770zR r02;
        C17770zR r03;
        AnonymousClass1NM r2 = (AnonymousClass1NM) super.A16();
        C17770zR r04 = r2.A01;
        if (r04 != null) {
            r0 = r04.A16();
        } else {
            r0 = null;
        }
        r2.A01 = r0;
        C17770zR r05 = r2.A02;
        if (r05 != null) {
            r02 = r05.A16();
        } else {
            r02 = null;
        }
        r2.A02 = r02;
        C17770zR r06 = r2.A03;
        if (r06 != null) {
            r03 = r06.A16();
        } else {
            r03 = null;
        }
        r2.A03 = r03;
        r2.A0B = null;
        r2.A04 = null;
        r2.A05 = null;
        r2.A0C = null;
        r2.A0D = null;
        r2.A0E = null;
        r2.A06 = null;
        r2.A07 = null;
        r2.A0F = null;
        r2.A0G = null;
        r2.A0H = null;
        r2.A0I = null;
        r2.A0J = null;
        return r2;
    }
}
