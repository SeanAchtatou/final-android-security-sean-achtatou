package X;

import java.util.List;

/* renamed from: X.1Bx  reason: invalid class name and case insensitive filesystem */
public final class C20311Bx {
    public final C20301Bw A00 = new C20301Bw();

    public void A00(AnonymousClass1CY r3) {
        if (r3 != null) {
            this.A00.A00.add(r3.A03());
        }
    }

    public void A01(C16070wR r3) {
        if (r3 != null) {
            this.A00.A00.add(r3.A0T(false));
        }
    }

    public void A02(List list) {
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                C16070wR r2 = (C16070wR) list.get(i);
                if (r2 != null) {
                    this.A00.A00.add(r2.A0T(false));
                }
            }
        }
    }
}
