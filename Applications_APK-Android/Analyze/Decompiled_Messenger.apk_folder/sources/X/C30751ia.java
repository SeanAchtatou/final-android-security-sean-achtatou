package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

/* renamed from: X.1ia  reason: invalid class name and case insensitive filesystem */
public abstract class C30751ia {
    public long A00 = 0;
    public ScheduledFuture A01;
    public final AnonymousClass09P A02;
    public final C012109i A03;
    public final QuickPerformanceLogger A04;
    public final Runnable A05;
    public final Map A06;
    public final ScheduledExecutorService A07;

    public void A01(C30741iZ r6) {
        try {
            this.A04.markerStart(43253762);
            synchronized (this.A06) {
                this.A06.put(r6, 1);
            }
            this.A04.markerEnd(43253762, 2);
        } catch (Throwable th) {
            this.A04.markerEnd(43253762, 2);
            throw th;
        }
    }

    public C30751ia(ScheduledExecutorService scheduledExecutorService, C012109i r4, AnonymousClass09P r5, QuickPerformanceLogger quickPerformanceLogger) {
        this.A07 = scheduledExecutorService;
        this.A03 = r4;
        this.A02 = r5;
        this.A04 = quickPerformanceLogger;
        this.A06 = new HashMap();
        this.A05 = new C30761ib(this);
    }
}
