package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.io.Closeable;
import java.lang.reflect.Method;

/* renamed from: X.0q6  reason: invalid class name and case insensitive filesystem */
public final class C12850q6 implements C12860q7 {
    public static final C12850q6 A00 = new C12850q6();
    public static final Method A01;

    static {
        Method method;
        Class<Throwable> cls = Throwable.class;
        try {
            method = cls.getMethod(TurboLoader.Locator.$const$string(132), cls);
        } catch (Throwable unused) {
            method = null;
        }
        A01 = method;
    }

    public void CIf(Closeable closeable, Throwable th, Throwable th2) {
        if (th != th2) {
            try {
                A01.invoke(th, th2);
            } catch (Throwable unused) {
                AnonymousClass9HP.A00.CIf(closeable, th, th2);
            }
        }
    }
}
