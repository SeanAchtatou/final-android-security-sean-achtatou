package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

/* renamed from: X.1sJ  reason: invalid class name and case insensitive filesystem */
public final class C36091sJ implements ServiceConnection {
    public final /* synthetic */ C24421To A00;

    public C36091sJ(C24421To r1) {
        this.A00 = r1;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        C24421To.A01(this.A00, componentName, iBinder);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        C24421To.A01(this.A00, componentName, null);
    }
}
