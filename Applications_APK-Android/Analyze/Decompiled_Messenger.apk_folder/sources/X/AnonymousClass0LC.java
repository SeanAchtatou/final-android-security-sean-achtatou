package X;

import com.google.common.base.Preconditions;
import java.util.Comparator;
import java.util.Iterator;

/* renamed from: X.0LC  reason: invalid class name */
public final class AnonymousClass0LC extends AnonymousClass0UI {
    private Object A00;
    private Object A01;
    private final AnonymousClass0LB A02;
    private final AnonymousClass0LB A03;
    private final AnonymousClass191 A04;
    private final AnonymousClass191 A05;
    private final Comparator A06;

    private final void A00(Object obj) {
        Object obj2 = this.A00;
        if (obj2 != null) {
            boolean z = false;
            if (this.A06.compare(obj, obj2) > 0) {
                z = true;
            }
            Preconditions.checkState(z, "Left iterator keys must be strictly ascending. (%s %s)", this.A00, obj);
        }
    }

    private final void A01(Object obj) {
        Object obj2 = this.A01;
        if (obj2 != null) {
            boolean z = false;
            if (this.A06.compare(obj, obj2) > 0) {
                z = true;
            }
            Preconditions.checkState(z, "Right iterator keys must be strictly ascending. (%s %s)", this.A00, obj);
        }
    }

    public Object A02() {
        Object obj;
        Object obj2;
        if (this.A04.hasNext() || this.A05.hasNext()) {
            Object obj3 = null;
            if (this.A04.hasNext()) {
                obj = this.A02.AZM(this.A04.peek());
                A00(obj);
            } else {
                obj = null;
            }
            if (this.A05.hasNext()) {
                obj2 = this.A03.AZM(this.A05.peek());
                A01(obj2);
            } else {
                obj2 = null;
            }
            if (this.A04.hasNext() || !this.A05.hasNext()) {
                if (!this.A04.hasNext() || this.A05.hasNext()) {
                    int compare = this.A06.compare(obj, obj2);
                    if (compare > 0) {
                        this.A01 = obj2;
                    } else if (compare < 0) {
                        this.A00 = obj;
                    } else {
                        this.A01 = obj2;
                        this.A00 = obj;
                        obj3 = this.A04.next();
                    }
                }
                return new AnonymousClass0LA(this.A04.next(), null);
            }
            return new AnonymousClass0LA(obj3, this.A05.next());
        }
        this.A00 = AnonymousClass07B.A0C;
        return null;
    }

    public AnonymousClass0LC(Comparator comparator, Iterator it, Iterator it2, AnonymousClass0LB r5, AnonymousClass0LB r6) {
        this.A06 = comparator;
        this.A04 = C24931Xr.A03(it);
        this.A05 = C24931Xr.A03(it2);
        this.A02 = r5;
        this.A03 = r6;
    }
}
