package X;

import android.util.Printer;

/* renamed from: X.0iS  reason: invalid class name */
public final class AnonymousClass0iS implements Printer {
    public final /* synthetic */ C09730iO A00;

    public AnonymousClass0iS(C09730iO r1) {
        this.A00 = r1;
    }

    public void println(String str) {
        synchronized (this.A00.A02) {
            for (int i = 0; i < this.A00.A02.size(); i++) {
                ((AnonymousClass0jI) this.A00.A02.get(i)).Bew(str);
            }
        }
    }
}
