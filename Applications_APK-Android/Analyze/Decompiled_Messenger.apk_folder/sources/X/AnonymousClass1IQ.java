package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1IQ  reason: invalid class name */
public final class AnonymousClass1IQ {
    public final int A00;
    public final int A01;
    public final AnonymousClass1ID A02;
    public final List A03;
    public final boolean A04;
    public final int[] A05;
    public final int[] A06;

    private void A01(int i, int i2, int i3, boolean z) {
        int i4 = i - 1;
        int i5 = i4;
        if (z) {
            i2--;
            i4 = i;
            i5 = i2;
        }
        while (i3 >= 0) {
            AnonymousClass1IR r6 = (AnonymousClass1IR) this.A03.get(i3);
            int i6 = r6.A01;
            int i7 = r6.A00;
            int i8 = i6 + i7;
            int i9 = r6.A02 + i7;
            int i10 = 8;
            if (z) {
                for (int i11 = i4 - 1; i11 >= i8; i11--) {
                    if (this.A02.A03(i11, i5)) {
                        if (!this.A02.A02(i11, i5)) {
                            i10 = 4;
                        }
                        this.A05[i5] = (i11 << 5) | 16;
                        this.A06[i11] = (i5 << 5) | i10;
                        return;
                    }
                }
                continue;
            } else {
                for (int i12 = i2 - 1; i12 >= i9; i12--) {
                    if (this.A02.A03(i5, i12)) {
                        if (!this.A02.A02(i5, i12)) {
                            i10 = 4;
                        }
                        int i13 = i - 1;
                        this.A06[i13] = (i12 << 5) | 16;
                        this.A05[i12] = (i13 << 5) | i10;
                        return;
                    }
                }
                continue;
            }
            i4 = r6.A01;
            i2 = r6.A02;
            i3--;
        }
    }

    public void A02(AnonymousClass1IT r21) {
        AnonymousClass1IU r12;
        int i;
        int i2;
        AnonymousClass1IT r122 = r21;
        if (r122 instanceof AnonymousClass1IU) {
            r12 = (AnonymousClass1IU) r122;
        } else {
            r12 = new AnonymousClass1IU(r122);
        }
        ArrayList<C30350Eua> arrayList = new ArrayList<>();
        int i3 = this.A01;
        int i4 = this.A00;
        loop0:
        for (int size = this.A03.size() - 1; size >= 0; size--) {
            AnonymousClass1IR r7 = (AnonymousClass1IR) this.A03.get(size);
            int i5 = r7.A00;
            int i6 = r7.A01 + i5;
            int i7 = r7.A02 + i5;
            if (i6 < i3) {
                int i8 = i3 - i6;
                if (!this.A04) {
                    r12.Bld(i6, i8);
                } else {
                    int i9 = i8 - 1;
                    while (true) {
                        if (i9 < 0) {
                            break;
                        }
                        i = i6 + i9;
                        int i10 = this.A06[i];
                        i2 = i10 & 31;
                        if (i2 != 0) {
                            if (i2 != 4 && i2 != 8) {
                                if (i2 != 16) {
                                    break loop0;
                                }
                                arrayList.add(new C30350Eua(i, i6 + i9, true));
                            } else {
                                C30350Eua A002 = A00(arrayList, i10 >> 5, false);
                                r12.Bfk(i6 + i9, A002.A02 - 1);
                                if (i2 == 4) {
                                    r12.BRm(A002.A02 - 1, 1, null);
                                }
                            }
                        } else {
                            r12.Bld(i6 + i9, 1);
                            for (C30350Eua eua : arrayList) {
                                eua.A02--;
                            }
                        }
                        i9--;
                    }
                }
            }
            if (i7 < i4) {
                int i11 = i4 - i7;
                if (!this.A04) {
                    r12.BbO(i6, i11);
                } else {
                    for (int i12 = i11 - 1; i12 >= 0; i12--) {
                        i = i7 + i12;
                        int i13 = this.A05[i];
                        i2 = i13 & 31;
                        if (i2 == 0) {
                            r12.BbO(i6, 1);
                            for (C30350Eua eua2 : arrayList) {
                                eua2.A02++;
                            }
                        } else if (i2 == 4 || i2 == 8) {
                            r12.Bfk(A00(arrayList, i13 >> 5, true).A02, i6);
                            if (i2 == 4) {
                                r12.BRm(i6, 1, null);
                            }
                        } else if (i2 == 16) {
                            arrayList.add(new C30350Eua(i, i6, false));
                        } else {
                            throw new IllegalStateException(AnonymousClass08S.A0F("unknown flag for pos ", i, " ", Long.toBinaryString((long) i2)));
                        }
                    }
                }
            }
            while (true) {
                i5--;
                if (i5 < 0) {
                    break;
                }
                int[] iArr = this.A06;
                int i14 = r7.A01 + i5;
                if ((iArr[i14] & 31) == 2) {
                    r12.BRm(i14, 1, null);
                }
            }
            i3 = r7.A01;
            i4 = r7.A02;
        }
        r12.A00();
    }

    public AnonymousClass1IQ(AnonymousClass1ID r10, List list, int[] iArr, int[] iArr2, boolean z) {
        AnonymousClass1IR r0;
        this.A03 = list;
        this.A06 = iArr;
        this.A05 = iArr2;
        Arrays.fill(iArr, 0);
        Arrays.fill(this.A05, 0);
        this.A02 = r10;
        this.A01 = r10.A01();
        this.A00 = r10.A00();
        this.A04 = z;
        if (this.A03.isEmpty()) {
            r0 = null;
        } else {
            r0 = (AnonymousClass1IR) this.A03.get(0);
        }
        if (!(r0 != null && r0.A01 == 0 && r0.A02 == 0)) {
            AnonymousClass1IR r1 = new AnonymousClass1IR();
            r1.A01 = 0;
            r1.A02 = 0;
            r1.A03 = false;
            r1.A00 = 0;
            r1.A04 = false;
            this.A03.add(0, r1);
        }
        int i = this.A01;
        int i2 = this.A00;
        for (int size = this.A03.size() - 1; size >= 0; size--) {
            AnonymousClass1IR r6 = (AnonymousClass1IR) this.A03.get(size);
            int i3 = r6.A01;
            int i4 = r6.A00;
            int i5 = i3 + i4;
            int i6 = r6.A02 + i4;
            if (this.A04) {
                while (i > i5) {
                    int i7 = i - 1;
                    if (this.A06[i7] == 0) {
                        A01(i, i2, size, false);
                    }
                    i = i7;
                }
                while (i2 > i6) {
                    int i8 = i2 - 1;
                    if (this.A05[i8] == 0) {
                        A01(i, i2, size, true);
                    }
                    i2 = i8;
                }
            }
            for (int i9 = 0; i9 < r6.A00; i9++) {
                int i10 = r6.A01 + i9;
                int i11 = r6.A02 + i9;
                int i12 = 2;
                if (this.A02.A02(i10, i11)) {
                    i12 = 1;
                }
                this.A06[i10] = (i11 << 5) | i12;
                this.A05[i11] = (i10 << 5) | i12;
            }
            i = r6.A01;
            i2 = r6.A02;
        }
    }

    private static C30350Eua A00(List list, int i, boolean z) {
        int size = list.size() - 1;
        while (size >= 0) {
            C30350Eua eua = (C30350Eua) list.get(size);
            if (eua.A00 == i && eua.A01 == z) {
                list.remove(size);
                while (size < list.size()) {
                    C30350Eua eua2 = (C30350Eua) list.get(size);
                    int i2 = eua2.A02;
                    int i3 = -1;
                    if (z) {
                        i3 = 1;
                    }
                    eua2.A02 = i2 + i3;
                    size++;
                }
                return eua;
            }
            size--;
        }
        return null;
    }
}
