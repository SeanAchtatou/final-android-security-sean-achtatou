package X;

import android.app.ActivityManager;

/* renamed from: X.1if  reason: invalid class name and case insensitive filesystem */
public final class C30801if {
    public static final double[] A03 = {0.2d, 0.7d, 0.8d};
    public static final double[] A04 = {0.4d, 0.5d, 0.6d};
    public final ActivityManager A00;
    public final C25051Yd A01;
    public final Integer A02;

    public C30801if(C25051Yd r10, ActivityManager activityManager) {
        Integer num;
        this.A01 = r10;
        this.A00 = activityManager;
        long At3 = r10.At3(567055942223680L, (long) AnonymousClass07B.A00.intValue(), AnonymousClass0XE.A06);
        Integer[] A002 = AnonymousClass07B.A00(5);
        int length = A002.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                num = AnonymousClass07B.A00;
                break;
            }
            num = A002[i];
            if (((long) num.intValue()) == At3) {
                break;
            }
            i++;
        }
        this.A02 = num;
    }
}
