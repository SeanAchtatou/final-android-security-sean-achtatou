package X;

import com.google.common.collect.ImmutableCollection;
import java.util.Iterator;

/* renamed from: X.1Xx  reason: invalid class name and case insensitive filesystem */
public abstract class C24991Xx {
    public abstract C24991Xx add(Object obj);

    public abstract ImmutableCollection build();

    public static int A01(int i, int i2) {
        if (i2 >= 0) {
            int i3 = i + (i >> 1) + 1;
            if (i3 < i2) {
                i3 = Integer.highestOneBit(i2 - 1) << 1;
            }
            if (i3 < 0) {
                return Integer.MAX_VALUE;
            }
            return i3;
        }
        throw new AssertionError("cannot store more than MAX_VALUE elements");
    }

    public C24991Xx add(Object... objArr) {
        for (Object add : objArr) {
            add(add);
        }
        return this;
    }

    public C24991Xx addAll(Iterable iterable) {
        for (Object add : iterable) {
            add(add);
        }
        return this;
    }

    public C24991Xx addAll(Iterator it) {
        while (it.hasNext()) {
            add(it.next());
        }
        return this;
    }
}
