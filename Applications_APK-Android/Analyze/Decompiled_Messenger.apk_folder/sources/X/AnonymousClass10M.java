package X;

import android.content.Context;
import android.graphics.Typeface;

/* renamed from: X.10M  reason: invalid class name */
public enum AnonymousClass10M {
    ROBOTO_REGULAR,
    ROBOTO_MEDIUM,
    ROBOTO_BOLD;

    public Typeface A00(Context context) {
        Integer num;
        C32031l0 r1;
        if (!(this instanceof C32011ky)) {
            if (!(this instanceof AnonymousClass1I6)) {
                num = AnonymousClass07B.A00;
                r1 = C32031l0.REGULAR;
            } else {
                num = AnonymousClass07B.A00;
                r1 = C32031l0.MEDIUM;
            }
            Typeface A01 = C21871Ja.A01(context, num, r1, null);
            return A01 == null ? Typeface.DEFAULT : A01;
        }
        Typeface A012 = C21871Ja.A01(context, AnonymousClass07B.A00, C32031l0.BOLD, null);
        return A012 == null ? Typeface.DEFAULT_BOLD : A012;
    }
}
