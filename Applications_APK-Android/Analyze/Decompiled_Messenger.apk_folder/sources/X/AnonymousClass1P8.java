package X;

import java.util.concurrent.Callable;

/* renamed from: X.1P8  reason: invalid class name */
public final class AnonymousClass1P8 implements C23271Ow {
    private final double A00;
    private final double A01;
    private final double A02;
    private final double A03;
    private final Callable A04;
    private final boolean A05;

    public long Ago() {
        return C23271Ow.A00;
    }

    public double AvD() {
        if (this.A05) {
            return 1.0d;
        }
        try {
            double doubleValue = ((Double) this.A04.call()).doubleValue();
            if (this.A02 < doubleValue && doubleValue <= this.A00) {
                return this.A03;
            }
            if (this.A00 >= doubleValue || doubleValue >= 1.0d) {
                return 1.0d;
            }
            return this.A01;
        } catch (Exception unused) {
            return 1.0d;
        }
    }

    public AnonymousClass1P8(C25051Yd r10, Callable callable) {
        this.A04 = callable;
        AnonymousClass0XE r7 = AnonymousClass0XE.A06;
        C25051Yd r2 = r10;
        this.A02 = r2.Akl(1126406713114673L, 0.7d, r7);
        this.A00 = r2.Akl(1126406712983599L, 0.8d, r7);
        this.A03 = r2.Akl(1126406713180210L, 0.75d, r7);
        double Akl = r2.Akl(1126406713049136L, 0.5d, r7);
        this.A01 = Akl;
        double d = this.A02;
        if (d > 0.0d && d <= 1.0d) {
            double d2 = this.A00;
            if (d2 > 0.0d && d2 <= 1.0d) {
                double d3 = this.A03;
                if (d3 > 0.0d && d3 <= 2.0d && Akl > 0.0d && Akl <= 2.0d) {
                    return;
                }
            }
        }
        this.A05 = true;
    }
}
