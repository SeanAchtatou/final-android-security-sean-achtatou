package X;

import android.os.Looper;

/* renamed from: X.1gy  reason: invalid class name and case insensitive filesystem */
public final class C29781gy implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.appperf.ttrc.TTRCActivityListener$1";
    public final /* synthetic */ C29771gx A00;

    public C29781gy(C29771gx r1) {
        this.A00 = r1;
    }

    public void run() {
        C164137id r2 = (C164137id) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Akf, this.A00.A00);
        r2.A00 = ((AnonymousClass069) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBa, r2.A04)).now();
        C164137id.A01(r2);
        Looper.myQueue().removeIdleHandler(r2.A05);
        Looper.myQueue().addIdleHandler(r2.A05);
    }
}
