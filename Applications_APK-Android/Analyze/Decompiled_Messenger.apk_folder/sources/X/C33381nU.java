package X;

import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.UserKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/* renamed from: X.1nU  reason: invalid class name and case insensitive filesystem */
public final class C33381nU {
    public AnonymousClass0UN A00;

    public static final C33381nU A00(AnonymousClass1XY r1) {
        return new C33381nU(r1);
    }

    public ImmutableList A02(ThreadSummary threadSummary, ImmutableList immutableList) {
        Message A0E;
        if (immutableList == null) {
            A0E = null;
        } else {
            A0E = ((AnonymousClass1TU) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8n, this.A00)).A0E(immutableList, false);
        }
        return A01(threadSummary, A0E);
    }

    public C33381nU(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public ImmutableList A01(ThreadSummary threadSummary, Message message) {
        Preconditions.checkNotNull(threadSummary);
        if (message == null || !threadSummary.A0S.equals(((C12130oa) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B8a, this.A00)).A01())) {
            return RegularImmutableList.A02;
        }
        ImmutableList A0I = ((AnonymousClass1TU) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8n, this.A00)).A0I(message, threadSummary);
        HashMap hashMap = new HashMap(A0I.size());
        ArrayList arrayList = new ArrayList(A0I.size());
        C24971Xv it = A0I.iterator();
        while (it.hasNext()) {
            ThreadParticipant threadParticipant = (ThreadParticipant) it.next();
            UserKey A002 = threadParticipant.A00();
            hashMap.put(A002, Long.valueOf(threadParticipant.A02));
            arrayList.add(A002);
        }
        Collections.sort(arrayList, new AnonymousClass36Q(hashMap));
        return ImmutableList.copyOf((Collection) arrayList);
    }
}
