package X;

import android.util.Log;
import android.util.SparseArray;
import com.facebook.acra.ACRA;
import java.util.Arrays;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1Jw  reason: invalid class name and case insensitive filesystem */
public final class C22091Jw implements C21921Jf {
    public AnonymousClass242 A00;
    public final /* synthetic */ long A01;
    public final /* synthetic */ C16910xz A02;

    public void CMO(long j, long j2, int i, String str, AnonymousClass0lr r18, SparseArray sparseArray) {
        if (i <= 7 && j <= this.A01) {
            C12240os A0F = this.A02.A0F();
            A0F.A0J("timeSinceStart", Long.valueOf(j));
            A0F.A0K("name", str);
            AnonymousClass0lr r5 = r18;
            if (r18 != null) {
                C12240os A0F2 = A0F.A0F("data");
                if (this.A00 == null) {
                    this.A00 = new AnonymousClass242();
                }
                AnonymousClass242 r4 = this.A00;
                r4.A00 = A0F2;
                int i2 = 0;
                int i3 = 0;
                while (i2 < r5.A00) {
                    String[] strArr = r5.A02;
                    String str2 = strArr[i3];
                    String str3 = strArr[i3 + 1];
                    int i4 = r5.A01[i2];
                    if (str3 != null) {
                        switch (i4) {
                            case 1:
                                AnonymousClass242.A00(r4, i4).A0K(str2, str3);
                                continue;
                            case 2:
                                AnonymousClass242.A00(r4, i4).A0J(str2, Integer.valueOf(Integer.parseInt(str3)));
                                continue;
                            case 3:
                            case 4:
                            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                            case 8:
                                C16910xz A0E = AnonymousClass242.A00(r4, i4).A0E(str2);
                                for (String str4 : str3.split(",,,")) {
                                    if (i4 == 3) {
                                        C16910xz.A00(A0E, str4);
                                    } else if (i4 == 4) {
                                        C16910xz.A00(A0E, Integer.valueOf(Integer.parseInt(str4)));
                                    } else if (i4 == 6) {
                                        C16910xz.A00(A0E, Double.valueOf(Double.parseDouble(str4)));
                                    } else if (i4 == 8) {
                                        C16910xz.A00(A0E, Boolean.valueOf(Boolean.parseBoolean(str4)));
                                    } else {
                                        throw new IllegalArgumentException(AnonymousClass08S.A0A("Unsupported type: ", i4, ". We support only array types"));
                                    }
                                }
                                continue;
                            case 5:
                                AnonymousClass242.A00(r4, i4).A0J(str2, Double.valueOf(Double.parseDouble(str3)));
                                continue;
                            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                                C12240os.A01(AnonymousClass242.A00(r4, i4), str2, Boolean.valueOf(Boolean.parseBoolean(str3)));
                                continue;
                            default:
                                try {
                                    throw new IllegalArgumentException(AnonymousClass08S.A09("Unsupported type: ", i4));
                                } catch (NumberFormatException e) {
                                    Log.w("QPL", "Failed to parse int annotation", e);
                                    break;
                                }
                        }
                    }
                    i2++;
                    i3 += 2;
                }
                AnonymousClass242 r0 = this.A00;
                r0.A00 = null;
                Arrays.fill(r0.A01, (Object) null);
            }
        }
    }

    public C22091Jw(long j, C16910xz r3) {
        this.A01 = j;
        this.A02 = r3;
    }
}
