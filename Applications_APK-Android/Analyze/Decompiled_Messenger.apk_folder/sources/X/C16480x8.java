package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.orca.threadlist.ThreadListFragment;

/* renamed from: X.0x8  reason: invalid class name and case insensitive filesystem */
public final class C16480x8 extends AnonymousClass1A9 {
    public final /* synthetic */ ThreadListFragment A00;

    public C16480x8(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public void A08(RecyclerView recyclerView, int i, int i2) {
        this.A00.A0q.A01();
        ThreadListFragment.A0E(this.A00);
    }
}
