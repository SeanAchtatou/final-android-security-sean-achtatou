package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.1Mr  reason: invalid class name and case insensitive filesystem */
public final class C22751Mr {
    public final AnonymousClass06B A00;
    public final AnonymousClass0US A01;
    public final FbSharedPreferences A02;
    public final String A03;
    public final C04310Tq A04;

    public C22751Mr(FbSharedPreferences fbSharedPreferences, String str, AnonymousClass0US r3, AnonymousClass06B r4, C04310Tq r5) {
        this.A02 = fbSharedPreferences;
        this.A01 = r3;
        this.A03 = str;
        this.A00 = r4;
        this.A04 = r5;
    }
}
