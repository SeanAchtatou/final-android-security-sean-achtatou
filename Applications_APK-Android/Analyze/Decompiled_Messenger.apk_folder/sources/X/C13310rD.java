package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.0rD  reason: invalid class name and case insensitive filesystem */
public final class C13310rD implements AnonymousClass06U {
    public final /* synthetic */ C16660xW A00;

    public C13310rD(C16660xW r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(1225333604);
        this.A00.A06("airplane_mode_changed_broadcast");
        AnonymousClass09Y.A01(-1186501013, A002);
    }
}
