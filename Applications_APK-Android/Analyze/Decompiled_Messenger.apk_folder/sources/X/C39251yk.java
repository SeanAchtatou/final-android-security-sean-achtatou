package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.acra.ACRA;
import com.facebook.payments.paymentmethods.view.SimplePaymentMethodView;
import com.google.common.base.Preconditions;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1yk  reason: invalid class name and case insensitive filesystem */
public final class C39251yk implements C23733Blx {
    private final C23787Bmw A00;

    public static final C39251yk A00(AnonymousClass1XY r1) {
        return new C39251yk(r1);
    }

    public C39251yk(AnonymousClass1XY r2) {
        this.A00 = C23787Bmw.A00(r2);
    }

    public View B92(C24440Bzl bzl, Blw blw, View view, ViewGroup viewGroup) {
        C23830Bnl bnl;
        C23829Bnk bnk;
        C23832Bnn bnn;
        C23784Bmt bmt;
        C23870Bod bod;
        switch (blw.B1f().ordinal()) {
            case 0:
                C23761BmU bmU = (C23761BmU) blw;
                if (view == null) {
                    bod = new C23870Bod(viewGroup.getContext());
                } else {
                    bod = (C23870Bod) view;
                }
                bod.A0T(bzl);
                bod.A00.setText(bmU.A00.A00.getDisplayCountry());
                C23849Bo6 bo6 = new C23849Bo6(bmU, bzl);
                bod.A01.A04 = new C23847Bo4(bod, bo6);
                bod.setOnClickListener(new C23871Boe(bod));
                return bod;
            case 1:
            case 3:
            case 4:
            case 5:
            default:
                return this.A00.B92(bzl, blw, view, viewGroup);
            case 2:
                C23785Bmu bmu = (C23785Bmu) blw;
                if (view == null) {
                    bmt = new C23784Bmt(viewGroup.getContext());
                } else {
                    bmt = (C23784Bmt) view;
                }
                bmt.A0T(bzl);
                bmt.A03 = bmu;
                bmt.A04.A01(bmu.A04);
                bmt.A01.setVisibility(8);
                bmt.A00.setVisibility(8);
                C23785Bmu bmu2 = bmt.A03;
                boolean z = true;
                if (bmu2.A00 != null) {
                    Preconditions.checkArgument(!bmu2.A05);
                    bmt.A00.setText(bmt.A03.A00);
                    bmt.A00.setVisibility(0);
                    SimplePaymentMethodView simplePaymentMethodView = bmt.A04;
                    simplePaymentMethodView.A00.setTextColor(AnonymousClass01R.A00(simplePaymentMethodView.getContext(), 2132083085));
                    bmt.A01.setImageResource(2132213988);
                    bmt.A01.A02(AnonymousClass01R.A00(bmt.getContext(), 2132082968));
                    bmt.A01.setVisibility(0);
                }
                C23785Bmu bmu3 = bmt.A03;
                if (bmu3.A05) {
                    if (bmu3.A00 != null) {
                        z = false;
                    }
                    Preconditions.checkArgument(z);
                    bmt.A01.setImageResource(2132213984);
                    bmt.A01.A02(C24762CHc.A00(bmt.getContext()));
                    bmt.A01.setVisibility(0);
                }
                return bmt;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                C23831Bnm bnm = (C23831Bnm) blw;
                if (view == null) {
                    bnn = new C23832Bnn(viewGroup.getContext());
                } else {
                    bnn = (C23832Bnn) view;
                }
                bnn.A0T(bzl);
                bnn.A03 = bnm;
                if (C06850cB.A0B(bnm.A03)) {
                    bnn.A01.setText(2131830278);
                } else {
                    bnn.A01.setTransformationMethod(bnn.A02);
                    bnn.A01.setText(bnm.A03);
                }
                C12300p2 A002 = C187088mS.A00(bnm.A02, bnn.getContext(), AnonymousClass07B.A0C);
                ViewGroup.LayoutParams layoutParams = bnn.A00.getLayoutParams();
                layoutParams.width = ((Integer) A002.A01).intValue();
                bnn.A00.setLayoutParams(layoutParams);
                bnn.A00.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) A002.A00, (Drawable) null);
                return bnn;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                C23828Bnj bnj = (C23828Bnj) blw;
                if (view == null) {
                    bnk = new C23829Bnk(viewGroup.getContext());
                } else {
                    bnk = (C23829Bnk) view;
                }
                bnk.A00 = bzl;
                bnk.A03 = bnj;
                if (!C06850cB.A0B(bnj.A04)) {
                    bnk.A00.setTransformationMethod(bnk.A01);
                    bnk.A00.setText(bnk.A03.A04);
                }
                C187088mS.A01(bnk.A00);
                return bnk;
            case 8:
                if (view == null) {
                    bnl = new C23830Bnl(viewGroup.getContext());
                } else {
                    bnl = (C23830Bnl) view;
                }
                bnl.A00 = bzl;
                bnl.A00.setTransformationMethod(bnl.A01);
                if (!C06850cB.A0B(null)) {
                    bnl.A00.setText((CharSequence) null);
                    return bnl;
                }
                bnl.A00.setText(2131830279);
                return bnl;
        }
    }
}
