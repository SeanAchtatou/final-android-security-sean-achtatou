package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.util.TypedValue;
import com.facebook.ui.emoji.model.BasicEmoji;
import com.facebook.ui.emoji.model.DrawableBackedEmoji;
import com.facebook.ui.emoji.model.Emoji;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1L4  reason: invalid class name */
public final class AnonymousClass1L4 implements C22351Kz {
    private static volatile AnonymousClass1L4 A01;
    public AnonymousClass0UN A00;

    public static int A00(int[] iArr, int i, int i2) {
        if (i2 <= 0 || i2 >= 11) {
            return -1;
        }
        if (i2 > 1) {
            if (iArr[1] == 65039) {
                i2--;
            } else {
                System.arraycopy(iArr, 1, iArr, 2, i2 - 1);
            }
        }
        iArr[1] = i;
        if (iArr[i2] != 65039) {
            i2++;
        }
        if (!A07(iArr, i2)) {
            return -1;
        }
        return i2;
    }

    public boolean ANi(Spannable spannable, int i, int i2, int i3, boolean z) {
        int i4 = i;
        Spannable spannable2 = spannable;
        int i5 = i2;
        int i6 = i3;
        boolean z2 = z;
        if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
            return ((AnonymousClass1LL) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGe, this.A00)).A0C(spannable2, i4, i5, i6, z2);
        }
        C52762je r1 = (C52762je) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADg, this.A00);
        if (i <= 0) {
            i4 = C77313n1.A01;
        }
        C52772jf r0 = new C52772jf();
        r0.A01 = i4;
        r0.A02 = i2;
        r0.A00 = i3;
        r0.A03 = z;
        return C52762je.A02(r1, spannable, r0);
    }

    public Drawable AeN(int i) {
        return AlA(Emoji.A01(i, 0, null), C77313n1.A00);
    }

    private Drawable A01(DrawableBackedEmoji drawableBackedEmoji, int i) {
        Drawable drawable = ((C52762je) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADg, this.A00)).A00.getDrawable(drawableBackedEmoji.A03());
        if (drawable.getIntrinsicWidth() != i) {
            return new C1301967x(drawable, i);
        }
        return drawable;
    }

    public static final AnonymousClass1L4 A02(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1L4.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1L4(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private Emoji A03(String str) {
        Emoji A002 = ((AnonymousClass1L1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AjT, this.A00)).A00(str);
        if (A002 instanceof DrawableBackedEmoji) {
            return A002;
        }
        if (Build.VERSION.SDK_INT < 19) {
            return C77313n1.A02;
        }
        return null;
    }

    private List A04() {
        boolean A03 = ((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03();
        AnonymousClass1L1 r0 = (AnonymousClass1L1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AjT, this.A00);
        if (A03) {
            return r0.A03();
        }
        return r0.A02();
    }

    private List A05(List list) {
        if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Emoji A002 = ((AnonymousClass1L1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AjT, this.A00)).A00(((Emoji) list.get(i)).A04());
            if (A002 instanceof DrawableBackedEmoji) {
                arrayList.add(A002);
            }
        }
        return Collections.unmodifiableList(arrayList);
    }

    public static boolean A07(int[] iArr, int i) {
        AnonymousClass1LQ r2 = C21951Ji.A00;
        if (r2.A03(r2.A02, r2.A00, r2.A01, iArr, 0, i) == i) {
            return true;
        }
        return false;
    }

    public boolean AMz(Spannable spannable, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        Spannable spannable2 = spannable;
        if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
            AnonymousClass1LL r3 = (AnonymousClass1LL) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGe, this.A00);
            return r3.A0C(spannable2, (int) TypedValue.applyDimension(1, 32.0f, ((Resources) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEg, r3.A00)).getDisplayMetrics()), i3, i4, true);
        }
        C52772jf r1 = new C52772jf();
        r1.A04 = false;
        r1.A02 = i;
        r1.A00 = i2;
        return C52762je.A02((C52762je) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADg, this.A00), spannable, r1);
    }

    public boolean ANh(Spannable spannable, int i) {
        Spannable spannable2 = spannable;
        int i2 = i;
        if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
            return ((AnonymousClass1LL) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGe, this.A00)).A0C(spannable2, i2, 0, -1, true);
        }
        C52772jf r0 = new C52772jf();
        r0.A01 = i;
        return C52762je.A02((C52762je) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADg, this.A00), spannable, r0);
    }

    public Drawable AeO(Emoji emoji) {
        return Al9(emoji, C77313n1.A00);
    }

    public Drawable AeP(String str) {
        return AlA(str, C77313n1.A00);
    }

    public Drawable Al9(Emoji emoji, int i) {
        if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
            return new C77323n2(emoji.A04(), ((AnonymousClass0iR) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AdI, this.A00)).A01(), i);
        }
        if (!(emoji instanceof DrawableBackedEmoji)) {
            emoji = A03(emoji.A04());
        }
        if (emoji instanceof DrawableBackedEmoji) {
            return A01((DrawableBackedEmoji) emoji, i);
        }
        return null;
    }

    public Drawable B3I(String str) {
        return AlA(str, C77313n1.A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0024, code lost:
        if (r2 > 127999) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean BBp(com.facebook.ui.emoji.model.Emoji r8) {
        /*
            r7 = this;
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            java.lang.Object r6 = r0.ALa()
            int[] r6 = (int[]) r6
            if (r6 != 0) goto L_0x000e
            r0 = 11
            int[] r6 = new int[r0]
        L_0x000e:
            java.lang.String r0 = r8.A04()
            int r3 = com.facebook.ui.emoji.model.Emoji.A00(r6, r0)
            r5 = 1
            if (r3 <= r5) goto L_0x002f
            r2 = r6[r5]
            r0 = 127995(0x1f3fb, float:1.79359E-40)
            if (r2 < r0) goto L_0x0026
            r1 = 127999(0x1f3ff, float:1.79365E-40)
            r0 = 1
            if (r2 <= r1) goto L_0x0027
        L_0x0026:
            r0 = 0
        L_0x0027:
            if (r0 == 0) goto L_0x002f
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            r0.C0w(r6)
            return r5
        L_0x002f:
            r0 = 127995(0x1f3fb, float:1.79359E-40)
            int r4 = A00(r6, r0, r3)
            if (r4 < 0) goto L_0x0054
            r1 = 3
            int r0 = X.AnonymousClass1Y3.AjT
            X.0UN r3 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r3)
            X.1L1 r0 = (X.AnonymousClass1L1) r0
            boolean r0 = r0.A04()
            if (r0 != 0) goto L_0x0054
            boolean r0 = A07(r6, r4)
            r2 = 0
            if (r0 != 0) goto L_0x005d
            r0 = 0
        L_0x0051:
            if (r0 != 0) goto L_0x0054
            r4 = -1
        L_0x0054:
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            r0.C0w(r6)
            if (r4 >= 0) goto L_0x005c
            r5 = 0
        L_0x005c:
            return r5
        L_0x005d:
            r1 = 4
            int r0 = X.AnonymousClass1Y3.BLt
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r0, r3)
            X.0iy r0 = (X.C09960iy) r0
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x006e
            r0 = 1
            goto L_0x0051
        L_0x006e:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r6, r2, r4)
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AjT
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1L1 r0 = (X.AnonymousClass1L1) r0
            com.facebook.ui.emoji.model.Emoji r0 = r0.A00(r3)
            boolean r0 = r0 instanceof com.facebook.ui.emoji.model.DrawableBackedEmoji
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L4.BBp(com.facebook.ui.emoji.model.Emoji):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r1 == false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean BHQ(java.lang.String r4) {
        /*
            r3 = this;
            if (r4 == 0) goto L_0x0013
            int r2 = r4.length()
            X.1LQ r0 = X.C21951Ji.A00
            r1 = 0
            int r0 = r0.A01(r4, r1, r2)
            if (r0 != r2) goto L_0x0010
            r1 = 1
        L_0x0010:
            r0 = 1
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            if (r0 != 0) goto L_0x0018
            r0 = 0
            return r0
        L_0x0018:
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BLt
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0iy r0 = (X.C09960iy) r0
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x002b
            r0 = 1
            return r0
        L_0x002b:
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AjT
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1L1 r0 = (X.AnonymousClass1L1) r0
            com.facebook.ui.emoji.model.Emoji r0 = r0.A00(r4)
            boolean r0 = r0 instanceof com.facebook.ui.emoji.model.DrawableBackedEmoji
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L4.BHQ(java.lang.String):boolean");
    }

    public CharSequence BKQ(CharSequence charSequence, float f) {
        return BKR(charSequence, (int) (f + 0.5f));
    }

    public CharSequence BKR(CharSequence charSequence, int i) {
        CharSequence charSequence2 = charSequence;
        int i2 = i;
        if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
            AnonymousClass1LL r3 = (AnonymousClass1LL) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGe, this.A00);
            if ((charSequence2 instanceof Spannable) && !(charSequence2 instanceof Editable)) {
                r3.A0C((Spannable) charSequence2, i2, 0, -1, true);
            } else if (r3.A0A()) {
                SpannableString spannableString = new SpannableString(charSequence2);
                r3.A0C(spannableString, i2, 0, -1, true);
                return spannableString;
            } else {
                int length = charSequence2.length();
                if (C35531rN.A03(charSequence2, 0, length)) {
                    SpannableString spannableString2 = new SpannableString(charSequence2);
                    if (spannableString2.length() != length) {
                        r3.A09("Background modification: %d -> %d", Integer.valueOf(length), Integer.valueOf(spannableString2.length()));
                        length = spannableString2.length();
                    }
                    C35531rN.A02(r3, spannableString2, length, 0, length, i2);
                    C35531rN.A01(r3, spannableString2, 0, length, i2, false);
                    return spannableString2;
                }
                Spannable A012 = C35531rN.A01(r3, charSequence2, 0, length, i2, true);
                if (A012 != null) {
                    return A012;
                }
            }
            return charSequence2;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence2);
        ANh(spannableStringBuilder, i2);
        return spannableStringBuilder;
    }

    private AnonymousClass1L4(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }

    public static void A06(List list, int[] iArr, int i) {
        for (int i2 = 127995; i2 <= 127999; i2++) {
            iArr[1] = i2;
            list.add(new BasicEmoji(new String(iArr, 0, i)));
        }
    }

    public boolean AMy(Spannable spannable) {
        return AMz(spannable, 0, spannable.length());
    }

    public List AVF() {
        List A04 = A04();
        ArrayList arrayList = new ArrayList();
        int size = A04.size();
        for (int i = 0; i < size; i++) {
            arrayList.add(((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A02((C25818Cmj) A04.get(i)));
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0021, code lost:
        if (r12 > 127999) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List AVG(int r12) {
        /*
            r11 = this;
            java.util.List r9 = r11.A04()
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            int r7 = r9.size()
            r6 = 0
        L_0x000e:
            if (r6 >= r7) goto L_0x0094
            java.lang.Object r5 = r9.get(r6)
            X.Cmj r5 = (X.C25818Cmj) r5
            java.util.List r2 = r5.A01
            r0 = 127995(0x1f3fb, float:1.79359E-40)
            if (r12 < r0) goto L_0x0023
            r1 = 127999(0x1f3ff, float:1.79365E-40)
            r0 = 1
            if (r12 <= r1) goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 == 0) goto L_0x0073
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            java.lang.Object r4 = r0.ALa()
            int[] r4 = (int[]) r4
            if (r4 != 0) goto L_0x0034
            r0 = 11
            int[] r4 = new int[r0]
        L_0x0034:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.Iterator r10 = r2.iterator()
        L_0x003d:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x006a
            java.lang.Object r1 = r10.next()
            com.facebook.ui.emoji.model.Emoji r1 = (com.facebook.ui.emoji.model.Emoji) r1
            java.lang.String r0 = r1.A04()
            int r0 = com.facebook.ui.emoji.model.Emoji.A00(r4, r0)
            int r2 = A00(r4, r12, r0)
            if (r2 >= 0) goto L_0x005b
            r3.add(r1)
            goto L_0x003d
        L_0x005b:
            java.lang.String r1 = new java.lang.String
            r0 = 0
            r1.<init>(r4, r0, r2)
            com.facebook.ui.emoji.model.BasicEmoji r0 = new com.facebook.ui.emoji.model.BasicEmoji
            r0.<init>(r1)
            r3.add(r0)
            goto L_0x003d
        L_0x006a:
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            r0.C0w(r4)
            java.util.List r2 = java.util.Collections.unmodifiableList(r3)
        L_0x0073:
            java.util.List r1 = r11.A05(r2)
            X.Cmj r3 = new X.Cmj
            java.lang.Integer r0 = r5.A00
            r3.<init>(r0, r1)
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BLt
            X.0UN r0 = r11.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0iy r0 = (X.C09960iy) r0
            X.Cmp r0 = r0.A02(r3)
            r8.add(r0)
            int r6 = r6 + 1
            goto L_0x000e
        L_0x0094:
            java.util.List r0 = java.util.Collections.unmodifiableList(r8)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L4.AVG(int):java.util.List");
    }

    public Drawable AlA(String str, int i) {
        if (!BHQ(str)) {
            return null;
        }
        if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
            return new C77323n2(str, ((AnonymousClass0iR) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AdI, this.A00)).A01(), i);
        }
        Emoji A03 = A03(str);
        if (A03 instanceof DrawableBackedEmoji) {
            return A01((DrawableBackedEmoji) A03, i);
        }
        return null;
    }

    public Emoji AlB(String str) {
        if (!C06850cB.A0B(str)) {
            int length = str.length();
            boolean z = false;
            if (C21951Ji.A00.A01(str, 0, length) == length) {
                z = true;
            }
            if (z) {
                if (((C09960iy) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BLt, this.A00)).A03()) {
                    return ((AnonymousClass1L1) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AjT, this.A00)).A00(str);
                }
                return A03(str);
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0024, code lost:
        if (r2 > 127999) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List AlC(com.facebook.ui.emoji.model.Emoji r10) {
        /*
            r9 = this;
            java.lang.String r1 = r10.A04()
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            java.lang.Object r5 = r0.ALa()
            int[] r5 = (int[]) r5
            if (r5 != 0) goto L_0x0012
            r0 = 11
            int[] r5 = new int[r0]
        L_0x0012:
            int r7 = com.facebook.ui.emoji.model.Emoji.A00(r5, r1)
            r8 = 1
            if (r7 <= r8) goto L_0x0060
            r2 = r5[r8]
            r0 = 127995(0x1f3fb, float:1.79359E-40)
            if (r2 < r0) goto L_0x0026
            r1 = 127999(0x1f3ff, float:1.79365E-40)
            r0 = 1
            if (r2 <= r1) goto L_0x0027
        L_0x0026:
            r0 = 0
        L_0x0027:
            if (r0 == 0) goto L_0x0060
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            if (r10 == 0) goto L_0x0043
            r4 = 0
            r3 = 2
            if (r7 > r3) goto L_0x0050
            java.lang.String r1 = new java.lang.String
            int r0 = r7 + -1
            r1.<init>(r5, r4, r0)
        L_0x003b:
            com.facebook.ui.emoji.model.BasicEmoji r0 = new com.facebook.ui.emoji.model.BasicEmoji
            r0.<init>(r1)
            r6.add(r0)
        L_0x0043:
            A06(r6, r5, r7)
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            r0.C0w(r5)
        L_0x004b:
            java.util.List r0 = r9.A05(r6)
            return r0
        L_0x0050:
            int r2 = r7 + -2
            java.lang.System.arraycopy(r5, r3, r5, r8, r2)
            java.lang.String r1 = new java.lang.String
            int r0 = r7 + -1
            r1.<init>(r5, r4, r0)
            java.lang.System.arraycopy(r5, r8, r5, r3, r2)
            goto L_0x003b
        L_0x0060:
            r0 = 127995(0x1f3fb, float:1.79359E-40)
            int r0 = A00(r5, r0, r7)
            if (r0 >= 0) goto L_0x007a
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            r0.C0w(r5)
            if (r10 != 0) goto L_0x0075
            java.util.List r6 = java.util.Collections.emptyList()
            goto L_0x004b
        L_0x0075:
            java.util.List r6 = java.util.Collections.singletonList(r10)
            goto L_0x004b
        L_0x007a:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            if (r10 == 0) goto L_0x0084
            r1.add(r10)
        L_0x0084:
            A06(r1, r5, r0)
            X.0ZG r0 = com.facebook.ui.emoji.model.Emoji.A01
            r0.C0w(r5)
            java.util.List r6 = java.util.Collections.unmodifiableList(r1)
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L4.AlC(com.facebook.ui.emoji.model.Emoji):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        if (r0 == null) goto L_0x0041;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List AlD(java.lang.String r8) {
        /*
            r7 = this;
            int r6 = r8.length()
            r5 = 0
            r1 = 0
        L_0x0006:
            if (r1 >= r6) goto L_0x004a
            X.1LQ r0 = X.C21951Ji.A00
            int r4 = r0.A01(r8, r1, r6)
            if (r4 > r1) goto L_0x0013
            int r1 = r1 + 1
            goto L_0x0006
        L_0x0013:
            java.lang.String r3 = r8.substring(r1, r4)
            if (r5 != 0) goto L_0x001e
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
        L_0x001e:
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BLt
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0iy r0 = (X.C09960iy) r0
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x0043
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AjT
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1L1 r0 = (X.AnonymousClass1L1) r0
            com.facebook.ui.emoji.model.Emoji r0 = r0.A00(r3)
        L_0x003e:
            r5.add(r0)
        L_0x0041:
            r1 = r4
            goto L_0x0006
        L_0x0043:
            com.facebook.ui.emoji.model.Emoji r0 = r7.A03(r3)
            if (r0 != 0) goto L_0x003e
            goto L_0x0041
        L_0x004a:
            if (r5 != 0) goto L_0x0051
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x0051:
            java.util.List r0 = java.util.Collections.unmodifiableList(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L4.AlD(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0021, code lost:
        if (r2 > 127999) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int B3C(com.facebook.ui.emoji.model.Emoji r4, int r5) {
        /*
            r3 = this;
            java.lang.String r2 = r4.A04()
            int r1 = r2.length()
            r0 = 3
            if (r1 < r0) goto L_0x0028
            r0 = 0
            int r0 = java.lang.Character.codePointAt(r2, r0)
            int r0 = java.lang.Character.charCount(r0)
            int r2 = java.lang.Character.codePointAt(r2, r0)
            r0 = 127995(0x1f3fb, float:1.79359E-40)
            if (r2 < r0) goto L_0x0023
            r1 = 127999(0x1f3ff, float:1.79365E-40)
            r0 = 1
            if (r2 <= r1) goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 != 0) goto L_0x0027
            return r5
        L_0x0027:
            return r2
        L_0x0028:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L4.B3C(com.facebook.ui.emoji.model.Emoji, int):int");
    }

    public void BDB(Editable editable, Emoji emoji) {
        editable.replace(Selection.getSelectionStart(editable), Selection.getSelectionEnd(editable), emoji.A04());
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 163 */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009d, code lost:
        if ((r6.A01.A00(com.facebook.ui.emoji.model.Emoji.A02(r10, r7, r2)) instanceof com.facebook.ui.emoji.model.DrawableBackedEmoji) != false) goto L_0x009f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean BH9(java.lang.CharSequence r10) {
        /*
            r9 = this;
            boolean r0 = X.C06850cB.A0B(r10)
            r3 = 0
            if (r0 != 0) goto L_0x0087
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BLt
            X.0UN r0 = r9.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0iy r0 = (X.C09960iy) r0
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x00a6
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AGe
            X.0UN r0 = r9.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1LL r5 = (X.AnonymousClass1LL) r5
            r4 = 2147483647(0x7fffffff, float:NaN)
            if (r10 == 0) goto L_0x0087
            int r0 = r10.length()
            if (r0 == 0) goto L_0x0087
            int r3 = r10.length()
            r6 = 0
            r7 = 1
            r2 = 0
        L_0x0035:
            if (r6 >= r3) goto L_0x00d2
            int r1 = java.lang.Character.codePointAt(r10, r6)
            boolean r0 = java.lang.Character.isWhitespace(r1)
            if (r0 == 0) goto L_0x0048
            int r0 = java.lang.Character.charCount(r1)
            int r6 = r6 + r0
            r7 = 1
            goto L_0x0035
        L_0x0048:
            X.1LQ r0 = X.C21951Ji.A00
            int r0 = r0.A01(r10, r6, r3)
            if (r6 >= r0) goto L_0x0057
            int r2 = r2 + 1
            if (r2 > r4) goto L_0x0087
            r6 = r0
        L_0x0055:
            r7 = 0
            goto L_0x0035
        L_0x0057:
            if (r7 == 0) goto L_0x0087
            X.1LQ r0 = X.AnonymousClass1LN.A00
            int r1 = r0.A01(r10, r6, r3)
            if (r6 >= r1) goto L_0x0087
            java.lang.String r0 = com.facebook.ui.emoji.model.Emoji.A02(r10, r6, r1)
            java.lang.String r0 = r5.A07(r0)
            if (r0 == 0) goto L_0x0087
            int r2 = r2 + 1
            if (r2 > r4) goto L_0x0087
            r6 = r1
            goto L_0x0055
        L_0x0071:
            if (r8 == 0) goto L_0x0087
            X.1LQ r0 = X.AnonymousClass1LN.A00
            int r2 = r0.A01(r10, r7, r4)
            if (r7 >= r2) goto L_0x0087
            java.lang.String r1 = com.facebook.ui.emoji.model.Emoji.A02(r10, r7, r2)
            X.1L1 r0 = r6.A01
            java.lang.String r0 = r0.A01(r1)
            if (r0 != 0) goto L_0x009f
        L_0x0087:
            r0 = 0
            return r0
        L_0x0089:
            X.1LQ r0 = X.C21951Ji.A00
            int r2 = r0.A01(r10, r7, r4)
            if (r7 >= r2) goto L_0x0071
            java.lang.String r1 = com.facebook.ui.emoji.model.Emoji.A02(r10, r7, r2)
            X.1L1 r0 = r6.A01
            com.facebook.ui.emoji.model.Emoji r0 = r0.A00(r1)
            boolean r0 = r0 instanceof com.facebook.ui.emoji.model.DrawableBackedEmoji
            if (r0 == 0) goto L_0x0071
        L_0x009f:
            int r3 = r3 + 1
            if (r3 > r5) goto L_0x0087
            r7 = r2
            r8 = 0
            goto L_0x00bf
        L_0x00a6:
            int r1 = X.AnonymousClass1Y3.ADg
            X.0UN r0 = r9.A00
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.2je r6 = (X.C52762je) r6
            r5 = 2147483647(0x7fffffff, float:NaN)
            boolean r0 = X.C06850cB.A0B(r10)
            if (r0 != 0) goto L_0x0087
            int r4 = r10.length()
            r7 = 0
            r8 = 1
        L_0x00bf:
            if (r7 >= r4) goto L_0x00d2
            int r1 = java.lang.Character.codePointAt(r10, r7)
            boolean r0 = java.lang.Character.isWhitespace(r1)
            if (r0 == 0) goto L_0x0089
            int r0 = java.lang.Character.charCount(r1)
            int r7 = r7 + r0
            r8 = 1
            goto L_0x00bf
        L_0x00d2:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L4.BH9(java.lang.CharSequence):boolean");
    }
}
