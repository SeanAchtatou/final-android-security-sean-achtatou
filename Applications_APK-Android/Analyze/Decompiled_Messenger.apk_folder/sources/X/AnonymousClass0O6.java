package X;

/* renamed from: X.0O6  reason: invalid class name */
public final class AnonymousClass0O6 {
    public static int A02(int i, int i2, float f) {
        return Math.round(((float) (i2 - i)) * f) + i;
    }

    public static float A01(float f, float f2, float f3) {
        return A00(((f2 - f) * f3) + f, f, f2);
    }

    public static float A00(float f, float f2, float f3) {
        return Math.min(Math.max(f2, f3), Math.max(Math.min(f2, f3), f));
    }
}
