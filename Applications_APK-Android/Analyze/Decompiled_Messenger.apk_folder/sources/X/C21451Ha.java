package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.mms.MmsData;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Ha  reason: invalid class name and case insensitive filesystem */
public final class C21451Ha {
    private static volatile C21451Ha A03;
    public AnonymousClass0UN A00;
    private final DeprecatedAnalyticsLogger A01;
    private final C10960l9 A02;

    public static void A05(C21451Ha r1, C11670nb r2) {
        if (C010708t.A0X(2)) {
            r2.A05();
        }
        r1.A01.A09(r2);
    }

    public static C22361La A00(C21451Ha r1, String str) {
        C22361La A04 = r1.A01.A04(str, false);
        if (A04.A0B()) {
            A04.A08(ImmutableMap.of("qe_group_rollout", "SmsTakeoverRolloutDefault"));
        }
        return A04;
    }

    public static C11670nb A01(String str) {
        C11670nb r2 = new C11670nb(str);
        C11670nb.A02(r2, ImmutableMap.of("qe_group_rollout", "SmsTakeoverRolloutDefault"), false);
        return r2;
    }

    public static final C21451Ha A02(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C21451Ha.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C21451Ha(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static String A03(Message message) {
        if (message.A0z != null) {
            return "sticker";
        }
        MmsData mmsData = message.A0N;
        if (mmsData.A02.isEmpty()) {
            return null;
        }
        MediaResource mediaResource = (MediaResource) mmsData.A02.get(0);
        String str = mediaResource.A0a;
        if (Platform.stringIsNullOrEmpty(str)) {
            return mediaResource.A0L.name();
        }
        return str;
    }

    public static void A04(C11670nb r1, boolean z, String str, int i, int i2) {
        r1.A0E("is_mms", z);
        if (!Platform.stringIsNullOrEmpty(str)) {
            r1.A0D("mms_media_type", str);
            r1.A09("mms_media_count", i);
        }
        if (i2 > 1) {
            r1.A09(C99084oO.$const$string(80), i2);
        }
    }

    public static void A06(C21451Ha r3, String str) {
        C11670nb A012 = A01("sms_takeover_sys_notification_action");
        A012.A0D("action", str);
        A012.A0D("state_now", C190318tG.A00(r3.A0B()));
        A05(r3, A012);
    }

    public static void A07(C21451Ha r2, String str, String str2) {
        C11670nb A012 = A01("sms_takeover_delete_thread_dialog_action");
        A012.A0D("call_context", str);
        A012.A0D("delete_thread_dialog_action", str2);
        A05(r2, A012);
    }

    public static void A08(C21451Ha r2, String str, String str2) {
        C11670nb A012 = A01("sms_takeover_explain_dialog_action");
        A012.A0D("call_context", str);
        A012.A0D("explain_dialog_action", str2);
        A05(r2, A012);
    }

    public static void A09(C21451Ha r3, String str, String str2) {
        C11670nb A012 = A01("sms_takeover_chat_head_action");
        A012.A0D("action", str);
        A012.A0D("reason", str2);
        A012.A0D("state_now", C190318tG.A00(r3.A0B()));
        A05(r3, A012);
    }

    public static void A0A(C21451Ha r3, boolean z, boolean z2, String str, int i, int i2) {
        C11670nb A012 = A01("sms_takeover_send_message");
        A012.A0E("is_resend", z2);
        A012.A0D("state_now", C190318tG.A00(r3.A0B()));
        A04(A012, z, str, i, i2);
        A05(r3, A012);
    }

    public Integer A0B() {
        if (!this.A02.A0B()) {
            return AnonymousClass07B.A00;
        }
        return this.A02.A05();
    }

    public void A0C(CallerContext callerContext, String str) {
        C11670nb A012 = A01("sms_takeover_group_name_action");
        A012.A0D("call_context", callerContext.toString());
        A012.A0D("action", str);
        A05(this, A012);
    }

    public void A0D(C138436dA r4, boolean z) {
        String str;
        C11670nb A012 = A01("sms_takeover_block_contact");
        if (z) {
            str = "block";
        } else {
            str = "unblock";
        }
        A012.A0D("action", str);
        A012.A0D("call_context", r4.toString());
        A05(this, A012);
    }

    public void A0E(Object obj, Integer num, Integer num2) {
        C11670nb A012 = A01("sms_takeover_state_change");
        A012.A0C("call_context", obj);
        A012.A0D("state_before", C190318tG.A00(num));
        A012.A0D("state_now", C190318tG.A00(num2));
        A012.A0E(ECX.$const$string(40), ((C197479Ql) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APc, this.A00)).A02());
        A05(this, A012);
    }

    public void A0F(String str) {
        C22361La A002 = A00(this, "sms_takeover_add_user_to_white_list");
        if (A002.A0B()) {
            A002.A06("call_context", str);
            A002.A0A();
        }
    }

    public void A0G(String str) {
        C22361La A002 = A00(this, "sms_takeover_remove_user_from_white_list");
        if (A002.A0B()) {
            A002.A06("call_context", str);
            A002.A0A();
        }
    }

    public void A0H(String str, ThreadKey threadKey, int i, boolean z, String str2) {
        C11670nb A012 = A01("sms_takeover_matching_banner");
        A012.A0D("action", str);
        A012.A09("num_times_shown", i);
        A012.A0E("in_chat_head", z);
        if (threadKey != null) {
            A012.A0D("thread_key", threadKey.A0H());
        }
        if (!Platform.stringIsNullOrEmpty(str2)) {
            A012.A0D("reason", str2);
        }
        A05(this, A012);
    }

    public void A0I(String str, String str2) {
        C11670nb A012 = A01("sms_takeover_qp_action");
        A012.A0D("qp_source", str);
        A012.A0D("qp_action", str2);
        A05(this, A012);
    }

    public void A0J(String str, String str2, String str3, Integer num) {
        C11670nb A012 = A01("sms_takeover_nux_action");
        A012.A0D("nux_action", str);
        A012.A0D("nux_caller_context", str2);
        A012.A0D("nux_optin_flow", str3);
        A012.A0E("nux_to_full_mode", true);
        A012.A0D("state_before", C190318tG.A00(num));
        A05(this, A012);
    }

    public void A0K(String str, String str2, String str3, boolean z) {
        C11670nb A012 = A01("sms_takeover_auto_matching_interstitial_action");
        A012.A0D("action", str);
        A012.A0D("sms_mode", str2);
        A012.A0D("source", str3);
        A012.A0E("call_log_upload_enabled", z);
        A05(this, A012);
    }

    public void A0L(boolean z, String str, Integer num) {
        String str2;
        C11670nb A012 = A01("sms_takeover_optin_result");
        if (z) {
            str2 = "succeed";
        } else {
            str2 = "cancelled";
        }
        A012.A0D("optin_result", str2);
        A012.A0D("call_context", str);
        A012.A0D("state_before", C190318tG.A00(num));
        A05(this, A012);
    }

    private C21451Ha(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = C06920cI.A00(r3);
        this.A02 = C10960l9.A00(r3);
    }
}
