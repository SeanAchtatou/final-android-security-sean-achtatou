package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.tigon.nativeservice.common.NativePlatformContextHolder;

/* renamed from: X.1l3  reason: invalid class name and case insensitive filesystem */
public final class C32061l3 implements AnonymousClass06U {
    public final /* synthetic */ NativePlatformContextHolder A00;

    public C32061l3(NativePlatformContextHolder nativePlatformContextHolder) {
        this.A00 = nativePlatformContextHolder;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r5) {
        int A002 = AnonymousClass09Y.A00(-1051194949);
        NativePlatformContextHolder.handleConnectivityUpdate(this.A00);
        AnonymousClass09Y.A01(-1299864699, A002);
    }
}
