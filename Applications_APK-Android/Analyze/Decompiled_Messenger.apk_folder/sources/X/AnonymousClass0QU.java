package X;

import android.os.Build;

/* renamed from: X.0QU  reason: invalid class name */
public final class AnonymousClass0QU implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.systrace.TraceListenerNotifier$1";
    public final /* synthetic */ C001100s A00;
    public final /* synthetic */ long A01;

    public AnonymousClass0QU(C001100s r1, long j) {
        this.A00 = r1;
        this.A01 = j;
    }

    public void run() {
        synchronized (this.A00.A01) {
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                }
            } else {
                while (C001100s.A03.lastModified() == this.A01) {
                }
            }
            this.A00.A01();
        }
    }
}
