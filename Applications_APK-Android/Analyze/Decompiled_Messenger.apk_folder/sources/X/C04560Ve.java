package X;

import com.google.common.base.Preconditions;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.0Ve  reason: invalid class name and case insensitive filesystem */
public final class C04560Ve {
    public int A00;
    public final AnonymousClass1YF A01 = new AnonymousClass1YF(this);
    public final ConcurrentLinkedQueue A02 = new ConcurrentLinkedQueue();
    public final AtomicInteger A03 = new AtomicInteger();
    public final AtomicInteger A04 = new AtomicInteger();
    public final ReentrantLock A05 = new ReentrantLock();
    private final C04550Vd A06;

    public void A00() {
        this.A05.lock();
        int i = this.A00;
        if (i > 0) {
            this.A00 = i + 1;
        }
    }

    public void A01() {
        while (true) {
            C07820eD r5 = (C07820eD) this.A02.poll();
            if (r5 != null) {
                C04550Vd r4 = this.A06;
                try {
                    C179878Tk A002 = C25201Ys.A00(r4.A08, r5);
                    if (A002 != null) {
                        A002.onTaskScheduled();
                    }
                    C04550Vd.A03(r4, r5);
                } catch (RejectedExecutionException e) {
                    C04590Vi r2 = r4.A02;
                    Preconditions.checkNotNull(r2);
                    r4.A0E.execute(new AnonymousClass8Tj(r4, r5, r2, e));
                }
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002b A[Catch:{ Exception -> 0x003d, all -> 0x003f }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002f A[Catch:{ Exception -> 0x003d, all -> 0x003f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r8 = this;
            java.util.concurrent.locks.ReentrantLock r0 = r8.A05
            int r6 = r0.getHoldCount()
            r0 = 1
            if (r6 == r0) goto L_0x000a
            r0 = 0
        L_0x000a:
            r7 = 0
            java.lang.String r5 = " getHoldCount()="
            java.lang.String r4 = " holdCount="
            if (r0 == 0) goto L_0x006b
            int r0 = r8.A00     // Catch:{ Exception -> 0x003d }
            if (r0 > 0) goto L_0x0026
            java.util.concurrent.atomic.AtomicInteger r0 = r8.A04     // Catch:{ Exception -> 0x003d }
            int r0 = r0.get()     // Catch:{ Exception -> 0x003d }
            if (r0 > 0) goto L_0x0028
            java.util.concurrent.atomic.AtomicInteger r0 = r8.A03     // Catch:{ Exception -> 0x003d }
            int r0 = r0.get()     // Catch:{ Exception -> 0x003d }
            if (r0 <= 0) goto L_0x0026
            goto L_0x0028
        L_0x0026:
            r0 = 1
            goto L_0x0029
        L_0x0028:
            r0 = 0
        L_0x0029:
            if (r0 == 0) goto L_0x002f
            r8.A01()     // Catch:{ Exception -> 0x003d }
            goto L_0x006b
        L_0x002f:
            java.util.concurrent.atomic.AtomicInteger r0 = r8.A04     // Catch:{ Exception -> 0x003d }
            int r0 = r0.get()     // Catch:{ Exception -> 0x003d }
            if (r0 != 0) goto L_0x006b
            X.1YF r0 = r8.A01     // Catch:{ Exception -> 0x003d }
            r0.A02()     // Catch:{ Exception -> 0x003d }
            goto L_0x006b
        L_0x003d:
            r7 = move-exception
            throw r7     // Catch:{ all -> 0x003f }
        L_0x003f:
            r3 = move-exception
            int r0 = r8.A00     // Catch:{ IllegalMonitorStateException -> 0x004e }
            if (r0 <= 0) goto L_0x0048
            int r0 = r0 + -1
            r8.A00 = r0     // Catch:{ IllegalMonitorStateException -> 0x004e }
        L_0x0048:
            java.util.concurrent.locks.ReentrantLock r0 = r8.A05     // Catch:{ IllegalMonitorStateException -> 0x004e }
            r0.unlock()     // Catch:{ IllegalMonitorStateException -> 0x004e }
            goto L_0x006a
        L_0x004e:
            r2 = move-exception
            java.lang.IllegalMonitorStateException r3 = new java.lang.IllegalMonitorStateException
            java.lang.String r1 = r2.getMessage()
            java.util.concurrent.locks.ReentrantLock r0 = r8.A05
            int r0 = r0.getHoldCount()
            java.lang.String r0 = X.AnonymousClass08S.A0N(r1, r4, r6, r5, r0)
            r3.<init>(r0)
            r3.initCause(r2)
            if (r7 == 0) goto L_0x006a
            X.AnonymousClass8NQ.A00(r3, r7)
        L_0x006a:
            throw r3
        L_0x006b:
            int r0 = r8.A00     // Catch:{ IllegalMonitorStateException -> 0x0079 }
            if (r0 <= 0) goto L_0x0073
            int r0 = r0 + -1
            r8.A00 = r0     // Catch:{ IllegalMonitorStateException -> 0x0079 }
        L_0x0073:
            java.util.concurrent.locks.ReentrantLock r0 = r8.A05     // Catch:{ IllegalMonitorStateException -> 0x0079 }
            r0.unlock()     // Catch:{ IllegalMonitorStateException -> 0x0079 }
            return
        L_0x0079:
            r3 = move-exception
            java.lang.IllegalMonitorStateException r2 = new java.lang.IllegalMonitorStateException
            java.lang.String r1 = r3.getMessage()
            java.util.concurrent.locks.ReentrantLock r0 = r8.A05
            int r0 = r0.getHoldCount()
            java.lang.String r0 = X.AnonymousClass08S.A0N(r1, r4, r6, r5, r0)
            r2.<init>(r0)
            r2.initCause(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04560Ve.A02():void");
    }

    public void A03() {
        this.A04.incrementAndGet();
        try {
            this.A05.lock();
            this.A00++;
        } finally {
            this.A04.decrementAndGet();
        }
    }

    public C04560Ve(C04550Vd r2) {
        this.A06 = r2;
    }
}
