package X;

import android.content.Intent;
import android.net.Uri;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.send.trigger.NavigationTrigger;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

@UserScoped
/* renamed from: X.0ci  reason: invalid class name and case insensitive filesystem */
public final class C07150ci {
    private static C05540Zi A02;
    public AnonymousClass0UN A00;
    private C04310Tq A01;

    public static final C07150ci A00(AnonymousClass1XY r4) {
        C07150ci r0;
        synchronized (C07150ci.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C07150ci((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (C07150ci) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public ThreadKey A02(Intent intent) {
        C15000uY r2 = (C15000uY) AnonymousClass1XX.A03(AnonymousClass1Y3.BGy, this.A00);
        if (intent.hasExtra("thread_key")) {
            return (ThreadKey) intent.getParcelableExtra("thread_key");
        }
        String $const$string = AnonymousClass24B.$const$string(30);
        if (intent.hasExtra($const$string)) {
            return ThreadKey.A06(intent.getStringExtra($const$string));
        }
        if (intent.hasExtra("user_id")) {
            return r2.A01(Long.parseLong(intent.getStringExtra("user_id")));
        }
        return null;
    }

    public ListenableFuture A03(Message message, NavigationTrigger navigationTrigger, C53972lf r8) {
        ((C191016u) this.A01.get()).ASx(message.A0U, "sendMsg");
        ((C08770fv) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B9x, this.A00)).A0H(message.A0w);
        return AnonymousClass169.A03(((C420527z) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEE, this.A00)).A0J(message, ErrorReportingConstants.APP_NAME_KEY, navigationTrigger, r8), new C89214Oc(), (Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A6Z, this.A00));
    }

    private C07150ci(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.AXk, r3);
    }

    public static void A01(Intent intent) {
        Uri data = intent.getData();
        String authority = data.getAuthority();
        String str = data.getPathSegments().get(0);
        String uri = data.toString();
        if ("user".equals(authority)) {
            intent.putExtra("user_id", str);
        } else if ("groupthreadfbid".equals(authority)) {
            intent.putExtra("thread_key", ThreadKey.A00(Long.parseLong(str)));
        } else if (uri.startsWith(C52652jT.A0Y)) {
            intent.putExtra("thread_key", ThreadKey.A03(Long.parseLong(str)));
        }
    }
}
