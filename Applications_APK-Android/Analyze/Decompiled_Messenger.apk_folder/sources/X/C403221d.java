package X;

import com.facebook.video.engine.api.VideoPlayerParams;
import java.util.List;

/* renamed from: X.21d  reason: invalid class name and case insensitive filesystem */
public interface C403221d extends AnonymousClass8AC {
    int AbS();

    int AbT();

    List Adn();

    int AsZ();

    int B8T();

    int B8W();

    AnonymousClass8K8 B8f();

    VideoPlayerParams B8g();

    int B8h();

    Integer B8n();

    int B8w();

    boolean BFb();

    boolean BFc();

    boolean BFu();

    boolean BGC();

    boolean BH3();

    boolean BH6();

    void BwX(C53752lJ r1);

    void C8b(boolean z);

    void C8d(boolean z, C53752lJ r2);

    void CBH(AnonymousClass8BB r1);

    void CL7(String str, AnonymousClass22T r2, String str2);

    boolean isPlaying();
}
