package X;

import android.graphics.Bitmap;

/* renamed from: X.1Nl  reason: invalid class name and case insensitive filesystem */
public final class C22951Nl implements C22891Nf {
    public final /* synthetic */ AnonymousClass1NW A00;

    public C22951Nl(AnonymousClass1NW r1) {
        this.A00 = r1;
    }

    public void C0t(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        try {
            AnonymousClass1NW r8 = this.A00;
            synchronized (r8) {
                int A01 = AnonymousClass1SJ.A01(bitmap);
                boolean z = false;
                if (r8.A00 > 0) {
                    z = true;
                }
                C05520Zg.A06(z, "No bitmaps registered.");
                long j = (long) A01;
                long j2 = r8.A01;
                boolean z2 = false;
                if (j <= j2) {
                    z2 = true;
                }
                Object[] objArr = {Integer.valueOf(A01), Long.valueOf(j2)};
                if (z2) {
                    r8.A01 = j2 - j;
                    r8.A00--;
                } else {
                    throw new IllegalArgumentException(C05520Zg.A00("Bitmap size bigger than the total registered size: %d, %d", objArr));
                }
            }
            bitmap.recycle();
        } catch (Throwable th) {
            bitmap.recycle();
            throw th;
        }
    }
}
