package X;

/* renamed from: X.0ae  reason: invalid class name and case insensitive filesystem */
public final class C05980ae {
    public AnonymousClass0UN A00;
    public final AnonymousClass0US A01;
    public final AnonymousClass0US A02;
    public final AnonymousClass0US A03;
    public final AnonymousClass0US A04;
    public final AnonymousClass0US A05;
    public final AnonymousClass0US A06;
    public final AnonymousClass0US A07;
    public final AnonymousClass0US A08;
    public final AnonymousClass0US A09;
    public final AnonymousClass0US A0A;
    public final AnonymousClass0US A0B;
    public final AnonymousClass0US A0C;
    public final AnonymousClass0US A0D;
    public final AnonymousClass0US A0E;
    public final AnonymousClass0US A0F;
    public final AnonymousClass0US A0G;
    public final AnonymousClass0US A0H;
    public final AnonymousClass0US A0I;
    public final AnonymousClass0US A0J;
    public final AnonymousClass0US A0K;
    public final AnonymousClass0US A0L;
    public final AnonymousClass0US A0M;
    public final AnonymousClass0US A0N;
    public final AnonymousClass0US A0O;
    public final AnonymousClass0US A0P;
    public final AnonymousClass0US A0Q;
    public final AnonymousClass0US A0R;
    public final AnonymousClass0US A0S;

    public static final C05980ae A00(AnonymousClass1XY r1) {
        return new C05980ae(r1);
    }

    public C05980ae(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A01 = AnonymousClass0VB.A00(AnonymousClass1Y3.BIr, r3);
        this.A02 = AnonymousClass0VB.A00(AnonymousClass1Y3.Amg, r3);
        this.A0D = AnonymousClass0VB.A00(AnonymousClass1Y3.B84, r3);
        this.A0M = AnonymousClass0UQ.A00(AnonymousClass1Y3.A4a, r3);
        this.A0N = AnonymousClass0VB.A00(AnonymousClass1Y3.Ak9, r3);
        this.A0O = AnonymousClass0VB.A00(AnonymousClass1Y3.AJA, r3);
        this.A0P = AnonymousClass0VB.A00(AnonymousClass1Y3.A6i, r3);
        this.A0Q = AnonymousClass0VB.A00(AnonymousClass1Y3.AXx, r3);
        this.A0R = AnonymousClass0VB.A00(AnonymousClass1Y3.A57, r3);
        this.A0S = AnonymousClass0VB.A00(AnonymousClass1Y3.Aan, r3);
        this.A03 = AnonymousClass0VB.A00(AnonymousClass1Y3.BHK, r3);
        this.A04 = AnonymousClass0VB.A00(AnonymousClass1Y3.BF3, r3);
        this.A05 = AnonymousClass0VB.A00(AnonymousClass1Y3.AD6, r3);
        this.A06 = AnonymousClass0VB.A00(AnonymousClass1Y3.AnE, r3);
        this.A07 = AnonymousClass0VB.A00(AnonymousClass1Y3.Aar, r3);
        this.A08 = AnonymousClass0VB.A00(AnonymousClass1Y3.BS9, r3);
        this.A09 = AnonymousClass0VB.A00(AnonymousClass1Y3.BDK, r3);
        this.A0A = AnonymousClass0VB.A00(AnonymousClass1Y3.Aas, r3);
        this.A0B = AnonymousClass0UQ.A00(AnonymousClass1Y3.Ap1, r3);
        this.A0C = AnonymousClass0VB.A00(AnonymousClass1Y3.ATx, r3);
        this.A0E = AnonymousClass0VB.A00(AnonymousClass1Y3.BGa, r3);
        this.A0F = AnonymousClass0VB.A00(AnonymousClass1Y3.ATF, r3);
        this.A0G = AnonymousClass0VB.A00(AnonymousClass1Y3.AF3, r3);
        this.A0H = AnonymousClass0VB.A00(AnonymousClass1Y3.AmB, r3);
        this.A0I = AnonymousClass0VB.A00(AnonymousClass1Y3.BSy, r3);
        this.A0J = AnonymousClass0VB.A00(AnonymousClass1Y3.BKs, r3);
        this.A0K = AnonymousClass0VB.A00(AnonymousClass1Y3.BDA, r3);
        this.A0L = AnonymousClass0VB.A00(AnonymousClass1Y3.BOc, r3);
    }
}
