package X;

import android.os.Bundle;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.model.MontageThreadInfo;
import com.facebook.messaging.service.model.FetchThreadParams;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.1TK  reason: invalid class name */
public final class AnonymousClass1TK implements CallerContextable {
    public static final CallerContext A02 = CallerContext.A04(AnonymousClass1TK.class);
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.loader.UserMontageThreadLoader";
    public AnonymousClass0UN A00;
    @LoggedInUser
    private final C04310Tq A01;

    public static final AnonymousClass1TK A00(AnonymousClass1XY r1) {
        return new AnonymousClass1TK(r1);
    }

    public MontageThreadInfo A01(ThreadSummary threadSummary, ImmutableList immutableList) {
        Message A0E = ((AnonymousClass1TU) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A8n, this.A00)).A0E(immutableList, false);
        AnonymousClass2V7 A002 = MontageThreadInfo.A00(immutableList, 1, threadSummary);
        ImmutableList A012 = ((C33381nU) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ApM, this.A00)).A01(threadSummary, A0E);
        A002.A03 = A012;
        C28931fb.A06(A012, "seenByUserList");
        A002.A04.add("seenByUserList");
        return new MontageThreadInfo(A002);
    }

    public ListenableFuture A02() {
        MessagesCollection A07;
        User user = (User) this.A01.get();
        if (user == null) {
            return C05350Yp.A03(null);
        }
        ThreadKey A012 = C12150od.A01(user.A0D);
        if (A012 == null) {
            return C05350Yp.A03(null);
        }
        int i = AnonymousClass1Y3.AuB;
        ThreadSummary A08 = ((C26681bq) AnonymousClass1XX.A02(0, i, this.A00)).A08(A012);
        MontageThreadInfo montageThreadInfo = null;
        if (!(A08 == null || (A07 = ((C26681bq) AnonymousClass1XX.A02(0, i, this.A00)).A07(A012)) == null)) {
            montageThreadInfo = A01(A08, A07.A01.reverse());
        }
        if (montageThreadInfo != null) {
            return C05350Yp.A03(montageThreadInfo);
        }
        C60292wu r2 = new C60292wu();
        r2.A03 = ThreadCriteria.A00(A012);
        r2.A01 = C09510hU.PREFER_CACHE_IF_UP_TO_DATE;
        r2.A00 = 100;
        r2.A05 = false;
        FetchThreadParams fetchThreadParams = new FetchThreadParams(r2);
        Bundle bundle = new Bundle();
        bundle.putParcelable("fetchThreadParams", fetchThreadParams);
        return AnonymousClass169.A00(((BlueServiceOperationFactory) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B1W, this.A00)).newInstance("fetch_thread", bundle, 1, A02).CHd(), new C51792hp(this));
    }

    public AnonymousClass1TK(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(9, r3);
        this.A01 = AnonymousClass0WY.A01(r3);
    }
}
