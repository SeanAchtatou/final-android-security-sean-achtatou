package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.widget.recyclerview.BetterRecyclerView;

/* renamed from: X.18u  reason: invalid class name and case insensitive filesystem */
public final class C195718u extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ BetterRecyclerView A00;

    public C195718u(BetterRecyclerView betterRecyclerView) {
        this.A00 = betterRecyclerView;
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        C33781o8 A05;
        View A0Z = this.A00.A0Z(motionEvent.getX(), motionEvent.getY());
        if (A0Z != null && this.A00.A07 != null) {
            int A02 = RecyclerView.A02(A0Z);
            if (A02 == -1) {
                return true;
            }
            BetterRecyclerView betterRecyclerView = this.A00;
            D61 d61 = betterRecyclerView.A07;
            C20831Dz r0 = betterRecyclerView.A0J;
            long j = -1;
            if (!(r0 == null || !r0.hasStableIds() || (A05 = RecyclerView.A05(A0Z)) == null)) {
                j = A05.A07;
            }
            d61.Bbx(betterRecyclerView, A0Z, A02, j);
            return true;
        } else if (A0Z != null) {
            return true;
        } else {
            BetterRecyclerView betterRecyclerView2 = this.A00;
            View.OnClickListener onClickListener = null;
            if (onClickListener == null) {
                return true;
            }
            onClickListener.onClick(betterRecyclerView2);
            return true;
        }
    }
}
