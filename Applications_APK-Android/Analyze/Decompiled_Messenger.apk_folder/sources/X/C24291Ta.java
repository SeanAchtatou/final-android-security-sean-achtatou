package X;

import android.net.Uri;
import com.facebook.inject.ContextScoped;
import java.util.Set;

@ContextScoped
/* renamed from: X.1Ta  reason: invalid class name and case insensitive filesystem */
public final class C24291Ta {
    private static C04470Uu A03;
    public final AnonymousClass1HG A00;
    public final AnonymousClass1HL A01;
    public final C24301Tb A02;

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c6, code lost:
        if (r2.equals("/play") == false) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0176, code lost:
        if (r2.equals("/list") == false) goto L_0x00c8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(android.content.Context r12, android.net.Uri r13, com.facebook.messaging.games.interfaces.GamesStartConfig r14) {
        /*
            r11 = this;
            r6 = 0
            if (r13 == 0) goto L_0x0030
            java.lang.String r5 = r13.getAuthority()
            r2 = -1
            int r4 = r5.hashCode()
            r0 = -2004813920(0xffffffff8880f7a0, float:-7.761936E-34)
            r1 = 2
            r3 = 1
            if (r4 == r0) goto L_0x003b
            r0 = 3599307(0x36ebcb, float:5.043703E-39)
            if (r4 == r0) goto L_0x0031
            r0 = 655232739(0x270e0ee3, float:1.9714529E-15)
            if (r4 != r0) goto L_0x002a
            r0 = 9
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x002a
            r2 = 0
        L_0x002a:
            if (r2 == 0) goto L_0x00ad
            if (r2 == r3) goto L_0x0045
            if (r2 == r1) goto L_0x0045
        L_0x0030:
            return r6
        L_0x0031:
            java.lang.String r0 = "user"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x002a
            r2 = 1
            goto L_0x002a
        L_0x003b:
            java.lang.String r0 = "groupthreadfbid"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x002a
            r2 = 2
            goto L_0x002a
        L_0x0045:
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.lang.String r1 = "trigger"
            java.lang.String r0 = "games_list"
            r3.putString(r1, r0)
            X.3Ql r6 = new X.3Ql
            X.3Qo r2 = new X.3Qo
            java.lang.String r1 = "android.intent.action.VIEW"
            r0 = 0
            r2.<init>(r1, r0, r3)
            r6.<init>(r2)
            X.1HG r8 = r11.A00
            java.lang.String r1 = r13.toString()
            X.14b r0 = r8.A01
            X.0gA r7 = X.AnonymousClass1HG.A02
            r0.CH3(r7)
            X.14b r5 = r8.A01
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            java.lang.String r4 = X.C855543x.A00(r0)
            org.json.JSONObject r9 = new org.json.JSONObject
            r9.<init>()
            java.lang.String r0 = "intent_url"
            r9.put(r0, r1)     // Catch:{ JSONException -> 0x007e }
            goto L_0x009c
        L_0x007e:
            r10 = move-exception
            X.09P r3 = r8.A00
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            java.lang.String r2 = X.C855543x.A00(r0)
            java.lang.String r1 = "json exception while creating action tag"
            java.lang.String r0 = "messenger_games_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r2)
            X.06G r0 = X.AnonymousClass06F.A02(r0, r1)
            r0.A03 = r10
            X.06F r0 = r0.A00()
            r3.CGQ(r0)
        L_0x009c:
            java.lang.String r0 = r9.toString()
            r5.AOM(r7, r4, r0)
            X.14b r1 = r8.A01
            r1.AYS(r7)
            boolean r0 = r6.BHl(r13, r12)
            return r0
        L_0x00ad:
            java.lang.String r2 = r13.getPath()
            int r1 = r2.hashCode()
            r0 = 46727501(0x2c9014d, float:2.9535042E-37)
            if (r1 == r0) goto L_0x016f
            r0 = 46848995(0x2cadbe3, float:2.9807441E-37)
            if (r1 != r0) goto L_0x00c8
            java.lang.String r0 = "/play"
            boolean r0 = r2.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x00c9
        L_0x00c8:
            r1 = -1
        L_0x00c9:
            if (r1 == 0) goto L_0x00fc
            if (r1 != r3) goto L_0x017a
            X.1HL r3 = r11.A01
            X.1rH r0 = r3.A01
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 2306127928758965458(0x20010322004914d2, double:1.586038304577329E-154)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x017a
            X.Eh9 r1 = new X.Eh9
            r1.<init>()
            r1.A01(r14)
            r0 = 1
            r1.A0H = r0
            com.facebook.messaging.games.interfaces.GamesStartConfig r1 = r1.A00()
            X.1GC r0 = r3.A00
        L_0x00f7:
            r0.A02(r12, r1)
            r0 = 1
            return r0
        L_0x00fc:
            X.1Tb r5 = r11.A02
            com.facebook.messaging.model.threadkey.ThreadKey r7 = r14.A02
            java.lang.String r0 = "game_id"
            java.lang.String r4 = r13.getQueryParameter(r0)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r4)
            if (r0 != 0) goto L_0x017a
            java.lang.String r0 = "context_type"
            java.lang.String r8 = r13.getQueryParameter(r0)
            java.lang.String r0 = "context_id"
            java.lang.String r9 = r13.getQueryParameter(r0)
            java.lang.String r0 = "payload"
            java.lang.String r3 = r13.getQueryParameter(r0)
            java.lang.String r0 = "source"
            java.lang.String r2 = r13.getQueryParameter(r0)
            java.lang.String r0 = "metadata"
            java.lang.String r6 = r13.getQueryParameter(r0)
            com.facebook.graphql.enums.GraphQLInstantGameContextType r0 = com.facebook.graphql.enums.GraphQLInstantGameContextType.A07
            java.lang.Enum r1 = com.facebook.graphql.enums.EnumHelper.A00(r8, r0)
            com.facebook.graphql.enums.GraphQLInstantGameContextType r1 = (com.facebook.graphql.enums.GraphQLInstantGameContextType) r1
            com.facebook.graphql.enums.GraphQLInstantGameContextType r0 = com.facebook.graphql.enums.GraphQLInstantGameContextType.A04
            if (r1 != r0) goto L_0x0158
            r7 = 0
        L_0x0137:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            if (r0 == 0) goto L_0x013f
            java.lang.String r2 = r14.A09
        L_0x013f:
            X.Eh9 r0 = new X.Eh9
            r0.<init>()
            r0.A01(r14)
            r0.A06 = r4
            r0.A02 = r7
            r0.A05 = r3
            r0.A08 = r2
            r0.A0A = r6
            com.facebook.messaging.games.interfaces.GamesStartConfig r1 = r0.A00()
            X.1GC r0 = r5.A00
            goto L_0x00f7
        L_0x0158:
            com.facebook.graphql.enums.GraphQLInstantGameContextType r0 = com.facebook.graphql.enums.GraphQLInstantGameContextType.A07
            java.lang.Enum r1 = com.facebook.graphql.enums.EnumHelper.A00(r8, r0)
            com.facebook.graphql.enums.GraphQLInstantGameContextType r1 = (com.facebook.graphql.enums.GraphQLInstantGameContextType) r1
            com.facebook.graphql.enums.GraphQLInstantGameContextType r0 = com.facebook.graphql.enums.GraphQLInstantGameContextType.A06
            if (r1 != r0) goto L_0x0137
            if (r9 == 0) goto L_0x0137
            long r0 = java.lang.Long.parseLong(r9)
            com.facebook.messaging.model.threadkey.ThreadKey r7 = com.facebook.messaging.model.threadkey.ThreadKey.A00(r0)
            goto L_0x0137
        L_0x016f:
            java.lang.String r0 = "/list"
            boolean r0 = r2.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x00c9
            goto L_0x00c8
        L_0x017a:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24291Ta.A03(android.content.Context, android.net.Uri, com.facebook.messaging.games.interfaces.GamesStartConfig):boolean");
    }

    public static final C24291Ta A01(AnonymousClass1XY r4) {
        C24291Ta r0;
        synchronized (C24291Ta.class) {
            C04470Uu A002 = C04470Uu.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C24291Ta((AnonymousClass1XY) A03.A01());
                }
                C04470Uu r1 = A03;
                r0 = (C24291Ta) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static String A02(Uri uri) {
        if (uri == null || !uri.getPath().equals("/play")) {
            return null;
        }
        return uri.getQueryParameter("game_id");
    }

    private C24291Ta(AnonymousClass1XY r2) {
        this.A02 = C24301Tb.A00(r2);
        this.A01 = AnonymousClass1HL.A00(r2);
        this.A00 = new AnonymousClass1HG(r2);
    }

    public static Uri A00(Uri uri, String str) {
        if (!uri.getPath().equals("/play")) {
            return uri;
        }
        Set<String> queryParameterNames = uri.getQueryParameterNames();
        Uri.Builder clearQuery = uri.buildUpon().clearQuery();
        for (String next : queryParameterNames) {
            if (!next.equals("source")) {
                clearQuery.appendQueryParameter(next, uri.getQueryParameter(next));
            }
        }
        clearQuery.appendQueryParameter("source", str);
        return clearQuery.build();
    }
}
