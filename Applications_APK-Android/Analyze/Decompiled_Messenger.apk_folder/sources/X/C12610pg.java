package X;

import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.base.Strings;
import java.util.Comparator;

/* renamed from: X.0pg  reason: invalid class name and case insensitive filesystem */
public final class C12610pg implements Comparator {
    public int compare(Object obj, Object obj2) {
        ThreadSummary threadSummary = (ThreadSummary) obj;
        ThreadSummary threadSummary2 = (ThreadSummary) obj2;
        long j = threadSummary.A0B;
        long j2 = threadSummary2.A0B;
        if (j > j2) {
            return -1;
        }
        if (j2 > j) {
            return 1;
        }
        return Strings.nullToEmpty(threadSummary.A0S.toString()).compareTo(Strings.nullToEmpty(threadSummary2.A0S.toString()));
    }
}
