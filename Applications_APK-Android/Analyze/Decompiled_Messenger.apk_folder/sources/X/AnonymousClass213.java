package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.213  reason: invalid class name */
public final class AnonymousClass213 {
    public static final Set A00;

    static {
        String[] strArr = new String[37];
        String $const$string = AnonymousClass80H.$const$string(AnonymousClass1Y3.A3R);
        String $const$string2 = TurboLoader.Locator.$const$string(73);
        String $const$string3 = TurboLoader.Locator.$const$string(18);
        String $const$string4 = TurboLoader.Locator.$const$string(72);
        String $const$string5 = ECX.$const$string(35);
        System.arraycopy(new String[]{"com.facebook.adsmanager", "com.facebook.akira", $const$string, "com.facebook.appmanager.dev", "com.facebook.bishop", "com.facebook.creatorstudio", "com.facebook.daykira", $const$string2, "com.facebook.lite", "com.facebook.games", "com.facebook.phone", "com.facebook.home", "com.facebook.home.dev", $const$string3, "com.facebook.carbon", "com.instagram.lite", "com.facebook.katana", "com.facebook.kototoro", "com.facebook.lasso", "com.facebook.orca", "com.facebook.talk", "com.facebook.mk", $const$string4, "com.oculus.vrshell", $const$string5, "com.oculus.horizon.dev", "com.oculus.vrshell.home"}, 0, strArr, 0, 27);
        String $const$string6 = C99084oO.$const$string(48);
        String $const$string7 = C99084oO.$const$string(145);
        String $const$string8 = C99084oO.$const$string(437);
        System.arraycopy(new String[]{"com.oculus.twilight", $const$string6, "com.facebook.bonfire", "com.facebook.study", "com.instagram.threadsapp", "com.facebook.wakizashi", TurboLoader.Locator.$const$string(8), "com.facebook.work", $const$string7, $const$string8}, 0, strArr, 27, 10);
        A00 = Collections.unmodifiableSet(new HashSet(Arrays.asList(strArr)));
        Collections.unmodifiableSet(new HashSet(Arrays.asList("com.facebook.adsmanager", "com.facebook.lite", "com.facebook.phone", "com.facebook.home", "com.facebook.home.dev", "com.facebook.katana", "com.facebook.orca", "com.facebook.talk", "com.facebook.mk", $const$string4, $const$string6, "com.facebook.bonfire", "com.facebook.wakizashi", "com.facebook.work", $const$string7, $const$string8)));
        Collections.unmodifiableSet(new HashSet(Arrays.asList("com.facebook.globalsecurity", "com.facebook.viewpoints")));
        Collections.unmodifiableSet(new HashSet(Arrays.asList($const$string2, $const$string3, "com.facebook.carbon", "com.instagram.lite", "com.instagram.threadsapp")));
        Collections.unmodifiableSet(new HashSet(Arrays.asList("com.oculus.assistant", "com.oculus.vrshell.home", $const$string5, "com.oculus.horizon.dev", "com.oculus.ocms", "com.oculus.twilight")));
        Collections.unmodifiableSet(new HashSet(Arrays.asList("com.facebook.bishop")));
    }
}
