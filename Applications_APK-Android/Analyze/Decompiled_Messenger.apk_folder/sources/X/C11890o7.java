package X;

import com.facebook.location.foreground.ForegroundLocationFrameworkController;

/* renamed from: X.0o7  reason: invalid class name and case insensitive filesystem */
public final class C11890o7 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.location.foreground.ForegroundLocationFrameworkController$1";
    private int A00;
    public final /* synthetic */ ForegroundLocationFrameworkController A01;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Can't wrap try/catch for region: R(27:20|21|22|(2:24|(1:26)(23:27|29|30|31|(5:33|(1:35)(3:36|37|39)|40|(2:42|45)(1:46)|47)|48|49|(1:51)|53|(2:55|57)(1:56)|58|(1:60)|62|(2:64|66)(1:65)|67|(3:69|70|(1:72))|(1:77)(3:78|(1:80)|81)|82|(2:84|(1:87))|(1:89)(1:90)|91|92|93))|28|29|30|31|(0)|48|49|(0)|53|(0)(0)|58|(0)|62|(0)(0)|67|(0)|(0)(0)|82|(0)|(0)(0)|91|92|93) */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x06d8, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01e8, code lost:
        if (r6 == X.AnonymousClass07B.A00) goto L_0x01ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0227, code lost:
        if (r3 == X.AnonymousClass07B.A00) goto L_0x0229;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:48:0x01c8 */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x04ce A[SYNTHETIC, Splitter:B:137:0x04ce] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0159 A[Catch:{ IllegalStateException -> 0x01c8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01e5 A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01ed A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01ee A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0224 A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x022c A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x022d A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x025e A[SYNTHETIC, Splitter:B:69:0x025e] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0282 A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x028c A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0320 A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0336 A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0340 A[Catch:{ Exception -> 0x0276, all -> 0x05be }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x03a9 A[Catch:{ Exception -> 0x0713 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r25 = this;
            r3 = 2
            r15 = 15
            r0 = r25
            int r2 = X.AnonymousClass1Y3.B5j     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.0Ud r1 = (X.AnonymousClass0Ud) r1     // Catch:{ Exception -> 0x0713 }
            boolean r3 = r1.A0H()     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0Tq r1 = r1.A0B     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r2 = r1.get()     // Catch:{ Exception -> 0x0713 }
            r5 = 0
            r12 = 1
            r1 = 0
            if (r2 == 0) goto L_0x0023
            r1 = 1
        L_0x0023:
            r4 = 12
            if (r3 == 0) goto L_0x0701
            if (r1 == 0) goto L_0x0701
            X.1Y7 r2 = X.C134956Sw.A00     // Catch:{ Exception -> 0x0054 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0054 }
            X.0Tq r1 = r1.A0B     // Catch:{ Exception -> 0x0054 }
            java.lang.Object r1 = r1.get()     // Catch:{ Exception -> 0x0054 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0054 }
            X.063 r6 = r2.A09(r1)     // Catch:{ Exception -> 0x0054 }
            X.1Y7 r6 = (X.AnonymousClass1Y7) r6     // Catch:{ Exception -> 0x0054 }
            r3 = 17
            int r2 = X.AnonymousClass1Y3.B6q     // Catch:{ Exception -> 0x0054 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0054 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0054 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ Exception -> 0x0054 }
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1     // Catch:{ Exception -> 0x0054 }
            X.1hn r1 = r1.edit()     // Catch:{ Exception -> 0x0054 }
            r1.putBoolean(r6, r12)     // Catch:{ Exception -> 0x0054 }
            r1.commit()     // Catch:{ Exception -> 0x0054 }
            goto L_0x0068
        L_0x0054:
            r6 = move-exception
            int r2 = X.AnonymousClass1Y3.Amr     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r15, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.09P r3 = (X.AnonymousClass09P) r3     // Catch:{ Exception -> 0x0713 }
            java.lang.String r2 = "ForegroundLocationFrameworkController"
            java.lang.String r1 = "Something is wrong when set fresh install pref"
            r3.softReport(r2, r1, r6)     // Catch:{ Exception -> 0x0713 }
        L_0x0068:
            r3 = 16
            int r2 = X.AnonymousClass1Y3.B8F     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.4dB r1 = (X.C93674dB) r1     // Catch:{ Exception -> 0x0713 }
            X.1ZE r2 = r1.A00     // Catch:{ Exception -> 0x0713 }
            java.lang.String r1 = "geopixel_rtt"
            X.0bW r3 = r2.A01(r1)     // Catch:{ Exception -> 0x0713 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ Exception -> 0x0713 }
            r1 = 218(0xda, float:3.05E-43)
            r2.<init>(r3, r1)     // Catch:{ Exception -> 0x0713 }
            boolean r1 = r2.A0G()     // Catch:{ Exception -> 0x0713 }
            if (r1 == 0) goto L_0x008e
            r2.A06()     // Catch:{ Exception -> 0x0713 }
        L_0x008e:
            r3 = 7
            int r2 = X.AnonymousClass1Y3.AAJ     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.1nm r1 = (X.C33561nm) r1     // Catch:{ Exception -> 0x0713 }
            java.lang.Integer r2 = r1.A05()     // Catch:{ Exception -> 0x0713 }
            java.lang.Integer r1 = X.AnonymousClass07B.A0N     // Catch:{ Exception -> 0x0713 }
            r13 = 8
            if (r2 != r1) goto L_0x05c2
            r0.A00 = r5     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r3 = r0.A01     // Catch:{ Exception -> 0x0713 }
            long r1 = com.facebook.location.foreground.ForegroundLocationFrameworkController.A00(r3)     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController.A06(r3, r1)     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r5 = r0.A01     // Catch:{ Exception -> 0x0713 }
            monitor-enter(r5)     // Catch:{ Exception -> 0x0713 }
            r3 = 5
            int r2 = X.AnonymousClass1Y3.Asv     // Catch:{ all -> 0x05be }
            X.0UN r1 = r5.A05     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x05be }
            X.069 r1 = (X.AnonymousClass069) r1     // Catch:{ all -> 0x05be }
            long r1 = r1.now()     // Catch:{ all -> 0x05be }
            r5.A02 = r1     // Catch:{ all -> 0x05be }
            r3 = 9
            int r2 = X.AnonymousClass1Y3.BD4     // Catch:{ all -> 0x05be }
            X.0UN r1 = r5.A05     // Catch:{ all -> 0x05be }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x05be }
            X.2mN r4 = (X.C54412mN) r4     // Catch:{ all -> 0x05be }
            java.lang.String r14 = r5.A06     // Catch:{ all -> 0x05be }
            boolean r1 = com.facebook.location.foreground.ForegroundLocationFrameworkController.A07(r5)     // Catch:{ all -> 0x05be }
            if (r1 == 0) goto L_0x011c
            int r2 = X.AnonymousClass1Y3.Asv     // Catch:{ all -> 0x05be }
            X.0UN r1 = r5.A05     // Catch:{ all -> 0x05be }
            r3 = 5
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x05be }
            X.069 r1 = (X.AnonymousClass069) r1     // Catch:{ all -> 0x05be }
            long r9 = r1.now()     // Catch:{ all -> 0x05be }
            long r1 = r5.A01     // Catch:{ all -> 0x05be }
            long r9 = r9 - r1
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r1 = r5.A05     // Catch:{ all -> 0x05be }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ all -> 0x05be }
            X.4VN r8 = (X.AnonymousClass4VN) r8     // Catch:{ all -> 0x05be }
            r6 = 563443875381685(0x20073000b01b5, double:2.78378262185747E-309)
            r1 = 90000(0x15f90, double:4.4466E-319)
            X.1Yd r8 = r8.A04     // Catch:{ all -> 0x05be }
            long r6 = r8.At1(r6, r1)     // Catch:{ all -> 0x05be }
            r1 = 5000(0x1388, double:2.4703E-320)
            long r6 = r6 - r1
            int r1 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x011a
            int r2 = X.AnonymousClass1Y3.Asv     // Catch:{ all -> 0x05be }
            X.0UN r1 = r5.A05     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x05be }
            X.069 r1 = (X.AnonymousClass069) r1     // Catch:{ all -> 0x05be }
            long r1 = r1.now()     // Catch:{ all -> 0x05be }
            r5.A01 = r1     // Catch:{ all -> 0x05be }
            goto L_0x011c
        L_0x011a:
            r3 = 1
            goto L_0x011d
        L_0x011c:
            r3 = 0
        L_0x011d:
            r4.A03()     // Catch:{ all -> 0x05be }
            r4.A03 = r3     // Catch:{ all -> 0x05be }
            int r6 = X.AnonymousClass1Y3.BM3     // Catch:{ all -> 0x05be }
            X.0UN r2 = r4.A02     // Catch:{ all -> 0x05be }
            r1 = 3
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r6, r2)     // Catch:{ all -> 0x05be }
            X.2ew r7 = (X.C50882ew) r7     // Catch:{ all -> 0x05be }
            X.A52 r9 = X.C54412mN.A00(r4)     // Catch:{ all -> 0x05be }
            X.A4Q r11 = X.C54412mN.A02(r4)     // Catch:{ all -> 0x05be }
            java.lang.String r6 = "fgl_scan_start"
            X.C50882ew.A05(r7, r12)     // Catch:{ IllegalStateException -> 0x01c8 }
            r1 = 0
            X.C50882ew.A03(r7, r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            X.C50882ew.A04(r7, r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            X.C50882ew.A06(r7, r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            int r1 = r7.A01     // Catch:{ IllegalStateException -> 0x01c8 }
            int r1 = r1 + r12
            r7.A01 = r1     // Catch:{ IllegalStateException -> 0x01c8 }
            X.069 r1 = r7.A0C     // Catch:{ IllegalStateException -> 0x01c8 }
            long r1 = r1.now()     // Catch:{ IllegalStateException -> 0x01c8 }
            r7.A07 = r1     // Catch:{ IllegalStateException -> 0x01c8 }
            r7.A08 = r1     // Catch:{ IllegalStateException -> 0x01c8 }
            X.1La r6 = X.C50882ew.A00(r7, r6)     // Catch:{ IllegalStateException -> 0x01c8 }
            if (r6 == 0) goto L_0x01c8
            java.lang.String r8 = "location_manager_params"
            if (r9 != 0) goto L_0x015f
            r7 = 0
            goto L_0x0198
        L_0x015f:
            com.fasterxml.jackson.databind.node.JsonNodeFactory r1 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance     // Catch:{ IllegalStateException -> 0x01c8 }
            com.fasterxml.jackson.databind.node.ObjectNode r7 = new com.fasterxml.jackson.databind.node.ObjectNode     // Catch:{ IllegalStateException -> 0x01c8 }
            r7.<init>(r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.Integer r1 = r9.A03     // Catch:{ IllegalStateException -> 0x01c8 }
            int r1 = r1.intValue()     // Catch:{ IllegalStateException -> 0x01c8 }
            switch(r1) {
                case 1: goto L_0x01a3;
                case 2: goto L_0x01a0;
                default: goto L_0x016f;
            }     // Catch:{ IllegalStateException -> 0x01c8 }
        L_0x016f:
            java.lang.String r2 = "LOW_POWER"
        L_0x0171:
            java.lang.String r1 = "priority"
            r7.put(r1, r2)     // Catch:{ IllegalStateException -> 0x01c8 }
            long r1 = r9.A01     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r10 = "desired_age_ms"
            r7.put(r10, r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            float r2 = r9.A00     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r1 = "desired_accuracy_meters"
            r7.put(r1, r2)     // Catch:{ IllegalStateException -> 0x01c8 }
            long r1 = r9.A02     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r10 = "timeout_ms"
            r7.put(r10, r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.Long r2 = r9.A04     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r1 = "age_limit_ms"
            r7.put(r1, r2)     // Catch:{ IllegalStateException -> 0x01c8 }
            r2 = 0
            java.lang.String r1 = "accuracy_limit_meters"
            r7.put(r1, r2)     // Catch:{ IllegalStateException -> 0x01c8 }
        L_0x0198:
            r6.A04(r8, r7)     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r9 = "wifi_scan_params"
            if (r11 != 0) goto L_0x01a8
            goto L_0x01a6
        L_0x01a0:
            java.lang.String r2 = "HIGH_ACCURACY"
            goto L_0x0171
        L_0x01a3:
            java.lang.String r2 = "BALANCED_POWER_AND_ACCURACY"
            goto L_0x0171
        L_0x01a6:
            r8 = 0
            goto L_0x01bd
        L_0x01a8:
            com.fasterxml.jackson.databind.node.JsonNodeFactory r1 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance     // Catch:{ IllegalStateException -> 0x01c8 }
            com.fasterxml.jackson.databind.node.ObjectNode r8 = new com.fasterxml.jackson.databind.node.ObjectNode     // Catch:{ IllegalStateException -> 0x01c8 }
            r8.<init>(r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            long r1 = r11.A01     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r7 = "timeout_ms"
            r8.put(r7, r1)     // Catch:{ IllegalStateException -> 0x01c8 }
            long r1 = r11.A00     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r7 = "age_limit_ms"
            r8.put(r7, r1)     // Catch:{ IllegalStateException -> 0x01c8 }
        L_0x01bd:
            r6.A04(r9, r8)     // Catch:{ IllegalStateException -> 0x01c8 }
            java.lang.String r1 = "is_high_freq"
            r6.A07(r1, r3)     // Catch:{ IllegalStateException -> 0x01c8 }
            r6.A0A()     // Catch:{ IllegalStateException -> 0x01c8 }
        L_0x01c8:
            X.A52 r3 = X.C54412mN.A00(r4)     // Catch:{ all -> 0x05be }
            int r6 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r2 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r12, r6, r2)     // Catch:{ all -> 0x05be }
            X.4VN r7 = (X.AnonymousClass4VN) r7     // Catch:{ all -> 0x05be }
            r1 = 844918852223056(0x30073000d0050, double:4.17445378407029E-309)
            java.lang.Integer r6 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x05be }
            java.lang.Integer r6 = X.AnonymousClass4VN.A01(r7, r1, r6)     // Catch:{ all -> 0x05be }
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x05be }
            if (r6 == r1) goto L_0x01ea
            java.lang.Integer r2 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x05be }
            r1 = 0
            if (r6 != r2) goto L_0x01eb
        L_0x01ea:
            r1 = 1
        L_0x01eb:
            if (r1 != 0) goto L_0x01ee
            goto L_0x01fe
        L_0x01ee:
            X.0Tq r1 = r4.A07     // Catch:{ all -> 0x05be }
            java.lang.Object r6 = r1.get()     // Catch:{ all -> 0x05be }
            X.EDv r6 = (X.C28950EDv) r6     // Catch:{ all -> 0x05be }
            com.facebook.common.callercontext.CallerContext r1 = X.C54412mN.A09     // Catch:{ all -> 0x05be }
            java.lang.String r1 = r1.A02     // Catch:{ all -> 0x05be }
            r6.A05(r3, r1)     // Catch:{ all -> 0x05be }
            goto L_0x0207
        L_0x01fe:
            X.7mr r1 = new X.7mr     // Catch:{ all -> 0x05be }
            r1.<init>()     // Catch:{ all -> 0x05be }
            com.google.common.util.concurrent.ListenableFuture r6 = X.C05350Yp.A04(r1)     // Catch:{ all -> 0x05be }
        L_0x0207:
            X.A4Q r7 = X.C54412mN.A02(r4)     // Catch:{ all -> 0x05be }
            int r3 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r2 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r12, r3, r2)     // Catch:{ all -> 0x05be }
            X.4VN r8 = (X.AnonymousClass4VN) r8     // Catch:{ all -> 0x05be }
            r1 = 844918852419667(0x3007300100053, double:4.174453785041677E-309)
            java.lang.Integer r3 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x05be }
            java.lang.Integer r3 = X.AnonymousClass4VN.A01(r8, r1, r3)     // Catch:{ all -> 0x05be }
            java.lang.Integer r1 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x05be }
            if (r3 == r1) goto L_0x0229
            java.lang.Integer r2 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x05be }
            r1 = 0
            if (r3 != r2) goto L_0x022a
        L_0x0229:
            r1 = 1
        L_0x022a:
            if (r1 != 0) goto L_0x022d
            goto L_0x023b
        L_0x022d:
            X.0Tq r1 = r4.A08     // Catch:{ all -> 0x05be }
            java.lang.Object r3 = r1.get()     // Catch:{ all -> 0x05be }
            X.A4L r3 = (X.A4L) r3     // Catch:{ all -> 0x05be }
            java.lang.String r1 = "ForegroundLocationFrameworkSignalReader"
            r3.A05(r7, r1)     // Catch:{ all -> 0x05be }
            goto L_0x0244
        L_0x023b:
            X.7mr r1 = new X.7mr     // Catch:{ all -> 0x05be }
            r1.<init>()     // Catch:{ all -> 0x05be }
            com.google.common.util.concurrent.ListenableFuture r3 = X.C05350Yp.A04(r1)     // Catch:{ all -> 0x05be }
        L_0x0244:
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r1 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r12, r2, r1)     // Catch:{ all -> 0x05be }
            X.4VN r8 = (X.AnonymousClass4VN) r8     // Catch:{ all -> 0x05be }
            X.1Yd r10 = r8.A04     // Catch:{ all -> 0x05be }
            r1 = 2306124625933107384(0x20010021008600b8, double:1.5849443529535822E-154)
            X.0XE r9 = X.AnonymousClass0XE.A07     // Catch:{ all -> 0x05be }
            boolean r1 = r10.Aer(r1, r9)     // Catch:{ all -> 0x05be }
            r10 = 0
            if (r1 == 0) goto L_0x0280
            X.EEl r2 = r8.A02     // Catch:{ Exception -> 0x0276 }
            android.content.Context r1 = r8.A00     // Catch:{ Exception -> 0x0276 }
            X.EEr r2 = r2.Al5(r1)     // Catch:{ Exception -> 0x0276 }
            X.EEr r1 = X.C28967EEr.A04     // Catch:{ Exception -> 0x0276 }
            if (r2 != r1) goto L_0x0280
            X.1Yd r9 = r8.A04     // Catch:{ Exception -> 0x0276 }
            r1 = 2306124625932976310(0x20010021008400b6, double:1.5849443529101683E-154)
            boolean r10 = r9.Aem(r1)     // Catch:{ Exception -> 0x0276 }
            goto L_0x0280
        L_0x0276:
            r9 = move-exception
            X.09P r8 = r8.A03     // Catch:{ all -> 0x05be }
            java.lang.String r2 = "ForegroundLocationFrameworkConfig"
            java.lang.String r1 = "Exception getting ble scanner state"
            r8.softReport(r2, r1, r9)     // Catch:{ all -> 0x05be }
        L_0x0280:
            if (r10 != 0) goto L_0x028c
            X.7mr r1 = new X.7mr     // Catch:{ all -> 0x05be }
            r1.<init>()     // Catch:{ all -> 0x05be }
            com.google.common.util.concurrent.ListenableFuture r7 = X.C05350Yp.A04(r1)     // Catch:{ all -> 0x05be }
            goto L_0x0308
        L_0x028c:
            X.A53 r1 = r4.A00     // Catch:{ all -> 0x05be }
            if (r1 != 0) goto L_0x02d8
            X.A53 r10 = new X.A53     // Catch:{ all -> 0x05be }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r1 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r12, r2, r1)     // Catch:{ all -> 0x05be }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x05be }
            X.1Yd r8 = r1.A04     // Catch:{ all -> 0x05be }
            r1 = 563091693895804(0x200210064007c, double:2.78204261412472E-309)
            long r8 = r8.At0(r1)     // Catch:{ all -> 0x05be }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r1 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r12, r2, r1)     // Catch:{ all -> 0x05be }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x05be }
            X.1Yd r11 = r1.A04     // Catch:{ all -> 0x05be }
            r1 = 563091693961341(0x200210065007d, double:2.782042614448515E-309)
            long r1 = r11.At0(r1)     // Catch:{ all -> 0x05be }
            int r11 = (int) r1     // Catch:{ all -> 0x05be }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r1 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r12, r2, r1)     // Catch:{ all -> 0x05be }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x05be }
            X.1Yd r7 = r1.A04     // Catch:{ all -> 0x05be }
            r1 = 563091696189583(0x200210087008f, double:2.782042625457493E-309)
            long r1 = r7.At0(r1)     // Catch:{ all -> 0x05be }
            int r7 = (int) r1     // Catch:{ all -> 0x05be }
            r10.<init>(r8, r11, r7)     // Catch:{ all -> 0x05be }
            r4.A00 = r10     // Catch:{ all -> 0x05be }
        L_0x02d8:
            X.0Tq r1 = r4.A06     // Catch:{ all -> 0x05be }
            java.lang.Object r7 = r1.get()     // Catch:{ all -> 0x05be }
            com.facebook.blescan.BleScanOperation r7 = (com.facebook.blescan.BleScanOperation) r7     // Catch:{ all -> 0x05be }
            r8 = 11
            int r2 = X.AnonymousClass1Y3.BBa     // Catch:{ all -> 0x05be }
            X.0UN r1 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r2, r1)     // Catch:{ all -> 0x05be }
            X.069 r1 = (X.AnonymousClass069) r1     // Catch:{ all -> 0x05be }
            r1.now()     // Catch:{ all -> 0x05be }
            X.2MJ r9 = new X.2MJ     // Catch:{ all -> 0x05be }
            r9.<init>(r4)     // Catch:{ all -> 0x05be }
            r8 = 9
            int r2 = X.AnonymousClass1Y3.Ax6     // Catch:{ all -> 0x05be }
            X.0UN r1 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r8, r2, r1)     // Catch:{ all -> 0x05be }
            X.0VK r1 = (X.AnonymousClass0VK) r1     // Catch:{ all -> 0x05be }
            X.C05350Yp.A08(r7, r9, r1)     // Catch:{ all -> 0x05be }
            X.A53 r1 = r4.A00     // Catch:{ all -> 0x05be }
            r7.A05(r1)     // Catch:{ all -> 0x05be }
        L_0x0308:
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            X.0UN r1 = r4.A02     // Catch:{ all -> 0x05be }
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r12, r2, r1)     // Catch:{ all -> 0x05be }
            X.4VN r9 = (X.AnonymousClass4VN) r9     // Catch:{ all -> 0x05be }
            X.1Yd r10 = r9.A04     // Catch:{ all -> 0x05be }
            r1 = 281968898343825(0x1007300060391, double:1.393111458673867E-309)
            boolean r1 = r10.Aem(r1)     // Catch:{ all -> 0x05be }
            r11 = 0
            if (r1 == 0) goto L_0x0334
            X.1Yd r10 = r9.A04     // Catch:{ all -> 0x05be }
            r1 = 2306124978111644558(0x200100730000038e, double:1.5850610004243074E-154)
            boolean r2 = r10.Aem(r1)     // Catch:{ all -> 0x05be }
            boolean r1 = r9.A03()     // Catch:{ all -> 0x05be }
            if (r2 == 0) goto L_0x0333
            if (r1 == 0) goto L_0x0334
        L_0x0333:
            r11 = 1
        L_0x0334:
            if (r11 != 0) goto L_0x0340
            X.7mr r1 = new X.7mr     // Catch:{ all -> 0x05be }
            r1.<init>()     // Catch:{ all -> 0x05be }
            com.google.common.util.concurrent.ListenableFuture r1 = X.C05350Yp.A04(r1)     // Catch:{ all -> 0x05be }
            goto L_0x035d
        L_0x0340:
            X.0UN r2 = r4.A02     // Catch:{ all -> 0x05be }
            int r1 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x05be }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r12, r1, r2)     // Catch:{ all -> 0x05be }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x05be }
            X.1Yd r8 = r1.A04     // Catch:{ all -> 0x05be }
            r1 = 563443874726319(0x20073000101af, double:2.78378261861953E-309)
            r8.At0(r1)     // Catch:{ all -> 0x05be }
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x05be }
            r1.<init>()     // Catch:{ all -> 0x05be }
            com.google.common.util.concurrent.ListenableFuture r1 = X.C05350Yp.A04(r1)     // Catch:{ all -> 0x05be }
        L_0x035d:
            com.google.common.util.concurrent.ListenableFuture[] r1 = new com.google.common.util.concurrent.ListenableFuture[]{r6, r3, r7, r1}     // Catch:{ all -> 0x05be }
            java.util.ArrayList r2 = X.C04300To.A04(r1)     // Catch:{ all -> 0x05be }
            X.Ddi r1 = new X.Ddi     // Catch:{ all -> 0x05be }
            r1.<init>()     // Catch:{ all -> 0x05be }
            java.lang.Iterable r1 = X.AnonymousClass0j4.A00(r2, r1)     // Catch:{ all -> 0x05be }
            com.google.common.util.concurrent.ListenableFuture r7 = X.C05350Yp.A01(r1)     // Catch:{ all -> 0x05be }
            X.1EL r1 = new X.1EL     // Catch:{ all -> 0x05be }
            r1.<init>(r4, r14)     // Catch:{ all -> 0x05be }
            X.1G0 r1 = X.AnonymousClass1G0.A00(r7, r1)     // Catch:{ all -> 0x05be }
            r4.A01 = r1     // Catch:{ all -> 0x05be }
            X.0Yh r6 = r1.A00     // Catch:{ all -> 0x05be }
            int r3 = X.AnonymousClass1Y3.Ax6     // Catch:{ all -> 0x05be }
            X.0UN r2 = r4.A02     // Catch:{ all -> 0x05be }
            r1 = 9
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)     // Catch:{ all -> 0x05be }
            X.0VK r1 = (X.AnonymousClass0VK) r1     // Catch:{ all -> 0x05be }
            X.C05350Yp.A08(r7, r6, r1)     // Catch:{ all -> 0x05be }
            monitor-exit(r5)     // Catch:{ Exception -> 0x0713 }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.4VN r3 = (X.AnonymousClass4VN) r3     // Catch:{ Exception -> 0x0713 }
            r1 = 281968899261332(0x1007300140394, double:1.393111463206954E-309)
            r4 = 0
            X.1Yd r3 = r3.A04     // Catch:{ Exception -> 0x0713 }
            boolean r1 = r3.Aeo(r1, r4)     // Catch:{ Exception -> 0x0713 }
            if (r1 == 0) goto L_0x04cb
            com.facebook.location.foreground.ForegroundLocationFrameworkController r4 = r0.A01     // Catch:{ Exception -> 0x0713 }
            boolean r1 = r4.A07     // Catch:{ Exception -> 0x0713 }
            if (r1 != 0) goto L_0x04cb
            r3 = 9
            int r2 = X.AnonymousClass1Y3.BD4     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r4.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.2mN r6 = (X.C54412mN) r6     // Catch:{ Exception -> 0x0713 }
            java.lang.String r8 = r4.A06     // Catch:{ Exception -> 0x0713 }
            int r3 = X.AnonymousClass1Y3.BPU     // Catch:{ Exception -> 0x0713 }
            X.0UN r2 = r6.A02     // Catch:{ Exception -> 0x0713 }
            r1 = 7
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)     // Catch:{ Exception -> 0x0713 }
            X.25O r1 = (X.AnonymousClass25O) r1     // Catch:{ Exception -> 0x0713 }
            X.8pw r7 = r1.A01()     // Catch:{ Exception -> 0x0713 }
            if (r7 == 0) goto L_0x04c7
            int r2 = X.AnonymousClass1Y3.Abl     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r6.A02     // Catch:{ Exception -> 0x0713 }
            r5 = 12
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.2pA r4 = (X.C56042pA) r4     // Catch:{ Exception -> 0x0713 }
            boolean r1 = X.C56052pB.A01()     // Catch:{ Exception -> 0x0713 }
            r9 = 0
            if (r1 == 0) goto L_0x0470
            java.util.List r3 = r4.A03(r12)     // Catch:{ SecurityException -> 0x0470 }
            X.2i6 r9 = r4.A06     // Catch:{ Exception -> 0x0713 }
            long r1 = r9.A00     // Catch:{ Exception -> 0x0713 }
            r9.A00(r3, r1)     // Catch:{ Exception -> 0x0713 }
            X.069 r1 = r4.A03     // Catch:{ Exception -> 0x0713 }
            long r23 = r1.now()     // Catch:{ Exception -> 0x0713 }
            r17 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r19 = -1
            r21 = -1
            r16 = r3
            java.util.List r9 = X.C180888aM.A01(r16, r17, r19, r21, r23)     // Catch:{ Exception -> 0x0713 }
            X.2pE r1 = r4.A07     // Catch:{ Exception -> 0x0713 }
            if (r1 == 0) goto L_0x0468
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0713 }
            r1 = 17
            if (r2 < r1) goto L_0x042e
            if (r3 == 0) goto L_0x042e
            boolean r1 = r3.isEmpty()     // Catch:{ Exception -> 0x0713 }
            if (r1 != 0) goto L_0x042e
            r20 = -9223372036854775808
            java.util.Iterator r11 = r3.iterator()     // Catch:{ Exception -> 0x0713 }
        L_0x0419:
            boolean r1 = r11.hasNext()     // Catch:{ Exception -> 0x0713 }
            if (r1 == 0) goto L_0x0430
            java.lang.Object r1 = r11.next()     // Catch:{ Exception -> 0x0713 }
            android.net.wifi.ScanResult r1 = (android.net.wifi.ScanResult) r1     // Catch:{ Exception -> 0x0713 }
            long r1 = r1.timestamp     // Catch:{ Exception -> 0x0713 }
            int r10 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r10 <= 0) goto L_0x0419
            r20 = r1
            goto L_0x0419
        L_0x042e:
            r20 = -1
        L_0x0430:
            r16 = 0
            r11 = -1
            int r1 = (r20 > r16 ? 1 : (r20 == r16 ? 0 : -1))
            if (r1 < 0) goto L_0x0452
            X.069 r1 = r4.A03     // Catch:{ Exception -> 0x0713 }
            long r18 = r1.now()     // Catch:{ Exception -> 0x0713 }
            r16 = 1000(0x3e8, double:4.94E-321)
            long r20 = r20 / r16
            long r18 = r18 - r20
            long r1 = r18 / r16
            int r10 = (int) r1     // Catch:{ Exception -> 0x0713 }
            X.06B r1 = r4.A02     // Catch:{ Exception -> 0x0713 }
            long r1 = r1.now()     // Catch:{ Exception -> 0x0713 }
            long r1 = r1 - r18
            long r1 = r1 / r16
            int r11 = (int) r1     // Catch:{ Exception -> 0x0713 }
            goto L_0x0453
        L_0x0452:
            r10 = -1
        L_0x0453:
            r2 = 0
            if (r3 != 0) goto L_0x045a
            r3 = 0
        L_0x0457:
            if (r9 == 0) goto L_0x0463
            goto L_0x045f
        L_0x045a:
            int r3 = r3.size()     // Catch:{ Exception -> 0x0713 }
            goto L_0x0457
        L_0x045f:
            int r2 = r9.size()     // Catch:{ Exception -> 0x0713 }
        L_0x0463:
            X.2pE r1 = r4.A07     // Catch:{ Exception -> 0x0713 }
            r1.A00(r10, r11, r3, r2)     // Catch:{ Exception -> 0x0713 }
        L_0x0468:
            X.06B r2 = r4.A02     // Catch:{ Exception -> 0x0713 }
            X.069 r1 = r4.A03     // Catch:{ Exception -> 0x0713 }
            java.util.List r9 = X.A4I.A01(r9, r2, r1)     // Catch:{ Exception -> 0x0713 }
        L_0x0470:
            int r2 = X.AnonymousClass1Y3.Abl     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r6.A02     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.2pA r1 = (X.C56042pA) r1     // Catch:{ Exception -> 0x0713 }
            X.A4I r3 = r1.A02()     // Catch:{ Exception -> 0x0713 }
            X.2ey r2 = new X.2ey     // Catch:{ Exception -> 0x0713 }
            r2.<init>()     // Catch:{ Exception -> 0x0713 }
            r2.A01 = r7     // Catch:{ Exception -> 0x0713 }
            java.lang.String r1 = "FG_LOCATION_CACHE"
            r2.A08 = r1     // Catch:{ Exception -> 0x0713 }
            r2.A0D = r9     // Catch:{ Exception -> 0x0713 }
            r2.A02 = r3     // Catch:{ Exception -> 0x0713 }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x0713 }
            r2.A04 = r1     // Catch:{ Exception -> 0x0713 }
            X.2mO r5 = new X.2mO     // Catch:{ Exception -> 0x0713 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0713 }
            int r2 = X.AnonymousClass1Y3.B3D     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r6.A02     // Catch:{ Exception -> 0x0713 }
            r4 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.2ev r1 = (X.C50872ev) r1     // Catch:{ Exception -> 0x0713 }
            r3 = 0
            r1.A02(r3)     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r6.A02     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.2ev r1 = (X.C50872ev) r1     // Catch:{ Exception -> 0x0713 }
            com.google.common.util.concurrent.ListenableFuture r5 = r1.A01(r5, r8, r3, r3)     // Catch:{ Exception -> 0x0713 }
            X.3wh r4 = new X.3wh     // Catch:{ Exception -> 0x0713 }
            r4.<init>()     // Catch:{ Exception -> 0x0713 }
            r3 = 9
            int r2 = X.AnonymousClass1Y3.Ax6     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r6.A02     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.0VK r1 = (X.AnonymousClass0VK) r1     // Catch:{ Exception -> 0x0713 }
            X.C05350Yp.A08(r5, r4, r1)     // Catch:{ Exception -> 0x0713 }
        L_0x04c7:
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            r1.A07 = r12     // Catch:{ Exception -> 0x0713 }
        L_0x04cb:
            com.facebook.location.foreground.ForegroundLocationFrameworkController r3 = r0.A01     // Catch:{ Exception -> 0x0713 }
            monitor-enter(r3)     // Catch:{ Exception -> 0x0713 }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x06d8 }
            X.0UN r1 = r3.A05     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ all -> 0x06d8 }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x06d8 }
            X.1Yd r5 = r1.A04     // Catch:{ all -> 0x06d8 }
            r1 = 281968899326869(0x1007300150395, double:1.39311146353075E-309)
            boolean r1 = r5.Aem(r1)     // Catch:{ all -> 0x06d8 }
            if (r1 == 0) goto L_0x059b
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x06d8 }
            X.0UN r1 = r3.A05     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ all -> 0x06d8 }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x06d8 }
            X.1Yd r5 = r1.A04     // Catch:{ all -> 0x06d8 }
            r1 = 563443876233659(0x20073001801bb, double:2.78378262606678E-309)
            long r16 = r5.At0(r1)     // Catch:{ all -> 0x06d8 }
            monitor-enter(r3)     // Catch:{ all -> 0x06d8 }
            r5 = 17
            int r2 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x0598 }
            X.0UN r1 = r3.A05     // Catch:{ all -> 0x0598 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ all -> 0x0598 }
            com.facebook.prefs.shared.FbSharedPreferences r6 = (com.facebook.prefs.shared.FbSharedPreferences) r6     // Catch:{ all -> 0x0598 }
            X.1Y7 r5 = X.AnonymousClass4Ur.A01     // Catch:{ all -> 0x0598 }
            r1 = 0
            long r9 = r6.At2(r5, r1)     // Catch:{ all -> 0x0598 }
            r5 = 6
            int r2 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x0598 }
            X.0UN r1 = r3.A05     // Catch:{ all -> 0x0598 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)     // Catch:{ all -> 0x0598 }
            X.06B r1 = (X.AnonymousClass06B) r1     // Catch:{ all -> 0x0598 }
            long r5 = r1.now()     // Catch:{ all -> 0x0598 }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r7
            long r5 = r5 - r9
            monitor-exit(r3)     // Catch:{ all -> 0x06d8 }
            int r1 = (r16 > r5 ? 1 : (r16 == r5 ? 0 : -1))
            if (r1 >= 0) goto L_0x059b
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x06d8 }
            X.0UN r1 = r3.A05     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ all -> 0x06d8 }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x06d8 }
            X.1Yd r5 = r1.A04     // Catch:{ all -> 0x06d8 }
            r1 = 563443876102585(0x20073001601b9, double:2.78378262541919E-309)
            long r17 = r5.At0(r1)     // Catch:{ all -> 0x06d8 }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ all -> 0x06d8 }
            X.0UN r1 = r3.A05     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ all -> 0x06d8 }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ all -> 0x06d8 }
            X.1Yd r4 = r1.A04     // Catch:{ all -> 0x06d8 }
            r1 = 563443876168122(0x20073001701ba, double:2.783782625742985E-309)
            long r19 = r4.At0(r1)     // Catch:{ all -> 0x06d8 }
            r4 = 9
            int r2 = X.AnonymousClass1Y3.BD4     // Catch:{ all -> 0x06d8 }
            X.0UN r1 = r3.A05     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ all -> 0x06d8 }
            X.2mN r1 = (X.C54412mN) r1     // Catch:{ all -> 0x06d8 }
            int r4 = X.AnonymousClass1Y3.AHw     // Catch:{ all -> 0x06d8 }
            X.0UN r2 = r1.A02     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r4, r2)     // Catch:{ all -> 0x06d8 }
            X.2ex r1 = (X.C50892ex) r1     // Catch:{ all -> 0x06d8 }
            r21 = 0
            r16 = r1
            boolean r1 = r16.A02(r17, r19, r21)     // Catch:{ all -> 0x06d8 }
            if (r1 == 0) goto L_0x059b
            r2 = 17
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x06d8 }
            X.0UN r6 = r3.A05     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r6)     // Catch:{ all -> 0x06d8 }
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1     // Catch:{ all -> 0x06d8 }
            X.1hn r5 = r1.edit()     // Catch:{ all -> 0x06d8 }
            X.1Y7 r4 = X.AnonymousClass4Ur.A01     // Catch:{ all -> 0x06d8 }
            r2 = 6
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x06d8 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r6)     // Catch:{ all -> 0x06d8 }
            X.06B r1 = (X.AnonymousClass06B) r1     // Catch:{ all -> 0x06d8 }
            long r1 = r1.now()     // Catch:{ all -> 0x06d8 }
            long r1 = r1 / r7
            r5.BzA(r4, r1)     // Catch:{ all -> 0x06d8 }
            r5.commit()     // Catch:{ all -> 0x06d8 }
            goto L_0x059b
        L_0x0598:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x06d8 }
            throw r1     // Catch:{ all -> 0x06d8 }
        L_0x059b:
            monitor-exit(r3)     // Catch:{ Exception -> 0x0713 }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ Exception -> 0x0713 }
            X.1Yd r3 = r1.A04     // Catch:{ Exception -> 0x0713 }
            r1 = 281968899589014(0x1007300190396, double:1.39311146482592E-309)
            boolean r1 = r3.Aem(r1)     // Catch:{ Exception -> 0x0713 }
            if (r1 == 0) goto L_0x0737
            com.facebook.location.foreground.ForegroundLocationFrameworkController r2 = r0.A01     // Catch:{ Exception -> 0x0713 }
            boolean r1 = r2.A08     // Catch:{ Exception -> 0x0713 }
            if (r1 != 0) goto L_0x0737
            r2.A08 = r12     // Catch:{ Exception -> 0x0713 }
            return
        L_0x05be:
            r1 = move-exception
            monitor-exit(r5)     // Catch:{ Exception -> 0x0713 }
            goto L_0x06da
        L_0x05c2:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C     // Catch:{ Exception -> 0x0713 }
            if (r2 != r1) goto L_0x06db
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ Exception -> 0x0713 }
            X.1Yd r3 = r1.A04     // Catch:{ Exception -> 0x0713 }
            r1 = 285426346628779(0x10398000016ab, double:1.41019352287255E-309)
            boolean r1 = r3.Aem(r1)     // Catch:{ Exception -> 0x0713 }
            if (r1 == 0) goto L_0x06db
            r0.A00 = r5     // Catch:{ Exception -> 0x0713 }
            int r2 = X.AnonymousClass1Y3.BGZ     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r13, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.4VN r1 = (X.AnonymousClass4VN) r1     // Catch:{ Exception -> 0x0713 }
            X.1Yd r3 = r1.A04     // Catch:{ Exception -> 0x0713 }
            r1 = 566901323400985(0x2039800010719, double:2.80086468474365E-309)
            long r4 = r3.At0(r1)     // Catch:{ Exception -> 0x0713 }
            r2 = 0
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x0607
            com.facebook.location.foreground.ForegroundLocationFrameworkController r3 = r0.A01     // Catch:{ Exception -> 0x0713 }
            long r1 = com.facebook.location.foreground.ForegroundLocationFrameworkController.A00(r3)     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController.A06(r3, r1)     // Catch:{ Exception -> 0x0713 }
        L_0x0607:
            r3 = 10
            int r2 = X.AnonymousClass1Y3.ACX     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.2f1 r5 = (X.C50932f1) r5     // Catch:{ Exception -> 0x0713 }
            java.lang.String r11 = "ForegroundLocationIpValidationHelper"
            X.A54 r2 = new X.A54     // Catch:{ Exception -> 0x0713 }
            r2.<init>()     // Catch:{ Exception -> 0x0713 }
            r2.A01 = r12     // Catch:{ Exception -> 0x0713 }
            r1 = 0
            r2.A03 = r1     // Catch:{ Exception -> 0x0713 }
            r2.A00 = r1     // Catch:{ Exception -> 0x0713 }
            r2.A02 = r1     // Catch:{ Exception -> 0x0713 }
            X.0Tq r1 = r5.A03     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r4 = r1.get()     // Catch:{ Exception -> 0x0713 }
            X.A4u r4 = (X.C21271A4u) r4     // Catch:{ Exception -> 0x0713 }
            X.A4x r10 = new X.A4x     // Catch:{ Exception -> 0x0713 }
            r10.<init>(r2)     // Catch:{ Exception -> 0x0713 }
            monitor-enter(r4)     // Catch:{ Exception -> 0x0713 }
            X.A50 r8 = new X.A50     // Catch:{ all -> 0x06d5 }
            r8.<init>()     // Catch:{ all -> 0x06d5 }
            X.A50 r12 = new X.A50     // Catch:{ all -> 0x06d5 }
            r12.<init>()     // Catch:{ all -> 0x06d5 }
            X.A50 r13 = new X.A50     // Catch:{ all -> 0x06d5 }
            r13.<init>()     // Catch:{ all -> 0x06d5 }
            boolean r1 = r10.A06     // Catch:{ all -> 0x06d5 }
            if (r1 == 0) goto L_0x0660
            X.0Tq r1 = r4.A08     // Catch:{ all -> 0x06d5 }
            java.lang.Object r9 = r1.get()     // Catch:{ all -> 0x06d5 }
            X.EDv r9 = (X.C28950EDv) r9     // Catch:{ all -> 0x06d5 }
            X.A52 r1 = r10.A01     // Catch:{ all -> 0x06d5 }
            r9.A05(r1, r11)     // Catch:{ all -> 0x06d5 }
            X.A4w r6 = new X.A4w     // Catch:{ all -> 0x06d5 }
            r7 = r4
            r6.<init>(r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x06d5 }
            java.util.concurrent.ScheduledExecutorService r1 = r4.A06     // Catch:{ all -> 0x06d5 }
            r9.addListener(r6, r1)     // Catch:{ all -> 0x06d5 }
            r4.A02 = r9     // Catch:{ all -> 0x06d5 }
        L_0x0660:
            boolean r1 = r10.A07     // Catch:{ all -> 0x06d5 }
            if (r1 == 0) goto L_0x068d
            X.0Tq r1 = r4.A09     // Catch:{ all -> 0x06d5 }
            java.lang.Object r3 = r1.get()     // Catch:{ all -> 0x06d5 }
            X.A4L r3 = (X.A4L) r3     // Catch:{ all -> 0x06d5 }
            X.A4Q r1 = r10.A03     // Catch:{ all -> 0x06d5 }
            r3.A05(r1, r11)     // Catch:{ all -> 0x06d5 }
            X.A4v r2 = new X.A4v     // Catch:{ all -> 0x06d5 }
            r17 = r4
            r18 = r12
            r19 = r3
            r20 = r10
            r21 = r11
            r22 = r8
            r23 = r13
            r16 = r2
            r16.<init>(r17, r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x06d5 }
            java.util.concurrent.ScheduledExecutorService r1 = r4.A06     // Catch:{ all -> 0x06d5 }
            r3.addListener(r2, r1)     // Catch:{ all -> 0x06d5 }
            r4.A03 = r3     // Catch:{ all -> 0x06d5 }
        L_0x068d:
            boolean r1 = r10.A04     // Catch:{ all -> 0x06d5 }
            if (r1 == 0) goto L_0x06ba
            X.0Tq r1 = r4.A07     // Catch:{ all -> 0x06d5 }
            java.lang.Object r3 = r1.get()     // Catch:{ all -> 0x06d5 }
            com.facebook.blescan.BleScanOperation r3 = (com.facebook.blescan.BleScanOperation) r3     // Catch:{ all -> 0x06d5 }
            X.A53 r1 = r10.A00     // Catch:{ all -> 0x06d5 }
            r3.A05(r1)     // Catch:{ all -> 0x06d5 }
            X.A4z r2 = new X.A4z     // Catch:{ all -> 0x06d5 }
            r17 = r4
            r18 = r13
            r19 = r3
            r20 = r10
            r21 = r11
            r22 = r8
            r23 = r12
            r16 = r2
            r16.<init>(r17, r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x06d5 }
            java.util.concurrent.ScheduledExecutorService r1 = r4.A06     // Catch:{ all -> 0x06d5 }
            r3.addListener(r2, r1)     // Catch:{ all -> 0x06d5 }
            r4.A01 = r3     // Catch:{ all -> 0x06d5 }
        L_0x06ba:
            java.lang.Object r3 = r8.value     // Catch:{ all -> 0x06d5 }
            java.lang.Object r2 = r12.value     // Catch:{ all -> 0x06d5 }
            java.lang.Object r1 = r13.value     // Catch:{ all -> 0x06d5 }
            r6 = r4
            r7 = r10
            r8 = r11
            r9 = r3
            r10 = r2
            r11 = r1
            X.C21271A4u.A00(r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x06d5 }
            monitor-exit(r4)     // Catch:{ Exception -> 0x0713 }
            X.A5E r2 = new X.A5E     // Catch:{ Exception -> 0x0713 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0713 }
            X.0VK r1 = r5.A02     // Catch:{ Exception -> 0x0713 }
            X.C05350Yp.A08(r4, r2, r1)     // Catch:{ Exception -> 0x0713 }
            return
        L_0x06d5:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ Exception -> 0x0713 }
            goto L_0x06da
        L_0x06d8:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ Exception -> 0x0713 }
        L_0x06da:
            throw r1     // Catch:{ Exception -> 0x0713 }
        L_0x06db:
            int r2 = X.AnonymousClass1Y3.ANp     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.4Uz r2 = (X.C90774Uz) r2     // Catch:{ Exception -> 0x0713 }
            java.lang.String r1 = "FOREGROUND_LOCATION_CHECK_FAILED"
            X.C90774Uz.A01(r2, r1)     // Catch:{ Exception -> 0x0713 }
            int r2 = r0.A00     // Catch:{ Exception -> 0x0713 }
            int r2 = r2 + r12
            r0.A00 = r2     // Catch:{ Exception -> 0x0713 }
            r1 = 4
            int r4 = java.lang.Math.min(r2, r1)     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r3 = r0.A01     // Catch:{ Exception -> 0x0713 }
            long r1 = com.facebook.location.foreground.ForegroundLocationFrameworkController.A00(r3)     // Catch:{ Exception -> 0x0713 }
            long r1 = r1 << r4
            com.facebook.location.foreground.ForegroundLocationFrameworkController.A06(r3, r1)     // Catch:{ Exception -> 0x0713 }
            return
        L_0x0701:
            int r2 = X.AnonymousClass1Y3.ANp     // Catch:{ Exception -> 0x0713 }
            com.facebook.location.foreground.ForegroundLocationFrameworkController r1 = r0.A01     // Catch:{ Exception -> 0x0713 }
            X.0UN r1 = r1.A05     // Catch:{ Exception -> 0x0713 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r2, r1)     // Catch:{ Exception -> 0x0713 }
            X.4Uz r2 = (X.C90774Uz) r2     // Catch:{ Exception -> 0x0713 }
            java.lang.String r1 = "FOREGROUND_LOCATION_CHECK_FAILED"
            X.C90774Uz.A01(r2, r1)     // Catch:{ Exception -> 0x0713 }
            return
        L_0x0713:
            r4 = move-exception
            r2 = 11
            int r1 = X.AnonymousClass1Y3.BM3
            com.facebook.location.foreground.ForegroundLocationFrameworkController r0 = r0.A01
            X.0UN r3 = r0.A05
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r3)
            X.2ew r2 = (X.C50882ew) r2
            r0 = -9223372036854775808
            r2.A08 = r0
            r2.A07 = r0
            int r0 = X.AnonymousClass1Y3.Amr
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r15, r0, r3)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "ForegroundLocationFrameworkController"
            java.lang.String r0 = "Something is wrong when requesting location"
            r2.softReport(r1, r0, r4)
        L_0x0737:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11890o7.run():void");
    }

    public C11890o7(ForegroundLocationFrameworkController foregroundLocationFrameworkController) {
        this.A01 = foregroundLocationFrameworkController;
    }
}
