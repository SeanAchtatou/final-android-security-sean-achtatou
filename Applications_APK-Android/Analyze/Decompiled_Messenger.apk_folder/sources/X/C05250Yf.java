package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Yf  reason: invalid class name and case insensitive filesystem */
public final class C05250Yf {
    private static volatile C05250Yf A01;
    public final AnonymousClass1YI A00;

    public static final C05250Yf A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C05250Yf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C05250Yf(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C05250Yf(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WA.A00(r2);
    }
}
