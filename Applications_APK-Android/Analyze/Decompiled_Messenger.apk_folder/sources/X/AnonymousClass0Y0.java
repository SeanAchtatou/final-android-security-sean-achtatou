package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.ScheduledFuture;

/* renamed from: X.0Y0  reason: invalid class name */
public interface AnonymousClass0Y0 extends ListenableFuture, ScheduledFuture {
}
