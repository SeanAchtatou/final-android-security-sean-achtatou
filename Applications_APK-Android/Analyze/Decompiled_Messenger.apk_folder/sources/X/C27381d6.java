package X;

/* renamed from: X.1d6  reason: invalid class name and case insensitive filesystem */
public final class C27381d6 implements AnonymousClass1XU {
    private final AnonymousClass1XU[] A00;

    public boolean isEnabled() {
        for (AnonymousClass1XU isEnabled : this.A00) {
            if (isEnabled.isEnabled()) {
                return true;
            }
        }
        return false;
    }

    public C27381d6(AnonymousClass1XU... r1) {
        this.A00 = r1;
    }
}
