package X;

import android.content.Context;
import android.content.pm.Signature;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/* renamed from: X.0TW  reason: invalid class name */
public final class AnonymousClass0TW {
    public final Map A00;
    public final Set A01;

    public boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AnonymousClass0TW)) {
            return false;
        }
        AnonymousClass0TW r5 = (AnonymousClass0TW) obj;
        Set set = r5.A01;
        if (set != null) {
            z = set.equals(this.A01);
        } else {
            z = false;
            if (this.A01 == null) {
                z = true;
            }
        }
        Map map = r5.A00;
        if (map != null) {
            z2 = map.equals(this.A00);
        } else {
            z2 = false;
            if (this.A00 == null) {
                z2 = true;
            }
        }
        return z && z2;
    }

    public static boolean A02(AnonymousClass0TO r1) {
        if (AnonymousClass0TN.A0g.contains(r1) || AnonymousClass0TN.A0u.contains(r1) || AnonymousClass0TN.A0m.contains(r1) || AnonymousClass0TN.A0q.contains(r1) || AnonymousClass0TN.A0j.contains(r1)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (Build.VERSION.SDK_INT > 18) {
            return Objects.hash(this.A01, this.A00);
        }
        return Arrays.hashCode(Arrays.asList(this.A01, this.A00).toArray(new Object[0]));
    }

    public AnonymousClass0TW(Map map) {
        HashSet hashSet = new HashSet();
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            AnonymousClass0TO r2 = (AnonymousClass0TO) entry.getKey();
            Set set = (Set) entry.getValue();
            if (set == null || !set.contains("*|all_packages|*")) {
                if (!hashMap.containsKey(r2)) {
                    hashMap.put(r2, new HashSet());
                }
                ((Set) hashMap.get(r2)).addAll(set);
            } else {
                hashSet.add(r2);
            }
        }
        this.A01 = Collections.unmodifiableSet(hashSet);
        this.A00 = Collections.unmodifiableMap(hashMap);
    }

    public static C06620bo A00(int i, Context context) {
        int length;
        try {
            int i2 = i;
            String[] packagesForUid = context.getPackageManager().getPackagesForUid(i);
            if (packagesForUid == null || (length = packagesForUid.length) == 0) {
                throw new C53332ka(AnonymousClass08S.A09("No packageName associated with uid=", i));
            }
            List unmodifiableList = Collections.unmodifiableList(Arrays.asList(packagesForUid));
            Signature A012 = C28061eC.A01(C28061eC.A00(context, packagesForUid[0]));
            int i3 = 1;
            if (length > 1) {
                while (i3 < length) {
                    if (A012.equals(C28061eC.A01(C28061eC.A00(context, packagesForUid[i3])))) {
                        i3++;
                    } else {
                        throw new C53322kZ(AnonymousClass08S.A0J("packageName=", Arrays.toString(packagesForUid)));
                    }
                }
            }
            return new C06620bo(i2, unmodifiableList, C28061eC.A03(A012), null, null);
        } catch (RuntimeException e) {
            throw new SecurityException(e);
        }
    }

    public static C06620bo A01(Context context) {
        if (Binder.getCallingPid() != Process.myPid()) {
            return A00(Binder.getCallingUid(), context);
        }
        throw new IllegalStateException("This method should be called on behalf of an IPC transaction from binder thread.");
    }

    private static boolean A03(AnonymousClass0TO r2, AnonymousClass0TO r3, boolean z) {
        if (!r2.equals(r3)) {
            if (!z) {
                return false;
            }
            AnonymousClass0TO r0 = (AnonymousClass0TO) AnonymousClass0TN.A0d.get(r3);
            if (r0 == null) {
                r0 = AnonymousClass0TN.A01;
            }
            if (r2.equals(r0)) {
                return true;
            }
            return false;
        }
        return true;
    }

    public static boolean A04(AnonymousClass0TW r7, C06620bo r8, Context context) {
        AnonymousClass0TO r4;
        boolean contains = AnonymousClass0TN.A0e.contains(C28061eC.A02(context, context.getPackageName()));
        if (r8 == null || (r4 = r8.A01) == null || r4 == null) {
            return false;
        }
        for (AnonymousClass0TO A03 : r7.A01) {
            if (A03(r4, A03, contains)) {
                return true;
            }
        }
        HashSet hashSet = new HashSet();
        for (AnonymousClass0TO r1 : r7.A00.keySet()) {
            if (A03(r4, r1, contains)) {
                hashSet.addAll(r8.A04);
                hashSet.retainAll((Collection) r7.A00.get(r1));
            }
        }
        return !hashSet.isEmpty();
    }
}
