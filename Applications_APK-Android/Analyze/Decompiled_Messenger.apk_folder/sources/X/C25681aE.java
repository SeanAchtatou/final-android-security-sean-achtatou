package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.model.UserEmailAddress;

/* renamed from: X.1aE  reason: invalid class name and case insensitive filesystem */
public final class C25681aE implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new UserEmailAddress(parcel);
    }

    public Object[] newArray(int i) {
        return new UserEmailAddress[i];
    }
}
