package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.stash.core.Stash;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

/* renamed from: X.1jA  reason: invalid class name and case insensitive filesystem */
public final class C31111jA extends C31091j8 implements Stash {
    public final int A00;
    public final QuickPerformanceLogger A01;
    public final String A02;
    private final File A03;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public InputStream A00(String str) {
        InputStream fileInputStream;
        int hashCode = ((this.A00 + AnonymousClass1Y3.A4R) * 31) + str.hashCode();
        this.A01.markerStart(42991628, hashCode, "stash_name", this.A02);
        if (!this.A00.hasKey(str)) {
            this.A01.markerEnd(42991628, hashCode, (short) 3);
            return null;
        }
        try {
            this.A01.markerStart(42991645, hashCode, "stash_name", this.A02);
            File resource = this.A00.getResource(str);
            if (this.A01.isMarkerOn(42991645, hashCode)) {
                fileInputStream = new C35691rd(this, resource, hashCode);
            } else {
                fileInputStream = new FileInputStream(resource);
            }
            this.A01.markerEnd(42991628, hashCode, (short) 2);
            return fileInputStream;
        } catch (IOException unused) {
            this.A01.markerEnd(42991645, hashCode, (short) 3);
            this.A01.markerEnd(42991628, hashCode, (short) 3);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:5|6|(1:8)(1:9)) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0054, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0055, code lost:
        r7.A01.markerEnd(42991629, r6, (short) 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005b, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r7.A03.mkdirs();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0041, code lost:
        if (r4 != false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0043, code lost:
        r1 = new X.C45042Ko(r7, r3, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0049, code lost:
        r1 = new java.io.FileOutputStream(r3);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x003c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.OutputStream A01(java.lang.String r8) {
        /*
            r7 = this;
            int r0 = r7.A00
            int r0 = r0 + 527
            int r6 = r0 * 31
            int r0 = r8.hashCode()
            int r6 = r6 + r0
            com.facebook.quicklog.QuickPerformanceLogger r1 = r7.A01
            java.lang.String r0 = r7.A02
            java.lang.String r3 = "stash_name"
            r5 = 42991629(0x290000d, float:2.1158927E-37)
            r1.markerStart(r5, r6, r3, r0)
            com.facebook.quicklog.QuickPerformanceLogger r2 = r7.A01
            java.lang.String r0 = r7.A02
            r1 = 42991646(0x290001e, float:2.1158966E-37)
            r2.markerStart(r1, r6, r3, r0)
            com.facebook.quicklog.QuickPerformanceLogger r0 = r7.A01
            boolean r4 = r0.isMarkerOn(r1, r6)
            com.facebook.stash.core.Stash r0 = r7.A00
            java.io.File r3 = r0.insert(r8)
            r2 = 2
            if (r4 == 0) goto L_0x0036
            X.2Ko r1 = new X.2Ko     // Catch:{ FileNotFoundException -> 0x003c }
            r1.<init>(r7, r3, r6)     // Catch:{ FileNotFoundException -> 0x003c }
            goto L_0x004e
        L_0x0036:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x003c }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x003c }
            goto L_0x004e
        L_0x003c:
            java.io.File r0 = r7.A03     // Catch:{ all -> 0x0054 }
            r0.mkdirs()     // Catch:{ all -> 0x0054 }
            if (r4 == 0) goto L_0x0049
            X.2Ko r1 = new X.2Ko     // Catch:{ all -> 0x0054 }
            r1.<init>(r7, r3, r6)     // Catch:{ all -> 0x0054 }
            goto L_0x004e
        L_0x0049:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x0054 }
            r1.<init>(r3)     // Catch:{ all -> 0x0054 }
        L_0x004e:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r7.A01
            r0.markerEnd(r5, r6, r2)
            return r1
        L_0x0054:
            r2 = move-exception
            com.facebook.quicklog.QuickPerformanceLogger r1 = r7.A01
            r0 = 3
            r1.markerEnd(r5, r6, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31111jA.A01(java.lang.String):java.io.OutputStream");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public Set getAllKeys() {
        this.A01.markerStart(42991637, this.A00, "stash_name", this.A02);
        try {
            return this.A00.getAllKeys();
        } finally {
            this.A01.markerEnd(42991637, this.A00, (short) 2);
        }
    }

    public File getResource(String str) {
        int hashCode = ((this.A00 + AnonymousClass1Y3.A4R) * 31) + str.hashCode();
        this.A01.markerStart(42991628, hashCode, "stash_name", this.A02);
        short s = 3;
        try {
            if (this.A00.hasKey(str)) {
                s = 2;
            }
            return this.A00.getResource(str);
        } finally {
            this.A01.markerEnd(42991628, hashCode, s);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public File insert(String str) {
        int hashCode = ((this.A00 + AnonymousClass1Y3.A4R) * 31) + str.hashCode();
        this.A01.markerStart(42991629, hashCode, "stash_name", this.A02);
        try {
            this.A03.mkdirs();
            return this.A00.insert(str);
        } finally {
            this.A01.markerEnd(42991629, hashCode, (short) 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public boolean remove(String str) {
        int hashCode = ((this.A00 + AnonymousClass1Y3.A4R) * 31) + str.hashCode();
        this.A01.markerStart(42991635, hashCode, "stash_name", this.A02);
        try {
            return this.A00.remove(str);
        } finally {
            this.A01.markerEnd(42991635, hashCode, (short) 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public boolean removeAll() {
        this.A01.markerStart(42991639, this.A00, "stash_name", this.A02);
        try {
            return this.A00.removeAll();
        } finally {
            this.A01.markerEnd(42991639, this.A00, (short) 2);
        }
    }

    public C31111jA(String str, File file, Stash stash, QuickPerformanceLogger quickPerformanceLogger) {
        super(stash);
        this.A02 = str;
        this.A03 = file;
        this.A01 = quickPerformanceLogger;
        this.A00 = file.hashCode();
    }
}
