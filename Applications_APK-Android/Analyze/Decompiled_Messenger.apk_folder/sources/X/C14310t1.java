package X;

import java.util.ListIterator;

/* renamed from: X.0t1  reason: invalid class name and case insensitive filesystem */
public abstract class C14310t1 extends C27001cU implements ListIterator {
    public void add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean hasPrevious() {
        return ((ListIterator) this.A00).hasPrevious();
    }

    public final int nextIndex() {
        return ((ListIterator) this.A00).nextIndex();
    }

    public final Object previous() {
        return A00(((ListIterator) this.A00).previous());
    }

    public final int previousIndex() {
        return ((ListIterator) this.A00).previousIndex();
    }

    public void set(Object obj) {
        throw new UnsupportedOperationException();
    }

    public C14310t1(ListIterator listIterator) {
        super(listIterator);
    }
}
