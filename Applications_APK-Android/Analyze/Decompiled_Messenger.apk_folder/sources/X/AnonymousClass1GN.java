package X;

import android.content.Context;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1GN  reason: invalid class name */
public final class AnonymousClass1GN extends C17770zR {
    public static final AnonymousClass1GU A07 = C33051ml.A00;
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public AnonymousClass1LB A01;
    @Comparable(type = 13)
    public C20131Ba A02;
    @Comparable(type = 13)
    public AnonymousClass1JH A03;
    @Comparable(type = 13)
    public C21781Ir A04;
    @Comparable(type = 13)
    public AnonymousClass1GU A05 = A07;
    @Comparable(type = 13)
    public String A06;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r4) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(4);
        ComponentBuilderCBuilderShape0_0S0300000.A04(componentBuilderCBuilderShape0_0S0300000, r4, 0, 0, new AnonymousClass1GN(r4.A09));
        return componentBuilderCBuilderShape0_0S0300000;
    }

    static {
        if (C33051ml.A00 == null) {
            C33051ml.A00 = new C33051ml();
        }
    }

    public AnonymousClass1GN(Context context) {
        super("SearchBar");
        this.A00 = new AnonymousClass0UN(3, AnonymousClass1XX.get(context));
    }
}
