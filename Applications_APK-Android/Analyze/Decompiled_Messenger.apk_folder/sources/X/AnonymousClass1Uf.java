package X;

import com.facebook.omnistore.util.DeviceIdUtil;
import java.util.HashMap;

/* renamed from: X.1Uf  reason: invalid class name */
public final class AnonymousClass1Uf extends HashMap<Long, String> {
    public AnonymousClass1Uf() {
        put(256002347743983L, "am");
        put(105910932827969L, "ami");
        put(181425161904154L, "amd");
        put(350685531728L, "af");
        put(1442593136019356L, "aw");
        put(312713275593566L, "awm");
        put(1665147590233624L, "awvc");
        put(358698234273213L, "ag");
        put(615857748495393L, "ac");
        put(1355626171143913L, "bonfire");
        put(1348564698517390L, "aloha");
        put(1104941186305379L, "atalk");
        put(628551730674460L, "aktalk");
        put(314368878911397L, "ainternalcameraapp");
        put(DeviceIdUtil.GRAPHQL_TEST_APP_ID, "agraphtest");
        put(DeviceIdUtil.INSTANT_ARTICLES_SAMPLE_APP_ID, "aarticlessample");
        put(DeviceIdUtil.AI_DEMOS_SAMPLE_APP_ID, "aaidemossample");
        put(DeviceIdUtil.APP_MANAGER_APP_ID, "appmanager");
    }
}
