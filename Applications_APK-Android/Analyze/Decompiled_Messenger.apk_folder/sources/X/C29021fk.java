package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.mig.scheme.schemes.LightColorScheme;

/* renamed from: X.1fk  reason: invalid class name and case insensitive filesystem */
public final class C29021fk implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return LightColorScheme.A00();
    }

    public Object[] newArray(int i) {
        return new LightColorScheme[i];
    }
}
