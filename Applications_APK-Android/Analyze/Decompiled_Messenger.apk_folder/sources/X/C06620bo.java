package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.0bo  reason: invalid class name and case insensitive filesystem */
public final class C06620bo {
    public final int A00;
    public final AnonymousClass0TO A01;
    public final String A02;
    public final String A03;
    public final List A04;

    public static String A00(C06620bo r0) {
        if (r0 == null) {
            return "null";
        }
        return r0.toString();
    }

    public String A01() {
        if (!this.A04.isEmpty()) {
            return (String) this.A04.iterator().next();
        }
        throw new IllegalStateException("Invalid AppIdentity object: no package names");
    }

    public String toString() {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder("AppIdentity{uid=");
        sb.append(this.A00);
        sb.append(", packageNames=");
        sb.append(this.A04);
        sb.append(", sha1=");
        AnonymousClass0TO r2 = this.A01;
        if (r2 == null) {
            str = "null";
        } else {
            str = r2.sha1Hash;
        }
        sb.append(str);
        sb.append(", sha2=");
        if (r2 == null) {
            str2 = "null";
        } else {
            str2 = r2.sha256Hash;
        }
        sb.append(str2);
        sb.append(", version=");
        String str3 = this.A03;
        if (str3 == null) {
            str3 = "null";
        }
        sb.append(str3);
        sb.append(", domain=");
        String str4 = this.A02;
        if (str4 == null) {
            str4 = "null";
        }
        sb.append(str4);
        sb.append('}');
        return sb.toString();
    }

    public C06620bo(int i, List list, AnonymousClass0TO r5, String str, String str2) {
        this.A00 = i;
        this.A04 = Collections.unmodifiableList(list);
        this.A01 = r5;
        this.A03 = str;
        this.A02 = str2;
        if (list.isEmpty()) {
            throw new IllegalArgumentException("At least one package name is required");
        }
    }
}
