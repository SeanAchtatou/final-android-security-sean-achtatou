package X;

import java.util.Arrays;

/* renamed from: X.0jq  reason: invalid class name and case insensitive filesystem */
public final class C10280jq extends C10260jo {
    public static final char[] SPACES;
    private static final String SYS_LF;
    public static final C10280jq instance = new C10280jq();

    public boolean isInline() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    static {
        String str;
        try {
            str = System.getProperty(C99084oO.$const$string(6));
        } catch (Throwable unused) {
            str = null;
        }
        if (str == null) {
            str = "\n";
        }
        SYS_LF = str;
        char[] cArr = new char[64];
        SPACES = cArr;
        Arrays.fill(cArr, ' ');
    }

    public void writeIndentation(C11710np r4, int i) {
        r4.writeRaw(SYS_LF);
        if (i > 0) {
            int i2 = i + i;
            while (i2 > 64) {
                char[] cArr = SPACES;
                r4.writeRaw(cArr, 0, 64);
                i2 -= cArr.length;
            }
            r4.writeRaw(SPACES, 0, i2);
        }
    }
}
