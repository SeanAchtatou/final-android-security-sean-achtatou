package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0kL  reason: invalid class name and case insensitive filesystem */
public final class C10530kL implements C10520kK {
    private static volatile C10530kL A01;
    private AnonymousClass0UN A00;

    public static final C10530kL A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C10530kL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C10530kL(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public C09240gn Aik() {
        return (C09230gm) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BAy, this.A00);
    }

    public C08850g3 Aim() {
        return (C08840g2) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BLr, this.A00);
    }

    public AnonymousClass10B Aio() {
        return (AnonymousClass10B) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B7I, this.A00);
    }

    private C10530kL(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
