package X;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.LruCache;
import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import javax.inject.Singleton;
import org.webrtc.audio.WebRtcAudioRecord;

@Singleton
/* renamed from: X.0si  reason: invalid class name and case insensitive filesystem */
public final class C14160si extends C14180sk {
    private static volatile C14160si A08;
    public AnonymousClass0UN A00;
    public final AudioManager A01;
    public final TelephonyManager A02;
    public final C14150sh A03;
    public final boolean A04;
    private final AnonymousClass0Ud A05;
    private final C001500z A06;
    private final C14210sn A07 = new C14210sn();

    public boolean A0G() {
        C001500z r1 = this.A06;
        return (r1 == C001500z.A07 || r1 == C001500z.A0C) && this.A03.A01.Aep(C05690aA.A0x, true) && this.A05.A0H() && this.A01.getStreamVolume(1) > 0;
    }

    public static final C14160si A01(AnonymousClass1XY r13) {
        if (A08 == null) {
            synchronized (C14160si.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A08, r13);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r13.getApplicationInjector();
                        A08 = new C14160si(applicationInjector, AnonymousClass0UU.A05(applicationInjector), new C14150sh(applicationInjector), C04490Ux.A0Q(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.A7y, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AQ9, applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.Ap7, applicationInjector), C04490Ux.A0c(applicationInjector), AnonymousClass0Ud.A00(applicationInjector), AnonymousClass0UU.A08(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A08;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002c, code lost:
        if (X.AnonymousClass3EI.A01(r3) == false) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(X.C14160si r4, android.content.Context r5, java.lang.String r6) {
        /*
            int r1 = X.AnonymousClass1Y3.ABg
            X.0UN r0 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            X.3VI r2 = (X.AnonymousClass3VI) r2
            int r1 = X.AnonymousClass1Y3.AWS
            X.0UN r0 = r4.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r1, r0)
            X.3EI r3 = (X.AnonymousClass3EI) r3
            boolean r0 = X.AnonymousClass3RG.A01(r5)
            if (r0 == 0) goto L_0x002e
            X.1Yd r2 = r2.A00
            r0 = 282239481021698(0x100b200020502, double:1.394448314728835E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x002e
            boolean r1 = X.AnonymousClass3EI.A01(r3)
            r0 = 1
            if (r1 != 0) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            r1 = 0
            if (r0 == 0) goto L_0x003e
            boolean r0 = r4.A0B(r6)
            if (r0 == 0) goto L_0x003e
            r0 = 0
            r4.A08(r0, r6)
            r0 = 1
            return r0
        L_0x003e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14160si.A04(X.0si, android.content.Context, java.lang.String):boolean");
    }

    public AnonymousClass9RY A0C(String str) {
        C71243c4 A012 = ((C71233c3) this.A01.get()).A01(str);
        if (A012 instanceof AnonymousClass9RY) {
            return (AnonymousClass9RY) A012;
        }
        return null;
    }

    private C14160si(AnonymousClass1XY r3, C001500z r4, C14150sh r5, AudioManager audioManager, C04310Tq r7, C04310Tq r8, C04310Tq r9, TelephonyManager telephonyManager, AnonymousClass0Ud r11, Boolean bool) {
        super(r7, r8, r9);
        this.A00 = new AnonymousClass0UN(0, r3);
        this.A06 = r4;
        this.A03 = r5;
        this.A01 = audioManager;
        this.A02 = telephonyManager;
        this.A05 = r11;
        this.A04 = bool.booleanValue();
    }

    public static final C14160si A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static C191028uk A02(C14160si r4, String str, float f) {
        if (!r4.A0G()) {
            return null;
        }
        C191028uk A052 = C14180sk.A05(r4);
        Boolean bool = false;
        A052.A01 = bool.booleanValue();
        C71243c4 A012 = ((C71233c3) r4.A01.get()).A01(str);
        if (A012.A03(A052, 1, f)) {
            C14180sk.A07(r4, str, A012, f);
            return A052;
        }
        C14180sk.A06(r4, str, A012);
        r4.A00.remove(A052);
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r1 == null) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(X.C14160si r2, java.lang.String r3, java.lang.String r4) {
        /*
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x001f
            X.0sn r0 = r2.A07
            if (r4 == 0) goto L_0x0011
            java.lang.Object r1 = r0.A03(r4)
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 != 0) goto L_0x001f
            X.0sn r0 = r2.A07
            if (r4 == 0) goto L_0x001b
            r0.A05(r4, r4)
        L_0x001b:
            r0 = 0
            r2.A08(r0, r3)
        L_0x001f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14160si.A03(X.0si, java.lang.String, java.lang.String):void");
    }

    public C191028uk A0D(Uri uri, Context context) {
        String str;
        float f;
        String uri2 = uri.toString();
        C24971Xv it = AnonymousClass9RU.A00.iterator();
        while (true) {
            if (!it.hasNext()) {
                str = null;
                break;
            }
            str = (String) it.next();
            AnonymousClass9RY A0C = A0C(str);
            if (A0C != null && uri2.equals(A0C.A00(context))) {
                break;
            }
        }
        if (str != null) {
            char c = 65535;
            switch (str.hashCode()) {
                case -1690710689:
                    if (str.equals("messagekid")) {
                        c = 8;
                        break;
                    }
                    break;
                case -1606828125:
                    if (str.equals("doubleknock")) {
                        c = 10;
                        break;
                    }
                    break;
                case -1494521591:
                    if (str.equals("singlepop")) {
                        c = 24;
                        break;
                    }
                    break;
                case -1323665194:
                    if (str.equals("dreamy")) {
                        c = 21;
                        break;
                    }
                    break;
                case -1082154559:
                    if (str.equals("fanfare")) {
                        c = 7;
                        break;
                    }
                    break;
                case -1010578843:
                    if (str.equals("openup")) {
                        c = 19;
                        break;
                    }
                    break;
                case -930826704:
                    if (str.equals("ripple")) {
                        c = 18;
                        break;
                    }
                    break;
                case -901890033:
                    if (str.equals("sizzle")) {
                        c = 22;
                        break;
                    }
                    break;
                case -824614255:
                    if (str.equals("blipbeeb")) {
                        c = 13;
                        break;
                    }
                    break;
                case -806135584:
                    if (str.equals("doublepop")) {
                        c = 25;
                        break;
                    }
                    break;
                case -701784216:
                    if (str.equals("zipzap")) {
                        c = 6;
                        break;
                    }
                    break;
                case -474234170:
                    if (str.equals("orchestrahit")) {
                        c = 16;
                        break;
                    }
                    break;
                case -341289037:
                    if (str.equals("resonate")) {
                        c = 0;
                        break;
                    }
                    break;
                case -81857902:
                    if (str.equals("vibration")) {
                        c = 23;
                        break;
                    }
                    break;
                case 114595:
                    if (str.equals("tap")) {
                        c = 4;
                        break;
                    }
                    break;
                case 3441010:
                    if (str.equals("ping")) {
                        c = 3;
                        break;
                    }
                    break;
                case 93745868:
                    if (str.equals("birde")) {
                        c = 1;
                        break;
                    }
                    break;
                case 93826904:
                    if (str.equals("bling")) {
                        c = 11;
                        break;
                    }
                    break;
                case 94631228:
                    if (str.equals("chime")) {
                        c = 14;
                        break;
                    }
                    break;
                case 99162322:
                    if (str.equals("hello")) {
                        c = 26;
                        break;
                    }
                    break;
                case 107027353:
                    if (str.equals("pulse")) {
                        c = 9;
                        break;
                    }
                    break;
                case 751549073:
                    if (str.equals("babygiggle")) {
                        c = 12;
                        break;
                    }
                    break;
                case 901835663:
                    if (str.equals("triple pop")) {
                        c = 20;
                        break;
                    }
                    break;
                case 950345194:
                    if (str.equals(AnonymousClass24B.$const$string(268))) {
                        c = 27;
                        break;
                    }
                    break;
                case 1206458640:
                    if (str.equals("rimshot")) {
                        c = 17;
                        break;
                    }
                    break;
                case 1316697938:
                    if (str.equals("whistle")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1832029684:
                    if (str.equals("dogbark")) {
                        c = 15;
                        break;
                    }
                    break;
                case 1936513698:
                    if (str.equals("crickets")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            f = 0.2f;
            switch (c) {
                case 0:
                    f = 0.015f;
                    break;
                case 1:
                    f = 0.075f;
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    f = 0.1f;
                    break;
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case 8:
                case Process.SIGKILL:
                    f = 0.15f;
                    break;
                case AnonymousClass1Y3.A01 /*10*/:
                    f = 0.175f;
                    break;
                case Process.SIGSTOP:
                case 20:
                    f = 0.25f;
                    break;
                case AnonymousClass1Y3.A05 /*21*/:
                case AnonymousClass1Y3.A06 /*22*/:
                case 23:
                    f = 0.3f;
                    break;
                case AnonymousClass1Y3.A07 /*24*/:
                    f = 0.35f;
                    break;
                case 25:
                case AnonymousClass1Y3.A08 /*26*/:
                case AnonymousClass1Y3.A09 /*27*/:
                    f = 0.4f;
                    break;
            }
        } else {
            f = 1.0f;
        }
        if (this.A01.getStreamVolume(1) <= 0) {
            return null;
        }
        C191028uk A052 = C14180sk.A05(this);
        Object obj = uri;
        LruCache lruCache = this.A00;
        if (uri == null) {
            obj = "NULL";
        }
        lruCache.put(A052, obj);
        A052.A08(uri, 1, f);
        return A052;
    }

    public void A0E(Context context) {
        if (A0G() && !A04(this, context, "m4_ax_send")) {
            A08(null, "send");
        }
    }

    public void A0F(Context context, boolean z) {
        if (!A0G()) {
            return;
        }
        if (z) {
            A04(this, context, "m4_ax_receive_3");
        } else {
            A08(null, "in_app_notification");
        }
    }
}
