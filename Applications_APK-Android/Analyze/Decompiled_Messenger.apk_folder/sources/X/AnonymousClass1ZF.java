package X;

import com.facebook.analytics2.loggermodule.Analytics2HandlerThreadFactory;
import com.facebook.analytics2.loggermodule.FbUploadJobInstrumentation;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1ZF  reason: invalid class name */
public final class AnonymousClass1ZF extends AnonymousClass0UV {
    private static volatile C25751aL A00;
    private static volatile C25721aI A01;
    private static volatile C06230bA A02;

    public static final C25751aL A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C25751aL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = (C25751aL) AnonymousClass0VB.A00(AnonymousClass1Y3.B4e, r4.getApplicationInjector()).get();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C25721aI A01(AnonymousClass1XY r5) {
        Object obj;
        if (A01 == null) {
            synchronized (C25721aI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        AnonymousClass0VB A003 = AnonymousClass0VB.A00(AnonymousClass1Y3.A1X, applicationInjector);
                        AnonymousClass0VB A004 = AnonymousClass0VB.A00(AnonymousClass1Y3.ALL, applicationInjector);
                        if (C006006f.A01()) {
                            obj = A004.get();
                        } else {
                            obj = A003.get();
                        }
                        A01 = (C25721aI) obj;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r28v0, resolved type: X.0bM} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r30v1, resolved type: X.0cA} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r30v2, resolved type: X.0cA} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r30v3, resolved type: X.0cA} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C06230bA A02(X.AnonymousClass1XY r31) {
        /*
            X.0bA r0 = X.AnonymousClass1ZF.A02
            if (r0 != 0) goto L_0x022c
            java.lang.Class<X.0bA> r27 = X.C06230bA.class
            monitor-enter(r27)
            X.0bA r0 = X.AnonymousClass1ZF.A02     // Catch:{ all -> 0x0229 }
            r1 = r31
            X.0WD r26 = X.AnonymousClass0WD.A00(r0, r1)     // Catch:{ all -> 0x0229 }
            if (r26 == 0) goto L_0x0227
            X.1XY r4 = r1.getApplicationInjector()     // Catch:{ all -> 0x021f }
            android.content.Context r25 = X.AnonymousClass1YA.A00(r4)     // Catch:{ all -> 0x021f }
            X.0X5 r2 = new X.0X5     // Catch:{ all -> 0x021f }
            int[] r0 = X.AnonymousClass0X6.A02     // Catch:{ all -> 0x021f }
            r2.<init>(r4, r0)     // Catch:{ all -> 0x021f }
            X.0X5 r3 = new X.0X5     // Catch:{ all -> 0x021f }
            int[] r0 = X.AnonymousClass0X6.A01     // Catch:{ all -> 0x021f }
            r3.<init>(r4, r0)     // Catch:{ all -> 0x021f }
            int r0 = X.AnonymousClass1Y3.AT0     // Catch:{ all -> 0x021f }
            X.0VG r24 = X.AnonymousClass0VG.A00(r0, r4)     // Catch:{ all -> 0x021f }
            int r0 = X.AnonymousClass1Y3.BEf     // Catch:{ all -> 0x021f }
            X.0UQ r23 = X.AnonymousClass0UQ.A00(r0, r4)     // Catch:{ all -> 0x021f }
            X.0bB r22 = X.C06240bB.A01(r4)     // Catch:{ all -> 0x021f }
            X.0bC r21 = X.C06250bC.A00(r4)     // Catch:{ all -> 0x021f }
            X.0bG r13 = X.C06290bG.A00(r4)     // Catch:{ all -> 0x021f }
            X.0bH r12 = X.C06300bH.A00(r4)     // Catch:{ all -> 0x021f }
            X.0c9 r11 = com.facebook.analytics.AnalyticsClientModule.A05(r4)     // Catch:{ all -> 0x021f }
            X.0cA r20 = com.facebook.analytics.AnalyticsClientModule.A06(r4)     // Catch:{ all -> 0x021f }
            java.lang.Class<com.facebook.analytics.NewAnalyticsSamplingPolicyConfig> r10 = com.facebook.analytics.NewAnalyticsSamplingPolicyConfig.class
            X.0d3 r15 = new X.0d3     // Catch:{ all -> 0x021f }
            r15.<init>(r4)     // Catch:{ all -> 0x021f }
            int r0 = X.AnonymousClass1Y3.AeN     // Catch:{ all -> 0x021f }
            X.0VB r14 = X.AnonymousClass0VB.A00(r0, r4)     // Catch:{ all -> 0x021f }
            java.lang.Class<com.facebook.analytics2.loggermodule.Analytics2HandlerThreadFactory> r9 = com.facebook.analytics2.loggermodule.Analytics2HandlerThreadFactory.class
            int r0 = X.AnonymousClass1Y3.ALy     // Catch:{ all -> 0x021f }
            X.0VB r1 = X.AnonymousClass0VB.A00(r0, r4)     // Catch:{ all -> 0x021f }
            X.0d4 r8 = X.C07240d4.A00(r4)     // Catch:{ all -> 0x021f }
            X.1aI r0 = A01(r4)     // Catch:{ all -> 0x021f }
            boolean r0 = r0.AlH()     // Catch:{ all -> 0x021f }
            if (r0 == 0) goto L_0x009d
            java.lang.Class<com.facebook.analytics2.loggermodule.FbUploadJobInstrumentation> r7 = com.facebook.analytics2.loggermodule.FbUploadJobInstrumentation.class
        L_0x0070:
            int r0 = X.AnonymousClass1Y3.ATP     // Catch:{ all -> 0x021f }
            X.0UQ r19 = X.AnonymousClass0UQ.A00(r0, r4)     // Catch:{ all -> 0x021f }
            int r0 = X.AnonymousClass1Y3.BT8     // Catch:{ all -> 0x021f }
            X.0UQ r18 = X.AnonymousClass0UQ.A00(r0, r4)     // Catch:{ all -> 0x021f }
            X.1aK r6 = X.C25731aJ.A00(r4)     // Catch:{ all -> 0x021f }
            X.0d9 r17 = new X.0d9     // Catch:{ all -> 0x021f }
            r17.<init>()     // Catch:{ all -> 0x021f }
            java.util.Iterator r4 = r2.iterator()     // Catch:{ all -> 0x021f }
        L_0x0089:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x021f }
            if (r0 == 0) goto L_0x009f
            java.lang.Object r2 = r4.next()     // Catch:{ all -> 0x021f }
            X.0dB r2 = (X.C07290dB) r2     // Catch:{ all -> 0x021f }
            r0 = r17
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x021f }
            r0.A02(r2)     // Catch:{ all -> 0x021f }
            goto L_0x0089
        L_0x009d:
            r7 = 0
            goto L_0x0070
        L_0x009f:
            X.0d9 r16 = new X.0d9     // Catch:{ all -> 0x021f }
            r16.<init>()     // Catch:{ all -> 0x021f }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x021f }
        L_0x00a8:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x021f }
            if (r0 == 0) goto L_0x00bc
            java.lang.Object r2 = r3.next()     // Catch:{ all -> 0x021f }
            X.0dB r2 = (X.C07290dB) r2     // Catch:{ all -> 0x021f }
            r0 = r16
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x021f }
            r0.A02(r2)     // Catch:{ all -> 0x021f }
            goto L_0x00a8
        L_0x00bc:
            X.0bM r5 = new X.0bM     // Catch:{ all -> 0x021f }
            r0 = 0
            r28 = r5
            r29 = r1
            r30 = r19
            r31 = r0
            r28.<init>(r29, r30, r31)     // Catch:{ all -> 0x021f }
            X.0bM r4 = new X.0bM     // Catch:{ all -> 0x021f }
            r2 = 1
            r0 = r19
            r4.<init>(r1, r0, r2)     // Catch:{ all -> 0x021f }
            X.0oK r3 = new X.0oK     // Catch:{ all -> 0x021f }
            java.lang.Object r0 = r19.get()     // Catch:{ all -> 0x021f }
            X.1aI r0 = (X.C25721aI) r0     // Catch:{ all -> 0x021f }
            r3.<init>(r0)     // Catch:{ all -> 0x021f }
            X.0dD r1 = new X.0dD     // Catch:{ all -> 0x021f }
            java.lang.Object r0 = r19.get()     // Catch:{ all -> 0x021f }
            X.1aI r0 = (X.C25721aI) r0     // Catch:{ all -> 0x021f }
            r1.<init>(r0)     // Catch:{ all -> 0x021f }
            int r2 = r20.C0o()     // Catch:{ all -> 0x021f }
            r0 = -1
            r30 = r20
            if (r2 != r0) goto L_0x00f3
            r30 = r5
        L_0x00f3:
            X.0dE r2 = new X.0dE     // Catch:{ all -> 0x021f }
            r0 = r25
            r2.<init>(r0)     // Catch:{ all -> 0x021f }
            r0 = r17
            r2.A06 = r0     // Catch:{ all -> 0x021f }
            r0 = r16
            r2.A05 = r0     // Catch:{ all -> 0x021f }
            X.0dF r0 = new X.0dF     // Catch:{ all -> 0x021f }
            r28 = r0
            r29 = r24
            r28.<init>(r29)     // Catch:{ all -> 0x021f }
            r2.A0H = r0     // Catch:{ all -> 0x021f }
            r2.A0L = r15     // Catch:{ all -> 0x021f }
            X.0dG r0 = new X.0dG     // Catch:{ all -> 0x021f }
            r28 = r0
            r29 = r23
            r28.<init>(r29)     // Catch:{ all -> 0x021f }
            r2.A0G = r0     // Catch:{ all -> 0x021f }
            X.0cg r0 = new X.0cg     // Catch:{ all -> 0x021f }
            r23 = r0
            r24 = r22
            r23.<init>(r24)     // Catch:{ all -> 0x021f }
            r2.A0F = r0     // Catch:{ all -> 0x021f }
            X.0ch r0 = new X.0ch     // Catch:{ all -> 0x021f }
            r0.<init>(r14)     // Catch:{ all -> 0x021f }
            r2.A0E = r0     // Catch:{ all -> 0x021f }
            r2.A09 = r5     // Catch:{ all -> 0x021f }
            r2.A08 = r4     // Catch:{ all -> 0x021f }
            r0 = r30
            r2.A0D = r0     // Catch:{ all -> 0x021f }
            r2.A0C = r4     // Catch:{ all -> 0x021f }
            java.lang.Class<com.facebook.analytics2.uploader.fbhttp.FbHttpUploader> r0 = com.facebook.analytics2.uploader.fbhttp.FbHttpUploader.class
            r2.A0P = r0     // Catch:{ all -> 0x021f }
            r0 = r21
            r2.A0B = r0     // Catch:{ all -> 0x021f }
            r2.A00 = r13     // Catch:{ all -> 0x021f }
            r2.A01 = r12     // Catch:{ all -> 0x021f }
            r2.A0N = r10     // Catch:{ all -> 0x021f }
            r2.A0M = r9     // Catch:{ all -> 0x021f }
            r2.A0O = r7     // Catch:{ all -> 0x021f }
            r2.A04 = r8     // Catch:{ all -> 0x021f }
            r2.A0I = r3     // Catch:{ all -> 0x021f }
            r2.A0K = r1     // Catch:{ all -> 0x021f }
            X.0dv r0 = new X.0dv     // Catch:{ all -> 0x021f }
            r0.<init>()     // Catch:{ all -> 0x021f }
            r2.A0J = r0     // Catch:{ all -> 0x021f }
            r2.A07 = r11     // Catch:{ all -> 0x021f }
            r0 = r20
            r2.A0A = r0     // Catch:{ all -> 0x021f }
            java.lang.Object r0 = r18.get()     // Catch:{ all -> 0x021f }
            X.1aL r0 = (X.C25751aL) r0     // Catch:{ all -> 0x021f }
            int r1 = X.AnonymousClass1Y3.AWK     // Catch:{ all -> 0x021f }
            X.0UN r0 = r0.A00     // Catch:{ all -> 0x021f }
            r4 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x021f }
            X.1Yd r3 = (X.C25051Yd) r3     // Catch:{ all -> 0x021f }
            r0 = 18295916435865631(0x41000a0000001f, double:1.891329755697461E-307)
            boolean r0 = r3.Aeo(r0, r4)     // Catch:{ all -> 0x021f }
            if (r0 == 0) goto L_0x01b3
            X.0XQ r3 = r6.A00     // Catch:{ all -> 0x021f }
            X.95w r0 = X.C1936795w.A02     // Catch:{ all -> 0x021f }
            if (r0 != 0) goto L_0x018a
            X.95w r1 = new X.95w     // Catch:{ all -> 0x021f }
            java.lang.String r0 = "pigeon_normal_pri_health_metrics"
            X.0aI r0 = r3.A00(r0)     // Catch:{ all -> 0x021f }
            r1.<init>(r0)     // Catch:{ all -> 0x021f }
            X.C1936795w.A02 = r1     // Catch:{ all -> 0x021f }
        L_0x018a:
            X.95w r4 = X.C1936795w.A02     // Catch:{ all -> 0x021f }
            X.0XQ r3 = r6.A00     // Catch:{ all -> 0x021f }
            X.95w r0 = X.C1936795w.A01     // Catch:{ all -> 0x021f }
            if (r0 != 0) goto L_0x019f
            X.95w r1 = new X.95w     // Catch:{ all -> 0x021f }
            java.lang.String r0 = "pigeon_high_pri_health_metrics"
            X.0aI r0 = r3.A00(r0)     // Catch:{ all -> 0x021f }
            r1.<init>(r0)     // Catch:{ all -> 0x021f }
            X.C1936795w.A01 = r1     // Catch:{ all -> 0x021f }
        L_0x019f:
            X.95w r1 = X.C1936795w.A01     // Catch:{ all -> 0x021f }
            r0 = r17
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x021f }
            r0.A02(r4)     // Catch:{ all -> 0x021f }
            r0 = r16
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x021f }
            r0.A02(r1)     // Catch:{ all -> 0x021f }
            r2.A03 = r4     // Catch:{ all -> 0x021f }
            r2.A02 = r1     // Catch:{ all -> 0x021f }
        L_0x01b3:
            X.0bA r1 = new X.0bA     // Catch:{ all -> 0x021f }
            r1.<init>(r2)     // Catch:{ all -> 0x021f }
            java.lang.Object r0 = r19.get()     // Catch:{ all -> 0x021f }
            X.1aI r0 = (X.C25721aI) r0     // Catch:{ all -> 0x021f }
            boolean r0 = r0.B31()     // Catch:{ all -> 0x021f }
            if (r0 == 0) goto L_0x01e6
            boolean r0 = r20.BFp()     // Catch:{ all -> 0x021f }
            if (r0 == 0) goto L_0x01e9
            X.23u r2 = new X.23u     // Catch:{ all -> 0x021f }
            r4 = r1
            X.23v r0 = new X.23v     // Catch:{ all -> 0x021f }
            java.lang.String r6 = "micro_batch"
            java.lang.String r7 = "micro_batch"
            r8 = 47
            r9 = 0
            r3 = r0
            r5 = r25
            r3.<init>(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x021f }
            r2.<init>(r0)     // Catch:{ all -> 0x021f }
            r0 = r17
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x021f }
            r0.A02(r2)     // Catch:{ all -> 0x021f }
        L_0x01e6:
            X.AnonymousClass1ZF.A02 = r1     // Catch:{ all -> 0x021f }
            goto L_0x0224
        L_0x01e9:
            X.23u r2 = new X.23u     // Catch:{ all -> 0x021f }
            r4 = r1
            X.23v r0 = new X.23v     // Catch:{ all -> 0x021f }
            java.lang.String r6 = "normal"
            java.lang.String r7 = "normal"
            r8 = 97
            r9 = 0
            r3 = r0
            r5 = r25
            r3.<init>(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x021f }
            r2.<init>(r0)     // Catch:{ all -> 0x021f }
            r0 = r17
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x021f }
            r0.A02(r2)     // Catch:{ all -> 0x021f }
            X.23u r2 = new X.23u     // Catch:{ all -> 0x021f }
            X.23v r0 = new X.23v     // Catch:{ all -> 0x021f }
            java.lang.String r6 = "high"
            java.lang.String r7 = "hipri"
            r8 = 11
            r9 = 1
            r3 = r0
            r3.<init>(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x021f }
            r2.<init>(r0)     // Catch:{ all -> 0x021f }
            r0 = r16
            X.0bK r0 = r0.A00     // Catch:{ all -> 0x021f }
            r0.A02(r2)     // Catch:{ all -> 0x021f }
            goto L_0x01e6
        L_0x021f:
            r0 = move-exception
            r26.A01()     // Catch:{ all -> 0x0229 }
            throw r0     // Catch:{ all -> 0x0229 }
        L_0x0224:
            r26.A01()     // Catch:{ all -> 0x0229 }
        L_0x0227:
            monitor-exit(r27)     // Catch:{ all -> 0x0229 }
            goto L_0x022c
        L_0x0229:
            r0 = move-exception
            monitor-exit(r27)     // Catch:{ all -> 0x0229 }
            throw r0
        L_0x022c:
            X.0bA r0 = X.AnonymousClass1ZF.A02
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ZF.A02(X.1XY):X.0bA");
    }

    public static final Class A03() {
        return Analytics2HandlerThreadFactory.class;
    }

    public static final Class A04(AnonymousClass1XY r0) {
        if (A01(r0).AlH()) {
            return FbUploadJobInstrumentation.class;
        }
        return null;
    }
}
