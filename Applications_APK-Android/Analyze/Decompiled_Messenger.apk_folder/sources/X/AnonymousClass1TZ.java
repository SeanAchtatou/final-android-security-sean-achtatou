package X;

import android.content.res.Resources;
import com.facebook.messaging.model.messages.GenericAdminMessageInfo;
import com.facebook.messaging.model.messages.InstantGameInfoProperties;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.1TZ  reason: invalid class name */
public final class AnonymousClass1TZ implements AnonymousClass1TW {
    private AnonymousClass0UN A00;
    public final Resources A01;
    public final AnonymousClass1GC A02;
    public final C24291Ta A03;
    public final C04310Tq A04;
    public final C04310Tq A05;

    private InstantGameInfoProperties A01(ThreadSummary threadSummary) {
        GenericAdminMessageInfo genericAdminMessageInfo;
        MessagesCollection A07;
        Message message = null;
        if (!(threadSummary == null || (A07 = ((C26681bq) this.A04.get()).A07(threadSummary.A0S)) == null || A07.A08())) {
            message = A07.A07(0);
        }
        if (message == null || (genericAdminMessageInfo = message.A09) == null || !(genericAdminMessageInfo.A01() instanceof InstantGameInfoProperties)) {
            return null;
        }
        return (InstantGameInfoProperties) genericAdminMessageInfo.A01();
    }

    public static final AnonymousClass1TZ A00(AnonymousClass1XY r1) {
        return new AnonymousClass1TZ(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0103, code lost:
        if ((!com.google.common.base.Platform.stringIsNullOrEmpty(X.C24291Ta.A02(r1))) == false) goto L_0x0105;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C23761Qt Afj(com.facebook.messaging.model.threads.ThreadSummary r7) {
        /*
            r6 = this;
            com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType r1 = com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType.GAME
            com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType r0 = r7.A0J
            boolean r0 = r1.equals(r0)
            r2 = 0
            r4 = 0
            if (r0 == 0) goto L_0x0094
            int r1 = X.AnonymousClass1Y3.BOE
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1rH r0 = (X.C35471rH) r0
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r3 = (X.C25051Yd) r3
            r0 = 2306127928755098791(0x20010322000e14a7, double:1.5860383032966236E-154)
            boolean r0 = r3.Aem(r0)
            if (r0 == 0) goto L_0x0094
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r7.A0K
            r5 = 0
            if (r0 == 0) goto L_0x0092
            android.net.Uri r1 = r0.A01
        L_0x0032:
            if (r1 == 0) goto L_0x0042
            java.lang.Integer r0 = X.AnonymousClass07B.A0D
            java.lang.String r0 = X.AnonymousClass2SW.A00(r0)
            android.net.Uri r0 = X.C24291Ta.A00(r1, r0)
            java.lang.String r5 = r0.toString()
        L_0x0042:
            r1 = r4
        L_0x0043:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)
            if (r0 == 0) goto L_0x004f
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r5)
            if (r0 != 0) goto L_0x0112
        L_0x004f:
            int r3 = X.AnonymousClass1Y3.BOE
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r3, r0)
            X.1rH r0 = (X.C35471rH) r0
            int r3 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r0.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r3, r2)
            X.1Yd r0 = (X.C25051Yd) r0
            r2 = 2306127928756343989(0x20010322002114b5, double:1.586038303709054E-154)
            boolean r0 = r0.Aem(r2)
            if (r0 == 0) goto L_0x0112
            X.1Sz r4 = new X.1Sz
            android.content.res.Resources r2 = r6.A01
            r0 = 2131825810(0x7f111492, float:1.9284487E38)
            java.lang.String r3 = r2.getString(r0)
            java.lang.String r2 = "instant_game"
            java.lang.String r0 = "play_game"
            r4.<init>(r3, r2, r0)
            X.1Qm r0 = X.C23691Qm.A01
            r4.A01 = r0
            X.2SX r0 = new X.2SX
            r0.<init>(r6, r1, r5)
            r4.A00 = r0
            X.1Qt r0 = new X.1Qt
            r0.<init>(r4)
            return r0
        L_0x0092:
            r1 = r4
            goto L_0x0032
        L_0x0094:
            if (r7 == 0) goto L_0x00b6
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = r7.A0G
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r0 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A0R
            if (r1 == r0) goto L_0x00a0
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r0 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A0e
            if (r1 != r0) goto L_0x00b6
        L_0x00a0:
            r0 = 1
        L_0x00a1:
            if (r0 == 0) goto L_0x00ba
            com.facebook.messaging.model.messages.InstantGameInfoProperties r0 = r6.A01(r7)
            if (r0 == 0) goto L_0x00b4
            java.lang.String r1 = r0.A0A
        L_0x00ab:
            com.facebook.messaging.model.messages.InstantGameInfoProperties r0 = r6.A01(r7)
            if (r0 == 0) goto L_0x00b8
            java.lang.String r5 = r0.A05
            goto L_0x0043
        L_0x00b4:
            r1 = r4
            goto L_0x00ab
        L_0x00b6:
            r0 = 0
            goto L_0x00a1
        L_0x00b8:
            r5 = r4
            goto L_0x0043
        L_0x00ba:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r7.A0S
            boolean r0 = r1.A0N()
            if (r0 == 0) goto L_0x0105
            long r0 = r1.A01
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            com.facebook.user.model.UserKey r1 = com.facebook.user.model.UserKey.A00(r0)
            X.0Tq r0 = r6.A05
            java.lang.Object r0 = r0.get()
            X.0t0 r0 = (X.C14300t0) r0
            com.facebook.user.model.User r1 = r0.A03(r1)
            if (r1 == 0) goto L_0x0105
            boolean r0 = r1.A1Y
            if (r0 == 0) goto L_0x0105
            com.google.common.collect.ImmutableList r0 = r1.A0Z
            boolean r0 = X.C013509w.A02(r0)
            if (r0 != 0) goto L_0x0105
            com.google.common.collect.ImmutableList r1 = r1.A0Z
            java.lang.Object r0 = r1.get(r2)
            com.facebook.messaging.business.common.calltoaction.model.NestedCallToAction r0 = (com.facebook.messaging.business.common.calltoaction.model.NestedCallToAction) r0
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = r0.A00
            if (r0 == 0) goto L_0x0105
            android.net.Uri r1 = r0.A01
            if (r1 != 0) goto L_0x00f8
            android.net.Uri r1 = r0.A00
        L_0x00f8:
            java.lang.String r0 = X.C24291Ta.A02(r1)
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            r0 = r0 ^ 1
            r3 = r1
            if (r0 != 0) goto L_0x0107
        L_0x0105:
            r1 = 0
            r3 = r4
        L_0x0107:
            if (r1 != 0) goto L_0x010c
            r5 = r4
            goto L_0x0042
        L_0x010c:
            java.lang.String r5 = r3.toString()
            goto L_0x0042
        L_0x0112:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TZ.Afj(com.facebook.messaging.model.threads.ThreadSummary):X.1Qt");
    }

    private AnonymousClass1TZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = C04490Ux.A0L(r3);
        this.A04 = C26681bq.A05(r3);
        this.A05 = C14300t0.A02(r3);
        this.A03 = C24291Ta.A01(r3);
        this.A02 = AnonymousClass1GB.A00(r3);
    }
}
