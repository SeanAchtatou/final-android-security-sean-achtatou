package X;

import android.content.Context;
import com.facebook.resources.impl.qt.QTFile;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0pv  reason: invalid class name and case insensitive filesystem */
public final class C12760pv {
    private static volatile C12760pv A00;

    public static final C12760pv A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C12760pv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C12760pv();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static File A01(File file, String str) {
        File file2 = new File(file, str);
        if (!file2.exists() && !file2.mkdir()) {
            throw new AnonymousClass4X5(AnonymousClass08S.A0S("Unable to create ", str, " directory under ", file.getAbsolutePath()));
        } else if (file2.isDirectory()) {
            return file2;
        } else {
            throw new AnonymousClass4X5(AnonymousClass08S.A0P(str, " is not a directory under ", file.getAbsolutePath()));
        }
    }

    public QTFile A02(Context context, int i, String str, String str2) {
        Optional optional;
        ArrayList<QTFile> arrayList = new ArrayList<>();
        ArrayList<QTFile> arrayList2 = new ArrayList<>();
        File[] listFiles = A01(A01(context.getFilesDir(), "strings"), "qt").listFiles();
        if (listFiles != null) {
            for (File A002 : listFiles) {
                try {
                    optional = Optional.of(QTFile.A00(A002));
                } catch (C12770pw | AnonymousClass3AJ unused) {
                    optional = Optional.absent();
                }
                if (optional.isPresent()) {
                    arrayList2.add(optional.get());
                }
            }
        }
        for (QTFile qTFile : arrayList2) {
            if (i == qTFile.A00 && str.equals(qTFile.A04) && str2.equals(qTFile.A05)) {
                arrayList.add(qTFile);
            }
        }
        if (!arrayList.isEmpty()) {
            Preconditions.checkArgument(!arrayList.isEmpty(), "Expecting at least one QT file");
            QTFile qTFile2 = (QTFile) Collections.max(arrayList, new AnonymousClass9OF());
            arrayList.remove(qTFile2);
            try {
                if (qTFile2.A03.equals(C03380Nn.A03(AnonymousClass0q2.A08(qTFile2.A02), "MD5"))) {
                    for (QTFile qTFile3 : arrayList) {
                        qTFile3.A02.delete();
                    }
                    return qTFile2;
                }
                qTFile2.A02.delete();
                throw new C1753386z(i, str, str2);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new C12770pw(String.format(Locale.US, "No QT files found for build %d and locale %s and user %s", Integer.valueOf(i), str, str2));
        }
    }
}
