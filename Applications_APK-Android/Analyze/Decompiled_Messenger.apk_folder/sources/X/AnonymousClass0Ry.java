package X;

import android.util.Base64;
import io.card.payment.BuildConfig;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0Ry  reason: invalid class name */
public final class AnonymousClass0Ry {
    public int A00;
    public int A01;
    public String A02;
    public List A03;
    public List A04;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass0Ry r5 = (AnonymousClass0Ry) obj;
            if (!A00().equals(r5.A00()) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public List A00() {
        if (this.A03 == null) {
            ArrayList arrayList = new ArrayList();
            for (String decode : this.A04) {
                InetAddress inetAddress = null;
                try {
                    inetAddress = InetAddress.getByAddress(Base64.decode(decode, 0));
                } catch (IllegalArgumentException | UnknownHostException unused) {
                }
                if (inetAddress != null) {
                    arrayList.add(inetAddress);
                }
            }
            this.A03 = arrayList;
        }
        return this.A03;
    }

    public int hashCode() {
        return (this.A02.hashCode() * 31) + A00().hashCode();
    }

    public String toString() {
        return "AE{'" + this.A02 + '\'' + ", " + A00().toString() + ", " + this.A01 + ", " + this.A00 + '}';
    }

    public AnonymousClass0Ry() {
        this(BuildConfig.FLAVOR, null, 0, 0);
    }

    public AnonymousClass0Ry(String str, List list, int i, int i2) {
        this.A02 = str;
        this.A03 = list;
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                arrayList.add(Base64.encodeToString(((InetAddress) it.next()).getAddress(), 0));
            }
            this.A04 = arrayList;
        }
        this.A01 = i;
        this.A00 = i2;
    }
}
