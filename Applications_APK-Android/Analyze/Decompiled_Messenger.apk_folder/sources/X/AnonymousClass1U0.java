package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Process;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Preconditions;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1U0  reason: invalid class name */
public final class AnonymousClass1U0 implements AnonymousClass1YQ, C13270r7 {
    public static final Class A0L = AnonymousClass1U0.class;
    private static volatile AnonymousClass1U0 A0M;
    public AnonymousClass0UN A00;
    private ScheduledFuture A01;
    private boolean A02;
    private boolean A03;
    public final Context A04;
    public final Handler A05;
    public final C04460Ut A06;
    public final AnonymousClass0Ud A07;
    public final AnonymousClass00M A08;
    public final QuickPerformanceLogger A09;
    public final AtomicBoolean A0A = new AtomicBoolean(false);
    private final AnonymousClass09P A0B;
    private final C11330mk A0C;
    private final C29311gD A0D = new C29311gD(this);
    private final C29331gF A0E;
    private final Runnable A0F = new AnonymousClass1U1(this, A0L, "stopMqtt");
    private final HashSet A0G = new HashSet();
    private final Set A0H;
    private final ScheduledExecutorService A0I;
    private final C04310Tq A0J;
    private final C04310Tq A0K;

    private synchronized void A01() {
        ScheduledFuture scheduledFuture = this.A01;
        if (scheduledFuture != null) {
            if (!scheduledFuture.isDone()) {
                this.A01.cancel(false);
            }
            this.A01 = null;
        }
    }

    public static synchronized void A02(AnonymousClass1U0 r5) {
        synchronized (r5) {
            ScheduledFuture scheduledFuture = r5.A01;
            if (scheduledFuture == null || scheduledFuture.isDone()) {
                try {
                    r5.A01 = r5.A0I.schedule(r5.A0F, 480000, TimeUnit.MILLISECONDS);
                } catch (Throwable th) {
                    C010708t.A0F(A0L, th, "Failed to schedule stopping service, trying to stop it now", new Object[0]);
                    r5.A0B.CGQ(AnonymousClass06F.A02("MqttPushServiceManager", AnonymousClass08S.A0J("stopServiceDelayed got exception ", th.toString())).A00());
                    AnonymousClass00S.A04(r5.A05, r5.A0F, 1351895259);
                }
            }
        }
        return;
    }

    public String getSimpleName() {
        return "MqttPushServiceManager";
    }

    public synchronized void init() {
        int A032 = C000700l.A03(1540512798);
        if (!this.A03) {
            this.A03 = true;
            Preconditions.checkState(this.A08.A05());
            AnonymousClass00S.A04(this.A05, new AnonymousClass1TN(this), 1696068965);
            IntentFilter intentFilter = new IntentFilter();
            String $const$string = TurboLoader.Locator.$const$string(12);
            intentFilter.addAction($const$string);
            this.A04.registerReceiver(new C06870cD($const$string, new C33551nl(this)), intentFilter, null, this.A05);
            C06600bl BMm = this.A06.BMm();
            BMm.A02("ACTION_MQTT_FORCE_REBIND", new C06530be(this));
            BMm.A02($const$string, new C07610dq(this));
            BMm.A01(this.A05);
            BMm.A00().A00();
        }
        C000700l.A09(-2135534346, A032);
    }

    public void onAppPaused() {
    }

    public static final AnonymousClass1U0 A00(AnonymousClass1XY r5) {
        if (A0M == null) {
            synchronized (AnonymousClass1U0.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0M, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0M = new AnonymousClass1U0(applicationInjector, AnonymousClass1CU.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0M;
    }

    public static void A03(AnonymousClass1U0 r5, Intent intent) {
        A05(r5, "onWakeupBroadcast");
        if (A09(r5)) {
            String $const$string = TurboLoader.Locator.$const$string(49);
            if (intent.hasExtra($const$string)) {
                long longExtra = intent.getLongExtra($const$string, 0);
                C24391Tl r4 = (C24391Tl) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B2Y, r5.A00);
                r4.A06.A03(r4.A01, new Intent().setAction("Orca.EXPIRE_CONNECTION").putExtra(TurboLoader.Locator.$const$string(49), longExtra));
                return;
            }
            A06(r5, "onWakeupBroadcast", intent.getBooleanExtra(AnonymousClass24B.$const$string(AnonymousClass1Y3.A40), false));
            return;
        }
        A02(r5);
    }

    public static void A04(AnonymousClass1U0 r2, String str) {
        if (r2.A09.isMarkerOn(5505203) && !r2.A0G.contains(str)) {
            r2.A0G.add(str);
            r2.A09.markerPoint(5505203, str);
        }
    }

    public static void A05(AnonymousClass1U0 r3, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("event", str);
        hashMap.put("pid", String.valueOf(Process.myPid()));
        if (str.equals("doInit")) {
            hashMap.put("persistent_level", ((C06550bg) r3.A0J.get()).name());
        }
        r3.A0C.BIq("service_manager", hashMap);
    }

    public static void A07(AnonymousClass1U0 r3, boolean z) {
        Preconditions.checkState(r3.A08.A05());
        if (r3.A02 != z) {
            r3.A02 = z;
        }
        if (A09(r3)) {
            A06(r3, "setEnabledForMainProcess", false);
        } else if (z) {
            A02(r3);
        } else {
            r3.A01();
            ((C24391Tl) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B2Y, r3.A00)).A01();
        }
    }

    public static void A08(AnonymousClass1U0 r1, boolean z) {
        if (r1.A08.A05()) {
            A07(r1, z);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0046, code lost:
        if (r5.A07.A0H() == false) goto L_0x0048;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A09(X.AnonymousClass1U0 r5) {
        /*
            boolean r0 = r5.A02
            r1 = 0
            if (r0 == 0) goto L_0x006b
            X.0Tq r0 = r5.A0K
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x006b
            X.0Tq r0 = r5.A0J
            java.lang.Object r1 = r0.get()
            X.0bg r1 = (X.C06550bg) r1
            int r0 = r1.ordinal()
            r4 = 1
            switch(r0) {
                case 0: goto L_0x0030;
                case 1: goto L_0x0068;
                default: goto L_0x001d;
            }
        L_0x001d:
            java.lang.Class r2 = X.AnonymousClass1U0.A0L
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r0 = "Invalid value from HighestMqttPersistenceProvider: %s"
            X.C010708t.A0C(r2, r0, r1)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Invalid MqttServicePersistence"
            r1.<init>(r0)
            throw r1
        L_0x0030:
            java.util.concurrent.atomic.AtomicBoolean r0 = r5.A0A
            boolean r3 = r0.get()
            X.0Ud r0 = r5.A07
            boolean r0 = r0.A0J()
            r2 = 1
            if (r0 == 0) goto L_0x0048
            X.0Ud r0 = r5.A07
            boolean r1 = r0.A0H()
            r0 = 1
            if (r1 != 0) goto L_0x0049
        L_0x0048:
            r0 = 0
        L_0x0049:
            if (r3 != 0) goto L_0x004e
            if (r0 != 0) goto L_0x004e
            r2 = 0
        L_0x004e:
            if (r2 != 0) goto L_0x0068
            java.util.Set r0 = r5.A0H
            java.util.Iterator r1 = r0.iterator()
        L_0x0056:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r1.next()
            X.1gG r0 = (X.C29341gG) r0
            boolean r0 = r0.BDY()
            if (r0 == 0) goto L_0x0056
        L_0x0068:
            return r4
        L_0x0069:
            r4 = 0
            return r4
        L_0x006b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1U0.A09(X.1U0):boolean");
    }

    public void onAppActive() {
        A05(this, "onAppActive");
        A04(this, "appActive");
        this.A0A.set(true);
        if (A09(this)) {
            A06(this, "onAppActive", true);
        }
    }

    public void onAppStopped() {
        A05(this, "onAppStopped");
        this.A0A.set(false);
        if (!A09(this)) {
            A02(this);
        }
    }

    public void onDeviceActive() {
        A05(this, "onDeviceActive");
        if (A09(this)) {
            A06(this, "onDeviceActive", true);
        }
    }

    public void onDeviceStopped() {
        A05(this, "onDeviceStopped");
        if (!A09(this)) {
            A02(this);
        }
    }

    private AnonymousClass1U0(AnonymousClass1XY r4, AnonymousClass1CU r5) {
        this.A00 = new AnonymousClass0UN(2, r4);
        this.A04 = AnonymousClass1YA.A00(r4);
        this.A07 = AnonymousClass0Ud.A00(r4);
        this.A09 = AnonymousClass0ZD.A03(r4);
        this.A0K = AnonymousClass0XJ.A0K(r4);
        this.A0H = new AnonymousClass0X5(r4, AnonymousClass0X6.A2S);
        this.A06 = C04430Uq.A02(r4);
        this.A08 = C04900Wt.A00(r4);
        AnonymousClass0WT.A00(r4);
        this.A0B = C04750Wa.A01(r4);
        this.A05 = C29321gE.A00(r4);
        this.A0E = C29331gF.A00(r4);
        this.A0I = C29351gH.A00(r4);
        this.A0J = AnonymousClass0VG.A00(AnonymousClass1Y3.A5z, r4);
        this.A0C = r5.A01("mqtt_instance");
    }

    public static void A06(AnonymousClass1U0 r3, String str, boolean z) {
        r3.A01();
        if (r3.A09.isMarkerOn(5505203)) {
            r3.A09.markerEnd(5505203, 2);
        }
        C24391Tl r32 = (C24391Tl) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B2Y, r3.A00);
        if (r32.A00) {
            Intent intent = new Intent();
            if (z) {
                intent.setAction("Orca.PERSISTENT_KICK_SKIP_PING");
            } else {
                intent.setAction(TurboLoader.Locator.$const$string(123));
            }
            intent.putExtra("caller", str);
            r32.A06.A03(r32.A01, intent);
        } else if (r32.A06.A02(r32.A01, new Intent(), r32.A02, 1).A00 != null) {
            r32.A00 = true;
            AnonymousClass00S.A04(r32.A03, new C34441pa(r32), -1399533619);
        }
    }

    public void A0A() {
        boolean andSet;
        init();
        C29331gF r7 = this.A0E;
        C29311gD r6 = this.A0D;
        synchronized (r7) {
            int i = r7.A00 + 1;
            r7.A00 = i;
            andSet = r7.A03.getAndSet(true);
            ScheduledFuture scheduledFuture = r7.A01;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(false);
            }
            r7.A01 = r7.A02.schedule(new AnonymousClass3RX(r7, i, r6), 60, TimeUnit.SECONDS);
        }
        if (!andSet) {
            AnonymousClass00S.A04(this.A05, new AnonymousClass3RY(this), -1385097801);
        }
    }
}
