package X;

import android.app.Activity;
import android.content.Context;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;

/* renamed from: X.20e  reason: invalid class name and case insensitive filesystem */
public final class C400720e extends C114615cW {
    public final /* synthetic */ C148676vB A00;

    public C400720e(C148676vB r1) {
        this.A00 = r1;
    }

    public void BVr(InboxUnitThreadItem inboxUnitThreadItem) {
        String str;
        Context A1j = this.A00.A1j();
        if (A1j != null) {
            int i = AnonymousClass1Y3.B24;
            C148676vB r1 = this.A00;
            C45072Kr r2 = (C45072Kr) AnonymousClass1XX.A02(4, i, r1.A01);
            ThreadSummary threadSummary = inboxUnitThreadItem.A01;
            C13060qW A17 = r1.A17();
            if (this.A00.A06 == C10950l8.A0A) {
                str = C05360Yq.$const$string(AnonymousClass1Y3.A4n);
            } else {
                str = "other";
            }
            r2.A04(threadSummary, A17, A1j, str, false);
        }
    }

    public void Bbw(InboxUnitItem inboxUnitItem) {
        if (inboxUnitItem instanceof InboxUnitThreadItem) {
            ThreadSummary threadSummary = ((InboxUnitThreadItem) inboxUnitItem).A01;
            int i = AnonymousClass1Y3.Avd;
            C148676vB r1 = this.A00;
            C91414Ya r5 = (C91414Ya) AnonymousClass1XX.A02(2, i, r1.A01);
            ThreadKey threadKey = threadSummary.A0S;
            C10950l8 r3 = r1.A06;
            Activity A2J = r1.A2J();
            String A002 = C148676vB.A00(this.A00);
            if (r3 == null) {
                r3 = C10950l8.A07;
            }
            r5.A00.A04(threadKey, r3, A002);
            r5.A01.A03(threadKey, "message_requests");
            if (A2J != null) {
                A2J.overridePendingTransition(2130772075, 2130772077);
            }
        }
    }

    public void Bfh(InboxUnitThreadItem inboxUnitThreadItem) {
        Context A1j = this.A00.A1j();
        if (A1j != null) {
            C148676vB r3 = this.A00;
            r3.A08 = new AnonymousClass1B2((C20101Ax) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AgA, r3.A01), CallerContext.A04(C148676vB.class), this.A00.A2J(), A1j, this.A00.A17(), null);
            C148676vB r0 = this.A00;
            r0.A08.A05(inboxUnitThreadItem, r0.A17(), new C123435rc());
            C148676vB r02 = this.A00;
            r02.A08.A03 = r02.A0B;
        }
    }
}
