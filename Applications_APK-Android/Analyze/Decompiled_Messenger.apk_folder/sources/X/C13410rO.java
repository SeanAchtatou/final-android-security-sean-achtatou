package X;

import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0rO  reason: invalid class name and case insensitive filesystem */
public final class C13410rO {
    private static volatile C13410rO A02;
    public String A00 = BuildConfig.FLAVOR;
    public final C24361Ti A01;

    public static final C13410rO A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C13410rO.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C13410rO(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C13410rO(AnonymousClass1XY r2) {
        this.A01 = C24361Ti.A01(r2);
    }
}
