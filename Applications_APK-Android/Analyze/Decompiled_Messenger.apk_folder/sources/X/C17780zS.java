package X;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.Layout;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.ContextChain;
import com.facebook.common.dextricks.DexStore;
import com.facebook.forker.Process;
import com.facebook.graphql.enums.GraphQLMinutiaeBubblePosition;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.ipc.stories.model.StoryBucket;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.ComponentHost;
import com.facebook.litho.ComponentTree;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.payments.ui.PaymentFormEditTextView;
import com.facebook.resources.ui.FbAutoFitTextView;
import com.facebook.resources.ui.FbFrameLayout;
import com.facebook.widget.CustomFrameLayout;
import io.card.payment.BuildConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0zS  reason: invalid class name and case insensitive filesystem */
public abstract class C17780zS implements C17790zT, C17800zU {
    public static final AnonymousClass119 A01 = new C31501jn();
    public static final int A02 = -1048037474;
    private static final Map A03 = new HashMap();
    private static final AtomicInteger A04 = new AtomicInteger();
    public final int A00;

    public int A0L() {
        if (!(this instanceof C14910uL)) {
            return 0;
        }
        C14910uL r0 = (C14910uL) this;
        boolean z = r0.A0g;
        ClickableSpan[] clickableSpanArr = r0.A0m;
        if (!z || clickableSpanArr == null) {
            return 0;
        }
        return clickableSpanArr.length;
    }

    public int A0M() {
        return 3;
    }

    public int A0N(int i, int i2) {
        if (!(this instanceof C14910uL)) {
            return Integer.MIN_VALUE;
        }
        C14910uL r0 = (C14910uL) this;
        CharSequence charSequence = r0.A0c;
        Layout layout = r0.A0T;
        ClickableSpan[] clickableSpanArr = r0.A0m;
        if (charSequence instanceof Spanned) {
            Spanned spanned = (Spanned) charSequence;
            for (int i3 = 0; i3 < clickableSpanArr.length; i3++) {
                ClickableSpan clickableSpan = clickableSpanArr[i3];
                layout.getSelectionPath(spanned.getSpanStart(clickableSpan), spanned.getSpanEnd(clickableSpan), C14910uL.A0q);
                C14910uL.A0q.computeBounds(C14910uL.A0s, true);
                if (C14910uL.A0s.contains((float) i, (float) i2)) {
                    return i3;
                }
            }
        }
        return Integer.MIN_VALUE;
    }

    public C17760zQ A0S(AnonymousClass0p4 r2) {
        return null;
    }

    public AnonymousClass1KE A0T(AnonymousClass0p4 r8, AnonymousClass1KE r9) {
        if (!(this instanceof C21751Io)) {
            return r9;
        }
        C21751Io r5 = (C21751Io) this;
        AnonymousClass1KE A002 = AnonymousClass1KE.A00(r9);
        A002.A02(ThreadSummary.class, r5.A01.A01);
        A002.A02(InboxUnitThreadItem.class, r5.A01);
        Class<C16400x0> cls = C16400x0.class;
        InboxUnitThreadItem inboxUnitThreadItem = r5.A01;
        C35671rb r1 = (C35671rb) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AN2, r5.A00);
        A002.A02(cls, C21761Ip.A00(inboxUnitThreadItem.A01.A0B() ? r1.A01() ? AnonymousClass07B.A0C : AnonymousClass07B.A0N : r1.A01() ? AnonymousClass07B.A00 : AnonymousClass07B.A01));
        A002.A02(BasicMontageThreadInfo.class, r5.A01.A02);
        return A002;
    }

    public void A0X(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        if (this instanceof AnonymousClass1KX) {
            accessibilityNodeInfoCompat.A0J(C99084oO.$const$string(2));
        } else if (this instanceof C14910uL) {
            C14910uL r0 = (C14910uL) this;
            CharSequence charSequence = r0.A0c;
            boolean z = r0.A0j;
            if (C15320v6.getImportantForAccessibility(view) == 0) {
                C15320v6.setImportantForAccessibility(view, 1);
            }
            CharSequence contentDescription = accessibilityNodeInfoCompat.A02.getContentDescription();
            CharSequence charSequence2 = charSequence;
            if (contentDescription != null) {
                charSequence2 = contentDescription;
            }
            accessibilityNodeInfoCompat.A02.setText(charSequence2);
            if (contentDescription == null) {
                contentDescription = charSequence;
            }
            accessibilityNodeInfoCompat.A02.setContentDescription(contentDescription);
            accessibilityNodeInfoCompat.A09(DexStore.LOAD_RESULT_OATMEAL_QUICKENED);
            accessibilityNodeInfoCompat.A09(512);
            accessibilityNodeInfoCompat.A0A(11);
            if (!z) {
                accessibilityNodeInfoCompat.A0W(true);
            }
        }
    }

    public void A0Y(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, int i, int i2, int i3) {
        if (this instanceof C14910uL) {
            C14910uL r0 = (C14910uL) this;
            CharSequence charSequence = r0.A0c;
            Layout layout = r0.A0T;
            ClickableSpan[] clickableSpanArr = r0.A0m;
            if (charSequence instanceof Spanned) {
                Spanned spanned = (Spanned) charSequence;
                ClickableSpan clickableSpan = clickableSpanArr[i];
                int spanStart = spanned.getSpanStart(clickableSpan);
                int spanEnd = spanned.getSpanEnd(clickableSpan);
                int lineForOffset = layout.getLineForOffset(spanStart);
                int lineVisibleEnd = lineForOffset == layout.getLineForOffset(spanEnd) ? spanEnd : layout.getLineVisibleEnd(lineForOffset);
                Path path = C14910uL.A0q;
                layout.getSelectionPath(spanStart, lineVisibleEnd, path);
                RectF rectF = C14910uL.A0s;
                path.computeBounds(rectF, true);
                Rect rect = C14910uL.A0r;
                rect.set(((int) rectF.left) + i2, ((int) rectF.top) + i3, i2 + ((int) rectF.right), i3 + ((int) rectF.bottom));
                if (rect.isEmpty()) {
                    rect.set(0, 0, 1, 1);
                    accessibilityNodeInfoCompat.A02.setBoundsInParent(rect);
                    accessibilityNodeInfoCompat.A02.setContentDescription(BuildConfig.FLAVOR);
                    return;
                }
                accessibilityNodeInfoCompat.A02.setBoundsInParent(rect);
                accessibilityNodeInfoCompat.A0S(true);
                accessibilityNodeInfoCompat.A02.setFocusable(true);
                accessibilityNodeInfoCompat.A02.setEnabled(true);
                accessibilityNodeInfoCompat.A0Z(true);
                if (clickableSpan instanceof AnonymousClass69S) {
                    AnonymousClass69S r4 = (AnonymousClass69S) clickableSpan;
                    accessibilityNodeInfoCompat.A02.setText(r4.A00);
                    String str = r4.A01;
                    if (str != null) {
                        accessibilityNodeInfoCompat.A0J(str);
                    } else {
                        accessibilityNodeInfoCompat.A0J("android.widget.Button");
                    }
                } else {
                    accessibilityNodeInfoCompat.A02.setText(spanned.subSequence(spanStart, spanEnd));
                    accessibilityNodeInfoCompat.A0J("android.widget.Button");
                }
            }
        }
    }

    public void A0Z(AnonymousClass0p4 r16) {
        if (this instanceof AnonymousClass1KX) {
            AnonymousClass1KX r4 = (AnonymousClass1KX) this;
            C23871Rg r3 = new C23871Rg();
            r3.A00((C22151Kc) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B8X, r4.A09));
            r4.A0A.userTileDrawableCachingBuilder = (C22151Kc) r3.A00;
        } else if (this instanceof C31971kt) {
            C31971kt r2 = (C31971kt) this;
            C23871Rg r1 = new C23871Rg();
            r1.A00(true);
            Object obj = r1.A00;
            if (obj != null) {
                r2.A06.storiesTrayAccessible = ((Boolean) obj).booleanValue();
            }
        } else if (this instanceof C37311v4) {
            C37311v4 r22 = (C37311v4) this;
            C23871Rg r12 = new C23871Rg();
            r12.A00(C860646n.STICKER_OVERLAY);
            Object obj2 = r12.A00;
            if (obj2 != null) {
                r22.A05.selectedOverlay = (C860646n) obj2;
            }
        } else if (this instanceof AnonymousClass1v5) {
            C23871Rg r23 = new C23871Rg();
            r23.A00(true);
            ((AnonymousClass1v5) this).A05.shouldAnimateScale = ((Boolean) r23.A00).booleanValue();
        } else if (this instanceof AnonymousClass1v6) {
            AnonymousClass1v6 r24 = (AnonymousClass1v6) this;
            C23871Rg r13 = new C23871Rg();
            r13.A00(0);
            Object obj3 = r13.A00;
            if (obj3 != null) {
                r24.A05.affordanceAnimationState = ((Integer) obj3).intValue();
            }
        } else if (this instanceof AnonymousClass1JJ) {
            AnonymousClass1JJ r42 = (AnonymousClass1JJ) this;
            C23871Rg r10 = new C23871Rg();
            C23871Rg r9 = new C23871Rg();
            C23871Rg r7 = new C23871Rg();
            C23871Rg r5 = new C23871Rg();
            String str = r42.A04;
            String str2 = r42.A05;
            String str3 = r42.A06;
            Boolean bool = r42.A02;
            Boolean bool2 = r42.A03;
            C45722Nf r0 = new C45722Nf();
            r0.A00 = str;
            boolean booleanValue = bool.booleanValue();
            r0.A01 = booleanValue;
            r10.A00(r0);
            C45722Nf r02 = new C45722Nf();
            r02.A00 = str2;
            r02.A01 = booleanValue;
            r9.A00(r02);
            C45722Nf r03 = new C45722Nf();
            r03.A00 = str3;
            r03.A01 = booleanValue;
            r7.A00(r03);
            r5.A00(bool2);
            Object obj4 = r10.A00;
            if (obj4 != null) {
                r42.A01.currentPasswordContainer = (C45722Nf) obj4;
            }
            Object obj5 = r9.A00;
            if (obj5 != null) {
                r42.A01.newPasswordContainer = (C45722Nf) obj5;
            }
            Object obj6 = r7.A00;
            if (obj6 != null) {
                r42.A01.retypedPasswordContainer = (C45722Nf) obj6;
            }
            Object obj7 = r5.A00;
            if (obj7 != null) {
                r42.A01.saveButtonEnabled = ((Boolean) obj7).booleanValue();
            }
        } else if (this instanceof AnonymousClass1JO) {
            AnonymousClass1JO r32 = (AnonymousClass1JO) this;
            C23871Rg r25 = new C23871Rg();
            String str4 = r32.A04;
            C45722Nf r04 = new C45722Nf();
            if (str4 == null) {
                str4 = BuildConfig.FLAVOR;
            }
            r04.A00 = str4;
            r25.A00(r04);
            Object obj8 = r25.A00;
            if (obj8 != null) {
                r32.A02.stateContainer = (C45722Nf) obj8;
            }
        } else if (this instanceof AnonymousClass1JE) {
            AnonymousClass1JE r14 = (AnonymousClass1JE) this;
            C23871Rg r43 = new C23871Rg();
            C23871Rg r33 = new C23871Rg();
            C23871Rg r26 = new C23871Rg();
            Uri uri = r14.A01;
            AnonymousClass3JC r11 = r14.A08;
            C27482DdQ ddQ = r14.A04;
            CallerContext callerContext = r14.A02;
            C27495Ddd ddd = r14.A06;
            AnonymousClass852 r52 = (AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, r14.A09);
            CallerContext A012 = CallerContext.A01(callerContext, r14.A03);
            r33.A00(A012);
            if (!r52.A00(A012, ddQ).A04.A0V()) {
                C22013Amj amj = r52.A00(A012, ddQ).A00;
                if (r11 == null) {
                    r11 = AnonymousClass3JC.A0G;
                }
                r43.A00(amj.AW7(uri, null, r11, A012, r16.A03(), ddd));
            }
            r26.A00(new AtomicReference());
            C156827Mz r15 = r14.A07;
            r15.lastFrescoState = (C27481DdP) r43.A00;
            r15.callerContextWithContextChain = (CallerContext) r33.A00;
            r15.prefetchData = (AtomicReference) r26.A00;
        }
    }

    public void A0a(AnonymousClass0p4 r6) {
        if (this instanceof AnonymousClass1JE) {
            AnonymousClass1JE r4 = (AnonymousClass1JE) this;
            C27482DdQ ddQ = r4.A04;
            C156827Mz r0 = r4.A07;
            C27481DdP ddP = r0.frescoState;
            C27482DdQ A002 = ((AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, r4.A09)).A00(r0.callerContextWithContextChain, ddQ);
            if (A002.A04.A0O() && ddP != null) {
                A002.A00.BW6(ddP);
            }
        }
    }

    public void A0b(AnonymousClass0p4 r58) {
        AnonymousClass0p4 r0 = r58;
        if (this instanceof C21911Je) {
            C21911Je r1 = (C21911Je) this;
            AnonymousClass1JP r12 = new AnonymousClass1JP();
            AnonymousClass1JP r10 = new AnonymousClass1JP();
            AnonymousClass1JP r9 = new AnonymousClass1JP();
            AnonymousClass1JP r8 = new AnonymousClass1JP();
            AnonymousClass1JP r7 = new AnonymousClass1JP();
            AnonymousClass1JP r6 = new AnonymousClass1JP();
            AnonymousClass1JP r5 = new AnonymousClass1JP();
            AnonymousClass1JP r4 = new AnonymousClass1JP();
            AnonymousClass1JP r3 = new AnonymousClass1JP();
            AnonymousClass1JP r2 = new AnonymousClass1JP();
            C27041cY.A01("onLoadStyle");
            TypedArray A042 = r0.A04(C008407o.A2r, 0);
            r7.A00(A042.getDrawable(0));
            A042.recycle();
            TypedArray A043 = r0.A04(C008407o.A2q, 0);
            r10.A00(Integer.valueOf(A043.getDimensionPixelSize(4, 0)));
            r12.A00(Boolean.valueOf(A043.getBoolean(0, false)));
            r3.A00(Float.valueOf(A043.getDimension(1, 0.0f)));
            r8.A00(Boolean.valueOf(A043.getBoolean(3, false)));
            r9.A00(Integer.valueOf(A043.getColor(2, C21901Jd.A0Q)));
            A043.recycle();
            TypedArray A044 = r0.A04(C008407o.A0A, 0);
            r2.A00(AnonymousClass3DL.A00(r0.A09, A044));
            A044.recycle();
            TypedArray A045 = r0.A04(C008407o.A2p, 0);
            r6.A00(AnonymousClass1KU.values()[A045.getInt(2, AnonymousClass1KU.ONE_LETTER.ordinal())]);
            r5.A00(Integer.valueOf(A045.getColor(1, AnonymousClass01R.A00(r0.A09, 2132083163))));
            r4.A00(Float.valueOf((float) A045.getDimensionPixelSize(0, C007106r.A04(r0.A03(), 2132148404))));
            A045.recycle();
            C27041cY.A00();
            Object obj = r12.A00;
            if (obj != null) {
                r1.A0E = ((Boolean) obj).booleanValue();
            }
            Object obj2 = r10.A00;
            if (obj2 != null) {
                r1.A06 = ((Integer) obj2).intValue();
            }
            Object obj3 = r9.A00;
            if (obj3 != null) {
                r1.A05 = ((Integer) obj3).intValue();
            }
            Object obj4 = r8.A00;
            if (obj4 != null) {
                r1.A0G = ((Boolean) obj4).booleanValue();
            }
            Object obj5 = r7.A00;
            if (obj5 != null) {
                r1.A0A = (Drawable) obj5;
            }
            Object obj6 = r6.A00;
            if (obj6 != null) {
                r1.A0D = (AnonymousClass1KU) obj6;
            }
            Object obj7 = r5.A00;
            if (obj7 != null) {
                r1.A07 = ((Integer) obj7).intValue();
            }
            Object obj8 = r4.A00;
            if (obj8 != null) {
                r1.A01 = ((Float) obj8).floatValue();
            }
            Object obj9 = r3.A00;
            if (obj9 != null) {
                r1.A00 = ((Float) obj9).floatValue();
            }
            Object obj10 = r2.A00;
            if (obj10 != null) {
                r1.A0C = (C22131Ka) obj10;
            }
        } else if (this instanceof C14910uL) {
            C14910uL r15 = (C14910uL) this;
            AnonymousClass1JP r28 = new AnonymousClass1JP();
            AnonymousClass1JP r27 = new AnonymousClass1JP();
            AnonymousClass1JP r26 = new AnonymousClass1JP();
            AnonymousClass1JP r25 = new AnonymousClass1JP();
            AnonymousClass1JP r24 = new AnonymousClass1JP();
            AnonymousClass1JP r23 = new AnonymousClass1JP();
            AnonymousClass1JP r22 = new AnonymousClass1JP();
            AnonymousClass1JP r21 = new AnonymousClass1JP();
            AnonymousClass1JP r20 = new AnonymousClass1JP();
            AnonymousClass1JP r19 = new AnonymousClass1JP();
            AnonymousClass1JP r18 = new AnonymousClass1JP();
            AnonymousClass1JP r17 = new AnonymousClass1JP();
            AnonymousClass1JP r16 = new AnonymousClass1JP();
            AnonymousClass1JP r14 = new AnonymousClass1JP();
            AnonymousClass1JP r13 = new AnonymousClass1JP();
            AnonymousClass1JP r122 = new AnonymousClass1JP();
            AnonymousClass1JP r11 = new AnonymousClass1JP();
            AnonymousClass1JP r102 = new AnonymousClass1JP();
            AnonymousClass1JP r92 = new AnonymousClass1JP();
            AnonymousClass1JP r82 = new AnonymousClass1JP();
            AnonymousClass1JP r72 = new AnonymousClass1JP();
            AnonymousClass1JP r62 = new AnonymousClass1JP();
            AnonymousClass1JP r52 = new AnonymousClass1JP();
            AnonymousClass1JP r42 = new AnonymousClass1JP();
            AnonymousClass1JP r32 = new AnonymousClass1JP();
            AnonymousClass1JP r29 = new AnonymousClass1JP();
            AnonymousClass1JP r110 = new AnonymousClass1JP();
            C70423ac.A02(r0, r28, r27, r26, r25, r24, r23, r22, r21, r20, r19, r18, r17, r16, r14, r13, r122, r11, r102, r92, r82, r72, r62, r52, r42, r32, r29, r110);
            Object obj11 = r28.A00;
            if (obj11 != null) {
                r15.A0U = (TextUtils.TruncateAt) obj11;
            }
            Object obj12 = r27.A00;
            if (obj12 != null) {
                r15.A01 = ((Float) obj12).floatValue();
            }
            Object obj13 = r26.A00;
            if (obj13 != null) {
                r15.A0l = ((Boolean) obj13).booleanValue();
            }
            Object obj14 = r25.A00;
            if (obj14 != null) {
                r15.A06 = ((Float) obj14).floatValue();
            }
            Object obj15 = r24.A00;
            if (obj15 != null) {
                r15.A0I = ((Integer) obj15).intValue();
            }
            Object obj16 = r23.A00;
            if (obj16 != null) {
                r15.A0F = ((Integer) obj16).intValue();
            }
            Object obj17 = r22.A00;
            if (obj17 != null) {
                r15.A0H = ((Integer) obj17).intValue();
            }
            Object obj18 = r21.A00;
            if (obj18 != null) {
                r15.A0E = ((Integer) obj18).intValue();
            }
            Object obj19 = r20.A00;
            if (obj19 != null) {
                r15.A0J = ((Integer) obj19).intValue();
            }
            Object obj20 = r19.A00;
            if (obj20 != null) {
                r15.A0G = ((Integer) obj20).intValue();
            }
            Object obj21 = r18.A00;
            if (obj21 != null) {
                r15.A0j = ((Boolean) obj21).booleanValue();
            }
            Object obj22 = r17.A00;
            if (obj22 != null) {
                r15.A0c = (CharSequence) obj22;
            }
            Object obj23 = r16.A00;
            if (obj23 != null) {
                r15.A0P = (ColorStateList) obj23;
            }
            Object obj24 = r14.A00;
            if (obj24 != null) {
                r15.A0D = ((Integer) obj24).intValue();
            }
            Object obj25 = r13.A00;
            if (obj25 != null) {
                r15.A08 = ((Integer) obj25).intValue();
            }
            Object obj26 = r122.A00;
            if (obj26 != null) {
                r15.A0N = ((Integer) obj26).intValue();
            }
            Object obj27 = r11.A00;
            if (obj27 != null) {
                r15.A0X = (AnonymousClass1JS) obj27;
            }
            Object obj28 = r102.A00;
            if (obj28 != null) {
                r15.A07 = ((Integer) obj28).intValue();
            }
            Object obj29 = r92.A00;
            if (obj29 != null) {
                r15.A0B = ((Integer) obj29).intValue();
            }
            Object obj30 = r82.A00;
            if (obj30 != null) {
                r15.A0C = ((Integer) obj30).intValue();
            }
            Object obj31 = r72.A00;
            if (obj31 != null) {
                r15.A0O = ((Integer) obj31).intValue();
            }
            Object obj32 = r62.A00;
            if (obj32 != null) {
                r15.A05 = ((Float) obj32).floatValue();
            }
            Object obj33 = r52.A00;
            if (obj33 != null) {
                r15.A03 = ((Float) obj33).floatValue();
            }
            Object obj34 = r42.A00;
            if (obj34 != null) {
                r15.A04 = ((Float) obj34).floatValue();
            }
            Object obj35 = r32.A00;
            if (obj35 != null) {
                r15.A0L = ((Integer) obj35).intValue();
            }
            Object obj36 = r29.A00;
            if (obj36 != null) {
                r15.A0Y = (C14920uM) obj36;
            }
            Object obj37 = r110.A00;
            if (obj37 != null) {
                r15.A0Q = (Typeface) obj37;
            }
        } else if (this instanceof AnonymousClass10H) {
            AnonymousClass10H r73 = (AnonymousClass10H) this;
            AnonymousClass1JP r93 = new AnonymousClass1JP();
            AnonymousClass1JP r83 = new AnonymousClass1JP();
            TypedArray A046 = r0.A04(C636538d.A02, 0);
            int indexCount = A046.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = A046.getIndex(i);
                if (index == 0) {
                    r93.A00(r0.A09.getResources().getDrawable(A046.getResourceId(index, 0)));
                } else if (index == 1) {
                    r83.A00(AnonymousClass10H.A05[A046.getInteger(index, -1)]);
                }
            }
            A046.recycle();
            Object obj38 = r93.A00;
            if (obj38 != null) {
                r73.A00 = (Drawable) obj38;
            }
            Object obj39 = r83.A00;
            if (obj39 != null) {
                r73.A01 = (ImageView.ScaleType) obj39;
            }
        } else if (this instanceof C22331Kx) {
            C22331Kx r152 = (C22331Kx) this;
            AnonymousClass1JP r282 = new AnonymousClass1JP();
            AnonymousClass1JP r272 = new AnonymousClass1JP();
            AnonymousClass1JP r262 = new AnonymousClass1JP();
            AnonymousClass1JP r252 = new AnonymousClass1JP();
            AnonymousClass1JP r242 = new AnonymousClass1JP();
            AnonymousClass1JP r232 = new AnonymousClass1JP();
            AnonymousClass1JP r222 = new AnonymousClass1JP();
            AnonymousClass1JP r212 = new AnonymousClass1JP();
            AnonymousClass1JP r202 = new AnonymousClass1JP();
            AnonymousClass1JP r192 = new AnonymousClass1JP();
            AnonymousClass1JP r182 = new AnonymousClass1JP();
            AnonymousClass1JP r172 = new AnonymousClass1JP();
            AnonymousClass1JP r162 = new AnonymousClass1JP();
            AnonymousClass1JP r142 = new AnonymousClass1JP();
            AnonymousClass1JP r132 = new AnonymousClass1JP();
            AnonymousClass1JP r123 = new AnonymousClass1JP();
            AnonymousClass1JP r112 = new AnonymousClass1JP();
            AnonymousClass1JP r103 = new AnonymousClass1JP();
            AnonymousClass1JP r94 = new AnonymousClass1JP();
            AnonymousClass1JP r84 = new AnonymousClass1JP();
            AnonymousClass1JP r74 = new AnonymousClass1JP();
            AnonymousClass1JP r63 = new AnonymousClass1JP();
            AnonymousClass1JP r53 = new AnonymousClass1JP();
            AnonymousClass1JP r43 = new AnonymousClass1JP();
            AnonymousClass1JP r33 = new AnonymousClass1JP();
            AnonymousClass1JP r210 = new AnonymousClass1JP();
            AnonymousClass1JP r111 = new AnonymousClass1JP();
            C70423ac.A02(r0, r282, r272, r262, r252, r242, r232, r222, r212, r202, r192, r182, r172, r162, r142, r132, r123, r112, r103, r94, r84, r74, r63, r53, r43, r33, r210, r111);
            Object obj40 = r282.A00;
            if (obj40 != null) {
                r152.A0N = (TextUtils.TruncateAt) obj40;
            }
            Object obj41 = r272.A00;
            if (obj41 != null) {
                r152.A00 = ((Float) obj41).floatValue();
            }
            Object obj42 = r262.A00;
            if (obj42 != null) {
                r152.A0W = ((Boolean) obj42).booleanValue();
            }
            Object obj43 = r252.A00;
            if (obj43 != null) {
                r152.A04 = ((Float) obj43).floatValue();
            }
            Object obj44 = r242.A00;
            if (obj44 != null) {
                r152.A0E = ((Integer) obj44).intValue();
            }
            Object obj45 = r232.A00;
            if (obj45 != null) {
                r152.A0B = ((Integer) obj45).intValue();
            }
            Object obj46 = r222.A00;
            if (obj46 != null) {
                r152.A0D = ((Integer) obj46).intValue();
            }
            Object obj47 = r212.A00;
            if (obj47 != null) {
                r152.A0A = ((Integer) obj47).intValue();
            }
            Object obj48 = r202.A00;
            if (obj48 != null) {
                r152.A0F = ((Integer) obj48).intValue();
            }
            Object obj49 = r192.A00;
            if (obj49 != null) {
                r152.A0C = ((Integer) obj49).intValue();
            }
            Object obj50 = r182.A00;
            if (obj50 != null) {
                r152.A0V = ((Boolean) obj50).booleanValue();
            }
            Object obj51 = r172.A00;
            if (obj51 != null) {
                r152.A0S = (CharSequence) obj51;
            }
            Object obj52 = r162.A00;
            if (obj52 != null) {
                r152.A0K = (ColorStateList) obj52;
            }
            Object obj53 = r142.A00;
            if (obj53 != null) {
                r152.A09 = ((Integer) obj53).intValue();
            }
            Object obj54 = r132.A00;
            if (obj54 != null) {
                r152.A06 = ((Integer) obj54).intValue();
            }
            Object obj55 = r123.A00;
            if (obj55 != null) {
                r152.A0I = ((Integer) obj55).intValue();
            }
            Object obj56 = r112.A00;
            if (obj56 != null) {
                r152.A0Q = (AnonymousClass1JS) obj56;
            }
            Object obj57 = r103.A00;
            if (obj57 != null) {
                r152.A05 = ((Integer) obj57).intValue();
            }
            Object obj58 = r94.A00;
            if (obj58 != null) {
                r152.A07 = ((Integer) obj58).intValue();
            }
            Object obj59 = r84.A00;
            if (obj59 != null) {
                r152.A08 = ((Integer) obj59).intValue();
            }
            Object obj60 = r74.A00;
            if (obj60 != null) {
                r152.A0J = ((Integer) obj60).intValue();
            }
            Object obj61 = r63.A00;
            if (obj61 != null) {
                r152.A03 = ((Float) obj61).floatValue();
            }
            Object obj62 = r53.A00;
            if (obj62 != null) {
                r152.A01 = ((Float) obj62).floatValue();
            }
            Object obj63 = r43.A00;
            if (obj63 != null) {
                r152.A02 = ((Float) obj63).floatValue();
            }
            Object obj64 = r33.A00;
            if (obj64 != null) {
                r152.A0G = ((Integer) obj64).intValue();
            }
            Object obj65 = r210.A00;
            if (obj65 != null) {
                r152.A0R = (C14920uM) obj65;
            }
            Object obj66 = r111.A00;
            if (obj66 != null) {
                r152.A0L = (Typeface) obj66;
            }
        }
    }

    public void A0c(AnonymousClass0p4 r15) {
        GSTModelShape1S0000000 gSTModelShape1S0000000;
        GSTModelShape1S0000000 gSTModelShape1S00000002;
        AnonymousClass1PS r8;
        if (this instanceof AnonymousClass1v5) {
            AnonymousClass1v5 r4 = (AnonymousClass1v5) this;
            AnonymousClass1JP r3 = new AnonymousClass1JP();
            C73513gF r82 = r4.A02;
            int i = AnonymousClass1Y3.AF0;
            AnonymousClass0UN r5 = r4.A00;
            AnonymousClass1MZ r2 = (AnonymousClass1MZ) AnonymousClass1XX.A02(2, i, r5);
            C001500z r6 = (C001500z) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BH4, r5);
            Class<GSTModelShape1S0000000> cls = GSTModelShape1S0000000.class;
            GSTModelShape1S0000000 gSTModelShape1S00000003 = (GSTModelShape1S0000000) r82.A0J(-1386324491, cls, 1751183580);
            if (!(gSTModelShape1S00000003 == null || (gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSTModelShape1S00000003.A0J(3226745, cls, -1945658519)) == null || (gSTModelShape1S00000002 = (GSTModelShape1S0000000) gSTModelShape1S0000000.A0J(100313435, cls, 848589908)) == null)) {
                String A0P = r82.A0P(-1902891955);
                String A0P2 = r82.A0P(-1277195731);
                GraphQLMinutiaeBubblePosition A0Q = r82.A0Q();
                String str = null;
                if (!(A0Q == null || A0Q == GraphQLMinutiaeBubblePosition.A01)) {
                    str = A0Q.toString();
                }
                String A41 = gSTModelShape1S00000002.A41();
                if (r6 == C001500z.A0D) {
                    r8 = AnonymousClass1PS.A02(BitmapFactory.decodeResource(r15.A09.getResources(), 2131230771), new AnonymousClass8BW());
                } else {
                    r8 = null;
                    if (A41 != null) {
                        AnonymousClass1QO A022 = r2.A02(AnonymousClass1Q0.A01(A41), AnonymousClass239.A00);
                        try {
                            AnonymousClass1PS r1 = (AnonymousClass1PS) C77543nP.A01(A022);
                            if (r1 != null) {
                                AnonymousClass1PS A05 = ((AnonymousClass1S5) ((AnonymousClass1S9) r1.A0A())).A05();
                                r1.close();
                                A022.AT6();
                                r8 = A05;
                            } else {
                                A022.AT6();
                            }
                        } catch (Throwable unused) {
                            A022.AT6();
                        }
                    }
                }
                if (((r8 != null && r8.A0B()) || AnonymousClass0VW.A00()) && !TextUtils.isEmpty(A0P) && !TextUtils.isEmpty(A0P2) && str != null) {
                    r3.A00(new AnonymousClass5XZ(r15.A09, r8, A0P, A0P2, str, false));
                    AnonymousClass1PS.A05(r8);
                }
            }
            r4.A01 = (AnonymousClass5XZ) r3.A00;
        } else if (this instanceof AnonymousClass1vC) {
            AnonymousClass1vC r42 = (AnonymousClass1vC) this;
            AnonymousClass1JP r52 = new AnonymousClass1JP();
            GSTModelShape1S0000000 gSTModelShape1S00000004 = r42.A03;
            int i2 = AnonymousClass1Y3.B3Z;
            AnonymousClass0UN r32 = r42.A04;
            C208169ri r22 = (C208169ri) AnonymousClass1XX.A02(1, i2, r32);
            C61832zZ r33 = (C61832zZ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCv, r32);
            r22.A02 = "story_feedback";
            r22.A00 = gSTModelShape1S00000004.A3m();
            r22.A01 = gSTModelShape1S00000004.A0P(-1934345189);
            C23408Bea A012 = r22.A01();
            r33.A0Q(AnonymousClass1vC.A08);
            r33.A0R(new C23415Beh(A012, true));
            r33.A0A(A012.A06());
            r33.A08(new C94434eW());
            r52.A00(r33.A0F());
            r42.A02 = (AnonymousClass303) r52.A00;
        } else if (this instanceof AnonymousClass1vD) {
            AnonymousClass1vD r0 = (AnonymousClass1vD) this;
            StoryBucket storyBucket = r0.A02;
            StoryCard storyCard = r0.A03;
            C95464gT r23 = (C95464gT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgM, r0.A00);
            storyCard.getId();
            r23.A0G(storyCard, new AnonymousClass473(new AnonymousClass474()));
            r23.A06 = storyBucket;
        } else if (this instanceof C37331vE) {
            C37331vE r43 = (C37331vE) this;
            AnonymousClass1JP r53 = new AnonymousClass1JP();
            StoryCard storyCard2 = r43.A04;
            int i3 = AnonymousClass1Y3.AAn;
            AnonymousClass0UN r34 = r43.A01;
            C208169ri r24 = (C208169ri) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B3Z, r34);
            C61832zZ r62 = (C61832zZ) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BCv, r34);
            ((C95254g6) AnonymousClass1XX.A02(0, i3, r34)).A03 = storyCard2;
            r24.A02 = "contribution_sticker";
            r24.A00 = "ContributionSticker";
            r24.A01 = "https://lookaside.facebook.com/assets/400755604113787/";
            r24.A03 = true;
            C23408Bea A013 = r24.A01();
            r62.A0Q(CallerContext.A09("StoryViewerContributionStickerComponentSpec"));
            r62.A0R(new C23415Beh(A013, true));
            r62.A0A(A013.A06());
            r62.A08(new C94444eX());
            r53.A00(r62.A0F());
            r43.A00 = (AnonymousClass303) r53.A00;
        } else if (this instanceof AnonymousClass1KM) {
            ((AnonymousClass1KM) this).A01.Bxj();
        } else if (this instanceof AnonymousClass1JE) {
            AnonymousClass1JE r44 = (AnonymousClass1JE) this;
            AnonymousClass1JP r54 = new AnonymousClass1JP();
            Uri uri = r44.A01;
            AnonymousClass3JC r10 = r44.A08;
            C27482DdQ ddQ = r44.A04;
            C27495Ddd ddd = r44.A06;
            AnonymousClass852 r12 = (AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, r44.A09);
            C156827Mz r02 = r44.A07;
            C27481DdP ddP = r02.lastFrescoState;
            CallerContext callerContext = r02.callerContextWithContextChain;
            if (!r12.A00(callerContext, ddQ).A04.A0V()) {
                C22013Amj amj = r12.A00(callerContext, ddQ).A00;
                if (r10 == null) {
                    r10 = AnonymousClass3JC.A0G;
                }
                C27481DdP BjP = amj.BjP(ddP, uri, null, r10, callerContext, r15.A03(), ddd);
                if (!(ddP == BjP || r15.A04 == null)) {
                    r15.A0E(new C61322yh(Process.WAIT_RESULT_STOPPED, BjP));
                }
                r54.A00(BjP);
            }
            C156827Mz r63 = r44.A07;
            r63.frescoState = (C27481DdP) r54.A00;
            C27484DdS ddS = ((AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, r44.A09)).A00(r63.callerContextWithContextChain, r44.A04).A04;
            if (ddS.A0D()) {
                C30392EvT evT = new C30392EvT(ddS.A02());
                C17770zR r35 = r15.A04;
                if (r35.A08 == null) {
                    r35.A08 = new ArrayList();
                }
                r35.A08.add(new AnonymousClass38a("imagePrefetch", evT, r35));
            }
        }
    }

    public void A0e(AnonymousClass0p4 r54, C17490yz r55) {
        AnonymousClass1JP r3;
        float f;
        int i;
        int height;
        C27481DdP ddP;
        AnonymousClass0p4 r4 = r54;
        if (this instanceof AnonymousClass1KX) {
            AnonymousClass1KX r32 = (AnonymousClass1KX) this;
            AnonymousClass1JP r7 = new AnonymousClass1JP();
            AnonymousClass1JP r6 = new AnonymousClass1JP();
            AnonymousClass1JP r5 = new AnonymousClass1JP();
            AnonymousClass1JP r2 = new AnonymousClass1JP();
            int i2 = r32.A04;
            Context context = r4.A09;
            if (i2 <= 0) {
                i2 = C007106r.A00(context, 50.0f);
            }
            r7.A00(Integer.valueOf((r55.getWidth() - r55.Awt()) - r55.Awu()));
            r6.A00(Integer.valueOf((r55.getHeight() - r55.Awv()) - r55.Aws()));
            if (i2 <= 0) {
                r5.A00(r7.A00);
                r2.A00(r6.A00);
            } else {
                Integer valueOf = Integer.valueOf(i2);
                r5.A00(valueOf);
                r2.A00(valueOf);
            }
            r32.A0H = (Integer) r7.A00;
            r32.A0G = (Integer) r6.A00;
            r32.A0F = (Integer) r5.A00;
            r32.A0E = (Integer) r2.A00;
        } else if (this instanceof AnonymousClass1NM) {
            AnonymousClass1NM r12 = (AnonymousClass1NM) this;
            AnonymousClass1JP r11 = new AnonymousClass1JP();
            AnonymousClass1JP r10 = new AnonymousClass1JP();
            AnonymousClass1JP r9 = new AnonymousClass1JP();
            AnonymousClass1JP r8 = new AnonymousClass1JP();
            AnonymousClass1JP r72 = new AnonymousClass1JP();
            AnonymousClass1JP r62 = new AnonymousClass1JP();
            ComponentTree componentTree = r12.A06;
            ComponentTree componentTree2 = r12.A07;
            Integer num = r12.A0F;
            Integer num2 = r12.A0E;
            Integer num3 = r12.A0G;
            Integer num4 = r12.A0H;
            Integer num5 = r12.A0I;
            C17770zR r16 = r12.A01;
            C17770zR r17 = r12.A02;
            C17770zR r18 = r12.A03;
            Drawable drawable = r12.A00;
            if (componentTree == null || componentTree2 == null || num == null || num2 == null || num3 == null || num4 == null) {
                AnonymousClass064.A00(num5);
                AnonymousClass1NM.A01(r4, num5.intValue(), r16, drawable, r17, r18, null, r11, r10, r9, r8, r72, r62);
            } else {
                componentTree.A0O(View.MeasureSpec.makeMeasureSpec(num.intValue(), 1073741824), View.MeasureSpec.makeMeasureSpec(num2.intValue(), 1073741824), null);
                r11.A00(componentTree);
                r10.A00(componentTree2);
                r9.A00(num);
                r8.A00(num2);
                r72.A00(num3);
                r62.A00(num4);
            }
            r12.A04 = (ComponentTree) r11.A00;
            r12.A05 = (ComponentTree) r10.A00;
            r12.A0C = (Integer) r9.A00;
            r12.A0B = (Integer) r8.A00;
            r12.A0D = (Integer) r72.A00;
            r12.A0J = (Integer) r62.A00;
        } else if (this instanceof AnonymousClass1vC) {
            AnonymousClass1vC r33 = (AnonymousClass1vC) this;
            AnonymousClass1JP r22 = new AnonymousClass1JP();
            AnonymousClass1JP r1 = new AnonymousClass1JP();
            r22.A00(Integer.valueOf(r55.getWidth()));
            r1.A00(Integer.valueOf(r55.getHeight()));
            r33.A07 = (Integer) r22.A00;
            r33.A06 = (Integer) r1.A00;
        } else if (this instanceof C14910uL) {
            C14910uL r0 = (C14910uL) this;
            C14910uL r52 = r0;
            AnonymousClass1JP r182 = new AnonymousClass1JP();
            AnonymousClass1JP r172 = new AnonymousClass1JP();
            AnonymousClass1JP r162 = new AnonymousClass1JP();
            AnonymousClass1JP r15 = new AnonymousClass1JP();
            AnonymousClass1JP r63 = new AnonymousClass1JP();
            CharSequence charSequence = r0.A0c;
            TextUtils.TruncateAt truncateAt = r0.A0U;
            boolean z = r52.A0l;
            int i3 = r52.A0F;
            int i4 = r52.A0H;
            int i5 = r52.A0E;
            int i6 = r52.A0J;
            int i7 = r52.A0G;
            float f2 = r52.A05;
            float f3 = r52.A03;
            float f4 = r52.A04;
            int i8 = r52.A0L;
            boolean z2 = r52.A0j;
            int i9 = r52.A0M;
            ColorStateList colorStateList = r52.A0P;
            int i10 = r52.A0D;
            int i11 = r52.A0N;
            float f5 = r52.A01;
            float f6 = r52.A06;
            C14910uL r02 = r52;
            C14920uM r112 = r02.A0Y;
            int i12 = r02.A0O;
            Typeface typeface = r52.A0Q;
            C14910uL r03 = r52;
            Layout.Alignment alignment = r03.A0R;
            AnonymousClass1JS r82 = r03.A0X;
            int i13 = r03.A07;
            int i14 = r52.A0B;
            boolean z3 = r52.A0i;
            C22311Kv r50 = r52.A0V;
            C14910uL r04 = r52;
            CharSequence charSequence2 = r04.A0a;
            float f7 = r04.A02;
            C14910uL r05 = r52;
            Layout layout = r05.A0S;
            Integer num6 = r05.A0f;
            Integer num7 = r05.A0e;
            AnonymousClass1JP r13 = r162;
            Layout layout2 = layout;
            r182.A00(charSequence);
            if (!TextUtils.isEmpty(charSequence)) {
                float width = (float) ((r55.getWidth() - r55.Awt()) - r55.Awu());
                float height2 = (float) ((r55.getHeight() - r55.Awv()) - r55.Aws());
                if (layout != null && ((float) num6.intValue()) == width && ((float) num7.intValue()) == height2) {
                    AnonymousClass1JP r23 = r172;
                    r23.A00(layout2);
                    r3 = r23;
                } else {
                    r3 = r172;
                    r3.A00(C14910uL.A01(r4, View.MeasureSpec.makeMeasureSpec((int) width, 1073741824), truncateAt, z, i3, f2, f3, f4, i8, z2, charSequence, i9, colorStateList, i10, i11, f5, f6, 0.0f, i12, typeface, C14910uL.A04(alignment, r82), z3, r55.B16(), i4, i5, i6, i7, r4.A09.getResources().getDisplayMetrics().density, i13, i14, 0, r50, f7));
                }
                float A002 = (float) C22561Ly.A00((Layout) r3.A00);
                switch (r112.ordinal()) {
                    case 1:
                        f = (height2 - A002) / 2.0f;
                        break;
                    case 2:
                        f = height2 - A002;
                        break;
                    default:
                        f = 0.0f;
                        break;
                }
                r13.A00(Float.valueOf(f));
                if (charSequence2 != null && !charSequence2.equals(BuildConfig.FLAVOR)) {
                    Layout layout3 = (Layout) r3.A00;
                    int i15 = 0;
                    while (true) {
                        if (i15 >= layout3.getLineCount()) {
                            i15 = -1;
                        } else if (layout3.getEllipsisCount(i15) <= 0) {
                            i15++;
                        }
                    }
                    if (i15 != -1) {
                        Layout layout4 = (Layout) r3.A00;
                        Rect rect = new Rect();
                        layout4.getPaint().getTextBounds(charSequence2.toString(), 0, charSequence2.length(), rect);
                        int offsetForHorizontal = layout4.getOffsetForHorizontal(i15, (width - ((float) rect.width())) + layout4.getLineLeft(i15));
                        if (offsetForHorizontal > 0) {
                            int i16 = offsetForHorizontal - 1;
                            if (layout4.getEllipsisCount(i15) <= 0 || i16 <= (i = layout4.getEllipsisStart(i15) + layout4.getLineStart(i15))) {
                                i = i16;
                            }
                            charSequence = TextUtils.concat(charSequence.subSequence(0, i), charSequence2);
                        }
                        Layout A012 = C14910uL.A01(r4, View.MeasureSpec.makeMeasureSpec((int) width, 1073741824), truncateAt, z, i3, f2, f3, f4, i8, z2, charSequence, i9, colorStateList, i10, i11, f5, f6, 0.0f, i12, typeface, C14910uL.A04(alignment, r82), z3, r55.B16(), i4, i5, i6, i7, r4.A09.getResources().getDisplayMetrics().density, i13, i14, 0, r50, f7);
                        r182.A00(charSequence);
                        r3.A00(A012);
                    }
                }
                CharSequence charSequence3 = (CharSequence) r182.A00;
                if (charSequence3 instanceof Spanned) {
                    Spanned spanned = (Spanned) charSequence3;
                    r15.A00(spanned.getSpans(0, charSequence3.length(), ClickableSpan.class));
                    r63.A00(spanned.getSpans(0, charSequence3.length(), ImageSpan.class));
                }
            }
            r52.A0b = (CharSequence) r182.A00;
            r52.A0T = (Layout) r172.A00;
            C14910uL r06 = r52;
            r06.A0d = (Float) r162.A00;
            r06.A0m = (ClickableSpan[]) r15.A00;
            r06.A0n = (ImageSpan[]) r63.A00;
        } else if (this instanceof AnonymousClass10H) {
            AnonymousClass10H r24 = (AnonymousClass10H) this;
            AnonymousClass1JP r53 = new AnonymousClass1JP();
            AnonymousClass1JP r42 = new AnonymousClass1JP();
            AnonymousClass1JP r34 = new AnonymousClass1JP();
            Drawable drawable2 = r24.A00;
            ImageView.ScaleType scaleType = r24.A01;
            int Awt = r55.Awt() + r55.Awu();
            int Awv = r55.Awv() + r55.Aws();
            if (ImageView.ScaleType.FIT_XY == scaleType || drawable2 == null || drawable2.getIntrinsicWidth() <= 0 || drawable2.getIntrinsicHeight() <= 0) {
                r53.A00(null);
                r42.A00(Integer.valueOf(r55.getWidth() - Awt));
                height = r55.getHeight() - Awv;
            } else {
                r53.A00(AnonymousClass1AN.A00(drawable2, scaleType, r55.getWidth() - Awt, r55.getHeight() - Awv));
                r42.A00(Integer.valueOf(drawable2.getIntrinsicWidth()));
                height = drawable2.getIntrinsicHeight();
            }
            r34.A00(Integer.valueOf(height));
            r24.A02 = (AnonymousClass1AN) r53.A00;
            r24.A04 = (Integer) r42.A00;
            r24.A03 = (Integer) r34.A00;
        } else if (this instanceof C15060uf) {
            C15060uf r14 = (C15060uf) this;
            r14.A01 = r55.getWidth();
            r14.A00 = r55.getHeight();
        } else if ((this instanceof AnonymousClass1JE) && (ddP = ((AnonymousClass1JE) this).A07.frescoState) != null) {
            ddP.A02 = r55.getWidth();
            ddP.A01 = r55.getHeight();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:134:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0i(X.AnonymousClass0p4 r26, java.lang.Object r27) {
        /*
            r25 = this;
            r10 = r27
            r1 = r25
            boolean r0 = r1 instanceof X.AnonymousClass1KX
            if (r0 != 0) goto L_0x0135
            boolean r0 = r1 instanceof X.AnonymousClass1v9
            if (r0 != 0) goto L_0x011e
            boolean r0 = r1 instanceof X.AnonymousClass1I8
            if (r0 != 0) goto L_0x030b
            boolean r0 = r1 instanceof X.C31981ku
            if (r0 != 0) goto L_0x0300
            boolean r0 = r1 instanceof X.AnonymousClass1KF
            if (r0 != 0) goto L_0x0300
            boolean r0 = r1 instanceof X.AnonymousClass1vA
            if (r0 != 0) goto L_0x00d7
            boolean r0 = r1 instanceof X.AnonymousClass1vD
            r9 = r26
            if (r0 != 0) goto L_0x0082
            boolean r0 = r1 instanceof X.AnonymousClass1JV
            if (r0 != 0) goto L_0x02e9
            boolean r0 = r1 instanceof X.C17020yC
            if (r0 != 0) goto L_0x029c
            boolean r0 = r1 instanceof X.AnonymousClass10I
            if (r0 != 0) goto L_0x01fb
            boolean r0 = r1 instanceof X.AnonymousClass1JW
            if (r0 != 0) goto L_0x01c0
            boolean r0 = r1 instanceof X.AnonymousClass1JX
            if (r0 != 0) goto L_0x0163
            boolean r0 = r1 instanceof X.AnonymousClass10H
            if (r0 != 0) goto L_0x013f
            boolean r0 = r1 instanceof X.C15060uf
            if (r0 != 0) goto L_0x0151
            boolean r0 = r1 instanceof X.AnonymousClass1JE
            if (r0 == 0) goto L_0x0081
            r5 = r1
            X.1JE r5 = (X.AnonymousClass1JE) r5
            X.3hG r10 = (X.C74093hG) r10
            android.net.Uri r14 = r5.A01
            r15 = 0
            X.DdQ r13 = r5.A04
            X.Ddd r4 = r5.A06
            X.3JC r3 = r5.A08
            int r2 = X.AnonymousClass1Y3.ANV
            X.0UN r1 = r5.A09
            r0 = 0
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.852 r11 = (X.AnonymousClass852) r11
            X.7Mz r0 = r5.A07
            com.facebook.common.callercontext.CallerContext r12 = r0.callerContextWithContextChain
            X.DdP r2 = r0.frescoState
            X.DdQ r0 = r11.A00(r12, r13)
            X.DdS r1 = r0.A04
            boolean r0 = r1.A0B()
            if (r0 == 0) goto L_0x0072
            if (r2 == 0) goto L_0x0072
            X.AnonymousClass1JE.A02(r2)
        L_0x0072:
            boolean r0 = r1.A0S()
            if (r0 == 0) goto L_0x0081
            r16 = r3
            r17 = r4
            r18 = r2
            X.AnonymousClass1JE.A03(r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
        L_0x0081:
            return
        L_0x0082:
            r0 = r1
            X.1vD r0 = (X.AnonymousClass1vD) r0
            com.facebook.resources.ui.FbFrameLayout r10 = (com.facebook.resources.ui.FbFrameLayout) r10
            X.4O4 r6 = r0.A06
            com.facebook.ipc.stories.model.StoryCard r14 = r0.A03
            X.3gF r5 = r0.A01
            X.4Rw r12 = r0.A05
            java.lang.String r4 = r0.A08
            int r1 = X.AnonymousClass1Y3.AgM
            X.0UN r3 = r0.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.4gT r2 = (X.C95464gT) r2
            int r1 = X.AnonymousClass1Y3.BPQ
            r0 = 1
            java.lang.Object r15 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.4MH r15 = (X.AnonymousClass4MH) r15
            X.4Rv r11 = new X.4Rv
            r13 = r2
            r17 = r9
            r18 = r10
            r19 = r6
            r20 = r4
            r16 = r5
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19, r20)
            r2.A08 = r11
            X.4Ru r1 = new X.4Ru
            r16 = r2
            r17 = r14
            r18 = r5
            r19 = r9
            r20 = r10
            r21 = r6
            r22 = r12
            r23 = r15
            r24 = r4
            r15 = r1
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24)
            android.view.View r0 = r2.A04
            if (r0 == 0) goto L_0x0081
            r0.setOnClickListener(r1)
            return
        L_0x00d7:
            r0 = r1
            X.1vA r0 = (X.AnonymousClass1vA) r0
            X.3gF r4 = r0.A01
            java.lang.String r3 = r0.A07
            int r2 = X.AnonymousClass1Y3.A6y
            X.0UN r1 = r0.A00
            r0 = 6
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.3zH r1 = (X.C84343zH) r1
            java.lang.String r0 = "StoryOverlayFundraiserSticker"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0081
            r0 = 3355(0xd1b, float:4.701E-42)
            java.lang.String r3 = r4.A0P(r0)
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r1.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1ZE r1 = (X.AnonymousClass1ZE) r1
            java.lang.String r0 = "stories_fundraiser_sticker_consumption_viewed"
            X.0bW r2 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r1 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 565(0x235, float:7.92E-43)
            r1.<init>(r2, r0)
            boolean r0 = r1.A0G()
            if (r0 == 0) goto L_0x0081
            java.lang.String r0 = "fundraiser_sticker_id"
            r1.A0D(r0, r3)
            r1.A06()
            return
        L_0x011e:
            r0 = r1
            X.1v9 r0 = (X.AnonymousClass1v9) r0
            com.facebook.payments.ui.PaymentFormEditTextView r10 = (com.facebook.payments.ui.PaymentFormEditTextView) r10
            java.lang.String r2 = r0.A03
            android.view.View$OnClickListener r1 = r0.A01
            int r0 = r0.A00
            r10.A0L(r2)
            r10.A0T(r0)
            if (r1 == 0) goto L_0x0081
            r10.setOnClickListener(r1)
            return
        L_0x0135:
            r0 = r1
            X.1KX r0 = (X.AnonymousClass1KX) r0
            X.1MR r10 = (X.AnonymousClass1MR) r10
            java.lang.Integer r1 = r0.A0F
            java.lang.Integer r0 = r0.A0E
            goto L_0x0148
        L_0x013f:
            r0 = r1
            X.10H r0 = (X.AnonymousClass10H) r0
            X.1MR r10 = (X.AnonymousClass1MR) r10
            java.lang.Integer r1 = r0.A04
            java.lang.Integer r0 = r0.A03
        L_0x0148:
            int r3 = r1.intValue()
            int r2 = r0.intValue()
            goto L_0x015a
        L_0x0151:
            r0 = r1
            X.0uf r0 = (X.C15060uf) r0
            X.1MR r10 = (X.AnonymousClass1MR) r10
            int r3 = r0.A01
            int r2 = r0.A00
        L_0x015a:
            android.graphics.drawable.Drawable r1 = r10.A00
            if (r1 == 0) goto L_0x0081
            r0 = 0
            r1.setBounds(r0, r0, r3, r2)
            return
        L_0x0163:
            android.widget.RelativeLayout r10 = (android.widget.RelativeLayout) r10
            r0 = 0
            android.view.View r11 = r10.getChildAt(r0)
            X.6je r11 = (X.C142086je) r11
            r0 = 1
            android.view.View r3 = r10.getChildAt(r0)
            X.5UH r3 = (X.AnonymousClass5UH) r3
            android.os.CountDownTimer r0 = r11.A02
            if (r0 == 0) goto L_0x017d
            r0.cancel()
            r0 = 0
            r11.A02 = r0
        L_0x017d:
            X.06B r2 = r11.A04
            r4 = 1
            if (r2 == 0) goto L_0x01bc
            java.lang.String r0 = r11.A05
            if (r0 == 0) goto L_0x01bc
            long r12 = r11.A01
            long r0 = r11.A00
            long r12 = r12 + r0
            long r0 = r2.now()
            long r12 = r12 - r0
            r1 = 0
            int r0 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x01bc
            r0 = 0
            r11.A00(r0)
            X.6jd r10 = new X.6jd
            r14 = 1000(0x3e8, double:4.94E-321)
            r10.<init>(r11, r12, r14)
            android.os.CountDownTimer r0 = r10.start()
            r11.A02 = r0
        L_0x01a7:
            X.0zR r0 = r9.A04
            if (r0 != 0) goto L_0x01b7
            r0 = 0
        L_0x01ac:
            r3.A00 = r0
            X.5UG r0 = new X.5UG
            r0.<init>(r3)
            r3.setOnClickListener(r0)
            return
        L_0x01b7:
            X.1JX r0 = (X.AnonymousClass1JX) r0
            X.10N r0 = r0.A03
            goto L_0x01ac
        L_0x01bc:
            r11.A00(r4)
            goto L_0x01a7
        L_0x01c0:
            X.1JW r1 = (X.AnonymousClass1JW) r1
            android.view.View r10 = (android.view.View) r10
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r1.A02
            X.2Fx r2 = r1.A01
            X.Eim r10 = (X.C29796Eim) r10
            X.0y3 r0 = r0.AzL()
            int r0 = r0.AhV()
            r10.A00 = r0
            X.0zR r1 = r9.A04
            r0 = 0
            if (r1 == 0) goto L_0x01dd
            X.1JW r1 = (X.AnonymousClass1JW) r1
            X.10N r0 = r1.A00
        L_0x01dd:
            r10.A01 = r0
            android.widget.AdapterView$OnItemSelectedListener r0 = r10.A03
            r10.setOnItemSelectedListener(r0)
            r1 = 2132410394(0x7f1a001a, float:2.0470164E38)
            int r0 = r10.A00
            if (r0 == r1) goto L_0x01f3
            r10.A00 = r1
            r10.getContext()
            X.C208399sD.A00(r10)
        L_0x01f3:
            r10.A02 = r2
            java.lang.String r0 = r2.A00
            r10.A01(r0)
            return
        L_0x01fb:
            r0 = r1
            X.10I r0 = (X.AnonymousClass10I) r0
            android.widget.RelativeLayout r10 = (android.widget.RelativeLayout) r10
            X.9gP r6 = r0.A04
            com.facebook.mig.scheme.interfaces.MigColorScheme r4 = r0.A05
            boolean r3 = r0.A09
            boolean r7 = r0.A08
            r8 = 1
            r0 = 0
            android.view.View r2 = r10.getChildAt(r0)
            X.EjB r2 = (X.C29818EjB) r2
            r5 = 1
            android.view.View r1 = r10.getChildAt(r8)
            X.EjF r1 = (X.C29822EjF) r1
            android.graphics.Typeface r0 = r2.A00
            if (r0 == 0) goto L_0x021e
            r2.setTypeface(r0)
        L_0x021e:
            r2.setFocusable(r8)
            r2.setFocusableInTouchMode(r8)
            r2.setClickable(r8)
            r2.setLongClickable(r8)
            r2.setCursorVisible(r8)
            X.0zR r0 = r9.A04
            if (r0 != 0) goto L_0x0297
            r0 = 0
        L_0x0232:
            r2.A02 = r0
            android.text.TextWatcher r0 = r2.A05
            r2.addTextChangedListener(r0)
            r2.A04 = r6
            X.0zR r0 = r9.A04
            if (r0 != 0) goto L_0x0292
            r0 = 0
        L_0x0240:
            r2.A01 = r0
            r2.A03 = r1
            android.text.Editable r0 = r2.getText()
            java.lang.String r8 = r0.toString()
            java.lang.String r0 = r6.A00
            boolean r0 = r8.equals(r0)
            if (r0 != 0) goto L_0x026f
            android.text.Editable r0 = r2.getText()
            java.lang.String r0 = r0.toString()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x026a
            java.lang.String r0 = r6.A00
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x026f
        L_0x026a:
            java.lang.String r0 = r6.A00
            r2.setText(r0)
        L_0x026f:
            if (r7 == 0) goto L_0x0274
            X.C35511rL.A01(r2)
        L_0x0274:
            r1.A02 = r5
            r1.A00 = r2
            android.view.View$OnClickListener r0 = r1.A03
            r1.setOnClickListener(r0)
            r1.A01 = r6
            r1.A03()
            r0 = 2132214783(0x7f1703ff, float:2.0073418E38)
            r2.setBackgroundResource(r0)
            r10 = 0
            r6 = r9
            r7 = r4
            r8 = r2
            r9 = r1
            r11 = r3
            X.AnonymousClass10I.A00(r6, r7, r8, r9, r10, r11)
            return
        L_0x0292:
            X.10I r0 = (X.AnonymousClass10I) r0
            X.10N r0 = r0.A03
            goto L_0x0240
        L_0x0297:
            X.10I r0 = (X.AnonymousClass10I) r0
            X.10N r0 = r0.A02
            goto L_0x0232
        L_0x029c:
            r0 = r1
            X.0yC r0 = (X.C17020yC) r0
            android.widget.RelativeLayout r10 = (android.widget.RelativeLayout) r10
            com.facebook.mig.scheme.interfaces.MigColorScheme r3 = r0.A04
            r0 = 0
            android.view.View r2 = r10.getChildAt(r0)
            X.EjC r2 = (X.C29819EjC) r2
            android.graphics.Typeface r0 = r2.A00
            if (r0 == 0) goto L_0x02b1
            r2.setTypeface(r0)
        L_0x02b1:
            X.0zR r1 = r9.A04
            r0 = 0
            if (r1 == 0) goto L_0x02ba
            X.0yC r1 = (X.C17020yC) r1
            X.10N r0 = r1.A02
        L_0x02ba:
            r2.A02 = r0
            android.text.TextWatcher r0 = r2.A07
            r2.addTextChangedListener(r0)
            X.0zR r0 = r9.A04
            if (r0 != 0) goto L_0x02e4
            r0 = 0
        L_0x02c6:
            r2.A01 = r0
            r0 = 1
            android.view.View r1 = r10.getChildAt(r0)
            X.EjI r1 = (X.C29825EjI) r1
            r1.A02 = r2
            android.view.View$OnClickListener r0 = r1.A05
            r1.setOnClickListener(r0)
            r2.A03 = r1
            r0 = 2132214783(0x7f1703ff, float:2.0073418E38)
            r2.setBackgroundResource(r0)
            android.content.Context r0 = r9.A09
            X.C17020yC.A00(r0, r3, r2, r1)
            return
        L_0x02e4:
            X.0yC r0 = (X.C17020yC) r0
            X.10N r0 = r0.A01
            goto L_0x02c6
        L_0x02e9:
            android.view.View r10 = (android.view.View) r10
            X.9oG r10 = (X.C206439oG) r10
            X.0zR r1 = r9.A04
            r0 = 0
            if (r1 == 0) goto L_0x02f6
            X.1JV r1 = (X.AnonymousClass1JV) r1
            X.10N r0 = r1.A00
        L_0x02f6:
            r10.A00 = r0
            X.Eio r0 = new X.Eio
            r0.<init>(r10)
            r10.A07 = r0
            return
        L_0x0300:
            X.ESq r10 = (X.C29268ESq) r10
            X.ESt r0 = r10.A08
            r0.BOS()
            r10.A05()
            return
        L_0x030b:
            r0 = r1
            X.1I8 r0 = (X.AnonymousClass1I8) r0
            android.widget.ImageView r10 = (android.widget.ImageView) r10
            r4 = 0
            boolean r3 = r0.A06
            java.lang.CharSequence r2 = r0.A04
            X.1I9 r1 = r0.A03
            android.widget.ImageView$ScaleType r0 = r0.A02
            r10.setImageDrawable(r4)
            r10.setScaleType(r0)
            r10.setSelected(r3)
            r10.setContentDescription(r2)
            X.C21651Ie.A01(r10, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A0i(X.0p4, java.lang.Object):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0672, code lost:
        if (r14.equals("BOTTOM_LEFT") == false) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0681, code lost:
        if (r14.equals(X.C99084oO.$const$string(99)) == false) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0690, code lost:
        if (r14.equals(X.C99084oO.$const$string(129)) == false) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x069b, code lost:
        if (r14.equals("TOP_RIGHT") == false) goto L_0x0455;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x0b75, code lost:
        if (r5 == r17.A02) goto L_0x0ba2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00f5, code lost:
        if (r1 <= 0) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x010e, code lost:
        if (r3 >= r12) goto L_0x0110;
     */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00e4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0j(X.AnonymousClass0p4 r39, java.lang.Object r40) {
        /*
            r38 = this;
            r0 = r40
            r2 = r38
            boolean r1 = r2 instanceof X.AnonymousClass1KX
            r11 = r39
            if (r1 != 0) goto L_0x0ac0
            boolean r1 = r2 instanceof X.AnonymousClass1NM
            if (r1 != 0) goto L_0x0a75
            boolean r1 = r2 instanceof X.AnonymousClass1I8
            if (r1 != 0) goto L_0x0a63
            boolean r1 = r2 instanceof X.C31981ku
            if (r1 != 0) goto L_0x0a4d
            boolean r1 = r2 instanceof X.AnonymousClass1KF
            if (r1 != 0) goto L_0x0a37
            boolean r1 = r2 instanceof X.AnonymousClass1vB
            if (r1 != 0) goto L_0x069f
            boolean r1 = r2 instanceof X.AnonymousClass1v5
            if (r1 != 0) goto L_0x032e
            boolean r1 = r2 instanceof X.AnonymousClass1vC
            if (r1 != 0) goto L_0x09b7
            boolean r1 = r2 instanceof X.AnonymousClass1vD
            if (r1 != 0) goto L_0x021f
            boolean r1 = r2 instanceof X.C37331vE
            if (r1 != 0) goto L_0x0174
            boolean r1 = r2 instanceof X.AnonymousClass1JV
            if (r1 != 0) goto L_0x093b
            boolean r1 = r2 instanceof X.AnonymousClass1JX
            if (r1 != 0) goto L_0x08c5
            boolean r1 = r2 instanceof X.C14910uL
            if (r1 != 0) goto L_0x008d
            boolean r1 = r2 instanceof X.AnonymousClass10H
            if (r1 != 0) goto L_0x08b8
            boolean r1 = r2 instanceof X.C15060uf
            if (r1 != 0) goto L_0x08ac
            boolean r1 = r2 instanceof X.AnonymousClass1JE
            if (r1 == 0) goto L_0x008c
            r4 = r2
            X.1JE r4 = (X.AnonymousClass1JE) r4
            X.3hG r0 = (X.C74093hG) r0
            android.net.Uri r9 = r4.A01
            r16 = 0
            X.DdQ r8 = r4.A04
            X.3JC r7 = r4.A08
            X.Ddd r6 = r4.A06
            int r3 = X.AnonymousClass1Y3.ANV
            X.0UN r2 = r4.A09
            r1 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.852 r5 = (X.AnonymousClass852) r5
            X.7Mz r1 = r4.A07
            com.facebook.common.callercontext.CallerContext r4 = r1.callerContextWithContextChain
            X.DdP r3 = r1.frescoState
            X.DdQ r1 = r5.A00(r4, r8)
            X.DdS r2 = r1.A04
            boolean r1 = r2.A0B()
            if (r1 == 0) goto L_0x0077
            if (r3 == 0) goto L_0x0077
            X.AnonymousClass1JE.A02(r3)
        L_0x0077:
            boolean r1 = r2.A0S()
            if (r1 != 0) goto L_0x008c
            r10 = r11
            r11 = r0
            r12 = r5
            r13 = r4
            r14 = r8
            r15 = r9
            r17 = r7
            r18 = r6
            r19 = r3
            X.AnonymousClass1JE.A03(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
        L_0x008c:
            return
        L_0x008d:
            r4 = r2
            X.0uL r4 = (X.C14910uL) r4
            X.0d2 r0 = (X.AnonymousClass0d2) r0
            int r15 = r4.A0M
            int r14 = r4.A08
            android.content.res.ColorStateList r13 = r4.A0P
            X.10N r5 = r4.A0W
            int r3 = r4.A0A
            int r12 = r4.A09
            float r10 = r4.A00
            boolean r9 = r4.A0h
            X.2CC r8 = r4.A0Z
            java.lang.CharSequence r7 = r4.A0b
            android.text.Layout r1 = r4.A0T
            java.lang.Float r2 = r4.A0d
            android.text.style.ClickableSpan[] r6 = r4.A0m
            android.text.style.ImageSpan[] r4 = r4.A0n
            r17 = r5
            if (r5 == 0) goto L_0x015e
            X.4Jt r5 = new X.4Jt
            r16 = r5
            r18 = r7
            r16.<init>(r17, r18)
        L_0x00bb:
            if (r2 != 0) goto L_0x0158
            r17 = 0
        L_0x00bf:
            java.lang.String r16 = r11.A0B()
            r11 = r13
            r0.A0A = r1
            r2 = r17
            r0.A01 = r2
            r0.A0G = r9
            r0.A0E = r7
            r0.A0K = r6
            android.os.Handler r1 = r0.A09
            if (r1 != 0) goto L_0x00eb
            if (r6 == 0) goto L_0x0156
            int r1 = r6.length
            r9 = r1
            r2 = 0
        L_0x00d9:
            if (r2 >= r9) goto L_0x0156
            r1 = r6[r2]
            boolean r1 = r1 instanceof X.C70523an
            if (r1 == 0) goto L_0x0153
            r1 = 1
        L_0x00e2:
            if (r1 == 0) goto L_0x00eb
            android.os.Handler r2 = new android.os.Handler
            r2.<init>()
            r0.A09 = r2
        L_0x00eb:
            r0.A0D = r8
            r0.A0C = r5
            r5 = 1
            r2 = 0
            if (r6 == 0) goto L_0x00f7
            int r1 = r6.length
            r6 = 1
            if (r1 > 0) goto L_0x00f8
        L_0x00f7:
            r6 = 0
        L_0x00f8:
            r0.A0J = r6
            r0.A02 = r14
            r0.A00 = r10
            if (r15 == 0) goto L_0x012f
            r6 = 0
            r0.A06 = r6
            r0.A05 = r15
        L_0x0105:
            if (r3 < 0) goto L_0x0110
            int r1 = r7.length()
            if (r12 > r1) goto L_0x0110
            r1 = 1
            if (r3 < r12) goto L_0x0111
        L_0x0110:
            r1 = 0
        L_0x0111:
            if (r1 == 0) goto L_0x012b
            X.AnonymousClass0d2.A03(r0, r3, r12)
        L_0x0116:
            if (r4 == 0) goto L_0x0161
            int r8 = r4.length
            r6 = 0
        L_0x011a:
            if (r6 >= r8) goto L_0x0161
            r1 = r4[r6]
            android.graphics.drawable.Drawable r3 = r1.getDrawable()
            r3.setCallback(r0)
            r3.setVisible(r5, r2)
            int r6 = r6 + 1
            goto L_0x011a
        L_0x012b:
            X.AnonymousClass0d2.A03(r0, r2, r2)
            goto L_0x0116
        L_0x012f:
            if (r13 != 0) goto L_0x0133
            android.content.res.ColorStateList r11 = X.C14910uL.A0p
        L_0x0133:
            r0.A06 = r11
            int r6 = r11.getDefaultColor()
            r0.A05 = r6
            android.text.Layout r1 = r0.A0A
            if (r1 == 0) goto L_0x0105
            android.text.TextPaint r8 = r1.getPaint()
            android.content.res.ColorStateList r9 = r0.A06
            int[] r6 = r0.getState()
            int r1 = r0.A05
            int r1 = r9.getColorForState(r6, r1)
            r8.setColor(r1)
            goto L_0x0105
        L_0x0153:
            int r2 = r2 + 1
            goto L_0x00d9
        L_0x0156:
            r1 = 0
            goto L_0x00e2
        L_0x0158:
            float r17 = r2.floatValue()
            goto L_0x00bf
        L_0x015e:
            r5 = 0
            goto L_0x00bb
        L_0x0161:
            r0.A0L = r4
            r2 = r16
            r0.A0F = r2
            r0.invalidateSelf()
            boolean r1 = r7 instanceof X.C24431Tp
            if (r1 == 0) goto L_0x008c
            X.1Tp r7 = (X.C24431Tp) r7
            r7.Bfi(r0)
            return
        L_0x0174:
            r6 = r2
            X.1vE r6 = (X.C37331vE) r6
            com.facebook.resources.ui.FbFrameLayout r0 = (com.facebook.resources.ui.FbFrameLayout) r0
            com.facebook.ipc.stories.model.StoryCard r7 = r6.A04
            com.facebook.ipc.stories.model.AudienceControlData r5 = r6.A03
            X.3gF r8 = r6.A02
            int r3 = X.AnonymousClass1Y3.AAn
            X.0UN r2 = r6.A01
            r1 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.4g6 r4 = (X.C95254g6) r4
            X.303 r3 = r6.A00
            android.content.Context r2 = r0.getContext()
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = new com.facebook.drawee.fbpipeline.FbDraweeView
            r1.<init>(r2)
            X.Dp9 r2 = new X.Dp9
            r2.<init>(r1)
            r1 = 2131300732(0x7f09117c, float:1.8219502E38)
            r2.A02(r1)
            r1 = -1
            r2.A00(r1, r1)
            android.view.View r9 = r2.A00
            com.facebook.drawee.fbpipeline.FbDraweeView r9 = (com.facebook.drawee.fbpipeline.FbDraweeView) r9
            r9.A08(r3)
            com.facebook.widget.PillFrameLayout r2 = new com.facebook.widget.PillFrameLayout
            android.content.Context r1 = r0.getContext()
            r2.<init>(r1)
            X.12v r6 = new X.12v
            r6.<init>(r2)
            android.content.res.Resources r2 = r11.A03()
            r1 = 2132148356(0x7f160084, float:1.9938688E38)
            int r3 = r2.getDimensionPixelSize(r1)
            android.content.res.Resources r2 = r11.A03()
            r1 = 2132148225(0x7f160001, float:1.9938422E38)
            int r1 = r2.getDimensionPixelSize(r1)
            r6.A03(r3, r1)
            r6.A04(r9)
            android.view.View r3 = r6.A00
            r0.removeAllViews()
            r0.addView(r3)
            android.view.View r2 = r4.A00
            r1 = 0
            if (r2 == 0) goto L_0x01e3
            r1 = 1
        L_0x01e3:
            if (r1 != 0) goto L_0x01ef
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r8.Af1()
            if (r1 == 0) goto L_0x01ef
            r4.A00 = r3
            r4.A01 = r1
        L_0x01ef:
            if (r7 == 0) goto L_0x008c
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r7.A0J()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = X.C95184fz.A02(r1)
            if (r1 == 0) goto L_0x008c
            r4.A01()
            android.content.Context r3 = r0.getContext()
            int r2 = X.AnonymousClass1Y3.AjS
            X.0UN r1 = r4.A02
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4To r1 = (X.C90454To) r1
            X.4Tq r0 = r4.A04
            r1.A04(r0, r3)
            X.40Q r1 = new X.40Q
            r1.<init>(r5)
            android.view.View r0 = r4.A00
            if (r0 == 0) goto L_0x008c
            r0.setOnClickListener(r1)
            return
        L_0x021f:
            r1 = r2
            X.1vD r1 = (X.AnonymousClass1vD) r1
            com.facebook.resources.ui.FbFrameLayout r0 = (com.facebook.resources.ui.FbFrameLayout) r0
            com.facebook.ipc.stories.model.StoryCard r5 = r1.A03
            X.3gF r6 = r1.A01
            X.46A r3 = r1.A07
            X.4Jw r8 = r1.A04
            X.0Tq r7 = r1.A09
            int r2 = X.AnonymousClass1Y3.AgM
            X.0UN r4 = r1.A00
            r1 = 0
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.4gT r12 = (X.C95464gT) r12
            int r2 = X.AnonymousClass1Y3.B6l
            r1 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.4h4 r2 = (X.AnonymousClass4h4) r2
            com.facebook.graphql.enums.GraphQLStoryOverlayPollStyle r4 = r6.A0R()
            com.facebook.graphql.enums.GraphQLStoryOverlayPollStyle r1 = com.facebook.graphql.enums.GraphQLStoryOverlayPollStyle.A02
            r14 = 0
            if (r4 != r1) goto L_0x024c
            r14 = 1
        L_0x024c:
            if (r14 == 0) goto L_0x0323
            X.5W3 r4 = new X.5W3
            android.content.Context r9 = r0.getContext()
            r6 = 0
            r1 = 0
            r4.<init>(r9, r6, r1)
        L_0x0259:
            X.12v r9 = new X.12v
            r9.<init>(r4)
            android.content.res.Resources r4 = r11.A03()
            r1 = 2132148316(0x7f16005c, float:1.9938607E38)
            int r6 = r4.getDimensionPixelSize(r1)
            android.content.res.Resources r4 = r11.A03()
            r1 = 2132148298(0x7f16004a, float:1.993857E38)
            int r1 = r4.getDimensionPixelSize(r1)
            r9.A03(r6, r1)
            r6 = 2132410848(0x7f1a01e0, float:2.0471085E38)
            android.view.View r1 = r9.A00
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            android.content.Context r1 = r1.getContext()
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r1)
            android.view.View r1 = r9.A00
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            r4.inflate(r6, r1)
            r6 = 2132410849(0x7f1a01e1, float:2.0471087E38)
            android.view.View r1 = r9.A00
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            android.content.Context r1 = r1.getContext()
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r1)
            android.view.View r1 = r9.A00
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            r4.inflate(r6, r1)
            r1 = 2131298005(0x7f0906d5, float:1.821397E38)
            r9.A02(r1)
            android.view.View r13 = r9.A00
            r4 = 2131298007(0x7f0906d7, float:1.8213975E38)
            if (r14 == 0) goto L_0x0320
            java.lang.String r1 = "ig_poll_style_tag"
        L_0x02b2:
            r13.setTag(r4, r1)
            r0.removeAllViews()
            r0.addView(r13)
            android.view.View r4 = r12.A04
            r1 = 0
            if (r4 == 0) goto L_0x02c1
            r1 = 1
        L_0x02c1:
            if (r1 != 0) goto L_0x02cb
            r15 = 1
            r16 = 0
            r17 = 1
            r12.A0F(r13, r14, r15, r16, r17)
        L_0x02cb:
            if (r5 == 0) goto L_0x008c
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r5.A0J()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = X.C95184fz.A02(r1)
            if (r1 == 0) goto L_0x008c
            r12.A0D()
            android.content.Context r9 = r0.getContext()
            int r6 = X.AnonymousClass1Y3.AjS
            X.0UN r4 = r12.A05
            r1 = 7
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r6, r4)
            X.4To r4 = (X.C90454To) r4
            X.4Tq r1 = r12.A0I
            r4.A04(r1, r9)
            java.lang.String r9 = r5.getId()
            java.lang.Class<X.1vD> r6 = X.AnonymousClass1vD.class
            java.lang.Object[] r4 = new java.lang.Object[]{r11, r0}
            r1 = 829436257(0x31703161, float:3.4952665E-9)
            X.10N r6 = A0E(r6, r11, r1, r4)
            r4 = 0
            java.lang.String r1 = "StoryViewerPollStickerComponent"
            r8.CIQ(r9, r1, r4, r6)
            java.lang.Object r1 = r7.get()
            if (r1 == 0) goto L_0x0313
            java.lang.Object r1 = r7.get()
            com.facebook.user.model.User r1 = (com.facebook.user.model.User) r1
            java.lang.String r4 = r1.A0j
        L_0x0313:
            if (r4 == 0) goto L_0x09a5
            java.lang.String r1 = r5.getAuthorId()
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x09a5
            return
        L_0x0320:
            java.lang.String r1 = "fb_poll_style_tag"
            goto L_0x02b2
        L_0x0323:
            com.facebook.widget.PillFrameLayout r4 = new com.facebook.widget.PillFrameLayout
            android.content.Context r1 = r0.getContext()
            r4.<init>(r1)
            goto L_0x0259
        L_0x032e:
            r3 = r2
            X.1v5 r3 = (X.AnonymousClass1v5) r3
            com.facebook.resources.ui.FbFrameLayout r0 = (com.facebook.resources.ui.FbFrameLayout) r0
            com.facebook.ipc.stories.model.StoryCard r1 = r3.A03
            r35 = r1
            X.4Ky r1 = r3.A06
            r37 = r1
            X.3gF r1 = r3.A02
            r34 = r1
            X.4Jw r4 = r3.A04
            X.46A r1 = r3.A07
            r36 = r1
            int r2 = X.AnonymousClass1Y3.AM1
            X.0UN r5 = r3.A00
            r1 = 5
            java.lang.Object r33 = X.AnonymousClass1XX.A02(r1, r2, r5)
            r1 = r33
            X.4go r1 = (X.C95644go) r1
            r33 = r1
            int r2 = X.AnonymousClass1Y3.B6l
            r1 = 4
            java.lang.Object r32 = X.AnonymousClass1XX.A02(r1, r2, r5)
            r1 = r32
            X.4h4 r1 = (X.AnonymousClass4h4) r1
            r32 = r1
            X.5CS r1 = r3.A05
            boolean r1 = r1.shouldAnimateScale
            r31 = r1
            X.5XZ r1 = r3.A01
            r30 = r1
            if (r1 == 0) goto L_0x008c
            java.lang.String r8 = r35.getId()
            java.lang.Class<X.1v5> r5 = X.AnonymousClass1v5.class
            r1 = r35
            java.lang.Object[] r2 = new java.lang.Object[]{r11, r1}
            r1 = -1741503230(0xffffffff9832c502, float:-2.3105424E-24)
            X.10N r7 = A0E(r5, r11, r1, r2)
            r1 = r32
            java.lang.Object[] r2 = new java.lang.Object[]{r11, r1}
            r1 = 829436257(0x31703161, float:3.4952665E-9)
            X.10N r2 = A0E(r5, r11, r1, r2)
            java.lang.String r1 = "FeelingsStickerOverlayComponentSpec"
            r4.CIQ(r8, r1, r7, r2)
            r1 = 0
            android.view.View r29 = r0.getChildAt(r1)
            r1 = r29
            android.widget.ImageView r1 = (android.widget.ImageView) r1
            r29 = r1
            r1 = 1
            android.view.View r28 = r0.getChildAt(r1)
            r27 = 4
            r2 = r28
            r1 = r27
            r2.setVisibility(r1)
            com.facebook.litho.LithoView r1 = new com.facebook.litho.LithoView
            r1.<init>(r11)
            r0.addView(r1)
            r1 = 2
            android.view.View r26 = r0.getChildAt(r1)
            r1 = r26
            com.facebook.litho.LithoView r1 = (com.facebook.litho.LithoView) r1
            r26 = r1
            r13 = 2
            java.lang.String r2 = "color"
            java.lang.String r1 = "diameter"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r13)
            X.1oT r2 = new X.1oT
            r2.<init>()
            X.0z4 r3 = r11.A0B
            X.0zR r1 = r11.A04
            if (r1 == 0) goto L_0x03db
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x03db:
            r4.clear()
            r1 = -1
            r2.A02 = r1
            r1 = 0
            r4.set(r1)
            r1 = 1090519040(0x41000000, float:8.0)
            int r1 = r3.A00(r1)
            r2.A03 = r1
            r1 = 1
            r4.set(r1)
            X.AnonymousClass11F.A0C(r13, r4, r5)
            r1 = r26
            r1.A0a(r2)
            r1 = r34
            com.facebook.graphql.enums.GraphQLMinutiaeBubblePosition r3 = r1.A0Q()
            r2 = 0
            if (r3 == 0) goto L_0x040a
            com.facebook.graphql.enums.GraphQLMinutiaeBubblePosition r1 = com.facebook.graphql.enums.GraphQLMinutiaeBubblePosition.A01
            if (r3 == r1) goto L_0x040a
            java.lang.String r2 = r3.toString()
        L_0x040a:
            r3 = r35
            r14 = r2
            r4 = 0
            if (r2 == 0) goto L_0x0464
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r3.A0J()
            int r25 = X.C95184fz.A00(r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r3.A0J()
            int r12 = X.C95184fz.A01(r1)
            r1 = r34
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r24 = r1.Af1()
            double r22 = r24.A0Q()
            r1 = 4598175219545276416(0x3fd0000000000000, double:0.25)
            double r22 = r22 * r1
            r1 = r25
            double r9 = (double) r1
            double r20 = r22 * r9
            double r7 = (double) r12
            double r5 = r20 / r7
            double r3 = r24.A0V()
            double r1 = r24.A0U()
            r18 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r1 = r1 / r18
            double r3 = r3 + r1
            double r1 = r24.A0W()
            double r15 = r24.A0Q()
            double r15 = r15 / r18
            double r1 = r1 + r15
            int r15 = r14.hashCode()
            switch(r15) {
                case -475662734: goto L_0x0694;
                case -154073903: goto L_0x0685;
                case 1533816552: goto L_0x0676;
                case 1573315995: goto L_0x066b;
                default: goto L_0x0455;
            }
        L_0x0455:
            r15 = -1
        L_0x0456:
            r16 = 4604930618986332160(0x3fe8000000000000, double:0.75)
            if (r15 == 0) goto L_0x05d4
            r14 = 1
            if (r15 == r14) goto L_0x05c7
            r14 = 2
            if (r15 == r14) goto L_0x05e4
            r14 = 3
            if (r15 == r14) goto L_0x05ba
            r4 = 0
        L_0x0464:
            X.4Q5 r7 = new X.4Q5
            r1 = r35
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r1.A0J()
            int r2 = X.C95184fz.A01(r1)
            r1 = r35
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r1.A0J()
            int r1 = X.C95184fz.A00(r1)
            r7.<init>(r2, r1)
            if (r4 != 0) goto L_0x0599
            r1 = r26
            r2 = r27
            r1.setVisibility(r2)
            r2 = 1
        L_0x0487:
            r3 = r29
            r4 = r30
            r3.setImageDrawable(r4)
            r3.setFocusable(r2)
            android.view.View r16 = r0.getRootView()
            r3 = r35
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r12 = r34.Af1()
            double r5 = r12.A0W()
            double r1 = r12.A0Q()
            r14 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r1 = r1 / r14
            double r5 = r5 + r1
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r3.A0J()
            int r4 = X.C95184fz.A00(r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r3.A0J()
            int r3 = X.C95184fz.A01(r1)
            double r9 = r12.A0Q()
            r1 = 4598175219545276416(0x3fd0000000000000, double:0.25)
            double r9 = r9 * r1
            double r1 = (double) r4
            double r9 = r9 * r1
            double r1 = (double) r3
            double r9 = r9 / r1
            double r3 = r12.A0Q()
            r1 = 4610560118520545280(0x3ffc000000000000, double:1.75)
            double r3 = r3 * r1
            com.facebook.graphservice.factory.GraphQLServiceFactory r1 = X.C05850aR.A02()
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r8 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.A00(r1)
            double r1 = r12.A0V()
            double r1 = r1 - r9
            r8.A0F(r1)
            double r1 = r3 / r14
            double r5 = r5 - r1
            r8.A0G(r5)
            double r1 = r12.A0U()
            double r9 = r9 * r14
            double r1 = r1 + r9
            r8.A0E(r1)
            r8.A0C(r3)
            double r1 = r12.A0T()
            r8.A0D(r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r8.A03()
            android.view.View r1 = r0.getRootView()
            android.content.res.Resources r2 = r1.getResources()
            r1 = 2132148265(0x7f160029, float:1.9938503E38)
            int r1 = r2.getDimensionPixelSize(r1)
            int r1 = r1 << 1
            r4 = r33
            r5 = r16
            r6 = r7
            X.4gq r4 = r4.A06(r5, r6, r3, r1)
            r1 = r29
            X.C95644go.A03(r1, r4)
            com.google.common.collect.ImmutableList r2 = r35.A0T()
            if (r2 == 0) goto L_0x0597
            r1 = r34
            int r6 = r2.indexOf(r1)
        L_0x0521:
            X.4Kx r5 = new X.4Kx
            r3 = r35
            r2 = r34
            r1 = r37
            r5.<init>(r3, r2, r1, r6)
            r1 = r29
            r1.setOnTouchListener(r5)
            android.graphics.RectF r1 = r4.A01
            float r2 = r1.left
            float r1 = r1.width()
            int r1 = (int) r1
            int r1 = r1 / r13
            float r1 = (float) r1
            float r2 = r2 + r1
            r1 = r28
            r1.setTranslationX(r2)
            android.graphics.RectF r1 = r4.A01
            float r2 = r1.top
            float r1 = r1.height()
            int r1 = (int) r1
            int r1 = r1 / r13
            float r1 = (float) r1
            float r2 = r2 + r1
            r1 = r28
            r1.setTranslationY(r2)
            java.lang.String r3 = "6732"
            r6 = 0
            r1 = r32
            r2 = r36
            r4 = r0
            r5 = r28
            r1.A02(r2, r3, r4, r5, r6)
            if (r31 == 0) goto L_0x008c
            r1 = 0
            r0 = r29
            android.animation.AnimatorSet r2 = X.AnonymousClass1v5.A00(r0, r1)
            r1 = 1
            r0 = r26
            android.animation.AnimatorSet r0 = X.AnonymousClass1v5.A00(r0, r1)
            android.animation.AnimatorSet r1 = new android.animation.AnimatorSet
            r1.<init>()
            android.animation.Animator[] r0 = new android.animation.Animator[]{r2, r0}
            r1.playTogether(r0)
            r1.start()
            r3 = 0
            X.0zR r0 = r11.A04
            if (r0 == 0) goto L_0x008c
            X.2yh r2 = new X.2yh
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            r2.<init>(r1, r0)
            r11.A0E(r2)
            return
        L_0x0597:
            r6 = -1
            goto L_0x0521
        L_0x0599:
            android.view.View r3 = r0.getRootView()
            android.content.res.Resources r2 = r11.A03()
            r1 = 2132148265(0x7f160029, float:1.9938503E38)
            int r1 = r2.getDimensionPixelSize(r1)
            r2 = 1
            r5 = r33
            X.4gq r1 = r5.A06(r3, r7, r4, r1)
            r3 = r26
            X.C95644go.A03(r3, r1)
            r1 = 0
            r3.setVisibility(r1)
            goto L_0x0487
        L_0x05ba:
            double r14 = r24.A0U()
            double r14 = r14 + r5
            double r14 = r14 / r18
            double r3 = r3 - r14
            double r14 = r24.A0Q()
            goto L_0x05f0
        L_0x05c7:
            double r14 = r24.A0U()
            double r14 = r14 + r5
            double r14 = r14 / r18
            double r3 = r3 + r14
            double r14 = r24.A0Q()
            goto L_0x05e0
        L_0x05d4:
            double r14 = r24.A0U()
            double r14 = r14 + r5
            double r14 = r14 / r18
            double r3 = r3 - r14
            double r14 = r24.A0Q()
        L_0x05e0:
            double r14 = r14 * r16
            double r1 = r1 - r14
            goto L_0x05f3
        L_0x05e4:
            double r14 = r24.A0U()
            double r14 = r14 + r5
            double r14 = r14 / r18
            double r3 = r3 + r14
            double r14 = r24.A0Q()
        L_0x05f0:
            double r14 = r14 * r16
            double r1 = r1 + r14
        L_0x05f3:
            double r20 = r20 / r18
            android.graphics.RectF r14 = new android.graphics.RectF
            double r3 = r3 * r7
            double r7 = r3 - r20
            float r15 = (float) r7
            double r1 = r1 * r9
            double r7 = r1 - r20
            float r9 = (float) r7
            double r3 = r3 + r20
            float r7 = (float) r3
            double r1 = r1 + r20
            float r3 = (float) r1
            r14.<init>(r15, r9, r7, r3)
            double r1 = r24.A0V()
            double r3 = r24.A0U()
            double r3 = r3 / r18
            double r1 = r1 + r3
            float r3 = (float) r1
            float r4 = (float) r12
            float r3 = r3 * r4
            double r1 = r24.A0W()
            double r7 = r24.A0Q()
            double r7 = r7 / r18
            double r1 = r1 + r7
            float r9 = (float) r1
            r1 = r25
            float r8 = (float) r1
            float r9 = r9 * r8
            android.graphics.Matrix r10 = new android.graphics.Matrix
            r10.<init>()
            double r1 = r24.A0T()
            float r7 = (float) r1
            r10.preRotate(r7, r3, r9)
            r10.mapRect(r14)
            com.facebook.graphservice.factory.GraphQLServiceFactory r1 = X.C05850aR.A02()
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r7 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.A00(r1)
            float r1 = r14.centerX()
            float r1 = r1 / r4
            double r1 = (double) r1
            double r3 = r5 / r18
            double r1 = r1 - r3
            r7.A0F(r1)
            float r1 = r14.centerY()
            float r1 = r1 / r8
            double r1 = (double) r1
            double r3 = r22 / r18
            double r1 = r1 - r3
            r7.A0G(r1)
            r7.A0E(r5)
            r2 = r22
            r7.A0C(r2)
            double r1 = r24.A0T()
            r7.A0D(r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r7.A03()
            goto L_0x0464
        L_0x066b:
            java.lang.String r15 = "BOTTOM_LEFT"
            boolean r14 = r14.equals(r15)
            r15 = 3
            if (r14 != 0) goto L_0x0456
            goto L_0x0455
        L_0x0676:
            r15 = 99
            java.lang.String r15 = X.C99084oO.$const$string(r15)
            boolean r14 = r14.equals(r15)
            r15 = 2
            if (r14 != 0) goto L_0x0456
            goto L_0x0455
        L_0x0685:
            r15 = 129(0x81, float:1.81E-43)
            java.lang.String r15 = X.C99084oO.$const$string(r15)
            boolean r14 = r14.equals(r15)
            r15 = 0
            if (r14 != 0) goto L_0x0456
            goto L_0x0455
        L_0x0694:
            java.lang.String r15 = "TOP_RIGHT"
            boolean r14 = r14.equals(r15)
            r15 = 1
            if (r14 != 0) goto L_0x0456
            goto L_0x0455
        L_0x069f:
            r10 = r2
            X.1vB r10 = (X.AnonymousClass1vB) r10
            com.facebook.resources.ui.FbFrameLayout r0 = (com.facebook.resources.ui.FbFrameLayout) r0
            com.facebook.ipc.stories.model.StoryCard r1 = r10.A01
            r29 = r1
            X.4gh r1 = r10.A03
            r16 = r1
            X.4Jw r4 = r10.A02
            X.46A r1 = r10.A04
            r28 = r1
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r12 = r10.A00
            r1 = 0
            java.lang.Object r18 = X.AnonymousClass1XX.A02(r1, r2, r12)
            r1 = r18
            X.06B r1 = (X.AnonymousClass06B) r1
            r18 = r1
            int r2 = X.AnonymousClass1Y3.B6C
            r1 = 3
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r12)
            X.3Dz r2 = (X.C64873Dz) r2
            int r3 = X.AnonymousClass1Y3.A08
            r1 = 4
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r1, r3, r12)
            com.facebook.messaging.montage.forked.viewer.store.StoryFeedbackStore r9 = (com.facebook.messaging.montage.forked.viewer.store.StoryFeedbackStore) r9
            int r3 = X.AnonymousClass1Y3.ASp
            r1 = 2
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r3, r12)
            X.0mD r8 = (X.C11170mD) r8
            int r3 = X.AnonymousClass1Y3.A6Z
            r1 = 6
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r3, r12)
            java.util.concurrent.Executor r7 = (java.util.concurrent.Executor) r7
            int r3 = X.AnonymousClass1Y3.AM1
            r1 = 7
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r3, r12)
            X.4go r6 = (X.C95644go) r6
            int r3 = X.AnonymousClass1Y3.AoG
            r1 = 1
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r12)
            com.facebook.user.model.User r3 = (com.facebook.user.model.User) r3
            int r5 = X.AnonymousClass1Y3.B6l
            r1 = 5
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r5, r12)
            X.4h4 r5 = (X.AnonymousClass4h4) r5
            X.0Tq r1 = r10.A05
            r23 = r1
            r22 = r29
            X.3gF r10 = X.C90304Sw.A02(r29)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r13 = r10.A0S()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r29.A0J()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = X.C95184fz.A02(r1)
            if (r1 == 0) goto L_0x008c
            if (r13 == 0) goto L_0x008c
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r17 = r10.B9h()
            if (r17 == 0) goto L_0x008c
            java.lang.String r10 = r13.A3m()
            if (r10 == 0) goto L_0x008c
            java.lang.String r12 = r29.getId()
            java.lang.Class<X.1vB> r15 = X.AnonymousClass1vB.class
            java.lang.Object[] r14 = new java.lang.Object[]{r11, r5}
            r1 = 829436257(0x31703161, float:3.4952665E-9)
            X.10N r11 = A0E(r15, r11, r1, r14)
            r15 = 0
            java.lang.String r1 = "RatingStickerComponent"
            r4.CIQ(r12, r1, r15, r11)
            r14 = 0
            android.view.View r4 = r0.getChildAt(r14)
            r12 = 1
            r4.setFocusable(r12)
            android.content.res.Resources r11 = r4.getResources()
            r1 = 2131820989(0x7f1101bd, float:1.9274709E38)
            java.lang.String r1 = r11.getString(r1)
            r4.setContentDescription(r1)
            X.5Vu r11 = new X.5Vu
            android.content.Context r1 = r4.getContext()
            r11.<init>(r1, r2)
            r4.setLayerType(r12, r15)
            X.4gi r2 = new X.4gi
            r1 = r16
            r2.<init>(r1, r11)
            r4.setOnTouchListener(r2)
            X.473 r16 = r1.B9U()
            int r15 = X.C90304Sw.A00(r13)
            long r1 = r29.A08()
            com.google.common.collect.ImmutableList r12 = X.C90304Sw.A04(r13, r15, r12)
            r9.A06(r10, r1, r12)
            java.util.Map r1 = r9.A04
            boolean r1 = r1.containsKey(r10)
            r2 = -1
            if (r1 == 0) goto L_0x0825
            com.facebook.messaging.montage.forked.model.viewer.ViewerPollVoteInfo r1 = r9.A02(r10)
            if (r1 == 0) goto L_0x0825
            int r12 = r1.A01
        L_0x078e:
            if (r12 > r2) goto L_0x0794
            int r12 = X.C90304Sw.A00(r13)
        L_0x0794:
            r16 = 0
            if (r12 <= r2) goto L_0x079a
            r16 = 1
        L_0x079a:
            java.lang.String r2 = r3.A0j
            java.lang.String r1 = r29.getAuthorId()
            boolean r1 = r2.equals(r1)
            r15 = 0
            if (r1 == 0) goto L_0x0819
            r1 = -282985720(0xffffffffef21fb08, float:-5.0130565E28)
            boolean r1 = r13.getBooleanValue(r1)
            if (r1 == 0) goto L_0x07ca
            if (r16 != 0) goto L_0x07ca
            java.lang.String r2 = r13.A3m()
            java.util.Map r1 = r9.A03
            java.lang.Object r1 = r1.get(r2)
            com.google.common.base.Preconditions.checkNotNull(r1)
            com.facebook.messaging.montage.forked.model.viewer.PollVoteResults r1 = (com.facebook.messaging.montage.forked.model.viewer.PollVoteResults) r1
            com.google.common.collect.ImmutableList r1 = r1.A01
            int r1 = X.C90304Sw.A01(r1)
            if (r1 != 0) goto L_0x07ca
            r15 = 1
        L_0x07ca:
            android.view.View r1 = r0.getRootView()
            r19 = r1
            r20 = r17
            r21 = r6
            X.4gq r2 = X.C95244g5.A00(r19, r20, r21, r22)
            if (r16 == 0) goto L_0x0828
            r0 = 1065353216(0x3f800000, float:1.0)
            X.C95644go.A04(r4, r2)
            r4.setScaleY(r0)
            android.view.ViewParent r0 = r4.getParent()
            r0.requestLayout()
            X.C15320v6.setBackground(r4, r11)
            r0 = 2
            r11.A01 = r0
            java.lang.String r1 = r3.A0j
            java.lang.String r0 = r29.getAuthorId()
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0a33
            r0 = 4
            if (r12 != r0) goto L_0x080c
            r1 = 1065353216(0x3f800000, float:1.0)
        L_0x0800:
            int r0 = r12 + 1
            double r2 = (double) r0
            X.C112205Vu.A04(r11, r14, r2)
            r11.A00 = r1
            r11.invalidateSelf()
            return
        L_0x080c:
            float[] r2 = r11.A0A
            r1 = r2[r12]
            int r0 = r12 + 1
            r0 = r2[r0]
            float r1 = r1 + r0
            r0 = 1073741824(0x40000000, float:2.0)
            float r1 = r1 / r0
            goto L_0x0800
        L_0x0819:
            r1 = -282985720(0xffffffffef21fb08, float:-5.0130565E28)
            boolean r1 = r13.getBooleanValue(r1)
            if (r1 == 0) goto L_0x07ca
            r15 = r16 ^ 1
            goto L_0x07ca
        L_0x0825:
            r12 = -1
            goto L_0x078e
        L_0x0828:
            if (r15 != 0) goto L_0x084f
            r0 = 1065353216(0x3f800000, float:1.0)
            X.C95644go.A04(r4, r2)
            r4.setScaleY(r0)
            android.view.ViewParent r0 = r4.getParent()
            r0.requestLayout()
            X.C15320v6.setBackground(r4, r11)
            r0 = 1
            r11.A01 = r0
            java.lang.String r1 = r3.A0j
            java.lang.String r0 = r29.getAuthorId()
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0a33
            r11.A05(r14)
            return
        L_0x084f:
            r1 = 1069547520(0x3fc00000, float:1.5)
            X.C95644go.A04(r4, r2)
            r4.setScaleY(r1)
            android.view.ViewParent r1 = r4.getParent()
            r1.requestLayout()
            X.C15320v6.setBackground(r4, r11)
            r1 = 3
            r11.A01 = r1
            r1 = 1
            r11.A05(r1)
            java.lang.Object r1 = r23.get()
            X.4N8 r1 = (X.AnonymousClass4N8) r1
            java.lang.String r19 = r1.A01()
            android.view.View r25 = r0.getRootView()
            java.lang.String r3 = r3.A0j
            r21 = r3
            r24 = r4
            X.4gL r1 = new X.4gL
            r20 = r9
            r22 = r7
            r23 = r8
            r26 = r6
            r27 = r11
            r15 = r1
            r16 = r29
            r17 = r18
            r18 = r10
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            r11.A03 = r1
            java.lang.String r1 = r29.getAuthorId()
            boolean r1 = r3.equals(r1)
            if (r1 != 0) goto L_0x008c
            java.lang.String r10 = r29.A0V()
            java.lang.String r7 = "6683"
            r6 = r28
            r8 = r0
            r9 = r4
            r5.A02(r6, r7, r8, r9, r10)
            return
        L_0x08ac:
            r1 = r2
            X.0uf r1 = (X.C15060uf) r1
            X.1MR r0 = (X.AnonymousClass1MR) r0
            android.graphics.drawable.Drawable r2 = r1.A02
            r1 = 0
            r0.A02(r2, r1)
            return
        L_0x08b8:
            r1 = r2
            X.10H r1 = (X.AnonymousClass10H) r1
            X.1MR r0 = (X.AnonymousClass1MR) r0
            android.graphics.drawable.Drawable r2 = r1.A00
            X.1AN r1 = r1.A02
            r0.A02(r2, r1)
            return
        L_0x08c5:
            r3 = r2
            X.1JX r3 = (X.AnonymousClass1JX) r3
            android.widget.RelativeLayout r0 = (android.widget.RelativeLayout) r0
            java.lang.String r9 = r3.A05
            java.lang.String r5 = r3.A04
            long r6 = r3.A01
            long r1 = r3.A00
            int r8 = X.AnonymousClass1Y3.AgK
            X.0UN r4 = r3.A02
            r3 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r3, r8, r4)
            X.06B r4 = (X.AnonymousClass06B) r4
            android.view.View r3 = r0.getChildAt(r3)
            X.6je r3 = (X.C142086je) r3
            r3.A05 = r9
            r3.A04 = r4
            r3.A01 = r6
            r3.A00 = r1
            X.1I7 r1 = X.AnonymousClass1I7.SMALL_MEDIUM
            int r1 = r1.B5k()
            float r1 = (float) r1
            r4 = 2
            r3.setTextSize(r4, r1)
            X.1JZ r1 = X.AnonymousClass1JZ.A0I
            int r1 = r1.AhV()
            r3.setTextColor(r1)
            X.10M r2 = X.AnonymousClass10M.ROBOTO_MEDIUM
            android.content.Context r1 = r11.A09
            android.graphics.Typeface r1 = r2.A00(r1)
            r3.setTypeface(r1)
            r1 = 1
            android.view.View r2 = r0.getChildAt(r1)
            X.5UH r2 = (X.AnonymousClass5UH) r2
            r0 = 2132214319(0x7f17022f, float:2.0072477E38)
            r2.setBackgroundResource(r0)
            r2.setText(r5)
            X.1I7 r0 = X.AnonymousClass1I7.SMALL_MEDIUM
            int r0 = r0.B5k()
            float r0 = (float) r0
            r2.setTextSize(r4, r0)
            X.1JZ r0 = X.AnonymousClass1JZ.A0C
            int r0 = r0.AhV()
            r2.setTextColor(r0)
            X.10M r1 = X.AnonymousClass10M.ROBOTO_MEDIUM
            android.content.Context r0 = r11.A09
            android.graphics.Typeface r0 = r1.A00(r0)
            r2.setTypeface(r0)
            r3.A03 = r2
            return
        L_0x093b:
            r1 = r2
            X.1JV r1 = (X.AnonymousClass1JV) r1
            android.view.View r0 = (android.view.View) r0
            java.lang.String r4 = r1.A01
            X.9oG r0 = (X.C206439oG) r0
            r2 = 2132412137(0x7f1a06e9, float:2.04737E38)
            int r1 = r0.A01
            if (r2 == r1) goto L_0x0963
            r0.A01 = r2
            r0.removeAllViews()
            int r1 = r0.A01
            r0.setContentView(r1)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0.A0B = r1
            int r2 = r0.A02
            boolean r1 = r0.A0D
            com.facebook.widget.splitinput.SplitFieldCodeInputView.A02(r0, r2, r1)
        L_0x0963:
            X.10M r2 = X.AnonymousClass10M.ROBOTO_BOLD
            android.content.Context r1 = r11.A09
            android.graphics.Typeface r3 = r2.A00(r1)
            java.util.ArrayList r1 = r0.A0A
            java.util.Iterator r2 = r1.iterator()
        L_0x0971:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x0981
            java.lang.Object r1 = r2.next()
            android.widget.EditText r1 = (android.widget.EditText) r1
            r1.setTypeface(r3)
            goto L_0x0971
        L_0x0981:
            X.9oF r3 = new X.9oF
            r2 = 2132346604(0x7f1906ec, float:2.0340782E38)
            r1 = 3
            r3.<init>(r1, r2)
            r0.A08 = r3
            X.9oF r3 = new X.9oF
            r2 = 2132214783(0x7f1703ff, float:2.0073418E38)
            r1 = 1
            r3.<init>(r1, r2)
            r0.A09 = r3
            boolean r1 = android.text.TextUtils.isEmpty(r4)
            if (r1 == 0) goto L_0x09a1
            r0.A03()
            return
        L_0x09a1:
            r0.A04(r4)
            return
        L_0x09a5:
            r1 = 2131298005(0x7f0906d5, float:1.821397E38)
            android.view.View r6 = r0.findViewById(r1)
            java.lang.String r7 = r5.A0V()
            java.lang.String r4 = "7051"
            r5 = r0
            r2.A02(r3, r4, r5, r6, r7)
            return
        L_0x09b7:
            r1 = r2
            X.1vC r1 = (X.AnonymousClass1vC) r1
            com.facebook.resources.ui.FbFrameLayout r0 = (com.facebook.resources.ui.FbFrameLayout) r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9 = r1.A03
            android.view.View$OnTouchListener r4 = r1.A01
            X.44o r8 = r1.A05
            int r2 = r1.A00
            X.303 r5 = r1.A02
            java.lang.Integer r7 = r1.A07
            java.lang.Integer r6 = r1.A06
            if (r2 == 0) goto L_0x09fc
            r1 = 2
            if (r2 == r1) goto L_0x09e8
            r1 = 3
            if (r2 != r1) goto L_0x09e4
            r1 = 2131300732(0x7f09117c, float:1.8219502E38)
            android.view.View r2 = r0.findViewById(r1)
            com.facebook.drawee.fbpipeline.FbDraweeView r2 = (com.facebook.drawee.fbpipeline.FbDraweeView) r2
            if (r2 == 0) goto L_0x09e4
            r1 = 0
            r2.setVisibility(r1)
            r2.A08(r5)
        L_0x09e4:
            r0.setOnTouchListener(r4)
            return
        L_0x09e8:
            r1 = 2131300732(0x7f09117c, float:1.8219502E38)
            android.view.View r2 = r0.findViewById(r1)
            com.facebook.drawee.fbpipeline.FbDraweeView r2 = (com.facebook.drawee.fbpipeline.FbDraweeView) r2
            if (r2 == 0) goto L_0x09e4
            r1 = 4
            r2.setVisibility(r1)
            r1 = 0
            r2.A08(r1)
            goto L_0x09e4
        L_0x09fc:
            r0.removeAllViews()
            android.content.Context r2 = r0.getContext()
            com.facebook.drawee.fbpipeline.FbDraweeView r1 = new com.facebook.drawee.fbpipeline.FbDraweeView
            r1.<init>(r2)
            X.Dp9 r2 = new X.Dp9
            r2.<init>(r1)
            r1 = 2131300732(0x7f09117c, float:1.8219502E38)
            r2.A02(r1)
            r1 = -1
            r2.A00(r1, r1)
            android.view.View r3 = r2.A00
            com.facebook.drawee.fbpipeline.FbDraweeView r3 = (com.facebook.drawee.fbpipeline.FbDraweeView) r3
            int r2 = r7.intValue()
            int r1 = r6.intValue()
            X.C95644go.A02(r2, r1, r3, r9)
            r3.A08(r5)
            r0.addView(r3)
            X.0p4 r2 = r8.A00
            r1 = 1
            X.AnonymousClass1v6.A00(r2, r1)
            goto L_0x09e4
        L_0x0a33:
            X.AnonymousClass1vB.A00(r13, r10, r9, r11, r14)
            return
        L_0x0a37:
            r1 = r2
            X.1KF r1 = (X.AnonymousClass1KF) r1
            X.ESq r0 = (X.C29268ESq) r0
            com.google.common.collect.ImmutableList r2 = r1.A02
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r1.A01
            r0.A07(r2)
            int r1 = r1.B9m()
            android.graphics.Paint r0 = r0.A04
            r0.setColor(r1)
            return
        L_0x0a4d:
            r1 = r2
            X.1ku r1 = (X.C31981ku) r1
            X.ESq r0 = (X.C29268ESq) r0
            com.google.common.collect.ImmutableList r2 = r1.A02
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r1.A01
            r0.A07(r2)
            int r1 = r1.B9m()
            android.graphics.Paint r0 = r0.A04
            r0.setColor(r1)
            return
        L_0x0a63:
            r1 = r2
            X.1I8 r1 = (X.AnonymousClass1I8) r1
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            int r3 = r1.A00
            int r2 = r1.A01
            r1 = 0
            android.graphics.drawable.Drawable r1 = X.AnonymousClass1M2.A00(r1, r3, r2)
            r0.setBackgroundDrawable(r1)
            return
        L_0x0a75:
            X.1NM r2 = (X.AnonymousClass1NM) r2
            X.1Kq r0 = (X.C22261Kq) r0
            com.facebook.litho.ComponentTree r10 = r2.A04
            com.facebook.litho.ComponentTree r11 = r2.A05
            java.lang.Integer r1 = r2.A0C
            int r9 = r1.intValue()
            java.lang.Integer r1 = r2.A0B
            int r8 = r1.intValue()
            java.lang.Integer r1 = r2.A0D
            int r7 = r1.intValue()
            java.lang.Integer r1 = r2.A0J
            int r6 = r1.intValue()
            X.1BY r5 = r2.A08
            X.1MD r4 = r2.A0A
            java.lang.String r3 = r2.A0K
            r2 = 1
            r0.A06 = r11
            r0.A02 = r9
            r0.A01 = r8
            r0.A04 = r7
            r0.A05 = r6
            r0.A07 = r5
            r0.A08 = r4
            r0.A0B = r3
            r0.A0C = r2
            r0.A0D = r2
            if (r5 == 0) goto L_0x0ab7
            java.util.Set r1 = r5.A01
            r1.add(r0)
        L_0x0ab7:
            com.facebook.litho.LithoView r1 = r0.A0G
            r1.A0b(r10)
            X.C22261Kq.A03(r0)
            return
        L_0x0ac0:
            r3 = r2
            X.1KX r3 = (X.AnonymousClass1KX) r3
            X.1MR r0 = (X.AnonymousClass1MR) r0
            X.1JY r1 = r3.A0B
            r21 = r1
            boolean r1 = r3.A0J
            r20 = r1
            boolean r1 = r3.A0I
            r19 = r1
            int r1 = r3.A04
            r18 = r1
            int r15 = r3.A03
            boolean r14 = r3.A0K
            android.graphics.drawable.Drawable r13 = r3.A08
            X.1KU r12 = r3.A0D
            int r10 = r3.A05
            float r9 = r3.A01
            android.graphics.Typeface r8 = r3.A07
            float r7 = r3.A00
            X.1KZ r6 = r3.A0C
            android.graphics.Path r2 = r3.A06
            int r5 = r3.A02
            X.1KY r1 = r3.A0A
            X.1Kc r1 = r1.userTileDrawableCachingBuilder
            r17 = r1
            java.lang.Integer r1 = r3.A0H
            int r16 = r1.intValue()
            java.lang.Integer r1 = r3.A0G
            int r4 = r1.intValue()
            X.1Jd r3 = r17.A01()
            android.content.Context r11 = r11.A09
            r1 = r17
            r1.A06 = r11
            boolean r11 = r1.A0E
            r1 = r20
            if (r1 != r11) goto L_0x0b77
            r1 = r17
            boolean r11 = r1.A0D
            r1 = r19
            if (r1 != r11) goto L_0x0b77
            r1 = r17
            int r11 = r1.A04
            r1 = r18
            if (r1 != r11) goto L_0x0b77
            r1 = r17
            int r1 = r1.A03
            if (r15 != r1) goto L_0x0b77
            r1 = r17
            boolean r1 = r1.A0G
            if (r14 != r1) goto L_0x0b77
            r1 = r17
            android.graphics.drawable.Drawable r1 = r1.A09
            if (r13 != r1) goto L_0x0b77
            r1 = r17
            X.1KU r1 = r1.A0C
            if (r12 != r1) goto L_0x0b77
            r1 = r17
            int r1 = r1.A05
            if (r10 != r1) goto L_0x0b77
            r1 = r17
            float r1 = r1.A01
            int r1 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r1 != 0) goto L_0x0b77
            r1 = r17
            android.graphics.Typeface r1 = r1.A08
            if (r8 != r1) goto L_0x0b77
            r1 = r17
            float r1 = r1.A00
            int r1 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r1 != 0) goto L_0x0b77
            r1 = r17
            X.1KZ r1 = r1.A0B
            boolean r1 = com.google.common.base.Objects.equal(r6, r1)
            if (r1 == 0) goto L_0x0b77
            r1 = r17
            android.graphics.Path r1 = r1.A07
            boolean r1 = com.google.common.base.Objects.equal(r2, r1)
            if (r1 == 0) goto L_0x0b77
            r1 = r17
            X.1JY r11 = r1.A0A
            r1 = r21
            boolean r1 = com.google.common.base.Objects.equal(r1, r11)
            if (r1 == 0) goto L_0x0b77
            r1 = r17
            int r1 = r1.A02
            if (r5 == r1) goto L_0x0ba2
        L_0x0b77:
            r11 = r20
            r1 = r17
            r1.A0E = r11
            r11 = r19
            r1.A0D = r11
            r11 = r18
            r1.A04 = r11
            r1.A03 = r15
            r1.A0G = r14
            r1.A09 = r13
            r1.A0C = r12
            r1.A05 = r10
            r1.A01 = r9
            r1.A08 = r8
            r1.A00 = r7
            r1.A0B = r6
            r1.A07 = r2
            r2 = r21
            r1.A0A = r2
            r1.A02 = r5
            r2 = 1
            r1.A0F = r2
        L_0x0ba2:
            X.1Jd r5 = r17.A01()
            android.graphics.drawable.Drawable r1 = r0.A00
            if (r1 == 0) goto L_0x0bbc
            if (r3 == 0) goto L_0x0bbc
            if (r3 == r5) goto L_0x0bbc
            java.lang.String r1 = "onDetach"
            X.C27041cY.A01(r1)
            r3.A08()
            r3.A06()
            X.C27041cY.A00()
        L_0x0bbc:
            r8 = r16
            r7 = 0
            android.graphics.drawable.Drawable r3 = r5.A04
            int r2 = r16 - r7
            int r1 = r4 - r7
            r3.setBounds(r7, r7, r2, r1)
            android.graphics.drawable.Drawable r1 = r5.A03
            if (r1 == 0) goto L_0x0bcf
            r1.setBounds(r7, r7, r8, r4)
        L_0x0bcf:
            android.graphics.drawable.Drawable r3 = r5.A04
            java.lang.String r1 = "createDrawableMatrix"
            X.C27041cY.A01(r1)
            r2 = 0
            X.1AN r2 = X.AnonymousClass1AN.A00(r3, r2, r8, r4)
            X.C27041cY.A00()
            java.lang.String r1 = "mountMatrixDrawable"
            X.C27041cY.A01(r1)
            r0.A02(r3, r2)
            X.C27041cY.A00()
            java.lang.String r0 = "userTileDrawableController:onAttach"
            X.C27041cY.A01(r0)
            r5.A07()
            X.C27041cY.A00()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A0j(X.0p4, java.lang.Object):void");
    }

    public void A0k(AnonymousClass0p4 r11, Object obj) {
        Object obj2 = obj;
        if ((this instanceof C31981ku) || (this instanceof AnonymousClass1KF)) {
            ((C29268ESq) obj2).A04();
        } else if (this instanceof AnonymousClass1JV) {
            C206439oG r5 = (C206439oG) ((View) obj2);
            r5.A00 = null;
            r5.A07 = null;
        } else if (this instanceof C17020yC) {
            RelativeLayout relativeLayout = (RelativeLayout) obj2;
            C29819EjC ejC = (C29819EjC) relativeLayout.getChildAt(0);
            ejC.removeTextChangedListener(ejC.A07);
            ejC.A01 = null;
            ejC.A02 = null;
            C29825EjI ejI = (C29825EjI) relativeLayout.getChildAt(1);
            ejI.setOnClickListener(null);
            ejI.A02 = null;
        } else if (this instanceof AnonymousClass10I) {
            RelativeLayout relativeLayout2 = (RelativeLayout) obj2;
            C29818EjB ejB = (C29818EjB) relativeLayout2.getChildAt(0);
            ejB.removeTextChangedListener(ejB.A05);
            ejB.A01 = null;
            ejB.A02 = null;
            C29822EjF ejF = (C29822EjF) relativeLayout2.getChildAt(1);
            ejF.setOnClickListener(null);
            ejF.A00 = null;
        } else if (this instanceof AnonymousClass1JW) {
            C29796Eim eim = (C29796Eim) ((View) obj2);
            eim.setOnItemSelectedListener(null);
            eim.A01 = null;
        } else if (this instanceof AnonymousClass1JX) {
            RelativeLayout relativeLayout3 = (RelativeLayout) obj2;
            C142086je r2 = (C142086je) relativeLayout3.getChildAt(0);
            AnonymousClass5UH r1 = (AnonymousClass5UH) relativeLayout3.getChildAt(1);
            CountDownTimer countDownTimer = r2.A02;
            if (countDownTimer != null) {
                countDownTimer.cancel();
                r2.A02 = null;
            }
            r1.A00 = null;
            r1.setOnClickListener(null);
        } else if (this instanceof AnonymousClass1JE) {
            AnonymousClass1JE r3 = (AnonymousClass1JE) this;
            C74093hG r52 = (C74093hG) obj2;
            C27482DdQ ddQ = r3.A04;
            AnonymousClass852 r6 = (AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, r3.A09);
            C156827Mz r0 = r3.A07;
            CallerContext callerContext = r0.callerContextWithContextChain;
            C27481DdP ddP = r0.frescoState;
            C27484DdS ddS = r6.A00(callerContext, ddQ).A04;
            if (ddS.A0B() && ddP != null) {
                AnonymousClass1JE.A02(ddP);
                C22015Aml aml = new C22015Aml(r52, r6, callerContext, ddQ, ddP);
                synchronized (ddP) {
                    ddP.A0G = aml;
                }
                AnonymousClass00S.A05(AnonymousClass1JE.A0A, aml, 80, 491837549);
            } else if (ddS.A0S()) {
                AnonymousClass1JE.A01(r52, r6, callerContext, ddQ, ddP);
            }
        }
    }

    public void A0l(AnonymousClass0p4 r7, Object obj) {
        if (this instanceof AnonymousClass1KX) {
            C21901Jd A012 = ((AnonymousClass1KX) this).A0A.userTileDrawableCachingBuilder.A01();
            A012.A08();
            A012.A06();
            ((AnonymousClass1MR) obj).A01();
        } else if (this instanceof AnonymousClass1NM) {
            C22261Kq r8 = (C22261Kq) obj;
            AnonymousClass1BY r0 = r8.A07;
            if (r0 != null) {
                r0.A01.remove(r8);
            }
            r8.A02 = 0;
            r8.A01 = 0;
            r8.A04 = 0;
            r8.A05 = 0;
            r8.A07 = null;
            r8.A08 = null;
            r8.A0C = true;
            r8.A0D = true;
            r8.A0G.A0V();
            r8.A0G.A0b(null);
            C22261Kq.A03(r8);
        } else if (this instanceof AnonymousClass1v5) {
            AnonymousClass5XZ r02 = ((AnonymousClass1v5) this).A01;
            if (r02 != null) {
                AnonymousClass1PS.A05(r02.A0O);
            }
        } else if (this instanceof AnonymousClass1vC) {
            AnonymousClass303 r03 = ((AnonymousClass1vC) this).A02;
            if (r03 != null) {
                Animatable Acz = r03.Acz();
                if (Acz instanceof C62022zs) {
                    Drawable current = ((C62022zs) Acz).getCurrent();
                    if (current instanceof C23431Bex) {
                        ((C23431Bex) current).pause();
                    }
                }
            }
        } else if (this instanceof AnonymousClass1vD) {
            FbFrameLayout fbFrameLayout = (FbFrameLayout) obj;
            C95464gT r3 = (C95464gT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgM, ((AnonymousClass1vD) this).A00);
            if (r3.A04 != null) {
                for (int i = 0; i < r3.A0C.size(); i++) {
                    ((FbAutoFitTextView) r3.A0C.get(i)).setText(BuildConfig.FLAVOR);
                    ((FbAutoFitTextView) r3.A0A.get(i)).setText(BuildConfig.FLAVOR);
                    View view = (View) r3.A09.get(i);
                    C95464gT.A07(r3, view, C95464gT.A0C(r3, C95464gT.A0U), (GradientDrawable) view.getBackground());
                }
            }
            if (r3.A00 > 0.0f) {
                for (FbAutoFitTextView fbAutoFitTextView : r3.A0C) {
                    fbAutoFitTextView.setVisibility(4);
                    fbAutoFitTextView.setTextSize(0, r3.A00);
                }
            }
            r3.A0E(fbFrameLayout.getContext());
            r3.A0D = false;
            if (r3.A04 != null) {
                AnimatorSet animatorSet = r3.A02;
                if (animatorSet != null) {
                    animatorSet.cancel();
                    r3.A02 = null;
                }
                r3.A04.clearAnimation();
            }
            r3.A0E = false;
            r3.A0F = false;
            r3.A0H = false;
            r3.A0D = false;
            r3.A00 = -1.0f;
            r3.A02 = null;
            r3.A04 = null;
        } else if (this instanceof C37331vE) {
            C37331vE r32 = (C37331vE) this;
            FbFrameLayout fbFrameLayout2 = (FbFrameLayout) obj;
            C95254g6 r4 = (C95254g6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AAn, r32.A01);
            AnonymousClass303 r04 = r32.A00;
            if (r04 != null) {
                Animatable Acz2 = r04.Acz();
                if (Acz2 instanceof C62022zs) {
                    Drawable current2 = ((C62022zs) Acz2).getCurrent();
                    if (current2 instanceof C23431Bex) {
                        ((C23431Bex) current2).pause();
                    }
                }
                Context context = fbFrameLayout2.getContext();
                C90454To r05 = (C90454To) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AjS, r4.A02);
                C90474Tq r1 = r4.A04;
                C90454To.A01(r05, context);
                r05.A03.remove(r1);
                r4.A01 = null;
                r4.A00 = null;
            }
        } else if (!(this instanceof C14910uL)) {
            if (!(this instanceof AnonymousClass10H)) {
                if (this instanceof C17670zH) {
                    ComponentHost componentHost = (ComponentHost) obj;
                    if (componentHost.isPressed()) {
                        componentHost.setPressed(false);
                        return;
                    }
                    return;
                } else if (!(this instanceof C15060uf)) {
                    if (this instanceof AnonymousClass1JE) {
                        AnonymousClass1JE r33 = (AnonymousClass1JE) this;
                        C74093hG r82 = (C74093hG) obj;
                        C27482DdQ ddQ = r33.A04;
                        AnonymousClass852 r42 = (AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, r33.A09);
                        C156827Mz r06 = r33.A07;
                        CallerContext callerContext = r06.callerContextWithContextChain;
                        C27481DdP ddP = r06.frescoState;
                        C27484DdS ddS = r42.A00(callerContext, ddQ).A04;
                        if (!ddS.A0S() && ddS.A0P()) {
                            if (ddS.A0B() && ddP != null) {
                                AnonymousClass1JE.A02(ddP);
                            }
                            AnonymousClass1JE.A01(r82, r42, callerContext, ddQ, ddP);
                            return;
                        }
                        return;
                    }
                    return;
                }
            }
            ((AnonymousClass1MR) obj).A01();
        } else {
            AnonymousClass0d2 r83 = (AnonymousClass0d2) obj;
            CharSequence charSequence = ((C14910uL) this).A0c;
            r83.A0A = null;
            r83.A01 = 0.0f;
            r83.A0E = null;
            r83.A0K = null;
            r83.A0J = false;
            r83.A02 = 0;
            r83.A0D = null;
            r83.A0C = null;
            r83.A06 = null;
            r83.A05 = 0;
            ImageSpan[] imageSpanArr = r83.A0L;
            if (imageSpanArr != null) {
                int length = imageSpanArr.length;
                for (int i2 = 0; i2 < length; i2++) {
                    Drawable drawable = r83.A0L[i2].getDrawable();
                    drawable.setCallback(null);
                    drawable.setVisible(false, false);
                }
                r83.A0L = null;
            }
            if (charSequence instanceof C24431Tp) {
                ((C24431Tp) charSequence).BtF(r83);
            }
        }
    }

    public void A0n(AnonymousClass11I r2, AnonymousClass11I r3) {
        if (this instanceof AnonymousClass1KX) {
            ((AnonymousClass1KY) r3).userTileDrawableCachingBuilder = ((AnonymousClass1KY) r2).userTileDrawableCachingBuilder;
        } else if (this instanceof AnonymousClass1JF) {
            AnonymousClass1RF r22 = (AnonymousClass1RF) r2;
            AnonymousClass1RF r32 = (AnonymousClass1RF) r3;
            r32.pressed = r22.pressed;
            r32._transition = r22._transition;
        } else if (this instanceof C37301v3) {
            ((AnonymousClass5CM) r3).version = ((AnonymousClass5CM) r2).version;
        } else if (this instanceof C31971kt) {
            ((C33061mm) r3).storiesTrayAccessible = ((C33061mm) r2).storiesTrayAccessible;
        } else if (this instanceof C37311v4) {
            ((AnonymousClass5CN) r3).selectedOverlay = ((AnonymousClass5CN) r2).selectedOverlay;
        } else if (this instanceof AnonymousClass1v5) {
            ((AnonymousClass5CS) r3).shouldAnimateScale = ((AnonymousClass5CS) r2).shouldAnimateScale;
        } else if (this instanceof AnonymousClass1v6) {
            ((AnonymousClass5CG) r3).affordanceAnimationState = ((AnonymousClass5CG) r2).affordanceAnimationState;
        } else if (this instanceof AnonymousClass1JJ) {
            AnonymousClass5EU r23 = (AnonymousClass5EU) r2;
            AnonymousClass5EU r33 = (AnonymousClass5EU) r3;
            r33.currentPasswordContainer = r23.currentPasswordContainer;
            r33.newPasswordContainer = r23.newPasswordContainer;
            r33.retypedPasswordContainer = r23.retypedPasswordContainer;
            r33.saveButtonEnabled = r23.saveButtonEnabled;
        } else if (this instanceof AnonymousClass1JK) {
            ((AnonymousClass5CL) r3).version = ((AnonymousClass5CL) r2).version;
        } else if (this instanceof AnonymousClass11H) {
            ((AnonymousClass5CK) r3).version = ((AnonymousClass5CK) r2).version;
        } else if (this instanceof C37321v7) {
            ((AnonymousClass5CI) r3).selectedRecoveryType = ((AnonymousClass5CI) r2).selectedRecoveryType;
        } else if (this instanceof AnonymousClass1v8) {
            ((AnonymousClass5CH) r3).selectedSecurityType = ((AnonymousClass5CH) r2).selectedSecurityType;
        } else if (this instanceof AnonymousClass1JN) {
            ((AnonymousClass5CJ) r3).version = ((AnonymousClass5CJ) r2).version;
        } else if (this instanceof AnonymousClass1JO) {
            AnonymousClass5EV r24 = (AnonymousClass5EV) r2;
            AnonymousClass5EV r34 = (AnonymousClass5EV) r3;
            r34.stateContainer = r24.stateContainer;
            r34.version = r24.version;
        } else if (this instanceof AnonymousClass1JE) {
            C156827Mz r25 = (C156827Mz) r2;
            C156827Mz r35 = (C156827Mz) r3;
            r35.callerContextWithContextChain = r25.callerContextWithContextChain;
            r35.frescoState = r25.frescoState;
            r35.lastFrescoState = r25.lastFrescoState;
            r35.prefetchData = r25.prefetchData;
        }
    }

    public void A0o(AnonymousClass1KE r3) {
        if (this instanceof C31961ks) {
            C31961ks r1 = (C31961ks) this;
            if (r3 != null) {
                r1.A02 = (C16400x0) r3.A01(C16400x0.class);
                r1.A01 = (ThreadSummary) r3.A01(ThreadSummary.class);
            }
        } else if (this instanceof AnonymousClass1RG) {
            AnonymousClass1RG r12 = (AnonymousClass1RG) this;
            if (r3 != null) {
                r12.A00 = (InboxUnitThreadItem) r3.A01(InboxUnitThreadItem.class);
                r12.A02 = (C16400x0) r3.A01(C16400x0.class);
                r12.A01 = (ThreadSummary) r3.A01(ThreadSummary.class);
            }
        } else if (this instanceof C22021Jp) {
            C22021Jp r13 = (C22021Jp) this;
            if (r3 != null) {
                r13.A01 = (ThreadSummary) r3.A01(ThreadSummary.class);
            }
        } else if (this instanceof AnonymousClass1RJ) {
            AnonymousClass1RJ r14 = (AnonymousClass1RJ) this;
            if (r3 != null) {
                r14.A02 = (ThreadSummary) r3.A01(ThreadSummary.class);
            }
        } else if (this instanceof C22161Kd) {
            C22161Kd r15 = (C22161Kd) this;
            if (r3 != null) {
                r15.A02 = (C16400x0) r3.A01(C16400x0.class);
                r15.A01 = (ThreadSummary) r3.A01(ThreadSummary.class);
            }
        } else if (this instanceof AnonymousClass1RH) {
            AnonymousClass1RH r16 = (AnonymousClass1RH) this;
            if (r3 != null) {
                r16.A02 = (C16400x0) r3.A01(C16400x0.class);
                r16.A01 = (ThreadSummary) r3.A01(ThreadSummary.class);
            }
        } else if (this instanceof AnonymousClass1JF) {
            AnonymousClass1JF r17 = (AnonymousClass1JF) this;
            if (r3 != null) {
                r17.A01 = (BasicMontageThreadInfo) r3.A01(BasicMontageThreadInfo.class);
            }
        } else if (this instanceof AnonymousClass1J1) {
            AnonymousClass1J1 r18 = (AnonymousClass1J1) this;
            if (r3 != null) {
                r18.A02 = (ThreadSummary) r3.A01(ThreadSummary.class);
            }
        } else if (this instanceof AnonymousClass1JE) {
            AnonymousClass1JE r19 = (AnonymousClass1JE) this;
            if (r3 != null) {
                r19.A03 = (ContextChain) r3.A01(ContextChain.class);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r9.equals("imagePrefetch") == false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0p(java.lang.String r9) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.AnonymousClass1JE
            if (r0 == 0) goto L_0x006a
            r3 = r8
            X.1JE r3 = (X.AnonymousClass1JE) r3
            int r1 = r9.hashCode()
            r0 = 77595762(0x4a00472, float:3.76199E-36)
            if (r1 != r0) goto L_0x0019
            java.lang.String r0 = "imagePrefetch"
            boolean r0 = r9.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r1 = -1
        L_0x001a:
            if (r1 != 0) goto L_0x006a
            android.net.Uri r6 = r3.A01
            X.3JC r5 = r3.A08
            X.DdQ r7 = r3.A04
            int r2 = X.AnonymousClass1Y3.ANV
            X.0UN r1 = r3.A09
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.852 r1 = (X.AnonymousClass852) r1
            X.7Mz r0 = r3.A07
            com.facebook.common.callercontext.CallerContext r4 = r0.callerContextWithContextChain
            java.util.concurrent.atomic.AtomicReference r3 = r0.prefetchData
            X.DdQ r2 = r1.A00(r4, r7)
            java.lang.Object r0 = r3.get()
            X.1QO r0 = (X.AnonymousClass1QO) r0
            if (r0 == 0) goto L_0x0042
            r0.AT6()
        L_0x0042:
            r0 = 0
            r3.set(r0)
            if (r6 == 0) goto L_0x006a
            X.DdS r0 = r2.A04
            boolean r0 = r0.A0D()
            if (r0 == 0) goto L_0x006a
            X.DdN r0 = r2.A01
            if (r0 != 0) goto L_0x005b
            X.DdN r0 = new X.DdN
            r0.<init>(r2)
            r2.A01 = r0
        L_0x005b:
            X.DdN r1 = r2.A01
            X.DdS r0 = r2.A04
            java.lang.Integer r0 = r0.A05()
            X.1QO r0 = r1.A00(r0, r6, r5, r4)
            r3.set(r0)
        L_0x006a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A0p(java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r4.equals("imagePrefetch") == false) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0q(java.lang.String r4) {
        /*
            r3 = this;
            boolean r0 = r3 instanceof X.AnonymousClass1JE
            if (r0 == 0) goto L_0x002f
            r2 = r3
            X.1JE r2 = (X.AnonymousClass1JE) r2
            int r1 = r4.hashCode()
            r0 = 77595762(0x4a00472, float:3.76199E-36)
            if (r1 != r0) goto L_0x0019
            java.lang.String r0 = "imagePrefetch"
            boolean r0 = r4.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            r1 = -1
        L_0x001a:
            if (r1 != 0) goto L_0x002f
            X.7Mz r0 = r2.A07
            java.util.concurrent.atomic.AtomicReference r1 = r0.prefetchData
            java.lang.Object r0 = r1.get()
            X.1QO r0 = (X.AnonymousClass1QO) r0
            if (r0 == 0) goto L_0x002b
            r0.AT6()
        L_0x002b:
            r0 = 0
            r1.set(r0)
        L_0x002f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A0q(java.lang.String):void");
    }

    public boolean A0r() {
        return (this instanceof AnonymousClass10H) || (this instanceof AnonymousClass1JE);
    }

    public boolean A0s() {
        return (this instanceof AnonymousClass1KX) || (this instanceof AnonymousClass1v9) || (this instanceof AnonymousClass1NM) || (this instanceof AnonymousClass1RX) || (this instanceof C31981ku) || (this instanceof AnonymousClass1KF) || (this instanceof AnonymousClass1JU) || (this instanceof AnonymousClass1vB) || (this instanceof AnonymousClass1v5) || (this instanceof AnonymousClass1JV) || (this instanceof C17020yC) || (this instanceof AnonymousClass10I) || (this instanceof AnonymousClass1JW) || (this instanceof AnonymousClass1JX) || (this instanceof C21961Jj) || (this instanceof AnonymousClass1JK) || (this instanceof C21971Jk) || (this instanceof C22241Km) || (this instanceof AnonymousClass1QG) || (this instanceof AnonymousClass11H) || (this instanceof C37321v7) || (this instanceof AnonymousClass1vF) || (this instanceof AnonymousClass1v8) || (this instanceof AnonymousClass1JN) || (this instanceof AnonymousClass1JO) || (this instanceof C14910uL) || (this instanceof AnonymousClass10H) || (this instanceof AnonymousClass1KM) || (this instanceof AnonymousClass1JE);
    }

    public boolean A0t() {
        if ((this instanceof AnonymousClass1KX) || (this instanceof AnonymousClass1v9) || (this instanceof AnonymousClass1NM) || (this instanceof AnonymousClass1I8) || (this instanceof C31981ku) || (this instanceof AnonymousClass1KF) || (this instanceof AnonymousClass1vA) || (this instanceof AnonymousClass1JU) || (this instanceof AnonymousClass1vB) || (this instanceof AnonymousClass1v5) || (this instanceof AnonymousClass1vC) || (this instanceof AnonymousClass1vD) || (this instanceof C37331vE) || (this instanceof AnonymousClass1JV) || (this instanceof C17020yC) || (this instanceof AnonymousClass10I) || (this instanceof AnonymousClass1JW) || (this instanceof AnonymousClass1JX) || (this instanceof C14910uL) || (this instanceof AnonymousClass10H)) {
            return false;
        }
        boolean z = this instanceof AnonymousClass1JE;
        return false;
    }

    public boolean A0u() {
        return this instanceof AnonymousClass1JE;
    }

    public boolean A0v() {
        return this instanceof AnonymousClass1NM;
    }

    public boolean A0w() {
        return (this instanceof AnonymousClass1KX) || (this instanceof AnonymousClass1JF) || (this instanceof C37301v3) || (this instanceof C31971kt) || (this instanceof C37311v4) || (this instanceof AnonymousClass1v5) || (this instanceof AnonymousClass1v6) || (this instanceof AnonymousClass1JJ) || (this instanceof AnonymousClass1JK) || (this instanceof AnonymousClass11H) || (this instanceof C37321v7) || (this instanceof AnonymousClass1v8) || (this instanceof AnonymousClass1JN) || (this instanceof AnonymousClass1JO) || (this instanceof AnonymousClass1JE);
    }

    public boolean A0x() {
        return (this instanceof AnonymousClass1KX) || (this instanceof C14910uL);
    }

    public boolean A0y() {
        return this instanceof C14910uL;
    }

    public boolean A0z() {
        return (this instanceof AnonymousClass1KX) || (this instanceof AnonymousClass1NM) || (this instanceof AnonymousClass1vC) || (this instanceof C14910uL) || (this instanceof AnonymousClass10H);
    }

    public boolean A10() {
        return (this instanceof AnonymousClass1I8) || (this instanceof AnonymousClass1vD) || (this instanceof C37331vE) || (this instanceof AnonymousClass1JV) || (this instanceof C17020yC) || (this instanceof AnonymousClass10I) || (this instanceof AnonymousClass1JW) || (this instanceof AnonymousClass1JX) || (this instanceof C14910uL) || (this instanceof AnonymousClass10H) || (this instanceof C15060uf) || (this instanceof AnonymousClass1JE);
    }

    public boolean A11() {
        return false;
    }

    public Object ALY(C61302yf r2, Object obj, Object[] objArr) {
        return null;
    }

    private static int A0D(Object obj) {
        int intValue;
        Map map = A03;
        synchronized (map) {
            if (!map.containsKey(obj)) {
                map.put(obj, Integer.valueOf(A04.incrementAndGet()));
            }
            intValue = ((Integer) map.get(obj)).intValue();
        }
        return intValue;
    }

    public static AnonymousClass10N A0E(Class cls, AnonymousClass0p4 r5, int i, Object[] objArr) {
        C17770zR r0;
        if (r5 == null || (r0 = r5.A04) == null) {
            C09070gU.A01(AnonymousClass07B.A0C, "ComponentContext:NoScopeEventHandler", "Creating event handler without scope.");
            return C151456zm.A00;
        }
        Class<?> cls2 = r0.getClass();
        if (cls != cls2) {
            Integer num = AnonymousClass07B.A01;
            String simpleName = cls2.getSimpleName();
            C09070gU.A01(num, AnonymousClass08S.A0J("ComponentLifecycle:WrongContextForEventHandler:", simpleName), String.format("A Event handler from %s was created using a context from %s. Event Handlers must be created using a ComponentContext from its Component.", cls.getSimpleName(), simpleName));
        }
        AnonymousClass10N A06 = r5.A06(i, objArr);
        ComponentTree componentTree = r5.A05;
        if (componentTree != null) {
            componentTree.A0U.A02(r5.A04.A06, A06);
        }
        return A06;
    }

    public static C61302yf A0F(AnonymousClass0p4 r1, int i, String str) {
        C61302yf r0;
        C17770zR r02 = r1.A04;
        if (r02 == null) {
            return null;
        }
        ComponentTree componentTree = r1.A05;
        String A0A = AnonymousClass08S.A0A(r02.A06, i, str);
        synchronized (componentTree.A0V) {
            AnonymousClass1RE r12 = componentTree.A0V;
            synchronized (r12) {
                Map map = r12.A00;
                if (map == null || !map.containsKey(A0A)) {
                    r0 = null;
                } else {
                    r0 = (C61302yf) r12.A00.get(A0A);
                }
            }
        }
        return r0;
    }

    public static void A0G(AnonymousClass0p4 r2, AnonymousClass5QC r3) {
        C17770zR r0 = r2.A04;
        if (r0 != null) {
            AnonymousClass10N r02 = r0.A04;
            if (r02 != null) {
                r02.A00(r3);
                return;
            }
            return;
        }
        throw new RuntimeException("No component scope found for handler to throw error", r3.A00);
    }

    public static void A0H(AnonymousClass0p4 r1, Exception exc) {
        if (AnonymousClass07c.enableOnErrorHandling) {
            AnonymousClass5QC r0 = new AnonymousClass5QC();
            r0.A00 = exc;
            A0G(r1, r0);
        } else if (exc instanceof RuntimeException) {
            throw ((RuntimeException) exc);
        } else {
            throw new RuntimeException(exc);
        }
    }

    public C17490yz A0Q(AnonymousClass0p4 r5) {
        C17470yx r1;
        AnonymousClass10X r0;
        boolean A002;
        C17470yx r12;
        AnonymousClass10X r02;
        boolean A003;
        if (!(this instanceof C21841Ix)) {
            if (this instanceof C16980y8) {
                C16980y8 r3 = (C16980y8) this;
                C123205rD r03 = null;
                if (r03 != null) {
                    r1 = r03.create(r5);
                } else {
                    r1 = new C22321Kw(r5);
                }
                if (r3.A05) {
                    r0 = AnonymousClass10X.ROW_REVERSE;
                } else {
                    r0 = AnonymousClass10X.ROW;
                }
                C17470yx Aa9 = r1.Aa9(r0);
                C14940uO r04 = r3.A01;
                if (r04 != null) {
                    Aa9.ANx(r04);
                }
                C14940uO r05 = r3.A00;
                if (r05 != null) {
                    Aa9.ANw(r05);
                }
                C14950uP r06 = r3.A02;
                if (r06 != null) {
                    Aa9.BHb(r06);
                }
                AnonymousClass6I0 r07 = r3.A03;
                if (r07 != null) {
                    Aa9.CNU(r07);
                }
                List<C17770zR> list = r3.A04;
                if (list == null) {
                    return Aa9;
                }
                for (C17770zR r2 : list) {
                    if (!r5.A0J()) {
                        C31941kq r08 = r5.A06;
                        if (r08 == null) {
                            A002 = false;
                        } else {
                            A002 = r08.A00();
                        }
                        if (A002) {
                            Aa9.AOU(r2);
                        } else {
                            Aa9.ASH(r2);
                        }
                    }
                }
                return Aa9;
            } else if (!(this instanceof AnonymousClass11D)) {
                return C31951kr.A02(r5, (C17770zR) this, false, false);
            } else {
                AnonymousClass11D r32 = (AnonymousClass11D) this;
                C123205rD r09 = null;
                if (r09 != null) {
                    r12 = r09.create(r5);
                } else {
                    r12 = new C22321Kw(r5);
                }
                if (r32.A05) {
                    r02 = AnonymousClass10X.COLUMN_REVERSE;
                } else {
                    r02 = AnonymousClass10X.COLUMN;
                }
                C17470yx Aa92 = r12.Aa9(r02);
                C14940uO r010 = r32.A01;
                if (r010 != null) {
                    Aa92.ANx(r010);
                }
                C14940uO r011 = r32.A00;
                if (r011 != null) {
                    Aa92.ANw(r011);
                }
                C14950uP r012 = r32.A02;
                if (r012 != null) {
                    Aa92.BHb(r012);
                }
                AnonymousClass6I0 r013 = r32.A03;
                if (r013 != null) {
                    Aa92.CNU(r013);
                }
                List<C17770zR> list2 = r32.A04;
                if (list2 == null) {
                    return Aa92;
                }
                for (C17770zR r22 : list2) {
                    if (!r5.A0J()) {
                        C31941kq r014 = r5.A06;
                        if (r014 == null) {
                            A003 = false;
                        } else {
                            A003 = r014.A00();
                        }
                        if (A003) {
                            Aa92.AOU(r22);
                        } else {
                            Aa92.ASH(r22);
                        }
                    }
                }
                return Aa92;
            }
            return AnonymousClass0p4.A0F;
        }
        C17770zR r13 = ((C21841Ix) this).A00;
        if (r13 == null) {
            return AnonymousClass0p4.A0F;
        }
        return C31951kr.A02(r5, r13, false, false);
    }

    public C15690vh A0R() {
        return new C15720vn(A0M(), true);
    }

    public Integer A0U() {
        if (!(this instanceof AnonymousClass1KX)) {
            if (!(this instanceof AnonymousClass1v9) && !(this instanceof AnonymousClass1NM) && !(this instanceof AnonymousClass1I8)) {
                if (!(this instanceof C31981ku) && !(this instanceof AnonymousClass1KF)) {
                    if (!(this instanceof AnonymousClass1vA) && !(this instanceof AnonymousClass1JU) && !(this instanceof AnonymousClass1vB) && !(this instanceof AnonymousClass1v5) && !(this instanceof AnonymousClass1vC) && !(this instanceof AnonymousClass1vD) && !(this instanceof C37331vE) && !(this instanceof AnonymousClass1JV) && !(this instanceof C17020yC) && !(this instanceof AnonymousClass10I) && !(this instanceof AnonymousClass1JW) && !(this instanceof AnonymousClass1JX)) {
                        if (!(this instanceof C14910uL) && !(this instanceof AnonymousClass10H)) {
                            if (!(this instanceof AnonymousClass1KM) && !(this instanceof C17670zH)) {
                                if (!(this instanceof C15060uf) && !(this instanceof AnonymousClass1JE)) {
                                    return AnonymousClass07B.A00;
                                }
                            }
                        }
                    }
                }
            }
            return AnonymousClass07B.A0C;
        }
        return AnonymousClass07B.A01;
    }

    public Object A0V(Context context) {
        if (this instanceof AnonymousClass1KM) {
            return ((AnonymousClass1KM) this).A02.AWD(context, null);
        }
        boolean A022 = C27041cY.A02();
        if (A022) {
            C27041cY.A01(AnonymousClass08S.A0J("createMountContent:", ((C17770zR) this).A1A()));
        }
        try {
            return A0W(context);
        } finally {
            if (A022) {
                C27041cY.A00();
            }
        }
    }

    public Object A0W(Context context) {
        View view;
        if (!(this instanceof AnonymousClass1KX)) {
            if (this instanceof AnonymousClass1v9) {
                return new PaymentFormEditTextView(context);
            }
            if (this instanceof AnonymousClass1NM) {
                return new C22261Kq(context);
            }
            if (this instanceof AnonymousClass1I8) {
                return new ImageView(context);
            }
            if (this instanceof C31981ku) {
                int i = AnonymousClass1Y3.B7e;
                AnonymousClass0UN r2 = ((C31981ku) this).A00;
                Resources resources = context.getResources();
                ESz eSz = new ESz();
                eSz.A01 = resources.getDimensionPixelSize(2132148239);
                eSz.A00 = resources.getDimensionPixelSize(2132148344);
                eSz.A03 = true;
                C29276ESy A002 = eSz.A00();
                return new C29268ESq(new C29241ERo(), new DHK(context, A002, new DHN((C83273xM) AnonymousClass1XX.A02(1, i, r2))), A002, C64873Dz.A00((BNo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Avn, r2)));
            } else if (!(this instanceof AnonymousClass1KF)) {
                if (this instanceof AnonymousClass1vA) {
                    AnonymousClass16W A012 = C13950sM.A01(context);
                    ((ViewGroup) A012.A00).setClipChildren(false);
                    ((ViewGroup) A012.A00).setClipToPadding(false);
                    A012.A03(-1, -1);
                    view = A012.A00;
                } else if (this instanceof AnonymousClass1JU) {
                    int dimensionPixelSize = context.getResources().getDimensionPixelSize(2132148265);
                    AnonymousClass16W A013 = C13950sM.A01(context);
                    ((ViewGroup) A013.A00).setClipChildren(false);
                    ((ViewGroup) A013.A00).setClipToPadding(false);
                    A013.A03(-1, -1);
                    C28125Dp4 dp4 = new C28125Dp4(new ImageView(context));
                    dp4.A00(-2, -2);
                    View view2 = dp4.A00;
                    view2.setContentDescription(view2.getContext().getText(2131820997));
                    A013.A06(dp4);
                    AnonymousClass16W A014 = C13950sM.A01(context);
                    A014.A03(-1, -1);
                    C28132DpB dpB = new C28132DpB(new View(context));
                    dpB.A01(new FrameLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
                    A014.A06(dpB);
                    A013.A06(A014);
                    C28132DpB dpB2 = new C28132DpB(new View(context));
                    dpB2.A00(-1, -1);
                    A013.A06(dpB2);
                    view = A013.A00;
                } else if (this instanceof AnonymousClass1vB) {
                    AnonymousClass16W A015 = C13950sM.A01(context);
                    ((ViewGroup) A015.A00).setClipChildren(false);
                    ((ViewGroup) A015.A00).setClipToPadding(false);
                    A015.A03(-1, -1);
                    C28132DpB dpB3 = new C28132DpB(new View(context));
                    dpB3.A00(-2, -2);
                    A015.A06(dpB3);
                    view = A015.A00;
                } else if (this instanceof AnonymousClass1v5) {
                    AnonymousClass16W A016 = C13950sM.A01(context);
                    ((ViewGroup) A016.A00).setClipChildren(false);
                    ((ViewGroup) A016.A00).setClipToPadding(false);
                    A016.A03(-1, -1);
                    C28125Dp4 dp42 = new C28125Dp4(new ImageView(context));
                    dp42.A00(-2, -2);
                    View view3 = dp42.A00;
                    view3.setContentDescription(view3.getContext().getText(2131820944));
                    A016.A06(dp42);
                    C28132DpB dpB4 = new C28132DpB(new View(context));
                    dpB4.A00(context.getResources().getDimensionPixelOffset(2132148265), context.getResources().getDimensionPixelOffset(2132148265));
                    A016.A06(dpB4);
                    FbFrameLayout fbFrameLayout = (FbFrameLayout) A016.A00;
                    ((ImageView) fbFrameLayout.getChildAt(0)).setId(2131298008);
                    return fbFrameLayout;
                } else if (this instanceof AnonymousClass1vC) {
                    AnonymousClass16W A017 = C13950sM.A01(context);
                    ((ViewGroup) A017.A00).setClipChildren(false);
                    ((ViewGroup) A017.A00).setClipToPadding(false);
                    view = A017.A00;
                } else if ((this instanceof AnonymousClass1vD) || (this instanceof C37331vE)) {
                    C183912v r1 = new C183912v(new CustomFrameLayout(context));
                    r1.A03(-2, -2);
                    view = r1.A00;
                } else if (this instanceof AnonymousClass1JV) {
                    return new C206439oG(context, true);
                } else {
                    if (this instanceof C17020yC) {
                        RelativeLayout relativeLayout = new RelativeLayout(context);
                        C29819EjC ejC = new C29819EjC(context);
                        ejC.setSingleLine(true);
                        relativeLayout.addView(ejC, new RelativeLayout.LayoutParams(-1, -1));
                        C29825EjI ejI = new C29825EjI(context);
                        ejI.setImageDrawable(AnonymousClass3DO.A01(context.getResources(), 2132344978, null));
                        C29825EjI.A00(ejI);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(C007106r.A00(context, 28.0f), -2);
                        layoutParams.addRule(11);
                        layoutParams.addRule(15);
                        relativeLayout.addView(ejI, layoutParams);
                        return relativeLayout;
                    } else if (this instanceof AnonymousClass10I) {
                        RelativeLayout relativeLayout2 = new RelativeLayout(context);
                        C29818EjB ejB = new C29818EjB(context);
                        ejB.setSingleLine(true);
                        relativeLayout2.addView(ejB, new RelativeLayout.LayoutParams(-1, -1));
                        C29822EjF ejF = new C29822EjF(context);
                        ejF.setImageDrawable(AnonymousClass01R.A03(context, ((AnonymousClass1MT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJa, ((AnonymousClass10I) this).A01)).A03(C29631gj.A0V, AnonymousClass07B.A0N)));
                        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(C007106r.A00(context, 32.0f), -2);
                        layoutParams2.addRule(11);
                        layoutParams2.addRule(15);
                        relativeLayout2.addView(ejF, layoutParams2);
                        return relativeLayout2;
                    } else if (this instanceof AnonymousClass1JW) {
                        C29796Eim eim = new C29796Eim(context, 0);
                        eim.setBackgroundResource(17170445);
                        return eim;
                    } else if (this instanceof AnonymousClass1JX) {
                        RelativeLayout relativeLayout3 = new RelativeLayout(context);
                        C142086je r12 = new C142086je(context);
                        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams3.addRule(13);
                        relativeLayout3.addView(r12, layoutParams3);
                        AnonymousClass5UH r13 = new AnonymousClass5UH(context);
                        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams4.addRule(13);
                        relativeLayout3.addView(r13, layoutParams4);
                        return relativeLayout3;
                    } else if (this instanceof C14910uL) {
                        return new AnonymousClass0d2();
                    } else {
                        if (!(this instanceof AnonymousClass10H)) {
                            if (this instanceof C17670zH) {
                                return new ComponentHost(new AnonymousClass0p4(context), null);
                            }
                            if (!(this instanceof C15060uf)) {
                                if (this instanceof AnonymousClass1JE) {
                                    return new C74093hG(!((AnonymousClass852) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ANV, ((AnonymousClass1JE) this).A09)).A02.A04.A0F());
                                }
                                throw new RuntimeException("Trying to mount a MountSpec that doesn't implement @OnCreateMountContent");
                            }
                        }
                    }
                }
                return (FbFrameLayout) view;
            } else {
                int i2 = AnonymousClass1Y3.B7e;
                AnonymousClass0UN r22 = ((AnonymousClass1KF) this).A00;
                ESz eSz2 = new ESz();
                eSz2.A01 = context.getResources().getDimensionPixelSize(2132148243);
                eSz2.A00 = context.getResources().getDimensionPixelSize(2132148233);
                C29276ESy A003 = eSz2.A00();
                return new C29268ESq(new C29240ERn(), new DHK(context, A003, new DHN((C83273xM) AnonymousClass1XX.A02(1, i2, r22))), A003, C64873Dz.A00((BNo) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Avn, r22)));
            }
        }
        return new AnonymousClass1MR();
    }

    public void A0d(AnonymousClass0p4 r2, int i, int i2) {
        r2.A00 = i;
        r2.A01 = i2;
        A0b(r2);
        r2.A00 = 0;
        r2.A01 = 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0204, code lost:
        if ((r13 - r11) <= r4) goto L_0x0206;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0f(X.AnonymousClass0p4 r52, X.C17490yz r53, int r54, int r55, X.AnonymousClass10L r56) {
        /*
            r51 = this;
            r3 = r51
            r11 = r54
            r10 = r55
            boolean r0 = r3 instanceof X.AnonymousClass1KX
            r12 = r52
            r9 = r56
            if (r0 != 0) goto L_0x0396
            boolean r0 = r3 instanceof X.AnonymousClass1v9
            if (r0 != 0) goto L_0x036b
            boolean r0 = r3 instanceof X.AnonymousClass1NM
            if (r0 != 0) goto L_0x02ee
            boolean r0 = r3 instanceof X.C31981ku
            if (r0 != 0) goto L_0x02d5
            boolean r0 = r3 instanceof X.AnonymousClass1KF
            if (r0 != 0) goto L_0x02b8
            boolean r0 = r3 instanceof X.AnonymousClass1JU
            if (r0 != 0) goto L_0x0389
            boolean r0 = r3 instanceof X.AnonymousClass1vB
            if (r0 != 0) goto L_0x0389
            boolean r0 = r3 instanceof X.AnonymousClass1v5
            if (r0 != 0) goto L_0x0389
            boolean r0 = r3 instanceof X.AnonymousClass1JV
            if (r0 != 0) goto L_0x0289
            boolean r0 = r3 instanceof X.C17020yC
            if (r0 != 0) goto L_0x027a
            boolean r0 = r3 instanceof X.AnonymousClass10I
            if (r0 != 0) goto L_0x027a
            boolean r0 = r3 instanceof X.AnonymousClass1JW
            if (r0 != 0) goto L_0x025c
            boolean r0 = r3 instanceof X.AnonymousClass1JX
            if (r0 != 0) goto L_0x02a7
            boolean r0 = r3 instanceof X.C14910uL
            if (r0 != 0) goto L_0x0118
            boolean r0 = r3 instanceof X.AnonymousClass10H
            if (r0 != 0) goto L_0x00bf
            boolean r0 = r3 instanceof X.AnonymousClass1KM
            if (r0 != 0) goto L_0x006b
            boolean r0 = r3 instanceof X.AnonymousClass1JE
            if (r0 != 0) goto L_0x0062
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "You must override onMeasure() if you return true in canMeasure(), ComponentLifecycle is: "
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x0062:
            r0 = r3
            X.1JE r0 = (X.AnonymousClass1JE) r0
            float r0 = r0.A00
            X.AnonymousClass1NE.A02(r11, r10, r0, r9)
            return
        L_0x006b:
            X.1KM r3 = (X.AnonymousClass1KM) r3
            android.content.Context r1 = r12.A09
            X.0vh r0 = X.C15040ud.A00(r1, r3)
            if (r0 != 0) goto L_0x00ba
            java.lang.Object r2 = r3.A0V(r1)
        L_0x0079:
            android.view.View r2 = (android.view.View) r2
            android.view.ViewGroup$LayoutParams r4 = new android.view.ViewGroup$LayoutParams
            int r1 = r9.A01
            int r0 = r9.A00
            r4.<init>(r1, r0)
            r2.setLayoutParams(r4)
            X.3ph r0 = r3.A01
            r0.APu(r2)
            int r1 = r2.getVisibility()
            r0 = 8
            if (r1 != r0) goto L_0x00aa
            r0 = 0
            r9.A01 = r0
            r9.A00 = r0
        L_0x0099:
            X.3ph r0 = r3.A01
            r0.CJY(r2)
            android.content.Context r0 = r12.A09
            X.0vh r0 = X.C15040ud.A00(r0, r3)
            if (r0 == 0) goto L_0x00a9
            r0.C0t(r2)
        L_0x00a9:
            return
        L_0x00aa:
            r2.measure(r11, r10)
            int r0 = r2.getMeasuredWidth()
            r9.A01 = r0
            int r0 = r2.getMeasuredHeight()
            r9.A00 = r0
            goto L_0x0099
        L_0x00ba:
            java.lang.Object r2 = r0.ALb(r1, r3)
            goto L_0x0079
        L_0x00bf:
            r0 = r3
            X.10H r0 = (X.AnonymousClass10H) r0
            android.graphics.drawable.Drawable r1 = r0.A00
            if (r1 == 0) goto L_0x03e4
            int r0 = r1.getIntrinsicWidth()
            if (r0 <= 0) goto L_0x03e4
            int r0 = r1.getIntrinsicHeight()
            if (r0 <= 0) goto L_0x03e4
            int r4 = r1.getIntrinsicHeight()
            int r3 = r1.getIntrinsicWidth()
            int r0 = android.view.View.MeasureSpec.getMode(r11)
            if (r0 != 0) goto L_0x00eb
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            if (r0 != 0) goto L_0x00eb
            r9.A01 = r3
            r9.A00 = r4
            return
        L_0x00eb:
            float r2 = (float) r3
            float r0 = (float) r4
            float r2 = r2 / r0
            int r0 = android.view.View.MeasureSpec.getMode(r11)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r1) goto L_0x0102
            int r0 = android.view.View.MeasureSpec.getSize(r11)
            if (r0 <= r3) goto L_0x0102
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            int r11 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
        L_0x0102:
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            if (r0 != r1) goto L_0x0114
            int r0 = android.view.View.MeasureSpec.getSize(r10)
            if (r0 <= r4) goto L_0x0114
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            int r10 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r0)
        L_0x0114:
            X.AnonymousClass1NE.A02(r11, r10, r2, r9)
            return
        L_0x0118:
            r8 = r3
            X.0uL r8 = (X.C14910uL) r8
            X.1JP r3 = new X.1JP
            r3.<init>()
            X.1JP r17 = new X.1JP
            r17.<init>()
            X.1JP r15 = new X.1JP
            r15.<init>()
            java.lang.CharSequence r0 = r8.A0c
            r28 = r0
            android.text.TextUtils$TruncateAt r0 = r8.A0U
            r20 = r0
            boolean r0 = r8.A0l
            r21 = r0
            int r7 = r8.A0I
            int r0 = r8.A0F
            r22 = r0
            int r0 = r8.A0H
            r41 = r0
            int r0 = r8.A0E
            r42 = r0
            int r0 = r8.A0J
            r43 = r0
            int r0 = r8.A0G
            r44 = r0
            float r0 = r8.A05
            r23 = r0
            float r0 = r8.A03
            r24 = r0
            float r0 = r8.A04
            r25 = r0
            int r0 = r8.A0L
            r26 = r0
            boolean r0 = r8.A0j
            r27 = r0
            int r0 = r8.A0M
            r29 = r0
            android.content.res.ColorStateList r0 = r8.A0P
            r30 = r0
            int r0 = r8.A0D
            r31 = r0
            int r0 = r8.A0N
            r32 = r0
            float r14 = r8.A01
            float r6 = r8.A06
            r35 = 0
            int r0 = r8.A0O
            r36 = r0
            android.graphics.Typeface r0 = r8.A0Q
            r37 = r0
            android.text.Layout$Alignment r13 = r8.A0R
            X.1JS r0 = r8.A0X
            int r1 = r8.A07
            r46 = r1
            int r1 = r8.A0B
            r47 = r1
            int r1 = r8.A0C
            r48 = r1
            boolean r1 = r8.A0i
            r39 = r1
            X.1Kv r1 = r8.A0V
            r49 = r1
            boolean r5 = r8.A0k
            int r4 = r8.A0K
            float r1 = r8.A02
            r50 = r1
            r1 = r28
            boolean r16 = android.text.TextUtils.isEmpty(r1)
            r2 = 0
            r1 = 0
            if (r16 == 0) goto L_0x01c4
            r3.A00(r2)
            r9.A01 = r1
            r9.A00 = r1
        L_0x01af:
            java.lang.Object r0 = r3.A00
            android.text.Layout r0 = (android.text.Layout) r0
            r8.A0S = r0
            r0 = r17
            java.lang.Object r0 = r0.A00
            java.lang.Integer r0 = (java.lang.Integer) r0
            r8.A0f = r0
            java.lang.Object r0 = r15.A00
            java.lang.Integer r0 = (java.lang.Integer) r0
            r8.A0e = r0
            return
        L_0x01c4:
            X.1JS r38 = X.C14910uL.A04(r13, r0)
            X.0zG r40 = r53.B16()
            android.content.Context r0 = r12.A09
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            float r0 = r0.density
            r33 = r14
            r34 = r6
            r18 = r12
            r19 = r11
            r45 = r0
            android.text.Layout r0 = X.C14910uL.A01(r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50)
            r3.A00(r0)
            int r12 = r0.getWidth()
            int r13 = X.C14910uL.A00(r11, r12)
            if (r5 == 0) goto L_0x0206
            int r12 = r0.getLineCount()
            r5 = 1
            if (r12 <= r5) goto L_0x0206
            int r5 = X.C22561Ly.A01(r0)
            int r11 = X.C14910uL.A00(r11, r5)
            int r5 = r13 - r11
            if (r5 > r4) goto L_0x0207
        L_0x0206:
            r11 = r13
        L_0x0207:
            r9.A01 = r11
            int r5 = X.C22561Ly.A00(r0)
            int r4 = r0.getLineCount()
            if (r4 >= r7) goto L_0x0225
            android.text.TextPaint r0 = r0.getPaint()
            int r0 = r0.getFontMetricsInt(r2)
            float r0 = (float) r0
            float r0 = r0 * r6
            float r0 = r0 + r14
            int r0 = java.lang.Math.round(r0)
            int r7 = r7 - r4
            int r0 = r0 * r7
            int r5 = r5 + r0
        L_0x0225:
            int r2 = X.C14910uL.A00(r10, r5)
            r9.A00 = r2
            int r0 = r9.A01
            if (r0 < 0) goto L_0x0231
            if (r2 >= 0) goto L_0x0246
        L_0x0231:
            int r0 = java.lang.Math.max(r0, r1)
            r9.A01 = r0
            int r0 = java.lang.Math.max(r2, r1)
            r9.A00 = r0
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            java.lang.String r1 = "TextSpec:WrongTextSize"
            java.lang.String r0 = "Text layout measured to less than 0 pixels"
            X.C09070gU.A01(r2, r1, r0)
        L_0x0246:
            int r0 = r9.A01
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            r0 = r17
            r0.A00(r1)
            int r0 = r9.A00
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            r15.A00(r1)
            goto L_0x01af
        L_0x025c:
            X.Eim r2 = new X.Eim
            android.content.Context r1 = r12.A09
            r0 = 0
            r2.<init>(r1, r0)
            int r1 = X.AnonymousClass1NE.A00(r11)
            int r0 = X.AnonymousClass1NE.A00(r10)
            r2.measure(r1, r0)
            int r0 = r2.getMeasuredWidth()
            r9.A01 = r0
            int r0 = r2.getMeasuredHeight()
            goto L_0x02b5
        L_0x027a:
            int r0 = android.view.View.MeasureSpec.getSize(r11)
            r9.A01 = r0
            android.content.Context r1 = r12.A09
            r0 = 1112539136(0x42500000, float:52.0)
            int r0 = X.C007106r.A00(r1, r0)
            goto L_0x02b5
        L_0x0289:
            X.9oG r2 = new X.9oG
            android.content.Context r1 = r12.A09
            r0 = 0
            r2.<init>(r1, r0)
            int r1 = X.AnonymousClass1NE.A00(r11)
            int r0 = X.AnonymousClass1NE.A00(r10)
            r2.measure(r1, r0)
            int r0 = android.view.View.MeasureSpec.getSize(r11)
            r9.A01 = r0
            int r0 = r2.getMeasuredHeight()
            goto L_0x02b5
        L_0x02a7:
            int r0 = android.view.View.MeasureSpec.getSize(r11)
            r9.A01 = r0
            android.content.Context r1 = r12.A09
            r0 = 1109393408(0x42200000, float:40.0)
            int r0 = X.C007106r.A00(r1, r0)
        L_0x02b5:
            r9.A00 = r0
            return
        L_0x02b8:
            android.content.res.Resources r1 = r12.A03()
            r0 = 2132148243(0x7f160013, float:1.9938458E38)
            int r2 = r1.getDimensionPixelSize(r0)
            android.content.res.Resources r1 = r12.A03()
            r0 = 2132148233(0x7f160009, float:1.9938438E38)
            int r0 = r1.getDimensionPixelSize(r0)
            int r0 = r0 << 1
            int r2 = r2 + r0
            X.AnonymousClass1NE.A03(r11, r10, r2, r2, r9)
            return
        L_0x02d5:
            android.content.res.Resources r2 = r12.A03()
            r0 = 2132148239(0x7f16000f, float:1.993845E38)
            int r1 = r2.getDimensionPixelSize(r0)
            r0 = 2132148344(0x7f160078, float:1.9938663E38)
            int r0 = r2.getDimensionPixelSize(r0)
            int r0 = r0 << 1
            int r1 = r1 + r0
            X.AnonymousClass1NE.A03(r11, r10, r1, r1, r9)
            return
        L_0x02ee:
            r5 = r3
            X.1NM r5 = (X.AnonymousClass1NM) r5
            X.1JP r15 = new X.1JP
            r15.<init>()
            X.1JP r14 = new X.1JP
            r14.<init>()
            X.1JP r13 = new X.1JP
            r13.<init>()
            X.1JP r10 = new X.1JP
            r10.<init>()
            X.1JP r8 = new X.1JP
            r8.<init>()
            X.1JP r7 = new X.1JP
            r7.<init>()
            X.1JP r6 = new X.1JP
            r6.<init>()
            X.0zR r4 = r5.A01
            X.0zR r3 = r5.A02
            X.0zR r2 = r5.A03
            android.graphics.drawable.Drawable r1 = r5.A00
            r23 = r15
            r24 = r14
            r25 = r13
            r26 = r10
            r27 = r8
            r28 = r7
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            r6.A00(r0)
            r16 = r12
            r17 = r11
            r18 = r4
            r19 = r1
            r20 = r3
            r21 = r2
            r22 = r9
            X.AnonymousClass1NM.A01(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28)
            java.lang.Object r0 = r15.A00
            com.facebook.litho.ComponentTree r0 = (com.facebook.litho.ComponentTree) r0
            r5.A06 = r0
            java.lang.Object r0 = r14.A00
            com.facebook.litho.ComponentTree r0 = (com.facebook.litho.ComponentTree) r0
            r5.A07 = r0
            java.lang.Object r0 = r13.A00
            java.lang.Integer r0 = (java.lang.Integer) r0
            r5.A0F = r0
            java.lang.Object r0 = r10.A00
            java.lang.Integer r0 = (java.lang.Integer) r0
            r5.A0E = r0
            java.lang.Object r0 = r8.A00
            java.lang.Integer r0 = (java.lang.Integer) r0
            r5.A0G = r0
            java.lang.Object r0 = r7.A00
            java.lang.Integer r0 = (java.lang.Integer) r0
            r5.A0H = r0
            java.lang.Object r0 = r6.A00
            java.lang.Integer r0 = (java.lang.Integer) r0
            r5.A0I = r0
            return
        L_0x036b:
            com.facebook.payments.ui.PaymentFormEditTextView r3 = new com.facebook.payments.ui.PaymentFormEditTextView
            android.content.Context r0 = r12.A09
            r3.<init>(r0)
            int r2 = X.AnonymousClass1NE.A00(r11)
            r1 = 0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r1)
            r3.measure(r2, r0)
            int r0 = r3.getMeasuredHeight()
            r9.A00 = r0
            int r0 = r3.getMeasuredWidth()
            goto L_0x0393
        L_0x0389:
            int r0 = android.view.View.MeasureSpec.getSize(r10)
            r9.A00 = r0
            int r0 = android.view.View.MeasureSpec.getSize(r11)
        L_0x0393:
            r9.A01 = r0
            return
        L_0x0396:
            r0 = r3
            X.1KX r0 = (X.AnonymousClass1KX) r0
            int r2 = r0.A04
            android.content.Context r1 = r12.A09
            if (r2 > 0) goto L_0x03a5
            r0 = 1112014848(0x42480000, float:50.0)
            int r2 = X.C007106r.A00(r1, r0)
        L_0x03a5:
            if (r2 <= 0) goto L_0x03e4
            int r0 = android.view.View.MeasureSpec.getMode(r11)
            if (r0 != 0) goto L_0x03b8
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            if (r0 != 0) goto L_0x03b8
            r9.A01 = r2
            r9.A00 = r2
            return
        L_0x03b8:
            r3 = 1065353216(0x3f800000, float:1.0)
            int r0 = android.view.View.MeasureSpec.getMode(r11)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r0 != r1) goto L_0x03ce
            int r0 = android.view.View.MeasureSpec.getSize(r11)
            if (r0 <= r2) goto L_0x03ce
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            int r11 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r0)
        L_0x03ce:
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            if (r0 != r1) goto L_0x03e0
            int r0 = android.view.View.MeasureSpec.getSize(r10)
            if (r0 <= r2) goto L_0x03e0
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            int r10 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r0)
        L_0x03e0:
            X.AnonymousClass1NE.A02(r11, r10, r3, r9)
            return
        L_0x03e4:
            r0 = 0
            r9.A01 = r0
            r9.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A0f(X.0p4, X.0yz, int, int, X.10L):void");
    }

    public void A0g(AnonymousClass0p4 r4, Object obj) {
        if (!(this instanceof AnonymousClass1KM)) {
            r4.A08 = "bind";
            boolean A022 = C27041cY.A02();
            if (A022) {
                C27041cY.A01(AnonymousClass08S.A0J("onBind:", ((C17770zR) this).A1A()));
            }
            try {
                A0i(r4, obj);
                r4.A08 = null;
            } finally {
                if (A022) {
                    C27041cY.A00();
                }
            }
        } else {
            ((AnonymousClass1KM) this).A01.APu((View) obj);
        }
    }

    public void A0h(AnonymousClass0p4 r4, Object obj) {
        r4.A08 = "mount";
        boolean A022 = C27041cY.A02();
        if (A022) {
            C27041cY.A01(AnonymousClass08S.A0J("onMount:", ((C17770zR) this).A1A()));
        }
        try {
            A0j(r4, obj);
        } catch (Exception e) {
            r4.A08 = null;
            A0H(r4, e);
        } catch (Throwable th) {
            if (A022) {
                C27041cY.A00();
            }
            throw th;
        }
        if (A022) {
            C27041cY.A00();
        }
        r4.A08 = null;
    }

    public void A0m(AnonymousClass0p4 r2, Object obj) {
        if (!(this instanceof AnonymousClass1KM)) {
            A0k(r2, obj);
        } else {
            ((AnonymousClass1KM) this).A01.CJY((View) obj);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:94:0x016e A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A12(X.C17770zR r10, X.C17770zR r11) {
        /*
            r9 = this;
            boolean r0 = r9 instanceof X.AnonymousClass1I8
            if (r0 != 0) goto L_0x00d1
            boolean r0 = r9 instanceof X.AnonymousClass10H
            if (r0 != 0) goto L_0x013d
            boolean r0 = r9 instanceof X.C17670zH
            if (r0 != 0) goto L_0x00cf
            boolean r0 = r9 instanceof X.C15060uf
            if (r0 != 0) goto L_0x00c0
            boolean r0 = r9 instanceof X.AnonymousClass1JE
            if (r0 != 0) goto L_0x001b
            r1 = 1
            boolean r0 = X.C17770zR.A0A(r10, r11, r1)
            r0 = r0 ^ r1
            return r0
        L_0x001b:
            r8 = r9
            X.1JE r8 = (X.AnonymousClass1JE) r8
            X.1JE r10 = (X.AnonymousClass1JE) r10
            X.1JE r11 = (X.AnonymousClass1JE) r11
            X.1mx r7 = new X.1mx
            r4 = 0
            if (r10 != 0) goto L_0x00bc
            r1 = r4
        L_0x0028:
            if (r11 != 0) goto L_0x00b8
            r0 = r4
        L_0x002b:
            r7.<init>(r1, r0)
            X.1mx r6 = new X.1mx
            r1 = 0
            r6.<init>(r4, r4)
            X.1mx r5 = new X.1mx
            if (r10 == 0) goto L_0x003a
            X.3JC r1 = r10.A08
        L_0x003a:
            if (r11 != 0) goto L_0x00b5
            r0 = r4
        L_0x003d:
            r5.<init>(r1, r0)
            X.1mx r2 = new X.1mx
            if (r10 != 0) goto L_0x00b2
            r1 = r4
        L_0x0045:
            if (r11 != 0) goto L_0x00af
            r0 = r4
        L_0x0048:
            r2.<init>(r1, r0)
            X.1mx r3 = new X.1mx
            if (r10 != 0) goto L_0x00a8
            r1 = r4
        L_0x0050:
            if (r11 == 0) goto L_0x0058
            float r0 = r11.A00
            java.lang.Float r4 = java.lang.Float.valueOf(r0)
        L_0x0058:
            r3.<init>(r1, r4)
            r4 = 0
            int r1 = X.AnonymousClass1Y3.ANV
            X.0UN r0 = r8.A09
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.852 r1 = (X.AnonymousClass852) r1
            r0 = 0
            X.DdQ r0 = r1.A00(r0, r0)
            X.DdS r0 = r0.A04
            boolean r0 = r0.A0C()
            r4 = 1
            if (r0 == 0) goto L_0x00a7
            java.lang.Object r1 = r7.A01
            java.lang.Object r0 = r7.A00
            boolean r0 = X.C1756388o.A00(r1, r0)
            if (r0 == 0) goto L_0x00a7
            java.lang.Object r1 = r6.A01
            java.lang.Object r0 = r6.A00
            boolean r0 = X.C1756388o.A00(r1, r0)
            if (r0 == 0) goto L_0x00a7
            java.lang.Object r1 = r5.A01
            java.lang.Object r0 = r5.A00
            boolean r0 = X.C1756388o.A00(r1, r0)
            if (r0 == 0) goto L_0x00a7
            java.lang.Object r1 = r3.A01
            java.lang.Object r0 = r3.A00
            boolean r0 = X.C1756388o.A00(r1, r0)
            if (r0 == 0) goto L_0x00a7
            java.lang.Object r1 = r2.A01
            java.lang.Object r0 = r2.A00
            boolean r0 = X.C1756388o.A00(r1, r0)
            if (r0 == 0) goto L_0x00a7
            r4 = 0
        L_0x00a7:
            return r4
        L_0x00a8:
            float r0 = r10.A00
            java.lang.Float r1 = java.lang.Float.valueOf(r0)
            goto L_0x0050
        L_0x00af:
            X.DdQ r0 = r11.A04
            goto L_0x0048
        L_0x00b2:
            X.DdQ r1 = r10.A04
            goto L_0x0045
        L_0x00b5:
            X.3JC r0 = r11.A08
            goto L_0x003d
        L_0x00b8:
            android.net.Uri r0 = r11.A01
            goto L_0x002b
        L_0x00bc:
            android.net.Uri r1 = r10.A01
            goto L_0x0028
        L_0x00c0:
            X.0uf r10 = (X.C15060uf) r10
            android.graphics.drawable.Drawable r1 = r10.A02
            X.0uf r11 = (X.C15060uf) r11
            android.graphics.drawable.Drawable r0 = r11.A02
            boolean r0 = X.C49982dB.A00(r1, r0)
            r0 = r0 ^ 1
            return r0
        L_0x00cf:
            r0 = 1
            return r0
        L_0x00d1:
            X.1I8 r10 = (X.AnonymousClass1I8) r10
            X.1I8 r11 = (X.AnonymousClass1I8) r11
            X.1mx r4 = new X.1mx
            r5 = 0
            if (r10 != 0) goto L_0x0136
            r1 = r5
        L_0x00db:
            if (r11 != 0) goto L_0x012f
            r0 = r5
        L_0x00de:
            r4.<init>(r1, r0)
            X.1mx r3 = new X.1mx
            if (r10 != 0) goto L_0x0128
            r1 = r5
        L_0x00e6:
            if (r11 != 0) goto L_0x0121
            r0 = r5
        L_0x00e9:
            r3.<init>(r1, r0)
            X.1mx r2 = new X.1mx
            if (r10 != 0) goto L_0x011e
            r0 = r5
        L_0x00f1:
            if (r11 == 0) goto L_0x00f5
            java.lang.Integer r5 = r11.A05
        L_0x00f5:
            r2.<init>(r0, r5)
            java.lang.Object r1 = r4.A00
            java.lang.Object r0 = r4.A01
            boolean r0 = r1.equals(r0)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x016e
            java.lang.Object r1 = r3.A00
            java.lang.Object r0 = r3.A01
            boolean r0 = r1.equals(r0)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x016e
            java.lang.Object r1 = r2.A00
            java.lang.Object r0 = r2.A01
            boolean r0 = r1.equals(r0)
            r1 = r0 ^ 1
            r0 = 0
            if (r1 == 0) goto L_0x016f
            goto L_0x016e
        L_0x011e:
            java.lang.Integer r0 = r10.A05
            goto L_0x00f1
        L_0x0121:
            int r0 = r11.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x00e9
        L_0x0128:
            int r0 = r10.A01
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            goto L_0x00e6
        L_0x012f:
            int r0 = r11.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x00de
        L_0x0136:
            int r0 = r10.A00
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            goto L_0x00db
        L_0x013d:
            X.10H r10 = (X.AnonymousClass10H) r10
            X.10H r11 = (X.AnonymousClass10H) r11
            X.1mx r4 = new X.1mx
            r3 = 0
            if (r10 != 0) goto L_0x0176
            r1 = r3
        L_0x0147:
            if (r11 != 0) goto L_0x0173
            r0 = r3
        L_0x014a:
            r4.<init>(r1, r0)
            X.1mx r2 = new X.1mx
            if (r10 != 0) goto L_0x0170
            r0 = r3
        L_0x0152:
            if (r11 == 0) goto L_0x0156
            android.graphics.drawable.Drawable r3 = r11.A00
        L_0x0156:
            r2.<init>(r0, r3)
            java.lang.Object r1 = r4.A00
            java.lang.Object r0 = r4.A01
            if (r1 != r0) goto L_0x016e
            java.lang.Object r1 = r2.A00
            android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
            java.lang.Object r0 = r2.A01
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            boolean r1 = X.C49982dB.A00(r1, r0)
            r0 = 0
            if (r1 != 0) goto L_0x016f
        L_0x016e:
            r0 = 1
        L_0x016f:
            return r0
        L_0x0170:
            android.graphics.drawable.Drawable r0 = r10.A00
            goto L_0x0152
        L_0x0173:
            android.widget.ImageView$ScaleType r0 = r11.A01
            goto L_0x014a
        L_0x0176:
            android.widget.ImageView$ScaleType r1 = r10.A01
            goto L_0x0147
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A12(X.0zR, X.0zR):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x0631  */
    /* JADX WARNING: Removed duplicated region for block: B:509:0x0c5f A[PHI: r1 r4 
      PHI: (r1v56 char) = (r1v57 char), (r1v99 char), (r1v106 char), (r1v176 char), (r1v257 char), (r1v265 char), (r1v270 char) binds: [B:474:0x0bab, B:361:0x08b9, B:336:0x0811, B:214:0x0402, B:83:0x00cd, B:480:0x0be1, B:74:0x00a5] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r4v13 java.lang.Object) = (r4v15 java.lang.Object), (r4v27 java.lang.Object), (r4v28 java.lang.Object), (r4v53 java.lang.Object), (r4v68 java.lang.Object), (r4v69 java.lang.Object), (r4v70 java.lang.Object) binds: [B:474:0x0bab, B:361:0x08b9, B:336:0x0811, B:214:0x0402, B:83:0x00cd, B:480:0x0be1, B:74:0x00a5] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:710:0x116f A[PHI: r1 r3 
      PHI: (r1v0 char) = (r1v1 char), (r1v60 char), (r1v92 char), (r1v155 char), (r1v199 char), (r1v204 char) binds: [B:662:0x0fbc, B:461:0x0b71, B:389:0x0979, B:259:0x0533, B:696:0x1126, B:203:0x03cb] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r3v0 java.lang.Object) = (r3v2 java.lang.Object), (r3v17 java.lang.Object), (r3v32 java.lang.Object), (r3v56 java.lang.Object), (r3v70 java.lang.Object), (r3v71 java.lang.Object) binds: [B:662:0x0fbc, B:461:0x0b71, B:389:0x0979, B:259:0x0533, B:696:0x1126, B:203:0x03cb] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object AXa(X.AnonymousClass10N r14, java.lang.Object r15) {
        /*
            r13 = this;
            boolean r0 = r13 instanceof X.AnonymousClass1vG
            if (r0 != 0) goto L_0x0fb8
            boolean r0 = r13 instanceof X.C21611Ia
            if (r0 != 0) goto L_0x0f3e
            boolean r0 = r13 instanceof X.AnonymousClass1vK
            if (r0 != 0) goto L_0x0e1d
            boolean r0 = r13 instanceof X.AnonymousClass1NM
            if (r0 != 0) goto L_0x0de5
            boolean r0 = r13 instanceof X.AnonymousClass1JF
            if (r0 != 0) goto L_0x0c87
            boolean r0 = r13 instanceof X.AnonymousClass1IZ
            if (r0 != 0) goto L_0x0f66
            boolean r0 = r13 instanceof X.AnonymousClass1J6
            if (r0 != 0) goto L_0x0c4b
            boolean r0 = r13 instanceof X.AnonymousClass1J1
            if (r0 != 0) goto L_0x0ba1
            boolean r0 = r13 instanceof X.AnonymousClass1GN
            if (r0 != 0) goto L_0x0b6d
            boolean r0 = r13 instanceof X.C37301v3
            if (r0 != 0) goto L_0x0ad8
            boolean r0 = r13 instanceof X.C32101l9
            if (r0 != 0) goto L_0x0a6a
            boolean r0 = r13 instanceof X.AnonymousClass1K6
            if (r0 != 0) goto L_0x09c0
            boolean r0 = r13 instanceof X.C181010e
            if (r0 != 0) goto L_0x0969
            boolean r0 = r13 instanceof X.AnonymousClass1vP
            if (r0 != 0) goto L_0x08aa
            boolean r0 = r13 instanceof X.C31971kt
            if (r0 != 0) goto L_0x080a
            boolean r0 = r13 instanceof X.AnonymousClass1vB
            if (r0 != 0) goto L_0x07f1
            boolean r0 = r13 instanceof X.C37311v4
            if (r0 != 0) goto L_0x05d3
            boolean r0 = r13 instanceof X.AnonymousClass1v5
            if (r0 != 0) goto L_0x055a
            boolean r0 = r13 instanceof X.AnonymousClass1v6
            if (r0 != 0) goto L_0x0529
            boolean r0 = r13 instanceof X.AnonymousClass1vD
            if (r0 != 0) goto L_0x04f2
            boolean r0 = r13 instanceof X.AnonymousClass1GJ
            if (r0 != 0) goto L_0x04bb
            boolean r0 = r13 instanceof X.AnonymousClass1JJ
            if (r0 != 0) goto L_0x0436
            boolean r0 = r13 instanceof X.C21961Jj
            if (r0 != 0) goto L_0x03fb
            boolean r0 = r13 instanceof X.AnonymousClass1JK
            if (r0 != 0) goto L_0x0cc8
            boolean r0 = r13 instanceof X.C21971Jk
            if (r0 != 0) goto L_0x0e43
            boolean r0 = r13 instanceof X.C22241Km
            if (r0 != 0) goto L_0x1122
            boolean r0 = r13 instanceof X.AnonymousClass1QG
            if (r0 != 0) goto L_0x03c7
            boolean r0 = r13 instanceof X.AnonymousClass11H
            if (r0 != 0) goto L_0x028a
            boolean r0 = r13 instanceof X.C37321v7
            if (r0 != 0) goto L_0x01fb
            boolean r0 = r13 instanceof X.AnonymousClass1vF
            if (r0 != 0) goto L_0x01c7
            boolean r0 = r13 instanceof X.AnonymousClass1v8
            if (r0 != 0) goto L_0x0160
            boolean r0 = r13 instanceof X.AnonymousClass1JN
            if (r0 != 0) goto L_0x00c9
            boolean r0 = r13 instanceof X.AnonymousClass1JO
            if (r0 != 0) goto L_0x0bdd
            boolean r0 = r13 instanceof X.C22291Kt
            if (r0 != 0) goto L_0x009e
            boolean r0 = X.AnonymousClass07c.enableOnErrorHandling
            if (r0 == 0) goto L_0x009c
            int r1 = r14.A01
            int r0 = X.C17780zS.A02
            if (r1 != r0) goto L_0x009c
            r0 = r13
            X.0zR r0 = (X.C17770zR) r0
            X.10N r0 = r0.A04
            X.5QC r15 = (X.AnonymousClass5QC) r15
            r0.A00(r15)
        L_0x009c:
            r0 = 0
            return r0
        L_0x009e:
            int r2 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r1 = 0
            r4 = 0
            if (r2 == r0) goto L_0x0c5f
            r0 = 466811311(0x1bd2f9af, float:3.490294E-22)
            if (r2 != r0) goto L_0x0be4
            X.5E3 r15 = (X.AnonymousClass5E3) r15
            X.0zV r1 = r14.A00
            X.0uv r3 = r15.A01
            android.view.View r0 = r15.A00
            androidx.core.view.accessibility.AccessibilityNodeInfoCompat r2 = r15.A02
            X.1Kt r1 = (X.C22291Kt) r1
            java.lang.String r1 = r1.A03
            r3.A0I(r0, r2)
            if (r1 == 0) goto L_0x00c4
            android.view.accessibility.AccessibilityNodeInfo r0 = r2.A02
            r0.setContentDescription(r1)
        L_0x00c4:
            r0 = 1
            r2.A0S(r0)
            return r4
        L_0x00c9:
            int r0 = r14.A01
            r1 = 0
            r4 = 0
            switch(r0) {
                case -1048037474: goto L_0x0c5f;
                case -936136569: goto L_0x0140;
                case -476912923: goto L_0x0106;
                case 57598607: goto L_0x00f8;
                case 96515278: goto L_0x00d1;
                default: goto L_0x00d0;
            }
        L_0x00d0:
            return r4
        L_0x00d1:
            X.5u7 r15 = (X.C124845u7) r15
            X.0zV r0 = r14.A00
            int r3 = r15.A00
            X.1JN r0 = (X.AnonymousClass1JN) r0
            X.9jI r2 = r0.A03
            X.9ji r1 = r0.A01
            if (r1 == 0) goto L_0x0c49
            r0 = 6
            if (r3 != r0) goto L_0x0c49
            X.9gP r0 = r2.A00
            java.lang.String r0 = r0.A00
            java.lang.String r0 = r0.trim()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0c49
            X.9iL r0 = r1.A00
            X.C203279iL.A00(r0)
            r0 = 1
            goto L_0x0c44
        L_0x00f8:
            X.0zV r0 = r14.A00
            X.1JN r0 = (X.AnonymousClass1JN) r0
            X.9ji r0 = r0.A01
            if (r0 == 0) goto L_0x0be4
            X.9iL r0 = r0.A00
            X.C203279iL.A00(r0)
            return r4
        L_0x0106:
            X.EjJ r15 = (X.C29826EjJ) r15
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r1]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            java.lang.String r1 = r15.A00
            X.1JN r2 = (X.AnonymousClass1JN) r2
            X.9ji r0 = r2.A01
            if (r0 == 0) goto L_0x012e
            java.lang.String r2 = r1.trim()
            X.9iL r1 = r0.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecAccountSearch r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecAccountSearch) r0
            r0.A06 = r2
            boolean r0 = r1.A03
            if (r0 == 0) goto L_0x012e
            r0 = 0
            r1.A03 = r0
            r1.A2c()
        L_0x012e:
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0be4
            X.2yh r2 = new X.2yh
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:AccountLoginRecSearchRootComponent.increaseUiVersion"
            r3.A0G(r2, r0)
            return r4
        L_0x0140:
            X.0zV r0 = r14.A00
            X.1JN r0 = (X.AnonymousClass1JN) r0
            X.9ji r0 = r0.A01
            if (r0 == 0) goto L_0x0be4
            X.9iL r0 = r0.A00
            com.facebook.litho.LithoView r1 = r0.A00
            if (r1 == 0) goto L_0x0be4
            java.lang.String r0 = "contact_point_field_tag"
            android.view.View r1 = X.C124565tf.A00(r1, r0)
            android.widget.EditText r1 = (android.widget.EditText) r1
            if (r1 == 0) goto L_0x0be4
            r1.requestFocus()
            r0 = 0
            X.AnonymousClass8VS.A02(r1, r0)
            return r4
        L_0x0160:
            r5 = r13
            X.1v8 r5 = (X.AnonymousClass1v8) r5
            int r0 = r14.A01
            r2 = 0
            r4 = 0
            switch(r0) {
                case -1972293012: goto L_0x016b;
                case -1048037474: goto L_0x0c5a;
                case -670330384: goto L_0x018a;
                case -228443111: goto L_0x019a;
                default: goto L_0x016a;
            }
        L_0x016a:
            return r4
        L_0x016b:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r2]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            X.1v8 r1 = (X.AnonymousClass1v8) r1
            X.5CH r2 = new X.5CH
            r2.<init>()
            X.5CH r0 = r1.A02
            r5.A0n(r0, r2)
            r3.A0H(r2)
            X.9jh r1 = r1.A01
            int r0 = r2.selectedSecurityType
            r2 = 2
            if (r0 != r2) goto L_0x01b9
            return r4
        L_0x018a:
            X.0zV r0 = r14.A00
            X.1v8 r0 = (X.AnonymousClass1v8) r0
            X.9jh r0 = r0.A01
            if (r0 == 0) goto L_0x0be4
            X.9ik r1 = r0.A00
            X.9iE r0 = X.C203229iE.A0C
            r1.A2X(r0)
            return r4
        L_0x019a:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r2]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            X.1v8 r1 = (X.AnonymousClass1v8) r1
            X.5CH r2 = new X.5CH
            r2.<init>()
            X.5CH r0 = r1.A02
            r5.A0n(r0, r2)
            r3.A0H(r2)
            X.9jh r1 = r1.A01
            int r0 = r2.selectedSecurityType
            r2 = 1
            if (r0 != r2) goto L_0x01b9
            return r4
        L_0x01b9:
            if (r1 == 0) goto L_0x01c3
            X.9ik r0 = r1.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r1 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecSecurity r1 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecSecurity) r1
            r1.A01 = r2
        L_0x01c3:
            X.AnonymousClass1v8.A01(r3, r2)
            return r4
        L_0x01c7:
            int r2 = r14.A01
            r0 = -1880424122(0xffffffff8feb0146, float:-2.317328E-29)
            r1 = 0
            r5 = 0
            if (r2 == r0) goto L_0x01d6
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r2 == r0) goto L_0x0ecb
            return r5
        L_0x01d6:
            X.0zV r0 = r14.A00
            X.1vF r0 = (X.AnonymousClass1vF) r0
            X.9ZP r1 = r0.A01
            if (r1 == 0) goto L_0x0e7a
            X.0Dj r0 = X.C02200Dj.A00()
            X.15w r4 = r0.A0J()
            int r3 = X.AnonymousClass1Y3.B2o
            X.9Yp r2 = r1.A00
            X.0UN r1 = r2.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r3, r1)
            android.content.Intent r1 = (android.content.Intent) r1
            android.content.Context r0 = r2.A1j()
            r4.A0B(r1, r0)
            return r5
        L_0x01fb:
            r5 = r13
            X.1v7 r5 = (X.C37321v7) r5
            int r0 = r14.A01
            r3 = 0
            r4 = 0
            switch(r0) {
                case -1048037474: goto L_0x0206;
                case -670330384: goto L_0x020c;
                case 1089119481: goto L_0x021c;
                case 1469135926: goto L_0x0248;
                default: goto L_0x0205;
            }
        L_0x0205:
            return r4
        L_0x0206:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r3]
            goto L_0x0c63
        L_0x020c:
            X.0zV r0 = r14.A00
            X.1v7 r0 = (X.C37321v7) r0
            X.9hi r0 = r0.A03
            if (r0 == 0) goto L_0x0be4
            X.9hh r1 = r0.A00
            java.lang.String r0 = "action_recovery_send_code"
            r1.A2g(r0)
            return r4
        L_0x021c:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r3]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            X.1v7 r1 = (X.C37321v7) r1
            X.5CI r2 = new X.5CI
            r2.<init>()
            X.5CI r0 = r1.A04
            r5.A0n(r0, r2)
            r3.A0H(r2)
            X.9hi r1 = r1.A03
            int r0 = r2.selectedRecoveryType
            r2 = 2
            if (r0 == r2) goto L_0x0be4
            if (r1 == 0) goto L_0x0244
            X.9hh r0 = r1.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r1 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection r1 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection) r1
            r1.A00 = r2
        L_0x0244:
            X.C37321v7.A01(r3, r2)
            return r4
        L_0x0248:
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r3]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            X.1v7 r2 = (X.C37321v7) r2
            X.5CI r1 = new X.5CI
            r1.<init>()
            X.5CI r0 = r2.A04
            r5.A0n(r0, r1)
            r3.A0H(r1)
            X.9hi r2 = r2.A03
            int r1 = r1.selectedRecoveryType
            r0 = 1
            if (r1 == r0) goto L_0x0be4
            if (r2 == 0) goto L_0x0271
            X.9hh r0 = r2.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r1 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection r1 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection) r1
            r0 = 1
            r1.A00 = r0
        L_0x0271:
            r1 = 1
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0be4
            X.2yh r2 = new X.2yh
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:AccountLoginRecSelectMethodRootComponent.setSelectedRecoveryType"
            r3.A0G(r2, r0)
            return r4
        L_0x028a:
            int r0 = r14.A01
            r4 = 0
            r6 = 0
            switch(r0) {
                case -1048037474: goto L_0x0db6;
                case -952092468: goto L_0x0292;
                case -855949748: goto L_0x02b4;
                case 96515278: goto L_0x0d73;
                case 1196116736: goto L_0x02c0;
                case 1415173789: goto L_0x02e9;
                case 1782726174: goto L_0x02fe;
                case 1874929519: goto L_0x039c;
                default: goto L_0x0291;
            }
        L_0x0291:
            return r6
        L_0x0292:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r4 = r0[r4]
            X.0p4 r4 = (X.AnonymousClass0p4) r4
            X.11H r1 = (X.AnonymousClass11H) r1
            X.9j9 r3 = r1.A05
            X.9id r2 = r1.A03
            boolean r0 = r3.A00
            r1 = r0 ^ 1
            r3.A00 = r1
            if (r2 == 0) goto L_0x02b0
            X.9iJ r0 = r2.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueCredentials r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueCredentials) r0
            r0.A0A = r1
        L_0x02b0:
            X.AnonymousClass11H.A0K(r4)
            return r6
        L_0x02b4:
            X.0zV r0 = r14.A00
            X.11H r0 = (X.AnonymousClass11H) r0
            X.9id r0 = r0.A03
            if (r0 == 0) goto L_0x0ccf
            r0.A00()
            return r6
        L_0x02c0:
            X.EjK r15 = (X.C29827EjK) r15
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r4]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            java.lang.String r1 = r15.A00
            X.11H r2 = (X.AnonymousClass11H) r2
            X.9id r0 = r2.A03
            if (r0 == 0) goto L_0x03c3
            java.lang.String r2 = r1.trim()
            X.9iJ r1 = r0.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueCredentials r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueCredentials) r0
            r0.A05 = r2
            boolean r0 = r1.A0C
            if (r0 == 0) goto L_0x03c3
            r1.A0C = r4
            r1.A2c()
            goto L_0x03c3
        L_0x02e9:
            X.0zV r0 = r14.A00
            X.11H r0 = (X.AnonymousClass11H) r0
            X.9id r1 = r0.A03
            if (r1 == 0) goto L_0x0ccf
            X.9iJ r0 = r1.A00
            r0.A2W()
            X.9iJ r1 = r1.A00
            X.9iE r0 = X.C203229iE.A0E
            r1.A2X(r0)
            return r6
        L_0x02fe:
            X.0zV r0 = r14.A00
            X.11H r0 = (X.AnonymousClass11H) r0
            X.9id r5 = r0.A03
            if (r5 == 0) goto L_0x0ccf
            X.9iJ r0 = r5.A00
            android.content.Context r0 = r0.A1j()
            if (r0 == 0) goto L_0x032c
            r3 = 9
            int r2 = X.AnonymousClass1Y3.BOz
            X.9iJ r1 = r5.A00
            X.0UN r0 = r1.A01
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.6Qf r3 = (X.C134336Qf) r3
            r1.A1j()
            java.lang.Integer r2 = X.AnonymousClass07B.A01
            X.6Qj r1 = X.C134376Qj.A0C
            r0 = 169(0xa9, float:2.37E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r3.A03(r2, r1, r0)
        L_0x032c:
            r2 = 6
            int r1 = X.AnonymousClass1Y3.ALY
            X.9iJ r0 = r5.A00
            X.0UN r0 = r0.A01
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9fm r1 = (X.C202119fm) r1
            java.lang.Integer r0 = X.AnonymousClass07B.A0D
            r1.A02(r0)
            java.lang.Integer r3 = X.AnonymousClass07B.A00
            r2 = 3
            int r1 = X.AnonymousClass1Y3.BRH
            X.9iJ r0 = r5.A00
            X.0UN r0 = r0.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.55A r0 = (X.AnonymousClass55A) r0
            r0.A02(r3)
            X.9iJ r0 = r5.A00
            r0.A2W()
            X.9iJ r0 = r5.A00
            android.content.Context r4 = r0.A1j()
            if (r4 == 0) goto L_0x0ccf
            int r2 = X.AnonymousClass1Y3.BL9
            X.9iJ r0 = r5.A00
            X.0UN r1 = r0.A01
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2Wk r0 = (X.AnonymousClass2Wk) r0
            X.1YI r2 = r0.A02
            r1 = 107(0x6b, float:1.5E-43)
            r0 = 0
            boolean r0 = r2.AbO(r1, r0)
            if (r0 == 0) goto L_0x0388
            r2 = 4
            int r1 = X.AnonymousClass1Y3.ABl
            X.9iJ r0 = r5.A00
            X.0UN r0 = r0.A01
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.91U r2 = (X.AnonymousClass91U) r2
            r1 = 201(0xc9, float:2.82E-43)
            r2.A02(r4, r1, r3)
            return r6
        L_0x0388:
            r3 = 2
            int r2 = X.AnonymousClass1Y3.ABl
            X.9iJ r0 = r5.A00
            X.0UN r1 = r0.A01
            r0 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.91U r2 = (X.AnonymousClass91U) r2
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r2.A03(r4, r3, r1, r6)
            return r6
        L_0x039c:
            X.EjJ r15 = (X.C29826EjJ) r15
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r4]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            java.lang.String r1 = r15.A00
            X.11H r2 = (X.AnonymousClass11H) r2
            X.9id r0 = r2.A03
            if (r0 == 0) goto L_0x03c3
            java.lang.String r2 = r1.trim()
            X.9iJ r1 = r0.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueCredentials r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueCredentials) r0
            r0.A03 = r2
            boolean r0 = r1.A0B
            if (r0 == 0) goto L_0x03c3
            r1.A0B = r4
            r1.A2c()
        L_0x03c3:
            X.AnonymousClass11H.A0K(r3)
            return r6
        L_0x03c7:
            int r0 = r14.A01
            r1 = 0
            r3 = 0
            switch(r0) {
                case -1048037474: goto L_0x116f;
                case -417937898: goto L_0x03eb;
                case 677066169: goto L_0x03cf;
                case 1998236502: goto L_0x03eb;
                default: goto L_0x03ce;
            }
        L_0x03ce:
            return r3
        L_0x03cf:
            X.Eip r15 = (X.C29799Eip) r15
            X.0zV r0 = r14.A00
            java.lang.String r2 = r15.A00
            X.1QG r0 = (X.AnonymousClass1QG) r0
            X.9jZ r1 = r0.A05
            X.9jj r0 = r0.A04
            r1.A00 = r2
            if (r0 == 0) goto L_0x1129
            X.9iN r1 = r0.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecPin r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecPin) r0
            r0.A05 = r2
            X.C203299iN.A00(r1)
            return r3
        L_0x03eb:
            X.0zV r0 = r14.A00
            X.1QG r0 = (X.AnonymousClass1QG) r0
            X.9jj r0 = r0.A04
            if (r0 == 0) goto L_0x1129
            X.9iN r1 = r0.A00
            java.lang.String r0 = "action_recovery_resend_code"
            r1.A2g(r0)
            return r3
        L_0x03fb:
            int r2 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r1 = 0
            r4 = 0
            if (r2 == r0) goto L_0x0c5f
            r0 = 677066169(0x285b35b9, float:1.2168591E-14)
            if (r2 != r0) goto L_0x0be4
            X.Eip r15 = (X.C29799Eip) r15
            X.0zV r1 = r14.A00
            java.lang.String r3 = r15.A00
            X.1Jj r1 = (X.C21961Jj) r1
            X.9jb r0 = r1.A02
            X.9jf r2 = r1.A01
            r0.A00 = r3
            if (r2 == 0) goto L_0x0be4
            X.9iM r1 = r2.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueTwoFacAuth r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueTwoFacAuth) r0
            r0.A01 = r3
            X.C203289iM.A00(r1)
            X.9iM r0 = r2.A00
            X.9io r0 = r0.A07
            X.14b r2 = r0.A00
            X.0gA r1 = X.C08870g7.A2b
            r0 = 522(0x20a, float:7.31E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r2.AOI(r1, r0)
            return r4
        L_0x0436:
            int r0 = r14.A01
            r2 = 0
            r5 = 0
            switch(r0) {
                case -1879460902: goto L_0x04b1;
                case -1048037474: goto L_0x04a5;
                case -952092468: goto L_0x0498;
                case -728244833: goto L_0x047a;
                case -356643487: goto L_0x045c;
                case -309973846: goto L_0x043e;
                case -272271848: goto L_0x043e;
                case 96515278: goto L_0x0a12;
                default: goto L_0x043d;
            }
        L_0x043d:
            return r5
        L_0x043e:
            X.EjK r15 = (X.C29827EjK) r15
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r4 = r0[r2]
            X.0p4 r4 = (X.AnonymousClass0p4) r4
            java.lang.String r3 = r15.A00
            X.1JJ r1 = (X.AnonymousClass1JJ) r1
            X.A0T r2 = r1.A00
            X.5EU r0 = r1.A01
            boolean r1 = r0.saveButtonEnabled
            java.lang.String r0 = r3.trim()
            r2.A02 = r0
            X.AnonymousClass1JJ.A01(r4, r1, r2)
            return r5
        L_0x045c:
            X.EjK r15 = (X.C29827EjK) r15
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r4 = r0[r2]
            X.0p4 r4 = (X.AnonymousClass0p4) r4
            java.lang.String r3 = r15.A00
            X.1JJ r1 = (X.AnonymousClass1JJ) r1
            X.A0T r2 = r1.A00
            X.5EU r0 = r1.A01
            boolean r1 = r0.saveButtonEnabled
            java.lang.String r0 = r3.trim()
            r2.A03 = r0
            X.AnonymousClass1JJ.A01(r4, r1, r2)
            return r5
        L_0x047a:
            X.EjK r15 = (X.C29827EjK) r15
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r4 = r0[r2]
            X.0p4 r4 = (X.AnonymousClass0p4) r4
            java.lang.String r3 = r15.A00
            X.1JJ r1 = (X.AnonymousClass1JJ) r1
            X.A0T r2 = r1.A00
            X.5EU r0 = r1.A01
            boolean r1 = r0.saveButtonEnabled
            java.lang.String r0 = r3.trim()
            r2.A01 = r0
            X.AnonymousClass1JJ.A01(r4, r1, r2)
            return r5
        L_0x0498:
            X.5xl r15 = (X.C126945xl) r15
            X.0zV r0 = r14.A00
            boolean r1 = r15.A01
            X.1JJ r0 = (X.AnonymousClass1JJ) r0
            X.A0T r0 = r0.A00
            r0.A05 = r1
            return r5
        L_0x04a5:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r2]
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r5
        L_0x04b1:
            X.0zV r0 = r14.A00
            X.1JJ r0 = (X.AnonymousClass1JJ) r0
            X.A0T r0 = r0.A00
            r0.A01()
            return r5
        L_0x04bb:
            int r3 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r2 = 0
            r1 = 0
            if (r3 == r0) goto L_0x04e6
            r0 = 1378299283(0x52272d93, float:1.79506037E11)
            if (r3 == r0) goto L_0x04da
            r0 = 1489149912(0x58c29fd8, float:1.71193424E15)
            if (r3 != r0) goto L_0x04d9
            X.0zV r0 = r14.A00
            X.1GJ r0 = (X.AnonymousClass1GJ) r0
            X.1BQ r0 = r0.A00
            if (r0 == 0) goto L_0x04d9
            r0.BdZ()
        L_0x04d9:
            return r1
        L_0x04da:
            X.0zV r0 = r14.A00
            X.1GJ r0 = (X.AnonymousClass1GJ) r0
            X.1BQ r0 = r0.A00
            if (r0 == 0) goto L_0x04d9
            r0.BdY()
            return r1
        L_0x04e6:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r2]
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r1
        L_0x04f2:
            r3 = r13
            X.1vD r3 = (X.AnonymousClass1vD) r3
            int r2 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r1 = 0
            r5 = 0
            if (r2 == r0) goto L_0x0ecb
            r0 = 829436257(0x31703161, float:3.4952665E-9)
            if (r2 != r0) goto L_0x0e7a
            java.lang.Object[] r1 = r14.A02
            r0 = 1
            r4 = r1[r0]
            com.facebook.resources.ui.FbFrameLayout r4 = (com.facebook.resources.ui.FbFrameLayout) r4
            int r1 = X.AnonymousClass1Y3.AgM
            X.0UN r3 = r3.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.4gT r2 = (X.C95464gT) r2
            int r1 = X.AnonymousClass1Y3.B6l
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.4h4 r1 = (X.AnonymousClass4h4) r1
            android.content.Context r0 = r4.getContext()
            r2.A0E(r0)
            r1.A01()
            return r5
        L_0x0529:
            r4 = r13
            X.1v6 r4 = (X.AnonymousClass1v6) r4
            int r2 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r1 = 0
            r3 = 0
            if (r2 == r0) goto L_0x116f
            r0 = 829436257(0x31703161, float:3.4952665E-9)
            if (r2 != r0) goto L_0x1129
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r4.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.06B r0 = (X.AnonymousClass06B) r0
            java.util.concurrent.atomic.AtomicLong r2 = X.AnonymousClass1v6.A0C
            long r0 = r0.now()
            r2.set(r0)
            java.util.concurrent.atomic.AtomicBoolean r0 = X.AnonymousClass1v6.A09
            r1 = 0
            r0.set(r1)
            java.util.concurrent.atomic.AtomicInteger r0 = X.AnonymousClass1v6.A0A
            r0.set(r1)
            return r3
        L_0x055a:
            int r3 = r14.A01
            r0 = -1741503230(0xffffffff9832c502, float:-2.3105424E-24)
            r2 = 1
            r1 = 0
            r5 = 0
            if (r3 == r0) goto L_0x0578
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r3 == r0) goto L_0x0ecb
            r0 = 829436257(0x31703161, float:3.4952665E-9)
            if (r3 != r0) goto L_0x0e7a
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r2]
            X.4h4 r0 = (X.AnonymousClass4h4) r0
            r0.A01()
            return r5
        L_0x0578:
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r2]
            com.facebook.ipc.stories.model.StoryCard r3 = (com.facebook.ipc.stories.model.StoryCard) r3
            com.google.common.collect.ImmutableList r1 = r3.A0T()
            if (r1 == 0) goto L_0x0e7a
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0e7a
            X.1Xv r4 = r1.iterator()
        L_0x058e:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0e7a
            java.lang.Object r2 = r4.next()
            X.3gF r2 = (X.C73513gF) r2
            java.lang.String r1 = r2.getTypeName()
            r0 = 360(0x168, float:5.04E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x058e
            r0 = -1902891955(0xffffffff8e942c4d, float:-3.6527477E-30)
            java.lang.String r0 = r2.A0P(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0e7a
            r0 = -1277195731(0xffffffffb3df8a2d, float:-1.0409381E-7)
            java.lang.String r0 = r2.A0P(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0e7a
            com.facebook.graphql.enums.GraphQLMinutiaeBubblePosition r0 = r2.A0Q()
            if (r0 == 0) goto L_0x0e7a
            r3.getId()
            r0 = 3355(0xd1b, float:4.701E-42)
            r2.A0P(r0)
            goto L_0x058e
        L_0x05d3:
            r7 = r13
            X.1v4 r7 = (X.C37311v4) r7
            int r0 = r14.A01
            r8 = 2
            r2 = 1
            r3 = 0
            r12 = 0
            switch(r0) {
                case -1741503230: goto L_0x06cc;
                case -1336101728: goto L_0x0738;
                case -1048037474: goto L_0x072c;
                case -888481032: goto L_0x063a;
                case 829436257: goto L_0x0604;
                case 1991993034: goto L_0x05e0;
                default: goto L_0x05df;
            }
        L_0x05df:
            return r12
        L_0x05e0:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r6 = r0[r3]
            X.0p4 r6 = (X.AnonymousClass0p4) r6
            r5 = r0[r2]
            java.lang.String r5 = (java.lang.String) r5
            r4 = r0[r8]
            java.lang.String r4 = (java.lang.String) r4
            X.1v4 r1 = (X.C37311v4) r1
            X.4Sj r3 = r1.A06
            int r2 = X.AnonymousClass1Y3.Aja
            X.0UN r1 = r7.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4N1 r2 = (X.AnonymousClass4N1) r2
            X.46n r0 = X.C860646n.STICKER_OVERLAY
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            goto L_0x0627
        L_0x0604:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r6 = r0[r3]
            X.0p4 r6 = (X.AnonymousClass0p4) r6
            r5 = r0[r2]
            java.lang.String r5 = (java.lang.String) r5
            r4 = r0[r8]
            java.lang.String r4 = (java.lang.String) r4
            X.1v4 r1 = (X.C37311v4) r1
            X.4Sj r3 = r1.A06
            int r2 = X.AnonymousClass1Y3.Aja
            X.0UN r1 = r7.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4N1 r2 = (X.AnonymousClass4N1) r2
            X.46n r0 = X.C860646n.STICKER_OVERLAY
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
        L_0x0627:
            X.C37311v4.A00(r6, r0)
            r3.Bkg()
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r1 == r0) goto L_0x05df
            java.lang.String r1 = X.AnonymousClass4FF.A00(r1)
            r0 = 0
            r2.A01(r5, r4, r1, r0)
            return r12
        L_0x063a:
            X.5yW r15 = (X.C127415yW) r15
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r6 = r0[r3]
            X.0p4 r6 = (X.AnonymousClass0p4) r6
            r4 = r0[r2]
            java.lang.String r4 = (java.lang.String) r4
            r3 = r0[r8]
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r11 = r15.A01
            X.1v4 r1 = (X.C37311v4) r1
            X.3gF r10 = r1.A01
            X.4Sj r8 = r1.A06
            int r1 = X.AnonymousClass1Y3.Aja
            X.0UN r2 = r7.A00
            r0 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r1, r2)
            X.4N1 r5 = (X.AnonymousClass4N1) r5
            int r1 = X.AnonymousClass1Y3.AbL
            r0 = 1
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r0, r1, r2)
            X.4FE r9 = (X.AnonymousClass4FE) r9
            if (r10 == 0) goto L_0x06b7
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r2 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = 796370404(0x2f77a5e4, float:2.2523455E-10)
            r0 = -320690508(0xffffffffece2a6b4, float:-2.1920357E27)
            com.facebook.graphservice.tree.TreeJNI r0 = r10.A0J(r1, r2, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r0
            if (r0 == 0) goto L_0x06b7
            java.lang.String r7 = r0.A3m()
            if (r7 == 0) goto L_0x06b7
            X.3vv r10 = new X.3vv
            r10.<init>()
            com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000 r1 = r9.A00
            X.0Tq r0 = r9.A04
            java.lang.Object r0 = r0.get()
            java.lang.String r0 = (java.lang.String) r0
            r1.A0B(r0)
            java.lang.String r0 = "story_card_question_id"
            r1.A09(r0, r7)
            java.lang.String r0 = "answer"
            r1.A09(r0, r11)
            java.lang.String r0 = "input"
            r10.A04(r0, r1)
            X.AkD r2 = X.C26931cN.A01(r10)
            X.0mD r1 = r9.A01
            X.6c1 r0 = X.C137796c1.A02
            com.google.common.util.concurrent.ListenableFuture r2 = r1.A06(r2, r0)
            X.4FD r1 = new X.4FD
            r1.<init>(r9, r4, r7)
            java.util.concurrent.Executor r0 = r9.A03
            X.C05350Yp.A08(r2, r1, r0)
        L_0x06b7:
            X.46n r0 = X.C860646n.STICKER_OVERLAY
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            X.C37311v4.A00(r6, r0)
            r8.Bkg()
            if (r1 == r1) goto L_0x05df
            java.lang.String r1 = X.AnonymousClass4FF.A00(r1)
            r0 = 0
            r5.A01(r4, r3, r1, r0)
            return r12
        L_0x06cc:
            java.lang.Object[] r0 = r14.A02
            r5 = r0[r2]
            java.lang.String r5 = (java.lang.String) r5
            r4 = r0[r8]
            java.lang.String r4 = (java.lang.String) r4
            int r2 = X.AnonymousClass1Y3.Aja
            X.0UN r1 = r7.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.4N1 r3 = (X.AnonymousClass4N1) r3
            X.1ZE r0 = r3.A00
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000.A05(r0)
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x05df
            java.lang.String r1 = "sticker_impression"
            java.lang.String r0 = "action"
            r2.A0D(r0, r1)
            X.4N7 r0 = r3.A01
            java.lang.String r1 = r0.A02()
            java.lang.String r0 = "tray_session_id"
            r2.A0D(r0, r1)
            X.4N7 r0 = r3.A01
            java.lang.String r1 = r0.A01()
            java.lang.String r0 = "viewer_session_id"
            r2.A0D(r0, r1)
            r2.A0S(r5)
            java.lang.String r0 = "sticker_id"
            r2.A0D(r0, r4)
            java.lang.String r1 = "question"
            java.lang.String r0 = "sticker_type"
            r2.A0D(r0, r1)
            java.lang.String r0 = "stories_interactive_feedback"
            r2.A0O(r0)
            X.4N7 r0 = r3.A01
            java.lang.String r1 = r0.A02()
            java.lang.String r0 = "pigeon_reserved_keyword_uuid"
            r2.A0D(r0, r1)
            r2.A06()
            return r12
        L_0x072c:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r3]
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r12
        L_0x0738:
            X.2Zp r15 = (X.C48122Zp) r15
            X.0zV r2 = r14.A00
            java.lang.Object[] r1 = r14.A02
            r9 = r1[r3]
            X.0p4 r9 = (X.AnonymousClass0p4) r9
            r8 = r1[r8]
            X.46n r8 = (X.C860646n) r8
            r0 = 3
            r5 = r1[r0]
            java.lang.String r5 = (java.lang.String) r5
            r0 = 4
            r4 = r1[r0]
            java.lang.String r4 = (java.lang.String) r4
            android.view.MotionEvent r10 = r15.A00
            X.1v4 r2 = (X.C37311v4) r2
            X.4Sj r6 = r2.A06
            int r2 = X.AnonymousClass1Y3.Aja
            X.0UN r1 = r7.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.4N1 r3 = (X.AnonymousClass4N1) r3
            int r1 = r10.getAction()
            r0 = 1
            if (r1 == 0) goto L_0x07da
            if (r1 == r0) goto L_0x076f
            r0 = 0
        L_0x076a:
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r0)
            return r12
        L_0x076f:
            X.46n r0 = X.C860646n.RESPONSE_EDITOR_OVERLAY
            if (r8 != r0) goto L_0x07dc
            X.C37311v4.A00(r9, r8)
            r6.Bkh()
            X.4IS r6 = r3.A02
            X.4N6 r2 = new X.4N6
            X.06B r0 = r6.A01
            long r0 = r0.now()
            r2.<init>(r4, r0)
            r6.A00 = r2
            java.lang.String r6 = r2.A01
            X.1ZE r0 = r3.A00
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000.A05(r0)
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x07da
            java.lang.String r1 = "sticker_session_start"
            java.lang.String r0 = "action"
            r2.A0D(r0, r1)
            X.4N7 r0 = r3.A01
            java.lang.String r1 = r0.A02()
            java.lang.String r0 = "tray_session_id"
            r2.A0D(r0, r1)
            X.4N7 r0 = r3.A01
            java.lang.String r1 = r0.A01()
            java.lang.String r0 = "viewer_session_id"
            r2.A0D(r0, r1)
            r2.A0S(r5)
            java.lang.String r0 = "sticker_session_id"
            r2.A0D(r0, r6)
            java.lang.String r0 = "sticker_id"
            r2.A0D(r0, r4)
            java.lang.String r1 = "question"
            java.lang.String r0 = "sticker_type"
            r2.A0D(r0, r1)
            java.lang.String r0 = "stories_interactive_feedback"
            r2.A0O(r0)
            X.4N7 r0 = r3.A01
            java.lang.String r1 = r0.A02()
            java.lang.String r0 = "pigeon_reserved_keyword_uuid"
            r2.A0D(r0, r1)
            r2.A06()
        L_0x07da:
            r0 = 1
            goto L_0x076a
        L_0x07dc:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            X.C37311v4.A00(r9, r8)
            r6.Bkg()
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r1 == r0) goto L_0x07da
            java.lang.String r1 = X.AnonymousClass4FF.A00(r1)
            r0 = 0
            r3.A01(r5, r4, r1, r0)
            goto L_0x07da
        L_0x07f1:
            int r3 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r1 = 0
            r2 = 0
            if (r3 == r0) goto L_0x0f9e
            r0 = 829436257(0x31703161, float:3.4952665E-9)
            if (r3 != r0) goto L_0x0f6d
            java.lang.Object[] r1 = r14.A02
            r0 = 1
            r0 = r1[r0]
            X.4h4 r0 = (X.AnonymousClass4h4) r0
            r0.A01()
            return r2
        L_0x080a:
            r5 = r13
            X.1kt r5 = (X.C31971kt) r5
            int r0 = r14.A01
            r1 = 0
            r4 = 0
            switch(r0) {
                case -1933296085: goto L_0x088f;
                case -1048037474: goto L_0x0c5f;
                case -843172861: goto L_0x0862;
                case -352845993: goto L_0x0850;
                case 1590505951: goto L_0x0815;
                default: goto L_0x0814;
            }
        L_0x0814:
            return r4
        L_0x0815:
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r1]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            int r2 = X.AnonymousClass1Y3.Asl
            X.0UN r1 = r5.A00
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.22o r0 = (X.C406922o) r0
            X.1ZE r1 = r0.A00
            java.lang.String r0 = "ax_expand_montage_and_active_now_tray"
            X.0bW r2 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r1 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 89
            r1.<init>(r2, r0)
            boolean r0 = r1.A0G()
            if (r0 == 0) goto L_0x083e
            r1.A06()
        L_0x083e:
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0be4
            X.2yh r2 = new X.2yh
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:MontageAndActiveNowComponent.updateStoriesTrayAccessibleState"
            r3.A0G(r2, r0)
            return r4
        L_0x0850:
            java.lang.Object[] r2 = r14.A02
            r0 = 1
            r1 = r2[r0]
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r1 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r1
            r0 = 2
            r0 = r2[r0]
            X.1nR r0 = (X.C33351nR) r0
            X.1BF r0 = r0.A06
            r0.BfU(r1)
            return r4
        L_0x0862:
            X.216 r15 = (X.AnonymousClass216) r15
            java.lang.Object[] r0 = r14.A02
            r7 = r0[r1]
            X.0p4 r7 = (X.AnonymousClass0p4) r7
            android.view.View r6 = r15.A02
            int r5 = r15.A00
            android.os.Bundle r4 = r15.A01
            X.0uv r3 = r15.A03
            r0 = 262144(0x40000, float:3.67342E-40)
            if (r5 != r0) goto L_0x0886
            X.0zR r0 = r7.A04
            if (r0 == 0) goto L_0x0886
            X.2yh r2 = new X.2yh
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:MontageAndActiveNowComponent.updateStoriesTrayAccessibleState"
            r7.A0G(r2, r0)
        L_0x0886:
            boolean r0 = r3.A0D(r6, r5, r4)
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r0)
            return r4
        L_0x088f:
            X.5E3 r15 = (X.AnonymousClass5E3) r15
            X.0zV r0 = r14.A00
            X.0uv r3 = r15.A01
            android.view.View r2 = r15.A00
            androidx.core.view.accessibility.AccessibilityNodeInfoCompat r1 = r15.A02
            X.1kt r0 = (X.C31971kt) r0
            X.1mm r0 = r0.A06
            boolean r0 = r0.storiesTrayAccessible
            r3.A0I(r2, r1)
            if (r0 == 0) goto L_0x0be4
            X.Cpq r0 = X.C25973Cpq.A05
            r1.A0H(r0)
            return r4
        L_0x08aa:
            r5 = r13
            X.1vP r5 = (X.AnonymousClass1vP) r5
            int r2 = r14.A01
            r0 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            r1 = 0
            r4 = 0
            if (r2 == r0) goto L_0x08f6
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r2 == r0) goto L_0x0c5f
            r0 = 1803022739(0x6b77f193, float:2.9974548E26)
            if (r2 != r0) goto L_0x0be4
            X.0zV r0 = r14.A00
            X.1vP r0 = (X.AnonymousClass1vP) r0
            com.facebook.messaging.montage.forked.model.hcontroller.ControllerParams r3 = r0.A01
            int r2 = X.AnonymousClass1Y3.A03
            X.0UN r1 = r5.A00
            r0 = 3
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.AmU r5 = (X.C21999AmU) r5
            com.facebook.ipc.stories.model.StoryCard r0 = r3.A04
            if (r0 == 0) goto L_0x0be4
            X.50f r0 = r0.getMedia()
            if (r0 == 0) goto L_0x0be4
            com.facebook.ipc.stories.model.StoryCard r0 = r3.A04
            java.lang.String r7 = X.C861849i.A00(r0)
            X.50f r0 = r0.getMedia()
            java.lang.String r8 = r0.A08
            com.facebook.ipc.stories.model.StoryCard r0 = r3.A04
            java.lang.String r10 = X.AnonymousClass1vP.A00(r0)
            java.lang.String r9 = "story"
            java.lang.String r6 = "warning_screen_visible"
            r11 = 0
            X.C21999AmU.A01(r5, r6, r7, r8, r9, r10, r11)
            return r4
        L_0x08f6:
            X.0zV r0 = r14.A00
            X.1vP r0 = (X.AnonymousClass1vP) r0
            com.facebook.messaging.montage.forked.model.hcontroller.ControllerParams r2 = r0.A01
            int r3 = X.AnonymousClass1Y3.A03
            X.0UN r1 = r5.A00
            r0 = 3
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r3, r1)
            X.AmU r5 = (X.C21999AmU) r5
            com.facebook.ipc.stories.model.StoryCard r0 = r2.A04
            if (r0 == 0) goto L_0x092b
            X.50f r0 = r0.getMedia()
            if (r0 == 0) goto L_0x092b
            com.facebook.ipc.stories.model.StoryCard r0 = r2.A04
            java.lang.String r7 = X.C861849i.A00(r0)
            X.50f r0 = r0.getMedia()
            java.lang.String r8 = r0.A08
            com.facebook.ipc.stories.model.StoryCard r0 = r2.A04
            java.lang.String r10 = X.AnonymousClass1vP.A00(r0)
            java.lang.String r9 = "story"
            java.lang.String r6 = "warning_screen_uncover_tapped"
            r11 = 0
            X.C21999AmU.A01(r5, r6, r7, r8, r9, r10, r11)
        L_0x092b:
            java.lang.String r0 = "HideWarningScreen"
            X.469 r1 = r2.A03(r0)
            r0 = 0
            r1.A03(r0)
            r1.A02(r4)
            X.43z r1 = r2.A08
            java.lang.Class<X.46A> r0 = X.AnonymousClass46A.class
            java.lang.Object r2 = r1.A01(r0)
            X.46A r2 = (X.AnonymousClass46A) r2
            X.0bK r0 = r2.A01     // Catch:{ all -> 0x0962 }
            java.util.List r0 = r0.A00()     // Catch:{ all -> 0x0962 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0962 }
        L_0x094c:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0962 }
            if (r0 == 0) goto L_0x095c
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0962 }
            X.43B r0 = (X.AnonymousClass43B) r0     // Catch:{ all -> 0x0962 }
            r0.C3g()     // Catch:{ all -> 0x0962 }
            goto L_0x094c
        L_0x095c:
            X.0bK r0 = r2.A01
            r0.A01()
            return r4
        L_0x0962:
            r1 = move-exception
            X.0bK r0 = r2.A01
            r0.A01()
            throw r1
        L_0x0969:
            r6 = r13
            X.10e r6 = (X.C181010e) r6
            int r4 = r14.A01
            r0 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            r2 = 1
            r1 = 0
            r3 = 0
            if (r4 == r0) goto L_0x09ae
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r4 == r0) goto L_0x116f
            r0 = 71235917(0x43ef94d, float:2.2448866E-36)
            if (r4 != r0) goto L_0x1129
            java.lang.Object[] r0 = r14.A02
            r5 = r0[r1]
            X.0p4 r5 = (X.AnonymousClass0p4) r5
            r4 = r0[r2]
            X.0wO r4 = (X.C16040wO) r4
            int r2 = X.AnonymousClass1Y3.AP4
            X.0UN r1 = r6.A01
            r0 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2KL r3 = (X.AnonymousClass2KL) r3
            android.content.Context r2 = r5.A09
            java.util.Map r0 = X.AnonymousClass2KL.A03
            java.lang.Object r1 = r0.get(r4)
            X.732 r1 = (X.AnonymousClass732) r1
            if (r1 != 0) goto L_0x09a7
            r0 = 0
        L_0x09a2:
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r0)
            return r3
        L_0x09a7:
            X.5iN r0 = r3.A00
            r0.A01(r2, r1)
            r0 = 1
            goto L_0x09a2
        L_0x09ae:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r2 = r0[r2]
            X.0wO r2 = (X.C16040wO) r2
            X.10e r1 = (X.C181010e) r1
            X.1nz r1 = r1.A02
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r1.A01(r2, r0)
            return r3
        L_0x09c0:
            r6 = r13
            X.1K6 r6 = (X.AnonymousClass1K6) r6
            int r3 = r14.A01
            r0 = -1306401257(0xffffffffb221e617, float:-9.42375E-9)
            r2 = 1
            r1 = 0
            r5 = 0
            if (r3 == r0) goto L_0x0a32
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r3 == r0) goto L_0x0ecb
            r0 = -547357317(0xffffffffdf5ffd7b, float:-1.6140192E19)
            if (r3 != r0) goto L_0x0e7a
            java.lang.Object[] r0 = r14.A02
            r5 = r0[r1]
            X.0p4 r5 = (X.AnonymousClass0p4) r5
            r4 = r0[r2]
            X.0vY r4 = (X.C15600vY) r4
            int r1 = X.AnonymousClass1Y3.BBg
            X.0UN r3 = r6.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            int r1 = X.AnonymousClass1Y3.AP4
            r0 = 2
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.2KL r3 = (X.AnonymousClass2KL) r3
            boolean r0 = r2.booleanValue()
            if (r0 == 0) goto L_0x0a10
            android.content.Context r2 = r5.A09
            java.lang.Class r1 = r4.A00
            java.util.Map r0 = X.AnonymousClass2KL.A02
            java.lang.Object r1 = r0.get(r1)
            X.732 r1 = (X.AnonymousClass732) r1
            if (r1 == 0) goto L_0x0a10
            X.5iN r0 = r3.A00
            r0.A01(r2, r1)
            r0 = 1
            goto L_0x0a2a
        L_0x0a10:
            r0 = 0
            goto L_0x0a2a
        L_0x0a12:
            X.0zV r0 = r14.A00
            X.1JJ r0 = (X.AnonymousClass1JJ) r0
            X.A0T r1 = r0.A00
            boolean r0 = r1.A04
            if (r0 == 0) goto L_0x0a2f
            java.lang.String r0 = r1.A03
        L_0x0a1e:
            if (r0 == 0) goto L_0x0a10
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0a10
            r1.A01()
            r0 = 1
        L_0x0a2a:
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            return r5
        L_0x0a2f:
            java.lang.String r0 = r1.A02
            goto L_0x0a1e
        L_0x0a32:
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r2]
            X.0vY r3 = (X.C15600vY) r3
            int r2 = X.AnonymousClass1Y3.A7C
            X.0UN r1 = r6.A00
            r0 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0rN r4 = (X.C13400rN) r4
            X.0vE r0 = r3.A01
            int r0 = r0.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r4.A01 = r0
            X.10b r1 = r3.A03
            X.0vQ r0 = r1.A00
            X.0vD r0 = r0.Ae6()
            r0.BUD()
            X.0vQ r3 = r1.A00
            X.0vW r0 = r1.A01
            android.content.Context r2 = r0.A00
            X.1nz r1 = r0.A02
            X.0qW r0 = r0.A01
            r3.BSD(r2, r1, r0)
            r4.A01 = r5
            r4.A00 = r5
            return r5
        L_0x0a6a:
            r7 = r13
            X.1l9 r7 = (X.C32101l9) r7
            int r1 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r2 = 0
            r6 = 0
            if (r1 == r0) goto L_0x0ad2
            r0 = -301563750(0xffffffffee06809a, float:-1.0406615E28)
            if (r1 == r0) goto L_0x0aa7
            r0 = 1154961974(0x44d75236, float:1722.5691)
            if (r1 != r0) goto L_0x0ccf
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r5 = r0[r2]
            X.0p4 r5 = (X.AnonymousClass0p4) r5
            X.1l9 r1 = (X.C32101l9) r1
            X.1nz r4 = r1.A01
            X.0vE r3 = r1.A02
            int r2 = X.AnonymousClass1Y3.A7C
            X.0UN r1 = r7.A00
            r0 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.0rN r1 = (X.C13400rN) r1
            int r0 = r3.A00
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.A01 = r0
            android.content.Context r0 = r5.A09
            r4.Bw0(r0)
            return r6
        L_0x0aa7:
            java.lang.Object[] r0 = r14.A02
            r4 = r0[r2]
            X.0p4 r4 = (X.AnonymousClass0p4) r4
            int r1 = X.AnonymousClass1Y3.BBg
            X.0UN r3 = r7.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            int r1 = X.AnonymousClass1Y3.AP4
            r0 = 3
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.2KL r1 = (X.AnonymousClass2KL) r1
            boolean r0 = r2.booleanValue()
            if (r0 == 0) goto L_0x0d71
            android.content.Context r2 = r4.A09
            X.5iN r1 = r1.A00
            X.732 r0 = X.AnonymousClass2KL.A01
            r1.A01(r2, r0)
            r0 = 1
            goto L_0x0dac
        L_0x0ad2:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r2]
            goto L_0x0dba
        L_0x0ad8:
            int r0 = r14.A01
            r3 = 0
            r6 = 0
            switch(r0) {
                case -1975520952: goto L_0x0b49;
                case -1760520986: goto L_0x0b3b;
                case -1048037474: goto L_0x0b35;
                case -256462297: goto L_0x0b27;
                case 96515278: goto L_0x0b04;
                case 1931733614: goto L_0x0ae0;
                default: goto L_0x0adf;
            }
        L_0x0adf:
            return r6
        L_0x0ae0:
            X.EjJ r15 = (X.C29826EjJ) r15
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r5 = r0[r3]
            X.0p4 r5 = (X.AnonymousClass0p4) r5
            java.lang.String r4 = r15.A00
            X.1v3 r1 = (X.C37301v3) r1
            X.9gP r3 = r1.A06
            X.0zR r0 = r5.A04
            if (r0 == 0) goto L_0x0b01
            X.2yh r2 = new X.2yh
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:RequestCodeComponent.increaseUiVersion"
            r5.A0G(r2, r0)
        L_0x0b01:
            r3.A00 = r4
            return r6
        L_0x0b04:
            X.5u7 r15 = (X.C124845u7) r15
            X.0zV r0 = r14.A00
            int r3 = r15.A00
            X.1v3 r0 = (X.C37301v3) r0
            X.9gP r2 = r0.A06
            android.view.View$OnClickListener r1 = r0.A03
            if (r1 == 0) goto L_0x0d71
            r0 = 6
            if (r3 != r0) goto L_0x0d71
            java.lang.String r0 = r2.A00
            java.lang.String r0 = r0.trim()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0d71
            r1.onClick(r6)
            r0 = 1
            goto L_0x0dac
        L_0x0b27:
            X.2Zq r15 = (X.C48132Zq) r15
            X.0zV r0 = r14.A00
            android.view.View r1 = r15.A00
            X.1v3 r0 = (X.C37301v3) r0
            android.view.View$OnClickListener r0 = r0.A03
            r0.onClick(r1)
            return r6
        L_0x0b35:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r3]
            goto L_0x0dba
        L_0x0b3b:
            X.2Zq r15 = (X.C48132Zq) r15
            X.0zV r0 = r14.A00
            android.view.View r1 = r15.A00
            X.1v3 r0 = (X.C37301v3) r0
            android.view.View$OnClickListener r0 = r0.A04
            r0.onClick(r1)
            return r6
        L_0x0b49:
            X.Ein r15 = (X.C29797Ein) r15
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r3]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            java.lang.String r1 = r15.A00
            X.1v3 r2 = (X.C37301v3) r2
            X.2Fx r0 = r2.A05
            r0.A00 = r1
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0ccf
            X.2yh r2 = new X.2yh
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:RequestCodeComponent.increaseUiVersion"
            r3.A0G(r2, r0)
            return r6
        L_0x0b6d:
            int r0 = r14.A01
            r1 = 0
            r3 = 0
            switch(r0) {
                case -1505368922: goto L_0x0b75;
                case -1351902487: goto L_0x0b87;
                case -1048037474: goto L_0x116f;
                case 378110312: goto L_0x0b91;
                default: goto L_0x0b74;
            }
        L_0x0b74:
            return r3
        L_0x0b75:
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r1 = r0[r1]
            X.0p4 r1 = (X.AnonymousClass0p4) r1
            X.1GN r2 = (X.AnonymousClass1GN) r2
            X.1Ir r0 = r2.A04
            if (r0 == 0) goto L_0x1129
            r0.Bux(r1)
            return r3
        L_0x0b87:
            X.0zV r0 = r14.A00
            X.1GN r0 = (X.AnonymousClass1GN) r0
            X.1Ba r0 = r0.A02
            r0.BSB()
            return r3
        L_0x0b91:
            X.5MP r15 = (X.AnonymousClass5MP) r15
            X.0zV r0 = r14.A00
            java.lang.String r1 = r15.A00
            X.1GN r0 = (X.AnonymousClass1GN) r0
            X.1JH r0 = r0.A03
            if (r0 == 0) goto L_0x1129
            r0.BrY(r1)
            return r3
        L_0x0ba1:
            r5 = r13
            X.1J1 r5 = (X.AnonymousClass1J1) r5
            int r2 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r1 = 0
            r4 = 0
            if (r2 == r0) goto L_0x0c5f
            r0 = 904927013(0x35f01725, float:1.7888129E-6)
            if (r2 != r0) goto L_0x0be4
            java.lang.Object[] r1 = r14.A02
            r0 = 1
            r3 = r1[r0]
            com.facebook.messaging.model.threads.ThreadSummary r3 = (com.facebook.messaging.model.threads.ThreadSummary) r3
            int r2 = X.AnonymousClass1Y3.APZ
            X.0UN r1 = r5.A00
            r0 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.2KI r5 = (X.AnonymousClass2KI) r5
            java.lang.String r0 = r3.A0s
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            r0 = r0 ^ 1
            X.AnonymousClass064.A03(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0S
            long r6 = r0.A0G()
            java.lang.String r8 = r3.A0s
            long r9 = r3.A0B
            r5.A02(r6, r8, r9)
            return r4
        L_0x0bdd:
            int r0 = r14.A01
            r1 = 0
            r4 = 0
            switch(r0) {
                case -1048037474: goto L_0x0c5f;
                case -670330384: goto L_0x0be5;
                case 96515278: goto L_0x0c21;
                case 1196116736: goto L_0x0bf1;
                default: goto L_0x0be4;
            }
        L_0x0be4:
            return r4
        L_0x0be5:
            X.0zV r0 = r14.A00
            X.1JO r0 = (X.AnonymousClass1JO) r0
            X.9j4 r0 = r0.A01
            if (r0 == 0) goto L_0x0be4
            r0.A00()
            return r4
        L_0x0bf1:
            X.EjK r15 = (X.C29827EjK) r15
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r1]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            java.lang.String r1 = r15.A00
            X.1JO r2 = (X.AnonymousClass1JO) r2
            X.9j4 r0 = r2.A01
            if (r0 == 0) goto L_0x0c0f
            java.lang.String r1 = r1.trim()
            X.9ia r0 = r0.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecPassword r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecPassword) r0
            r0.A00 = r1
        L_0x0c0f:
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0be4
            X.2yh r2 = new X.2yh
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r2.<init>(r1, r0)
            java.lang.String r0 = "updateState:AccountLoginRecPasswordRootComponent.increaseUiVersion"
            r3.A0G(r2, r0)
            return r4
        L_0x0c21:
            X.5u7 r15 = (X.C124845u7) r15
            X.0zV r0 = r14.A00
            int r3 = r15.A00
            X.1JO r0 = (X.AnonymousClass1JO) r0
            X.9j4 r2 = r0.A01
            X.5EV r0 = r0.A02
            X.2Nf r1 = r0.stateContainer
            if (r2 == 0) goto L_0x0c49
            r0 = 6
            if (r3 != r0) goto L_0x0c49
            java.lang.String r0 = r1.A00
            java.lang.String r0 = r0.trim()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0c49
            r2.A00()
            r0 = 1
        L_0x0c44:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r0)
            return r4
        L_0x0c49:
            r0 = 0
            goto L_0x0c44
        L_0x0c4b:
            int r1 = r14.A01
            r0 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            r2 = 0
            r4 = 0
            if (r1 == r0) goto L_0x0c6b
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r1 == r0) goto L_0x0c5a
            return r4
        L_0x0c5a:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r2]
            goto L_0x0c63
        L_0x0c5f:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r1]
        L_0x0c63:
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r4
        L_0x0c6b:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r2]
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.1J6 r1 = (X.AnonymousClass1J6) r1
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r3 = r1.A01
            X.1BN r2 = r1.A02
            X.AnonymousClass064.A00(r2)
            X.1Qt r1 = r3.A04
            X.AnonymousClass064.A00(r1)
            android.content.Context r0 = r0.A09
            r2.BQe(r0, r3, r1)
            return r4
        L_0x0c87:
            r4 = r13
            X.1JF r4 = (X.AnonymousClass1JF) r4
            int r2 = r14.A01
            r0 = -1336101728(0xffffffffb05cb4a0, float:-8.029222E-10)
            r1 = 0
            if (r2 == r0) goto L_0x0dc2
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r6 = 0
            if (r2 == r0) goto L_0x0db1
            r0 = 792338070(0x2f3a1e96, float:1.6927468E-10)
            if (r2 != r0) goto L_0x0ccf
            X.0zV r3 = r14.A00
            X.1JF r3 = (X.AnonymousClass1JF) r3
            X.1BO r5 = r3.A02
            int r2 = X.AnonymousClass1Y3.AEL
            X.0UN r1 = r4.A00
            r0 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.4Ou r4 = (X.C89374Ou) r4
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r3 = r3.A01
            X.AnonymousClass064.A00(r3)
            java.lang.String r2 = "thread_item_profile_image"
            r0 = 63
            java.lang.String r1 = X.C99084oO.$const$string(r0)
            java.lang.String r0 = "tap"
            r4.A07(r2, r1, r0)
            com.facebook.messaging.montage.model.MontageBucketKey r0 = r3.A02
            long r0 = r0.A00
            r5.Bff(r0)
            return r6
        L_0x0cc8:
            int r0 = r14.A01
            r5 = 0
            r6 = 0
            switch(r0) {
                case -1048037474: goto L_0x0d58;
                case -952092468: goto L_0x0cd0;
                case -855949748: goto L_0x0cff;
                case 96515278: goto L_0x0d5d;
                case 1196116736: goto L_0x0d0b;
                case 1415173789: goto L_0x0d43;
                default: goto L_0x0ccf;
            }
        L_0x0ccf:
            return r6
        L_0x0cd0:
            X.0zV r1 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r4 = r0[r5]
            X.0p4 r4 = (X.AnonymousClass0p4) r4
            X.1JK r1 = (X.AnonymousClass1JK) r1
            X.9j9 r3 = r1.A02
            X.9j5 r2 = r1.A01
            boolean r0 = r3.A00
            r1 = r0 ^ 1
            r3.A00 = r1
            if (r2 == 0) goto L_0x0cee
            X.9iO r0 = r2.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatchLogin r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatchLogin) r0
            r0.A09 = r1
        L_0x0cee:
            X.0zR r0 = r4.A04
            if (r0 == 0) goto L_0x0ccf
            X.2yh r2 = new X.2yh
            java.lang.Object[] r0 = new java.lang.Object[r5]
            r2.<init>(r5, r0)
            java.lang.String r0 = "updateState:AccountRegSoftmatchLoginComponent.increaseUiVersion"
            r4.A0G(r2, r0)
            return r6
        L_0x0cff:
            X.0zV r0 = r14.A00
            X.1JK r0 = (X.AnonymousClass1JK) r0
            X.9j5 r0 = r0.A01
            if (r0 == 0) goto L_0x0ccf
            r0.A00()
            return r6
        L_0x0d0b:
            X.EjK r15 = (X.C29827EjK) r15
            X.0zV r2 = r14.A00
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r5]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            java.lang.String r1 = r15.A00
            X.1JK r2 = (X.AnonymousClass1JK) r2
            X.9j5 r0 = r2.A01
            if (r0 == 0) goto L_0x0d32
            java.lang.String r2 = r1.trim()
            X.9iO r1 = r0.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatchLogin r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatchLogin) r0
            r0.A06 = r2
            boolean r0 = r1.A03
            if (r0 == 0) goto L_0x0d32
            r1.A03 = r5
            r1.A2c()
        L_0x0d32:
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x0ccf
            X.2yh r2 = new X.2yh
            java.lang.Object[] r0 = new java.lang.Object[r5]
            r2.<init>(r5, r0)
            java.lang.String r0 = "updateState:AccountRegSoftmatchLoginComponent.increaseUiVersion"
            r3.A0G(r2, r0)
            return r6
        L_0x0d43:
            X.0zV r0 = r14.A00
            X.1JK r0 = (X.AnonymousClass1JK) r0
            X.9j5 r1 = r0.A01
            if (r1 == 0) goto L_0x0ccf
            X.9iO r0 = r1.A00
            r0.A2W()
            X.9iO r1 = r1.A00
            X.9iE r0 = X.C203229iE.A0E
            r1.A2X(r0)
            return r6
        L_0x0d58:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r5]
            goto L_0x0dba
        L_0x0d5d:
            X.5u7 r15 = (X.C124845u7) r15
            X.0zV r0 = r14.A00
            int r2 = r15.A00
            X.1JK r0 = (X.AnonymousClass1JK) r0
            X.9j5 r1 = r0.A01
            if (r1 == 0) goto L_0x0d71
            r0 = 6
            if (r2 != r0) goto L_0x0d71
            r1.A00()
            r0 = 1
            goto L_0x0dac
        L_0x0d71:
            r0 = 0
            goto L_0x0dac
        L_0x0d73:
            X.5u7 r15 = (X.C124845u7) r15
            X.0zV r0 = r14.A00
            int r5 = r15.A00
            X.11H r0 = (X.AnonymousClass11H) r0
            boolean r4 = r0.A09
            boolean r3 = r0.A0A
            X.9j9 r2 = r0.A05
            X.9id r1 = r0.A03
            if (r1 == 0) goto L_0x0d71
            r0 = 6
            if (r5 != r0) goto L_0x0d71
            X.2Nf r0 = r2.A02
            java.lang.String r0 = r0.A00
            java.lang.String r0 = r0.trim()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0d71
            X.9gP r0 = r2.A01
            java.lang.String r0 = r0.A00
            java.lang.String r0 = r0.trim()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0d71
            if (r3 != 0) goto L_0x0d71
            if (r4 != 0) goto L_0x0d71
            r1.A00()
            r0 = 1
        L_0x0dac:
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r0)
            return r6
        L_0x0db1:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r1]
            goto L_0x0dba
        L_0x0db6:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r4]
        L_0x0dba:
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r6
        L_0x0dc2:
            X.2Zp r15 = (X.C48122Zp) r15
            java.lang.Object[] r0 = r14.A02
            r3 = r0[r1]
            X.0p4 r3 = (X.AnonymousClass0p4) r3
            android.view.MotionEvent r0 = r15.A00
            int r2 = r0.getAction()
            r0 = 1
            if (r2 == 0) goto L_0x0de1
            if (r2 == r0) goto L_0x0ddd
            r0 = 3
            if (r2 == r0) goto L_0x0ddd
        L_0x0dd8:
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r1)
            return r6
        L_0x0ddd:
            X.AnonymousClass1JF.A00(r3, r1)
            goto L_0x0dd8
        L_0x0de1:
            X.AnonymousClass1JF.A00(r3, r0)
            goto L_0x0dd8
        L_0x0de5:
            int r3 = r14.A01
            r0 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            r1 = 0
            r2 = 0
            if (r3 == r0) goto L_0x0e0d
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r3 == r0) goto L_0x0f9e
            r0 = 71235917(0x43ef94d, float:2.2448866E-36)
            if (r3 != r0) goto L_0x0f6d
            X.5O1 r15 = (X.AnonymousClass5O1) r15
            X.0zV r0 = r14.A00
            android.view.View r1 = r15.A00
            X.1NM r0 = (X.AnonymousClass1NM) r0
            X.1MF r0 = r0.A09
            if (r0 == 0) goto L_0x0e0a
            r0.Bdx(r1)
            r0 = 1
            goto L_0x0f61
        L_0x0e0a:
            r0 = 0
            goto L_0x0f61
        L_0x0e0d:
            X.2Zq r15 = (X.C48132Zq) r15
            X.0zV r0 = r14.A00
            android.view.View r1 = r15.A00
            X.1NM r0 = (X.AnonymousClass1NM) r0
            X.1MF r0 = r0.A09
            if (r0 == 0) goto L_0x0f6d
            r0.onClick(r1)
            return r2
        L_0x0e1d:
            r3 = r13
            X.1vK r3 = (X.AnonymousClass1vK) r3
            int r0 = r14.A01
            r1 = 0
            r5 = 0
            switch(r0) {
                case -1254653778: goto L_0x0e28;
                case -1048037474: goto L_0x0ecb;
                case 1803022739: goto L_0x0ed7;
                case 2019308025: goto L_0x0eec;
                default: goto L_0x0e27;
            }
        L_0x0e27:
            return r5
        L_0x0e28:
            X.0zV r0 = r14.A00
            X.1vK r0 = (X.AnonymousClass1vK) r0
            X.DGF r3 = r0.A01
            X.7KV r0 = r3.A04
            java.lang.String r2 = "migration_screen_cancel"
            X.14b r1 = r0.A00
            X.0gA r0 = X.C08870g7.A0O
            r1.AOI(r0, r2)
            X.DGi r0 = r3.A06
            if (r0 == 0) goto L_0x0e7a
            X.DGE r0 = r0.A00
            r0.A2W()
            return r5
        L_0x0e43:
            int r2 = r14.A01
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            r1 = 0
            r5 = 0
            if (r2 == r0) goto L_0x0ecb
            r0 = -1047175806(0xffffffffc1955d82, float:-18.670658)
            if (r2 == r0) goto L_0x0e7b
            r0 = 2037761066(0x7975c42a, float:7.9755713E34)
            if (r2 != r0) goto L_0x0e7a
            X.0zV r0 = r14.A00
            X.1Jk r0 = (X.C21971Jk) r0
            X.9jg r3 = r0.A02
            if (r3 == 0) goto L_0x0e7a
            X.9iU r0 = r3.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatch r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatch) r0
            int r2 = r0.A00
            r1 = 1
            int r2 = r2 + r1
            java.util.List r0 = r0.A09
            int r0 = r0.size()
            if (r2 < r0) goto L_0x0e71
            r1 = 0
        L_0x0e71:
            if (r1 == 0) goto L_0x0e97
            X.9iU r1 = r3.A00
            X.9iE r0 = X.C203229iE.A0G
            r1.A2X(r0)
        L_0x0e7a:
            return r5
        L_0x0e7b:
            X.0zV r0 = r14.A00
            X.1Jk r0 = (X.C21971Jk) r0
            X.9jg r1 = r0.A02
            if (r1 == 0) goto L_0x0e7a
            X.9iU r0 = r1.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r0.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatch r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatch) r0
            com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount r0 = r0.A08()
            if (r0 == 0) goto L_0x0e7a
            X.9iU r1 = r1.A00
            X.9iE r0 = X.C203229iE.A0H
            r1.A2X(r0)
            return r5
        L_0x0e97:
            X.9iU r1 = r3.A00
            X.9hp r0 = r1.A01
            if (r0 != 0) goto L_0x0ec6
            r4 = 0
        L_0x0e9e:
            X.20s r3 = r1.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r0 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatch r0 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRegSoftMatch) r0
            com.facebook.messaging.phoneconfirmation.protocol.RegisterMessengerOnlyUserRegData r6 = new com.facebook.messaging.phoneconfirmation.protocol.RegisterMessengerOnlyUserRegData
            java.lang.String r7 = r0.A06
            java.lang.String r8 = r0.A03
            java.lang.String r9 = r0.A01
            java.lang.String r10 = r0.A04
            java.lang.String r11 = r0.A07
            java.lang.String r12 = r0.A02
            r6.<init>(r7, r8, r9, r10, r11, r12)
            java.lang.String r1 = r0.A08
            r0 = 1
            com.facebook.messaging.phoneconfirmation.protocol.RegisterMessengerOnlyUserParams r2 = new com.facebook.messaging.phoneconfirmation.protocol.RegisterMessengerOnlyUserParams
            r2.<init>(r6, r1, r0, r4)
            r1 = 2131821052(0x7f1101fc, float:1.9274836E38)
            java.lang.String r0 = "action_create_account_after_ignoring_recovery"
            r3.A03(r2, r1, r0)
            return r5
        L_0x0ec6:
            java.lang.String r4 = r0.Ala()
            goto L_0x0e9e
        L_0x0ecb:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r1]
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r5
        L_0x0ed7:
            int r2 = X.AnonymousClass1Y3.AVG
            X.0UN r1 = r3.A00
            r0 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.7KV r0 = (X.AnonymousClass7KV) r0
            java.lang.String r2 = "migration_screen_opened"
            X.14b r1 = r0.A00
            X.0gA r0 = X.C08870g7.A0O
            r1.AOK(r0, r2)
            return r5
        L_0x0eec:
            X.0zV r0 = r14.A00
            X.1vK r0 = (X.AnonymousClass1vK) r0
            X.DGF r4 = r0.A01
            X.7KV r0 = r4.A04
            java.lang.String r2 = "migration_screen_accept"
            X.14b r1 = r0.A00
            X.0gA r0 = X.C08870g7.A0O
            r1.AOI(r0, r2)
            X.BdN r3 = r4.A02
            X.0Tq r0 = r3.A01
            java.lang.Object r2 = r0.get()
            com.facebook.user.model.User r2 = (com.facebook.user.model.User) r2
            if (r2 == 0) goto L_0x0f2c
            X.3km r0 = r3.A00
            X.14b r1 = r0.A00
            X.0gA r0 = X.C08870g7.A0N
            r1.CH3(r0)
            java.lang.String r1 = r2.A0j
            com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000 r2 = new com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000
            r0 = 34
            r2.<init>(r0)
            r2.A0B(r1)
            X.3vz r1 = new X.3vz
            r1.<init>()
            java.lang.String r0 = "input"
            r1.A04(r0, r2)
            r0 = 0
            X.C23364BdN.A01(r3, r1, r0)
        L_0x0f2c:
            X.DGI r2 = r4.A05
            X.DGP r1 = r2.A01()
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES
            r1.A01 = r0
            X.DGW r0 = r1.A00()
            r2.A02(r0)
            return r5
        L_0x0f3e:
            int r5 = r14.A01
            r0 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            r4 = 2
            r3 = 1
            r1 = 0
            r2 = 0
            if (r5 == r0) goto L_0x0faa
            r0 = -1048037474(0xffffffffc188379e, float:-17.027157)
            if (r5 == r0) goto L_0x0f9e
            r0 = 71235917(0x43ef94d, float:2.2448866E-36)
            if (r5 != r0) goto L_0x0f6d
            java.lang.Object[] r0 = r14.A02
            r1 = r0[r3]
            com.facebook.messaging.inbox2.items.InboxUnitItem r1 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r1
            r0 = r0[r4]
            X.1BC r0 = (X.AnonymousClass1BC) r0
            boolean r0 = r0.Bc6(r1)
        L_0x0f61:
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r0)
            return r2
        L_0x0f66:
            int r0 = r14.A01
            r1 = 0
            r2 = 0
            switch(r0) {
                case -1351902487: goto L_0x0f7b;
                case -1048037474: goto L_0x0f9e;
                case -1034712717: goto L_0x0f6e;
                case 411929216: goto L_0x0f86;
                case 1122643471: goto L_0x0f92;
                default: goto L_0x0f6d;
            }
        L_0x0f6d:
            return r2
        L_0x0f6e:
            X.0zV r0 = r14.A00
            X.1IZ r0 = (X.AnonymousClass1IZ) r0
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r1 = r0.A01
            X.1BG r0 = r0.A04
            boolean r0 = r0.Bc6(r1)
            goto L_0x0f61
        L_0x0f7b:
            java.lang.Object[] r1 = r14.A02
            r0 = 1
            r0 = r1[r0]
            X.1NF r0 = (X.AnonymousClass1NF) r0
            r0.A00()
            return r2
        L_0x0f86:
            X.0zV r0 = r14.A00
            X.1IZ r0 = (X.AnonymousClass1IZ) r0
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r1 = r0.A01
            X.1BG r0 = r0.A04
            r0.BcG(r1)
            return r2
        L_0x0f92:
            X.0zV r0 = r14.A00
            X.1IZ r0 = (X.AnonymousClass1IZ) r0
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r1 = r0.A01
            X.1BG r0 = r0.A04
            r0.Bbw(r1)
            return r2
        L_0x0f9e:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r1]
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r2
        L_0x0faa:
            java.lang.Object[] r0 = r14.A02
            r1 = r0[r3]
            com.facebook.messaging.inbox2.items.InboxUnitItem r1 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r1
            r0 = r0[r4]
            X.1BC r0 = (X.AnonymousClass1BC) r0
            r0.Bbw(r1)
            return r2
        L_0x0fb8:
            int r0 = r14.A01
            r1 = 0
            r3 = 0
            switch(r0) {
                case -1613216430: goto L_0x10cc;
                case -1048037474: goto L_0x116f;
                case -513663253: goto L_0x10bd;
                case 1157125299: goto L_0x109c;
                case 1856422461: goto L_0x0fc0;
                default: goto L_0x0fbf;
            }
        L_0x0fbf:
            return r3
        L_0x0fc0:
            X.0zV r0 = r14.A00
            X.1vG r0 = (X.AnonymousClass1vG) r0
            X.5jb r0 = r0.A01
            if (r0 == 0) goto L_0x1129
            com.facebook.quicksilver.QuicksilverActivity r4 = r0.A00
            int r2 = X.AnonymousClass1Y3.BGB
            X.0UN r1 = r4.A02
            r0 = 4
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.ChG r5 = (X.C25506ChG) r5
            android.view.LayoutInflater r2 = r4.getLayoutInflater()
            X.Ckk r0 = new X.Ckk
            r0.<init>(r4)
            r5.A03 = r0
            r1 = 2132411948(0x7f1a062c, float:2.0473316E38)
            r0 = 0
            android.view.View r4 = r2.inflate(r1, r3, r0)
            r0 = 2131296266(0x7f09000a, float:1.8210444E38)
            android.view.View r0 = X.C012809p.A01(r4, r0)
            com.facebook.widget.text.BetterTextView r0 = (com.facebook.widget.text.BetterTextView) r0
            r5.A04 = r0
            r0 = 2131296265(0x7f090009, float:1.8210442E38)
            android.view.View r0 = X.C012809p.A01(r4, r0)
            com.facebook.widget.text.BetterTextView r0 = (com.facebook.widget.text.BetterTextView) r0
            r5.A05 = r0
            r0 = 2131296264(0x7f090008, float:1.821044E38)
            android.view.View r0 = X.C012809p.A01(r4, r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = (com.facebook.drawee.fbpipeline.FbDraweeView) r0
            r5.A00 = r0
            int r7 = X.AnonymousClass1Y3.BB1
            X.0UN r0 = r5.A02
            r6 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r7, r0)
            X.Cdo r0 = (X.C25310Cdo) r0
            com.facebook.quicksilver.model.GameInformation r0 = r0.A04
            if (r0 == 0) goto L_0x1047
            com.facebook.widget.text.BetterTextView r1 = r5.A04
            java.lang.String r0 = r0.A0O
            r1.setText(r0)
            com.facebook.widget.text.BetterTextView r1 = r5.A05
            X.0UN r0 = r5.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r7, r0)
            X.Cdo r0 = (X.C25310Cdo) r0
            com.facebook.quicksilver.model.GameInformation r0 = r0.A04
            java.lang.String r0 = r0.A0T
            r1.setText(r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r2 = r5.A00
            X.0UN r0 = r5.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r7, r0)
            X.Cdo r0 = (X.C25310Cdo) r0
            com.facebook.quicksilver.model.GameInformation r0 = r0.A04
            java.lang.String r0 = r0.A0Q
            android.net.Uri r1 = android.net.Uri.parse(r0)
            com.facebook.common.callercontext.CallerContext r0 = X.C25506ChG.A08
            r2.A09(r1, r0)
        L_0x1047:
            r0 = 2131296263(0x7f090007, float:1.8210438E38)
            android.view.View r1 = X.C012809p.A01(r4, r0)
            android.widget.LinearLayout r1 = (android.widget.LinearLayout) r1
            r0 = 8
            r1.setVisibility(r0)
            r0 = 2131296269(0x7f09000d, float:1.821045E38)
            android.view.View r1 = X.C012809p.A01(r4, r0)
            android.widget.LinearLayout r1 = (android.widget.LinearLayout) r1
            X.ChJ r0 = new X.ChJ
            r0.<init>(r5)
            r1.setOnClickListener(r0)
            X.6Tz r1 = new X.6Tz
            android.content.Context r0 = r5.A06
            r1.<init>(r0)
            r5.A01 = r1
            r0 = 1
            r1.setCancelable(r0)
            X.6Tz r0 = r5.A01
            r0.setContentView(r4)
            X.6Tz r1 = r5.A01
            X.Cjv r0 = new X.Cjv
            r0.<init>(r5)
            r1.setOnDismissListener(r0)
            X.6Tz r0 = r5.A01
            android.view.Window r1 = r0.getWindow()
            r0 = 1024(0x400, float:1.435E-42)
            r1.addFlags(r0)
            X.6Tz r0 = r5.A01
            r0.show()
            X.Chu r0 = r5.A07
            X.14b r1 = r0.A00
            X.0gA r0 = X.C25545Chu.A01
            r1.CH1(r0)
            return r3
        L_0x109c:
            X.2Zq r15 = (X.C48132Zq) r15
            X.0zV r0 = r14.A00
            android.view.View r4 = r15.A00
            X.1vG r0 = (X.AnonymousClass1vG) r0
            X.5jb r0 = r0.A01
            if (r0 == 0) goto L_0x1129
            com.facebook.quicksilver.QuicksilverActivity r0 = r0.A00
            X.CeO r0 = r0.A05
            if (r0 == 0) goto L_0x1129
            int r2 = X.AnonymousClass1Y3.AcB
            X.0UN r1 = r0.A04
            r0 = 25
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.Cet r0 = (X.C25369Cet) r0
            r0.A02 = r4
            return r3
        L_0x10bd:
            X.0zV r0 = r14.A00
            X.1vG r0 = (X.AnonymousClass1vG) r0
            X.5jb r0 = r0.A01
            if (r0 == 0) goto L_0x1129
            com.facebook.quicksilver.QuicksilverActivity r1 = r0.A00
            r0 = 0
            r1.A1I(r0)
            return r3
        L_0x10cc:
            X.0zV r0 = r14.A00
            X.1vG r0 = (X.AnonymousClass1vG) r0
            X.5jb r0 = r0.A01
            if (r0 == 0) goto L_0x1129
            com.facebook.quicksilver.QuicksilverActivity r4 = r0.A00
            X.Cdo r0 = r4.A09
            com.facebook.quicksilver.model.GameInformation r0 = r0.A04
            com.facebook.graphql.enums.GraphQLGamesInstantPlaySupportedOrientation r1 = r0.A04
            com.facebook.graphql.enums.GraphQLGamesInstantPlaySupportedOrientation r0 = com.facebook.graphql.enums.GraphQLGamesInstantPlaySupportedOrientation.A02
            if (r1 != r0) goto L_0x1129
            int r2 = X.AnonymousClass1Y3.Aiq
            X.0UN r1 = r4.A02
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.CeD r1 = (X.C25332CeD) r1
            java.lang.String r0 = "share_menu_show"
            r1.A05(r0)
            X.CeO r1 = r4.A05
            if (r1 == 0) goto L_0x10f9
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            X.C25340CeO.A0C(r1, r0)
        L_0x10f9:
            X.Cdl r0 = new X.Cdl
            r0.<init>(r4)
            com.facebook.quicksilver.views.common.QuicksilverShareMenuDialogFragment r2 = new com.facebook.quicksilver.views.common.QuicksilverShareMenuDialogFragment
            r2.<init>()
            r2.A01 = r0
            X.0qW r1 = r4.B4m()     // Catch:{ IllegalArgumentException -> 0x110f }
            java.lang.String r0 = "quicksilver_menu_popover"
            r2.A25(r1, r0)     // Catch:{ IllegalArgumentException -> 0x110f }
            return r3
        L_0x110f:
            r2 = 5
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r4.A02
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "instant_game"
            java.lang.String r0 = "saveInstantceState has already been called"
            r2.CGS(r1, r0)
            return r3
        L_0x1122:
            int r0 = r14.A01
            r1 = 0
            r3 = 0
            switch(r0) {
                case -1733638550: goto L_0x1159;
                case -1048037474: goto L_0x116f;
                case -417937898: goto L_0x1145;
                case 677066169: goto L_0x112a;
                case 1998236502: goto L_0x1145;
                default: goto L_0x1129;
            }
        L_0x1129:
            return r3
        L_0x112a:
            X.Eip r15 = (X.C29799Eip) r15
            X.0zV r1 = r14.A00
            java.lang.String r2 = r15.A00
            X.1Km r1 = (X.C22241Km) r1
            X.9ja r0 = r1.A04
            X.9jd r1 = r1.A06
            r0.A00 = r2
            if (r1 == 0) goto L_0x1129
            com.facebook.messaging.neue.nux.phoneconfirmation.ConfirmPhoneFragment r0 = r1.A00
            com.facebook.messaging.neue.nux.phoneconfirmation.ConfirmPhoneFragment.A08(r0, r2)
            com.facebook.messaging.neue.nux.phoneconfirmation.ConfirmPhoneFragment r0 = r1.A00
            com.facebook.messaging.neue.nux.phoneconfirmation.ConfirmPhoneFragment.A09(r0, r2)
            return r3
        L_0x1145:
            X.0zV r0 = r14.A00
            X.1Km r0 = (X.C22241Km) r0
            X.9jd r0 = r0.A06
            if (r0 == 0) goto L_0x1129
            com.facebook.messaging.neue.nux.phoneconfirmation.ConfirmPhoneFragment r2 = r0.A00
            int r0 = r2.A00
            int r0 = r0 + 1
            r2.A00 = r0
            com.facebook.messaging.neue.nux.phoneconfirmation.ConfirmPhoneFragment.A04(r2)
            return r3
        L_0x1159:
            X.0zV r0 = r14.A00
            X.1Km r0 = (X.C22241Km) r0
            X.9jd r0 = r0.A06
            if (r0 == 0) goto L_0x1129
            com.facebook.messaging.neue.nux.phoneconfirmation.ConfirmPhoneFragment r2 = r0.A00
            r0 = 489(0x1e9, float:6.85E-43)
            java.lang.String r1 = X.AnonymousClass80H.$const$string(r0)
            java.lang.String r0 = "confirm_phone_go_back_to_request_code"
            r2.A2Y(r1, r0)
            return r3
        L_0x116f:
            java.lang.Object[] r0 = r14.A02
            r0 = r0[r1]
            X.0p4 r0 = (X.AnonymousClass0p4) r0
            X.5QC r15 = (X.AnonymousClass5QC) r15
            A0G(r0, r15)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.AXa(X.10N, java.lang.Object):java.lang.Object");
    }

    public C17780zS() {
        this.A00 = A0D(getClass());
    }

    public C17780zS(int i) {
        this.A00 = A0D(Integer.valueOf(i));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r3v21, types: [com.facebook.litho.ComponentBuilderCBuilderShape1_0S0300000, X.11F] */
    /* JADX WARN: Type inference failed for: r7v60, types: [android.text.SpannableStringBuilder, android.text.Spannable] */
    /* JADX WARN: Type inference failed for: r5v76, types: [com.facebook.litho.ComponentBuilderCBuilderShape2_0S0200000, X.11F] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:1012:0x2d4d  */
    /* JADX WARNING: Removed duplicated region for block: B:1029:0x2db6  */
    /* JADX WARNING: Removed duplicated region for block: B:1303:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:608:0x1d75 A[Catch:{ all -> 0x1dd1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:685:0x1f9f  */
    /* JADX WARNING: Removed duplicated region for block: B:687:0x1fa2  */
    /* JADX WARNING: Removed duplicated region for block: B:690:0x1faf  */
    /* JADX WARNING: Removed duplicated region for block: B:700:0x1fde  */
    public X.C17770zR A0O(X.AnonymousClass0p4 r42) {
        /*
            r41 = this;
            r2 = r41
            boolean r1 = r2 instanceof X.C21911Je
            r0 = r42
            if (r1 != 0) goto L_0x3799
            boolean r1 = r2 instanceof X.AnonymousClass1vG
            if (r1 != 0) goto L_0x36df
            boolean r1 = r2 instanceof X.AnonymousClass1vH
            if (r1 != 0) goto L_0x35d5
            boolean r1 = r2 instanceof X.C21611Ia
            if (r1 != 0) goto L_0x356e
            boolean r1 = r2 instanceof X.C17030yD
            if (r1 != 0) goto L_0x34e6
            boolean r1 = r2 instanceof X.AnonymousClass1L2
            if (r1 != 0) goto L_0x343b
            boolean r1 = r2 instanceof X.C32071l6
            if (r1 != 0) goto L_0x3356
            boolean r1 = r2 instanceof X.AnonymousClass1J5
            if (r1 != 0) goto L_0x328f
            boolean r1 = r2 instanceof X.AnonymousClass1J7
            if (r1 != 0) goto L_0x30f7
            boolean r1 = r2 instanceof X.AnonymousClass1J3
            if (r1 != 0) goto L_0x2e3d
            boolean r1 = r2 instanceof X.C24261Sx
            if (r1 != 0) goto L_0x2cf4
            boolean r1 = r2 instanceof X.AnonymousClass1IM
            if (r1 != 0) goto L_0x3095
            boolean r1 = r2 instanceof X.AnonymousClass1NP
            if (r1 != 0) goto L_0x2c93
            boolean r1 = r2 instanceof X.AnonymousClass1NO
            if (r1 != 0) goto L_0x2c0d
            boolean r1 = r2 instanceof X.AnonymousClass1QV
            if (r1 != 0) goto L_0x2b83
            boolean r1 = r2 instanceof X.AnonymousClass1vI
            if (r1 != 0) goto L_0x2b46
            boolean r1 = r2 instanceof X.AnonymousClass1vJ
            if (r1 != 0) goto L_0x29cf
            boolean r1 = r2 instanceof X.AnonymousClass1vK
            if (r1 != 0) goto L_0x289b
            boolean r1 = r2 instanceof X.AnonymousClass1QW
            if (r1 != 0) goto L_0x27dc
            boolean r1 = r2 instanceof X.AnonymousClass1M5
            if (r1 != 0) goto L_0x2795
            boolean r1 = r2 instanceof X.C23891Rj
            if (r1 != 0) goto L_0x274e
            boolean r1 = r2 instanceof X.AnonymousClass1vL
            if (r1 != 0) goto L_0x26d9
            boolean r1 = r2 instanceof X.AnonymousClass1NK
            if (r1 != 0) goto L_0x2692
            boolean r1 = r2 instanceof X.AnonymousClass1NL
            if (r1 != 0) goto L_0x2619
            boolean r1 = r2 instanceof X.AnonymousClass1M3
            if (r1 != 0) goto L_0x25a4
            boolean r1 = r2 instanceof X.AnonymousClass1M4
            if (r1 != 0) goto L_0x255d
            boolean r1 = r2 instanceof X.C22071Ju
            if (r1 != 0) goto L_0x24df
            boolean r1 = r2 instanceof X.C22041Jr
            if (r1 != 0) goto L_0x24a4
            boolean r1 = r2 instanceof X.AnonymousClass1J4
            if (r1 != 0) goto L_0x243b
            boolean r1 = r2 instanceof X.C32081l7
            if (r1 != 0) goto L_0x23e6
            boolean r1 = r2 instanceof X.AnonymousClass1vM
            if (r1 != 0) goto L_0x22d8
            boolean r1 = r2 instanceof X.C22101Jx
            if (r1 != 0) goto L_0x22bd
            boolean r1 = r2 instanceof X.C31961ks
            if (r1 != 0) goto L_0x2226
            boolean r1 = r2 instanceof X.AnonymousClass1RG
            if (r1 != 0) goto L_0x2124
            boolean r1 = r2 instanceof X.C22021Jp
            if (r1 != 0) goto L_0x2060
            boolean r1 = r2 instanceof X.AnonymousClass1RJ
            if (r1 != 0) goto L_0x1eaa
            boolean r1 = r2 instanceof X.C22161Kd
            if (r1 != 0) goto L_0x1e40
            boolean r1 = r2 instanceof X.AnonymousClass1RH
            if (r1 != 0) goto L_0x1c0a
            boolean r1 = r2 instanceof X.C21981Jl
            if (r1 != 0) goto L_0x1b7b
            boolean r1 = r2 instanceof X.C32091l8
            if (r1 != 0) goto L_0x1b32
            boolean r1 = r2 instanceof X.AnonymousClass1JF
            if (r1 != 0) goto L_0x19ef
            boolean r1 = r2 instanceof X.AnonymousClass1vN
            if (r1 != 0) goto L_0x19c8
            boolean r1 = r2 instanceof X.AnonymousClass1Rz
            if (r1 != 0) goto L_0x19a4
            boolean r1 = r2 instanceof X.AnonymousClass1IZ
            if (r1 != 0) goto L_0x17dd
            boolean r1 = r2 instanceof X.AnonymousClass1J6
            if (r1 != 0) goto L_0x1707
            boolean r1 = r2 instanceof X.AnonymousClass1J1
            if (r1 != 0) goto L_0x16da
            boolean r1 = r2 instanceof X.AnonymousClass1vO
            if (r1 != 0) goto L_0x15b7
            boolean r1 = r2 instanceof X.AnonymousClass1GN
            if (r1 != 0) goto L_0x1385
            boolean r1 = r2 instanceof X.C37301v3
            if (r1 != 0) goto L_0x1124
            boolean r1 = r2 instanceof X.C32101l9
            if (r1 != 0) goto L_0x0f54
            boolean r1 = r2 instanceof X.AnonymousClass1K6
            if (r1 != 0) goto L_0x0df6
            boolean r1 = r2 instanceof X.C22421Lg
            if (r1 != 0) goto L_0x0d23
            boolean r1 = r2 instanceof X.AnonymousClass1vP
            if (r1 != 0) goto L_0x0b7a
            boolean r1 = r2 instanceof X.AnonymousClass1vQ
            if (r1 != 0) goto L_0x0a0a
            boolean r1 = r2 instanceof X.AnonymousClass1v6
            if (r1 != 0) goto L_0x0813
            boolean r1 = r2 instanceof X.AnonymousClass1vR
            if (r1 != 0) goto L_0x06de
            boolean r1 = r2 instanceof X.AnonymousClass1vS
            if (r1 != 0) goto L_0x05ff
            boolean r1 = r2 instanceof X.AnonymousClass1vT
            if (r1 != 0) goto L_0x166b
            boolean r1 = r2 instanceof X.AnonymousClass1GJ
            if (r1 != 0) goto L_0x0505
            boolean r1 = r2 instanceof X.AnonymousClass1RW
            if (r1 != 0) goto L_0x2a55
            boolean r1 = r2 instanceof X.AnonymousClass1JJ
            if (r1 != 0) goto L_0x03a6
            boolean r1 = r2 instanceof X.C37341vU
            if (r1 != 0) goto L_0x0337
            boolean r1 = r2 instanceof X.C37351vV
            if (r1 != 0) goto L_0x0374
            boolean r1 = r2 instanceof X.C21641Id
            if (r1 != 0) goto L_0x0304
            boolean r1 = r2 instanceof X.AnonymousClass1II
            if (r1 != 0) goto L_0x02fd
            boolean r1 = r2 instanceof X.C21841Ix
            if (r1 != 0) goto L_0x02f9
            boolean r1 = r2 instanceof X.C16980y8
            if (r1 != 0) goto L_0x02f5
            boolean r1 = r2 instanceof X.AnonymousClass11D
            if (r1 != 0) goto L_0x02f1
            boolean r1 = r2 instanceof X.C22331Kx
            if (r1 != 0) goto L_0x01bd
            boolean r1 = r2 instanceof X.C21301Ga
            if (r1 != 0) goto L_0x0176
            boolean r1 = r2 instanceof X.C22291Kt
            if (r1 != 0) goto L_0x0151
            boolean r1 = r2 instanceof X.C33991oT
            if (r1 == 0) goto L_0x02fd
            r1 = r2
            X.1oT r1 = (X.C33991oT) r1
            int r6 = r1.A03
            int r5 = r1.A02
            int r4 = r1.A00
            int r3 = r1.A01
            android.graphics.drawable.GradientDrawable r2 = new android.graphics.drawable.GradientDrawable
            r2.<init>()
            r1 = 1
            r2.setShape(r1)
            r2.setColor(r5)
            r2.setSize(r6, r6)
            if (r3 <= 0) goto L_0x0145
            if (r4 == 0) goto L_0x0145
            r2.setStroke(r3, r4)
        L_0x0145:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r0 = X.AnonymousClass10H.A00(r0)
            r0.A3U(r2)
            X.10H r0 = r0.A33()
            return r0
        L_0x0151:
            r1 = r2
            X.1Kt r1 = (X.C22291Kt) r1
            int r4 = r1.A00
            android.graphics.drawable.Drawable r3 = r1.A01
            android.widget.ImageView$ScaleType r2 = r1.A02
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r1 = X.AnonymousClass10H.A00(r0)
            android.content.Context r0 = r0.A09
            android.content.res.Resources r0 = r0.getResources()
            android.graphics.drawable.Drawable r0 = X.AnonymousClass1MX.A01(r0, r3, r4)
            r1.A3U(r0)
            java.lang.Object r0 = r1.A02
            X.10H r0 = (X.AnonymousClass10H) r0
            r0.A01 = r2
            X.10H r0 = r1.A33()
            return r0
        L_0x0176:
            r1 = r2
            X.1Ga r1 = (X.C21301Ga) r1
            int r2 = r1.A01
            int r6 = r1.A00
            int r5 = r1.A02
            java.lang.String r1 = r1.A03
            r4 = 1
            if (r2 == r4) goto L_0x0185
            r4 = 0
        L_0x0185:
            if (r4 == 0) goto L_0x01b8
            X.1we r3 = X.AnonymousClass11D.A02(r0, r1)
        L_0x018b:
            r2 = 0
            r3.A1o(r2)
            X.0uO r1 = X.C14940uO.FLEX_START
            r3.A35(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r1 = X.C21641Id.A00(r0)
            r1.A3F(r6)
            r1.A1o(r2)
            r0 = 0
            if (r4 == 0) goto L_0x01a2
            r0 = r5
        L_0x01a2:
            r1.A2F(r0)
            if (r4 == 0) goto L_0x01a8
            r5 = 0
        L_0x01a8:
            r1.A27(r5)
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.A1n(r0)
            r3.A33(r1)
            X.0zR r0 = r3.A31()
            return r0
        L_0x01b8:
            X.1wd r3 = X.C16980y8.A02(r0, r1)
            goto L_0x018b
        L_0x01bd:
            r3 = r2
            X.1Kx r3 = (X.C22331Kx) r3
            java.lang.CharSequence r1 = r3.A0S
            r18 = r1
            android.text.TextUtils$TruncateAt r1 = r3.A0N
            r40 = r1
            int r12 = r3.A0E
            int r1 = r3.A0B
            r39 = r1
            int r11 = r3.A0D
            int r10 = r3.A0A
            int r9 = r3.A0F
            int r8 = r3.A0C
            float r7 = r3.A03
            float r6 = r3.A01
            float r5 = r3.A02
            int r4 = r3.A0G
            boolean r1 = r3.A0V
            r38 = r1
            int r1 = r3.A0H
            r37 = r1
            android.content.res.ColorStateList r1 = r3.A0K
            r36 = r1
            int r1 = r3.A09
            r35 = r1
            int r1 = r3.A06
            r34 = r1
            int r1 = r3.A0I
            r33 = r1
            float r1 = r3.A00
            r32 = r1
            float r1 = r3.A04
            r31 = r1
            int r1 = r3.A0J
            r30 = r1
            android.graphics.Typeface r1 = r3.A0L
            r29 = r1
            android.text.Layout$Alignment r1 = r3.A0M
            r28 = r1
            X.1JS r1 = r3.A0Q
            r27 = r1
            int r1 = r3.A05
            r26 = r1
            int r1 = r3.A07
            r25 = r1
            int r1 = r3.A08
            r24 = r1
            X.1Kv r1 = r3.A0O
            r23 = r1
            boolean r1 = r3.A0W
            r22 = r1
            X.0uM r1 = r3.A0R
            r21 = r1
            boolean r1 = r3.A0U
            r20 = r1
            boolean r1 = r3.A0T
            r19 = r1
            r2 = 0
            boolean r14 = r3.A0X
            int r15 = X.AnonymousClass1Y3.BBG
            X.0UN r13 = r3.A0P
            r1 = 0
            java.lang.Object r15 = X.AnonymousClass1XX.A02(r1, r15, r13)
            X.1Kz r15 = (X.C22351Kz) r15
            int r3 = X.AnonymousClass1Y3.AjT
            r1 = 1
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r1, r3, r13)
            X.1L1 r13 = (X.AnonymousClass1L1) r13
            r1 = r18
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r3 = X.C14910uL.A02(r0)
            if (r18 == 0) goto L_0x0258
            if (r14 == 0) goto L_0x0258
            if (r13 == 0) goto L_0x0258
            r13 = r15
            r15 = r33
            java.lang.CharSequence r1 = r13.BKR(r1, r15)
        L_0x0258:
            r3.A3j(r1)
            r1 = r40
            r3.A3X(r1)
            java.lang.Object r1 = r3.A00
            X.0uL r1 = (X.C14910uL) r1
            r0 = r22
            r1.A0l = r0
            r1.A0I = r12
            r0 = r39
            r3.A3I(r0)
            r1.A0H = r11
            r1.A0E = r10
            r1.A0J = r9
            r1.A0G = r8
            r1.A05 = r7
            r1.A03 = r6
            r1.A04 = r5
            r1.A0L = r4
            r0 = r38
            r3.A3o(r0)
            r0 = r37
            r3.A3K(r0)
            r0 = r36
            r1.A0P = r0
            r0 = r35
            r1.A0D = r0
            r0 = r34
            r1.A08 = r0
            r0 = -1
            r1.A0A = r0
            r1.A09 = r0
            r0 = r33
            r1.A0N = r0
            r0 = r32
            r1.A01 = r0
            r0 = r31
            r1.A06 = r0
            r0 = r30
            r3.A3R(r0)
            r0 = r29
            r3.A3T(r0)
            r0 = r28
            r3.A3W(r0)
            r0 = r27
            r1.A0X = r0
            r0 = r26
            r1.A07 = r0
            r0 = r25
            r1.A0B = r0
            r0 = r24
            r1.A0C = r0
            r0 = 0
            r1.A0i = r0
            r0 = r23
            r1.A0V = r0
            r0 = r22
            r1.A0l = r0
            r0 = r21
            r3.A3a(r0)
            r0 = 0
            r1.A00 = r0
            r1.A0Z = r2
            r0 = r20
            r3.A3n(r0)
            r1.A0a = r2
            r0 = r19
            r1.A0g = r0
            r1.A0W = r2
            r0 = 2139095039(0x7f7fffff, float:3.4028235E38)
            r1.A02 = r0
            X.0uL r0 = r3.A35()
            return r0
        L_0x02f1:
            r0 = r2
            X.11D r0 = (X.AnonymousClass11D) r0
            return r0
        L_0x02f5:
            r0 = r2
            X.0y8 r0 = (X.C16980y8) r0
            return r0
        L_0x02f9:
            r0 = r2
            X.1Ix r0 = (X.C21841Ix) r0
            return r0
        L_0x02fd:
            X.1we r0 = X.AnonymousClass11D.A00(r0)
            X.11D r0 = r0.A00
            return r0
        L_0x0304:
            r1 = r2
            X.1Id r1 = (X.C21641Id) r1
            int r3 = r1.A01
            float r2 = r1.A00
            r1 = 0
            int r1 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r1 < 0) goto L_0x031e
            r1 = 1065353216(0x3f800000, float:1.0)
            float r2 = java.lang.Math.min(r1, r2)
            r1 = 1132396544(0x437f0000, float:255.0)
            float r2 = r2 * r1
            int r1 = (int) r2
            int r3 = X.C15970wH.A03(r3, r1)
        L_0x031e:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.AnonymousClass10H.A00(r0)
            android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_XY
            java.lang.Object r0 = r2.A02
            X.10H r0 = (X.AnonymousClass10H) r0
            r0.A01 = r1
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r3)
            r2.A3U(r0)
            X.10H r0 = r2.A33()
            return r0
        L_0x0337:
            r1 = r2
            X.1vU r1 = (X.C37341vU) r1
            X.5oW r4 = r1.A00
            java.lang.CharSequence r3 = r1.A02
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r1.A01
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            r2.A3c(r1)
            X.10K r0 = r4.A0F()
            r2.A3f(r0)
            r2.A3l(r3)
            X.10G r1 = X.AnonymousClass10G.TOP
            X.1JM r0 = r4.A0D()
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2X(r1, r0)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            X.1JM r0 = r4.A0B()
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2X(r1, r0)
            X.10G r1 = X.AnonymousClass10G.A04
            X.1JM r0 = r4.A0C()
            goto L_0x0394
        L_0x0374:
            r1 = r2
            X.1vV r1 = (X.C37351vV) r1
            X.5oW r4 = r1.A00
            java.lang.CharSequence r3 = r1.A02
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r1.A01
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            r2.A3c(r1)
            X.10K r0 = r4.A0E()
            r2.A3f(r0)
            r2.A3l(r3)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            X.1JM r0 = r4.A09()
        L_0x0394:
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2X(r1, r0)
            android.text.Layout$Alignment r0 = android.text.Layout.Alignment.ALIGN_CENTER
            r2.A3V(r0)
            X.0yD r0 = r2.A39()
            return r0
        L_0x03a6:
            r3 = r2
            X.1JJ r3 = (X.AnonymousClass1JJ) r3
            boolean r6 = r3.A08
            X.5EU r1 = r3.A01
            X.2Nf r8 = r1.currentPasswordContainer
            X.2Nf r4 = r1.newPasswordContainer
            X.2Nf r5 = r1.retypedPasswordContainer
            boolean r2 = r1.saveButtonEnabled
            boolean r9 = r3.A07
            X.1we r1 = X.AnonymousClass11D.A00(r0)
            X.10G r7 = X.AnonymousClass10G.TOP
            X.5UN r3 = X.AnonymousClass5UN.A04
            int r3 = r3.B3A()
            float r3 = (float) r3
            r1.A2Z(r7, r3)
            X.10G r7 = X.AnonymousClass10G.A04
            X.5UN r3 = X.AnonymousClass5UN.A04
            int r3 = r3.B3A()
            float r3 = (float) r3
            r1.A2Z(r7, r3)
            if (r9 == 0) goto L_0x04fd
            X.1we r9 = X.AnonymousClass11D.A00(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r7 = X.C17030yD.A00(r0)
            r3 = 2131823454(0x7f110b5e, float:1.9279708E38)
            if (r6 == 0) goto L_0x03e5
            r3 = 2131834561(0x7f1136c1, float:1.9302236E38)
        L_0x03e5:
            r7.A3P(r3)
            X.5cp r3 = X.C114805cp.A01
            r7.A3f(r3)
            android.text.Layout$Alignment r3 = android.text.Layout.Alignment.ALIGN_CENTER
            r7.A3V(r3)
            X.0yD r3 = r7.A39()
            r9.A3A(r3)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r10 = X.C17030yD.A00(r0)
            X.10G r7 = X.AnonymousClass10G.TOP
            X.5UN r3 = X.AnonymousClass5UN.A02
            int r3 = r3.B3A()
            float r3 = (float) r3
            r10.A2X(r7, r3)
            X.10G r7 = X.AnonymousClass10G.BOTTOM
            X.5UN r3 = X.AnonymousClass5UN.A01
            int r3 = r3.B3A()
            float r3 = (float) r3
            r10.A2X(r7, r3)
            r3 = 2131830058(0x7f11252a, float:1.9293103E38)
            r10.A3P(r3)
            X.5cp r3 = X.C114805cp.A09
            r10.A3f(r3)
            android.text.Layout$Alignment r3 = android.text.Layout.Alignment.ALIGN_CENTER
            r10.A3V(r3)
            X.0yD r3 = r10.A39()
            r9.A3A(r3)
            X.11D r3 = r9.A00
        L_0x042e:
            r1.A34(r3)
            if (r6 == 0) goto L_0x04e1
            java.lang.Class<X.1JJ> r7 = X.AnonymousClass1JJ.class
            java.lang.Object[] r6 = new java.lang.Object[]{r0}
            r3 = -728244833(0xffffffffd497dd9f, float:-5.218066E12)
            X.10N r7 = A0E(r7, r0, r3, r6)
            r9 = 2131823517(0x7f110b9d, float:1.9279836E38)
            r10 = 0
            r11 = 0
            r6 = r0
            X.0zR r3 = X.AnonymousClass1JJ.A00(r6, r7, r8, r9, r10, r11)
            r1.A34(r3)
            java.lang.Class<X.1JJ> r7 = X.AnonymousClass1JJ.class
            java.lang.Object[] r6 = new java.lang.Object[]{r0}
            r3 = -272271848(0xffffffffefc57618, float:-1.2222263E29)
            X.10N r7 = A0E(r7, r0, r3, r6)
            r9 = 2131828852(0x7f112074, float:1.9290657E38)
            r6 = r0
            r8 = r4
            X.0zR r3 = X.AnonymousClass1JJ.A00(r6, r7, r8, r9, r10, r11)
            r1.A34(r3)
            java.lang.Class<X.1JJ> r6 = X.AnonymousClass1JJ.class
            java.lang.Object[] r4 = new java.lang.Object[]{r0}
            r3 = -356643487(0xffffffffeabe0d61, float:-1.1487954E26)
            X.10N r7 = A0E(r6, r0, r3, r4)
            r9 = 2131831223(0x7f1129b7, float:1.9295466E38)
            r11 = 1
            r6 = r0
            r8 = r5
            X.0zR r3 = X.AnonymousClass1JJ.A00(r6, r7, r8, r9, r10, r11)
            r1.A34(r3)
        L_0x0480:
            r4 = 1
            java.lang.String r3 = "text"
            java.lang.String[] r7 = new java.lang.String[]{r3}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r4)
            X.5dg r5 = new X.5dg
            r5.<init>()
            X.0z4 r8 = r0.A0B
            X.0zR r3 = r0.A04
            if (r3 == 0) goto L_0x049b
            java.lang.String r4 = r3.A06
            r5.A07 = r4
        L_0x049b:
            r6.clear()
            r3 = 2131830060(0x7f11252c, float:1.9293107E38)
            java.lang.String r3 = r8.A09(r3)
            r5.A00 = r3
            r3 = 0
            r6.set(r3)
            r5.A01 = r2
            X.10G r4 = X.AnonymousClass10G.TOP
            X.5UN r2 = X.AnonymousClass5UN.A03
            int r2 = r2.B3A()
            float r2 = (float) r2
            int r3 = r8.A00(r2)
            X.11G r2 = r5.A14()
            r2.BJx(r4, r3)
            java.lang.Class<X.1JJ> r4 = X.AnonymousClass1JJ.class
            java.lang.Object[] r3 = new java.lang.Object[]{r0}
            r2 = -1879460902(0xffffffff8ff9b3da, float:-2.4622572E-29)
            X.10N r2 = A0E(r4, r0, r2, r3)
            X.11G r0 = r5.A14()
            r0.A0G(r2)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r6, r7)
            r1.A34(r5)
            X.0zR r0 = r1.A31()
            return r0
        L_0x04e1:
            java.lang.Class<X.1JJ> r6 = X.AnonymousClass1JJ.class
            java.lang.Object[] r5 = new java.lang.Object[]{r0}
            r3 = -309973846(0xffffffffed862caa, float:-5.1906234E27)
            X.10N r6 = A0E(r6, r0, r3, r5)
            r8 = 2131828852(0x7f112074, float:1.9290657E38)
            r9 = 1
            r10 = 1
            r5 = r0
            r7 = r4
            X.0zR r3 = X.AnonymousClass1JJ.A00(r5, r6, r7, r8, r9, r10)
            r1.A34(r3)
            goto L_0x0480
        L_0x04fd:
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            X.11D r3 = r3.A00
            goto L_0x042e
        L_0x0505:
            r1 = r2
            X.1GJ r1 = (X.AnonymousClass1GJ) r1
            com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem r7 = r1.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A02
            boolean r5 = r1.A03
            java.lang.Integer r1 = r7.A0I()
            int r1 = r1.intValue()
            switch(r1) {
                case 0: goto L_0x052e;
                case 1: goto L_0x0519;
                case 2: goto L_0x0586;
                default: goto L_0x0519;
            }
        L_0x0519:
            r1 = 0
        L_0x051a:
            X.1wd r2 = X.C16980y8.A00(r0)
            r0 = 1113587712(0x42600000, float:56.0)
            r2.A1p(r0)
            r0 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r0)
            r2.A3A(r1)
            X.0y8 r0 = r2.A00
            return r0
        L_0x052e:
            X.1wd r4 = X.C16980y8.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r4.A2V(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r2 = X.C22291Kt.A00(r0)
            r1 = 2132346436(0x7f190644, float:2.0340442E38)
            r2.A36(r1)
            if (r5 == 0) goto L_0x0584
            int r1 = r6.AzK()
        L_0x0547:
            r2.A33(r1)
            r1 = 1103101952(0x41c00000, float:24.0)
            r2.A1p(r1)
            r2.A1z(r1)
            X.10G r1 = X.AnonymousClass10G.START
            r3 = 1105199104(0x41e00000, float:28.0)
            r2.A2X(r1, r3)
            r4.A39(r2)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            java.lang.String r1 = r7.A01
            r2.A3l(r1)
            if (r5 == 0) goto L_0x0581
            X.10J r1 = X.AnonymousClass10J.A01
        L_0x0569:
            X.10K r1 = (X.AnonymousClass10K) r1
            r2.A3f(r1)
            r2.A3c(r6)
            X.0uO r1 = X.C14940uO.CENTER
            r2.A2V(r1)
            X.10G r1 = X.AnonymousClass10G.START
            r2.A2X(r1, r3)
            r4.A39(r2)
            X.0y8 r1 = r4.A00
            goto L_0x051a
        L_0x0581:
            X.5cp r1 = X.C114805cp.A04
            goto L_0x0569
        L_0x0584:
            r1 = 0
            goto L_0x0547
        L_0x0586:
            X.1wd r4 = X.C16980y8.A00(r0)
            r3 = 1065353216(0x3f800000, float:1.0)
            r4.A1n(r3)
            X.0uO r1 = X.C14940uO.CENTER
            r4.A3D(r1)
            X.1wd r1 = X.C16980y8.A00(r0)
            r1.A1n(r3)
            r4.A39(r1)
            X.3D2 r5 = new X.3D2
            r5.<init>()
            X.0z4 r6 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x05ad
            java.lang.String r2 = r1.A06
            r5.A07 = r2
        L_0x05ad:
            X.0uO r2 = X.C14940uO.CENTER
            X.11G r1 = r5.A14()
            r1.ANz(r2)
            r1 = 1108344832(0x42100000, float:36.0)
            int r2 = r6.A00(r1)
            X.11G r1 = r5.A14()
            r1.CNK(r2)
            r1 = 1108344832(0x42100000, float:36.0)
            int r2 = r6.A00(r1)
            X.11G r1 = r5.A14()
            r1.BC2(r2)
            r4.A3A(r5)
            X.1wd r1 = X.C16980y8.A00(r0)
            r1.A1n(r3)
            r4.A39(r1)
            java.lang.Class<X.1GJ> r3 = X.AnonymousClass1GJ.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 1489149912(0x58c29fd8, float:1.71193424E15)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r4.A2U(r1)
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 1378299283(0x52272d93, float:1.79506037E11)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r4.A2Q(r1)
            X.0y8 r1 = r4.A00
            goto L_0x051a
        L_0x05ff:
            r1 = r2
            X.1vS r1 = (X.AnonymousClass1vS) r1
            r14 = 0
            com.facebook.mig.scheme.interfaces.MigColorScheme r4 = r1.A06
            X.9bN r7 = r1.A05
            android.preference.Preference r8 = r1.A03
            boolean r13 = r1.A07
            android.preference.Preference r12 = r1.A01
            android.preference.Preference r11 = r1.A02
            boolean r10 = r1.A08
            long r5 = r1.A00
            int r2 = X.AnonymousClass1Y3.Axb
            X.0UN r3 = r1.A04
            r1 = 0
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.7LM r9 = (X.AnonymousClass7LM) r9
            int r2 = X.AnonymousClass1Y3.Ae6
            r1 = 1
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.61q r3 = (X.C1287261q) r3
            X.7Kf r2 = r9.A01(r0, r4)
            java.lang.CharSequence r9 = r8.getTitle()
            java.lang.CharSequence r8 = r8.getSummary()
            X.9bO r1 = new X.9bO
            r1.<init>(r7)
            r2.A0C(r9, r8, r13, r1)
            r2.A02()
            r1 = 2131827112(0x7f1119a8, float:1.9287127E38)
            r2.A03(r1)
            java.lang.CharSequence r9 = r12.getTitle()
            java.lang.CharSequence r8 = r12.getSummary()
            X.9bS r1 = new X.9bS
            r1.<init>(r7)
            r2.A0B(r9, r8, r1)
            if (r11 == 0) goto L_0x0666
            java.lang.CharSequence r9 = r11.getTitle()
            java.lang.CharSequence r8 = r11.getSummary()
            X.9bT r1 = new X.9bT
            r1.<init>(r7)
            r2.A0B(r9, r8, r1)
        L_0x0666:
            if (r10 == 0) goto L_0x16d5
            r2.A02()
            r1 = 2131826962(0x7f111912, float:1.9286823E38)
            r2.A03(r1)
            X.2yK r12 = X.C61092yK.A02
            r8 = 2131833425(0x7f113251, float:1.9299932E38)
            java.lang.String r1 = java.lang.String.valueOf(r5)
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r10 = r0.A0D(r8, r1)
            r1 = 2131822308(0x7f1106e4, float:1.9277384E38)
            java.lang.String r11 = r0.A0C(r1)
            r3.A01 = r4
            r1 = 2131822531(0x7f1107c3, float:1.9277836E38)
            java.lang.String r1 = r0.A0C(r1)
            r3.A03 = r1
            r1 = 2131822531(0x7f1107c3, float:1.9277836E38)
            java.lang.String r1 = r0.A0C(r1)
            r3.A02 = r1
            X.9bM r1 = new X.9bM
            r1.<init>(r7)
            r3.A00 = r1
            X.2sA r15 = r3.A01()
            X.7L7 r9 = new X.7L7
            r13 = 1
            r16 = r14
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            r2.A05(r9)
            X.5zo r3 = new X.5zo
            r3.<init>()
            r3.A03 = r4
            X.10J r1 = X.AnonymousClass10J.A0B
            r3.A04 = r1
            r1 = 2131822532(0x7f1107c4, float:1.9277838E38)
            java.lang.String r0 = r0.A0C(r1)
            r3.A05 = r0
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r0 = r1.B3A()
            r3.A02 = r0
            int r0 = r1.B3A()
            r3.A00 = r0
            X.2rz r0 = r3.A00()
            r2.A07(r0)
            goto L_0x16d5
        L_0x06de:
            X.1vR r2 = (X.AnonymousClass1vR) r2
            X.43z r1 = r2.A02
            r30 = r1
            com.facebook.ipc.stories.model.StoryBucket r9 = r2.A00
            com.facebook.ipc.stories.model.StoryCard r8 = r2.A01
            X.4O9 r1 = r2.A06
            r29 = r1
            X.43i r7 = r2.A05
            boolean r6 = r2.A08
            X.466 r1 = r2.A04
            r28 = r1
            X.463 r11 = r2.A07
            X.4Jw r10 = r2.A03
            boolean r5 = r2.A09
            com.google.common.collect.ImmutableList r12 = r8.A0T()
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            r1 = 1120403456(0x42c80000, float:100.0)
            r4.A20(r1)
            r4.A1q(r1)
            boolean r1 = X.AnonymousClass1vR.A00(r8)
            if (r1 == 0) goto L_0x07dc
            java.lang.Class<com.facebook.ipc.stories.model.StoryBucketLaunchConfig> r2 = com.facebook.ipc.stories.model.StoryBucketLaunchConfig.class
            r1 = r30
            java.lang.Object r3 = r1.A01(r2)
            com.facebook.ipc.stories.model.StoryBucketLaunchConfig r3 = (com.facebook.ipc.stories.model.StoryBucketLaunchConfig) r3
            X.1Xv r16 = r12.iterator()
        L_0x071e:
            boolean r1 = r16.hasNext()
            if (r1 == 0) goto L_0x07dc
            java.lang.Object r2 = r16.next()
            X.3gF r2 = (X.C73513gF) r2
            r1 = 11
            r12 = 397(0x18d, float:5.56E-43)
            java.lang.String r17 = X.C99084oO.$const$string(r12)
            java.lang.String r18 = "bucket"
            r12 = 419(0x1a3, float:5.87E-43)
            java.lang.String r19 = X.C99084oO.$const$string(r12)
            java.lang.String r20 = "delegate"
            java.lang.String r21 = "interruptManager"
            r12 = 548(0x224, float:7.68E-43)
            java.lang.String r22 = X.C99084oO.$const$string(r12)
            r12 = 556(0x22c, float:7.79E-43)
            java.lang.String r23 = X.C99084oO.$const$string(r12)
            java.lang.String r24 = "storyCard"
            r12 = 752(0x2f0, float:1.054E-42)
            java.lang.String r25 = X.C99084oO.$const$string(r12)
            r12 = 753(0x2f1, float:1.055E-42)
            java.lang.String r26 = X.C99084oO.$const$string(r12)
            r12 = 785(0x311, float:1.1E-42)
            java.lang.String r27 = X.C99084oO.$const$string(r12)
            java.lang.String[] r13 = new java.lang.String[]{r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27}
            java.util.BitSet r12 = new java.util.BitSet
            r12.<init>(r1)
            X.4Sf r14 = new X.4Sf
            android.content.Context r1 = r0.A09
            r14.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0776
            java.lang.String r15 = r1.A06
            r14.A07 = r15
        L_0x0776:
            r12.clear()
            r1 = r30
            r14.A04 = r1
            r1 = 9
            r12.set(r1)
            r14.A02 = r9
            r1 = 1
            r12.set(r1)
            java.lang.String r1 = r3.A0J
            r14.A0A = r1
            r1 = 5
            r12.set(r1)
            java.lang.String r1 = r3.A0R
            r14.A0B = r1
            r1 = 10
            r12.set(r1)
            r14.A03 = r8
            r1 = 7
            r12.set(r1)
            r1 = r28
            r14.A06 = r1
            r1 = 3
            r12.set(r1)
            r14.A01 = r2
            r1 = 8
            r12.set(r1)
            r1 = r29
            r14.A08 = r1
            r1 = 0
            r12.set(r1)
            r14.A09 = r11
            r1 = 6
            r12.set(r1)
            r14.A05 = r10
            r1 = 2
            r12.set(r1)
            java.lang.Class<X.46A> r2 = X.AnonymousClass46A.class
            r1 = r30
            java.lang.Object r1 = r1.A01(r2)
            X.46A r1 = (X.AnonymousClass46A) r1
            r14.A07 = r1
            r1 = 4
            r12.set(r1)
            r1 = 11
            X.AnonymousClass11F.A0C(r1, r12, r13)
            r4.A3A(r14)
            goto L_0x071e
        L_0x07dc:
            r9 = 2
            java.lang.String r2 = "isCurrentlyConfirmingReply"
            java.lang.String r1 = "navigationDelegate"
            java.lang.String[] r8 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r9)
            X.5P5 r2 = new X.5P5
            android.content.Context r1 = r0.A09
            r2.<init>(r1)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x07f9
            java.lang.String r0 = r0.A06
            r2.A07 = r0
        L_0x07f9:
            r3.clear()
            r2.A03 = r5
            r2.A01 = r7
            r0 = 1
            r3.set(r0)
            r2.A02 = r6
            r0 = 0
            r3.set(r0)
            X.AnonymousClass11F.A0C(r9, r3, r8)
            r4.A3A(r2)
            X.11D r0 = r4.A00
            return r0
        L_0x0813:
            r3 = r2
            X.1v6 r3 = (X.AnonymousClass1v6) r3
            com.facebook.ipc.stories.model.StoryCard r1 = r3.A03
            r27 = r1
            X.3gF r1 = r3.A01
            r17 = r1
            X.4eN r1 = r3.A06
            r26 = r1
            X.4Jw r1 = r3.A04
            r16 = r1
            com.facebook.ipc.stories.model.StoryBucket r1 = r3.A02
            r30 = r1
            int r2 = X.AnonymousClass1Y3.AM1
            X.0UN r5 = r3.A00
            r1 = 7
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r5)
            X.4go r2 = (X.C95644go) r2
            int r4 = X.AnonymousClass1Y3.AmE
            r1 = 1
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r1, r4, r5)
            X.4eK r12 = (X.C94324eK) r12
            int r4 = X.AnonymousClass1Y3.AgK
            r1 = 0
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r1, r4, r5)
            X.06B r11 = (X.AnonymousClass06B) r11
            int r4 = X.AnonymousClass1Y3.BPQ
            r1 = 4
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r1, r4, r5)
            X.4MH r10 = (X.AnonymousClass4MH) r10
            int r4 = X.AnonymousClass1Y3.A0A
            r1 = 3
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r1, r4, r5)
            X.4eO r9 = (X.C94364eO) r9
            int r4 = X.AnonymousClass1Y3.A9P
            r1 = 6
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r4, r5)
            android.os.Vibrator r8 = (android.os.Vibrator) r8
            int r4 = X.AnonymousClass1Y3.AEY
            r1 = 2
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r4, r5)
            X.4Mz r7 = (X.C88964Mz) r7
            int r4 = X.AnonymousClass1Y3.BTk
            r1 = 5
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r4, r5)
            X.4dR r6 = (X.C93824dR) r6
            X.0Tq r1 = r3.A08
            r25 = r1
            X.5CG r1 = r3.A05
            int r1 = r1.affordanceAnimationState
            r24 = r1
            r23 = r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r27.A0J()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = X.C95184fz.A02(r1)
            r3 = 0
            if (r1 == 0) goto L_0x111d
            if (r17 == 0) goto L_0x111d
            r13 = r17
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r5 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r4 = 61559079(0x3ab5127, float:1.0069108E-36)
            r1 = 216831994(0xcec97fa, float:3.6453025E-31)
            com.facebook.graphservice.tree.TreeJNI r14 = r13.A0J(r4, r5, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r14 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r14
            if (r14 == 0) goto L_0x111d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = r13.Af1()
            if (r4 == 0) goto L_0x111d
            r1 = r27
            java.lang.String r15 = r1.getId()
            java.lang.Class<X.1v6> r13 = X.AnonymousClass1v6.class
            java.lang.Object[] r5 = new java.lang.Object[]{r0}
            r1 = 829436257(0x31703161, float:3.4952665E-9)
            X.10N r5 = A0E(r13, r0, r1, r5)
            java.lang.String r1 = "StoryViewerReactionStickerContainerComponentSpec"
            r18 = r16
            r19 = r15
            r20 = r1
            r21 = r3
            r22 = r5
            r18.CIQ(r19, r20, r21, r22)
            java.util.HashMap r13 = new java.util.HashMap
            r13.<init>()
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r5 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r3 = 1445967214(0x562fb56e, float:4.8298442E13)
            r1 = -1876259565(0xffffffff902a8d13, float:-3.3635268E-29)
            com.facebook.graphservice.tree.TreeJNI r14 = r14.A0J(r3, r5, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r14 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r14
            r3 = 1616278368(0x60567360, float:6.18112E19)
            r1 = -1101511134(0xffffffffbe584622, float:-0.21120504)
            com.google.common.collect.ImmutableList r1 = r14.A0M(r3, r5, r1)
            X.1Xv r16 = r1.iterator()
        L_0x08e8:
            boolean r1 = r16.hasNext()
            if (r1 == 0) goto L_0x090f
            java.lang.Object r15 = r16.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r15 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r15
            com.facebook.graphql.enums.GraphQLInspirationsAnimationAssetType r3 = com.facebook.graphql.enums.GraphQLInspirationsAnimationAssetType.A03
            r1 = 3575610(0x368f3a, float:5.010497E-39)
            java.lang.Enum r14 = r15.A0O(r1, r3)
            com.facebook.graphql.enums.GraphQLInspirationsAnimationAssetType r14 = (com.facebook.graphql.enums.GraphQLInspirationsAnimationAssetType) r14
            r3 = 93121264(0x58ceaf0, float:1.3251839E-35)
            r1 = 1919293208(0x72661718, float:4.557406E30)
            com.facebook.graphservice.tree.TreeJNI r1 = r15.A0J(r3, r5, r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r13.put(r14, r1)
            goto L_0x08e8
        L_0x090f:
            X.4Q5 r5 = new X.4Q5
            r1 = r27
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r1.A0J()
            int r3 = X.C95184fz.A01(r1)
            r1 = r27
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r1.A0J()
            int r1 = X.C95184fz.A00(r1)
            r5.<init>(r3, r1)
            android.content.res.Resources r3 = r23.A03()
            r1 = 2132148265(0x7f160029, float:1.9938503E38)
            int r1 = r3.getDimensionPixelSize(r1)
            int r1 = r1 << 1
            X.4gq r15 = r2.A07(r5, r4, r1)
            r3 = 4
            java.lang.String r5 = "affordanceAnimationState"
            java.lang.String r4 = "affordanceAsset"
            java.lang.String r2 = "listener"
            r1 = 652(0x28c, float:9.14E-43)
            java.lang.String r1 = X.C99084oO.$const$string(r1)
            java.lang.String[] r5 = new java.lang.String[]{r5, r4, r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r3)
            X.1vC r3 = new X.1vC
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x095e
            java.lang.String r2 = r1.A06
            r3.A07 = r2
        L_0x095e:
            r4.clear()
            r2 = 0
            X.11G r1 = r3.A14()
            r1.A0V(r2)
            X.1LC r2 = X.AnonymousClass1LC.A00
            X.11G r1 = r3.A14()
            r1.BxJ(r2)
            android.graphics.RectF r1 = r15.A01
            float r1 = r1.width()
            int r2 = (int) r1
            X.11G r1 = r3.A14()
            r1.CNK(r2)
            android.graphics.RectF r1 = r15.A01
            float r1 = r1.height()
            int r2 = (int) r1
            X.11G r1 = r3.A14()
            r1.BC2(r2)
            double r1 = r15.A00
            float r14 = (float) r1
            X.11G r1 = r3.A14()
            r1.A05(r14)
            X.10G r14 = X.AnonymousClass10G.TOP
            android.graphics.RectF r1 = r15.A01
            float r1 = r1.top
            int r2 = (int) r1
            X.11G r1 = r3.A14()
            r1.BxI(r14, r2)
            X.10G r14 = X.AnonymousClass10G.START
            android.graphics.RectF r1 = r15.A01
            float r1 = r1.left
            int r2 = (int) r1
            X.11G r1 = r3.A14()
            r1.BxI(r14, r2)
            com.facebook.graphql.enums.GraphQLInspirationsAnimationAssetType r1 = com.facebook.graphql.enums.GraphQLInspirationsAnimationAssetType.A01
            java.lang.Object r1 = r13.get(r1)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r3.A03 = r1
            r1 = 1
            r4.set(r1)
            r1 = r24
            r3.A00 = r1
            r1 = 0
            r4.set(r1)
            X.44o r1 = new X.44o
            r1.<init>(r0)
            r3.A05 = r1
            r1 = 2
            r4.set(r1)
            java.lang.String r2 = "sticker_affordance"
            X.11G r1 = r3.A14()
            r1.A0S(r2)
            X.4eJ r1 = new X.4eJ
            r18 = r1
            r19 = r11
            r20 = r26
            r21 = r0
            r22 = r27
            r23 = r17
            r24 = r25
            r25 = r12
            r26 = r13
            r27 = r9
            r28 = r7
            r29 = r6
            r31 = r8
            r32 = r10
            r18.<init>(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)
            r3.A01 = r1
            r0 = 3
            r4.set(r0)
            r0 = 4
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x0a0a:
            r1 = r2
            X.1vQ r1 = (X.AnonymousClass1vQ) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r13 = r1.A03
            X.4px r5 = r1.A02
            boolean r9 = r1.A06
            java.lang.String r4 = r1.A04
            boolean r2 = r1.A05
            int r8 = r1.A00
            int r3 = X.AnonymousClass1Y3.A1G
            X.0UN r10 = r1.A01
            r1 = 0
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r1, r3, r10)
            X.0sy r11 = (X.C14290sy) r11
            int r3 = X.AnonymousClass1Y3.BRG
            r1 = 2
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r3, r10)
            X.0yl r6 = (X.C17350yl) r6
            int r3 = X.AnonymousClass1Y3.A3P
            r1 = 3
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r3, r10)
            X.2UG r7 = (X.AnonymousClass2UG) r7
            int r3 = X.AnonymousClass1Y3.Axb
            r1 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r10)
            X.7LM r1 = (X.AnonymousClass7LM) r1
            X.7Kf r3 = r1.A01(r0, r13)
            boolean r1 = r6.A05()
            if (r1 == 0) goto L_0x0b45
            r10 = 2131828714(0x7f111fea, float:1.9290377E38)
            android.content.Context r1 = r0.A02()
            java.lang.String r1 = X.C121665oc.A06(r11, r1)
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r15 = r0.A0D(r10, r1)
        L_0x0a5c:
            X.C17190yT.A00()
            com.google.common.base.Preconditions.checkNotNull(r13)
            r1 = 2131828716(0x7f111fec, float:1.929038E38)
            java.lang.String r14 = r0.A0C(r1)
            X.2tU r10 = new X.2tU
            r11 = 0
            r10.<init>(r11, r13, r14, r15)
            r3.A07(r10)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            X.4qR r10 = new X.4qR
            r10.<init>(r5)
            if (r9 == 0) goto L_0x0a9e
            r1 = 2131828707(0x7f111fe3, float:1.9290362E38)
            java.lang.String r13 = r0.A0C(r1)
            r1 = 2131828710(0x7f111fe6, float:1.9290369E38)
            java.lang.String r9 = r0.A0C(r1)
            java.lang.String r1 = "PUBLIC"
            X.7LR r12 = new X.7LR
            r12.<init>(r13, r9, r1)
            java.lang.Object r9 = r12.A02
            java.lang.String r1 = "Tag for radio button group option cannot be null"
            com.google.common.base.Preconditions.checkNotNull(r9, r1)
            r11.add(r12)
        L_0x0a9e:
            boolean r1 = r6.A05()
            if (r1 != 0) goto L_0x0ac3
            r1 = 2131828703(0x7f111fdf, float:1.9290354E38)
            java.lang.String r13 = r0.A0C(r1)
            r1 = 2131828708(0x7f111fe4, float:1.9290364E38)
            java.lang.String r9 = r0.A0C(r1)
            java.lang.String r1 = "FRIENDS_AND_CONNECTIONS"
            X.7LR r12 = new X.7LR
            r12.<init>(r13, r9, r1)
            java.lang.Object r9 = r12.A02
            java.lang.String r1 = "Tag for radio button group option cannot be null"
            com.google.common.base.Preconditions.checkNotNull(r9, r1)
            r11.add(r12)
        L_0x0ac3:
            r1 = 2131828704(0x7f111fe0, float:1.9290356E38)
            java.lang.String r13 = r0.A0C(r1)
            r1 = 2131828709(0x7f111fe5, float:1.9290367E38)
            java.lang.String r9 = r0.A0C(r1)
            java.lang.String r1 = "FRIENDS"
            X.7LR r12 = new X.7LR
            r12.<init>(r13, r9, r1)
            java.lang.Object r9 = r12.A02
            java.lang.String r1 = "Tag for radio button group option cannot be null"
            com.google.common.base.Preconditions.checkNotNull(r9, r1)
            r11.add(r12)
            r1 = 2131828702(0x7f111fde, float:1.9290352E38)
            java.lang.String r9 = r0.A0C(r1)
            java.lang.String r1 = "CUSTOM"
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x0b3d
            boolean r1 = r7.A02()
            if (r1 == 0) goto L_0x0b3d
            android.content.res.Resources r12 = r0.A03()
            r7 = 2131689623(0x7f0f0097, float:1.9008267E38)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r7 = r12.getQuantityString(r7, r8, r1)
        L_0x0b0a:
            java.lang.String r1 = "CUSTOM"
            X.7LR r8 = new X.7LR
            r8.<init>(r9, r7, r1)
            java.lang.Object r7 = r8.A02
            java.lang.String r1 = "Tag for radio button group option cannot be null"
            com.google.common.base.Preconditions.checkNotNull(r7, r1)
            r11.add(r8)
            r1 = 120(0x78, float:1.68E-43)
            java.lang.String r1 = X.C99084oO.$const$string(r1)
            com.google.common.base.Preconditions.checkNotNull(r10, r1)
            java.util.Iterator r8 = r11.iterator()
        L_0x0b28:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x235b
            java.lang.Object r7 = r8.next()
            X.7LR r7 = (X.AnonymousClass7LR) r7
            java.lang.Object r1 = r7.A02
            boolean r1 = com.google.common.base.Objects.equal(r4, r1)
            r7.A03 = r1
            goto L_0x0b28
        L_0x0b3d:
            r1 = 2131828701(0x7f111fdd, float:1.929035E38)
            java.lang.String r7 = r0.A0C(r1)
            goto L_0x0b0a
        L_0x0b45:
            X.0A4 r11 = new X.0A4
            android.content.res.Resources r1 = r0.A03()
            r11.<init>(r1)
            r1 = 2131828712(0x7f111fe8, float:1.9290373E38)
            java.lang.String r1 = r0.A0C(r1)
            r11.A03(r1)
            java.lang.String r1 = " "
            r11.A03(r1)
            X.4q2 r10 = new X.4q2
            r10.<init>(r5, r13)
            r1 = 33
            r11.A04(r10, r1)
            r1 = 2131828713(0x7f111fe9, float:1.9290375E38)
            java.lang.String r1 = r0.A0C(r1)
            r11.A03(r1)
            r11.A01()
            android.text.SpannableString r15 = r11.A00()
            goto L_0x0a5c
        L_0x0b7a:
            r1 = r2
            X.1vP r1 = (X.AnonymousClass1vP) r1
            com.facebook.messaging.montage.forked.model.hcontroller.ControllerParams r9 = r1.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r8 = r1.A02
            int r2 = X.AnonymousClass1Y3.BCv
            X.0UN r3 = r1.A00
            r1 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.2zZ r7 = (X.C61832zZ) r7
            int r2 = X.AnonymousClass1Y3.BD6
            r1 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.4VQ r2 = (X.AnonymousClass4VQ) r2
            int r1 = X.AnonymousClass1Y3.AJa
            r10 = 1
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r10, r1, r3)
            X.1MT r6 = (X.AnonymousClass1MT) r6
            com.facebook.ipc.stories.model.StoryCard r5 = r9.A04
            if (r5 == 0) goto L_0x3797
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r4.A2V(r1)
            r4.A3D(r1)
            r1 = 1120403456(0x42c80000, float:100.0)
            r4.A20(r1)
            r4.A1q(r1)
            com.facebook.ipc.stories.model.StoryBackgroundInfo r1 = r5.A0N()
            android.graphics.drawable.Drawable r1 = X.C103234vt.A00(r1)
            r4.A2H(r1)
            r1 = 2131820929(0x7f110181, float:1.9274587E38)
            r4.A25(r1)
            X.50f r1 = r5.getMedia()
            if (r1 == 0) goto L_0x0d20
            java.lang.String r1 = r1.A07
            if (r1 == 0) goto L_0x0d20
            android.net.Uri r1 = android.net.Uri.parse(r1)
            X.1Pv r3 = X.C23521Pv.A00(r1)
            X.32H r1 = r2.A00
            r3.A09 = r1
            X.36u r2 = new X.36u
            r2.<init>()
            r2.A05(r10)
            r2.A06(r10)
            X.36v r1 = new X.36v
            r1.<init>(r2)
            r3.A02 = r1
            r3.A0E = r10
            X.1Q0 r3 = r3.A02()
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r2 = X.C78223oa.A00(r0)
            r1 = 1120403456(0x42c80000, float:100.0)
            r2.A20(r1)
            r2.A1q(r1)
            r7.A0A(r3)
            com.facebook.common.callercontext.CallerContext r1 = X.AnonymousClass1vP.A03
            r7.A0Q(r1)
            X.46Q r1 = new X.46Q
            r1.<init>(r9)
            r7.A08(r1)
            X.303 r1 = r7.A0F()
            r2.A3U(r1)
            X.3oa r1 = r2.A35()
        L_0x0c1c:
            r4.A3A(r1)
            X.4BB r1 = r5.A0Q()
            r3 = 0
            if (r1 == 0) goto L_0x0c7a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = r1.A0Q()
            if (r2 == 0) goto L_0x0c7a
            r1 = -1038316416(0xffffffffc21c8c80, float:-39.137207)
            java.lang.String r1 = r2.A0P(r1)
            if (r1 == 0) goto L_0x0c7a
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r7 = X.C17030yD.A00(r0)
            r7.A3l(r1)
            X.1LC r1 = X.AnonymousClass1LC.A00
            r7.A2k(r1)
            X.10G r2 = X.AnonymousClass10G.TOP
            r1 = 1112014848(0x42480000, float:50.0)
            r7.A2c(r2, r1)
            X.10G r2 = X.AnonymousClass10G.LEFT
            r1 = 2132148322(0x7f160062, float:1.9938619E38)
            r7.A2j(r2, r1)
            X.10G r2 = X.AnonymousClass10G.RIGHT
            r7.A2j(r2, r1)
            X.0uO r1 = X.C14940uO.CENTER
            r7.A2V(r1)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r7.A3V(r1)
            X.10J r1 = X.AnonymousClass10J.A01
            r7.A3f(r1)
            r3 = 1077936128(0x40400000, float:3.0)
            java.lang.Object r2 = r7.A02
            X.0yD r2 = (X.C17030yD) r2
            X.0z4 r1 = r7.A02
            int r1 = r1.A01(r3)
            float r1 = (float) r1
            r2.A00 = r1
            r7.A3c(r8)
            X.0yD r3 = r7.A39()
        L_0x0c7a:
            r4.A3A(r3)
            X.4BB r2 = r5.A0Q()
            r1 = 0
            if (r2 == 0) goto L_0x0d0a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = r2.A0Q()
            if (r3 == 0) goto L_0x0d0a
            r2 = 146300310(0x8b85d96, float:1.1096099E-33)
            java.lang.String r7 = r3.A0P(r2)
            if (r7 == 0) goto L_0x0d0a
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r3 = X.C22291Kt.A00(r0)
            X.1gj r2 = X.C29631gj.A0l
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            int r1 = r6.A03(r2, r1)
            r3.A36(r1)
            r1 = -1
            r3.A33(r1)
            java.lang.Object r1 = r3.A00
            X.1Kt r1 = (X.C22291Kt) r1
            r5.A3A(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            r2.A3l(r7)
            X.0uO r1 = X.C14940uO.CENTER
            r2.A2V(r1)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r2.A3V(r1)
            X.10J r1 = X.AnonymousClass10J.A0J
            r2.A3f(r1)
            r2.A3c(r8)
            X.0yD r1 = r2.A39()
            r5.A3A(r1)
            X.1LC r1 = X.AnonymousClass1LC.A00
            r5.A2k(r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            r1 = 2132148285(0x7f16003d, float:1.9938544E38)
            r5.A2j(r2, r1)
            X.10G r2 = X.AnonymousClass10G.LEFT
            r1 = 2132148302(0x7f16004e, float:1.9938578E38)
            r5.A2j(r2, r1)
            X.10G r2 = X.AnonymousClass10G.RIGHT
            r5.A2j(r2, r1)
            X.0uO r1 = X.C14940uO.CENTER
            r5.A3D(r1)
            r5.A2V(r1)
            X.0uP r1 = X.C14950uP.FLEX_END
            r5.A3E(r1)
            java.lang.Class<X.1vP> r3 = X.AnonymousClass1vP.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r5.A2y(r1)
            X.11D r1 = r5.A00
        L_0x0d0a:
            r4.A3A(r1)
            java.lang.Class<X.1vP> r3 = X.AnonymousClass1vP.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 1803022739(0x6b77f193, float:2.9974548E26)
            X.10N r0 = A0E(r3, r0, r1, r2)
            r4.A2U(r0)
            X.11D r0 = r4.A00
            return r0
        L_0x0d20:
            r1 = 0
            goto L_0x0c1c
        L_0x0d23:
            r1 = r2
            X.1Lg r1 = (X.C22421Lg) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r5 = r1.A01
            r8 = 0
            int r3 = X.AnonymousClass1Y3.AJa
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1MT r6 = (X.AnonymousClass1MT) r6
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            if (r8 == 0) goto L_0x0dc2
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r7 = X.AnonymousClass10H.A00(r0)
            android.graphics.drawable.ShapeDrawable r3 = new android.graphics.drawable.ShapeDrawable
            android.graphics.drawable.shapes.OvalShape r1 = new android.graphics.drawable.shapes.OvalShape
            r1.<init>()
            r3.<init>(r1)
            android.graphics.Paint r2 = r3.getPaint()
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r2.setColor(r1)
            r7.A3U(r3)
            r1 = 1107296256(0x42000000, float:32.0)
            r7.A1z(r1)
            r7.A1p(r1)
        L_0x0d5c:
            r4.A39(r7)
            if (r8 == 0) goto L_0x0d8b
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r2 = X.C22291Kt.A00(r0)
            X.1gj r1 = X.C29631gj.A1g
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            int r0 = r6.A03(r1, r0)
            r2.A36(r0)
            X.0uO r0 = X.C14940uO.CENTER
            r2.A2V(r0)
            int r0 = r5.B9u()
            r2.A33(r0)
            X.1LC r0 = X.AnonymousClass1LC.A00
            r2.A2k(r0)
        L_0x0d81:
            java.lang.Object r0 = r2.A00
            X.1Kt r0 = (X.C22291Kt) r0
            r4.A3A(r0)
            X.11D r0 = r4.A00
            return r0
        L_0x0d8b:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r2 = X.C22291Kt.A00(r0)
            X.1gj r1 = X.C29631gj.A1g
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            int r0 = r6.A03(r1, r0)
            r2.A36(r0)
            X.0uO r0 = X.C14940uO.CENTER
            r2.A2V(r0)
            int r0 = r5.AzK()
            r2.A33(r0)
            X.1LC r0 = X.AnonymousClass1LC.A00
            r2.A2k(r0)
            X.10G r1 = X.AnonymousClass10G.TOP
            r0 = 1092616192(0x41200000, float:10.0)
            r2.A2b(r1, r0)
            X.1LC r0 = X.AnonymousClass1LC.A00
            r2.A2k(r0)
            java.lang.String r0 = "compose_icon"
            r2.A2q(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r2.A2l(r0)
            goto L_0x0d81
        L_0x0dc2:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r7 = X.AnonymousClass10H.A00(r0)
            android.graphics.drawable.ShapeDrawable r3 = new android.graphics.drawable.ShapeDrawable
            android.graphics.drawable.shapes.OvalShape r1 = new android.graphics.drawable.shapes.OvalShape
            r1.<init>()
            r3.<init>(r1)
            android.graphics.Paint r2 = r3.getPaint()
            X.1GU r1 = X.AnonymousClass1I2.A00(r5)
            int r1 = r1.Adt()
            r2.setColor(r1)
            r7.A3U(r3)
            r1 = 1112539136(0x42500000, float:52.0)
            r7.A1z(r1)
            r7.A1p(r1)
            java.lang.String r1 = "grey_circle"
            r7.A2q(r1)
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r7.A2l(r1)
            goto L_0x0d5c
        L_0x0df6:
            X.1K6 r2 = (X.AnonymousClass1K6) r2
            com.google.common.collect.ImmutableList r1 = r2.A02
            X.0v9 r7 = r2.A01
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            com.google.common.collect.ImmutableList$Builder r6 = new com.google.common.collect.ImmutableList$Builder
            r6.<init>()
            X.1Xv r15 = r1.iterator()
        L_0x0e09:
            boolean r1 = r15.hasNext()
            if (r1 == 0) goto L_0x0f0a
            java.lang.Object r11 = r15.next()
            X.0vY r11 = (X.C15600vY) r11
            r8 = 4
            java.lang.String r5 = "badgeInfo"
            java.lang.String r3 = "colorScheme"
            java.lang.String r2 = "content"
            java.lang.String r1 = "size"
            java.lang.String[] r5 = new java.lang.String[]{r5, r3, r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r8)
            X.1GZ r2 = new X.1GZ
            r2.<init>()
            X.0z4 r8 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0e36
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0e36:
            r3.clear()
            r9 = 1
            java.lang.String r1 = "icon"
            java.lang.String[] r13 = new java.lang.String[]{r1}
            java.util.BitSet r12 = new java.util.BitSet
            r12.<init>(r9)
            X.1NP r10 = new X.1NP
            r10.<init>()
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0e54
            java.lang.String r14 = r1.A06
            r10.A07 = r14
        L_0x0e54:
            r12.clear()
            X.10Z r1 = r11.A02
            android.graphics.drawable.Drawable r1 = r1.A04
            r10.A05 = r1
            r1 = 0
            r12.set(r1)
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            int r1 = X.AnonymousClass1JG.A00(r1)
            float r1 = (float) r1
            int r1 = r9.A00(r1)
            r10.A04 = r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r7.A00
            int r1 = r1.AzK()
            r10.A01 = r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r7.A00
            int r1 = r1.AkX()
            r10.A00 = r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r7.A00
            android.content.res.ColorStateList r1 = r1.AfS()
            int r1 = X.AnonymousClass1KA.A00(r1)
            r10.A02 = r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r7.A00
            android.content.res.ColorStateList r1 = r1.AfS()
            int r1 = X.AnonymousClass1KA.A01(r1)
            r10.A03 = r1
            java.lang.String r9 = r11.A04
            X.11G r1 = r10.A14()
            r1.A0O(r9)
            java.lang.Class<X.1K6> r14 = X.AnonymousClass1K6.class
            java.lang.Object[] r9 = new java.lang.Object[]{r0, r11}
            r1 = -1306401257(0xffffffffb221e617, float:-9.42375E-9)
            X.10N r9 = A0E(r14, r0, r1, r9)
            X.11G r1 = r10.A14()
            r1.A0G(r9)
            java.lang.Object[] r9 = new java.lang.Object[]{r0, r11}
            r1 = -547357317(0xffffffffdf5ffd7b, float:-1.6140192E19)
            X.10N r9 = A0E(r14, r0, r1, r9)
            X.11G r1 = r10.A14()
            r1.A0J(r9)
            java.lang.String r9 = "navbutton_"
            java.lang.String r1 = r11.A05
            java.lang.String r9 = X.AnonymousClass08S.A0J(r9, r1)
            X.11G r1 = r10.A14()
            r1.A0S(r9)
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r12, r13)
            X.0zR r1 = r10.A16()
            r2.A01 = r1
            r1 = 2
            r3.set(r1)
            X.0vE r1 = r11.A01
            r2.A02 = r1
            r1 = 0
            r3.set(r1)
            r2.A03 = r7
            r1 = 1
            r3.set(r1)
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            int r1 = X.AnonymousClass1JG.A00(r1)
            float r1 = (float) r1
            int r1 = r8.A00(r1)
            r2.A00 = r1
            r1 = 3
            r3.set(r1)
            r1 = 4
            X.AnonymousClass11F.A0C(r1, r3, r5)
            r6.add(r2)
            goto L_0x0e09
        L_0x0f0a:
            com.google.common.collect.ImmutableList r6 = r6.build()
            X.1Jr r3 = new X.1Jr
            r3.<init>()
            X.0z4 r5 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x0f1d
            java.lang.String r1 = r0.A06
            r3.A07 = r1
        L_0x0f1d:
            r1 = 0
            X.11G r0 = r3.A14()
            r0.AaD(r1)
            X.10G r2 = X.AnonymousClass10G.LEFT
            r0 = 1098907648(0x41800000, float:16.0)
            int r1 = r5.A00(r0)
            X.11G r0 = r3.A14()
            r0.BwM(r2, r1)
            if (r6 == 0) goto L_0x0f40
            java.util.List r0 = r3.A01
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0f4e
            r3.A01 = r6
        L_0x0f40:
            r0 = 1098907648(0x41800000, float:16.0)
            int r0 = r5.A00(r0)
            r3.A00 = r0
            r4.A3A(r3)
            X.11D r0 = r4.A00
            return r0
        L_0x0f4e:
            java.util.List r0 = r3.A01
            r0.addAll(r6)
            goto L_0x0f40
        L_0x0f54:
            X.1l9 r2 = (X.C32101l9) r2
            java.lang.String r1 = r2.A05
            r20 = r1
            com.google.common.collect.ImmutableList r1 = r2.A04
            r19 = r1
            X.0vE r9 = r2.A02
            X.0v9 r5 = r2.A03
            boolean r4 = r2.A06
            int r3 = X.AnonymousClass1Y3.A20
            X.0UN r2 = r2.A00
            r1 = 5
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r1, r3, r2)
            com.facebook.user.model.UserKey r13 = (com.facebook.user.model.UserKey) r13
            int r1 = X.AnonymousClass1Y3.BIu
            r6 = 1
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r6, r1, r2)
            X.0ty r8 = (X.C14760ty) r8
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            X.1wd r2 = X.C16980y8.A00(r0)
            X.10G r7 = X.AnonymousClass10G.START
            int r1 = X.C32101l9.A07
            float r1 = (float) r1
            r2.A2Z(r7, r1)
            X.10G r7 = X.AnonymousClass10G.END
            int r1 = X.C32101l9.A07
            float r1 = (float) r1
            r2.A2Z(r7, r1)
            X.0uO r1 = X.C14940uO.CENTER
            r2.A3D(r1)
            r1 = 1114636288(0x42700000, float:60.0)
            r2.A1p(r1)
            r12 = 4
            java.lang.String r11 = "badgeInfo"
            java.lang.String r10 = "colorScheme"
            java.lang.String r7 = "content"
            java.lang.String r1 = "size"
            java.lang.String[] r17 = new java.lang.String[]{r11, r10, r7, r1}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r12)
            X.1GZ r16 = new X.1GZ
            r16.<init>()
            X.0z4 r1 = r0.A0B
            r18 = r1
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0fbf
            java.lang.String r10 = r1.A06
            r1 = r16
            r1.A07 = r10
        L_0x0fbf:
            r7.clear()
            X.1Jy r14 = X.C22111Jy.A04
            r15 = 2
            java.lang.String r10 = "profileConfig"
            java.lang.String r1 = "threadTileViewData"
            java.lang.String[] r12 = new java.lang.String[]{r10, r1}
            java.util.BitSet r11 = new java.util.BitSet
            r11.<init>(r15)
            X.1J5 r10 = new X.1J5
            r10.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0fdf
            java.lang.String r15 = r1.A06
            r10.A07 = r15
        L_0x0fdf:
            r11.clear()
            X.1J0 r1 = X.AnonymousClass1J0.A0B
            r10.A02 = r1
            r1 = 0
            r11.set(r1)
            X.1Gs r1 = r14.tileBadge
            if (r13 != 0) goto L_0x111e
            X.1H4 r1 = r8.A0A()
        L_0x0ff2:
            r10.A04 = r1
            r11.set(r6)
            r8 = 0
            X.11G r1 = r10.A14()
            r1.AaD(r8)
            r8 = 2131832919(0x7f113057, float:1.9298905E38)
            android.content.Context r1 = r0.A09
            android.content.res.Resources r1 = r1.getResources()
            java.lang.String r8 = r1.getString(r8)
            X.11G r1 = r10.A14()
            r1.A0O(r8)
            java.lang.Class<X.1l9> r13 = X.C32101l9.class
            java.lang.Object[] r8 = new java.lang.Object[]{r0}
            r1 = 1154961974(0x44d75236, float:1722.5691)
            X.10N r8 = A0E(r13, r0, r1, r8)
            X.11G r1 = r10.A14()
            r1.A0G(r8)
            java.lang.Object[] r8 = new java.lang.Object[]{r0}
            r1 = -301563750(0xffffffffee06809a, float:-1.0406615E28)
            X.10N r8 = A0E(r13, r0, r1, r8)
            X.11G r1 = r10.A14()
            r1.A0J(r8)
            java.lang.String r8 = "tabs-toolbar-profile-image"
            X.11G r1 = r10.A14()
            r1.A0S(r8)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r11, r12)
            X.0zR r8 = r10.A16()
            r1 = r16
            r1.A01 = r8
            r1 = 2
            r7.set(r1)
            r1 = r16
            r1.A02 = r9
            r1 = 0
            r7.set(r1)
            r1 = r16
            r1.A03 = r5
            r7.set(r6)
            java.lang.Integer r1 = X.C32101l9.A08
            int r8 = X.AnonymousClass10P.A00(r1)
            r1 = r18
            int r8 = r1.A03(r8)
            r1 = r16
            r1.A00 = r8
            r1 = 3
            r7.set(r1)
            X.10G r9 = X.AnonymousClass10G.END
            X.1JQ r1 = X.AnonymousClass1JQ.LARGE
            int r1 = r1.B3A()
            float r8 = (float) r1
            r1 = r18
            int r8 = r1.A00(r8)
            X.11G r1 = r16.A14()
            r1.BJx(r9, r8)
            r8 = 4
            r1 = r17
            X.AnonymousClass11F.A0C(r8, r7, r1)
            r1 = r16
            r2.A3A(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r7 = X.AnonymousClass1L2.A00(r0)
            java.lang.Object r1 = r7.A02
            X.1L2 r1 = (X.AnonymousClass1L2) r1
            r1.A03 = r6
            r1 = 1065353216(0x3f800000, float:1.0)
            r7.A1n(r1)
            r7.A1o(r1)
            r1 = r20
            r7.A3k(r1)
            X.10J r1 = r5.A01
            X.10M r1 = r1.B7S()
            r7.A3g(r1)
            X.10J r1 = r5.A01
            X.1I7 r1 = r1.mTextSize
            r7.A3e(r1)
            X.10J r6 = r5.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r5.A00
            X.0y3 r1 = r6.B5d(r1)
            r7.A3d(r1)
            X.1L2 r1 = r7.A38()
            r2.A3A(r1)
            r9 = 2
            java.lang.String r6 = "buttons"
            java.lang.String r1 = "colorScheme"
            java.lang.String[] r8 = new java.lang.String[]{r6, r1}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r9)
            X.1K6 r6 = new X.1K6
            android.content.Context r1 = r0.A09
            r6.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x10ec
            java.lang.String r1 = r1.A06
            r6.A07 = r1
        L_0x10ec:
            r7.clear()
            r1 = r19
            r6.A02 = r1
            r1 = 0
            r7.set(r1)
            r6.A01 = r5
            r1 = 1
            r7.set(r1)
            X.AnonymousClass11F.A0C(r9, r7, r8)
            r2.A3A(r6)
            X.0y8 r1 = r2.A00
            r3.A3A(r1)
            X.11D r3 = r3.A00
            if (r4 == 0) goto L_0x111d
            X.1we r1 = X.AnonymousClass11D.A00(r0)
            r1.A3A(r3)
            X.AnonymousClass1IA.A01(r0, r1, r9)
            java.lang.String r0 = "shadow_container"
            r1.A2o(r0)
            X.11D r3 = r1.A00
        L_0x111d:
            return r3
        L_0x111e:
            X.1H4 r1 = r8.A0K(r13, r1)
            goto L_0x0ff2
        L_0x1124:
            r1 = r2
            X.1v3 r1 = (X.C37301v3) r1
            android.graphics.drawable.Drawable r13 = r1.A02
            java.lang.String r12 = r1.A0A
            java.lang.CharSequence r11 = r1.A09
            X.2Fx r5 = r1.A05
            X.9gP r3 = r1.A06
            int r9 = r1.A00
            int r8 = r1.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r1.A08
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            int r1 = r7.B9m()
            r4.A23(r1)
            android.content.res.Resources r2 = r0.A03()
            r1 = 2131034117(0x7f050005, float:1.7678742E38)
            boolean r14 = r2.getBoolean(r1)
            X.1we r10 = X.AnonymousClass11D.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r10.A3D(r1)
            if (r14 == 0) goto L_0x117a
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r6 = X.AnonymousClass10H.A00(r0)
            android.widget.ImageView$ScaleType r2 = android.widget.ImageView.ScaleType.FIT_CENTER
            java.lang.Object r1 = r6.A02
            X.10H r1 = (X.AnonymousClass10H) r1
            r1.A01 = r2
            r1 = 1126825984(0x432a0000, float:170.0)
            r6.A1p(r1)
            X.10G r2 = X.AnonymousClass10G.TOP
            r1 = 1112539136(0x42500000, float:52.0)
            r6.A2X(r2, r1)
            r6.A3U(r13)
            X.10H r1 = r6.A33()
            r10.A3A(r1)
        L_0x117a:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r6 = X.C17030yD.A00(r0)
            r6.A3l(r12)
            r6.A3c(r7)
            X.10J r1 = X.AnonymousClass10J.A0E
            r6.A3f(r1)
            X.10G r2 = X.AnonymousClass10G.TOP
            int r1 = X.C37301v3.A0B
            float r1 = (float) r1
            r6.A2X(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            int r1 = X.C37301v3.A0C
            float r1 = (float) r1
            r6.A2X(r2, r1)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r6.A3V(r1)
            X.0yD r1 = r6.A39()
            r10.A3A(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r6 = X.C17030yD.A00(r0)
            r6.A3l(r11)
            r6.A3c(r7)
            X.10J r1 = X.AnonymousClass10J.A02
            r6.A3f(r1)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2X(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            int r1 = X.C37301v3.A0C
            float r1 = (float) r1
            r6.A2X(r2, r1)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r6.A3V(r1)
            X.0yD r1 = r6.A39()
            r10.A3A(r1)
            X.1we r6 = X.AnonymousClass11D.A00(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r2 = X.C112145Vo.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r2.A2V(r1)
            X.11D r1 = r10.A00
            r2.A3Y(r1)
            X.5Vo r1 = r2.A37()
            r6.A3A(r1)
            if (r14 == 0) goto L_0x1381
            X.0uP r1 = X.C14950uP.FLEX_START
        L_0x11f1:
            r6.A3E(r1)
            r1 = 1065353216(0x3f800000, float:1.0)
            r6.A1n(r1)
            X.11D r1 = r6.A00
            r4.A3A(r1)
            X.1wd r12 = X.C16980y8.A00(r0)
            X.10G r2 = X.AnonymousClass10G.A04
            int r1 = X.C37301v3.A0C
            float r1 = (float) r1
            r12.A2X(r2, r1)
            r6 = 0
            r1 = 1
            java.lang.String r14 = "stateContainer"
            java.lang.String[] r13 = new java.lang.String[]{r14}
            java.util.BitSet r11 = new java.util.BitSet
            r11.<init>(r1)
            X.10I r10 = new X.10I
            android.content.Context r1 = r0.A09
            r10.<init>(r1)
            X.0z4 r2 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1228
            java.lang.String r1 = r1.A06
            r10.A07 = r1
        L_0x1228:
            r11.clear()
            r10.A05 = r7
            r10.A04 = r3
            r11.set(r6)
            r1 = 2131829570(0x7f112342, float:1.9292113E38)
            java.lang.String r1 = r2.A09(r1)
            r10.A06 = r1
            r1 = 1
            r10.A09 = r1
            X.10G r6 = X.AnonymousClass10G.A09
            r1 = 1090519040(0x41000000, float:8.0)
            int r2 = r2.A00(r1)
            X.11G r1 = r10.A14()
            r1.BwM(r6, r2)
            java.lang.Class<X.1v3> r6 = X.C37301v3.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 1931733614(0x7323ea6e, float:1.2986743E31)
            X.10N r1 = A0E(r6, r0, r1, r2)
            r10.A02 = r1
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 96515278(0x5c0b4ce, float:1.812201E-35)
            X.10N r1 = A0E(r6, r0, r1, r2)
            r10.A03 = r1
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r11, r13)
            r12.A3A(r10)
            java.lang.String[] r11 = new java.lang.String[]{r14}
            java.util.BitSet r10 = new java.util.BitSet
            r10.<init>(r1)
            X.1JW r6 = new X.1JW
            r6.<init>()
            X.0z4 r13 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1288
            java.lang.String r2 = r1.A06
            r6.A07 = r2
        L_0x1288:
            r10.clear()
            r6.A02 = r7
            r6.A01 = r5
            r1 = 0
            r10.set(r1)
            r1 = 1116471296(0x428c0000, float:70.0)
            int r2 = r13.A00(r1)
            X.11G r1 = r6.A14()
            r1.CNK(r2)
            X.1LC r2 = X.AnonymousClass1LC.A00
            X.11G r1 = r6.A14()
            r1.BxJ(r2)
            X.10G r5 = X.AnonymousClass10G.START
            r1 = 0
            int r2 = r13.A00(r1)
            X.11G r1 = r6.A14()
            r1.BxI(r5, r2)
            X.10G r5 = X.AnonymousClass10G.A09
            r1 = 0
            int r2 = r13.A00(r1)
            X.11G r1 = r6.A14()
            r1.BxI(r5, r2)
            r1 = 1090519040(0x41000000, float:8.0)
            int r2 = r13.A00(r1)
            X.11G r1 = r6.A14()
            r1.BwM(r5, r2)
            java.lang.Class<X.1v3> r5 = X.C37301v3.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1975520952(0xffffffff8a3ff148, float:-9.2416954E-33)
            X.10N r1 = A0E(r5, r0, r1, r2)
            r6.A00 = r1
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r10, r11)
            r12.A3A(r6)
            X.0y8 r1 = r12.A00
            r4.A3A(r1)
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r2, r1)
            r1 = 0
            r5.A1o(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape1_0S0300000 r6 = X.C48092Zm.A00(r0)
            r6.A37(r9)
            java.lang.Object r1 = r6.A02
            X.2Zm r1 = (X.C48092Zm) r1
            r1.A01 = r7
            java.lang.String r1 = r3.A00
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            r2 = r1 ^ 1
            java.lang.Object r1 = r6.A02
            X.2Zm r1 = (X.C48092Zm) r1
            r1.A05 = r2
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2X(r2, r1)
            java.lang.Class<X.1v3> r3 = X.C37301v3.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -256462297(0xfffffffff0b6b227, float:-4.5233316E29)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r6.A2y(r1)
            X.2Zm r1 = r6.A35()
            r5.A3A(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r6 = X.C115165dU.A00(r0)
            r6.A3P(r8)
            java.lang.Object r1 = r6.A02
            X.5dU r1 = (X.C115165dU) r1
            r1.A00 = r7
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2X(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2X(r2, r1)
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1760520986(0xffffffff971094e6, float:-4.6716846E-25)
            X.10N r0 = A0E(r3, r0, r1, r2)
            r6.A2y(r0)
            X.5dU r0 = r6.A3A()
            r5.A3A(r0)
            X.11D r0 = r5.A00
            r4.A3A(r0)
            X.11D r0 = r4.A00
            return r0
        L_0x1381:
            X.0uP r1 = X.C14950uP.CENTER
            goto L_0x11f1
        L_0x1385:
            r5 = r2
            X.1GN r5 = (X.AnonymousClass1GN) r5
            X.1LB r4 = r5.A01
            X.1GU r3 = r5.A05
            X.1JH r8 = r5.A03
            r2 = 0
            java.lang.String r9 = r5.A06
            int r1 = X.AnonymousClass1Y3.AOz
            X.0UN r5 = r5.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r1, r5)
            X.1MX r7 = (X.AnonymousClass1MX) r7
            int r2 = X.AnonymousClass1Y3.AJa
            r1 = 1
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r2, r5)
            X.1MT r6 = (X.AnonymousClass1MT) r6
            int r2 = X.AnonymousClass1Y3.A3W
            r1 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r5)
            X.1CK r2 = (X.AnonymousClass1CK) r2
            r10 = 0
            if (r8 == 0) goto L_0x1568
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0200000 r5 = X.C124665tp.A00(r0)
            r1 = 1120403456(0x42c80000, float:100.0)
            r5.A20(r1)
            java.lang.String r1 = r8.B21()
            java.lang.Object r2 = r5.A01
            X.5tp r2 = (X.C124665tp) r2
            r2.A0R = r1
            java.lang.String r1 = r8.Ayw()
            r2.A0S = r1
            r1 = 16
            r2.A0J = r1
            X.0y3 r1 = r3.B5c()
            int r1 = r1.AhV()
            java.lang.Object r2 = r5.A01
            X.5tp r2 = (X.C124665tp) r2
            r2.A0I = r1
            r1 = 1
            r2.A0H = r1
            r1 = 311(0x137, float:4.36E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r1)
            r5.A2m(r1)
            r5.A23(r10)
            r2 = 1
            java.lang.Object r1 = r5.A01
            X.5tp r1 = (X.C124665tp) r1
            r1.A0U = r2
            r2 = 2131230911(0x7f0800bf, float:1.8077888E38)
            int r1 = r3.Ao0()
            android.graphics.drawable.Drawable r2 = r7.A04(r2, r1)
            java.lang.Object r1 = r5.A01
            X.5tp r1 = (X.C124665tp) r1
            r1.A0L = r2
            java.lang.Class<X.1GN> r7 = X.AnonymousClass1GN.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 378110312(0x16898168, float:2.221521E-25)
            X.10N r2 = A0E(r7, r0, r1, r2)
            java.lang.Object r1 = r5.A01
            X.5tp r1 = (X.C124665tp) r1
            r1.A0N = r2
            java.lang.String r1 = r8.Ayw()
            boolean r1 = X.C06850cB.A0B(r1)
            if (r1 == 0) goto L_0x155f
            r2 = 1
            java.lang.Object r1 = r5.A01
            X.5tp r1 = (X.C124665tp) r1
            r1.A0U = r2
        L_0x1426:
            X.0uP r9 = X.C14950uP.SPACE_BETWEEN
            X.1we r7 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r1 = (float) r1
            r7.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A09
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            r7.A2Z(r2, r1)
            X.1wd r2 = X.C16980y8.A00(r0)
            java.lang.Class<X.1GN> r11 = X.AnonymousClass1GN.class
            java.lang.Object[] r8 = new java.lang.Object[]{r0}
            r1 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            X.10N r1 = A0E(r11, r0, r1, r8)
            r2.A2y(r1)
            android.content.Context r8 = r0.A09
            r1 = 1101004800(0x41a00000, float:20.0)
            int r1 = X.C007106r.A00(r8, r1)
            float r11 = (float) r1
            int r8 = r3.Adt()
            int r1 = r3.Ayy()
            android.graphics.drawable.Drawable r1 = X.AnonymousClass1M2.A01(r11, r8, r1)
            r2.A2H(r1)
            X.10G r8 = X.AnonymousClass10G.A04
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r1 = (float) r1
            r2.A2Z(r8, r1)
            X.10G r8 = X.AnonymousClass10G.A09
            float r1 = (float) r10
            r2.A2Z(r8, r1)
            X.0uO r1 = X.C14940uO.CENTER
            r2.A3D(r1)
            r2.A3E(r9)
            X.1wd r8 = X.C16980y8.A00(r0)
            r8.A3D(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r10 = X.C22291Kt.A00(r0)
            r10.A2V(r1)
            X.1gj r9 = X.C29631gj.A1J
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            int r1 = r6.A03(r9, r1)
            r10.A36(r1)
            int r1 = r3.Ao0()
            r10.A33(r1)
            r1 = 1103101952(0x41c00000, float:24.0)
            r10.A1p(r1)
            r10.A1z(r1)
            java.lang.String r1 = "tooltip_anchor_key"
            r10.A2o(r1)
            r8.A39(r10)
            X.10G r6 = X.AnonymousClass10G.START
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r6, r1)
            r8.A39(r5)
            java.lang.String r1 = "row2_layout_key"
            r8.A2o(r1)
            r2.A39(r8)
            if (r4 != 0) goto L_0x14f9
            r5 = 0
        L_0x14d4:
            r2.A3A(r5)
            java.lang.String r1 = "row1_layout_key"
            r2.A2o(r1)
            X.0y8 r1 = r2.A00
            r7.A3A(r1)
            java.lang.Class<X.1GN> r3 = X.AnonymousClass1GN.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1505368922(0xffffffffa645e4a6, float:-6.865798E-16)
            X.10N r0 = A0E(r3, r0, r1, r2)
            r7.A2U(r0)
            java.lang.String r0 = "column_layout_key"
            r7.A2o(r0)
            X.11D r0 = r7.A00
            return r0
        L_0x14f9:
            r1 = 7
            java.lang.String r8 = "clickListener"
            java.lang.String r9 = "colorScheme"
            java.lang.String r10 = "isSelected"
            java.lang.String r11 = "selectedColor"
            java.lang.String r12 = "selectedContentDescriptionResId"
            java.lang.String r13 = "textResId"
            java.lang.String r14 = "unselectedContentDescriptionResId"
            java.lang.String[] r8 = new java.lang.String[]{r8, r9, r10, r11, r12, r13, r14}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r1)
            X.2by r5 = new X.2by
            android.content.Context r1 = r0.A09
            r5.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1520
            java.lang.String r1 = r1.A06
            r5.A07 = r1
        L_0x1520:
            r6.clear()
            int r1 = r4.A02
            r5.A02 = r1
            r1 = 5
            r6.set(r1)
            int r1 = r4.A01
            r5.A01 = r1
            r1 = 4
            r6.set(r1)
            int r1 = r4.A03
            r5.A03 = r1
            r1 = 6
            r6.set(r1)
            X.1Bb r1 = r4.A04
            r5.A06 = r1
            r1 = 0
            r6.set(r1)
            boolean r1 = r4.A05
            r5.A07 = r1
            r1 = 2
            r6.set(r1)
            int r1 = r4.A00
            r5.A00 = r1
            r1 = 3
            r6.set(r1)
            r5.A05 = r3
            r1 = 1
            r6.set(r1)
            r1 = 7
            X.AnonymousClass11F.A0C(r1, r6, r8)
            goto L_0x14d4
        L_0x155f:
            r2 = 1
            java.lang.Object r1 = r5.A01
            X.5tp r1 = (X.C124665tp) r1
            r1.A0T = r2
            goto L_0x1426
        L_0x1568:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r5 = X.AnonymousClass1L2.A00(r0)
            boolean r1 = X.C06850cB.A0B(r9)
            if (r1 == 0) goto L_0x1587
            java.lang.String r9 = r2.A04()
            boolean r1 = X.C06850cB.A0A(r9)
            if (r1 == 0) goto L_0x1587
            android.content.res.Resources r2 = r0.A03()
            r1 = 2131832625(0x7f112f31, float:1.929831E38)
            java.lang.String r9 = r2.getString(r1)
        L_0x1587:
            r5.A3k(r9)
            r2 = 1
            java.lang.Object r1 = r5.A02
            X.1L2 r1 = (X.AnonymousClass1L2) r1
            r1.A03 = r2
            X.0y3 r1 = r3.B5c()
            r5.A3d(r1)
            X.1JL r1 = r3.B5i()
            r5.A3e(r1)
            X.10M r1 = r3.B7S()
            r5.A3g(r1)
            java.lang.String r1 = "android.widget.Button"
            r5.A2n(r1)
            android.text.Layout$Alignment r2 = android.text.Layout.Alignment.ALIGN_CENTER
            java.lang.Object r1 = r5.A02
            X.1L2 r1 = (X.AnonymousClass1L2) r1
            r1.A04 = r2
            r10 = 10
            goto L_0x1426
        L_0x15b7:
            r1 = r2
            X.1vO r1 = (X.AnonymousClass1vO) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r3 = r1.A02
            boolean r10 = r1.A04
            X.Ajy r6 = r1.A03
            X.2oT r13 = r1.A01
            int r2 = X.AnonymousClass1Y3.Axb
            X.0UN r4 = r1.A00
            r1 = 1
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.7LM r8 = (X.AnonymousClass7LM) r8
            int r2 = X.AnonymousClass1Y3.AKX
            r1 = 2
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.6ih r9 = (X.C141526ih) r9
            int r2 = X.AnonymousClass1Y3.AdM
            r1 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.DGZ r7 = (X.DGZ) r7
            if (r10 == 0) goto L_0x1659
            android.content.Context r5 = r0.A09
            r4 = 0
            java.lang.Boolean r1 = r9.A00
            boolean r1 = r1.booleanValue()
            r2 = 2131821147(0x7f11025b, float:1.9275029E38)
            if (r1 == 0) goto L_0x15f2
            r2 = 2131835152(0x7f113910, float:1.9303434E38)
        L_0x15f2:
            int r1 = X.C121665oc.A01()
            java.lang.String r1 = r5.getString(r1)
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r1 = r5.getString(r2, r1)
            android.text.SpannableString r6 = X.C141526ih.A00(r5, r3, r1, r4, r6)
            X.7Kf r2 = r8.A01(r0, r3)
            boolean r1 = r7.A01()
            if (r1 == 0) goto L_0x162e
            r1 = 2131830677(0x7f112795, float:1.9294358E38)
            java.lang.String r1 = r0.A0C(r1)
            X.62A r7 = new X.62A
            r7.<init>()
            r7.A05 = r1
            int r1 = r1.hashCode()
            long r4 = (long) r1
            r7.A00 = r4
            r7.A02 = r3
            X.2yM r1 = r7.A00()
            r2.A07(r1)
        L_0x162e:
            java.lang.String r9 = ""
            r1 = 2131821145(0x7f110259, float:1.9275025E38)
            java.lang.String r8 = r0.A0C(r1)
            com.google.common.base.Preconditions.checkNotNull(r13)
            X.7LH r7 = new X.7LH
            r11 = 1
            r12 = 0
            r7.<init>(r8, r9, r10, r11, r12, r13)
            r2.A06(r7)
            X.5zo r1 = new X.5zo
            r1.<init>()
            r1.A05 = r6
            X.10J r0 = X.AnonymousClass10J.A0B
            r1.A04 = r0
            r1.A03 = r3
            X.2rz r0 = r1.A00()
            r2.A07(r0)
            goto L_0x16d5
        L_0x1659:
            android.content.Context r5 = r0.A09
            r4 = 0
            java.lang.Boolean r1 = r9.A00
            boolean r1 = r1.booleanValue()
            r2 = 2131821146(0x7f11025a, float:1.9275027E38)
            if (r1 == 0) goto L_0x15f2
            r2 = 2131835151(0x7f11390f, float:1.9303432E38)
            goto L_0x15f2
        L_0x166b:
            r1 = r2
            X.1vT r1 = (X.AnonymousClass1vT) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r14 = r1.A02
            java.lang.CharSequence r13 = r1.A05
            X.9bn r8 = r1.A01
            java.lang.CharSequence r11 = r1.A06
            boolean r10 = r1.A0A
            java.lang.CharSequence r9 = r1.A07
            boolean r7 = r1.A0B
            java.lang.CharSequence r6 = r1.A04
            boolean r5 = r1.A09
            java.lang.CharSequence r4 = r1.A03
            boolean r3 = r1.A08
            int r12 = X.AnonymousClass1Y3.Axb
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r12, r2)
            X.7LM r1 = (X.AnonymousClass7LM) r1
            X.7Kf r2 = r1.A01(r0, r14)
            X.5zo r12 = new X.5zo
            r12.<init>()
            r12.A03 = r14
            X.10J r0 = X.AnonymousClass10J.A0B
            r12.A04 = r0
            r12.A05 = r13
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r0 = r1.B3A()
            r12.A02 = r0
            int r0 = r1.B3A()
            r12.A00 = r0
            X.2rz r0 = r12.A00()
            r2.A07(r0)
            X.9bi r0 = new X.9bi
            r0.<init>(r8)
            r2.A0D(r11, r10, r0)
            X.9bh r0 = new X.9bh
            r0.<init>(r8)
            r2.A0D(r9, r7, r0)
            X.9bg r0 = new X.9bg
            r0.<init>(r8)
            r2.A0D(r6, r5, r0)
            X.9bf r0 = new X.9bf
            r0.<init>(r8)
            r2.A0D(r4, r3, r0)
        L_0x16d5:
            X.0zR r0 = r2.A01()
            return r0
        L_0x16da:
            X.1J1 r2 = (X.AnonymousClass1J1) r2
            X.0zR r1 = r2.A01
            com.facebook.messaging.model.threads.ThreadSummary r5 = r2.A02
            X.AnonymousClass064.A00(r5)
            java.lang.String r2 = r5.A0s
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r2)
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x1e19
            X.1wd r4 = X.C16980y8.A00(r0)
            r4.A3A(r1)
            java.lang.Class<X.1J1> r3 = X.AnonymousClass1J1.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0, r5}
            r1 = 904927013(0x35f01725, float:1.7888129E-6)
            X.10N r0 = A0E(r3, r0, r1, r2)
            r4.A2U(r0)
            X.0y8 r1 = r4.A00
            return r1
        L_0x1707:
            r1 = r2
            X.1J6 r1 = (X.AnonymousClass1J6) r1
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r5 = r1.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r3 = r1.A03
            int r4 = X.AnonymousClass1Y3.AJa
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.1MT r8 = (X.AnonymousClass1MT) r8
            X.1Qt r7 = r5.A04
            if (r7 == 0) goto L_0x17db
            boolean r1 = r7.A07
            if (r1 == 0) goto L_0x177e
            r2 = 1
            java.lang.String r1 = "text"
            java.lang.String[] r5 = new java.lang.String[]{r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r2)
            X.5de r6 = new X.5de
            r6.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x173a
            java.lang.String r2 = r1.A06
            r6.A07 = r2
        L_0x173a:
            r4.clear()
            java.lang.String r1 = r7.A05
            r6.A04 = r1
            r1 = 0
            r4.set(r1)
            X.1Qm r1 = r7.A01
            X.1gj r2 = r1.mM4IconGlyph
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            int r1 = r8.A03(r2, r1)
            r6.A01 = r1
            java.lang.String r2 = r7.A04
            X.11G r1 = r6.A14()
            r1.A0O(r2)
            r6.A02 = r3
            boolean r1 = r7.A06
            r6.A05 = r1
            java.lang.Class<X.1J6> r3 = X.AnonymousClass1J6.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            X.10N r1 = A0E(r3, r0, r1, r2)
            X.11G r0 = r6.A14()
            r0.A0G(r1)
            r0 = -10824391(0xffffffffff5ad539, float:-2.9087882E38)
            r6.A00 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r6
        L_0x177e:
            X.1Qm r8 = r7.A01
            if (r8 == 0) goto L_0x17db
            r6 = 2
            java.lang.String r2 = "iconName"
            java.lang.String r1 = "iconSize"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r6)
            X.1NO r6 = new X.1NO
            android.content.Context r1 = r0.A09
            r6.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x179f
            java.lang.String r2 = r1.A06
            r6.A07 = r2
        L_0x179f:
            r4.clear()
            X.1qL r1 = r8.mM4ButtonGlyph
            r6.A00 = r1
            r1 = 0
            r4.set(r1)
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r6.A03 = r1
            r1 = 1
            r4.set(r1)
            r6.A02 = r3
            java.lang.Class<X.1J6> r3 = X.AnonymousClass1J6.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            X.10N r1 = A0E(r3, r0, r1, r2)
            X.11G r0 = r6.A14()
            r0.A0G(r1)
            java.lang.String r1 = r7.A04
            if (r1 == 0) goto L_0x17d8
        L_0x17cc:
            X.11G r0 = r6.A14()
            r0.A0O(r1)
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r6
        L_0x17d8:
            java.lang.String r1 = r7.A05
            goto L_0x17cc
        L_0x17db:
            r6 = 0
            return r6
        L_0x17dd:
            r3 = r2
            X.1IZ r3 = (X.AnonymousClass1IZ) r3
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r9 = r3.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r8 = r3.A08
            X.1BG r7 = r3.A04
            X.1BN r14 = r3.A02
            X.1BO r2 = r3.A03
            X.1BE r6 = r3.A05
            X.1BY r5 = r3.A07
            X.1BK r1 = r3.A06
            r16 = r1
            X.0UN r4 = r3.A00
            int r3 = X.AnonymousClass1Y3.B9O
            r1 = 0
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r1, r3, r4)
            X.0u0 r13 = (X.AnonymousClass0u0) r13
            android.content.res.ColorStateList r1 = r8.B9n()
            int r4 = X.AnonymousClass1KA.A00(r1)
            android.content.res.ColorStateList r1 = r8.B9n()
            int r3 = X.AnonymousClass1KA.A01(r1)
            r1 = 0
            android.graphics.drawable.Drawable r11 = X.AnonymousClass1M2.A01(r1, r4, r3)
            r12 = 4
            java.lang.String r10 = "callToActionListener"
            java.lang.String r4 = "colorScheme"
            java.lang.String r3 = "item"
            java.lang.String r1 = "montageListener"
            java.lang.String[] r10 = new java.lang.String[]{r10, r4, r3, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r12)
            X.1Io r3 = new X.1Io
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r12 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1835
            java.lang.String r15 = r1.A06
            r3.A07 = r15
        L_0x1835:
            r4.clear()
            r3.A01 = r9
            r1 = 2
            r4.set(r1)
            r3.A04 = r8
            r1 = 1
            r4.set(r1)
            r3.A02 = r14
            r1 = 0
            r4.set(r1)
            r3.A03 = r2
            r1 = 3
            r4.set(r1)
            com.facebook.messaging.model.threads.ThreadSummary r2 = r9.A01
            boolean r1 = X.AnonymousClass1H9.A01(r2)
            if (r1 != 0) goto L_0x1930
            r1 = 1
        L_0x1859:
            if (r1 == 0) goto L_0x1957
            java.util.UUID r1 = X.C188215g.A00()
            java.lang.String r14 = r1.toString()
            r2 = 1
            r1 = 222(0xde, float:3.11E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r1)
            java.lang.String[] r13 = new java.lang.String[]{r1}
            java.util.BitSet r12 = new java.util.BitSet
            r12.<init>(r2)
            X.1Ju r2 = new X.1Ju
            r2.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1880
            java.lang.String r15 = r1.A06
            r2.A07 = r15
        L_0x1880:
            r12.clear()
            r1 = 4
            X.AnonymousClass11F.A0C(r1, r4, r10)
            X.0zR r1 = r3.A16()
            r2.A01 = r1
            r1 = 0
            r12.set(r1)
            r2.A00 = r11
            r2.A02 = r5
            r2.A05 = r14
            java.lang.Class<X.1IZ> r4 = X.AnonymousClass1IZ.class
            java.lang.Object[] r3 = new java.lang.Object[]{r0}
            r1 = 411929216(0x188d8a80, float:3.6587495E-24)
            X.10N r3 = A0E(r4, r0, r1, r3)
            X.11G r1 = r2.A14()
            r1.A0M(r3)
            X.1MC r3 = new X.1MC
            r1 = r16
            r3.<init>(r1, r9)
            r2.A04 = r3
            X.1ME r1 = new X.1ME
            r1.<init>(r5, r7, r9, r14)
            r2.A03 = r1
            com.facebook.messaging.model.threads.ThreadSummary r3 = r9.A01
            com.facebook.messaging.model.threadkey.ThreadKey r10 = r3.A0S
            com.facebook.messaging.model.threadkey.ThreadKey r1 = X.C34001oU.A00
            boolean r1 = r10.equals(r1)
            if (r1 == 0) goto L_0x18ce
            boolean r1 = r3.A0B()
            r7 = 0
            if (r1 == 0) goto L_0x18cf
        L_0x18ce:
            r7 = 1
        L_0x18cf:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = X.C34001oU.A00
            boolean r1 = r10.equals(r1)
            if (r1 != 0) goto L_0x18dc
            boolean r1 = r9.A0D
            r4 = 1
            if (r1 != 0) goto L_0x18dd
        L_0x18dc:
            r4 = 0
        L_0x18dd:
            boolean r3 = r9.A0C
            com.facebook.messaging.model.threadkey.ThreadKey r1 = X.C34001oU.A00
            boolean r1 = r10.equals(r1)
            r1 = r1 ^ 1
            X.1Gm[] r1 = X.C21351Gl.A00(r7, r4, r3, r1)
            com.google.common.collect.ImmutableList r1 = X.AnonymousClass1IZ.A01(r9, r6, r1, r5)
            com.google.common.collect.ImmutableList r1 = X.AnonymousClass1IZ.A00(r0, r8, r1)
            r2.A07 = r1
            boolean r10 = r9.A0I()
            com.facebook.messaging.model.threads.ThreadSummary r1 = r9.A01
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r1.A0S
            com.facebook.messaging.model.threadkey.ThreadKey r1 = X.C34001oU.A00
            boolean r1 = r3.equals(r1)
            if (r1 != 0) goto L_0x190a
            boolean r1 = r9.A09
            r7 = 1
            if (r1 != 0) goto L_0x190b
        L_0x190a:
            r7 = 0
        L_0x190b:
            com.facebook.messaging.model.threadkey.ThreadKey r1 = X.C34001oU.A00
            boolean r1 = r3.equals(r1)
            if (r1 != 0) goto L_0x1918
            boolean r1 = r9.A0H
            r4 = 1
            if (r1 != 0) goto L_0x1919
        L_0x1918:
            r4 = 0
        L_0x1919:
            boolean r3 = r9.A0E
            boolean r1 = r9.A0F
            X.1Gm[] r1 = X.C21351Gl.A01(r10, r7, r4, r3, r1)
            com.google.common.collect.ImmutableList r1 = X.AnonymousClass1IZ.A01(r9, r6, r1, r5)
            com.google.common.collect.ImmutableList r0 = X.AnonymousClass1IZ.A00(r0, r8, r1)
            r2.A06 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r12, r13)
            return r2
        L_0x1930:
            X.0l8 r2 = r2.A0O
            X.0l8 r1 = X.C10950l8.A0A
            if (r2 != r1) goto L_0x1943
            X.1Yd r13 = r13.A00
            r1 = 284842231075866(0x103100000141a, double:1.407307608593593E-309)
            boolean r1 = r13.Aem(r1)
            goto L_0x1859
        L_0x1943:
            X.0l8 r1 = X.C10950l8.A08
            if (r2 != r1) goto L_0x1954
            X.1Yd r13 = r13.A00
            r1 = 284842231141403(0x103100001141b, double:1.40730760891739E-309)
            boolean r1 = r13.Aem(r1)
            goto L_0x1859
        L_0x1954:
            r1 = 0
            goto L_0x1859
        L_0x1957:
            if (r7 == 0) goto L_0x1991
            java.lang.Class<X.1IZ> r5 = X.AnonymousClass1IZ.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 1122643471(0x42ea2e0f, float:117.08996)
            X.10N r2 = A0E(r5, r0, r1, r2)
            X.11G r1 = r3.A14()
            r1.A0G(r2)
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1034712717(0xffffffffc2538973, float:-52.884228)
            X.10N r2 = A0E(r5, r0, r1, r2)
            X.11G r1 = r3.A14()
            r1.A0J(r2)
            r2 = 16843534(0x101030e, float:2.369575E-38)
            r1 = 0
            int r1 = r12.A06(r2, r1)
            if (r1 != 0) goto L_0x1996
            r1 = 0
            X.11G r0 = r3.A14()
            r0.A0C(r1)
        L_0x1991:
            r0 = 4
            X.AnonymousClass11F.A0C(r0, r4, r10)
            return r3
        L_0x1996:
            android.content.Context r0 = r0.A09
            android.graphics.drawable.Drawable r1 = X.AnonymousClass01R.A03(r0, r1)
            X.11G r0 = r3.A14()
            r0.A0C(r1)
            goto L_0x1991
        L_0x19a4:
            r1 = r2
            X.1Rz r1 = (X.AnonymousClass1Rz) r1
            java.lang.CharSequence r4 = r1.A02
            X.10J r3 = r1.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r2 = r1.A00
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r1 = X.C17030yD.A00(r0)
            r1.A3l(r4)
            r1.A3f(r3)
            r1.A3c(r2)
            r0 = 1
            r1.A3J(r0)
            java.lang.String r0 = "thread_last_msg"
            r1.A2p(r0)
            X.0yD r0 = r1.A39()
            return r0
        L_0x19c8:
            r1 = r2
            X.1vN r1 = (X.AnonymousClass1vN) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r1.A00
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r2 = X.C22291Kt.A00(r0)
            r0 = 2132346348(0x7f1905ec, float:2.0340263E38)
            r2.A36(r0)
            int r0 = r1.B2X()
            r2.A33(r0)
            r0 = 1098907648(0x41800000, float:16.0)
            r2.A1z(r0)
            r2.A1p(r0)
            android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.CENTER
            java.lang.Object r0 = r2.A00
            X.1Kt r0 = (X.C22291Kt) r0
            r0.A02 = r1
            return r0
        L_0x19ef:
            r4 = r2
            X.1JF r4 = (X.AnonymousClass1JF) r4
            X.1H4 r12 = r4.A05
            com.facebook.mig.scheme.interfaces.MigColorScheme r11 = r4.A04
            int r2 = X.AnonymousClass1Y3.APM
            X.0UN r3 = r4.A00
            r1 = 2
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.0uK r8 = (X.C14900uK) r8
            int r2 = X.AnonymousClass1Y3.B8a
            r1 = 0
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.0oa r10 = (X.C12130oa) r10
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r6 = r4.A01
            X.1RF r1 = r4.A03
            boolean r7 = r1.pressed
            r5 = 2
            java.lang.String r2 = "profileConfig"
            java.lang.String r1 = "threadTileViewData"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r5)
            X.1J5 r13 = new X.1J5
            r13.<init>()
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1a2d
            java.lang.String r2 = r1.A06
            r13.A07 = r2
        L_0x1a2d:
            r3.clear()
            X.1J0 r1 = X.AnonymousClass1J0.A09
            r13.A02 = r1
            r1 = 0
            r3.set(r1)
            r13.A04 = r12
            r1 = 1
            r3.set(r1)
            r13.A03 = r11
            boolean r1 = X.C14900uK.A01(r8, r6)
            if (r1 == 0) goto L_0x1a49
            r1 = 1
            if (r6 != 0) goto L_0x1a4a
        L_0x1a49:
            r1 = 0
        L_0x1a4a:
            if (r1 == 0) goto L_0x1aeb
            boolean r1 = X.C14900uK.A01(r8, r6)
            if (r1 == 0) goto L_0x1a55
            r1 = 1
            if (r6 != 0) goto L_0x1a56
        L_0x1a55:
            r1 = 0
        L_0x1a56:
            X.AnonymousClass064.A03(r1)
            com.facebook.messaging.montage.model.MontageBucketKey r1 = r6.A02
            long r1 = r1.A00
            boolean r2 = r10.A03(r1)
            boolean r1 = r6.A07
            if (r1 == 0) goto L_0x1a68
            r1 = 1
            if (r2 == 0) goto L_0x1a69
        L_0x1a68:
            r1 = 0
        L_0x1a69:
            if (r1 == 0) goto L_0x1a7c
            X.36Z r1 = X.AnonymousClass36Z.A01
        L_0x1a6d:
            int r1 = r1.ordinal()
            switch(r1) {
                case 1: goto L_0x1a7f;
                case 2: goto L_0x1a83;
                default: goto L_0x1a74;
            }
        L_0x1a74:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unexpected ring state."
            r1.<init>(r0)
            throw r1
        L_0x1a7c:
            X.36Z r1 = X.AnonymousClass36Z.A03
            goto L_0x1a6d
        L_0x1a7f:
            r1 = 2132083317(0x7f150275, float:1.9806773E38)
            goto L_0x1a86
        L_0x1a83:
            r1 = 2132082728(0x7f150028, float:1.9805578E38)
        L_0x1a86:
            int r1 = r9.A02(r1)
            r13.A00 = r1
            r2 = 1065353216(0x3f800000, float:1.0)
            if (r7 == 0) goto L_0x1a93
            r2 = 1063675494(0x3f666666, float:0.9)
        L_0x1a93:
            X.11G r1 = r13.A14()
            r1.A06(r2)
            java.lang.String r7 = "montage_press"
            X.11G r2 = r13.A14()
            java.lang.String r1 = r13.A07
            r2.A0T(r7, r1)
            X.11G r1 = r13.A14()
            java.lang.Integer r1 = r1.A03()
            if (r1 != 0) goto L_0x1aba
            java.lang.Integer r2 = X.C17760zQ.A02
            if (r2 == 0) goto L_0x1b26
            X.11G r1 = r13.A14()
            r1.A0P(r2)
        L_0x1aba:
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            if (r2 == 0) goto L_0x1b26
            X.11G r1 = r13.A14()
            r1.A0P(r2)
            java.lang.Class<X.1JF> r7 = X.AnonymousClass1JF.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1336101728(0xffffffffb05cb4a0, float:-8.029222E-10)
            X.10N r2 = A0E(r7, r0, r1, r2)
            X.11G r1 = r13.A14()
            r1.A0L(r2)
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 792338070(0x2f3a1e96, float:1.6927468E-10)
            X.10N r1 = A0E(r7, r0, r1, r2)
            X.11G r0 = r13.A14()
            r0.A0G(r1)
        L_0x1aeb:
            boolean r0 = X.C14900uK.A01(r8, r6)
            if (r0 == 0) goto L_0x1b1a
            if (r6 == 0) goto L_0x1b1a
            r2 = 10
            int r1 = X.AnonymousClass1Y3.BRG
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.0yl r0 = (X.C17350yl) r0
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r0.A00
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 282501476845027(0x100ef002d05e3, double:1.39574274608544E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x1b1a
            boolean r1 = r6.A07
            r0 = 1
            if (r1 != 0) goto L_0x1b1b
        L_0x1b1a:
            r0 = 0
        L_0x1b1b:
            if (r0 == 0) goto L_0x1b21
            com.facebook.messaging.model.messages.Message r0 = r6.A01
            r13.A01 = r0
        L_0x1b21:
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r3, r4)
            return r13
        L_0x1b26:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            r0 = 50
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            r1.<init>(r0)
            throw r1
        L_0x1b32:
            r1 = r2
            X.1l8 r1 = (X.C32091l8) r1
            java.util.List r5 = r1.A00
            r2 = 1
            java.lang.String r1 = "userKeys"
            java.lang.String[] r4 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.1Sx r1 = new X.1Sx
            r1.<init>()
            X.0z4 r2 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x1b52
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x1b52:
            r3.clear()
            r1.A05 = r5
            r0 = 0
            r3.set(r0)
            r0 = 3
            r1.A00 = r0
            r0 = 1098907648(0x41800000, float:16.0)
            int r0 = r2.A00(r0)
            r1.A03 = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = r2.A00(r0)
            r1.A01 = r0
            r0 = 1082130432(0x40800000, float:4.0)
            int r0 = r2.A00(r0)
            r1.A02 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
            return r1
        L_0x1b7b:
            r1 = r2
            X.1Jl r1 = (X.C21981Jl) r1
            java.lang.Integer r4 = r1.A02
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A01
            int r3 = X.AnonymousClass1Y3.B4O
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1nf r1 = (X.C33491nf) r1
            X.1mo r5 = new X.1mo
            X.1mn r1 = r1.A00
            int r3 = X.AnonymousClass1Y3.AJa
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1MT r3 = (X.AnonymousClass1MT) r3
            int r1 = r4.intValue()
            switch(r1) {
                case 0: goto L_0x1bbe;
                case 1: goto L_0x1bbb;
                case 2: goto L_0x1bb8;
                case 3: goto L_0x1bb5;
                default: goto L_0x1ba3;
            }
        L_0x1ba3:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "No drawable binding defined for "
            java.lang.String r1 = X.C21981Jl.A00(r4)
            java.lang.String r0 = "! Make sure you specify a drawable in M4DeliveryStateDrawableBinder."
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        L_0x1bb5:
            X.1gj r2 = X.C33071mn.A02
            goto L_0x1bc0
        L_0x1bb8:
            X.1gj r2 = X.C33071mn.A01
            goto L_0x1bc0
        L_0x1bbb:
            X.1gj r2 = X.C33071mn.A04
            goto L_0x1bc0
        L_0x1bbe:
            X.1gj r2 = X.C33071mn.A03
        L_0x1bc0:
            java.lang.Integer r1 = X.C33071mn.A05
            int r2 = r3.A03(r2, r1)
            int r1 = r4.intValue()
            switch(r1) {
                case 0: goto L_0x1be4;
                case 1: goto L_0x1be4;
                case 2: goto L_0x1be4;
                case 3: goto L_0x1bdf;
                default: goto L_0x1bcd;
            }
        L_0x1bcd:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "No m4 color binding defined for "
            java.lang.String r1 = X.C21981Jl.A00(r4)
            java.lang.String r0 = "! Make sure you specify a drawable in ThreadItemSendStateDrawableBinder."
            java.lang.String r0 = X.AnonymousClass08S.A0P(r2, r1, r0)
            r3.<init>(r0)
            throw r3
        L_0x1bdf:
            int r1 = r6.B0I()
            goto L_0x1be8
        L_0x1be4:
            int r1 = r6.B2X()
        L_0x1be8:
            r5.<init>(r2, r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r2 = X.C22291Kt.A00(r0)
            int r0 = r5.A01
            r2.A36(r0)
            int r0 = r5.A00
            r2.A33(r0)
            r0 = 1098907648(0x41800000, float:16.0)
            r2.A1z(r0)
            r2.A1p(r0)
            android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_CENTER
            java.lang.Object r0 = r2.A00
            X.1Kt r0 = (X.C22291Kt) r0
            r0.A02 = r1
            return r0
        L_0x1c0a:
            r1 = r2
            X.1RH r1 = (X.AnonymousClass1RH) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r5 = r1.A03
            com.facebook.messaging.model.threads.ThreadSummary r11 = r1.A01
            X.0x0 r7 = r1.A02
            int r2 = X.AnonymousClass1Y3.APM
            X.0UN r3 = r1.A00
            r1 = 1
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.0uK r4 = (X.C14900uK) r4
            int r2 = X.AnonymousClass1Y3.BQG
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.1n5 r1 = (X.C33141n5) r1
            X.AnonymousClass064.A00(r11)
            X.AnonymousClass064.A00(r7)
            java.util.Set r1 = r1.A00
            java.util.Iterator r2 = r1.iterator()
        L_0x1c33:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x1c7e
            java.lang.Object r1 = r2.next()
            X.1Rc r1 = (X.C23831Rc) r1
            X.2bu r6 = r1.B3Q(r11)
            if (r6 == 0) goto L_0x1c33
        L_0x1c45:
            if (r6 != 0) goto L_0x1e1a
            X.10J r6 = r7.A01
            android.content.res.Resources r2 = r0.A03()
            X.1I7 r1 = r6.mTextSize
            int r1 = r1.B5j()
            int r9 = r2.getDimensionPixelSize(r1)
            boolean r10 = r11.A0B()
            int r3 = X.AnonymousClass1Y3.B8g
            X.0UN r2 = r4.A00
            r1 = 2
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0tz r8 = (X.C14770tz) r8
            X.1Rw r4 = new X.1Rw
            r4.<init>(r11, r9, r10)
            X.1gw r1 = r8.A02
            java.lang.Object r7 = r1.A03(r4)
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7
            if (r7 != 0) goto L_0x1de4
            java.lang.String r2 = "CreateSnippetSpan"
            r1 = -1282273647(0xffffffffb3920e91, float:-6.8013044E-8)
            X.C005505z.A03(r2, r1)
            goto L_0x1c80
        L_0x1c7e:
            r6 = 0
            goto L_0x1c45
        L_0x1c80:
            int r2 = X.AnonymousClass1Y3.BEj     // Catch:{ all -> 0x1dd1 }
            X.0UN r1 = r8.A00     // Catch:{ all -> 0x1dd1 }
            r3 = 1
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x1dd1 }
            X.1Ry r7 = (X.C24041Ry) r7     // Catch:{ all -> 0x1dd1 }
            if (r10 != 0) goto L_0x1cac
            java.lang.Boolean r1 = r7.A01     // Catch:{ all -> 0x1dd1 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x1dd1 }
            if (r1 != 0) goto L_0x1cac
            com.facebook.messaging.model.threads.GroupThreadData r2 = r11.A0V     // Catch:{ all -> 0x1dd1 }
            boolean r1 = r2.A01()     // Catch:{ all -> 0x1dd1 }
            if (r1 == 0) goto L_0x1cac
            com.facebook.messaging.model.threads.GroupThreadAssociatedObject r1 = r2.A02     // Catch:{ all -> 0x1dd1 }
            com.facebook.messaging.model.threads.GroupThreadAssociatedFbGroup r1 = r1.A00()     // Catch:{ all -> 0x1dd1 }
            java.lang.String r1 = r1.A04     // Catch:{ all -> 0x1dd1 }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r1)     // Catch:{ all -> 0x1dd1 }
            r1 = 1
            if (r2 == 0) goto L_0x1cad
        L_0x1cac:
            r1 = 0
        L_0x1cad:
            if (r1 != 0) goto L_0x1cb1
            r7 = 0
            goto L_0x1cc8
        L_0x1cb1:
            android.content.res.Resources r7 = r7.A00     // Catch:{ all -> 0x1dd1 }
            r2 = 2131828630(0x7f111f96, float:1.9290206E38)
            com.facebook.messaging.model.threads.GroupThreadData r1 = r11.A0V     // Catch:{ all -> 0x1dd1 }
            com.facebook.messaging.model.threads.GroupThreadAssociatedObject r1 = r1.A02     // Catch:{ all -> 0x1dd1 }
            com.facebook.messaging.model.threads.GroupThreadAssociatedFbGroup r1 = r1.A00()     // Catch:{ all -> 0x1dd1 }
            java.lang.String r1 = r1.A04     // Catch:{ all -> 0x1dd1 }
            java.lang.Object[] r1 = new java.lang.Object[]{r1}     // Catch:{ all -> 0x1dd1 }
            java.lang.String r7 = r7.getString(r2, r1)     // Catch:{ all -> 0x1dd1 }
        L_0x1cc8:
            boolean r1 = com.google.common.base.Platform.stringIsNullOrEmpty(r7)     // Catch:{ all -> 0x1dd1 }
            if (r1 != 0) goto L_0x1cd6
            r1 = -1586062154(0xffffffffa1769cb6, float:-8.3555447E-19)
            X.C005505z.A00(r1)
            goto L_0x1ddf
        L_0x1cd6:
            X.0l8 r2 = r11.A0O     // Catch:{ all -> 0x1dd1 }
            X.0l8 r1 = X.C10950l8.A08     // Catch:{ all -> 0x1dd1 }
            boolean r1 = r2.equals(r1)     // Catch:{ all -> 0x1dd1 }
            if (r1 == 0) goto L_0x1cee
            android.content.res.Resources r2 = r8.A01     // Catch:{ all -> 0x1dd1 }
            r1 = 2131833827(0x7f1133e3, float:1.9300747E38)
            java.lang.String r7 = r2.getString(r1)     // Catch:{ all -> 0x1dd1 }
            r1 = 1938729205(0x738ea8f5, float:2.2605377E31)
            goto L_0x1ddc
        L_0x1cee:
            java.lang.String r7 = r11.A0p     // Catch:{ all -> 0x1dd1 }
            boolean r15 = X.C06850cB.A0B(r7)     // Catch:{ all -> 0x1dd1 }
            r15 = r15 ^ r3
            if (r15 != 0) goto L_0x1cf9
            java.lang.String r7 = r11.A0w     // Catch:{ all -> 0x1dd1 }
        L_0x1cf9:
            com.facebook.messaging.model.messages.ParticipantInfo r3 = r11.A0Q     // Catch:{ all -> 0x1dd1 }
            if (r7 == 0) goto L_0x1d09
            if (r15 != 0) goto L_0x1d10
            if (r3 != 0) goto L_0x1d10
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r11.A0S     // Catch:{ all -> 0x1dd1 }
            boolean r1 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r1)     // Catch:{ all -> 0x1dd1 }
            if (r1 != 0) goto L_0x1d10
        L_0x1d09:
            java.lang.String r7 = ""
            r1 = 1608502037(0x5fdfcb15, float:3.2252012E19)
            goto L_0x1ddc
        L_0x1d10:
            int r10 = X.AnonymousClass1Y3.B4l     // Catch:{ all -> 0x1dd1 }
            X.0UN r2 = r8.A00     // Catch:{ all -> 0x1dd1 }
            r1 = 2
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r10, r2)     // Catch:{ all -> 0x1dd1 }
            X.1pk r1 = (X.C34521pk) r1     // Catch:{ all -> 0x1dd1 }
            boolean r1 = r1.A01()     // Catch:{ all -> 0x1dd1 }
            if (r1 == 0) goto L_0x1d36
            r10 = 3
            int r2 = X.AnonymousClass1Y3.AUg     // Catch:{ all -> 0x1dd1 }
            X.0UN r1 = r8.A00     // Catch:{ all -> 0x1dd1 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r10, r2, r1)     // Catch:{ all -> 0x1dd1 }
            X.8B2 r2 = (X.AnonymousClass8B2) r2     // Catch:{ all -> 0x1dd1 }
            java.lang.Integer r1 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x1dd1 }
            java.lang.CharSequence r1 = r2.A04(r7, r1)     // Catch:{ all -> 0x1dd1 }
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x1dd1 }
        L_0x1d36:
            java.util.regex.Pattern r1 = X.C14770tz.A08     // Catch:{ all -> 0x1dd1 }
            java.util.regex.Matcher r2 = r1.matcher(r7)     // Catch:{ all -> 0x1dd1 }
            boolean r1 = r2.find()     // Catch:{ all -> 0x1dd1 }
            if (r1 == 0) goto L_0x1d48
            java.lang.String r1 = " "
            java.lang.String r7 = r2.replaceAll(r1)     // Catch:{ all -> 0x1dd1 }
        L_0x1d48:
            X.0A4 r10 = new X.0A4     // Catch:{ all -> 0x1dd1 }
            android.content.res.Resources r1 = r8.A01     // Catch:{ all -> 0x1dd1 }
            r10.<init>(r1)     // Catch:{ all -> 0x1dd1 }
            r14 = 0
            if (r3 == 0) goto L_0x1d8c
            com.facebook.messaging.model.threadkey.ThreadKey r13 = r11.A0S     // Catch:{ all -> 0x1dd1 }
            com.facebook.user.model.UserKey r12 = com.facebook.messaging.model.threadkey.ThreadKey.A07(r13)     // Catch:{ all -> 0x1dd1 }
            if (r13 == 0) goto L_0x1d60
            X.1fF r2 = r13.A05     // Catch:{ all -> 0x1dd1 }
            X.1fF r1 = X.C28711fF.ONE_TO_ONE     // Catch:{ all -> 0x1dd1 }
            if (r2 == r1) goto L_0x1d72
        L_0x1d60:
            boolean r1 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r13)     // Catch:{ all -> 0x1dd1 }
            if (r1 == 0) goto L_0x1d70
            com.google.common.collect.ImmutableList r1 = r11.A0m     // Catch:{ all -> 0x1dd1 }
            int r2 = r1.size()     // Catch:{ all -> 0x1dd1 }
            r1 = 2
            if (r2 > r1) goto L_0x1d70
            goto L_0x1d72
        L_0x1d70:
            r1 = 0
            goto L_0x1d73
        L_0x1d72:
            r1 = 1
        L_0x1d73:
            if (r1 == 0) goto L_0x1d7e
            com.facebook.user.model.UserKey r1 = r3.A01     // Catch:{ all -> 0x1dd1 }
            boolean r2 = r1.equals(r12)     // Catch:{ all -> 0x1dd1 }
            r1 = 1
            if (r2 != 0) goto L_0x1d7f
        L_0x1d7e:
            r1 = 0
        L_0x1d7f:
            if (r1 != 0) goto L_0x1d8c
            if (r15 == 0) goto L_0x1d8b
            java.lang.String r1 = X.C22351Kz.A00     // Catch:{ all -> 0x1dd1 }
            boolean r1 = r1.equals(r7)     // Catch:{ all -> 0x1dd1 }
            if (r1 == 0) goto L_0x1d8c
        L_0x1d8b:
            r14 = 1
        L_0x1d8c:
            if (r14 == 0) goto L_0x1dbf
            com.facebook.user.model.UserKey r2 = r3.A01     // Catch:{ all -> 0x1dd1 }
            X.0Tq r1 = r8.A06     // Catch:{ all -> 0x1dd1 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x1dd1 }
            boolean r1 = r2.equals(r1)     // Catch:{ all -> 0x1dd1 }
            if (r1 == 0) goto L_0x1db6
            android.content.res.Resources r2 = r8.A01     // Catch:{ all -> 0x1dd1 }
            r1 = 2131833829(0x7f1133e5, float:1.9300751E38)
            java.lang.String r1 = r2.getString(r1)     // Catch:{ all -> 0x1dd1 }
        L_0x1da5:
            android.content.res.Resources r3 = r8.A01     // Catch:{ all -> 0x1dd1 }
            r2 = 2131833828(0x7f1133e4, float:1.930075E38)
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r7}     // Catch:{ all -> 0x1dd1 }
            java.lang.String r1 = r3.getString(r2, r1)     // Catch:{ all -> 0x1dd1 }
            r10.A03(r1)     // Catch:{ all -> 0x1dd1 }
            goto L_0x1dc2
        L_0x1db6:
            X.1bq r2 = r8.A03     // Catch:{ all -> 0x1dd1 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r11.A0S     // Catch:{ all -> 0x1dd1 }
            java.lang.String r1 = r2.A0B(r1, r3)     // Catch:{ all -> 0x1dd1 }
            goto L_0x1da5
        L_0x1dbf:
            r10.A03(r7)     // Catch:{ all -> 0x1dd1 }
        L_0x1dc2:
            android.text.SpannableStringBuilder r7 = new android.text.SpannableStringBuilder     // Catch:{ all -> 0x1dd1 }
            android.text.SpannableString r1 = r10.A00()     // Catch:{ all -> 0x1dd1 }
            r7.<init>(r1)     // Catch:{ all -> 0x1dd1 }
            X.1Kz r1 = r8.A05     // Catch:{ all -> 0x1dd1 }
            r1.ANh(r7, r9)     // Catch:{ all -> 0x1dd1 }
            goto L_0x1dd9
        L_0x1dd1:
            r1 = move-exception
            r0 = 623211119(0x2525726f, float:1.435024E-16)
            X.C005505z.A00(r0)
            throw r1
        L_0x1dd9:
            r1 = -510192336(0xffffffffe1971530, float:-3.4837313E20)
        L_0x1ddc:
            X.C005505z.A00(r1)
        L_0x1ddf:
            X.1gw r1 = r8.A02
            r1.A05(r4, r7)
        L_0x1de4:
            r4 = 3
            java.lang.String r3 = "colorScheme"
            java.lang.String r2 = "snippetText"
            java.lang.String r1 = "textStyle"
            java.lang.String[] r3 = new java.lang.String[]{r3, r2, r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r4)
            X.1Rz r1 = new X.1Rz
            r1.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x1e01
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x1e01:
            r2.clear()
            r1.A02 = r7
            r0 = 1
            r2.set(r0)
            r1.A01 = r6
            r0 = 2
            r2.set(r0)
            r1.A00 = r5
            r0 = 0
            r2.set(r0)
            X.AnonymousClass11F.A0C(r4, r2, r3)
        L_0x1e19:
            return r1
        L_0x1e1a:
            r4 = 1
            java.lang.String r1 = "snippetAction"
            java.lang.String[] r3 = new java.lang.String[]{r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r4)
            X.2bv r1 = new X.2bv
            r1.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x1e33
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x1e33:
            r2.clear()
            r1.A01 = r6
            r0 = 0
            r2.set(r0)
            X.AnonymousClass11F.A0C(r4, r2, r3)
            return r1
        L_0x1e40:
            r1 = r2
            X.1Kd r1 = (X.C22161Kd) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r5 = r1.A03
            com.facebook.messaging.model.threads.ThreadSummary r9 = r1.A01
            X.0x0 r8 = r1.A02
            int r3 = X.AnonymousClass1Y3.APM
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0uK r1 = (X.C14900uK) r1
            X.AnonymousClass064.A00(r9)
            X.AnonymousClass064.A00(r8)
            int r3 = X.AnonymousClass1Y3.AnB
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Ke r1 = (X.C22171Ke) r1
            com.facebook.messaging.ui.name.MessengerThreadNameViewData r7 = r1.A03(r9)
            r6 = 2
            java.lang.String r2 = "data"
            java.lang.String r1 = "textStyle"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r6)
            X.1J4 r2 = new X.1J4
            r2.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x1e84
            java.lang.String r1 = r0.A06
            r2.A07 = r1
        L_0x1e84:
            r3.clear()
            r2.A00 = r7
            r0 = 0
            r3.set(r0)
            boolean r0 = r9.A0B()
            r2.A03 = r0
            X.10J r0 = r8.A00
            r2.A02 = r0
            r0 = 1
            r3.set(r0)
            java.lang.String r1 = "thread_name"
            X.11G r0 = r2.A14()
            r0.A0S(r1)
            r2.A01 = r5
            X.AnonymousClass11F.A0C(r6, r3, r4)
            return r2
        L_0x1eaa:
            r1 = r2
            X.1RJ r1 = (X.AnonymousClass1RJ) r1
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r9 = r1.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A03
            com.facebook.messaging.model.threads.ThreadSummary r7 = r1.A02
            int r3 = X.AnonymousClass1Y3.BJ1
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1Sa r5 = (X.C24051Sa) r5
            com.google.common.base.Preconditions.checkNotNull(r7)
            com.google.common.collect.ImmutableList$Builder r4 = new com.google.common.collect.ImmutableList$Builder
            r4.<init>()
            boolean r1 = r9.A0F
            if (r1 == 0) goto L_0x205d
            r2 = 1
            java.lang.String r1 = "colorScheme"
            java.lang.String[] r8 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.5Fa r2 = new X.5Fa
            android.content.Context r1 = r0.A09
            r2.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1ee5
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x1ee5:
            r3.clear()
            r2.A01 = r6
            r1 = 0
            r3.set(r1)
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r3, r8)
        L_0x1ef2:
            if (r2 == 0) goto L_0x1ef7
            r4.add(r2)
        L_0x1ef7:
            boolean r1 = r9.A0C
            if (r1 == 0) goto L_0x205a
            r2 = 1
            java.lang.String r1 = "colorScheme"
            java.lang.String[] r8 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.1vN r2 = new X.1vN
            r2.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1f14
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x1f14:
            r3.clear()
            r2.A00 = r6
            r1 = 0
            r3.set(r1)
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r3, r8)
        L_0x1f21:
            if (r2 == 0) goto L_0x1f26
            r4.add(r2)
        L_0x1f26:
            boolean r1 = r9.A0A
            if (r1 == 0) goto L_0x2057
            r2 = 1
            java.lang.String r1 = "colorScheme"
            java.lang.String[] r8 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.5FZ r2 = new X.5FZ
            r2.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1f43
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x1f43:
            r3.clear()
            r2.A00 = r6
            r1 = 0
            r3.set(r1)
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r3, r8)
        L_0x1f50:
            if (r2 == 0) goto L_0x1f55
            r4.add(r2)
        L_0x1f55:
            X.0uK r8 = r5.A01
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r7.A0S
            boolean r1 = r1.A0O()
            r10 = 1
            if (r1 != 0) goto L_0x1f92
            r3 = 2
            int r2 = X.AnonymousClass1Y3.B8g
            X.0UN r1 = r8.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.0tz r3 = (X.C14770tz) r3
            com.facebook.messaging.model.messages.ParticipantInfo r1 = r7.A0Q
            if (r1 == 0) goto L_0x1f7e
            com.facebook.user.model.UserKey r2 = r1.A01
            X.0Tq r1 = r3.A06
            java.lang.Object r1 = r1.get()
            boolean r2 = r2.equals(r1)
            r1 = 1
            if (r2 != 0) goto L_0x1f7f
        L_0x1f7e:
            r1 = 0
        L_0x1f7f:
            if (r1 == 0) goto L_0x2054
            r3 = 5
            int r2 = X.AnonymousClass1Y3.Am6
            X.0UN r1 = r8.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1NQ r1 = (X.AnonymousClass1NQ) r1
            boolean r1 = r1.A01(r7)
            if (r1 == 0) goto L_0x2054
        L_0x1f92:
            r9 = 0
            if (r10 != 0) goto L_0x200e
            boolean r1 = r7.A0B()
            if (r1 == 0) goto L_0x1f9d
        L_0x1f9b:
            java.lang.Integer r9 = X.AnonymousClass07B.A0C
        L_0x1f9d:
            if (r9 != 0) goto L_0x1fde
            r2 = 0
        L_0x1fa0:
            if (r2 == 0) goto L_0x1fa5
            r4.add(r2)
        L_0x1fa5:
            com.google.common.collect.ImmutableList r3 = r4.build()
            boolean r1 = r3.isEmpty()
            if (r1 != 0) goto L_0x22bb
            X.1Jr r1 = new X.1Jr
            r1.<init>()
            X.0z4 r2 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x1fbe
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x1fbe:
            if (r3 == 0) goto L_0x1fca
            java.util.List r0 = r1.A01
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x1fd8
            r1.A01 = r3
        L_0x1fca:
            X.1JQ r0 = X.AnonymousClass1JQ.MEDIUM
            int r0 = r0.B3A()
            float r0 = (float) r0
            int r0 = r2.A00(r0)
            r1.A00 = r0
            return r1
        L_0x1fd8:
            java.util.List r0 = r1.A01
            r0.addAll(r3)
            goto L_0x1fca
        L_0x1fde:
            r7 = 2
            java.lang.String r2 = "colorScheme"
            java.lang.String r1 = "threadStatus"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r7)
            X.1Jp r2 = new X.1Jp
            android.content.Context r1 = r0.A09
            r2.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1ffb
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x1ffb:
            r3.clear()
            r2.A02 = r6
            r1 = 0
            r3.set(r1)
            r2.A03 = r9
            r1 = 1
            r3.set(r1)
            X.AnonymousClass11F.A0C(r7, r3, r5)
            goto L_0x1fa0
        L_0x200e:
            r3 = 0
            int r2 = X.AnonymousClass1Y3.BQ8
            X.0UN r1 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.1NR r1 = (X.AnonymousClass1NR) r1
            java.lang.Integer r8 = r1.A01(r7)
            java.lang.Integer r1 = X.AnonymousClass07B.A0N
            if (r8 == r1) goto L_0x2050
            boolean r1 = r7.A0B()
            if (r1 != 0) goto L_0x1f9b
            X.0uK r1 = r5.A01
            int r3 = X.AnonymousClass1Y3.Am6
            X.0UN r2 = r1.A00
            r1 = 5
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1NQ r1 = (X.AnonymousClass1NQ) r1
            boolean r1 = r1.A01(r7)
            if (r1 == 0) goto L_0x2047
            X.0uK r1 = r5.A01
            java.util.List r1 = r1.A02(r7)
            boolean r2 = r1.isEmpty()
            r1 = 1
            if (r2 == 0) goto L_0x2048
        L_0x2047:
            r1 = 0
        L_0x2048:
            if (r1 == 0) goto L_0x204e
            java.lang.Integer r9 = X.AnonymousClass07B.A01
            goto L_0x1f9d
        L_0x204e:
            if (r8 == 0) goto L_0x1f9d
        L_0x2050:
            java.lang.Integer r9 = X.AnonymousClass07B.A00
            goto L_0x1f9d
        L_0x2054:
            r10 = 0
            goto L_0x1f92
        L_0x2057:
            r2 = 0
            goto L_0x1f50
        L_0x205a:
            r2 = 0
            goto L_0x1f21
        L_0x205d:
            r2 = 0
            goto L_0x1ef2
        L_0x2060:
            r6 = r2
            X.1Jp r6 = (X.C22021Jp) r6
            java.lang.Integer r8 = r6.A03
            com.facebook.mig.scheme.interfaces.MigColorScheme r5 = r6.A02
            int r2 = X.AnonymousClass1Y3.APM
            X.0UN r4 = r6.A00
            r1 = 2
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.0uK r7 = (X.C14900uK) r7
            int r2 = X.AnonymousClass1Y3.BQ8
            r1 = 1
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.1NR r3 = (X.AnonymousClass1NR) r3
            int r2 = X.AnonymousClass1Y3.AN2
            r1 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r4)
            X.1rb r2 = (X.C35671rb) r2
            com.facebook.messaging.model.threads.ThreadSummary r6 = r6.A01
            X.AnonymousClass064.A00(r6)
            int r1 = r8.intValue()
            switch(r1) {
                case 0: goto L_0x20ee;
                case 1: goto L_0x20c4;
                case 2: goto L_0x2098;
                default: goto L_0x2090;
            }
        L_0x2090:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unsupported thread status provided!"
            r1.<init>(r0)
            throw r1
        L_0x2098:
            boolean r1 = r2.A01()
            if (r1 == 0) goto L_0x22bb
            r4 = 1
            java.lang.String r1 = "colorScheme"
            java.lang.String[] r3 = new java.lang.String[]{r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r4)
            X.1l7 r1 = new X.1l7
            r1.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x20b7
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x20b7:
            r2.clear()
            r1.A00 = r5
            r0 = 0
            r2.set(r0)
            X.AnonymousClass11F.A0C(r4, r2, r3)
            return r1
        L_0x20c4:
            r4 = 1
            java.lang.String r1 = "userKeys"
            java.lang.String[] r3 = new java.lang.String[]{r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r4)
            X.1l8 r1 = new X.1l8
            r1.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x20dd
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x20dd:
            r2.clear()
            java.util.List r0 = r7.A02(r6)
            r1.A00 = r0
            r0 = 0
            r2.set(r0)
            X.AnonymousClass11F.A0C(r4, r2, r3)
            return r1
        L_0x20ee:
            java.lang.Integer r7 = r3.A01(r6)
            if (r7 == 0) goto L_0x22bb
            r6 = 2
            java.lang.String r2 = "colorScheme"
            java.lang.String r1 = "sendState"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r6)
            X.1Jl r1 = new X.1Jl
            android.content.Context r2 = r0.A09
            r1.<init>(r2)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x2111
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x2111:
            r3.clear()
            r1.A01 = r5
            r0 = 0
            r3.set(r0)
            r1.A02 = r7
            r0 = 1
            r3.set(r0)
            X.AnonymousClass11F.A0C(r6, r3, r4)
            return r1
        L_0x2124:
            r1 = r2
            X.1RG r1 = (X.AnonymousClass1RG) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r3 = r1.A03
            com.facebook.messaging.model.threads.ThreadSummary r10 = r1.A01
            X.0x0 r9 = r1.A02
            com.facebook.messaging.inbox2.items.InboxUnitThreadItem r8 = r1.A00
            X.AnonymousClass064.A00(r10)
            r1 = 1
            java.lang.String r6 = "colorScheme"
            java.lang.String[] r5 = new java.lang.String[]{r6}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r1)
            X.1Kd r7 = new X.1Kd
            android.content.Context r1 = r0.A09
            r7.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x214d
            java.lang.String r1 = r1.A06
            r7.A07 = r1
        L_0x214d:
            r4.clear()
            r7.A03 = r3
            r1 = 0
            r4.set(r1)
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r4, r5)
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r4 = r10.A0G
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A0J
            if (r4 == r1) goto L_0x2165
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r1 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A3A
            r2 = 0
            if (r4 != r1) goto L_0x2166
        L_0x2165:
            r2 = 1
        L_0x2166:
            r1 = 0
            if (r2 == 0) goto L_0x216a
            r1 = 1
        L_0x216a:
            if (r1 != 0) goto L_0x21e2
            r4 = 0
        L_0x216d:
            if (r8 == 0) goto L_0x21e0
            java.lang.String r1 = r8.A08
            boolean r1 = X.C06850cB.A0B(r1)
            if (r1 != 0) goto L_0x21e0
            r3 = 2
            java.lang.String r2 = "emojiStatus"
            java.lang.String r1 = "textStyle"
            java.lang.String[] r6 = new java.lang.String[]{r2, r1}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r3)
            X.5Cj r3 = new X.5Cj
            r3.<init>()
            X.0z4 r2 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x2194
            java.lang.String r10 = r1.A06
            r3.A07 = r10
        L_0x2194:
            r5.clear()
            java.lang.String r1 = r8.A08
            r3.A01 = r1
            r1 = 0
            r5.set(r1)
            X.10J r1 = r9.A00
            r3.A00 = r1
            r1 = 1
            r5.set(r1)
            X.10G r8 = X.AnonymousClass10G.START
            X.1JQ r1 = X.AnonymousClass1JQ.XSMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            int r2 = r2.A00(r1)
            X.11G r1 = r3.A14()
            r1.BJx(r8, r2)
            r2 = 0
            X.11G r1 = r3.A14()
            r1.AaD(r2)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r5, r6)
        L_0x21c7:
            if (r3 != 0) goto L_0x21cb
            if (r4 == 0) goto L_0x21df
        L_0x21cb:
            X.1wd r1 = X.C16980y8.A00(r0)
            r1.A3A(r7)
            r1.A3A(r3)
            r1.A3A(r4)
            X.0uO r0 = X.C14940uO.CENTER
            r1.A3D(r0)
            X.0y8 r7 = r1.A00
        L_0x21df:
            return r7
        L_0x21e0:
            r3 = 0
            goto L_0x21c7
        L_0x21e2:
            r1 = 1
            java.lang.String[] r6 = new java.lang.String[]{r6}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r1)
            X.2SY r4 = new X.2SY
            r4.<init>()
            X.0z4 r10 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x21fb
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x21fb:
            r5.clear()
            r4.A00 = r3
            r1 = 0
            r5.set(r1)
            X.10G r3 = X.AnonymousClass10G.START
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            int r2 = r10.A00(r1)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            r2 = 0
            X.11G r1 = r4.A14()
            r1.AaD(r2)
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r5, r6)
            goto L_0x216d
        L_0x2226:
            r1 = r2
            X.1ks r1 = (X.C31961ks) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r1.A03
            com.facebook.messaging.model.threads.ThreadSummary r5 = r1.A01
            X.0x0 r8 = r1.A02
            int r3 = X.AnonymousClass1Y3.APM
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.0uK r1 = (X.C14900uK) r1
            X.AnonymousClass064.A00(r5)
            X.AnonymousClass064.A00(r8)
            int r2 = X.AnonymousClass1Y3.BAo
            X.0UN r6 = r1.A00
            r1 = 9
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r2, r6)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x225b
            long r3 = r5.A06
            r9 = -1
            int r2 = (r3 > r9 ? 1 : (r3 == r9 ? 0 : -1))
            r1 = 1
            if (r2 != 0) goto L_0x225c
        L_0x225b:
            r1 = 0
        L_0x225c:
            if (r1 == 0) goto L_0x22b8
            long r2 = r5.A06
        L_0x2260:
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            r4 = 0
            if (r1 <= 0) goto L_0x2274
            r4 = 3
            int r1 = X.AnonymousClass1Y3.Aaw
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r1, r6)
            X.1S0 r1 = (X.AnonymousClass1S0) r1
            java.lang.String r4 = r1.A05(r2)
        L_0x2274:
            boolean r1 = android.text.TextUtils.isEmpty(r4)
            if (r1 != 0) goto L_0x22bb
            java.lang.String r1 = " · "
            java.lang.String r6 = X.AnonymousClass08S.A0J(r1, r4)
            X.10J r5 = r8.A02
            r4 = 3
            java.lang.String r3 = "colorScheme"
            java.lang.String r2 = "text"
            java.lang.String r1 = "textStyle"
            java.lang.String[] r3 = new java.lang.String[]{r3, r2, r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r4)
            X.1Jx r1 = new X.1Jx
            r1.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x229f
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x229f:
            r2.clear()
            r1.A02 = r6
            r0 = 1
            r2.set(r0)
            r1.A01 = r5
            r0 = 2
            r2.set(r0)
            r1.A00 = r7
            r0 = 0
            r2.set(r0)
            X.AnonymousClass11F.A0C(r4, r2, r3)
            return r1
        L_0x22b8:
            long r2 = r5.A0B
            goto L_0x2260
        L_0x22bb:
            r1 = 0
            return r1
        L_0x22bd:
            r1 = r2
            X.1Jx r1 = (X.C22101Jx) r1
            java.lang.String r3 = r1.A02
            X.10J r2 = r1.A01
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r1.A00
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r0 = X.C17030yD.A00(r0)
            r0.A3l(r3)
            r0.A3f(r2)
            r0.A3c(r1)
            X.0yD r0 = r0.A39()
            return r0
        L_0x22d8:
            r1 = r2
            X.1vM r1 = (X.AnonymousClass1vM) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r9 = r1.A02
            com.google.common.collect.ImmutableList r10 = r1.A03
            X.52X r7 = r1.A01
            java.lang.String r6 = r1.A04
            int r3 = X.AnonymousClass1Y3.Axb
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.7LM r8 = (X.AnonymousClass7LM) r8
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            X.52O r4 = new X.52O
            r4.<init>(r7)
            X.1Xv r7 = r10.iterator()
        L_0x22fc:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L_0x231e
            java.lang.Object r1 = r7.next()
            android.util.Pair r1 = (android.util.Pair) r1
            java.lang.Object r2 = r1.first
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            java.lang.Object r1 = r1.second
            X.7LR r3 = new X.7LR
            r3.<init>(r2, r1)
            java.lang.Object r2 = r3.A02
            java.lang.String r1 = "Tag for radio button group option cannot be null"
            com.google.common.base.Preconditions.checkNotNull(r2, r1)
            r5.add(r3)
            goto L_0x22fc
        L_0x231e:
            X.7Kf r3 = r8.A01(r0, r9)
            r0 = 2131827767(0x7f111c37, float:1.9288456E38)
            r3.A03(r0)
            r0 = 120(0x78, float:1.68E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            com.google.common.base.Preconditions.checkNotNull(r4, r0)
            java.util.Iterator r2 = r5.iterator()
        L_0x2335:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x234a
            java.lang.Object r1 = r2.next()
            X.7LR r1 = (X.AnonymousClass7LR) r1
            java.lang.Object r0 = r1.A02
            boolean r0 = com.google.common.base.Objects.equal(r6, r0)
            r1.A03 = r0
            goto L_0x2335
        L_0x234a:
            X.7Lg r1 = new X.7Lg
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r5)
            r1.<init>(r0, r4)
            r3.A04(r1)
            r1 = 2131827766(0x7f111c36, float:1.9288454E38)
            goto L_0x23db
        L_0x235b:
            X.7Lg r7 = new X.7Lg
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.copyOf(r11)
            r7.<init>(r1, r10)
            r3.A04(r7)
            java.lang.String r1 = "PUBLIC"
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x237b
            r7 = 2131828705(0x7f111fe1, float:1.9290358E38)
            X.0p4 r1 = r3.A01
            java.lang.String r1 = r1.A0C(r7)
            r3.A08(r1)
        L_0x237b:
            r3.A02()
            java.lang.String r1 = "FRIENDS"
            boolean r1 = r1.equals(r4)
            if (r1 != 0) goto L_0x238e
            java.lang.String r1 = "FRIENDS_AND_CONNECTIONS"
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x239d
        L_0x238e:
            r1 = 2131828428(0x7f111ecc, float:1.9289797E38)
            java.lang.String r4 = r0.A0C(r1)
            X.4qS r1 = new X.4qS
            r1.<init>(r5)
            r3.A09(r4, r1)
        L_0x239d:
            r1 = 2131828460(0x7f111eec, float:1.9289861E38)
            java.lang.String r4 = r0.A0C(r1)
            X.4q3 r1 = new X.4q3
            r1.<init>(r5)
            r3.A09(r4, r1)
            r3.A02()
            boolean r1 = r6.A06()
            if (r1 == 0) goto L_0x2b41
            r1 = 2131828483(0x7f111f03, float:1.9289908E38)
            java.lang.String r4 = r0.A0C(r1)
            X.4qf r1 = new X.4qf
            r1.<init>(r5, r2)
            r3.A0D(r4, r2, r1)
            r1 = 2131828480(0x7f111f00, float:1.9289902E38)
            java.lang.String r1 = r0.A0C(r1)
            X.4py r0 = new X.4py
            r0.<init>(r5)
            r3.A09(r1, r0)
            r1 = 2131828481(0x7f111f01, float:1.9289904E38)
            if (r2 == 0) goto L_0x23db
            r1 = 2131828482(0x7f111f02, float:1.9289906E38)
        L_0x23db:
            X.0p4 r0 = r3.A01
            java.lang.String r0 = r0.A0C(r1)
            r3.A08(r0)
            goto L_0x2b41
        L_0x23e6:
            r1 = r2
            X.1l7 r1 = (X.C32081l7) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r1.A00
            X.1wd r4 = X.C16980y8.A00(r0)
            r6 = 2
            java.lang.String r2 = "color"
            java.lang.String r1 = "diameter"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r6)
            X.1oT r1 = new X.1oT
            r1.<init>()
            X.0z4 r2 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x240c
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x240c:
            r3.clear()
            int r0 = r7.Aea()
            r1.A02 = r0
            r0 = 0
            r3.set(r0)
            r0 = 1096810496(0x41600000, float:14.0)
            int r0 = r2.A00(r0)
            r1.A03 = r0
            r0 = 1
            r3.set(r0)
            X.AnonymousClass11F.A0C(r6, r3, r5)
            r4.A3A(r1)
            X.0uP r0 = X.C14950uP.CENTER
            r4.A3E(r0)
            r0 = 1098907648(0x41800000, float:16.0)
            r4.A1z(r0)
            r4.A1p(r0)
            X.0y8 r0 = r4.A00
            return r0
        L_0x243b:
            r1 = r2
            X.1J4 r1 = (X.AnonymousClass1J4) r1
            com.facebook.messaging.ui.name.ThreadNameViewData r9 = r1.A00
            X.10J r7 = r1.A02
            boolean r8 = r1.A03
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A01
            X.10M r2 = r7.B7S()
            android.content.Context r1 = r0.A09
            android.graphics.Typeface r4 = r2.A00(r1)
            X.AnonymousClass064.A00(r4)
            r5 = 3
            java.lang.String r3 = "data"
            java.lang.String r2 = "textColor"
            java.lang.String r1 = "textSize"
            java.lang.String[] r3 = new java.lang.String[]{r3, r2, r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r5)
            X.1RX r1 = new X.1RX
            android.content.Context r5 = r0.A09
            r1.<init>(r5)
            X.0z4 r5 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x2474
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x2474:
            r2.clear()
            r1.A07 = r9
            r0 = 0
            r2.set(r0)
            r1.A08 = r8
            X.1I7 r0 = r7.mTextSize
            int r0 = r0.B5j()
            int r0 = r5.A03(r0)
            r1.A02 = r0
            r0 = 2
            r2.set(r0)
            X.0y3 r0 = r7.B5d(r6)
            int r0 = r0.AhV()
            r1.A01 = r0
            r0 = 1
            r2.set(r0)
            r1.A03 = r4
            r0 = 3
            X.AnonymousClass11F.A0C(r0, r2, r3)
            return r1
        L_0x24a4:
            r1 = r2
            X.1Jr r1 = (X.C22041Jr) r1
            java.util.List r7 = r1.A01
            int r6 = r1.A00
            boolean r1 = X.C013509w.A02(r7)
            if (r1 != 0) goto L_0x3797
            X.1wd r5 = X.C16980y8.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r5.A3D(r1)
            r4 = 0
            int r3 = r7.size()
        L_0x24bf:
            if (r4 >= r3) goto L_0x2e3a
            java.lang.Object r1 = r7.get(r4)
            X.0zR r1 = (X.C17770zR) r1
            if (r4 == 0) goto L_0x24db
            X.1kz r2 = X.C21841Ix.A00(r0)
            r2.A34(r1)
            X.10G r1 = X.AnonymousClass10G.LEFT
            r2.A2e(r1, r6)
            r5.A39(r2)
        L_0x24d8:
            int r4 = r4 + 1
            goto L_0x24bf
        L_0x24db:
            r5.A3A(r1)
            goto L_0x24d8
        L_0x24df:
            r1 = r2
            X.1Ju r1 = (X.C22071Ju) r1
            X.0zR r13 = r1.A01
            android.graphics.drawable.Drawable r12 = r1.A00
            X.1MF r11 = r1.A03
            X.1BY r10 = r1.A02
            java.util.List r9 = r1.A06
            java.util.List r6 = r1.A07
            X.1MD r8 = r1.A04
            java.lang.String r7 = r1.A05
            if (r12 != 0) goto L_0x24fc
            r3 = 0
            r2 = -1
            r1 = 520093696(0x1f000000, float:2.7105054E-20)
            android.graphics.drawable.Drawable r12 = X.AnonymousClass1M2.A01(r3, r2, r1)
        L_0x24fc:
            r2 = 1
            java.lang.String r1 = "content"
            java.lang.String[] r5 = new java.lang.String[]{r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r2)
            X.1NM r3 = new X.1NM
            r3.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x2515
            java.lang.String r1 = r1.A06
            r3.A07 = r1
        L_0x2515:
            r4.clear()
            if (r13 != 0) goto L_0x2558
            r1 = 0
        L_0x251b:
            r3.A01 = r1
            r1 = 0
            r4.set(r1)
            r3.A00 = r12
            r3.A08 = r10
            r3.A0A = r8
            r3.A0K = r7
            r3.A09 = r11
            int r2 = X.C22071Ju.A09
            int r1 = X.C22071Ju.A08
            X.0zR r1 = X.C22071Ju.A00(r0, r9, r2, r1)
            if (r1 == 0) goto L_0x253a
            if (r1 != 0) goto L_0x2553
            r1 = 0
        L_0x2538:
            r3.A02 = r1
        L_0x253a:
            int r2 = X.C22071Ju.A0B
            int r1 = X.C22071Ju.A0A
            X.0zR r0 = X.C22071Ju.A00(r0, r6, r2, r1)
            if (r0 == 0) goto L_0x2549
            if (r0 != 0) goto L_0x254e
            r0 = 0
        L_0x2547:
            r3.A03 = r0
        L_0x2549:
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x254e:
            X.0zR r0 = r0.A16()
            goto L_0x2547
        L_0x2553:
            X.0zR r1 = r1.A16()
            goto L_0x2538
        L_0x2558:
            X.0zR r1 = r13.A16()
            goto L_0x251b
        L_0x255d:
            r1 = r2
            X.1M4 r1 = (X.AnonymousClass1M4) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A00
            r3 = 2
            java.lang.String r2 = "iconName"
            java.lang.String r1 = "iconSize"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r3)
            X.1NO r3 = new X.1NO
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r2 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x2581
            java.lang.String r0 = r0.A06
            r3.A07 = r0
        L_0x2581:
            r4.clear()
            X.1qL r0 = X.C34891qL.A0x
            r3.A00 = r0
            r0 = 0
            r4.set(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r3.A03 = r0
            r0 = 1
            r4.set(r0)
            r3.A02 = r6
            r0 = 2131826915(0x7f1118e3, float:1.9286728E38)
            java.lang.String r0 = r2.A09(r0)
            r3.A04 = r0
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x25a4:
            r1 = r2
            X.1M3 r1 = (X.AnonymousClass1M3) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A01
            int r3 = X.AnonymousClass1Y3.AJa
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1MT r7 = (X.AnonymousClass1MT) r7
            r4 = 1
            java.lang.String r1 = "icon"
            java.lang.String[] r3 = new java.lang.String[]{r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r4)
            X.1NP r1 = new X.1NP
            r1.<init>()
            X.0z4 r5 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x25cf
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x25cf:
            r2.clear()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            int r0 = X.AnonymousClass1JG.A00(r0)
            float r0 = (float) r0
            int r0 = r5.A00(r0)
            r1.A04 = r0
            X.1qL r4 = X.C34891qL.A0C
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = r7.A02(r4, r0)
            android.graphics.drawable.Drawable r0 = r5.A07(r0)
            r1.A05 = r0
            r0 = 0
            r2.set(r0)
            int r0 = r6.Aqi()
            r1.A01 = r0
            android.content.res.ColorStateList r0 = r6.AfT()
            int r0 = X.AnonymousClass1KA.A00(r0)
            r1.A02 = r0
            android.content.res.ColorStateList r0 = r6.AfT()
            int r0 = X.AnonymousClass1KA.A01(r0)
            r1.A03 = r0
            r0 = 2131826916(0x7f1118e4, float:1.928673E38)
            java.lang.String r0 = r5.A09(r0)
            r1.A06 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r2, r3)
            return r1
        L_0x2619:
            r1 = r2
            X.1NL r1 = (X.AnonymousClass1NL) r1
            boolean r4 = r1.A02
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A01
            int r3 = X.AnonymousClass1Y3.AJa
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1MT r7 = (X.AnonymousClass1MT) r7
            r5 = 1
            java.lang.String r1 = "icon"
            java.lang.String[] r3 = new java.lang.String[]{r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r5)
            X.1NP r1 = new X.1NP
            r1.<init>()
            X.0z4 r5 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x2646
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x2646:
            r2.clear()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            int r0 = X.AnonymousClass1JG.A00(r0)
            float r0 = (float) r0
            int r0 = r5.A00(r0)
            r1.A04 = r0
            r1.A07 = r4
            X.1qL r4 = X.C34891qL.A1B
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = r7.A02(r4, r0)
            android.graphics.drawable.Drawable r0 = r5.A07(r0)
            r1.A05 = r0
            r0 = 0
            r2.set(r0)
            int r0 = r6.Aqi()
            r1.A01 = r0
            android.content.res.ColorStateList r0 = r6.AfV()
            int r0 = X.AnonymousClass1KA.A00(r0)
            r1.A02 = r0
            android.content.res.ColorStateList r0 = r6.AfV()
            int r0 = X.AnonymousClass1KA.A01(r0)
            r1.A03 = r0
            r0 = 2131826917(0x7f1118e5, float:1.9286732E38)
            java.lang.String r0 = r5.A09(r0)
            r1.A06 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r2, r3)
            return r1
        L_0x2692:
            r1 = r2
            X.1NK r1 = (X.AnonymousClass1NK) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A00
            r3 = 2
            java.lang.String r2 = "iconName"
            java.lang.String r1 = "iconSize"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r3)
            X.1NO r3 = new X.1NO
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r2 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x26b6
            java.lang.String r0 = r0.A06
            r3.A07 = r0
        L_0x26b6:
            r4.clear()
            X.1qL r0 = X.C34891qL.A0v
            r3.A00 = r0
            r0 = 0
            r4.set(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r3.A03 = r0
            r0 = 1
            r4.set(r0)
            r3.A02 = r6
            r0 = 2131826918(0x7f1118e6, float:1.9286734E38)
            java.lang.String r0 = r2.A09(r0)
            r3.A04 = r0
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x26d9:
            r1 = r2
            X.1vL r1 = (X.AnonymousClass1vL) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A01
            int r3 = X.AnonymousClass1Y3.AJa
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1MT r7 = (X.AnonymousClass1MT) r7
            r4 = 1
            java.lang.String r1 = "icon"
            java.lang.String[] r3 = new java.lang.String[]{r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r4)
            X.1NP r1 = new X.1NP
            r1.<init>()
            X.0z4 r5 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x2704
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x2704:
            r2.clear()
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            int r0 = X.AnonymousClass1JG.A00(r0)
            float r0 = (float) r0
            int r0 = r5.A00(r0)
            r1.A04 = r0
            X.1qL r4 = X.C34891qL.A07
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            int r0 = r7.A02(r4, r0)
            android.graphics.drawable.Drawable r0 = r5.A07(r0)
            r1.A05 = r0
            r0 = 0
            r2.set(r0)
            int r0 = r6.Aqi()
            r1.A01 = r0
            android.content.res.ColorStateList r0 = r6.AfU()
            int r0 = X.AnonymousClass1KA.A00(r0)
            r1.A02 = r0
            android.content.res.ColorStateList r0 = r6.AfU()
            int r0 = X.AnonymousClass1KA.A01(r0)
            r1.A03 = r0
            r0 = 2131826919(0x7f1118e7, float:1.9286736E38)
            java.lang.String r0 = r5.A09(r0)
            r1.A06 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r2, r3)
            return r1
        L_0x274e:
            r1 = r2
            X.1Rj r1 = (X.C23891Rj) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A00
            r3 = 2
            java.lang.String r2 = "iconName"
            java.lang.String r1 = "iconSize"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r3)
            X.1NO r3 = new X.1NO
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r2 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x2772
            java.lang.String r0 = r0.A06
            r3.A07 = r0
        L_0x2772:
            r4.clear()
            X.1qL r0 = X.C34891qL.A06
            r3.A00 = r0
            r0 = 0
            r4.set(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r3.A03 = r0
            r0 = 1
            r4.set(r0)
            r3.A02 = r6
            r0 = 2131826920(0x7f1118e8, float:1.9286738E38)
            java.lang.String r0 = r2.A09(r0)
            r3.A04 = r0
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x2795:
            r1 = r2
            X.1M5 r1 = (X.AnonymousClass1M5) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A00
            r3 = 2
            java.lang.String r2 = "iconName"
            java.lang.String r1 = "iconSize"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r3)
            X.1NO r3 = new X.1NO
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r2 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x27b9
            java.lang.String r0 = r0.A06
            r3.A07 = r0
        L_0x27b9:
            r4.clear()
            X.1qL r0 = X.C34891qL.A0A
            r3.A00 = r0
            r0 = 0
            r4.set(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r3.A03 = r0
            r0 = 1
            r4.set(r0)
            r3.A02 = r6
            r0 = 2131826921(0x7f1118e9, float:1.928674E38)
            java.lang.String r0 = r2.A09(r0)
            r3.A04 = r0
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x27dc:
            r1 = r2
            X.1QW r1 = (X.AnonymousClass1QW) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A03
            X.4rp r5 = r1.A02
            X.4rt r4 = r1.A01
            int r3 = X.AnonymousClass1Y3.Axb
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.7LM r1 = (X.AnonymousClass7LM) r1
            X.7Kf r3 = r1.A01(r0, r6)
            java.lang.Boolean r1 = r5.A00
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x280b
            r1 = 2131835148(0x7f11390c, float:1.9303426E38)
            java.lang.String r2 = r0.A0C(r1)
            X.4ry r1 = new X.4ry
            r1.<init>(r4)
            r3.A09(r2, r1)
        L_0x280b:
            java.lang.Boolean r1 = r5.A00
            boolean r1 = r1.booleanValue()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x2824
            r1 = 2131827534(0x7f111b4e, float:1.9287983E38)
            java.lang.String r2 = r0.A0C(r1)
            X.4rx r1 = new X.4rx
            r1.<init>(r4)
            r3.A09(r2, r1)
        L_0x2824:
            java.lang.Boolean r1 = r5.A00
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x283b
            r1 = 2131835149(0x7f11390d, float:1.9303428E38)
            java.lang.String r2 = r0.A0C(r1)
            X.4rv r1 = new X.4rv
            r1.<init>(r4)
            r3.A09(r2, r1)
        L_0x283b:
            java.lang.Boolean r1 = r5.A00
            boolean r1 = r1.booleanValue()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x2854
            r1 = 2131827532(0x7f111b4c, float:1.928798E38)
            java.lang.String r2 = r0.A0C(r1)
            X.4rw r1 = new X.4rw
            r1.<init>(r4)
            r3.A09(r2, r1)
        L_0x2854:
            java.lang.Boolean r1 = r5.A00
            boolean r1 = r1.booleanValue()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x286d
            r1 = 2131827573(0x7f111b75, float:1.9288062E38)
            java.lang.String r2 = r0.A0C(r1)
            X.4ru r1 = new X.4ru
            r1.<init>(r4)
            r3.A09(r2, r1)
        L_0x286d:
            r1 = 2131827533(0x7f111b4d, float:1.9287981E38)
            java.lang.String r2 = r0.A0C(r1)
            X.4s0 r1 = new X.4s0
            r1.<init>(r4)
            r3.A09(r2, r1)
            X.0Tq r1 = r5.A01
            java.lang.Object r1 = r1.get()
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x2b41
            r1 = 2131823632(0x7f110c10, float:1.928007E38)
            java.lang.String r1 = r0.A0C(r1)
            X.4rz r0 = new X.4rz
            r0.<init>(r4)
            r3.A09(r1, r0)
            goto L_0x2b41
        L_0x289b:
            r1 = r2
            X.1vK r1 = (X.AnonymousClass1vK) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r1.A02
            X.DGF r8 = r1.A01
            com.google.common.collect.ImmutableList r6 = r1.A03
            int r2 = X.AnonymousClass1Y3.ACY
            X.0UN r3 = r1.A00
            r1 = 2
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.5nJ r5 = (X.C120895nJ) r5
            int r2 = X.AnonymousClass1Y3.Axb
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.7LM r1 = (X.AnonymousClass7LM) r1
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            X.7Kf r4 = r1.A01(r0, r7)
            r1 = 2131821825(0x7f110501, float:1.9276404E38)
            java.lang.String r11 = r0.A0C(r1)
            android.content.Context r10 = r0.A09
            X.DGT r9 = new X.DGT
            r9.<init>(r8)
            r1 = 2131821820(0x7f1104fc, float:1.9276394E38)
            java.lang.String r3 = r10.getString(r1)
            r1 = 0
            android.text.SpannableString r1 = X.C141526ih.A00(r10, r7, r3, r1, r9)
            r4.A0A(r11, r1)
            r1 = 2131821824(0x7f110500, float:1.9276402E38)
            java.lang.String r3 = r0.A0C(r1)
            r1 = 2131821823(0x7f1104ff, float:1.92764E38)
            java.lang.String r1 = r0.A0C(r1)
            r4.A0A(r3, r1)
            r1 = 2131821844(0x7f110514, float:1.9276443E38)
            r4.A03(r1)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r6)
            java.util.Comparator r1 = X.C22829BEj.A03
            java.util.Collections.sort(r3, r1)
            java.util.Iterator r3 = r3.iterator()
        L_0x2902:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x2916
            java.lang.Object r1 = r3.next()
            X.BEj r1 = (X.C22829BEj) r1
            X.2rz r1 = r5.A01(r0, r7, r1, r8)
            r4.A07(r1)
            goto L_0x2902
        L_0x2916:
            android.content.res.Resources r1 = r0.A03()
            java.lang.CharSequence r1 = X.C123305rP.A00(r1, r6)
            r4.A08(r1)
            X.0zR r1 = r4.A01()
            r2.A3A(r1)
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape1_0S0300000 r6 = X.C48092Zm.A00(r0)
            java.lang.Object r1 = r6.A02
            X.2Zm r1 = (X.C48092Zm) r1
            r1.A01 = r7
            r1 = 2131821822(0x7f1104fe, float:1.9276398E38)
            java.lang.String r1 = r0.A0C(r1)
            r6.A3D(r1)
            java.lang.Class<X.1vK> r5 = X.AnonymousClass1vK.class
            java.lang.Object[] r4 = new java.lang.Object[]{r0}
            r1 = 2019308025(0x785c31f9, float:1.7864357E34)
            X.10N r1 = A0E(r5, r0, r1, r4)
            r6.A2y(r1)
            X.2Zm r1 = r6.A35()
            r3.A3A(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r6 = X.C115165dU.A00(r0)
            java.lang.Object r1 = r6.A02
            X.5dU r1 = (X.C115165dU) r1
            r1.A00 = r7
            r1 = 2131821821(0x7f1104fd, float:1.9276396E38)
            java.lang.String r4 = r0.A0C(r1)
            java.lang.Object r1 = r6.A02
            X.5dU r1 = (X.C115165dU) r1
            r1.A01 = r4
            java.lang.Object r4 = r6.A00
            java.util.BitSet r4 = (java.util.BitSet) r4
            r1 = 0
            r4.set(r1)
            java.lang.Object[] r4 = new java.lang.Object[]{r0}
            r1 = -1254653778(0xffffffffb53780ae, float:-6.8360066E-7)
            X.10N r0 = A0E(r5, r0, r1, r4)
            r6.A2y(r0)
            X.10G r1 = X.AnonymousClass10G.TOP
            X.1JQ r0 = X.AnonymousClass1JQ.LARGE
            int r0 = r0.B3A()
            float r0 = (float) r0
            r6.A2X(r1, r0)
            X.5dU r0 = r6.A3A()
            r3.A3A(r0)
            r0 = 0
            r3.A1n(r0)
            r3.A1o(r0)
            r0 = 1120403456(0x42c80000, float:100.0)
            r3.A20(r0)
            X.10G r1 = X.AnonymousClass10G.A09
            X.1JQ r0 = X.AnonymousClass1JQ.XLARGE
            int r0 = r0.B3A()
            float r0 = (float) r0
            r3.A2Z(r1, r0)
            X.10G r1 = X.AnonymousClass10G.A04
            X.1JQ r0 = X.AnonymousClass1JQ.XLARGE
            int r0 = r0.B3A()
            float r0 = (float) r0
            r3.A2Z(r1, r0)
            X.11D r0 = r3.A00
            r2.A3A(r0)
            X.10G r1 = X.AnonymousClass10G.TOP
            X.1JQ r0 = X.AnonymousClass1JQ.XLARGE
            int r0 = r0.B3A()
            float r0 = (float) r0
            r2.A2Z(r1, r0)
            X.11D r0 = r2.A00
            return r0
        L_0x29cf:
            r1 = r2
            X.1vJ r1 = (X.AnonymousClass1vJ) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r9 = r1.A02
            com.google.common.collect.ImmutableList r8 = r1.A03
            X.DGF r4 = r1.A01
            boolean r5 = r1.A05
            X.0Tq r6 = r1.A04
            int r2 = X.AnonymousClass1Y3.ACY
            X.0UN r3 = r1.A00
            r1 = 1
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.5nJ r7 = (X.C120895nJ) r7
            int r2 = X.AnonymousClass1Y3.Axb
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r2, r3)
            X.7LM r1 = (X.AnonymousClass7LM) r1
            java.lang.Object r6 = r6.get()
            com.facebook.user.model.User r6 = (com.facebook.user.model.User) r6
            X.7Kf r3 = r1.A01(r0, r9)
            r1 = 2131821844(0x7f110514, float:1.9276443E38)
            r3.A03(r1)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r8)
            java.util.Comparator r1 = X.C22829BEj.A03
            java.util.Collections.sort(r2, r1)
            java.util.Iterator r2 = r2.iterator()
        L_0x2a0e:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x2a22
            java.lang.Object r1 = r2.next()
            X.BEj r1 = (X.C22829BEj) r1
            X.2rz r1 = r7.A01(r0, r9, r1, r4)
            r3.A07(r1)
            goto L_0x2a0e
        L_0x2a22:
            android.content.res.Resources r0 = r0.A03()
            java.lang.CharSequence r0 = X.C123305rP.A00(r0, r8)
            r3.A08(r0)
            if (r6 == 0) goto L_0x2b41
            boolean r0 = r6.A14
            if (r0 == 0) goto L_0x2b41
            r3.A02()
            java.lang.String r0 = "Internal Only"
            r3.A0E(r0)
            X.DGS r2 = new X.DGS
            r2.<init>(r4)
            java.lang.String r1 = "Active Status Broadcast"
            java.lang.String r0 = "Whether this app broadcasts Active Status"
            r3.A0C(r1, r0, r5, r2)
            X.BdO r2 = new X.BdO
            r2.<init>(r4)
            java.lang.String r1 = "Roll Back Migration"
            java.lang.String r0 = "Rolls back the user to the un-migrated state, causes the onboarding screen to show"
            r3.A0B(r1, r0, r2)
            goto L_0x2b41
        L_0x2a55:
            r1 = r2
            X.1RW r1 = (X.AnonymousClass1RW) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r5 = r1.A02
            boolean r9 = r1.A06
            java.lang.Boolean r11 = r1.A05
            r15 = 0
            java.lang.Boolean r10 = r1.A03
            java.lang.Boolean r6 = r1.A04
            r13 = 0
            boolean r8 = r1.A07
            boolean r7 = r1.A08
            X.DCo r4 = r1.A01
            int r3 = X.AnonymousClass1Y3.Axb
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.7LM r1 = (X.AnonymousClass7LM) r1
            X.7Kf r3 = r1.A01(r0, r5)
            r1 = 2131829264(0x7f112210, float:1.9291492E38)
            r3.A03(r1)
            r1 = 2131829267(0x7f112213, float:1.9291498E38)
            java.lang.String r5 = r0.A0C(r1)
            r1 = 2131829266(0x7f112212, float:1.9291496E38)
            java.lang.String r2 = r0.A0C(r1)
            X.DCe r1 = new X.DCe
            r1.<init>(r4)
            r3.A0C(r5, r2, r9, r1)
            if (r11 == 0) goto L_0x2ab1
            r1 = 2131829263(0x7f11220f, float:1.929149E38)
            java.lang.String r9 = r0.A0C(r1)
            r1 = 2131829262(0x7f11220e, float:1.9291488E38)
            java.lang.String r5 = r0.A0C(r1)
            boolean r2 = r11.booleanValue()
            X.DCf r1 = new X.DCf
            r1.<init>(r4)
            r3.A0C(r9, r5, r2, r1)
        L_0x2ab1:
            if (r15 == 0) goto L_0x2ae3
            r1 = 2131829261(0x7f11220d, float:1.9291486E38)
            r3.A03(r1)
            if (r10 == 0) goto L_0x2ace
            r1 = 2131829255(0x7f112207, float:1.9291474E38)
            java.lang.String r5 = r0.A0C(r1)
            boolean r2 = r10.booleanValue()
            X.DCi r1 = new X.DCi
            r1.<init>(r4)
            r3.A0D(r5, r2, r1)
        L_0x2ace:
            if (r6 == 0) goto L_0x2ae3
            r1 = 2131829260(0x7f11220c, float:1.9291484E38)
            java.lang.String r5 = r0.A0C(r1)
            boolean r2 = r6.booleanValue()
            X.DCg r1 = new X.DCg
            r1.<init>(r4)
            r3.A0D(r5, r2, r1)
        L_0x2ae3:
            if (r15 == 0) goto L_0x2b07
            if (r13 == 0) goto L_0x2b07
            r1 = 2131829252(0x7f112204, float:1.9291468E38)
            r3.A03(r1)
            r1 = 2131829254(0x7f112206, float:1.9291472E38)
            java.lang.String r6 = r0.A0C(r1)
            r1 = 2131829253(0x7f112205, float:1.929147E38)
            java.lang.String r5 = r0.A0C(r1)
            boolean r2 = r13.booleanValue()
            X.DCj r1 = new X.DCj
            r1.<init>(r4)
            r3.A0C(r6, r5, r2, r1)
        L_0x2b07:
            if (r15 == 0) goto L_0x2b18
            r1 = 2131829256(0x7f112208, float:1.9291476E38)
            java.lang.String r2 = r0.A0C(r1)
            X.DCm r1 = new X.DCm
            r1.<init>(r4)
            r3.A09(r2, r1)
        L_0x2b18:
            if (r8 == 0) goto L_0x2b29
            r1 = 2131829265(0x7f112211, float:1.9291494E38)
            java.lang.String r2 = r0.A0C(r1)
            X.DCh r1 = new X.DCh
            r1.<init>(r4)
            r3.A09(r2, r1)
        L_0x2b29:
            if (r7 == 0) goto L_0x2b41
            r1 = 2131829258(0x7f11220a, float:1.929148E38)
            java.lang.String r2 = r0.A0C(r1)
            r1 = 2131829257(0x7f112209, float:1.9291478E38)
            java.lang.String r1 = r0.A0C(r1)
            X.DCk r0 = new X.DCk
            r0.<init>(r4)
            r3.A0B(r2, r1, r0)
        L_0x2b41:
            X.0zR r0 = r3.A01()
            return r0
        L_0x2b46:
            r1 = r2
            X.1vI r1 = (X.AnonymousClass1vI) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r1.A02
            boolean r6 = r1.A03
            X.2oT r5 = r1.A01
            int r3 = X.AnonymousClass1Y3.Axb
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.7LM r4 = (X.AnonymousClass7LM) r4
            r1 = 2131834134(0x7f113516, float:1.930137E38)
            if (r6 == 0) goto L_0x2b62
            r1 = 2131834135(0x7f113517, float:1.9301372E38)
        L_0x2b62:
            java.lang.String r3 = r0.A0C(r1)
            r2 = 2131827122(0x7f1119b2, float:1.9287148E38)
            if (r6 == 0) goto L_0x2b6e
            r2 = 2131827123(0x7f1119b3, float:1.928715E38)
        L_0x2b6e:
            X.7Kf r1 = r4.A01(r0, r7)
            r1.A0D(r3, r6, r5)
            X.0p4 r0 = r1.A01
            java.lang.String r0 = r0.A0C(r2)
            r1.A08(r0)
            X.0zR r0 = r1.A01()
            return r0
        L_0x2b83:
            r1 = r2
            X.1QV r1 = (X.AnonymousClass1QV) r1
            com.facebook.mig.scheme.interfaces.MigColorScheme r8 = r1.A03
            X.DBo r5 = r1.A02
            java.lang.Boolean r10 = r1.A05
            java.lang.Boolean r9 = r1.A04
            java.lang.String r7 = r1.A06
            boolean r6 = r1.A07
            X.DC3 r3 = r1.A01
            int r4 = X.AnonymousClass1Y3.Axb
            X.0UN r2 = r1.A00
            r1 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.7LM r1 = (X.AnonymousClass7LM) r1
            X.7Kf r4 = r1.A01(r0, r8)
            if (r10 == 0) goto L_0x2bbd
            android.preference.Preference r0 = r5.A03()
            java.lang.CharSequence r8 = r0.getTitle()
            java.lang.CharSequence r2 = r0.getSummary()
            boolean r1 = r10.booleanValue()
            X.DBm r0 = new X.DBm
            r0.<init>(r3)
            r4.A0C(r8, r2, r1, r0)
        L_0x2bbd:
            if (r9 == 0) goto L_0x2bd3
            android.preference.Preference r0 = r5.A02()
            java.lang.CharSequence r2 = r0.getTitle()
            boolean r1 = r9.booleanValue()
            X.DBt r0 = new X.DBt
            r0.<init>(r3)
            r4.A0D(r2, r1, r0)
        L_0x2bd3:
            if (r7 == 0) goto L_0x2be5
            android.preference.Preference r0 = r5.A04()
            java.lang.CharSequence r1 = r0.getTitle()
            X.DBq r0 = new X.DBq
            r0.<init>(r3)
            r4.A0B(r1, r7, r0)
        L_0x2be5:
            if (r6 == 0) goto L_0x2c08
            android.preference.Preference r0 = r5.A01
            if (r0 != 0) goto L_0x2bfa
            android.preference.Preference r1 = new android.preference.Preference
            android.content.Context r0 = r5.A00
            r1.<init>(r0)
            r5.A01 = r1
            r0 = 2131830665(0x7f112789, float:1.9294334E38)
            r1.setTitle(r0)
        L_0x2bfa:
            android.preference.Preference r0 = r5.A01
            java.lang.CharSequence r1 = r0.getTitle()
            X.DBu r0 = new X.DBu
            r0.<init>(r3)
            r4.A09(r1, r0)
        L_0x2c08:
            X.0zR r0 = r4.A01()
            return r0
        L_0x2c0d:
            r1 = r2
            X.1NO r1 = (X.AnonymousClass1NO) r1
            X.1qL r11 = r1.A00
            java.lang.Integer r8 = r1.A03
            boolean r6 = r1.A05
            com.facebook.mig.scheme.interfaces.MigColorScheme r5 = r1.A02
            java.lang.String r4 = r1.A04
            int r3 = X.AnonymousClass1Y3.AJa
            X.0UN r2 = r1.A01
            r1 = 0
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1MT r10 = (X.AnonymousClass1MT) r10
            X.1qL r2 = X.C34891qL.A0h
            if (r11 == r2) goto L_0x2c2a
            r1 = 1
        L_0x2c2a:
            com.google.common.base.Preconditions.checkArgument(r1)
            r9 = 0
            r7 = 1
            java.lang.String r1 = "icon"
            java.lang.String[] r3 = new java.lang.String[]{r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r7)
            X.1NP r12 = new X.1NP
            r12.<init>()
            X.0z4 r7 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x2c49
            java.lang.String r0 = r0.A06
            r12.A07 = r0
        L_0x2c49:
            r2.clear()
            int r0 = r10.A02(r11, r8)
            android.graphics.drawable.Drawable r0 = r7.A07(r0)
            r12.A05 = r0
            r2.set(r9)
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r8 != r0) goto L_0x2c5f
            java.lang.Integer r0 = X.AnonymousClass07B.A01
        L_0x2c5f:
            int r0 = X.AnonymousClass1JG.A00(r0)
            float r0 = (float) r0
            int r0 = r7.A00(r0)
            r12.A04 = r0
            r12.A07 = r6
            int r0 = r5.AzK()
            r12.A01 = r0
            int r0 = r5.AkX()
            r12.A00 = r0
            android.content.res.ColorStateList r0 = r5.AfS()
            int r0 = X.AnonymousClass1KA.A00(r0)
            r12.A02 = r0
            android.content.res.ColorStateList r0 = r5.AfS()
            int r0 = X.AnonymousClass1KA.A01(r0)
            r12.A03 = r0
            r12.A06 = r4
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r2, r3)
            return r12
        L_0x2c93:
            r1 = r2
            X.1NP r1 = (X.AnonymousClass1NP) r1
            android.graphics.drawable.Drawable r10 = r1.A05
            int r9 = r1.A04
            boolean r7 = r1.A07
            int r6 = r1.A01
            int r5 = r1.A00
            int r4 = r1.A02
            int r3 = r1.A03
            java.lang.String r8 = r1.A06
            if (r9 != 0) goto L_0x2cb5
            android.content.Context r2 = r0.A09
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            int r1 = X.AnonymousClass1JG.A00(r1)
            float r1 = (float) r1
            int r9 = X.C007106r.A00(r2, r1)
        L_0x2cb5:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000 r2 = X.C22291Kt.A00(r0)
            r2.A27(r9)
            r2.A2F(r9)
            r2.A37(r10)
            r2.A2z(r8)
            r0 = 2
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            r2.A2n(r0)
            android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.CENTER
            java.lang.Object r0 = r2.A00
            X.1Kt r0 = (X.C22291Kt) r0
            r0.A02 = r1
            int r0 = r9 >> 1
            float r0 = (float) r0
            if (r7 == 0) goto L_0x2ce9
            r2.A33(r6)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass1M2.A01(r0, r4, r3)
            r2.A2H(r0)
        L_0x2ce4:
            java.lang.Object r0 = r2.A00
            X.1Kt r0 = (X.C22291Kt) r0
            return r0
        L_0x2ce9:
            r2.A33(r5)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass1M2.A05(r4, r0)
            r2.A2H(r0)
            goto L_0x2ce4
        L_0x2cf4:
            r1 = r2
            X.1Sx r1 = (X.C24261Sx) r1
            java.util.List r6 = r1.A05
            int r4 = r1.A00
            int r12 = r1.A03
            int r11 = r1.A01
            int r7 = r1.A02
            boolean r8 = r1.A06
            com.facebook.mig.scheme.interfaces.MigColorScheme r9 = r1.A04
            r5 = -1
            if (r9 != 0) goto L_0x2d0c
            com.facebook.mig.scheme.interfaces.MigColorScheme r9 = X.C17190yT.A00()
        L_0x2d0c:
            r10 = 1
            r1 = 0
            if (r4 <= 0) goto L_0x2d11
            r1 = 1
        L_0x2d11:
            com.google.common.base.Preconditions.checkArgument(r1)
            boolean r1 = r6.isEmpty()
            if (r1 != 0) goto L_0x3797
            r3 = 16
            r1 = -1
            if (r12 != r1) goto L_0x2d26
            android.content.Context r2 = r0.A09
            float r1 = (float) r3
            int r12 = X.C007106r.A00(r2, r1)
        L_0x2d26:
            r3 = 2
            r1 = -1
            if (r11 != r1) goto L_0x2d31
            android.content.Context r2 = r0.A09
            float r1 = (float) r3
            int r11 = X.C007106r.A00(r2, r1)
        L_0x2d31:
            r3 = 4
            r1 = -1
            if (r7 != r1) goto L_0x2d3c
            android.content.Context r2 = r0.A09
            float r1 = (float) r3
            int r7 = X.C007106r.A00(r2, r1)
        L_0x2d3c:
            if (r8 == 0) goto L_0x2d98
            int r1 = r6.size()
            if (r1 <= r4) goto L_0x2d98
            int r4 = r4 - r10
        L_0x2d45:
            r14 = 1
        L_0x2d46:
            X.1wd r5 = X.C16980y8.A00(r0)
            r8 = 0
        L_0x2d4b:
            if (r8 >= r4) goto L_0x2db4
            java.lang.Object r2 = r6.get(r8)
            com.facebook.user.model.UserKey r2 = (com.facebook.user.model.UserKey) r2
            r13 = 0
            if (r8 <= 0) goto L_0x2d57
            r13 = 1
        L_0x2d57:
            r1 = 0
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r10 = X.C21911Je.A00(r0, r1, r1)
            r10.A2F(r12)
            r10.A27(r12)
            X.1JY r1 = X.AnonymousClass1JY.A04(r2)
            r10.A3h(r1)
            if (r13 != 0) goto L_0x2d8a
            android.graphics.Path r2 = X.AnonymousClass1K7.A01(r12)
        L_0x2d6f:
            java.lang.Object r1 = r10.A00
            X.1Je r1 = (X.C21911Je) r1
            r1.A08 = r2
            r1.A06 = r12
            X.10G r2 = X.AnonymousClass10G.START
            r1 = 0
            if (r13 == 0) goto L_0x2d7d
            int r1 = -r7
        L_0x2d7d:
            r10.A2e(r2, r1)
            X.1Je r1 = r10.A3A()
            r5.A3A(r1)
            int r8 = r8 + 1
            goto L_0x2d4b
        L_0x2d8a:
            int r1 = r12 - r7
            float r3 = (float) r1
            float r1 = (float) r12
            float r3 = r3 / r1
            float r2 = (float) r11
            float r2 = r2 / r1
            r1 = 1127481344(0x43340000, float:180.0)
            android.graphics.Path r2 = X.AnonymousClass1K7.A00(r3, r2, r1, r12)
            goto L_0x2d6f
        L_0x2d98:
            int r1 = r6.size()
            if (r1 < r5) goto L_0x2daa
            if (r5 > r4) goto L_0x2daa
            int r1 = r6.size()
            int r4 = java.lang.Math.min(r4, r1)
            r14 = 0
            goto L_0x2d46
        L_0x2daa:
            int r4 = r4 - r10
            int r1 = r6.size()
            int r4 = java.lang.Math.min(r4, r1)
            goto L_0x2d45
        L_0x2db4:
            if (r14 == 0) goto L_0x2e3a
            int r2 = r6.size()
            int r2 = r2 - r4
            r10 = r12
            X.1wd r6 = X.C16980y8.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r6.A3D(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r4 = X.C14910uL.A02(r0)
            java.lang.String r1 = "+"
            java.lang.String r1 = X.AnonymousClass08S.A09(r1, r2)
            r4.A3j(r1)
            X.0y3 r1 = r9.Aqj()
            int r1 = r1.AhV()
            r4.A3K(r1)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r4.A3W(r1)
            r1 = 1065353216(0x3f800000, float:1.0)
            r4.A1n(r1)
            android.content.Context r1 = r0.A09
            float r8 = (float) r12
            int r1 = X.C007106r.A01(r1, r8)
            int r1 = r1 / 3
            float r1 = (float) r1
            r4.A3E(r1)
            android.content.Context r3 = r0.A09
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            X.1l0 r1 = X.C32031l0.A05
            r0 = 0
            android.graphics.Typeface r0 = X.C21871Ja.A01(r3, r2, r1, r0)
            com.google.common.base.Preconditions.checkNotNull(r0)
            r4.A3T(r0)
            r6.A39(r4)
            r6.A2F(r12)
            r6.A27(r12)
            android.graphics.drawable.ShapeDrawable r4 = new android.graphics.drawable.ShapeDrawable
            android.graphics.drawable.shapes.PathShape r3 = new android.graphics.drawable.shapes.PathShape
            int r12 = r12 - r7
            float r2 = (float) r12
            float r2 = r2 / r8
            float r1 = (float) r11
            float r1 = r1 / r8
            r0 = 1127481344(0x43340000, float:180.0)
            android.graphics.Path r0 = X.AnonymousClass1K7.A00(r2, r1, r0, r10)
            r3.<init>(r0, r8, r8)
            r4.<init>(r3)
            int r1 = r9.AmP()
            android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.SRC_IN
            r4.setColorFilter(r1, r0)
            r6.A2H(r4)
            X.10G r1 = X.AnonymousClass10G.START
            int r0 = -r7
            r6.A2e(r1, r0)
            X.0y8 r0 = r6.A00
            r5.A3A(r0)
        L_0x2e3a:
            X.0y8 r0 = r5.A00
            return r0
        L_0x2e3d:
            X.1J3 r2 = (X.AnonymousClass1J3) r2
            com.google.common.collect.ImmutableList r1 = r2.A07
            r20 = r1
            com.google.common.collect.ImmutableList r3 = r2.A06
            X.1J0 r13 = r2.A04
            X.1Jy r12 = r2.A03
            int r7 = r2.A00
            boolean r6 = r2.A08
            com.facebook.mig.scheme.interfaces.MigColorScheme r11 = r2.A05
            com.facebook.messaging.model.messages.Message r5 = r2.A02
            int r4 = X.AnonymousClass1Y3.A8h
            X.0UN r2 = r2.A01
            r1 = 0
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.1H8 r10 = (X.AnonymousClass1H8) r10
            int r2 = r20.size()
            r4 = 1
            r8 = 0
            if (r2 < 0) goto L_0x2e65
            r1 = 1
        L_0x2e65:
            com.google.common.base.Preconditions.checkArgument(r1)
            int r1 = r20.size()
            r2 = 0
            if (r1 == 0) goto L_0x30e3
            if (r1 == r4) goto L_0x3050
            r1 = r20
            java.lang.Object r9 = r1.get(r8)
            com.facebook.user.model.UserKey r9 = (com.facebook.user.model.UserKey) r9
            java.lang.Object r8 = X.AnonymousClass0j4.A04(r3, r8, r2)
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r7 = r1.get(r4)
            com.facebook.user.model.UserKey r7 = (com.facebook.user.model.UserKey) r7
            java.lang.Object r3 = X.AnonymousClass0j4.A04(r3, r4, r2)
            java.lang.String r3 = (java.lang.String) r3
            X.1we r6 = X.AnonymousClass11D.A00(r0)
            int r1 = r13.A05
            float r1 = (float) r1
            r6.A1z(r1)
            int r1 = r13.A05
            float r1 = (float) r1
            r6.A1p(r1)
            android.content.Context r2 = r0.A09
            int r1 = r13.A08
            float r1 = (float) r1
            int r18 = X.C007106r.A00(r2, r1)
            java.lang.String r17 = "params"
            java.lang.String[] r16 = new java.lang.String[]{r17}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r4)
            X.1Je r4 = new X.1Je
            android.content.Context r1 = r0.A09
            r4.<init>(r1)
            X.0z4 r1 = r0.A0B
            r19 = r1
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x2ec2
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x2ec2:
            r5.clear()
            int r1 = X.AnonymousClass1J3.A00(r13, r11)
            X.1JY r1 = X.AnonymousClass1KQ.A00(r7, r3, r1)
            r4.A0B = r1
            r1 = 0
            r5.set(r1)
            int r3 = r13.A08
            int r2 = r3 >> 1
            int r1 = r13.A05
            int r1 = r1 - r2
            int r14 = r1 - r2
            double r14 = (double) r14
            int r2 = r2 - r1
            double r1 = (double) r2
            double r1 = java.lang.Math.hypot(r14, r1)
            float r14 = (float) r1
            float r1 = (float) r3
            float r14 = r14 / r1
            r3 = 1077936128(0x40400000, float:3.0)
            float r3 = r3 / r1
            r2 = 1124532224(0x43070000, float:135.0)
            r1 = r18
            android.graphics.Path r1 = X.AnonymousClass1K7.A00(r14, r3, r2, r1)
            r4.A08 = r1
            android.content.Context r2 = r0.A09
            int r1 = r13.A08
            float r1 = (float) r1
            int r1 = X.C007106r.A00(r2, r1)
            r4.A06 = r1
            X.1KU r1 = X.AnonymousClass1KU.TWO_LETTER
            r4.A0D = r1
            X.0y3 r1 = r11.B5U()
            int r1 = r1.AhV()
            r4.A07 = r1
            android.content.Context r2 = r0.A09
            int r1 = r13.A06
            float r1 = (float) r1
            int r1 = X.C007106r.A00(r2, r1)
            float r1 = (float) r1
            r4.A01 = r1
            X.10M r2 = X.AnonymousClass10M.ROBOTO_BOLD
            android.content.Context r1 = r0.A09
            android.graphics.Typeface r1 = r2.A00(r1)
            r4.A09 = r1
            X.1LC r2 = X.AnonymousClass1LC.A00
            X.11G r1 = r4.A14()
            r1.BxJ(r2)
            X.10G r3 = X.AnonymousClass10G.RIGHT
            r2 = 0
            r1 = r19
            int r2 = r1.A00(r2)
            X.11G r1 = r4.A14()
            r1.BxI(r3, r2)
            X.10G r3 = X.AnonymousClass10G.TOP
            r2 = 0
            r1 = r19
            int r2 = r1.A00(r2)
            X.11G r1 = r4.A14()
            r1.BxI(r3, r2)
            boolean r1 = r7.A08()
            if (r1 == 0) goto L_0x2f57
            int r1 = X.AnonymousClass1KV.A00(r11)
            r4.A05 = r1
        L_0x2f57:
            r2 = 1
            r1 = r16
            X.AnonymousClass11F.A0C(r2, r5, r1)
            r6.A3A(r4)
            java.lang.String[] r5 = new java.lang.String[]{r17}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r2)
            X.1Je r3 = new X.1Je
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r14 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x2f7a
            java.lang.String r2 = r1.A06
            r3.A07 = r2
        L_0x2f7a:
            r4.clear()
            int r1 = X.AnonymousClass1J3.A00(r13, r11)
            X.1JY r1 = X.AnonymousClass1KQ.A00(r9, r8, r1)
            r3.A0B = r1
            r1 = 0
            r4.set(r1)
            r1 = 1
            r3.A0E = r1
            android.content.Context r2 = r0.A09
            int r1 = r13.A08
            float r1 = (float) r1
            int r1 = X.C007106r.A00(r2, r1)
            r3.A06 = r1
            X.1KU r1 = X.AnonymousClass1KU.TWO_LETTER
            r3.A0D = r1
            X.0y3 r1 = r11.B5U()
            int r1 = r1.AhV()
            r3.A07 = r1
            android.content.Context r2 = r0.A09
            int r1 = r13.A06
            float r1 = (float) r1
            int r1 = X.C007106r.A00(r2, r1)
            float r1 = (float) r1
            r3.A01 = r1
            X.10M r2 = X.AnonymousClass10M.ROBOTO_BOLD
            android.content.Context r1 = r0.A09
            android.graphics.Typeface r1 = r2.A00(r1)
            r3.A09 = r1
            X.1LC r2 = X.AnonymousClass1LC.A00
            X.11G r1 = r3.A14()
            r1.BxJ(r2)
            X.10G r7 = X.AnonymousClass10G.LEFT
            r1 = 0
            int r2 = r14.A00(r1)
            X.11G r1 = r3.A14()
            r1.BxI(r7, r2)
            X.10G r7 = X.AnonymousClass10G.BOTTOM
            r1 = 0
            int r2 = r14.A00(r1)
            X.11G r1 = r3.A14()
            r1.BxI(r7, r2)
            boolean r1 = r9.A08()
            if (r1 == 0) goto L_0x2fee
            int r1 = X.AnonymousClass1KV.A00(r11)
            r3.A05 = r1
        L_0x2fee:
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r4, r5)
            r6.A3A(r3)
            X.1Jy r1 = X.C22111Jy.A04
            if (r12 != r1) goto L_0x3000
            r1 = 0
        L_0x2ffa:
            r6.A3A(r1)
            X.11D r2 = r6.A00
            return r2
        L_0x3000:
            r5 = 4
            java.lang.String r4 = "badgeBorderSizeDp"
            java.lang.String r3 = "badgeSizeDp"
            java.lang.String r2 = "badgeType"
            java.lang.String r1 = "containerSizeDp"
            java.lang.String[] r3 = new java.lang.String[]{r4, r3, r2, r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r5)
            X.2QU r1 = new X.2QU
            android.content.Context r4 = r0.A09
            r1.<init>(r4)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x3021
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x3021:
            r2.clear()
            int r0 = r13.A05
            r1.A02 = r0
            r0 = 3
            r2.set(r0)
            int r0 = r13.A04
            r1.A01 = r0
            r0 = 1
            r2.set(r0)
            float r0 = r13.A00
            r1.A00 = r0
            r0 = 0
            r2.set(r0)
            r1.A03 = r12
            r0 = 2
            r2.set(r0)
            r1.A04 = r11
            r0 = r20
            java.lang.String r0 = r10.A02(r0)
            r1.A05 = r0
            X.AnonymousClass11F.A0C(r5, r2, r3)
            goto L_0x2ffa
        L_0x3050:
            r1 = r20
            java.lang.Object r9 = r1.get(r8)
            com.facebook.user.model.UserKey r9 = (com.facebook.user.model.UserKey) r9
            java.lang.Object r8 = X.AnonymousClass0j4.A04(r3, r8, r2)
            java.lang.String r8 = (java.lang.String) r8
            r2 = 1
            java.lang.String r1 = "userKey"
            java.lang.String[] r4 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.1J7 r2 = new X.1J7
            android.content.Context r1 = r0.A09
            r2.<init>(r1)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x3079
            java.lang.String r0 = r0.A06
            r2.A07 = r0
        L_0x3079:
            r3.clear()
            r2.A06 = r9
            r0 = 0
            r3.set(r0)
            r2.A07 = r8
            r2.A04 = r13
            r2.A03 = r12
            r2.A00 = r7
            r2.A08 = r6
            r2.A05 = r11
            r2.A02 = r5
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
            return r2
        L_0x3095:
            r1 = r2
            X.1IM r1 = (X.AnonymousClass1IM) r1
            int r7 = r1.A00
            com.facebook.mig.scheme.interfaces.MigColorScheme r3 = r1.A01
            java.lang.Integer r2 = r1.A02
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            if (r2 != r1) goto L_0x30e4
            int r6 = r3.Akh()
        L_0x30a6:
            r2 = 1
            java.lang.String r1 = "orientation"
            java.lang.String[] r4 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.1Ga r2 = new X.1Ga
            r2.<init>()
            X.0z4 r5 = r0.A0B
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x30c1
            java.lang.String r1 = r0.A06
            r2.A07 = r1
        L_0x30c1:
            r3.clear()
            r0 = 1065353216(0x3f800000, float:1.0)
            int r0 = r5.A00(r0)
            r2.A02 = r0
            r2.A00 = r6
            r2.A01 = r7
            r0 = 0
            r3.set(r0)
            if (r7 != 0) goto L_0x30e9
            r1 = 1120403456(0x42c80000, float:100.0)
            X.11G r0 = r2.A14()
            r0.CNJ(r1)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
        L_0x30e3:
            return r2
        L_0x30e4:
            int r6 = r3.B2A()
            goto L_0x30a6
        L_0x30e9:
            r1 = 1120403456(0x42c80000, float:100.0)
            X.11G r0 = r2.A14()
            r0.BC1(r1)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
            return r2
        L_0x30f7:
            r1 = r2
            X.1J7 r1 = (X.AnonymousClass1J7) r1
            com.facebook.user.model.UserKey r10 = r1.A06
            java.lang.String r13 = r1.A07
            X.1J0 r9 = r1.A04
            X.1Jy r8 = r1.A03
            int r14 = r1.A00
            boolean r3 = r1.A08
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r1.A05
            com.facebook.messaging.model.messages.Message r12 = r1.A02
            int r4 = X.AnonymousClass1Y3.A8h
            X.0UN r2 = r1.A01
            r1 = 0
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r4, r2)
            X.1H8 r6 = (X.AnonymousClass1H8) r6
            X.0zR r17 = X.C21891Jc.A00(r0, r9, r8, r14)
            if (r17 != 0) goto L_0x328b
            int r1 = r9.A05
            float r2 = (float) r1
        L_0x311e:
            android.content.Context r4 = r0.A09
            r1 = 0
            if (r17 == 0) goto L_0x3124
            r1 = 1
        L_0x3124:
            android.graphics.Path r11 = X.AnonymousClass1K8.A00(r4, r8, r9, r2, r1)
            if (r3 == 0) goto L_0x312e
            r16 = 1
            if (r12 != 0) goto L_0x3130
        L_0x312e:
            r16 = 0
        L_0x3130:
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            X.0uP r1 = X.C14950uP.CENTER
            r5.A3E(r1)
            X.0uO r1 = X.C14940uO.CENTER
            r5.A3D(r1)
            int r1 = r9.A05
            float r1 = (float) r1
            r5.A1z(r1)
            int r1 = r9.A05
            float r1 = (float) r1
            r5.A1p(r1)
            r4 = 0
            r1 = r17
            if (r3 == 0) goto L_0x3150
            r1 = r4
        L_0x3150:
            r5.A3A(r1)
            if (r16 == 0) goto L_0x31e8
            r15 = 0
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r3 = new com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000
            r1 = 7
            r3.<init>(r1)
            X.5FC r2 = new X.5FC
            android.content.Context r1 = r0.A09
            r2.<init>(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000.A07(r3, r0, r15, r15, r2)
            java.lang.Object r1 = r3.A02
            X.5FC r1 = (X.AnonymousClass5FC) r1
            r1.A07 = r10
            java.lang.Object r2 = r3.A00
            java.util.BitSet r2 = (java.util.BitSet) r2
            r2.set(r15)
            java.lang.Object r1 = r3.A02
            X.5FC r1 = (X.AnonymousClass5FC) r1
            r1.A08 = r13
            r1.A05 = r9
            r1.A03 = r8
            r1.A00 = r14
            r1.A06 = r7
            r1.A02 = r12
        L_0x3183:
            r5.A39(r3)
            if (r16 != 0) goto L_0x31df
            X.1Jy r1 = X.C22111Jy.A04
            if (r8 == r1) goto L_0x31df
            r12 = 0
            if (r17 == 0) goto L_0x3190
            r12 = 1
        L_0x3190:
            r11 = 4
            java.lang.String r4 = "badgeBorderSizeDp"
            java.lang.String r3 = "badgeSizeDp"
            java.lang.String r2 = "badgeType"
            java.lang.String r1 = "containerSizeDp"
            java.lang.String[] r2 = new java.lang.String[]{r4, r3, r2, r1}
            java.util.BitSet r1 = new java.util.BitSet
            r1.<init>(r11)
            X.2QU r4 = new X.2QU
            android.content.Context r3 = r0.A09
            r4.<init>(r3)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x31b1
            java.lang.String r0 = r0.A06
            r4.A07 = r0
        L_0x31b1:
            r1.clear()
            int r0 = r9.A05
            r4.A02 = r0
            r0 = 3
            r1.set(r0)
            int r0 = r9.A04
            r4.A01 = r0
            r0 = 1
            r1.set(r0)
            if (r12 == 0) goto L_0x31e5
            float r0 = r9.A01
        L_0x31c8:
            r4.A00 = r0
            r0 = 0
            r1.set(r0)
            r4.A03 = r8
            r0 = 2
            r1.set(r0)
            java.lang.String r0 = r6.A01(r10)
            r4.A05 = r0
            r4.A04 = r7
            X.AnonymousClass11F.A0C(r11, r1, r2)
        L_0x31df:
            r5.A3A(r4)
            X.11D r0 = r5.A00
            return r0
        L_0x31e5:
            float r0 = r9.A00
            goto L_0x31c8
        L_0x31e8:
            if (r12 == 0) goto L_0x320e
            com.facebook.litho.ComponentBuilderCBuilderShape1_0S0300000 r3 = X.C60112wb.A00(r0)
            r3.A1z(r2)
            r3.A1p(r2)
            java.lang.Object r1 = r3.A02
            X.2wb r1 = (X.C60112wb) r1
            r1.A03 = r12
            java.lang.Object r2 = r3.A00
            java.util.BitSet r2 = (java.util.BitSet) r2
            r1 = 0
            r2.set(r1)
            X.36c r2 = X.AnonymousClass36c.A00()
            java.lang.Object r1 = r3.A02
            X.2wb r1 = (X.C60112wb) r1
            r1.A01 = r2
            goto L_0x3183
        L_0x320e:
            r1 = 0
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r3 = X.C21911Je.A00(r0, r1, r1)
            r3.A1z(r2)
            r3.A1p(r2)
            int r1 = r7.B9m()
            boolean r12 = X.C012609n.A02(r1)
            X.1J0 r1 = X.AnonymousClass1J0.A0B
            boolean r1 = r9.equals(r1)
            if (r1 == 0) goto L_0x3282
            r1 = 2132346501(0x7f190685, float:2.0340573E38)
            if (r12 == 0) goto L_0x3231
            r1 = 2132346502(0x7f190686, float:2.0340575E38)
        L_0x3231:
            X.1JY r1 = X.AnonymousClass1KQ.A00(r10, r13, r1)
            r3.A3h(r1)
            java.lang.Object r1 = r3.A00
            X.1Je r1 = (X.C21911Je) r1
            r1.A08 = r11
            android.content.Context r1 = r0.A09
            int r1 = X.C007106r.A00(r1, r2)
            java.lang.Object r2 = r3.A00
            X.1Je r2 = (X.C21911Je) r2
            r2.A06 = r1
            X.1KU r1 = X.AnonymousClass1KU.TWO_LETTER
            r2.A0D = r1
            X.0y3 r1 = r7.B5U()
            int r1 = r1.AhV()
            r2.A07 = r1
            android.content.Context r2 = r0.A09
            int r1 = r9.A07
            float r1 = (float) r1
            int r1 = X.C007106r.A00(r2, r1)
            float r2 = (float) r1
            java.lang.Object r1 = r3.A00
            X.1Je r1 = (X.C21911Je) r1
            r1.A01 = r2
            X.10M r2 = X.AnonymousClass10M.ROBOTO_BOLD
            android.content.Context r1 = r0.A09
            android.graphics.Typeface r2 = r2.A00(r1)
            java.lang.Object r1 = r3.A00
            X.1Je r1 = (X.C21911Je) r1
            r1.A09 = r2
            int r2 = X.AnonymousClass1KV.A00(r7)
            java.lang.Object r1 = r3.A00
            X.1Je r1 = (X.C21911Je) r1
            r1.A05 = r2
            goto L_0x3183
        L_0x3282:
            r1 = 2132346505(0x7f190689, float:2.0340582E38)
            if (r12 == 0) goto L_0x3231
            r1 = 2132346506(0x7f19068a, float:2.0340584E38)
            goto L_0x3231
        L_0x328b:
            float r2 = r9.A02
            goto L_0x311e
        L_0x328f:
            r1 = r2
            X.1J5 r1 = (X.AnonymousClass1J5) r1
            X.1J0 r11 = r1.A02
            X.1H4 r10 = r1.A04
            com.facebook.messaging.model.messages.Message r9 = r1.A01
            int r8 = r1.A00
            r7 = 0
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A03
            X.1Gs r12 = r10.B6D()
            X.1Jy[] r4 = X.C22111Jy.A00
            int r3 = r4.length
            r2 = 0
        L_0x32a5:
            if (r2 >= r3) goto L_0x32b0
            r5 = r4[r2]
            X.1Gs r1 = r5.tileBadge
            if (r1 == r12) goto L_0x32b2
            int r2 = r2 + 1
            goto L_0x32a5
        L_0x32b0:
            X.1Jy r5 = X.C22111Jy.A04
        L_0x32b2:
            int r1 = r10.Aw7()
            if (r1 != 0) goto L_0x32e0
            r2 = 1
            java.lang.String r1 = "profileConfig"
            java.lang.String[] r4 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.5Tj r2 = new X.5Tj
            r2.<init>()
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x32d1
            java.lang.String r0 = r0.A06
            r2.A07 = r0
        L_0x32d1:
            r3.clear()
            r2.A00 = r11
            r3.set(r7)
            r2.A01 = r6
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
            return r2
        L_0x32e0:
            boolean r1 = r10.BHA()
            if (r1 == 0) goto L_0x3304
            com.google.common.collect.ImmutableList r1 = r10.B8C()
            X.1Xv r2 = r1.iterator()
        L_0x32ee:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x334f
            java.lang.Object r1 = r2.next()
            com.facebook.user.model.UserKey r1 = (com.facebook.user.model.UserKey) r1
            boolean r1 = r1.A08()
            if (r1 == 0) goto L_0x32ee
            r2 = 1
        L_0x3301:
            r1 = 1
            if (r2 == 0) goto L_0x3305
        L_0x3304:
            r1 = 0
        L_0x3305:
            if (r1 != 0) goto L_0x3351
            com.google.common.collect.ImmutableList r1 = r10.B8C()
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x3351
            r2 = 1
            java.lang.String r1 = "userKeys"
            java.lang.String[] r4 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.1J3 r2 = new X.1J3
            android.content.Context r1 = r0.A09
            r2.<init>(r1)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x332c
            java.lang.String r0 = r0.A06
            r2.A07 = r0
        L_0x332c:
            r3.clear()
            r2.A04 = r11
            com.google.common.collect.ImmutableList r0 = r10.B8C()
            r2.A07 = r0
            r3.set(r7)
            com.google.common.collect.ImmutableList r0 = r10.Akd()
            r2.A06 = r0
            r2.A03 = r5
            r2.A00 = r8
            r2.A08 = r7
            r2.A05 = r6
            r2.A02 = r9
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
            return r2
        L_0x334f:
            r2 = 0
            goto L_0x3301
        L_0x3351:
            X.0zR r2 = X.AnonymousClass1J5.A00(r0, r10, r11, r5)
            return r2
        L_0x3356:
            r1 = r2
            X.1l6 r1 = (X.C32071l6) r1
            android.net.Uri r15 = r1.A01
            X.1J0 r7 = r1.A03
            X.1Jy r5 = r1.A02
            int r2 = r1.A00
            r4 = 0
            r1 = 0
            r3 = 0
            X.0zR r6 = X.C21891Jc.A00(r0, r7, r5, r2)
            if (r6 != 0) goto L_0x3437
            int r2 = r7.A05
            float r13 = (float) r2
        L_0x336d:
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            X.0uP r8 = X.C14950uP.CENTER
            r2.A3E(r8)
            X.0uO r8 = X.C14940uO.CENTER
            r2.A3D(r8)
            int r8 = r7.A05
            float r8 = (float) r8
            r2.A1z(r8)
            int r8 = r7.A05
            float r8 = (float) r8
            r2.A1p(r8)
            r2.A3A(r6)
            r12 = 0
            if (r6 == 0) goto L_0x338e
            r12 = 1
        L_0x338e:
            android.content.Context r8 = r0.A09
            int r11 = X.C007106r.A00(r8, r13)
            X.1KR r10 = new X.1KR
            r10.<init>()
            X.1KS r8 = X.AnonymousClass1KS.A01
            r10.A05 = r8
            com.facebook.user.profilepic.PicSquare r14 = new com.facebook.user.profilepic.PicSquare
            com.facebook.user.profilepic.PicSquareUrlWithSize r9 = new com.facebook.user.profilepic.PicSquareUrlWithSize
            java.lang.String r8 = r15.toString()
            r9.<init>(r11, r8)
            r14.<init>(r9)
            r10.A04 = r14
            X.1Gs r8 = r5.tileBadge
            r10.A06 = r8
            X.1JY r9 = new X.1JY
            r9.<init>(r10)
            r8 = 0
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r10 = X.C21911Je.A00(r0, r8, r8)
            r10.A1z(r13)
            r10.A1p(r13)
            r10.A3h(r9)
            android.content.Context r8 = r0.A09
            android.graphics.Path r9 = X.AnonymousClass1K8.A00(r8, r5, r7, r13, r12)
            java.lang.Object r8 = r10.A00
            X.1Je r8 = (X.C21911Je) r8
            r8.A08 = r9
            r8.A06 = r11
            X.1Je r8 = r10.A3A()
            r2.A3A(r8)
            X.1Jy r8 = X.C22111Jy.A04
            if (r5 != r8) goto L_0x33ea
            r10 = 0
        L_0x33de:
            if (r10 == 0) goto L_0x33e7
            r0 = 4
            X.AnonymousClass11F.A0C(r0, r3, r4)
            r2.A3A(r1)
        L_0x33e7:
            X.11D r0 = r2.A00
            return r0
        L_0x33ea:
            java.lang.Object r10 = new java.lang.Object
            r10.<init>()
            r9 = 4
            java.lang.String r8 = "badgeBorderSizeDp"
            java.lang.String r4 = "badgeSizeDp"
            java.lang.String r3 = "badgeType"
            java.lang.String r1 = "containerSizeDp"
            java.lang.String[] r4 = new java.lang.String[]{r8, r4, r3, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r9)
            X.2QU r1 = new X.2QU
            android.content.Context r8 = r0.A09
            r1.<init>(r8)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x3410
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x3410:
            r3.clear()
            int r0 = r7.A05
            r1.A02 = r0
            r0 = 3
            r3.set(r0)
            int r0 = r7.A04
            r1.A01 = r0
            r0 = 1
            r3.set(r0)
            if (r6 == 0) goto L_0x3434
            float r0 = r7.A01
        L_0x3427:
            r1.A00 = r0
            r0 = 0
            r3.set(r0)
            r1.A03 = r5
            r0 = 2
            r3.set(r0)
            goto L_0x33de
        L_0x3434:
            float r0 = r7.A00
            goto L_0x3427
        L_0x3437:
            float r13 = r7.A02
            goto L_0x336d
        L_0x343b:
            X.1L2 r2 = (X.AnonymousClass1L2) r2
            java.lang.CharSequence r1 = r2.A0C
            r17 = r1
            X.10M r1 = r2.A0B
            r22 = r1
            X.1JL r1 = r2.A0A
            r21 = r1
            X.0y3 r1 = r2.A09
            r20 = r1
            boolean r1 = r2.A0E
            r16 = r1
            int r1 = r2.A03
            r19 = r1
            android.text.TextUtils$TruncateAt r15 = r2.A05
            android.text.Layout$Alignment r14 = r2.A04
            boolean r12 = r2.A0F
            float r11 = r2.A00
            float r10 = r2.A01
            X.1Kv r9 = r2.A06
            X.0uM r8 = r2.A08
            int r7 = r2.A02
            boolean r6 = r2.A0D
            int r3 = X.AnonymousClass1Y3.ABW
            X.0UN r2 = r2.A07
            r1 = 0
            java.lang.Object r13 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.1K5 r13 = (X.AnonymousClass1K5) r13
            r2 = 1
            java.lang.String r1 = "text"
            java.lang.String[] r5 = new java.lang.String[]{r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r2)
            X.1Kx r3 = new X.1Kx
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r1 = r0.A0B
            r18 = r1
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x3491
            java.lang.String r1 = r1.A06
            r3.A07 = r1
        L_0x3491:
            r4.clear()
            r2 = 0
            r3.A0U = r2
            r3.A0M = r14
            r3.A0N = r15
            r1 = r19
            r3.A0B = r1
            if (r16 == 0) goto L_0x34a8
            r14 = 0
            r1 = r17
            java.lang.CharSequence r17 = r13.getTransformation(r1, r14)
        L_0x34a8:
            r1 = r17
            r3.A0S = r1
            r4.set(r2)
            android.content.Context r1 = r0.A09
            r0 = r22
            android.graphics.Typeface r0 = r0.A00(r1)
            r3.A0L = r0
            int r1 = r21.B5j()
            r0 = r18
            int r0 = r0.A03(r1)
            r3.A0I = r0
            int r0 = r20.AhV()
            r3.A0H = r0
            r3.A0X = r12
            r3.A00 = r11
            r1 = 1
            r0 = r19
            if (r0 != r1) goto L_0x34d5
            r2 = 1
        L_0x34d5:
            r3.A0V = r2
            r3.A04 = r10
            r3.A0O = r9
            r3.A0R = r8
            r3.A09 = r7
            r3.A0T = r6
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x34e6:
            r1 = r2
            X.0yD r1 = (X.C17030yD) r1
            java.lang.CharSequence r12 = r1.A07
            X.10K r9 = r1.A06
            int r11 = r1.A01
            android.text.TextUtils$TruncateAt r10 = r1.A03
            android.text.Layout$Alignment r13 = r1.A02
            boolean r8 = r1.A09
            float r7 = r1.A00
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A05
            X.1Kv r5 = r1.A04
            boolean r4 = r1.A08
            if (r6 != 0) goto L_0x3503
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = X.C17190yT.A00()
        L_0x3503:
            r14 = 4
            java.lang.String r15 = "text"
            java.lang.String r3 = "textColor"
            java.lang.String r2 = "textSize"
            java.lang.String r1 = "typeface"
            java.lang.String[] r3 = new java.lang.String[]{r15, r3, r2, r1}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r14)
            X.1L2 r1 = new X.1L2
            android.content.Context r14 = r0.A09
            r1.<init>(r14)
            X.0zR r0 = r0.A04
            if (r0 == 0) goto L_0x3524
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x3524:
            r2.clear()
            r1.A04 = r13
            boolean r0 = r9.AcY()
            r1.A0E = r0
            r1.A05 = r10
            r1.A03 = r11
            r1.A0C = r12
            r0 = 0
            r2.set(r0)
            X.10M r0 = r9.B7S()
            r1.A0B = r0
            r0 = 3
            r2.set(r0)
            X.1JL r0 = r9.B5i()
            r1.A0A = r0
            r0 = 2
            r2.set(r0)
            X.0y3 r0 = r9.B5d(r6)
            r1.A09 = r0
            r0 = 1
            r2.set(r0)
            r1.A0F = r8
            r1.A00 = r7
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.A01 = r0
            r1.A06 = r5
            int r0 = r6.Aea()
            r1.A02 = r0
            r1.A0D = r4
            r0 = 4
            X.AnonymousClass11F.A0C(r0, r2, r3)
            return r1
        L_0x356e:
            r1 = r2
            X.1Ia r1 = (X.C21611Ia) r1
            X.0zR r2 = r1.A00
            com.facebook.messaging.inbox2.items.InboxUnitItem r6 = r1.A01
            X.1BC r5 = r1.A02
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r4.A1n(r1)
            r4.A1o(r1)
            X.0uO r1 = X.C14940uO.FLEX_START
            r4.A3C(r1)
            r4.A3A(r2)
            java.lang.Class<X.1Ia> r3 = X.C21611Ia.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0, r6, r5}
            r1 = 71235917(0x43ef94d, float:2.2448866E-36)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r4.A2R(r1)
            boolean r1 = r6 instanceof com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem
            if (r1 == 0) goto L_0x35c6
            r1 = r6
            com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem r1 = (com.facebook.messaging.inbox2.sectionheader.InboxUnitSectionHeaderItem) r1
            X.1ck r2 = r1.A02
            r1 = -579238035(0xffffffffdd79876d, float:-1.12377874E18)
            boolean r1 = r2.getBooleanValue(r1)
        L_0x35ab:
            if (r1 == 0) goto L_0x35c1
            r1 = 2130970378(0x7f04070a, float:1.7549464E38)
            r4.A22(r1)
            java.lang.Object[] r2 = new java.lang.Object[]{r0, r6, r5}
            r1 = -1351902487(0xffffffffaf6b9ae9, float:-2.142816E-10)
            X.10N r0 = A0E(r3, r0, r1, r2)
            r4.A2y(r0)
        L_0x35c1:
            X.0zR r0 = r4.A31()
            return r0
        L_0x35c6:
            X.1IY r1 = r6.A06()
            int r1 = r1.ordinal()
            switch(r1) {
                case 0: goto L_0x35d3;
                case 1: goto L_0x35d3;
                case 5: goto L_0x35d3;
                case 23: goto L_0x35d3;
                case 24: goto L_0x35d3;
                case 30: goto L_0x35d3;
                case 32: goto L_0x35d3;
                case 35: goto L_0x35d3;
                default: goto L_0x35d1;
            }
        L_0x35d1:
            r1 = 0
            goto L_0x35ab
        L_0x35d3:
            r1 = 1
            goto L_0x35ab
        L_0x35d5:
            r1 = r2
            X.1vH r1 = (X.AnonymousClass1vH) r1
            com.facebook.payments.paymentmethods.model.AltPayPaymentMethod r7 = r1.A02
            java.lang.String r11 = r1.A03
            java.lang.String r9 = r1.A04
            android.view.View$OnClickListener r10 = r1.A00
            X.0zZ r5 = r1.A01
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            com.facebook.payments.paymentmethods.model.AltPayPricepoint r1 = r7.A00
            com.google.common.collect.ImmutableList r1 = r1.A00
            X.1Xv r3 = r1.iterator()
        L_0x35ef:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x3609
            java.lang.Object r2 = r3.next()
            java.lang.String r2 = (java.lang.String) r2
            boolean r1 = android.webkit.URLUtil.isValidUrl(r2)
            if (r1 == 0) goto L_0x35ef
            android.net.Uri r1 = android.net.Uri.parse(r2)
            r8.add(r1)
            goto L_0x35ef
        L_0x3609:
            com.facebook.payments.paymentmethods.model.AltPayPricepoint r1 = r7.A00
            boolean r1 = r1.A07
            r6 = 1094713344(0x41400000, float:12.0)
            if (r1 == 0) goto L_0x36dd
            X.1wd r3 = X.C16980y8.A00(r0)
            X.10G r1 = X.AnonymousClass10G.RIGHT
            r3.A2X(r1, r6)
            X.1v9 r4 = new X.1v9
            r4.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x3627
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x3627:
            r4.A02 = r5
            r4.A03 = r11
            r4.A01 = r10
            r2 = 1065353216(0x3f800000, float:1.0)
            X.11G r1 = r4.A14()
            r1.AaB(r2)
            r1 = 1
            X.0zZ[] r2 = new X.C17840zZ[r1]
            r4.A04 = r2
            X.0zZ r1 = r4.A02
            r5 = 0
            r2[r5] = r1
            r3.A3A(r4)
            X.1v9 r4 = new X.1v9
            r4.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x3650
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x3650:
            java.lang.String r2 = ""
            X.0zZ r1 = new X.0zZ
            r1.<init>(r2)
            r4.A02 = r1
            r4.A03 = r9
            r1 = 2
            r4.A00 = r1
            r2 = 1077936128(0x40400000, float:3.0)
            X.11G r1 = r4.A14()
            r1.AaB(r2)
            r1 = 1
            X.0zZ[] r2 = new X.C17840zZ[r1]
            r4.A04 = r2
            X.0zZ r1 = r4.A02
            r2[r5] = r1
            r3.A3A(r4)
            X.0y8 r2 = r3.A00
        L_0x3675:
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            r1 = -1
            r3.A23(r1)
            r3.A3A(r2)
            r2 = 1
            java.lang.String r1 = "iconUris"
            java.lang.String[] r5 = new java.lang.String[]{r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r2)
            X.5HJ r2 = new X.5HJ
            r2.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x3699
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x3699:
            r4.clear()
            r2.A01 = r8
            r1 = 0
            r4.set(r1)
            r1 = 1117782016(0x42a00000, float:80.0)
            r2.A00 = r1
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r4, r5)
            r3.A3A(r2)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C14910uL.A02(r0)
            com.facebook.payments.paymentmethods.model.AltPayPricepoint r0 = r7.A00
            java.lang.String r0 = r0.A02
            r2.A3j(r0)
            r2.A3D(r6)
            android.text.Layout$Alignment r0 = android.text.Layout.Alignment.ALIGN_NORMAL
            r2.A3W(r0)
            X.10G r0 = X.AnonymousClass10G.BOTTOM
            r1 = 1090519040(0x41000000, float:8.0)
            r2.A2X(r0, r1)
            X.10G r0 = X.AnonymousClass10G.RIGHT
            r2.A2X(r0, r1)
            X.10G r0 = X.AnonymousClass10G.TOP
            r2.A2X(r0, r1)
            r0 = 2130969432(0x7f040358, float:1.7547546E38)
            r2.A3L(r0)
            r3.A39(r2)
            X.11D r0 = r3.A00
            return r0
        L_0x36dd:
            r2 = 0
            goto L_0x3675
        L_0x36df:
            r1 = r2
            X.1vG r1 = (X.AnonymousClass1vG) r1
            java.lang.String r8 = r1.A03
            java.lang.String r7 = r1.A02
            boolean r4 = r1.A04
            int r3 = X.AnonymousClass1Y3.AwC
            X.0UN r2 = r1.A00
            r1 = 2
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r1, r3, r2)
            X.4lH r6 = (X.C97444lH) r6
            r5 = 1
            if (r6 == 0) goto L_0x3797
            if (r4 == 0) goto L_0x3730
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r4.A1n(r1)
            X.AnonymousClass1vG.A01(r4, r0)
            if (r7 == 0) goto L_0x3709
            X.AnonymousClass1vG.A02(r4, r0)
        L_0x3709:
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r3.A3D(r1)
            android.content.Context r2 = r0.A09
            r1 = 2132083400(0x7f1502c8, float:1.9806941E38)
            int r1 = X.AnonymousClass01R.A00(r2, r1)
            r3.A23(r1)
            r1 = 1120403456(0x42c80000, float:100.0)
            r3.A1q(r1)
            r3.A39(r4)
            X.11F r0 = X.AnonymousClass1vG.A00(r0, r7, r6)
            r3.A39(r0)
            X.11D r0 = r3.A00
            return r0
        L_0x3730:
            X.1wd r4 = X.C16980y8.A00(r0)
            X.10G r2 = X.AnonymousClass10G.A04
            r1 = 2132148224(0x7f160000, float:1.993842E38)
            r4.A2f(r2, r1)
            if (r8 == 0) goto L_0x3742
            if (r7 == 0) goto L_0x3742
            X.AnonymousClass1vG.A02(r4, r0)
        L_0x3742:
            X.AnonymousClass1vG.A01(r4, r0)
            X.1wd r3 = X.C16980y8.A00(r0)
            X.0uO r1 = X.C14940uO.CENTER
            r3.A3D(r1)
            android.content.Context r2 = r0.A09
            r1 = 2132083400(0x7f1502c8, float:1.9806941E38)
            int r1 = X.AnonymousClass01R.A00(r2, r1)
            r3.A23(r1)
            X.11F r1 = X.AnonymousClass1vG.A00(r0, r7, r6)
            r3.A39(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.AnonymousClass1L2.A00(r0)
            r2.A3k(r8)
            X.10M r0 = X.AnonymousClass10M.ROBOTO_MEDIUM
            r2.A3g(r0)
            X.2Zo r0 = X.C48112Zo.A01
            r2.A3e(r0)
            X.5d3 r0 = X.C114945d3.A04
            r2.A3d(r0)
            r0 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r0)
            java.lang.Object r1 = r2.A02
            X.1L2 r1 = (X.AnonymousClass1L2) r1
            r1.A03 = r5
            android.text.TextUtils$TruncateAt r0 = android.text.TextUtils.TruncateAt.END
            r1.A05 = r0
            X.10G r1 = X.AnonymousClass10G.START
            r0 = 2132148243(0x7f160013, float:1.9938458E38)
            r2.A2f(r1, r0)
            r3.A39(r2)
            r3.A39(r4)
            X.0y8 r0 = r3.A00
            return r0
        L_0x3797:
            r0 = 0
            return r0
        L_0x3799:
            X.1Je r2 = (X.C21911Je) r2
            X.1JY r1 = r2.A0B
            r23 = r1
            boolean r1 = r2.A0F
            r18 = r1
            int r1 = r2.A04
            r17 = r1
            int r1 = r2.A03
            r16 = r1
            boolean r1 = r2.A0E
            r22 = r1
            int r1 = r2.A06
            r21 = r1
            int r1 = r2.A05
            r20 = r1
            boolean r1 = r2.A0G
            r19 = r1
            android.graphics.drawable.Drawable r14 = r2.A0A
            X.1KU r13 = r2.A0D
            int r12 = r2.A07
            float r11 = r2.A01
            android.graphics.Typeface r10 = r2.A09
            float r9 = r2.A00
            X.1Ka r8 = r2.A0C
            android.graphics.Path r7 = r2.A08
            int r6 = r2.A02
            r2 = 1
            java.lang.String r1 = "params"
            java.lang.String[] r5 = new java.lang.String[]{r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r2)
            X.1KX r3 = new X.1KX
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r2 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x37ea
            java.lang.String r15 = r1.A06
            r3.A07 = r15
        L_0x37ea:
            r4.clear()
            r1 = r23
            r3.A0B = r1
            r1 = 0
            r4.set(r1)
            r1 = r18
            r3.A0J = r1
            r1 = r22
            r3.A0I = r1
            r1 = r21
            r3.A04 = r1
            r1 = r20
            r3.A03 = r1
            r1 = r19
            r3.A0K = r1
            r3.A08 = r14
            r3.A0D = r13
            r3.A05 = r12
            r3.A01 = r11
            r3.A07 = r10
            r3.A00 = r9
            if (r8 == 0) goto L_0x381f
            X.1Gs r9 = X.C21381Gs.A0R
            r1 = 2132345022(0x7f1900be, float:2.0337574E38)
            r8.A01(r9, r1)
        L_0x381f:
            if (r8 == 0) goto L_0x385a
            X.1KZ r1 = r8.A00()
        L_0x3825:
            r3.A0C = r1
            r3.A02 = r6
            r3.A06 = r7
            if (r18 == 0) goto L_0x3855
            X.10G r6 = X.AnonymousClass10G.ALL
            r1 = 2132148233(0x7f160009, float:1.9938438E38)
            if (r16 <= 0) goto L_0x3836
            r1 = r16
        L_0x3836:
            int r2 = r2.A03(r1)
            X.11G r1 = r3.A14()
            r1.BwM(r6, r2)
            r1 = 2132214795(0x7f17040b, float:2.0073442E38)
            if (r17 <= 0) goto L_0x3848
            r1 = r17
        L_0x3848:
            android.content.Context r0 = r0.A09
            android.graphics.drawable.Drawable r1 = X.AnonymousClass01R.A03(r0, r1)
            X.11G r0 = r3.A14()
            r0.A0C(r1)
        L_0x3855:
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x385a:
            X.1KZ r1 = X.AnonymousClass1KZ.A0C
            goto L_0x3825
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A0O(X.0p4):X.0zR");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v19, resolved type: X.DfN} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v20, resolved type: X.DfM} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v22, resolved type: X.1JX} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v607, resolved type: X.11D} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v644, resolved type: X.10I} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v52, resolved type: X.DfN} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v53, resolved type: X.DfN} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v54, resolved type: X.DfN} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v703, resolved type: X.11D} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v704, resolved type: X.11D} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x1155, code lost:
        if (r12.A0H() != false) goto L_0x1157;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0658, code lost:
        if (r12.A03 != false) goto L_0x065a;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C17770zR A0P(X.AnonymousClass0p4 r28, int r29, int r30) {
        /*
            r27 = this;
            r2 = r27
            boolean r1 = r2 instanceof X.AnonymousClass1RX
            r0 = r28
            if (r1 != 0) goto L_0x130f
            boolean r1 = r2 instanceof X.C21961Jj
            if (r1 != 0) goto L_0x0f6c
            boolean r1 = r2 instanceof X.C21971Jk
            if (r1 != 0) goto L_0x0b9f
            boolean r1 = r2 instanceof X.C22241Km
            if (r1 != 0) goto L_0x09bb
            boolean r1 = r2 instanceof X.AnonymousClass1QG
            if (r1 != 0) goto L_0x070d
            boolean r1 = r2 instanceof X.AnonymousClass11H
            if (r1 != 0) goto L_0x110d
            boolean r1 = r2 instanceof X.C37321v7
            if (r1 != 0) goto L_0x02e0
            boolean r1 = r2 instanceof X.AnonymousClass1vF
            if (r1 != 0) goto L_0x0188
            boolean r1 = r2 instanceof X.AnonymousClass1v8
            if (r1 != 0) goto L_0x0037
            boolean r1 = r2 instanceof X.AnonymousClass1JN
            if (r1 != 0) goto L_0x04cb
            boolean r1 = r2 instanceof X.AnonymousClass1JO
            if (r1 != 0) goto L_0x0dcd
            X.1we r0 = X.AnonymousClass11D.A00(r0)
            X.11D r0 = r0.A00
            return r0
        L_0x0037:
            r1 = r2
            X.1v8 r1 = (X.AnonymousClass1v8) r1
            X.5oW r3 = r1.A03
            X.CqM r6 = r1.A00
            com.facebook.mig.scheme.interfaces.MigColorScheme r2 = r1.A04
            X.9jh r5 = r1.A01
            X.5CH r1 = r1.A02
            int r1 = r1.selectedSecurityType
            int r7 = android.view.View.MeasureSpec.getSize(r30)
            X.5ob r4 = new X.5ob
            r4.<init>(r3, r2, r1)
            if (r1 != 0) goto L_0x0074
            r2 = 1
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0069
            X.2yh r3 = new X.2yh
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r2 = 0
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            r3.<init>(r2, r1)
            java.lang.String r1 = "updateState:AccountLoginRecSecurityRootComponent.setSelectedSecurityType"
            r0.A0G(r3, r1)
        L_0x0069:
            if (r5 == 0) goto L_0x0074
            X.9ik r1 = r5.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r2 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecSecurity r2 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecSecurity) r2
            r1 = 1
            r2.A01 = r1
        L_0x0074:
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r5 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r5.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r6
            X.5oW r10 = r4.A01
            X.1we r6 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r10.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r10.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r10.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            r6.A2C(r7)
            r12 = 0
            r2 = 2
            java.lang.String r9 = "loginStyle"
            java.lang.String r7 = "text"
            java.lang.String[] r11 = new java.lang.String[]{r9, r7}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0z4 r8 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x00cf
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x00cf:
            r3.clear()
            r2.A00 = r10
            r3.set(r12)
            r1 = 2131821087(0x7f11021f, float:1.9274907E38)
            java.lang.String r1 = r8.A09(r1)
            r2.A02 = r1
            r8 = 1
            r3.set(r8)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r3, r11)
            r6.A3A(r2)
            java.lang.String[] r7 = new java.lang.String[]{r9, r7}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r1)
            X.1vV r2 = new X.1vV
            r2.<init>()
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0103
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0103:
            r3.clear()
            r2.A00 = r10
            r3.set(r12)
            r1 = 2131821083(0x7f11021b, float:1.92749E38)
            java.lang.String r1 = r9.A09(r1)
            r2.A02 = r1
            r3.set(r8)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r3, r7)
            r6.A3A(r2)
            X.0zR r1 = X.AnonymousClass1v8.A00(r0, r4, r8)
            r6.A3A(r1)
            r1 = 2
            X.0zR r1 = X.AnonymousClass1v8.A00(r0, r4, r1)
            r6.A3A(r1)
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r1)
            X.11D r1 = r2.A00
            r6.A3A(r1)
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r8 = (float) r1
            X.5X7 r4 = new X.5X7
            android.content.Context r1 = r0.A09
            r4.<init>(r1)
            X.0z4 r7 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0153
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x0153:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r4.A05 = r1
            r1 = 2131821096(0x7f110228, float:1.9274926E38)
            java.lang.String r1 = r7.A09(r1)
            r4.A03 = r1
            java.lang.String r2 = "continue_button"
            X.11G r1 = r4.A14()
            r1.A0S(r2)
            X.10G r3 = X.AnonymousClass10G.TOP
            int r2 = r7.A00(r8)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            r1 = 0
            int r2 = r7.A00(r1)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            java.lang.Class<X.1v8> r3 = X.AnonymousClass1v8.class
            goto L_0x0f4d
        L_0x0188:
            r1 = r2
            X.1vF r1 = (X.AnonymousClass1vF) r1
            X.5oW r9 = r1.A02
            com.google.common.collect.ImmutableList r6 = r1.A03
            X.2tA r3 = r1.A00
            int r5 = android.view.View.MeasureSpec.getSize(r30)
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r9.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r4.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r9.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r4.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r9.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r4.A2Z(r2, r1)
            r4.A27(r5)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r1 = (float) r1
            r4.A2Z(r2, r1)
            r12 = 0
            r2 = 2
            java.lang.String r8 = "loginStyle"
            java.lang.String r7 = "text"
            java.lang.String[] r10 = new java.lang.String[]{r8, r7}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r2)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0z4 r11 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x01f0
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x01f0:
            r5.clear()
            r2.A00 = r9
            r5.set(r12)
            r1 = 2131821089(0x7f110221, float:1.9274911E38)
            java.lang.String r1 = r11.A09(r1)
            r2.A02 = r1
            r1 = 1
            r5.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r5, r10)
            r4.A3A(r2)
            java.lang.String[] r8 = new java.lang.String[]{r8, r7}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r1)
            X.1vV r2 = new X.1vV
            r2.<init>()
            X.0z4 r5 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0224
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0224:
            r7.clear()
            r2.A00 = r9
            r7.set(r12)
            r1 = 2131821088(0x7f110220, float:1.927491E38)
            java.lang.String r1 = r5.A09(r1)
            r2.A02 = r1
            r1 = 1
            r7.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r7, r8)
            r4.A3A(r2)
            X.3go r5 = X.C58042t4.A02(r0)
            r1 = 1
            r5.A38(r1)
            X.2t4 r1 = r5.A01
            r1.A0K = r3
            r1 = 1065353216(0x3f800000, float:1.0)
            r5.A1n(r1)
            r1 = 0
            r5.A1o(r1)
            X.1GA r1 = new X.1GA
            r1.<init>(r0)
            X.2tI r3 = new X.2tI
            r3.<init>()
            if (r6 == 0) goto L_0x0273
            java.util.List r2 = r3.A00
            java.util.List r1 = java.util.Collections.EMPTY_LIST
            if (r2 != r1) goto L_0x026e
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r3.A00 = r1
        L_0x026e:
            java.util.List r1 = r3.A00
            r1.add(r6)
        L_0x0273:
            r5.A36(r3)
            X.2t4 r1 = r5.A31()
            r4.A3A(r1)
            X.1JQ r1 = X.AnonymousClass1JQ.LARGE
            int r1 = r1.B3A()
            float r7 = (float) r1
            X.5X7 r5 = new X.5X7
            android.content.Context r1 = r0.A09
            r5.<init>(r1)
            X.0z4 r6 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0295
            java.lang.String r2 = r1.A06
            r5.A07 = r2
        L_0x0295:
            java.lang.Integer r1 = X.AnonymousClass07B.A0n
            r5.A05 = r1
            r1 = 2131821071(0x7f11020f, float:1.9274875E38)
            java.lang.String r1 = r6.A09(r1)
            r5.A03 = r1
            java.lang.String r2 = "not_my_accounts_button"
            X.11G r1 = r5.A14()
            r1.A0S(r2)
            X.10G r3 = X.AnonymousClass10G.TOP
            int r2 = r6.A00(r7)
            X.11G r1 = r5.A14()
            r1.BJx(r3, r2)
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            r1 = 0
            int r2 = r6.A00(r1)
            X.11G r1 = r5.A14()
            r1.BJx(r3, r2)
            java.lang.Class<X.1vF> r3 = X.AnonymousClass1vF.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1880424122(0xffffffff8feb0146, float:-2.317328E-29)
            X.10N r1 = A0E(r3, r0, r1, r2)
            X.11G r0 = r5.A14()
            r0.A0G(r1)
            r4.A3A(r5)
            X.11D r0 = r4.A00
            return r0
        L_0x02e0:
            r6 = r2
            X.1v7 r6 = (X.C37321v7) r6
            X.5oW r11 = r6.A05
            com.facebook.account.recovery.common.model.AccountCandidateModel r14 = r6.A01
            X.CqM r5 = r6.A00
            com.facebook.mig.scheme.interfaces.MigColorScheme r13 = r6.A06
            X.9hi r2 = r6.A03
            int r4 = X.AnonymousClass1Y3.BCv
            X.0UN r3 = r6.A02
            r1 = 0
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r1, r4, r3)
            X.2zZ r12 = (X.C61832zZ) r12
            X.5CI r1 = r6.A04
            int r1 = r1.selectedRecoveryType
            int r3 = android.view.View.MeasureSpec.getSize(r30)
            X.9ZN r10 = new X.9ZN
            r15 = r1
            r10.<init>(r11, r12, r13, r14, r15)
            if (r1 != 0) goto L_0x0321
            com.google.common.collect.ImmutableList r1 = r14.A02()
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x04b0
            r1 = 1
            X.C37321v7.A01(r0, r1)
            if (r2 == 0) goto L_0x0321
            X.9hh r1 = r2.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r2 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection r2 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection) r2
            r1 = 1
            r2.A00 = r1
        L_0x0321:
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r4 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r4.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r5
            X.5oW r9 = r10.A03
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r2 = r1.B3A()
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            X.10G r6 = X.AnonymousClass10G.TOP
            X.1JM r1 = r9.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r6, r1)
            X.10G r6 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r9.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r6, r1)
            X.10G r6 = X.AnonymousClass10G.A04
            X.1JM r1 = r9.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r6, r1)
            r5.A2C(r3)
            r6 = 2
            java.lang.String r3 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r8 = new java.lang.String[]{r3, r1}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r6)
            X.1vU r3 = new X.1vU
            r3.<init>()
            X.0z4 r6 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0381
            java.lang.String r1 = r1.A06
            r3.A07 = r1
        L_0x0381:
            r7.clear()
            r3.A00 = r9
            r1 = 0
            r7.set(r1)
            r1 = 2131821064(0x7f110208, float:1.927486E38)
            java.lang.String r1 = r6.A09(r1)
            r3.A02 = r1
            r1 = 1
            r7.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r7, r8)
            r5.A3A(r3)
            X.1wd r6 = X.C16980y8.A00(r0)
            r1 = 1120403456(0x42c80000, float:100.0)
            r6.A20(r1)
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2X(r3, r1)
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            X.0uP r1 = X.C14950uP.CENTER
            r3.A3E(r1)
            r8 = 1109393408(0x42200000, float:40.0)
            r3.A1p(r8)
            r3.A1z(r8)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r7 = X.C78223oa.A00(r0)
            X.2zZ r9 = r10.A02
            com.facebook.account.recovery.common.model.AccountCandidateModel r1 = r10.A01
            java.lang.String r1 = r1.profilePictureUri
            android.net.Uri r1 = android.net.Uri.parse(r1)
            r9.A0P(r1)
            com.facebook.common.callercontext.CallerContext r1 = X.C37321v7.A07
            r9.A0Q(r1)
            X.303 r1 = r9.A0F()
            r7.A3U(r1)
            android.content.res.Resources r1 = r0.A03()
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()
            float r1 = r1.density
            float r1 = r1 * r8
            X.36c r1 = X.AnonymousClass36c.A01(r1)
            r7.A3T(r1)
            X.3oa r1 = r7.A35()
            r3.A3A(r1)
            X.11D r1 = r3.A00
            r6.A3A(r1)
            float r8 = (float) r2
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r3 = X.C17030yD.A00(r0)
            r2 = 2131821063(0x7f110207, float:1.9274859E38)
            com.facebook.account.recovery.common.model.AccountCandidateModel r1 = r10.A01
            java.lang.String r1 = r1.name
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            r3.A3S(r2, r1)
            X.5oW r1 = r10.A03
            X.10K r1 = r1.A0E()
            r3.A3f(r1)
            X.10G r1 = X.AnonymousClass10G.LEFT
            r3.A2X(r1, r8)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r3.A3V(r1)
            X.0yD r1 = r3.A39()
            r6.A3A(r1)
            X.0y8 r1 = r6.A00
            r5.A3A(r1)
            r2 = 0
            r1 = 1
            X.0zR r1 = X.C37321v7.A00(r0, r10, r1, r2, r2)
            r5.A3A(r1)
            r1 = 2
            X.0zR r1 = X.C37321v7.A00(r0, r10, r1, r2, r2)
            r5.A3A(r1)
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r1)
            X.11D r1 = r2.A00
            r5.A3A(r1)
            X.5X7 r6 = new X.5X7
            android.content.Context r1 = r0.A09
            r6.<init>(r1)
            X.0z4 r7 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0461
            java.lang.String r2 = r1.A06
            r6.A07 = r2
        L_0x0461:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r6.A05 = r1
            r1 = 2131821096(0x7f110228, float:1.9274926E38)
            java.lang.String r1 = r7.A09(r1)
            r6.A03 = r1
            java.lang.String r2 = "continue_button"
            X.11G r1 = r6.A14()
            r1.A0S(r2)
            X.10G r3 = X.AnonymousClass10G.TOP
            int r2 = r7.A00(r8)
            X.11G r1 = r6.A14()
            r1.BJx(r3, r2)
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            r1 = 0
            int r2 = r7.A00(r1)
            X.11G r1 = r6.A14()
            r1.BJx(r3, r2)
            java.lang.Class<X.1v7> r3 = X.C37321v7.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -670330384(0xffffffffd80b91f0, float:-6.1383565E14)
            X.10N r1 = A0E(r3, r0, r1, r2)
            X.11G r0 = r6.A14()
            r0.A0G(r1)
            r5.A3A(r6)
            X.11D r0 = r5.A00
            r4.A3Y(r0)
            goto L_0x06a2
        L_0x04b0:
            com.google.common.collect.ImmutableList r1 = r14.A00()
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0321
            r1 = 2
            X.C37321v7.A01(r0, r1)
            if (r2 == 0) goto L_0x0321
            X.9hh r1 = r2.A00
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueBase r2 = r1.A03
            com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection r2 = (com.facebook.messaging.accountlogin.fragment.segue.AccountLoginSegueRecMethodSelection) r2
            r1 = 2
            r2.A00 = r1
            goto L_0x0321
        L_0x04cb:
            X.1JN r2 = (X.AnonymousClass1JN) r2
            X.5oW r13 = r2.A04
            java.lang.String r4 = r2.A05
            boolean r3 = r2.A06
            boolean r1 = r2.A07
            X.9jI r14 = r2.A03
            X.CqM r2 = r2.A00
            int r5 = android.view.View.MeasureSpec.getSize(r30)
            X.9jl r12 = new X.9jl
            r15 = r4
            r16 = r3
            r17 = r1
            r12.<init>(r13, r14, r15, r16, r17)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r4 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r4.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r2
            X.5oW r8 = r12.A01
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r8.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r3.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r8.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r3.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r8.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r3.A2Z(r2, r1)
            r3.A2C(r5)
            r11 = 0
            r2 = 2
            java.lang.String r7 = "loginStyle"
            java.lang.String r6 = "text"
            java.lang.String[] r9 = new java.lang.String[]{r7, r6}
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>(r2)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0z4 r10 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0542
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0542:
            r5.clear()
            r2.A00 = r8
            r5.set(r11)
            r1 = 2131821081(0x7f110219, float:1.9274895E38)
            java.lang.String r1 = r10.A09(r1)
            r2.A02 = r1
            r1 = 1
            r5.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r5, r9)
            r3.A3A(r2)
            java.lang.String[] r7 = new java.lang.String[]{r7, r6}
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>(r1)
            X.1vV r2 = new X.1vV
            r2.<init>()
            X.0z4 r5 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0576
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0576:
            r6.clear()
            r2.A00 = r8
            r6.set(r11)
            r1 = 2131821080(0x7f110218, float:1.9274893E38)
            java.lang.String r1 = r5.A09(r1)
            r2.A02 = r1
            r1 = 1
            r6.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r6, r7)
            r3.A3A(r2)
            boolean r1 = r12.A04
            if (r1 == 0) goto L_0x06c5
            r2 = 1
            java.lang.String r1 = "stateContainer"
            java.lang.String[] r8 = new java.lang.String[]{r1}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r2)
            X.10I r1 = new X.10I
            android.content.Context r2 = r0.A09
            r1.<init>(r2)
            X.0z4 r6 = r0.A0B
            X.0zR r2 = r0.A04
            if (r2 == 0) goto L_0x05b3
            java.lang.String r5 = r2.A06
            r1.A07 = r5
        L_0x05b3:
            r7.clear()
            X.9jI r2 = r12.A00
            X.9gP r2 = r2.A00
            r1.A04 = r2
            r7.set(r11)
            java.lang.String r5 = "phone_number_email_field"
            X.11G r2 = r1.A14()
            r2.A0S(r5)
            java.lang.String r2 = "contact_point_field_tag"
            r1.A07 = r2
            r2 = 2131826662(0x7f1117e6, float:1.9286215E38)
            java.lang.String r2 = r6.A09(r2)
            r1.A06 = r2
            java.lang.Class<X.1JN> r6 = X.AnonymousClass1JN.class
            java.lang.Object[] r5 = new java.lang.Object[]{r0}
            r2 = -476912923(0xffffffffe392e2e5, float:-5.419148E21)
            X.10N r2 = A0E(r6, r0, r2, r5)
            r1.A02 = r2
            java.lang.Object[] r5 = new java.lang.Object[]{r0}
            r2 = 96515278(0x5c0b4ce, float:1.812201E-35)
            X.10N r2 = A0E(r6, r0, r2, r5)
            r1.A03 = r2
            r2 = 1
            X.AnonymousClass11F.A0C(r2, r7, r8)
        L_0x05f5:
            r3.A3A(r1)
            java.lang.String r1 = r12.A02
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 == 0) goto L_0x06a7
            X.1we r1 = X.AnonymousClass11D.A00(r0)
            X.11D r1 = r1.A00
        L_0x0606:
            r3.A3A(r1)
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r1)
            X.11D r1 = r2.A00
            r3.A3A(r1)
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r8 = (float) r1
            X.5X7 r6 = new X.5X7
            android.content.Context r1 = r0.A09
            r6.<init>(r1)
            X.0z4 r7 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x062f
            java.lang.String r2 = r1.A06
            r6.A07 = r2
        L_0x062f:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r6.A05 = r1
            r1 = 2131821096(0x7f110228, float:1.9274926E38)
            java.lang.String r1 = r7.A09(r1)
            r6.A03 = r1
            java.lang.String r2 = "continue_button"
            X.11G r1 = r6.A14()
            r1.A0S(r2)
            X.9jI r1 = r12.A00
            X.9gP r1 = r1.A00
            java.lang.String r1 = r1.A00
            java.lang.String r1 = r1.trim()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x065a
            boolean r2 = r12.A03
            r1 = 1
            if (r2 == 0) goto L_0x065b
        L_0x065a:
            r1 = 0
        L_0x065b:
            r6.A06 = r1
            X.10G r5 = X.AnonymousClass10G.TOP
            int r2 = r7.A00(r8)
            X.11G r1 = r6.A14()
            r1.BJx(r5, r2)
            X.10G r5 = X.AnonymousClass10G.BOTTOM
            r1 = 0
            int r2 = r7.A00(r1)
            X.11G r1 = r6.A14()
            r1.BJx(r5, r2)
            java.lang.Class<X.1JN> r5 = X.AnonymousClass1JN.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 57598607(0x36ee28f, float:7.020199E-37)
            X.10N r2 = A0E(r5, r0, r1, r2)
            X.11G r1 = r6.A14()
            r1.A0G(r2)
            r3.A3A(r6)
            X.11D r1 = r3.A00
            r4.A3Y(r1)
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -936136569(0xffffffffc833b087, float:-184002.11)
            X.10N r0 = A0E(r5, r0, r1, r2)
            r4.A2U(r0)
        L_0x06a2:
            X.5Vo r0 = r4.A37()
            return r0
        L_0x06a7:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            java.lang.String r1 = ""
            r2.A2n(r1)
            java.lang.String r1 = r12.A02
            r2.A3l(r1)
            X.10J r1 = X.AnonymousClass10J.A0T
            r2.A3f(r1)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_NORMAL
            r2.A3V(r1)
            X.0yD r1 = r2.A39()
            goto L_0x0606
        L_0x06c5:
            X.10J r1 = X.AnonymousClass10J.A0J
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r6 = X.C17030yD.A00(r0)
            r6.A3f(r1)
            X.9jI r1 = r12.A00
            X.9gP r1 = r1.A00
            java.lang.String r1 = r1.A00
            r6.A3l(r1)
            r6.A2v(r11)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2X(r2, r1)
            X.0yD r1 = r6.A39()
            r5.A3A(r1)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2X(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2X(r2, r1)
            X.11D r1 = r5.A00
            goto L_0x05f5
        L_0x070d:
            r3 = r2
            X.1QG r3 = (X.AnonymousClass1QG) r3
            X.5oW r12 = r3.A06
            X.9jZ r13 = r3.A05
            java.lang.String r14 = r3.A07
            X.CqM r5 = r3.A03
            long r15 = r3.A02
            long r1 = r3.A01
            int r3 = r3.A00
            int r4 = android.view.View.MeasureSpec.getSize(r30)
            X.9jm r11 = new X.9jm
            r17 = r1
            r19 = r3
            r11.<init>(r12, r13, r14, r15, r17, r19)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r7 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r7.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r5
            X.5oW r9 = r11.A04
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r3 = r1.B3A()
            X.1we r6 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r9.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r9.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r9.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            r6.A2C(r4)
            r8 = 2
            java.lang.String r2 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r8)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0z4 r8 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x078b
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x078b:
            r4.clear()
            r2.A00 = r9
            r1 = 0
            r4.set(r1)
            r1 = 2131821079(0x7f110217, float:1.9274891E38)
            java.lang.String r1 = r8.A09(r1)
            r2.A02 = r1
            r1 = 1
            r4.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r4, r5)
            r6.A3A(r2)
            X.1we r8 = X.AnonymousClass11D.A00(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r2.A3V(r1)
            r1 = 2131821077(0x7f110215, float:1.9274887E38)
            r2.A3P(r1)
            X.5oW r1 = r11.A04
            X.10K r1 = r1.A0E()
            r2.A3f(r1)
            X.0yD r1 = r2.A39()
            r8.A3A(r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r5 = X.C17030yD.A00(r0)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r5.A3V(r1)
            android.content.Context r4 = r0.A09
            r2 = 2131821078(0x7f110216, float:1.927489E38)
            java.lang.String r1 = r11.A05
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r1 = r4.getString(r2, r1)
            r5.A3l(r1)
            X.5oW r1 = r11.A04
            X.10K r1 = r1.A0E()
            r5.A3f(r1)
            X.0yD r1 = r5.A39()
            r8.A3A(r1)
            X.11D r1 = r8.A00
            r6.A3A(r1)
            X.1JQ r1 = X.AnonymousClass1JQ.LARGE
            int r1 = r1.B3A()
            float r5 = (float) r1
            float r8 = (float) r3
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            X.1JV r9 = new X.1JV
            r9.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0814
            java.lang.String r2 = r1.A06
            r9.A07 = r2
        L_0x0814:
            java.lang.String r2 = "pin_filed"
            X.11G r1 = r9.A14()
            r1.A0S(r2)
            X.9jZ r1 = r11.A03
            java.lang.String r1 = r1.A00
            r9.A01 = r1
            java.lang.Class<X.1QG> r3 = X.AnonymousClass1QG.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 677066169(0x285b35b9, float:1.2168591E-14)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r9.A00 = r1
            r4.A3A(r9)
            X.0uO r1 = X.C14940uO.CENTER
            r4.A3D(r1)
            X.10G r1 = X.AnonymousClass10G.TOP
            r4.A2X(r1, r5)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            r4.A2X(r1, r8)
            X.11D r1 = r4.A00
            r6.A3A(r1)
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r1)
            X.11D r1 = r2.A00
            r6.A3A(r1)
            int r2 = r11.A00
            r1 = 1
            java.lang.String r10 = "resend_button"
            if (r2 == r1) goto L_0x094f
            r1 = 2
            if (r2 == r1) goto L_0x08e4
            r5 = 2
            java.lang.String r2 = "buttonText"
            java.lang.String r1 = "countDownText"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r5)
            X.1JX r5 = new X.1JX
            android.content.Context r1 = r0.A09
            r5.<init>(r1)
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0880
            java.lang.String r2 = r1.A06
            r5.A07 = r2
        L_0x0880:
            r3.clear()
            X.11G r1 = r5.A14()
            r1.A0S(r10)
            long r1 = r11.A02
            r5.A01 = r1
            long r1 = r11.A01
            r5.A00 = r1
            r1 = 2131821093(0x7f110225, float:1.927492E38)
            java.lang.String r1 = r9.A09(r1)
            r5.A05 = r1
            r1 = 1
            r3.set(r1)
            r1 = 2131821100(0x7f11022c, float:1.9274934E38)
            java.lang.String r1 = r9.A09(r1)
            r5.A04 = r1
            r1 = 0
            r3.set(r1)
            java.lang.Class<X.1QG> r10 = X.AnonymousClass1QG.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -417937898(0xffffffffe716c616, float:-7.12009E23)
            X.10N r0 = A0E(r10, r0, r1, r2)
            r5.A03 = r0
            X.10G r2 = X.AnonymousClass10G.TOP
            int r1 = r9.A00(r8)
            X.11G r0 = r5.A14()
            r0.BJx(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            r0 = 0
            int r1 = r9.A00(r0)
            X.11G r0 = r5.A14()
            r0.BJx(r2, r1)
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r3, r4)
        L_0x08da:
            r6.A3A(r5)
            X.11D r0 = r6.A00
            r7.A3Y(r0)
            goto L_0x1240
        L_0x08e4:
            r2 = 1
            java.lang.String r1 = "buttonText"
            java.lang.String[] r4 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.DfM r5 = new X.DfM
            android.content.Context r1 = r0.A09
            r5.<init>(r1)
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0901
            java.lang.String r2 = r1.A06
            r5.A07 = r2
        L_0x0901:
            r3.clear()
            X.11G r1 = r5.A14()
            r1.A0S(r10)
            long r1 = r11.A02
            r5.A01 = r1
            long r1 = r11.A01
            r5.A00 = r1
            r1 = 2131821100(0x7f11022c, float:1.9274934E38)
            java.lang.String r1 = r9.A09(r1)
            r5.A04 = r1
            r1 = 0
            r3.set(r1)
            java.lang.Class<X.1QG> r10 = X.AnonymousClass1QG.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -417937898(0xffffffffe716c616, float:-7.12009E23)
            X.10N r0 = A0E(r10, r0, r1, r2)
            r5.A03 = r0
            X.10G r2 = X.AnonymousClass10G.TOP
            int r1 = r9.A00(r8)
            X.11G r0 = r5.A14()
            r0.BJx(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            r0 = 0
            int r1 = r9.A00(r0)
            X.11G r0 = r5.A14()
            r0.BJx(r2, r1)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
            goto L_0x08da
        L_0x094f:
            r2 = 1
            java.lang.String r1 = "buttonText"
            java.lang.String[] r4 = new java.lang.String[]{r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r2)
            X.DfN r5 = new X.DfN
            android.content.Context r1 = r0.A09
            r5.<init>(r1)
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x096c
            java.lang.String r2 = r1.A06
            r5.A07 = r2
        L_0x096c:
            r3.clear()
            X.11G r1 = r5.A14()
            r1.A0S(r10)
            long r1 = r11.A02
            r5.A01 = r1
            long r1 = r11.A01
            r5.A00 = r1
            r1 = 2131821100(0x7f11022c, float:1.9274934E38)
            java.lang.String r1 = r9.A09(r1)
            r5.A04 = r1
            r1 = 0
            r3.set(r1)
            java.lang.Class<X.1QG> r10 = X.AnonymousClass1QG.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -417937898(0xffffffffe716c616, float:-7.12009E23)
            X.10N r0 = A0E(r10, r0, r1, r2)
            r5.A03 = r0
            X.10G r2 = X.AnonymousClass10G.TOP
            int r1 = r9.A00(r8)
            X.11G r0 = r5.A14()
            r0.BJx(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            r0 = 0
            int r1 = r9.A00(r0)
            X.11G r0 = r5.A14()
            r0.BJx(r2, r1)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r3, r4)
            goto L_0x08da
        L_0x09bb:
            r1 = r2
            X.1Km r1 = (X.C22241Km) r1
            X.5oW r11 = r1.A05
            X.9ja r12 = r1.A04
            X.CqM r2 = r1.A02
            long r13 = r1.A01
            long r15 = r1.A00
            int r3 = android.view.View.MeasureSpec.getSize(r30)
            X.9jn r10 = new X.9jn
            r10.<init>(r11, r12, r13, r15)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r6 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r6.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r2
            X.5oW r8 = r10.A03
            X.1we r7 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r8.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r7.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r8.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r7.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r8.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r7.A2Z(r2, r1)
            r7.A2C(r3)
            r3 = 2
            java.lang.String r2 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r3)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0z4 r3 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0a2b
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0a2b:
            r4.clear()
            r2.A00 = r8
            r1 = 0
            r4.set(r1)
            r1 = 2131821099(0x7f11022b, float:1.9274932E38)
            java.lang.String r1 = r3.A09(r1)
            r2.A02 = r1
            r1 = 1
            r4.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r4, r5)
            r7.A3A(r2)
            X.1we r3 = X.AnonymousClass11D.A00(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r2.A3V(r1)
            r1 = 2131821098(0x7f11022a, float:1.927493E38)
            r2.A3P(r1)
            X.5oW r1 = r10.A03
            X.10K r1 = r1.A0E()
            r2.A3f(r1)
            X.0yD r1 = r2.A39()
            r3.A3A(r1)
            X.11D r1 = r3.A00
            r7.A3A(r1)
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r8 = (float) r1
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r5 = (float) r1
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            X.1JV r9 = new X.1JV
            r9.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0a8f
            java.lang.String r2 = r1.A06
            r9.A07 = r2
        L_0x0a8f:
            java.lang.String r2 = "pin_filed"
            X.11G r1 = r9.A14()
            r1.A0S(r2)
            X.9ja r1 = r10.A02
            java.lang.String r1 = r1.A00
            r9.A01 = r1
            java.lang.Class<X.1Km> r3 = X.C22241Km.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 677066169(0x285b35b9, float:1.2168591E-14)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r9.A00 = r1
            r4.A3A(r9)
            X.0uO r1 = X.C14940uO.CENTER
            r4.A3D(r1)
            X.10G r1 = X.AnonymousClass10G.TOP
            r4.A2X(r1, r8)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            r4.A2X(r1, r5)
            X.11D r1 = r4.A00
            r7.A3A(r1)
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r1)
            X.11D r1 = r2.A00
            r7.A3A(r1)
            X.5X7 r4 = new X.5X7
            android.content.Context r1 = r0.A09
            r4.<init>(r1)
            X.0z4 r3 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0ae3
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x0ae3:
            java.lang.Integer r1 = X.AnonymousClass07B.A0n
            r4.A05 = r1
            r1 = 2131821097(0x7f110229, float:1.9274928E38)
            java.lang.String r1 = r3.A09(r1)
            r4.A03 = r1
            java.lang.String r2 = "change_number_button"
            X.11G r1 = r4.A14()
            r1.A0S(r2)
            java.lang.Class<X.1Km> r3 = X.C22241Km.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1733638550(0xffffffff98aac66a, float:-4.4144317E-24)
            X.10N r2 = A0E(r3, r0, r1, r2)
            X.11G r1 = r4.A14()
            r1.A0G(r2)
            r7.A3A(r4)
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r9 = (float) r1
            r3 = 2
            java.lang.String r2 = "buttonText"
            java.lang.String r1 = "countDownText"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r3)
            X.1JX r3 = new X.1JX
            android.content.Context r1 = r0.A09
            r3.<init>(r1)
            X.0z4 r8 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0b36
            java.lang.String r2 = r1.A06
            r3.A07 = r2
        L_0x0b36:
            r4.clear()
            java.lang.String r2 = "resend_button"
            X.11G r1 = r3.A14()
            r1.A0S(r2)
            long r1 = r10.A01
            r3.A01 = r1
            long r1 = r10.A00
            r3.A00 = r1
            r1 = 2131821093(0x7f110225, float:1.927492E38)
            java.lang.String r1 = r8.A09(r1)
            r3.A05 = r1
            r1 = 1
            r4.set(r1)
            r1 = 2131821100(0x7f11022c, float:1.9274934E38)
            java.lang.String r1 = r8.A09(r1)
            r3.A04 = r1
            r1 = 0
            r4.set(r1)
            java.lang.Class<X.1Km> r10 = X.C22241Km.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -417937898(0xffffffffe716c616, float:-7.12009E23)
            X.10N r0 = A0E(r10, r0, r1, r2)
            r3.A03 = r0
            X.10G r2 = X.AnonymousClass10G.TOP
            int r1 = r8.A00(r9)
            X.11G r0 = r3.A14()
            r0.BJx(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            r0 = 0
            int r1 = r8.A00(r0)
            X.11G r0 = r3.A14()
            r0.BJx(r2, r1)
            r0 = 2
            X.AnonymousClass11F.A0C(r0, r4, r5)
            r7.A3A(r3)
            X.11D r0 = r7.A00
            r6.A3Y(r0)
            X.5Vo r0 = r6.A37()
            return r0
        L_0x0b9f:
            r5 = r2
            X.1Jk r5 = (X.C21971Jk) r5
            com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount r7 = r5.A04
            X.5oW r8 = r5.A03
            X.CqM r6 = r5.A00
            int r3 = X.AnonymousClass1Y3.BQw
            X.0UN r2 = r5.A01
            r1 = 0
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r3, r2)
            java.util.Locale r4 = (java.util.Locale) r4
            X.0Tq r13 = r5.A05
            int r3 = android.view.View.MeasureSpec.getSize(r30)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r5 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r5.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r6
            X.1we r6 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r8.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r8.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r8.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            r6.A2C(r3)
            X.0uO r1 = X.C14940uO.CENTER
            r6.A3D(r1)
            X.1we r9 = X.AnonymousClass11D.A00(r0)
            r9.A3D(r1)
            r9.A3C(r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r1 = (float) r1
            r9.A2Z(r2, r1)
            r3 = 2
            java.lang.String r2 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r11 = new java.lang.String[]{r2, r1}
            java.util.BitSet r10 = new java.util.BitSet
            r10.<init>(r3)
            X.1vU r3 = new X.1vU
            r3.<init>()
            X.0z4 r14 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0c2c
            java.lang.String r1 = r1.A06
            r3.A07 = r1
        L_0x0c2c:
            r10.clear()
            r3.A00 = r8
            r1 = 0
            r10.set(r1)
            java.lang.String r1 = r7.A03
            java.lang.Object[] r12 = new java.lang.Object[]{r1}
            r2 = 2131829641(0x7f112389, float:1.9292257E38)
            android.content.res.Resources r1 = r14.A00
            java.lang.String r1 = r1.getString(r2, r12)
            r3.A02 = r1
            r1 = 1
            r10.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r10, r11)
            r9.A3A(r3)
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r11 = (float) r1
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r10 = (float) r1
            r3 = 1125908480(0x431c0000, float:156.0)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r2 = X.C78223oa.A00(r0)
            r14 = 1065353216(0x3f800000, float:1.0)
            java.lang.Object r1 = r2.A02
            X.3oa r1 = (X.C78223oa) r1
            r1.A05 = r14
            r2.A1o(r14)
            android.graphics.drawable.ColorDrawable r12 = new android.graphics.drawable.ColorDrawable
            android.content.Context r14 = r0.A09
            r1 = 2132083218(0x7f150212, float:1.9806572E38)
            int r1 = X.AnonymousClass01R.A00(r14, r1)
            r12.<init>(r1)
            java.lang.Object r1 = r2.A02
            X.3oa r1 = (X.C78223oa) r1
            r1.A0B = r12
            java.lang.Object r12 = r13.get()
            X.2zZ r12 = (X.C61832zZ) r12
            java.lang.String r1 = r7.A06
            android.net.Uri r1 = android.net.Uri.parse(r1)
            r12.A0P(r1)
            com.facebook.common.callercontext.CallerContext r1 = X.C21971Jk.A06
            r12.A0Q(r1)
            X.303 r1 = r12.A0F()
            r2.A3U(r1)
            android.content.res.Resources r1 = r0.A03()
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()
            float r1 = r1.density
            float r1 = r1 * r3
            X.36c r1 = X.AnonymousClass36c.A01(r1)
            r2.A3T(r1)
            X.10G r1 = X.AnonymousClass10G.TOP
            r2.A2X(r1, r11)
            X.10G r1 = X.AnonymousClass10G.A09
            r2.A2X(r1, r10)
            r2.A1p(r3)
            X.3oa r1 = r2.A35()
            r9.A3A(r1)
            X.10K r10 = r8.A0E()
            X.1JQ r2 = X.AnonymousClass1JQ.XLARGE
            int r1 = r2.B3A()
            float r11 = (float) r1
            int r1 = r2.B3A()
            float r8 = (float) r1
            int r3 = r7.A00
            r1 = 1
            r2 = 2131829638(0x7f112386, float:1.929225E38)
            if (r3 != r1) goto L_0x0ce0
            r2 = 2131829639(0x7f112387, float:1.9292253E38)
        L_0x0ce0:
            java.lang.String r1 = r7.A03
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r3 = r0.A0D(r2, r1)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C17030yD.A00(r0)
            android.text.Layout$Alignment r1 = android.text.Layout.Alignment.ALIGN_CENTER
            r2.A3V(r1)
            X.10G r1 = X.AnonymousClass10G.TOP
            r2.A2X(r1, r11)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            r2.A2X(r1, r8)
            r2.A3f(r10)
            r2.A3l(r3)
            X.0yD r1 = r2.A39()
            r9.A3A(r1)
            X.11D r1 = r9.A00
            r6.A3A(r1)
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r1)
            X.11D r1 = r2.A00
            r6.A3A(r1)
            android.content.Context r3 = r0.A09
            r2 = 2131829647(0x7f11238f, float:1.929227E38)
            java.lang.String r1 = r7.A03
            java.lang.String r1 = r1.toUpperCase(r4)
            java.lang.Object[] r1 = new java.lang.Object[]{r1}
            java.lang.String r7 = r3.getString(r2, r1)
            X.5X7 r4 = new X.5X7
            android.content.Context r1 = r0.A09
            r4.<init>(r1)
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0d3f
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x0d3f:
            r2 = 1120403456(0x42c80000, float:100.0)
            X.11G r1 = r4.A14()
            r1.BL9(r2)
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r4.A05 = r1
            java.lang.String r2 = "continue_button_test_key"
            X.11G r1 = r4.A14()
            r1.A0S(r2)
            java.lang.Class<X.1Jk> r3 = X.C21971Jk.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -1047175806(0xffffffffc1955d82, float:-18.670658)
            X.10N r2 = A0E(r3, r0, r1, r2)
            X.11G r1 = r4.A14()
            r1.A0G(r2)
            r4.A03 = r7
            r6.A3A(r4)
            X.1JQ r1 = X.AnonymousClass1JQ.SMALL
            int r1 = r1.B3A()
            float r8 = (float) r1
            X.5X7 r4 = new X.5X7
            android.content.Context r1 = r0.A09
            r4.<init>(r1)
            X.0z4 r7 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0d86
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x0d86:
            java.lang.Integer r1 = X.AnonymousClass07B.A0n
            r4.A05 = r1
            X.10G r3 = X.AnonymousClass10G.TOP
            int r2 = r7.A00(r8)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            r1 = 0
            int r2 = r7.A00(r1)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            java.lang.String r2 = "skip_button_test_key"
            X.11G r1 = r4.A14()
            r1.A0S(r2)
            java.lang.Class<X.1Jk> r3 = X.C21971Jk.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 2037761066(0x7975c42a, float:7.9755713E34)
            X.10N r1 = A0E(r3, r0, r1, r2)
            X.11G r0 = r4.A14()
            r0.A0G(r1)
            r0 = 2131829640(0x7f112388, float:1.9292255E38)
            java.lang.String r0 = r7.A09(r0)
            r4.A03 = r0
            goto L_0x0f5f
        L_0x0dcd:
            r1 = r2
            X.1JO r1 = (X.AnonymousClass1JO) r1
            X.5oW r5 = r1.A03
            X.CqM r2 = r1.A00
            X.5EV r1 = r1.A02
            X.2Nf r1 = r1.stateContainer
            int r4 = android.view.View.MeasureSpec.getSize(r30)
            X.5oa r3 = new X.5oa
            r3.<init>(r5, r1)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r5 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r5.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r2
            X.5oW r9 = r3.A00
            X.1we r6 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r9.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r9.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r9.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r6.A2Z(r2, r1)
            r6.A2C(r4)
            r12 = 0
            r2 = 2
            java.lang.String r8 = "loginStyle"
            java.lang.String r7 = "text"
            java.lang.String[] r10 = new java.lang.String[]{r8, r7}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r2)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0z4 r11 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0e3c
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0e3c:
            r4.clear()
            r2.A00 = r9
            r4.set(r12)
            r1 = 2131821073(0x7f110211, float:1.9274879E38)
            java.lang.String r1 = r11.A09(r1)
            r2.A02 = r1
            r1 = 1
            r4.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r4, r10)
            r6.A3A(r2)
            java.lang.String[] r8 = new java.lang.String[]{r8, r7}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r1)
            X.1vV r2 = new X.1vV
            r2.<init>()
            X.0z4 r4 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0e70
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0e70:
            r7.clear()
            r2.A00 = r9
            r7.set(r12)
            r1 = 2131821072(0x7f110210, float:1.9274877E38)
            java.lang.String r1 = r4.A09(r1)
            r2.A02 = r1
            r1 = 1
            r7.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r7, r8)
            r6.A3A(r2)
            r2 = 1
            java.lang.String r1 = "stateContainer"
            java.lang.String[] r9 = new java.lang.String[]{r1}
            java.util.BitSet r8 = new java.util.BitSet
            r8.<init>(r2)
            X.0yC r7 = new X.0yC
            r7.<init>()
            X.0z4 r4 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0ea7
            java.lang.String r2 = r1.A06
            r7.A07 = r2
        L_0x0ea7:
            r8.clear()
            java.lang.String r2 = "password_field"
            X.11G r1 = r7.A14()
            r1.A0S(r2)
            X.2Nf r1 = r3.A01
            r7.A03 = r1
            r8.set(r12)
            r1 = 2131830050(0x7f112522, float:1.9293086E38)
            java.lang.String r1 = r4.A09(r1)
            r7.A05 = r1
            java.lang.Class<X.1JO> r4 = X.AnonymousClass1JO.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 1196116736(0x474b4b00, float:52043.0)
            X.10N r1 = A0E(r4, r0, r1, r2)
            r7.A02 = r1
            java.lang.Class<X.1JN> r4 = X.AnonymousClass1JN.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 96515278(0x5c0b4ce, float:1.812201E-35)
            X.10N r1 = A0E(r4, r0, r1, r2)
            r7.A01 = r1
            r1 = 1
            X.AnonymousClass11F.A0C(r1, r8, r9)
            r6.A3A(r7)
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            r1 = 1065353216(0x3f800000, float:1.0)
            r2.A1n(r1)
            X.11D r1 = r2.A00
            r6.A3A(r1)
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r8 = (float) r1
            X.5X7 r4 = new X.5X7
            android.content.Context r1 = r0.A09
            r4.<init>(r1)
            X.0z4 r7 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0f0e
            java.lang.String r2 = r1.A06
            r4.A07 = r2
        L_0x0f0e:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r4.A05 = r1
            r1 = 2131821096(0x7f110228, float:1.9274926E38)
            java.lang.String r1 = r7.A09(r1)
            r4.A03 = r1
            X.2Nf r1 = r3.A01
            java.lang.String r1 = r1.A00
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            r1 = r1 ^ 1
            r4.A06 = r1
            java.lang.String r2 = "continue_button"
            X.11G r1 = r4.A14()
            r1.A0S(r2)
            X.10G r3 = X.AnonymousClass10G.TOP
            int r2 = r7.A00(r8)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            X.10G r3 = X.AnonymousClass10G.BOTTOM
            r1 = 0
            int r2 = r7.A00(r1)
            X.11G r1 = r4.A14()
            r1.BJx(r3, r2)
            java.lang.Class<X.1JO> r3 = X.AnonymousClass1JO.class
        L_0x0f4d:
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = -670330384(0xffffffffd80b91f0, float:-6.1383565E14)
            X.10N r1 = A0E(r3, r0, r1, r2)
            X.11G r0 = r4.A14()
            r0.A0G(r1)
        L_0x0f5f:
            r6.A3A(r4)
            X.11D r0 = r6.A00
            r5.A3Y(r0)
            X.5Vo r0 = r5.A37()
            return r0
        L_0x0f6c:
            X.1Jj r2 = (X.C21961Jj) r2
            X.5oW r6 = r2.A03
            X.9jb r5 = r2.A02
            java.lang.String r4 = r2.A05
            java.lang.String r1 = r2.A04
            X.CqM r2 = r2.A00
            int r3 = android.view.View.MeasureSpec.getSize(r30)
            X.9jo r8 = new X.9jo
            r8.<init>(r6, r5, r4, r1)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r7 = X.C112145Vo.A00(r0)
            java.lang.Object r1 = r7.A00
            X.5Vo r1 = (X.C112145Vo) r1
            r1.A03 = r2
            X.5oW r6 = r8.A01
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            X.10G r2 = X.AnonymousClass10G.TOP
            X.1JM r1 = r6.A08()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.BOTTOM
            X.1JM r1 = r6.A06()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r2, r1)
            X.10G r2 = X.AnonymousClass10G.A04
            X.1JM r1 = r6.A07()
            int r1 = r1.B3A()
            float r1 = (float) r1
            r5.A2Z(r2, r1)
            r5.A2C(r3)
            java.lang.String r1 = r8.A03
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 == 0) goto L_0x10dc
            r9 = 2
            java.lang.String r2 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r9)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x0fe3
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x0fe3:
            r3.clear()
            r2.A00 = r6
            r1 = 0
            r3.set(r1)
            r1 = 2131821103(0x7f11022f, float:1.927494E38)
            java.lang.String r1 = r9.A09(r1)
            r2.A02 = r1
            r1 = 1
            r3.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r3, r4)
        L_0x0ffd:
            r5.A3A(r2)
            java.lang.String r1 = r8.A02
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 == 0) goto L_0x10ab
            r9 = 2
            java.lang.String r2 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r9)
            X.1vV r2 = new X.1vV
            r2.<init>()
            X.0z4 r9 = r0.A0B
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1025
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x1025:
            r3.clear()
            r2.A00 = r6
            r1 = 0
            r3.set(r1)
            r1 = 2131821102(0x7f11022e, float:1.9274938E38)
            java.lang.String r1 = r9.A09(r1)
            r2.A02 = r1
            r1 = 1
            r3.set(r1)
            r1 = 2
            X.AnonymousClass11F.A0C(r1, r3, r4)
        L_0x103f:
            r5.A3A(r2)
            X.1JQ r1 = X.AnonymousClass1JQ.XLARGE
            int r1 = r1.B3A()
            float r9 = (float) r1
            X.1JQ r1 = X.AnonymousClass1JQ.MEDIUM
            int r1 = r1.B3A()
            float r6 = (float) r1
            X.1we r4 = X.AnonymousClass11D.A00(r0)
            X.1JV r10 = new X.1JV
            r10.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x1061
            java.lang.String r2 = r1.A06
            r10.A07 = r2
        L_0x1061:
            java.lang.String r2 = "pin_filed"
            X.11G r1 = r10.A14()
            r1.A0S(r2)
            X.9jb r1 = r8.A00
            java.lang.String r1 = r1.A00
            r10.A01 = r1
            java.lang.Class<X.1Jj> r3 = X.C21961Jj.class
            java.lang.Object[] r2 = new java.lang.Object[]{r0}
            r1 = 677066169(0x285b35b9, float:1.2168591E-14)
            X.10N r1 = A0E(r3, r0, r1, r2)
            r10.A00 = r1
            r4.A3A(r10)
            X.0uO r1 = X.C14940uO.CENTER
            r4.A3D(r1)
            X.10G r1 = X.AnonymousClass10G.TOP
            r4.A2X(r1, r9)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            r4.A2X(r1, r6)
            X.11D r1 = r4.A00
            r5.A3A(r1)
            X.1we r1 = X.AnonymousClass11D.A00(r0)
            r0 = 1065353216(0x3f800000, float:1.0)
            r1.A1n(r0)
            X.11D r0 = r1.A00
            r5.A3A(r0)
            X.11D r0 = r5.A00
            r7.A3Y(r0)
            goto L_0x1240
        L_0x10ab:
            r9 = 2
            java.lang.String r2 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r9)
            X.1vV r2 = new X.1vV
            r2.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x10c6
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x10c6:
            r3.clear()
            r2.A00 = r6
            r1 = 0
            r3.set(r1)
            java.lang.String r1 = r8.A02
            r2.A02 = r1
            r1 = 1
            r3.set(r1)
            X.AnonymousClass11F.A0C(r9, r3, r4)
            goto L_0x103f
        L_0x10dc:
            r9 = 2
            java.lang.String r2 = "loginStyle"
            java.lang.String r1 = "text"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>(r9)
            X.1vU r2 = new X.1vU
            r2.<init>()
            X.0zR r1 = r0.A04
            if (r1 == 0) goto L_0x10f7
            java.lang.String r1 = r1.A06
            r2.A07 = r1
        L_0x10f7:
            r3.clear()
            r2.A00 = r6
            r1 = 0
            r3.set(r1)
            java.lang.String r1 = r8.A03
            r2.A02 = r1
            r1 = 1
            r3.set(r1)
            X.AnonymousClass11F.A0C(r9, r3, r4)
            goto L_0x0ffd
        L_0x110d:
            r1 = r2
            X.11H r1 = (X.AnonymousClass11H) r1
            X.5oW r12 = r1.A06
            java.lang.String r11 = r1.A08
            boolean r10 = r1.A09
            boolean r9 = r1.A0B
            boolean r8 = r1.A0A
            r24 = 0
            X.9j9 r7 = r1.A05
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r1.A07
            X.6Qm r5 = r1.A01
            X.CqM r4 = r1.A00
            int r2 = X.AnonymousClass1Y3.BL9
            X.0UN r13 = r1.A02
            r1 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r1, r2, r13)
            X.2Wk r3 = (X.AnonymousClass2Wk) r3
            int r2 = X.AnonymousClass1Y3.AMH
            r1 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r1, r2, r13)
            X.6Qd r2 = (X.C134316Qd) r2
            int r1 = android.view.View.MeasureSpec.getSize(r30)
            android.content.res.Resources r14 = r0.A03()
            r13 = 2132148337(0x7f160071, float:1.993865E38)
            int r13 = r14.getDimensionPixelSize(r13)
            r20 = 0
            if (r13 <= r1) goto L_0x114d
            r20 = 1
        L_0x114d:
            if (r13 > r1) goto L_0x1157
            boolean r13 = r12.A0H()
            r19 = 0
            if (r13 == 0) goto L_0x1159
        L_0x1157:
            r19 = 1
        L_0x1159:
            X.9jW r15 = new X.9jW
            r17 = r12
            r18 = r11
            r21 = r10
            r22 = r9
            r23 = r8
            r25 = r7
            r26 = r5
            r16 = r6
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
            com.facebook.litho.ComponentBuilderCBuilderShape2_0S0300000 r7 = X.C112145Vo.A00(r0)
            java.lang.Object r5 = r7.A00
            X.5Vo r5 = (X.C112145Vo) r5
            r5.A03 = r4
            X.5oW r14 = r15.A02
            X.1JQ r4 = X.AnonymousClass1JQ.MEDIUM
            int r12 = r4.B3A()
            X.1JQ r4 = X.AnonymousClass1JQ.SMALL
            int r11 = r4.B3A()
            X.1JQ r4 = X.AnonymousClass1JQ.XSMALL
            int r10 = r4.B3A()
            X.1JM r4 = r14.A0A()
            int r9 = r4.B3A()
            java.lang.Integer r16 = X.AnonymousClass07B.A0i
            int r4 = X.AnonymousClass1Y3.BLh
            X.0UN r3 = r3.A00
            r6 = 0
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r6, r4, r3)
            X.1eN r5 = (X.C28171eN) r5
            X.9M6 r4 = X.AnonymousClass9M6.A0a
            r3 = 1
            int r3 = r5.A03(r4, r3)
            if (r3 <= 0) goto L_0x11ab
            r6 = 1
        L_0x11ab:
            if (r6 == 0) goto L_0x130b
            java.lang.Integer r13 = X.AnonymousClass07B.A0N
        L_0x11af:
            boolean r3 = r15.A08
            r8 = 1065353216(0x3f800000, float:1.0)
            r6 = 0
            if (r3 == 0) goto L_0x1245
            X.1we r2 = X.AnonymousClass11D.A00(r0)
            X.10G r4 = X.AnonymousClass10G.TOP
            X.1JM r3 = r14.A08()
            int r3 = r3.B3A()
            float r3 = (float) r3
            r2.A2Z(r4, r3)
            X.10G r4 = X.AnonymousClass10G.BOTTOM
            X.1JM r3 = r14.A06()
            int r3 = r3.B3A()
            float r3 = (float) r3
            r2.A2Z(r4, r3)
            X.10G r4 = X.AnonymousClass10G.A04
            X.1JM r3 = r14.A07()
            int r3 = r3.B3A()
            float r3 = (float) r3
            r2.A2Z(r4, r3)
            r2.A2C(r1)
            float r4 = (float) r11
            float r3 = (float) r9
            X.0zR r1 = X.AnonymousClass11H.A03(r0, r15, r4, r3)
            r2.A3A(r1)
            X.0zR r1 = X.AnonymousClass11H.A02(r0, r15, r6, r6)
            r2.A3A(r1)
            X.0zR r1 = X.AnonymousClass11H.A04(r0, r15, r3, r6)
            r2.A3A(r1)
            float r3 = (float) r12
            X.0zR r1 = X.AnonymousClass11H.A00(r0, r15, r6, r3)
            r2.A3A(r1)
            float r1 = (float) r10
            X.0zR r1 = X.AnonymousClass11H.A0D(r0, r15, r1, r4)
            r2.A3A(r1)
            X.0zR r1 = X.AnonymousClass11H.A0I(r0, r15, r13, r3, r6)
            r2.A3A(r1)
            X.0zR r1 = X.AnonymousClass11H.A01(r0, r15, r3, r6)
            r2.A3A(r1)
            X.1we r1 = X.AnonymousClass11D.A00(r0)
            r1.A1n(r8)
            X.11D r1 = r1.A00
            r2.A3A(r1)
            r18 = 0
            r19 = 0
            r14 = r0
            r17 = r3
            X.0zR r0 = X.AnonymousClass11H.A0J(r14, r15, r16, r17, r18, r19)
            r2.A3A(r0)
            X.11D r0 = r2.A00
        L_0x1238:
            r7.A3Y(r0)
            java.lang.String r0 = "unified_login_root"
            r7.A2p(r0)
        L_0x1240:
            X.5Vo r0 = r7.A37()
            return r0
        L_0x1245:
            X.1we r5 = X.AnonymousClass11D.A00(r0)
            X.10G r4 = X.AnonymousClass10G.TOP
            X.1JM r3 = r14.A08()
            int r3 = r3.B3A()
            float r3 = (float) r3
            r5.A2Z(r4, r3)
            X.10G r4 = X.AnonymousClass10G.BOTTOM
            X.1JM r3 = r14.A06()
            int r3 = r3.B3A()
            float r3 = (float) r3
            r5.A2Z(r4, r3)
            X.10G r3 = X.AnonymousClass10G.A04
            X.1JM r4 = r14.A07()
            int r4 = r4.B3A()
            float r4 = (float) r4
            r5.A2Z(r3, r4)
            r5.A2C(r1)
            X.6Qm r1 = r15.A00
            X.0zR r1 = r2.A01(r0, r1)
            r5.A3A(r1)
            X.1we r1 = X.AnonymousClass11D.A00(r0)
            r1.A1n(r8)
            X.11D r1 = r1.A00
            r5.A3A(r1)
            float r2 = (float) r11
            X.5oW r1 = r15.A02
            int r4 = r1.A04()
            boolean r1 = r15.A07
            if (r1 != 0) goto L_0x1309
            if (r4 == 0) goto L_0x1309
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r3 = X.AnonymousClass10H.A00(r0)
            r1 = 2132279332(0x7f180024, float:2.0204339E38)
            r3.A3H(r1)
            X.10G r1 = X.AnonymousClass10G.TOP
            r3.A2X(r1, r2)
            X.10G r1 = X.AnonymousClass10G.BOTTOM
            r3.A2X(r1, r2)
            float r1 = (float) r4
            r3.A1p(r1)
            X.10H r1 = r3.A33()
        L_0x12b4:
            r5.A3A(r1)
            X.1we r1 = X.AnonymousClass11D.A00(r0)
            r1.A1n(r8)
            X.11D r1 = r1.A00
            r5.A3A(r1)
            float r3 = (float) r9
            X.0zR r1 = X.AnonymousClass11H.A03(r0, r15, r2, r3)
            r5.A3A(r1)
            X.0zR r1 = X.AnonymousClass11H.A02(r0, r15, r6, r6)
            r5.A3A(r1)
            X.0zR r1 = X.AnonymousClass11H.A04(r0, r15, r3, r6)
            r5.A3A(r1)
            float r3 = (float) r12
            X.0zR r1 = X.AnonymousClass11H.A00(r0, r15, r6, r3)
            r5.A3A(r1)
            float r1 = (float) r10
            X.0zR r1 = X.AnonymousClass11H.A0D(r0, r15, r1, r2)
            r5.A3A(r1)
            X.0zR r1 = X.AnonymousClass11H.A0I(r0, r15, r13, r3, r6)
            r5.A3A(r1)
            r18 = 0
            r19 = 0
            r14 = r0
            r17 = r3
            X.0zR r1 = X.AnonymousClass11H.A0J(r14, r15, r16, r17, r18, r19)
            r5.A3A(r1)
            X.0zR r0 = X.AnonymousClass11H.A01(r0, r15, r2, r6)
            r5.A3A(r0)
            X.11D r0 = r5.A00
            goto L_0x1238
        L_0x1309:
            r1 = 0
            goto L_0x12b4
        L_0x130b:
            java.lang.Integer r13 = X.AnonymousClass07B.A0C
            goto L_0x11af
        L_0x130f:
            X.1RX r2 = (X.AnonymousClass1RX) r2
            com.facebook.messaging.ui.name.ThreadNameViewData r3 = r2.A07
            int r1 = r2.A01
            r20 = r1
            int r15 = r2.A02
            android.graphics.Typeface r1 = r2.A03
            r19 = r1
            int r14 = r2.A00
            android.text.Layout$Alignment r6 = r2.A04
            X.1Kv r13 = r2.A05
            boolean r4 = r2.A08
            int r5 = X.AnonymousClass1Y3.ASV
            X.0UN r2 = r2.A06
            r1 = 0
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r1, r5, r2)
            X.1Sb r12 = (X.C24061Sb) r12
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C14910uL.A02(r0)
            r1 = 0
            r2.A3n(r1)
            r2.A3I(r14)
            android.text.TextUtils$TruncateAt r1 = android.text.TextUtils.TruncateAt.END
            r2.A3X(r1)
            java.lang.Object r5 = r2.A00
            X.0uL r5 = (X.C14910uL) r5
            r5.A0N = r15
            r1 = r20
            r2.A3K(r1)
            r2.A3W(r6)
            r1 = r19
            r2.A3T(r1)
            r5.A0V = r13
            java.lang.String r11 = "thread_name"
            if (r3 != 0) goto L_0x1371
            java.lang.String r1 = ""
            r2.A3j(r1)
            X.1kz r1 = X.C21841Ix.A00(r0)
            X.0uL r0 = r2.A35()
            r1.A34(r0)
            r1.A2p(r11)
            X.1Ix r1 = r1.A31()
            return r1
        L_0x1371:
            boolean r1 = r3.A02
            if (r1 == 0) goto L_0x13a6
            java.lang.String r1 = r3.A01
            java.lang.CharSequence r1 = r12.A01(r1, r15)
            r2.A3j(r1)
            X.1kz r5 = X.C21841Ix.A00(r0)
            X.0uL r1 = r2.A35()
            r5.A34(r1)
            r5.A2p(r11)
            java.lang.String r3 = r3.A01
            r1 = r3
            if (r4 == 0) goto L_0x139e
            android.content.Context r2 = r0.A09
            r1 = 2131833842(0x7f1133f2, float:1.9300777E38)
            java.lang.Object[] r0 = new java.lang.Object[]{r3}
            java.lang.String r1 = r2.getString(r1, r0)
        L_0x139e:
            r5.A2z(r1)
            X.1Ix r1 = r5.A31()
            return r1
        L_0x13a6:
            com.google.common.collect.ImmutableList r10 = r3.A00
            int r9 = r10.size()
            java.lang.String r8 = r12.A04(r10)
            if (r4 == 0) goto L_0x13bf
            android.content.Context r4 = r0.A09
            r3 = 2131833842(0x7f1133f2, float:1.9300777E38)
            java.lang.Object[] r1 = new java.lang.Object[]{r8}
            java.lang.String r8 = r4.getString(r3, r1)
        L_0x13bf:
            r1 = 2
            if (r9 < r1) goto L_0x14a3
            int r1 = android.view.View.MeasureSpec.getMode(r29)
            if (r1 == 0) goto L_0x14a3
            r3 = -2110321208(0xffffffff82370dc8, float:-1.3448672E-37)
            java.lang.String r1 = "ThreadNameComponent#GroupName"
            X.C005505z.A03(r1, r3)
            int r7 = android.view.View.MeasureSpec.getSize(r29)     // Catch:{ all -> 0x149b }
            r3 = 0
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r3)     // Catch:{ all -> 0x149b }
            X.10L r5 = new X.10L     // Catch:{ all -> 0x149b }
            r5.<init>()     // Catch:{ all -> 0x149b }
            java.lang.Object r3 = r10.get(r3)     // Catch:{ all -> 0x149b }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x149b }
            int r1 = r9 + -1
            java.lang.String r1 = r12.A02(r1)     // Catch:{ all -> 0x149b }
            java.lang.String r1 = X.AnonymousClass08S.A0J(r3, r1)     // Catch:{ all -> 0x149b }
            java.lang.CharSequence r1 = r12.A01(r1, r15)     // Catch:{ all -> 0x149b }
            r2.A3j(r1)     // Catch:{ all -> 0x149b }
            X.0uL r1 = r2.A35()     // Catch:{ all -> 0x149b }
            r4 = 1
        L_0x13fa:
            if (r4 > r9) goto L_0x145d
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x149b }
            r18 = r1
            r1 = 0
            com.google.common.collect.ImmutableList r2 = r10.subList(r1, r4)     // Catch:{ all -> 0x149b }
            X.1Sc r1 = r12.A00     // Catch:{ all -> 0x149b }
            java.lang.String r1 = X.C24071Sc.A02(r1, r2)     // Catch:{ all -> 0x149b }
            r3.<init>(r1)     // Catch:{ all -> 0x149b }
            if (r4 >= r9) goto L_0x1419
            int r1 = r9 - r4
            java.lang.String r1 = r12.A02(r1)     // Catch:{ all -> 0x149b }
            r3.append(r1)     // Catch:{ all -> 0x149b }
        L_0x1419:
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r2 = X.C14910uL.A02(r0)     // Catch:{ all -> 0x149b }
            r1 = 0
            r2.A3n(r1)     // Catch:{ all -> 0x149b }
            r2.A3I(r14)     // Catch:{ all -> 0x149b }
            android.text.TextUtils$TruncateAt r1 = android.text.TextUtils.TruncateAt.END     // Catch:{ all -> 0x149b }
            r2.A3X(r1)     // Catch:{ all -> 0x149b }
            java.lang.Object r1 = r2.A00     // Catch:{ all -> 0x149b }
            X.0uL r1 = (X.C14910uL) r1     // Catch:{ all -> 0x149b }
            r1.A0N = r15     // Catch:{ all -> 0x149b }
            r16 = r2
            r17 = r20
            r16.A3K(r17)     // Catch:{ all -> 0x149b }
            r17 = r19
            r16.A3T(r17)     // Catch:{ all -> 0x149b }
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x149b }
            java.lang.CharSequence r1 = r12.A01(r1, r15)     // Catch:{ all -> 0x149b }
            r2.A3j(r1)     // Catch:{ all -> 0x149b }
            java.lang.Object r1 = r2.A00     // Catch:{ all -> 0x149b }
            X.0uL r1 = (X.C14910uL) r1     // Catch:{ all -> 0x149b }
            r1.A0V = r13     // Catch:{ all -> 0x149b }
            X.0uL r1 = r2.A35()     // Catch:{ all -> 0x149b }
            r1.A1F(r0, r6, r6, r5)     // Catch:{ all -> 0x149b }
            int r2 = r5.A01     // Catch:{ all -> 0x149b }
            if (r2 <= r7) goto L_0x1458
            goto L_0x145b
        L_0x1458:
            int r4 = r4 + 1
            goto L_0x13fa
        L_0x145b:
            r1 = r18
        L_0x145d:
            X.1kq r2 = r0.A06     // Catch:{ all -> 0x149b }
            if (r2 != 0) goto L_0x1462
            goto L_0x1465
        L_0x1462:
            X.15v r2 = r2.A01     // Catch:{ all -> 0x149b }
            goto L_0x1466
        L_0x1465:
            r2 = 0
        L_0x1466:
            if (r2 == 0) goto L_0x148b
            java.util.Map r3 = r2.A0h     // Catch:{ all -> 0x149b }
            int r2 = r1.A00     // Catch:{ all -> 0x149b }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x149b }
            r3.remove(r2)     // Catch:{ all -> 0x149b }
            X.1kz r0 = X.C21841Ix.A00(r0)     // Catch:{ all -> 0x149b }
            r0.A34(r1)     // Catch:{ all -> 0x149b }
            r0.A2p(r11)     // Catch:{ all -> 0x149b }
            r0.A2z(r8)     // Catch:{ all -> 0x149b }
            X.1Ix r1 = r0.A31()     // Catch:{ all -> 0x149b }
            r0 = -1167272737(0xffffffffba6cd4df, float:-9.034406E-4)
            X.C005505z.A00(r0)
            return r1
        L_0x148b:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x149b }
            java.lang.String r1 = r1.A1A()     // Catch:{ all -> 0x149b }
            java.lang.String r0 = ": Trying to access the cached InternalNode for a component outside of a LayoutState calculation. If that is what you must do, see Component#measureMightNotCacheInternalNode."
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x149b }
            r2.<init>(r0)     // Catch:{ all -> 0x149b }
            throw r2     // Catch:{ all -> 0x149b }
        L_0x149b:
            r1 = move-exception
            r0 = 325683594(0x1369898a, float:2.9476547E-27)
            X.C005505z.A00(r0)
            throw r1
        L_0x14a3:
            java.lang.String r1 = r12.A04(r10)
            java.lang.CharSequence r1 = r12.A01(r1, r15)
            r2.A3j(r1)
            X.1kz r1 = X.C21841Ix.A00(r0)
            X.0uL r0 = r2.A35()
            r1.A34(r0)
            r1.A2p(r11)
            r1.A2z(r8)
            X.1Ix r1 = r1.A31()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17780zS.A0P(X.0p4, int, int):X.0zR");
    }
}
