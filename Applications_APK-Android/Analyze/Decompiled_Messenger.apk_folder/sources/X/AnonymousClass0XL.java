package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0XL  reason: invalid class name */
public final class AnonymousClass0XL extends AnonymousClass0UV {
    public static C05920aY A00(AnonymousClass1XX r0, Context context) {
        C24851Xi A01 = r0.getInjectorThreadStack().A01();
        if (A01 == null) {
            A01 = AnonymousClass1XX.get(context).getScopeAwareInjector();
        }
        return A01.B9T();
    }

    public static final C05920aY A01(AnonymousClass1XY r1) {
        C24781Xb scopeUnawareInjector = r1.getScopeUnawareInjector();
        Context A00 = AnonymousClass1YA.A00(r1);
        C24851Xi A01 = scopeUnawareInjector.getInjectorThreadStack().A01();
        if (A01 == null) {
            A01 = AnonymousClass1XX.get(A00).getScopeAwareInjector();
        }
        return A01.B9T();
    }
}
