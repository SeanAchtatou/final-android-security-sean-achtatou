package X;

import android.content.ContentResolver;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1PW  reason: invalid class name */
public final class AnonymousClass1PW {
    public AnonymousClass1Q3 A00;
    public AnonymousClass1Q3 A01;
    public AnonymousClass1Q3 A02;
    public Map A03 = new HashMap();
    public AnonymousClass1Q3 A04;
    public AnonymousClass1Q3 A05;
    public AnonymousClass1Q3 A06;
    public AnonymousClass1Q3 A07;
    public AnonymousClass1Q3 A08;
    public AnonymousClass1Q3 A09;
    public AnonymousClass1Q3 A0A;
    public AnonymousClass1Q3 A0B;
    public AnonymousClass1Q3 A0C;
    public AnonymousClass1Q3 A0D;
    public AnonymousClass1Q3 A0E;
    public AnonymousClass1Q3 A0F;
    public AnonymousClass1Q3 A0G;
    public Map A0H = new HashMap();
    public Map A0I = new HashMap();
    private AnonymousClass1Q3 A0J;
    public final C23341Pd A0K;
    public final AnonymousClass1PR A0L;
    public final C23351Pe A0M;
    public final boolean A0N;
    public final boolean A0O;
    public final boolean A0P;
    private final ContentResolver A0Q;
    private final C23281Ox A0R;
    private final boolean A0S;
    private final boolean A0T;
    private final boolean A0U;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003c, code lost:
        if (r4.A0T != false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized X.AnonymousClass1Q3 A00() {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x000c
            java.lang.String r0 = "ProducerSequenceFactory#getCommonNetworkFetchToEncodedMemorySequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x005d }
        L_0x000c:
            X.1Q3 r0 = r4.A0J     // Catch:{ all -> 0x005d }
            if (r0 != 0) goto L_0x0050
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "ProducerSequenceFactory#getCommonNetworkFetchToEncodedMemorySequence:init"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x005d }
        L_0x001b:
            X.1Pd r0 = r4.A0K     // Catch:{ all -> 0x005d }
            X.1Ox r3 = r4.A0R     // Catch:{ all -> 0x005d }
            X.1Q2 r2 = new X.1Q2     // Catch:{ all -> 0x005d }
            X.1iR r1 = r0.A0I     // Catch:{ all -> 0x005d }
            X.1Nc r0 = r0.A0H     // Catch:{ all -> 0x005d }
            r2.<init>(r1, r0, r3)     // Catch:{ all -> 0x005d }
            X.1Q3 r0 = A04(r4, r2)     // Catch:{ all -> 0x005d }
            X.1QA r3 = new X.1QA     // Catch:{ all -> 0x005d }
            r3.<init>(r0)     // Catch:{ all -> 0x005d }
            r4.A0J = r3     // Catch:{ all -> 0x005d }
            X.1Pd r2 = r4.A0K     // Catch:{ all -> 0x005d }
            boolean r0 = r4.A0U     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x003e
            boolean r0 = r4.A0T     // Catch:{ all -> 0x005d }
            r1 = 1
            if (r0 == 0) goto L_0x003f
        L_0x003e:
            r1 = 0
        L_0x003f:
            X.1Pe r0 = r4.A0M     // Catch:{ all -> 0x005d }
            X.1QB r0 = r2.A00(r3, r1, r0)     // Catch:{ all -> 0x005d }
            r4.A0J = r0     // Catch:{ all -> 0x005d }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0050
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x005d }
        L_0x0050:
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0059
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x005d }
        L_0x0059:
            X.1Q3 r0 = r4.A0J     // Catch:{ all -> 0x005d }
            monitor-exit(r4)
            return r0
        L_0x005d:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1PW.A00():X.1Q3");
    }

    private synchronized AnonymousClass1Q3 A01() {
        if (this.A02 == null) {
            C23341Pd r3 = this.A0K;
            this.A02 = A07(new C196519Mh(r3.A0E.AaQ(), r3.A00));
        }
        return this.A02;
    }

    public static synchronized AnonymousClass1Q3 A02(AnonymousClass1PW r4) {
        AnonymousClass1Q3 r0;
        synchronized (r4) {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("ProducerSequenceFactory#getBackgroundLocalFileFetchToEncodeMemorySequence");
            }
            if (r4.A00 == null) {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A02("ProducerSequenceFactory#getBackgroundLocalFileFetchToEncodeMemorySequence:init");
                }
                C23341Pd r3 = r4.A0K;
                r4.A00 = r4.A0K.A01(A04(r4, new C80593sj(r3.A0E.AaQ(), r3.A0I)), r4.A0L);
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
            }
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
            r0 = r4.A00;
        }
        return r0;
    }

    public static synchronized AnonymousClass1Q3 A03(AnonymousClass1PW r3) {
        AnonymousClass1Q3 r0;
        synchronized (r3) {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("ProducerSequenceFactory#getBackgroundNetworkFetchToEncodedMemorySequence");
            }
            if (r3.A01 == null) {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A02("ProducerSequenceFactory#getBackgroundNetworkFetchToEncodedMemorySequence:init");
                }
                r3.A01 = r3.A0K.A01(r3.A00(), r3.A0L);
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
            }
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
            r0 = r3.A01;
        }
        return r0;
    }

    public static AnonymousClass1Q3 A04(AnonymousClass1PW r10, AnonymousClass1Q3 r11) {
        AnonymousClass1Q5 r3;
        AnonymousClass1Q3 r9 = r11;
        if (AnonymousClass1Q4.A03 && (!r10.A0P || AnonymousClass1Q4.A00 == null)) {
            C23341Pd r32 = r10.A0K;
            r9 = new C55992p5(r32.A0E.AaN(), r32.A0I, r11);
        }
        if (r10.A0S) {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("ProducerSequenceFactory#newDiskCacheSequence");
            }
            if (r10.A0N) {
                C23341Pd r0 = r10.A0K;
                C30911iq r5 = r0.A08;
                C22601Mc r6 = r0.A0A;
                r3 = new AnonymousClass1Q5(r5, r0.A09, r6, new AnonymousClass9Mm(r5, r6, r0.A0I, r0.A0H, r9));
            } else {
                C23341Pd r02 = r10.A0K;
                r3 = new AnonymousClass1Q5(r02.A08, r02.A09, r02.A0A, r9);
            }
            C23341Pd r03 = r10.A0K;
            r9 = new AnonymousClass1Q6(r03.A08, r03.A09, r03.A0A, r3);
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
        }
        C23341Pd r4 = r10.A0K;
        C23311Pa r04 = r4.A0C;
        C22601Mc r2 = r4.A0A;
        return new AnonymousClass1Q8(r2, r4.A0L, new AnonymousClass1Q7(r04, r2, r9));
    }

    public static synchronized AnonymousClass1Q3 A05(AnonymousClass1PW r4, AnonymousClass1Q3 r5) {
        AnonymousClass1Q3 r3;
        synchronized (r4) {
            r3 = (AnonymousClass1Q3) r4.A03.get(r5);
            if (r3 == null) {
                C23341Pd r0 = r4.A0K;
                r3 = new C196589Mp(r5, r0.A05, r0.A04, r0.A03);
                r4.A03.put(r5, r3);
            }
        }
        return r3;
    }

    private AnonymousClass1Q3 A07(AnonymousClass1Q3 r5) {
        C23341Pd r3 = this.A0K;
        C23311Pa r1 = r3.A0B;
        C22601Mc r0 = r3.A0A;
        AnonymousClass0mS A012 = r3.A01(new AnonymousClass1QE(r0, new AnonymousClass1QD(r1, r0, r5)), this.A0L);
        C23341Pd r02 = this.A0K;
        return new AnonymousClass1QF(r02.A0B, r02.A0A, A012);
    }

    private AnonymousClass1Q3 A09(AnonymousClass1Q3 r6) {
        C23341Pd r4 = this.A0K;
        return A0A(r6, new C52822jk[]{new C52832jl(r4.A0E.AaT(), r4.A0I, r4.A00)});
    }

    public AnonymousClass1PW(ContentResolver contentResolver, C23341Pd r3, C23281Ox r4, boolean z, boolean z2, AnonymousClass1PR r7, boolean z3, boolean z4, boolean z5, boolean z6, C23351Pe r12) {
        this.A0Q = contentResolver;
        this.A0K = r3;
        this.A0R = r4;
        this.A0U = z;
        this.A0P = z2;
        this.A0L = r7;
        this.A0T = z3;
        this.A0O = z4;
        this.A0N = z5;
        this.A0S = z6;
        this.A0M = r12;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005f, code lost:
        if (r1.startsWith("video/") == false) goto L_0x0061;
     */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0180  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1Q3 A06(X.AnonymousClass1PW r8, X.AnonymousClass1Q0 r9) {
        /*
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0187 }
            if (r0 == 0) goto L_0x000b
            java.lang.String r0 = "ProducerSequenceFactory#getBasicDecodedImageSequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x0187 }
        L_0x000b:
            X.C05520Zg.A02(r9)     // Catch:{ all -> 0x0187 }
            android.net.Uri r3 = r9.A02     // Catch:{ all -> 0x0187 }
            java.lang.String r0 = "Uri is null."
            X.C05520Zg.A03(r3, r0)     // Catch:{ all -> 0x0187 }
            int r0 = r9.A01     // Catch:{ all -> 0x0187 }
            if (r0 == 0) goto L_0x0140
            switch(r0) {
                case 2: goto L_0x002c;
                case 3: goto L_0x0032;
                case 4: goto L_0x0050;
                case 5: goto L_0x00a4;
                case 6: goto L_0x00c4;
                case 7: goto L_0x00e4;
                case 8: goto L_0x0121;
                default: goto L_0x001c;
            }     // Catch:{ all -> 0x0187 }
        L_0x001c:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0187 }
            java.lang.String r1 = "Unsupported uri scheme! Uri is: "
            java.lang.String r0 = A0B(r3)     // Catch:{ all -> 0x0187 }
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x0187 }
            r2.<init>(r0)     // Catch:{ all -> 0x0187 }
            throw r2     // Catch:{ all -> 0x0187 }
        L_0x002c:
            X.1Q3 r1 = r8.A01()     // Catch:{ all -> 0x0187 }
            goto L_0x017a
        L_0x0032:
            monitor-enter(r8)     // Catch:{ all -> 0x0187 }
            X.1Q3 r0 = r8.A0B     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x004c
            X.1Pd r3 = r8.A0K     // Catch:{ all -> 0x0184 }
            X.3sj r2 = new X.3sj     // Catch:{ all -> 0x0184 }
            X.1Mg r0 = r3.A0E     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r1 = r0.AaQ()     // Catch:{ all -> 0x0184 }
            X.1iR r0 = r3.A0I     // Catch:{ all -> 0x0184 }
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0184 }
            X.1Q3 r0 = r8.A09(r2)     // Catch:{ all -> 0x0184 }
            r8.A0B = r0     // Catch:{ all -> 0x0184 }
        L_0x004c:
            X.1Q3 r1 = r8.A0B     // Catch:{ all -> 0x0184 }
            goto L_0x0179
        L_0x0050:
            android.content.ContentResolver r0 = r8.A0Q     // Catch:{ all -> 0x0187 }
            java.lang.String r1 = r0.getType(r3)     // Catch:{ all -> 0x0187 }
            if (r1 == 0) goto L_0x0061
            java.lang.String r0 = "video/"
            boolean r1 = r1.startsWith(r0)     // Catch:{ all -> 0x0187 }
            r0 = 1
            if (r1 != 0) goto L_0x0062
        L_0x0061:
            r0 = 0
        L_0x0062:
            if (r0 == 0) goto L_0x006a
            X.1Q3 r1 = r8.A01()     // Catch:{ all -> 0x0187 }
            goto L_0x017a
        L_0x006a:
            monitor-enter(r8)     // Catch:{ all -> 0x0187 }
            X.1Q3 r0 = r8.A08     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x00a0
            X.1Pd r7 = r8.A0K     // Catch:{ all -> 0x0184 }
            X.8tO r6 = new X.8tO     // Catch:{ all -> 0x0184 }
            X.1Mg r5 = r7.A0E     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r0 = r5.AaQ()     // Catch:{ all -> 0x0184 }
            X.1iR r2 = r7.A0I     // Catch:{ all -> 0x0184 }
            android.content.ContentResolver r1 = r7.A00     // Catch:{ all -> 0x0184 }
            r6.<init>(r0, r2, r1)     // Catch:{ all -> 0x0184 }
            X.8tL r4 = new X.8tL     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r0 = r5.AaQ()     // Catch:{ all -> 0x0184 }
            r4.<init>(r0, r2, r1)     // Catch:{ all -> 0x0184 }
            X.2jl r3 = new X.2jl     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r2 = r5.AaT()     // Catch:{ all -> 0x0184 }
            X.1iR r1 = r7.A0I     // Catch:{ all -> 0x0184 }
            android.content.ContentResolver r0 = r7.A00     // Catch:{ all -> 0x0184 }
            r3.<init>(r2, r1, r0)     // Catch:{ all -> 0x0184 }
            X.2jk[] r0 = new X.C52822jk[]{r4, r3}     // Catch:{ all -> 0x0184 }
            X.1Q3 r0 = r8.A0A(r6, r0)     // Catch:{ all -> 0x0184 }
            r8.A08 = r0     // Catch:{ all -> 0x0184 }
        L_0x00a0:
            X.1Q3 r1 = r8.A08     // Catch:{ all -> 0x0184 }
            goto L_0x0179
        L_0x00a4:
            monitor-enter(r8)     // Catch:{ all -> 0x0187 }
            X.1Q3 r0 = r8.A06     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x00c0
            X.1Pd r4 = r8.A0K     // Catch:{ all -> 0x0184 }
            X.8tM r3 = new X.8tM     // Catch:{ all -> 0x0184 }
            X.1Mg r0 = r4.A0E     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r2 = r0.AaQ()     // Catch:{ all -> 0x0184 }
            X.1iR r1 = r4.A0I     // Catch:{ all -> 0x0184 }
            android.content.res.AssetManager r0 = r4.A01     // Catch:{ all -> 0x0184 }
            r3.<init>(r2, r1, r0)     // Catch:{ all -> 0x0184 }
            X.1Q3 r0 = r8.A09(r3)     // Catch:{ all -> 0x0184 }
            r8.A06 = r0     // Catch:{ all -> 0x0184 }
        L_0x00c0:
            X.1Q3 r1 = r8.A06     // Catch:{ all -> 0x0184 }
            goto L_0x0179
        L_0x00c4:
            monitor-enter(r8)     // Catch:{ all -> 0x0187 }
            X.1Q3 r0 = r8.A0C     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x00e0
            X.1Pd r4 = r8.A0K     // Catch:{ all -> 0x0184 }
            X.2ji r3 = new X.2ji     // Catch:{ all -> 0x0184 }
            X.1Mg r0 = r4.A0E     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r2 = r0.AaQ()     // Catch:{ all -> 0x0184 }
            X.1iR r1 = r4.A0I     // Catch:{ all -> 0x0184 }
            android.content.res.Resources r0 = r4.A02     // Catch:{ all -> 0x0184 }
            r3.<init>(r2, r1, r0)     // Catch:{ all -> 0x0184 }
            X.1Q3 r0 = r8.A09(r3)     // Catch:{ all -> 0x0184 }
            r8.A0C = r0     // Catch:{ all -> 0x0184 }
        L_0x00e0:
            X.1Q3 r1 = r8.A0C     // Catch:{ all -> 0x0184 }
            goto L_0x0179
        L_0x00e4:
            monitor-enter(r8)     // Catch:{ all -> 0x0187 }
            X.1Q3 r0 = r8.A05     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x011e
            X.1Pd r3 = r8.A0K     // Catch:{ all -> 0x0184 }
            X.8tN r4 = new X.8tN     // Catch:{ all -> 0x0184 }
            X.1iR r2 = r3.A0I     // Catch:{ all -> 0x0184 }
            r4.<init>(r2)     // Catch:{ all -> 0x0184 }
            boolean r0 = X.AnonymousClass1Q4.A03     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x010a
            boolean r0 = r8.A0P     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x00fe
            com.facebook.webpsupport.WebpBitmapFactoryImpl r0 = X.AnonymousClass1Q4.A00     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x010a
        L_0x00fe:
            X.2p5 r1 = new X.2p5     // Catch:{ all -> 0x0184 }
            X.1Mg r0 = r3.A0E     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r0 = r0.AaN()     // Catch:{ all -> 0x0184 }
            r1.<init>(r0, r2, r4)     // Catch:{ all -> 0x0184 }
            r4 = r1
        L_0x010a:
            X.1Pd r3 = r8.A0K     // Catch:{ all -> 0x0184 }
            X.1QA r2 = new X.1QA     // Catch:{ all -> 0x0184 }
            r2.<init>(r4)     // Catch:{ all -> 0x0184 }
            r1 = 1
            X.1Pe r0 = r8.A0M     // Catch:{ all -> 0x0184 }
            X.1QB r0 = r3.A00(r2, r1, r0)     // Catch:{ all -> 0x0184 }
            X.1Q3 r0 = r8.A08(r0)     // Catch:{ all -> 0x0184 }
            r8.A05 = r0     // Catch:{ all -> 0x0184 }
        L_0x011e:
            X.1Q3 r1 = r8.A05     // Catch:{ all -> 0x0184 }
            goto L_0x0179
        L_0x0121:
            monitor-enter(r8)     // Catch:{ all -> 0x0187 }
            X.1Q3 r0 = r8.A0G     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x013d
            X.1Pd r4 = r8.A0K     // Catch:{ all -> 0x0184 }
            X.2jt r3 = new X.2jt     // Catch:{ all -> 0x0184 }
            X.1Mg r0 = r4.A0E     // Catch:{ all -> 0x0184 }
            java.util.concurrent.Executor r2 = r0.AaQ()     // Catch:{ all -> 0x0184 }
            X.1iR r1 = r4.A0I     // Catch:{ all -> 0x0184 }
            android.content.ContentResolver r0 = r4.A00     // Catch:{ all -> 0x0184 }
            r3.<init>(r2, r1, r0)     // Catch:{ all -> 0x0184 }
            X.1Q3 r0 = r8.A09(r3)     // Catch:{ all -> 0x0184 }
            r8.A0G = r0     // Catch:{ all -> 0x0184 }
        L_0x013d:
            X.1Q3 r1 = r8.A0G     // Catch:{ all -> 0x0184 }
            goto L_0x0179
        L_0x0140:
            monitor-enter(r8)     // Catch:{ all -> 0x0187 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x014c
            java.lang.String r0 = "ProducerSequenceFactory#getNetworkFetchSequence"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x0184 }
        L_0x014c:
            X.1Q3 r0 = r8.A0E     // Catch:{ all -> 0x0184 }
            if (r0 != 0) goto L_0x016e
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x015b
            java.lang.String r0 = "ProducerSequenceFactory#getNetworkFetchSequence:init"
            X.AnonymousClass1NB.A02(r0)     // Catch:{ all -> 0x0184 }
        L_0x015b:
            X.1Q3 r0 = r8.A00()     // Catch:{ all -> 0x0184 }
            X.1Q3 r0 = r8.A08(r0)     // Catch:{ all -> 0x0184 }
            r8.A0E = r0     // Catch:{ all -> 0x0184 }
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x016e
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x0184 }
        L_0x016e:
            boolean r0 = X.AnonymousClass1NB.A03()     // Catch:{ all -> 0x0184 }
            if (r0 == 0) goto L_0x0177
            X.AnonymousClass1NB.A01()     // Catch:{ all -> 0x0184 }
        L_0x0177:
            X.1Q3 r1 = r8.A0E     // Catch:{ all -> 0x0184 }
        L_0x0179:
            monitor-exit(r8)     // Catch:{ all -> 0x0187 }
        L_0x017a:
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x0183
            X.AnonymousClass1NB.A01()
        L_0x0183:
            return r1
        L_0x0184:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0187 }
            throw r0     // Catch:{ all -> 0x0187 }
        L_0x0187:
            r1 = move-exception
            boolean r0 = X.AnonymousClass1NB.A03()
            if (r0 == 0) goto L_0x0191
            X.AnonymousClass1NB.A01()
        L_0x0191:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1PW.A06(X.1PW, X.1Q0):X.1Q3");
    }

    private AnonymousClass1Q3 A08(AnonymousClass1Q3 r14) {
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("ProducerSequenceFactory#newBitmapCacheGetToDecodeSequence");
        }
        C23341Pd r1 = this.A0K;
        AnonymousClass1Q3 A072 = A07(new AnonymousClass1QC(r1.A0H, r1.A0E.AaO(), r1.A0F, r1.A0G, r1.A0K, r1.A0M, r1.A0J, r14, r1.A06, r1.A0D));
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
        return A072;
    }

    private AnonymousClass1Q3 A0A(AnonymousClass1Q3 r6, C52822jk[] r7) {
        return A08(new C52872jp(this.A0K.A00(new C52862jo(r7), true, this.A0M), new C52852jn(5, this.A0K.A0E.AaP(), this.A0K.A00(new AnonymousClass1QA(A04(this, r6)), true, this.A0M))));
    }

    public static String A0B(Uri uri) {
        String valueOf = String.valueOf(uri);
        if (valueOf.length() > 30) {
            return AnonymousClass08S.A0J(valueOf.substring(0, 30), "...");
        }
        return valueOf;
    }

    public AnonymousClass1Q3 A0C(AnonymousClass1Q0 r7) {
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("ProducerSequenceFactory#getDecodedImageProducerSequence");
        }
        AnonymousClass1Q3 A062 = A06(this, r7);
        if (r7.A0B != null) {
            synchronized (this) {
                if (!this.A0I.containsKey(A062)) {
                    C23341Pd r0 = this.A0K;
                    C635636z r4 = new C635636z(A062, r0.A07, r0.A0E.AaN());
                    C23341Pd r02 = this.A0K;
                    this.A0I.put(A062, new AnonymousClass370(r02.A0B, r02.A0A, r4));
                }
                A062 = (AnonymousClass1Q3) this.A0I.get(A062);
            }
        }
        if (this.A0O) {
            A062 = A05(this, A062);
        }
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
        return A062;
    }

    public AnonymousClass1Q3 A0D(AnonymousClass1Q0 r4) {
        AnonymousClass1PW r2;
        AnonymousClass1Q3 r0;
        C05520Zg.A02(r4);
        boolean z = false;
        if (r4.A0A.mValue <= C23531Pw.ENCODED_MEMORY_CACHE.mValue) {
            z = true;
        }
        C05520Zg.A04(z);
        int i = r4.A01;
        if (i == 0) {
            r2 = this;
            synchronized (r2) {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A02("ProducerSequenceFactory#getNetworkFetchToEncodedMemoryPrefetchSequence");
                }
                if (this.A0F == null) {
                    if (AnonymousClass1NB.A03()) {
                        AnonymousClass1NB.A02("ProducerSequenceFactory#getNetworkFetchToEncodedMemoryPrefetchSequence:init");
                    }
                    this.A0F = new C196609Mr(A03(this));
                    if (AnonymousClass1NB.A03()) {
                        AnonymousClass1NB.A01();
                    }
                }
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
                r0 = this.A0F;
            }
        } else if (i == 2 || i == 3) {
            r2 = this;
            synchronized (r2) {
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A02("ProducerSequenceFactory#getLocalFileFetchToEncodedMemoryPrefetchSequence");
                }
                if (this.A0A == null) {
                    if (AnonymousClass1NB.A03()) {
                        AnonymousClass1NB.A02("ProducerSequenceFactory#getLocalFileFetchToEncodedMemoryPrefetchSequence:init");
                    }
                    this.A0A = new C196609Mr(A02(this));
                    if (AnonymousClass1NB.A03()) {
                        AnonymousClass1NB.A01();
                    }
                }
                if (AnonymousClass1NB.A03()) {
                    AnonymousClass1NB.A01();
                }
                r0 = this.A0A;
            }
        } else {
            throw new IllegalArgumentException(AnonymousClass08S.A0J("Unsupported uri scheme for encoded image fetch! Uri is: ", A0B(r4.A02)));
        }
        return r0;
    }
}
