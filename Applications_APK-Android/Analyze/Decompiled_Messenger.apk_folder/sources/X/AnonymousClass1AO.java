package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1AO  reason: invalid class name */
public final class AnonymousClass1AO extends C16070wR {
    @Comparable(type = 13)
    public C23610BiW A00;
    public AnonymousClass0UN A01;
    public AnonymousClass10N A02;
    @Comparable(type = 13)
    public C35301r0 A03;
    @Comparable(type = 13)
    public AnonymousClass654 A04;
    @Comparable(type = 13)
    public C12260oy A05;
    @Comparable(type = 13)
    public AnonymousClass248 A06;
    @Comparable(type = 14)
    public AnonymousClass5CT A07 = new AnonymousClass5CT();

    public AnonymousClass1AO(Context context) {
        super("ResponseQuerySection");
        this.A01 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    public static C156877Ne A07(C1290563a r3) {
        int i = r3.A01;
        if (i == 0) {
            return C156877Ne.INITIAL_STATE;
        }
        if (i == 1) {
            return C156877Ne.DOWNLOADING_STATE;
        }
        if (i != 2) {
            throw new IllegalStateException(AnonymousClass08S.A09("Unsupported GraphServiceResponse.State ", i));
        } else if (r3.A03 == null || r3.A02 != null) {
            return C156877Ne.IDLE_STATE;
        } else {
            return C156877Ne.DOWNLOAD_ERROR;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0027, code lost:
        if (r1 == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        if (r0 != false) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C156837Na A06(X.AnonymousClass248 r4) {
        /*
            X.7Ne r0 = A07(r4)
            int r0 = r0.ordinal()
            r3 = 1
            switch(r0) {
                case 0: goto L_0x0014;
                case 1: goto L_0x0014;
                case 2: goto L_0x0017;
                case 3: goto L_0x0014;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unsupported RenderSectionEvent.FetchState"
            r1.<init>(r0)
            throw r1
        L_0x0014:
            X.7Na r0 = X.C156837Na.UNSET
            return r0
        L_0x0017:
            int r0 = r4.A00
            if (r0 == r3) goto L_0x003a
            X.BIh r0 = r4.A02
            if (r0 == 0) goto L_0x0029
            int r2 = r0.A00
            r0 = 3
            r1 = 0
            if (r0 != r2) goto L_0x0026
            r1 = 1
        L_0x0026:
            r0 = 1
            if (r1 != 0) goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            if (r0 == 0) goto L_0x003a
        L_0x002c:
            X.102 r0 = r4.A00
            if (r3 != 0) goto L_0x0037
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x003c;
                case 1: goto L_0x0037;
                case 2: goto L_0x003f;
                default: goto L_0x0037;
            }
        L_0x0037:
            X.7Na r0 = X.C156837Na.FROM_LOCAL_CACHE
            return r0
        L_0x003a:
            r3 = 0
            goto L_0x002c
        L_0x003c:
            X.7Na r0 = X.C156837Na.FROM_NETWORK
            return r0
        L_0x003f:
            X.7Na r0 = X.C156837Na.FROM_LOCAL_STALE_CACHE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AO.A06(X.248):X.7Na");
    }
}
