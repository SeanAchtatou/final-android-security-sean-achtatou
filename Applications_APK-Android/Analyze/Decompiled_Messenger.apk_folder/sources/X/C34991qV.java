package X;

import com.facebook.auth.userscope.UserScoped;
import com.google.common.base.Preconditions;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@UserScoped
/* renamed from: X.1qV  reason: invalid class name and case insensitive filesystem */
public final class C34991qV {
    private static C05540Zi A02;
    public int A00 = 0;
    public final Set A01 = new CopyOnWriteArraySet();

    public void A01(boolean z) {
        if (z && this.A00 == 2) {
            return;
        }
        if (z || this.A00 != 0) {
            boolean z2 = false;
            if (z) {
                if (this.A00 == 1) {
                    z2 = true;
                }
                Preconditions.checkState(z2);
                this.A00 = 2;
                return;
            }
            this.A00 = 0;
            for (C34951qR Bie : this.A01) {
                Bie.Bie();
            }
        }
    }

    public static final C34991qV A00(AnonymousClass1XY r3) {
        C34991qV r0;
        synchronized (C34991qV.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r3)) {
                    A02.A01();
                    A02.A00 = new C34991qV();
                }
                C05540Zi r1 = A02;
                r0 = (C34991qV) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public boolean A02() {
        int i = this.A00;
        if (i == 1 || i == 2) {
            return true;
        }
        return false;
    }
}
