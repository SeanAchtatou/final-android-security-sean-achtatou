package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.send.PendingSendQueueKey;

/* renamed from: X.1Hb  reason: invalid class name and case insensitive filesystem */
public final class C21461Hb implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new PendingSendQueueKey(parcel);
    }

    public Object[] newArray(int i) {
        return new PendingSendQueueKey[i];
    }
}
