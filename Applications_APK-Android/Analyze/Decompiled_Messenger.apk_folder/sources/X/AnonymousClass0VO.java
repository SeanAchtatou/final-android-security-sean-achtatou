package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0VO  reason: invalid class name */
public final class AnonymousClass0VO extends AnonymousClass0UV {
    private static volatile AnonymousClass0VP A00;

    public static final AnonymousClass0VP A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass0VP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = AnonymousClass0VP.A0O;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
