package X;

import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1Kl  reason: invalid class name */
public final class AnonymousClass1Kl implements AnonymousClass1NH {
    public final /* synthetic */ AnonymousClass1BE A00;

    public AnonymousClass1Kl(AnonymousClass1BE r1) {
        this.A00 = r1;
    }

    public void Bri(InboxUnitThreadItem inboxUnitThreadItem, C21361Gm r3) {
        AnonymousClass1BE r0 = this.A00;
        if (r0 != null) {
            r0.BpT(inboxUnitThreadItem);
        }
    }
}
