package X;

/* renamed from: X.1bT  reason: invalid class name and case insensitive filesystem */
public final class C26451bT {
    private C08030eZ A00;
    public final C08010eX A01;

    /* JADX WARNING: Can't wrap try/catch for region: R(3:41|42|71) */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r2 = r3.A04;
        X.C26321bG.A00(r2, r2.A01, 87);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00e9 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C26451bT(X.C08010eX r16, X.C08030eZ r17, android.content.Context r18) {
        /*
            r15 = this;
            r15.<init>()
            r0 = r16
            r15.A01 = r0
            r0 = r17
            r15.A00 = r0
            android.content.Context r4 = r18.getApplicationContext()
            X.0eX r3 = r15.A01
            X.0eZ r0 = r15.A00
            X.1az r6 = r0.A02
            boolean r0 = r3.A08
            if (r0 != 0) goto L_0x0171
            boolean r0 = r3.A0C()
            if (r0 == 0) goto L_0x0171
            boolean r0 = r3.A0E()
            if (r0 == 0) goto L_0x002a
            X.1b0 r1 = r3.A03
            r0 = 1
            r1.A00 = r0
        L_0x002a:
            monitor-enter(r3)
            boolean r0 = r3.A08     // Catch:{ all -> 0x016e }
            if (r0 != 0) goto L_0x016c
            X.1bG r8 = r3.A04     // Catch:{ all -> 0x016e }
            boolean r0 = X.C26321bG.A01(r8)     // Catch:{ all -> 0x016e }
            if (r0 != 0) goto L_0x0048
            X.1bU r5 = new X.1bU     // Catch:{ all -> 0x016e }
            r2 = 5
            r7 = 0
            r1 = 0
            r5.<init>(r2, r7, r1, r7)     // Catch:{ all -> 0x016e }
            java.lang.String r0 = X.C26481bW.A00(r2)     // Catch:{ all -> 0x016e }
            r5.A01(r0)     // Catch:{ all -> 0x016e }
            r8.A02 = r5     // Catch:{ all -> 0x016e }
        L_0x0048:
            r0 = 1
            r3.A08 = r0     // Catch:{ all -> 0x016e }
            X.1bG r8 = r3.A04     // Catch:{ all -> 0x016e }
            boolean r0 = X.C26321bG.A01(r8)     // Catch:{ all -> 0x016e }
            if (r0 != 0) goto L_0x0065
            X.1bU r5 = new X.1bU     // Catch:{ all -> 0x016e }
            r2 = 15
            r7 = 0
            r1 = 0
            r5.<init>(r2, r7, r1, r7)     // Catch:{ all -> 0x016e }
            java.lang.String r0 = X.C26481bW.A00(r2)     // Catch:{ all -> 0x016e }
            r5.A01(r0)     // Catch:{ all -> 0x016e }
            r8.A00 = r5     // Catch:{ all -> 0x016e }
        L_0x0065:
            X.0ee r5 = r3.A05     // Catch:{ all -> 0x016e }
            monitor-enter(r5)     // Catch:{ all -> 0x016e }
            android.util.SparseArray r0 = r5.A00     // Catch:{ all -> 0x011f }
            int r2 = r0.size()     // Catch:{ all -> 0x011f }
            r8 = r2
            int[] r7 = new int[r2]     // Catch:{ all -> 0x011f }
            r1 = 0
        L_0x0072:
            if (r1 >= r2) goto L_0x007f
            android.util.SparseArray r0 = r5.A00     // Catch:{ all -> 0x011f }
            int r0 = r0.keyAt(r1)     // Catch:{ all -> 0x011f }
            r7[r1] = r0     // Catch:{ all -> 0x011f }
            int r1 = r1 + 1
            goto L_0x0072
        L_0x007f:
            monitor-exit(r5)     // Catch:{ all -> 0x016e }
            r5 = 0
        L_0x0081:
            if (r5 >= r8) goto L_0x00f9
            r10 = r7[r5]     // Catch:{ all -> 0x016e }
            java.lang.String r14 = X.C07210ct.A00(r10)     // Catch:{ all -> 0x016e }
            boolean r0 = r3.A0F(r10)     // Catch:{ all -> 0x016e }
            if (r0 != 0) goto L_0x00b9
            X.0ee r2 = r3.A05     // Catch:{ all -> 0x016e }
            X.2rj r1 = X.C57282rj.A00     // Catch:{ all -> 0x016e }
            monitor-enter(r2)     // Catch:{ all -> 0x016e }
            android.util.SparseArray r0 = r2.A00     // Catch:{ all -> 0x0122 }
            r0.put(r10, r1)     // Catch:{ all -> 0x0122 }
            monitor-exit(r2)     // Catch:{ all -> 0x016e }
            X.1b0 r10 = r3.A03     // Catch:{ all -> 0x016e }
            X.1bU r9 = new X.1bU     // Catch:{ all -> 0x016e }
            r12 = 35
            r11 = 0
            r1 = 0
            r9.<init>(r12, r11, r1, r11)     // Catch:{ all -> 0x016e }
            java.lang.String r2 = "booster"
            X.1bV r0 = r9.A09     // Catch:{ all -> 0x016e }
            java.util.Map r1 = r0.A02     // Catch:{ all -> 0x016e }
            r1.put(r2, r14)     // Catch:{ all -> 0x016e }
            java.lang.String r0 = X.C26481bW.A00(r12)     // Catch:{ all -> 0x016e }
            r9.A01(r0)     // Catch:{ all -> 0x016e }
            r10.BJ9(r9)     // Catch:{ all -> 0x016e }
            goto L_0x00f6
        L_0x00b9:
            X.1bG r11 = r3.A04     // Catch:{ all -> 0x016e }
            X.1bU r9 = new X.1bU     // Catch:{ all -> 0x016e }
            r13 = 10
            r12 = 0
            r1 = 0
            r9.<init>(r13, r12, r1, r12)     // Catch:{ all -> 0x016e }
            java.lang.String r2 = "booster"
            X.1bV r0 = r9.A09     // Catch:{ all -> 0x016e }
            java.util.Map r1 = r0.A02     // Catch:{ all -> 0x016e }
            r1.put(r2, r14)     // Catch:{ all -> 0x016e }
            java.lang.String r0 = X.C26481bW.A00(r13)     // Catch:{ all -> 0x016e }
            r9.A01(r0)     // Catch:{ all -> 0x016e }
            r11.A01 = r9     // Catch:{ all -> 0x016e }
            X.0ee r0 = r3.A05     // Catch:{ Exception -> 0x00e9 }
            X.0f6 r0 = r0.A01(r10)     // Catch:{ Exception -> 0x00e9 }
            if (r0 == 0) goto L_0x00f6
            r0.A02(r4)     // Catch:{ Exception -> 0x00e9 }
            X.1bG r2 = r3.A04     // Catch:{ Exception -> 0x00e9 }
            X.1bU r1 = r2.A01     // Catch:{ Exception -> 0x00e9 }
            X.C26321bG.A00(r2, r1, r12)     // Catch:{ Exception -> 0x00e9 }
            goto L_0x00f6
        L_0x00e9:
            X.1bG r2 = r3.A04     // Catch:{ all -> 0x016e }
            X.1bU r1 = r2.A01     // Catch:{ all -> 0x016e }
            r0 = 87
            java.lang.Short r0 = java.lang.Short.valueOf(r0)     // Catch:{ all -> 0x016e }
            X.C26321bG.A00(r2, r1, r0)     // Catch:{ all -> 0x016e }
        L_0x00f6:
            int r5 = r5 + 1
            goto L_0x0081
        L_0x00f9:
            X.1bG r2 = r3.A04     // Catch:{ all -> 0x016e }
            boolean r0 = X.C26321bG.A01(r2)     // Catch:{ all -> 0x016e }
            if (r0 != 0) goto L_0x0107
            X.1bU r1 = r2.A00     // Catch:{ all -> 0x016e }
            r0 = 0
            X.C26321bG.A00(r2, r1, r0)     // Catch:{ all -> 0x016e }
        L_0x0107:
            X.C08010eX.A01(r3)     // Catch:{ all -> 0x016e }
            android.os.Handler r2 = r3.A03     // Catch:{ all -> 0x016e }
            X.0f1 r1 = new X.0f1     // Catch:{ all -> 0x016e }
            r1.<init>(r3)     // Catch:{ all -> 0x016e }
            r0 = 418810106(0x18f688fa, float:6.372782E-24)
            X.AnonymousClass00S.A04(r2, r1, r0)     // Catch:{ all -> 0x016e }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x016e }
            r5.<init>()     // Catch:{ all -> 0x016e }
            if (r6 == 0) goto L_0x0148
            goto L_0x0125
        L_0x011f:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x016e }
            goto L_0x0124
        L_0x0122:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x016e }
        L_0x0124:
            throw r0     // Catch:{ all -> 0x016e }
        L_0x0125:
            boolean r0 = r3.A0D()     // Catch:{ all -> 0x016e }
            if (r0 == 0) goto L_0x0148
            X.0eZ r0 = r3.A07     // Catch:{ all -> 0x016e }
            android.os.HandlerThread r2 = r0.A01     // Catch:{ all -> 0x016e }
            android.content.Context r0 = r4.getApplicationContext()     // Catch:{ all -> 0x016e }
            r6.A00 = r0     // Catch:{ all -> 0x016e }
            r6.A02 = r3     // Catch:{ all -> 0x016e }
            android.os.Handler r1 = new android.os.Handler     // Catch:{ all -> 0x016e }
            android.os.Looper r0 = r2.getLooper()     // Catch:{ all -> 0x016e }
            r1.<init>(r0)     // Catch:{ all -> 0x016e }
            r6.A01 = r1     // Catch:{ all -> 0x016e }
            r5.add(r6)     // Catch:{ all -> 0x016e }
            r0 = 1
            X.C08040ea.A08 = r0     // Catch:{ all -> 0x016e }
        L_0x0148:
            X.1bZ r0 = new X.1bZ     // Catch:{ all -> 0x016e }
            r0.<init>()     // Catch:{ all -> 0x016e }
            r5.add(r0)     // Catch:{ all -> 0x016e }
            r5.add(r3)     // Catch:{ all -> 0x016e }
            X.0ew r0 = X.C08250ew.A02     // Catch:{ all -> 0x016e }
            if (r0 != 0) goto L_0x015e
            X.0ew r0 = new X.0ew     // Catch:{ all -> 0x016e }
            r0.<init>(r4, r5)     // Catch:{ all -> 0x016e }
            X.C08250ew.A02 = r0     // Catch:{ all -> 0x016e }
        L_0x015e:
            X.1bG r2 = r3.A04     // Catch:{ all -> 0x016e }
            boolean r0 = X.C26321bG.A01(r2)     // Catch:{ all -> 0x016e }
            if (r0 != 0) goto L_0x016c
            X.1bU r1 = r2.A02     // Catch:{ all -> 0x016e }
            r0 = 0
            X.C26321bG.A00(r2, r1, r0)     // Catch:{ all -> 0x016e }
        L_0x016c:
            monitor-exit(r3)     // Catch:{ all -> 0x016e }
            return
        L_0x016e:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x016e }
            throw r0
        L_0x0171:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26451bT.<init>(X.0eX, X.0eZ, android.content.Context):void");
    }
}
