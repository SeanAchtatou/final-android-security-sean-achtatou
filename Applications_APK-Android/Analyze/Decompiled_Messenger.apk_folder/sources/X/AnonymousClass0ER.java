package X;

import com.facebook.profilo.logger.Logger;
import java.io.InputStream;

/* renamed from: X.0ER  reason: invalid class name */
public final class AnonymousClass0ER extends InputStream {
    private int A00;
    private InputStream A01;

    public synchronized void mark(int i) {
        this.A01.mark(i);
    }

    public synchronized void reset() {
        this.A01.reset();
    }

    public int available() {
        return this.A01.available();
    }

    public void close() {
        this.A01.close();
    }

    public boolean markSupported() {
        return this.A01.markSupported();
    }

    public long skip(long j) {
        return this.A01.skip(j);
    }

    public String toString() {
        return "InputStreamWrapper for " + this.A01;
    }

    public AnonymousClass0ER(InputStream inputStream, int i) {
        this.A01 = inputStream;
        this.A00 = i;
    }

    public int read() {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 12, 0, 0, this.A00, 0, 0);
        try {
            return this.A01.read();
        } finally {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
        }
    }

    public int read(byte[] bArr) {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 12, 0, 0, this.A00, 0, 0);
        try {
            return this.A01.read(bArr);
        } finally {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        int writeStandardEntry = Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 12, 0, 0, this.A00, 0, 0);
        try {
            return this.A01.read(bArr, i, i2);
        } finally {
            Logger.writeStandardEntry(AnonymousClass00n.A08, 6, 9, 0, 0, this.A00, writeStandardEntry, 0);
        }
    }
}
