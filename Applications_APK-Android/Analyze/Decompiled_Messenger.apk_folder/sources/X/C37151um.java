package X;

import android.view.View;
import android.view.Window;

/* renamed from: X.1um  reason: invalid class name and case insensitive filesystem */
public final class C37151um implements View.OnSystemUiVisibilityChangeListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Window A01;

    public C37151um(int i, Window window) {
        this.A00 = i;
        this.A01 = window;
    }

    public void onSystemUiVisibilityChange(int i) {
        int i2 = this.A00;
        if (i != i2) {
            C37141ul.A00(this.A01, i2);
        }
    }
}
