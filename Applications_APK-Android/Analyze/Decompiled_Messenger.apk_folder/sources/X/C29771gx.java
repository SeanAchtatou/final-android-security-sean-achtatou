package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1gx  reason: invalid class name and case insensitive filesystem */
public final class C29771gx {
    private static volatile C29771gx A02;
    public AnonymousClass0UN A00;
    public final Runnable A01 = new C29781gy(this);

    public static final C29771gx A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C29771gx.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C29771gx(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C29771gx(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
