package X;

import com.facebook.inject.InjectorModule;
import com.facebook.user.model.User;

@InjectorModule
/* renamed from: X.0XM  reason: invalid class name */
public final class AnonymousClass0XM extends AnonymousClass0UV {
    public static final AnonymousClass0XN A00(AnonymousClass1XY r0) {
        return AnonymousClass0XN.A01(r0);
    }

    public static final User A01(AnonymousClass1XY r0) {
        return C25481Zu.A00(r0).A09();
    }
}
