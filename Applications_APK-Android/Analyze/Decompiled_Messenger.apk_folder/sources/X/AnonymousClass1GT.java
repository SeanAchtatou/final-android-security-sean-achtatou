package X;

import java.util.concurrent.Executor;

/* renamed from: X.1GT  reason: invalid class name */
public abstract class AnonymousClass1GT {
    public static final boolean A01 = AnonymousClass0VW.A00();
    public final AnonymousClass1GR A00 = new AnonymousClass1GR(this);

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A02(java.lang.Object... r13) {
        /*
            r12 = this;
            r4 = r12
            X.1rF r4 = (X.C35451rF) r4
            X.16R r3 = r4.A00
            boolean r1 = r3.A0N
            r0 = 1
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r0)
            if (r1 != 0) goto L_0x0028
            boolean r0 = X.AnonymousClass16R.A06(r3)
            if (r0 == 0) goto L_0x0025
            java.lang.Integer r3 = X.AnonymousClass07B.A0C
        L_0x0016:
            X.16R r0 = r4.A00
            X.17O r2 = r0.A0C
            X.1G5 r1 = new X.1G5
            java.lang.String r0 = "REGULAR"
            r1.<init>(r3, r0)
            r2.CHB(r1)
            return r11
        L_0x0025:
            java.lang.Integer r3 = X.AnonymousClass07B.A01
            goto L_0x0016
        L_0x0028:
            r2 = 20
            int r1 = X.AnonymousClass1Y3.BGY
            X.0UN r0 = r3.A08
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1G4 r5 = (X.AnonymousClass1G4) r5
            X.1Yd r2 = r5.A04
            r0 = 282428459976064(0x100de00080580, double:1.395381994820227E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0071
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AGk
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper r0 = (com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper) r0
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x0065
            X.1Yd r2 = r5.A04
            r0 = 563903437275753(0x200de00110269, double:2.78605315929744E-309)
            long r9 = r2.At0(r0)
        L_0x005d:
            r6 = 0
            r1 = -1
            int r0 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x00ca
            goto L_0x0074
        L_0x0065:
            X.1Yd r2 = r5.A04
            r0 = 563903437013608(0x200de000d0268, double:2.78605315800227E-309)
            long r9 = r2.At0(r0)
            goto L_0x005d
        L_0x0071:
            r9 = -1
            goto L_0x005d
        L_0x0074:
            X.0ao r0 = r5.A03     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            com.google.common.base.Supplier r0 = r0.A00     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            java.lang.Object r0 = r0.get()     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            android.database.sqlite.SQLiteDatabase r0 = (android.database.sqlite.SQLiteDatabase) r0     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            boolean r0 = r0.isOpen()     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            if (r0 == 0) goto L_0x00ca
            X.0ao r3 = r5.A03     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            X.0a7 r1 = r5.A02     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            X.0nH r2 = new X.0nH     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            java.lang.String r0 = "last_successful_fetch_ms"
            java.lang.String r0 = X.C05660a7.A00(r1, r0)     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            r2.<init>(r0)     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            r0 = 0
            long r7 = r3.A02(r2, r0)     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            X.06B r0 = r5.A01     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            long r2 = r0.now()     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            long r2 = r2 - r7
            r0 = 60000(0xea60, double:2.9644E-319)
            long r9 = r9 * r0
            int r0 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r0 <= 0) goto L_0x00cd
            X.1Yd r2 = r5.A04     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            r0 = 282428460500357(0x100de00100585, double:1.39538199741058E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ SQLiteException | IllegalStateException -> 0x00b6 }
            if (r0 != 0) goto L_0x00cd
            goto L_0x00cc
        L_0x00b6:
            r3 = move-exception
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "inbox2_ads"
            java.lang.String r0 = "last_successful_fetch getValueForKeyAsLong failed"
            r2.softReport(r1, r0, r3)
            r6 = 0
            goto L_0x00cd
        L_0x00ca:
            r6 = 0
            goto L_0x00cd
        L_0x00cc:
            r6 = 1
        L_0x00cd:
            if (r6 == 0) goto L_0x00e0
            X.16R r0 = r4.A00
            X.17O r3 = r0.A0C
            X.1G5 r2 = new X.1G5
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            java.lang.String r0 = "REGULAR"
            r2.<init>(r1, r0)
            r3.CHB(r2)
            return r11
        L_0x00e0:
            r0 = 0
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r0)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1GT.A02(java.lang.Object[]):java.lang.Object");
    }

    public void A03() {
    }

    public void A04(Object obj) {
    }

    public void A01(Executor executor, Object... objArr) {
        if (A01) {
            this.A00.execute(objArr);
        } else {
            this.A00.executeOnExecutor(executor, objArr);
        }
    }
}
