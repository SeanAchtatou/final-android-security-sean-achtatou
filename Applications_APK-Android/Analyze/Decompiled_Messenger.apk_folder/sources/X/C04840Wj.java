package X;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Wj  reason: invalid class name and case insensitive filesystem */
public final class C04840Wj {
    private static volatile C04840Wj A04;
    private int A00 = 0;
    private final ArrayList A01 = new ArrayList();
    private final EnumMap A02 = new EnumMap(AnonymousClass0WW.class);
    private final ReentrantLock A03 = new ReentrantLock();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [X.0WW, X.3bb]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    private C70973bb A00(AnonymousClass0WW r3) {
        if (this.A02.containsKey(r3)) {
            return (C70973bb) this.A02.get(r3);
        }
        C70973bb r1 = new C70973bb();
        this.A02.put((Enum) r3, (Object) r1);
        return r1;
    }

    public static final C04840Wj A01(AnonymousClass1XY r3) {
        if (A04 == null) {
            synchronized (C04840Wj.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A04 = new C04840Wj();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void A02(AnonymousClass0WW r9) {
        if (r9 != AnonymousClass0WW.A05) {
            ArrayList arrayList = new ArrayList();
            this.A03.lock();
            try {
                C70973bb A002 = A00(r9);
                A002.A01 = true;
                if (A002.A00 != null) {
                    for (int i = 0; i < A002.A00.size(); i++) {
                        C70983bc r2 = (C70983bc) this.A01.get(((Integer) A002.A00.get(i)).intValue());
                        int i2 = r2.A00 - 1;
                        r2.A00 = i2;
                        boolean z = false;
                        if (i2 == 0) {
                            z = true;
                        }
                        if (z) {
                            arrayList.add(r2);
                        }
                    }
                    A002.A00.clear();
                }
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    C70983bc r1 = (C70983bc) arrayList.get(i3);
                    AnonymousClass0VL r3 = r1.A03;
                    C70963ba r0 = r1.A02;
                    C05350Yp.A08(r3.CIC(r0), r1.A01, r1.A04);
                }
            } finally {
                this.A03.unlock();
            }
        }
    }

    public void A03(AnonymousClass0VL r13, AnonymousClass0WW[] r14, AnonymousClass0Y8 r15, Object obj, C04800Wf r17, Executor executor) {
        int length;
        C70963ba r9 = new C70963ba(r15, obj);
        AnonymousClass0VL r7 = r13;
        Executor executor2 = executor;
        C04800Wf r10 = r17;
        if (r14 != null) {
            this.A03.lock();
            boolean z = false;
            int i = 0;
            int i2 = 0;
            while (true) {
                try {
                    length = r14.length;
                    if (i >= length) {
                        break;
                    }
                    C70973bb A002 = A00(r14[i]);
                    if (A002.A01) {
                        i2++;
                    } else {
                        int i3 = this.A00;
                        if (A002.A00 == null) {
                            A002.A00 = new ArrayList();
                        }
                        A002.A00.add(Integer.valueOf(i3));
                    }
                    i++;
                } finally {
                    this.A03.unlock();
                }
            }
            if (i2 == length) {
                z = true;
            } else {
                this.A01.add(this.A00, new C70983bc(r7, length - i2, r9, r10, executor2));
                this.A00++;
            }
            if (!z) {
                return;
            }
        }
        C05350Yp.A08(r13.CIC(r9), r10, executor2);
    }
}
