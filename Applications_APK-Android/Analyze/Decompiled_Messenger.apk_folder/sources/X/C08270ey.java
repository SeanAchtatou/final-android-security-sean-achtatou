package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ey  reason: invalid class name and case insensitive filesystem */
public final class C08270ey extends C07980eU {
    private static volatile C08270ey A00;

    public static final C08270ey A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C08270ey.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C08270ey();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
