package X;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.0eO  reason: invalid class name and case insensitive filesystem */
public abstract class C07920eO implements ListenableFuture {
    public static final C05490Zd ATOMIC_HELPER;
    private static final boolean GENERATE_CANCELLATION_CAUSES = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    private static final Object NULL = new Object();
    private static final long SPIN_THRESHOLD_NANOS = 1000;
    private static final Logger log;
    public volatile C07520dh listeners;
    public volatile Object value;
    public volatile C07500df waiters;

    public static void complete(C07920eO r4) {
        C07520dh r3 = null;
        while (true) {
            r4.releaseWaiters();
            r4.afterDone();
            C07520dh clearListeners = r4.clearListeners(r3);
            while (true) {
                if (clearListeners != null) {
                    r3 = clearListeners.A00;
                    Runnable runnable = clearListeners.A01;
                    if (runnable instanceof C08740fs) {
                        C08740fs r2 = (C08740fs) runnable;
                        r4 = r2.A00;
                        if (r4.value == r2) {
                            if (ATOMIC_HELPER.A04(r4, r2, getFutureValue(r2.A01))) {
                            }
                        } else {
                            continue;
                        }
                    } else {
                        executeListener(runnable, clearListeners.A02);
                    }
                    clearListeners = r3;
                } else {
                    return;
                }
            }
        }
    }

    private void removeWaiter(C07500df r6) {
        r6.thread = null;
        while (true) {
            C07500df r3 = this.waiters;
            if (r3 != C07500df.A00) {
                C07500df r2 = null;
                while (r3 != null) {
                    C07500df r1 = r3.next;
                    if (r3.thread != null) {
                        r2 = r3;
                    } else if (r2 != null) {
                        r2.next = r1;
                        if (r2.thread == null) {
                        }
                    } else if (!ATOMIC_HELPER.A03(this, r3, r1)) {
                    }
                    r3 = r1;
                }
                return;
            }
            return;
        }
    }

    public void afterDone() {
    }

    public void interruptTask() {
    }

    public final void maybePropagateCancellation(Future future) {
        boolean z = false;
        if (future != null) {
            z = true;
        }
        if (z && isCancelled()) {
            future.cancel(wasInterrupted());
        }
    }

    static {
        C05490Zd r5;
        Class<C07920eO> cls = C07920eO.class;
        log = Logger.getLogger(cls.getName());
        try {
            r5 = new C07430dT();
        } catch (Throwable th) {
            Logger logger = log;
            Level level = Level.SEVERE;
            logger.log(level, "UnsafeAtomicHelper is broken!", th);
            logger.log(level, AnonymousClass24B.$const$string(AnonymousClass1Y3.A5q), th);
            r5 = new C29977Emq();
        }
        ATOMIC_HELPER = r5;
    }

    private static CancellationException cancellationExceptionWithCause(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }

    private C07520dh clearListeners(C07520dh r4) {
        C07520dh r2;
        do {
            r2 = this.listeners;
        } while (!ATOMIC_HELPER.A02(this, r2, C07520dh.A03));
        while (r2 != null) {
            C07520dh r0 = r2.A00;
            r2.A00 = r4;
            r4 = r2;
            r2 = r0;
        }
        return r4;
    }

    private Object getDoneValue(Object obj) {
        if (obj instanceof C08750ft) {
            throw cancellationExceptionWithCause("Task was cancelled.", ((C08750ft) obj).A00);
        } else if (obj instanceof C08760fu) {
            throw new ExecutionException(((C08760fu) obj).A00);
        } else if (obj == NULL) {
            return null;
        } else {
            return obj;
        }
    }

    public static Object getFutureValue(ListenableFuture listenableFuture) {
        if (listenableFuture instanceof C06720by) {
            return ((C07920eO) listenableFuture).value;
        }
        try {
            Object A06 = C05350Yp.A06(listenableFuture);
            if (A06 == null) {
                return NULL;
            }
            return A06;
        } catch (ExecutionException e) {
            return new C08760fu(e.getCause());
        } catch (CancellationException e2) {
            return new C08750ft(false, e2);
        } catch (Throwable th) {
            return new C08760fu(th);
        }
    }

    private void releaseWaiters() {
        C07500df r2;
        do {
            r2 = this.waiters;
        } while (!ATOMIC_HELPER.A03(this, r2, C07500df.A00));
        while (r2 != null) {
            Thread thread = r2.thread;
            if (thread != null) {
                r2.thread = null;
                LockSupport.unpark(thread);
            }
            r2 = r2.next;
        }
    }

    public void addListener(Runnable runnable, Executor executor) {
        Preconditions.checkNotNull(runnable, "Runnable was null.");
        Preconditions.checkNotNull(executor, "Executor was null.");
        C07520dh r2 = this.listeners;
        if (r2 != C07520dh.A03) {
            C07520dh r1 = new C07520dh(runnable, executor);
            do {
                r1.A00 = r2;
                if (!ATOMIC_HELPER.A02(this, r2, r1)) {
                    r2 = this.listeners;
                } else {
                    return;
                }
            } while (r2 != C07520dh.A03);
        }
        executeListener(runnable, executor);
    }

    public boolean cancel(boolean z) {
        CancellationException cancellationException;
        Object obj = this.value;
        boolean z2 = false;
        if (obj == null) {
            z2 = true;
        }
        if (!z2 && !(obj instanceof C08740fs)) {
            return false;
        }
        if (GENERATE_CANCELLATION_CAUSES) {
            cancellationException = new CancellationException("Future.cancel() was called.");
        } else {
            cancellationException = null;
        }
        C08750ft r3 = new C08750ft(z, cancellationException);
        boolean z3 = false;
        C07920eO r2 = this;
        while (true) {
            if (ATOMIC_HELPER.A04(r2, obj, r3)) {
                if (z) {
                    r2.interruptTask();
                }
                complete(r2);
                if (!(obj instanceof C08740fs)) {
                    break;
                }
                ListenableFuture listenableFuture = ((C08740fs) obj).A01;
                if (!(listenableFuture instanceof C06720by)) {
                    listenableFuture.cancel(z);
                    break;
                }
                r2 = (C07920eO) listenableFuture;
                obj = r2.value;
                boolean z4 = false;
                if (obj == null) {
                    z4 = true;
                }
                if (!z4 && !(obj instanceof C08740fs)) {
                    break;
                }
                z3 = true;
            } else {
                obj = r2.value;
                if (!(obj instanceof C08740fs)) {
                    return z3;
                }
            }
        }
        return true;
    }

    public boolean isCancelled() {
        return this.value instanceof C08750ft;
    }

    public boolean isDone() {
        Object obj = this.value;
        boolean z = true;
        boolean z2 = false;
        if (obj != null) {
            z2 = true;
        }
        if (obj instanceof C08740fs) {
            z = false;
        }
        return z2 & z;
    }

    public boolean set(Object obj) {
        if (obj == null) {
            obj = NULL;
        }
        if (!ATOMIC_HELPER.A04(this, null, obj)) {
            return false;
        }
        complete(this);
        return true;
    }

    public boolean setException(Throwable th) {
        Preconditions.checkNotNull(th);
        if (!ATOMIC_HELPER.A04(this, null, new C08760fu(th))) {
            return false;
        }
        complete(this);
        return true;
    }

    public final Throwable trustedGetException() {
        return ((C08760fu) this.value).A00;
    }

    public final boolean wasInterrupted() {
        Object obj = this.value;
        if (!(obj instanceof C08750ft) || !((C08750ft) obj).A01) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.RuntimeException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static void executeListener(Runnable runnable, Executor executor) {
        try {
            AnonymousClass07A.A04(executor, runnable, 1385640313);
        } catch (RuntimeException e) {
            Logger logger = log;
            Level level = Level.SEVERE;
            logger.log(level, "RuntimeException while executing runnable " + runnable + " with executor " + executor, (Throwable) e);
        }
    }

    public boolean setFuture(ListenableFuture listenableFuture) {
        C08740fs r3;
        C05490Zd r2;
        C08760fu r0;
        Preconditions.checkNotNull(listenableFuture);
        Object obj = this.value;
        if (obj == null) {
            if (listenableFuture.isDone()) {
                if (ATOMIC_HELPER.A04(this, null, getFutureValue(listenableFuture))) {
                    complete(this);
                    return true;
                }
                return false;
            }
            r3 = new C08740fs(this, listenableFuture);
            r2 = ATOMIC_HELPER;
            if (r2.A04(this, null, r3)) {
                try {
                    listenableFuture.addListener(r3, C25141Ym.INSTANCE);
                    return true;
                } catch (Throwable unused) {
                    r0 = C08760fu.A01;
                }
            } else {
                obj = this.value;
            }
        }
        if (obj instanceof C08750ft) {
            listenableFuture.cancel(((C08750ft) obj).A01);
        }
        return false;
        r2.A04(this, r3, r0);
        return true;
    }

    public Object get() {
        boolean z;
        boolean z2;
        if (!Thread.interrupted()) {
            Object obj = this.value;
            boolean z3 = false;
            if (obj != null) {
                z3 = true;
            }
            boolean z4 = false;
            if (!(obj instanceof C08740fs)) {
                z4 = true;
            }
            if (!z3 || !z4) {
                C07500df r1 = this.waiters;
                if (r1 != C07500df.A00) {
                    C07500df r3 = new C07500df();
                    while (true) {
                        C05490Zd r0 = ATOMIC_HELPER;
                        r0.A00(r3, r1);
                        if (!r0.A03(this, r1, r3)) {
                            r1 = this.waiters;
                            if (r1 == C07500df.A00) {
                                break;
                            }
                        } else {
                            do {
                                LockSupport.park(this);
                                if (!Thread.interrupted()) {
                                    obj = this.value;
                                    z = false;
                                    if (obj != null) {
                                        z = true;
                                    }
                                    z2 = false;
                                    if (!(obj instanceof C08740fs)) {
                                        z2 = true;
                                    }
                                } else {
                                    removeWaiter(r3);
                                }
                            } while (!(z & z2));
                        }
                    }
                    obj = this.value;
                } else {
                    obj = this.value;
                }
            }
            return getDoneValue(obj);
        }
        throw new InterruptedException();
    }

    public Object get(long j, TimeUnit timeUnit) {
        long j2;
        Object obj;
        long nanos = timeUnit.toNanos(j);
        if (!Thread.interrupted()) {
            Object obj2 = this.value;
            boolean z = false;
            if (obj2 != null) {
                z = true;
            }
            boolean z2 = false;
            if (!(obj2 instanceof C08740fs)) {
                z2 = true;
            }
            if (z && z2) {
                return getDoneValue(obj2);
            }
            if (nanos > 0) {
                j2 = System.nanoTime() + nanos;
            } else {
                j2 = 0;
            }
            if (nanos >= SPIN_THRESHOLD_NANOS) {
                C07500df r1 = this.waiters;
                if (r1 != C07500df.A00) {
                    C07500df r4 = new C07500df();
                    while (true) {
                        C05490Zd r0 = ATOMIC_HELPER;
                        r0.A00(r4, r1);
                        if (r0.A03(this, r1, r4)) {
                            while (true) {
                                LockSupport.parkNanos(this, nanos);
                                if (Thread.interrupted()) {
                                    removeWaiter(r4);
                                    break;
                                }
                                obj = this.value;
                                boolean z3 = false;
                                if (obj != null) {
                                    z3 = true;
                                }
                                boolean z4 = false;
                                if (!(obj instanceof C08740fs)) {
                                    z4 = true;
                                }
                                if (z3 && z4) {
                                    break;
                                }
                                nanos = j2 - System.nanoTime();
                                if (nanos < SPIN_THRESHOLD_NANOS) {
                                    removeWaiter(r4);
                                    break;
                                }
                            }
                        } else {
                            r1 = this.waiters;
                            if (r1 == C07500df.A00) {
                                break;
                            }
                        }
                    }
                    obj = this.value;
                } else {
                    obj = this.value;
                }
                return getDoneValue(obj);
            }
            while (nanos > 0) {
                obj = this.value;
                boolean z5 = false;
                if (obj != null) {
                    z5 = true;
                }
                boolean z6 = false;
                if (!(obj instanceof C08740fs)) {
                    z6 = true;
                }
                if (z5 && z6) {
                    return getDoneValue(obj);
                }
                if (!Thread.interrupted()) {
                    nanos = j2 - System.nanoTime();
                }
            }
            throw new TimeoutException();
        }
        throw new InterruptedException();
    }
}
