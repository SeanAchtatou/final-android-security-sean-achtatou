package X;

import com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper;
import java.util.concurrent.Executor;

/* renamed from: X.17S  reason: invalid class name */
public final class AnonymousClass17S extends AnonymousClass1EP {
    public AnonymousClass0UN A00;
    public C33461nc A01;
    public AnonymousClass101 A02;

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        if (r2 != false) goto L_0x003b;
     */
    /* renamed from: A0D */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void CHB(X.C33461nc r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            X.1G0 r1 = r5.A00     // Catch:{ all -> 0x003d }
            r0 = 0
            if (r1 == 0) goto L_0x0007
            r0 = 1
        L_0x0007:
            if (r0 == 0) goto L_0x0034
            X.1nc r4 = r5.A01     // Catch:{ all -> 0x003d }
            r2 = 0
            if (r4 == 0) goto L_0x0039
            X.0hU r3 = r6.A00     // Catch:{ all -> 0x003d }
            X.0hU r0 = X.C09510hU.STALE_DATA_OKAY     // Catch:{ all -> 0x003d }
            if (r3 == r0) goto L_0x0038
            X.0hU r1 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA     // Catch:{ all -> 0x003d }
            if (r3 != r1) goto L_0x001d
            X.0hU r0 = r4.A00     // Catch:{ all -> 0x003d }
            if (r0 != r1) goto L_0x0039
            goto L_0x0038
        L_0x001d:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x003d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x003d }
            r1.<init>()     // Catch:{ all -> 0x003d }
            java.lang.String r0 = "invalid data freshness"
            r1.append(r0)     // Catch:{ all -> 0x003d }
            r1.append(r3)     // Catch:{ all -> 0x003d }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x003d }
            r2.<init>(r0)     // Catch:{ all -> 0x003d }
            throw r2     // Catch:{ all -> 0x003d }
        L_0x0034:
            super.CHB(r6)     // Catch:{ all -> 0x003d }
            goto L_0x003b
        L_0x0038:
            r2 = 1
        L_0x0039:
            if (r2 == 0) goto L_0x0034
        L_0x003b:
            monitor-exit(r5)
            return
        L_0x003d:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass17S.CHB(X.1nc):void");
    }

    public synchronized void ARp() {
        if (this.A01 != null) {
            this.A01 = null;
        }
        super.ARp();
    }

    public static final AnonymousClass17S A00(AnonymousClass1XY r2) {
        return new AnonymousClass17S(r2, AnonymousClass0UX.A0S(r2));
    }

    public static boolean A01(AnonymousClass17S r7, AnonymousClass101 r8) {
        long At0;
        AnonymousClass1G4 r3 = (AnonymousClass1G4) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BGY, r7.A00);
        long j = r8.A00;
        if (((InboxUnitGraphQLQueryExecutorHelper) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AGk, r3.A00)).A03()) {
            At0 = r3.A04.At0(563903436292708L);
        } else {
            At0 = r3.A04.At0(563903436227171L);
        }
        r3.A01.now();
        r3.A01.now();
        C010708t.A01.BFj(2);
        if (At0 == -1 || r3.A01.now() - j <= At0 * 60000) {
            return false;
        }
        return true;
    }

    private AnonymousClass17S(AnonymousClass1XY r3, Executor executor) {
        super(executor);
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
