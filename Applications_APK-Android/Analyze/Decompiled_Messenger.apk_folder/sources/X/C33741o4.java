package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.base.Preconditions;

/* renamed from: X.1o4  reason: invalid class name and case insensitive filesystem */
public final class C33741o4 {
    public C06790c5 A00;
    public C04460Ut A01;
    public AnonymousClass0UN A02;
    public C193617v A03;
    private FbSharedPreferences A04;
    private final AnonymousClass0ZM A05 = new C33751o5(this);

    public static final C33741o4 A00(AnonymousClass1XY r1) {
        return new C33741o4(r1);
    }

    public void A02() {
        if (this.A00 == null) {
            AnonymousClass180 r13 = new AnonymousClass180(this);
            AnonymousClass181 r11 = new AnonymousClass181(this);
            AnonymousClass18Q r1 = new AnonymousClass18Q(this);
            AnonymousClass18O r9 = new AnonymousClass18O(this);
            C32801mM r8 = new C32801mM(this);
            AnonymousClass18R r7 = new AnonymousClass18R(this);
            AnonymousClass18T r6 = new AnonymousClass18T(this);
            AnonymousClass18U r5 = new AnonymousClass18U(this);
            AnonymousClass18V r4 = new AnonymousClass18V(this);
            AnonymousClass18W r3 = new AnonymousClass18W(this);
            C06600bl BMm = this.A01.BMm();
            BMm.A02(C06680bu.A0z, new AnonymousClass18Y(this));
            BMm.A02(C06680bu.A0Z, new AnonymousClass18Z(this));
            BMm.A02(C06680bu.A0e, new C194118a(this));
            BMm.A02(C06680bu.A0c, new C194218b(this));
            BMm.A02(C06680bu.A0b, r13);
            BMm.A02(TurboLoader.Locator.$const$string(2), r11);
            BMm.A02(C06680bu.A0C, r1);
            BMm.A02(C06680bu.A0p, new AnonymousClass18X(this));
            BMm.A02(C06680bu.A0h, r9);
            BMm.A02(C06680bu.A0E, r8);
            BMm.A02(C06680bu.A0G, r7);
            BMm.A02(C06680bu.A0r, r6);
            BMm.A02(C06680bu.A0V, r5);
            BMm.A02(C06680bu.A0U, r4);
            BMm.A02(C06680bu.A00, r3);
            this.A00 = BMm.A00();
        }
        Preconditions.checkNotNull(this.A00);
        this.A00.A00();
        this.A04.C0i(C05690aA.A13, this.A05);
    }

    public void A03() {
        C06790c5 r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
        this.A04.CJt(C05690aA.A13, this.A05);
    }

    public C33741o4(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(1, r3);
        this.A01 = C04430Uq.A02(r3);
        this.A04 = FbSharedPreferencesModule.A00(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        if (r1 == X.C61242yZ.A02) goto L_0x0041;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C33741o4 r6, android.content.Intent r7) {
        /*
            r7.getAction()
            r7.getExtras()
            X.17v r0 = r6.A03
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.String r2 = r7.getAction()
            r1 = -2053046278(0xffffffff85a0fffa, float:-1.5140359E-35)
            java.lang.String r0 = "threadsUpdatedReceiver action=%s"
            X.C005505z.A05(r0, r2, r1)
            java.lang.String r0 = "broadcast_extras"
            android.os.Bundle r2 = r7.getBundleExtra(r0)     // Catch:{ all -> 0x00d7 }
            java.util.List r5 = java.util.Collections.emptyList()     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = "multiple_thread_keys"
            boolean r0 = r7.hasExtra(r1)     // Catch:{ all -> 0x00d7 }
            if (r0 == 0) goto L_0x002d
            java.util.ArrayList r5 = r7.getParcelableArrayListExtra(r1)     // Catch:{ all -> 0x00d7 }
        L_0x002d:
            r3 = 0
            if (r2 == 0) goto L_0x0048
            java.lang.String r0 = "broadcast_cause"
            java.io.Serializable r1 = r2.getSerializable(r0)     // Catch:{ all -> 0x00d7 }
            X.2yZ r1 = (X.C61242yZ) r1     // Catch:{ all -> 0x00d7 }
            X.2yZ r0 = X.C61242yZ.A06     // Catch:{ all -> 0x00d7 }
            if (r1 == r0) goto L_0x0041
            X.2yZ r0 = X.C61242yZ.A02     // Catch:{ all -> 0x00d7 }
            r2 = 0
            if (r1 != r0) goto L_0x0042
        L_0x0041:
            r2 = 1
        L_0x0042:
            X.2yZ r0 = X.C61242yZ.A01     // Catch:{ all -> 0x00d7 }
            if (r1 != r0) goto L_0x0049
            r3 = 1
            goto L_0x0049
        L_0x0048:
            r2 = 0
        L_0x0049:
            java.util.Iterator r1 = r5.iterator()     // Catch:{ all -> 0x00d7 }
        L_0x004d:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x00d7 }
            if (r0 == 0) goto L_0x0060
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x00d7 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = (com.facebook.messaging.model.threadkey.ThreadKey) r0     // Catch:{ all -> 0x00d7 }
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r0)     // Catch:{ all -> 0x00d7 }
            if (r0 == 0) goto L_0x004d
            goto L_0x0062
        L_0x0060:
            r0 = 0
            goto L_0x0063
        L_0x0062:
            r0 = 1
        L_0x0063:
            if (r0 != 0) goto L_0x006e
            if (r3 == 0) goto L_0x006e
            r0 = -1375805620(0xffffffffadfedf4c, float:-2.897562E-11)
            X.C005505z.A00(r0)
            return
        L_0x006e:
            if (r2 == 0) goto L_0x0071
            goto L_0x0074
        L_0x0071:
            X.1FD r4 = X.AnonymousClass1FD.AUTOMATIC_REFRESH     // Catch:{ all -> 0x00d7 }
            goto L_0x0076
        L_0x0074:
            X.1FD r4 = X.AnonymousClass1FD.RECEIPTS_REFRESH     // Catch:{ all -> 0x00d7 }
        L_0x0076:
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ all -> 0x00d7 }
            X.0UN r0 = r6.A02     // Catch:{ all -> 0x00d7 }
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x00d7 }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ all -> 0x00d7 }
            r0 = 285035504735590(0x1033d00021566, double:1.40826250734875E-309)
            boolean r0 = r2.Aem(r0)     // Catch:{ all -> 0x00d7 }
            if (r0 == 0) goto L_0x00ba
            X.23F r3 = new X.23F     // Catch:{ all -> 0x00d7 }
            r3.<init>(r6, r7, r4, r5)     // Catch:{ all -> 0x00d7 }
            int r1 = X.AnonymousClass1Y3.ASI     // Catch:{ all -> 0x00d7 }
            X.0UN r0 = r6.A02     // Catch:{ all -> 0x00d7 }
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x00d7 }
            X.0g4 r2 = (X.AnonymousClass0g4) r2     // Catch:{ all -> 0x00d7 }
            int r1 = X.AnonymousClass1Y3.BFn     // Catch:{ all -> 0x00d7 }
            X.0UN r0 = r6.A02     // Catch:{ all -> 0x00d7 }
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)     // Catch:{ all -> 0x00d7 }
            X.108 r1 = (X.AnonymousClass108) r1     // Catch:{ all -> 0x00d7 }
            r1.A02(r3)     // Catch:{ all -> 0x00d7 }
            java.lang.String r0 = "threadListUpdate"
            r1.A02 = r0     // Catch:{ all -> 0x00d7 }
            java.lang.String r0 = "ForUiThread"
            r1.A03(r0)     // Catch:{ all -> 0x00d7 }
            X.1TL r1 = r1.A01()     // Catch:{ all -> 0x00d7 }
            java.lang.String r0 = "KeepExisting"
            r2.A04(r1, r0)     // Catch:{ all -> 0x00d7 }
            goto L_0x00d0
        L_0x00ba:
            X.17v r1 = r6.A03     // Catch:{ all -> 0x00d7 }
            java.lang.String r0 = "threads updated"
            r1.BtU(r0)     // Catch:{ all -> 0x00d7 }
            X.17v r2 = r6.A03     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = r7.getAction()     // Catch:{ all -> 0x00d7 }
            java.lang.String r0 = "calling_class"
            java.lang.String r0 = r7.getStringExtra(r0)     // Catch:{ all -> 0x00d7 }
            r2.BtY(r1, r4, r5, r0)     // Catch:{ all -> 0x00d7 }
        L_0x00d0:
            r0 = -641207120(0xffffffffd9c7f4b0, float:-7.0353196E15)
            X.C005505z.A00(r0)
            return
        L_0x00d7:
            r1 = move-exception
            r0 = -1641597983(0xffffffff9e2733e1, float:-8.851635E-21)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33741o4.A01(X.1o4, android.content.Intent):void");
    }
}
