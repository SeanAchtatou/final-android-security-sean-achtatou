package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import java.util.Map;

/* renamed from: X.1Pn  reason: invalid class name and case insensitive filesystem */
public class C23441Pn implements C23411Pk {
    public void Bk1(String str, String str2, String str3) {
    }

    public void Bk3(String str, String str2, Map map) {
    }

    public void Bk5(String str, String str2, Throwable th, Map map) {
    }

    public void Bk7(String str, String str2, Map map) {
        C37431vf r0;
        if (this instanceof C23431Pm) {
            C23431Pm r1 = (C23431Pm) this;
            if (map != null && !map.isEmpty() && (r0 = (C37431vf) r1.A01.A03(str)) != null) {
                AnonymousClass54I.A00(r0, str2, map);
            }
        } else if (this instanceof AnonymousClass1RY) {
            AnonymousClass1RY r12 = (AnonymousClass1RY) this;
            if (map != null && !map.isEmpty()) {
                AnonymousClass54I.A00(r12.A00, str2, map);
            }
        }
    }

    public void Bk9(String str, String str2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void Bls(String str) {
        if (this instanceof C23471Pq) {
            C23471Pq r1 = (C23471Pq) this;
            if (AnonymousClass1QP.A01()) {
                r1.A01.markerEnd(1179653, System.identityHashCode(str), (short) 4);
            }
        } else if (this instanceof C23401Pj) {
            C23401Pj r4 = (C23401Pj) this;
            synchronized (r4) {
                r4.A00.A04("requests_cancelled", 1);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void Bm2(AnonymousClass1Q0 r7, String str, Throwable th, boolean z) {
        if (this instanceof C23471Pq) {
            C23471Pq r5 = (C23471Pq) this;
            if (AnonymousClass1QP.A01()) {
                int identityHashCode = System.identityHashCode(str);
                C21061Ew withMarker = r5.A01.withMarker(1179653, identityHashCode);
                withMarker.A08("exception", th.getClass().getName());
                withMarker.A08("cause", th.toString());
                withMarker.BK9();
                r5.A01.markerEnd(1179653, identityHashCode, (short) 3);
            }
        } else if (this instanceof C23401Pj) {
            C23401Pj r4 = (C23401Pj) this;
            synchronized (r4) {
                r4.A00.A04("requests_failed", 1);
            }
        } else if (this instanceof C23431Pm) {
            ((C23431Pm) this).A01.A04(str);
        }
    }

    public void Bm7(AnonymousClass1Q0 r6, Object obj, String str, boolean z) {
        if (this instanceof C23471Pq) {
            C23471Pq r4 = (C23471Pq) this;
            if (AnonymousClass1QP.A01()) {
                int identityHashCode = System.identityHashCode(str);
                r4.A01.markerStart(1179653, identityHashCode);
                if (r4.A01.isMarkerOn(1179653, identityHashCode)) {
                    CallerContext callerContext = obj instanceof CallerContext ? (CallerContext) obj : CallerContext.A06;
                    String str2 = r4.A00.A05;
                    if (str2 == null) {
                        str2 = "UNKNOWN";
                    }
                    C21061Ew withMarker = r4.A01.withMarker(1179653, identityHashCode);
                    AnonymousClass55L.A00(withMarker, str2, r6, callerContext, z);
                    withMarker.A08("image_request_id", str);
                    withMarker.BK9();
                }
            }
        } else if (this instanceof C23431Pm) {
            C23431Pm r3 = (C23431Pm) this;
            C37431vf r2 = (C37431vf) r3.A00.A04(Integer.valueOf(System.identityHashCode(r6)));
            if (r2 != null) {
                r3.A01.A05(str, r2);
                r2.A02.A06 = r6.A02;
            }
        } else if (this instanceof AnonymousClass1RY) {
            ((AnonymousClass1RY) this).A00.A02.A06 = r6.A02;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void Bm9(AnonymousClass1Q0 r9, String str, boolean z) {
        C33651nv r0;
        if (this instanceof AnonymousClass1SD) {
            AnonymousClass1SD r5 = (AnonymousClass1SD) this;
            if (AnonymousClass1QP.A01()) {
                int identityHashCode = System.identityHashCode(str);
                r5.A01.markerStart(1179652, identityHashCode);
                if (r5.A01.isMarkerOn(1179652, identityHashCode)) {
                    String str2 = r5.A00.A05;
                    if (str2 == null) {
                        str2 = "UNKNOWN";
                    }
                    C21061Ew withMarker = r5.A01.withMarker(1179652, identityHashCode);
                    AnonymousClass55L.A00(withMarker, str2, r9, CallerContext.A06, z);
                    withMarker.BK9();
                }
                r5.A01.markerEnd(1179652, identityHashCode, (short) 2);
            }
        } else if (this instanceof AnonymousClass1MI) {
            AnonymousClass1MI r4 = (AnonymousClass1MI) this;
            synchronized (r4) {
                if (!z) {
                    if (AnonymousClass1MI.A00(r4)) {
                        Optional optional = r4.A00;
                        if (optional.isPresent() && !((C33651nv) optional.get()).A03.isPresent() && ((C33651nv) optional.get()).A02.equals(r9.A02)) {
                            AnonymousClass1QT r6 = r4.A03;
                            C33651nv r1 = (C33651nv) r4.A00.get();
                            long now = r4.A02.now();
                            synchronized (r6) {
                                Preconditions.checkState(r6.A00.BFQ());
                                Preconditions.checkState(r1.A02.toString().equals(r6.A00.B4F(r6.A09, null)));
                                C30281hn edit = r6.A00.edit();
                                edit.BzA(r6.A06, now);
                                edit.commit();
                                r0 = (C33651nv) r6.A00().get();
                            }
                            r4.A00 = Optional.fromNullable(r0);
                        }
                    }
                }
            }
        } else if (this instanceof C23471Pq) {
            C23471Pq r12 = (C23471Pq) this;
            if (AnonymousClass1QP.A01()) {
                r12.A01.markerEnd(1179653, System.identityHashCode(str), (short) 2);
            }
        } else if (this instanceof C23401Pj) {
            C23401Pj r42 = (C23401Pj) this;
            synchronized (r42) {
                r42.A00.A04("requests_succeeded", 1);
            }
        } else if (this instanceof C23431Pm) {
            ((C23431Pm) this).A01.A04(str);
        }
    }

    public void Bt9(String str, String str2, boolean z) {
        if (this instanceof C23401Pj) {
            C23401Pj r4 = (C23401Pj) this;
            String format = String.format(null, "%s_%s", str2, z ? OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS : "failed");
            synchronized (r4) {
                r4.A00.A04(format, 1);
            }
        } else if (this instanceof C37501vm) {
            ((C37501vm) this).A00.A00 = C621030a.A00(str2);
        } else if (this instanceof C23431Pm) {
            C37431vf r0 = (C37431vf) ((C23431Pm) this).A01.A03(str);
            if (r0 != null) {
                AnonymousClass54I.A01(r0, str2, z);
            }
        } else if (this instanceof AnonymousClass1RY) {
            AnonymousClass54I.A01(((AnonymousClass1RY) this).A00, str2, z);
        }
    }

    public boolean C3R(String str) {
        return !(this instanceof C23431Pm) ? this instanceof AnonymousClass1RY : ((C23431Pm) this).A01.A03(str) != null;
    }
}
