package X;

import com.facebook.acra.ACRA;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.forker.Process;
import com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo;
import com.facebook.webrtc.models.FbWebrtcParticipantInfo;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.Executor;
import org.webrtc.audio.WebRtcAudioRecord;

@UserScoped
/* renamed from: X.1m3  reason: invalid class name and case insensitive filesystem */
public final class C32621m3 implements C32651m6 {
    private static C05540Zi A08;
    public int A00;
    public AnonymousClass0UN A01;
    public C32631m4 A02 = C32631m4.UNKNOWN;
    public String A03;
    public Map A04;
    public boolean A05;
    public final C32641m5 A06 = new C32641m5();
    public final Comparator A07 = new C33831oD();

    public ImmutableList A0E() {
        ImmutableList A032 = A03(this, false);
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = A032.iterator();
        while (it.hasNext()) {
            FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
            if (fbWebrtcConferenceParticipantInfo.A04()) {
                builder.add((Object) fbWebrtcConferenceParticipantInfo.A02());
            }
        }
        return builder.build();
    }

    public void A0I() {
        this.A04 = null;
        this.A00 = 1;
        this.A02 = C32631m4.UNKNOWN;
        this.A06.Bhv();
        ((C34961qS) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Axj, this.A01)).A07(this);
        this.A03 = null;
        this.A05 = false;
    }

    public boolean A0R() {
        C24971Xv it = A03(this, false).iterator();
        while (it.hasNext()) {
            if (((FbWebrtcConferenceParticipantInfo) it.next()).A03.A00 == C32631m4.CONTACTING) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0S() {
        /*
            r3 = this;
            r0 = 0
            com.google.common.collect.ImmutableList r0 = A03(r3, r0)
            X.1Xv r2 = r0.iterator()
        L_0x0009:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0023
            java.lang.Object r0 = r2.next()
            com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo r0 = (com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo) r0
            com.facebook.webrtc.models.FbWebrtcParticipantInfo r0 = r0.A03
            X.1m4 r1 = r0.A00
            X.1m4 r0 = X.C32631m4.CONNECTING
            if (r1 == r0) goto L_0x0021
            X.1m4 r0 = X.C32631m4.RINGING
            if (r1 != r0) goto L_0x0009
        L_0x0021:
            r0 = 1
            return r0
        L_0x0023:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32621m3.A0S():boolean");
    }

    public static int A00(C32621m3 r3, boolean z) {
        ImmutableList A0G;
        int i = 0;
        if (r3.A04 != null) {
            if (z) {
                A0G = r3.A0C();
            } else {
                A0G = r3.A0G();
            }
            C24971Xv it = A0G.iterator();
            while (it.hasNext()) {
                if (((FbWebrtcConferenceParticipantInfo) it.next()).A04()) {
                    i++;
                }
            }
        }
        return i;
    }

    public static final C32621m3 A01(AnonymousClass1XY r4) {
        C32621m3 r0;
        synchronized (C32621m3.class) {
            C05540Zi A002 = C05540Zi.A00(A08);
            A08 = A002;
            try {
                if (A002.A03(r4)) {
                    A08.A00 = new C32621m3((AnonymousClass1XY) A08.A01());
                }
                C05540Zi r1 = A08;
                r0 = (C32621m3) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A08.A02();
                throw th;
            }
        }
        return r0;
    }

    public static C163997iO A02(C32621m3 r4, String str, C32631m4 r6) {
        C163997iO r3 = new C163997iO(str);
        C163997iO.A00(r3).A00 = r6;
        r3.A01 = ((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, r4.A01)).now();
        r3.A02 = C123945sc.A01;
        return r3;
    }

    public static ImmutableList A03(C32621m3 r4, boolean z) {
        ImmutableList A0G;
        if (z) {
            A0G = r4.A0C();
        } else {
            A0G = r4.A0G();
        }
        ArrayList arrayList = new ArrayList(A0G);
        if (arrayList.isEmpty()) {
            return RegularImmutableList.A02;
        }
        FbWebrtcConferenceParticipantInfo A0B = r4.A0B((String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, r4.A01));
        if (A0B != null) {
            arrayList.remove(A0B);
        }
        return ImmutableList.copyOf((Collection) arrayList);
    }

    public static ImmutableList A04(C32621m3 r2, boolean z) {
        ImmutableList A032;
        if (r2.A04 == null) {
            return RegularImmutableList.A02;
        }
        if (z) {
            A032 = A03(r2, true);
        } else {
            A032 = A03(r2, false);
        }
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = A032.iterator();
        while (it.hasNext()) {
            FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
            if (fbWebrtcConferenceParticipantInfo.A04() || fbWebrtcConferenceParticipantInfo.A05()) {
                builder.add((Object) fbWebrtcConferenceParticipantInfo.A02());
            }
        }
        return builder.build();
    }

    public static void A06(C32621m3 r5, ImmutableList immutableList, ImmutableList immutableList2, String str, boolean z) {
        C32631m4 r0;
        r5.A04 = Collections.synchronizedMap(new AnonymousClass04a());
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (!C06850cB.A0B(str2) && !str2.contentEquals((String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, r5.A01)) && !C06850cB.A0C(str2, str)) {
                if (immutableList2.contains(str2)) {
                    r0 = C32631m4.CONTACTING;
                } else {
                    r0 = C32631m4.UNKNOWN;
                }
                r5.A0N(str2, r0);
            }
        }
        r5.A06.A00(new AnonymousClass2OT(r5, str));
        r5.A0Q(z);
        r5.A00 = 1;
        ((C34961qS) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Axj, r5.A01)).A06(r5);
    }

    public static boolean A08(C32621m3 r6, Predicate predicate, boolean z) {
        if (r6.A04 != null) {
            C24971Xv it = r6.A0C().iterator();
            while (it.hasNext()) {
                FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
                if ((z || !((String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, r6.A01)).contentEquals(fbWebrtcConferenceParticipantInfo.A02())) && predicate.apply(fbWebrtcConferenceParticipantInfo)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int A09() {
        int i = 0;
        if (this.A04 != null) {
            C24971Xv it = A0G().iterator();
            while (it.hasNext()) {
                FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
                if (fbWebrtcConferenceParticipantInfo.A04() || fbWebrtcConferenceParticipantInfo.A05()) {
                    i++;
                }
            }
        }
        return i;
    }

    public FbWebrtcConferenceParticipantInfo A0A() {
        if (this.A04 != null) {
            C24971Xv it = A0G().iterator();
            while (it.hasNext()) {
                FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
                if (fbWebrtcConferenceParticipantInfo.A04() && !fbWebrtcConferenceParticipantInfo.A02().contentEquals((String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, this.A01))) {
                    return fbWebrtcConferenceParticipantInfo;
                }
            }
        }
        return null;
    }

    public FbWebrtcConferenceParticipantInfo A0B(String str) {
        Map map = this.A04;
        if (map != null) {
            if (map.containsKey(str)) {
                return (FbWebrtcConferenceParticipantInfo) this.A04.get(str);
            }
            C24971Xv it = A0C().iterator();
            while (it.hasNext()) {
                FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
                if (fbWebrtcConferenceParticipantInfo.A02().equals(str)) {
                    return fbWebrtcConferenceParticipantInfo;
                }
            }
        }
        return null;
    }

    public ImmutableList A0C() {
        Map map = this.A04;
        if (map == null) {
            return RegularImmutableList.A02;
        }
        return ImmutableList.copyOf(map.values());
    }

    public ImmutableList A0D() {
        if (this.A04 == null) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        C24971Xv it = A0C().iterator();
        while (it.hasNext()) {
            builder.add((Object) ((FbWebrtcConferenceParticipantInfo) it.next()).A02());
        }
        return builder.build();
    }

    public ImmutableList A0F() {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = A03(this, false).iterator();
        while (it.hasNext()) {
            builder.add((Object) ((FbWebrtcConferenceParticipantInfo) it.next()).A02());
        }
        return builder.build();
    }

    public ImmutableList A0G() {
        Map map = this.A04;
        if (map == null) {
            return RegularImmutableList.A02;
        }
        return ImmutableList.copyOf(map.values());
    }

    public ImmutableList A0H() {
        if (this.A04 == null) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = new ImmutableList.Builder();
        C24971Xv it = A03(this, false).iterator();
        while (it.hasNext()) {
            FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
            if (fbWebrtcConferenceParticipantInfo.A04()) {
                builder.add((Object) fbWebrtcConferenceParticipantInfo);
            }
        }
        return builder.build();
    }

    public void A0J(long j, String str, int i, boolean z) {
        FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo;
        Map map = this.A04;
        if (map != null && (fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) map.get(str)) != null) {
            C163997iO r2 = new C163997iO(fbWebrtcConferenceParticipantInfo);
            if (i == 0) {
                C163997iO.A00(r2).A05 = z;
            } else if (i == 1) {
                C163997iO.A00(r2).A02 = Optional.of(Long.valueOf(j));
                C163997iO.A00(r2).A07 = z;
                C163997iO.A00(r2).A09 = false;
                C163997iO.A00(r2).A08 = false;
            } else if (i == 2) {
                C163997iO.A00(r2).A01 = Optional.of(Long.valueOf(j));
                C163997iO.A00(r2).A06 = z;
            }
            if (A0U(new FbWebrtcConferenceParticipantInfo(r2))) {
                this.A06.Bhu();
            }
        }
    }

    public void A0K(C17040yE r2) {
        this.A06.A04.add(r2);
    }

    public void A0L(C17040yE r2) {
        this.A06.A04.remove(r2);
    }

    public void A0M(ImmutableList immutableList, ImmutableList immutableList2) {
        A06(this, immutableList, immutableList2, (String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, this.A01), false);
    }

    public void A0O(String str, C32631m4 r9) {
        FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo;
        Map map = this.A04;
        if (map != null && (fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) map.get(str)) != null) {
            C163997iO r6 = new C163997iO(fbWebrtcConferenceParticipantInfo);
            C163997iO.A00(r6).A00 = r9;
            if (r9 == C32631m4.CONNECTED && fbWebrtcConferenceParticipantInfo.A00 == 0) {
                long now = ((AnonymousClass069) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBa, this.A01)).now();
                boolean z = false;
                if (now > 0) {
                    z = true;
                }
                Preconditions.checkArgument(z);
                r6.A00 = now;
            }
            A0U(new FbWebrtcConferenceParticipantInfo(r6));
        }
    }

    public void A0P(String str, byte[] bArr) {
        FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo;
        Map map = this.A04;
        if (map != null && (fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) map.get(str)) != null) {
            C163997iO r3 = new C163997iO(fbWebrtcConferenceParticipantInfo);
            r3.A05 = ((C164177ih) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ARA, this.A01)).A01(bArr);
            A0U(new FbWebrtcConferenceParticipantInfo(r3));
        }
    }

    public boolean A0T() {
        FbWebrtcConferenceParticipantInfo A0B = A0B((String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, this.A01));
        if (A0B == null || A0B.A03.A00 != C32631m4.CONNECTED) {
            return false;
        }
        return true;
    }

    public boolean A0U(FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo) {
        C32631m4 r5;
        Map map = this.A04;
        if (map != null) {
            String A022 = fbWebrtcConferenceParticipantInfo.A02();
            FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo2 = (FbWebrtcConferenceParticipantInfo) map.get(A022);
            if (fbWebrtcConferenceParticipantInfo2 == null || !fbWebrtcConferenceParticipantInfo2.equals(fbWebrtcConferenceParticipantInfo)) {
                if (this.A04.put(A022, fbWebrtcConferenceParticipantInfo) == null) {
                    this.A06.Bhv();
                    fbWebrtcConferenceParticipantInfo.toString();
                } else {
                    this.A06.Bht();
                    fbWebrtcConferenceParticipantInfo.toString();
                }
                if (!fbWebrtcConferenceParticipantInfo.A02().equals((String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, this.A01))) {
                    C32631m4 r2 = fbWebrtcConferenceParticipantInfo.A03.A00;
                    if (fbWebrtcConferenceParticipantInfo2 != null) {
                        r5 = fbWebrtcConferenceParticipantInfo2.A03.A00;
                    } else {
                        r5 = C32631m4.UNKNOWN;
                    }
                    if (r2 != r5) {
                        int compare = this.A07.compare(r2, this.A02);
                        if (compare > 0 || (compare == 0 && r2 == C32631m4.CONNECTED)) {
                            this.A02 = r2;
                            A07(r2);
                        } else if (r5 == this.A02) {
                            C32631m4 r0 = C32631m4.UNKNOWN;
                            C24971Xv it = A03(this, false).iterator();
                            while (it.hasNext()) {
                                FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo3 = (FbWebrtcConferenceParticipantInfo) it.next();
                                if (this.A07.compare(fbWebrtcConferenceParticipantInfo3.A03.A00, r0) > 0) {
                                    r0 = fbWebrtcConferenceParticipantInfo3.A03.A00;
                                }
                            }
                            this.A02 = r0;
                            if (r5 != r0) {
                                A07(r0);
                            }
                        }
                    }
                }
                if (!fbWebrtcConferenceParticipantInfo.A04()) {
                    return true;
                }
                this.A00 = Math.max(this.A00, A00(this, false));
                return true;
            }
        }
        return false;
    }

    public Map Bxv() {
        String str;
        if (this.A04 == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        C24971Xv it = A0G().iterator();
        while (it.hasNext()) {
            FbWebrtcConferenceParticipantInfo fbWebrtcConferenceParticipantInfo = (FbWebrtcConferenceParticipantInfo) it.next();
            boolean equals = fbWebrtcConferenceParticipantInfo.A02().equals((String) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Awq, this.A01));
            sb.append("\n\t");
            sb.append("Participant Type: ");
            if (equals) {
                str = "Self";
            } else {
                str = "Remote";
            }
            sb.append(str);
            sb.append(" - Video On: ");
            FbWebrtcParticipantInfo fbWebrtcParticipantInfo = fbWebrtcConferenceParticipantInfo.A03;
            sb.append(fbWebrtcParticipantInfo.A07);
            sb.append(" - Video cname: ");
            sb.append(fbWebrtcParticipantInfo.A04);
        }
        return Collections.singletonMap("Call Participants Info", sb.toString());
    }

    private C32621m3(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(9, r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0041, code lost:
        if (A03(r6, false).size() == 1) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A05(X.C32621m3 r6, com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo r7, com.facebook.user.model.User r8, boolean r9) {
        /*
            boolean r0 = r8.A0G()
            if (r0 == 0) goto L_0x0009
            r0 = 1
            r6.A05 = r0
        L_0x0009:
            if (r9 == 0) goto L_0x0062
            boolean r0 = r8.A1K
            r5 = 1
            if (r0 == 0) goto L_0x008e
            com.google.common.collect.ImmutableList r0 = r8.A0U
            X.1Xv r4 = r0.iterator()
        L_0x0016:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x008c
            java.lang.Object r0 = r4.next()
            com.facebook.user.model.AlohaUser r0 = (com.facebook.user.model.AlohaUser) r0
            java.lang.String r3 = r0.fbId
            r2 = 7
            int r1 = X.AnonymousClass1Y3.Awq
            X.0UN r0 = r6.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = X.C06850cB.A0C(r3, r0)
            if (r0 == 0) goto L_0x0016
            r0 = 1
        L_0x0036:
            if (r0 == 0) goto L_0x008e
            r0 = 0
            com.google.common.collect.ImmutableList r0 = A03(r6, r0)
            int r0 = r0.size()
            if (r0 != r5) goto L_0x008e
        L_0x0043:
            if (r5 == 0) goto L_0x0062
            int r4 = X.AnonymousClass1Y3.BTr
            X.0UN r0 = r6.A01
            r3 = 4
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r4, r0)
            X.0sv r2 = (X.C14260sv) r2
            r0 = 1
            r2.A0Y = r0
            r1 = 0
            r2.A0H(r1)
            X.0UN r0 = r6.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r4, r0)
            X.0sv r0 = (X.C14260sv) r0
            r0.A0I(r1)
        L_0x0062:
            com.facebook.user.model.Name r0 = r8.A0L
            if (r0 == 0) goto L_0x008b
            X.7iO r2 = new X.7iO
            r2.<init>(r7)
            java.lang.String r0 = r0.firstName
            r2.A06 = r0
            java.lang.String r0 = r8.A09()
            r2.A07 = r0
            java.lang.String r1 = r8.A0x
            java.lang.String r0 = "call_guest"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0083
            X.5sc r0 = X.C123945sc.A02
            r2.A02 = r0
        L_0x0083:
            com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo r0 = new com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo
            r0.<init>(r2)
            r6.A0U(r0)
        L_0x008b:
            return
        L_0x008c:
            r0 = 0
            goto L_0x0036
        L_0x008e:
            r5 = 0
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32621m3.A05(X.1m3, com.facebook.webrtc.models.FbWebrtcConferenceParticipantInfo, com.facebook.user.model.User, boolean):void");
    }

    private void A07(C32631m4 r4) {
        switch (r4.ordinal()) {
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                C163497hM.A04((C163497hM) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ad7, this.A01), "MWS", "CONTACTING");
                return;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                C163497hM.A04((C163497hM) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ad7, this.A01), "MWS", "RINGING");
                return;
            case 8:
                C163497hM.A04((C163497hM) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ad7, this.A01), "MWS", "CONNECTING");
                return;
            case Process.SIGKILL:
                int A002 = A00(this, false);
                if (A002 == 2) {
                    C163497hM.A04((C163497hM) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ad7, this.A01), "MWS", "CONNECTED");
                    return;
                } else if (A002 == 3) {
                    C163497hM.A04((C163497hM) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ad7, this.A01), "MWS", "CONNECTED_3_PLUS");
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void A0N(String str, C32631m4 r4) {
        A0U(new FbWebrtcConferenceParticipantInfo(A02(this, str, r4)));
    }

    public void A0Q(boolean z) {
        if (A0C().size() > 0) {
            HashSet hashSet = new HashSet();
            ImmutableList.Builder builder = ImmutableList.builder();
            this.A06.A00(new C161847eP(this, hashSet, builder, z));
            if (!hashSet.isEmpty()) {
                ImmutableList build = builder.build();
                AnonymousClass7OU r3 = (AnonymousClass7OU) AnonymousClass1XX.A02(6, AnonymousClass1Y3.A8j, this.A01);
                C05350Yp.A08(((AnonymousClass0VL) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ALA, r3.A00)).CIF(new AnonymousClass7OV(r3, build)), new C161737eE(this, z), (Executor) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A6Z, this.A01));
            }
        }
    }
}
