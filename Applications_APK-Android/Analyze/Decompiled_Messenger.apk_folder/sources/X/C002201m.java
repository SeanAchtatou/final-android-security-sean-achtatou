package X;

/* renamed from: X.01m  reason: invalid class name and case insensitive filesystem */
public final class C002201m implements AnonymousClass015 {
    public static C002201m A00;

    public void handleUncaughtException(Thread thread, Throwable th, C02120Da r5) {
        if (th instanceof C03870Qf) {
            th = ((C03870Qf) th).mInner;
        }
        AnonymousClass01P.A0A(C002101k.A0B, th);
        C010708t.A0R("AppStateLoggerExceptionHandler", th, "Handling uncaught exception");
    }
}
