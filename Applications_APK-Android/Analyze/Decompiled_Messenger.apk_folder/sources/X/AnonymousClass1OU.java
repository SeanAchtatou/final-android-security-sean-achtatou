package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1OU  reason: invalid class name */
public final class AnonymousClass1OU extends AnonymousClass0UV {
    private static volatile AnonymousClass1OX A00;
    private static volatile AnonymousClass1OX A01;
    private static volatile AnonymousClass1OW A02;
    private static volatile AnonymousClass1OY A03;
    private static volatile C23051Oa A04;
    private static volatile AnonymousClass1OV A05;
    private static volatile AnonymousClass1OV A06;

    public static final AnonymousClass1OX A00(AnonymousClass1XY r5) {
        if (A00 == null) {
            synchronized (AnonymousClass1OX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A00 = new AnonymousClass1OX(AnonymousClass0WT.A00(applicationInjector), AnonymousClass067.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final AnonymousClass1OX A01(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass1OX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        A01 = A00(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final AnonymousClass1OW A02(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (AnonymousClass1OW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = new AnonymousClass1OW(A01(applicationInjector), A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final AnonymousClass1OY A03(AnonymousClass1XY r7) {
        if (A03 == null) {
            synchronized (AnonymousClass1OY.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A03 = new AnonymousClass1OY(new C23091Oe(), AnonymousClass1OZ.A00(applicationInjector), A04(applicationInjector), AnonymousClass0ZD.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final C23051Oa A04(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C23051Oa.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C23061Ob(C06920cI.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public static final AnonymousClass1OV A05(AnonymousClass1XY r3) {
        if (A05 == null) {
            synchronized (AnonymousClass1OV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r3);
                if (A002 != null) {
                    try {
                        A05 = A06(r3.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static final AnonymousClass1OV A06(AnonymousClass1XY r6) {
        if (A06 == null) {
            synchronized (AnonymousClass1OV.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A06 = new AnonymousClass1OV(A02(applicationInjector), A04(applicationInjector), AnonymousClass0ZD.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }
}
