package X;

/* renamed from: X.0Fr  reason: invalid class name and case insensitive filesystem */
public final class C02630Fr extends AnonymousClass0FM {
    public int acraActiveRadioTimeS;
    public int acraRadioWakeupCount;
    public int acraTailRadioTimeS;
    public long acraTxBytes;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                C02630Fr r8 = (C02630Fr) obj;
                if (!(this.acraActiveRadioTimeS == r8.acraActiveRadioTimeS && this.acraTailRadioTimeS == r8.acraTailRadioTimeS && this.acraRadioWakeupCount == r8.acraRadioWakeupCount && this.acraTxBytes == r8.acraTxBytes)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    private void A00(C02630Fr r3) {
        this.acraActiveRadioTimeS = r3.acraActiveRadioTimeS;
        this.acraTailRadioTimeS = r3.acraTailRadioTimeS;
        this.acraRadioWakeupCount = r3.acraRadioWakeupCount;
        this.acraTxBytes = r3.acraTxBytes;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((C02630Fr) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02630Fr r52 = (C02630Fr) r5;
        C02630Fr r62 = (C02630Fr) r6;
        if (r62 == null) {
            r62 = new C02630Fr();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.acraActiveRadioTimeS = this.acraActiveRadioTimeS - r52.acraActiveRadioTimeS;
        r62.acraTailRadioTimeS = this.acraTailRadioTimeS - r52.acraTailRadioTimeS;
        r62.acraRadioWakeupCount = this.acraRadioWakeupCount - r52.acraRadioWakeupCount;
        r62.acraTxBytes = this.acraTxBytes - r52.acraTxBytes;
        return r62;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r5, AnonymousClass0FM r6) {
        C02630Fr r52 = (C02630Fr) r5;
        C02630Fr r62 = (C02630Fr) r6;
        if (r62 == null) {
            r62 = new C02630Fr();
        }
        if (r52 == null) {
            r62.A00(this);
            return r62;
        }
        r62.acraActiveRadioTimeS = this.acraActiveRadioTimeS + r52.acraActiveRadioTimeS;
        r62.acraTailRadioTimeS = this.acraTailRadioTimeS + r52.acraTailRadioTimeS;
        r62.acraRadioWakeupCount = this.acraRadioWakeupCount + r52.acraRadioWakeupCount;
        r62.acraTxBytes = this.acraTxBytes + r52.acraTxBytes;
        return r62;
    }

    public String toString() {
        return "AcraRadioMetrics{acraActiveRadioTimeS=" + this.acraActiveRadioTimeS + ", acraTailRadioTimeS=" + this.acraTailRadioTimeS + ", acraRadioWakeupCount=" + this.acraRadioWakeupCount + ", acraTxBytes=" + this.acraTxBytes + '}';
    }

    public int hashCode() {
        long j = this.acraTxBytes;
        return (((((((super.hashCode() * 31) + this.acraActiveRadioTimeS) * 31) + this.acraTailRadioTimeS) * 31) + this.acraRadioWakeupCount) * 31) + ((int) (j ^ (j >>> 32)));
    }
}
