package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0bH  reason: invalid class name and case insensitive filesystem */
public final class C06300bH {
    private static volatile C06300bH A01;
    public AnonymousClass0UN A00;

    public static final C06300bH A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C06300bH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C06300bH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C06300bH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
