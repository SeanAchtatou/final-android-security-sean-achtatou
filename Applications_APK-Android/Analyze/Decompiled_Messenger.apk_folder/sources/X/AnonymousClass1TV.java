package X;

import com.google.common.collect.ImmutableList;

/* renamed from: X.1TV  reason: invalid class name */
public final class AnonymousClass1TV extends C20831Dz {
    public AnonymousClass0UN A00;
    public ImmutableList A01;
    public Integer A02;

    public static final AnonymousClass1TV A00(AnonymousClass1XY r1) {
        return new AnonymousClass1TV(r1);
    }

    public int ArU() {
        ImmutableList immutableList = this.A01;
        if (immutableList == null) {
            return 0;
        }
        return immutableList.size();
    }

    private AnonymousClass1TV(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
