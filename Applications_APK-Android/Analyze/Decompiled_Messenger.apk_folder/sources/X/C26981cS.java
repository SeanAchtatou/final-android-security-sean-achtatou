package X;

import android.os.Handler;
import android.os.Message;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.1cS  reason: invalid class name and case insensitive filesystem */
public final class C26981cS implements Handler.Callback {
    public final /* synthetic */ C08790fx A00;

    public C26981cS(C08790fx r1) {
        this.A00 = r1;
    }

    public boolean handleMessage(Message message) {
        if (message.what == 1) {
            C08790fx r4 = this.A00;
            int intValue = ((Integer) message.obj).intValue();
            synchronized (r4.A04) {
                if (r4.A03.get(intValue) != null) {
                    C08980gI r7 = (C08980gI) r4.A03.get(intValue);
                    if (((AnonymousClass069) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBa, r4.A00)).now() - r7.A00 >= C08790fx.A00(r4, intValue)) {
                        r4.A03.remove(intValue);
                        int i = (int) r7.A01;
                        if (i > 0) {
                            AnonymousClass08Z.A02(8, "Critical Path Entered", i);
                        }
                        r4.A03.size();
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r4.A00)).markerPoint(5505191, AnonymousClass08S.A0M("timeout", "_critical_path(", intValue, ")"));
                    }
                }
                if (!r4.A09()) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r4.A00)).markerEnd(5505191, 4);
                }
            }
            C08790fx.A06(r4, true);
        }
        return true;
    }
}
