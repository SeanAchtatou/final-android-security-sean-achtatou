package X;

import io.card.payment.BuildConfig;
import java.io.File;

/* renamed from: X.1tq  reason: invalid class name and case insensitive filesystem */
public final class C36811tq {
    public static String A00(File file, String str) {
        String A0P = AnonymousClass08S.A0P(file.getAbsolutePath(), "/mobileconfig/", BuildConfig.FLAVOR);
        if (str == null || str.isEmpty() || str.equals("0")) {
            return AnonymousClass08S.A0J(A0P, "sessionless.data/");
        }
        return AnonymousClass08S.A0P(A0P, str, ".data/");
    }
}
