package X;

import java.util.concurrent.Callable;

/* renamed from: X.0hV  reason: invalid class name and case insensitive filesystem */
public final class C09520hV implements Callable {
    public final /* synthetic */ C09500hT A00;
    public final /* synthetic */ C05530Zh A01;

    public C09520hV(C05530Zh r1, C09500hT r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0039, code lost:
        if (r4.A00 == X.C09510hU.DO_NOT_CHECK_SERVER) goto L_0x003b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x040d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object call() {
        /*
            r21 = this;
            r1 = r21
            X.0hT r0 = r1.A00
            X.1a7 r2 = r0.A01
            X.1a7 r0 = X.C25611a7.ALL
            if (r2 != r0) goto L_0x0015
            X.0Zh r2 = r1.A01
            X.100 r0 = r2.A01
            if (r0 == 0) goto L_0x0015
            X.1Zw r0 = r2.A07
            r0.A06()
        L_0x0015:
            r20 = 0
            X.0Zh r0 = r1.A01     // Catch:{ Exception -> 0x043d }
            X.1Zx r0 = r0.A06     // Catch:{ Exception -> 0x043d }
            X.0hT r4 = r1.A00     // Catch:{ Exception -> 0x043d }
            X.1a7 r3 = X.C25611a7.ALL     // Catch:{ Exception -> 0x043d }
            X.1a7 r2 = X.C25611a7.TOP     // Catch:{ Exception -> 0x043d }
            java.util.EnumSet r3 = java.util.EnumSet.of(r3, r2)     // Catch:{ Exception -> 0x043d }
            X.1a7 r2 = r4.A01     // Catch:{ Exception -> 0x043d }
            boolean r2 = r3.contains(r2)     // Catch:{ Exception -> 0x043d }
            com.google.common.base.Preconditions.checkArgument(r2)     // Catch:{ Exception -> 0x043d }
            X.1a7 r3 = r4.A01     // Catch:{ Exception -> 0x043d }
            X.1a7 r2 = X.C25611a7.ALL     // Catch:{ Exception -> 0x043d }
            if (r3 == r2) goto L_0x003b
            X.0hU r5 = r4.A00     // Catch:{ Exception -> 0x043d }
            X.0hU r3 = X.C09510hU.DO_NOT_CHECK_SERVER     // Catch:{ Exception -> 0x043d }
            r2 = 0
            if (r5 != r3) goto L_0x003c
        L_0x003b:
            r2 = 1
        L_0x003c:
            com.google.common.base.Preconditions.checkArgument(r2)     // Catch:{ Exception -> 0x043d }
            X.0hU r6 = r4.A00     // Catch:{ Exception -> 0x043d }
            X.1a7 r5 = r4.A01     // Catch:{ Exception -> 0x043d }
            java.lang.String r3 = "InboxUnitFetcherWithUnitStore.fetchInternal(%s, %s)"
            r2 = -1347811333(0xffffffffafaa07fb, float:-3.092849E-10)
            X.C005505z.A06(r3, r6, r5, r2)     // Catch:{ Exception -> 0x043d }
            X.0hU r3 = r4.A00     // Catch:{ all -> 0x0435 }
            X.0hU r2 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA     // Catch:{ all -> 0x0435 }
            if (r3 != r2) goto L_0x005a
            X.100 r3 = X.C25511Zx.A05(r0, r4)     // Catch:{ all -> 0x0435 }
            r0 = 1622867909(0x60baffc5, float:1.0779764E20)
            goto L_0x0407
        L_0x005a:
            X.0hU r2 = X.C09510hU.STALE_DATA_OKAY     // Catch:{ all -> 0x0435 }
            if (r3 == r2) goto L_0x03b8
            X.0hU r2 = X.C09510hU.DO_NOT_CHECK_SERVER     // Catch:{ all -> 0x0435 }
            if (r3 == r2) goto L_0x03b8
            X.1a7 r6 = r4.A01     // Catch:{ all -> 0x0435 }
            X.1a7 r3 = X.C25611a7.ALL     // Catch:{ all -> 0x0435 }
            r5 = 1
            r2 = 0
            if (r6 != r3) goto L_0x006b
            r2 = 1
        L_0x006b:
            com.google.common.base.Preconditions.checkState(r2)     // Catch:{ all -> 0x0435 }
            X.1cR r2 = X.C25511Zx.A02(r0, r4)     // Catch:{ all -> 0x0435 }
            int[] r3 = X.AnonymousClass2RL.A00     // Catch:{ all -> 0x0435 }
            int r2 = r2.ordinal()     // Catch:{ all -> 0x0435 }
            r3 = r3[r2]     // Catch:{ all -> 0x0435 }
            if (r3 == r5) goto L_0x03b0
            r2 = 2
            if (r3 == r2) goto L_0x03b0
            r2 = 3
            if (r3 == r2) goto L_0x0092
            r2 = 4
            if (r3 != r2) goto L_0x03aa
            X.102 r3 = X.AnonymousClass102.FROM_CACHE_UP_TO_DATE     // Catch:{ all -> 0x0435 }
            X.1a7 r2 = X.C25611a7.ALL     // Catch:{ all -> 0x0435 }
            X.100 r3 = X.C25511Zx.A03(r0, r3, r2)     // Catch:{ all -> 0x0435 }
            r0 = 833564890(0x31af30da, float:5.098724E-9)
            goto L_0x0407
        L_0x0092:
            X.1a4 r7 = r0.A03     // Catch:{ all -> 0x0435 }
            X.0go r2 = r7.A02     // Catch:{ all -> 0x0435 }
            android.database.sqlite.SQLiteDatabase r8 = r2.A06()     // Catch:{ all -> 0x0435 }
            X.0W6 r2 = X.C06070an.A01     // Catch:{ all -> 0x0435 }
            java.lang.String r3 = r2.A00     // Catch:{ all -> 0x0435 }
            r6 = 0
            X.0W6 r2 = X.C06070an.A08     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = r2.A00     // Catch:{ all -> 0x0435 }
            java.lang.String[] r10 = new java.lang.String[]{r3, r2}     // Catch:{ all -> 0x0435 }
            X.0av r2 = r7.A01     // Catch:{ all -> 0x0435 }
            java.lang.String r11 = r2.A02()     // Catch:{ all -> 0x0435 }
            X.0av r2 = r7.A01     // Catch:{ all -> 0x0435 }
            java.lang.String[] r12 = r2.A04()     // Catch:{ all -> 0x0435 }
            java.lang.String r9 = "units"
            r13 = 0
            r14 = 0
            r15 = 0
            android.database.Cursor r8 = r8.query(r9, r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x0435 }
            java.util.HashMap r19 = new java.util.HashMap     // Catch:{ all -> 0x03a5 }
            r19.<init>()     // Catch:{ all -> 0x03a5 }
        L_0x00c1:
            boolean r2 = r8.moveToNext()     // Catch:{ all -> 0x03a5 }
            if (r2 == 0) goto L_0x00d9
            java.lang.String r7 = r8.getString(r6)     // Catch:{ all -> 0x03a5 }
            long r2 = r8.getLong(r5)     // Catch:{ all -> 0x03a5 }
            java.lang.Long r3 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x03a5 }
            r2 = r19
            r2.put(r7, r3)     // Catch:{ all -> 0x03a5 }
            goto L_0x00c1
        L_0x00d9:
            r8.close()     // Catch:{ all -> 0x0435 }
            int r5 = X.AnonymousClass1Y3.AGk     // Catch:{ all -> 0x0435 }
            X.0UN r3 = r0.A00     // Catch:{ all -> 0x0435 }
            r2 = 3
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r5, r3)     // Catch:{ all -> 0x0435 }
            com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper r3 = (com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper) r3     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = r0.A0C     // Catch:{ all -> 0x0435 }
            com.facebook.graphql.query.GQSQStringShape0S0000000_I0 r5 = r3.A02(r2)     // Catch:{ all -> 0x0435 }
            com.fasterxml.jackson.databind.node.ObjectNode r7 = new com.fasterxml.jackson.databind.node.ObjectNode     // Catch:{ all -> 0x0435 }
            com.fasterxml.jackson.databind.node.JsonNodeFactory r2 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance     // Catch:{ all -> 0x0435 }
            r7.<init>(r2)     // Catch:{ all -> 0x0435 }
            java.util.Set r2 = r19.entrySet()     // Catch:{ all -> 0x0435 }
            java.util.Iterator r6 = r2.iterator()     // Catch:{ all -> 0x0435 }
        L_0x00fc:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x0435 }
            if (r2 == 0) goto L_0x0118
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x0435 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x0435 }
            java.lang.Object r3 = r2.getKey()     // Catch:{ all -> 0x0435 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0435 }
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x0435 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x0435 }
            r7.put(r3, r2)     // Catch:{ all -> 0x0435 }
            goto L_0x00fc
        L_0x0118:
            java.lang.String r3 = r7.toString()     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "formattedUnitUpdateTimestamps"
            r5.A09(r2, r3)     // Catch:{ all -> 0x0435 }
            java.lang.String r3 = r0.A0C     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "serviceType"
            r5.A09(r2, r3)     // Catch:{ all -> 0x0435 }
            java.lang.String r3 = r4.A02     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "fetch_type"
            r5.A09(r2, r3)     // Catch:{ all -> 0x0435 }
            java.lang.String r3 = r0.A09     // Catch:{ all -> 0x0435 }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ all -> 0x0435 }
            if (r2 != 0) goto L_0x013c
            java.lang.String r2 = "categoryId"
            r5.A09(r2, r3)     // Catch:{ all -> 0x0435 }
        L_0x013c:
            java.lang.String r3 = r0.A0D     // Catch:{ all -> 0x0435 }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ all -> 0x0435 }
            if (r2 != 0) goto L_0x014d
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.of(r3)     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "unit_ids"
            r5.A08(r2, r3)     // Catch:{ all -> 0x0435 }
        L_0x014d:
            java.lang.Long r2 = r0.A08     // Catch:{ all -> 0x0435 }
            if (r2 == 0) goto L_0x015a
            java.lang.String r3 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "gameListThreadId"
            r5.A09(r2, r3)     // Catch:{ all -> 0x0435 }
        L_0x015a:
            java.lang.String r3 = r0.A0A     // Catch:{ all -> 0x0435 }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ all -> 0x0435 }
            if (r2 != 0) goto L_0x0167
            java.lang.String r2 = "gameListQueryText"
            r5.A09(r2, r3)     // Catch:{ all -> 0x0435 }
        L_0x0167:
            java.lang.String r3 = r0.A0B     // Catch:{ all -> 0x0435 }
            boolean r2 = com.google.common.base.Platform.stringIsNullOrEmpty(r3)     // Catch:{ all -> 0x0435 }
            if (r2 != 0) goto L_0x0174
            java.lang.String r2 = "gameListSubUnitId"
            r5.A09(r2, r3)     // Catch:{ all -> 0x0435 }
        L_0x0174:
            X.1Yd r4 = r0.A06     // Catch:{ all -> 0x0435 }
            r2 = 282428460500357(0x100de00100585, double:1.39538199741058E-309)
            boolean r2 = r4.Aem(r2)     // Catch:{ all -> 0x0435 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "separate_ads_fetch_enabled"
            r5.A05(r2, r3)     // Catch:{ all -> 0x0435 }
            X.1cN r5 = X.C26931cN.A00(r5)     // Catch:{ all -> 0x0435 }
            X.0gx r2 = X.C09290gx.NETWORK_ONLY     // Catch:{ all -> 0x0435 }
            r5.A0B(r2)     // Catch:{ all -> 0x0435 }
            r4 = 6
            int r3 = X.AnonymousClass1Y3.AoI     // Catch:{ all -> 0x0435 }
            X.0UN r2 = r0.A00     // Catch:{ all -> 0x0435 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x0435 }
            X.0xx r2 = (X.C16900xx) r2     // Catch:{ all -> 0x0435 }
            com.google.common.collect.ImmutableList r2 = r2.A01()     // Catch:{ all -> 0x0435 }
            r5.A0D(r2)     // Catch:{ all -> 0x0435 }
            com.facebook.graphql.executor.GraphQLResult r18 = X.C25511Zx.A01(r0, r5)     // Catch:{ all -> 0x0435 }
            r2 = r18
            java.lang.Object r2 = r2.A03     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r2 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r2     // Catch:{ all -> 0x0435 }
            java.util.HashSet r9 = new java.util.HashSet     // Catch:{ all -> 0x0435 }
            r9.<init>()     // Catch:{ all -> 0x0435 }
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ all -> 0x0435 }
            r6.<init>()     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = r2.A1n()     // Catch:{ all -> 0x0435 }
            java.lang.Class<X.1ck> r4 = X.C27161ck.class
            r3 = 104993457(0x64212b1, float:3.6501077E-35)
            r2 = 738428414(0x2c0385fe, float:1.86906E-12)
            com.google.common.collect.ImmutableList r7 = r5.A0M(r3, r4, r2)     // Catch:{ all -> 0x0435 }
            X.1Xv r5 = r7.iterator()     // Catch:{ all -> 0x0435 }
        L_0x01cb:
            boolean r2 = r5.hasNext()     // Catch:{ all -> 0x0435 }
            if (r2 == 0) goto L_0x0207
            java.lang.Object r4 = r5.next()     // Catch:{ all -> 0x0435 }
            X.1ck r4 = (X.C27161ck) r4     // Catch:{ all -> 0x0435 }
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitUpdateStatus r3 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitUpdateStatus.UNSET_OR_UNRECOGNIZED_ENUM_VALUE     // Catch:{ all -> 0x0435 }
            r2 = -71302478(0xfffffffffbc002b2, float:-1.9939513E36)
            java.lang.Enum r2 = r4.A0O(r2, r3)     // Catch:{ all -> 0x0435 }
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitUpdateStatus r2 = (com.facebook.graphql.enums.GraphQLMessengerInboxUnitUpdateStatus) r2     // Catch:{ all -> 0x0435 }
            java.lang.String r4 = r4.A0U()     // Catch:{ all -> 0x0435 }
            if (r4 != 0) goto L_0x01f8
            int r3 = X.AnonymousClass1Y3.BQ3     // Catch:{ all -> 0x0435 }
            X.0UN r2 = r0.A00     // Catch:{ all -> 0x0435 }
            java.lang.Object r3 = X.AnonymousClass1XX.A03(r3, r2)     // Catch:{ all -> 0x0435 }
            X.0dK r3 = (X.C07380dK) r3     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "inbox_unit_null_id"
            r3.A03(r2)     // Catch:{ all -> 0x0435 }
            goto L_0x01cb
        L_0x01f8:
            int r3 = r2.ordinal()     // Catch:{ all -> 0x0435 }
            r2 = 1
            if (r3 == r2) goto L_0x0203
            r6.add(r4)     // Catch:{ all -> 0x0435 }
            goto L_0x01cb
        L_0x0203:
            r9.add(r4)     // Catch:{ all -> 0x0435 }
            goto L_0x01cb
        L_0x0207:
            X.0go r2 = r0.A04     // Catch:{ all -> 0x0435 }
            android.database.sqlite.SQLiteDatabase r5 = r2.A06()     // Catch:{ all -> 0x0435 }
            r2 = 1688415057(0x64a32b51, float:2.4079524E22)
            X.C007406x.A01(r5, r2)     // Catch:{ all -> 0x0435 }
            X.1a4 r2 = r0.A03     // Catch:{ all -> 0x039d }
            com.google.common.collect.ImmutableMap r8 = r2.A01(r6)     // Catch:{ all -> 0x039d }
            com.google.common.collect.ImmutableList$Builder r11 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x039d }
            X.1Xv r17 = r7.iterator()     // Catch:{ all -> 0x039d }
            r10 = 0
            r7 = 0
            r6 = 1
        L_0x0224:
            boolean r2 = r17.hasNext()     // Catch:{ all -> 0x039d }
            if (r2 == 0) goto L_0x02c3
            java.lang.Object r4 = r17.next()     // Catch:{ all -> 0x039d }
            X.1ck r4 = (X.C27161ck) r4     // Catch:{ all -> 0x039d }
            java.lang.String r15 = r4.A0U()     // Catch:{ all -> 0x039d }
            boolean r2 = r9.contains(r15)     // Catch:{ all -> 0x039d }
            if (r2 == 0) goto L_0x0267
            X.1a4 r3 = r0.A03     // Catch:{ all -> 0x039d }
            com.facebook.graphservice.factory.GraphQLServiceFactory r2 = X.C05850aR.A02()     // Catch:{ all -> 0x039d }
            X.1ck r2 = X.C27161ck.A01(r4, r2)     // Catch:{ all -> 0x039d }
            r3.A03(r2, r7, r6)     // Catch:{ all -> 0x039d }
            com.facebook.graphservice.factory.GraphQLServiceFactory r2 = X.C05850aR.A02()     // Catch:{ all -> 0x039d }
            X.1ck r2 = X.C27161ck.A01(r4, r2)     // Catch:{ all -> 0x039d }
            r11.add(r2)     // Catch:{ all -> 0x039d }
        L_0x0252:
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r3 = r4.A0Q()     // Catch:{ all -> 0x039d }
            if (r6 == 0) goto L_0x025d
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r2 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A0I     // Catch:{ all -> 0x039d }
            if (r3 == r2) goto L_0x025d
            goto L_0x0263
        L_0x025d:
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r2 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A0I     // Catch:{ all -> 0x039d }
            if (r3 != r2) goto L_0x0264
            r6 = 0
            goto L_0x0264
        L_0x0263:
            r10 = 1
        L_0x0264:
            int r7 = r7 + 1
            goto L_0x0224
        L_0x0267:
            boolean r2 = r8.containsKey(r15)     // Catch:{ all -> 0x039d }
            if (r2 == 0) goto L_0x0252
            X.1a4 r12 = r0.A03     // Catch:{ all -> 0x039d }
            X.0W6 r2 = X.C06070an.A01     // Catch:{ all -> 0x039d }
            java.lang.String r2 = r2.A00     // Catch:{ all -> 0x039d }
            X.0av r3 = X.C06160ax.A03(r2, r15)     // Catch:{ all -> 0x039d }
            X.0av r2 = r12.A01     // Catch:{ all -> 0x039d }
            X.0av[] r2 = new X.C06140av[]{r3, r2}     // Catch:{ all -> 0x039d }
            X.1a6 r16 = X.C06160ax.A01(r2)     // Catch:{ all -> 0x039d }
            android.content.ContentValues r14 = new android.content.ContentValues     // Catch:{ all -> 0x039d }
            r14.<init>()     // Catch:{ all -> 0x039d }
            X.0W6 r2 = X.C06070an.A02     // Catch:{ all -> 0x039d }
            java.lang.String r3 = r2.A00     // Catch:{ all -> 0x039d }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x039d }
            r14.put(r3, r2)     // Catch:{ all -> 0x039d }
            X.0W6 r2 = X.C06070an.A00     // Catch:{ all -> 0x039d }
            java.lang.String r3 = r2.A00     // Catch:{ all -> 0x039d }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x039d }
            r14.put(r3, r2)     // Catch:{ all -> 0x039d }
            X.0go r2 = r12.A02     // Catch:{ all -> 0x039d }
            android.database.sqlite.SQLiteDatabase r13 = r2.A06()     // Catch:{ all -> 0x039d }
            java.lang.String r12 = r16.A02()     // Catch:{ all -> 0x039d }
            java.lang.String[] r3 = r16.A04()     // Catch:{ all -> 0x039d }
            java.lang.String r2 = "units"
            r13.update(r2, r14, r12, r3)     // Catch:{ all -> 0x039d }
            java.lang.Object r2 = r8.get(r15)     // Catch:{ all -> 0x039d }
            X.Egy r2 = (X.C29722Egy) r2     // Catch:{ all -> 0x039d }
            X.1ck r3 = r2.A01     // Catch:{ all -> 0x039d }
            com.facebook.graphservice.factory.GraphQLServiceFactory r2 = X.C05850aR.A02()     // Catch:{ all -> 0x039d }
            X.1ck r2 = X.C27161ck.A01(r3, r2)     // Catch:{ all -> 0x039d }
            r11.add(r2)     // Catch:{ all -> 0x039d }
            goto L_0x0252
        L_0x02c3:
            X.3zS r7 = new X.3zS     // Catch:{ all -> 0x039d }
            com.google.common.collect.ImmutableList r2 = r11.build()     // Catch:{ all -> 0x039d }
            r7.<init>(r2, r10)     // Catch:{ all -> 0x039d }
            java.util.Set r2 = r19.keySet()     // Catch:{ all -> 0x039d }
            java.util.Iterator r10 = r2.iterator()     // Catch:{ all -> 0x039d }
        L_0x02d4:
            boolean r2 = r10.hasNext()     // Catch:{ all -> 0x039d }
            if (r2 == 0) goto L_0x0314
            java.lang.Object r3 = r10.next()     // Catch:{ all -> 0x039d }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x039d }
            boolean r2 = r8.containsKey(r3)     // Catch:{ all -> 0x039d }
            if (r2 != 0) goto L_0x02d4
            boolean r2 = r9.contains(r3)     // Catch:{ all -> 0x039d }
            if (r2 != 0) goto L_0x02d4
            X.1a4 r4 = r0.A03     // Catch:{ all -> 0x039d }
            X.0W6 r2 = X.C06070an.A01     // Catch:{ all -> 0x039d }
            java.lang.String r2 = r2.A00     // Catch:{ all -> 0x039d }
            X.0av r3 = X.C06160ax.A03(r2, r3)     // Catch:{ all -> 0x039d }
            X.0av r2 = r4.A01     // Catch:{ all -> 0x039d }
            X.0av[] r2 = new X.C06140av[]{r3, r2}     // Catch:{ all -> 0x039d }
            X.1a6 r3 = X.C06160ax.A01(r2)     // Catch:{ all -> 0x039d }
            X.0go r2 = r4.A02     // Catch:{ all -> 0x039d }
            android.database.sqlite.SQLiteDatabase r6 = r2.A06()     // Catch:{ all -> 0x039d }
            java.lang.String r4 = r3.A02()     // Catch:{ all -> 0x039d }
            java.lang.String[] r3 = r3.A04()     // Catch:{ all -> 0x039d }
            java.lang.String r2 = "units"
            r6.delete(r2, r4, r3)     // Catch:{ all -> 0x039d }
            goto L_0x02d4
        L_0x0314:
            X.0ao r6 = r0.A05     // Catch:{ all -> 0x039d }
            X.0a7 r3 = r0.A02     // Catch:{ all -> 0x039d }
            X.0nH r4 = new X.0nH     // Catch:{ all -> 0x039d }
            java.lang.String r2 = "last_successful_fetch_ms"
            java.lang.String r2 = X.C05660a7.A00(r3, r2)     // Catch:{ all -> 0x039d }
            r4.<init>(r2)     // Catch:{ all -> 0x039d }
            r2 = r18
            long r2 = r2.A00     // Catch:{ all -> 0x039d }
            r6.A05(r4, r2)     // Catch:{ all -> 0x039d }
            X.0ao r4 = r0.A05     // Catch:{ all -> 0x039d }
            X.0a7 r2 = r0.A02     // Catch:{ all -> 0x039d }
            X.0nH r3 = r2.A01()     // Catch:{ all -> 0x039d }
            r2 = 0
            r4.A07(r3, r2)     // Catch:{ all -> 0x039d }
            r5.setTransactionSuccessful()     // Catch:{ all -> 0x039d }
            r2 = 466624570(0x1bd0203a, float:3.443154E-22)
            X.C007406x.A02(r5, r2)     // Catch:{ all -> 0x0435 }
            boolean r4 = r7.A01     // Catch:{ all -> 0x0435 }
            com.facebook.prefs.shared.FbSharedPreferences r2 = r0.A07     // Catch:{ all -> 0x0435 }
            X.1hn r3 = r2.edit()     // Catch:{ all -> 0x0435 }
            X.1Y7 r2 = X.C05690aA.A0c     // Catch:{ all -> 0x0435 }
            r3.putBoolean(r2, r4)     // Catch:{ all -> 0x0435 }
            r3.commit()     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()     // Catch:{ all -> 0x0435 }
            java.lang.Class<com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000> r4 = com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000.class
            java.lang.String r3 = "Viewer"
            r2 = 1649765866(0x62556dea, float:9.8426916E20)
            X.11R r6 = r5.newTreeBuilder(r3, r4, r2)     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r6 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r6     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.factory.GraphQLServiceFactory r5 = X.C05850aR.A02()     // Catch:{ all -> 0x0435 }
            java.lang.String r3 = "MessengerInboxUnitsConnection"
            r2 = 921501124(0x36ecfdc4, float:7.0628903E-6)
            X.11R r4 = r5.newTreeBuilder(r3, r4, r2)     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000 r4 = (com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000) r4     // Catch:{ all -> 0x0435 }
            com.google.common.collect.ImmutableList r3 = r7.A00     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "nodes"
            r4.setTreeList(r2, r3)     // Catch:{ all -> 0x0435 }
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 921501124(0x36ecfdc4, float:7.0628903E-6)
            com.facebook.graphservice.interfaces.Tree r3 = r4.getResult(r3, r2)     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r3 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r3     // Catch:{ all -> 0x0435 }
            java.lang.String r2 = "messenger_inbox_units"
            r6.setTree(r2, r3)     // Catch:{ all -> 0x0435 }
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r3 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r2 = 1649765866(0x62556dea, float:9.8426916E20)
            com.facebook.graphservice.interfaces.Tree r4 = r6.getResult(r3, r2)     // Catch:{ all -> 0x0435 }
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r4 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r4     // Catch:{ all -> 0x0435 }
            X.102 r3 = X.AnonymousClass102.FROM_SERVER     // Catch:{ all -> 0x0435 }
            X.1a7 r2 = X.C25611a7.ALL     // Catch:{ all -> 0x0435 }
            X.100 r3 = X.C25511Zx.A04(r0, r3, r2, r4)     // Catch:{ all -> 0x0435 }
            r0 = -182080894(0xfffffffff525aa82, float:-2.1000666E32)
            goto L_0x0407
        L_0x039d:
            r2 = move-exception
            r0 = 1155550759(0x44e04e27, float:1794.4423)
            X.C007406x.A02(r5, r0)     // Catch:{ all -> 0x0435 }
            throw r2     // Catch:{ all -> 0x0435 }
        L_0x03a5:
            r0 = move-exception
            r8.close()     // Catch:{ all -> 0x0435 }
            goto L_0x03af
        L_0x03aa:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0435 }
            r0.<init>()     // Catch:{ all -> 0x0435 }
        L_0x03af:
            throw r0     // Catch:{ all -> 0x0435 }
        L_0x03b0:
            X.100 r3 = X.C25511Zx.A05(r0, r4)     // Catch:{ all -> 0x0435 }
            r0 = 1032304785(0x3d87b891, float:0.06627)
            goto L_0x0407
        L_0x03b8:
            X.1cR r3 = X.C25511Zx.A02(r0, r4)     // Catch:{ all -> 0x0435 }
            X.1cR r2 = X.C26971cR.UP_TO_DATE     // Catch:{ all -> 0x0435 }
            if (r3 != r2) goto L_0x03cc
            X.102 r3 = X.AnonymousClass102.FROM_CACHE_UP_TO_DATE     // Catch:{ all -> 0x0435 }
            X.1a7 r2 = r4.A01     // Catch:{ all -> 0x0435 }
            X.100 r3 = X.C25511Zx.A03(r0, r3, r2)     // Catch:{ all -> 0x0435 }
            r0 = 2048037475(0x7a129263, float:1.902611E35)
            goto L_0x0407
        L_0x03cc:
            X.1cR r2 = X.C26971cR.NO_DATA     // Catch:{ all -> 0x0435 }
            if (r3 == r2) goto L_0x03dc
            X.102 r3 = X.AnonymousClass102.FROM_CACHE_STALE     // Catch:{ all -> 0x0435 }
            X.1a7 r2 = r4.A01     // Catch:{ all -> 0x0435 }
            X.100 r3 = X.C25511Zx.A03(r0, r3, r2)     // Catch:{ all -> 0x0435 }
            r0 = -1437046714(0xffffffffaa586846, float:-1.9220831E-13)
            goto L_0x0407
        L_0x03dc:
            X.0hU r3 = r4.A00     // Catch:{ all -> 0x0435 }
            X.0hU r2 = X.C09510hU.DO_NOT_CHECK_SERVER     // Catch:{ all -> 0x0435 }
            if (r3 == r2) goto L_0x03ea
            X.100 r3 = X.C25511Zx.A05(r0, r4)     // Catch:{ all -> 0x0435 }
            r0 = -1021073696(0xffffffffc323a6e0, float:-163.65186)
            goto L_0x0407
        L_0x03ea:
            X.100 r3 = new X.100     // Catch:{ all -> 0x0435 }
            X.101 r5 = new X.101     // Catch:{ all -> 0x0435 }
            X.102 r6 = X.AnonymousClass102.NO_DATA     // Catch:{ all -> 0x0435 }
            X.1a7 r7 = r4.A01     // Catch:{ all -> 0x0435 }
            X.06B r2 = r0.A01     // Catch:{ all -> 0x0435 }
            long r8 = r2.now()     // Catch:{ all -> 0x0435 }
            com.google.common.collect.ImmutableList r10 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x0435 }
            r5.<init>(r6, r7, r8, r10)     // Catch:{ all -> 0x0435 }
            r3.<init>(r0, r5)     // Catch:{ all -> 0x0435 }
            r0 = -1927924780(0xffffffff8d1633d4, float:-4.6284705E-31)
            X.C005505z.A00(r0)     // Catch:{ Exception -> 0x043d }
            goto L_0x040a
        L_0x0407:
            X.C005505z.A00(r0)     // Catch:{ Exception -> 0x043d }
        L_0x040a:
            X.0Zh r5 = r1.A01     // Catch:{ Exception -> 0x043d }
            monitor-enter(r5)     // Catch:{ Exception -> 0x043d }
            X.0Zh r4 = r1.A01     // Catch:{ all -> 0x0432 }
            r4.A01 = r3     // Catch:{ all -> 0x0432 }
            r0 = r20
            r4.A00 = r0     // Catch:{ all -> 0x0432 }
            r0 = 0
            r4.A03 = r0     // Catch:{ all -> 0x0432 }
            r2 = 1
            monitor-enter(r4)     // Catch:{ all -> 0x0432 }
            X.0c5 r0 = r4.A04     // Catch:{ all -> 0x042f }
            boolean r0 = r0.A02()     // Catch:{ all -> 0x042f }
            if (r0 != 0) goto L_0x0427
            X.0c5 r0 = r4.A04     // Catch:{ all -> 0x042f }
            r0.A00()     // Catch:{ all -> 0x042f }
        L_0x0427:
            monitor-exit(r4)     // Catch:{ all -> 0x0432 }
            X.0Zh r0 = r1.A01     // Catch:{ all -> 0x0432 }
            X.C05530Zh.A01(r0, r2)     // Catch:{ all -> 0x0432 }
            monitor-exit(r5)     // Catch:{ all -> 0x0432 }
            return r3
        L_0x042f:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0432 }
            throw r0     // Catch:{ all -> 0x0432 }
        L_0x0432:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0432 }
            throw r0     // Catch:{ Exception -> 0x043d }
        L_0x0435:
            r2 = move-exception
            r0 = -735096753(0xffffffffd42f504f, float:-3.01186653E12)
            X.C005505z.A00(r0)     // Catch:{ Exception -> 0x043d }
            throw r2     // Catch:{ Exception -> 0x043d }
        L_0x043d:
            r3 = move-exception
            X.0Zh r2 = r1.A01
            monitor-enter(r2)
            X.0Zh r1 = r1.A01     // Catch:{ all -> 0x0449 }
            r0 = r20
            r1.A00 = r0     // Catch:{ all -> 0x0449 }
            monitor-exit(r2)     // Catch:{ all -> 0x0449 }
            throw r3
        L_0x0449:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0449 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09520hV.call():java.lang.Object");
    }
}
