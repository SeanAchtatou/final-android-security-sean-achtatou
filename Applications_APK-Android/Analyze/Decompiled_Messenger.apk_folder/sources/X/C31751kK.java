package X;

import android.app.Activity;
import android.content.Context;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1kK  reason: invalid class name and case insensitive filesystem */
public final class C31751kK {
    private static volatile C31751kK A0B;
    public Activity A00;
    public Context A01;
    public C13060qW A02;
    public AnonymousClass0UN A03;
    public boolean A04;
    private C71033bi A05;
    private boolean A06;
    public final C04310Tq A07;
    public final C04310Tq A08;
    private final C31761kL A09;
    public volatile AnonymousClass46T A0A;

    private C71033bi A00() {
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(8, AnonymousClass1Y3.APr, this.A03)).AOz();
        if (this.A05 == null) {
            this.A05 = new C71033bi(this.A09, new C71013bg(this));
        }
        return this.A05;
    }

    public static final C31751kK A01(AnonymousClass1XY r4) {
        if (A0B == null) {
            synchronized (C31751kK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0B, r4);
                if (A002 != null) {
                    try {
                        A0B = new C31751kK(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0B;
    }

    public static void A02(C31751kK r5) {
        String str;
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(8, AnonymousClass1Y3.APr, r5.A03)).AOz();
        if (!r5.A06) {
            str = "maybeDisableDetector: already disabled";
        } else if (r5.A00 == null && r5.A01 == null) {
            r5.A06 = false;
            C71033bi A002 = r5.A00();
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.APr, A002.A01)).AOz();
            if (A002.A02) {
                A002.A02 = false;
                C010708t.A0J(AnonymousClass24B.$const$string(155), "stopReceiving");
                C71033bi.A00(A002, true);
                str = "Disabled";
            } else {
                throw new IllegalStateException("Already disabled");
            }
        } else {
            str = "maybeDisableDetector: context present";
        }
        C010708t.A0J("RageShakeDetector", str);
    }

    public static void A03(C31751kK r9) {
        String str;
        int i = AnonymousClass1Y3.APr;
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(8, i, r9.A03)).AOz();
        if (r9.A06) {
            str = "maybeEnableDetector: already enabled";
        } else if (!((Boolean) r9.A07.get()).booleanValue()) {
            str = "maybeEnableDetector: RageShake not available";
        } else if (r9.A00 == null && r9.A01 == null) {
            str = "maybeEnableDetector: no context";
        } else {
            r9.A06 = true;
            C71033bi A002 = r9.A00();
            ((AnonymousClass1Y6) AnonymousClass1XX.A02(1, i, A002.A01)).AOz();
            if (!A002.A02) {
                A002.A02 = true;
                C010708t.A0J(AnonymousClass24B.$const$string(155), "startReceiving");
                C71033bi.A00(A002, false);
                C010708t.A0J("RageShakeDetector", "Enabled");
                USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ap7, r9.A03)).A01("bug_report_rageshake_status"), 108);
                if (uSLEBaseShape0S0000000.A0G()) {
                    long At2 = ((FbSharedPreferences) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B6q, r9.A03)).At2(C71053bk.A05, 0);
                    boolean z = true;
                    if (At2 != 0 && ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r9.A03)).now() - At2 < 86400000) {
                        z = false;
                    }
                    if (z) {
                        String str2 = "default";
                        switch (((FbSharedPreferences) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B6q, r9.A03)).Aeq(C71053bk.A01).ordinal()) {
                            case 0:
                                str2 = "override_enabled";
                                break;
                            case 1:
                                str2 = "override_disabled";
                                break;
                        }
                        uSLEBaseShape0S0000000.A0D("status", str2);
                        uSLEBaseShape0S0000000.A06();
                        int i2 = AnonymousClass1Y3.B6q;
                        AnonymousClass0UN r4 = r9.A03;
                        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(6, i2, r4)).edit();
                        edit.BzA(C71053bk.A05, ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r4)).now());
                        edit.commit();
                        return;
                    }
                    return;
                }
                return;
            }
            throw new IllegalStateException("Already enabled");
        }
        C010708t.A0J("RageShakeDetector", str);
    }

    private C31751kK(AnonymousClass1XY r3) {
        this.A03 = new AnonymousClass0UN(9, r3);
        this.A08 = AnonymousClass0XJ.A0K(r3);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.AxP, r3);
        this.A09 = new C31761kL(r3);
    }
}
