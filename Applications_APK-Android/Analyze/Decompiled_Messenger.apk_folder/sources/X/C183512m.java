package X;

import java.io.Serializable;
import java.lang.reflect.Member;

/* renamed from: X.12m  reason: invalid class name and case insensitive filesystem */
public abstract class C183512m extends C10080jW implements Serializable {
    private static final long serialVersionUID = 7364428299211355871L;
    public final transient C10090jX _annotations;

    public abstract Class getDeclaringClass();

    public abstract Member getMember();

    public abstract Object getValue(Object obj);

    public abstract void setValue(Object obj, Object obj2);

    public C183512m(C10090jX r1) {
        this._annotations = r1;
    }
}
