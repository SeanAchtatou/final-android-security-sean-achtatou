package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.fragment.app.FragmentActivity;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.util.TriState;
import com.facebook.content.SecureContextHelper;
import com.facebook.dialtone.activity.DialtoneUnsupportedCarrierInterstitialActivity;
import com.facebook.dialtone.activity.DialtoneWifiInterstitialActivity;
import com.facebook.dialtone.whitelist.DialtoneWhitelistRegexes;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import io.card.payment.BuildConfig;
import java.util.Iterator;
import java.util.regex.Pattern;

/* renamed from: X.1ES  reason: invalid class name */
public abstract class AnonymousClass1ES {
    public Activity A07() {
        if (!(this instanceof C28161eM)) {
            return null;
        }
        Optional optional = ((C28161eM) this).A00;
        if (!optional.isPresent()) {
            return null;
        }
        return ((C14710tr) optional.get()).A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (r1.startsWith("mqtt-mini") == false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Boolean A08() {
        /*
            r2 = this;
            boolean r0 = r2 instanceof X.C14730tt
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r0 = r2
            X.0tt r0 = (X.C14730tt) r0
            X.13w r0 = r0.A02
            X.07i r0 = r0.A04
            java.lang.String r1 = r0.A0R
            if (r1 == 0) goto L_0x001a
            java.lang.String r0 = "mqtt-mini"
            boolean r1 = r1.startsWith(r0)
            r0 = 1
            if (r1 != 0) goto L_0x001b
        L_0x001a:
            r0 = 0
        L_0x001b:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ES.A08():java.lang.Boolean");
    }

    public String A09() {
        return !(this instanceof C28161eM) ? BuildConfig.FLAVOR : ((AnonymousClass155) ((C28161eM) this).A0L.get()).A0A(AnonymousClass157.NORMAL);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A0A() {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C28161eM
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r4 = r5
            X.1eM r4 = (X.C28161eM) r4
            X.1Yd r3 = r4.A0M
            r1 = 844777117450290(0x3005200000032, double:4.17375352124984E-309)
            java.lang.String r0 = "FETCH_DID_NOT_RETURN_RESULTS"
            java.lang.String r1 = r3.B4E(r1, r0)
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0038
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ Exception -> 0x0026 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0026 }
            java.util.ArrayList r0 = com.facebook.common.util.JSONUtil.A0R(r0)     // Catch:{ Exception -> 0x0026 }
            goto L_0x0039
        L_0x0026:
            r3 = move-exception
            X.0US r0 = r4.A0C
            java.lang.Object r2 = r0.get()
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = r3.getMessage()
            java.lang.String r0 = "getBlacklistedIntents"
            r2.softReport(r0, r1, r3)
        L_0x0038:
            r0 = 0
        L_0x0039:
            if (r0 != 0) goto L_0x003d
            java.util.List r0 = X.C28161eM.A0P
        L_0x003d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ES.A0A():java.util.List");
    }

    public void A0B() {
        if (this instanceof C28161eM) {
            C28161eM r4 = (C28161eM) this;
            AnonymousClass1Y8 r1 = AnonymousClass158.A09;
            int AqN = ((FbSharedPreferences) r4.A0D.get()).AqN(r1, 0);
            C30281hn edit = ((FbSharedPreferences) r4.A0D.get()).edit();
            edit.Bz6(r1, AqN + 1);
            edit.commit();
        }
    }

    public void A0C() {
        if (this instanceof C28161eM) {
            C28161eM r1 = (C28161eM) this;
            if (r1.A0L()) {
                r1.A0f(true);
            }
        }
    }

    public void A0D() {
        if (this instanceof C28161eM) {
            C28161eM r4 = (C28161eM) this;
            Optional optional = r4.A00;
            if (optional.isPresent() && !((C14710tr) optional.get()).A01()) {
                Activity A07 = r4.A07();
                if (A07 == null) {
                    ((AnonymousClass09P) r4.A0B.get()).CGS("dialtone", "currentAcitvity is null");
                    return;
                }
                Intent intent = new Intent(A07, DialtoneWifiInterstitialActivity.class);
                intent.addFlags(67108864);
                ((SecureContextHelper) r4.A0H.get()).CGs(intent, AnonymousClass1Y3.A87, A07);
            }
        }
    }

    public void A0E(C14710tr r3) {
        if (this instanceof C28161eM) {
            ((C28161eM) this).A00 = Optional.of(r3);
        }
    }

    public void A0F(C14250su r3) {
        if (this instanceof C28161eM) {
            C28161eM r1 = (C28161eM) this;
            synchronized (r1) {
                if (r3 != null) {
                    C28161eM.A02(r1).add(r3);
                }
            }
        }
    }

    public void A0G(C14250su r3) {
        if (this instanceof C28161eM) {
            C28161eM r1 = (C28161eM) this;
            synchronized (r1) {
                if (r3 != null) {
                    C28161eM.A02(r1).remove(r3);
                }
            }
        }
    }

    public void A0H(Integer num) {
        String str;
        if (this instanceof C28161eM) {
            C28161eM r4 = (C28161eM) this;
            if (!((C05720aD) r4.A0K.get()).A07("autoflex_placeholder")) {
                Activity A07 = r4.A07();
                if (A07 == null || !(A07 instanceof FragmentActivity)) {
                    str = "flex_photo_upgrade_no_fragment";
                } else {
                    C13060qW B4m = ((FragmentActivity) A07).B4m();
                    if (r4.A0h(num)) {
                        str = "flex_photo_upgrade_without_interstitial";
                    } else {
                        String A0c = r4.A0c(num);
                        r4.A0d(A0c, A07);
                        ((C414325l) r4.A0I.get()).A0B(A0c, B4m, null);
                        r4.A0e(A0c, !AnonymousClass5VN.A00(num));
                        return;
                    }
                }
                r4.A0X(str, true);
            }
        }
    }

    public void A0I(String str) {
        if (this instanceof C28161eM) {
            ((C04460Ut) ((C28161eM) this).A0A.get()).C4x(new Intent(AnonymousClass80H.$const$string(57)).putExtra(AnonymousClass80H.$const$string(202), str));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void A0J(String str) {
        if (this instanceof C28161eM) {
            C28161eM r6 = (C28161eM) this;
            Optional optional = r6.A00;
            if (optional.isPresent() && !((C14710tr) optional.get()).A01()) {
                Activity A07 = r6.A07();
                if (A07 == null) {
                    ((AnonymousClass09P) r6.A0B.get()).CGS("dialtone", "currentAcitvity is null");
                    return;
                }
                Intent intent = new Intent(A07, DialtoneUnsupportedCarrierInterstitialActivity.class);
                intent.addFlags(67108864);
                char c = 65535;
                int hashCode = str.hashCode();
                if (hashCode != -843027230) {
                    if (hashCode != 862561006) {
                        if (hashCode == 1014790088 && str.equals("unsupported_category")) {
                            c = 1;
                        }
                    } else if (str.equals("unsupported_carrier")) {
                        c = 0;
                    }
                } else if (str.equals("not_in_region")) {
                    c = 2;
                }
                if (c == 0 || c == 1) {
                    intent.putExtra("dialtone_wrong_carrier_flag", true);
                } else if (c != 2) {
                    intent.putExtra("dialtone_wrong_carrier_flag", false);
                } else {
                    intent.putExtra("dialtone_not_in_region_flag", true);
                }
                ((SecureContextHelper) r6.A0H.get()).CGs(intent, AnonymousClass1Y3.A87, A07);
            }
        }
    }

    public boolean A0K() {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        return ((TriState) ((C28161eM) this).A0O.get()).asBoolean(true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        if (r1 != false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0L() {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C28161eM
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r4 = r5
            X.1eM r4 = (X.C28161eM) r4
            X.0XN r0 = r4.A06
            boolean r0 = r0.A0I()
            r3 = 0
            if (r0 != 0) goto L_0x0021
            X.1eN r2 = r4.A0N
            X.9M6 r0 = X.AnonymousClass9M6.A08
            r1 = 1
            int r0 = r2.A03(r0, r1)
            if (r0 == r1) goto L_0x001e
            r1 = 0
        L_0x001e:
            if (r1 == 0) goto L_0x0021
        L_0x0020:
            return r3
        L_0x0021:
            boolean r0 = r4.A0K()
            if (r0 == 0) goto L_0x0020
            X.0Z9 r0 = r4.A07
            boolean r0 = r0.A06()
            if (r0 == 0) goto L_0x0031
            r3 = 1
            return r3
        L_0x0031:
            boolean r3 = r4.A0g()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ES.A0L():boolean");
    }

    public boolean A0M() {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        C28161eM r1 = (C28161eM) this;
        if (!r1.A0K()) {
            return false;
        }
        return r1.A07.A06();
    }

    public boolean A0N() {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        return ((C05720aD) ((C28161eM) this).A0K.get()).A08("dialtone_toggle_interstitial");
    }

    public boolean A0O() {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        return ((C05720aD) ((C28161eM) this).A0K.get()).A08("flex_plus");
    }

    public boolean A0P() {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        return ((C05720aD) ((C28161eM) this).A0K.get()).A08("photo_dialtone");
    }

    public boolean A0Q() {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        return ((C05720aD) ((C28161eM) this).A0K.get()).A08("video_screencap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f9, code lost:
        if (X.AnonymousClass80H.$const$string(X.AnonymousClass1Y3.A3m).equals(r10.getStringExtra("key_uri")) != false) goto L_0x00fb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0R(android.content.Context r9, android.content.Intent r10) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.C28161eM
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r2 = r8
            X.1eM r2 = (X.C28161eM) r2
            android.net.Uri r0 = r10.getData()
            if (r0 == 0) goto L_0x012f
            java.lang.String r5 = r0.getHost()
        L_0x0013:
            r7 = 0
            r6 = 1
            java.lang.String r3 = "ref"
            if (r5 == 0) goto L_0x0021
            java.lang.String r0 = "start"
            boolean r0 = r5.equals(r0)
            if (r0 != 0) goto L_0x003b
        L_0x0021:
            java.lang.String r1 = "dialtone://start"
            boolean r0 = r10.hasExtra(r1)
            if (r0 != 0) goto L_0x003b
            java.lang.String r4 = "key_uri"
            java.lang.String r0 = r10.getStringExtra(r4)
            if (r0 == 0) goto L_0x0081
            java.lang.String r0 = r10.getStringExtra(r4)
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x0081
        L_0x003b:
            java.lang.String r3 = r10.getStringExtra(r3)
            boolean r0 = r2.A0L()
            if (r0 == 0) goto L_0x0057
            com.google.common.base.Optional r1 = r2.A00
            boolean r0 = r1.isPresent()
            if (r0 == 0) goto L_0x005e
            java.lang.Object r0 = r1.get()
            X.0tr r0 = (X.C14710tr) r0
            boolean r0 = r0.A05
            if (r0 == 0) goto L_0x005e
        L_0x0057:
            r2.A0Y(r3, r6)
            r2.A0b(r9)
        L_0x005d:
            return r6
        L_0x005e:
            boolean r0 = r1.isPresent()
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = X.C27881du.A0M
            android.net.Uri r3 = android.net.Uri.parse(r0)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r0 = "android.intent.action.VIEW"
            r1.<init>(r0)
            android.content.Intent r1 = r1.setData(r3)
            X.0US r0 = r2.A0H
            java.lang.Object r0 = r0.get()
            com.facebook.content.SecureContextHelper r0 = (com.facebook.content.SecureContextHelper) r0
            r0.startFacebookActivity(r1, r9)
            return r6
        L_0x0081:
            java.lang.String r1 = "switch_to_dialtone"
            if (r5 == 0) goto L_0x008b
            boolean r0 = r5.equals(r1)
            if (r0 != 0) goto L_0x00a7
        L_0x008b:
            boolean r0 = r10.hasExtra(r1)
            if (r0 != 0) goto L_0x00a7
            java.lang.String r0 = r10.getStringExtra(r4)
            if (r0 == 0) goto L_0x00bf
            java.lang.String r1 = r10.getStringExtra(r4)
            r0 = 456(0x1c8, float:6.39E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            boolean r0 = r1.startsWith(r0)
            if (r0 == 0) goto L_0x00bf
        L_0x00a7:
            java.lang.String r1 = r10.getStringExtra(r3)
            r2.A0Y(r1, r6)
            r2.A0b(r9)
            X.0US r0 = r2.A0F
            java.lang.Object r1 = r0.get()
            X.0Ut r1 = (X.C04460Ut) r1
            java.lang.String r0 = "com.facebook.zero.ACTION_ZERO_INTERSTITIAL_REFRESH"
            r1.C4y(r0)
            return r6
        L_0x00bf:
            java.lang.String r1 = "open_zb_portal"
            if (r5 == 0) goto L_0x00c9
            boolean r0 = r5.equals(r1)
            if (r0 != 0) goto L_0x0110
        L_0x00c9:
            boolean r0 = r10.hasExtra(r1)
            if (r0 != 0) goto L_0x0110
            java.lang.String r1 = r10.getStringExtra(r4)
            java.lang.String r0 = "dialtone://open_zb_portal"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0110
            java.lang.String r1 = "switch_to_full_fb"
            if (r5 == 0) goto L_0x00e5
            boolean r0 = r5.equals(r1)
            if (r0 != 0) goto L_0x00fb
        L_0x00e5:
            boolean r0 = r10.hasExtra(r1)
            if (r0 != 0) goto L_0x00fb
            java.lang.String r1 = r10.getStringExtra(r4)
            r0 = 457(0x1c9, float:6.4E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x010f
        L_0x00fb:
            java.lang.String r1 = r10.getStringExtra(r3)
            r2.A0X(r1, r6)
            if (r1 == 0) goto L_0x010f
            java.lang.String r0 = "dialtone_settings_screen"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x010f
            r2.A0b(r9)
        L_0x010f:
            return r7
        L_0x0110:
            android.app.Activity r0 = r2.A07()
            if (r0 != 0) goto L_0x0132
            com.google.common.base.Optional r1 = r2.A00
            boolean r0 = r1.isPresent()
            if (r0 == 0) goto L_0x005d
            java.lang.Object r0 = r1.get()
            X.0tr r0 = (X.C14710tr) r0
            X.5VO r1 = new X.5VO
            r1.<init>(r2)
            java.util.Set r0 = r0.A03
            r0.add(r1)
            return r6
        L_0x012f:
            r5 = 0
            goto L_0x0013
        L_0x0132:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r2.A0H(r0)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ES.A0R(android.content.Context, android.content.Intent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0058, code lost:
        if (r0 == false) goto L_0x005b;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0S(android.net.Uri r14, com.facebook.common.callercontext.CallerContext r15) {
        /*
            r13 = this;
            boolean r0 = r13 instanceof X.C28161eM
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r6 = r13
            X.1eM r6 = (X.C28161eM) r6
            X.1ET r5 = r6.A08
            r3 = 0
            if (r14 == 0) goto L_0x005b
            java.lang.String r2 = r14.toString()
            int r1 = X.AnonymousClass1Y3.AX5
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            com.facebook.dialtone.whitelist.DialtoneWhitelistRegexes r1 = (com.facebook.dialtone.whitelist.DialtoneWhitelistRegexes) r1
            X.9TR r0 = X.AnonymousClass9TR.URI
            com.google.common.collect.ImmutableSet r0 = r1.A02(r0)
            boolean r0 = X.AnonymousClass1ET.A01(r2, r0)
            if (r0 != 0) goto L_0x005a
            java.lang.String r0 = "efg"
            java.lang.String r0 = r14.getQueryParameter(r0)     // Catch:{ UnsupportedOperationException -> 0x0057 }
            if (r0 != 0) goto L_0x0032
            r0 = 0
            goto L_0x0058
        L_0x0032:
            java.lang.String r4 = new java.lang.String     // Catch:{  }
            byte[] r1 = android.util.Base64.decode(r0, r3)     // Catch:{  }
            java.lang.String r0 = "UTF-8"
            r4.<init>(r1, r0)     // Catch:{  }
            r2 = 1
            int r1 = X.AnonymousClass1Y3.AmL     // Catch:{  }
            X.0UN r0 = r5.A00     // Catch:{  }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{  }
            X.0jJ r1 = (X.AnonymousClass0jJ) r1     // Catch:{  }
            java.lang.Class<java.util.Map> r0 = java.util.Map.class
            java.lang.Object r1 = r1.readValue(r4, r0)     // Catch:{  }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{  }
            java.lang.String r0 = "dtw"
            boolean r0 = r1.containsKey(r0)     // Catch:{  }
            goto L_0x0058
        L_0x0057:
            r0 = 0
        L_0x0058:
            if (r0 == 0) goto L_0x005b
        L_0x005a:
            r3 = 1
        L_0x005b:
            if (r3 == 0) goto L_0x0075
            boolean r12 = X.C28161eM.A06(r6, r14)
            if (r12 != 0) goto L_0x0076
            X.0US r0 = r6.A0E
            java.lang.Object r2 = r0.get()
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 854(0x356, float:1.197E-42)
            r0 = 1
            boolean r0 = r2.AbO(r1, r0)
            if (r0 == 0) goto L_0x0076
            r3 = 0
        L_0x0075:
            return r3
        L_0x0076:
            java.lang.Integer r8 = X.AnonymousClass07B.A00
            java.lang.String r9 = r14.toString()
            r11 = 0
            java.lang.String r7 = "dialtone_whitelisted_impression"
            r10 = r15
            X.C28161eM.A04(r6, r7, r8, r9, r10, r11, r12)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ES.A0S(android.net.Uri, com.facebook.common.callercontext.CallerContext):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        if (((X.C05720aD) r5.A0K.get()).A08("video_preview_for_flex_plus") == false) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0T(X.AnonymousClass8ZY r13, com.facebook.common.callercontext.CallerContext r14, android.net.Uri r15) {
        /*
            r12 = this;
            boolean r0 = r12 instanceof X.C28161eM
            if (r0 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r5 = r12
            X.1eM r5 = (X.C28161eM) r5
            boolean r0 = r5.A0O()
            r4 = 1
            r3 = 0
            if (r0 == 0) goto L_0x0033
            java.lang.Integer r0 = r13.A00
            boolean r0 = X.AnonymousClass5VN.A00(r0)
            if (r0 != 0) goto L_0x0033
        L_0x0019:
            if (r4 == 0) goto L_0x0063
            boolean r11 = X.C28161eM.A06(r5, r15)
            if (r11 != 0) goto L_0x0056
            X.0US r0 = r5.A0E
            java.lang.Object r2 = r0.get()
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 854(0x356, float:1.197E-42)
            r0 = 1
            boolean r0 = r2.AbO(r1, r0)
            if (r0 == 0) goto L_0x0056
            return r3
        L_0x0033:
            boolean r0 = r5.A0O()
            if (r0 == 0) goto L_0x004a
            X.0US r0 = r5.A0K
            java.lang.Object r1 = r0.get()
            X.0aD r1 = (X.C05720aD) r1
            java.lang.String r0 = "video_preview_for_flex_plus"
            boolean r1 = r1.A08(r0)
            r0 = 1
            if (r1 != 0) goto L_0x004b
        L_0x004a:
            r0 = 0
        L_0x004b:
            if (r0 == 0) goto L_0x0054
            java.lang.Integer r1 = r13.A00
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            if (r1 != r0) goto L_0x0054
            goto L_0x0019
        L_0x0054:
            r4 = 0
            goto L_0x0019
        L_0x0056:
            java.lang.Integer r7 = X.AnonymousClass07B.A0Y
            if (r15 != 0) goto L_0x0064
            r10 = 0
        L_0x005b:
            java.lang.String r6 = "dialtone_whitelisted_impression"
            java.lang.String r8 = ""
            r9 = r14
            X.C28161eM.A04(r5, r6, r7, r8, r9, r10, r11)
        L_0x0063:
            return r4
        L_0x0064:
            java.lang.String r10 = r15.toString()
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ES.A0T(X.8ZY, com.facebook.common.callercontext.CallerContext, android.net.Uri):boolean");
    }

    public boolean A0U(String str) {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        C28161eM r4 = (C28161eM) this;
        String str2 = str;
        boolean A01 = AnonymousClass1ET.A01(str, ((DialtoneWhitelistRegexes) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AX5, r4.A08.A00)).A02(AnonymousClass9TR.FACEWEB));
        if (((AnonymousClass1YI) r4.A0E.get()).AbO(AnonymousClass1Y3.A6r, true)) {
            return false;
        }
        if (!A01) {
            return A01;
        }
        C28161eM.A04(r4, "dialtone_whitelisted_impression", AnonymousClass07B.A0C, str2, null, null, false);
        return A01;
    }

    public boolean A0V(String str) {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        return AnonymousClass1ET.A01(str, ((DialtoneWhitelistRegexes) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AX5, ((C28161eM) this).A08.A00)).A02(AnonymousClass9TR.VIDEO));
    }

    public boolean A0W(String str, CallerContext callerContext, Uri uri) {
        boolean z;
        boolean z2;
        String str2 = str;
        if (!(this instanceof C28161eM)) {
            return false;
        }
        C28161eM r6 = (C28161eM) this;
        CallerContext callerContext2 = callerContext;
        if (C06850cB.A0B(str)) {
            str2 = callerContext.A02;
        }
        if (!((AnonymousClass1YI) r6.A0E.get()).AbO(AnonymousClass1Y3.A6r, true)) {
            boolean z3 = true;
            if (!((C05720aD) r6.A0K.get()).A08("zero_whitelist_story_rectangular_thumbnail") || !AnonymousClass24B.$const$string(AnonymousClass1Y3.AA9).equals(str2)) {
                if (!AnonymousClass1ET.A01(str2, ((DialtoneWhitelistRegexes) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AX5, r6.A08.A00)).A02(AnonymousClass9TR.PHOTO))) {
                    AnonymousClass1ET r3 = r6.A08;
                    if (str2.contains(C99084oO.$const$string(18))) {
                        ImmutableSet A02 = ((DialtoneWhitelistRegexes) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AX5, r3.A00)).A02(AnonymousClass9TR.PHOTO);
                        if (str2 != null) {
                            Iterator it = A02.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (((Pattern) it.next()).matcher(str2).find()) {
                                        z2 = true;
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                    z2 = false;
                    if (!z2) {
                        z3 = false;
                    }
                }
                z = z3;
            } else {
                z = true;
            }
            if (z3) {
                Uri uri2 = uri;
                boolean A06 = C28161eM.A06(r6, uri2);
                if (A06 || !((AnonymousClass1YI) r6.A0E.get()).AbO(854, true)) {
                    C28161eM.A04(r6, "dialtone_whitelisted_impression", AnonymousClass07B.A01, str2, callerContext2, uri2.toString(), A06);
                }
            }
            return z;
        }
        return false;
    }

    public boolean A0X(String str, boolean z) {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        C28161eM r3 = (C28161eM) this;
        if (!r3.A07.A06()) {
            return false;
        }
        C28161eM.A05(r3, false);
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r3.A05.A01("dialtone_switch_to_paid_mode"), AnonymousClass1Y3.A11);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("carrier_id", r3.A09());
            if (str == null) {
                str = BuildConfig.FLAVOR;
            }
            uSLEBaseShape0S0000000.A0D("event_location", str);
            uSLEBaseShape0S0000000.A08(AnonymousClass80H.$const$string(5), false);
            uSLEBaseShape0S0000000.A06();
        }
        r3.A07.A04();
        C30281hn edit = ((FbSharedPreferences) r3.A0D.get()).edit();
        edit.putBoolean(C05730aE.A0M, false);
        edit.commit();
        r3.A0f(r3.A07.A06());
        return true;
    }

    public boolean A0Y(String str, boolean z) {
        if (!(this instanceof C28161eM)) {
            return false;
        }
        C28161eM r4 = (C28161eM) this;
        if (!r4.A0K()) {
            r4.A07.A04();
        } else if (!r4.A0L()) {
            C28161eM.A05(r4, true);
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r4.A05.A01("dialtone_switch_to_free_mode"), 134);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0D("carrier_id", r4.A09());
                if (str == null) {
                    str = BuildConfig.FLAVOR;
                }
                uSLEBaseShape0S0000000.A0D("event_location", str);
                uSLEBaseShape0S0000000.A08(AnonymousClass80H.$const$string(5), true);
                uSLEBaseShape0S0000000.A06();
            }
            r4.A07.A05();
            C30281hn edit = ((FbSharedPreferences) r4.A0D.get()).edit();
            edit.putBoolean(C05730aE.A0M, true);
            edit.commit();
            r4.A0f(r4.A07.A06());
            return true;
        }
        return false;
    }

    public int A0Z() {
        return 0;
    }

    public Bitmap A0a(float f, float f2, AnonymousClass8ZY r4) {
        return null;
    }

    public void A0b(Context context) {
    }
}
