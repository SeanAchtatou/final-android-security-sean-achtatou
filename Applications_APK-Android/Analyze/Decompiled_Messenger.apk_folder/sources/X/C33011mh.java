package X;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1mh  reason: invalid class name and case insensitive filesystem */
public class C33011mh<E> extends AbstractCollection<E> {
    public final Predicate A00;
    public final Collection A01;

    public boolean add(Object obj) {
        Preconditions.checkArgument(this.A00.apply(obj));
        return this.A01.add(obj);
    }

    public void clear() {
        AnonymousClass0j4.A0B(this.A01, this.A00);
    }

    public boolean contains(Object obj) {
        boolean z;
        Collection collection = this.A01;
        Preconditions.checkNotNull(collection);
        try {
            z = collection.contains(obj);
        } catch (ClassCastException | NullPointerException unused) {
            z = false;
        }
        if (z) {
            return this.A00.apply(obj);
        }
        return false;
    }

    public boolean isEmpty() {
        return !AnonymousClass0j4.A0A(this.A01, this.A00);
    }

    public Iterator iterator() {
        return C24931Xr.A05(this.A01.iterator(), this.A00);
    }

    public boolean removeAll(Collection collection) {
        return AnonymousClass0j4.A0B(this.A01, Predicates.and(this.A00, Predicates.in(collection)));
    }

    public boolean retainAll(Collection collection) {
        return AnonymousClass0j4.A0B(this.A01, Predicates.and(this.A00, new Predicates.NotPredicate(Predicates.in(collection))));
    }

    public C33011mh(Collection collection, Predicate predicate) {
        this.A01 = collection;
        this.A00 = predicate;
    }

    public boolean addAll(Collection collection) {
        for (Object apply : collection) {
            Preconditions.checkArgument(this.A00.apply(apply));
        }
        return this.A01.addAll(collection);
    }

    public boolean containsAll(Collection collection) {
        return AnonymousClass0j4.A09(collection, Predicates.in(this));
    }

    public boolean remove(Object obj) {
        if (!contains(obj) || !this.A01.remove(obj)) {
            return false;
        }
        return true;
    }

    public int size() {
        return C24931Xr.A00(iterator());
    }

    public Object[] toArray() {
        Iterator it = iterator();
        ArrayList A002 = C04300To.A00();
        C24931Xr.A09(A002, it);
        return A002.toArray();
    }

    public Object[] toArray(Object[] objArr) {
        Iterator it = iterator();
        ArrayList A002 = C04300To.A00();
        C24931Xr.A09(A002, it);
        return A002.toArray(objArr);
    }
}
