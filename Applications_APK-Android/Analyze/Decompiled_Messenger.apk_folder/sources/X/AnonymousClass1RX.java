package X;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Layout;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.ui.name.ThreadNameViewData;

/* renamed from: X.1RX  reason: invalid class name */
public final class AnonymousClass1RX extends C17770zR {
    public static final Typeface A09 = Typeface.DEFAULT;
    public static final Layout.Alignment A0A = Layout.Alignment.ALIGN_NORMAL;
    @Comparable(type = 3)
    public int A00 = 1;
    @Comparable(type = 3)
    public int A01;
    @Comparable(type = 3)
    public int A02;
    @Comparable(type = 13)
    public Typeface A03 = A09;
    @Comparable(type = 13)
    public Layout.Alignment A04 = A0A;
    @Comparable(type = 13)
    public C22311Kv A05;
    public AnonymousClass0UN A06;
    @Comparable(type = 13)
    public ThreadNameViewData A07;
    @Comparable(type = 3)
    public boolean A08;

    public AnonymousClass1RX(Context context) {
        super("ThreadNameComponent");
        this.A06 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
