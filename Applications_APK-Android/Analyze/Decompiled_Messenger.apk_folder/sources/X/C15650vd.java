package X;

import android.animation.Animator;

/* renamed from: X.0vd  reason: invalid class name and case insensitive filesystem */
public final class C15650vd implements Animator.AnimatorListener {
    public final /* synthetic */ C15630vb A00;
    public final /* synthetic */ C15160up A01;

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
    }

    public C15650vd(C15160up r1, C15630vb r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void onAnimationRepeat(Animator animator) {
        this.A01.A02(1.0f, this.A00, true);
        C15630vb r3 = this.A00;
        r3.A07 = r3.A04;
        r3.A05 = r3.A01;
        r3.A06 = r3.A03;
        int[] iArr = r3.A0G;
        int length = (r3.A0C + 1) % iArr.length;
        r3.A0C = length;
        r3.A0D = iArr[length];
        C15160up r1 = this.A01;
        if (r1.A01) {
            r1.A01 = false;
            animator.cancel();
            animator.setDuration(1332);
            animator.start();
            C15630vb r12 = this.A00;
            if (r12.A0F) {
                r12.A0F = false;
                return;
            }
            return;
        }
        r1.A00 += 1.0f;
    }

    public void onAnimationStart(Animator animator) {
        this.A01.A00 = 0.0f;
    }
}
