package X;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.common.collect.ImmutableSet;
import java.util.ArrayList;
import java.util.Set;

/* renamed from: X.0hB  reason: invalid class name and case insensitive filesystem */
public final class C09360hB implements AnonymousClass0Z1 {
    private final C09370hC A00;

    public String Amj() {
        return null;
    }

    public static final C09360hB A00(AnonymousClass1XY r4) {
        return new C09360hB(new C09370hC(C04490Ux.A0J(r4), C04490Ux.A0t(r4)));
    }

    public String getCustomData(Throwable th) {
        if (th instanceof OutOfMemoryError) {
            return "n/a";
        }
        try {
            C09370hC r6 = this.A00;
            Set<String> set = AnonymousClass6JN.A00;
            ImmutableSet A04 = ImmutableSet.A04(r6.A01);
            ArrayList<PackageInfo> A002 = C04300To.A00();
            for (String str : set) {
                if (!A04.contains(str)) {
                    try {
                        A002.add(r6.A00.getPackageInfo(str, 0));
                    } catch (PackageManager.NameNotFoundException unused) {
                    }
                }
            }
            if (A002 == null || A002.isEmpty()) {
                return "none";
            }
            StringBuilder sb = new StringBuilder();
            for (PackageInfo packageInfo : A002) {
                sb.append(packageInfo.packageName);
                sb.append("={");
                sb.append(packageInfo.versionCode);
                sb.append(",");
                sb.append(packageInfo.versionName);
                sb.append("}\n");
            }
            return sb.toString();
        } catch (Exception e) {
            throw new C37691w5(e);
        } catch (C37691w5 unused2) {
            return "exception";
        }
    }

    public C09360hB(C09370hC r1) {
        this.A00 = r1;
    }
}
