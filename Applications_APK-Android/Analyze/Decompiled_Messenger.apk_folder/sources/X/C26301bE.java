package X;

/* renamed from: X.1bE  reason: invalid class name and case insensitive filesystem */
public final class C26301bE {
    public final C26311bF A00 = new C26311bF();

    public C26311bF A00() {
        C26311bF r2 = this.A00;
        if (r2.A07) {
            r2.A02 = r2.A03 + r2.A00 + r2.A06;
            r2.A0B = r2.A0A;
        }
        return r2;
    }

    public void A01(int i, int[] iArr, int i2) {
        C26311bF r1 = this.A00;
        r1.A00 = i;
        r1.A0A = iArr;
        r1.A01 = i2;
        r1.A07 = true;
    }

    public void A02(int i, int[] iArr, int i2) {
        C26311bF r1 = this.A00;
        r1.A03 = i;
        r1.A0C = iArr;
        r1.A04 = i2;
        r1.A07 = true;
    }
}
