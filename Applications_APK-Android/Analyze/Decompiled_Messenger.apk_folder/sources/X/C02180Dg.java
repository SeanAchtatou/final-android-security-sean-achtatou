package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.util.ParcelablePair;

/* renamed from: X.0Dg  reason: invalid class name and case insensitive filesystem */
public final class C02180Dg implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ParcelablePair(parcel);
    }

    public Object[] newArray(int i) {
        return new ParcelablePair[i];
    }
}
