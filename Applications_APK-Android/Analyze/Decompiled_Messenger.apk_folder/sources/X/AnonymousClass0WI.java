package X;

import java.io.File;
import java.io.IOException;

/* renamed from: X.0WI  reason: invalid class name */
public final class AnonymousClass0WI {
    private final AnonymousClass0X0 A00;
    private final File A01;
    private final File A02;
    private final File A03;

    public boolean A01(Object obj) {
        try {
            this.A00.CNY(this.A03, obj);
            if ((this.A01.exists() && !this.A01.delete()) || (this.A02.exists() && !this.A02.renameTo(this.A01))) {
                this.A03.delete();
                return false;
            } else if (this.A03.renameTo(this.A02)) {
                return true;
            } else {
                this.A03.delete();
                this.A01.renameTo(this.A02);
                return false;
            }
        } catch (IOException e) {
            C010708t.A0M("AtomicFileHelper", "Cannot write data to file", e);
            this.A03.delete();
            return false;
        }
    }

    public Object A00() {
        if (this.A02.exists()) {
            try {
                return this.A00.BzW(this.A02);
            } catch (IOException e) {
                C010708t.A0M("AtomicFileHelper", "Cannot read file", e);
            }
        }
        if (!this.A01.exists()) {
            return null;
        }
        try {
            return this.A00.BzW(this.A01);
        } catch (IOException e2) {
            C010708t.A0M("AtomicFileHelper", "Cannot read file", e2);
            return null;
        }
    }

    private AnonymousClass0WI(AnonymousClass0X0 r1, File file, File file2, File file3) {
        this.A00 = r1;
        this.A02 = file;
        this.A03 = file2;
        this.A01 = file3;
    }

    public AnonymousClass0WI(AnonymousClass0X0 r5, File file, String str) {
        this(r5, new File(file, str), new File(file, AnonymousClass08S.A0J(str, ".tmp")), new File(file, AnonymousClass08S.A0J(str, ".old")));
    }
}
