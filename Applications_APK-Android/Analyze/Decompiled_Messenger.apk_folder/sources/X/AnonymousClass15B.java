package X;

import android.os.HandlerThread;

/* renamed from: X.15B  reason: invalid class name */
public final class AnonymousClass15B {
    public C1936895x A00;
    public C1936895x A01;
    public final AnonymousClass15I A02;
    public final C07290dB A03;
    public final AnonymousClass15K A04;
    public final C06400bR A05;
    public final C06320bJ A06;

    public AnonymousClass15B(HandlerThread handlerThread, Integer num, C07290dB r5, AnonymousClass15I r6, C06400bR r7, C07250d5 r8, C06320bJ r9) {
        this.A04 = new AnonymousClass15K(this, handlerThread.getLooper(), r8, num);
        this.A03 = r5;
        this.A02 = r6;
        this.A05 = r7;
        this.A06 = r9;
    }
}
