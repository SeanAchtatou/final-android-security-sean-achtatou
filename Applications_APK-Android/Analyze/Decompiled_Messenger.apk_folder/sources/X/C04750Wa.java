package X;

import com.facebook.common.util.TriState;
import com.facebook.inject.InjectorModule;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@InjectorModule
/* renamed from: X.0Wa  reason: invalid class name and case insensitive filesystem */
public final class C04750Wa extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile ExecutorService A01;

    public static final AnonymousClass09P A00() {
        return null;
    }

    public static final TriState A02() {
        return null;
    }

    public static final AnonymousClass0US A03(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.Amr, r1);
    }

    public static final ExecutorService A04(AnonymousClass1XY r11) {
        if (A01 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r11);
                if (A002 != null) {
                    try {
                        r11.getApplicationInjector();
                        A01 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new C04570Vg("ErrorReportingThread-", 10));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C04310Tq A05(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.Amd, r1);
    }

    public static final AnonymousClass09P A01(AnonymousClass1XY r0) {
        return C04920Ww.A00(r0);
    }
}
