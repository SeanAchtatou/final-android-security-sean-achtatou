package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import com.facebook.cameracore.mediapipeline.asyncscripting.AsyncScriptingClient;
import com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService;
import com.facebook.cameracore.mediapipeline.asyncscripting.RemoteHostConnection;

/* renamed from: X.0Kq  reason: invalid class name and case insensitive filesystem */
public final class C03250Kq implements ServiceConnection {
    public final /* synthetic */ AsyncScriptingClient A00;

    public C03250Kq(AsyncScriptingClient asyncScriptingClient) {
        this.A00 = asyncScriptingClient;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        IAsyncScriptingService iAsyncScriptingService;
        synchronized (this.A00.mConnections) {
            AsyncScriptingClient asyncScriptingClient = this.A00;
            if (iBinder == null) {
                iAsyncScriptingService = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.facebook.cameracore.mediapipeline.asyncscripting.IAsyncScriptingService");
                if (queryLocalInterface == null || !(queryLocalInterface instanceof IAsyncScriptingService)) {
                    iAsyncScriptingService = new IAsyncScriptingService.Stub.Proxy(iBinder);
                } else {
                    iAsyncScriptingService = (IAsyncScriptingService) queryLocalInterface;
                }
            }
            asyncScriptingClient.mRemoteService = iAsyncScriptingService;
            for (RemoteHostConnection onServiceConnected : this.A00.mConnections) {
                onServiceConnected.onServiceConnected(this.A00.mRemoteService);
            }
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.A00.mConnections) {
            for (RemoteHostConnection remoteHostConnection : this.A00.mConnections) {
                int A03 = C000700l.A03(352204552);
                synchronized (remoteHostConnection.mLock) {
                    try {
                        remoteHostConnection.mService = null;
                        remoteHostConnection.mVm = null;
                        if (remoteHostConnection.mHybridData.isValid()) {
                            remoteHostConnection.onDisconnected();
                        }
                    } catch (Throwable th) {
                        while (true) {
                            C000700l.A09(327764032, A03);
                            throw th;
                        }
                    }
                }
                C000700l.A09(-475555631, A03);
            }
            this.A00.mRemoteService = null;
        }
    }
}
