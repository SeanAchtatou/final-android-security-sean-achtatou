package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1JZ  reason: invalid class name */
public final class AnonymousClass1JZ extends Enum implements C16930y3 {
    private static final /* synthetic */ AnonymousClass1JZ[] A00;
    public static final AnonymousClass1JZ A01;
    public static final AnonymousClass1JZ A02;
    public static final AnonymousClass1JZ A03;
    public static final AnonymousClass1JZ A04;
    public static final AnonymousClass1JZ A05;
    public static final AnonymousClass1JZ A06;
    public static final AnonymousClass1JZ A07;
    public static final AnonymousClass1JZ A08;
    public static final AnonymousClass1JZ A09;
    public static final AnonymousClass1JZ A0A;
    public static final AnonymousClass1JZ A0B;
    public static final AnonymousClass1JZ A0C;
    public static final AnonymousClass1JZ A0D;
    public static final AnonymousClass1JZ A0E;
    public static final AnonymousClass1JZ A0F;
    public static final AnonymousClass1JZ A0G;
    public static final AnonymousClass1JZ A0H;
    public static final AnonymousClass1JZ A0I;
    public static final AnonymousClass1JZ A0J;
    public static final AnonymousClass1JZ A0K;
    public static final AnonymousClass1JZ A0L;
    public static final AnonymousClass1JZ A0M;
    private final int colorInt;
    private final int colorResId;

    static {
        AnonymousClass1JZ r0 = new AnonymousClass1JZ("PRIMARY", 0, 2132083164, C15320v6.MEASURED_STATE_MASK);
        A0C = r0;
        AnonymousClass1JZ r02 = new AnonymousClass1JZ("PRIMARY_DARK", 1, 2132082776, -1);
        A0D = r02;
        AnonymousClass1JZ r03 = new AnonymousClass1JZ("SECONDARY", 2, 2132083283, Integer.MIN_VALUE);
        A0G = r03;
        AnonymousClass1JZ r04 = new AnonymousClass1JZ("SECONDARY_DARK", 3, 2132083267, -2130706433);
        A0H = r04;
        AnonymousClass1JZ r05 = new AnonymousClass1JZ("TERTIARY", 4, 2132083274, 1459617792);
        A0I = r05;
        AnonymousClass1JZ r06 = new AnonymousClass1JZ("TERTIARY_DARK", 5, 2132083267, -2130706433);
        A0J = r06;
        AnonymousClass1JZ r07 = new AnonymousClass1JZ("HINT", 6, 2132083274, 1459617792);
        A08 = r07;
        AnonymousClass1JZ r08 = new AnonymousClass1JZ("HINT_DARK", 7, 2132083267, -2130706433);
        A09 = r08;
        AnonymousClass1JZ r13 = new AnonymousClass1JZ("DISABLED", 8, 2132083167, 520093696);
        A04 = r13;
        AnonymousClass1JZ r12 = new AnonymousClass1JZ("DISABLED_DARK", 9, 2132083171, 872415231);
        A05 = r12;
        AnonymousClass1JZ r11 = new AnonymousClass1JZ("INVERSE_PRIMARY", 10, 2132082776, -1);
        A0A = r11;
        AnonymousClass1JZ r10 = new AnonymousClass1JZ("INVERSE_PRIMARY_DARK", 11, 2132082776, -1);
        A0B = r10;
        AnonymousClass1JZ r9 = new AnonymousClass1JZ("BLUE", 12, 2132083269, -16737793);
        A02 = r9;
        AnonymousClass1JZ r8 = new AnonymousClass1JZ("BLUE_DARK", 13, 2132083270, -15096833);
        A03 = r8;
        AnonymousClass1JZ r7 = new AnonymousClass1JZ("RED", 14, 2132083281, -54999);
        A0E = r7;
        AnonymousClass1JZ r6 = new AnonymousClass1JZ("RED_DARK", 15, 2132083282, -48574);
        A0F = r6;
        AnonymousClass1JZ r5 = new AnonymousClass1JZ("GREEN", 16, 2132083272, -10824391);
        A06 = r5;
        AnonymousClass1JZ r4 = new AnonymousClass1JZ("GREEN_DARK", 17, 2132083273, -9644465);
        A07 = r4;
        AnonymousClass1JZ r3 = new AnonymousClass1JZ("WHITE", 18, 2132082802, -1);
        A0K = r3;
        AnonymousClass1JZ r14 = new AnonymousClass1JZ("WHITE_50", 19, 2132082780, -2130706433);
        A0L = r14;
        AnonymousClass1JZ r26 = new AnonymousClass1JZ("WHITE_70_DONOTUSE", 20, 2132082750, -1275068417);
        A0M = r26;
        AnonymousClass1JZ r262 = new AnonymousClass1JZ("BLACK_74_DONOTUSE", 21, 2132083261, -1124073472);
        A01 = r262;
        AnonymousClass1JZ r45 = r26;
        AnonymousClass1JZ r46 = r262;
        A00 = new AnonymousClass1JZ[]{r0, r02, r03, r04, r05, r06, r07, r08, r13, r12, r11, r10, r9, r8, r7, r6, r5, r4, r3, r14, r45, r46};
    }

    public static AnonymousClass1JZ valueOf(String str) {
        return (AnonymousClass1JZ) Enum.valueOf(AnonymousClass1JZ.class, str);
    }

    public static AnonymousClass1JZ[] values() {
        return (AnonymousClass1JZ[]) A00.clone();
    }

    public int AhV() {
        return this.colorInt;
    }

    public int Aha() {
        return this.colorResId;
    }

    private AnonymousClass1JZ(String str, int i, int i2, int i3) {
        this.colorResId = i2;
        this.colorInt = i3;
    }
}
