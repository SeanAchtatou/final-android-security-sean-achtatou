package X;

import java.util.HashMap;

/* renamed from: X.1rK  reason: invalid class name and case insensitive filesystem */
public final class C35501rK implements C31551js {
    public final HashMap A00 = new HashMap();
    private final C06480bZ A01;

    public boolean BHG() {
        return false;
    }

    public void BxL(Runnable runnable, String str) {
        HashMap hashMap = this.A00;
        C35591rT r1 = new C35591rT(runnable, hashMap);
        hashMap.put(runnable, r1);
        this.A01.CIC(r1);
    }

    public void BxM(Runnable runnable, String str) {
        throw new IllegalStateException("postAtFront is not supported for IdleExecutorHandler");
    }

    public void C1H(Runnable runnable) {
        C35591rT r1 = (C35591rT) this.A00.remove(runnable);
        if (r1 != null) {
            r1.A00 = null;
        }
    }

    public C35501rK(C06480bZ r2) {
        this.A01 = r2;
    }
}
