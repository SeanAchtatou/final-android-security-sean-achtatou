package X;

import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import java.util.Iterator;

/* renamed from: X.1lp  reason: invalid class name and case insensitive filesystem */
public final class C32481lp {
    public int A00;
    public InboxUnitItem A01;
    public InboxUnitItem A02;
    public ImmutableList.Builder A03 = ImmutableList.builder();
    public boolean A04;
    private int A05;
    private int A06;

    private void A00(InboxUnitItem inboxUnitItem) {
        boolean z;
        if (inboxUnitItem.A02.A0Q() == GraphQLMessengerInboxUnitType.A0I) {
            this.A04 = true;
        }
        if (this.A01 == null) {
            z = false;
        } else {
            z = !Objects.equal(inboxUnitItem.A02.A0U(), this.A01.A02.A0U());
        }
        if (z) {
            this.A05 = 0;
            this.A06++;
        }
        this.A03.add((Object) inboxUnitItem);
        this.A01 = inboxUnitItem;
        inboxUnitItem.A0C(this.A05);
        int i = this.A00;
        this.A00 = i + 1;
        inboxUnitItem.A0B(i);
        inboxUnitItem.A0D(this.A06);
        this.A05++;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004f, code lost:
        if (r0 != false) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(com.facebook.messaging.inbox2.items.InboxUnitItem r6) {
        /*
            r5 = this;
            com.facebook.messaging.inbox2.items.InboxUnitItem r3 = r5.A02
            if (r3 == 0) goto L_0x002f
            r0 = 0
            r5.A02 = r0
            X.1oK r1 = r6.A05()
            X.1oK r0 = X.C33901oK.A0Z
            if (r1 == r0) goto L_0x002f
            X.1ck r0 = r3.A02
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r4 = r0.A0Q()
            X.1ck r0 = r6.A02
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r2 = r0.A0Q()
            if (r4 != r2) goto L_0x002f
            com.facebook.messaging.inbox2.items.InboxUnitItem r0 = r5.A01
            if (r0 == 0) goto L_0x003a
            X.1ck r0 = r0.A02
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r1 = r0.A0Q()
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A0I
            if (r1 != r0) goto L_0x003a
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A02
            if (r2 != r0) goto L_0x003a
        L_0x002f:
            X.1oK r1 = r6.A05()
            X.1oK r0 = X.C33901oK.A0Z
            if (r1 != r0) goto L_0x005b
            r5.A02 = r6
            return
        L_0x003a:
            com.facebook.graphql.enums.GraphQLMessengerInboxUnitType r0 = com.facebook.graphql.enums.GraphQLMessengerInboxUnitType.A0H
            if (r4 != r0) goto L_0x0043
            boolean r0 = r5.A04
            if (r0 != 0) goto L_0x0043
            goto L_0x002f
        L_0x0043:
            boolean r0 = r5.A04
            if (r0 != 0) goto L_0x0054
            int r0 = r4.ordinal()
            switch(r0) {
                case 0: goto L_0x0052;
                case 6: goto L_0x0052;
                case 29: goto L_0x0052;
                case 34: goto L_0x0052;
                case 36: goto L_0x0052;
                default: goto L_0x004e;
            }
        L_0x004e:
            r0 = 0
        L_0x004f:
            if (r0 == 0) goto L_0x0054
            goto L_0x002f
        L_0x0052:
            r0 = 1
            goto L_0x004f
        L_0x0054:
            r0 = 1
            r5.A04 = r0
            r5.A00(r3)
            goto L_0x002f
        L_0x005b:
            r5.A00(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32481lp.A01(com.facebook.messaging.inbox2.items.InboxUnitItem):void");
    }

    public void A02(Iterable iterable) {
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            A01((InboxUnitItem) it.next());
        }
    }
}
