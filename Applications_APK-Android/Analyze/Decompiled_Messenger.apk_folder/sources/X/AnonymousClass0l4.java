package X;

import android.view.ViewGroup;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0l4  reason: invalid class name */
public final class AnonymousClass0l4 extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C10920l5 A01;

    public static final C10920l5 A00(AnonymousClass1XY r8) {
        if (A01 == null) {
            synchronized (A00) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r8);
                if (A002 != null) {
                    try {
                        r8.getApplicationInjector();
                        A01 = new C10920l5(2132411701, 2132476937, C13820s8.A00(), new ViewGroup.LayoutParams(-1, -1), "ThreadListFragment");
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
