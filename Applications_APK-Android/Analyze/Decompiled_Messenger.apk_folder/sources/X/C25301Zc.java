package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.1Zc  reason: invalid class name and case insensitive filesystem */
public final class C25301Zc implements C04810Wg {
    public final /* synthetic */ AnonymousClass1ZX A00;

    public void Bqf(Object obj) {
        C30378EvB evB = null;
        if (evB != null) {
            evB.onJobSuccess(obj);
        }
    }

    public C25301Zc(AnonymousClass1ZX r1) {
        this.A00 = r1;
    }

    public void BYh(Throwable th) {
        C83953yW r0;
        if (!(th instanceof CancellationException) && (r0 = null) != null) {
            r0.onJobFailed(th);
        }
    }
}
