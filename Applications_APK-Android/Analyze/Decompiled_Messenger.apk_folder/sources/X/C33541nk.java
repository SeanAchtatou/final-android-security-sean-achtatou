package X;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.1nk  reason: invalid class name and case insensitive filesystem */
public abstract class C33541nk implements C32651m6, AnonymousClass062 {
    public C34981qU A00;
    public C34961qS A01;
    public Boolean A02;
    private C34841qG A03;
    private C35051qb A04;
    private String A05;

    public void A0L() {
    }

    public void A0M() {
    }

    public final Optional A0G() {
        return Optional.fromNullable(this.A04);
    }

    public final Optional A0H() {
        return Optional.fromNullable(this.A03);
    }

    public final void A0I() {
        if (this.A03 != null) {
            C34961qS r0 = this.A01;
            if (r0 != null) {
                r0.A07(this);
            }
            A0L();
            this.A03 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0018, code lost:
        if (r0.A00.Aem(2306129015380908056L) != false) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        if (r1 != false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0K(X.C35051qb r4) {
        /*
            r3 = this;
            X.1qb r0 = r3.A04
            boolean r0 = com.google.common.base.Objects.equal(r0, r4)
            if (r0 == 0) goto L_0x001e
            X.1qU r0 = r3.A00
            if (r0 == 0) goto L_0x001a
            X.1Yd r2 = r0.A00
            r0 = 2306129015380908056(0x2001041f00001818, double:1.5863982120897392E-154)
            boolean r0 = r2.Aem(r0)
            r1 = 0
            if (r0 == 0) goto L_0x001b
        L_0x001a:
            r1 = 1
        L_0x001b:
            r0 = 0
            if (r1 == 0) goto L_0x001f
        L_0x001e:
            r0 = 1
        L_0x001f:
            com.google.common.base.Preconditions.checkNotNull(r4)
            r3.A04 = r4
            if (r0 == 0) goto L_0x003d
            com.google.common.base.Optional r0 = r3.A0H()
            boolean r0 = r0.isPresent()
            if (r0 == 0) goto L_0x003d
            com.google.common.base.Optional r0 = r3.A0H()
            java.lang.Object r0 = r0.get()
            X.1qG r0 = (X.C34841qG) r0
            r0.C2D(r4)
        L_0x003d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33541nk.A0K(X.1qb):void");
    }

    public Map Bxv() {
        String obj;
        Boolean bool = this.A02;
        if (bool == null || !bool.booleanValue()) {
            return Collections.emptyMap();
        }
        C35051qb r0 = this.A04;
        if (r0 == null) {
            obj = null;
        } else {
            obj = r0.toString();
        }
        if (obj == null) {
            obj = "No current ViewState";
        }
        return Collections.singletonMap(this.A05, AnonymousClass08S.A0J("ViewState: ", obj));
    }

    public C33541nk(String str) {
        this.A05 = str;
    }

    public final void A0J(C34841qG r4) {
        Preconditions.checkNotNull(r4);
        C34841qG r0 = this.A03;
        if (r0 != r4) {
            if (r0 != null) {
                A0I();
            }
            this.A03 = r4;
            C35051qb r1 = this.A04;
            if (r1 != null && A0H().isPresent()) {
                ((C34841qG) A0H().get()).C2D(r1);
            }
            C34841qG r2 = this.A03;
            Context context = null;
            if (r2 != null) {
                if (r2 instanceof Fragment) {
                    context = ((Fragment) r2).A1j();
                } else if (r2 instanceof Activity) {
                    context = (Context) r2;
                } else if (r2 instanceof View) {
                    context = ((View) r2).getContext();
                }
            }
            if (context != null) {
                AnonymousClass1XX r12 = AnonymousClass1XX.get(context);
                this.A01 = C34961qS.A00(r12);
                this.A00 = new C34981qU(r12);
                this.A02 = C184313a.A02(r12);
                C34961qS r02 = this.A01;
                Preconditions.checkNotNull(r02);
                r02.A06(this);
            }
            A0M();
        }
    }
}
