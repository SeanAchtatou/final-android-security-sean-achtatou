package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.1ad  reason: invalid class name and case insensitive filesystem */
public final class C25931ad extends C25941ae {
    private C25931ad(AnonymousClass0US r2) {
        super(r2, ImmutableSet.A04(C25951af.A07));
    }

    public static final C25931ad A00(AnonymousClass1XY r2) {
        return new C25931ad(AnonymousClass0VB.A00(AnonymousClass1Y3.ApA, r2));
    }
}
