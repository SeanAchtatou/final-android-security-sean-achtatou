package X;

/* renamed from: X.1wU  reason: invalid class name and case insensitive filesystem */
public final class C37871wU extends RuntimeException {
    public final AnonymousClass1NY mEncodedImage;

    public C37871wU(String str, AnonymousClass1NY r2) {
        super(str);
        this.mEncodedImage = r2;
    }
}
