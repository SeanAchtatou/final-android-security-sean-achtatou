package X;

/* renamed from: X.0rQ  reason: invalid class name and case insensitive filesystem */
public enum C13430rQ {
    MQTT_DISCONNECTED,
    MQTT_CONNECTED_WAITING_FOR_PRESENCE,
    PRESENCE_MAP_RECEIVED,
    TP_DISABLED,
    TP_WAITING_FOR_FULL_LIST,
    TP_FULL_LIST_RECEIVED
}
