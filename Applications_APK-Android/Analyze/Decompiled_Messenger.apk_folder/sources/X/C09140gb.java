package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0gb  reason: invalid class name and case insensitive filesystem */
public final class C09140gb {
    private static volatile C09140gb A01;
    public AnonymousClass0UN A00;

    public static final C09140gb A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C09140gb.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C09140gb(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C09140gb(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
