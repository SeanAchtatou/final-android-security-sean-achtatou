package X;

import android.content.Context;
import android.content.res.Resources;

/* renamed from: X.0zy  reason: invalid class name and case insensitive filesystem */
public final class C18040zy {
    private AnonymousClass0UN A00;
    public final Resources A01;
    private final Context A02;
    private final C001300x A03;
    private final Boolean A04;

    public static final C18040zy A00(AnonymousClass1XY r2) {
        return new C18040zy(r2, AnonymousClass0WA.A00(r2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006f, code lost:
        if (r3 < r1) goto L_0x0071;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01() {
        /*
            r7 = this;
            X.00x r0 = r7.A03
            X.00z r1 = r0.A02
            X.00z r0 = X.C001500z.A07
            r6 = 0
            if (r1 != r0) goto L_0x0035
            java.lang.Boolean r0 = r7.A04
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0035
            int r0 = X.AnonymousClass1Y3.BAo
            X.0UN r1 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r0, r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r5 = 1
            if (r0 == 0) goto L_0x0036
            int r0 = X.AnonymousClass1Y3.AOJ
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r5, r0, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 288149355896471(0x1061200001e97, double:1.42364697619729E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0036
        L_0x0035:
            return r6
        L_0x0036:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 >= r0) goto L_0x0048
            android.content.Context r1 = r7.A02
            java.lang.String r0 = "android.permission.SYSTEM_ALERT_WINDOW"
            int r1 = r1.checkCallingOrSelfPermission(r0)
            r0 = -1
            if (r1 != r0) goto L_0x0048
            return r6
        L_0x0048:
            android.content.res.Resources r0 = r7.A01
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r1 = r0.widthPixels
            int r0 = r0.heightPixels
            int r4 = java.lang.Math.min(r1, r0)
            int r3 = java.lang.Math.max(r1, r0)
            android.content.res.Resources r1 = r7.A01
            r0 = 2132148338(0x7f160072, float:1.9938651E38)
            int r2 = r1.getDimensionPixelSize(r0)
            android.content.res.Resources r1 = r7.A01
            r0 = 2132148337(0x7f160071, float:1.993865E38)
            int r1 = r1.getDimensionPixelSize(r0)
            if (r4 < r2) goto L_0x0071
            r0 = 1
            if (r3 >= r1) goto L_0x0072
        L_0x0071:
            r0 = 0
        L_0x0072:
            if (r0 == 0) goto L_0x0035
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18040zy.A01():boolean");
    }

    public C18040zy(AnonymousClass1XY r3, AnonymousClass1YI r4) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A02 = AnonymousClass1YA.A00(r3);
        this.A03 = AnonymousClass0UU.A02(r3);
        this.A01 = C04490Ux.A0L(r3);
        this.A04 = Boolean.valueOf(r4.AbO(689, false));
    }
}
