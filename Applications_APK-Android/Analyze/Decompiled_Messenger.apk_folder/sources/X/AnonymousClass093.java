package X;

import android.os.Build;
import android.os.StrictMode;
import java.lang.reflect.Method;

/* renamed from: X.093  reason: invalid class name */
public final class AnonymousClass093 {
    public static Method A00;
    public static final boolean A01;
    public static final boolean A02;

    static {
        boolean z;
        boolean z2 = true;
        try {
            A00 = StrictMode.VmPolicy.Builder.class.getMethod("permitNonSdkApiUsage", new Class[0]);
            z = true;
        } catch (NoClassDefFoundError | NoSuchMethodException unused) {
            z = false;
        }
        A01 = z;
        if (Build.VERSION.SDK_INT < 28) {
            z2 = false;
        }
        A02 = z2;
    }
}
