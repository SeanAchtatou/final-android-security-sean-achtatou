package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;

/* renamed from: X.1xz  reason: invalid class name and case insensitive filesystem */
public final class C38781xz implements AnonymousClass0Jo {
    private C22361La A00;
    private final boolean A01;

    public AnonymousClass0Jo ANM(String str, int i) {
        if (this.A01) {
            this.A00.A02(str, i);
        }
        return this;
    }

    public AnonymousClass0Jo ANN(String str, long j) {
        if (this.A01) {
            this.A00.A03(str, j);
        }
        return this;
    }

    public AnonymousClass0Jo ANO(String str, String str2) {
        if (this.A01 && str2 != null) {
            this.A00.A06(str, str2);
        }
        return this;
    }

    public AnonymousClass0Jo ANP(String str, boolean z) {
        if (this.A01) {
            this.A00.A07(str, z);
        }
        return this;
    }

    public void BIw() {
        if (this.A01) {
            this.A00.A0A();
        }
    }

    public C38781xz(DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        C22361La A04 = deprecatedAnalyticsLogger.A04("fbandroid_cold_start", false);
        this.A00 = A04;
        this.A01 = A04.A0B();
    }

    public AnonymousClass0Jo ANL(String str, char c) {
        ANO(str, Character.toString(c));
        return this;
    }
}
