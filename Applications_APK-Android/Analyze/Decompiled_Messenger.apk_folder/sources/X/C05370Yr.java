package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.TypedValue;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.0Yr  reason: invalid class name and case insensitive filesystem */
public final class C05370Yr {
    public static boolean A00;
    private static C27787Diw A01;
    private static boolean A02;
    private static final Object A03 = new Object();

    public static C27787Diw A01() {
        if (!A02) {
            synchronized (A03) {
                if (!A02) {
                    int i = Build.VERSION.SDK_INT;
                    C27787Diw diw = null;
                    if (i < 26) {
                        if (i >= 24) {
                            diw = new C27766Dib();
                        } else if (i >= 14) {
                            diw = new C27765Dia();
                        }
                    }
                    A01 = diw;
                    A02 = true;
                }
            }
        }
        return A01;
    }

    public static void A04(Throwable th) {
        Throwable cause;
        if ((th instanceof InvocationTargetException) && (cause = th.getCause()) != null && (cause instanceof Resources.NotFoundException)) {
            throw ((Resources.NotFoundException) cause);
        }
    }

    public static Drawable A00(Object obj, Resources resources, TypedValue typedValue, int i, Resources.Theme theme, boolean z) {
        try {
            C27787Diw A012 = A01();
            if (A012 != null) {
                return A012.BIM(obj, resources, typedValue, i, theme, z);
            }
            throw new C06170ay();
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            A04(e);
            throw new C06170ay(e);
        }
    }

    public static Object A02(Resources resources) {
        try {
            C27787Diw A012 = A01();
            if (A012 != null) {
                return A012.B19(resources);
            }
            throw new C06170ay();
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            A04(e);
            throw new C06170ay(e);
        }
    }

    public static void A03(Object obj, int i, TypedValue typedValue, boolean z) {
        try {
            C27787Diw A012 = A01();
            if (A012 != null) {
                A012.B8G(obj, i, typedValue, z);
                return;
            }
            throw new C06170ay();
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            A04(e);
            throw new C06170ay(e);
        }
    }
}
