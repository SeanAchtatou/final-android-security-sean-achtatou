package X;

import java.util.Iterator;

/* renamed from: X.1Fx  reason: invalid class name and case insensitive filesystem */
public final class C21271Fx extends C24971Xv {
    public final /* synthetic */ Iterator A00;

    public C21271Fx(Iterator it) {
        this.A00 = it;
    }

    public boolean hasNext() {
        return this.A00.hasNext();
    }

    public Object next() {
        return this.A00.next();
    }
}
