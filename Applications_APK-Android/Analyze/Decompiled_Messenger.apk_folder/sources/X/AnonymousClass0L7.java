package X;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0L7  reason: invalid class name */
public final class AnonymousClass0L7<E> extends AbstractSet<E> implements Set<E> {
    private final Map A00;

    public boolean add(Object obj) {
        if (this.A00.put(obj, Boolean.TRUE) != null) {
            return true;
        }
        return false;
    }

    public void clear() {
        this.A00.clear();
    }

    public boolean contains(Object obj) {
        return this.A00.containsKey(obj);
    }

    public boolean containsAll(Collection collection) {
        return this.A00.keySet().containsAll(collection);
    }

    public boolean isEmpty() {
        return this.A00.isEmpty();
    }

    public Iterator iterator() {
        return this.A00.keySet().iterator();
    }

    public boolean remove(Object obj) {
        if (this.A00.remove(obj) != null) {
            return true;
        }
        return false;
    }

    public boolean removeAll(Collection collection) {
        return this.A00.keySet().removeAll(collection);
    }

    public boolean retainAll(Collection collection) {
        return this.A00.keySet().retainAll(collection);
    }

    public int size() {
        return this.A00.size();
    }

    public AnonymousClass0L7(int i) {
        this.A00 = Collections.synchronizedMap(new AnonymousClass0L6(this, i));
    }

    public Object[] toArray() {
        return this.A00.keySet().toArray();
    }

    public Object[] toArray(Object[] objArr) {
        return this.A00.keySet().toArray(objArr);
    }
}
