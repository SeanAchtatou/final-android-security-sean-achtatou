package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.01k  reason: invalid class name and case insensitive filesystem */
public final class C002101k extends Enum {
    public static final C002101k A00 = new C002101k("ANR", 29, 'r', "ANR");
    public static final C002101k A01 = new C002101k("ANR_AM_CONFIRMED", 34, 'R', "AnrAmConfirmed");
    public static final C002101k A02 = new C002101k("ANR_AM_CONFIRMED_MT_UNBLOCKED", 35, 'U', "AnrAmConfirmedMtUnblocked");
    public static final C002101k A03 = new C002101k("ANR_AM_EXPIRED", 36, 'm', "AnrAmExpired");
    public static final C002101k A04 = new C002101k("ANR_MT_UNBLOCKED", 33, 'u', "AnrMtUnblocked");
    public static final C002101k A05 = new C002101k("ANR_RECOVERED", 30, 'c', "ANRRecovered");
    public static final C002101k A06 = new C002101k("ANR_SIGQUIT", 32, 'q', "AnrSigquit");
    public static final C002101k A07 = new C002101k("ANR_SIGQUIT_NATIVE", 31, 'Q', "ANR_SIGQUIT");
    public static final C002101k A08 = new C002101k("APPSTATELOGGER_CRASH", 37, 'C', "AppStateLoggerCrash");
    public static final C002101k A09 = new C002101k("DONE_WRITING", 3, 'd', "DoneWriting");
    public static final C002101k A0A = new C002101k("INITIAL", 1, 'i', "Initial");
    public static final C002101k A0B = new C002101k("JAVA_CRASH", 4, 'j', "JavaCrash");
    public static final C002101k A0C = new C002101k("JAVA_EXIT", 25, 'x', "JavaExit");
    public static final C002101k A0D = new C002101k("NO_STATUS", 0, '0', "No status");
    public static final C002101k A0E = new C002101k("SELF_SIGKILL", 27, '9', "SelfSigKill");
    public static final C002101k A0F = new C002101k("WRITING_FILE", 2, 'w', "WritingFile");
    private final String mStringValue;
    public final char mSymbol;

    static {
        new C002101k("NATIVE_CRASH_SIGSEGV", 5, 's', "NativeCrash_SIGSEGV");
        new C002101k("NATIVE_CRASH_SIGABRT", 6, 'a', "NativeCrash_SIGABRT");
        new C002101k("NATIVE_CRASH_SIGFPE", 7, 'f', "NativeCrash_SIGFPE");
        new C002101k("NATIVE_CRASH_SIGILL", 8, 'l', "NativeCrash_SIGILL");
        new C002101k("NATIVE_CRASH_SIGBUS", 9, 'b', "NativeCrash_SIGBUS");
        new C002101k("NATIVE_CRASH_SIGTRAP", 10, 'T', "NativeCrash_SIGTRAP");
        new C002101k("NATIVE_CRASH_SIGXFSZ", 11, 'z', "NativeCrash_SIGXFSZ");
        new C002101k("NATIVE_CRASH_SIGXCPU", 12, 'p', "NativeCrash_SIGXCPU");
        new C002101k("NATIVE_CRASH_SIGSYS", 13, 'y', "NativeCrash_SIGSYS");
        new C002101k("NATIVE_CRASH_SIGSTKFLT", 14, 'S', "NativeCrash_SIGSTKFLT");
        new C002101k("NATIVE_CRASH_SIGHUP", 15, 'h', "NativeCrash_SIGHUP");
        new C002101k("NATIVE_CRASH_SIGINT", 16, 'I', "NativeCrash_SIGINT");
        new C002101k("NATIVE_CRASH_SIGUSR2", 17, '2', "NativeCrash_SIGUSR2");
        new C002101k("NATIVE_CRASH_SIGALRM", 18, 'L', "NativeCrash_SIGALRM");
        new C002101k("NATIVE_CRASH_SIGTERM", 19, 'e', "NativeCrash_SIGTERM");
        new C002101k("NATIVE_CRASH_SIGVTALRM", 20, 'v', "NativeCrash_SIGVTALRM");
        new C002101k("NATIVE_CRASH_SIGPROF", 21, 'P', "NativeCrash_SIGPROF");
        new C002101k("NATIVE_CRASH_SIGIO", 22, 'o', "NativeCrash_SIGIO");
        new C002101k("NATIVE_CRASH_SIGPWR", 23, 'W', "NativeCrash_SIGPWR");
        new C002101k("NATIVE_CRASH_OTHER", 24, 'n', "NativeCrash_Other");
        new C002101k("NATIVE_EXIT", 26, 'X', "NativeExit");
        new C002101k("SELF_SIGSTOP", 28, 't', "SelfSigStop");
        new C002101k("PREALLOCATED_OOME", 38, 'O', "PreallocatedOOME");
    }

    private C002101k(String str, int i, char c, String str2) {
        this.mSymbol = c;
        this.mStringValue = str2;
    }

    public String toString() {
        return this.mStringValue;
    }
}
