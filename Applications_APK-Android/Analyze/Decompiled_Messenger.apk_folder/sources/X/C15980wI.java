package X;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

/* renamed from: X.0wI  reason: invalid class name and case insensitive filesystem */
public final class C15980wI {
    public static void A00(View view, int i) {
        if (Build.VERSION.SDK_INT >= 17) {
            view.setBackgroundColor(i);
        } else {
            view.setBackgroundDrawable(new ColorDrawable(i));
        }
    }

    public static void A01(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }
}
