package X;

/* renamed from: X.1G4  reason: invalid class name */
public final class AnonymousClass1G4 {
    public AnonymousClass0UN A00;
    public final AnonymousClass06B A01 = AnonymousClass067.A02();
    public final C05660a7 A02;
    public final C06080ao A03;
    public final C25051Yd A04;

    public static final AnonymousClass1G4 A00(AnonymousClass1XY r2) {
        new C05670a8(r2);
        return new AnonymousClass1G4(r2);
    }

    private AnonymousClass1G4(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(2, r4);
        this.A04 = AnonymousClass0WT.A00(r4);
        this.A03 = C06080ao.A00(r4);
        this.A02 = new C05660a7("MESSENGER_INBOX2", null, null);
    }
}
