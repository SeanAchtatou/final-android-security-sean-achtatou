package X;

import com.mapbox.mapboxsdk.geometry.LatLng;
import java.util.Iterator;

/* renamed from: X.1zE  reason: invalid class name and case insensitive filesystem */
public final class C39551zE implements C28384Du0 {
    public final /* synthetic */ C28394DuB A00;

    public C39551zE(C28394DuB duB) {
        this.A00 = duB;
    }

    public boolean BeB(LatLng latLng) {
        if (this.A00.A0W.isEmpty() || !this.A00.A0D.A0I(latLng)) {
            return false;
        }
        Iterator it = this.A00.A0W.iterator();
        while (it.hasNext()) {
            ((C30516Ey1) it.next()).onLocationComponentLongClick();
        }
        return true;
    }
}
