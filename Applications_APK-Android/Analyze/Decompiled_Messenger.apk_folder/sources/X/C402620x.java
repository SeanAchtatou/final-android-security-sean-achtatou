package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.fbservice.ops.BlueServiceOperation$1;
import com.facebook.fbservice.service.BlueService;
import com.facebook.fbservice.service.BlueServiceLogic;
import com.facebook.fbservice.service.IBlueService;
import com.facebook.fbservice.service.OperationResult;
import com.google.common.base.Preconditions;
import java.util.concurrent.ExecutorService;

/* renamed from: X.20x  reason: invalid class name and case insensitive filesystem */
public final class C402620x {
    public Bundle A00;
    public Handler A01;
    public CallerContext A02;
    public C402520w A03;
    public AnonymousClass2G4 A04;
    public AnonymousClass2G1 A05 = AnonymousClass2G1.INIT;
    public C156107Jr A06;
    public C34711q3 A07;
    public IBlueService A08;
    public AnonymousClass0UN A09;
    public String A0A;
    public String A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final Context A0I;
    public final Context A0J;
    public final AnonymousClass8TY A0K;
    public final ExecutorService A0L;
    private final C05920aY A0M;

    public static final C402620x A00(AnonymousClass1XY r4) {
        Context A002 = AnonymousClass1YA.A00(r4);
        ExecutorService A0a = AnonymousClass0UX.A0a(r4);
        C05920aY A012 = C10580kT.A01(r4);
        C04910Wu.A00(r4);
        return new C402620x(r4, A002, A0a, A012);
    }

    private void A01() {
        if (this.A08.C0R(this.A0A, new BlueServiceOperation$1(this))) {
            this.A0G = true;
        } else {
            A06(this, OperationResult.A02(C14880uI.A0C, AnonymousClass08S.A0J("Unknown operation: ", this.A0A)));
        }
    }

    public static void A02(C402620x r5) {
        if (r5.A08 == null) {
            if (!r5.A0D) {
                Intent intent = new Intent(r5.A0I, BlueService.class);
                C27251ct r1 = (C27251ct) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AW9, r5.A09);
                if (r1.A02 && !r1.A01(intent)) {
                    r5.A08 = (BlueServiceLogic) AnonymousClass1XX.A03(AnonymousClass1Y3.AVT, r5.A09);
                    C34711q3 r0 = (C34711q3) AnonymousClass1XX.A03(AnonymousClass1Y3.ABI, r5.A09);
                    synchronized (r5) {
                        r5.A07 = r0;
                        r0.A02();
                    }
                } else if (C006406k.A02(r5.A0J, intent, r5.A0K, 1, 615707233)) {
                    r5.A0D = true;
                    return;
                } else {
                    A06(r5, OperationResult.A02(C14880uI.A0C, C05360Yq.$const$string(741)));
                    return;
                }
            } else {
                return;
            }
        }
        A03(r5);
    }

    public static void A03(C402620x r5) {
        C14880uI r1;
        String str;
        AnonymousClass2G1 r12 = r5.A05;
        boolean z = false;
        if (r12 == AnonymousClass2G1.A04) {
            boolean z2 = false;
            if (r5.A0B != null) {
                z2 = true;
            }
            Preconditions.checkState(z2, C05360Yq.$const$string(946));
            if (r5.A0A == null) {
                z = true;
            }
            Preconditions.checkState(z, C05360Yq.$const$string(AnonymousClass1Y3.A7d));
            Preconditions.checkState(!r5.A0G, "Registered for completion and haven't yet sent");
            try {
                r5.A0A = r5.A08.CHM(r5.A0B, r5.A00, r5.A0H, r5.A02);
                if (r5.A08 != null) {
                    r5.A01();
                    r5.A05 = AnonymousClass2G1.A03;
                    return;
                }
                throw new RemoteException();
            } catch (RemoteException unused) {
                r1 = C14880uI.A0C;
                str = "BlueService.<method> or registerCompletionHandler failed";
                A06(r5, OperationResult.A02(r1, str));
            }
        } else if (r12 == AnonymousClass2G1.A03) {
            if (r5.A0A != null) {
                z = true;
            }
            Preconditions.checkState(z, "null operation id");
            if (!r5.A0G) {
                try {
                    r5.A01();
                } catch (RemoteException unused2) {
                    r1 = C14880uI.A0C;
                    str = "BlueService.registerCompletionHandler failed";
                    A06(r5, OperationResult.A02(r1, str));
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000a, code lost:
        if (r4 == X.AnonymousClass2G1.A01) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A04(X.C402620x r5) {
        /*
            X.2G1 r4 = r5.A05
            X.2G1 r3 = X.AnonymousClass2G1.INIT
            r2 = 0
            if (r4 == r3) goto L_0x000c
            X.2G1 r1 = X.AnonymousClass2G1.COMPLETED
            r0 = 0
            if (r4 != r1) goto L_0x000d
        L_0x000c:
            r0 = 1
        L_0x000d:
            com.google.common.base.Preconditions.checkState(r0)
            r5.A05 = r3
            r0 = 0
            r5.A0B = r0
            r5.A0H = r2
            r5.A00 = r0
            r5.A02 = r0
            r5.A0A = r0
            r5.A0G = r2
            A05(r5)
            r5.A08 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C402620x.A04(X.20x):void");
    }

    public static void A05(C402620x r5) {
        C34711q3 r1;
        if (r5.A0D) {
            try {
                C006406k.A01(r5.A0J, r5.A0K, 1759318896);
            } catch (IllegalArgumentException e) {
                C010708t.A0W("BlueServiceOperation", e, "Exception unbinding %s", r5.A0B);
            }
            r5.A0D = false;
        }
        synchronized (r5) {
            r1 = r5.A07;
            r5.A07 = null;
        }
        if (r1 != null) {
            r1.A01();
        }
    }

    public static void A06(C402620x r3, OperationResult operationResult) {
        if (r3.A0F) {
            r3.A0E = true;
            A05(r3);
            r3.A08 = null;
            r3.A04 = null;
            r3.A03 = null;
            C156107Jr r0 = r3.A06;
            if (r0 != null) {
                r0.CI0();
                return;
            }
            return;
        }
        C156047Jl r2 = new C156047Jl(r3, operationResult);
        Handler handler = r3.A01;
        if (handler != null) {
            AnonymousClass00S.A04(handler, r2, 307589974);
        } else {
            AnonymousClass07A.A04(r3.A0L, r2, 1401584281);
        }
    }

    public void A07(C156107Jr r3) {
        C156107Jr r0;
        AnonymousClass2G1 r1 = this.A05;
        if ((r1 == AnonymousClass2G1.A04 || r1 == AnonymousClass2G1.A03) && (r0 = this.A06) != null) {
            r0.CI0();
        }
        this.A06 = r3;
        AnonymousClass2G1 r12 = this.A05;
        if ((r12 == AnonymousClass2G1.A04 || r12 == AnonymousClass2G1.A03) && r3 != null) {
            r3.APp();
        }
    }

    public void A08(String str, boolean z, Bundle bundle, CallerContext callerContext) {
        ViewerContext Awn;
        boolean z2 = true;
        boolean z3 = false;
        if (this.A05 == AnonymousClass2G1.INIT) {
            z3 = true;
        }
        Preconditions.checkState(z3, C05360Yq.$const$string(884));
        if (this.A0B != null) {
            z2 = false;
        }
        Preconditions.checkState(z2, "Initially operationType should be null");
        Preconditions.checkNotNull(str, "non-null operationType");
        this.A05 = AnonymousClass2G1.A04;
        this.A0B = str;
        this.A0H = z;
        this.A00 = new Bundle(bundle);
        this.A02 = callerContext;
        if (Looper.myLooper() != null) {
            this.A01 = new Handler();
        }
        if (!this.A00.containsKey("overridden_viewer_context") && (Awn = this.A0M.Awn()) != null) {
            this.A00.putParcelable("overridden_viewer_context", Awn);
        }
        this.A00.putString(C05360Yq.$const$string(AnonymousClass1Y3.AAA), AnonymousClass00M.A00().A01);
        C156107Jr r0 = this.A06;
        if (r0 != null) {
            r0.APp();
        }
        A02(this);
    }

    static {
        AnonymousClass0TG.A07();
    }

    private C402620x(AnonymousClass1XY r3, Context context, ExecutorService executorService, C05920aY r6) {
        this.A09 = new AnonymousClass0UN(1, r3);
        this.A0I = context;
        this.A0K = new AnonymousClass8TY(this);
        this.A0L = executorService;
        this.A0M = r6;
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.getParent() instanceof Activity) {
                context = activity.getParent();
            }
        }
        this.A0J = context;
    }
}
