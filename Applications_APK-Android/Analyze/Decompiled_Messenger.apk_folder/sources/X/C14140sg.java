package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0sg  reason: invalid class name and case insensitive filesystem */
public final class C14140sg {
    private static C05540Zi A01;
    public AnonymousClass0UN A00;

    public static final C14140sg A00(AnonymousClass1XY r4) {
        C14140sg r0;
        synchronized (C14140sg.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C14140sg((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (C14140sg) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C14140sg(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }
}
