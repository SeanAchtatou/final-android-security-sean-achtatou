package X;

import java.io.File;
import java.util.Comparator;

/* renamed from: X.0PL  reason: invalid class name */
public final class AnonymousClass0PL implements Comparator {
    public int compare(Object obj, Object obj2) {
        return ((File) obj).getName().compareTo(((File) obj2).getName());
    }
}
