package X;

import android.app.ActivityManager;
import android.os.Build;
import com.facebook.acra.util.StatFsUtil;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.dextricks.DalvikConstants;

/* renamed from: X.1P4  reason: invalid class name */
public final class AnonymousClass1P4 implements C23111Og {
    public long A00;
    public AnonymousClass1P7 A01;
    public AnonymousClass0UN A02;
    public final ActivityManager A03;
    private final AnonymousClass1P6 A04 = new AnonymousClass1P5(this);
    private final AnonymousClass1OV A05;

    public static final AnonymousClass1P4 A00(AnonymousClass1XY r2) {
        return new AnonymousClass1P4(r2, C04490Ux.A04(r2));
    }

    /* renamed from: A01 */
    public C30851ik get() {
        int i;
        C23211Oq A022 = this.A05.A02(844579549282321L, this.A04);
        double A012 = A022.A01("java_heap_limit_multiplier", 1.0d);
        double A013 = A022.A01("device_total_memory_multiplier", 0.0d);
        C30851ik A002 = this.A01.get();
        if (A012 == 1.0d && A013 == 0.0d) {
            i = A002.A02;
        } else {
            int min = Math.min(this.A03.getMemoryClass() * 1048576, Integer.MAX_VALUE);
            if (min < 33554432) {
                i = 4194304;
            } else if (min < 67108864) {
                i = 6291456;
            } else {
                int i2 = Build.VERSION.SDK_INT;
                i = DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE;
                if (i2 >= 11) {
                    ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                    this.A03.getMemoryInfo(memoryInfo);
                    int i3 = (int) ((((double) min) * A012) + (((double) memoryInfo.totalMem) * A013));
                    int i4 = min >> 2;
                    int i5 = i3;
                    if (i3 > i4) {
                        i5 = i4;
                    }
                    if (i5 >= 8388608) {
                        i = i5;
                    }
                    USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ap7, this.A02)).A01("adaptive_image_memory_cache"), 2);
                    if (uSLEBaseShape0S0000000.A0G()) {
                        uSLEBaseShape0S0000000.A0B("adaptive_value", Integer.valueOf(i));
                        uSLEBaseShape0S0000000.A0B("heap_limit", Integer.valueOf(min));
                        uSLEBaseShape0S0000000.A0A("heap_limit_multiplier", Float.valueOf((float) A012));
                        uSLEBaseShape0S0000000.A0B("old_value", Integer.valueOf(i4));
                        uSLEBaseShape0S0000000.A0B("total_memory", Integer.valueOf((int) (memoryInfo.totalMem / StatFsUtil.IN_MEGA_BYTE)));
                        uSLEBaseShape0S0000000.A0A("total_memory_multiplier", Float.valueOf((float) A013));
                        uSLEBaseShape0S0000000.A0B("uncapped_value", Integer.valueOf(i3));
                        uSLEBaseShape0S0000000.A06();
                    }
                }
            }
        }
        int i6 = A002.A00;
        int i7 = A002.A04;
        return new C30851ik(i, i6, i7, A002.A03, A002.A01, A002.A05);
    }

    public AnonymousClass1P4(AnonymousClass1XY r3, ActivityManager activityManager) {
        this.A02 = new AnonymousClass0UN(1, r3);
        this.A05 = AnonymousClass1OU.A05(r3);
        this.A03 = activityManager;
        this.A01 = new AnonymousClass1P7(activityManager);
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            this.A03.getMemoryInfo(memoryInfo);
            this.A00 = memoryInfo.totalMem;
            return;
        }
        this.A00 = StatFsUtil.IN_GIGA_BYTE;
    }
}
