package X;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: X.0DE  reason: invalid class name */
public final class AnonymousClass0DE {
    public Class A00 = Class.forName("java.lang.ref.FinalizerReference");
    public Class A01;
    public Constructor A02;
    public Field A03;
    public Method A04;

    public static int A00(Exception exc) {
        C010708t.A0L(AnonymousClass0DE.class.getName(), "FinalizerLatency failed", exc);
        return 3;
    }

    public AnonymousClass0DE() {
        Class<?> cls = Class.forName("java.lang.ref.FinalizerReference$Sentinel");
        this.A01 = cls;
        Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
        this.A02 = declaredConstructor;
        declaredConstructor.setAccessible(true);
        Method declaredMethod = this.A00.getDeclaredMethod("enqueueSentinelReference", this.A01);
        this.A04 = declaredMethod;
        declaredMethod.setAccessible(true);
        Field declaredField = this.A01.getDeclaredField("finalized");
        this.A03 = declaredField;
        declaredField.setAccessible(true);
    }
}
