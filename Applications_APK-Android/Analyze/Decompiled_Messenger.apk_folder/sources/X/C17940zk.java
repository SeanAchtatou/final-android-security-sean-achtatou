package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.NotificationSetting;

/* renamed from: X.0zk  reason: invalid class name and case insensitive filesystem */
public final class C17940zk implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new NotificationSetting(parcel);
    }

    public Object[] newArray(int i) {
        return new NotificationSetting[i];
    }
}
