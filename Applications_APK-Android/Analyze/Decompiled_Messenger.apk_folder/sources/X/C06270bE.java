package X;

import android.database.Observable;

/* renamed from: X.0bE  reason: invalid class name and case insensitive filesystem */
public final class C06270bE extends Observable {
    public void A00() {
        synchronized (this.mObservers) {
            int size = this.mObservers.size();
            for (int i = 0; i < size; i++) {
                ((C06430bU) this.mObservers.get(i)).onBackground();
            }
        }
    }

    public void A01() {
        synchronized (this.mObservers) {
            int size = this.mObservers.size();
            for (int i = 0; i < size; i++) {
                ((C06430bU) this.mObservers.get(i)).onForeground();
            }
        }
    }

    public void A02() {
        synchronized (this.mObservers) {
            int size = this.mObservers.size();
            for (int i = 0; i < size; i++) {
                ((C06430bU) this.mObservers.get(i)).CLQ();
            }
        }
    }

    public void A03(AnonymousClass23I r5) {
        synchronized (this.mObservers) {
            int size = this.mObservers.size();
            for (int i = 0; i < size; i++) {
                ((C06430bU) this.mObservers.get(i)).CLP(r5);
            }
        }
    }
}
