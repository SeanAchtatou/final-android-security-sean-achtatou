package X;

import android.database.sqlite.SQLiteDatabase;
import com.facebook.common.stringformat.StringFormatUtil;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0W4  reason: invalid class name */
public abstract class AnonymousClass0W4 {
    public final String A00;
    private final ImmutableList A01;
    private final ImmutableList A02;

    public void A0B(SQLiteDatabase sQLiteDatabase) {
    }

    public static String A00(String str) {
        return AnonymousClass08S.A0J("DROP TABLE IF EXISTS ", str);
    }

    public static String A01(String str, AnonymousClass0W6 r7) {
        return AnonymousClass08S.A0U("ALTER TABLE ", str, " ADD COLUMN ", r7.A00, " ", r7.A01);
    }

    public static String A02(String str, AnonymousClass0W6 r3, int i) {
        return "ALTER TABLE " + str + " ADD COLUMN " + r3.A00 + " " + r3.A01 + " DEFAULT " + i;
    }

    public static String A03(String str, ImmutableList immutableList, ImmutableList immutableList2) {
        return A04(str, immutableList, immutableList2, "CREATE TABLE");
    }

    public static String A04(String str, ImmutableList immutableList, ImmutableList immutableList2, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append(" ");
        sb.append(str);
        sb.append(" (");
        sb.append(Joiner.on(", ").join(AnonymousClass0TH.A00(immutableList, AnonymousClass0W6.A02)));
        if (immutableList2 != null && !immutableList2.isEmpty()) {
            sb.append(", ");
            sb.append(Joiner.on(", ").join(AnonymousClass0TH.A00(immutableList2, AnonymousClass8PL.A00)));
        }
        sb.append(")");
        return sb.toString();
    }

    public static String A05(String str, String str2, ImmutableList immutableList) {
        return A07(str, str2, Joiner.on(", ").join(AnonymousClass0TH.A00(immutableList, AnonymousClass0W6.A03)), false);
    }

    public static String A06(String str, String str2, ImmutableList immutableList) {
        return A07(str, str2, Joiner.on(", ").join(immutableList), false);
    }

    private static String A07(String str, String str2, String str3, boolean z) {
        StringBuilder sb = new StringBuilder(str.length() + 41 + str2.length() + str3.length());
        sb.append("CREATE");
        if (z) {
            sb.append(" UNIQUE");
        }
        sb.append(" INDEX IF NOT EXISTS ");
        sb.append(str2);
        sb.append(" ON ");
        sb.append(str);
        sb.append(" (");
        sb.append(str3);
        sb.append(")");
        return sb.toString();
    }

    public static void A08(SQLiteDatabase sQLiteDatabase, String str, ImmutableList immutableList, ImmutableList immutableList2) {
        String A0J = AnonymousClass08S.A0J(str, "_temp");
        String join = Joiner.on(", ").join(AnonymousClass0TH.A00(immutableList, AnonymousClass0W6.A03));
        String formatStrLocaleSafe = StringFormatUtil.formatStrLocaleSafe("INSERT INTO %s SELECT %s FROM %s", A0J, join, str);
        String formatStrLocaleSafe2 = StringFormatUtil.formatStrLocaleSafe("INSERT INTO %s SELECT %s FROM %s", str, join, A0J);
        String A04 = A04(A0J, immutableList, immutableList2, "CREATE TEMPORARY TABLE");
        C007406x.A00(-1149920851);
        sQLiteDatabase.execSQL(A04);
        C007406x.A00(-1785752329);
        C007406x.A00(850764554);
        sQLiteDatabase.execSQL(formatStrLocaleSafe);
        C007406x.A00(251701695);
        String A002 = A00(str);
        C007406x.A00(-2016946717);
        sQLiteDatabase.execSQL(A002);
        C007406x.A00(1712495344);
        String A03 = A03(str, immutableList, immutableList2);
        C007406x.A00(857230874);
        sQLiteDatabase.execSQL(A03);
        C007406x.A00(-816686386);
        C007406x.A00(500927114);
        sQLiteDatabase.execSQL(formatStrLocaleSafe2);
        C007406x.A00(-500326047);
        String A003 = A00(A0J);
        C007406x.A00(-1662462568);
        sQLiteDatabase.execSQL(A003);
        C007406x.A00(831759200);
    }

    public void A09(SQLiteDatabase sQLiteDatabase) {
        String A03 = A03(this.A00, this.A01, this.A02);
        C007406x.A00(112597731);
        sQLiteDatabase.execSQL(A03);
        C007406x.A00(-1177953716);
    }

    public void A0A(SQLiteDatabase sQLiteDatabase) {
        String A04 = A04(this.A00, this.A01, this.A02, "CREATE TABLE IF NOT EXISTS");
        C007406x.A00(823592685);
        sQLiteDatabase.execSQL(A04);
        C007406x.A00(2141850423);
    }

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String A002 = A00(this.A00);
        C007406x.A00(497723133);
        sQLiteDatabase.execSQL(A002);
        C007406x.A00(-1942166866);
        A09(sQLiteDatabase);
    }

    public AnonymousClass0W4(String str, ImmutableList immutableList) {
        this.A00 = str;
        this.A01 = immutableList;
        this.A02 = null;
    }

    public AnonymousClass0W4(String str, ImmutableList immutableList, C06060am r4) {
        this(str, immutableList, ImmutableList.of(r4));
    }

    public AnonymousClass0W4(String str, ImmutableList immutableList, ImmutableList immutableList2) {
        this.A00 = str;
        this.A01 = immutableList;
        this.A02 = immutableList2;
    }
}
