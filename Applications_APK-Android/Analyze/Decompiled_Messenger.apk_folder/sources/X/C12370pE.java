package X;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.util.TriState;
import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.service.model.FetchMessageAfterTimestampParams;
import com.facebook.messaging.service.model.FetchMessageResult;
import com.facebook.messaging.service.model.FetchMoreMessagesParams;
import com.facebook.messaging.service.model.FetchMoreMessagesResult;
import com.facebook.messaging.service.model.FetchThreadParams;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@UserScoped
/* renamed from: X.0pE  reason: invalid class name and case insensitive filesystem */
public final class C12370pE extends C11150lz {
    private static C05540Zi A06;
    public AnonymousClass0UN A00;
    public final AnonymousClass06B A01 = AnonymousClass067.A02();
    public final C08770fv A02;
    public final C04310Tq A03;
    @LoggedInUser
    public final C04310Tq A04;
    public final C04310Tq A05;

    public static Message A02(MessagesCollection messagesCollection) {
        if (messagesCollection != null) {
            C24971Xv it = messagesCollection.A01.iterator();
            while (it.hasNext()) {
                Message message = (Message) it.next();
                if (!message.A14) {
                    return message;
                }
            }
        }
        return null;
    }

    private C12370pE(AnonymousClass1XY r3) {
        super("DbServiceHandler");
        this.A00 = new AnonymousClass0UN(17, r3);
        this.A05 = C25771aN.A02(r3);
        this.A02 = C08770fv.A00(r3);
        this.A04 = AnonymousClass0WY.A01(r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.BKW, r3);
    }

    public static final C12370pE A01(AnonymousClass1XY r4) {
        C12370pE r0;
        synchronized (C12370pE.class) {
            C05540Zi A002 = C05540Zi.A00(A06);
            A06 = A002;
            try {
                if (A002.A03(r4)) {
                    A06.A00 = new C12370pE((AnonymousClass1XY) A06.A01());
                }
                C05540Zi r1 = A06;
                r0 = (C12370pE) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A06.A02();
                throw th;
            }
        }
        return r0;
    }

    public static void A03(OperationResult operationResult) {
        Bundle bundle = operationResult.resultDataBundle;
        if (bundle != null) {
            bundle.putString("source", "db");
        }
    }

    public static void A04(OperationResult operationResult, OperationResult operationResult2) {
        Bundle bundle;
        if (operationResult.resultDataBundle != null && (bundle = operationResult2.resultDataBundle) != null && bundle.getString("source") != null) {
            operationResult.resultDataBundle.putString("source", operationResult2.resultDataBundle.getString("source"));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        if (r1.A01.equals(r8) != false) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0030, code lost:
        if (r10.A03 < r3.A03) goto L_0x0032;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A05(X.C12370pE r9, com.facebook.messaging.model.messages.Message r10, com.facebook.messaging.service.model.FetchThreadResult r11) {
        /*
            com.facebook.messaging.model.threads.ThreadSummary r2 = r11.A05
            if (r2 == 0) goto L_0x005f
            com.facebook.messaging.model.messages.MessagesCollection r0 = r11.A03
            r3 = 0
            if (r0 == 0) goto L_0x000d
            com.facebook.messaging.model.messages.Message r3 = r0.A05()
        L_0x000d:
            if (r3 == 0) goto L_0x005f
            r4 = 7
            int r1 = X.AnonymousClass1Y3.BJQ
            X.0UN r0 = r9.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.0aY r0 = (X.C05920aY) r0
            com.facebook.auth.viewercontext.ViewerContext r0 = r0.B9R()
            java.lang.String r1 = r0.mUserId
            com.facebook.user.model.UserKey r8 = new com.facebook.user.model.UserKey
            X.1aB r0 = X.C25651aB.A03
            r8.<init>(r0, r1)
            if (r10 == 0) goto L_0x0032
            long r6 = r10.A03
            long r4 = r3.A03
            int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            r0 = 0
            if (r1 >= 0) goto L_0x0033
        L_0x0032:
            r0 = 1
        L_0x0033:
            if (r0 == 0) goto L_0x005f
            com.facebook.messaging.model.messages.ParticipantInfo r1 = r3.A0K
            boolean r0 = r1.A01()
            if (r0 == 0) goto L_0x0046
            com.facebook.user.model.UserKey r0 = r1.A01
            boolean r1 = r0.equals(r8)
            r0 = 1
            if (r1 == 0) goto L_0x0047
        L_0x0046:
            r0 = 0
        L_0x0047:
            if (r0 == 0) goto L_0x005f
            boolean r0 = r2.A0B()
            if (r0 == 0) goto L_0x005f
            r2 = 3
            int r1 = X.AnonymousClass1Y3.A21
            X.0UN r0 = r9.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9oq r1 = (X.C206749oq) r1
            java.lang.String r0 = "FETCH_THREAD_OPERATION"
            r1.A04(r3, r0)
        L_0x005f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12370pE.A05(X.0pE, com.facebook.messaging.model.messages.Message, com.facebook.messaging.service.model.FetchThreadResult):void");
    }

    public static void A06(C12370pE r12, FetchThreadParams fetchThreadParams, FetchThreadResult fetchThreadResult, C27311cz r15) {
        int i;
        MessagesCollection messagesCollection = fetchThreadResult.A03;
        if (!messagesCollection.A02) {
            ThreadSummary threadSummary = fetchThreadResult.A05;
            int A042 = fetchThreadParams.A01 - messagesCollection.A04();
            if (A042 > 0) {
                FetchMoreMessagesParams fetchMoreMessagesParams = new FetchMoreMessagesParams(threadSummary.A0S, messagesCollection.A06().A03, A042 + 1, true, false);
                Bundle bundle = new Bundle();
                bundle.putParcelable("fetchMoreMessagesParams", fetchMoreMessagesParams);
                FetchMoreMessagesResult fetchMoreMessagesResult = (FetchMoreMessagesResult) r15.BAz(new C11060ln("fetch_more_messages", bundle)).A08();
                C52232ij r7 = (C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, r12.A00);
                SQLiteDatabase A062 = ((C25771aN) r7.A0F.get()).A06();
                C007406x.A01(A062, 1905310862);
                try {
                    if (!r7.A07.A02(fetchThreadResult.A03, fetchMoreMessagesResult.A02)) {
                        C007406x.A02(A062, 361452367);
                        MessagesCollection A002 = AnonymousClass0mB.A00((AnonymousClass0mB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BFT, r12.A00), messagesCollection, fetchMoreMessagesResult.A02, true);
                        C61222yX A003 = FetchThreadResult.A00();
                        A003.A01 = DataFetchDisposition.A0G;
                        A003.A04 = threadSummary;
                        A003.A02 = A002;
                        A003.A06 = fetchThreadResult.A07;
                        A003.A00 = r12.A01.now();
                        A003.A00();
                    }
                    C52232ij.A06(r7, fetchMoreMessagesResult.A02, true);
                    A062.setTransactionSuccessful();
                    i = 1809711243;
                    C007406x.A02(A062, i);
                    MessagesCollection A0022 = AnonymousClass0mB.A00((AnonymousClass0mB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BFT, r12.A00), messagesCollection, fetchMoreMessagesResult.A02, true);
                    C61222yX A0032 = FetchThreadResult.A00();
                    A0032.A01 = DataFetchDisposition.A0G;
                    A0032.A04 = threadSummary;
                    A0032.A02 = A0022;
                    A0032.A06 = fetchThreadResult.A07;
                    A0032.A00 = r12.A01.now();
                    A0032.A00();
                } catch (SQLException e) {
                    C52232ij.A09(e);
                    i = -1778705123;
                } catch (Throwable th) {
                    C007406x.A02(A062, -1195153417);
                    throw th;
                }
            }
        }
    }

    private void A07(FetchThreadResult fetchThreadResult, FetchThreadResult fetchThreadResult2) {
        if (fetchThreadResult2.A05 != null) {
            SQLiteDatabase A062 = ((C25771aN) this.A05.get()).A06();
            C007406x.A01(A062, 2017149726);
            try {
                A05(this, A02(fetchThreadResult.A03), fetchThreadResult2);
                ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, this.A00)).A0X(fetchThreadResult, fetchThreadResult2, "DbServiceHandler.handleFetchThread");
                A062.setTransactionSuccessful();
            } finally {
                C007406x.A02(A062, 1082607647);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0298, code lost:
        if (r4 == X.C10950l8.A04) goto L_0x029a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.fbservice.service.OperationResult A0x(X.C11060ln r25, X.C27311cz r26) {
        /*
            r24 = this;
            r1 = r25
            java.lang.String r9 = r1.A05
            android.os.Bundle r0 = r1.A00
            java.lang.String r8 = "fetchThreadListParams"
            android.os.Parcelable r5 = r0.getParcelable(r8)
            com.facebook.messaging.service.model.FetchThreadListParams r5 = (com.facebook.messaging.service.model.FetchThreadListParams) r5
            X.0l8 r4 = r5.A03
            com.facebook.common.callercontext.CallerContext r0 = r1.A01
            if (r0 == 0) goto L_0x004c
            java.lang.String r2 = r0.A0G()
        L_0x0018:
            r3 = 9
            int r1 = X.AnonymousClass1Y3.AwM
            r6 = r24
            X.0UN r0 = r6.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.0pN r7 = (X.C12450pN) r7
            X.0pV r3 = X.C12500pT.A05
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "fetchThreadList (DSH)(folder="
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = ",trigger="
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            r7.A03(r3, r0)
            X.0hU r10 = r5.A01
            r1 = 2097853903(0x7d0ab5cf, float:1.1523592E37)
            java.lang.String r0 = "DbServiceHandler.handleFetchThreadList"
            X.C005505z.A03(r0, r1)
            goto L_0x004e
        L_0x004c:
            r2 = 0
            goto L_0x0018
        L_0x004e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x03ae }
            r1.<init>()     // Catch:{ all -> 0x03ae }
            java.lang.String r0 = "DbServiceHandler.handleFetchThreadList - "
            r1.append(r0)     // Catch:{ all -> 0x03ae }
            r1.append(r4)     // Catch:{ all -> 0x03ae }
            java.lang.String r0 = " - "
            r1.append(r0)     // Catch:{ all -> 0x03ae }
            r1.append(r2)     // Catch:{ all -> 0x03ae }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x03ae }
            r0 = -162064296(0xfffffffff6571858, float:-1.0906617E33)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x03ae }
            r2 = 5
            int r1 = X.AnonymousClass1Y3.AV7     // Catch:{ all -> 0x03a6 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x03a6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x03a6 }
            X.0ph r0 = (X.C12620ph) r0     // Catch:{ all -> 0x03a6 }
            com.facebook.messaging.service.model.FetchThreadListResult r3 = r0.A06(r5)     // Catch:{ all -> 0x03a6 }
            int r2 = X.AnonymousClass1Y3.BRG     // Catch:{ all -> 0x03a6 }
            X.0UN r1 = r6.A00     // Catch:{ all -> 0x03a6 }
            r0 = 14
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x03a6 }
            X.0yl r0 = (X.C17350yl) r0     // Catch:{ all -> 0x03a6 }
            boolean r0 = r0.A08()     // Catch:{ all -> 0x03a6 }
            if (r0 == 0) goto L_0x027d
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r3.A06     // Catch:{ all -> 0x03a6 }
            com.google.common.collect.ImmutableList r0 = r0.A00     // Catch:{ all -> 0x03a6 }
            X.1Xv r15 = r0.iterator()     // Catch:{ all -> 0x03a6 }
        L_0x0096:
            boolean r0 = r15.hasNext()     // Catch:{ all -> 0x03a6 }
            if (r0 == 0) goto L_0x027d
            java.lang.Object r1 = r15.next()     // Catch:{ all -> 0x03a6 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1     // Catch:{ all -> 0x03a6 }
            com.google.common.collect.ImmutableList r0 = r1.A0o     // Catch:{ all -> 0x03a6 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x03a6 }
            if (r0 != 0) goto L_0x0096
            com.google.common.collect.ImmutableList r1 = r1.A0o     // Catch:{ all -> 0x03a6 }
            r0 = 0
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x03a6 }
            com.facebook.messaging.model.messages.Message r0 = (com.facebook.messaging.model.messages.Message) r0     // Catch:{ all -> 0x03a6 }
            java.lang.String r2 = r0.A0q     // Catch:{ all -> 0x03a6 }
            int r1 = X.AnonymousClass1Y3.Ae7     // Catch:{ all -> 0x03a6 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x03a6 }
            r7 = 13
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x03a6 }
            X.5hU r11 = (X.C117495hU) r11     // Catch:{ all -> 0x03a6 }
            java.util.concurrent.locks.ReadWriteLock r0 = r11.A01     // Catch:{ all -> 0x03a6 }
            java.util.concurrent.locks.Lock r1 = r0.readLock()     // Catch:{ all -> 0x03a6 }
            r1.lock()     // Catch:{ all -> 0x03a6 }
            java.util.HashMap r0 = r11.A00     // Catch:{ all -> 0x0278 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x0278 }
            r1.unlock()     // Catch:{ all -> 0x03a6 }
            if (r0 != 0) goto L_0x0096
            r11 = 1
            int r1 = X.AnonymousClass1Y3.AcJ     // Catch:{ all -> 0x03a6 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x03a6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r11, r1, r0)     // Catch:{ all -> 0x03a6 }
            X.1br r0 = (X.C26691br) r0     // Catch:{ all -> 0x03a6 }
            java.lang.String r1 = "msg_id"
            X.0av r11 = X.C06160ax.A03(r1, r2)     // Catch:{ all -> 0x03a6 }
            X.0Tq r1 = r0.A08     // Catch:{ all -> 0x03a6 }
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x03a6 }
            X.1aN r1 = (X.C25771aN) r1     // Catch:{ all -> 0x03a6 }
            android.database.sqlite.SQLiteDatabase r16 = r1.A06()     // Catch:{ all -> 0x03a6 }
            java.lang.String[] r18 = X.B4y.A06     // Catch:{ all -> 0x03a6 }
            java.lang.String r19 = r11.A02()     // Catch:{ all -> 0x03a6 }
            java.lang.String[] r20 = r11.A04()     // Catch:{ all -> 0x03a6 }
            java.lang.String r17 = "montage_directs"
            r21 = 0
            r22 = 0
            r23 = 0
            android.database.Cursor r13 = r16.query(r17, r18, r19, r20, r21, r22, r23)     // Catch:{ all -> 0x03a6 }
            int r11 = X.AnonymousClass1Y3.ABo     // Catch:{ all -> 0x03a6 }
            X.0UN r1 = r0.A00     // Catch:{ all -> 0x03a6 }
            r0 = 2
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r11, r1)     // Catch:{ all -> 0x03a6 }
            X.B4y r0 = (X.B4y) r0     // Catch:{ all -> 0x03a6 }
            X.B4z r12 = new X.B4z     // Catch:{ all -> 0x03a6 }
            r12.<init>(r0, r13)     // Catch:{ all -> 0x03a6 }
            android.database.Cursor r0 = r12.A0I     // Catch:{ all -> 0x0270 }
            boolean r0 = r0.moveToNext()     // Catch:{ all -> 0x0270 }
            if (r0 == 0) goto L_0x0250
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A06     // Catch:{ all -> 0x0270 }
            java.lang.String r13 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0G     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)     // Catch:{ all -> 0x0270 }
            X.1TG r11 = com.facebook.messaging.model.messages.Message.A00()     // Catch:{ all -> 0x0270 }
            r11.A06(r13)     // Catch:{ all -> 0x0270 }
            r11.A0T = r0     // Catch:{ all -> 0x0270 }
            X.B4y r0 = r12.A0J     // Catch:{ all -> 0x0270 }
            X.0jC r14 = r0.A04     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0B     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r14.A03(r0)     // Catch:{ all -> 0x0270 }
            r11.A0J = r0     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0H     // Catch:{ all -> 0x0270 }
            long r0 = r1.getLong(r0)     // Catch:{ all -> 0x0270 }
            r11.A04 = r0     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0D     // Catch:{ all -> 0x0270 }
            long r0 = r1.getLong(r0)     // Catch:{ all -> 0x0270 }
            r11.A03 = r0     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A07     // Catch:{ all -> 0x0270 }
            int r0 = r1.getInt(r0)     // Catch:{ all -> 0x0270 }
            X.1V7 r0 = X.AnonymousClass1V7.A00(r0)     // Catch:{ all -> 0x0270 }
            r11.A0C = r0     // Catch:{ all -> 0x0270 }
            X.B4y r0 = r12.A0J     // Catch:{ all -> 0x0270 }
            X.0oH r14 = r0.A00     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A00     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            com.google.common.collect.ImmutableList r0 = r14.A03(r0, r13)     // Catch:{ all -> 0x0270 }
            r11.A08(r0)     // Catch:{ all -> 0x0270 }
            X.B4y r0 = r12.A0J     // Catch:{ all -> 0x0270 }
            X.0oQ r13 = r0.A02     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0F     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            com.google.common.collect.ImmutableMap r0 = r13.A02(r0)     // Catch:{ all -> 0x0270 }
            r11.A0A(r0)     // Catch:{ all -> 0x0270 }
            X.B4y r0 = r12.A0J     // Catch:{ all -> 0x0270 }
            X.0oS r13 = r0.A03     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A02     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            com.google.common.collect.ImmutableMap r1 = r13.A02(r0)     // Catch:{ all -> 0x0270 }
            com.google.common.base.Preconditions.checkNotNull(r1)     // Catch:{ all -> 0x0270 }
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x0270 }
            r0.<init>(r1)     // Catch:{ all -> 0x0270 }
            r11.A10 = r0     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A08     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            r11.A0u = r0     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0E     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            r11.A0w = r0     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A01     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            X.1u2 r0 = com.facebook.messaging.database.threads.MessageCursorUtil.A01(r0)     // Catch:{ all -> 0x0270 }
            r11.A09 = r0     // Catch:{ all -> 0x0270 }
            X.B4y r0 = r12.A0J     // Catch:{ all -> 0x0270 }
            X.0oF r13 = r0.A01     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A09     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            java.util.List r0 = r13.A04(r0)     // Catch:{ all -> 0x0270 }
            r11.A09(r0)     // Catch:{ all -> 0x0270 }
            X.B4y r0 = r12.A0J     // Catch:{ all -> 0x0270 }
            X.0oN r13 = r0.A05     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0C     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            com.facebook.messaging.model.share.SentShareAttachment r0 = r13.A01(r0)     // Catch:{ all -> 0x0270 }
            r11.A0S = r0     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A0A     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            X.1R6 r0 = com.facebook.messaging.database.threads.MessageCursorUtil.A02(r0)     // Catch:{ all -> 0x0270 }
            r11.A02(r0)     // Catch:{ all -> 0x0270 }
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A03     // Catch:{ all -> 0x0270 }
            boolean r0 = r1.isNull(r0)     // Catch:{ all -> 0x0270 }
            if (r0 != 0) goto L_0x021f
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A03     // Catch:{ all -> 0x0270 }
            int r0 = r1.getInt(r0)     // Catch:{ all -> 0x0270 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0270 }
            r11.A0i = r0     // Catch:{ all -> 0x0270 }
        L_0x021f:
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A05     // Catch:{ all -> 0x0270 }
            boolean r0 = r1.isNull(r0)     // Catch:{ all -> 0x0270 }
            if (r0 != 0) goto L_0x0233
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A05     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            r11.A0p = r0     // Catch:{ all -> 0x0270 }
        L_0x0233:
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A04     // Catch:{ all -> 0x0270 }
            boolean r0 = r1.isNull(r0)     // Catch:{ all -> 0x0270 }
            if (r0 != 0) goto L_0x024b
            android.database.Cursor r1 = r12.A0I     // Catch:{ all -> 0x0270 }
            int r0 = r12.A04     // Catch:{ all -> 0x0270 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0270 }
            X.4o1 r0 = X.C98894o1.A00(r0)     // Catch:{ all -> 0x0270 }
            r11.A0E = r0     // Catch:{ all -> 0x0270 }
        L_0x024b:
            com.facebook.messaging.model.messages.Message r11 = r11.A00()     // Catch:{ all -> 0x0270 }
            goto L_0x0251
        L_0x0250:
            r11 = 0
        L_0x0251:
            if (r11 == 0) goto L_0x0259
            android.database.Cursor r0 = r12.A0I     // Catch:{ all -> 0x03a6 }
            r0.close()     // Catch:{ all -> 0x03a6 }
            goto L_0x025f
        L_0x0259:
            android.database.Cursor r0 = r12.A0I     // Catch:{ all -> 0x03a6 }
            r0.close()     // Catch:{ all -> 0x03a6 }
            r11 = 0
        L_0x025f:
            if (r11 == 0) goto L_0x0096
            int r1 = X.AnonymousClass1Y3.Ae7     // Catch:{ all -> 0x03a6 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x03a6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)     // Catch:{ all -> 0x03a6 }
            X.5hU r0 = (X.C117495hU) r0     // Catch:{ all -> 0x03a6 }
            r0.A01(r2, r11)     // Catch:{ all -> 0x03a6 }
            goto L_0x0096
        L_0x0270:
            r1 = move-exception
            android.database.Cursor r0 = r12.A0I     // Catch:{ all -> 0x03a6 }
            r0.close()     // Catch:{ all -> 0x03a6 }
            goto L_0x03a5
        L_0x0278:
            r0 = move-exception
            r1.unlock()     // Catch:{ all -> 0x03a6 }
            throw r0     // Catch:{ all -> 0x03a6 }
        L_0x027d:
            X.0Tq r0 = r6.A03     // Catch:{ all -> 0x03a6 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x03a6 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x03a6 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x03a6 }
            if (r0 == 0) goto L_0x030f
            boolean r0 = r4.A01()     // Catch:{ all -> 0x03a6 }
            if (r0 != 0) goto L_0x030f
            X.0l8 r0 = X.C10950l8.A0D     // Catch:{ all -> 0x03a6 }
            if (r4 == r0) goto L_0x029a
            X.0l8 r1 = X.C10950l8.A04     // Catch:{ all -> 0x03a6 }
            r0 = 0
            if (r4 != r1) goto L_0x029b
        L_0x029a:
            r0 = 1
        L_0x029b:
            if (r0 != 0) goto L_0x030f
            r2 = 6
            int r1 = X.AnonymousClass1Y3.Aeq     // Catch:{ all -> 0x03a6 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x03a6 }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x03a6 }
            X.0cb r1 = (X.C07080cb) r1     // Catch:{ all -> 0x03a6 }
            X.1eA r0 = X.C12740pt.A0A     // Catch:{ all -> 0x03a6 }
            java.lang.String r0 = r1.A02(r0)     // Catch:{ all -> 0x03a6 }
            r4 = 1
            if (r0 != 0) goto L_0x0302
            int r1 = X.AnonymousClass1Y3.AcJ     // Catch:{ all -> 0x03a6 }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x03a6 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ all -> 0x03a6 }
            X.1br r0 = (X.C26691br) r0     // Catch:{ all -> 0x03a6 }
            boolean r0 = r0.A01     // Catch:{ all -> 0x03a6 }
            if (r0 != 0) goto L_0x0302
        L_0x02bf:
            X.162 r2 = new X.162     // Catch:{ all -> 0x03a6 }
            r2.<init>()     // Catch:{ all -> 0x03a6 }
            X.0u3 r0 = X.AnonymousClass0u3.LOCAL_DISK_CACHE     // Catch:{ all -> 0x03a6 }
            r2.A07 = r0     // Catch:{ all -> 0x03a6 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x03a6 }
            r2.A02 = r0     // Catch:{ all -> 0x03a6 }
            com.facebook.common.util.TriState r1 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x03a6 }
            r2.A04 = r1     // Catch:{ all -> 0x03a6 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r4)     // Catch:{ all -> 0x03a6 }
            r2.A05 = r0     // Catch:{ all -> 0x03a6 }
            r2.A06 = r1     // Catch:{ all -> 0x03a6 }
            com.facebook.fbservice.results.DataFetchDisposition r0 = new com.facebook.fbservice.results.DataFetchDisposition     // Catch:{ all -> 0x03a6 }
            r0.<init>(r2)     // Catch:{ all -> 0x03a6 }
            X.161 r4 = new X.161     // Catch:{ all -> 0x03a6 }
            r4.<init>()     // Catch:{ all -> 0x03a6 }
            r4.A00(r3)     // Catch:{ all -> 0x03a6 }
            r4.A02 = r0     // Catch:{ all -> 0x03a6 }
            X.06B r0 = r6.A01     // Catch:{ all -> 0x03a6 }
            long r0 = r0.now()     // Catch:{ all -> 0x03a6 }
            r4.A00 = r0     // Catch:{ all -> 0x03a6 }
            com.facebook.messaging.service.model.FetchThreadListResult r2 = new com.facebook.messaging.service.model.FetchThreadListResult     // Catch:{ all -> 0x03a6 }
            r2.<init>(r4)     // Catch:{ all -> 0x03a6 }
            com.facebook.fbservice.results.DataFetchDisposition r1 = r3.A02     // Catch:{ all -> 0x03a6 }
            com.facebook.fbservice.results.DataFetchDisposition r0 = com.facebook.fbservice.results.DataFetchDisposition.A0I     // Catch:{ all -> 0x03a6 }
            if (r1 == r0) goto L_0x030f
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A04(r2)     // Catch:{ all -> 0x03a6 }
            A03(r1)     // Catch:{ all -> 0x03a6 }
            goto L_0x0304
        L_0x0302:
            r4 = 0
            goto L_0x02bf
        L_0x0304:
            r0 = 847630223(0x3285cf8f, float:1.5577625E-8)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x03ae }
            r0 = 590660981(0x2334c575, float:9.799633E-18)
            goto L_0x039a
        L_0x030f:
            com.facebook.fbservice.results.DataFetchDisposition r7 = r3.A02     // Catch:{ all -> 0x03a6 }
            int[] r1 = X.C43602Ez.A00     // Catch:{ all -> 0x03a6 }
            int r0 = r10.ordinal()     // Catch:{ all -> 0x03a6 }
            r4 = r1[r0]     // Catch:{ all -> 0x03a6 }
            r2 = 1
            if (r4 == r2) goto L_0x0330
            r0 = 2
            if (r4 == r0) goto L_0x0334
            r0 = 3
            r1 = 0
            if (r4 != r0) goto L_0x0333
            boolean r0 = r7.A08     // Catch:{ all -> 0x03a6 }
            if (r0 == 0) goto L_0x0333
            com.facebook.common.util.TriState r0 = r7.A04     // Catch:{ all -> 0x03a6 }
            boolean r0 = r0.asBoolean(r1)     // Catch:{ all -> 0x03a6 }
            if (r0 != 0) goto L_0x0333
            goto L_0x0334
        L_0x0330:
            boolean r2 = r7.A08     // Catch:{ all -> 0x03a6 }
            goto L_0x0334
        L_0x0333:
            r2 = 0
        L_0x0334:
            if (r2 == 0) goto L_0x033e
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A04(r3)     // Catch:{ all -> 0x03a6 }
            A03(r1)     // Catch:{ all -> 0x03a6 }
            goto L_0x0391
        L_0x033e:
            X.0kg r1 = new X.0kg     // Catch:{ all -> 0x03a6 }
            r1.<init>()     // Catch:{ all -> 0x03a6 }
            r1.A00(r5)     // Catch:{ all -> 0x03a6 }
            X.0hU r0 = X.C09510hU.CHECK_SERVER_FOR_NEW_DATA     // Catch:{ all -> 0x03a6 }
            r1.A02 = r0     // Catch:{ all -> 0x03a6 }
            X.0l8 r0 = r5.A03     // Catch:{ all -> 0x03a6 }
            r1.A04 = r0     // Catch:{ all -> 0x03a6 }
            com.facebook.http.interfaces.RequestPriority r0 = r5.A02     // Catch:{ all -> 0x03a6 }
            r1.A03 = r0     // Catch:{ all -> 0x03a6 }
            com.facebook.messaging.service.model.FetchThreadListParams r0 = new com.facebook.messaging.service.model.FetchThreadListParams     // Catch:{ all -> 0x03a6 }
            r0.<init>(r1)     // Catch:{ all -> 0x03a6 }
            android.os.Bundle r1 = new android.os.Bundle     // Catch:{ all -> 0x03a6 }
            r1.<init>()     // Catch:{ all -> 0x03a6 }
            r1.putParcelable(r8, r0)     // Catch:{ all -> 0x03a6 }
            X.0ln r0 = new X.0ln     // Catch:{ all -> 0x03a6 }
            r0.<init>(r9, r1)     // Catch:{ all -> 0x03a6 }
            r1 = r26
            com.facebook.fbservice.service.OperationResult r0 = r1.BAz(r0)     // Catch:{ all -> 0x03a6 }
            java.lang.Object r3 = r0.A08()     // Catch:{ all -> 0x03a6 }
            com.facebook.messaging.service.model.FetchThreadListResult r3 = (com.facebook.messaging.service.model.FetchThreadListResult) r3     // Catch:{ all -> 0x03a6 }
            java.lang.String r1 = "DbServiceHandler.handleFetchThreadList#insertData"
            r0 = 1101668468(0x41aa2074, float:21.265846)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x03a6 }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BJD     // Catch:{ all -> 0x039e }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x039e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x039e }
            X.2ij r0 = (X.C52232ij) r0     // Catch:{ all -> 0x039e }
            com.facebook.messaging.service.model.FetchThreadListResult r0 = r0.A0P(r3)     // Catch:{ all -> 0x039e }
            com.facebook.fbservice.service.OperationResult r1 = com.facebook.fbservice.service.OperationResult.A04(r0)     // Catch:{ all -> 0x039e }
            r0 = 472396345(0x1c283239, float:5.565145E-22)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x03a6 }
        L_0x0391:
            r0 = -1925616131(0xffffffff8d396dfd, float:-5.713992E-31)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x03ae }
            r0 = 233585723(0xdec3c3b, float:1.4559123E-30)
        L_0x039a:
            X.C005505z.A00(r0)
            return r1
        L_0x039e:
            r1 = move-exception
            r0 = -1042263870(0xffffffffc1e050c2, float:-28.039433)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x03a6 }
        L_0x03a5:
            throw r1     // Catch:{ all -> 0x03a6 }
        L_0x03a6:
            r1 = move-exception
            r0 = 1127444917(0x433371b5, float:179.44417)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x03ae }
            throw r1     // Catch:{ all -> 0x03ae }
        L_0x03ae:
            r1 = move-exception
            r0 = -1384882183(0xffffffffad745ff9, float:-1.3891104E-11)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12370pE.A0x(X.0ln, X.1cz):com.facebook.fbservice.service.OperationResult");
    }

    public OperationResult A0y(C11060ln r34, C27311cz r35) {
        AnonymousClass04b A07;
        C27311cz r32;
        int i;
        ThreadKey threadKey;
        AnonymousClass04b r1;
        HashMap hashMap;
        FetchThreadParams fetchThreadParams;
        MessagesCollection A022;
        boolean z;
        MessagesCollection messagesCollection;
        MessagesCollection messagesCollection2;
        ((C60222wn) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B4i, this.A00)).A04();
        C11060ln r6 = r34;
        Bundle bundle = r6.A00;
        CallerContext callerContext = r6.A01;
        FetchThreadParams fetchThreadParams2 = (FetchThreadParams) bundle.getParcelable("fetchThreadParams");
        FetchThreadParams fetchThreadParams3 = fetchThreadParams2;
        ImmutableSet immutableSet = fetchThreadParams2.A04.A00;
        C24971Xv it = immutableSet.iterator();
        while (it.hasNext()) {
            C08770fv.A04(this.A02, ((ThreadKey) it.next()).A0J().hashCode(), "db_thread");
        }
        ((C12450pN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AwM, this.A00)).A04(C12500pT.A05, "fetchThreads (DSH). %s", immutableSet);
        ArrayList arrayList = new ArrayList();
        AnonymousClass04b r21 = new AnonymousClass04b();
        AnonymousClass04b r20 = new AnonymousClass04b();
        AnonymousClass04b r12 = new AnonymousClass04b();
        HashSet hashSet = new HashSet(immutableSet);
        AnonymousClass04b r3 = new AnonymousClass04b();
        C24971Xv it2 = immutableSet.iterator();
        while (it2.hasNext()) {
            r3.put((ThreadKey) it2.next(), new HashMap());
        }
        C005505z.A03("DbServiceHandler.handleFetchThread", 616116590);
        FetchThreadParams fetchThreadParams4 = fetchThreadParams3;
        try {
            int i2 = fetchThreadParams4.A01;
            ImmutableSet immutableSet2 = fetchThreadParams4.A04.A00;
            Preconditions.checkNotNull(immutableSet2);
            if (immutableSet2.isEmpty()) {
                A07 = new AnonymousClass04b();
            } else {
                long now = AnonymousClass06A.A00.now();
                A07 = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, this.A00)).A07(immutableSet2, i2);
                long now2 = (AnonymousClass06A.A00.now() - now) / ((long) Math.max(immutableSet2.size(), 1));
                int size = A07.size();
                for (int i3 = 0; i3 < size; i3++) {
                    Map map = (Map) r3.get((ThreadKey) A07.A07(i3));
                    map.put("fetch_location", C52172id.A00(AnonymousClass07B.A0C));
                    map.put("thread_db_duration", Long.toString(now2));
                    ((FetchThreadResult) A07.A09(i3)).A00 = map;
                }
            }
            AnonymousClass04b r15 = r21;
            AnonymousClass04b r14 = r20;
            FetchThreadParams fetchThreadParams5 = fetchThreadParams3;
            C09510hU r13 = fetchThreadParams5.A02;
            int i4 = fetchThreadParams5.A01;
            boolean booleanValue = ((Boolean) this.A03.get()).booleanValue();
            int size2 = A07.size();
            for (int i5 = 0; i5 < size2; i5++) {
                ThreadKey threadKey2 = (ThreadKey) A07.A07(i5);
                FetchThreadResult fetchThreadResult = (FetchThreadResult) A07.A09(i5);
                ThreadSummary threadSummary = fetchThreadResult.A05;
                if (!booleanValue || threadSummary == null || (messagesCollection2 = fetchThreadResult.A03) == null || !messagesCollection2.A09(i4) || !fetchThreadResult.A05.A14) {
                    if (!ThreadKey.A0C(threadKey2)) {
                        if (FetchThreadResult.A01(fetchThreadParams3, fetchThreadResult)) {
                            r15.put(threadKey2, fetchThreadResult);
                        } else if (r13 != C09510hU.DO_NOT_CHECK_SERVER) {
                            r14.put(threadKey2, fetchThreadResult);
                        }
                    }
                    r12.put(threadKey2, fetchThreadResult);
                } else {
                    AnonymousClass162 r2 = new AnonymousClass162();
                    r2.A07 = AnonymousClass0u3.LOCAL_DISK_CACHE;
                    r2.A02 = TriState.YES;
                    TriState triState = TriState.NO;
                    r2.A04 = triState;
                    r2.A06 = triState;
                    DataFetchDisposition dataFetchDisposition = new DataFetchDisposition(r2);
                    C61222yX r22 = new C61222yX(fetchThreadResult);
                    r22.A01 = dataFetchDisposition;
                    r22.A00 = this.A01.now();
                    r12.put(threadKey2, r22.A00());
                }
            }
            int size3 = r12.size();
            for (int i6 = 0; i6 < size3; i6++) {
                arrayList.add(r12.A09(i6));
                hashSet.remove(r12.A07(i6));
            }
            r32 = r35;
            if (!r20.isEmpty()) {
                r1 = new AnonymousClass04b();
                hashMap = new HashMap();
                boolean z2 = false;
                if (fetchThreadParams3.A02 == C09510hU.STALE_DATA_OKAY) {
                    z2 = true;
                }
                int size4 = r14.size();
                for (int i7 = 0; i7 < size4; i7++) {
                    ThreadKey threadKey3 = (ThreadKey) r14.A07(i7);
                    FetchThreadResult fetchThreadResult2 = (FetchThreadResult) r14.A09(i7);
                    if (fetchThreadResult2.A05 != null && (messagesCollection = fetchThreadResult2.A03) != null && !messagesCollection.A08() && !fetchThreadResult2.A05.A0O.A01()) {
                        if (z2) {
                            r1.put(threadKey3, fetchThreadResult2);
                        } else {
                            hashMap.put(threadKey3, fetchThreadResult2);
                        }
                    }
                }
                if (!hashMap.isEmpty()) {
                    ThreadCriteria threadCriteria = fetchThreadParams3.A04;
                    C60292wu r10 = new C60292wu();
                    r10.A00(fetchThreadParams3);
                    r10.A03 = new ThreadCriteria(threadCriteria.A01, hashMap.keySet());
                    fetchThreadParams = new FetchThreadParams(r10);
                    C27311cz r8 = r32;
                    ArrayList<FetchThreadResult> arrayList2 = new ArrayList<>(hashMap.size());
                    HashMap hashMap2 = new HashMap();
                    C09510hU r142 = fetchThreadParams.A02;
                    for (Map.Entry entry : hashMap.entrySet()) {
                        ThreadKey threadKey4 = (ThreadKey) entry.getKey();
                        FetchThreadResult fetchThreadResult3 = (FetchThreadResult) entry.getValue();
                        if (r142 == C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
                            z = true;
                        } else {
                            z = fetchThreadResult3.A02.A04.asBoolean(false);
                        }
                        if (z) {
                            hashMap2.put(threadKey4, fetchThreadResult3);
                        } else {
                            arrayList2.add(fetchThreadResult3);
                        }
                    }
                    ThreadCriteria threadCriteria2 = fetchThreadParams.A04;
                    C60292wu r132 = new C60292wu();
                    r132.A00(fetchThreadParams);
                    r132.A01 = C09510hU.CHECK_SERVER_FOR_NEW_DATA;
                    r132.A03 = new ThreadCriteria(threadCriteria2.A01, hashMap2.keySet());
                    FetchThreadParams fetchThreadParams6 = new FetchThreadParams(r132);
                    Bundle bundle2 = new Bundle();
                    bundle2.putParcelable("fetchThreadParams", fetchThreadParams6);
                    ArrayList<FetchThreadResult> A0A = r8.BAz(new C11060ln("fetch_threads", bundle2, null, null, callerContext, null)).A0A();
                    if (A0A != null) {
                        for (FetchThreadResult fetchThreadResult4 : A0A) {
                            ThreadSummary threadSummary2 = fetchThreadResult4.A05;
                            if (threadSummary2 != null) {
                                FetchThreadResult fetchThreadResult5 = (FetchThreadResult) hashMap2.get(threadSummary2.A0S);
                                if (fetchThreadResult5 != null) {
                                    A022 = fetchThreadResult5.A03;
                                } else {
                                    A022 = MessagesCollection.A02(threadSummary2.A0S);
                                }
                                Message A023 = A02(A022);
                                SQLiteDatabase A062 = ((C25771aN) this.A05.get()).A06();
                                C007406x.A01(A062, -1715874951);
                                try {
                                    A05(this, A023, fetchThreadResult4);
                                    ((C52232ij) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BJD, this.A00)).A0X(fetchThreadResult5, fetchThreadResult4, "fetchMoreRecentMessagesFromServerIfNeeded");
                                    A062.setTransactionSuccessful();
                                    C007406x.A02(A062, -1652909740);
                                    if (A022 == null) {
                                        A022 = fetchThreadResult4.A03;
                                        if (A022 == null) {
                                            th = new NullPointerException();
                                            throw th;
                                        }
                                    } else {
                                        MessagesCollection messagesCollection3 = fetchThreadResult4.A03;
                                        if (messagesCollection3 != null) {
                                            A022 = AnonymousClass0mB.A00((AnonymousClass0mB) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BFT, this.A00), messagesCollection3, A022, true);
                                        }
                                    }
                                    C61222yX A002 = FetchThreadResult.A00();
                                    A002.A01 = DataFetchDisposition.A0G;
                                    A002.A04 = fetchThreadResult4.A05;
                                    A002.A02 = A022;
                                    A002.A06 = fetchThreadResult4.A07;
                                    A002.A00 = this.A01.now();
                                    arrayList2.add(A002.A00());
                                } catch (Throwable th) {
                                    th = th;
                                    C007406x.A02(A062, 1934049121);
                                }
                            }
                        }
                    }
                    for (FetchThreadResult A063 : arrayList2) {
                        A06(this, fetchThreadParams3, A063, r32);
                    }
                    AnonymousClass04b A072 = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, this.A00)).A07(hashMap.keySet(), fetchThreadParams3.A01);
                    int size5 = A072.size();
                    for (int i8 = 0; i8 < size5; i8++) {
                        Object A073 = A072.A07(i8);
                        C61222yX r7 = new C61222yX((FetchThreadResult) A072.A09(i8));
                        r7.A01 = DataFetchDisposition.A0G;
                        r1.put(A073, r7.A00());
                    }
                }
                int size6 = r1.size();
                for (int i9 = 0; i9 < size6; i9++) {
                    arrayList.add(r1.A09(i9));
                    hashSet.remove(r1.A07(i9));
                }
            }
        } catch (IOException e) {
            if (fetchThreadParams.A03 != C09510hU.CHECK_SERVER_FOR_NEW_DATA) {
                for (ThreadKey threadKey5 : hashMap.keySet()) {
                    FetchThreadResult fetchThreadResult6 = (FetchThreadResult) hashMap.get(threadKey5);
                    C61222yX A003 = FetchThreadResult.A00();
                    A003.A01 = DataFetchDisposition.A09;
                    A003.A04 = fetchThreadResult6.A05;
                    A003.A02 = fetchThreadResult6.A03;
                    A003.A07 = fetchThreadResult6.A00;
                    A003.A06 = fetchThreadResult6.A07;
                    A003.A00 = fetchThreadResult6.A01;
                    r1.put(threadKey5, A003.A00());
                }
            } else {
                throw e;
            }
        } catch (Throwable th2) {
            C005505z.A00(264662857);
            throw th2;
        }
        if (!hashSet.isEmpty()) {
            String str = r6.A04;
            String str2 = r6.A05;
            Bundle bundle3 = r6.A00;
            C16890xw r102 = r6.A03;
            CallerContext callerContext2 = r6.A01;
            C11030ld r122 = r6.A02;
            C60292wu r62 = new C60292wu();
            FetchThreadParams fetchThreadParams7 = fetchThreadParams3;
            r62.A00(fetchThreadParams7);
            r62.A03 = new ThreadCriteria(fetchThreadParams7.A04.A01, hashSet);
            bundle3.putParcelable("fetchThreadParams", new FetchThreadParams(r62));
            C11060ln r63 = new C11060ln(str2, bundle3, str, r102, callerContext2, r122);
            AnonymousClass04b r64 = new AnonymousClass04b();
            r64.A0B(r21);
            r64.A0B(r20);
            int i10 = ((FetchThreadParams) r63.A00.getParcelable("fetchThreadParams")).A01;
            ArrayList arrayList3 = new ArrayList();
            OperationResult BAz = r32.BAz(r63);
            ArrayList<FetchThreadResult> arrayList4 = new ArrayList<>();
            ArrayList A0A2 = BAz.A0A();
            if (A0A2 != null) {
                arrayList4.addAll(A0A2);
            }
            for (FetchThreadResult fetchThreadResult7 : arrayList4) {
                if (fetchThreadResult7 != FetchThreadResult.A09) {
                    ThreadSummary threadSummary3 = fetchThreadResult7.A05;
                    FetchThreadResult fetchThreadResult8 = null;
                    if (threadSummary3 != null) {
                        threadKey = threadSummary3.A0S;
                        fetchThreadResult8 = (FetchThreadResult) r64.get(threadKey);
                    } else {
                        threadKey = null;
                    }
                    boolean z3 = false;
                    if (fetchThreadResult8 != null) {
                        MessagesCollection messagesCollection4 = fetchThreadResult8.A03;
                        if (messagesCollection4 != null && !messagesCollection4.A08()) {
                            z3 = true;
                        }
                        A07(fetchThreadResult8, fetchThreadResult7);
                    }
                    Map map2 = fetchThreadResult7.A00;
                    HashMap hashMap3 = new HashMap();
                    if (map2 != null) {
                        hashMap3.putAll(map2);
                    }
                    if (r3.containsKey(threadKey)) {
                        hashMap3.put("thread_db_duration", ((Map) r3.get(threadKey)).get("thread_db_duration"));
                    }
                    fetchThreadResult7.A00 = hashMap3;
                    if (threadSummary3 == null) {
                        C61222yX r16 = new C61222yX(fetchThreadResult7);
                        r16.A07 = hashMap3;
                        r16.A01 = DataFetchDisposition.A0G;
                        arrayList3.add(r16.A00());
                    } else if (z3) {
                        C61222yX r17 = new C61222yX(((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, this.A00)).A0F(threadKey, i10));
                        r17.A07 = hashMap3;
                        r17.A01 = DataFetchDisposition.A0G;
                        arrayList3.add(r17.A00());
                    } else {
                        C61222yX r18 = new C61222yX(fetchThreadResult7);
                        r18.A07 = hashMap3;
                        r18.A01 = DataFetchDisposition.A0G;
                        arrayList3.add(r18.A00());
                    }
                }
            }
            arrayList.addAll(arrayList3);
        }
        if (arrayList.isEmpty()) {
            arrayList.add(FetchThreadResult.A09);
        }
        OperationResult A052 = OperationResult.A05(arrayList);
        if (hashSet.isEmpty()) {
            A03(A052);
            i = -19766471;
        } else {
            C180878aL.A00(A052);
            i = -1472738964;
        }
        C005505z.A00(i);
        return A052;
    }

    public OperationResult A0z(C11060ln r14, C27311cz r15) {
        OperationResult operationResult;
        int i;
        FetchMessageAfterTimestampParams fetchMessageAfterTimestampParams = (FetchMessageAfterTimestampParams) r14.A00.getParcelable("fetchMessageAfterTimestampParams");
        ThreadKey threadKey = fetchMessageAfterTimestampParams.A01;
        long j = fetchMessageAfterTimestampParams.A00;
        ((C12450pN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AwM, this.A00)).A04(C12500pT.A05, "handleFetchMessageAfterTimestamp (DSH). %s", threadKey);
        C005505z.A03("DbServiceHandler.handleFetchMessageAfterTimestamp", 1922127757);
        try {
            FetchMoreMessagesResult A0E = ((C26691br) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcJ, this.A00)).A0E(threadKey, j, (long) -1, 2, " ASC");
            MessagesCollection messagesCollection = A0E.A02;
            if (messagesCollection != null && messagesCollection.A04() == 2 && messagesCollection.A07(1).A03 == j) {
                operationResult = OperationResult.A04(new FetchMessageResult(AnonymousClass102.FROM_CACHE_UP_TO_DATE, messagesCollection.A07(0), A0E.A00));
                i = -1519287536;
            } else {
                operationResult = r15.BAz(r14);
                i = 875239172;
            }
            C005505z.A00(i);
            return operationResult;
        } catch (Throwable th) {
            C005505z.A00(-1744769902);
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0014, code lost:
        if (r0.A08() != false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.fbservice.service.OperationResult A00(X.C12370pE r7, X.C11060ln r8, X.C27311cz r9, com.facebook.messaging.service.model.FetchThreadResult r10, long r11, int r13) {
        /*
            com.facebook.fbservice.service.OperationResult r3 = r9.BAz(r8)
            java.lang.Object r6 = r3.A08()
            com.facebook.messaging.service.model.FetchThreadResult r6 = (com.facebook.messaging.service.model.FetchThreadResult) r6
            com.facebook.messaging.model.messages.MessagesCollection r0 = r10.A03
            r5 = 1
            if (r0 == 0) goto L_0x0016
            boolean r0 = r0.A08()
            r2 = 1
            if (r0 == 0) goto L_0x0017
        L_0x0016:
            r2 = 0
        L_0x0017:
            r7.A07(r10, r6)
            java.util.Map r0 = r6.A00
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            if (r0 == 0) goto L_0x0026
            r4.putAll(r0)
        L_0x0026:
            java.lang.String r1 = java.lang.Long.toString(r11)
            java.lang.String r0 = "thread_db_duration"
            r4.put(r0, r1)
            r6.A00 = r4
            com.facebook.messaging.model.threads.ThreadSummary r0 = r6.A05
            if (r0 == 0) goto L_0x005e
            if (r2 == 0) goto L_0x005e
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r0.A0S
            int r1 = X.AnonymousClass1Y3.AcJ
            X.0UN r0 = r7.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.1br r0 = (X.C26691br) r0
            com.facebook.messaging.service.model.FetchThreadResult r0 = r0.A0F(r2, r13)
            X.2yX r1 = new X.2yX
            r1.<init>(r0)
        L_0x004c:
            r1.A07 = r4
            com.facebook.fbservice.results.DataFetchDisposition r0 = com.facebook.fbservice.results.DataFetchDisposition.A0G
            r1.A01 = r0
            com.facebook.messaging.service.model.FetchThreadResult r0 = r1.A00()
            com.facebook.fbservice.service.OperationResult r0 = com.facebook.fbservice.service.OperationResult.A04(r0)
            A04(r0, r3)
            return r0
        L_0x005e:
            X.2yX r1 = new X.2yX
            r1.<init>(r6)
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12370pE.A00(X.0pE, X.0ln, X.1cz, com.facebook.messaging.service.model.FetchThreadResult, long, int):com.facebook.fbservice.service.OperationResult");
    }
}
