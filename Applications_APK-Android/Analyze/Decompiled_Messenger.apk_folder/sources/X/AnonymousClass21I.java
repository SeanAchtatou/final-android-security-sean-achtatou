package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.21I  reason: invalid class name */
public final class AnonymousClass21I {
    private static volatile AnonymousClass21I A01;
    public AnonymousClass235 A00;

    public static final AnonymousClass21I A00(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass21I.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = new AnonymousClass21I();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A01(String str, String str2, Integer num, String str3) {
        AnonymousClass235 r2 = this.A00;
        if (r2 == null) {
            return;
        }
        if (num == AnonymousClass07B.A01) {
            if (str != null) {
                AnonymousClass21H.A02(r2.A00, str, null, str3);
            }
        } else if (str != null) {
            AnonymousClass21H.A02(r2.A00, str, str2, str3);
        }
    }
}
