package X;

import android.os.Process;
import com.facebook.systrace.TraceDirect;
import java.lang.reflect.Method;

/* renamed from: X.08Z  reason: invalid class name */
public final class AnonymousClass08Z {
    public static long A00;

    static {
        if (C000800m.A03) {
            Method method = C000800m.A02;
            C000300h.A01(method);
            C000800m.A00(method, true);
        }
        C001000r.A01(false);
    }

    public static void A00(long j, String str) {
        if (!A05(j)) {
            return;
        }
        if (TraceDirect.checkNative()) {
            TraceDirect.nativeBeginSection(str);
            return;
        }
        AnonymousClass09R r1 = new AnonymousClass09R('B');
        r1.A01(Process.myPid());
        r1.A03(str);
        AnonymousClass0HL.A00(r1.toString());
    }

    public static void A01(long j, String str, int i) {
        if (A05(j)) {
            TraceDirect.asyncTraceBegin(str, i, 0);
        }
    }

    public static void A02(long j, String str, int i) {
        if (A05(j)) {
            TraceDirect.asyncTraceEnd(str, i, 0);
        }
    }

    public static void A03(long j, String str, int i) {
        if (!A05(j)) {
            return;
        }
        if (TraceDirect.checkNative()) {
            TraceDirect.nativeTraceCounter(str, i);
            return;
        }
        AnonymousClass09R r1 = new AnonymousClass09R('C');
        r1.A01(Process.myPid());
        r1.A03(str);
        r1.A01(i);
        AnonymousClass0HL.A00(r1.toString());
    }

    public static void A04(long j, String str, String str2, int i) {
        if (!A05(j)) {
            return;
        }
        if (TraceDirect.checkNative()) {
            TraceDirect.nativeTraceMetadata(str, str2, i);
            return;
        }
        AnonymousClass09R r1 = new AnonymousClass09R('M');
        r1.A01(Process.myPid());
        r1.A03(str);
        r1.A01(i);
        r1.A03(str2);
        AnonymousClass0HL.A00(r1.toString());
    }

    public static boolean A05(long j) {
        if (C001000r.A02(j) || (j & A00) != 0) {
            return true;
        }
        return false;
    }
}
