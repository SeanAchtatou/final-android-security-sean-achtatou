package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.NicknamesMap;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;

/* renamed from: X.13A  reason: invalid class name */
public final class AnonymousClass13A implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        String str;
        ImmutableMap immutableMap = null;
        if (parcel.readInt() != 0) {
            str = parcel.readString();
        } else {
            str = null;
        }
        if (parcel.readInt() != 0) {
            HashMap hashMap = new HashMap();
            C417826y.A0P(parcel, hashMap);
            immutableMap = ImmutableMap.copyOf(hashMap);
        }
        return new NicknamesMap(str, immutableMap);
    }

    public Object[] newArray(int i) {
        return new NicknamesMap[i];
    }
}
