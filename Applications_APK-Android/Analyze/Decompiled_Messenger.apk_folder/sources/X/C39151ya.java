package X;

import android.view.View;
import com.facebook.messaging.model.threadkey.ThreadKey;

/* renamed from: X.1ya  reason: invalid class name and case insensitive filesystem */
public final class C39151ya implements C60262wr {
    public final /* synthetic */ ThreadKey A00;
    public final /* synthetic */ C418127b A01;

    public C39151ya(C418127b r1, ThreadKey threadKey) {
        this.A01 = r1;
        this.A00 = threadKey;
    }

    public void onClick(View view) {
        this.A01.CJc(this.A00);
    }
}
