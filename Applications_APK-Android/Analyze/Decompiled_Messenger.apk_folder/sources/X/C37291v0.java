package X;

import com.facebook.common.util.TriState;
import com.facebook.messaging.model.messagemetadata.MessagePlatformPersona;
import com.facebook.messaging.typingattribution.TypingAttributionData;
import com.facebook.user.model.UserKey;

/* renamed from: X.1v0  reason: invalid class name and case insensitive filesystem */
public final class C37291v0 {
    public volatile int A00;
    public volatile long A01;
    public volatile long A02;
    public volatile long A03 = -1;
    public volatile long A04 = -1;
    public volatile long A05;
    public volatile TriState A06 = TriState.UNSET;
    public volatile MessagePlatformPersona A07;
    public volatile TypingAttributionData A08;
    public volatile UserKey A09;
    public volatile boolean A0A;
    public volatile boolean A0B;
    public volatile boolean A0C;
}
