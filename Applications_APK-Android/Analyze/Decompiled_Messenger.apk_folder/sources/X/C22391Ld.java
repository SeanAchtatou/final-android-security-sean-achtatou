package X;

import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;

/* renamed from: X.1Ld  reason: invalid class name and case insensitive filesystem */
public final class C22391Ld {
    public static final C15940wE A07 = new C15940wE(100);
    public int A00 = Integer.MAX_VALUE;
    public int A01 = 2;
    public int A02 = 0;
    public int A03 = 2;
    public Layout A04 = null;
    public boolean A05 = true;
    public final AnonymousClass1QX A06 = new AnonymousClass1QX();

    public static StaticLayout A00(CharSequence charSequence, int i, int i2, TextPaint textPaint, int i3, Layout.Alignment alignment, float f, float f2, boolean z, TextUtils.TruncateAt truncateAt, int i4, int i5, C22311Kv r22) {
        Layout.Alignment alignment2 = alignment;
        int i6 = i3;
        TextPaint textPaint2 = textPaint;
        int i7 = i2;
        CharSequence charSequence2 = charSequence;
        int i8 = i;
        int i9 = i4;
        int i10 = i5;
        TextUtils.TruncateAt truncateAt2 = truncateAt;
        boolean z2 = z;
        float f3 = f2;
        float f4 = f;
        try {
            return new StaticLayout(charSequence2, i8, i7, textPaint2, i6, alignment2, A01(r22), f4, f3, z2, truncateAt2, i9, i10);
        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("utext_close")) {
                return new StaticLayout(charSequence2, i8, i7, textPaint2, i6, alignment2, A01(r22), f4, f3, z2, truncateAt2, i9, i10);
            }
            throw e;
        }
    }

    public void A09(int i) {
        int i2 = 1;
        if (i <= 0) {
            i2 = 0;
        }
        AnonymousClass1QX r1 = this.A06;
        if (r1.A0F != i || r1.A0E != i2) {
            r1.A0F = i;
            r1.A0E = i2;
            this.A04 = null;
        }
    }

    public static TextDirectionHeuristic A01(C22311Kv r1) {
        if (r1 == C22181Kf.A04) {
            return TextDirectionHeuristics.LTR;
        }
        if (r1 == C22181Kf.A05) {
            return TextDirectionHeuristics.RTL;
        }
        if (r1 != C22181Kf.A01) {
            if (r1 == C22181Kf.A02) {
                return TextDirectionHeuristics.FIRSTSTRONG_RTL;
            }
            if (r1 == C22181Kf.A00) {
                return TextDirectionHeuristics.ANYRTL_LTR;
            }
            if (r1 == C22181Kf.A03) {
                return TextDirectionHeuristics.LOCALE;
            }
        }
        return TextDirectionHeuristics.FIRSTSTRONG_LTR;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:68:0x01c0 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:133:0x00e5 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:125:0x01ce */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:130:0x02c6 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:129:0x02c6 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:132:0x02c6 */
    /* JADX WARN: Type inference failed for: r1v34, types: [android.text.BoringLayout] */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0072, code lost:
        if (android.os.Build.VERSION.SDK_INT >= 23) goto L_0x0315;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01ce, code lost:
        r4 = r1.getLineStart(0);
        r2 = r1.getLineCount();
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01d9, code lost:
        if (r11 >= r2) goto L_0x0217;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01db, code lost:
        r0 = r1.getLineEnd(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01df, code lost:
        if (r0 >= r4) goto L_0x01e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01e2, code lost:
        r11 = r11 + 1;
        r4 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01e6, code lost:
        r4 = android.text.StaticLayout.class;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        r2 = r4.getDeclaredField("mLines");
        r2.setAccessible(true);
        r0 = r4.getDeclaredField("mColumns");
        r0.setAccessible(true);
        r10 = (int[]) r2.get(r1);
        r7 = r0.getInt(r1);
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0205, code lost:
        if (r6 >= r7) goto L_0x0219;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0207, code lost:
        r5 = (r7 * r11) + r6;
        r4 = r5 + r7;
        r2 = r10[r5];
        r10[r5] = r10[r4];
        r10[r4] = r2;
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0217, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0219, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x021a, code lost:
        if (r0 != false) goto L_0x00e5;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:110:0x02a9 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:66:0x01a3 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.text.Layout A02() {
        /*
            r38 = this;
            r9 = r38
            boolean r0 = r9.A05
            if (r0 == 0) goto L_0x000b
            android.text.Layout r0 = r9.A04
            if (r0 == 0) goto L_0x000b
            return r0
        L_0x000b:
            X.1QX r0 = r9.A06
            java.lang.CharSequence r0 = r0.A0J
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            r23 = 0
            if (r0 == 0) goto L_0x0018
            return r23
        L_0x0018:
            boolean r0 = r9.A05
            r8 = 0
            r2 = 1
            if (r0 == 0) goto L_0x003a
            X.1QX r0 = r9.A06
            java.lang.CharSequence r1 = r0.A0J
            boolean r0 = r1 instanceof android.text.Spannable
            if (r0 == 0) goto L_0x003a
            r3 = r1
            android.text.Spannable r3 = (android.text.Spannable) r3
            int r1 = r1.length()
            int r1 = r1 - r2
            java.lang.Class<android.text.style.ClickableSpan> r0 = android.text.style.ClickableSpan.class
            java.lang.Object[] r0 = r3.getSpans(r8, r1, r0)
            android.text.style.ClickableSpan[] r0 = (android.text.style.ClickableSpan[]) r0
            int r0 = r0.length
            if (r0 <= 0) goto L_0x003a
            r8 = 1
        L_0x003a:
            boolean r0 = r9.A05
            if (r0 == 0) goto L_0x0055
            if (r8 != 0) goto L_0x0055
            X.1QX r0 = r9.A06
            int r16 = r0.hashCode()
            X.0wE r1 = X.C22391Ld.A07
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)
            java.lang.Object r0 = r1.A03(r0)
            android.text.Layout r0 = (android.text.Layout) r0
            if (r0 == 0) goto L_0x0057
            return r0
        L_0x0055:
            r16 = -1
        L_0x0057:
            X.1QX r4 = r9.A06
            boolean r0 = r4.A0L
            if (r0 == 0) goto L_0x0061
            r3 = 1
        L_0x005e:
            if (r3 != r2) goto L_0x0074
            goto L_0x0064
        L_0x0061:
            int r3 = r4.A0D
            goto L_0x005e
        L_0x0064:
            java.lang.CharSequence r1 = r4.A0J     // Catch:{ NullPointerException -> 0x006d }
            android.text.TextPaint r0 = r4.A0G     // Catch:{ NullPointerException -> 0x006d }
            android.text.BoringLayout$Metrics r23 = android.text.BoringLayout.isBoring(r1, r0)     // Catch:{ NullPointerException -> 0x006d }
            goto L_0x0074
        L_0x006d:
            r4 = move-exception
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 >= r0) goto L_0x0315
        L_0x0074:
            X.1QX r5 = r9.A06
            int r4 = r5.A0E
            if (r4 == 0) goto L_0x02f9
            if (r4 == r2) goto L_0x02f5
            r0 = 2
            if (r4 != r0) goto L_0x0309
            java.lang.CharSequence r1 = r5.A0J
            android.text.TextPaint r0 = r5.A0G
            float r0 = android.text.Layout.getDesiredWidth(r1, r0)
            double r0 = (double) r0
            double r0 = java.lang.Math.ceil(r0)
            int r4 = (int) r0
            X.1QX r0 = r9.A06
            int r0 = r0.A0F
            int r5 = java.lang.Math.min(r4, r0)
        L_0x0095:
            X.1QX r4 = r9.A06
            android.text.TextPaint r1 = r4.A0G
            r0 = 0
            int r0 = r1.getFontMetricsInt(r0)
            float r1 = (float) r0
            float r0 = r4.A0B
            float r1 = r1 * r0
            float r0 = r4.A0A
            float r1 = r1 + r0
            int r4 = java.lang.Math.round(r1)
            int r0 = r9.A01
            if (r0 != r2) goto L_0x02ed
            int r0 = r9.A00
            int r0 = r0 * r4
            int r1 = java.lang.Math.min(r5, r0)
        L_0x00b4:
            int r0 = r9.A03
            if (r0 != r2) goto L_0x02e5
            int r0 = r9.A02
            int r0 = r0 * r4
            int r19 = java.lang.Math.max(r1, r0)
        L_0x00bf:
            if (r23 == 0) goto L_0x00fc
            X.1QX r0 = r9.A06
            java.lang.CharSequence r6 = r0.A0J
            android.text.TextPaint r5 = r0.A0G
            android.text.Layout$Alignment r4 = r0.A06
            float r3 = r0.A0B
            float r2 = r0.A0A
            boolean r1 = r0.A0K
            android.text.TextUtils$TruncateAt r0 = r0.A0H
            r17 = r6
            r18 = r5
            r20 = r4
            r21 = r3
            r22 = r2
            r24 = r1
            r25 = r0
            r26 = r19
            android.text.BoringLayout r1 = android.text.BoringLayout.make(r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
        L_0x00e5:
            boolean r0 = r9.A05
            if (r0 == 0) goto L_0x00f6
            if (r8 != 0) goto L_0x00f6
            r9.A04 = r1
            X.0wE r2 = X.C22391Ld.A07
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)
            r2.A04(r0, r1)
        L_0x00f6:
            X.1QX r2 = r9.A06
            r0 = 1
            r2.A07 = r0
            return r1
        L_0x00fc:
            X.1QX r0 = r9.A06     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            java.lang.CharSequence r2 = r0.A0J     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            int r13 = r2.length()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            X.1QX r1 = r9.A06     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.TextPaint r0 = r1.A0G     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r37 = r0
            android.text.Layout$Alignment r0 = r1.A06     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r36 = r0
            float r0 = r1.A0B     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r35 = r0
            float r15 = r1.A0A     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            boolean r14 = r1.A0K     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.TextUtils$TruncateAt r7 = r1.A0H     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            X.1Kv r0 = r1.A0I     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r18 = r0
            int r12 = r1.A0C     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            int r11 = r1.A03     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            int r10 = r1.A04     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r6 = 0
            boolean r5 = r1.A08     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r4 = 0
            r17 = r19
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r0 = 23
            if (r1 < r0) goto L_0x0184
            r20 = r2
            r21 = r4
            r22 = r13
            r23 = r37
            r24 = r17
            android.text.StaticLayout$Builder r0 = android.text.StaticLayout.Builder.obtain(r20, r21, r22, r23, r24)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r1 = r36
            android.text.StaticLayout$Builder r1 = r0.setAlignment(r1)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r0 = r35
            android.text.StaticLayout$Builder r0 = r1.setLineSpacing(r15, r0)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.StaticLayout$Builder r0 = r0.setIncludePad(r14)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.StaticLayout$Builder r1 = r0.setEllipsize(r7)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r0 = r19
            android.text.StaticLayout$Builder r0 = r1.setEllipsizedWidth(r0)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.StaticLayout$Builder r1 = r0.setMaxLines(r3)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.TextDirectionHeuristic r0 = A01(r18)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.StaticLayout$Builder r0 = r1.setTextDirection(r0)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.StaticLayout$Builder r0 = r0.setBreakStrategy(r12)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.StaticLayout$Builder r0 = r0.setHyphenationFrequency(r11)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            android.text.StaticLayout$Builder r2 = r0.setIndents(r6, r6)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r0 = 26
            if (r1 < r0) goto L_0x0177
            r2.setJustificationMode(r10)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
        L_0x0177:
            r0 = 28
            if (r1 < r0) goto L_0x017e
            r2.setUseLineSpacingFromFallbacks(r5)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
        L_0x017e:
            android.text.StaticLayout r1 = r2.build()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            goto L_0x00e5
        L_0x0184:
            r24 = r19
            r30 = r19
            r31 = r3
            r20 = r2
            r21 = r4
            r22 = r13
            r23 = r37
            r25 = r36
            r26 = r35
            r27 = r15
            r28 = r14
            r29 = r7
            r32 = r18
            android.text.StaticLayout r1 = A00(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)     // Catch:{ LinkageError -> 0x01a3 }
            goto L_0x01c0
        L_0x01a3:
            android.text.StaticLayout r1 = new android.text.StaticLayout     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r25 = r19
            r31 = r19
            r20 = r1
            r21 = r2
            r22 = r4
            r23 = r13
            r24 = r37
            r26 = r36
            r27 = r35
            r28 = r15
            r29 = r14
            r30 = r7
            r20.<init>(r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
        L_0x01c0:
            if (r3 <= 0) goto L_0x01ce
        L_0x01c2:
            int r0 = r1.getLineCount()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            if (r0 <= r3) goto L_0x01ce
            int r0 = r1.getLineStart(r3)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            if (r0 < r13) goto L_0x021d
        L_0x01ce:
            r0 = 0
            int r4 = r1.getLineStart(r0)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            int r2 = r1.getLineCount()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r11 = 0
        L_0x01d8:
            r5 = 1
            if (r11 >= r2) goto L_0x0217
            int r0 = r1.getLineEnd(r11)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            if (r0 >= r4) goto L_0x01e2
            goto L_0x01e6
        L_0x01e2:
            int r11 = r11 + 1
            r4 = r0
            goto L_0x01d8
        L_0x01e6:
            java.lang.Class<android.text.StaticLayout> r4 = android.text.StaticLayout.class
            java.lang.String r0 = "mLines"
            java.lang.reflect.Field r2 = r4.getDeclaredField(r0)     // Catch:{ Exception -> 0x0217 }
            r2.setAccessible(r5)     // Catch:{ Exception -> 0x0217 }
            java.lang.String r0 = "mColumns"
            java.lang.reflect.Field r0 = r4.getDeclaredField(r0)     // Catch:{ Exception -> 0x0217 }
            r0.setAccessible(r5)     // Catch:{ Exception -> 0x0217 }
            java.lang.Object r10 = r2.get(r1)     // Catch:{ Exception -> 0x0217 }
            int[] r10 = (int[]) r10     // Catch:{ Exception -> 0x0217 }
            int r7 = r0.getInt(r1)     // Catch:{ Exception -> 0x0217 }
            r6 = 0
        L_0x0205:
            if (r6 >= r7) goto L_0x0219
            int r5 = r7 * r11
            int r5 = r5 + r6
            int r4 = r5 + r7
            r2 = r10[r5]     // Catch:{ Exception -> 0x0217 }
            r0 = r10[r4]     // Catch:{ Exception -> 0x0217 }
            r10[r5] = r0     // Catch:{ Exception -> 0x0217 }
            r10[r4] = r2     // Catch:{ Exception -> 0x0217 }
            int r6 = r6 + 1
            goto L_0x0205
        L_0x0217:
            r0 = 1
            goto L_0x021a
        L_0x0219:
            r0 = 0
        L_0x021a:
            if (r0 != 0) goto L_0x00e5
            goto L_0x01ce
        L_0x021d:
            if (r0 <= r4) goto L_0x022d
            int r5 = r0 + -1
            char r1 = r2.charAt(r5)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            boolean r1 = java.lang.Character.isSpace(r1)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            if (r1 == 0) goto L_0x022d
            r0 = r5
            goto L_0x021d
        L_0x022d:
            r24 = r19
            r30 = r19
            r31 = r3
            r20 = r2
            r21 = r4
            r22 = r0
            r23 = r37
            r25 = r36
            r26 = r35
            r27 = r15
            r28 = r14
            r29 = r7
            r32 = r18
            android.text.StaticLayout r1 = A00(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)     // Catch:{ LinkageError -> 0x024c }
            goto L_0x0269
        L_0x024c:
            r23 = r0
            android.text.StaticLayout r1 = new android.text.StaticLayout     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r25 = r19
            r31 = r19
            r20 = r1
            r21 = r2
            r22 = r4
            r24 = r37
            r26 = r36
            r27 = r35
            r28 = r15
            r29 = r14
            r30 = r7
            r20.<init>(r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
        L_0x0269:
            int r5 = r1.getLineCount()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            if (r5 < r3) goto L_0x02c6
            int r5 = r3 + -1
            int r5 = r1.getEllipsisCount(r5)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            if (r5 != 0) goto L_0x02c6
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r5.<init>()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            java.lang.CharSequence r1 = r2.subSequence(r4, r0)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r5.append(r1)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            java.lang.String r1 = " …"
            r5.append(r1)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            java.lang.String r20 = r5.toString()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            int r22 = r20.length()     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r21 = 0
            r24 = r19
            r30 = r19
            r31 = r3
            r23 = r37
            r25 = r36
            r26 = r35
            r27 = r15
            r28 = r14
            r29 = r7
            android.text.StaticLayout r1 = A00(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)     // Catch:{ LinkageError -> 0x02a9 }
            goto L_0x02c6
        L_0x02a9:
            android.text.StaticLayout r1 = new android.text.StaticLayout     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
            r25 = 0
            r28 = r19
            r34 = r19
            r23 = r1
            r24 = r20
            r26 = r22
            r27 = r37
            r29 = r36
            r30 = r35
            r31 = r15
            r32 = r14
            r33 = r7
            r23.<init>(r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34)     // Catch:{ IndexOutOfBoundsException -> 0x02c9 }
        L_0x02c6:
            r13 = r0
            goto L_0x01c2
        L_0x02c9:
            r4 = move-exception
            X.1QX r0 = r9.A06
            java.lang.CharSequence r0 = r0.A0J
            boolean r0 = r0 instanceof java.lang.String
            if (r0 != 0) goto L_0x0315
            java.lang.String r1 = "TextLayoutBuilder"
            java.lang.String r0 = "Hit bug #35412, retrying with Spannables removed"
            android.util.Log.e(r1, r0, r4)
            X.1QX r1 = r9.A06
            java.lang.CharSequence r0 = r1.A0J
            java.lang.String r0 = r0.toString()
            r1.A0J = r0
            goto L_0x00fc
        L_0x02e5:
            int r0 = r9.A02
            int r19 = java.lang.Math.max(r1, r0)
            goto L_0x00bf
        L_0x02ed:
            int r0 = r9.A00
            int r1 = java.lang.Math.min(r5, r0)
            goto L_0x00b4
        L_0x02f5:
            int r5 = r5.A0F
            goto L_0x0095
        L_0x02f9:
            java.lang.CharSequence r1 = r5.A0J
            android.text.TextPaint r0 = r5.A0G
            float r0 = android.text.Layout.getDesiredWidth(r1, r0)
            double r0 = (double) r0
            double r0 = java.lang.Math.ceil(r0)
            int r5 = (int) r0
            goto L_0x0095
        L_0x0309:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unexpected measure mode "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r4)
            r1.<init>(r0)
            throw r1
        L_0x0315:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22391Ld.A02():android.text.Layout");
    }

    public void A03(float f) {
        AnonymousClass1QX r2 = this.A06;
        if (r2.A09 == Float.MAX_VALUE && r2.A0A != f) {
            r2.A0A = f;
            this.A04 = null;
        }
    }

    public void A04(float f, float f2, float f3, int i) {
        this.A06.A00();
        AnonymousClass1QX r0 = this.A06;
        r0.A02 = f;
        r0.A00 = f2;
        r0.A01 = f3;
        r0.A05 = i;
        r0.A0G.setShadowLayer(f, f2, f3, i);
        this.A04 = null;
    }

    public void A05(int i) {
        AnonymousClass1QX r1 = this.A06;
        if (r1.A03 != i) {
            r1.A03 = i;
            if (Build.VERSION.SDK_INT >= 23) {
                this.A04 = null;
            }
        }
    }

    public void A06(int i) {
        AnonymousClass1QX r1 = this.A06;
        if (r1.A04 != i) {
            r1.A04 = i;
            if (Build.VERSION.SDK_INT >= 26) {
                this.A04 = null;
            }
        }
    }

    public void A07(int i) {
        this.A06.A00();
        this.A06.A0G.setColor(i);
        this.A04 = null;
    }

    public void A08(int i) {
        float f = (float) i;
        if (this.A06.A0G.getTextSize() != f) {
            this.A06.A00();
            this.A06.A0G.setTextSize(f);
            this.A04 = null;
        }
    }

    public void A0A(ColorStateList colorStateList) {
        int i;
        this.A06.A00();
        TextPaint textPaint = this.A06.A0G;
        if (colorStateList != null) {
            i = colorStateList.getDefaultColor();
        } else {
            i = C15320v6.MEASURED_STATE_MASK;
        }
        textPaint.setColor(i);
        this.A04 = null;
    }

    public void A0B(Typeface typeface) {
        if (this.A06.A0G.getTypeface() != typeface) {
            this.A06.A00();
            this.A06.A0G.setTypeface(typeface);
            this.A04 = null;
        }
    }

    public void A0C(Layout.Alignment alignment) {
        AnonymousClass1QX r1 = this.A06;
        if (r1.A06 != alignment) {
            r1.A06 = alignment;
            this.A04 = null;
        }
    }

    public void A0D(CharSequence charSequence) {
        if (charSequence != this.A06.A0J) {
            if (Build.VERSION.SDK_INT >= 21 && (charSequence instanceof SpannableStringBuilder)) {
                try {
                    charSequence.hashCode();
                } catch (NullPointerException e) {
                    throw new IllegalArgumentException("The given text contains a null span. Due to an Android framework bug, this will cause an exception later down the line.", e);
                }
            }
            if (charSequence == null || !charSequence.equals(this.A06.A0J)) {
                this.A06.A0J = charSequence;
                this.A04 = null;
            }
        }
    }
}
