package X;

import com.facebook.messaging.inbox2.items.InboxTrackableItem;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0v8  reason: invalid class name and case insensitive filesystem */
public final class C15340v8 {
    public final C15480vM A00;
    private final C17610zB A01 = new C17610zB();

    public static synchronized InboxTrackableItem A00(C15340v8 r3, InboxUnitItem inboxUnitItem) {
        InboxTrackableItem inboxTrackableItem;
        synchronized (r3) {
            Object A07 = r3.A01.A07(inboxUnitItem.A07().ArZ());
            Preconditions.checkNotNull(A07, "Item of type %s is not in the impression logging cache (must call notifyItemsCreated)", inboxUnitItem.A06());
            inboxTrackableItem = (InboxTrackableItem) A07;
        }
        return inboxTrackableItem;
    }

    public synchronized void A03(InboxUnitItem inboxUnitItem) {
        InboxTrackableItem A07 = inboxUnitItem.A07();
        this.A01.A0D(A07.ArZ(), A07);
    }

    public synchronized void A04(ImmutableList immutableList) {
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            A03((InboxUnitItem) it.next());
        }
    }

    public synchronized void A05(boolean z) {
        this.A00.A04(z);
    }

    public synchronized void A06(boolean z) {
        this.A00.A05(z);
    }

    public void A01(InboxUnitItem inboxUnitItem) {
        this.A00.A02(A00(this, inboxUnitItem));
    }

    public void A02(InboxUnitItem inboxUnitItem) {
        this.A00.A03(A00(this, inboxUnitItem));
    }

    public C15340v8(C15470vL r2, AnonymousClass1AT r3, C32871mT r4) {
        this.A00 = r2.A00(r3, r4);
    }
}
