package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.18n  reason: invalid class name and case insensitive filesystem */
public final class C195318n {
    private static volatile C195318n A01;
    public final AnonymousClass0jD A00;

    public static final C195318n A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C195318n.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C195318n(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C195318n(AnonymousClass1XY r2) {
        C12630pi.A02(r2);
        this.A00 = AnonymousClass0jD.A00(r2);
    }
}
