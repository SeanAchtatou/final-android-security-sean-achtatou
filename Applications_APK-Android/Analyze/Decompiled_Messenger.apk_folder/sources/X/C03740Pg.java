package X;

/* renamed from: X.0Pg  reason: invalid class name and case insensitive filesystem */
public abstract class C03740Pg {
    public abstract void nativeDisable();

    public abstract void nativeEnable(boolean z, boolean z2, boolean z3);

    public abstract boolean nativeIsEgressLigerCodecLoggerEnabled();

    public abstract boolean nativeIsIngressLigerCodecLoggerEnabled();

    public abstract boolean nativeIsTigonObserverEnabled();
}
