package X;

import android.content.Context;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.1l2  reason: invalid class name and case insensitive filesystem */
public final class C32051l2 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.carrier.CarrierMonitor$1";
    public final /* synthetic */ AnonymousClass125 A00;

    public void run() {
        try {
            if (((Context) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BCt, this.A00.A01)).checkCallingOrSelfPermission(TurboLoader.Locator.$const$string(10)) == 0) {
                AnonymousClass125.A05(this.A00, 17);
            } else {
                AnonymousClass125.A05(this.A00, 1);
            }
        } catch (RuntimeException unused) {
            AnonymousClass125.A05(this.A00, 1);
        }
    }

    public C32051l2(AnonymousClass125 r1) {
        this.A00 = r1;
    }
}
