package X;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0An  reason: invalid class name and case insensitive filesystem */
public interface C01510An extends ScheduledExecutorService, ExecutorService {
    C01940Cf C4X(Runnable runnable, long j, TimeUnit timeUnit);
}
