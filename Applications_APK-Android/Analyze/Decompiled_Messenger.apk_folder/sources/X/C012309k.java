package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

/* renamed from: X.09k  reason: invalid class name and case insensitive filesystem */
public final class C012309k {
    public final Context A00;
    public final C009207y A01;

    public static String A00(Intent intent) {
        String str;
        Bundle bundleExtra = intent.getBundleExtra("auth_bundle");
        if (bundleExtra == null) {
            str = "Invalid auth bundle";
        } else {
            PendingIntent pendingIntent = (PendingIntent) bundleExtra.getParcelable("auth_pending_intent");
            if (pendingIntent == null) {
                str = "Invalid auth intent";
            } else if (Build.VERSION.SDK_INT >= 17) {
                return pendingIntent.getCreatorPackage();
            } else {
                return pendingIntent.getTargetPackage();
            }
        }
        C010708t.A0I("SignatureAuthSecureIntent", str);
        return null;
    }

    public void A01(Intent intent) {
        PendingIntent activity = PendingIntent.getActivity(this.A00, 0, new Intent().setPackage(this.A00.getPackageName()), 134217728);
        Bundle bundle = new Bundle();
        bundle.putParcelable("auth_pending_intent", activity);
        intent.putExtra("auth_bundle", bundle);
    }

    public void A03(Intent intent, String str) {
        AnonymousClass0SR r0;
        if (AnonymousClass0AB.A01(this.A00, str, this.A01)) {
            intent.setPackage(str);
            C009207y r1 = this.A01;
            Context context = this.A00;
            A01(intent);
            if (r1.A09(context, intent)) {
                r0 = AnonymousClass0SR.A02;
            } else {
                r0 = AnonymousClass0SR.A01;
            }
            r0.A01();
        }
    }

    public C012309k(Context context, AnonymousClass09P r3) {
        C009207y r0;
        this.A00 = context;
        if (r3 == null) {
            r0 = C009207y.A01;
        } else {
            r0 = new C009207y(r3);
        }
        this.A01 = r0;
    }

    public void A02(Intent intent) {
        String packageName = intent.getComponent().getPackageName();
        if (packageName != null && AnonymousClass0AB.A01(this.A00, packageName, this.A01)) {
            A01(intent);
            this.A01.A00(this.A00, intent);
        }
    }

    public boolean A04(Intent intent) {
        return AnonymousClass0AB.A01(this.A00, A00(intent), this.A01);
    }
}
