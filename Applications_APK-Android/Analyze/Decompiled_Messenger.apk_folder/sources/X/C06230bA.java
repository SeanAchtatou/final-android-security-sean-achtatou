package X;

import android.content.Context;
import com.facebook.analytics2.logger.DefaultHandlerThreadFactory;
import com.facebook.flexiblesampling.SamplingResult;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0bA  reason: invalid class name and case insensitive filesystem */
public final class C06230bA {
    private static final C06360bN A0F = new C06360bN(0, 0, 0, 0);
    private static final C06360bN A0G;
    private static final C06360bN A0H = new C06360bN(0, 0, 0, 0);
    private static final C06360bN A0I;
    public C06830c9 A00;
    public C06840cA A01;
    public C11970oK A02;
    public C07660dv A03;
    public C07310dD A04;
    public final C06380bP A05;
    public final C06290bG A06;
    public final C06260bD A07;
    public final C07140ch A08;
    public final C07330dF A09;
    public final Class A0A;
    public final C06300bH A0B;
    public final C06400bR A0C;
    public final C06390bQ A0D;
    private final AnonymousClass0ZF A0E;

    public AnonymousClass0ZF A05(String str, boolean z, Integer num, boolean z2) {
        return A01(null, str, z, num, z2);
    }

    static {
        TimeUnit timeUnit = TimeUnit.MINUTES;
        A0I = new C06360bN(timeUnit.toMillis(15), timeUnit.toMillis(45), 0, timeUnit.toMillis(30));
        TimeUnit timeUnit2 = TimeUnit.MINUTES;
        A0G = new C06360bN(timeUnit2.toMillis(15), timeUnit2.toMillis(45), 0, timeUnit2.toMillis(30));
    }

    public C06230bA(C07320dE r39) {
        C06370bO.A01 = this;
        if (C06370bO.A02 == null) {
            C06370bO.A02 = new C06370bO();
        }
        this.A0E = C06370bO.A02;
        this.A05 = new AnonymousClass0ZG(6);
        C07320dE r13 = r39;
        C07330dF r0 = r13.A0H;
        AnonymousClass064.A00(r0);
        this.A09 = r0;
        C06300bH r02 = r13.A01;
        AnonymousClass064.A00(r02);
        this.A0B = r02;
        this.A06 = r13.A00;
        this.A0D = C06390bQ.A00();
        this.A08 = r13.A0E;
        C06260bD r03 = r13.A0B;
        AnonymousClass064.A00(r03);
        this.A07 = r03;
        this.A0A = r13.A0N;
        this.A02 = r13.A0I;
        this.A04 = r13.A0K;
        this.A03 = r13.A0J;
        C06830c9 r04 = r13.A07;
        this.A00 = r04 == null ? new C06310bI() : r04;
        C06840cA r05 = r13.A0A;
        this.A01 = r05 == null ? new AnonymousClass96A() : r05;
        Context context = r13.A0Q;
        AnonymousClass064.A00(context);
        Class cls = r13.A0P;
        AnonymousClass064.A00(cls);
        C07290dB r35 = r13.A06;
        C07290dB r18 = r13.A05;
        Class cls2 = r13.A0N;
        Class cls3 = r13.A0M;
        cls3 = cls3 == null ? DefaultHandlerThreadFactory.class : cls3;
        C06390bQ r3 = this.A0D;
        AnonymousClass0d3 r2 = r13.A0L;
        AnonymousClass064.A00(r2);
        C07340dG r1 = r13.A0G;
        AnonymousClass064.A00(r1);
        C25761aM r9 = new C25761aM(r3, r2, r1, r13.A0F);
        C1936795w r16 = r13.A02;
        C1936795w r15 = r13.A03;
        C06260bD r8 = this.A07;
        C06390bQ r14 = this.A0D;
        C07330dF r7 = r13.A0H;
        C07300dC r6 = r13.A0D;
        r6 = r6 == null ? new C29593Edp(A0I, A0G) : r6;
        C07300dC r5 = r13.A0C;
        r5 = r5 == null ? new C29593Edp(A0H, A0F) : r5;
        C06320bJ r4 = r13.A09;
        r4 = r4 == null ? new AnonymousClass96B(50, 50) : r4;
        C06320bJ r32 = r13.A08;
        this.A0C = new C06400bR(context, cls, r35, r18, cls2, null, cls3, r9, r16, r15, r8, r14, r7, r6, r5, r4, r32 == null ? new AnonymousClass96B(1, 50) : r32, this.A01, r13.A04, r13.A0O);
        Class cls4 = this.A0A;
        if (cls4 != null) {
            r8.A02.registerObserver(new C06420bT(cls4, r13.A0Q));
        }
    }

    public static AnonymousClass0ZF A00(C06230bA r3, String str, String str2, Integer num, boolean z) {
        AnonymousClass0ZF r2 = (AnonymousClass0ZF) r3.A05.ALa();
        if (r2 == null) {
            r2 = new AnonymousClass0ZF();
        }
        r2.A07 = r3;
        r2.A0C = str;
        r2.A0B = str2;
        r2.A0A = num;
        r2.A0G = z;
        C12240os A022 = r3.A0D.A02();
        r2.A08 = A022;
        A022.A0B(C11910oA.A00());
        if (!r2.A0H) {
            r2.A0H = true;
            return r2;
        }
        throw new IllegalStateException("Expected immutability");
    }

    private AnonymousClass0ZF A01(String str, String str2, boolean z, Integer num, boolean z2) {
        boolean contains;
        C11970oK r0 = this.A02;
        if (r0 == null) {
            contains = false;
        } else {
            contains = r0.A00.contains(str2);
        }
        if (!contains) {
            SamplingResult A022 = ((C04980Xe) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BC6, this.A0B.A00)).A02(str2, z);
            if (A022.A01() && !this.A00.BHC(str2)) {
                AnonymousClass0ZF A002 = A00(this, str, str2, num, z2);
                A02(A002, A022);
                if (this.A03 != null) {
                    boolean z3 = true;
                    if (AnonymousClass0oD.A01.A00 != 1) {
                        z3 = false;
                    }
                    if (z3) {
                        A002.A06 = AnonymousClass16C.A00(AnonymousClass07B.A0q) | A002.A06;
                    }
                }
                return A002;
            }
        }
        return this.A0E;
    }

    public static void A02(AnonymousClass0ZF r4, SamplingResult samplingResult) {
        r4.A08.A0J("sampling_rate", Integer.valueOf(samplingResult.A00));
        if (samplingResult.A01) {
            r4.A06 = AnonymousClass16C.A00(AnonymousClass07B.A0n) | r4.A06;
        }
        if (samplingResult.A03) {
            r4.A06 = AnonymousClass16C.A00(AnonymousClass07B.A0o) | r4.A06;
        }
        if (samplingResult.A02) {
            r4.A06 = AnonymousClass16C.A00(AnonymousClass07B.A0p) | r4.A06;
        }
    }

    public AnonymousClass0ZF A03(C32411li r7) {
        return A01(r7.A02, r7.A01, r7.A04, r7.A00, r7.A03);
    }

    public String A06() {
        C29671gn A012 = C06400bR.A01(this.A0C);
        synchronized (A012) {
            AnonymousClass962 r0 = A012.A00;
            if (r0 == null) {
                return null;
            }
            String str = r0.A01;
            return str;
        }
    }

    public AnonymousClass0ZF A04(String str, Integer num, boolean z) {
        SamplingResult A002 = SamplingResult.A00();
        AnonymousClass0ZF A003 = A00(this, null, str, num, z);
        A003.A0G();
        A02(A003, A002);
        A003.A06 = AnonymousClass16C.A00(AnonymousClass07B.A02) | A003.A06;
        return A003;
    }
}
