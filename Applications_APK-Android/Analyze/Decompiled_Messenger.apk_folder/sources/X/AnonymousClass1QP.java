package X;

import android.util.Pair;
import com.facebook.proxygen.TraceFieldType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1QP  reason: invalid class name */
public final class AnonymousClass1QP {
    public static AnonymousClass1QP A04 = new AnonymousClass1QP();
    public static final Map A05;
    public int A00 = 0;
    public List A01 = new ArrayList();
    public final List A02 = new ArrayList();
    public final AtomicBoolean A03 = new AtomicBoolean(false);

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("drawee_request_id", "drawee_request_id");
        hashMap.put("videoId", TraceFieldType.VideoId);
        hashMap.put(TraceFieldType.Bitrate, "video_bitrate");
        hashMap.put("streamType", AnonymousClass80H.$const$string(643));
        hashMap.put(C22298Ase.$const$string(92), "video_start_ms");
        hashMap.put("durationMs", "video_duration_ms");
        String $const$string = AnonymousClass24B.$const$string(863);
        hashMap.put($const$string, $const$string);
        hashMap.put("isPrefetch", "video_is_prefetch");
        hashMap.put("rlrInKbps", "rlr_in_kbps");
        hashMap.put("videoRequestType", "video_request_type");
        A05 = Collections.unmodifiableMap(hashMap);
    }

    public static synchronized Pair A00() {
        Pair pair;
        synchronized (AnonymousClass1QP.class) {
            AnonymousClass1QP r3 = A04;
            pair = new Pair(r3.A01, Integer.valueOf(r3.A00));
            r3.A01 = new ArrayList();
            r3.A00 = 0;
        }
        return pair;
    }

    public static boolean A01() {
        return A04.A03.get();
    }

    private AnonymousClass1QP() {
    }
}
