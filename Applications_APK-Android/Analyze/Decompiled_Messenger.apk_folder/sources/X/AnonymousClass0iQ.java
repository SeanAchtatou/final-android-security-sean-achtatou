package X;

import java.util.concurrent.ExecutorService;

/* renamed from: X.0iQ  reason: invalid class name */
public final class AnonymousClass0iQ implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rtc.fbwebrtc.RtcVoltronModulesLoader$1";
    public final /* synthetic */ C09720iM A00;

    public AnonymousClass0iQ(C09720iM r1) {
        this.A00 = r1;
    }

    public void run() {
        C09720iM r4 = this.A00;
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, r4.A00)).AbO(805, false)) {
            C010708t.A06(C09720iM.A03, "OpenH264 voltron loading disabled by kill switch");
            r4.A02.set(null);
            return;
        }
        C71723cv A002 = ((C71443cQ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B7a, r4.A00)).A00(AnonymousClass07B.A0Y);
        A002.A04("openh264");
        A002.A06().A05((ExecutorService) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BKH, r4.A00), new C71763cz(r4));
    }
}
