package X;

import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.facebook.messaging.montage.model.MontageThreadInfo;
import com.facebook.user.model.UserKey;

/* renamed from: X.1v1  reason: invalid class name */
public final class AnonymousClass1v1 {
    public final int A00;
    public final ThreadKey A01;
    public final BasicMontageThreadInfo A02;
    public final BasicMontageThreadInfo A03;
    public final MontageThreadInfo A04;
    public final UserKey A05;
    public final Boolean A06;

    public AnonymousClass1v1(MontageThreadInfo montageThreadInfo, UserKey userKey, Boolean bool, ThreadKey threadKey, int i, BasicMontageThreadInfo basicMontageThreadInfo, BasicMontageThreadInfo basicMontageThreadInfo2) {
        this.A04 = montageThreadInfo;
        this.A05 = userKey;
        this.A06 = bool;
        this.A01 = threadKey;
        this.A00 = i;
        this.A02 = basicMontageThreadInfo;
        this.A03 = basicMontageThreadInfo2;
    }
}
