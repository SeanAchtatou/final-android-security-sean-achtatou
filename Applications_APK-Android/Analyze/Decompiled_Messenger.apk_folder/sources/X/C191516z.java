package X;

import android.app.Activity;
import android.content.Intent;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import java.util.Set;
import java.util.concurrent.Future;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16z  reason: invalid class name and case insensitive filesystem */
public final class C191516z {
    private static volatile C191516z A07;
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public final AnonymousClass069 A04;
    public final C08770fv A05;
    private volatile Future A06;

    public synchronized void A02() {
        Future future = null;
        if (future != null) {
            if (!future.isDone()) {
                future.cancel(false);
            }
        }
    }

    public static final C191516z A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C191516z.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C191516z(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002c, code lost:
        if ((r3 - r1) <= com.facebook.proxygen.LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(android.app.Activity r16) {
        /*
            r15 = this;
            X.069 r0 = r15.A04
            long r0 = r0.now()
            r15.A02 = r0
            X.00T r13 = new X.00T
            r13.<init>()
            r13.A02()
            long r5 = r15.A01
            r1 = 0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x002e
            long r3 = r15.A02
            long r1 = r3 - r5
            r7 = 5000(0x1388, double:2.4703E-320)
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 <= 0) goto L_0x002e
            long r1 = r15.A03
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x002e
            long r3 = r3 - r1
            int r1 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            r0 = 1
            if (r1 > 0) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            if (r0 == 0) goto L_0x00d8
            X.069 r0 = r15.A04
            long r0 = r0.now()
            r15.A03 = r0
            java.lang.String r3 = A01(r16)
            X.0fv r2 = r15.A05
            long r10 = r15.A02
            boolean r0 = r2.A0M()
            if (r0 != 0) goto L_0x00d8
            boolean r0 = r2.A0N()
            if (r0 != 0) goto L_0x00d8
            r2.A01 = r10
            com.facebook.perf.startupstatemachine.StartupStateMachine r1 = r2.A0N
            monitor-enter(r1)
            r0 = 3
            r1.A03 = r0     // Catch:{ all -> 0x0056 }
            goto L_0x0059
        L_0x0056:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0059:
            monitor-exit(r1)
            com.facebook.quicklog.QuickPerformanceLogger r6 = r2.A05
            r7 = 5505026(0x540002, float:7.714184E-39)
            r8 = 0
            r9 = 0
            r12 = 0
            r14 = 0
            r6.markerStartWithCounterForLegacy(r7, r8, r9, r10, r12, r13, r14)
            X.0ko r1 = r2.A0L
            r0 = 1
            r1.A02(r0)
            com.facebook.common.classmarkers.ClassMarkerLoader.loadMessengerWarmStartupBeginMarker()
            r6 = 5505026(0x540002, float:7.714184E-39)
            if (r3 == 0) goto L_0x007f
            com.facebook.quicklog.QuickPerformanceLogger r1 = r2.A05
            java.lang.String r0 = "nav_"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r1.markerTag(r7, r0)
        L_0x007f:
            com.facebook.quicklog.QuickPerformanceLogger r5 = r2.A05
            java.lang.String r7 = "+"
            com.google.common.base.Joiner r4 = com.google.common.base.Joiner.on(r7)
            java.lang.Class r0 = r16.getClass()
            java.lang.String r3 = r0.getSimpleName()
            java.lang.Object[] r0 = new java.lang.Object[r8]
            java.lang.String r1 = "activity"
            java.lang.String r0 = r4.join(r1, r3, r0)
            r5.markerTag(r6, r0)
            com.facebook.quicklog.QuickPerformanceLogger r0 = r2.A05
            r0.markerAnnotate(r6, r1, r3)
            android.content.Intent r0 = r16.getIntent()
            r5 = 0
            if (r0 == 0) goto L_0x00c3
            java.lang.String r5 = r0.getAction()
            if (r5 == 0) goto L_0x00d9
            r4 = r5
        L_0x00ad:
            com.google.common.base.Joiner r1 = com.google.common.base.Joiner.on(r7)
            java.lang.Object[] r0 = new java.lang.Object[r8]
            java.lang.String r3 = "action"
            java.lang.String r1 = r1.join(r3, r4, r0)
            com.facebook.quicklog.QuickPerformanceLogger r0 = r2.A05
            r0.markerTag(r6, r1)
            com.facebook.quicklog.QuickPerformanceLogger r0 = r2.A05
            r0.markerAnnotate(r6, r3, r4)
        L_0x00c3:
            X.0g6 r0 = r2.A0K
            r0.A01(r6)
            X.0fx r1 = r2.A0H
            java.lang.String r0 = "android.intent.action.MAIN"
            boolean r0 = r0.equalsIgnoreCase(r5)
            if (r0 == 0) goto L_0x00d5
            X.C08790fx.A04(r1, r6)
        L_0x00d5:
            X.C08770fv.A03(r2)
        L_0x00d8:
            return
        L_0x00d9:
            java.lang.String r4 = ""
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C191516z.A03(android.app.Activity):void");
    }

    private C191516z(AnonymousClass1XY r2) {
        this.A05 = C08770fv.A00(r2);
        this.A04 = AnonymousClass067.A04(r2);
    }

    public static String A01(Activity activity) {
        Set<String> categories;
        Intent intent = activity.getIntent();
        if (intent == null || (categories = intent.getCategories()) == null || !categories.contains(TurboLoader.Locator.$const$string(14))) {
            return null;
        }
        return "launcher";
    }
}
