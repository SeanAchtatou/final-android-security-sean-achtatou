package X;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.acra.constants.ErrorReportingConstants;
import io.card.payment.BuildConfig;
import java.io.PrintWriter;
import java.util.List;

/* renamed from: X.07a  reason: invalid class name and case insensitive filesystem */
public final class C007707a {
    private final C27821do A00 = new C27821do();

    public static View A00(C007707a r4) {
        List A002 = r4.A00.A00();
        if (A002 != null && !A002.isEmpty()) {
            for (int size = A002.size() - 1; size >= 0; size--) {
                View view = ((C13070qb) A002.get(size)).A00;
                if (view.getVisibility() == 0) {
                    return view;
                }
            }
        }
        return null;
    }

    public static void A01(String str, PrintWriter printWriter, View view, int i, int i2) {
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        String name = view.getClass().getName();
        if (name.contains(".litho.")) {
            printWriter.println(view.toString());
        } else {
            printWriter.print(name);
            printWriter.print("{");
            printWriter.print(Integer.toHexString(view.hashCode()));
            printWriter.print(" ");
            int visibility = view.getVisibility();
            String str7 = "V";
            String str8 = ".";
            if (visibility == 0) {
                printWriter.print(str7);
            } else if (visibility == 4) {
                printWriter.print("I");
            } else if (visibility != 8) {
                printWriter.print(str8);
            } else {
                printWriter.print("G");
            }
            String str9 = "F";
            String str10 = str8;
            if (view.isFocusable()) {
                str10 = str9;
            }
            printWriter.print(str10);
            if (view.isEnabled()) {
                str2 = "E";
            } else {
                str2 = str8;
            }
            printWriter.print(str2);
            printWriter.print(str8);
            String str11 = "H";
            String str12 = str8;
            if (view.isHorizontalScrollBarEnabled()) {
                str12 = str11;
            }
            printWriter.print(str12);
            if (!view.isVerticalScrollBarEnabled()) {
                str7 = str8;
            }
            printWriter.print(str7);
            if (view.isClickable()) {
                str3 = "C";
            } else {
                str3 = str8;
            }
            printWriter.print(str3);
            if (view.isLongClickable()) {
                str4 = "L";
            } else {
                str4 = str8;
            }
            printWriter.print(str4);
            printWriter.print(" ");
            if (!view.isFocused()) {
                str9 = str8;
            }
            printWriter.print(str9);
            if (view.isSelected()) {
                str5 = "S";
            } else {
                str5 = str8;
            }
            printWriter.print(str5);
            if (!view.isHovered()) {
                str11 = str8;
            }
            printWriter.print(str11);
            if (view.isActivated()) {
                str6 = "A";
            } else {
                str6 = str8;
            }
            printWriter.print(str6);
            if (view.isDirty()) {
                str8 = "D";
            }
            printWriter.print(str8);
            int[] iArr = new int[2];
            view.getLocationOnScreen(iArr);
            printWriter.print(" ");
            printWriter.print(iArr[0] - i);
            printWriter.print(",");
            printWriter.print(iArr[1] - i2);
            printWriter.print("-");
            printWriter.print((iArr[0] + view.getWidth()) - i);
            printWriter.print(",");
            printWriter.print((iArr[1] + view.getHeight()) - i2);
            i = iArr[0];
            i2 = iArr[1];
            try {
                int id = view.getId();
                if (id != -1) {
                    printWriter.append((CharSequence) " #");
                    printWriter.append((CharSequence) Integer.toHexString(id));
                    Resources resources = view.getResources();
                    if (id > 0 && resources != null) {
                        int i3 = -16777216 & id;
                        String resourcePackageName = i3 != 16777216 ? i3 != 2130706432 ? resources.getResourcePackageName(id) : ErrorReportingConstants.APP_NAME_KEY : "android";
                        printWriter.print(" ");
                        printWriter.print(resourcePackageName);
                        printWriter.print(":");
                        printWriter.print(resources.getResourceTypeName(id));
                        printWriter.print("/");
                        printWriter.print(resources.getResourceEntryName(id));
                    }
                }
            } catch (Exception unused) {
            }
            if (view instanceof TextView) {
                try {
                    printWriter.print(" text=\"");
                    String replace = ((TextView) view).getText().toString().replace("\n", BuildConfig.FLAVOR).replace("\"", BuildConfig.FLAVOR);
                    if (replace.length() > 200) {
                        replace = AnonymousClass08S.A0J(replace.substring(0, AnonymousClass1Y3.A1c), "...");
                    }
                    printWriter.print(replace);
                    printWriter.print("\"");
                } catch (Exception unused2) {
                }
            }
            printWriter.println("}");
        }
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String A0J = AnonymousClass08S.A0J(str, "  ");
            for (int i4 = 0; i4 < childCount; i4++) {
                A01(A0J, printWriter, viewGroup.getChildAt(i4), i, i2);
            }
        }
    }
}
