package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0lU  reason: invalid class name */
public final class AnonymousClass0lU {
    private static volatile AnonymousClass0lU A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass0lU A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0lU.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0lU(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass0lU(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
