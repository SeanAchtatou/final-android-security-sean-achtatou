package X;

/* renamed from: X.1Q2  reason: invalid class name */
public final class AnonymousClass1Q2 implements AnonymousClass1Q3 {
    public final C22861Nc A00;
    public final C30661iR A01;
    public final C23281Ox A02;

    public void ByS(C23581Qb r4, AnonymousClass1QK r5) {
        r5.A07.Bk8(r5, AnonymousClass24B.$const$string(AnonymousClass1Y3.A59));
        C50152dV A04 = this.A02.A04(r4, r5);
        this.A02.A07(A04, new C50162dW(this, A04));
    }

    public AnonymousClass1Q2(C30661iR r1, C22861Nc r2, C23281Ox r3) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = r3;
    }

    public static void A00(AnonymousClass1S3 r3, int i, C196579Mo r5, C23581Qb r6, AnonymousClass1QK r7) {
        AnonymousClass1NY r1;
        AnonymousClass1PS A012 = AnonymousClass1PS.A01(r3.A01());
        try {
            r1 = new AnonymousClass1NY(A012);
            try {
                r1.A08 = r5;
                r1.A0A();
                r7.A00 = AnonymousClass07B.A01;
                r6.Bgg(r1, i);
                AnonymousClass1NY.A04(r1);
                AnonymousClass1PS.A05(A012);
            } catch (Throwable th) {
                th = th;
                AnonymousClass1NY.A04(r1);
                AnonymousClass1PS.A05(A012);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            r1 = null;
            AnonymousClass1NY.A04(r1);
            AnonymousClass1PS.A05(A012);
            throw th;
        }
    }
}
