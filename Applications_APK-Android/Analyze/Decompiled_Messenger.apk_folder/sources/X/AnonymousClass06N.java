package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.mobileconfig.MobileConfigCrashReportUtils;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

/* renamed from: X.06N  reason: invalid class name */
public final class AnonymousClass06N {
    public final Random A00;
    private final AnonymousClass1ZE A01;
    private final C06230bA A02;
    private final AnonymousClass0Ud A03;
    private final C09340h3 A04;
    private final AnonymousClass14M A05;
    private final MobileConfigCrashReportUtils A06;

    private static void A00(USLEBaseShape0S0000000 uSLEBaseShape0S0000000, Properties properties) {
        if (properties != null) {
            String property = properties.getProperty("appVersionCode");
            if (property != null) {
                uSLEBaseShape0S0000000.A0B("app_version_code", Integer.valueOf(Integer.parseInt(property)));
            }
            String property2 = properties.getProperty("appVersionName");
            if (property2 != null) {
                uSLEBaseShape0S0000000.A0D("app_version_name", property2);
            }
            String property3 = properties.getProperty("processName");
            if (property3 != null) {
                uSLEBaseShape0S0000000.A0D("process_name", property3);
            }
            String property4 = properties.getProperty("processId");
            if (property4 != null) {
                uSLEBaseShape0S0000000.A0B("process_id", Integer.valueOf(Integer.parseInt(property4)));
            }
            String property5 = properties.getProperty("installerName");
            if (property5 != null) {
                uSLEBaseShape0S0000000.A0D("installer_name", property5);
            }
            String property6 = properties.getProperty("aslCreationTime");
            if (property6 != null) {
                long parseLong = Long.parseLong(property6);
                if (parseLong > 2147483647L) {
                    parseLong = 2147483647L;
                }
                uSLEBaseShape0S0000000.A0B("asl_creation_time", Integer.valueOf((int) parseLong));
            }
        }
    }

    public long A01() {
        AnonymousClass0Ud r4 = this.A03;
        return ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, r4.A02)).now() - r4.A0G;
    }

    public String A02() {
        return this.A02.A06();
    }

    public String A03() {
        JsonNode A032 = this.A05.A03();
        if (A032 != null) {
            return A032.toString();
        }
        return null;
    }

    public String A04() {
        return this.A06.getSerializedCanaryData();
    }

    public String A05() {
        Map allLastFetchTimestamps = this.A06.getAllLastFetchTimestamps();
        StringBuilder sb = new StringBuilder((allLastFetchTimestamps.size() * 50) + 50);
        sb.append("{");
        boolean z = true;
        for (Map.Entry entry : allLastFetchTimestamps.entrySet()) {
            if (!z) {
                sb.append(",");
            }
            z = false;
            sb.append("\"");
            sb.append((String) entry.getKey());
            sb.append("\":");
            sb.append(((Long) entry.getValue()).toString());
        }
        sb.append("}");
        return sb.toString();
    }

    public String A06() {
        return this.A04.A0I();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r2.A00.nextInt(r1) != 0) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(android.content.Context r24, java.lang.String r25, int r26, java.util.concurrent.Callable r27, java.util.concurrent.Callable r28) {
        /*
            r23 = this;
            r2 = r23
            r1 = r26
            if (r26 <= 0) goto L_0x000f
            java.util.Random r0 = r2.A00
            int r1 = r0.nextInt(r1)
            r0 = 1
            if (r1 == 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x00e3
            X.1ZE r1 = r2.A01
            java.lang.String r0 = "fbandroid_application_info"
            X.0bW r3 = r1.A01(r0)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r0 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r1 = 162(0xa2, float:2.27E-43)
            r0.<init>(r3, r1)
            boolean r1 = r0.A0G()
            if (r1 == 0) goto L_0x00e3
            java.lang.Object r3 = r27.call()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            byte[] r3 = (byte[]) r3     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            if (r3 == 0) goto L_0x00e3
            int r1 = r3.length     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            if (r1 == 0) goto L_0x00e3
            java.lang.Object r1 = r28.call()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.util.Properties r1 = (java.util.Properties) r1     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            X.07M r5 = new X.07M     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            r5.<init>()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.io.ByteArrayInputStream r7 = new java.io.ByteArrayInputStream     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            r7.<init>(r3)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.Integer r9 = X.AnonymousClass01V.A00     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r10 = r10 / r3
            r12 = 0
            r14 = 0
            r16 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r6 = r24
            r8 = r25
            X.07N r7 = r5.A01(r6, r7, r8, r9, r10, r12, r14, r16, r18, r19, r20, r21, r22)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            char r5 = r7.A05     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            X.01k r3 = X.C002101k.A09     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            char r4 = r3.mSymbol     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            r3 = 1
            if (r5 == r4) goto L_0x006d
            r3 = 0
        L_0x006d:
            if (r3 == 0) goto L_0x00e3
            r7.A07()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            long r5 = r7.A0G     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r3
            r8 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r3 <= 0) goto L_0x0082
            r4 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x0083
        L_0x0082:
            int r4 = (int) r5     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
        L_0x0083:
            X.0Ud r2 = r2.A03     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            com.facebook.common.util.TriState r2 = r2.A0C()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r3 = r2.name()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r2 = "app_started_in_background"
            r0.A0D(r2, r3)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r3 = r7.A0L     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r2 = "checksum"
            r0.A0D(r2, r3)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r3 = r7.A0K     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r2 = "contents"
            r0.A0D(r2, r3)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r26)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r2 = "mc_sampling_rate"
            r0.A0B(r2, r3)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r3 = r7.A0N     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r2 = "reportId"
            r0.A0D(r2, r3)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            java.lang.String r2 = "reportTime"
            r0.A0B(r2, r3)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            A00(r0, r1)     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            r0.A06()     // Catch:{ IllegalStateException -> 0x00db, 0Cz -> 0x00d2, NoSuchAlgorithmException -> 0x00c9, Exception -> 0x00c0 }
            return
        L_0x00c0:
            r2 = move-exception
            java.lang.String r1 = "AppDataBridge"
            java.lang.String r0 = "Failed to send app info event"
            X.C010708t.A0L(r1, r0, r2)
            return
        L_0x00c9:
            r2 = move-exception
            java.lang.String r1 = "AppDataBridge"
            java.lang.String r0 = "Failed to create log parser"
            X.C010708t.A0L(r1, r0, r2)
            return
        L_0x00d2:
            r2 = move-exception
            java.lang.String r1 = "AppDataBridge"
            java.lang.String r0 = "Failed to parse log contents"
            X.C010708t.A0L(r1, r0, r2)
            return
        L_0x00db:
            r2 = move-exception
            java.lang.String r1 = "AppDataBridge"
            java.lang.String r0 = "Cannot read state file stream"
            X.C010708t.A0L(r1, r0, r2)
        L_0x00e3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass06N.A07(android.content.Context, java.lang.String, int, java.util.concurrent.Callable, java.util.concurrent.Callable):void");
    }

    public AnonymousClass06N(AnonymousClass0Ud r1, C06230bA r2, C09340h3 r3, AnonymousClass1ZE r4, MobileConfigCrashReportUtils mobileConfigCrashReportUtils, AnonymousClass14M r6, Random random) {
        this.A03 = r1;
        this.A02 = r2;
        this.A04 = r3;
        this.A01 = r4;
        this.A06 = mobileConfigCrashReportUtils;
        this.A05 = r6;
        this.A00 = random;
    }
}
