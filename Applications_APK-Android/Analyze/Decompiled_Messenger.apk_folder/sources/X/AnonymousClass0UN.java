package X;

import java.util.concurrent.atomic.AtomicReferenceArray;

/* renamed from: X.0UN  reason: invalid class name */
public final class AnonymousClass0UN {
    public final byte A00 = AnonymousClass0UO.A00().A00;
    public final C24851Xi A01;
    public final AtomicReferenceArray A02;

    public AnonymousClass0UN(int i, AnonymousClass1XY r3) {
        this.A01 = r3.getScopeAwareInjector();
        if (i > 0) {
            this.A02 = new AtomicReferenceArray(i);
        }
    }
}
