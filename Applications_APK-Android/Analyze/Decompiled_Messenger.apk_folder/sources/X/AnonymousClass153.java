package X;

/* renamed from: X.153  reason: invalid class name */
public final class AnonymousClass153 extends C14590te {
    private Boolean A00 = null;
    private final C04310Tq A01;

    public String Amb() {
        return "dialtone";
    }

    public static final AnonymousClass153 A00(AnonymousClass1XY r1) {
        return new AnonymousClass153(r1);
    }

    public boolean BEx() {
        if (this.A00 == null) {
            this.A00 = (Boolean) this.A01.get();
        }
        return this.A00.booleanValue();
    }

    private AnonymousClass153(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.Amx, r2);
    }
}
