package X;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import com.facebook.common.util.TriState;
import com.facebook.push.fbns.ipc.FbnsAIDLRequest;
import com.facebook.rti.orca.FbnsLiteBroadcastReceiver;
import io.card.payment.BuildConfig;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0GZ  reason: invalid class name */
public final class AnonymousClass0GZ implements AnonymousClass1YQ {
    private static volatile AnonymousClass0GZ A0E;
    public int A00;
    private FbnsLiteBroadcastReceiver A01;
    private Future A02;
    public final Context A03;
    public final C02220Dq A04;
    public final AnonymousClass0G5 A05;
    public final AnonymousClass0H1 A06;
    public final AnonymousClass03g A07;
    public final Runnable A08 = new AnonymousClass0H0(this);
    public final ExecutorService A09;
    private final C25921ac A0A;
    private final C04310Tq A0B;
    private final C04310Tq A0C;
    public volatile boolean A0D;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0026, code lost:
        if (X.AnonymousClass03g.A03(r2) == false) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A06(X.AnonymousClass0GZ r12) {
        /*
            r8 = 0
            r9 = 0
            r0 = 1
            r12.A0D = r0
            X.03g r1 = r12.A07
            boolean r1 = r1.A08()
            if (r1 == 0) goto L_0x00d7
            X.0Tq r1 = r12.A0C
            java.lang.Object r1 = r1.get()
            if (r1 != 0) goto L_0x0019
            A04(r12)
            return
        L_0x0019:
            X.03g r2 = r12.A07
            boolean r1 = X.AnonymousClass03g.A04(r2)
            if (r1 == 0) goto L_0x0028
            boolean r2 = X.AnonymousClass03g.A03(r2)
            r1 = 1
            if (r2 != 0) goto L_0x0029
        L_0x0028:
            r1 = 0
        L_0x0029:
            if (r1 == 0) goto L_0x003d
            X.0H1 r1 = r12.A06
            java.lang.String r3 = "MainServiceHelper"
            android.content.Context r2 = r1.A00     // Catch:{ all -> 0x0037 }
            android.content.Intent r1 = r1.A01     // Catch:{ all -> 0x0037 }
            r2.startService(r1)     // Catch:{ all -> 0x0037 }
            goto L_0x003d
        L_0x0037:
            r2 = move-exception
            java.lang.String r1 = "failed to startDummyStickyService"
            X.C010708t.A0S(r3, r2, r1)
        L_0x003d:
            X.03g r1 = r12.A07
            boolean r1 = r1.A0B()
            if (r1 == 0) goto L_0x005a
            android.content.Context r1 = r12.A03
            java.lang.String r5 = X.AnonymousClass0A9.A00(r1)
            if (r5 == 0) goto L_0x005a
            java.lang.String r2 = com.facebook.rti.push.service.FbnsService.A03(r5)
            java.lang.String r3 = "FbnsSuspendSwitch"
            java.lang.String r6 = "com.facebook.rti.intent.ACTION_FBNS_KILL_SWITCH_DISABLE_SERVICE"
            r7 = 0
            r4 = 1
            X.AnonymousClass0H3.A03(r1, r2, r3, r4, r5, r6, r7)
        L_0x005a:
            X.0G5 r2 = r12.A05
            X.03g r1 = r2.A00
            java.lang.Integer r1 = r1.A06()
            if (r1 == 0) goto L_0x006c
            int r1 = r1.intValue()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r1)
        L_0x006c:
            X.03g r1 = r2.A00
            boolean r1 = r1.A0C()
            if (r1 == 0) goto L_0x0078
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r0)
        L_0x0078:
            X.08E r4 = new X.08E
            r5 = 0
            r6 = 0
            r4.<init>(r5, r6, r8, r9)
            X.0G7 r3 = r2.A01
            X.0G6 r1 = r3.A01
            X.0G5 r1 = r1.A00
            X.03g r1 = r1.A00
            java.lang.String r2 = r1.A07()
            if (r2 == 0) goto L_0x00c8
            boolean r1 = X.AnonymousClass0A9.A01(r2)
            if (r1 == 0) goto L_0x0099
            android.content.Context r1 = r3.A00
            X.AnonymousClass0H3.A00(r1)
        L_0x0099:
            android.content.Context r5 = r3.A00
            java.lang.String r7 = "init"
            r9 = r2
            if (r2 != 0) goto L_0x00a4
            java.lang.String r9 = r5.getPackageName()
        L_0x00a4:
            java.lang.String r6 = com.facebook.rti.push.service.FbnsService.A03(r9)
            r8 = 1
            java.lang.String r10 = "Orca.START"
            r11 = r4
            X.AnonymousClass0H3.A03(r5, r6, r7, r8, r9, r10, r11)
            r2 = 1
        L_0x00b0:
            r1 = 0
            if (r2 == 0) goto L_0x00cf
            A08(r12, r0)
            X.03g r0 = r12.A07
            boolean r0 = r0.A0C()
            if (r0 == 0) goto L_0x00c4
            A05(r12)
        L_0x00c1:
            r12.A0D = r1
            return
        L_0x00c4:
            r12.A03()
            goto L_0x00c1
        L_0x00c8:
            android.content.Context r1 = r3.A00
            X.AnonymousClass0H3.A00(r1)
            r2 = 0
            goto L_0x00b0
        L_0x00cf:
            A08(r12, r1)
            A05(r12)
            r12.A0D = r0
        L_0x00d7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GZ.A06(X.0GZ):void");
    }

    public String getSimpleName() {
        return "FbnsLiteInitializer";
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BPr, r1);
    }

    public static final AnonymousClass0GZ A02(AnonymousClass1XY r4) {
        if (A0E == null) {
            synchronized (AnonymousClass0GZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0E, r4);
                if (A002 != null) {
                    try {
                        A0E = new AnonymousClass0GZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0E;
    }

    private void A03() {
        AnonymousClass0H9.A00(this.A03, FbnsLiteBroadcastReceiver.class, true);
        if (this.A01 == null && C51692hY.A00(this.A03)) {
            this.A01 = new FbnsLiteBroadcastReceiver();
            IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            if (C51692hY.A01(this.A03)) {
                intentFilter.addAction("android.intent.action.USER_PRESENT");
            }
            C009207y.A01.A08(this.A03, this.A01, intentFilter, null, null);
        }
    }

    public static void A04(AnonymousClass0GZ r2) {
        AnonymousClass0H1 r0 = r2.A06;
        try {
            r0.A00.stopService(r0.A01);
        } catch (Throwable th) {
            C010708t.A0S("MainServiceHelper", th, "failed to stopDummyStickyService");
        }
    }

    public static void A05(AnonymousClass0GZ r3) {
        AnonymousClass0H9.A00(r3.A03, FbnsLiteBroadcastReceiver.class, false);
        FbnsLiteBroadcastReceiver fbnsLiteBroadcastReceiver = r3.A01;
        if (fbnsLiteBroadcastReceiver != null) {
            C009207y.A01.A07(r3.A03, fbnsLiteBroadcastReceiver);
            r3.A01 = null;
        }
    }

    public static void A07(AnonymousClass0GZ r13, boolean z) {
        boolean A0C2 = r13.A07.A0C();
        int A052 = r13.A07.A05(A0C2);
        int i = 1;
        if (A0C2) {
            i = 100;
        }
        boolean z2 = true;
        r13.A0C.get();
        r13.A0A.B7Z();
        ((TriState) r13.A0B.get()).toString();
        if (r13.A0B.get() != TriState.YES) {
            z2 = false;
        }
        Context context = r13.A03;
        String str = (String) r13.A0C.get();
        String B7Z = r13.A0A.B7Z();
        int i2 = r13.A00;
        int i3 = 10000;
        if (z2 || !(!C01600Aw.A00(context).A02)) {
            i = 10000;
        } else {
            i3 = A052;
        }
        Integer valueOf = Integer.valueOf(i3);
        Bundle bundle = new Bundle();
        boolean z3 = false;
        if (i > 10000) {
            C010708t.A0P("FbnsClient", "Wrong analytics sampling rate: %d", Integer.valueOf(i));
            i = 1;
        }
        if (new Random().nextInt(10000) < i) {
            z3 = true;
        }
        AnonymousClass0HA.A05.A02(bundle, Integer.valueOf(i));
        AnonymousClass0HA.A07.A02(bundle, Boolean.valueOf(z3));
        if (z2) {
            AnonymousClass0HA.A01.A02(bundle, str);
        } else {
            AnonymousClass0HA.A01.A02(bundle, BuildConfig.FLAVOR);
        }
        AnonymousClass0HA.A03.A02(bundle, B7Z);
        AnonymousClass0HA.A02.A02(bundle, Boolean.valueOf(z2));
        AnonymousClass0HA.A04.A02(bundle, Integer.valueOf(i2));
        AnonymousClass0HA.A06.A02(bundle, valueOf);
        if (z) {
            AnonymousClass0H3.A01(context, bundle);
        } else {
            new C03040Hr(context).A03(new FbnsAIDLRequest(bundle, C03080Hx.SET_ANALYTICS_CONFIG.mOperationType));
        }
    }

    public static void A08(AnonymousClass0GZ r3, boolean z) {
        Future future = r3.A02;
        if (future != null) {
            future.cancel(true);
        }
        r3.A02 = AnonymousClass07A.A02(r3.A09, new AnonymousClass0H7(r3, z), 637359659);
    }

    private AnonymousClass0GZ(AnonymousClass1XY r3) {
        this.A0B = C04750Wa.A05(r3);
        this.A07 = AnonymousClass03g.A00(r3);
        this.A03 = AnonymousClass1YA.A00(r3);
        this.A0C = AnonymousClass0XJ.A0K(r3);
        this.A0A = C25901aa.A00(r3);
        this.A09 = AnonymousClass0UX.A0a(r3);
        this.A05 = AnonymousClass0G5.A00(r3);
        this.A04 = C02220Dq.A00(r3);
        this.A06 = new AnonymousClass0H1(this.A03);
    }

    public static final AnonymousClass0GZ A01(AnonymousClass1XY r0) {
        return A02(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (X.AnonymousClass03g.A03(r1) == false) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0020, code lost:
        if (r0 != false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void init() {
        /*
            r5 = this;
            r0 = -341292438(0xffffffffeba84a6a, float:-4.069019E26)
            int r3 = X.C000700l.A03(r0)
            X.03g r0 = r5.A07
            boolean r0 = r0.A0A()
            if (r0 != 0) goto L_0x0022
            X.03g r1 = r5.A07
            boolean r0 = X.AnonymousClass03g.A04(r1)
            if (r0 == 0) goto L_0x001e
            boolean r1 = X.AnonymousClass03g.A03(r1)
            r0 = 1
            if (r1 != 0) goto L_0x001f
        L_0x001e:
            r0 = 0
        L_0x001f:
            r4 = 0
            if (r0 == 0) goto L_0x0023
        L_0x0022:
            r4 = 1
        L_0x0023:
            java.lang.String r2 = "AppStateLoggerCore"
            java.lang.Object r1 = X.AnonymousClass01P.A0Z
            monitor-enter(r1)
            X.01P r0 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0033
            java.lang.String r0 = "AppStateLogger is not ready yet"
            X.C010708t.A0J(r2, r0)     // Catch:{ all -> 0x0066 }
            monitor-exit(r1)     // Catch:{ all -> 0x0066 }
            goto L_0x0043
        L_0x0033:
            monitor-exit(r1)     // Catch:{ all -> 0x0066 }
            X.01P r2 = X.AnonymousClass01P.A0Y
            com.facebook.analytics.appstatelogger.AppState r1 = r2.A09
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)
            r1.A0F = r0
            X.01i r0 = r2.A0B
            r0.A06()
        L_0x0043:
            X.03g r0 = r5.A07
            boolean r0 = r0.A08()
            if (r0 != 0) goto L_0x0052
            r0 = -1612784139(0xffffffff9fdeddf5, float:-9.4387854E-20)
            X.C000700l.A09(r0, r3)
            return
        L_0x0052:
            java.util.concurrent.ExecutorService r2 = r5.A09
            X.0Rf r1 = new X.0Rf
            r1.<init>(r5)
            r0 = 307028323(0x124ce163, float:6.464886E-28)
            X.AnonymousClass07A.A04(r2, r1, r0)
            r0 = 700618386(0x29c29692, float:8.6414505E-14)
            X.C000700l.A09(r0, r3)
            return
        L_0x0066:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0066 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GZ.init():void");
    }
}
