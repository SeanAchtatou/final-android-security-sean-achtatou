package X;

import java.util.Iterator;

/* renamed from: X.1f2  reason: invalid class name and case insensitive filesystem */
public abstract class C28581f2 implements Iterator, C17400yq {
    public C27701dc A00;
    public C27701dc A01;

    public C27701dc A00(C27701dc r2) {
        return r2.A00;
    }

    public C27701dc A01(C27701dc r2) {
        return r2.A01;
    }

    public void CIX(C27701dc r3) {
        C27701dc r0;
        if (this.A00 == r3 && r3 == this.A01) {
            this.A01 = null;
            this.A00 = null;
        }
        C27701dc r02 = this.A00;
        if (r02 == r3) {
            this.A00 = A00(r02);
        }
        C27701dc r1 = this.A01;
        if (r1 == r3) {
            C27701dc r03 = this.A00;
            if (r1 == r03 || r03 == null) {
                r0 = null;
            } else {
                r0 = A01(r1);
            }
            this.A01 = r0;
        }
    }

    public boolean hasNext() {
        if (this.A01 != null) {
            return true;
        }
        return false;
    }

    public Object next() {
        C27701dc r0;
        C27701dc r1 = this.A01;
        C27701dc r02 = this.A00;
        if (r1 == r02 || r02 == null) {
            r0 = null;
        } else {
            r0 = A01(r1);
        }
        this.A01 = r0;
        return r1;
    }

    public C28581f2(C27701dc r1, C27701dc r2) {
        this.A00 = r2;
        this.A01 = r1;
    }
}
