package X;

import android.content.res.Resources;
import android.util.SparseBooleanArray;

/* renamed from: X.1aT  reason: invalid class name and case insensitive filesystem */
public final class C25831aT {
    public final SparseBooleanArray A00;
    public final int[] A01;

    public C25831aT(Resources resources, int[] iArr) {
        SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
        this.A00 = sparseBooleanArray;
        sparseBooleanArray.put(C04960Xb.A00(resources), true);
        this.A01 = iArr;
    }
}
