package X;

import java.util.ArrayList;

/* renamed from: X.1GD  reason: invalid class name */
public final class AnonymousClass1GD {
    public ArrayList A00 = new ArrayList();
    public ArrayList A01 = new ArrayList();

    public static void A00(AnonymousClass1GD r1, String str, Object obj) {
        if (!r1.A01.isEmpty()) {
            r1.A00.add(str);
            r1.A00.add(obj);
            r1.A01.add(null);
            return;
        }
        throw new IllegalStateException("Adding entries can be only done after category is started. Call startCategory first");
    }

    public void A01(C22441Lj r7) {
        int size = this.A01.size() - 1;
        if (size >= 0 && this.A01.get(size) != null) {
            this.A01.remove(size);
        }
        int size2 = this.A01.size();
        int i = 0;
        for (int i2 = 0; i2 < size2; i2++) {
            String str = (String) this.A01.get(i2);
            if (str != null) {
                r7.CMo(str);
            } else {
                int i3 = i + 1;
                String str2 = (String) this.A00.get(i);
                i = i3 + 1;
                Object obj = this.A00.get(i3);
                if (obj instanceof String) {
                    r7.CMj(str2, (String) obj);
                } else if (obj instanceof Integer) {
                    r7.CMh(str2, ((Integer) obj).intValue());
                } else if (obj instanceof Long) {
                    r7.CMi(str2, ((Long) obj).longValue());
                } else if (obj instanceof Double) {
                    r7.CMg(str2, ((Double) obj).doubleValue());
                } else if (obj instanceof Boolean) {
                    r7.CMk(str2, ((Boolean) obj).booleanValue());
                } else if (obj instanceof String[]) {
                    r7.CMm(str2, (String[]) obj);
                } else if (obj instanceof int[]) {
                    r7.CMl(str2, (int[]) obj);
                }
            }
        }
    }
}
