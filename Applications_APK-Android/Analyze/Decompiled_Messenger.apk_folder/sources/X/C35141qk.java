package X;

import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1qk  reason: invalid class name and case insensitive filesystem */
public final class C35141qk {
    public C35161qm A00;
    public Set A01;
    public ScheduledExecutorService A02;
    public C04310Tq A03;
    public C04310Tq A04;
    public C04310Tq A05;

    public void A00(int i, Object obj) {
        if (this.A00 == null) {
            this.A00 = (C35161qm) this.A04.get();
        }
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, ((C35171qn) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Azv, this.A00.A00.A00.A00)).A00)).Aem(284850821010466L)) {
            if (this.A02 == null) {
                this.A02 = (ScheduledExecutorService) this.A05.get();
            }
            this.A02.schedule(new C164987k2(this, i, obj), 0, TimeUnit.SECONDS);
        }
    }

    public C35141qk(C35101qg r2) {
        C04310Tq r0 = r2.A01;
        C000300h.A01(r0);
        this.A04 = r0;
        C04310Tq r02 = r2.A00;
        C000300h.A01(r02);
        this.A03 = r02;
        C04310Tq r03 = r2.A02;
        C000300h.A01(r03);
        this.A05 = r03;
    }
}
