package X;

import com.google.common.base.Preconditions;

/* renamed from: X.1f3  reason: invalid class name and case insensitive filesystem */
public final class C28591f3 {
    public C05180Xy A00;
    public boolean A01;
    private final AnonymousClass1Y6 A02;

    public void A01() {
        synchronized (this) {
            this.A01 = true;
        }
        C05180Xy r0 = this.A00;
        if (r0 != null) {
            int size = r0.size();
            for (int i = 0; i < size; i++) {
                ((C27231cr) this.A00.A00.A07(i)).A03();
            }
            this.A00.clear();
        }
    }

    public synchronized void A02(C27231cr r3) {
        Preconditions.checkNotNull(r3);
        if (this.A01) {
            this.A02.BxR(new C199529aG(r3));
        } else {
            if (this.A00 == null) {
                this.A00 = new C05180Xy();
            }
            this.A00.add(r3);
            r3.A04(new C20231Bp(this));
        }
    }

    public static final C28591f3 A00(AnonymousClass1XY r2) {
        return new C28591f3(AnonymousClass0UX.A07(r2));
    }

    public C28591f3(AnonymousClass1Y6 r1) {
        this.A02 = r1;
    }
}
