package X;

import java.nio.ByteBuffer;

/* renamed from: X.1SE  reason: invalid class name */
public final class AnonymousClass1SE implements AnonymousClass1SS {
    public AnonymousClass1PS A00;
    private final int A01;

    private synchronized void A00() {
        if (isClosed()) {
            throw new AnonymousClass97C();
        }
    }

    public synchronized void close() {
        AnonymousClass1PS.A05(this.A00);
        this.A00 = null;
    }

    public synchronized ByteBuffer getByteBuffer() {
        return ((AnonymousClass1NZ) this.A00.A0A()).getByteBuffer();
    }

    public synchronized long getNativePtr() {
        A00();
        return ((AnonymousClass1NZ) this.A00.A0A()).getNativePtr();
    }

    public synchronized boolean isClosed() {
        return !AnonymousClass1PS.A07(this.A00);
    }

    public synchronized int size() {
        A00();
        return this.A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r4 > ((X.AnonymousClass1NZ) r3.A0A()).getSize()) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1SE(X.AnonymousClass1PS r3, int r4) {
        /*
            r2 = this;
            r2.<init>()
            X.C05520Zg.A02(r3)
            if (r4 < 0) goto L_0x0015
            java.lang.Object r0 = r3.A0A()
            X.1NZ r0 = (X.AnonymousClass1NZ) r0
            int r1 = r0.getSize()
            r0 = 1
            if (r4 <= r1) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            X.C05520Zg.A04(r0)
            X.1PS r0 = r3.clone()
            r2.A00 = r0
            r2.A01 = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1SE.<init>(X.1PS, int):void");
    }

    public synchronized byte read(int i) {
        A00();
        boolean z = true;
        boolean z2 = false;
        if (i >= 0) {
            z2 = true;
        }
        C05520Zg.A04(z2);
        if (i >= this.A01) {
            z = false;
        }
        C05520Zg.A04(z);
        return ((AnonymousClass1NZ) this.A00.A0A()).read(i);
    }

    public synchronized int read(int i, byte[] bArr, int i2, int i3) {
        A00();
        boolean z = false;
        if (i + i3 <= this.A01) {
            z = true;
        }
        C05520Zg.A04(z);
        return ((AnonymousClass1NZ) this.A00.A0A()).read(i, bArr, i2, i3);
    }
}
