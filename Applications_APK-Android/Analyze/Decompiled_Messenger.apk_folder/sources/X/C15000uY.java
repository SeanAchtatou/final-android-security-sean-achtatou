package X;

import android.content.Context;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.inject.ContextScoped;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.user.model.UserFbidIdentifier;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.util.Collection;
import java.util.Iterator;

@ContextScoped
/* renamed from: X.0uY  reason: invalid class name and case insensitive filesystem */
public final class C15000uY {
    private static C04470Uu A02;
    public final C04310Tq A00;
    private final Context A01;

    public static final C15000uY A00(AnonymousClass1XY r6) {
        C15000uY r0;
        synchronized (C15000uY.class) {
            C04470Uu A002 = C04470Uu.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r6)) {
                    AnonymousClass1XY r4 = (AnonymousClass1XY) A02.A01();
                    C04470Uu r3 = A02;
                    Context A003 = AnonymousClass1YA.A00(r4);
                    C04310Tq A03 = C10580kT.A03(r4);
                    C28801fO.A00(r4);
                    r3.A00 = new C15000uY(A003, A03);
                }
                C04470Uu r1 = A02;
                r0 = (C15000uY) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public ThreadKey A01(long j) {
        return ThreadKey.A04(j, Long.parseLong(((ViewerContext) this.A00.get()).mUserId));
    }

    public ThreadKey A03(UserKey userKey) {
        String A05;
        C25651aB r1 = userKey.type;
        if (r1 == C25651aB.A03) {
            return A01(Long.parseLong(userKey.id));
        }
        if (r1 == C25651aB.A05) {
            A05 = userKey.A07();
        } else if (r1 == C25651aB.A02) {
            A05 = userKey.A05();
        } else {
            throw new IllegalArgumentException("Unsupported UserKey type.");
        }
        return ThreadKey.A03(AnonymousClass7I7.A00(this.A01, ImmutableSet.A04(A05)));
    }

    public ImmutableList A04(Collection collection) {
        ImmutableList.Builder builder = new ImmutableList.Builder();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            builder.add((Object) A03(UserKey.A01((String) it.next())));
        }
        return builder.build();
    }

    private C15000uY(Context context, C04310Tq r2) {
        this.A01 = context;
        this.A00 = r2;
    }

    public ThreadKey A02(UserFbidIdentifier userFbidIdentifier) {
        return A01(Long.parseLong(userFbidIdentifier.getId()));
    }
}
