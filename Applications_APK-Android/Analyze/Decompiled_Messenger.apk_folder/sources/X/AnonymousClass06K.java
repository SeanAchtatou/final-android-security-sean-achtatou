package X;

import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.06K  reason: invalid class name */
public final class AnonymousClass06K {
    public static void A00(int i) {
        int i2 = AnonymousClass00n.A08;
        Logger.writeStandardEntry(i2, 6, 23, 0, 0, i, 0, 0);
        if (!TraceEvents.isEnabled(i2)) {
            C05970ad.A00();
        }
    }

    public static void A01(String str, int i) {
        if (!TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            C05970ad.A01(str);
            return;
        }
        Logger.writeBytesEntry(AnonymousClass00n.A08, 1, 83, Logger.writeStandardEntry(AnonymousClass00n.A08, 7, 22, 0, 0, i, 0, 0), str);
    }
}
