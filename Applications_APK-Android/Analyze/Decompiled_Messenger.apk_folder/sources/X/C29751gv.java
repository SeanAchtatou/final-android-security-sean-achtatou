package X;

/* renamed from: X.1gv  reason: invalid class name and case insensitive filesystem */
public final class C29751gv {
    public static Integer A00(String str) {
        if (str.equals("UNSET")) {
            return AnonymousClass07B.A00;
        }
        if (str.equals("CONNECTED")) {
            return AnonymousClass07B.A01;
        }
        if (str.equals("NO_CONNECTION")) {
            return AnonymousClass07B.A0C;
        }
        throw new IllegalArgumentException(str);
    }

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "CONNECTED";
            case 2:
                return "NO_CONNECTION";
            default:
                return "UNSET";
        }
    }
}
