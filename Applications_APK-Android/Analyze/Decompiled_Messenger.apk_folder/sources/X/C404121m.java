package X;

import android.net.Uri;

/* renamed from: X.21m  reason: invalid class name and case insensitive filesystem */
public final class C404121m implements AnonymousClass2NX {
    public final /* synthetic */ AnonymousClass210 A00;

    public C404121m(AnonymousClass210 r1) {
        this.A00 = r1;
    }

    public void BRd(Uri uri, boolean z, C06880cE r6) {
        if (!z) {
            AnonymousClass210 r2 = this.A00;
            r2.A0D("peerstate:" + uri);
        }
    }
}
