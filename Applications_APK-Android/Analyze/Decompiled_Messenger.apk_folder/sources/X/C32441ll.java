package X;

import java.io.Serializable;
import java.util.Map;

/* renamed from: X.1ll  reason: invalid class name and case insensitive filesystem */
public final class C32441ll implements Serializable {
    private static final long serialVersionUID = 1;
    public Map mChannelGroupSetting;
    public Map mChannelSettings;
    public Map mChannelToGroupMap;
    public Map mNotifIdToChannelMap;
    public final int mVersion;

    public C32441ll(Map map, Map map2, Map map3, Map map4, int i) {
        this.mChannelGroupSetting = map;
        this.mChannelSettings = map2;
        this.mNotifIdToChannelMap = map4;
        this.mChannelToGroupMap = map3;
        this.mVersion = i;
    }
}
