package X;

import android.accounts.Account;
import android.content.Context;
import android.text.TextUtils;
import com.facebook.acra.constants.ErrorReportingConstants;
import com.facebook.auth.credentials.FacebookCredentials;
import com.facebook.auth.viewercontext.ViewerContext;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.facebook.user.model.Name;
import com.facebook.user.model.User;
import com.facebook.user.model.UserEmailAddress;
import com.facebook.user.profilepic.PicSquareUrlWithSize;
import com.facebook.user.profilepic.ProfilePicUriWithFilePath;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.SettableFuture;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.0XN  reason: invalid class name */
public final class AnonymousClass0XN {
    private static volatile AnonymousClass0XN A0K;
    public C001500z A00;
    public User A01;
    public SettableFuture A02 = null;
    public boolean A03;
    public boolean A04;
    private AnonymousClass0UN A05;
    private Set A06;
    private ExecutorService A07;
    public final Context A08;
    public final AnonymousClass0XO A09;
    public final C05790aK A0A;
    public final C05810aM A0B;
    public final C25171Yp A0C;
    public final FbSharedPreferences A0D;
    public final ExecutorService A0E;
    public final AtomicBoolean A0F = new AtomicBoolean(false);
    public final AtomicReference A0G = new AtomicReference();
    public final AtomicReference A0H = new AtomicReference();
    private final C05890aV A0I;
    private final AnonymousClass09P A0J;

    private synchronized void A04(User user) {
        this.A0J.Bz1(user.A0j);
        this.A0J.Bz2("partial_user", Boolean.toString(user.A1c));
    }

    public static void A05(User user, AnonymousClass16O r11) {
        ImmutableList<PicSquareUrlWithSize> immutableList;
        int size;
        String str = user.A0j;
        Preconditions.checkArgument(!TextUtils.isEmpty(str), "No ID in logged-in user");
        r11.A0B(ErrorReportingConstants.USER_ID_KEY, str);
        Name name = user.A0L;
        if (name != null) {
            A06("first_name", name.firstName, r11);
            A06("last_name", name.lastName, r11);
            A06("name", name.displayName, r11);
        }
        r11.A09("birth_date_year", user.A07);
        r11.A09("birth_date_month", user.A06);
        r11.A09("birth_date_day", user.A05);
        A06("city", user.A0g, r11);
        A06("postal_code", user.A0v, r11);
        A06("region", user.A0y, r11);
        A06("current_location_prediction", user.A0i, r11);
        int i = user.A08;
        if (i != 0) {
            r11.A0B("gender", C07230cz.A01(i));
        }
        A06("primary_contact", user.A0w, r11);
        ImmutableList immutableList2 = user.A0T;
        ImmutableList immutableList3 = immutableList2;
        if (immutableList2 != null && (size = immutableList2.size()) > 0) {
            HashSet hashSet = new HashSet(size);
            for (int i2 = 0; i2 < size; i2++) {
                UserEmailAddress userEmailAddress = (UserEmailAddress) immutableList3.get(i2);
                if (userEmailAddress != null) {
                    String str2 = userEmailAddress.A00;
                    if (!TextUtils.isEmpty(str2)) {
                        hashSet.add(str2);
                    }
                }
            }
            r11.A0C("emails", hashSet);
        }
        A06("phones", user.A0C(), r11);
        A06("pic_square", user.A0D(), r11);
        if (user.A1n == null) {
            synchronized (user) {
                if (user.A1n == null) {
                    String str3 = null;
                    if (!(user.A1l == null || (immutableList = user.A1l.mPicSquareUrlsWithSizes) == null || immutableList.isEmpty())) {
                        JSONArray jSONArray = new JSONArray();
                        int size2 = immutableList.size();
                        for (int i3 = 0; i3 < size2; i3++) {
                            PicSquareUrlWithSize picSquareUrlWithSize = immutableList.get(i3);
                            try {
                                JSONObject jSONObject = new JSONObject();
                                jSONObject.put("profile_pic_size", picSquareUrlWithSize.size);
                                jSONObject.put("profile_pic_url", picSquareUrlWithSize.url);
                                jSONArray.put(jSONObject);
                            } catch (JSONException e) {
                                C010708t.A0L("User", "Profile square pic serialization", e);
                            }
                        }
                        str3 = jSONArray.toString();
                    }
                    user.A1n = str3;
                }
            }
        }
        A06("profile_pic_square", user.A1n, r11);
        ProfilePicUriWithFilePath profilePicUriWithFilePath = user.A1m;
        if (profilePicUriWithFilePath != null) {
            A06("inbox_profile_pic_uri", profilePicUriWithFilePath.profilePicUri, r11);
            A06("inbox_profile_pic_file_path", profilePicUriWithFilePath.filePath, r11);
        }
        A06("profile_pic_round", user.A0u, r11);
        A06("pic_cover", user.A0h, r11);
        float f = user.A04;
        AnonymousClass16O.A04(r11);
        Map map = r11.A00;
        C05770aI.A03("rank");
        map.put("rank", Float.valueOf(f));
        TriState triState = user.A0G;
        if (triState != TriState.UNSET) {
            r11.A0D("is_pushable", triState.asBoolean(false));
        }
        if (user.A14) {
            r11.A0D("is_employee", true);
        }
        if (user.A1A) {
            r11.A0D("is_work_user", true);
        }
        A06("type", user.A0x, r11);
        if (user.A1c) {
            r11.A0D("is_partial", true);
        }
        if (user.A1g) {
            r11.A0D("messenger_only_user_has_password", true);
        }
        if (user.A1b) {
            r11.A0D("is_minor", true);
        }
        TriState triState2 = user.A0H;
        if (triState2 != TriState.UNSET) {
            r11.A0D("profile_picture_is_silhouette", triState2.asBoolean(false));
        }
        if (user.A1J) {
            r11.A0D("has_profile_video", true);
        }
        r11.A0A("montage_thread_fbid", user.A0D);
        if (user.A1C) {
            r11.A0D("can_see_viewer_montage_thread", true);
        }
        if (user.A11) {
            r11.A0D("is_broadcast_recipient_holdout", false);
        }
        if (user.A1Q) {
            r11.A0D("is_deactivated_allowed_on_messenger", true);
        }
        if (user.A1X) {
            r11.A0D("is_messenger_only_deactivated", true);
        }
        Integer num = user.A0d;
        if (num != null) {
            r11.A0B("messenger_unified_stories_audience_mode", AnonymousClass0j0.A01(num));
        }
        r11.A0D("messenger_should_show_unified_stories_nux", user.A1h);
        r11.A0D("has_posted_to_messenger_stories", user.A1H);
        r11.A0D("has_posted_to_facebook_stories", user.A1G);
        r11.A0D("has_posted_to_unified_stories", user.A1I);
        A06("messenger_only_user_cloud_drive_backup_email", user.A0q, r11);
        A06("messenger_connected_instagram_username", user.A0l, r11);
        if (user.A1S) {
            r11.A0D("instagram_contact_import_enabled", true);
        }
        if (user.A1B) {
            r11.A0D("can_disconnect_instagram_account", true);
        }
        if (user.A1K) {
            r11.A0D("is_aloha_proxy_confirmed", true);
        }
        r11.A09("fb_friends_on_ig_count", user.A0A);
        r11.A0D("is_verified", user.A1d);
        r11.A0D("is_coworker", user.A1O);
        r11.A0A("registration_time", user.A0E);
        A06("username", user.A0s, r11);
        r11.A0B(C99084oO.$const$string(55), user.A0n);
        r11.A0B("account_status", user.A0k);
        A06("mme_referral_uri", user.A0r, r11);
        r11.A09("key_is_in_story_holdout", user.A0B);
        r11.A09("key_admined_page_count", user.A09);
    }

    public synchronized User A09() {
        boolean z;
        synchronized (this) {
            z = this.A0F.get();
        }
        if (z) {
            return null;
        }
        return A02();
    }

    public synchronized void A0A() {
        this.A03 = false;
        for (Runnable A022 : this.A06) {
            AnonymousClass07A.A02(this.A07, A022, -773810413);
        }
    }

    public synchronized void A0B() {
        this.A0F.set(false);
    }

    public synchronized void A0C(FacebookCredentials facebookCredentials) {
        AnonymousClass0XO r3 = this.A09;
        if (!r3.A02.A0B("is_imported", false)) {
            FbSharedPreferences fbSharedPreferences = (FbSharedPreferences) r3.A03.get();
            if (fbSharedPreferences.BBh(C10730kl.A0C) || fbSharedPreferences.BBh(C10730kl.A0B)) {
                AnonymousClass0XO.A01(r3);
            }
        }
        AnonymousClass16O A062 = r3.A02.A06();
        String str = facebookCredentials.A07;
        String str2 = facebookCredentials.A06;
        String str3 = facebookCredentials.A00;
        String str4 = facebookCredentials.A01;
        String str5 = facebookCredentials.A03;
        String str6 = facebookCredentials.A05;
        String str7 = facebookCredentials.A04;
        String str8 = facebookCredentials.A08;
        String str9 = facebookCredentials.A02;
        Preconditions.checkArgument(!TextUtils.isEmpty(str), "No user ID in credentials");
        Preconditions.checkArgument(!TextUtils.isEmpty(str2), "No token in credentials");
        AnonymousClass16O.A04(A062);
        A062.A01 = true;
        A062.A0B(ErrorReportingConstants.USER_ID_KEY, str);
        A062.A0B("access_token", str2);
        A062.A0B("page_admin_uid", str3);
        A062.A0B("page_admin_access_token", str4);
        A062.A0B("session_cookies_string", str5);
        A062.A0B("secret", str6);
        A062.A0B("session_key", str7);
        A062.A0B("username", str8);
        A062.A0B("analytics_claim", str9);
        A062.A0D("is_imported", true);
        A062.A06();
        this.A0H.set(null);
        this.A0G.set(null);
        this.A01 = null;
        this.A0F.set(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x010d, code lost:
        if (r2 == X.C001500z.A07) goto L_0x010f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0112  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D(com.facebook.user.model.User r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            int r2 = X.AnonymousClass1Y3.Az0     // Catch:{ all -> 0x011e }
            X.0UN r1 = r8.A05     // Catch:{ all -> 0x011e }
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x011e }
            X.16D r0 = (X.AnonymousClass16D) r0     // Catch:{ all -> 0x011e }
            r0.A01(r9)     // Catch:{ all -> 0x011e }
            java.lang.String r3 = "LoggedInUserSessionManager"
            X.0aK r5 = r8.A0A     // Catch:{ all -> 0x011e }
            if (r9 == 0) goto L_0x0086
            X.0cv r0 = new X.0cv     // Catch:{ all -> 0x011e }
            r0.<init>()     // Catch:{ all -> 0x011e }
            r0.A04(r9)     // Catch:{ all -> 0x011e }
            com.facebook.user.model.User r7 = r0.A02()     // Catch:{ all -> 0x011e }
            java.lang.String r0 = r7.A0j     // Catch:{ all -> 0x011e }
            X.0aI r1 = X.C05790aK.A01(r5, r0)     // Catch:{ all -> 0x011e }
            if (r1 == 0) goto L_0x0086
            X.16O r6 = r1.A06()     // Catch:{ all -> 0x011e }
            r0 = 0
            java.lang.String r4 = "is_imported"
            boolean r0 = r1.A0B(r4, r0)     // Catch:{ all -> 0x011e }
            r2 = 1
            if (r0 != 0) goto L_0x005f
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x011e }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x011e }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x011e }
            com.facebook.prefs.shared.FbSharedPreferences r1 = (com.facebook.prefs.shared.FbSharedPreferences) r1     // Catch:{ all -> 0x011e }
            X.1Y7 r0 = X.C10730kl.A07     // Catch:{ all -> 0x011e }
            boolean r0 = r1.BBh(r0)     // Catch:{ all -> 0x011e }
            if (r0 == 0) goto L_0x005f
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x011e }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x011e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x011e }
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0     // Catch:{ all -> 0x011e }
            X.1hn r1 = r0.edit()     // Catch:{ all -> 0x011e }
            X.1Y7 r0 = X.C10730kl.A07     // Catch:{ all -> 0x011e }
            r1.C1B(r0)     // Catch:{ all -> 0x011e }
            r1.commit()     // Catch:{ all -> 0x011e }
        L_0x005f:
            X.AnonymousClass16O.A04(r6)     // Catch:{ all -> 0x011e }
            r0 = 1
            r6.A01 = r2     // Catch:{ all -> 0x011e }
            A05(r7, r6)     // Catch:{ all -> 0x011e }
            r6.A0D(r4, r2)     // Catch:{ all -> 0x011e }
            r6.A06()     // Catch:{ all -> 0x011e }
            java.lang.String r4 = r9.A0j     // Catch:{ all -> 0x011e }
            int r2 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x011e }
            X.0UN r1 = r5.A00     // Catch:{ all -> 0x011e }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ all -> 0x011e }
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0     // Catch:{ all -> 0x011e }
            X.1hn r1 = r0.edit()     // Catch:{ all -> 0x011e }
            X.1Y7 r0 = X.C04350Ue.A04     // Catch:{ all -> 0x011e }
            r1.BzC(r0, r4)     // Catch:{ all -> 0x011e }
            r1.commit()     // Catch:{ all -> 0x011e }
        L_0x0086:
            r8.A01 = r9     // Catch:{ all -> 0x011e }
            r8.A04(r9)     // Catch:{ all -> 0x011e }
            android.content.Context r0 = r8.A08     // Catch:{ IOException -> 0x00ef }
            boolean r2 = r9.A14     // Catch:{ IOException -> 0x00ef }
            java.io.File r4 = new java.io.File     // Catch:{ IOException -> 0x00ef }
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()     // Catch:{ IOException -> 0x00ef }
            java.lang.String r1 = r0.dataDir     // Catch:{ IOException -> 0x00ef }
            java.lang.String r0 = "flags"
            r4.<init>(r1, r0)     // Catch:{ IOException -> 0x00ef }
            boolean r0 = r4.mkdirs()     // Catch:{ IOException -> 0x00ef }
            if (r0 != 0) goto L_0x00bf
            boolean r0 = r4.isDirectory()     // Catch:{ IOException -> 0x00ef }
            if (r0 != 0) goto L_0x00bf
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x00ef }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ef }
            r1.<init>()     // Catch:{ IOException -> 0x00ef }
            java.lang.String r0 = "could not create directory: "
            r1.append(r0)     // Catch:{ IOException -> 0x00ef }
            r1.append(r4)     // Catch:{ IOException -> 0x00ef }
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x00ef }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00ef }
        L_0x00be:
            throw r2     // Catch:{ IOException -> 0x00ef }
        L_0x00bf:
            r0 = 1
            boolean r0 = r4.setExecutable(r0, r0)     // Catch:{ IOException -> 0x00ef }
            if (r0 == 0) goto L_0x00ce
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x00ef }
            java.lang.String r0 = "is_employee"
            r1.<init>(r4, r0)     // Catch:{ IOException -> 0x00ef }
            goto L_0x00e5
        L_0x00ce:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x00ef }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ef }
            r1.<init>()     // Catch:{ IOException -> 0x00ef }
            java.lang.String r0 = "cannot set traverse bit on: "
            r1.append(r0)     // Catch:{ IOException -> 0x00ef }
            r1.append(r4)     // Catch:{ IOException -> 0x00ef }
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x00ef }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00ef }
            goto L_0x00be
        L_0x00e5:
            if (r2 == 0) goto L_0x00eb
            r1.createNewFile()     // Catch:{ IOException -> 0x00ef }
            goto L_0x00f5
        L_0x00eb:
            r1.delete()     // Catch:{ IOException -> 0x00ef }
            goto L_0x00f5
        L_0x00ef:
            r1 = move-exception
            java.lang.String r0 = "could not set employee flag"
            X.C010708t.A0S(r3, r1, r0)     // Catch:{ all -> 0x011e }
        L_0x00f5:
            com.google.common.util.concurrent.SettableFuture r1 = r8.A02     // Catch:{ all -> 0x011e }
            if (r1 == 0) goto L_0x00fe
            com.facebook.user.model.User r0 = r8.A01     // Catch:{ all -> 0x011e }
            r1.set(r0)     // Catch:{ all -> 0x011e }
        L_0x00fe:
            monitor-enter(r8)     // Catch:{ all -> 0x011e }
            r0 = 0
            r8.A02 = r0     // Catch:{ all -> 0x011b }
            monitor-exit(r8)     // Catch:{ all -> 0x011b }
            monitor-exit(r8)     // Catch:{ all -> 0x011e }
            X.00z r2 = r8.A00
            X.00z r0 = X.C001500z.A03
            if (r2 == r0) goto L_0x010f
            X.00z r1 = X.C001500z.A07
            r0 = 0
            if (r2 != r1) goto L_0x0110
        L_0x010f:
            r0 = 1
        L_0x0110:
            if (r0 == 0) goto L_0x0115
            r8.A03()
        L_0x0115:
            X.0aM r0 = r8.A0B
            r0.A02()
            return
        L_0x011b:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x011b }
            throw r0     // Catch:{ all -> 0x011e }
        L_0x011e:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x011e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XN.A0D(com.facebook.user.model.User):void");
    }

    public synchronized void A0E(User user) {
        int i;
        String str;
        int i2;
        if (C99084oO.$const$string(AnonymousClass1Y3.A2H).equals(user.A0n)) {
            ((AnonymousClass16D) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Az0, this.A05)).A01(user);
        }
        if (this.A01 != null) {
            C07220cv r2 = new C07220cv();
            r2.A04(user);
            User user2 = this.A01;
            String str2 = user2.A0n;
            String str3 = r2.A0p;
            if (str2 == null) {
                str2 = "UNKNOWN";
            }
            if (str3 == null) {
                str3 = "UNKNOWN";
            }
            r2.A0p = AnonymousClass08S.A0P(str2.split("#")[0], "#", str3);
            if (r2.A02 == 0 && (i2 = user2.A08) != 0) {
                r2.A02 = i2;
            }
            if (r2.A0E == TriState.UNSET) {
                r2.A0E = user2.A0G;
            }
            if (r2.A16.isEmpty()) {
                r2.A16 = this.A01.A0T;
            }
            if (r2.A17.isEmpty()) {
                r2.A17 = this.A01.A05();
            }
            User user3 = this.A01;
            String str4 = user3.A0k;
            if (str4 != null && !"unknown".equals(str4) && ((str = r2.A0k) == null || "unknown".equals(str))) {
                r2.A0k = str4;
            }
            if (user3.A14) {
                r2.A1A = true;
            }
            if (user3.A1A) {
                r2.A1C = true;
            }
            if (user3.A1c) {
                r2.A1i = true;
            }
            if (user3.A1Q) {
                r2.A1T = true;
            }
            if (user3.A1X) {
                r2.A1d = true;
            }
            if (user3.A1b) {
                r2.A1h = true;
            }
            if (user3.A1K) {
                r2.A1M = true;
            }
            if (user3.A1d) {
                r2.A1k = true;
            }
            if (user3.A1O) {
                r2.A1R = true;
            }
            long j = user3.A0E;
            if (j != 0) {
                r2.A0D = j;
            }
            if (r2.A05 == 0 || r2.A04 == 0) {
                if (r2.A06 == 0) {
                    int i3 = user3.A07;
                    int i4 = user3.A06;
                    i = user3.A05;
                    r2.A06 = i3;
                    r2.A05 = i4;
                } else {
                    int i5 = user3.A06;
                    i = user3.A05;
                    r2.A05 = i5;
                }
                r2.A04 = i;
            }
            if (r2.A0l == null) {
                r2.A0l = user3.A0g;
            }
            if (r2.A0u == null) {
                r2.A0u = user3.A0v;
            }
            if (r2.A0w == null) {
                r2.A0w = user3.A0y;
            }
            if (r2.A0o == null) {
                r2.A0o = user3.A0i;
            }
            if (r2.A0F == TriState.UNSET) {
                r2.A0F = user3.A0H;
            }
            if (r2.A0C == 0) {
                r2.A0C = user3.A0D;
            }
            if (r2.A0c == null) {
                r2.A0c = user3.A0d;
            }
            if (user3.A1h) {
                r2.A1o = true;
            }
            if (user3.A1H) {
                r2.A1J = true;
            }
            if (user3.A1G) {
                r2.A1I = true;
            }
            if (user3.A1I) {
                r2.A1K = true;
            }
            if (user3.A1g) {
                r2.A1n = true;
            }
            r2.A08 = r2.A08;
            if (C06850cB.A0B(r2.A0s)) {
                r2.A0s = this.A01.A0q;
            }
            if (r2.A0m == null) {
                r2.A0m = this.A01.A0l;
            }
            if (!r2.A1X) {
                r2.A1X = this.A01.A1S;
            }
            if (!r2.A1D) {
                r2.A1D = this.A01.A1B;
            }
            if (r2.A12 == null) {
                r2.A12 = this.A01.A0w;
            }
            if (r2.A0x == null) {
                r2.A0x = this.A01.A0s;
            }
            if (r2.A0t == null) {
                r2.A0t = this.A01.A0r;
            }
            r2.A03 = this.A01.A09;
            user = r2.A02();
        }
        A0D(user);
    }

    public synchronized void A0F(boolean z) {
        AnonymousClass0XO r2 = this.A09;
        AnonymousClass16O A062 = r2.A02.A06();
        A062.A08(ErrorReportingConstants.USER_ID_KEY);
        A062.A08("page_admin_uid");
        A062.A08("page_admin_access_token");
        A062.A08("access_token");
        A062.A08("session_cookies_string");
        A062.A08("secret");
        A062.A08("session_key");
        A062.A08("username");
        A062.A08("analytics_claim");
        A062.A06();
        if (!z) {
            if (r2.A00 == null) {
                r2.A00 = r2.A01.A00("underlying_account");
            }
            AnonymousClass16O A063 = r2.A00.A06();
            AnonymousClass16O.A04(A063);
            A063.A01 = true;
            A063.A06();
        }
    }

    public synchronized void A0G(boolean z) {
        synchronized (this) {
            try {
                this.A01 = null;
            } catch (Throwable th) {
                th = th;
                throw th;
            }
        }
        synchronized (this) {
            try {
                synchronized (this) {
                    this.A0F.set(true);
                }
                this.A0H.set(null);
                this.A0G.set(null);
                A0F(z);
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public synchronized boolean A0H() {
        boolean z;
        z = false;
        if (0 != 0) {
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (A08() == null) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0I() {
        /*
            r2 = this;
            monitor-enter(r2)
            com.facebook.user.model.User r0 = r2.A02()     // Catch:{ all -> 0x0011 }
            if (r0 == 0) goto L_0x000e
            com.facebook.auth.viewercontext.ViewerContext r1 = r2.A08()     // Catch:{ all -> 0x0011 }
            r0 = 1
            if (r1 != 0) goto L_0x000f
        L_0x000e:
            r0 = 0
        L_0x000f:
            monitor-exit(r2)
            return r0
        L_0x0011:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XN.A0I():boolean");
    }

    public synchronized boolean A0J() {
        return this.A03;
    }

    public static final AnonymousClass0XN A01(AnonymousClass1XY r14) {
        if (A0K == null) {
            synchronized (AnonymousClass0XN.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0K, r14);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r14.getApplicationInjector();
                        A0K = new AnonymousClass0XN(applicationInjector, FbSharedPreferencesModule.A00(applicationInjector), AnonymousClass0XO.A00(applicationInjector), C04750Wa.A01(applicationInjector), new C05790aK(applicationInjector), AnonymousClass1YA.A02(applicationInjector), C25171Yp.A00(applicationInjector), C05810aM.A00(applicationInjector), new C05890aV(applicationInjector), AnonymousClass0UU.A05(applicationInjector), AnonymousClass0UX.A0a(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0K;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:265:?, code lost:
        r0 = X.AnonymousClass07B.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:?, code lost:
        r0 = X.AnonymousClass07B.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x07d1, code lost:
        if (r2 == X.C001500z.A07) goto L_0x07d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x07dd, code lost:
        if (r2 == X.C001500z.A07) goto L_0x07df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:323:0x0848, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:325:0x084a, code lost:
        return r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:264:0x06e5 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:268:0x06ef */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x042f A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0441 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x0453 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0465 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0477 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x048a A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x04a0 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x04b2 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x04c4 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x04d6 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x04e8 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x04fa A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x050c A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x051e A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0530 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x0542 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x0556 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x056e A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x058a A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x059e A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x05b2 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x05c8 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x05dc A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x05f4 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:225:0x060c A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x0627 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x063f A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x0653 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x0667 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x067b A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x068f A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x06a3 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x06b7 A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x06cb A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x06de A[Catch:{ IOException -> 0x06fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x06f7 A[Catch:{ IOException -> 0x06fd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.facebook.user.model.User A02() {
        /*
            r23 = this;
            r6 = r23
            monitor-enter(r6)
            com.facebook.user.model.User r0 = r6.A01     // Catch:{ all -> 0x084b }
            if (r0 != 0) goto L_0x0849
            com.facebook.auth.viewercontext.ViewerContext r8 = r6.A08()     // Catch:{ all -> 0x084b }
            r11 = 0
            if (r8 == 0) goto L_0x0847
            X.0aK r7 = r6.A0A     // Catch:{ all -> 0x084b }
            java.lang.String r0 = r8.mUserId     // Catch:{ all -> 0x084b }
            X.0aI r3 = X.C05790aK.A01(r7, r0)     // Catch:{ all -> 0x084b }
            r2 = 0
            if (r3 == 0) goto L_0x07c4
            r0 = 0
            java.lang.String r10 = "is_imported"
            boolean r0 = r3.A0B(r10, r0)     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x01dd
            X.1aB r5 = X.C25651aB.A03     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "uid"
            java.lang.String r1 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x084b }
            if (r0 != 0) goto L_0x07c4
            X.0cv r2 = new X.0cv     // Catch:{ all -> 0x084b }
            r2.<init>()     // Catch:{ all -> 0x084b }
            r2.A03(r5, r1)     // Catch:{ all -> 0x084b }
            com.facebook.user.model.Name r7 = new com.facebook.user.model.Name     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "first_name"
            java.lang.String r5 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "last_name"
            java.lang.String r1 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "name"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r7.<init>(r5, r1, r0)     // Catch:{ all -> 0x084b }
            r2.A0J = r7     // Catch:{ all -> 0x084b }
            r1 = 0
            java.lang.String r0 = "birth_date_year"
            int r7 = r3.A04(r0, r1)     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "birth_date_month"
            int r5 = r3.A04(r0, r1)     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "birth_date_day"
            int r0 = r3.A04(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A06 = r7     // Catch:{ all -> 0x084b }
            r2.A05 = r5     // Catch:{ all -> 0x084b }
            r2.A04 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "city"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0l = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "postal_code"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0u = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "region"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0w = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "current_location_prediction"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0o = r0     // Catch:{ all -> 0x084b }
            java.lang.String r1 = "gender"
            java.lang.String r1 = r3.A07(r1, r11)     // Catch:{ all -> 0x084b }
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x0098
            r0 = 0
            goto L_0x009e
        L_0x0098:
            int r0 = X.C07230cz.A00(r1)     // Catch:{ IllegalArgumentException -> 0x009d }
            goto L_0x009e
        L_0x009d:
            r0 = 0
        L_0x009e:
            r2.A02 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "primary_contact"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A12 = r0     // Catch:{ all -> 0x084b }
            com.google.common.collect.ImmutableList$Builder r9 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "emails"
            java.util.Set r0 = r3.A09(r0, r11)     // Catch:{ all -> 0x084b }
            if (r0 != 0) goto L_0x00b6
            r0 = r11
            goto L_0x00d4
        L_0x00b6:
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x084b }
        L_0x00ba:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x00d0
            java.lang.Object r5 = r7.next()     // Catch:{ all -> 0x084b }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x084b }
            com.facebook.user.model.UserEmailAddress r1 = new com.facebook.user.model.UserEmailAddress     // Catch:{ all -> 0x084b }
            r0 = 0
            r1.<init>(r5, r0)     // Catch:{ all -> 0x084b }
            r9.add(r1)     // Catch:{ all -> 0x084b }
            goto L_0x00ba
        L_0x00d0:
            com.google.common.collect.ImmutableList r0 = r9.build()     // Catch:{ all -> 0x084b }
        L_0x00d4:
            r2.A16 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "phones"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0z = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "pic_square"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A11 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "profile_pic_square"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0v = r0     // Catch:{ all -> 0x084b }
            com.facebook.user.profilepic.ProfilePicUriWithFilePath r5 = new com.facebook.user.profilepic.ProfilePicUriWithFilePath     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "inbox_profile_pic_uri"
            java.lang.String r1 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "inbox_profile_pic_file_path"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r5.<init>(r1, r0)     // Catch:{ all -> 0x084b }
            r2.A0Q = r5     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "profile_pic_round"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A10 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "pic_cover"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0f = r0     // Catch:{ all -> 0x084b }
            java.lang.String r7 = "rank"
            r5 = 0
            X.C05770aI.A01(r3)     // Catch:{ all -> 0x084b }
            java.lang.Object r1 = r3.A02     // Catch:{ all -> 0x084b }
            monitor-enter(r1)     // Catch:{ all -> 0x084b }
            java.util.Map r0 = r3.A04     // Catch:{ ClassCastException -> 0x01d3 }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ ClassCastException -> 0x01d3 }
            java.lang.Float r0 = (java.lang.Float) r0     // Catch:{ ClassCastException -> 0x01d3 }
            if (r0 == 0) goto L_0x0128
            float r5 = r0.floatValue()     // Catch:{ ClassCastException -> 0x01d3 }
        L_0x0128:
            monitor-exit(r1)     // Catch:{ all -> 0x01d9 }
            r2.A01 = r5     // Catch:{ all -> 0x084b }
            java.lang.String r1 = "is_pushable"
            boolean r0 = r3.A0A(r1)     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x01c7
            r0 = 0
            boolean r0 = r3.A0B(r1, r0)     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x01c3
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x084b }
        L_0x013c:
            r2.A0E = r0     // Catch:{ all -> 0x084b }
            r1 = 0
            java.lang.String r0 = "is_employee"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1A = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_work_user"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1C = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "type"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A13 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_partial"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1i = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "messenger_only_user_has_password"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1n = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_minor"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1h = r0     // Catch:{ all -> 0x084b }
            java.lang.String r1 = "profile_picture_is_silhouette"
            boolean r0 = r3.A0A(r1)     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x01c0
            r0 = 0
            boolean r0 = r3.A0B(r1, r0)     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x01bd
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x084b }
        L_0x0180:
            r2.A0F = r0     // Catch:{ all -> 0x084b }
            r1 = 0
            java.lang.String r0 = "has_profile_video"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1L = r0     // Catch:{ all -> 0x084b }
            r0 = 0
            java.lang.String r5 = "montage_thread_fbid"
            long r0 = r3.A05(r5, r0)     // Catch:{ all -> 0x084b }
            r2.A0C = r0     // Catch:{ all -> 0x084b }
            r1 = 0
            java.lang.String r0 = "can_see_viewer_montage_thread"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1E = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_broadcast_recipient_holdout"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1O = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_deactivated_allowed_on_messenger"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1T = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_messenger_only_deactivated"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1d = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "messenger_unified_stories_audience_mode"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            goto L_0x01cb
        L_0x01bd:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x084b }
            goto L_0x0180
        L_0x01c0:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x084b }
            goto L_0x0180
        L_0x01c3:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x084b }
            goto L_0x013c
        L_0x01c7:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x084b }
            goto L_0x013c
        L_0x01cb:
            java.lang.Integer r0 = X.AnonymousClass0j0.A00(r0)     // Catch:{ IllegalArgumentException | NullPointerException -> 0x01d0 }
            goto L_0x01d1
        L_0x01d0:
            r0 = r11
        L_0x01d1:
            goto L_0x071d
        L_0x01d3:
            r0 = move-exception
            java.lang.RuntimeException r0 = X.C05770aI.A00(r3, r0, r7)     // Catch:{ all -> 0x01d9 }
            throw r0     // Catch:{ all -> 0x01d9 }
        L_0x01d9:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01d9 }
            goto L_0x083e
        L_0x01dd:
            int r1 = X.AnonymousClass1Y3.B6q     // Catch:{ all -> 0x084b }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x084b }
            r5 = 1
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x084b }
            com.facebook.prefs.shared.FbSharedPreferences r0 = (com.facebook.prefs.shared.FbSharedPreferences) r0     // Catch:{ all -> 0x084b }
            boolean r0 = r0.BFQ()     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x07c4
            X.16O r9 = r3.A06()     // Catch:{ all -> 0x084b }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x084b }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ all -> 0x084b }
            com.facebook.prefs.shared.FbSharedPreferences r4 = (com.facebook.prefs.shared.FbSharedPreferences) r4     // Catch:{ all -> 0x084b }
            X.1Y7 r0 = X.C10730kl.A07     // Catch:{ all -> 0x084b }
            boolean r0 = r4.BBh(r0)     // Catch:{ all -> 0x084b }
            if (r0 == 0) goto L_0x0715
            X.1Y7 r0 = X.C10730kl.A07     // Catch:{ all -> 0x084b }
            java.lang.String r3 = r4.B4F(r0, r11)     // Catch:{ all -> 0x084b }
            if (r3 == 0) goto L_0x0709
            r2 = 2
            int r1 = X.AnonymousClass1Y3.BSt     // Catch:{ all -> 0x084b }
            X.0UN r0 = r7.A00     // Catch:{ all -> 0x084b }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x084b }
            X.0pi r7 = (X.C12630pi) r7     // Catch:{ all -> 0x084b }
            X.1aB r12 = X.C25651aB.A03     // Catch:{ all -> 0x084b }
            X.0jJ r0 = r7.A01     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r3 = r0.readTree(r3)     // Catch:{ IOException -> 0x06fd }
            X.0cv r2 = new X.0cv     // Catch:{ IOException -> 0x06fd }
            r2.<init>()     // Catch:{ IOException -> 0x06fd }
            java.lang.String r13 = "uid"
            boolean r1 = r3.has(r13)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "Missing id field on profile"
            com.google.common.base.Preconditions.checkArgument(r1, r0)     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r13)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r1 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            if (r1 != 0) goto L_0x0241
            java.lang.String r0 = "id"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r1 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x0241:
            java.lang.String r0 = "UserSerialization"
            r2.A0p = r0     // Catch:{ IOException -> 0x06fd }
            r2.A03(r12, r1)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r13 = "contact_email"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r13)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = com.facebook.common.util.JSONUtil.A0T(r0)     // Catch:{ IOException -> 0x06fd }
            r1 = 0
            if (r0 == 0) goto L_0x0329
            com.facebook.user.model.UserEmailAddress r12 = new com.facebook.user.model.UserEmailAddress     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r13)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r12.<init>(r0, r1)     // Catch:{ IOException -> 0x06fd }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.of(r12)     // Catch:{ IOException -> 0x06fd }
            r2.A16 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0268:
            java.lang.String r12 = "phones"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = com.facebook.common.util.JSONUtil.A0T(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0364
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            com.google.common.collect.ImmutableList$Builder r13 = com.google.common.collect.ImmutableList.builder()     // Catch:{ IOException -> 0x06fd }
            java.util.Iterator r16 = r0.iterator()     // Catch:{ IOException -> 0x06fd }
        L_0x0280:
            boolean r0 = r16.hasNext()     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x035e
            java.lang.Object r12 = r16.next()     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r12 = (com.fasterxml.jackson.databind.JsonNode) r12     // Catch:{ IOException -> 0x06fd }
            java.lang.String r14 = "full_number"
            boolean r0 = r12.has(r14)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0308
            com.fasterxml.jackson.databind.JsonNode r0 = r12.get(r14)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r19 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r0 = 461(0x1cd, float:6.46E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r0 = r12.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r18 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x02aa:
            com.facebook.common.util.TriState r22 = com.facebook.common.util.TriState.UNSET     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "is_verified"
            boolean r14 = r12.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r14 == 0) goto L_0x02c0
            com.fasterxml.jackson.databind.JsonNode r0 = r12.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0305
            com.facebook.common.util.TriState r22 = com.facebook.common.util.TriState.YES     // Catch:{ IOException -> 0x06fd }
        L_0x02c0:
            java.lang.String r0 = "android_type"
            boolean r14 = r12.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r14 == 0) goto L_0x02dd
            com.fasterxml.jackson.databind.JsonNode r0 = r12.get(r0)     // Catch:{ IOException -> 0x06fd }
            int r21 = com.facebook.common.util.JSONUtil.A04(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x02d0:
            com.facebook.user.model.UserPhoneNumber r0 = new com.facebook.user.model.UserPhoneNumber     // Catch:{ IOException -> 0x06fd }
            r17 = r0
            r20 = r19
            r17.<init>(r18, r19, r20, r21, r22)     // Catch:{ IOException -> 0x06fd }
            r13.add(r0)     // Catch:{ IOException -> 0x06fd }
            goto L_0x0280
        L_0x02dd:
            java.lang.String r0 = "type"
            boolean r14 = r12.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r14 == 0) goto L_0x0302
            com.fasterxml.jackson.databind.JsonNode r0 = r12.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r12 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "other_phone"
            boolean r0 = r0.equals(r12)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x02f8
            r21 = 7
            goto L_0x02d0
        L_0x02f8:
            java.lang.String r0 = "cell"
            boolean r0 = r0.equals(r12)     // Catch:{ IOException -> 0x06fd }
            r21 = 2
            if (r0 != 0) goto L_0x02d0
        L_0x0302:
            r21 = 0
            goto L_0x02d0
        L_0x0305:
            com.facebook.common.util.TriState r22 = com.facebook.common.util.TriState.NO     // Catch:{ IOException -> 0x06fd }
            goto L_0x02c0
        L_0x0308:
            r0 = 224(0xe0, float:3.14E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r0 = r12.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r15 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "number"
            com.fasterxml.jackson.databind.JsonNode r0 = r12.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r14 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "+"
            java.lang.String r18 = X.AnonymousClass08S.A0P(r0, r15, r14)     // Catch:{ IOException -> 0x06fd }
            r19 = r18
            goto L_0x02aa
        L_0x0329:
            java.lang.String r12 = "emails"
            boolean r0 = r3.has(r12)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0268
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            com.google.common.collect.ImmutableList$Builder r14 = com.google.common.collect.ImmutableList.builder()     // Catch:{ IOException -> 0x06fd }
            java.util.Iterator r15 = r0.iterator()     // Catch:{ IOException -> 0x06fd }
        L_0x033d:
            boolean r0 = r15.hasNext()     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0356
            java.lang.Object r0 = r15.next()     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r0 = (com.fasterxml.jackson.databind.JsonNode) r0     // Catch:{ IOException -> 0x06fd }
            com.facebook.user.model.UserEmailAddress r13 = new com.facebook.user.model.UserEmailAddress     // Catch:{ IOException -> 0x06fd }
            java.lang.String r12 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r13.<init>(r12, r1)     // Catch:{ IOException -> 0x06fd }
            r14.add(r13)     // Catch:{ IOException -> 0x06fd }
            goto L_0x033d
        L_0x0356:
            com.google.common.collect.ImmutableList r0 = r14.build()     // Catch:{ IOException -> 0x06fd }
            r2.A16 = r0     // Catch:{ IOException -> 0x06fd }
            goto L_0x0268
        L_0x035e:
            com.google.common.collect.ImmutableList r0 = r13.build()     // Catch:{ IOException -> 0x06fd }
            r2.A17 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0364:
            java.lang.String r12 = "first_name"
            boolean r0 = r3.has(r12)     // Catch:{ IOException -> 0x06fd }
            r14 = 0
            if (r0 == 0) goto L_0x040a
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r13 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x0375:
            java.lang.String r12 = "last_name"
            boolean r0 = r3.has(r12)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0407
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r12 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x0385:
            java.lang.String r15 = "name"
            boolean r0 = r3.has(r15)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0395
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r15)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r14 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x0395:
            com.facebook.user.model.Name r0 = new com.facebook.user.model.Name     // Catch:{ IOException -> 0x06fd }
            r0.<init>(r13, r12, r14)     // Catch:{ IOException -> 0x06fd }
            r2.A0J = r0     // Catch:{ IOException -> 0x06fd }
            java.lang.String r12 = "birth_date_year"
            boolean r0 = r3.has(r12)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0405
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            int r14 = com.facebook.common.util.JSONUtil.A04(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x03ac:
            java.lang.String r12 = "birth_date_month"
            boolean r0 = r3.has(r12)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0403
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            int r13 = com.facebook.common.util.JSONUtil.A04(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x03bc:
            java.lang.String r12 = "birth_date_day"
            boolean r0 = r3.has(r12)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x03cc
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r12)     // Catch:{ IOException -> 0x06fd }
            int r1 = com.facebook.common.util.JSONUtil.A04(r0)     // Catch:{ IOException -> 0x06fd }
        L_0x03cc:
            r2.A06 = r14     // Catch:{ IOException -> 0x06fd }
            r2.A05 = r13     // Catch:{ IOException -> 0x06fd }
            r2.A04 = r1     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "city"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0l = r0     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "postal_code"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0u = r0     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "region"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0w = r0     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "current_location_prediction"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0o = r0     // Catch:{ IOException -> 0x06fd }
            goto L_0x040d
        L_0x0403:
            r13 = 0
            goto L_0x03bc
        L_0x0405:
            r14 = 0
            goto L_0x03ac
        L_0x0407:
            r12 = r11
            goto L_0x0385
        L_0x040a:
            r13 = r11
            goto L_0x0375
        L_0x040d:
            java.lang.String r0 = "gender"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IllegalArgumentException -> 0x0424 }
            java.lang.String r1 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IllegalArgumentException -> 0x0424 }
            if (r1 == 0) goto L_0x0424
            boolean r0 = r1.isEmpty()     // Catch:{ IllegalArgumentException -> 0x0424 }
            if (r0 != 0) goto L_0x0424
            int r0 = X.C07230cz.A00(r1)     // Catch:{ IllegalArgumentException -> 0x0424 }
            goto L_0x0425
        L_0x0424:
            r0 = 0
        L_0x0425:
            r2.A02 = r0     // Catch:{ IOException -> 0x06fd }
            java.lang.String r1 = "profile_pic_square"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0439
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            com.facebook.user.profilepic.PicSquare r0 = X.C12630pi.A03(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0P = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0439:
            java.lang.String r1 = "pic_square"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x044b
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A11 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x044b:
            java.lang.String r1 = "profile_pic_round"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x045d
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A10 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x045d:
            java.lang.String r1 = "pic_cover"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x046f
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0f = r0     // Catch:{ IOException -> 0x06fd }
        L_0x046f:
            java.lang.String r1 = "rank"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0482
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            double r0 = com.facebook.common.util.JSONUtil.A00(r0)     // Catch:{ IOException -> 0x06fd }
            float r12 = (float) r0     // Catch:{ IOException -> 0x06fd }
            r2.A01 = r12     // Catch:{ IOException -> 0x06fd }
        L_0x0482:
            java.lang.String r1 = "is_pushable"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x06f7
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x06f3
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ IOException -> 0x06fd }
        L_0x0496:
            r2.A0E = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0498:
            java.lang.String r1 = "is_employee"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x04aa
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1A = r0     // Catch:{ IOException -> 0x06fd }
        L_0x04aa:
            java.lang.String r1 = "is_work_user"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x04bc
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1C = r0     // Catch:{ IOException -> 0x06fd }
        L_0x04bc:
            java.lang.String r1 = "type"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x04ce
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A13 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x04ce:
            java.lang.String r1 = "is_messenger_user"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x04e0
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1B = r0     // Catch:{ IOException -> 0x06fd }
        L_0x04e0:
            java.lang.String r1 = "is_commerce"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x04f2
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A19 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x04f2:
            java.lang.String r1 = "messenger_install_time"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0504
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            long r0 = com.facebook.common.util.JSONUtil.A06(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0B = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0504:
            java.lang.String r1 = "added_time"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0516
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            long r0 = com.facebook.common.util.JSONUtil.A06(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A09 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0516:
            java.lang.String r1 = "is_partial"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0528
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1i = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0528:
            java.lang.String r1 = "is_minor"
            boolean r0 = r3.has(r1)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x053a
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r1)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1h = r0     // Catch:{ IOException -> 0x06fd }
        L_0x053a:
            java.lang.String r0 = "can_viewer_message"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x054e
            java.lang.String r0 = "can_viewer_message"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1F = r0     // Catch:{ IOException -> 0x06fd }
        L_0x054e:
            java.lang.String r0 = "profile_picture_is_silhouette"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0566
            java.lang.String r0 = "profile_picture_is_silhouette"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.valueOf(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0F = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0566:
            java.lang.String r0 = "has_profile_video"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x057a
            java.lang.String r0 = "has_profile_video"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1L = r0     // Catch:{ IOException -> 0x06fd }
        L_0x057a:
            X.06B r0 = r7.A00     // Catch:{ IOException -> 0x06fd }
            long r0 = r0.now()     // Catch:{ IOException -> 0x06fd }
            r2.A0A = r0     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = "montage_thread_fbid"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0596
            java.lang.String r0 = "montage_thread_fbid"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            long r0 = com.facebook.common.util.JSONUtil.A06(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0C = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0596:
            java.lang.String r0 = "can_see_viewer_montage_thread"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x05aa
            java.lang.String r0 = "can_see_viewer_montage_thread"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1E = r0     // Catch:{ IOException -> 0x06fd }
        L_0x05aa:
            java.lang.String r0 = "messenger_broadcast_flow_eligible"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x05c0
            java.lang.String r0 = "messenger_broadcast_flow_eligible"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r0 = r0 ^ 1
            r2.A1O = r0     // Catch:{ IOException -> 0x06fd }
        L_0x05c0:
            java.lang.String r0 = "is_deactivated_allowed_on_messenger"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x05d4
            java.lang.String r0 = "is_deactivated_allowed_on_messenger"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1T = r0     // Catch:{ IOException -> 0x06fd }
        L_0x05d4:
            java.lang.String r0 = "is_messenger_only_deactivated"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x05e8
            java.lang.String r0 = "is_messenger_only_deactivated"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1d = r0     // Catch:{ IOException -> 0x06fd }
        L_0x05e8:
            r0 = 135(0x87, float:1.89E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0604
            r0 = 135(0x87, float:1.89E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)     // Catch:{ IOException -> 0x06fd }
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1G = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0604:
            java.lang.String r0 = "viewer_connection_status"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x061f
            java.lang.String r0 = "viewer_connection_status"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 != 0) goto L_0x06e9
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ IOException -> 0x06fd }
        L_0x061a:
            com.google.common.base.Preconditions.checkNotNull(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0e = r0     // Catch:{ IOException -> 0x06fd }
        L_0x061f:
            java.lang.String r0 = "unified_stories_connection_type"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0637
            java.lang.String r0 = "unified_stories_connection_type"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 != 0) goto L_0x06df
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ IOException -> 0x06fd }
        L_0x0635:
            r2.A0d = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0637:
            java.lang.String r0 = "is_memorialized"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x064b
            java.lang.String r0 = "is_memorialized"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1Z = r0     // Catch:{ IOException -> 0x06fd }
        L_0x064b:
            java.lang.String r0 = "messenger_only_user_cloud_drive_backup_email"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x065f
            java.lang.String r0 = "messenger_only_user_cloud_drive_backup_email"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0s = r0     // Catch:{ IOException -> 0x06fd }
        L_0x065f:
            java.lang.String r0 = "messenger_instagram_contact_import_enabled"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0673
            java.lang.String r0 = "messenger_instagram_contact_import_enabled"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1X = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0673:
            java.lang.String r0 = "messenger_user_can_disconnect_from_instagram"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x0687
            java.lang.String r0 = "messenger_user_can_disconnect_from_instagram"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1D = r0     // Catch:{ IOException -> 0x06fd }
        L_0x0687:
            java.lang.String r0 = "facebook_friends_on_instagram_count"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x069b
            java.lang.String r0 = "facebook_friends_on_instagram_count"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            int r0 = r0.asInt()     // Catch:{ IOException -> 0x06fd }
            r2.A07 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x069b:
            java.lang.String r0 = "favorite_color"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x06af
            java.lang.String r0 = "favorite_color"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A0q = r0     // Catch:{ IOException -> 0x06fd }
        L_0x06af:
            java.lang.String r0 = "admined_pages_count"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x06c3
            java.lang.String r0 = "admined_pages_count"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            int r0 = com.facebook.common.util.JSONUtil.A04(r0)     // Catch:{ IOException -> 0x06fd }
            r2.A03 = r0     // Catch:{ IOException -> 0x06fd }
        L_0x06c3:
            java.lang.String r0 = "is_favorite_messenger_contact"
            boolean r0 = r3.has(r0)     // Catch:{ IOException -> 0x06fd }
            if (r0 == 0) goto L_0x06d7
            java.lang.String r0 = "is_favorites_messenger_contact"
            com.fasterxml.jackson.databind.JsonNode r0 = r3.get(r0)     // Catch:{ IOException -> 0x06fd }
            boolean r0 = r0.booleanValue()     // Catch:{ IOException -> 0x06fd }
            r2.A1U = r0     // Catch:{ IOException -> 0x06fd }
        L_0x06d7:
            com.facebook.user.model.User r0 = r2.A02()     // Catch:{ IOException -> 0x06fd }
            r2 = r0
            if (r0 == 0) goto L_0x0709
            goto L_0x0706
        L_0x06df:
            java.lang.Integer r0 = X.C40011zy.A00(r0)     // Catch:{ IllegalArgumentException -> 0x06e5 }
            goto L_0x0635
        L_0x06e5:
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ IOException -> 0x06fd }
            goto L_0x0635
        L_0x06e9:
            java.lang.Integer r0 = X.C29751gv.A00(r0)     // Catch:{ IllegalArgumentException -> 0x06ef }
            goto L_0x061a
        L_0x06ef:
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ IOException -> 0x06fd }
            goto L_0x061a
        L_0x06f3:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO     // Catch:{ IOException -> 0x06fd }
            goto L_0x0496
        L_0x06f7:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ IOException -> 0x06fd }
            r2.A0E = r0     // Catch:{ IOException -> 0x06fd }
            goto L_0x0498
        L_0x06fd:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "Unexpected serialization exception"
            r1.<init>(r0, r2)     // Catch:{ all -> 0x084b }
            throw r1     // Catch:{ all -> 0x084b }
        L_0x0706:
            A05(r0, r9)     // Catch:{ all -> 0x084b }
        L_0x0709:
            X.1hn r1 = r4.edit()     // Catch:{ all -> 0x084b }
            X.1Y7 r0 = X.C10730kl.A07     // Catch:{ all -> 0x084b }
            r1.C1B(r0)     // Catch:{ all -> 0x084b }
            r1.commit()     // Catch:{ all -> 0x084b }
        L_0x0715:
            r9.A0D(r10, r5)     // Catch:{ all -> 0x084b }
            r9.A06()     // Catch:{ all -> 0x084b }
            goto L_0x07c4
        L_0x071d:
            r2.A0c = r0     // Catch:{ all -> 0x084b }
            r1 = 0
            java.lang.String r0 = "messenger_should_show_unified_stories_nux"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1o = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "has_posted_to_messenger_stories"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1J = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "has_posted_to_facebook_stories"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1I = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "has_posted_to_unified_stories"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1K = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "messenger_only_user_cloud_drive_backup_email"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0s = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "messenger_connected_instagram_username"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0m = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "instagram_contact_import_enabled"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1X = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "can_disconnect_instagram_account"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1D = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_aloha_proxy_confirmed"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1M = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "fb_friends_on_ig_count"
            int r0 = r3.A04(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A07 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "is_verified"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1k = r0     // Catch:{ all -> 0x084b }
            r1 = 1
            java.lang.String r0 = "is_coworker"
            boolean r0 = r3.A0B(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A1R = r0     // Catch:{ all -> 0x084b }
            r0 = 0
            java.lang.String r5 = "registration_time"
            long r0 = r3.A05(r5, r0)     // Catch:{ all -> 0x084b }
            r2.A0D = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "username"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0x = r0     // Catch:{ all -> 0x084b }
            r0 = 55
            java.lang.String r0 = X.C99084oO.$const$string(r0)     // Catch:{ all -> 0x084b }
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0p = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "account_status"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0k = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "mme_referral_uri"
            java.lang.String r0 = r3.A07(r0, r11)     // Catch:{ all -> 0x084b }
            r2.A0t = r0     // Catch:{ all -> 0x084b }
            r1 = 0
            java.lang.String r0 = "key_is_in_story_holdout"
            int r0 = r3.A04(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A08 = r0     // Catch:{ all -> 0x084b }
            java.lang.String r0 = "key_admined_page_count"
            int r0 = r3.A04(r0, r1)     // Catch:{ all -> 0x084b }
            r2.A03 = r0     // Catch:{ all -> 0x084b }
            com.facebook.user.model.User r2 = r2.A02()     // Catch:{ all -> 0x084b }
        L_0x07c4:
            r6.A01 = r2     // Catch:{ all -> 0x084b }
            if (r2 == 0) goto L_0x0847
            X.00z r2 = r6.A00     // Catch:{ all -> 0x084b }
            X.00z r0 = X.C001500z.A03     // Catch:{ all -> 0x084b }
            if (r2 == r0) goto L_0x07d3
            X.00z r1 = X.C001500z.A07     // Catch:{ all -> 0x084b }
            r0 = 0
            if (r2 != r1) goto L_0x07d4
        L_0x07d3:
            r0 = 1
        L_0x07d4:
            if (r0 == 0) goto L_0x0809
            X.00z r0 = X.C001500z.A03     // Catch:{ all -> 0x084b }
            if (r2 == r0) goto L_0x07df
            X.00z r1 = X.C001500z.A07     // Catch:{ all -> 0x084b }
            r0 = 0
            if (r2 != r1) goto L_0x07e0
        L_0x07df:
            r0 = 1
        L_0x07e0:
            if (r0 == 0) goto L_0x0804
            X.1Yp r4 = r6.A0C     // Catch:{ all -> 0x084b }
            com.facebook.prefs.shared.FbSharedPreferences r1 = r4.A00     // Catch:{ all -> 0x084b }
            X.1Y7 r0 = X.C25171Yp.A02     // Catch:{ all -> 0x084b }
            r3 = 0
            int r0 = r1.AqN(r0, r3)     // Catch:{ all -> 0x084b }
            r2 = 1
            if (r0 == r2) goto L_0x07fe
            r2 = 2
            if (r0 != 0) goto L_0x07fe
            com.facebook.prefs.shared.FbSharedPreferences r1 = r4.A00     // Catch:{ all -> 0x084b }
            X.1Y7 r0 = X.C25171Yp.A01     // Catch:{ all -> 0x084b }
            boolean r0 = r1.Aep(r0, r3)     // Catch:{ all -> 0x084b }
            if (r0 != 0) goto L_0x07fe
            r2 = 0
        L_0x07fe:
            r0 = 1
            if (r2 == r0) goto L_0x0804
            r6.A03()     // Catch:{ all -> 0x084b }
        L_0x0804:
            X.0aM r0 = r6.A0B     // Catch:{ all -> 0x084b }
            r0.A02()     // Catch:{ all -> 0x084b }
        L_0x0809:
            java.lang.String r3 = r8.mUserId     // Catch:{ all -> 0x084b }
            if (r3 == 0) goto L_0x083f
            com.facebook.user.model.User r0 = r6.A01     // Catch:{ all -> 0x084b }
            java.lang.String r2 = r0.A0j     // Catch:{ all -> 0x084b }
            boolean r0 = r3.equals(r2)     // Catch:{ all -> 0x084b }
            if (r0 != 0) goto L_0x083f
            boolean r0 = r6.A04     // Catch:{ all -> 0x084b }
            if (r0 != 0) goto L_0x083f
            java.lang.String r1 = "User ID in credential does not match me user. current user ID "
            java.lang.String r0 = ", me user ID "
            java.lang.String r2 = X.AnonymousClass08S.A0S(r1, r3, r0, r2)     // Catch:{ all -> 0x084b }
            X.09P r1 = r6.A0J     // Catch:{ all -> 0x084b }
            java.lang.Class r0 = r6.getClass()     // Catch:{ all -> 0x084b }
            java.lang.String r0 = r0.getSimpleName()     // Catch:{ all -> 0x084b }
            r1.CGS(r0, r2)     // Catch:{ all -> 0x084b }
            r2 = r6
            monitor-enter(r2)     // Catch:{ all -> 0x084b }
            java.util.concurrent.atomic.AtomicBoolean r1 = r6.A0F     // Catch:{ all -> 0x083c }
            r0 = 1
            r1.set(r0)     // Catch:{ all -> 0x083c }
            monitor-exit(r2)     // Catch:{ all -> 0x084b }
            r6.A04 = r0     // Catch:{ all -> 0x084b }
            goto L_0x0847
        L_0x083c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x084b }
        L_0x083e:
            throw r0     // Catch:{ all -> 0x084b }
        L_0x083f:
            com.facebook.user.model.User r0 = r6.A01     // Catch:{ all -> 0x084b }
            r6.A04(r0)     // Catch:{ all -> 0x084b }
            com.facebook.user.model.User r0 = r6.A01     // Catch:{ all -> 0x084b }
            goto L_0x0849
        L_0x0847:
            monitor-exit(r6)
            return r11
        L_0x0849:
            monitor-exit(r6)
            return r0
        L_0x084b:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XN.A02():com.facebook.user.model.User");
    }

    public ViewerContext A07() {
        if (this.A0F.get()) {
            return null;
        }
        if (this.A0G.get() == null) {
            synchronized (this) {
                if (this.A0G.get() == null) {
                    AtomicReference atomicReference = this.A0G;
                    AnonymousClass0XO r9 = this.A09;
                    ViewerContext viewerContext = null;
                    String A072 = r9.A02.A07("page_admin_uid", null);
                    String A073 = r9.A02.A07("page_admin_access_token", null);
                    if (!(A072 == null || A073 == null)) {
                        String A074 = r9.A02.A07("session_cookies_string", null);
                        String A075 = r9.A02.A07("session_cookies_string", null);
                        String A076 = r9.A02.A07("session_cookies_string", null);
                        if (A074 == null || A075 == null || A076 == null) {
                            C010708t.A0K(AnonymousClass80H.$const$string(218), "Attempted to create page admin viewer context with invalid session data");
                        } else {
                            String A077 = r9.A02.A07("session_cookies_string", null);
                            C05580Zo r1 = new C05580Zo();
                            r1.A05 = A072;
                            r1.A01 = A073;
                            r1.A02 = A074;
                            r1.A04 = A075;
                            r1.A03 = A076;
                            r1.A00 = A077;
                            viewerContext = new ViewerContext(r1);
                        }
                    }
                    atomicReference.set(viewerContext);
                }
            }
        }
        return (ViewerContext) this.A0G.get();
    }

    public ViewerContext A08() {
        ViewerContext viewerContext;
        if (this.A0F.get()) {
            return null;
        }
        if (this.A0H.get() == null) {
            synchronized (this) {
                if (this.A0H.get() == null) {
                    AtomicReference atomicReference = this.A0H;
                    AnonymousClass0XO r13 = this.A09;
                    if (r13.A02.A0B("is_imported", false)) {
                        viewerContext = null;
                        String A072 = r13.A02.A07(ErrorReportingConstants.USER_ID_KEY, null);
                        String A073 = r13.A02.A07("access_token", null);
                        if (!TextUtils.isEmpty(A072) && !TextUtils.isEmpty(A073)) {
                            C05580Zo r2 = new C05580Zo();
                            r2.A05 = A072;
                            r2.A01 = A073;
                            r2.A02 = r13.A02.A07("session_cookies_string", null);
                            r2.A04 = r13.A02.A07("secret", null);
                            r2.A03 = r13.A02.A07("session_key", null);
                            r2.A06 = r13.A02.A07("username", null);
                            r2.A00 = r13.A02.A07("analytics_claim", null);
                            r2.A07 = r13.A02.A0A("page_admin_uid");
                            viewerContext = new ViewerContext(r2);
                        }
                    } else {
                        FbSharedPreferences fbSharedPreferences = (FbSharedPreferences) r13.A03.get();
                        viewerContext = null;
                        if (fbSharedPreferences.BFQ()) {
                            AnonymousClass16O A062 = r13.A02.A06();
                            if (fbSharedPreferences.BBh(C10730kl.A0C) || fbSharedPreferences.BBh(C10730kl.A0B)) {
                                String B4F = fbSharedPreferences.B4F(C10730kl.A0C, null);
                                String B4F2 = fbSharedPreferences.B4F(C10730kl.A0B, null);
                                if (!TextUtils.isEmpty(B4F) && !TextUtils.isEmpty(B4F2)) {
                                    C05580Zo r1 = new C05580Zo();
                                    r1.A05 = B4F;
                                    r1.A01 = B4F2;
                                    r1.A02 = fbSharedPreferences.B4F(C10730kl.A08, null);
                                    r1.A04 = fbSharedPreferences.B4F(C10730kl.A0A, null);
                                    r1.A03 = fbSharedPreferences.B4F(C10730kl.A09, null);
                                    r1.A06 = fbSharedPreferences.B4F(C10730kl.A0D, null);
                                    viewerContext = new ViewerContext(r1);
                                    String str = viewerContext.mUserId;
                                    String str2 = viewerContext.mAuthToken;
                                    String str3 = viewerContext.mSessionCookiesString;
                                    String str4 = viewerContext.mSessionSecret;
                                    String str5 = viewerContext.mSessionKey;
                                    String str6 = viewerContext.mUsername;
                                    Preconditions.checkArgument(!TextUtils.isEmpty(str), "No user ID in credentials");
                                    Preconditions.checkArgument(!TextUtils.isEmpty(str2), "No token in credentials");
                                    AnonymousClass16O.A04(A062);
                                    A062.A01 = true;
                                    A062.A0B(ErrorReportingConstants.USER_ID_KEY, str);
                                    A062.A0B("access_token", str2);
                                    A062.A0B("page_admin_uid", null);
                                    A062.A0B("page_admin_access_token", null);
                                    A062.A0B("session_cookies_string", str3);
                                    A062.A0B("secret", str4);
                                    A062.A0B("session_key", str5);
                                    A062.A0B("username", str6);
                                    A062.A0B("analytics_claim", null);
                                }
                                AnonymousClass0XO.A01(r13);
                            }
                            A062.A0D("is_imported", true);
                            A062.A06();
                        }
                    }
                    atomicReference.set(viewerContext);
                }
            }
        }
        return (ViewerContext) this.A0H.get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x004c, code lost:
        if (r12 == X.C001500z.A07) goto L_0x004e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private AnonymousClass0XN(X.AnonymousClass1XY r3, com.facebook.prefs.shared.FbSharedPreferences r4, X.AnonymousClass0XO r5, X.AnonymousClass09P r6, X.C05790aK r7, android.content.Context r8, X.C25171Yp r9, X.C05810aM r10, X.C05890aV r11, X.C001500z r12, java.util.concurrent.ExecutorService r13) {
        /*
            r2 = this;
            r2.<init>()
            java.util.concurrent.atomic.AtomicBoolean r1 = new java.util.concurrent.atomic.AtomicBoolean
            r0 = 0
            r1.<init>(r0)
            r2.A0F = r1
            java.util.concurrent.atomic.AtomicReference r0 = new java.util.concurrent.atomic.AtomicReference
            r0.<init>()
            r2.A0H = r0
            java.util.concurrent.atomic.AtomicReference r0 = new java.util.concurrent.atomic.AtomicReference
            r0.<init>()
            r2.A0G = r0
            r0 = 0
            r2.A02 = r0
            X.0UN r1 = new X.0UN
            r0 = 2
            r1.<init>(r0, r3)
            r2.A05 = r1
            java.util.concurrent.ExecutorService r0 = X.AnonymousClass0UX.A0X(r3)
            r2.A0E = r0
            r2.A0D = r4
            r2.A09 = r5
            r2.A0J = r6
            r2.A0A = r7
            r2.A08 = r8
            r2.A0C = r9
            r2.A0B = r10
            r2.A0I = r11
            r2.A00 = r12
            r2.A07 = r13
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r2.A06 = r0
            X.00z r0 = X.C001500z.A03
            if (r12 == r0) goto L_0x004e
            X.00z r1 = X.C001500z.A07
            r0 = 0
            if (r12 != r1) goto L_0x004f
        L_0x004e:
            r0 = 1
        L_0x004f:
            if (r0 == 0) goto L_0x0053
            monitor-enter(r9)
            monitor-exit(r9)
        L_0x0053:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XN.<init>(X.1XY, com.facebook.prefs.shared.FbSharedPreferences, X.0XO, X.09P, X.0aK, android.content.Context, X.1Yp, X.0aM, X.0aV, X.00z, java.util.concurrent.ExecutorService):void");
    }

    public static final AnonymousClass0XN A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    private void A03() {
        String $const$string;
        if (A0I()) {
            ViewerContext A082 = A08();
            User A092 = A09();
            if (A082 == null || A092 == null) {
                C010708t.A0I("LoggedInUserSessionManager", "Could not set SSO data, due to no user object or viewer context");
                return;
            }
            Context context = this.A08;
            C001500z r1 = this.A00;
            if (r1 == C001500z.A03) {
                $const$string = TurboLoader.Locator.$const$string(16);
            } else if (r1 == C001500z.A07) {
                $const$string = TurboLoader.Locator.$const$string(AnonymousClass1Y3.A19);
            } else {
                throw new RuntimeException("Unsupported product");
            }
            Account A002 = C17430yt.A00(context, $const$string);
            if (A002 != null) {
                C30291ho r3 = new C30291ho();
                r3.A00 = true;
                C30291ho.A00(r3, "userId", A082.mUserId);
                C30291ho.A00(r3, "accessToken", A082.mAuthToken);
                C30291ho.A00(r3, "name", A092.A08());
                C30291ho.A00(r3, "userName", A082.mUsername);
                r3.A02("experiment_metadata", null);
                if (this.A00 == C001500z.A07) {
                    if (!((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B50, this.A0I.A00)).AbO(AnonymousClass1Y3.A0m, true)) {
                        String A0D2 = A092.A0D();
                        if (A0D2 != null) {
                            C30291ho.A00(r3, "profilePicUrl", A0D2);
                        }
                        r3.A02("is_partial_account", Boolean.toString(A092.A1c));
                    }
                }
                r3.A01(this.A08, A002);
                C30281hn edit = this.A0C.A00.edit();
                edit.Bz6(C25171Yp.A02, 1);
                edit.commit();
                this.A0B.A03(false);
            }
        }
    }

    public static void A06(String str, String str2, AnonymousClass16O r3) {
        if (!TextUtils.isEmpty(str2)) {
            r3.A0B(str, str2);
        }
    }
}
