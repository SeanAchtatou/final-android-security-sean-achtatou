package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Us  reason: invalid class name */
public final class AnonymousClass1Us extends AnonymousClass1Ur {
    private static volatile AnonymousClass1Us A00;

    public static final AnonymousClass1Us A00(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (AnonymousClass1Us.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new AnonymousClass1Us(AnonymousClass1YA.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private AnonymousClass1Us(Context context) {
        super(context);
    }
}
