package X;

/* renamed from: X.0zx  reason: invalid class name and case insensitive filesystem */
public final class C18030zx {
    public static final String A00 = "com.facebook.orca.chatheads.ACTION_CLEAR_ALL_UNSEEN_THREADS";
    public static final String A01 = "com.facebook.orca.chatheads.ACTION_CLEAR_SMS_CHAT_HEADS";
    public static final String A02 = "com.facebook.orca.chatheads.ACTION_CLEAR_UNREAD_THREAD";
    public static final String A03 = "com.facebook.orca.chatheads.ACTION_CLOSE_CHAT_HEADS_FOR_BLOCKING";
    public static final String A04 = "com.facebook.orca.chatheads.ACTION_COLLAPSE_CHATHEADS";
    public static final String A05 = "com.facebook.orca.chatheads.ACTION_HIDE_CHATHEADS";
    public static final String A06 = "com.facebook.orca.chatheads.ACTION_NEW_MESSAGE_NOTIFICATION";
    public static final String A07 = "com.facebook.orca.chatheads.ACTION_OPEN_CHAT_HEAD";
    public static final String A08 = "com.facebook.orca.chatheads.ACTION_OPEN_CHAT_HEAD_FOR_REPLY";
    public static final String A09 = "com.facebook.orca.chatheads.ACTION_OPEN_CHAT_HEAD_FOR_THREAD_SETTINGS";
    public static final String A0A = "com.facebook.orca.chatheads.ACTION_OPEN_DIVE_HEAD";
    public static final String A0B = "com.facebook.orca.chatheads.ACTION_OPEN_MONTAGE_CHATHEAD";
    public static final String A0C = "com.facebook.orca.chatheads.ACTION_OPEN_TOP_CHAT_HEAD";
    public static final String A0D = "com.facebook.orca.chatheads.ACTION_REMOVE_CHAT_HEAD";
    public static final String A0E = "com.facebook.orca.chatheads.ACTION_SHOW_CHATHEADS";
    public static final String A0F = "com.facebook.orca.chatheads.ACTION_VIEW_CONTACTS";
    public static final String A0G = "com.facebook.orca.chatheads.ACTION_VIEW_GROUPS";
    public static final String A0H = "com.facebook.orca.chatheads.ACTION_VIEW_RECENT_THREADS";
    public static final String A0I = "com.facebook.orca.chatheads.EXTRA_BLOCKING_IN_FOREGROUND";
    public static final String A0J = "com.facebook.orca.chatheads.EXTRA_BLOCKING_URL";
    public static final String A0K = "com.facebook.orca.chatheads.EXTRA_DISPLAY_NAME";
    public static final String A0L = "com.facebook.orca.chatheads.EXTRA_FBID";
    public static final String A0M = "com.facebook.orca.chatheads.EXTRA_FOR_AFTER_UNLOCK_KEYGUARD";
    public static final String A0N = "com.facebook.orca.chatheads.EXTRA_GROUP_THREAD_FBID";
    public static final String A0O = "com.facebook.orca.chatheads.EXTRA_KEEP_CHAT_HEADS_OPEN_FOR_ACTIVITY_START";
    public static final String A0P = "com.facebook.orca.chatheads.EXTRA_LOGGED_IN_USER_ID";
    public static final String A0Q = "com.facebook.orca.chatheads.EXTRA_MESSAGE_NOTIFICATION";
    public static final String A0R = "com.facebook.orca.chatheads.EXTRA_OPTIMISTIC_GROUP_THREAD_ID";
    public static final String A0S = "com.facebook.orca.chatheads.EXTRA_REASON";
    public static final String A0T = "com.facebook.orca.chatheads.EXTRA_REPLY_INTENT";
    public static final String A0U = "com.facebook.orca.chatheads.EXTRA_SMS_THREAD_ID";
    public static final String A0V = "com.facebook.orca.chatheads.EXTRA_THREAD_KEY_STRING";
}
