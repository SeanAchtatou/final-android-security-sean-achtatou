package X;

import com.facebook.inject.InjectorModule;
import com.facebook.proxygen.ProxygenRadioMeter;

@InjectorModule
/* renamed from: X.1kJ  reason: invalid class name and case insensitive filesystem */
public final class C31741kJ extends AnonymousClass0UV {
    private static volatile ProxygenRadioMeter A00;

    public static final ProxygenRadioMeter A00(AnonymousClass1XY r6) {
        ProxygenRadioMeter proxygenRadioMeter;
        if (A00 == null) {
            synchronized (ProxygenRadioMeter.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        C25051Yd A003 = AnonymousClass0WT.A00(applicationInjector);
                        C31531jq r2 = new C31531jq(applicationInjector);
                        if (A003.Aem(282909496838148L)) {
                            r2.A01();
                            proxygenRadioMeter = new ProxygenRadioMeter(A003.Aem(282909496576002L));
                        } else {
                            proxygenRadioMeter = null;
                        }
                        A00 = proxygenRadioMeter;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
