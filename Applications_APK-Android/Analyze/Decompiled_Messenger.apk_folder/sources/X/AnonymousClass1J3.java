package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.messages.Message;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.1J3  reason: invalid class name */
public final class AnonymousClass1J3 extends C17770zR {
    public static final C22111Jy A09 = C22111Jy.A04;
    public static final AnonymousClass1J0 A0A = AnonymousClass1J0.A09;
    public static final MigColorScheme A0B = C17190yT.A00();
    public static final ImmutableList A0C = RegularImmutableList.A02;
    @Comparable(type = 3)
    public int A00 = 0;
    public AnonymousClass0UN A01;
    @Comparable(type = 13)
    public Message A02;
    @Comparable(type = 13)
    public C22111Jy A03 = A09;
    @Comparable(type = 13)
    public AnonymousClass1J0 A04 = A0A;
    @Comparable(type = 13)
    public MigColorScheme A05 = A0B;
    @Comparable(type = 5)
    public ImmutableList A06 = A0C;
    @Comparable(type = 5)
    public ImmutableList A07;
    @Comparable(type = 3)
    public boolean A08;

    public AnonymousClass1J3(Context context) {
        super("M4MigProfileImage");
        this.A01 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }

    public static int A00(AnonymousClass1J0 r2, MigColorScheme migColorScheme) {
        boolean A022 = C012609n.A02(migColorScheme.B9m());
        if (AnonymousClass1J0.A0B.equals(r2)) {
            if (A022) {
                return 2132346500;
            }
            return 2132346499;
        } else if (A022) {
            return 2132346504;
        } else {
            return 2132346503;
        }
    }
}
