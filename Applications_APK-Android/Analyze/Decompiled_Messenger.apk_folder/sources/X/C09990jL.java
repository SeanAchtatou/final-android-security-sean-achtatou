package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

@InjectorModule
/* renamed from: X.0jL  reason: invalid class name and case insensitive filesystem */
public final class C09990jL extends AnonymousClass0UV {
    public static final C14020sT A00() {
        return new C14020sT(Runtime.getRuntime());
    }

    public static final C14020sT A01() {
        return new C14020sT(Runtime.getRuntime());
    }

    public static final Boolean A02(AnonymousClass1XY r4) {
        FbSharedPreferences A00 = FbSharedPreferencesModule.A00(r4);
        AnonymousClass0VG A002 = AnonymousClass0VG.A00(AnonymousClass1Y3.AcD, r4);
        boolean z = false;
        if (!A00.Aep(AnonymousClass0jG.A05, false)) {
            z = ((AnonymousClass1YI) A002.get()).AbO(479, false);
        }
        return Boolean.valueOf(A00.Aep(AnonymousClass0jG.A00, z));
    }
}
