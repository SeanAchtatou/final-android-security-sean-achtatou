package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0hP  reason: invalid class name and case insensitive filesystem */
public final class C09460hP implements C09470hQ {
    private static volatile C09460hP A00;

    public long AtF() {
        return 21600000;
    }

    public long AtG() {
        return 3600000;
    }

    public long AtJ() {
        return 259200000;
    }

    public long AtO() {
        return 1200000;
    }

    public static final C09460hP A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C09460hP.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C09460hP();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
