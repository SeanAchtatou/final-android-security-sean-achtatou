package X;

import android.content.Context;
import io.card.payment.BuildConfig;
import java.io.DataInputStream;
import java.io.File;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Properties;
import java.util.Scanner;

/* renamed from: X.07M  reason: invalid class name */
public final class AnonymousClass07M {
    public final MessageDigest A00;
    public final byte[] A01 = new byte[32];
    public final byte[] A02;

    public AnonymousClass07M() {
        MessageDigest instance = MessageDigest.getInstance("MD5");
        this.A00 = instance;
        this.A02 = new byte[instance.getDigestLength()];
    }

    public static char A00(DataInputStream dataInputStream) {
        try {
            return (char) dataInputStream.readUnsignedByte();
        } catch (Exception e) {
            throw new C02110Cz("Error reading byte", e);
        }
    }

    public AnonymousClass07N A01(Context context, InputStream inputStream, String str, Integer num, long j, long j2, long j3, long j4, boolean z, boolean z2, Properties properties, Properties properties2, File file) {
        char A002;
        int i;
        String str2;
        int i2;
        char A003;
        InputStream inputStream2 = inputStream;
        DataInputStream dataInputStream = new DataInputStream(inputStream2);
        char A004 = A00(dataInputStream);
        char c = AnonymousClass01Y.BYTE_NOT_PRESENT.mLogSymbol;
        Integer num2 = num;
        if (num2 == AnonymousClass07B.A01 || num2 == AnonymousClass07B.A0C || num2 == AnonymousClass07B.A0N || num2 == AnonymousClass07B.A0Y) {
            c = A00(dataInputStream);
        }
        char c2 = ' ';
        if (num2 == AnonymousClass07B.A0N || num2 == AnonymousClass07B.A0Y) {
            A002 = A00(dataInputStream);
        } else {
            A002 = ' ';
        }
        int i3 = 0;
        if ((num2 != AnonymousClass07B.A0C && num2 != AnonymousClass07B.A0N && num2 != AnonymousClass07B.A0Y) || (A003 = A00(dataInputStream)) <= ' ') {
            i = 0;
        } else if (A003 < '@') {
            i = A003 - ' ';
            i3 = 1;
        } else if (A003 < '`') {
            i = A003 - '@';
            i3 = 3;
        } else {
            i3 = 9;
            i = A003 - '`';
        }
        if (num2 == AnonymousClass07B.A0Y) {
            c2 = A00(dataInputStream);
        }
        try {
            dataInputStream.readFully(this.A01);
            String str3 = new String(this.A01, "US-ASCII");
            try {
                this.A00.reset();
                Scanner useDelimiter = new Scanner(new DigestInputStream(new AnonymousClass0K8(inputStream2), this.A00), "US-ASCII").useDelimiter("\\A");
                if (useDelimiter.hasNext()) {
                    str2 = useDelimiter.next();
                } else {
                    str2 = BuildConfig.FLAVOR;
                }
                MessageDigest messageDigest = this.A00;
                byte[] bArr = this.A02;
                messageDigest.digest(bArr, 0, bArr.length);
                if (c2 == ' ') {
                    i2 = 0;
                } else if (c2 != '!') {
                    switch (c2) {
                        case 'a':
                            i2 = 100;
                            break;
                        case 'b':
                            i2 = AnonymousClass1Y3.A0w;
                            break;
                        case 'c':
                            i2 = AnonymousClass1Y3.A0y;
                            break;
                        case 'd':
                            i2 = AnonymousClass1Y3.A1B;
                            break;
                        case AnonymousClass1Y3.A0i /*101*/:
                            i2 = AnonymousClass1Y3.A1J;
                            break;
                        case AnonymousClass1Y3.A0j /*102*/:
                            i2 = AnonymousClass1Y3.A1c;
                            break;
                        case 'g':
                            i2 = 230;
                            break;
                        case AnonymousClass1Y3.A0k /*104*/:
                            i2 = 300;
                            break;
                        case AnonymousClass1Y3.A0l /*105*/:
                            i2 = 325;
                            break;
                        case AnonymousClass1Y3.A0m /*106*/:
                            i2 = AnonymousClass1Y3.A2l;
                            break;
                        case AnonymousClass1Y3.A0n /*107*/:
                            i2 = AnonymousClass1Y3.A3I;
                            break;
                        case 'l':
                            i2 = 500;
                            break;
                        case AnonymousClass1Y3.A0o /*109*/:
                            i2 = AnonymousClass1Y3.A87;
                            break;
                        default:
                            i2 = -2;
                            break;
                    }
                } else {
                    i2 = -1;
                }
                return new AnonymousClass07N(context, A004, c, A002, i3, i, i2, str3, str2, str, j, j2, j3, j4, z, this.A02, z2, properties, properties2, file);
            } catch (Exception e) {
                throw new C02110Cz("Error reading log contents", e);
            }
        } catch (Exception e2) {
            throw new C02110Cz("Error reading checksum", e2);
        }
    }
}
