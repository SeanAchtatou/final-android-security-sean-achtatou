package X;

import com.facebook.mobileconfig.MobileConfigCrashReportUtils;
import com.facebook.quicklog.PerformanceLoggingEvent;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fI  reason: invalid class name and case insensitive filesystem */
public final class C08400fI extends AnonymousClass0f8 {
    private static volatile C08400fI A00;

    public String Azg() {
        return "mcc_stats";
    }

    public static final C08400fI A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C08400fI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C08400fI();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public void AWj(PerformanceLoggingEvent performanceLoggingEvent, Object obj, Object obj2) {
        String str = (String) obj2;
        if (str != null) {
            performanceLoggingEvent.A0B("canary_ids", str);
        }
    }

    public long Azh() {
        return C08350fD.A09;
    }

    public Class B3O() {
        return String.class;
    }

    public Object CGO() {
        return MobileConfigCrashReportUtils.getInstance().getSerializedCanaryData();
    }

    public boolean BEZ(C08360fE r2) {
        return r2.A07;
    }
}
