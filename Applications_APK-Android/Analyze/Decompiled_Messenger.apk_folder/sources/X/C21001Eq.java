package X;

/* renamed from: X.1Eq  reason: invalid class name and case insensitive filesystem */
public final class C21001Eq {
    public static String A00 = "interstitial_views";
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06 = A0A.A09("query_uid");
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y8 A09 = C04350Ue.A0A.A09(AnonymousClass80H.$const$string(AnonymousClass1Y3.A1F));
    public static final AnonymousClass1Y8 A0A;

    static {
        AnonymousClass1Y8 A0D = C04350Ue.A0A.A09("interstitial/");
        A0A = A0D;
        A03 = A0D.A09("data/");
        AnonymousClass1Y8 r1 = A0A;
        A05 = r1.A09("data_type/");
        A07 = r1.A09("triToId/");
        AnonymousClass1Y8 r12 = A0A;
        A01 = r12.A09("AllTriggers");
        A04 = r12.A09("data_restored");
        AnonymousClass1Y7 r13 = C04350Ue.A05;
        A08 = (AnonymousClass1Y7) r13.A09("version/");
        A02 = (AnonymousClass1Y7) r13.A09("app_version/");
    }
}
