package X;

import com.facebook.analytics.AnalyticsClientModule;
import com.facebook.analytics.counterlogger.CommunicationScheduler;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0dK  reason: invalid class name and case insensitive filesystem */
public final class C07380dK {
    private static volatile C07380dK A02;
    public final CommunicationScheduler A00;
    public final Map A01 = new HashMap();

    public static final C07380dK A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C07380dK.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C07380dK(AnalyticsClientModule.A03(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public void A02(String str) {
        A04(str, 1, "core_counters");
    }

    public void A03(String str) {
        A04(str, 1, "counters");
    }

    public void A04(String str, long j, String str2) {
        boolean z;
        synchronized (this.A01) {
            try {
                if (!this.A01.containsKey(str2)) {
                    this.A01.put(str2, new HashMap());
                    z = false;
                } else {
                    z = true;
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        if (!z) {
            synchronized (this) {
                CommunicationScheduler communicationScheduler = this.A00;
                C192417j r3 = new C192417j(this, str2);
                synchronized (communicationScheduler.A05) {
                    Map map = communicationScheduler.A07;
                    Map map2 = (Map) map.get(str2);
                    if (map2 == null) {
                        map2 = new HashMap();
                        map.put(str2, map2);
                    }
                    C192417j r0 = (C192417j) map2.get("data");
                    if (r0 == null || r0 == r3) {
                        map2.put("data", r3);
                    } else {
                        C010708t.A0K("com.facebook.analytics.counterlogger.CommunicationScheduler", "Duplicate Logger Registration");
                    }
                }
            }
        }
        synchronized (this.A01) {
            try {
                Map map3 = (Map) this.A01.get(str2);
                if (!map3.containsKey(str)) {
                    map3.put(str, new C193417t());
                }
                C193417t r2 = (C193417t) map3.get(str);
                r2.A00++;
                r2.A02 += j;
                r2.A01 += j * j;
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    private C07380dK(CommunicationScheduler communicationScheduler) {
        this.A00 = communicationScheduler;
    }

    public static final C07380dK A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
