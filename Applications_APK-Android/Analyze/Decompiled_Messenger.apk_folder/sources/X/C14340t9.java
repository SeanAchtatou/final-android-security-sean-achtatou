package X;

import android.net.wifi.WifiManager;
import android.os.PowerManager;
import com.facebook.common.stringformat.StringFormatUtil;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0t9  reason: invalid class name and case insensitive filesystem */
public final class C14340t9 {
    private static volatile C14340t9 A09;
    public C14390tE A00;
    public boolean A01 = false;
    private WifiManager.WifiLock A02;
    private C14390tE A03;
    private boolean A04;
    public final PowerManager A05;
    public final C14350tA A06;
    private final WifiManager A07;
    private final C16330ws A08;

    public synchronized void A01() {
        A02();
        A06(true);
        synchronized (this) {
            C14390tE r0 = this.A00;
            if (r0 != null && r0.A04()) {
                this.A00.A01();
            }
            C14390tE r1 = this.A00;
            if (r1 != null) {
                this.A06.A02(r1);
                this.A00 = null;
            }
        }
    }

    public synchronized void A02() {
        WifiManager.WifiLock wifiLock = this.A02;
        if (wifiLock != null && wifiLock.isHeld()) {
            this.A02.release();
        }
        this.A02 = null;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0034 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A03(java.lang.String r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.A04     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0039
            X.0tE r0 = r7.A03     // Catch:{ all -> 0x003b }
            r6 = 1
            r5 = 0
            if (r0 != 0) goto L_0x0026
            X.0tA r4 = r7.A06     // Catch:{ all -> 0x003b }
            r3 = 32
            java.lang.String r2 = "%s:%s:%s"
            java.lang.String r1 = "IncallWakeLocks"
            java.lang.String r0 = "ProximitySensor"
            java.lang.Object[] r0 = new java.lang.Object[]{r1, r0, r8}     // Catch:{ all -> 0x003b }
            java.lang.String r0 = com.facebook.common.stringformat.StringFormatUtil.formatStrLocaleSafe(r2, r0)     // Catch:{ all -> 0x003b }
            X.0tE r0 = r4.A01(r3, r0)     // Catch:{ all -> 0x003b }
            r7.A03 = r0     // Catch:{ all -> 0x003b }
            r0.A03(r5)     // Catch:{ all -> 0x003b }
        L_0x0026:
            X.0tE r0 = r7.A03     // Catch:{ all -> 0x003b }
            boolean r0 = r0.A04()     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x0039
            X.0tE r0 = r7.A03     // Catch:{ IllegalArgumentException -> 0x0034 }
            r0.A00()     // Catch:{ IllegalArgumentException -> 0x0034 }
            goto L_0x0039
        L_0x0034:
            r7.A04 = r5     // Catch:{ all -> 0x003b }
            r7.A06(r6)     // Catch:{ all -> 0x003b }
        L_0x0039:
            monitor-exit(r7)
            return
        L_0x003b:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14340t9.A03(java.lang.String):void");
    }

    public synchronized void A04(String str) {
        A05(str);
        synchronized (this) {
            if (this.A00 == null) {
                C14390tE A012 = this.A06.A01(1, StringFormatUtil.formatStrLocaleSafe("%s:%s:%s", "IncallWakeLocks", "CPU", str));
                this.A00 = A012;
                A012.A03(false);
            }
            if (!this.A00.A04()) {
                this.A00.A00();
            }
        }
    }

    public synchronized void A05(String str) {
        if (this.A08.A03(true)) {
            if (this.A02 == null) {
                WifiManager.WifiLock createWifiLock = this.A07.createWifiLock(3, StringFormatUtil.formatStrLocaleSafe("%s:%s:%s", "IncallWakeLocks", "WIFI", str));
                this.A02 = createWifiLock;
                createWifiLock.setReferenceCounted(false);
            }
            if (!this.A02.isHeld()) {
                this.A02.acquire();
            }
        }
    }

    public synchronized void A06(boolean z) {
        C14390tE r0 = this.A03;
        if (r0 != null && r0.A04()) {
            if (z) {
                this.A03.A01();
            } else {
                this.A03.A02(1);
            }
        }
        C14390tE r1 = this.A03;
        if (r1 != null) {
            this.A06.A02(r1);
            this.A03 = null;
        }
    }

    public static final C14340t9 A00(AnonymousClass1XY r4) {
        if (A09 == null) {
            synchronized (C14340t9.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A09, r4);
                if (A002 != null) {
                    try {
                        A09 = new C14340t9(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A09;
    }

    private C14340t9(AnonymousClass1XY r6) {
        boolean z;
        this.A05 = C04490Ux.A0Y(r6);
        this.A06 = C14350tA.A00(r6);
        this.A07 = C04490Ux.A0V(r6);
        this.A08 = C16330ws.A00(r6);
        try {
            C14390tE A012 = this.A06.A01(32, StringFormatUtil.formatStrLocaleSafe("%s:%s", "IncallWakeLocks", "DetectProximitySensor"));
            if (A012 != null) {
                this.A06.A02(A012);
                z = true;
                this.A04 = z;
            }
        } catch (Exception unused) {
        }
        z = false;
        this.A04 = z;
    }
}
