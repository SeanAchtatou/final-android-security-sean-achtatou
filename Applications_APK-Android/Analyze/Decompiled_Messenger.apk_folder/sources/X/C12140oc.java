package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0oc  reason: invalid class name and case insensitive filesystem */
public final class C12140oc {
    private static volatile C12140oc A01;
    public final FbSharedPreferences A00;

    public static final C12140oc A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C12140oc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C12140oc(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void A01() {
        FbSharedPreferences fbSharedPreferences = this.A00;
        C30281hn edit = fbSharedPreferences.edit();
        AnonymousClass1Y8 r1 = AnonymousClass129.A0X;
        edit.Bz6(r1, fbSharedPreferences.AqN(r1, 0) + 1);
        edit.commit();
    }

    private C12140oc(AnonymousClass1XY r2) {
        AnonymousClass067.A03(r2);
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }
}
