package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.common.util.Triplet;

/* renamed from: X.0Df  reason: invalid class name and case insensitive filesystem */
public final class C02170Df implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new Triplet(parcel);
    }

    public Object[] newArray(int i) {
        return new Triplet[i];
    }
}
