package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import java.util.Arrays;

/* renamed from: X.1F5  reason: invalid class name */
public final class AnonymousClass1F5 {
    public boolean A00 = false;
    public final AnonymousClass1FP A01;
    public final SubscribeTopic A02;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass1F5)) {
            return false;
        }
        AnonymousClass1F5 r4 = (AnonymousClass1F5) obj;
        return this.A02.equals(r4.A02) && this.A01 == r4.A01 && this.A00 == r4.A00;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A02, this.A01, Boolean.valueOf(this.A00)});
    }

    public AnonymousClass1F5(String str, int i, AnonymousClass1FP r4) {
        this.A02 = new SubscribeTopic(str, i);
        this.A01 = r4;
    }
}
