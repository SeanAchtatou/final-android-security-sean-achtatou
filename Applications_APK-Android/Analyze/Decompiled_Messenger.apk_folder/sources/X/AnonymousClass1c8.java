package X;

import java.io.Serializable;

/* renamed from: X.1c8  reason: invalid class name */
public final class AnonymousClass1c8 implements Serializable {
    public static final C26821cC[] DEFAULT_KEY_DESERIALIZERS = {new C26831cD()};
    public static final C26801cA[] NO_ABSTRACT_TYPE_RESOLVERS = new C26801cA[0];
    public static final AnonymousClass1c7[] NO_DESERIALIZERS = new AnonymousClass1c7[0];
    public static final AnonymousClass1c9[] NO_MODIFIERS = new AnonymousClass1c9[0];
    public static final C26811cB[] NO_VALUE_INSTANTIATORS = new C26811cB[0];
    private static final long serialVersionUID = 3683541151102256824L;
    public final C26801cA[] _abstractTypeResolvers;
    public final AnonymousClass1c7[] _additionalDeserializers;
    public final C26821cC[] _additionalKeyDeserializers;
    public final AnonymousClass1c9[] _modifiers;
    public final C26811cB[] _valueInstantiators;

    public boolean hasDeserializerModifiers() {
        if (this._modifiers.length > 0) {
            return true;
        }
        return false;
    }

    public AnonymousClass1c8() {
        this(null, null, null, null, null);
    }

    public AnonymousClass1c8(AnonymousClass1c7[] r1, C26821cC[] r2, AnonymousClass1c9[] r3, C26801cA[] r4, C26811cB[] r5) {
        this._additionalDeserializers = r1 == null ? NO_DESERIALIZERS : r1;
        this._additionalKeyDeserializers = r2 == null ? DEFAULT_KEY_DESERIALIZERS : r2;
        this._modifiers = r3 == null ? NO_MODIFIERS : r3;
        this._abstractTypeResolvers = r4 == null ? NO_ABSTRACT_TYPE_RESOLVERS : r4;
        this._valueInstantiators = r5 == null ? NO_VALUE_INSTANTIATORS : r5;
    }
}
