package X;

import java.util.Map;

/* renamed from: X.04c  reason: invalid class name */
public final class AnonymousClass04c extends C04180St {
    public final /* synthetic */ AnonymousClass04a A00;

    public AnonymousClass04c(AnonymousClass04a r1) {
        this.A00 = r1;
    }

    public int A02() {
        return this.A00.A00;
    }

    public int A03(Object obj) {
        return this.A00.A06(obj);
    }

    public int A04(Object obj) {
        return this.A00.A05(obj);
    }

    public Object A05(int i, int i2) {
        return this.A00.A02[(i << 1) + i2];
    }

    public Object A06(int i, Object obj) {
        int i2 = (i << 1) + 1;
        Object[] objArr = this.A00.A02;
        Object obj2 = objArr[i2];
        objArr[i2] = obj;
        return obj2;
    }

    public void A08() {
        this.A00.clear();
    }

    public void A09(int i) {
        this.A00.A08(i);
    }

    public void A0A(Object obj, Object obj2) {
        this.A00.put(obj, obj2);
    }

    public Map A07() {
        return this.A00;
    }
}
