package X;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1Qn  reason: invalid class name and case insensitive filesystem */
public final class C23701Qn extends C20831Dz {
    public final int A00;
    public final Handler A01 = new C28603Dxs(this, Looper.getMainLooper());
    public final C28593Dxi A02;
    public final C28594Dxj A03;
    public final List A04;

    public static void A00(C23701Qn r3, C28697DzP dzP, int i, List list) {
        C28642DyW dyW = (C28642DyW) r3.A04.get(i);
        dzP.A00 = dyW;
        r3.A0G((AnonymousClass2NH) dzP.A0H, dyW, list.isEmpty());
    }

    public void A0D(C33781o8 r3) {
        C28697DzP dzP = (C28697DzP) r3;
        super.A0D(dzP);
        A0F((AnonymousClass2NH) dzP.A0H, dzP.A00);
    }

    public /* bridge */ /* synthetic */ void A0E(C33781o8 r1, int i, List list) {
        A00(this, (C28697DzP) r1, i, list);
    }

    public void A0G(AnonymousClass2NH r7, C28642DyW dyW, boolean z) {
        if (!(!this.A03.A0G || dyW.B8y() == null || dyW.B8y().getParent() == null || dyW.B8y().getParent() == r7)) {
            AnonymousClass2NH r1 = (AnonymousClass2NH) dyW.B8y().getParent();
            r1.removeViewAt(0);
            r1.A00 = -1.0f;
            r1.A01 = -1.0f;
        }
        if (z) {
            View A002 = this.A02.A00(dyW);
            A002.setLayoutParams(new AnonymousClass2NP(-1, -1));
            r7.addView(A002);
            C28660Dyo dyo = ((C28626DyF) A002.getTag(2131296783)).A08;
            C28650Dye dye = null;
            if (dyo instanceof C28650Dye) {
                dye = (C28650Dye) dyo;
            }
            int i = this.A00;
            r7.A02 = dye;
            ViewGroup.LayoutParams layoutParams = r7.getLayoutParams();
            if (i != 1) {
                layoutParams.width = -2;
                layoutParams.height = -1;
                C28650Dye dye2 = r7.A02;
                if (!(dye2 == null || dye2.A01 == null)) {
                    C25814Cmf cmf = dye.A01;
                    switch (cmf.A01.intValue()) {
                        case 0:
                            layoutParams.width = (int) cmf.A00;
                            break;
                        case 1:
                            layoutParams.width = -2;
                            r7.A01 = cmf.A00;
                            break;
                        case 2:
                            layoutParams.width = -2;
                            break;
                    }
                }
            } else {
                layoutParams.width = -1;
                layoutParams.height = -2;
                C28650Dye dye3 = r7.A02;
                if (!(dye3 == null || dye3.A00 == null)) {
                    C25814Cmf cmf2 = dye.A00;
                    switch (cmf2.A01.intValue()) {
                        case 0:
                            layoutParams.height = (int) cmf2.A00;
                            break;
                        case 1:
                            layoutParams.height = -2;
                            r7.A00 = cmf2.A00;
                            break;
                        case 2:
                            layoutParams.height = -2;
                            break;
                    }
                }
            }
            r7.setLayoutParams(layoutParams);
        } else if (dyW.B8y() != null) {
            this.A02.A00(dyW);
        } else {
            throw new IllegalStateException("component doesn't have view attached!");
        }
    }

    public boolean A0H(int i) {
        C28660Dyo dyo = ((C28626DyF) ((C28642DyW) this.A04.get(i))).A08;
        if (dyo instanceof C28650Dye) {
            return ((C28650Dye) dyo).A04;
        }
        return false;
    }

    public int ArU() {
        return this.A04.size();
    }

    public C23701Qn(C28593Dxi dxi, C28594Dxj dxj, int i) {
        this.A02 = dxi;
        this.A03 = dxj;
        this.A00 = i;
        this.A04 = Collections.unmodifiableList(dxj.A01.A02);
    }

    public void A0F(AnonymousClass2NH r2, C28642DyW dyW) {
        if (r2.getChildCount() != 0) {
            this.A02.A01(dyW);
            r2.removeViewAt(0);
            r2.A00 = -1.0f;
            r2.A01 = -1.0f;
        }
    }
}
