package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.1He  reason: invalid class name and case insensitive filesystem */
public final class C21481He {
    private final C21491Hf A00;

    public static final C21481He A00(AnonymousClass1XY r1) {
        return new C21481He(r1);
    }

    public boolean A01() {
        C21491Hf r3 = this.A00;
        if (!((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r3.A00)).BBh(C21501Hg.A00.A09("read_only"))) {
            C21491Hf r32 = this.A00;
            if (((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, r32.A00)).BBh(C21501Hg.A00.A09("generic_read_only_block"))) {
                return true;
            }
            return false;
        }
        return true;
    }

    public C21481He(AnonymousClass1XY r2) {
        this.A00 = new C21491Hf(r2);
    }
}
