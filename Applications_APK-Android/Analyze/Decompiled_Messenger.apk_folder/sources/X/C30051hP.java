package X;

import com.google.common.base.Objects;

/* renamed from: X.1hP  reason: invalid class name and case insensitive filesystem */
public enum C30051hP {
    DEFAULT("DEFAULT"),
    PLATFORM("PLATFORM");
    
    public final String dbValue;

    private C30051hP(String str) {
        this.dbValue = str;
    }

    public static C30051hP A00(String str) {
        for (C30051hP r1 : values()) {
            if (Objects.equal(r1.dbValue, str)) {
                return r1;
            }
        }
        return DEFAULT;
    }
}
