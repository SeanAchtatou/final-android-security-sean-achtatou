package X;

import android.text.TextUtils;
import java.util.ArrayList;

/* renamed from: X.0os  reason: invalid class name and case insensitive filesystem */
public final class C12240os extends C12250ot {
    public int A00;
    public final ArrayList A01;

    public static void A00(C12240os r1, String str) {
        if (!r1.A05) {
            throw new IllegalStateException("Expected object to be mutable");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException(AnonymousClass08S.A0J("key=", str));
        }
    }

    public C16910xz A0E(String str) {
        C16910xz A012 = this.A01.A01();
        A0I(str, A012);
        return A012;
    }

    public C12240os A0F(String str) {
        C12240os A02 = this.A01.A02();
        A0I(str, A02);
        return A02;
    }

    public Object A0G(int i) {
        if (i >= 0 && i < this.A00) {
            return this.A01.get((i << 1) + 1);
        }
        throw new ArrayIndexOutOfBoundsException(i);
    }

    public String A0H(int i) {
        if (i >= 0 && i < this.A00) {
            return (String) this.A01.get(i << 1);
        }
        throw new ArrayIndexOutOfBoundsException(i);
    }

    public void A0I(String str, C12250ot r3) {
        C000300h.A02(r3, "subParams cannot be null!");
        A00(this, str);
        r3.A07();
        A01(this, str, r3);
        r3.A07();
        r3.A00 = this;
    }

    public C12240os(int i) {
        this.A01 = new ArrayList(i << 1);
    }

    public static void A01(C12240os r1, String str, Object obj) {
        A00(r1, str);
        r1.A01.add(str);
        r1.A01.add(obj);
        r1.A00++;
    }

    public void A0J(String str, Number number) {
        A01(this, str, number);
    }

    public void A0K(String str, String str2) {
        A01(this, str, str2);
    }
}
