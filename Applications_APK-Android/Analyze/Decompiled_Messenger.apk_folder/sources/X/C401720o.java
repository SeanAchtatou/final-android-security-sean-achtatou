package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.20o  reason: invalid class name and case insensitive filesystem */
public final class C401720o {
    private static C04470Uu A01;
    public final AnonymousClass1YI A00;

    public static final C401720o A00(AnonymousClass1XY r4) {
        C401720o r0;
        synchronized (C401720o.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C401720o((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C401720o) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    private C401720o(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WA.A00(r2);
    }
}
