package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.orca.threadlist.ThreadListFragment;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.1Aq  reason: invalid class name and case insensitive filesystem */
public final class C20031Aq implements AnonymousClass1BP {
    public final /* synthetic */ ThreadListFragment A00;

    public C20031Aq(ThreadListFragment threadListFragment) {
        this.A00 = threadListFragment;
    }

    public void BdY() {
        ThreadListFragment.A0F(this.A00);
    }

    public void BdZ() {
        ThreadListFragment threadListFragment = this.A00;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(threadListFragment.A0C.A01("android_messenger_inbox_load_more_wait"), 16);
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBd, threadListFragment.A0O)).markerStart(5505122);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A06();
        }
    }
}
