package X;

/* renamed from: X.1T7  reason: invalid class name */
public final class AnonymousClass1T7 {
    public static final C17180yS A00 = C17180yS.A00(2, 7, 4, 5);

    public static int A00(AnonymousClass1Q1 r6, AnonymousClass36w r7, AnonymousClass1NY r8, boolean z) {
        int i;
        int i2;
        int i3;
        float max;
        if (z && r7 != null) {
            int A02 = A02(r6, r8);
            C17180yS r1 = A00;
            AnonymousClass1NY.A05(r8);
            boolean z2 = false;
            if (r1.contains(Integer.valueOf(r8.A00))) {
                i = A01(r6, r8);
            } else {
                i = 0;
            }
            if (A02 == 90 || A02 == 270 || i == 5 || i == 7) {
                z2 = true;
            }
            if (z2) {
                AnonymousClass1NY.A05(r8);
                i2 = r8.A01;
            } else {
                AnonymousClass1NY.A05(r8);
                i2 = r8.A05;
            }
            if (z2) {
                AnonymousClass1NY.A05(r8);
                i3 = r8.A05;
            } else {
                AnonymousClass1NY.A05(r8);
                i3 = r8.A01;
            }
            if (r7 == null) {
                max = 1.0f;
            } else {
                float f = (float) i2;
                float f2 = (float) i3;
                max = Math.max(((float) r7.A03) / f, ((float) r7.A02) / f2);
                float f3 = r7.A00;
                if (f * max > f3) {
                    max = f3 / f;
                }
                if (f2 * max > f3) {
                    max = f3 / f2;
                }
            }
            int i4 = (int) (r7.A01 + (max * 8.0f));
            if (i4 <= 8) {
                if (i4 < 1) {
                    return 1;
                }
                return i4;
            }
        }
        return 8;
    }

    public static int A02(AnonymousClass1Q1 r4, AnonymousClass1NY r5) {
        int i;
        boolean z = false;
        if (r4.A00 != -2) {
            z = true;
        }
        if (!z) {
            return 0;
        }
        AnonymousClass1NY.A05(r5);
        int i2 = r5.A02;
        if (i2 == 90 || i2 == 180 || i2 == 270) {
            AnonymousClass1NY.A05(r5);
            i = r5.A02;
        } else {
            i = 0;
        }
        boolean z2 = false;
        if (r4.A00 == -1) {
            z2 = true;
        }
        if (z2) {
            return i;
        }
        return (i + r4.A00()) % AnonymousClass1Y3.A2r;
    }

    public static int A01(AnonymousClass1Q1 r5, AnonymousClass1NY r6) {
        AnonymousClass1NY.A05(r6);
        int indexOf = A00.indexOf(Integer.valueOf(r6.A00));
        if (indexOf >= 0) {
            int i = 0;
            boolean z = false;
            if (r5.A00 == -1) {
                z = true;
            }
            if (!z) {
                i = r5.A00();
            }
            C17180yS r1 = A00;
            return ((Integer) r1.get((indexOf + (i / 90)) % r1.size())).intValue();
        }
        throw new IllegalArgumentException("Only accepts inverted exif orientations");
    }
}
