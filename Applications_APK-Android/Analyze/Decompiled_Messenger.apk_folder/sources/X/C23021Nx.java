package X;

import android.graphics.Bitmap;
import android.os.Build;
import java.util.ArrayList;

/* renamed from: X.1Nx  reason: invalid class name and case insensitive filesystem */
public final class C23021Nx {
    public static C23041Nz A02;
    public static C23041Nz A03;
    public final C22801Mw A00;
    private final C22831Mz A01;

    private AnonymousClass1S9 A02(C23541Px r10, C23031Ny r11, Bitmap.Config config) {
        ArrayList arrayList;
        int i;
        AnonymousClass9K9 r5;
        AnonymousClass1PS r6 = null;
        if (0 != 0) {
            try {
                i = r11.getFrameCount() - 1;
            } catch (Throwable th) {
                th = th;
                arrayList = null;
                AnonymousClass1PS.A05(r6);
                AnonymousClass1PS.A06(arrayList);
                throw th;
            }
        } else {
            i = 0;
        }
        if (r10.A08) {
            AnonymousClass1S5 r3 = new AnonymousClass1S5(A01(r11, config, i), C33271nJ.A03, 0, 0);
            AnonymousClass1PS.A05(null);
            AnonymousClass1PS.A06(null);
            return r3;
        }
        if (r10.A06) {
            DJA AbA = this.A00.AbA(new C80973tY(r11), null);
            arrayList = new ArrayList(AbA.A04.getFrameCount());
            DJ7 dj7 = new DJ7(AbA, new BTn(arrayList));
            for (int i2 = 0; i2 < AbA.A04.getFrameCount(); i2++) {
                AnonymousClass1PS A002 = A00(AbA.A04.getWidth(), AbA.A04.getHeight(), config);
                dj7.A03(i2, (Bitmap) A002.A0A());
                arrayList.add(A002);
            }
            try {
                r6 = AnonymousClass1PS.A00((AnonymousClass1PS) arrayList.get(i));
            } catch (Throwable th2) {
                th = th2;
                AnonymousClass1PS.A05(r6);
                AnonymousClass1PS.A06(arrayList);
                throw th;
            }
        } else {
            arrayList = null;
        }
        if (r10.A07 && r6 == null) {
            r6 = A01(r11, config, i);
        }
        r5 = new AnonymousClass9K9(r11);
        r5.A01 = AnonymousClass1PS.A00(r6);
        r5.A00 = i;
        r5.A03 = AnonymousClass1PS.A04(arrayList);
        r5.A02 = r10.A04;
        C80973tY r2 = new C80973tY(r5);
        AnonymousClass1PS.A05(r5.A01);
        r5.A01 = null;
        AnonymousClass1PS.A06(r5.A03);
        C33291nL r1 = new C33291nL(r2, true);
        AnonymousClass1PS.A05(r6);
        AnonymousClass1PS.A06(arrayList);
        return r1;
    }

    static {
        C23041Nz r0;
        C23041Nz r02;
        try {
            r0 = (C23041Nz) Class.forName("com.facebook.animated.gif.GifImage").newInstance();
        } catch (Throwable unused) {
            r0 = null;
        }
        A02 = r0;
        try {
            r02 = (C23041Nz) Class.forName("com.facebook.animated.webp.WebPImage").newInstance();
        } catch (Throwable unused2) {
            r02 = null;
        }
        A03 = r02;
    }

    private AnonymousClass1PS A00(int i, int i2, Bitmap.Config config) {
        AnonymousClass1PS A06 = this.A01.A06(i, i2, config);
        ((Bitmap) A06.A0A()).eraseColor(0);
        if (Build.VERSION.SDK_INT >= 12) {
            ((Bitmap) A06.A0A()).setHasAlpha(true);
        }
        return A06;
    }

    public AnonymousClass1S9 A03(AnonymousClass1NY r7, C23541Px r8, Bitmap.Config config) {
        C23031Ny decode;
        if (A02 != null) {
            AnonymousClass1PS A002 = AnonymousClass1PS.A00(r7.A0A);
            C05520Zg.A02(A002);
            try {
                AnonymousClass1SS r4 = (AnonymousClass1SS) A002.A0A();
                if (r4.getByteBuffer() != null) {
                    decode = A02.decode(r4.getByteBuffer(), r8);
                } else {
                    decode = A02.decode(r4.getNativePtr(), r4.size(), r8);
                }
                return A02(r8, decode, config);
            } finally {
                AnonymousClass1PS.A05(A002);
            }
        } else {
            throw new UnsupportedOperationException("To encode animated gif please add the dependency to the animated-gif module");
        }
    }

    public AnonymousClass1S9 A04(AnonymousClass1NY r7, C23541Px r8, Bitmap.Config config) {
        C23031Ny decode;
        if (A03 != null) {
            AnonymousClass1PS A002 = AnonymousClass1PS.A00(r7.A0A);
            C05520Zg.A02(A002);
            try {
                AnonymousClass1SS r4 = (AnonymousClass1SS) A002.A0A();
                if (r4.getByteBuffer() != null) {
                    decode = A03.decode(r4.getByteBuffer(), r8);
                } else {
                    decode = A03.decode(r4.getNativePtr(), r4.size(), r8);
                }
                return A02(r8, decode, config);
            } finally {
                AnonymousClass1PS.A05(A002);
            }
        } else {
            throw new UnsupportedOperationException("To encode animated webp please add the dependency to the animated-webp module");
        }
    }

    public C23021Nx(C22801Mw r1, C22831Mz r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    private AnonymousClass1PS A01(C23031Ny r5, Bitmap.Config config, int i) {
        AnonymousClass1PS A002 = A00(r5.getWidth(), r5.getHeight(), config);
        new DJ7(this.A00.AbA(new C80973tY(r5), null), new C23064BTo()).A03(i, (Bitmap) A002.A0A());
        return A002;
    }
}
