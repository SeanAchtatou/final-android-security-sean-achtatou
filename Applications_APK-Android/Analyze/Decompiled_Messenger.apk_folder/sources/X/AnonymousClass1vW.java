package X;

import android.view.View;
import android.view.ViewOutlineProvider;

/* renamed from: X.1vW  reason: invalid class name */
public final class AnonymousClass1vW {
    public static void A01(E76 e76, int i) {
        e76.A1y((float) i);
        e76.A2J(ViewOutlineProvider.BOUNDS);
    }

    public static void A00(View view, int i) {
        view.setElevation((float) C007106r.A00(view.getContext(), (float) i));
        view.setOutlineProvider(ViewOutlineProvider.BOUNDS);
    }
}
