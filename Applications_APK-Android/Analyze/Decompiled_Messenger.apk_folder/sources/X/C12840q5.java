package X;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;

/* renamed from: X.0q5  reason: invalid class name and case insensitive filesystem */
public final class C12840q5 implements Closeable {
    private static final C12860q7 A03;
    private Throwable A00;
    public final C12860q7 A01;
    private final Deque A02 = new ArrayDeque(4);

    static {
        C12860q7 r0;
        boolean z = false;
        if (C12850q6.A01 != null) {
            z = true;
        }
        if (z) {
            r0 = C12850q6.A00;
        } else {
            r0 = AnonymousClass9HP.A00;
        }
        A03 = r0;
    }

    public static C12840q5 A00() {
        return new C12840q5(A03);
    }

    public void A02(Closeable closeable) {
        if (closeable != null) {
            this.A02.addFirst(closeable);
        }
    }

    public void close() {
        Throwable th = this.A00;
        while (!this.A02.isEmpty()) {
            Closeable closeable = (Closeable) this.A02.removeFirst();
            try {
                closeable.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                } else {
                    this.A01.CIf(closeable, th, th2);
                }
            }
        }
        if (this.A00 == null && th != null) {
            Throwables.propagateIfPossible(th, IOException.class);
            throw new AssertionError(th);
        }
    }

    private C12840q5(C12860q7 r3) {
        Preconditions.checkNotNull(r3);
        this.A01 = r3;
    }

    public RuntimeException A01(Throwable th) {
        Preconditions.checkNotNull(th);
        this.A00 = th;
        Throwables.propagateIfPossible(th, IOException.class);
        throw new RuntimeException(th);
    }
}
