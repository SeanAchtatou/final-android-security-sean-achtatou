package X;

import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.1tV  reason: invalid class name and case insensitive filesystem */
public final class C36651tV {
    public static String A04;
    private static final AnonymousClass1Y7 A05 = ((AnonymousClass1Y7) C05690aA.A0m.A09("send_failure_reliability_serialized"));
    public final C36671tX A00;
    public final AnonymousClass06B A01;
    public final AnonymousClass18P A02;
    public final C25051Yd A03;

    public static final C36651tV A00(AnonymousClass1XY r8) {
        return new C36651tV(AnonymousClass067.A02(), AnonymousClass18P.A00(r8), AnonymousClass0WT.A00(r8), new C36661tW(AnonymousClass067.A02(), FbSharedPreferencesModule.A00(r8), C06920cI.A00(r8)));
    }

    private C36651tV(AnonymousClass06B r11, AnonymousClass18P r12, C25051Yd r13, C36661tW r14) {
        this.A01 = r11;
        this.A02 = r12;
        this.A03 = r13;
        C36691tZ r4 = new C36691tZ(r13.Aem(286843685838088L), "reliabilities_serialization_failed", "message_send_failure", this.A03.AqL(568318662936852L, 500), this.A03.AqL(568318663067926L, 72));
        this.A00 = r14;
        AnonymousClass1Y7 r1 = A05;
        r14.A00 = r4;
        r14.A01 = r1;
        r14.A02 = C36701ta.class;
    }
}
