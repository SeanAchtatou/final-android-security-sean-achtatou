package X;

import android.os.MessageQueue;

/* renamed from: X.0kn  reason: invalid class name and case insensitive filesystem */
public final class C10750kn implements MessageQueue.IdleHandler {
    public final /* synthetic */ C08770fv A00;

    public C10750kn(C08770fv r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r1.A0F.BHL() != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean queueIdle() {
        /*
            r4 = this;
            X.0fv r1 = r4.A00
            X.0WP r0 = r1.A0F
            boolean r0 = r0.BFP()
            if (r0 == 0) goto L_0x001b
            X.0WP r0 = r1.A0F
            boolean r0 = r0.BDj()
            if (r0 == 0) goto L_0x001b
            X.0WP r0 = r1.A0F
            boolean r1 = r0.BHL()
            r0 = 1
            if (r1 == 0) goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            r3 = 1
            if (r0 == 0) goto L_0x0052
            X.0fv r0 = r4.A00
            com.facebook.quicklog.QuickPerformanceLogger r2 = r0.A05
            r1 = 84
            java.lang.String r0 = "caught_on_idle"
            r2.moduleTag(r1, r0)
            X.0fv r0 = r4.A00
            X.0Ud r0 = r0.A0G
            com.facebook.common.util.TriState r1 = r0.A0C()
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
            if (r1 == r0) goto L_0x0052
            X.0fv r0 = r4.A00
            X.2Bp r3 = new X.2Bp
            r3.<init>(r4)
            r0.A06 = r3
            X.1Y6 r2 = r0.A0I
            r0 = 5000(0x1388, double:2.4703E-320)
            r2.BxS(r3, r0)
            X.0fv r1 = r4.A00
            java.lang.String r0 = "Idle_detected"
            r1.A0F(r0)
            X.0fv r0 = r4.A00
            r3 = 0
            r0.A0A = r3
        L_0x0052:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10750kn.queueIdle():boolean");
    }
}
