package X;

import android.os.Looper;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.16O  reason: invalid class name */
public final class AnonymousClass16O {
    public final Map A00 = new HashMap(4);
    public volatile boolean A01 = false;
    private volatile boolean A02 = false;
    public final /* synthetic */ C05770aI A03;

    private synchronized Map A00() {
        if (!this.A02) {
            this.A02 = true;
        } else {
            throw new RuntimeException("Trying to freeze an editor that is already frozen!");
        }
        return this.A00;
    }

    private synchronized void A02() {
        this.A02 = false;
    }

    public void A07(int i) {
        if (i != 0 && Looper.myLooper() == Looper.getMainLooper()) {
            if (i == 1) {
                C010708t.A0K("LightSharedPreferencesImpl", "commit is called on the main thread.");
            } else {
                throw new IllegalStateException("commit is called on the main thread.");
            }
        }
        try {
            Set A012 = A01(A00());
            if (!A012.isEmpty()) {
                C05770aI.A02(this.A03, A012);
                A03(this);
            }
        } finally {
            A02();
        }
    }

    public AnonymousClass16O(C05770aI r3) {
        this.A03 = r3;
    }

    private Set A01(Map map) {
        C13770s2 r5 = new C13770s2();
        synchronized (this.A03.A02) {
            if (this.A01) {
                r5.addAll(this.A03.A04.keySet());
                this.A03.A04.clear();
            }
            for (Map.Entry entry : map.entrySet()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value == C05770aI.A0A) {
                    this.A03.A04.remove(str);
                } else {
                    C05770aI.A03(value);
                    if (!value.equals(this.A03.A04.get(str))) {
                        this.A03.A04.put(str, value);
                    }
                }
                r5.add(str);
            }
            AtomicBoolean atomicBoolean = this.A03.A07;
            boolean z = false;
            if (!r5.isEmpty()) {
                z = true;
            }
            atomicBoolean.compareAndSet(false, z);
        }
        this.A01 = false;
        map.clear();
        return r5;
    }

    public static void A03(AnonymousClass16O r4) {
        HashMap hashMap;
        if (r4.A03.A07.get()) {
            synchronized (r4.A03.A02) {
                r4.A03.A07.set(false);
                hashMap = new HashMap(r4.A03.A04);
            }
            try {
                AnonymousClass0XS r2 = r4.A03.A01;
                C05770aI.A03(hashMap);
                if (AnonymousClass0XS.A02 == 1) {
                    AnonymousClass0XS.A01(r2, hashMap);
                    return;
                }
                synchronized (AnonymousClass0XS.class) {
                    AnonymousClass0XS.A03.put(r2, hashMap);
                }
            } catch (IOException e) {
                C010708t.A0M("LightSharedPreferencesImpl", "Commit to disk failed.", e);
            }
        }
    }

    public static void A04(AnonymousClass16O r1) {
        if (r1.A02) {
            throw new ConcurrentModificationException("Editors shouldn't be modified during commit!");
        }
    }

    public void A06() {
        A07(this.A03.A00);
    }

    public void A05() {
        try {
            Set A012 = A01(A00());
            if (!A012.isEmpty()) {
                C05770aI.A02(this.A03, A012);
                AnonymousClass07A.A04(this.A03.A06, new C60182wj(this), 1446647426);
            }
        } finally {
            A02();
        }
    }

    public void A08(String str) {
        A04(this);
        Map map = this.A00;
        C05770aI.A03(str);
        map.put(str, C05770aI.A0A);
    }

    public void A09(String str, int i) {
        A04(this);
        Map map = this.A00;
        C05770aI.A03(str);
        map.put(str, Integer.valueOf(i));
    }

    public void A0A(String str, long j) {
        A04(this);
        Map map = this.A00;
        C05770aI.A03(str);
        map.put(str, Long.valueOf(j));
    }

    public void A0B(String str, String str2) {
        A04(this);
        if (str2 == null) {
            Map map = this.A00;
            C05770aI.A03(str);
            map.put(str, C05770aI.A0A);
            return;
        }
        Map map2 = this.A00;
        C05770aI.A03(str);
        map2.put(str, str2);
    }

    public void A0C(String str, Set set) {
        A04(this);
        if (set == null) {
            Map map = this.A00;
            C05770aI.A03(str);
            map.put(str, C05770aI.A0A);
            return;
        }
        Map map2 = this.A00;
        C05770aI.A03(str);
        map2.put(str, set);
    }

    public void A0D(String str, boolean z) {
        A04(this);
        Map map = this.A00;
        C05770aI.A03(str);
        map.put(str, Boolean.valueOf(z));
    }
}
