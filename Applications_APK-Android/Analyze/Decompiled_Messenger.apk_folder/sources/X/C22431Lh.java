package X;

import android.content.Context;
import com.facebook.auth.userscope.UserScoped;
import java.util.concurrent.atomic.AtomicInteger;

@UserScoped
/* renamed from: X.1Lh  reason: invalid class name and case insensitive filesystem */
public final class C22431Lh {
    private static C05540Zi A03;
    public final AnonymousClass04b A00 = new AnonymousClass04b(5);
    private final C07380dK A01;
    private final AtomicInteger A02 = new AtomicInteger(0);

    public synchronized void A01() {
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            ((C13860sC) this.A00.A09(i)).interrupt();
        }
        this.A00.clear();
    }

    public synchronized void A02(Context context, C10920l5 r9, int i) {
        Context context2 = context;
        C13860sC r1 = new C13860sC(this.A01, context2, r9, i, this.A02);
        this.A00.put(r9, r1);
        r1.start();
    }

    public static final C22431Lh A00(AnonymousClass1XY r4) {
        C22431Lh r0;
        synchronized (C22431Lh.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C22431Lh((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (C22431Lh) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    private C22431Lh(AnonymousClass1XY r3) {
        this.A01 = C07380dK.A00(r3);
    }
}
