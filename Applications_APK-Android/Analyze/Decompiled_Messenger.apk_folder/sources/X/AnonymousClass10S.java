package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;

/* renamed from: X.10S  reason: invalid class name */
public final class AnonymousClass10S {
    public final FbSharedPreferences A00;
    public final AnonymousClass10T A01;

    public static final AnonymousClass10S A00(AnonymousClass1XY r1) {
        return new AnonymousClass10S(r1);
    }

    public AnonymousClass10S(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass10T.A00(r2);
        this.A00 = FbSharedPreferencesModule.A00(r2);
    }
}
