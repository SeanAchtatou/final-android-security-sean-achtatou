package X;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Locale;
import java.util.TimeZone;

/* renamed from: X.0jr  reason: invalid class name and case insensitive filesystem */
public final class C10290jr implements Serializable {
    private static final long serialVersionUID = 4939673998947122190L;
    public final C10140jc _annotationIntrospector;
    public final C10060jU _classIntrospector;
    public final DateFormat _dateFormat;
    public final C10350jx _defaultBase64;
    public final CXG _handlerInstantiator;
    public final Locale _locale;
    public final BEG _propertyNamingStrategy;
    public final TimeZone _timeZone;
    public final C10300js _typeFactory;
    public final CXQ _typeResolverBuilder;
    public final C10160je _visibilityChecker;

    public C10290jr withTypeFactory(C10300js r13) {
        C10300js r5 = r13;
        if (this._typeFactory == r13) {
            return this;
        }
        return new C10290jr(this._classIntrospector, this._annotationIntrospector, this._visibilityChecker, this._propertyNamingStrategy, r5, this._typeResolverBuilder, this._dateFormat, this._handlerInstantiator, this._locale, this._timeZone, this._defaultBase64);
    }

    public C10290jr withVisibility(AnonymousClass0o9 r14, C10180jg r15) {
        return new C10290jr(this._classIntrospector, this._annotationIntrospector, this._visibilityChecker.withVisibility(r14, r15), this._propertyNamingStrategy, this._typeFactory, this._typeResolverBuilder, this._dateFormat, this._handlerInstantiator, this._locale, this._timeZone, this._defaultBase64);
    }

    public C10290jr(C10060jU r1, C10140jc r2, C10160je r3, BEG beg, C10300js r5, CXQ cxq, DateFormat dateFormat, CXG cxg, Locale locale, TimeZone timeZone, C10350jx r11) {
        this._classIntrospector = r1;
        this._annotationIntrospector = r2;
        this._visibilityChecker = r3;
        this._propertyNamingStrategy = beg;
        this._typeFactory = r5;
        this._typeResolverBuilder = cxq;
        this._dateFormat = dateFormat;
        this._handlerInstantiator = cxg;
        this._locale = locale;
        this._timeZone = timeZone;
        this._defaultBase64 = r11;
    }
}
