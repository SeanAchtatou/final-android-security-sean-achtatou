package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* renamed from: X.0Yx  reason: invalid class name and case insensitive filesystem */
public final class C05430Yx implements AnonymousClass0Z1 {
    private static final Comparator A01 = new C07350dH();
    private AnonymousClass0UN A00;

    public String Amj() {
        return "mqtt_ipc_client_log";
    }

    public static final C05430Yx A00(AnonymousClass1XY r1) {
        return new C05430Yx(r1);
    }

    public String getCustomData(Throwable th) {
        StringBuilder sb = new StringBuilder();
        ArrayList<C34431pZ> A03 = ((C29291gB) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A3R, this.A00)).A00.A03();
        Collections.sort(A03, A01);
        for (C34431pZ r3 : A03) {
            sb.append(StringFormatUtil.formatStrLocaleSafe("[%s] %s%n", Long.valueOf(r3.getStartTime()), r3.Aae()));
        }
        return sb.toString();
    }

    private C05430Yx(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
