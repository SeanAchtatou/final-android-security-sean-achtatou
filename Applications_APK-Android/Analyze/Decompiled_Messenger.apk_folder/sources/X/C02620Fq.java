package X;

/* renamed from: X.0Fq  reason: invalid class name and case insensitive filesystem */
public final class C02620Fq extends C007907e {
    public volatile C03150Kf A00;

    public AnonymousClass0FM A03() {
        return new C02600Fo();
    }

    public boolean A04(AnonymousClass0FM r3) {
        C02600Fo r32 = (C02600Fo) r3;
        synchronized (this) {
            if (r32 == null) {
                throw new IllegalArgumentException("Null value passed to getSnapshot!");
            } else if (this.A00 == null) {
                return false;
            } else {
                boolean B3N = this.A00.B3N(r32);
                return B3N;
            }
        }
    }
}
