package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1xL  reason: invalid class name and case insensitive filesystem */
public final class C38381xL {
    public C38371xK A00;
    public ImmutableList A01 = RegularImmutableList.A02;
    public String A02;
    public String A03;
    public Set A04 = new HashSet();
}
