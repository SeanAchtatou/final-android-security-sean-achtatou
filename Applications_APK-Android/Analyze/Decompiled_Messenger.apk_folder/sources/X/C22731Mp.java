package X;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.1Mp  reason: invalid class name and case insensitive filesystem */
public abstract class C22731Mp {
    public final ConcurrentMap A00 = AnonymousClass0TG.A07();
    private final C22751Mr A01;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x005d, code lost:
        if (r2.BBh((X.AnonymousClass1Y7) X.C90794Vb.A01(r4.A03, r3).A09(X.C90794Vb.A00)) != false) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void A00() {
        /*
            r8 = this;
            monitor-enter(r8)
            X.1Mr r4 = r8.A01     // Catch:{ all -> 0x00b0 }
            if (r4 == 0) goto L_0x00ae
            java.lang.String r3 = r8.A06()     // Catch:{ all -> 0x00b0 }
            com.facebook.prefs.shared.FbSharedPreferences r0 = r4.A02     // Catch:{ all -> 0x00b0 }
            boolean r0 = r0.BFQ()     // Catch:{ all -> 0x00b0 }
            if (r0 == 0) goto L_0x005f
            X.06B r0 = r4.A00     // Catch:{ all -> 0x00b0 }
            long r6 = r0.now()     // Catch:{ all -> 0x00b0 }
            com.facebook.prefs.shared.FbSharedPreferences r5 = r4.A02     // Catch:{ all -> 0x00b0 }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x00b0 }
            r0.get()     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = r4.A03     // Catch:{ all -> 0x00b0 }
            X.1Y7 r1 = X.C90794Vb.A01(r0, r3)     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = X.C90794Vb.A01     // Catch:{ all -> 0x00b0 }
            X.063 r2 = r1.A09(r0)     // Catch:{ all -> 0x00b0 }
            X.1Y7 r2 = (X.AnonymousClass1Y7) r2     // Catch:{ all -> 0x00b0 }
            r0 = 0
            long r0 = r5.At2(r2, r0)     // Catch:{ all -> 0x00b0 }
            long r6 = r6 - r0
            X.0Tq r0 = r4.A04     // Catch:{ all -> 0x00b0 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00b0 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x00b0 }
            long r1 = r0.longValue()     // Catch:{ all -> 0x00b0 }
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x005f
            com.facebook.prefs.shared.FbSharedPreferences r2 = r4.A02     // Catch:{ all -> 0x00b0 }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x00b0 }
            r0.get()     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = r4.A03     // Catch:{ all -> 0x00b0 }
            X.1Y7 r1 = X.C90794Vb.A01(r0, r3)     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = X.C90794Vb.A00     // Catch:{ all -> 0x00b0 }
            X.063 r0 = r1.A09(r0)     // Catch:{ all -> 0x00b0 }
            X.1Y7 r0 = (X.AnonymousClass1Y7) r0     // Catch:{ all -> 0x00b0 }
            boolean r1 = r2.BBh(r0)     // Catch:{ all -> 0x00b0 }
            r0 = 1
            if (r1 == 0) goto L_0x0060
        L_0x005f:
            r0 = 0
        L_0x0060:
            if (r0 == 0) goto L_0x00ae
            X.1Mr r4 = r8.A01     // Catch:{ all -> 0x00b0 }
            java.lang.String r5 = r8.A06()     // Catch:{ all -> 0x00b0 }
            com.fasterxml.jackson.databind.JsonNode r0 = r8.A02()     // Catch:{ all -> 0x00b0 }
            java.lang.String r2 = r0.toString()     // Catch:{ all -> 0x00b0 }
            com.facebook.prefs.shared.FbSharedPreferences r0 = r4.A02     // Catch:{ all -> 0x00b0 }
            X.1hn r3 = r0.edit()     // Catch:{ all -> 0x00b0 }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x00b0 }
            r0.get()     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = r4.A03     // Catch:{ all -> 0x00b0 }
            X.1Y7 r1 = X.C90794Vb.A01(r0, r5)     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = X.C90794Vb.A00     // Catch:{ all -> 0x00b0 }
            X.063 r0 = r1.A09(r0)     // Catch:{ all -> 0x00b0 }
            X.1Y7 r0 = (X.AnonymousClass1Y7) r0     // Catch:{ all -> 0x00b0 }
            r3.BzC(r0, r2)     // Catch:{ all -> 0x00b0 }
            X.0US r0 = r4.A01     // Catch:{ all -> 0x00b0 }
            r0.get()     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = r4.A03     // Catch:{ all -> 0x00b0 }
            X.1Y7 r1 = X.C90794Vb.A01(r0, r5)     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = X.C90794Vb.A01     // Catch:{ all -> 0x00b0 }
            X.063 r2 = r1.A09(r0)     // Catch:{ all -> 0x00b0 }
            X.1Y7 r2 = (X.AnonymousClass1Y7) r2     // Catch:{ all -> 0x00b0 }
            X.06B r0 = r4.A00     // Catch:{ all -> 0x00b0 }
            long r0 = r0.now()     // Catch:{ all -> 0x00b0 }
            r3.BzA(r2, r0)     // Catch:{ all -> 0x00b0 }
            r3.commit()     // Catch:{ all -> 0x00b0 }
            r8.A03()     // Catch:{ all -> 0x00b0 }
        L_0x00ae:
            monitor-exit(r8)
            return
        L_0x00b0:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22731Mp.A00():void");
    }

    public String A06() {
        return !(this instanceof AnonymousClass1N2) ? !(this instanceof C22721Mo) ? "cache_counters" : ((C22721Mo) this).A00 : ((AnonymousClass1N2) this).A04;
    }

    public JsonNode A02() {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        for (Map.Entry entry : this.A00.entrySet()) {
            if (((Long) entry.getValue()).longValue() != 0) {
                objectNode.put((String) entry.getKey(), ((Long) entry.getValue()).longValue());
            }
        }
        return objectNode;
    }

    public synchronized void A03() {
        if (!(this instanceof C23001Nv)) {
            synchronized (this) {
                this.A00.clear();
            }
            return;
        }
        C23001Nv r5 = (C23001Nv) this;
        for (Map.Entry entry : r5.A00.entrySet()) {
            if (!((String) entry.getKey()).contains(C35711rf.A00(AnonymousClass07B.A0C)) || ((Long) entry.getValue()).longValue() == 0) {
                r5.A00.remove((String) entry.getKey());
            }
        }
    }

    public void A04(String str, long j) {
        boolean replace;
        do {
            Long l = (Long) this.A00.get(str);
            if (l == null) {
                replace = false;
                if (((Long) this.A00.putIfAbsent(str, Long.valueOf(j))) == null) {
                    replace = true;
                    continue;
                } else {
                    continue;
                }
            } else {
                replace = this.A00.replace(str, l, Long.valueOf(l.longValue() + j));
                continue;
            }
        } while (!replace);
        A00();
    }

    public void A05(String str, long j) {
        boolean replace;
        do {
            Long l = (Long) this.A00.get(str);
            if (l == null) {
                replace = false;
                if (((Long) this.A00.putIfAbsent(str, Long.valueOf(j))) == null) {
                    replace = true;
                    continue;
                } else {
                    continue;
                }
            } else {
                replace = this.A00.replace(str, l, Long.valueOf(j));
                continue;
            }
        } while (!replace);
        A00();
    }

    public synchronized void AMh(C11670nb r4) {
        if (!(this instanceof AnonymousClass1N2)) {
            synchronized (this) {
                JsonNode A02 = A02();
                if (A02.size() > 0) {
                    r4.A0B(A06(), A02);
                }
                A03();
            }
            return;
        }
        AnonymousClass1N2 r2 = (AnonymousClass1N2) this;
        if (!r2.A00.isEmpty()) {
            r4.A0C(r2.A06(), r2.A00);
            r2.A00.clear();
        }
    }

    public C22731Mp(C22751Mr r2) {
        this.A01 = r2;
    }
}
