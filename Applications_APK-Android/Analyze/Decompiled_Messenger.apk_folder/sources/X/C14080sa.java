package X;

import android.view.View;
import android.view.ViewParent;
import androidx.fragment.app.Fragment;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0sa  reason: invalid class name and case insensitive filesystem */
public final class C14080sa implements CallerContextable {
    private static volatile C14080sa A00 = null;
    public static final String __redex_internal_original_name = "com.facebook.common.callercontexttagger.AnalyticsTagger";

    public static final C14080sa A01(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C14080sa.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C14080sa();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static CallerContext A00(View view) {
        CallerContext callerContext = (CallerContext) view.getTag(2131296560);
        if (callerContext == null) {
            ViewParent parent = view.getParent();
            while (parent instanceof View) {
                callerContext = (CallerContext) ((View) parent).getTag(2131296560);
                if (callerContext == null) {
                    parent = parent.getParent();
                }
            }
            return null;
        }
        return callerContext;
    }

    public void A02(View view, String str, Fragment fragment) {
        view.setTag(2131296560, new CallerContext(AnonymousClass07V.A00(fragment.getClass()), str, str, str));
    }

    public void A03(View view, String str, Class cls) {
        view.setTag(2131296560, CallerContext.A08(cls, str, str));
    }
}
