package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.profilo.ipc.TraceContext;

/* renamed from: X.0HW  reason: invalid class name */
public final class AnonymousClass0HW implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new TraceContext.TraceConfigExtras(parcel);
    }

    public Object[] newArray(int i) {
        return new TraceContext.TraceConfigExtras[i];
    }
}
