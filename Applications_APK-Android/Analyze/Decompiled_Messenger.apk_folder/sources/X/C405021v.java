package X;

import android.os.Handler;
import com.facebook.cameracore.mediapipeline.services.audio.interfaces.AudioPlatformComponentHost;
import com.facebook.common.dextricks.DexStore;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/* renamed from: X.21v  reason: invalid class name and case insensitive filesystem */
public final class C405021v implements C404721s {
    public DZS A00;
    public boolean A01;
    private long A02;
    public final WeakReference A03;
    public final WeakHashMap A04 = new WeakHashMap();
    public final byte[] A05 = new byte[DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED];
    private final C26954DIz A06 = new C26954DIz(this);
    public volatile Handler A07;
    public volatile C27286Da4 A08;

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A00(X.C405021v r3) {
        /*
            monitor-enter(r3)
            java.lang.ref.WeakReference r0 = r3.A03     // Catch:{ all -> 0x0031 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0031 }
            X.DIp r0 = (X.C26944DIp) r0     // Catch:{ all -> 0x0031 }
            r1 = 0
            if (r0 == 0) goto L_0x002f
            com.facebook.cameracore.mediapipeline.services.audio.interfaces.AudioPlatformComponentHost r2 = r0.Adg()     // Catch:{ all -> 0x0031 }
            if (r2 == 0) goto L_0x002f
            java.util.WeakHashMap r0 = r3.A04     // Catch:{ all -> 0x0031 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0031 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x0022
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x002f
        L_0x0022:
            r2.startRecording(r1)     // Catch:{ all -> 0x0031 }
            java.util.WeakHashMap r1 = r3.A04     // Catch:{ all -> 0x0031 }
            java.lang.Boolean r0 = java.lang.Boolean.TRUE     // Catch:{ all -> 0x0031 }
            r1.put(r2, r0)     // Catch:{ all -> 0x0031 }
            r0 = 1
            monitor-exit(r3)
            return r0
        L_0x002f:
            monitor-exit(r3)
            return r1
        L_0x0031:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C405021v.A00(X.21v):boolean");
    }

    public void C1t(C27286Da4 da4, C45282Lm r5, Handler handler) {
        AudioPlatformComponentHost Adg;
        this.A08 = null;
        if (this.A01) {
            synchronized (this) {
                C26944DIp dIp = (C26944DIp) this.A03.get();
                if (!(dIp == null || (Adg = dIp.Adg()) == null)) {
                    Adg.stopRecording();
                }
            }
        }
        DZS dzs = this.A00;
        if (!AnonymousClass00S.A04(dzs.A04, new DZY(dzs, r5, handler), 1445153436)) {
            DZS.A02(r5, handler, "removeOutput", "Failed to post message");
        }
    }

    public void release() {
        this.A07 = null;
        this.A04.clear();
    }

    public void A01(byte[] bArr, int i) {
        long j = this.A02;
        C27286Da4 da4 = this.A08;
        if (da4 != null) {
            da4.A00(bArr, i, j);
        }
        this.A02 += (long) (((i >> 1) * 1000000) / this.A00.A04());
    }

    public void ANJ(C27286Da4 da4, C45282Lm r6, Handler handler) {
        this.A08 = da4;
        this.A02 = 0;
        if (this.A01) {
            A00(this);
        }
        DZS dzs = this.A00;
        if (!AnonymousClass00S.A04(dzs.A04, new DZX(dzs, this.A06, r6, handler), 1158398296)) {
            DZS.A02(r6, handler, "addOutput", "Failed to post message");
        }
    }

    public void Bxk(DIY diy, Handler handler, C45282Lm r4, Handler handler2) {
        this.A07 = handler;
        this.A00.A05(r4, handler2);
    }

    public C405021v(C26944DIp dIp, DZS dzs, boolean z) {
        this.A03 = new WeakReference(dIp);
        this.A00 = dzs;
        this.A01 = z;
    }
}
