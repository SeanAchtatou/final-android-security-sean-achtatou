package X;

import com.facebook.redex.dynamicanalysis.DynamicAnalysis;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.09c  reason: invalid class name and case insensitive filesystem */
public final class C011509c extends C08190ep {
    private static volatile C011509c A01;
    private final C25051Yd A00;

    public static final C011509c A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C011509c.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C011509c(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void BeU(C04270Tg r21) {
        short[] sArr;
        C04270Tg r2 = r21;
        if (r2.A02 == 3997703) {
            boolean z = false;
            if (DynamicAnalysis.sNumStaticallyInstrumented > 0) {
                z = true;
            }
            if (z) {
                DynamicAnalysis.A00(r2);
                if (this.A00.Aer(281844343964411L, AnonymousClass0XE.A07)) {
                    if ((DynamicAnalysis.sTraceType & 1) == 1) {
                        int i = DynamicAnalysis.sNumStaticallyInstrumented;
                        if (i == 0) {
                            C010708t.A0P("DYNA", "Summary: Kind: %s, Session: %d, Rows: 0, Coverage: 0 (0.0)", "COLD_M", 0);
                        } else {
                            int i2 = 0;
                            for (short[] length : DynamicAnalysis.sMethodStatsArray) {
                                i2 += length.length;
                            }
                            int i3 = i2 / i;
                            if (1 <= i3) {
                                int i4 = 3;
                                if (i3 <= 3) {
                                    StringBuilder sb = new StringBuilder(3900);
                                    String A0M = AnonymousClass08S.A0M("COLD_M", ":", 0, ",ROW:");
                                    int i5 = 0;
                                    int i6 = 0;
                                    int i7 = 0;
                                    while (i5 < DynamicAnalysis.sNumStaticallyInstrumented) {
                                        short[] sArr2 = DynamicAnalysis.sMethodStatsArray[i5 % r13];
                                        int i8 = (i5 / r13) * i3;
                                        if ((i3 == i4 || sArr2[i8] != 0) && !(i3 == i4 && sArr2[i8] == 0 && sArr2[i8 + 1] == 0)) {
                                            i7++;
                                            sb.append(i5);
                                            sb.append(":");
                                            for (int i9 = 0; i9 < i3; i9++) {
                                                short s = sArr2[i8 + i9];
                                                if (s != 0) {
                                                    sb.append((int) s);
                                                }
                                                if (i9 != i3 - 1) {
                                                    sb.append(",");
                                                }
                                            }
                                            if (i3 == 1) {
                                                sb.append(",,");
                                            } else if (i3 == 2) {
                                                sb.append(",");
                                            }
                                            sb.append(";");
                                            if (sb.length() >= 3900) {
                                                i6++;
                                                C010708t.A0P("DYNA", "%s%s:%s", A0M, Integer.valueOf(i6), sb.toString());
                                                sb.setLength(0);
                                            }
                                        }
                                        i5++;
                                        i4 = 3;
                                    }
                                    if (sb.length() > 0) {
                                        i6++;
                                        C010708t.A0P("DYNA", "%s%s:%s", A0M, Integer.valueOf(i6), sb.toString());
                                    }
                                    Integer valueOf = Integer.valueOf(i6);
                                    Integer valueOf2 = Integer.valueOf(i7);
                                    int i10 = DynamicAnalysis.sNumStaticallyInstrumented;
                                    C010708t.A0P("DYNA", "Summary: Kind: %s, Session: %d, Rows: %d, Coverage: %d/%d (%.3f)", "COLD_M", 0, valueOf, valueOf2, Integer.valueOf(i10), Float.valueOf(((float) i7) / ((float) i10)));
                                }
                            }
                            throw new UnsupportedOperationException("Unexpected num stats per method!");
                        }
                    }
                    if ((DynamicAnalysis.sTraceType & 2) == 2) {
                        if (DynamicAnalysis.sBasicBlockStats.length == 0) {
                            C010708t.A0P("DYNA", "Summary: Kind: %s, Session: %d, Rows: 0, Coverage: 0 (0.0)", "COLD_B", 0);
                        } else {
                            StringBuilder sb2 = new StringBuilder(3900);
                            String A0M2 = AnonymousClass08S.A0M("COLD_B", ":", 0, ",ROW:");
                            int i11 = 0;
                            int i12 = 0;
                            int i13 = 0;
                            while (true) {
                                sArr = DynamicAnalysis.sBasicBlockStats;
                                if (i11 >= sArr.length) {
                                    break;
                                } else if (sArr[i11] == 0) {
                                    i11++;
                                } else {
                                    StringBuilder sb3 = new StringBuilder(128);
                                    sb3.append(i11);
                                    sb3.append(":");
                                    while (true) {
                                        short s2 = DynamicAnalysis.sBasicBlockStats[i11];
                                        short s3 = (short) (s2 & Short.MAX_VALUE);
                                        if (s3 != 0) {
                                            sb3.append((int) s3);
                                            i13 += Integer.bitCount(s3);
                                        }
                                        if ((s2 & 32768) == 0) {
                                            break;
                                        }
                                        sb3.append(",");
                                        i11++;
                                    }
                                    sb3.append(";");
                                    i11++;
                                    if (sb3.length() + sb2.length() >= 3900) {
                                        i12++;
                                        C010708t.A0P("DYNA", "%s%s:%s", A0M2, Integer.valueOf(i12), sb2.toString());
                                        sb2.setLength(0);
                                    }
                                    sb2.append(sb3.toString());
                                }
                            }
                            if (sb2.length() > 0) {
                                i12++;
                                C010708t.A0P("DYNA", "%s%s:%s", A0M2, Integer.valueOf(i12), sb2.toString());
                            }
                            C010708t.A0P("DYNA", "Summary: Kind: %s, Session: %d, Rows: %d, Coverage: %d (%.3f)", "COLD_B", 0, Integer.valueOf(i12), Integer.valueOf(i13), Float.valueOf(((float) i13) / ((float) sArr.length)));
                        }
                    }
                    String str = DynamicAnalysis.A05;
                    Integer valueOf3 = Integer.valueOf(DynamicAnalysis.A03);
                    Integer valueOf4 = Integer.valueOf(DynamicAnalysis.A02);
                    Integer valueOf5 = Integer.valueOf(DynamicAnalysis.sNumStaticallyInstrumented);
                    int i14 = 0;
                    for (short[] length2 : DynamicAnalysis.sMethodStatsArray) {
                        i14 += length2.length;
                    }
                    C010708t.A0P("DYNA", "Cold start traces dumped: %s, %d ms, cut order: %d, methods: %d, stat elements: %d", str, valueOf3, valueOf4, valueOf5, Integer.valueOf(i14));
                }
                C25051Yd r22 = this.A00;
                AnonymousClass0XE r5 = AnonymousClass0XE.A07;
                C010708t.A0P("DYNA|ColdStartTTIListener", "Init.COLD_START finished: UploadGK? %s, Sampling? %d, Interval? %d", Boolean.valueOf(r22.Aer(281844344095485L, r5)), Long.valueOf(this.A00.At4(563319320871265L, r5)), Long.valueOf(this.A00.At4(563319320609120L, r5)));
            }
        }
    }

    private C011509c(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }

    public AnonymousClass0Ti AsW() {
        return AnonymousClass0Ti.A00(3997703);
    }
}
