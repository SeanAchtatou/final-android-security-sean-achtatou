package X;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.google.common.collect.ImmutableList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0nY  reason: invalid class name and case insensitive filesystem */
public final class C11650nY {
    private static volatile C11650nY A02;
    public final C001500z A00;
    public final C07790eA A01;

    public static final C11650nY A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C11650nY.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C11650nY(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public void A01(Fragment fragment) {
        C12330pA r2;
        if (this.A00 == C001500z.A03) {
            C07790eA r7 = this.A01;
            synchronized (r7) {
                Activity activity = (Activity) AnonymousClass065.A00(fragment.A1j(), Activity.class);
                if (activity != null) {
                    try {
                        if (r7.A0A.containsKey(Integer.valueOf(activity.hashCode()))) {
                            r2 = (C12330pA) r7.A0A.get(Integer.valueOf(activity.hashCode()));
                        } else {
                            String str = null;
                            if (activity instanceof C11510nI) {
                                str = ((C11510nI) activity).Act();
                            }
                            r2 = new C12330pA(activity, str);
                            C07790eA.A03(r7, r2);
                        }
                        r2.A0L(fragment);
                        if (fragment.A0i && fragment.A0L == null) {
                            r2.A0M(fragment);
                        }
                    } catch (IllegalStateException | NullPointerException unused) {
                        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, r7.A00)).A01("hsm_add_fragment_error"), 235);
                        if (uSLEBaseShape0S0000000.A0G()) {
                            uSLEBaseShape0S0000000.A0D("fragment", fragment.toString());
                            uSLEBaseShape0S0000000.A0D("activity", activity.toString());
                            uSLEBaseShape0S0000000.A0D("activity_simple_class_name", AnonymousClass07V.A01(activity.getClass()));
                            uSLEBaseShape0S0000000.A0D("fragment_simple_class_name", AnonymousClass07V.A01(fragment.getClass()));
                            uSLEBaseShape0S0000000.A06();
                        }
                    }
                    if (fragment.A0i) {
                        List A07 = r7.A07();
                        if (!r7.A03.equals(A07)) {
                            AnonymousClass1YL r0 = r7.A07;
                            ImmutableList immutableList = C07790eA.A0B;
                            if (r0.A05(immutableList)) {
                                r7.A07.A04(immutableList, new C11620nT(r7.A07(), AnonymousClass07B.A00, C07790eA.A01(r7)), C25141Ym.INSTANCE);
                                r7.A03 = A07;
                            }
                        }
                    }
                }
            }
        }
    }

    public void A02(Fragment fragment) {
        if (this.A00 == C001500z.A03) {
            C07790eA r7 = this.A01;
            synchronized (r7) {
                Activity activity = (Activity) AnonymousClass065.A00(fragment.A1j(), Activity.class);
                if (activity != null) {
                    if (r7.A0A.containsKey(Integer.valueOf(activity.hashCode()))) {
                        C12330pA r1 = (C12330pA) r7.A0A.get(Integer.valueOf(activity.hashCode()));
                        r1.A0L(fragment);
                        r1.A0M(fragment);
                        C97954mH r0 = r1.A03;
                        if (r0 != null) {
                            String str = r0.A0F;
                            if (str != null && !str.equals(r7.A01) && !str.equals(r1.A06)) {
                                AnonymousClass1YL r02 = r7.A05;
                                ImmutableList immutableList = C07790eA.A0B;
                                if (r02.A05(immutableList)) {
                                    r7.A05.A04(immutableList, new C04210Sx(str), C25141Ym.INSTANCE);
                                }
                            }
                            r7.A01 = str;
                        }
                    }
                    List A07 = r7.A07();
                    if (!r7.A03.equals(A07)) {
                        AnonymousClass1YL r03 = r7.A07;
                        ImmutableList immutableList2 = C07790eA.A0B;
                        if (r03.A05(immutableList2)) {
                            r7.A07.A04(immutableList2, new C11620nT(r7.A07(), AnonymousClass07B.A0C, C07790eA.A01(r7)), C25141Ym.INSTANCE);
                            r7.A03 = A07;
                        }
                    }
                }
            }
        }
    }

    private C11650nY(AnonymousClass1XY r2) {
        this.A01 = C07790eA.A00(r2);
        this.A00 = AnonymousClass0UU.A05(r2);
    }
}
