package X;

/* renamed from: X.1QH  reason: invalid class name */
public final class AnonymousClass1QH extends AnonymousClass1QI implements AnonymousClass1MK {
    private final AnonymousClass1MK A00;
    private final C23411Pk A01;

    public void Blr(AnonymousClass1QK r3) {
        C23411Pk r1 = this.A01;
        if (r1 != null) {
            r1.Bls(r3.A0B);
        }
        AnonymousClass1MK r0 = this.A00;
        if (r0 != null) {
            r0.Blr(r3);
        }
    }

    public void Bm1(AnonymousClass1QK r5, Throwable th) {
        C23411Pk r3 = this.A01;
        if (r3 != null) {
            r3.Bm2(r5.A09, r5.A0B, th, r5.A08());
        }
        AnonymousClass1MK r0 = this.A00;
        if (r0 != null) {
            r0.Bm1(r5, th);
        }
    }

    public void Bm6(AnonymousClass1QK r6) {
        C23411Pk r4 = this.A01;
        if (r4 != null) {
            r4.Bm7(r6.A09, r6.A0A, r6.A0B, r6.A08());
        }
        AnonymousClass1MK r0 = this.A00;
        if (r0 != null) {
            r0.Bm6(r6);
        }
    }

    public void Bm8(AnonymousClass1QK r5) {
        C23411Pk r3 = this.A01;
        if (r3 != null) {
            r3.Bm9(r5.A09, r5.A0B, r5.A08());
        }
        AnonymousClass1MK r0 = this.A00;
        if (r0 != null) {
            r0.Bm8(r5);
        }
    }

    public AnonymousClass1QH(C23411Pk r1, AnonymousClass1MK r2) {
        super(r1, r2);
        this.A01 = r1;
        this.A00 = r2;
    }
}
