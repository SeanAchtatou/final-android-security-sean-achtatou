package X;

import android.content.Context;
import com.facebook.prefs.shared.FbSharedPreferences;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

/* renamed from: X.1EX  reason: invalid class name */
public final class AnonymousClass1EX {
    public long A00;
    public ArrayList A01;
    private File A02;
    public final int A03;
    public final Context A04;
    public final AnonymousClass06B A05;
    public final FbSharedPreferences A06;
    public final AnonymousClass1Y7 A07;
    public final Object A08 = new Object();
    public final String A09;
    public final ExecutorService A0A;

    private File A00() {
        int AqN = this.A06.AqN((AnonymousClass1Y7) this.A07.A09("LOGGER_BUFFER_HEAD"), 0);
        int AqN2 = this.A06.AqN((AnonymousClass1Y7) this.A07.A09("LOGGER_BUFFER_TAIL"), 0);
        int AqN3 = this.A06.AqN((AnonymousClass1Y7) this.A07.A09("LOGGER_BUFFER_SIZE"), 1);
        File file = new File(this.A04.getCacheDir(), AnonymousClass08S.A0A(this.A09, AqN, ".txt"));
        if (AqN3 == 5) {
            File file2 = new File(this.A04.getCacheDir(), AnonymousClass08S.A0A(this.A09, AqN2, ".txt"));
            boolean z = false;
            if (file.length() >= ((long) this.A03)) {
                z = true;
            }
            if (z) {
                file2.delete();
                File file3 = new File(this.A04.getCacheDir(), AnonymousClass08S.A0A(this.A09, AqN2, ".txt"));
                try {
                    file3.createNewFile();
                } catch (IOException unused) {
                }
                C30281hn edit = this.A06.edit();
                edit.Bz6((AnonymousClass1Y7) this.A07.A09("LOGGER_BUFFER_HEAD"), AqN2);
                edit.Bz6((AnonymousClass1Y7) this.A07.A09("LOGGER_BUFFER_TAIL"), (AqN2 + 1) % 5);
                edit.commit();
                return file3;
            }
        } else {
            boolean z2 = false;
            if (file.length() >= ((long) this.A03)) {
                z2 = true;
            }
            if (z2) {
                int i = (AqN + 1) % 5;
                File file4 = new File(this.A04.getCacheDir(), AnonymousClass08S.A0A(this.A09, i, ".txt"));
                C30281hn edit2 = this.A06.edit();
                edit2.Bz6((AnonymousClass1Y7) this.A07.A09("LOGGER_BUFFER_HEAD"), i);
                edit2.Bz6((AnonymousClass1Y7) this.A07.A09("LOGGER_BUFFER_SIZE"), AqN3 + 1);
                edit2.commit();
                return file4;
            }
        }
        return file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x002b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.AnonymousClass1EX r4, java.util.List r5) {
        /*
            java.io.FileWriter r3 = new java.io.FileWriter     // Catch:{ IOException -> 0x002c }
            java.io.File r1 = r4.A02     // Catch:{ IOException -> 0x002c }
            r0 = 1
            r3.<init>(r1, r0)     // Catch:{ IOException -> 0x002c }
            r2 = 0
        L_0x0009:
            int r0 = r5.size()     // Catch:{ all -> 0x0025 }
            if (r2 >= r0) goto L_0x0021
            java.lang.Object r1 = r5.get(r2)     // Catch:{ all -> 0x0025 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0025 }
            r0 = 10
            java.lang.String r0 = X.AnonymousClass08S.A06(r1, r0)     // Catch:{ all -> 0x0025 }
            r3.write(r0)     // Catch:{ all -> 0x0025 }
            int r2 = r2 + 1
            goto L_0x0009
        L_0x0021:
            r3.close()     // Catch:{ IOException -> 0x002c }
            goto L_0x0034
        L_0x0025:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x002b }
        L_0x002b:
            throw r0     // Catch:{ IOException -> 0x002c }
        L_0x002c:
            r2 = move-exception
            java.lang.String r1 = "PushBugReportBuffer"
            java.lang.String r0 = "Failed to flush logs"
            X.C010708t.A0L(r1, r0, r2)
        L_0x0034:
            java.io.File r0 = r4.A00()
            r4.A02 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1EX.A01(X.1EX, java.util.List):void");
    }

    public AnonymousClass1EX(Context context, FbSharedPreferences fbSharedPreferences, AnonymousClass06B r4, String str, AnonymousClass1Y7 r6, ExecutorService executorService, int i) {
        this.A04 = context;
        this.A06 = fbSharedPreferences;
        this.A05 = r4;
        this.A07 = r6;
        this.A09 = str;
        this.A03 = i;
        this.A0A = executorService;
        this.A01 = new ArrayList();
        this.A02 = A00();
    }
}
