package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.interstitial.triggers.InterstitialTriggerContext;

/* renamed from: X.1Ep  reason: invalid class name and case insensitive filesystem */
public final class C20991Ep implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InterstitialTriggerContext(parcel);
    }

    public Object[] newArray(int i) {
        return new InterstitialTriggerContext[i];
    }
}
