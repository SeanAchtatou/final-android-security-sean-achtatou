package X;

import com.google.android.gms.wearable.Asset;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1xG  reason: invalid class name and case insensitive filesystem */
public final class C38331xG {
    public final AnonymousClass94S A00;

    public static void A00(List list, AnonymousClass94S r14, String str, C23337Bcn bcn) {
        String str2;
        C23337Bcn bcn2 = bcn;
        int i = bcn2.A00;
        boolean z = false;
        int i2 = 14;
        if (i == 14) {
            r14.A00.put(str, null);
            return;
        }
        C23336Bcm bcm = bcn2.A01;
        if (i == 1) {
            r14.A00.put(str, bcm.A08);
        } else if (i == 11) {
            r14.A00.put(str, bcm.A0D);
        } else if (i == 12) {
            r14.A00.put(str, bcm.A0A);
        } else if (i == 15) {
            r14.A00.put(str, bcm.A09);
        } else if (i == 2) {
            r14.A00.put(str, bcm.A06);
        } else if (i == 3) {
            r14.A00.put(str, Double.valueOf(bcm.A00));
        } else if (i == 4) {
            r14.A00.put(str, Float.valueOf(bcm.A01));
        } else if (i == 5) {
            r14.A00.put(str, Long.valueOf(bcm.A04));
        } else if (i == 6) {
            r14.A00.put(str, Integer.valueOf(bcm.A02));
        } else if (i == 7) {
            r14.A00.put(str, Byte.valueOf((byte) bcm.A03));
        } else if (i == 8) {
            r14.A00.put(str, Boolean.valueOf(bcm.A07));
        } else if (i != 13) {
            int i3 = 9;
            if (i == 9) {
                AnonymousClass94S r5 = new AnonymousClass94S();
                for (C23338Bco bco : bcm.A0B) {
                    A00(list, r5, bco.A01, bco.A00);
                }
                r14.A00.put(str, r5);
            } else if (i == 10) {
                C23337Bcn[] bcnArr = bcm.A0C;
                int i4 = 14;
                for (C23337Bcn bcn3 : bcnArr) {
                    int i5 = bcn3.A00;
                    if (i4 == 14) {
                        if (i5 == 9 || i5 == 2 || i5 == 6) {
                            i4 = i5;
                        } else if (i5 != 14) {
                            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 48);
                            sb.append("Unexpected TypedValue type: ");
                            sb.append(i5);
                            sb.append(" for key ");
                            sb.append(str);
                            throw new IllegalArgumentException(sb.toString());
                        }
                    } else if (i5 != i4) {
                        StringBuilder sb2 = new StringBuilder(String.valueOf(str).length() + AnonymousClass1Y3.A0x);
                        sb2.append("The ArrayList elements should all be the same type, but ArrayList with key ");
                        sb2.append(str);
                        sb2.append(" contains items of type ");
                        sb2.append(i4);
                        sb2.append(" and ");
                        sb2.append(i5);
                        throw new IllegalArgumentException(sb2.toString());
                    }
                }
                ArrayList arrayList = new ArrayList(r5);
                int i6 = 0;
                while (i6 < r5) {
                    C23337Bcn bcn4 = bcnArr[i6];
                    if (bcn4.A00 == i2) {
                        arrayList.add(z);
                    } else if (i4 == i3) {
                        AnonymousClass94S r12 = new AnonymousClass94S();
                        for (C23338Bco bco2 : bcn4.A01.A0B) {
                            A00(list, r12, bco2.A01, bco2.A00);
                        }
                        arrayList.add(r12);
                    } else if (i4 == 2) {
                        arrayList.add(bcn4.A01.A06);
                    } else if (i4 == 6) {
                        arrayList.add(Integer.valueOf(bcn4.A01.A02));
                    } else {
                        StringBuilder sb3 = new StringBuilder(39);
                        sb3.append("Unexpected typeOfArrayList: ");
                        sb3.append(i4);
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    i6++;
                    z = false;
                    i2 = 14;
                    i3 = 9;
                }
                if (i4 == i2 || i4 == i3 || i4 == 2 || i4 == 6) {
                    r14.A00.put(str, arrayList);
                    return;
                }
                StringBuilder sb4 = new StringBuilder(39);
                sb4.append("Unexpected typeOfArrayList: ");
                sb4.append(i4);
                throw new IllegalStateException(sb4.toString());
            } else {
                StringBuilder sb5 = new StringBuilder(43);
                sb5.append("populateBundle: unexpected type ");
                sb5.append(i);
                throw new RuntimeException(sb5.toString());
            }
        } else if (list == null) {
            String valueOf = String.valueOf(str);
            if (valueOf.length() != 0) {
                str2 = "populateBundle: unexpected type for: ".concat(valueOf);
            } else {
                str2 = new String("populateBundle: unexpected type for: ");
            }
            throw new RuntimeException(str2);
        } else {
            r14.A00.put(str, (Asset) list.get((int) bcm.A05));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e4, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e5, code lost:
        r6 = java.lang.String.valueOf(r5.B7p());
        r3 = android.util.Base64.encodeToString(r5.AjY(), 0);
        r1 = new java.lang.StringBuilder((r6.length() + 50) + java.lang.String.valueOf(r3).length());
        r1.append("Unable to parse datamap from dataItem. uri=");
        r1.append(r6);
        r1.append(", data=");
        r1.append(r3);
        android.util.Log.w(X.ECX.$const$string(21), r1.toString());
        r2 = java.lang.String.valueOf(r5.B7p());
        r1 = new java.lang.StringBuilder(r2.length() + 44);
        r1.append("Unable to parse datamap from dataItem.  uri=");
        r1.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x014a, code lost:
        throw new java.lang.IllegalStateException(r1.toString(), r4);
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e4 A[ExcHandler: Bct | NullPointerException (r4v0 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:12:0x0033] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C38331xG(X.C205539mU r11) {
        /*
            r10 = this;
            r10.<init>()
            r11.B7p()
            java.lang.Object r5 = r11.freeze()
            X.9mU r5 = (X.C205539mU) r5
            byte[] r0 = r5.AjY()
            if (r0 != 0) goto L_0x0024
            java.util.Map r0 = r5.AdU()
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0024
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Cannot create DataMapItem from a DataItem  that wasn't made with DataMapItem."
            r1.<init>(r0)
            throw r1
        L_0x0024:
            byte[] r0 = r5.AjY()
            if (r0 != 0) goto L_0x0032
            X.94S r8 = new X.94S
            r8.<init>()
        L_0x002f:
            r10.A00 = r8
            return
        L_0x0032:
            r3 = 0
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r7.<init>()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.util.Map r0 = r5.AdU()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            int r4 = r0.size()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r6 = 0
        L_0x0041:
            if (r6 >= r4) goto L_0x0065
            java.util.Map r1 = r5.AdU()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.String r0 = java.lang.Integer.toString(r6)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            X.9mY r0 = (X.C205579mY) r0     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            if (r0 == 0) goto L_0x009c
            java.lang.String r2 = r0.getId()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            if (r2 == 0) goto L_0x00d6
            com.google.android.gms.wearable.Asset r1 = new com.google.android.gms.wearable.Asset     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r0 = 0
            r1.<init>(r0, r2, r0, r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r7.add(r1)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            int r6 = r6 + 1
            goto L_0x0041
        L_0x0065:
            byte[] r6 = r5.AjY()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            X.Bcq r4 = new X.Bcq     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r4.<init>()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            int r1 = r6.length     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            X.Bcp r2 = new X.Bcp     // Catch:{ Bct -> 0x00e2, IOException -> 0x00cd, Bct | NullPointerException -> 0x00e4 }
            r2.<init>(r6, r3, r1)     // Catch:{ Bct -> 0x00e2, IOException -> 0x00cd, Bct | NullPointerException -> 0x00e4 }
            r4.A05(r2)     // Catch:{ Bct -> 0x00e2, IOException -> 0x00cd, Bct | NullPointerException -> 0x00e4 }
            r1 = 0
            int r0 = r2.A02     // Catch:{ Bct -> 0x00e2, IOException -> 0x00cd, Bct | NullPointerException -> 0x00e4 }
            if (r0 != r1) goto L_0x00c5
            X.Bcu r9 = new X.Bcu     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r9.<init>(r4, r7)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            X.94S r8 = new X.94S     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r8.<init>()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            X.Bcq r0 = r9.A00     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            X.Bco[] r7 = r0.A00     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            int r6 = r7.length     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r4 = 0
        L_0x008c:
            if (r4 >= r6) goto L_0x002f
            r0 = r7[r4]     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.util.List r2 = r9.A01     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.String r1 = r0.A01     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            X.Bcn r0 = r0.A00     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            A00(r2, r8, r1, r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            int r4 = r4 + 1
            goto L_0x008c
        L_0x009c:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.String r2 = java.lang.String.valueOf(r5)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            int r0 = r2.length()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            int r0 = r0 + 64
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r1.<init>(r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.String r0 = "Cannot find DataItemAsset referenced in data at "
            r1.append(r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r1.append(r6)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.String r0 = " for "
            r1.append(r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r1.append(r2)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.String r0 = r1.toString()     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r4.<init>(r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            throw r4     // Catch:{ Bct | NullPointerException -> 0x00e4 }
        L_0x00c5:
            X.Bct r1 = new X.Bct     // Catch:{ Bct -> 0x00e2, IOException -> 0x00cd, Bct | NullPointerException -> 0x00e4 }
            java.lang.String r0 = "Protocol message end-group tag did not match expected tag."
            r1.<init>(r0)     // Catch:{ Bct -> 0x00e2, IOException -> 0x00cd, Bct | NullPointerException -> 0x00e4 }
            throw r1     // Catch:{ Bct -> 0x00e2, IOException -> 0x00cd, Bct | NullPointerException -> 0x00e4 }
        L_0x00cd:
            r2 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            java.lang.String r0 = "Reading from a byte array threw an IOException (should never happen)."
            r1.<init>(r0, r2)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            goto L_0x00e1
        L_0x00d6:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r0 = 191(0xbf, float:2.68E-43)
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
            r1.<init>(r0)     // Catch:{ Bct | NullPointerException -> 0x00e4 }
        L_0x00e1:
            throw r1     // Catch:{ Bct | NullPointerException -> 0x00e4 }
        L_0x00e2:
            r0 = move-exception
            throw r0     // Catch:{ Bct | NullPointerException -> 0x00e4 }
        L_0x00e4:
            r4 = move-exception
            android.net.Uri r0 = r5.B7p()
            java.lang.String r6 = java.lang.String.valueOf(r0)
            byte[] r0 = r5.AjY()
            java.lang.String r3 = android.util.Base64.encodeToString(r0, r3)
            int r0 = r6.length()
            int r2 = r0 + 50
            java.lang.String r0 = java.lang.String.valueOf(r3)
            int r0 = r0.length()
            int r2 = r2 + r0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r2)
            java.lang.String r0 = "Unable to parse datamap from dataItem. uri="
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = ", data="
            r1.append(r0)
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0 = 21
            java.lang.String r0 = X.ECX.$const$string(r0)
            android.util.Log.w(r0, r1)
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            android.net.Uri r0 = r5.B7p()
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r0 = r2.length()
            int r0 = r0 + 44
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Unable to parse datamap from dataItem.  uri="
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            r3.<init>(r0, r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38331xG.<init>(X.9mU):void");
    }
}
