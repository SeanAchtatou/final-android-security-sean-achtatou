package X;

import com.facebook.litho.annotations.Comparable;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1vF  reason: invalid class name */
public final class AnonymousClass1vF extends C17770zR {
    @Comparable(type = 13)
    public C58102tA A00;
    @Comparable(type = 13)
    public AnonymousClass9ZP A01;
    @Comparable(type = 13)
    public C121605oW A02;
    @Comparable(type = 5)
    public ImmutableList A03;

    public AnonymousClass1vF() {
        super("AccountLoginRecSelectAccountRootComponent");
    }
}
