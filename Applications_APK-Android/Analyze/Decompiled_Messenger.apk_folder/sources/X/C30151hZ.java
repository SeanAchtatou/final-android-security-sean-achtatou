package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.HashSet;
import java.util.Set;

@UserScoped
/* renamed from: X.1hZ  reason: invalid class name and case insensitive filesystem */
public final class C30151hZ {
    private static C05540Zi A04;
    public AnonymousClass0UN A00;
    public final AnonymousClass1HZ A01;
    public final Set A02;
    public final Object[] A03 = new Object[0];

    public static final C30151hZ A00(AnonymousClass1XY r4) {
        C30151hZ r0;
        synchronized (C30151hZ.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new C30151hZ((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (C30151hZ) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b3, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(com.facebook.messaging.model.threads.ThreadSummary r7) {
        /*
            r6 = this;
            boolean r0 = r7.A12
            r5 = 1
            if (r0 != 0) goto L_0x00b7
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r7.A0S
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0D(r4)
            if (r0 != 0) goto L_0x00b7
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0E(r4)
            r3 = 0
            if (r0 == 0) goto L_0x0039
            int r1 = X.AnonymousClass1Y3.BG8
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.2jP r0 = (X.C52622jP) r0
            java.util.Map r2 = r0.A00
            long r0 = r4.A0G()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            java.lang.Object r0 = r2.get(r0)
            java.util.Collection r0 = (java.util.Collection) r0
            if (r0 == 0) goto L_0x0037
            boolean r1 = r0.isEmpty()
            r0 = 1
            if (r1 == 0) goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            return r0
        L_0x0039:
            com.facebook.messaging.model.send.PendingSendQueueKey r1 = new com.facebook.messaging.model.send.PendingSendQueueKey
            X.1Hc r0 = X.C21471Hc.NORMAL
            r1.<init>(r4, r0)
            java.lang.Object[] r2 = r6.A03
            monitor-enter(r2)
            X.1HZ r0 = r6.A01     // Catch:{ all -> 0x00b4 }
            X.1v2 r0 = r0.A02(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x0051
            boolean r0 = r0.A02()     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x00b2
        L_0x0051:
            java.util.Set r0 = r6.A02     // Catch:{ all -> 0x00b4 }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 != 0) goto L_0x00b2
            com.facebook.messaging.model.send.PendingSendQueueKey r1 = new com.facebook.messaging.model.send.PendingSendQueueKey     // Catch:{ all -> 0x00b4 }
            X.1Hc r0 = X.C21471Hc.VIDEO     // Catch:{ all -> 0x00b4 }
            r1.<init>(r4, r0)     // Catch:{ all -> 0x00b4 }
            X.1HZ r0 = r6.A01     // Catch:{ all -> 0x00b4 }
            X.1v2 r0 = r0.A02(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x006e
            boolean r0 = r0.A02()     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x00b2
        L_0x006e:
            java.util.Set r0 = r6.A02     // Catch:{ all -> 0x00b4 }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 != 0) goto L_0x00b2
            com.facebook.messaging.model.send.PendingSendQueueKey r1 = new com.facebook.messaging.model.send.PendingSendQueueKey     // Catch:{ all -> 0x00b4 }
            X.1Hc r0 = X.C21471Hc.PHOTO     // Catch:{ all -> 0x00b4 }
            r1.<init>(r4, r0)     // Catch:{ all -> 0x00b4 }
            X.1HZ r0 = r6.A01     // Catch:{ all -> 0x00b4 }
            X.1v2 r0 = r0.A02(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x008b
            boolean r0 = r0.A02()     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x00b2
        L_0x008b:
            java.util.Set r0 = r6.A02     // Catch:{ all -> 0x00b4 }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 != 0) goto L_0x00b2
            com.facebook.messaging.model.send.PendingSendQueueKey r1 = new com.facebook.messaging.model.send.PendingSendQueueKey     // Catch:{ all -> 0x00b4 }
            X.1Hc r0 = X.C21471Hc.LIGHT_MEDIA     // Catch:{ all -> 0x00b4 }
            r1.<init>(r4, r0)     // Catch:{ all -> 0x00b4 }
            X.1HZ r0 = r6.A01     // Catch:{ all -> 0x00b4 }
            X.1v2 r0 = r0.A02(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x00a8
            boolean r0 = r0.A02()     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x00b2
        L_0x00a8:
            java.util.Set r0 = r6.A02     // Catch:{ all -> 0x00b4 }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x00b4 }
            if (r0 != 0) goto L_0x00b2
            monitor-exit(r2)     // Catch:{ all -> 0x00b4 }
            return r3
        L_0x00b2:
            monitor-exit(r2)     // Catch:{ all -> 0x00b4 }
            return r5
        L_0x00b4:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00b4 }
            throw r0
        L_0x00b7:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30151hZ.A01(com.facebook.messaging.model.threads.ThreadSummary):boolean");
    }

    private C30151hZ(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(3, r4);
        new AnonymousClass1HY(r4);
        this.A01 = new AnonymousClass1HZ((AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, this.A00));
        this.A02 = new HashSet();
    }
}
