package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0Pu  reason: invalid class name and case insensitive filesystem */
public final class C03830Pu extends C03160Kg {
    public long A00() {
        return -2269842438411178483L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        AnonymousClass0FZ r32 = (AnonymousClass0FZ) r3;
        dataOutput.writeFloat(r32.batteryLevelPct);
        dataOutput.writeLong(r32.batteryRealtimeMs);
        dataOutput.writeLong(r32.chargingRealtimeMs);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        AnonymousClass0FZ r32 = (AnonymousClass0FZ) r3;
        r32.batteryLevelPct = dataInput.readFloat();
        r32.batteryRealtimeMs = dataInput.readLong();
        r32.chargingRealtimeMs = dataInput.readLong();
        return true;
    }
}
