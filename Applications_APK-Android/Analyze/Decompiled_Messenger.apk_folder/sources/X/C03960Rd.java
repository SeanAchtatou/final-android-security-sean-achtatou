package X;

import com.facebook.common.dextricks.stats.ClassLoadingStats;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0Rd  reason: invalid class name and case insensitive filesystem */
public final class C03960Rd extends ClassLoadingStats {
    private final AtomicInteger A00 = new AtomicInteger();
    private final AtomicInteger A01 = new AtomicInteger();
    private final AtomicInteger A02 = new AtomicInteger();
    private final AtomicInteger A03 = new AtomicInteger();
    private final AtomicInteger A04 = new AtomicInteger();
    private final AtomicInteger A05 = new AtomicInteger();
    private final AtomicInteger A06 = new AtomicInteger();
    private final AtomicInteger A07 = new AtomicInteger();

    public int getLocatorAssistedClassLoads() {
        return 0;
    }

    public void decrementDexFileQueries() {
        this.A02.decrementAndGet();
    }

    public int getClassLoadsAttempted() {
        return this.A00.get();
    }

    public int getClassLoadsFailed() {
        return this.A01.get();
    }

    public int getDexFileQueries() {
        return this.A02.get();
    }

    public int getIncorrectDfaGuesses() {
        return this.A03.get();
    }

    public int getTurboLoaderClassLocationFailures() {
        return this.A04.get();
    }

    public int getTurboLoaderClassLocationSuccesses() {
        return this.A05.get();
    }

    public int getTurboLoaderMapGenerationFailures() {
        return this.A06.get();
    }

    public int getTurboLoaderMapGenerationSuccesses() {
        return this.A07.get();
    }

    public void incrementClassLoadsAttempted() {
        this.A00.incrementAndGet();
    }

    public void incrementClassLoadsFailed() {
        this.A01.incrementAndGet();
    }

    public void incrementDexFileQueries(int i) {
        this.A02.addAndGet(i);
    }

    public void incrementIncorrectDfaGuesses() {
        this.A03.incrementAndGet();
    }

    public void incrementTurboLoaderMapGenerationFailures() {
        this.A06.incrementAndGet();
    }

    public void incrementTurboLoaderMapGenerationSuccesses() {
        this.A07.incrementAndGet();
    }
}
