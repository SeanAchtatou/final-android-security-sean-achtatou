package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.09Z  reason: invalid class name */
public final class AnonymousClass09Z {
    private static volatile AnonymousClass09Z A01;
    private final C04310Tq A00;

    public static final AnonymousClass09Z A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass09Z.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass09Z(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C04310Tq A01(AnonymousClass1XY r1) {
        return AnonymousClass0VG.A00(AnonymousClass1Y3.AaF, r1);
    }

    public AnonymousClass0QR A02() {
        return (AnonymousClass0QR) this.A00.get();
    }

    private AnonymousClass09Z(AnonymousClass1XY r2) {
        this.A00 = A01(r2);
    }
}
