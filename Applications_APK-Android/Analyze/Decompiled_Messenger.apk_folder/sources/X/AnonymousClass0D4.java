package X;

import android.util.JsonWriter;
import io.card.payment.BuildConfig;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/* renamed from: X.0D4  reason: invalid class name */
public final class AnonymousClass0D4 {
    public static String A00(Throwable th) {
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            A03(jsonWriter, th);
            jsonWriter.close();
            return stringWriter.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static String A01(Throwable th) {
        if (th == null) {
            return BuildConfig.FLAVOR;
        }
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public static Throwable A02(Throwable th, Class cls) {
        while (th != null) {
            if (cls.isInstance(th)) {
                return th;
            }
            th = th.getCause();
        }
        return null;
    }

    private static void A03(JsonWriter jsonWriter, Throwable th) {
        jsonWriter.beginObject();
        jsonWriter.name("excls").value(th.getClass().getName());
        jsonWriter.name("msg").value(th.getMessage());
        jsonWriter.name("stack");
        jsonWriter.beginArray();
        StackTraceElement[] stackTrace = th.getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            jsonWriter.beginObject();
            jsonWriter.name("cls").value(stackTraceElement.getClassName());
            jsonWriter.name("method").value(stackTraceElement.getMethodName());
            jsonWriter.name("ln").value((long) stackTraceElement.getLineNumber());
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
        Throwable cause = th.getCause();
        if (cause != null) {
            jsonWriter.name("cause");
            A03(jsonWriter, cause);
        }
        jsonWriter.endObject();
    }
}
