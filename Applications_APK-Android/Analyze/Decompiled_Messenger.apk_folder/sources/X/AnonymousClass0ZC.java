package X;

import android.os.Build;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.Random;
import java.util.Set;

/* renamed from: X.0ZC  reason: invalid class name */
public final class AnonymousClass0ZC implements AnonymousClass0Z1 {
    public final ImmutableList A00;
    public final Random A01 = AnonymousClass0W9.A01();
    private final String A02 = "ClassInstanceCountReportDataSupplier";

    public String Amj() {
        return "class_instances";
    }

    public static final AnonymousClass0ZC A00(AnonymousClass1XY r3) {
        return new AnonymousClass0ZC(new AnonymousClass0X5(r3, AnonymousClass0X6.A0X));
    }

    public String getCustomData(Throwable th) {
        AnonymousClass0ZJ r0;
        if (!(th instanceof OutOfMemoryError)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int size = this.A00.size();
        if (size == 0) {
            r0 = null;
        } else {
            r0 = (AnonymousClass0ZJ) this.A00.get(this.A01.nextInt(size));
        }
        if (r0 != null && Build.VERSION.SDK_INT <= 27) {
            for (Class cls : r0.AhG()) {
                try {
                    long A002 = C179708Sf.A00(cls);
                    sb.append(cls.getSimpleName());
                    sb.append("=");
                    sb.append(Long.toString(A002));
                    sb.append("\n");
                } catch (Exception e) {
                    C010708t.A0L(this.A02, "Exception thrown while counting instances", e);
                }
            }
        }
        return sb.toString();
    }

    private AnonymousClass0ZC(Set set) {
        this.A00 = ImmutableList.copyOf((Collection) set);
    }
}
