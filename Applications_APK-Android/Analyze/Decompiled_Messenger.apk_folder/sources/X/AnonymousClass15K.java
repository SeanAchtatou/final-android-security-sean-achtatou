package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import java.io.File;
import java.util.Stack;

/* renamed from: X.15K  reason: invalid class name */
public final class AnonymousClass15K extends Handler {
    public AnonymousClass962 A00;
    public AnonymousClass969 A01;
    public Stack A02;
    public final Object A03 = new Object();
    public final Object A04 = new Object();
    private final C07250d5 A05;
    private final Integer A06;
    public final /* synthetic */ AnonymousClass15B A07;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass15K(AnonymousClass15B r2, Looper looper, C07250d5 r4, Integer num) {
        super(looper);
        this.A07 = r2;
        this.A05 = r4;
        this.A06 = num;
    }

    private C1936895x A00() {
        AnonymousClass15B r3 = this.A07;
        if (r3.A00 == null) {
            AnonymousClass15I r0 = r3.A02;
            AnonymousClass35W A002 = AnonymousClass35W.A00(!AnonymousClass96C.A00(r0.A01).A01());
            Context context = r0.A01;
            File file = new File(context.getDir("analytics", 0), r0.A09);
            int B3S = r0.A04.B3S();
            int AoP = r0.A04.AoP();
            int AfE = r0.A04.AfE();
            C06410bS r10 = r0.A02;
            C06390bQ r11 = r0.A07;
            String A003 = AnonymousClass0XR.A00();
            if (A003 == null) {
                A003 = "unknown";
            }
            r3.A00 = new C1936895x(new C187198mh(B3S, AoP, AfE, r10, r11, new File(file, A003), A002), new C29587Edj(r0.A01, r0.A00, new C631334v(file, r0.A03, 20000), r0.A06, r0.A08, r0.A05.AnO(), r0.A05.Ae0()));
        }
        return this.A07.A00;
    }

    private C1936895x A01() {
        AnonymousClass15B r5 = this.A07;
        if (r5.A01 == null) {
            AnonymousClass15I r6 = r5.A02;
            r5.A01 = new C1936895x(new C187208mi(r6.A04.B3S(), r6.A04.AoP(), r6.A04.AfE(), r6.A02, r6.A07), new AnonymousClass2FV(r6.A01, r6.A07, r6.A03));
            C1936895x r0 = this.A07.A01;
            r0.A00.A06(this.A00);
        }
        return this.A07.A01;
    }

    private void A02() {
        if (this.A05 != null) {
            AnonymousClass06K.A01("doWaitForWriteBlockRelease", 1259611385);
            this.A05.AQQ(this.A06);
            AnonymousClass06K.A00(-1418642414);
        }
    }

    public static void A03(AnonymousClass15K r2) {
        if (r2.A07.A06.AlF()) {
            synchronized (r2.A04) {
                r2.A01 = null;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:9|10|11|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r1 = A01();
        r1.A00.A07(r4);
        X.C1936895x.A00(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        X.AnonymousClass06K.A00(1211363381);
        r0 = r3.A07.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0043, code lost:
        if (r0 == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        throw new java.lang.RuntimeException(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0059, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        X.AnonymousClass06K.A00(-586632257);
        r0 = r3.A07.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r0 != null) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0066, code lost:
        r0.BY7(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0069, code lost:
        r4.A06();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006c, code lost:
        throw r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x002d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A04(X.C12240os r4) {
        /*
            r3 = this;
            java.lang.String r1 = "doWrite"
            r0 = 615948890(0x24b6a25a, float:7.920495E-17)
            X.AnonymousClass06K.A01(r1, r0)
            r3.A05(r4)     // Catch:{ all -> 0x006d }
            java.lang.String r1 = "writeToDisk"
            r0 = 486203295(0x1cfadf9f, float:1.6601417E-21)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ all -> 0x006d }
            r2 = 1
            X.95x r1 = r3.A00()     // Catch:{ 1wI | IOException -> 0x002d }
            X.95r r0 = r1.A00     // Catch:{ 1wI | IOException -> 0x002d }
            r0.A07(r4)     // Catch:{ 1wI | IOException -> 0x002d }
            X.C1936895x.A00(r1)     // Catch:{ 1wI | IOException -> 0x002d }
            r0 = 1058854323(0x3f1cd5b3, float:0.6126358)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x006d }
            X.15B r0 = r3.A07     // Catch:{ all -> 0x006d }
            X.0dB r0 = r0.A03     // Catch:{ all -> 0x006d }
            if (r0 == 0) goto L_0x0048
            goto L_0x0045
        L_0x002d:
            X.95x r1 = r3.A01()     // Catch:{ IOException -> 0x0052 }
            X.95r r0 = r1.A00     // Catch:{ IOException -> 0x0052 }
            r0.A07(r4)     // Catch:{ IOException -> 0x0052 }
            X.C1936895x.A00(r1)     // Catch:{ IOException -> 0x0052 }
            r0 = 1211363381(0x4833f035, float:184256.83)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x006d }
            X.15B r0 = r3.A07     // Catch:{ all -> 0x006d }
            X.0dB r0 = r0.A03     // Catch:{ all -> 0x006d }
            if (r0 == 0) goto L_0x0048
        L_0x0045:
            r0.BY7(r2)     // Catch:{ all -> 0x006d }
        L_0x0048:
            r4.A06()     // Catch:{ all -> 0x006d }
            r0 = 963837048(0x3972fc78, float:2.317297E-4)
            X.AnonymousClass06K.A00(r0)
            return
        L_0x0052:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0059 }
            r0.<init>(r1)     // Catch:{ all -> 0x0059 }
            throw r0     // Catch:{ all -> 0x0059 }
        L_0x0059:
            r1 = move-exception
            r0 = -586632257(0xffffffffdd08b3bf, float:-6.1565168E17)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x006d }
            X.15B r0 = r3.A07     // Catch:{ all -> 0x006d }
            X.0dB r0 = r0.A03     // Catch:{ all -> 0x006d }
            if (r0 == 0) goto L_0x0069
            r0.BY7(r2)     // Catch:{ all -> 0x006d }
        L_0x0069:
            r4.A06()     // Catch:{ all -> 0x006d }
            throw r1     // Catch:{ all -> 0x006d }
        L_0x006d:
            r1 = move-exception
            r0 = -1180268347(0xffffffffb9a688c5, float:-3.176389E-4)
            X.AnonymousClass06K.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass15K.A04(X.0os):void");
    }

    private void A05(C12240os r3) {
        if (this.A07.A03 != null) {
            AnonymousClass06K.A01("eventListener", -438023968);
            try {
                C07290dB r1 = this.A07.A03;
                if (r1 instanceof C07280dA) {
                    ((C07280dA) r1).A01(r3);
                } else {
                    r1.BY5();
                }
            } finally {
                AnonymousClass06K.A00(281970594);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        A01().A01(r3.A04, r3.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:?, code lost:
        r1 = r5.A07.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0182, code lost:
        if (r1 != null) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0184, code lost:
        r1.BY7(r3.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0189, code lost:
        r3.A01();
        X.AnonymousClass06K.A00(1840693563);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x019f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01a5, code lost:
        throw new java.lang.RuntimeException(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01a6, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:?, code lost:
        r1 = r5.A07.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01ab, code lost:
        if (r1 != null) goto L_0x01ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01ad, code lost:
        r1.BY7(r3.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01b2, code lost:
        r3.A01();
        X.AnonymousClass06K.A00(-275378790);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01bb, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        X.AnonymousClass06K.A01("handleAsapMessage", 339483972);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x01db, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x01dc, code lost:
        X.AnonymousClass06K.A00(158592914);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x01e2, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        if (r3 != false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        A02();
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
        A04(r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:99:0x0173 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r6) {
        /*
            r5 = this;
            X.15B r0 = r5.A07
            X.0bJ r0 = r0.A06
            boolean r0 = r0.AlE()
            r3 = 0
            if (r0 != 0) goto L_0x0014
        L_0x000b:
            java.lang.String r1 = "handleMessage"
            r0 = 682613647(0x28afdb8f, float:1.9524121E-14)
            X.AnonymousClass06K.A01(r1, r0)
            goto L_0x0044
        L_0x0014:
            java.lang.Object r1 = r5.A03
            monitor-enter(r1)
            java.util.Stack r0 = r5.A02     // Catch:{ all -> 0x01e3 }
            if (r0 == 0) goto L_0x0042
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x01e3 }
            if (r0 != 0) goto L_0x0042
            java.util.Stack r0 = r5.A02     // Catch:{ all -> 0x01e3 }
            java.lang.Object r2 = r0.pop()     // Catch:{ all -> 0x01e3 }
            X.0os r2 = (X.C12240os) r2     // Catch:{ all -> 0x01e3 }
            monitor-exit(r1)     // Catch:{ all -> 0x01e3 }
            r1 = 339483972(0x143c1d44, float:9.497346E-27)
            java.lang.String r0 = "handleAsapMessage"
            X.AnonymousClass06K.A01(r0, r1)
            if (r3 != 0) goto L_0x0038
            r5.A02()     // Catch:{ all -> 0x01db }
            r3 = 1
        L_0x0038:
            r5.A04(r2)     // Catch:{ all -> 0x01db }
            r0 = -312757312(0xffffffffed5bb3c0, float:-4.2496576E27)
            X.AnonymousClass06K.A00(r0)
            goto L_0x0014
        L_0x0042:
            monitor-exit(r1)     // Catch:{ all -> 0x01e3 }
            goto L_0x000b
        L_0x0044:
            int r2 = r6.what     // Catch:{ all -> 0x01d3 }
            r0 = 1
            if (r2 == r0) goto L_0x010e
            r0 = 2
            if (r2 == r0) goto L_0x00ca
            r0 = 3
            if (r2 == r0) goto L_0x0095
            r0 = 4
            if (r2 == r0) goto L_0x0064
            r0 = 7
            if (r2 == r0) goto L_0x005b
            r0 = 8
            if (r2 == r0) goto L_0x0198
            goto L_0x01c7
        L_0x005b:
            java.lang.Object r0 = r6.obj     // Catch:{ all -> 0x01d3 }
            android.os.ConditionVariable r0 = (android.os.ConditionVariable) r0     // Catch:{ all -> 0x01d3 }
            r0.open()     // Catch:{ all -> 0x01d3 }
            goto L_0x0198
        L_0x0064:
            java.lang.Object r2 = r6.obj     // Catch:{ all -> 0x01d3 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x01d3 }
            java.lang.String r1 = "doUserLogout"
            r0 = 469281581(0x1bf8ab2d, float:4.1138778E-22)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ all -> 0x01d3 }
            X.95x r0 = r5.A00()     // Catch:{ all -> 0x008c }
            X.961 r0 = r0.A01     // Catch:{ all -> 0x008c }
            r0.BMf(r2)     // Catch:{ all -> 0x008c }
            X.15B r0 = r5.A07     // Catch:{ all -> 0x008c }
            X.95x r0 = r0.A01     // Catch:{ all -> 0x008c }
            if (r0 == 0) goto L_0x0084
            X.961 r0 = r0.A01     // Catch:{ all -> 0x008c }
            r0.BMf(r2)     // Catch:{ all -> 0x008c }
        L_0x0084:
            r0 = 1611548347(0x600e46bb, float:4.100835E19)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x0198
        L_0x008c:
            r1 = move-exception
            r0 = 1404435460(0x53b5fc04, float:1.5632344E12)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x01d2
        L_0x0095:
            java.lang.Object r2 = r6.obj     // Catch:{ all -> 0x01d3 }
            X.962 r2 = (X.AnonymousClass962) r2     // Catch:{ all -> 0x01d3 }
            java.lang.String r1 = "doStartNewSession"
            r0 = -1273364217(0xffffffffb41a0107, float:-1.4342741E-7)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ all -> 0x01d3 }
            r5.A00 = r2     // Catch:{ all -> 0x00c1 }
            X.95x r0 = r5.A00()     // Catch:{ all -> 0x00c1 }
            X.95r r0 = r0.A00     // Catch:{ all -> 0x00c1 }
            r0.A06(r2)     // Catch:{ all -> 0x00c1 }
            X.15B r0 = r5.A07     // Catch:{ all -> 0x00c1 }
            X.95x r0 = r0.A01     // Catch:{ all -> 0x00c1 }
            if (r0 == 0) goto L_0x00b9
            X.962 r1 = r5.A00     // Catch:{ all -> 0x00c1 }
            X.95r r0 = r0.A00     // Catch:{ all -> 0x00c1 }
            r0.A06(r1)     // Catch:{ all -> 0x00c1 }
        L_0x00b9:
            r0 = 1007262706(0x3c099bf2, float:0.008398997)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x0198
        L_0x00c1:
            r1 = move-exception
            r0 = 803080739(0x2fde0a23, float:4.0388767E-10)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x01d2
        L_0x00ca:
            if (r3 != 0) goto L_0x00cf
            r5.A02()     // Catch:{ all -> 0x01d3 }
        L_0x00cf:
            java.lang.Object r2 = r6.obj     // Catch:{ all -> 0x01d3 }
            X.1gn r2 = (X.C29671gn) r2     // Catch:{ all -> 0x01d3 }
            java.lang.String r1 = "doBootstrapNewSession"
            r0 = 26794497(0x198da01, float:5.61488E-38)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ all -> 0x01d3 }
            X.15B r0 = r5.A07     // Catch:{ all -> 0x0105 }
            X.0bR r0 = r0.A05     // Catch:{ all -> 0x0105 }
            X.962 r0 = r2.A03(r0)     // Catch:{ all -> 0x0105 }
            r5.A00 = r0     // Catch:{ all -> 0x0105 }
            X.95x r0 = r5.A00()     // Catch:{ all -> 0x0105 }
            X.962 r1 = r5.A00     // Catch:{ all -> 0x0105 }
            X.95r r0 = r0.A00     // Catch:{ all -> 0x0105 }
            r0.A06(r1)     // Catch:{ all -> 0x0105 }
            X.15B r0 = r5.A07     // Catch:{ all -> 0x0105 }
            X.95x r0 = r0.A01     // Catch:{ all -> 0x0105 }
            if (r0 == 0) goto L_0x00fd
            X.962 r1 = r5.A00     // Catch:{ all -> 0x0105 }
            X.95r r0 = r0.A00     // Catch:{ all -> 0x0105 }
            r0.A06(r1)     // Catch:{ all -> 0x0105 }
        L_0x00fd:
            r0 = -466731135(0xffffffffe42e3f81, float:-1.2857238E22)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x0198
        L_0x0105:
            r1 = move-exception
            r0 = 928587577(0x37591f39, float:1.29414775E-5)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x01d2
        L_0x010e:
            if (r3 != 0) goto L_0x0113
            r5.A02()     // Catch:{ all -> 0x01d3 }
        L_0x0113:
            int r1 = r6.arg1     // Catch:{ all -> 0x01d3 }
            r0 = 2
            if (r1 == r0) goto L_0x0120
            java.lang.Object r0 = r6.obj     // Catch:{ all -> 0x01d3 }
            X.0os r0 = (X.C12240os) r0     // Catch:{ all -> 0x01d3 }
            r5.A04(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x0198
        L_0x0120:
            java.lang.Object r3 = r6.obj     // Catch:{ all -> 0x01d3 }
            X.969 r3 = (X.AnonymousClass969) r3     // Catch:{ all -> 0x01d3 }
            java.lang.String r1 = "doWrites"
            r0 = 824490405(0x3124b9a5, float:2.3970668E-9)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ all -> 0x01d3 }
            java.lang.Object r1 = r5.A04     // Catch:{ all -> 0x01bf }
            monitor-enter(r1)     // Catch:{ all -> 0x01bf }
            r0 = 1
            r3.A03 = r0     // Catch:{ all -> 0x01bc }
            X.969 r0 = r5.A01     // Catch:{ all -> 0x01bc }
            if (r0 != r3) goto L_0x0139
            r0 = 0
            r5.A01 = r0     // Catch:{ all -> 0x01bc }
        L_0x0139:
            monitor-exit(r1)     // Catch:{ all -> 0x01bc }
            r1 = 0
        L_0x013b:
            int r0 = r3.A01     // Catch:{ all -> 0x01bf }
            if (r1 >= r0) goto L_0x014b
            X.0os[] r0 = r3.A04     // Catch:{ all -> 0x01bf }
            r0 = r0[r1]     // Catch:{ all -> 0x01bf }
            if (r0 == 0) goto L_0x0148
            r5.A05(r0)     // Catch:{ all -> 0x01bf }
        L_0x0148:
            int r1 = r1 + 1
            goto L_0x013b
        L_0x014b:
            java.lang.String r1 = "writeToDisk"
            r0 = -541114069(0xffffffffdfbf412b, float:-2.7562687E19)
            X.AnonymousClass06K.A01(r1, r0)     // Catch:{ all -> 0x01bf }
            X.95x r2 = r5.A00()     // Catch:{ 1wI | IOException -> 0x0173 }
            X.0os[] r1 = r3.A04     // Catch:{ 1wI | IOException -> 0x0173 }
            int r0 = r3.A01     // Catch:{ 1wI | IOException -> 0x0173 }
            r2.A01(r1, r0)     // Catch:{ 1wI | IOException -> 0x0173 }
            X.15B r0 = r5.A07     // Catch:{ all -> 0x01bf }
            X.0dB r1 = r0.A03     // Catch:{ all -> 0x01bf }
            if (r1 == 0) goto L_0x0169
            int r0 = r3.A01     // Catch:{ all -> 0x01bf }
            r1.BY7(r0)     // Catch:{ all -> 0x01bf }
        L_0x0169:
            r3.A01()     // Catch:{ all -> 0x01bf }
            r0 = -1773078818(0xffffffff9650f6de, float:-1.6880004E-25)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01bf }
            goto L_0x0192
        L_0x0173:
            X.95x r2 = r5.A01()     // Catch:{ IOException -> 0x019f }
            X.0os[] r1 = r3.A04     // Catch:{ IOException -> 0x019f }
            int r0 = r3.A01     // Catch:{ IOException -> 0x019f }
            r2.A01(r1, r0)     // Catch:{ IOException -> 0x019f }
            X.15B r0 = r5.A07     // Catch:{ all -> 0x01bf }
            X.0dB r1 = r0.A03     // Catch:{ all -> 0x01bf }
            if (r1 == 0) goto L_0x0189
            int r0 = r3.A01     // Catch:{ all -> 0x01bf }
            r1.BY7(r0)     // Catch:{ all -> 0x01bf }
        L_0x0189:
            r3.A01()     // Catch:{ all -> 0x01bf }
            r0 = 1840693563(0x6db6c13b, float:7.069984E27)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01bf }
        L_0x0192:
            r0 = 1008913584(0x3c22ccb0, float:0.009936497)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
        L_0x0198:
            r0 = -885090897(0xffffffffcb3e95af, float:-1.2490159E7)
            X.AnonymousClass06K.A00(r0)
            return
        L_0x019f:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x01a6 }
            r0.<init>(r1)     // Catch:{ all -> 0x01a6 }
            throw r0     // Catch:{ all -> 0x01a6 }
        L_0x01a6:
            r2 = move-exception
            X.15B r0 = r5.A07     // Catch:{ all -> 0x01bf }
            X.0dB r1 = r0.A03     // Catch:{ all -> 0x01bf }
            if (r1 == 0) goto L_0x01b2
            int r0 = r3.A01     // Catch:{ all -> 0x01bf }
            r1.BY7(r0)     // Catch:{ all -> 0x01bf }
        L_0x01b2:
            r3.A01()     // Catch:{ all -> 0x01bf }
            r0 = -275378790(0xffffffffef960d9a, float:-9.287839E28)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01bf }
            throw r2     // Catch:{ all -> 0x01bf }
        L_0x01bc:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01bc }
            throw r0     // Catch:{ all -> 0x01bf }
        L_0x01bf:
            r1 = move-exception
            r0 = 1945364963(0x73f3e9e3, float:3.8649656E31)
            X.AnonymousClass06K.A00(r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x01d2
        L_0x01c7:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x01d3 }
            java.lang.String r0 = "Unknown what="
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r2)     // Catch:{ all -> 0x01d3 }
            r1.<init>(r0)     // Catch:{ all -> 0x01d3 }
        L_0x01d2:
            throw r1     // Catch:{ all -> 0x01d3 }
        L_0x01d3:
            r1 = move-exception
            r0 = 172747335(0xa4bea47, float:9.818157E-33)
            X.AnonymousClass06K.A00(r0)
            throw r1
        L_0x01db:
            r1 = move-exception
            r0 = 158592914(0x973ef92, float:2.9362706E-33)
            X.AnonymousClass06K.A00(r0)
            throw r1
        L_0x01e3:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x01e3 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass15K.handleMessage(android.os.Message):void");
    }
}
