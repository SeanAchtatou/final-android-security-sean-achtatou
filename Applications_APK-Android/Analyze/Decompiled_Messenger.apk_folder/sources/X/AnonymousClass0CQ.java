package X;

import io.card.payment.BuildConfig;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0CQ  reason: invalid class name */
public final class AnonymousClass0CQ {
    public int A00;
    public String A01 = BuildConfig.FLAVOR;
    public String A02 = BuildConfig.FLAVOR;
    public String A03 = BuildConfig.FLAVOR;
    public String A04 = BuildConfig.FLAVOR;
    public String A05 = BuildConfig.FLAVOR;

    public static AnonymousClass0CQ A00(String str) {
        AnonymousClass0CQ r3 = new AnonymousClass0CQ();
        if (str == null || str.isEmpty()) {
            return r3;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            r3.A01 = jSONObject.optString("ck");
            r3.A02 = jSONObject.optString("cs");
            r3.A00 = jSONObject.optInt("sr", 0);
            r3.A03 = jSONObject.optString("di");
            r3.A04 = jSONObject.optString("ds");
            r3.A05 = jSONObject.optString("rc");
            return r3;
        } catch (JSONException unused) {
            return new AnonymousClass0CQ();
        }
    }

    public String toString() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("ck", this.A01);
            jSONObject.putOpt("cs", this.A02);
            jSONObject.putOpt("di", this.A03);
            jSONObject.putOpt("ds", this.A04);
            jSONObject.put("sr", this.A00);
            jSONObject.putOpt("rc", this.A05);
            return jSONObject.toString();
        } catch (JSONException e) {
            C010708t.A0S("ConnAckPayload", e, "failed to serialize");
            return BuildConfig.FLAVOR;
        }
    }
}
