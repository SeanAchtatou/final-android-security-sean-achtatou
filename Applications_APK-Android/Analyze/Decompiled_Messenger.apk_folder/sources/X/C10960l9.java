package X;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Build;
import android.os.Handler;
import com.facebook.common.perftest.PerfTestConfig;
import com.facebook.common.perftest.base.PerfTestConfigBase;
import com.facebook.common.util.TriState;
import com.facebook.messaging.accountswitch.model.MessengerAccountInfo;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.base.Platform;
import com.google.common.collect.ImmutableSet;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0l9  reason: invalid class name and case insensitive filesystem */
public final class C10960l9 {
    public static final boolean A0B;
    private static volatile C10960l9 A0C;
    public ContentObserver A00;
    public AnonymousClass0UN A01;
    private TriState A02 = TriState.UNSET;
    public final Context A03;
    public final Handler A04;
    public final C001500z A05;
    public final C27061ca A06;
    public final FbSharedPreferences A07;
    public final Object A08 = new Object();
    public final C04310Tq A09;
    private final C10970lA A0A;

    public boolean A08() {
        return A0D(false);
    }

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 19) {
            z = true;
        }
        A0B = z;
    }

    public static final C10960l9 A01(AnonymousClass1XY r4) {
        if (A0C == null) {
            synchronized (C10960l9.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r4);
                if (A002 != null) {
                    try {
                        A0C = new C10960l9(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    public static boolean A02(C10960l9 r3) {
        if (!A0B || r3.A0A.A00.AbO(264, false) || !r3.A07.Aep(C10990lD.A0I, false)) {
            return false;
        }
        return true;
    }

    public void A06() {
        if (this.A07.BFQ() && !this.A07.BBh(C10990lD.A0W)) {
            int i = AnonymousClass1Y3.A7Q;
            if (((C27081cc) AnonymousClass1XX.A02(1, i, this.A01)).Aw9() >= 1) {
                boolean A022 = A02(this);
                String str = BuildConfig.FLAVOR;
                if (A022 || A08()) {
                    StringBuilder sb = new StringBuilder();
                    for (MessengerAccountInfo messengerAccountInfo : ((C27081cc) AnonymousClass1XX.A02(1, i, this.A01)).Abd()) {
                        String str2 = messengerAccountInfo.A04;
                        if (!Platform.stringIsNullOrEmpty(str2)) {
                            sb.append(str);
                            sb.append(str2);
                            str = " ";
                        }
                    }
                    str = sb.toString();
                }
                C30281hn edit = this.A07.edit();
                edit.BzC(C10990lD.A0W, str);
                edit.commit();
            }
        }
    }

    public void A07() {
        synchronized (this.A08) {
            this.A02 = TriState.UNSET;
        }
        AnonymousClass00S.A04(this.A04, new C55952p1(this), -715986828);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004e, code lost:
        if (r4.A02 == com.facebook.common.util.TriState.UNSET) goto L_0x0050;
     */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0076 A[Catch:{ all -> 0x006b, all -> 0x0085 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0079 A[Catch:{ all -> 0x006b, all -> 0x0085 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0D(boolean r5) {
        /*
            r4 = this;
            boolean r0 = X.C10960l9.A0B
            if (r0 == 0) goto L_0x0036
            X.00z r1 = X.C001500z.A07
            X.00z r0 = r4.A05
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0036
            android.database.ContentObserver r0 = r4.A00
            if (r0 != 0) goto L_0x0036
            java.lang.Object r2 = r4.A08
            monitor-enter(r2)
            android.database.ContentObserver r0 = r4.A00     // Catch:{ all -> 0x0026 }
            if (r0 == 0) goto L_0x001b
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            goto L_0x0036
        L_0x001b:
            X.1ci r1 = new X.1ci     // Catch:{ all -> 0x0026 }
            android.os.Handler r0 = r4.A04     // Catch:{ all -> 0x0026 }
            r1.<init>(r4, r0)     // Catch:{ all -> 0x0026 }
            r4.A00 = r1     // Catch:{ all -> 0x0026 }
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            goto L_0x0029
        L_0x0026:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0026 }
            goto L_0x0087
        L_0x0029:
            android.os.Handler r2 = r4.A04
            X.1cj r1 = new X.1cj
            r1.<init>(r4)
            r0 = 1179922269(0x46542f5d, float:13579.841)
            X.AnonymousClass00S.A04(r2, r1, r0)
        L_0x0036:
            com.facebook.common.util.TriState r0 = r4.A02
            java.lang.Boolean r0 = r0.asBooleanObject()
            if (r5 != 0) goto L_0x0045
            if (r0 == 0) goto L_0x0045
            boolean r0 = r0.booleanValue()
            return r0
        L_0x0045:
            java.lang.Object r3 = r4.A08
            monitor-enter(r3)
            if (r5 != 0) goto L_0x0050
            com.facebook.common.util.TriState r1 = r4.A02     // Catch:{ all -> 0x0085 }
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET     // Catch:{ all -> 0x0085 }
            if (r1 != r0) goto L_0x007d
        L_0x0050:
            r2 = 0
            boolean r0 = X.C10960l9.A0B     // Catch:{ all -> 0x006b }
            if (r0 == 0) goto L_0x0073
            android.content.Context r0 = r4.A03     // Catch:{ all -> 0x006b }
            java.lang.String r1 = android.provider.Telephony.Sms.getDefaultSmsPackage(r0)     // Catch:{ all -> 0x006b }
            if (r1 == 0) goto L_0x0074
            android.content.Context r0 = r4.A03     // Catch:{ all -> 0x006b }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ all -> 0x006b }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x006b }
            if (r0 == 0) goto L_0x0074
            r2 = 1
            goto L_0x0074
        L_0x006b:
            r2 = move-exception
            java.lang.String r1 = "SmsIntegrationState"
            java.lang.String r0 = "Exception in detecting sms default app"
            X.C010708t.A0T(r1, r2, r0)     // Catch:{ all -> 0x0085 }
        L_0x0073:
            r2 = 0
        L_0x0074:
            if (r2 == 0) goto L_0x0079
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.YES     // Catch:{ all -> 0x0085 }
            goto L_0x007b
        L_0x0079:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.NO     // Catch:{ all -> 0x0085 }
        L_0x007b:
            r4.A02 = r0     // Catch:{ all -> 0x0085 }
        L_0x007d:
            com.facebook.common.util.TriState r0 = r4.A02     // Catch:{ all -> 0x0085 }
            boolean r0 = r0.asBoolean()     // Catch:{ all -> 0x0085 }
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            return r0
        L_0x0085:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
        L_0x0087:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10960l9.A0D(boolean):boolean");
    }

    private C10960l9(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(2, r3);
        this.A03 = AnonymousClass1YA.A00(r3);
        this.A05 = AnonymousClass0UU.A05(r3);
        this.A04 = AnonymousClass0UX.A01(r3);
        this.A0A = C10970lA.A00(r3);
        this.A07 = FbSharedPreferencesModule.A00(r3);
        this.A09 = AnonymousClass0XJ.A0K(r3);
        this.A06 = C27061ca.A00(r3);
        PerfTestConfig.A01(r3);
    }

    public static final C10960l9 A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static boolean A03(C10960l9 r3) {
        r3.A06();
        if (((C27081cc) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A7Q, r3.A01)).Aw9() <= 1) {
            return false;
        }
        return true;
    }

    public ImmutableSet A04() {
        C07410dQ A012 = ImmutableSet.A01();
        if (A0B()) {
            A012.A01(C50452e3.BUSINESS);
        }
        return A012.build();
    }

    public Integer A05() {
        if (A08()) {
            return AnonymousClass07B.A0C;
        }
        if (A02(this)) {
            return AnonymousClass07B.A01;
        }
        return AnonymousClass07B.A00;
    }

    public boolean A09() {
        boolean z = false;
        if (A05() == AnonymousClass07B.A00 && this.A07.Aep(C10990lD.A08, false)) {
            z = true;
        }
        if (!z || this.A07.Aep(C10990lD.A0M, false)) {
            return false;
        }
        return true;
    }

    public boolean A0A() {
        if (!A03(this) || this.A06.A02((String) this.A09.get())) {
            return A02(this);
        }
        return false;
    }

    public boolean A0B() {
        if (PerfTestConfigBase.A00()) {
            return true;
        }
        if (A03(this) && !this.A06.A02((String) this.A09.get())) {
            return false;
        }
        if (A02(this) || A08()) {
            return true;
        }
        return false;
    }

    public boolean A0C(String str) {
        if (!A03(this)) {
            return false;
        }
        C27061ca r4 = this.A06;
        if (Platform.stringIsNullOrEmpty(str)) {
            return true;
        }
        String B4F = r4.A00.B4F(C10990lD.A0W, BuildConfig.FLAVOR);
        if (!Platform.stringIsNullOrEmpty(B4F)) {
            if (!B4F.matches(AnonymousClass08S.A0P("(\\s|^)", str, "(\\s|$)"))) {
                str = AnonymousClass08S.A0J(B4F, AnonymousClass08S.A0J(" ", str));
            } else {
                str = null;
            }
        }
        if (Platform.stringIsNullOrEmpty(str)) {
            return true;
        }
        C30281hn edit = r4.A00.edit();
        edit.BzC(C10990lD.A0W, str);
        edit.commit();
        return true;
    }
}
