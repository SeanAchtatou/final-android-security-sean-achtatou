package X;

import android.content.Context;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.0BI  reason: invalid class name */
public final class AnonymousClass0BI {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C01490Al A01;
    public final /* synthetic */ AnonymousClass0BD A02;
    public final /* synthetic */ AnonymousClass0AX A03;
    public final /* synthetic */ C01410Ac A04;
    public final /* synthetic */ AnonymousClass0BE A05;
    public final /* synthetic */ ExecutorService A06;
    public final /* synthetic */ ScheduledExecutorService A07;
    public final /* synthetic */ boolean A08;

    public AnonymousClass0BI(AnonymousClass0AX r1, C01410Ac r2, boolean z, AnonymousClass0BD r4, ScheduledExecutorService scheduledExecutorService, AnonymousClass0BE r6, Context context, C01490Al r8, ExecutorService executorService) {
        this.A03 = r1;
        this.A04 = r2;
        this.A08 = z;
        this.A02 = r4;
        this.A07 = scheduledExecutorService;
        this.A05 = r6;
        this.A00 = context;
        this.A01 = r8;
        this.A06 = executorService;
    }
}
