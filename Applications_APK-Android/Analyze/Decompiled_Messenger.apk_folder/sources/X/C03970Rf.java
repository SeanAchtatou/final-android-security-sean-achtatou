package X;

import android.content.Context;

/* renamed from: X.0Rf  reason: invalid class name and case insensitive filesystem */
public final class C03970Rf implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.orca.FbnsLiteInitializer$2";
    public final /* synthetic */ AnonymousClass0GZ A00;

    public C03970Rf(AnonymousClass0GZ r1) {
        this.A00 = r1;
    }

    public void run() {
        C005505z.A05("%s.init.run", "FbnsLiteInitializer", 1286377574);
        try {
            AnonymousClass0GZ r0 = this.A00;
            r0.A04.A02(r0.A08);
            AnonymousClass0GZ r1 = this.A00;
            r1.A00 = AnonymousClass8Y7.A00(r1.A03);
            this.A00.A08.run();
            AnonymousClass0GZ r02 = this.A00;
            Context context = r02.A03;
            boolean A09 = r02.A07.A09();
            AnonymousClass0A1.A00(context);
            Boolean valueOf = Boolean.valueOf(A09);
            C01760Bn.A00 = valueOf;
            context.getSharedPreferences("mqtt_stickiness_controller", 0).edit().putBoolean("mqtt_service_nonsticky", valueOf.booleanValue()).commit();
        } catch (Exception e) {
            C010708t.A0L("StickinessController", "Error updating Mqtt sticky bit in SP", e);
        } catch (Throwable th) {
            C005505z.A00(-2013494685);
            throw th;
        }
        C005505z.A00(1761341981);
    }
}
