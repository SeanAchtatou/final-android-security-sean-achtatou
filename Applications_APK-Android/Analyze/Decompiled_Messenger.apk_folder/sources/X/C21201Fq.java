package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.StandardTable;
import java.util.Map;

/* renamed from: X.1Fq  reason: invalid class name and case insensitive filesystem */
public final class C21201Fq extends C21101Ff<R, Map<C, V>> {
    public final /* synthetic */ StandardTable A00;

    public C21201Fq(StandardTable standardTable) {
        this.A00 = standardTable;
    }

    public boolean containsKey(Object obj) {
        boolean z;
        StandardTable standardTable = this.A00;
        if (obj != null) {
            Map map = standardTable.backingMap;
            Preconditions.checkNotNull(map);
            try {
                z = map.containsKey(obj);
            } catch (ClassCastException | NullPointerException unused) {
                z = false;
            }
            if (!z) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        if (r1 == false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get(java.lang.Object r3) {
        /*
            r2 = this;
            com.google.common.collect.StandardTable r0 = r2.A00
            if (r3 == 0) goto L_0x0012
            java.util.Map r0 = r0.backingMap
            com.google.common.base.Preconditions.checkNotNull(r0)
            boolean r1 = r0.containsKey(r3)     // Catch:{ ClassCastException | NullPointerException -> 0x000e }
            goto L_0x000f
        L_0x000e:
            r1 = 0
        L_0x000f:
            r0 = 1
            if (r1 != 0) goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            if (r0 == 0) goto L_0x001c
            com.google.common.collect.StandardTable r0 = r2.A00
            java.util.Map r0 = r0.A08(r3)
            return r0
        L_0x001c:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21201Fq.get(java.lang.Object):java.lang.Object");
    }

    public Object remove(Object obj) {
        if (obj == null) {
            return null;
        }
        return (Map) this.A00.backingMap.remove(obj);
    }
}
