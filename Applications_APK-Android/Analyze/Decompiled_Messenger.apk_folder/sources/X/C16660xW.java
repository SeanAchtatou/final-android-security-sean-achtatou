package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.common.banner.BasicBannerNotificationView;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.proxygen.LigerSamplePolicy;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0xW  reason: invalid class name and case insensitive filesystem */
public final class C16660xW extends C32501lr {
    public C06790c5 A00;
    public C06790c5 A01;
    public AnonymousClass0UN A02;
    public MigColorScheme A03;
    public Integer A04;
    public String A05 = "error";
    public boolean A06;
    public boolean A07;
    private BasicBannerNotificationView A08;
    private Integer A09 = AnonymousClass07B.A00;
    public final C04310Tq A0A;
    private final C11330mk A0B;
    private final C04310Tq A0C;

    public C16660xW(AnonymousClass1XY r4, Integer num) {
        super(null);
        this.A02 = new AnonymousClass0UN(14, r4);
        this.A0A = C04750Wa.A05(r4);
        this.A0C = AnonymousClass0VG.A00(AnonymousClass1Y3.A7P, r4);
        this.A04 = num;
        this.A0B = ((AnonymousClass1CU) AnonymousClass1XX.A02(13, AnonymousClass1Y3.AuL, this.A02)).A01("mqtt_instance");
    }

    public static void A00(C16660xW r3) {
        if (r3.A01 == null) {
            C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AKb, r3.A02)).BMm();
            BMm.A02("com.facebook.orca.CONNECTIVITY_CHANGED", new C13300rC(r3));
            r3.A01 = BMm.A00();
        }
        if (r3.A00 == null) {
            C06600bl BMm2 = ((C04460Ut) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ADa, r3.A02)).BMm();
            BMm2.A02("android.intent.action.AIRPLANE_MODE", new C13310rD(r3));
            r3.A00 = BMm2.A00();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x026f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C16660xW r18) {
        /*
            r2 = r18
            com.facebook.common.banner.BasicBannerNotificationView r0 = r2.A08
            if (r0 == 0) goto L_0x00e3
            int r1 = X.AnonymousClass1Y3.A11
            X.0UN r0 = r2.A02
            X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r2.A03
            if (r0 != 0) goto L_0x001d
            int r1 = X.AnonymousClass1Y3.Anh
            X.0UN r0 = r2.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A03(r1, r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = (com.facebook.mig.scheme.interfaces.MigColorScheme) r0
            r2.A03 = r0
        L_0x001d:
            int r1 = X.AnonymousClass1Y3.AxV
            X.0UN r0 = r2.A02
            r3 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1o0 r1 = (X.C33701o0) r1
            X.0Tq r0 = r2.A0C
            java.lang.Object r9 = r0.get()
            X.1o1 r9 = (X.C33711o1) r9
            java.lang.Integer r0 = r2.A09
            java.lang.String r10 = X.C55622oO.A00(r0)
            X.1o3 r0 = r1.AiE()
            int r0 = r0.ordinal()
            r4 = 10
            switch(r0) {
                case 1: goto L_0x00e4;
                case 2: goto L_0x0154;
                case 3: goto L_0x01af;
                case 4: goto L_0x01f9;
                default: goto L_0x0043;
            }
        L_0x0043:
            boolean r0 = r2.A04()
            if (r0 != 0) goto L_0x027c
            com.facebook.common.banner.BasicBannerNotificationView r5 = r2.A08
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r2.A03
            android.content.res.Resources r1 = r9.A00
            r0 = 2131823121(0x7f110a11, float:1.9279033E38)
            java.lang.String r0 = r1.getString(r0)
            X.2Se r4 = new X.2Se
            r4.<init>()
            r4.A07 = r0
            int r0 = X.C33711o1.A01(r6)
            r4.A02 = r0
            android.content.res.Resources r1 = r9.A00
            X.1I7 r0 = X.AnonymousClass1I7.SMALL
            int r0 = r0.B5j()
            float r0 = r1.getDimension(r0)
            r4.A00 = r0
            int r1 = X.C33711o1.A00(r6)
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r1)
            r4.A03 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r4.A08 = r0
            r4.A09 = r3
            r0 = 0
            r4.A01(r0)
            X.3AN r0 = r4.A00()
            r5.A0T(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            r2.A09 = r0
        L_0x0091:
            java.lang.Integer r4 = r2.A04
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            r0 = 0
            if (r4 != r1) goto L_0x0099
            r0 = 1
        L_0x0099:
            if (r0 == 0) goto L_0x00a0
            com.facebook.common.banner.BasicBannerNotificationView r0 = r2.A08
            X.C114425cC.A01(r0)
        L_0x00a0:
            java.lang.Integer r1 = r2.A04
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            if (r1 != r0) goto L_0x00e3
            java.lang.String r11 = "->"
            java.lang.Integer r0 = r2.A09
            java.lang.String r12 = X.C55622oO.A00(r0)
            java.lang.String r13 = "; show_reason="
            java.lang.String r14 = r2.A05
            java.lang.String r15 = "; monitor_status="
            int r1 = X.AnonymousClass1Y3.AxV
            X.0UN r0 = r2.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1o0 r0 = (X.C33701o0) r0
            X.1o3 r0 = r0.AiE()
            java.lang.String r16 = r0.name()
            java.lang.String r17 = "; was_connected="
            X.0UN r0 = r2.A02
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.1o0 r0 = (X.C33701o0) r0
            boolean r0 = r0.CN2()
            java.lang.String r18 = java.lang.String.valueOf(r0)
            java.lang.String r3 = X.AnonymousClass08S.A0W(r10, r11, r12, r13, r14, r15, r16, r17, r18)
            X.0mk r1 = r2.A0B
            java.lang.String r0 = "connectivity_banner"
            r1.BIp(r0, r3)
        L_0x00e3:
            return
        L_0x00e4:
            com.facebook.common.banner.BasicBannerNotificationView r4 = r2.A08
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r2.A03
            android.content.res.Resources r1 = r9.A00
            r0 = 2131823122(0x7f110a12, float:1.9279035E38)
            java.lang.String r8 = r1.getString(r0)
            android.content.res.Resources r1 = r9.A00
            r0 = 2131823123(0x7f110a13, float:1.9279037E38)
            java.lang.String r6 = r1.getString(r0)
            X.2Se r5 = new X.2Se
            r5.<init>()
            r5.A07 = r8
            r8 = 2132082826(0x7f15008a, float:1.9805777E38)
            android.content.res.Resources r1 = r9.A00
            r0 = 0
            int r0 = X.AnonymousClass3DO.A00(r1, r8, r0)
            r5.A02 = r0
            android.content.res.Resources r1 = r9.A00
            X.1I7 r0 = X.AnonymousClass1I7.SMALL
            int r0 = r0.B5j()
            float r0 = r1.getDimension(r0)
            r5.A00 = r0
            int r1 = X.C33711o1.A00(r7)
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r1)
            r5.A03 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r5.A08 = r0
            r5.A09 = r3
            r5.A01(r6)
            if (r7 == 0) goto L_0x014d
            int r0 = r7.AbV()
        L_0x0135:
            r5.A01 = r0
            X.3AN r0 = r5.A00()
            r4.A0T(r0)
            com.facebook.common.banner.BasicBannerNotificationView r1 = r2.A08
            X.91f r0 = new X.91f
            r0.<init>(r2)
            r1.A00 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            r2.A09 = r0
            goto L_0x0091
        L_0x014d:
            X.5d3 r0 = X.C114945d3.A01
            int r0 = r0.AhV()
            goto L_0x0135
        L_0x0154:
            boolean r0 = r2.A04()
            if (r0 != 0) goto L_0x027c
            com.facebook.common.banner.BasicBannerNotificationView r4 = r2.A08
            com.facebook.mig.scheme.interfaces.MigColorScheme r6 = r2.A03
            X.1Yd r8 = r9.A02
            r0 = 845618931302570(0x30116000400aa, double:4.17791263429589E-309)
            android.content.res.Resources r7 = r9.A00
            r5 = 2131823127(0x7f110a17, float:1.9279045E38)
            java.lang.String r5 = r7.getString(r5)
            java.lang.String r0 = r8.B4E(r0, r5)
            X.2Se r5 = new X.2Se
            r5.<init>()
            r5.A07 = r0
            int r0 = X.C33711o1.A01(r6)
            r5.A02 = r0
            android.content.res.Resources r1 = r9.A00
            X.1I7 r0 = X.AnonymousClass1I7.SMALL
            int r0 = r0.B5j()
            float r0 = r1.getDimension(r0)
            r5.A00 = r0
            int r1 = X.C33711o1.A00(r6)
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r1)
            r5.A03 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r5.A08 = r0
            r5.A09 = r3
            r0 = 0
            r5.A01(r0)
            X.3AN r0 = r5.A00()
            r4.A0T(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            r2.A09 = r0
            goto L_0x0091
        L_0x01af:
            boolean r0 = r1.BDc()
            if (r0 != 0) goto L_0x027c
            com.facebook.common.banner.BasicBannerNotificationView r6 = r2.A08
            com.facebook.mig.scheme.interfaces.MigColorScheme r7 = r2.A03
            android.content.res.Resources r1 = r9.A00
            r0 = 2131834860(0x7f1137ec, float:1.9302842E38)
            java.lang.String r0 = r1.getString(r0)
            X.2Se r5 = new X.2Se
            r5.<init>()
            r5.A07 = r0
            int r0 = X.C33711o1.A01(r7)
            r5.A02 = r0
            android.content.res.Resources r1 = r9.A00
            X.1I7 r0 = X.AnonymousClass1I7.SMALL
            int r0 = r0.B5j()
            float r0 = r1.getDimension(r0)
            r5.A00 = r0
            int r1 = X.C33711o1.A00(r7)
            android.graphics.drawable.ColorDrawable r0 = new android.graphics.drawable.ColorDrawable
            r0.<init>(r1)
            r5.A03 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r5.A08 = r0
            X.C33711o1.A03(r9, r5, r7)
            X.3AN r0 = r5.A00()
            r6.A0T(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            goto L_0x0258
        L_0x01f9:
            boolean r0 = r1.BDc()
            if (r0 != 0) goto L_0x027c
            com.facebook.mig.scheme.interfaces.MigColorScheme r5 = r2.A03
            X.1Yd r8 = r9.A02
            r0 = 845618931368107(0x30116000500ab, double:4.177912634619685E-309)
            android.content.res.Resources r7 = r9.A00
            r6 = 2131828875(0x7f11208b, float:1.9290703E38)
            java.lang.String r6 = r7.getString(r6)
            java.lang.String r1 = r8.B4E(r0, r6)
            int r0 = X.C33711o1.A00(r5)
            android.graphics.drawable.ColorDrawable r6 = new android.graphics.drawable.ColorDrawable
            r6.<init>(r0)
            X.2Se r7 = new X.2Se
            r7.<init>()
            r7.A07 = r1
            r0 = r5
            if (r5 != 0) goto L_0x022c
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = X.C17190yT.A00()
        L_0x022c:
            X.0y3 r0 = r0.B0J()
            int r1 = r0.AhV()
            r7.A02 = r1
            android.content.res.Resources r1 = r9.A00
            X.1I7 r0 = X.AnonymousClass1I7.SMALL
            int r0 = r0.B5j()
            float r1 = r1.getDimension(r0)
            r7.A00 = r1
            r7.A03 = r6
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            r7.A08 = r1
            X.C33711o1.A03(r9, r7, r5)
            X.3AN r1 = r7.A00()
            com.facebook.common.banner.BasicBannerNotificationView r0 = r2.A08
            r0.A0T(r1)
            java.lang.Integer r0 = X.AnonymousClass07B.A01
        L_0x0258:
            r2.A09 = r0
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r2.A02
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.1Yd r4 = (X.C25051Yd) r4
            r0 = 285117108983168(0x1035000001580, double:1.40866568590158E-309)
            boolean r0 = r4.Aeo(r0, r3)
            if (r0 == 0) goto L_0x0091
            com.facebook.common.banner.BasicBannerNotificationView r4 = r2.A08
            com.google.common.base.Optional r1 = r4.A04
            X.DJb r0 = new X.DJb
            r0.<init>(r2, r1, r4)
            r4.A00 = r0
            goto L_0x0091
        L_0x027c:
            com.facebook.common.banner.BasicBannerNotificationView r1 = r2.A08
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r2.A03
            X.3AN r0 = r9.A04(r0)
            r1.A0T(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            r2.A09 = r0
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16660xW.A01(X.0xW):void");
    }

    public static void A02(C16660xW r5, String str) {
        if (r5.A09 == AnonymousClass07B.A00) {
            ((ScheduledExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BKX, r5.A02)).schedule(new C185614d(r5, str), (long) LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT, TimeUnit.MILLISECONDS);
            return;
        }
        r5.A06(str);
    }

    public static void A03(C16660xW r12, String str) {
        Integer num;
        Integer num2;
        if (r12.A04 == AnonymousClass07B.A01 && (num = r12.A09) != (num2 = AnonymousClass07B.A00)) {
            String A002 = C55622oO.A00(num);
            String A003 = C55622oO.A00(num2);
            int i = AnonymousClass1Y3.AxV;
            r12.A0B.BIp("connectivity_banner", AnonymousClass08S.A0W(A002, "->", A003, "; hide_reason=", str, "; monitor_status=", ((C33701o0) AnonymousClass1XX.A02(0, i, r12.A02)).AiE().name(), "; was_connected=", String.valueOf(((C33701o0) AnonymousClass1XX.A02(0, i, r12.A02)).CN2())));
        }
        r12.A09 = AnonymousClass07B.A00;
        r12.A00.A05(r12);
        ((C55612oN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ArI, r12.A02)).A02(r12.A04, AnonymousClass07B.A00);
    }

    private boolean A04() {
        if (!this.A07 || !((C33701o0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxV, this.A02)).BDc()) {
            return false;
        }
        return true;
    }

    public void A06(String str) {
        boolean z = false;
        if (this.A06) {
            int i = AnonymousClass1Y3.AxV;
            if (!((C33701o0) AnonymousClass1XX.A02(0, i, this.A02)).BDc() && (((C33701o0) AnonymousClass1XX.A02(0, i, this.A02)).AiE() == C33731o3.NO_INTERNET || ((C33701o0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxV, this.A02)).AiE() == C33731o3.WAITING_TO_CONNECT)) {
                z = true;
            }
        }
        if (!z) {
            boolean z2 = false;
            if (!((C33701o0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxV, this.A02)).isConnected() || A05(this)) {
                z2 = true;
            }
            if (z2) {
                if (this.A00.A06(this)) {
                    this.A05 = str;
                }
                C33701o0 r1 = (C33701o0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxV, this.A02);
                if (r1.isConnected() && !A05(this)) {
                    ((ScheduledExecutorService) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BKX, this.A02)).schedule(new C1927891k(this, r1), 3000, TimeUnit.MILLISECONDS);
                    return;
                }
                return;
            }
        }
        A03(this, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View B90(ViewGroup viewGroup) {
        this.A08 = (BasicBannerNotificationView) ((LayoutInflater) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AhI, this.A02)).inflate(2132410497, viewGroup, false);
        A01(this);
        ((C55612oN) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ArI, this.A02)).A02(this.A04, this.A09);
        return this.A08;
    }

    public static boolean A05(C16660xW r3) {
        if (r3.A04() || ((C33701o0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxV, r3.A02)).AiE() == C33731o3.CONNECTED_CAPTIVE_PORTAL) {
            return true;
        }
        return false;
    }

    public void onResume() {
        A00(this);
        this.A01.A00();
        this.A00.A00();
        A02(this, "on_resume");
    }
}
