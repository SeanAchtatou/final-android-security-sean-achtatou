package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.1rH  reason: invalid class name and case insensitive filesystem */
public final class C35471rH {
    private static C04470Uu A01;
    public AnonymousClass0UN A00;

    public static final C35471rH A01(AnonymousClass1XY r4) {
        C35471rH r0;
        synchronized (C35471rH.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new C35471rH((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C35471rH) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public boolean A02() {
        return false;
    }

    public boolean A03() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(284919543698622L);
    }

    private C35471rH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public static final C35471rH A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
