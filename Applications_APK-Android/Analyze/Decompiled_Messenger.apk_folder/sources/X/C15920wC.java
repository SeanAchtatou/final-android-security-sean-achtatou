package X;

import android.os.Build;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0wC  reason: invalid class name and case insensitive filesystem */
public final class C15920wC implements Runnable {
    public static final String __redex_internal_original_name = "androidx.recyclerview.widget.RecyclerView$ViewFlinger";
    public int A00;
    public int A01;
    public Interpolator A02 = RecyclerView.A1B;
    public OverScroller A03;
    public boolean A04 = false;
    public boolean A05 = false;
    public final /* synthetic */ RecyclerView A06;

    public C15920wC(RecyclerView recyclerView) {
        this.A06 = recyclerView;
        this.A03 = new OverScroller(recyclerView.getContext(), RecyclerView.A1B);
    }

    public void A01() {
        if (this.A04) {
            this.A05 = true;
            return;
        }
        this.A06.removeCallbacks(this);
        C15320v6.postOnAnimation(this.A06, this);
    }

    public void A02(int i, int i2, int i3, Interpolator interpolator) {
        if (this.A02 != interpolator) {
            this.A02 = interpolator;
            this.A03 = new OverScroller(this.A06.getContext(), interpolator);
        }
        this.A06.A0l(2);
        this.A01 = 0;
        this.A00 = 0;
        this.A03.startScroll(0, 0, i, i2, i3);
        if (Build.VERSION.SDK_INT < 23) {
            this.A03.computeScrollOffset();
        }
        A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0116, code lost:
        if (r6 == false) goto L_0x0118;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r20 = this;
            r5 = r20
            r0 = -365608472(0xffffffffea3541e8, float:-5.47817E25)
            int r4 = X.C000700l.A03(r0)
            androidx.recyclerview.widget.RecyclerView r1 = r5.A06
            X.19T r0 = r1.A0L
            if (r0 != 0) goto L_0x001e
            r1.removeCallbacks(r5)
            android.widget.OverScroller r0 = r5.A03
            r0.abortAnimation()
            r0 = 185966960(0xb15a170, float:2.8817808E-32)
            X.C000700l.A09(r0, r4)
            return
        L_0x001e:
            r0 = 0
            r5.A05 = r0
            r3 = 1
            r5.A04 = r3
            r1.A0e()
            android.widget.OverScroller r2 = r5.A03
            boolean r0 = r2.computeScrollOffset()
            r1 = 0
            if (r0 == 0) goto L_0x0179
            int r7 = r2.getCurrX()
            int r6 = r2.getCurrY()
            int r0 = r5.A00
            int r10 = r7 - r0
            int r0 = r5.A01
            int r11 = r6 - r0
            r5.A00 = r7
            r5.A01 = r6
            androidx.recyclerview.widget.RecyclerView r9 = r5.A06
            int[] r12 = r9.A0r
            r12[r1] = r1
            r0 = 1
            r12[r3] = r1
            r13 = 0
            r14 = 1
            boolean r6 = r9.A1E(r10, r11, r12, r13, r14)
            if (r6 == 0) goto L_0x005f
            androidx.recyclerview.widget.RecyclerView r6 = r5.A06
            int[] r7 = r6.A0r
            r6 = r7[r1]
            int r10 = r10 - r6
            r6 = r7[r3]
            int r11 = r11 - r6
        L_0x005f:
            androidx.recyclerview.widget.RecyclerView r6 = r5.A06
            int r6 = r6.getOverScrollMode()
            r7 = 2
            if (r6 == r7) goto L_0x006d
            androidx.recyclerview.widget.RecyclerView r6 = r5.A06
            r6.A0n(r10, r11)
        L_0x006d:
            androidx.recyclerview.widget.RecyclerView r8 = r5.A06
            X.1Dz r6 = r8.A0J
            if (r6 == 0) goto L_0x01f2
            int[] r6 = r8.A0r
            r6[r1] = r1
            r6[r3] = r1
            r8.A0t(r10, r11, r6)
            androidx.recyclerview.widget.RecyclerView r8 = r5.A06
            int[] r6 = r8.A0r
            r13 = r6[r1]
            r14 = r6[r3]
            int r10 = r10 - r13
            int r11 = r11 - r14
            X.19T r6 = r8.A0L
            X.30i r9 = r6.A07
            if (r9 == 0) goto L_0x009f
            boolean r6 = r9.A04
            if (r6 != 0) goto L_0x009f
            boolean r6 = r9.A05
            if (r6 == 0) goto L_0x009f
            X.0wD r6 = r8.A0y
            int r8 = r6.A00()
            if (r8 != 0) goto L_0x01e1
            r9.A01()
        L_0x009f:
            androidx.recyclerview.widget.RecyclerView r6 = r5.A06
            java.util.ArrayList r6 = r6.A11
            boolean r6 = r6.isEmpty()
            if (r6 != 0) goto L_0x00ae
            androidx.recyclerview.widget.RecyclerView r6 = r5.A06
            r6.invalidate()
        L_0x00ae:
            androidx.recyclerview.widget.RecyclerView r8 = r5.A06
            int[] r6 = r8.A0r
            r6[r1] = r1
            r6[r3] = r1
            r17 = 0
            X.0v0 r12 = androidx.recyclerview.widget.RecyclerView.A04(r8)
            r15 = r10
            r16 = r11
            r18 = r3
            r19 = r6
            X.C15270v0.A01(r12, r13, r14, r15, r16, r17, r18, r19)
            androidx.recyclerview.widget.RecyclerView r9 = r5.A06
            int[] r8 = r9.A0r
            r6 = r8[r1]
            int r10 = r10 - r6
            r3 = r8[r3]
            int r11 = r11 - r3
            if (r13 != 0) goto L_0x00d4
            if (r14 == 0) goto L_0x00d7
        L_0x00d4:
            r9.A0p(r13, r14)
        L_0x00d7:
            androidx.recyclerview.widget.RecyclerView r3 = r5.A06
            boolean r3 = r3.awakenScrollBars()
            if (r3 != 0) goto L_0x00e4
            androidx.recyclerview.widget.RecyclerView r3 = r5.A06
            r3.invalidate()
        L_0x00e4:
            int r6 = r2.getCurrX()
            int r3 = r2.getFinalX()
            r9 = 0
            if (r6 != r3) goto L_0x00f0
            r9 = 1
        L_0x00f0:
            int r8 = r2.getCurrY()
            int r3 = r2.getFinalY()
            r6 = 0
            if (r8 != r3) goto L_0x00fc
            r6 = 1
        L_0x00fc:
            boolean r3 = r2.isFinished()
            if (r3 != 0) goto L_0x010a
            if (r9 != 0) goto L_0x0106
            if (r10 == 0) goto L_0x01de
        L_0x0106:
            if (r6 != 0) goto L_0x010a
            if (r11 == 0) goto L_0x01de
        L_0x010a:
            r9 = 1
        L_0x010b:
            androidx.recyclerview.widget.RecyclerView r8 = r5.A06
            X.19T r3 = r8.A0L
            X.30i r3 = r3.A07
            if (r3 == 0) goto L_0x0118
            boolean r6 = r3.A04
            r3 = 1
            if (r6 != 0) goto L_0x0119
        L_0x0118:
            r3 = 0
        L_0x0119:
            if (r3 != 0) goto L_0x01d1
            if (r9 == 0) goto L_0x01d1
            int r3 = r8.getOverScrollMode()
            if (r3 == r7) goto L_0x015d
            float r2 = r2.getCurrVelocity()
            int r7 = (int) r2
            if (r10 >= 0) goto L_0x01cb
            int r6 = -r7
        L_0x012b:
            if (r11 >= 0) goto L_0x01c6
            int r7 = -r7
        L_0x012e:
            androidx.recyclerview.widget.RecyclerView r8 = r5.A06
            if (r6 >= 0) goto L_0x01b2
            androidx.recyclerview.widget.RecyclerView.A0J(r8)
            android.widget.EdgeEffect r2 = r8.A0D
            boolean r2 = r2.isFinished()
            if (r2 == 0) goto L_0x0143
            android.widget.EdgeEffect r3 = r8.A0D
            int r2 = -r6
            r3.onAbsorb(r2)
        L_0x0143:
            if (r7 >= 0) goto L_0x019f
            androidx.recyclerview.widget.RecyclerView.A0L(r8)
            android.widget.EdgeEffect r2 = r8.A0F
            boolean r2 = r2.isFinished()
            if (r2 == 0) goto L_0x0156
            android.widget.EdgeEffect r3 = r8.A0F
            int r2 = -r7
            r3.onAbsorb(r2)
        L_0x0156:
            if (r6 != 0) goto L_0x015a
            if (r7 == 0) goto L_0x015d
        L_0x015a:
            X.C15320v6.postInvalidateOnAnimation(r8)
        L_0x015d:
            androidx.recyclerview.widget.RecyclerView r2 = r5.A06
            r2.A0l(r1)
            boolean r2 = androidx.recyclerview.widget.RecyclerView.A1C
            if (r2 == 0) goto L_0x0174
            androidx.recyclerview.widget.RecyclerView r2 = r5.A06
            X.1uj r6 = r2.A0I
            int[] r3 = r6.A03
            if (r3 == 0) goto L_0x0172
            r2 = -1
            java.util.Arrays.fill(r3, r2)
        L_0x0172:
            r6.A00 = r1
        L_0x0174:
            androidx.recyclerview.widget.RecyclerView r2 = r5.A06
            r2.CHt(r0)
        L_0x0179:
            androidx.recyclerview.widget.RecyclerView r0 = r5.A06
            X.19T r0 = r0.A0L
            X.30i r2 = r0.A07
            if (r2 == 0) goto L_0x018f
            boolean r0 = r2.A04
            if (r0 == 0) goto L_0x0188
            r2.A04(r1, r1)
        L_0x0188:
            boolean r0 = r5.A05
            if (r0 != 0) goto L_0x018f
            r2.A01()
        L_0x018f:
            r5.A04 = r1
            boolean r0 = r5.A05
            if (r0 == 0) goto L_0x0198
            r5.A01()
        L_0x0198:
            r0 = 809759386(0x3043f29a, float:7.1285344E-10)
            X.C000700l.A09(r0, r4)
            return
        L_0x019f:
            if (r7 <= 0) goto L_0x0156
            androidx.recyclerview.widget.RecyclerView.A0I(r8)
            android.widget.EdgeEffect r2 = r8.A0C
            boolean r2 = r2.isFinished()
            if (r2 == 0) goto L_0x0156
            android.widget.EdgeEffect r2 = r8.A0C
            r2.onAbsorb(r7)
            goto L_0x0156
        L_0x01b2:
            if (r6 <= 0) goto L_0x0143
            androidx.recyclerview.widget.RecyclerView.A0K(r8)
            android.widget.EdgeEffect r2 = r8.A0E
            boolean r2 = r2.isFinished()
            if (r2 == 0) goto L_0x0143
            android.widget.EdgeEffect r2 = r8.A0E
            r2.onAbsorb(r6)
            goto L_0x0143
        L_0x01c6:
            if (r11 > 0) goto L_0x012e
            r7 = 0
            goto L_0x012e
        L_0x01cb:
            r6 = 0
            if (r10 <= 0) goto L_0x012b
            r6 = r7
            goto L_0x012b
        L_0x01d1:
            r20.A01()
            androidx.recyclerview.widget.RecyclerView r2 = r5.A06
            X.1ux r0 = r2.A00
            if (r0 == 0) goto L_0x0179
            r0.A01(r2, r10, r11)
            goto L_0x0179
        L_0x01de:
            r9 = 0
            goto L_0x010b
        L_0x01e1:
            int r6 = r9.A00
            if (r6 < r8) goto L_0x01ed
            int r8 = r8 - r3
            r9.A00 = r8
            r9.A04(r13, r14)
            goto L_0x009f
        L_0x01ed:
            r9.A04(r13, r14)
            goto L_0x009f
        L_0x01f2:
            r13 = 0
            r14 = 0
            goto L_0x009f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15920wC.run():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public static int A00(C15920wC r10, int i, int i2, int i3, int i4) {
        int height;
        int i5;
        int abs = Math.abs(i);
        int abs2 = Math.abs(i2);
        boolean z = false;
        if (abs > abs2) {
            z = true;
        }
        int sqrt = (int) Math.sqrt((double) ((i3 * i3) + (i4 * i4)));
        int sqrt2 = (int) Math.sqrt((double) ((i * i) + (i2 * i2)));
        RecyclerView recyclerView = r10.A06;
        if (z) {
            height = recyclerView.getWidth();
        } else {
            height = recyclerView.getHeight();
        }
        float f = (float) height;
        float f2 = (float) (height >> 1);
        float sin = f2 + (((float) Math.sin((double) ((Math.min(1.0f, (((float) sqrt2) * 1.0f) / f) - 0.5f) * 0.47123894f))) * f2);
        if (sqrt > 0) {
            i5 = Math.round(Math.abs(sin / ((float) sqrt)) * 1000.0f) << 2;
        } else {
            if (!z) {
                abs = abs2;
            }
            i5 = (int) (((((float) abs) / f) + 1.0f) * 300.0f);
        }
        return Math.min(i5, 2000);
    }
}
