package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1eK  reason: invalid class name and case insensitive filesystem */
public abstract class C28141eK {
    public final AnonymousClass04e A00;
    private final C008807t A01;
    public final boolean A02;

    public Intent A0A(Intent intent, Context context) {
        return A0B(intent, context, null);
    }

    public Intent A0B(Intent intent, Context context, String str) {
        if (this instanceof C13340rH) {
            return C13340rH.A00((C13340rH) this, intent, context, str, A06(intent, context, 65600));
        }
        if (this instanceof C13350rI) {
            C13350rI r3 = (C13350rI) this;
            C13360rJ.A02(intent, context, str, r3.A00);
            return !A09(intent, context) ? C13350rI.A00(r3, intent, context, A06(intent, context, 65600)) : intent;
        } else if (this instanceof C14070sZ) {
            C14070sZ r32 = (C14070sZ) this;
            C13360rJ.A02(intent, context, str, r32.A00);
            return !A09(intent, context) ? C14070sZ.A00(r32, intent, context, A06(intent, context, 65600)) : intent;
        } else if (this instanceof C28871fV) {
            C28871fV r33 = (C28871fV) this;
            if (A09(intent, context)) {
                return null;
            }
            List A06 = A06(intent, context, 65600);
            if (A06.isEmpty()) {
                A06 = A06(intent, context, 0);
            }
            return C28871fV.A00(r33, intent, context, A06);
        } else if (!(this instanceof C28851fT)) {
            C14130sf r5 = (C14130sf) this;
            List A062 = A06(intent, context, 65600);
            Iterator it = A062.iterator();
            boolean z = false;
            while (it.hasNext()) {
                ActivityInfo activityInfo = (ActivityInfo) it.next();
                if (!C14130sf.A02(r5, intent, context, activityInfo, activityInfo.permission)) {
                    z = true;
                    it.remove();
                }
            }
            return C14130sf.A01(r5, intent, A062, z);
        } else {
            ((C28851fT) this).A00.C2T("AnyIntentScope", AnonymousClass08S.A0J("Any_UNSAFE scope used for launching activity: ", A05(intent)), null);
            return intent;
        }
    }

    public Intent A0C(Intent intent, Context context, String str) {
        boolean z;
        if (this instanceof C13340rH) {
            C13340rH r4 = (C13340rH) this;
            int i = context.getApplicationInfo().uid;
            C06620bo A002 = C13360rJ.A00(intent, null);
            int i2 = A002 == null ? -1 : A002.A00;
            if (!AnonymousClass0TW.A04(r4.A00, AnonymousClass0TW.A00(i2, context), context)) {
                String format = String.format("Access denied. Process %d cannot receive broadcasts from %d", Integer.valueOf(i), Integer.valueOf(i2));
                r4.A00.C2T(r4.A01, format, new SecurityException(format));
                return null;
            }
        } else if (this instanceof C13350rI) {
            C13350rI r42 = (C13350rI) this;
            int i3 = context.getApplicationInfo().uid;
            C06620bo A003 = C13360rJ.A00(intent, null);
            int i4 = A003 == null ? -1 : A003.A00;
            try {
                z = C28061eC.A04(context, i3, i4);
            } catch (SecurityException e) {
                r42.A00.C2T("SameKeyIntentScope", AnonymousClass08S.A09("Unexpected exception in verifying signature for: ", i4), e);
                z = r42.A0H();
            }
            if (!z) {
                String format2 = String.format("Access denied. Process %d cannot receive broadcasts from %d", Integer.valueOf(i3), Integer.valueOf(i4));
                r42.A00.C2T("SameKeyIntentScope", format2, null);
                throw new SecurityException(format2);
            }
        } else if (this instanceof C14070sZ) {
            C14070sZ r5 = (C14070sZ) this;
            String str2 = null;
            C06620bo A004 = C13360rJ.A00(intent, null);
            if (A004 != null) {
                str2 = A004.A01();
            }
            String packageName = context.getPackageName();
            if (!packageName.equals(str2)) {
                String format3 = String.format("Access denied. %s cannot receive broadcasts from %s", packageName, A004 == null ? "no_app_identity" : A004.A01() == null ? "null" : A004.A01());
                if (r5.A0H()) {
                    r5.A00.C2T("InternalIntentScope", AnonymousClass08S.A0J("Fail-open: ", format3), null);
                } else {
                    r5.A00.C2T("InternalIntentScope", format3, new SecurityException(format3));
                    return null;
                }
            }
        } else if ((this instanceof C28871fV) || !(this instanceof C28851fT)) {
            throw new UnsupportedOperationException();
        }
        return intent;
    }

    public Intent A0D(Intent intent, Context context, String str) {
        if (this instanceof C13340rH) {
            return C13340rH.A00((C13340rH) this, intent, context, str, A07(intent, context, 65600));
        }
        if (this instanceof C13350rI) {
            C13350rI r3 = (C13350rI) this;
            C13360rJ.A02(intent, context, str, r3.A00);
            return !A09(intent, context) ? C13350rI.A00(r3, intent, context, A07(intent, context, 65600)) : intent;
        } else if (this instanceof C14070sZ) {
            C14070sZ r32 = (C14070sZ) this;
            C13360rJ.A02(intent, context, str, r32.A00);
            return !A09(intent, context) ? C14070sZ.A00(r32, intent, context, A07(intent, context, 65600)) : intent;
        } else if (this instanceof C28871fV) {
            C28871fV r33 = (C28871fV) this;
            if (A09(intent, context)) {
                return null;
            }
            List A07 = A07(intent, context, 65600);
            if (A07.isEmpty()) {
                A07 = A07(intent, context, 0);
            }
            return C28871fV.A00(r33, intent, context, A07);
        } else if (!(this instanceof C28851fT)) {
            C14130sf r5 = (C14130sf) this;
            List A072 = A07(intent, context, 65600);
            Iterator it = A072.iterator();
            boolean z = false;
            while (it.hasNext()) {
                ServiceInfo serviceInfo = (ServiceInfo) it.next();
                if (!C14130sf.A02(r5, intent, context, serviceInfo, serviceInfo.permission)) {
                    z = true;
                    it.remove();
                }
            }
            return C14130sf.A01(r5, intent, A072, z);
        } else {
            ((C28851fT) this).A00.C2T("AnyIntentScope", AnonymousClass08S.A0J("Any_UNSAFE scope used for launching service: ", A05(intent)), null);
            return intent;
        }
    }

    public Integer A0E() {
        return !(this instanceof C13350rI) ? !(this instanceof C14070sZ) ? !(this instanceof C13340rH) ? !(this instanceof C28881fW) ? !(this instanceof C28861fU) ? !(this instanceof C28851fT) ? AnonymousClass07B.A0Y : AnonymousClass07B.A0o : AnonymousClass07B.A0i : AnonymousClass07B.A0n : !(((C13340rH) this) instanceof C15890w9) ? AnonymousClass07B.A0o : AnonymousClass07B.A0C : AnonymousClass07B.A00 : AnonymousClass07B.A01;
    }

    public List A0G(Intent intent, Context context, String str) {
        List list;
        if (this instanceof C13340rH) {
            C13340rH r1 = (C13340rH) this;
            if (r1.A02) {
                C13360rJ.A02(intent, context, str, r1.A00);
            }
            list = r1.A0F(intent, context);
            if (list.isEmpty()) {
                r1.A00.C2T(r1.A01, "No matching packages available.", null);
            }
        } else if (!(this instanceof C13350rI)) {
            if (this instanceof C14070sZ) {
                C13360rJ.A02(intent, context, str, ((C14070sZ) this).A00);
                if (!A09(intent, context)) {
                    intent.setPackage(context.getPackageName());
                }
            } else if ((this instanceof C28871fV) || !(this instanceof C28851fT)) {
                throw new UnsupportedOperationException();
            } else {
                ((C28851fT) this).A00.C2T("AnyIntentScope", AnonymousClass08S.A0J("Any_UNSAFE scope used for sending a broadcast: ", A05(intent)), null);
            }
            return Collections.singletonList(intent);
        } else {
            C13350rI r12 = (C13350rI) this;
            C13360rJ.A02(intent, context, str, r12.A00);
            list = r12.A0F(intent, context);
            if (list.isEmpty()) {
                r12.A00.C2T("SameKeyIntentScope", "No matching same-key packages", null);
                return list;
            }
        }
        return list;
    }

    public boolean A0I(Context context, PackageInfo packageInfo) {
        if (this instanceof C13340rH) {
            return C13340rH.A01((C13340rH) this, context, packageInfo.applicationInfo);
        }
        if (this instanceof C13350rI) {
            return C13350rI.A01((C13350rI) this, context, context.getApplicationInfo(), packageInfo.applicationInfo);
        }
        if (!(this instanceof C14070sZ) && !(this instanceof C28871fV)) {
            boolean z = this instanceof C28851fT;
        }
        throw new UnsupportedOperationException();
    }

    public static Integer A04(C28141eK r1) {
        Integer num;
        C008807t r12 = r1.A01;
        synchronized (r12) {
            num = r12.A00;
        }
        return num;
    }

    public static String A05(Intent intent) {
        if (intent == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder("intent(");
        sb.append("action = ");
        sb.append(intent.getAction());
        sb.append(", data= ");
        sb.append(intent.getData());
        sb.append(", type= ");
        sb.append(intent.getType());
        if (intent.getComponent() != null) {
            sb.append(", component = ");
            sb.append(intent.getComponent());
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            sb.append(", extras = [");
            for (String next : extras.keySet()) {
                sb.append(next);
                sb.append(" = ");
                sb.append(extras.get(next));
                sb.append(", ");
            }
            sb.append("]");
        }
        sb.append(")");
        return sb.toString();
    }

    public static List A08(List list, Intent intent) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ComponentInfo componentInfo = (ComponentInfo) it.next();
            Intent intent2 = new Intent(intent);
            intent2.setComponent(new ComponentName(componentInfo.packageName, componentInfo.name));
            intent2.setPackage(componentInfo.packageName);
            arrayList.add(intent2);
        }
        return arrayList;
    }

    public List A0F(Intent intent, Context context) {
        ArrayList arrayList = new ArrayList(1);
        ArrayList<PackageInfo> arrayList2 = new ArrayList<>();
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(64);
            arrayList2 = new ArrayList<>(1);
            for (PackageInfo next : installedPackages) {
                if (A0I(context, next)) {
                    arrayList2.add(next);
                }
            }
        } catch (RuntimeException e) {
            this.A00.C2T("BaseIntentScope", "Error querying PackageManager.", e);
        }
        for (PackageInfo packageInfo : arrayList2) {
            Intent intent2 = new Intent(intent);
            intent2.setPackage(packageInfo.packageName);
            arrayList.add(intent2);
        }
        return arrayList;
    }

    public C28141eK(C008807t r1, AnonymousClass04e r2, boolean z) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = z;
    }

    public static Intent A03(List list) {
        Intent[] intentArr = new Intent[(list.size() - 1)];
        int i = 0;
        while (i < list.size() - 1) {
            int i2 = i + 1;
            intentArr[i] = (Intent) list.get(i2);
            i = i2;
        }
        Intent createChooser = Intent.createChooser((Intent) list.get(0), "Choose an app to launch.");
        createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", intentArr);
        return createChooser;
    }

    public static List A06(Intent intent, Context context, int i) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, i);
        if (queryIntentActivities == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(1);
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            if (!(activityInfo == null || activityInfo.applicationInfo == null)) {
                arrayList.add(activityInfo);
            }
        }
        return arrayList;
    }

    public static List A07(Intent intent, Context context, int i) {
        List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, i);
        if (queryIntentServices == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(1);
        for (ResolveInfo resolveInfo : queryIntentServices) {
            ServiceInfo serviceInfo = resolveInfo.serviceInfo;
            if (!(serviceInfo == null || serviceInfo.applicationInfo == null)) {
                arrayList.add(serviceInfo);
            }
        }
        return arrayList;
    }

    public static boolean A09(Intent intent, Context context) {
        if (intent.getComponent() == null || !intent.getComponent().getPackageName().equals(context.getPackageName())) {
            return false;
        }
        return true;
    }

    public boolean A0H() {
        if (A04(this) == AnonymousClass07B.A00) {
            return true;
        }
        return false;
    }
}
