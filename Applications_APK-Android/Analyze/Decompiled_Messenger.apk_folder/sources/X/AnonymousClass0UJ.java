package X;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Queue;

/* renamed from: X.0UJ  reason: invalid class name */
public final class AnonymousClass0UJ extends AnonymousClass0UI {
    public final Queue A00;

    public AnonymousClass0UJ(Object... objArr) {
        ArrayDeque arrayDeque = new ArrayDeque(objArr.length);
        this.A00 = arrayDeque;
        Collections.addAll(arrayDeque, objArr);
    }
}
