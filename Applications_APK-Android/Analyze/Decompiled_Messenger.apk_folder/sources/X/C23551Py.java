package X;

import android.graphics.Bitmap;

/* renamed from: X.1Py  reason: invalid class name and case insensitive filesystem */
public class C23551Py {
    public Bitmap.Config A00 = Bitmap.Config.ARGB_8888;
    public AnonymousClass8W1 A01;
    public C22761Ms A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;

    public C23541Px A00() {
        return new C23541Px(this);
    }

    public C23551Py A01(boolean z) {
        this.A03 = z;
        return this;
    }

    public C23551Py A02(boolean z) {
        this.A04 = z;
        return this;
    }

    public C23551Py A03(boolean z) {
        this.A05 = z;
        return this;
    }
}
