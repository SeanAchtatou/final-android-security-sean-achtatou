package X;

/* renamed from: X.1MH  reason: invalid class name */
public final class AnonymousClass1MH {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;

    static {
        AnonymousClass1Y7 r1 = (AnonymousClass1Y7) C04350Ue.A05.A09("photos/");
        A01 = r1;
        r1.A09("simplepicker_last_open_time");
        AnonymousClass1Y7 r12 = A01;
        A00 = (AnonymousClass1Y7) r12.A09("analytics");
        r12.A09("MMP_NUX");
        C04350Ue.A05.A09("slideshow/entry_point");
        AnonymousClass1Y7 r13 = A01;
        r13.A09("hd_upload_nux");
        r13.A09("picker_highlights_nux");
        r13.A09("last_video_asset_count_log_time");
        r13.A09("last_new_asset_count_log_time");
        r13.A09("hi_res_photo_upload_pref");
        r13.A09("hi_res_video_upload_pref");
    }
}
