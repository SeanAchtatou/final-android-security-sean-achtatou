package X;

/* renamed from: X.0Gv  reason: invalid class name and case insensitive filesystem */
public final class C02890Gv implements C02780Gi {
    public void C2x(AnonymousClass0FM r6, C02910Gx r7) {
        C02660Fu r62 = (C02660Fu) r6;
        double d = r62.total.powerMah;
        if (d != 0.0d) {
            r7.AMT("sensor_power_mah", d);
        }
        long j = r62.total.activeTimeMs;
        if (j != 0) {
            r7.AMV("sensor_active_time_ms", j);
        }
        long j2 = r62.total.wakeUpTimeMs;
        if (j2 != 0) {
            r7.AMV("sensor_wakeup_time_ms", j2);
        }
    }
}
