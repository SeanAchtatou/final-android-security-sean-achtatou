package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0PU  reason: invalid class name */
public final class AnonymousClass0PU extends C03160Kg {
    public long A00() {
        return 6566077622105075903L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        C02500Fe r32 = (C02500Fe) r3;
        dataOutput.writeLong(r32.wifiScanCount);
        dataOutput.writeLong(r32.coarseTimeMs);
        dataOutput.writeLong(r32.mediumTimeMs);
        dataOutput.writeLong(r32.fineTimeMs);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        C02500Fe r32 = (C02500Fe) r3;
        r32.wifiScanCount = dataInput.readLong();
        r32.coarseTimeMs = dataInput.readLong();
        r32.mediumTimeMs = dataInput.readLong();
        r32.fineTimeMs = dataInput.readLong();
        return true;
    }
}
