package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import androidx.fragment.app.Fragment;
import com.google.common.base.Optional;

/* renamed from: X.0qt  reason: invalid class name and case insensitive filesystem */
public interface C13200qt extends C13180qr {
    void AZv(Activity activity);

    void BN7(Activity activity);

    void BNJ(Activity activity, int i, int i2, Intent intent);

    void BON(Activity activity, Resources.Theme theme, int i, boolean z);

    void BOW(Activity activity, Fragment fragment);

    boolean BPC(Activity activity);

    void BPY(Activity activity, Bundle bundle);

    void BPo(Activity activity, Bundle bundle);

    void BTK(Activity activity, Configuration configuration);

    void BTy(Activity activity);

    Dialog BUl(Activity activity, int i);

    void BUs(Menu menu);

    Optional BcM(Activity activity, int i, KeyEvent keyEvent);

    Optional BcN(Activity activity, int i, KeyEvent keyEvent);

    void BgX(Activity activity, Intent intent);

    boolean BhL(MenuItem menuItem);

    void Bif(Activity activity, boolean z, Configuration configuration);

    void BjC(Activity activity, Bundle bundle);

    boolean BjS(Activity activity, int i, Dialog dialog);

    void BjX(Menu menu);

    void BmT(Activity activity);

    void Bmx(Bundle bundle);

    Optional BnW(Activity activity);

    boolean Bo7(Activity activity, Throwable th);

    void BsC(CharSequence charSequence, int i);

    void Bt3(Activity activity, int i);

    void Btv(Activity activity);

    void Bty(Activity activity);

    void Bv9(Activity activity, boolean z);
}
