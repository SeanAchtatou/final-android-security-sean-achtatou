package X;

import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.facebook.messaging.inbox2.items.InboxUnitThreadItem;

/* renamed from: X.1Bu  reason: invalid class name and case insensitive filesystem */
public class C20281Bu extends AnonymousClass1I0 {
    public final C33671nx A00;

    public boolean A03(InboxUnitItem inboxUnitItem) {
        if (!(inboxUnitItem instanceof InboxUnitThreadItem) || !C55662oS.A00(((InboxUnitThreadItem) inboxUnitItem).A01)) {
            return false;
        }
        return true;
    }

    public C20281Bu(C33671nx r1) {
        this.A00 = r1;
    }
}
