package X;

import java.util.Collection;

/* renamed from: X.09w  reason: invalid class name and case insensitive filesystem */
public final class C013509w {
    public static boolean A01(Collection collection) {
        if (collection == null || collection.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean A02(Collection collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        }
        return false;
    }

    public static int A00(Collection collection) {
        if (A02(collection)) {
            return 0;
        }
        return collection.size();
    }
}
