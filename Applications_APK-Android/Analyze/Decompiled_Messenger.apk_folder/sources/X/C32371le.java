package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.attachment.Attachment;

/* renamed from: X.1le  reason: invalid class name and case insensitive filesystem */
public final class C32371le implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new Attachment(parcel);
    }

    public Object[] newArray(int i) {
        return new Attachment[i];
    }
}
