package X;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Map;

/* renamed from: X.1La  reason: invalid class name and case insensitive filesystem */
public abstract class C22361La {
    public int A00() {
        return ((C183011q) this).A00;
    }

    public C22361La A01(String str, double d) {
        C183011q r2 = (C183011q) this;
        r2.A01.A09(str, Double.valueOf(d));
        return r2;
    }

    public C22361La A02(String str, int i) {
        C183011q r2 = (C183011q) this;
        r2.A01.A09(str, Integer.valueOf(i));
        return r2;
    }

    public C22361La A03(String str, long j) {
        C183011q r2 = (C183011q) this;
        r2.A01.A09(str, Long.valueOf(j));
        return r2;
    }

    public C22361La A04(String str, JsonNode jsonNode) {
        C183011q r1 = (C183011q) this;
        C11960oJ.A02(str, jsonNode, r1.A01.A0B());
        return r1;
    }

    public C22361La A05(String str, Object obj) {
        C183011q r2 = (C183011q) this;
        if (obj != null) {
            r2.A01.A0A(str, obj.toString());
        }
        return r2;
    }

    public C22361La A06(String str, String str2) {
        C183011q r1 = (C183011q) this;
        r1.A01.A0A(str, str2);
        return r1;
    }

    public C22361La A07(String str, boolean z) {
        C183011q r2 = (C183011q) this;
        r2.A01.A08(str, Boolean.valueOf(z));
        return r2;
    }

    public C22361La A08(Map map) {
        String str;
        C183011q r3 = (C183011q) this;
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                Object value = entry.getValue();
                if (value instanceof JsonNode) {
                    C11960oJ.A02((String) entry.getKey(), (JsonNode) value, r3.A01.A0B());
                } else if (value instanceof String) {
                    r3.A01.A0A((String) entry.getKey(), (String) value);
                } else {
                    if (value instanceof Number) {
                        str = (String) entry.getKey();
                        value = (Number) value;
                        if (value == null) {
                        }
                    } else {
                        boolean z = value instanceof Boolean;
                        str = (String) entry.getKey();
                        if (z) {
                            value = (Boolean) value;
                            if (value == null) {
                            }
                        } else if (value == null) {
                        }
                    }
                    r3.A01.A0A(str, value.toString());
                }
            }
        }
        return r3;
    }

    public String A09() {
        return ((C183011q) this).A01.A0C();
    }

    public void A0A() {
        ((C183011q) this).A01.A0E();
    }

    public boolean A0B() {
        return ((C183011q) this).A01.A0G();
    }
}
