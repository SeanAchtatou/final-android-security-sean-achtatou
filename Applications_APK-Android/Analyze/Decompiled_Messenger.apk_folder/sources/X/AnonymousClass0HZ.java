package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;
import java.util.HashSet;

/* renamed from: X.0HZ  reason: invalid class name */
public final class AnonymousClass0HZ extends Handler {
    public final C004605l A00;
    public final HashSet A01 = new HashSet();

    public synchronized void A00(TraceContext traceContext) {
        if (this.A01.contains(Long.valueOf(traceContext.A05))) {
            sendMessage(obtainMessage(3, traceContext));
            this.A01.remove(Long.valueOf(traceContext.A05));
        }
        boolean z = AnonymousClass0PJ.A00;
    }

    public void handleMessage(Message message) {
        TraceContext traceContext = (TraceContext) message.obj;
        int i = message.what;
        if (i == 0) {
            long j = traceContext.A05;
            AnonymousClass054 r5 = AnonymousClass054.A07;
            if (AnonymousClass054.A01(r5, j) != null) {
                Logger.postFinishTrace(42, j);
                r5.A09(j, 4);
            }
        } else if (i == 1) {
            C004605l r0 = this.A00;
            if (r0 != null) {
                r0.Bsc(traceContext);
            }
        } else if (i == 2) {
            synchronized (this) {
                removeMessages(0, traceContext);
                if ((traceContext.A03 & 2) != 0) {
                    long j2 = traceContext.A05;
                    if (Logger.sInitialized) {
                        Logger.startWorkerThreadIfNecessary();
                        Logger.loggerWriteAndWakeupTraceWriter(Logger.sTraceWriter, j2, 41, 0, 0, j2);
                    }
                }
                sendMessageDelayed(obtainMessage(4, traceContext), (long) traceContext.A06.A00("trace_config.post_trace_extension_ms", 0));
            }
        } else if (i == 3) {
            synchronized (this) {
                removeMessages(0, traceContext);
            }
            C004605l r02 = this.A00;
            if (r02 != null) {
                r02.onTraceAbort(traceContext);
            }
        } else if (i == 4) {
            C004605l r03 = this.A00;
            if (r03 != null) {
                r03.onTraceStop(traceContext);
            }
            Logger.postFinishTrace(39, traceContext.A05);
        }
    }

    public AnonymousClass0HZ(C004605l r2, Looper looper) {
        super(looper);
        this.A00 = r2;
    }
}
