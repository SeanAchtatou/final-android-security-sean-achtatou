package X;

import android.app.ActivityManager;
import android.content.Context;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Tv  reason: invalid class name and case insensitive filesystem */
public final class C24491Tv {
    public final ActivityManager A00;
    public final C25051Yd A01;

    public static final C24491Tv A00(AnonymousClass1XY r1) {
        return new C24491Tv(r1);
    }

    public boolean A01(Context context) {
        if (!this.A01.Aem(281724085010871L)) {
            if (!C51692hY.A01(context)) {
                return false;
            }
            if (this.A01.Aem(281724085076408L)) {
                String packageName = context.getPackageName();
                List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = this.A00.getRunningAppProcesses();
                boolean z = false;
                if (runningAppProcesses != null && !runningAppProcesses.isEmpty()) {
                    Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        ActivityManager.RunningAppProcessInfo next = it.next();
                        if (Arrays.asList(next.pkgList).contains(packageName)) {
                            int i = next.importance;
                            if (i == 100 || i == 125) {
                                z = true;
                            }
                        }
                    }
                }
                return !z;
            }
        }
        return true;
    }

    public C24491Tv(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0WT.A00(r2);
        this.A00 = C04490Ux.A04(r2);
    }
}
