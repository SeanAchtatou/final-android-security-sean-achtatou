package X;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

/* renamed from: X.0G3  reason: invalid class name */
public final class AnonymousClass0G3 {
    static {
        C29981hI.A02 = true;
    }

    public static WifiInfo A00(WifiManager wifiManager) {
        WifiInfo wifiInfo;
        if (!C29981hI.A00()) {
            return wifiManager.getConnectionInfo();
        }
        try {
            C29981hI.A01.readLock().lock();
            C29991hJ r2 = C29981hI.A00;
            if (r2 != null) {
                if (r2.A02 && C25891aZ.A02(r2.A00)) {
                    C25891aZ.A01(r2.A00, C25891aZ.A08);
                }
                if (!r2.A01 || !C25891aZ.A02(r2.A00)) {
                    wifiInfo = wifiManager.getConnectionInfo();
                    return wifiInfo;
                }
            }
            wifiInfo = null;
            return wifiInfo;
        } finally {
            C29981hI.A01.readLock().unlock();
        }
    }
}
