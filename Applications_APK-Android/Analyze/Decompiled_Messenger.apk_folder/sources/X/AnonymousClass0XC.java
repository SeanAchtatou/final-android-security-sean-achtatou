package X;

import com.facebook.acra.ErrorReporter;

/* renamed from: X.0XC  reason: invalid class name */
public final class AnonymousClass0XC implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.errorreporting.FbErrorReporterImpl$5";
    public final /* synthetic */ Throwable A00;

    public AnonymousClass0XC(Throwable th) {
        this.A00 = th;
    }

    public void run() {
        try {
            ErrorReporter.getInstance().handleException(this.A00);
        } catch (Throwable unused) {
        }
    }
}
