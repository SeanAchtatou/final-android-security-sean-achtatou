package X;

import com.facebook.mobileconfig.MobileConfigCrashReportUtils;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0cH  reason: invalid class name and case insensitive filesystem */
public final class C06910cH implements AnonymousClass0Z1 {
    private static volatile C06910cH A00;

    public String Amj() {
        return "mobileconfig_canary";
    }

    public static final C06910cH A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C06910cH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C06910cH();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public String getCustomData(Throwable th) {
        return MobileConfigCrashReportUtils.getInstance().getSerializedCanaryData();
    }
}
