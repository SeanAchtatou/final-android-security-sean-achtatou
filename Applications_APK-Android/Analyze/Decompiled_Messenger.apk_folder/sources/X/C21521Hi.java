package X;

import com.facebook.user.model.UserKey;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/* renamed from: X.1Hi  reason: invalid class name and case insensitive filesystem */
public final class C21521Hi {
    private final AnonymousClass0r6 A00;

    public static final C21521Hi A00(AnonymousClass1XY r1) {
        return new C21521Hi(r1);
    }

    public ObjectNode A01(UserKey userKey) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        C195218m A0L = this.A00.A0L(userKey, -1);
        objectNode.put("m", A0L.A01() ? 1 : 0);
        objectNode.put("f", A0L.A00() ? 1 : 0);
        objectNode.put("w", A0L.A02() ? 1 : 0);
        return objectNode;
    }

    public C21521Hi(AnonymousClass1XY r2) {
        this.A00 = C14890uJ.A00(r2);
    }
}
