package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;

/* renamed from: X.0H9  reason: invalid class name */
public final class AnonymousClass0H9 {
    public static void A00(Context context, Class cls, boolean z) {
        ComponentName componentName = new ComponentName(context, cls);
        PackageManager packageManager = context.getPackageManager();
        int i = 2;
        if (z) {
            i = 1;
        }
        packageManager.setComponentEnabledSetting(componentName, i, 1);
        componentName.getShortClassName();
    }
}
