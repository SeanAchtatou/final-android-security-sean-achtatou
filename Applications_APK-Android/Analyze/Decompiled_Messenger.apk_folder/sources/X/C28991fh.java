package X;

import com.facebook.common.util.TriState;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1fh  reason: invalid class name and case insensitive filesystem */
public final class C28991fh {
    private static volatile C28991fh A02;
    public final AnonymousClass1YI A00;
    private volatile TriState A01 = TriState.UNSET;

    public static final C28991fh A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C28991fh.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C28991fh(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public boolean A01() {
        if (!this.A01.isSet()) {
            this.A01 = TriState.valueOf(this.A00.AbO(AnonymousClass1Y3.A1w, false));
        }
        return this.A01.asBoolean(false);
    }

    private C28991fh(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WA.A00(r2);
    }
}
