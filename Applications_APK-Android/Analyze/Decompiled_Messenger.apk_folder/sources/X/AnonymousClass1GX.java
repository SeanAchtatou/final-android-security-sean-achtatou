package X;

import java.util.BitSet;

/* renamed from: X.1GX  reason: invalid class name */
public final class AnonymousClass1GX extends AnonymousClass1CY {
    public AnonymousClass6OZ A00;
    public final BitSet A01 = new BitSet(2);
    public final String[] A02 = {"dataSource", "paginableList"};

    public AnonymousClass1CY A02(String str) {
        super.A02(str);
        return this;
    }
}
