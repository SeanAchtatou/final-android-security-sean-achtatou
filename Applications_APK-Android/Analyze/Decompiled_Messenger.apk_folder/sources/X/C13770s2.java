package X;

import com.facebook.common.stringformat.StringFormatUtil;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: X.0s2  reason: invalid class name and case insensitive filesystem */
public final class C13770s2<E> extends AbstractSet<E> implements Set<E>, Cloneable, Serializable {
    public static final Object A03 = new C20201Bh();
    private static final Object[] A04 = new Object[0];
    private static final long serialVersionUID = 0;
    public transient int A00;
    public transient int A01;
    public transient Object[] A02;
    private int mSize;

    private static int A01(Object obj, Object[] objArr) {
        int length = objArr.length;
        int A002 = A00(obj, length);
        int i = A002;
        do {
            Object obj2 = objArr[i];
            if (obj2 == null || obj2 == obj || obj2.equals(obj)) {
                return i;
            }
            i++;
            if (i == length) {
                i = 0;
                continue;
            }
        } while (i != A002);
        return i;
    }

    public void clear() {
        this.mSize = 0;
        Arrays.fill(this.A02, (Object) null);
        this.A00++;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof Set) {
                Set set = (Set) obj;
                if (size() == set.size()) {
                    Object[] objArr = this.A02;
                    int length = objArr.length;
                    int i = 0;
                    while (i < length) {
                        Object obj2 = objArr[i];
                        if (obj2 == null || set.contains(obj2)) {
                            i++;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int A022 = A02(-1);
        int i = 0;
        while (A022 >= 0) {
            i += this.A02[A022].hashCode();
            A022 = A02(A022);
        }
        return i;
    }

    public int A02(int i) {
        Object[] objArr;
        do {
            i++;
            objArr = this.A02;
            if (i >= objArr.length) {
                return Integer.MIN_VALUE;
            }
        } while (objArr[i] == null);
        return i;
    }

    public Object A03(int i) {
        Object obj = this.A02[i];
        if (obj == null) {
            throw new NoSuchElementException();
        } else if (obj == A03) {
            return null;
        } else {
            return obj;
        }
    }

    public boolean add(Object obj) {
        if (this.mSize >= this.A01) {
            this.A00++;
            int i = r5 << 1;
            if (i == 0) {
                i = 2;
            }
            Object[] objArr = new Object[i];
            for (Object obj2 : this.A02) {
                if (obj2 != null) {
                    objArr[A01(obj2, objArr)] = obj2;
                }
            }
            this.A02 = objArr;
            this.A01 = (int) (((float) i) * 0.75f);
        }
        if (obj == null) {
            obj = A03;
        }
        int length = this.A02.length;
        int A002 = A00(obj, length);
        while (true) {
            Object[] objArr2 = this.A02;
            Object obj3 = objArr2[A002];
            if (obj3 == null) {
                this.mSize++;
                this.A00++;
                objArr2[A002] = obj;
                return true;
            } else if (obj3 == obj || obj3.equals(obj)) {
                return false;
            } else {
                A002++;
                if (A002 == length) {
                    A002 = 0;
                }
            }
        }
        return false;
    }

    public boolean contains(Object obj) {
        Object[] objArr = this.A02;
        if (objArr.length == 0) {
            return false;
        }
        if (obj == null) {
            obj = A03;
        }
        if (objArr[A01(obj, objArr)] != null) {
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        if (this.mSize == 0) {
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new C193317s(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
        if (r0 != false) goto L_0x003b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean remove(java.lang.Object r8) {
        /*
            r7 = this;
            java.lang.Object[] r1 = r7.A02
            int r0 = r1.length
            r2 = 0
            if (r0 == 0) goto L_0x0049
            if (r8 != 0) goto L_0x000a
            java.lang.Object r8 = X.C13770s2.A03
        L_0x000a:
            int r6 = A01(r8, r1)
            java.lang.Object[] r1 = r7.A02
            r0 = r1[r6]
            if (r0 == 0) goto L_0x0049
            int r5 = r1.length
        L_0x0015:
            r4 = r6
        L_0x0016:
            r3 = 1
            int r6 = r6 + r3
            if (r6 < r5) goto L_0x001b
            r6 = 0
        L_0x001b:
            java.lang.Object[] r1 = r7.A02
            r2 = r1[r6]
            if (r2 != 0) goto L_0x002f
            int r0 = r7.A00
            int r0 = r0 + r3
            r7.A00 = r0
            int r0 = r7.mSize
            int r0 = r0 - r3
            r7.mSize = r0
            r0 = 0
            r1[r4] = r0
            return r3
        L_0x002f:
            int r1 = A00(r2, r5)
            r0 = 0
            if (r1 <= r4) goto L_0x0037
            r0 = 1
        L_0x0037:
            if (r6 >= r4) goto L_0x0044
            if (r0 != 0) goto L_0x003d
        L_0x003b:
            if (r1 > r6) goto L_0x0047
        L_0x003d:
            if (r3 != 0) goto L_0x0016
            java.lang.Object[] r0 = r7.A02
            r0[r4] = r2
            goto L_0x0015
        L_0x0044:
            if (r0 == 0) goto L_0x0047
            goto L_0x003b
        L_0x0047:
            r3 = 0
            goto L_0x003d
        L_0x0049:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13770s2.remove(java.lang.Object):boolean");
    }

    public int size() {
        return this.mSize;
    }

    private static int A00(Object obj, int i) {
        int hashCode = obj.hashCode();
        int i2 = hashCode + ((hashCode << 15) ^ -12931);
        int i3 = i2 ^ (i2 >>> 10);
        int i4 = i3 + (i3 << 3);
        int i5 = i4 ^ (i4 >>> 6);
        int i6 = i5 + (i5 << 2) + (i5 << 14);
        return ((i6 ^ (i6 >>> 16)) & Integer.MAX_VALUE) % i;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        objectInputStream.readFloat();
        this.A02 = new Object[readInt];
        this.mSize = 0;
        int readInt2 = objectInputStream.readInt();
        for (int i = 0; i < readInt2; i++) {
            add(objectInputStream.readObject());
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.A02.length);
        objectOutputStream.writeFloat(0.75f);
        objectOutputStream.writeInt(size());
        int A022 = A02(-1);
        while (A022 >= 0) {
            Object obj = this.A02[A022];
            if (obj == A03) {
                obj = null;
            }
            objectOutputStream.writeObject(obj);
            A022 = A02(A022);
        }
    }

    public Object clone() {
        try {
            C13770s2 r4 = (C13770s2) super.clone();
            Object[] objArr = new Object[this.A02.length];
            r4.A02 = objArr;
            Object[] objArr2 = this.A02;
            System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
            return r4;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder("{");
        boolean z = true;
        int A022 = A02(-1);
        while (A022 >= 0) {
            Object obj = this.A02[A022];
            if (!z) {
                sb.append(", ");
            }
            if (obj == A03) {
                obj = "null";
            }
            sb.append(obj);
            z = false;
            A022 = A02(A022);
        }
        sb.append('}');
        return sb.toString();
    }

    public C13770s2() {
        this(0);
    }

    private C13770s2(int i) {
        if (i >= 0) {
            this.mSize = 0;
            this.A00 = 0;
            this.A01 = i;
            float f = ((float) i) / 0.75f;
            int i2 = (int) f;
            if (i2 >= 0) {
                this.A02 = i2 == 0 ? A04 : new Object[i2];
                return;
            }
            throw new RuntimeException(StringFormatUtil.formatStrLocaleSafe("adjustedCapacity = %d, capacity = %d, LOAD_FACTOR = %s, (capacity / LOAD_FACTOR) = %s", Integer.valueOf(i2), Integer.valueOf(i), Float.toString(0.75f), Float.toString(f)));
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("capacity < 0: ", i));
    }

    public Object[] toArray() {
        int size = size();
        if (size == 0) {
            return A04;
        }
        Object[] objArr = new Object[size];
        int i = 0;
        int A022 = A02(-1);
        while (A022 >= 0) {
            int i2 = i + 1;
            Object obj = this.A02[A022];
            if (obj == A03) {
                obj = null;
            }
            objArr[i] = obj;
            A022 = A02(A022);
            i = i2;
        }
        return objArr;
    }

    public Object[] toArray(Object[] objArr) {
        int size = size();
        if (objArr.length < size) {
            objArr = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), size);
        }
        int i = 0;
        int A022 = A02(-1);
        while (A022 >= 0) {
            int i2 = i + 1;
            Object obj = this.A02[A022];
            if (obj == A03) {
                obj = null;
            }
            objArr[i] = obj;
            A022 = A02(A022);
            i = i2;
        }
        if (objArr.length > size) {
            objArr[size] = null;
        }
        return objArr;
    }
}
