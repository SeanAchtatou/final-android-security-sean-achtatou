package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1kt  reason: invalid class name and case insensitive filesystem */
public final class C31971kt extends C17770zR {
    public AnonymousClass0UN A00;
    public AnonymousClass0US A01;
    public AnonymousClass0US A02;
    @Comparable(type = 13)
    public C15340v8 A03;
    @Comparable(type = 13)
    public InboxUnitMontageAndActiveNowItem A04;
    @Comparable(type = 13)
    public AnonymousClass1BI A05;
    @Comparable(type = 14)
    public C33061mm A06 = new C33061mm();
    @Comparable(type = 13)
    public AnonymousClass1BF A07;
    @Comparable(type = 13)
    public MigColorScheme A08;

    public C31971kt(Context context) {
        super("MontageAndActiveNowComponent");
        AnonymousClass1XX r2 = AnonymousClass1XX.get(context);
        this.A00 = new AnonymousClass0UN(10, r2);
        this.A01 = AnonymousClass0UQ.A00(AnonymousClass1Y3.A5R, r2);
        this.A02 = AnonymousClass0UQ.A00(AnonymousClass1Y3.AEL, r2);
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0425, code lost:
        if (r2 == false) goto L_0x0427;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x0812, code lost:
        if (r17.A00.Aem(285838663554961L) == false) goto L_0x0814;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x02e8, code lost:
        if (r2.A07 != false) goto L_0x02ea;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0386  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0412  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0434  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0476 A[LOOP:14: B:146:0x0470->B:148:0x0476, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x04b0  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x04f2 A[Catch:{ all -> 0x08eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0517 A[Catch:{ all -> 0x08eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x054c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x05a2  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x05c0  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x061a  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x065f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x017b  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x0743  */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x0466 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x0432 A[EDGE_INSN: B:312:0x0432->B:129:0x0432 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x01e2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C17770zR A0O(X.AnonymousClass0p4 r44) {
        /*
            r43 = this;
            r2 = r43
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageAndActiveNowItem r0 = r2.A04
            r42 = r0
            X.0v8 r0 = r2.A03
            r41 = r0
            X.1BF r0 = r2.A07
            r40 = r0
            X.1BI r0 = r2.A05
            r39 = r0
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r2.A08
            r38 = r0
            int r1 = X.AnonymousClass1Y3.AYw
            X.0UN r3 = r2.A00
            r0 = 5
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.1HM r0 = (X.AnonymousClass1HM) r0
            int r4 = X.AnonymousClass1Y3.BER
            r1 = 1
            java.lang.Object r19 = X.AnonymousClass1XX.A02(r1, r4, r3)
            r1 = r19
            X.0bZ r1 = (X.C06480bZ) r1
            r19 = r1
            int r4 = X.AnonymousClass1Y3.BRG
            r1 = 6
            java.lang.Object r18 = X.AnonymousClass1XX.A02(r1, r4, r3)
            r1 = r18
            X.0yl r1 = (X.C17350yl) r1
            r18 = r1
            int r4 = X.AnonymousClass1Y3.AQC
            r1 = 8
            java.lang.Object r21 = X.AnonymousClass1XX.A02(r1, r4, r3)
            r1 = r21
            X.18s r1 = (X.C195518s) r1
            r21 = r1
            X.0US r1 = r2.A02
            r37 = r1
            X.0US r1 = r2.A01
            r36 = r1
            int r4 = X.AnonymousClass1Y3.AhV
            r1 = 0
            java.lang.Object r23 = X.AnonymousClass1XX.A02(r1, r4, r3)
            r1 = r23
            X.1T1 r1 = (X.AnonymousClass1T1) r1
            r23 = r1
            int r4 = X.AnonymousClass1Y3.A96
            r1 = 9
            java.lang.Object r22 = X.AnonymousClass1XX.A02(r1, r4, r3)
            r1 = r22
            X.0vJ r1 = (X.C15450vJ) r1
            r22 = r1
            int r4 = X.AnonymousClass1Y3.BUL
            r1 = 2
            java.lang.Object r17 = X.AnonymousClass1XX.A02(r1, r4, r3)
            r1 = r17
            X.1HN r1 = (X.AnonymousClass1HN) r1
            r17 = r1
            int r4 = X.AnonymousClass1Y3.AQb
            r1 = 7
            java.lang.Object r20 = X.AnonymousClass1XX.A02(r1, r4, r3)
            r1 = r20
            X.1rX r1 = (X.C35631rX) r1
            r20 = r1
            X.1mm r1 = r2.A06
            boolean r1 = r1.storiesTrayAccessible
            r16 = r1
            r13 = r1 ^ 1
            java.util.LinkedList r6 = new java.util.LinkedList
            r1 = r42
            com.google.common.collect.ImmutableList r1 = r1.A01
            r6.<init>(r1)
            java.util.LinkedList r3 = new java.util.LinkedList
            r1 = r42
            com.google.common.collect.ImmutableList r1 = r1.A04
            r3.<init>(r1)
            java.util.Collections.sort(r3)
            r1 = r42
            com.google.common.collect.ImmutableList r5 = r1.A03
            int r2 = X.AnonymousClass1Y3.BRG
            X.0UN r1 = r0.A00
            r26 = 1
            r7 = r26
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r7, r2, r1)
            X.0yl r1 = (X.C17350yl) r1
            int r4 = X.AnonymousClass1Y3.AOJ
            X.0UN r2 = r1.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r7, r4, r2)
            X.1Yd r7 = (X.C25051Yd) r7
            r1 = 563976452309638(0x200ef001a0286, double:2.786413901496163E-309)
            r4 = 40
            int r2 = r7.AqL(r1, r4)
            r1 = r42
            com.google.common.collect.ImmutableList r1 = r1.A03
            int r1 = r1.size()
            int r1 = java.lang.Math.min(r2, r1)
            r8 = 0
            com.google.common.collect.ImmutableList r25 = r5.subList(r8, r1)
            java.util.LinkedHashMap r2 = new java.util.LinkedHashMap
            r2.<init>()
            X.1Xv r7 = r25.iterator()
        L_0x00e4:
            boolean r1 = r7.hasNext()
            if (r1 == 0) goto L_0x00fc
            java.lang.Object r5 = r7.next()
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r5 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r5
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r4 = r5.A02
            boolean r1 = r4.A08
            if (r1 != 0) goto L_0x00e4
            com.facebook.user.model.UserKey r1 = r4.A03
            r2.put(r1, r5)
            goto L_0x00e4
        L_0x00fc:
            com.google.common.collect.ImmutableList$Builder r1 = com.google.common.collect.ImmutableList.builder()
            r4 = r42
            com.google.common.collect.ImmutableList r4 = r4.A03
            boolean r5 = X.C013509w.A01(r4)
            if (r5 == 0) goto L_0x01e6
            X.1Xv r7 = r4.iterator()
        L_0x010e:
            boolean r4 = r7.hasNext()
            if (r4 == 0) goto L_0x01e6
            java.lang.Object r5 = r7.next()
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r5 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r5
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r4 = r5.A02
            boolean r4 = r4.A08
            if (r4 == 0) goto L_0x010e
        L_0x0120:
            r24 = 0
            if (r5 != 0) goto L_0x01e2
            r9 = r24
        L_0x0126:
            r4 = r42
            X.1ck r7 = r4.A02
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r11 = new com.facebook.messaging.montage.inboxunit.InboxMontageItem
            r31 = 0
            r34 = 0
            r35 = 0
            r27 = r11
            X.12J r10 = new X.12J
            r10.<init>()
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r4 = new com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem
            r4.<init>(r10)
            r29 = 1
            r32 = 0
            r28 = r7
            r30 = r9
            r33 = r4
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r35)
            int r7 = X.AnonymousClass1Y3.BHw
            X.0UN r4 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r8, r7, r4)
            X.1GE r4 = (X.AnonymousClass1GE) r4
            boolean r4 = r4.A02()
            if (r4 == 0) goto L_0x0169
            X.1HP r4 = new X.1HP
            java.lang.Integer r9 = X.AnonymousClass07B.A01
            r10 = 0
            r12 = r38
            r8 = r4
            r8.<init>(r9, r10, r11, r12, r13)
            r1.add(r4)
        L_0x0169:
            if (r5 == 0) goto L_0x0175
            r4 = r5
            r5 = r38
            X.1HP r4 = X.AnonymousClass1HP.A00(r4, r5)
            r1.add(r4)
        L_0x0175:
            r4 = r42
            com.facebook.messaging.montage.model.MontageInboxNuxItem r5 = r4.A00
            if (r5 == 0) goto L_0x01a3
            X.1ck r7 = r4.A02
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r9 = new com.facebook.messaging.montage.inboxunit.InboxMontageItem
            r30 = 0
            r27 = r9
            X.12J r8 = new X.12J
            r8.<init>()
            com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem r4 = new com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem
            r4.<init>(r8)
            r29 = 3
            r28 = r7
            r31 = r5
            r33 = r4
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r35)
            X.1HP r24 = new X.1HP
            java.lang.Integer r4 = X.AnonymousClass07B.A0Y
            r7 = r24
            r10 = r38
            r7.<init>(r4, r9, r10)
        L_0x01a3:
            r4 = r42
            com.google.common.collect.ImmutableList r8 = r4.A02
            int r7 = X.AnonymousClass1Y3.A3B
            X.0UN r5 = r0.A00
            r4 = 2
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r7, r5)
            X.1lj r4 = (X.C32421lj) r4
            X.1Yd r7 = r4.A00
            r4 = 282325381809462(0x100c600180536, double:1.394872721010684E-309)
            boolean r4 = r7.Aem(r4)
            if (r4 == 0) goto L_0x0377
            boolean r4 = X.C013509w.A01(r8)
            if (r4 == 0) goto L_0x0377
            java.util.LinkedHashMap r9 = new java.util.LinkedHashMap
            r9.<init>()
            java.util.Iterator r5 = r6.iterator()
        L_0x01ce:
            boolean r3 = r5.hasNext()
            if (r3 == 0) goto L_0x01e9
            java.lang.Object r4 = r5.next()
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r4 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem) r4
            com.facebook.user.model.UserKey r3 = r4.A0K()
            r9.put(r3, r4)
            goto L_0x01ce
        L_0x01e2:
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r9 = r5.A02
            goto L_0x0126
        L_0x01e6:
            r5 = 0
            goto L_0x0120
        L_0x01e9:
            java.util.Set r3 = r2.entrySet()
            java.util.Iterator r5 = r3.iterator()
        L_0x01f1:
            boolean r3 = r5.hasNext()
            if (r3 == 0) goto L_0x0209
            java.lang.Object r3 = r5.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r4 = r3.getKey()
            java.lang.Object r3 = r3.getValue()
            r9.put(r4, r3)
            goto L_0x01f1
        L_0x0209:
            java.util.ArrayList r10 = new java.util.ArrayList
            int r3 = r8.size()
            r10.<init>(r3)
            X.1Xv r11 = r8.iterator()
        L_0x0216:
            boolean r3 = r11.hasNext()
            if (r3 == 0) goto L_0x024f
            java.lang.Object r3 = r11.next()
            com.facebook.user.model.UserKey r3 = (com.facebook.user.model.UserKey) r3
            java.lang.Object r8 = r9.get(r3)
            com.facebook.messaging.inbox2.items.InboxUnitItem r8 = (com.facebook.messaging.inbox2.items.InboxUnitItem) r8
            boolean r3 = r8 instanceof com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem
            if (r3 == 0) goto L_0x023f
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r8 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem) r8
            r7 = r38
            X.1HP r5 = new X.1HP
            java.lang.Integer r4 = X.AnonymousClass07B.A00
            com.google.common.base.Preconditions.checkNotNull(r8)
            r3 = 0
            r5.<init>(r4, r8, r3, r7)
            r10.add(r5)
            goto L_0x0216
        L_0x023f:
            boolean r3 = r8 instanceof com.facebook.messaging.montage.inboxunit.InboxMontageItem
            if (r3 == 0) goto L_0x0216
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r8 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r8
            r4 = r38
            X.1HP r3 = X.AnonymousClass1HP.A00(r8, r4)
            r10.add(r3)
            goto L_0x0216
        L_0x024f:
            int r3 = r6.size()
            if (r3 <= 0) goto L_0x036e
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x036e
            int r4 = X.AnonymousClass1Y3.A3B
            X.0UN r3 = r0.A00
            r2 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)
            X.1lj r2 = (X.C32421lj) r2
            X.1Yd r4 = r2.A00
            r2 = 282325381612851(0x100c600150533, double:1.394872720039296E-309)
            boolean r2 = r4.Aem(r2)
            if (r2 == 0) goto L_0x036e
            com.google.common.collect.ImmutableList$Builder r7 = new com.google.common.collect.ImmutableList$Builder
            r7.<init>()
            com.google.common.collect.ImmutableList$Builder r6 = new com.google.common.collect.ImmutableList$Builder
            r6.<init>()
            r5 = 0
        L_0x027e:
            int r2 = r10.size()
            if (r5 >= r2) goto L_0x02a8
            java.lang.Object r4 = r10.get(r5)
            X.1HP r4 = (X.AnonymousClass1HP) r4
            java.lang.Integer r3 = r4.A04
            java.lang.Integer r2 = X.AnonymousClass07B.A00
            if (r3 != r2) goto L_0x029b
            X.3hh r2 = new X.3hh
            r2.<init>(r4, r5)
            r7.add(r2)
        L_0x0298:
            int r5 = r5 + 1
            goto L_0x027e
        L_0x029b:
            java.lang.Integer r2 = X.AnonymousClass07B.A0N
            if (r3 != r2) goto L_0x0298
            X.3hh r2 = new X.3hh
            r2.<init>(r4, r5)
            r6.add(r2)
            goto L_0x0298
        L_0x02a8:
            java.util.ArrayList r11 = new java.util.ArrayList
            int r2 = r10.size()
            r11.<init>(r2)
            com.google.common.collect.ImmutableList r14 = r7.build()
            com.google.common.collect.ImmutableList r12 = r6.build()
            r9 = 0
            r8 = 0
            r7 = 0
        L_0x02bc:
            r6 = 0
        L_0x02bd:
            int r2 = r14.size()
            if (r9 >= r2) goto L_0x0340
            int r2 = r12.size()
            if (r8 >= r2) goto L_0x0340
            java.lang.Object r5 = r14.get(r9)
            X.3hh r5 = (X.C74353hh) r5
            java.lang.Object r4 = r12.get(r8)
            X.3hh r4 = (X.C74353hh) r4
            X.1HP r13 = r4.A01
            java.lang.Integer r3 = r13.A04
            java.lang.Integer r2 = X.AnonymousClass07B.A0N
            if (r3 != r2) goto L_0x02ea
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r2 = r13.A00
            if (r2 == 0) goto L_0x02ea
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r2 = r2.A02
            if (r2 == 0) goto L_0x02ea
            boolean r2 = r2.A07
            r3 = 1
            if (r2 == 0) goto L_0x02eb
        L_0x02ea:
            r3 = 0
        L_0x02eb:
            if (r3 != 0) goto L_0x0340
            int r3 = r5.A00
            int r2 = r4.A00
            r13 = 2
            if (r3 >= r2) goto L_0x030c
            int r3 = X.AnonymousClass1Y3.A3B
            X.0UN r2 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r13, r3, r2)
            X.1lj r2 = (X.C32421lj) r2
            X.1Yd r15 = r2.A00
            r2 = 563800357601865(0x200c6000a0249, double:2.78554387804091E-309)
            long r2 = r15.At0(r2)
            int r15 = (int) r2
            if (r7 < r15) goto L_0x032a
        L_0x030c:
            int r3 = r4.A00
            int r2 = r5.A00
            if (r3 >= r2) goto L_0x0334
            int r3 = X.AnonymousClass1Y3.A3B
            X.0UN r2 = r0.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r13, r3, r2)
            X.1lj r2 = (X.C32421lj) r2
            X.1Yd r13 = r2.A00
            r2 = 563800357732939(0x200c6000c024b, double:2.785543878688503E-309)
            long r2 = r13.At0(r2)
            int r13 = (int) r2
            if (r6 < r13) goto L_0x0334
        L_0x032a:
            X.1HP r2 = r5.A01
            r11.add(r2)
            int r7 = r7 + 1
            int r9 = r9 + 1
            goto L_0x02bc
        L_0x0334:
            X.1HP r2 = r4.A01
            r11.add(r2)
            int r6 = r6 + 1
            int r8 = r8 + 1
            r7 = 0
            goto L_0x02bd
        L_0x0340:
            int r2 = r14.size()
            if (r9 >= r2) goto L_0x0354
            java.lang.Object r2 = r14.get(r9)
            X.3hh r2 = (X.C74353hh) r2
            X.1HP r2 = r2.A01
            r11.add(r2)
            int r9 = r9 + 1
            goto L_0x0340
        L_0x0354:
            int r2 = r12.size()
            if (r8 >= r2) goto L_0x0368
            java.lang.Object r2 = r12.get(r8)
            X.3hh r2 = (X.C74353hh) r2
            X.1HP r2 = r2.A01
            r11.add(r2)
            int r8 = r8 + 1
            goto L_0x0354
        L_0x0368:
            r10.clear()
            r10.addAll(r11)
        L_0x036e:
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.copyOf(r10)
            r3.toString()
            goto L_0x048a
        L_0x0377:
            java.util.LinkedList r7 = new java.util.LinkedList
            r7.<init>()
            java.util.Iterator r11 = r6.iterator()
        L_0x0380:
            boolean r4 = r11.hasNext()
            if (r4 == 0) goto L_0x03e1
            java.lang.Object r10 = r11.next()
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r10 = (com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem) r10
            com.facebook.user.model.UserKey r9 = r10.A0K()
            if (r9 != 0) goto L_0x03da
            r8 = 0
        L_0x0393:
            if (r8 != 0) goto L_0x03a6
            r8 = r38
            X.1HP r6 = new X.1HP
            java.lang.Integer r5 = X.AnonymousClass07B.A00
            com.google.common.base.Preconditions.checkNotNull(r10)
            r4 = 0
            r6.<init>(r5, r10, r4, r8)
            r7.add(r6)
            goto L_0x0380
        L_0x03a6:
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r4 = r8.A02
            boolean r4 = r4.A07
            if (r4 != 0) goto L_0x0380
            r6 = 1
            int r5 = X.AnonymousClass1Y3.BRG
            X.0UN r4 = r0.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r6, r5, r4)
            X.0yl r4 = (X.C17350yl) r4
            int r6 = X.AnonymousClass1Y3.AOJ
            X.0UN r5 = r4.A00
            r4 = 1
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r4, r6, r5)
            X.1Yd r6 = (X.C25051Yd) r6
            r4 = 282501480646153(0x100ef00670609, double:1.3957427648655E-309)
            boolean r4 = r6.Aem(r4)
            if (r4 != 0) goto L_0x0380
            r2.remove(r9)
            r5 = r38
            X.1HP r4 = X.AnonymousClass1HP.A00(r8, r5)
            r7.add(r4)
            goto L_0x0380
        L_0x03da:
            java.lang.Object r8 = r2.get(r9)
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r8 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r8
            goto L_0x0393
        L_0x03e1:
            java.util.LinkedList r5 = new java.util.LinkedList
            java.util.Collection r2 = r2.values()
            r5.<init>(r2)
            X.1ra r2 = new X.1ra
            r2.<init>()
            java.util.Collections.sort(r5, r2)
            java.util.Iterator r9 = r3.iterator()
            r8 = -1
        L_0x03f7:
            boolean r2 = r9.hasNext()
            if (r2 == 0) goto L_0x046c
            java.lang.Object r2 = r9.next()
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r6 = r2.intValue()
            int r2 = r7.size()
            if (r6 >= r2) goto L_0x046c
            r4 = 0
        L_0x040e:
            int r8 = r8 + 1
            if (r8 > r6) goto L_0x0432
            java.lang.Object r4 = r7.get(r8)
            X.1HP r4 = (X.AnonymousClass1HP) r4
            java.lang.Integer r3 = r4.A04
            java.lang.Integer r2 = X.AnonymousClass07B.A0N
            if (r3 != r2) goto L_0x0427
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r2 = r4.A00
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r2 = r2.A02
            boolean r2 = r2.A07
            r4 = 1
            if (r2 != 0) goto L_0x0428
        L_0x0427:
            r4 = 0
        L_0x0428:
            if (r4 != 0) goto L_0x042f
            java.lang.Integer r2 = X.AnonymousClass07B.A0C
            r4 = 0
            if (r3 != r2) goto L_0x0430
        L_0x042f:
            r4 = 1
        L_0x0430:
            if (r4 == 0) goto L_0x040e
        L_0x0432:
            if (r4 != 0) goto L_0x0466
            r4 = r38
            java.util.Iterator r8 = r5.iterator()
            r3 = -1
        L_0x043b:
            boolean r2 = r8.hasNext()
            if (r2 == 0) goto L_0x046a
            java.lang.Object r2 = r8.next()
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r2 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r2
            int r3 = r3 + 1
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r2 = r2.A02
            if (r2 == 0) goto L_0x043b
            boolean r2 = r2.A07
            if (r2 == 0) goto L_0x043b
        L_0x0451:
            r2 = -1
            if (r3 == r2) goto L_0x0468
            java.lang.Object r2 = r5.get(r3)
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r2 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r2
            r5.remove(r3)
            X.1HP r2 = X.AnonymousClass1HP.A00(r2, r4)
        L_0x0461:
            if (r2 == 0) goto L_0x0466
            r7.add(r6, r2)
        L_0x0466:
            r8 = r6
            goto L_0x03f7
        L_0x0468:
            r2 = 0
            goto L_0x0461
        L_0x046a:
            r3 = -1
            goto L_0x0451
        L_0x046c:
            java.util.Iterator r3 = r5.iterator()
        L_0x0470:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x0486
            java.lang.Object r2 = r3.next()
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r2 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r2
            r5 = r38
            X.1HP r2 = X.AnonymousClass1HP.A00(r2, r5)
            r7.add(r2)
            goto L_0x0470
        L_0x0486:
            com.google.common.collect.ImmutableList r3 = com.google.common.collect.ImmutableList.copyOf(r7)
        L_0x048a:
            if (r24 == 0) goto L_0x04d4
            r2 = r24
            com.facebook.messaging.montage.model.MontageInboxNuxItem r2 = r2.A02
            if (r2 == 0) goto L_0x04d4
            boolean r2 = r2.A01
            if (r2 == 0) goto L_0x04cb
            r1.addAll(r3)
            r2 = r24
            r1.add(r2)
        L_0x049e:
            int r3 = X.AnonymousClass1Y3.BRG
            X.0UN r2 = r0.A00
            r4 = r26
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.0yl r2 = (X.C17350yl) r2
            boolean r2 = r2.A0E()
            if (r2 == 0) goto L_0x04bd
            X.1HP r4 = new X.1HP
            java.lang.Integer r3 = X.AnonymousClass07B.A0i
            r2 = 0
            r8 = r38
            r4.<init>(r3, r2, r2, r8)
            r1.add(r4)
        L_0x04bd:
            com.google.common.collect.ImmutableList r2 = r1.build()
            java.util.concurrent.locks.ReadWriteLock r1 = r0.A03
            java.util.concurrent.locks.Lock r9 = r1.writeLock()
            r9.lock()
            goto L_0x04d8
        L_0x04cb:
            r2 = r24
            r1.add(r2)
            r1.addAll(r3)
            goto L_0x049e
        L_0x04d4:
            r1.addAll(r3)
            goto L_0x049e
        L_0x04d8:
            java.util.List r1 = r0.A02     // Catch:{ all -> 0x08eb }
            r1.clear()     // Catch:{ all -> 0x08eb }
            java.util.List r7 = r0.A02     // Catch:{ all -> 0x08eb }
            com.google.common.collect.ImmutableList$Builder r8 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x08eb }
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ all -> 0x08eb }
            r6.<init>()     // Catch:{ all -> 0x08eb }
            X.1Xv r4 = r2.iterator()     // Catch:{ all -> 0x08eb }
        L_0x04ec:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x08eb }
            if (r1 == 0) goto L_0x050d
            java.lang.Object r3 = r4.next()     // Catch:{ all -> 0x08eb }
            X.1HP r3 = (X.AnonymousClass1HP) r3     // Catch:{ all -> 0x08eb }
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r1 = r3.A00     // Catch:{ all -> 0x08eb }
            if (r1 == 0) goto L_0x04ec
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r1 = r1.A02     // Catch:{ all -> 0x08eb }
            if (r1 == 0) goto L_0x04ec
            r8.add(r1)     // Catch:{ all -> 0x08eb }
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r1 = r3.A00     // Catch:{ all -> 0x08eb }
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r1 = r1.A02     // Catch:{ all -> 0x08eb }
            com.facebook.user.model.UserKey r1 = r1.A03     // Catch:{ all -> 0x08eb }
            r6.add(r1)     // Catch:{ all -> 0x08eb }
            goto L_0x04ec
        L_0x050d:
            X.1Xv r5 = r25.iterator()     // Catch:{ all -> 0x08eb }
        L_0x0511:
            boolean r1 = r5.hasNext()     // Catch:{ all -> 0x08eb }
            if (r1 == 0) goto L_0x0533
            java.lang.Object r4 = r5.next()     // Catch:{ all -> 0x08eb }
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r4 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r4     // Catch:{ all -> 0x08eb }
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r3 = r4.A02     // Catch:{ all -> 0x08eb }
            if (r3 == 0) goto L_0x0511
            boolean r1 = r3.A08     // Catch:{ all -> 0x08eb }
            if (r1 != 0) goto L_0x0511
            com.facebook.user.model.UserKey r1 = r3.A03     // Catch:{ all -> 0x08eb }
            boolean r1 = r6.contains(r1)     // Catch:{ all -> 0x08eb }
            if (r1 != 0) goto L_0x0511
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r1 = r4.A02     // Catch:{ all -> 0x08eb }
            r8.add(r1)     // Catch:{ all -> 0x08eb }
            goto L_0x0511
        L_0x0533:
            com.google.common.collect.ImmutableList r1 = r8.build()     // Catch:{ all -> 0x08eb }
            r7.addAll(r1)     // Catch:{ all -> 0x08eb }
            r9.unlock()
            X.10R r1 = r0.A01
            X.1Yd r1 = r1.A00
            r3 = 282325383054663(0x100c6002b0547, double:1.394872727162794E-309)
            boolean r1 = r1.Aem(r3)
            if (r1 == 0) goto L_0x0593
            X.1Xv r8 = r2.iterator()
            r7 = 0
            r6 = 0
            r5 = 0
        L_0x0553:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x057a
            java.lang.Object r4 = r8.next()
            X.1HP r4 = (X.AnonymousClass1HP) r4
            java.lang.Integer r3 = r4.A04
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            if (r3 != r1) goto L_0x0568
            int r6 = r6 + 1
            goto L_0x0553
        L_0x0568:
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            if (r3 == r1) goto L_0x0553
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r1 = r4.A00
            if (r1 == 0) goto L_0x0553
            boolean r1 = r1.A06
            if (r1 == 0) goto L_0x0577
            int r5 = r5 + 1
            goto L_0x0553
        L_0x0577:
            int r7 = r7 + 1
            goto L_0x0553
        L_0x057a:
            r4 = 4
            int r3 = X.AnonymousClass1Y3.A6A
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r1)
            X.4Lp r3 = (X.C88684Lp) r3
            java.lang.Object r1 = r3.A06
            monitor-enter(r1)
            r3.A01 = r5     // Catch:{ all -> 0x0590 }
            r3.A02 = r7     // Catch:{ all -> 0x0590 }
            r3.A00 = r6     // Catch:{ all -> 0x0590 }
            monitor-exit(r1)     // Catch:{ all -> 0x0590 }
            goto L_0x0593
        L_0x0590:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0590 }
            throw r0
        L_0x0593:
            X.10R r1 = r0.A01
            X.1Yd r1 = r1.A00
            r3 = 282325381743925(0x100c600170535, double:1.39487272068689E-309)
            boolean r1 = r1.Aem(r3)
            if (r1 == 0) goto L_0x05b9
            r4 = 3
            int r3 = X.AnonymousClass1Y3.Agi
            X.0UN r1 = r0.A00
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r1)
            X.1HR r3 = (X.AnonymousClass1HR) r3
            java.lang.String r1 = r2.toString()
            monitor-enter(r3)
            r3.A00 = r1     // Catch:{ all -> 0x05b5 }
            goto L_0x05b8
        L_0x05b5:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x05b8:
            monitor-exit(r3)
        L_0x05b9:
            r4 = 0
        L_0x05ba:
            int r1 = r2.size()
            if (r4 >= r1) goto L_0x060b
            java.lang.Object r5 = r2.get(r4)
            X.1HP r5 = (X.AnonymousClass1HP) r5
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r1 = r5.A00
            if (r1 == 0) goto L_0x05e7
            r1.A0C(r4)
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r3 = r5.A00
            r1 = r42
            com.facebook.messaging.inbox2.items.InboxTrackableItem r1 = r1.A07()
            int r1 = r1.A00
            r3.A0B(r1)
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r3 = r5.A00
            r1 = r42
            com.facebook.messaging.inbox2.items.InboxTrackableItem r1 = r1.A07()
            int r1 = r1.A02
            r3.A0D(r1)
        L_0x05e7:
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r1 = r5.A01
            if (r1 == 0) goto L_0x0608
            r1.A0C(r4)
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r3 = r5.A01
            r1 = r42
            com.facebook.messaging.inbox2.items.InboxTrackableItem r1 = r1.A07()
            int r1 = r1.A00
            r3.A0B(r1)
            com.facebook.messaging.montage.inboxunit.activenow.InboxUnitMontageActiveNowItem r3 = r5.A01
            r1 = r42
            com.facebook.messaging.inbox2.items.InboxTrackableItem r1 = r1.A07()
            int r1 = r1.A02
            r3.A0D(r1)
        L_0x0608:
            int r4 = r4 + 1
            goto L_0x05ba
        L_0x060b:
            r1 = r42
            com.google.common.collect.ImmutableList r1 = r1.A03
            X.1Xv r3 = r1.iterator()
            r7 = 1
        L_0x0614:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x062b
            java.lang.Object r1 = r3.next()
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r1 = (com.facebook.messaging.montage.inboxunit.InboxMontageItem) r1
            com.facebook.messaging.montage.model.BasicMontageThreadInfo r1 = r1.A02
            if (r1 == 0) goto L_0x0614
            int r1 = r1.A00
            int r7 = java.lang.Math.max(r7, r1)
            goto L_0x0614
        L_0x062b:
            r4 = 0
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r4)
            java.lang.String r1 = "montage_and_active_now_hscroll_perf"
            r8 = r23
            X.1T0 r30 = r8.A00(r3, r1)
            r3 = 5505210(0x5400ba, float:7.714442E-39)
            r1 = r22
            X.0vI r31 = r1.A00(r3)
            X.1nR r9 = new X.1nR
            r22 = r9
            r23 = r40
            r24 = r18
            r25 = r39
            r26 = r36
            r27 = r21
            r28 = r37
            r29 = r0
            r22.<init>(r23, r24, r25, r26, r27, r28, r29)
            int r1 = r2.size()
            r0 = 1
            r3 = r44
            if (r1 != r0) goto L_0x0743
            java.lang.Object r0 = r2.get(r4)
            X.1HP r0 = (X.AnonymousClass1HP) r0
            com.facebook.messaging.montage.inboxunit.InboxMontageItem r5 = r0.A00
            r7 = r3
            X.1wd r4 = X.C16980y8.A00(r3)
            r6 = 1065353216(0x3f800000, float:1.0)
            r4.A1n(r6)
            X.0uO r0 = X.C14940uO.CENTER
            r4.A3D(r0)
            r0 = 16843534(0x101030e, float:2.369575E-38)
            r4.A22(r0)
            X.10G r0 = X.AnonymousClass10G.A09
            r1 = 2132148239(0x7f16000f, float:1.993845E38)
            r4.A2h(r0, r1)
            X.10G r0 = X.AnonymousClass10G.A04
            r4.A2h(r0, r1)
            java.lang.Class<X.1kt> r2 = X.C31971kt.class
            java.lang.Object[] r1 = new java.lang.Object[]{r3, r5, r9}
            r0 = -352845993(0xffffffffeaf7ff57, float:-1.4990524E26)
            X.10N r0 = X.C17780zS.A0E(r2, r3, r0, r1)
            r4.A2y(r0)
            r1 = 1
            java.lang.String r0 = "migColorScheme"
            java.lang.String[] r5 = new java.lang.String[]{r0}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r1)
            X.1Lg r1 = new X.1Lg
            android.content.Context r0 = r3.A09
            r1.<init>(r0)
            X.0zR r0 = r3.A04
            if (r0 == 0) goto L_0x06b4
            java.lang.String r0 = r0.A06
            r1.A07 = r0
        L_0x06b4:
            r2.clear()
            r0 = r38
            r1.A01 = r0
            r0 = 0
            r2.set(r0)
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r2, r5)
            r4.A3A(r1)
            X.1we r3 = X.AnonymousClass11D.A00(r3)
            r3.A1n(r6)
            X.10G r1 = X.AnonymousClass10G.A04
            r0 = 2132148239(0x7f16000f, float:1.993845E38)
            r3.A2h(r1, r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r1 = X.C14910uL.A02(r7)
            r5 = 0
            r1.A3n(r5)
            r0 = 2131828685(0x7f111fcd, float:1.9290318E38)
            r1.A3N(r0)
            r0 = 2132148239(0x7f16000f, float:1.993845E38)
            r1.A3Q(r0)
            X.0y3 r0 = r38.AzL()
            int r0 = r0.AhV()
            r1.A3K(r0)
            android.text.TextUtils$TruncateAt r0 = android.text.TextUtils.TruncateAt.END
            r1.A3X(r0)
            r2 = 3
            r1.A3I(r2)
            X.0uL r0 = r1.A35()
            r3.A3A(r0)
            com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000 r1 = X.C14910uL.A02(r7)
            r1.A3n(r5)
            r0 = 2131828682(0x7f111fca, float:1.9290312E38)
            r1.A3N(r0)
            r0 = 2132148380(0x7f16009c, float:1.9938736E38)
            r1.A3Q(r0)
            X.0y3 r0 = r38.B5U()
            int r0 = r0.Aha()
            r1.A3M(r0)
            android.text.TextUtils$TruncateAt r0 = android.text.TextUtils.TruncateAt.END
            r1.A3X(r0)
            r1.A3I(r2)
            java.lang.String r0 = "android.widget.Button"
            r1.A2n(r0)
            X.0uL r0 = r1.A35()
            r3.A3A(r0)
            r4.A39(r3)
            r0 = 1118568448(0x42ac0000, float:86.0)
            r4.A1p(r0)
            X.0zR r0 = r4.A31()
            return r0
        L_0x0743:
            android.content.res.Resources r1 = r3.A03()
            X.3go r4 = X.C58042t4.A02(r3)
            java.lang.String r0 = "story_and_active_now_unit"
            r4.A2p(r0)
            X.2t3 r8 = new X.2t3
            X.2yd r10 = new X.2yd
            r10.<init>()
            r6 = 0
            r10.A06 = r6
            r0 = 1073741824(0x40000000, float:2.0)
            r10.A01(r0)
            X.3jr r5 = new X.3jr
            r0 = r19
            r5.<init>(r0)
            r10.A02 = r5
            X.2t7 r5 = r10.A00()
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r8.<init>(r6, r6, r0, r5)
            r4.A37(r8)
            X.2t4 r0 = r4.A01
            r0.A0T = r6
            X.3m1 r0 = new X.3m1
            r35 = r3
            r29 = r0
            r32 = r18
            r33 = r20
            r34 = r7
            r29.<init>(r30, r31, r32, r33, r34, r35)
            r4.A34(r0)
            X.10G r5 = X.AnonymousClass10G.BOTTOM
            X.1JQ r0 = X.AnonymousClass1JQ.MEDIUM
            int r0 = r0.B3A()
            float r0 = (float) r0
            r4.A2X(r5, r0)
            X.1GA r11 = new X.1GA
            r11.<init>(r3)
            r12 = 5
            java.lang.String r10 = "activeNowListener"
            java.lang.String r8 = "data"
            java.lang.String r7 = "impressionTracker"
            java.lang.String r5 = "migColorScheme"
            java.lang.String r0 = "montageListener"
            java.lang.String[] r8 = new java.lang.String[]{r10, r8, r7, r5, r0}
            java.util.BitSet r7 = new java.util.BitSet
            r7.<init>(r12)
            X.3l6 r5 = new X.3l6
            android.content.Context r0 = r11.A09
            r5.<init>(r0)
            r7.clear()
            r5.A05 = r2
            r0 = 1
            r7.set(r0)
            r0 = r41
            r5.A01 = r0
            r0 = 2
            r7.set(r0)
            r5.A03 = r9
            r0 = 4
            r7.set(r0)
            r0 = r39
            r5.A02 = r0
            r7.set(r6)
            r0 = r38
            r5.A04 = r0
            r0 = 3
            r7.set(r0)
            X.AnonymousClass1CY.A00(r12, r7, r8)
            r4.A36(r5)
            r0 = 2132148229(0x7f160005, float:1.993843E38)
            int r7 = r1.getDimensionPixelSize(r0)
            X.3m2 r1 = new X.3m2
            r1.<init>(r6, r7, r6, r6)
            X.2t4 r0 = r4.A01
            r0.A09 = r1
            r0 = 1118568448(0x42ac0000, float:86.0)
            r4.A1p(r0)
            r7 = 1120403456(0x42c80000, float:100.0)
            r4.A20(r7)
            android.content.Context r0 = r3.A09
            boolean r0 = X.AnonymousClass3RG.A01(r0)
            if (r0 == 0) goto L_0x0814
            r0 = r17
            X.1Yd r5 = r0.A00
            r0 = 285838663554961(0x103f800011791, double:1.412230639156806E-309)
            boolean r1 = r5.Aem(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0815
        L_0x0814:
            r0 = 0
        L_0x0815:
            if (r0 == 0) goto L_0x08e6
            r0 = 1
            if (r16 == 0) goto L_0x081b
            r0 = 4
        L_0x081b:
            r4.A29(r0)
            X.1we r5 = X.AnonymousClass11D.A00(r3)
            r5.A39(r4)
            if (r16 == 0) goto L_0x08e3
            X.1wd r4 = X.C16980y8.A00(r3)
            r4.A20(r7)
            r4.A1q(r7)
            X.1LC r0 = X.AnonymousClass1LC.A00
            r4.A2k(r0)
            r4.A23(r6)
            java.lang.Class<X.1kt> r6 = X.C31971kt.class
            java.lang.Object[] r1 = new java.lang.Object[]{r3}
            r0 = -1933296085(0xffffffff8cc43e2b, float:-3.0235998E-31)
            X.10N r0 = X.C17780zS.A0E(r6, r3, r0, r1)
            r4.A2S(r0)
            X.1Xv r2 = r2.iterator()
            r9 = 0
            r10 = 0
            r8 = 0
        L_0x0850:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x086c
            java.lang.Object r0 = r2.next()
            X.1HP r0 = (X.AnonymousClass1HP) r0
            java.lang.Integer r1 = r0.A04
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            if (r1 != r0) goto L_0x0865
            int r10 = r10 + 1
            goto L_0x0850
        L_0x0865:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            if (r1 != r0) goto L_0x0850
            int r8 = r8 + 1
            goto L_0x0850
        L_0x086c:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r6 = 1
            if (r10 <= 0) goto L_0x088a
            android.content.res.Resources r2 = r3.A03()
            r1 = 2131689614(0x7f0f008e, float:1.9008248E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r0 = r2.getQuantityString(r1, r10, r0)
            X.C35511rL.A06(r7, r0, r9)
        L_0x088a:
            if (r8 <= 0) goto L_0x08a2
            android.content.res.Resources r2 = r3.A03()
            r1 = 2131689613(0x7f0f008d, float:1.9008246E38)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            java.lang.Object[] r0 = new java.lang.Object[]{r0}
            java.lang.String r0 = r2.getQuantityString(r1, r8, r0)
            X.C35511rL.A06(r7, r0, r6)
        L_0x08a2:
            java.lang.String r0 = r7.toString()
            r4.A2z(r0)
            java.lang.String r0 = "android.widget.Button"
            r4.A2n(r0)
            java.lang.Class<X.1kt> r2 = X.C31971kt.class
            java.lang.Object[] r1 = new java.lang.Object[]{r3}
            r0 = -843172861(0xffffffffcdbe3403, float:-3.9888496E8)
            X.10N r2 = X.C17780zS.A0E(r2, r3, r0, r1)
            X.0zR r0 = r4.A00
            X.11G r0 = r0.A14()
            X.1jd r1 = X.AnonymousClass11G.A01(r0)
            int r0 = r1.A0A
            r0 = r0 | 2048(0x800, float:2.87E-42)
            r1.A0A = r0
            r1.A0N = r2
            r4.A2x()
            java.lang.Class<X.1kt> r2 = X.C31971kt.class
            java.lang.Object[] r1 = new java.lang.Object[]{r3}
            r0 = 1590505951(0x5ecd31df, float:7.3929221E18)
            X.10N r0 = X.C17780zS.A0E(r2, r3, r0, r1)
            r4.A2y(r0)
            r5.A39(r4)
        L_0x08e3:
            X.11D r0 = r5.A00
            return r0
        L_0x08e6:
            X.2t4 r0 = r4.A31()
            return r0
        L_0x08eb:
            r0 = move-exception
            r9.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31971kt.A0O(X.0p4):X.0zR");
    }

    public C17770zR A16() {
        C31971kt r1 = (C31971kt) super.A16();
        r1.A06 = new C33061mm();
        return r1;
    }
}
