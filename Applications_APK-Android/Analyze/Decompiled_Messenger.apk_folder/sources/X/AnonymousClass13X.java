package X;

import android.database.Cursor;

/* renamed from: X.13X  reason: invalid class name */
public final class AnonymousClass13X extends C28751fJ {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;

    public AnonymousClass13X(Cursor cursor) {
        super(cursor);
        this.A08 = cursor.getColumnIndex(C22298Ase.$const$string(75));
        this.A09 = cursor.getColumnIndex("thread_key");
        this.A04 = cursor.getColumnIndexOrThrow("event_reminder_key");
        this.A06 = cursor.getColumnIndexOrThrow("event_reminder_timestamp");
        this.A02 = cursor.getColumnIndexOrThrow(C22298Ase.$const$string(72));
        this.A07 = cursor.getColumnIndexOrThrow(C22298Ase.$const$string(74));
        this.A01 = cursor.getColumnIndexOrThrow(C22298Ase.$const$string(63));
        this.A05 = cursor.getColumnIndexOrThrow(C22298Ase.$const$string(73));
        this.A00 = cursor.getColumnIndexOrThrow(C22298Ase.$const$string(71));
        this.A03 = cursor.getColumnIndexOrThrow("event_reminder_guest_rsvps");
    }
}
