package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1cI  reason: invalid class name and case insensitive filesystem */
public final class C26881cI extends C26891cJ {
    private static volatile C26881cI A00;

    public static final C26881cI A01(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C26881cI.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new C26881cI(AnonymousClass1YA.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final C26881cI A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public C26881cI(Context context) {
        super(context);
    }
}
