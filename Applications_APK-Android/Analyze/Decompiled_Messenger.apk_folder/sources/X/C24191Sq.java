package X;

import com.facebook.common.time.AwakeTimeSinceBootClock;

/* renamed from: X.1Sq  reason: invalid class name and case insensitive filesystem */
public final class C24191Sq {
    public static final AnonymousClass07J A00;

    static {
        Class<C24201Sr> cls = C24201Sr.class;
        AnonymousClass06c r3 = new AnonymousClass06c(null, cls, AwakeTimeSinceBootClock.INSTANCE);
        r3.A03 = new AnonymousClass1QR(cls);
        A00 = r3.A00();
    }
}
