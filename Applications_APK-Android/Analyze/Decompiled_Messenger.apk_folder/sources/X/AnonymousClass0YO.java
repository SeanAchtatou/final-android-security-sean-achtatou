package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.0YO  reason: invalid class name */
public final class AnonymousClass0YO {
    public static final List A00 = Arrays.asList(AnonymousClass00I.A02("ctscan_v2_logger_events").split(","));
    public static final boolean A01 = Boolean.valueOf(AnonymousClass00I.A02("is_ctscan_v2_testing")).booleanValue();
}
