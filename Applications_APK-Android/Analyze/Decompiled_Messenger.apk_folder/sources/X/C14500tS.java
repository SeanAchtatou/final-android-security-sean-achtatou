package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo;
import com.google.common.base.Platform;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tS  reason: invalid class name and case insensitive filesystem */
public final class C14500tS {
    private static volatile C14500tS A01;
    public AnonymousClass0UN A00;

    public static final C14500tS A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C14500tS.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C14500tS(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static String A01(C14500tS r3) {
        int i = AnonymousClass1Y3.Asq;
        if (((AnonymousClass14J) AnonymousClass1XX.A02(4, i, r3.A00)).A01() != null) {
            return ((AnonymousClass14J) AnonymousClass1XX.A02(4, i, r3.A00)).A01().toString();
        }
        return null;
    }

    public void A02(C74283hZ r4) {
        if (!r4.A09) {
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_first_pixel_impression"), AnonymousClass1Y3.A24);
            if (uSLEBaseShape0S0000000.A0G()) {
                USLEBaseShape0S0000000 A0I = uSLEBaseShape0S0000000.A0I(r4.A08);
                A0I.A0B("ad_position", Integer.valueOf(r4.A02));
                A0I.A0D("item_id", r4.A07);
                A0I.A0O("messenger_inbox_ads");
                A0I.A06();
            }
        }
    }

    public void A03(C74283hZ r5) {
        if (Platform.stringIsNullOrEmpty(r5.A07) && !r5.A09 && r5.A06 != AnonymousClass07B.A00) {
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A02("inbox_ad_impression", C04970Xc.A04), AnonymousClass1Y3.A26);
            if (uSLEBaseShape0S0000000.A0G()) {
                USLEBaseShape0S0000000 A0I = uSLEBaseShape0S0000000.A0I(r5.A08);
                A0I.A0B("preclick_indicator_type", Integer.valueOf(((C74113hI) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Ayy, this.A00)).A01(r5.A05)));
                A0I.A0O("messenger_inbox_ads");
                A0I.A0B("ui_format", Integer.valueOf(r5.A03));
                A0I.A0D("impression_type", C74123hJ.A00(r5.A06));
                A0I.A0B("ad_position", Integer.valueOf(r5.A02));
                A0I.A0D("item_id", r5.A07);
                A0I.A0D("end_point", A01(this));
                A0I.A06();
            }
        }
    }

    public void A04(C74283hZ r6) {
        if (Platform.stringIsNullOrEmpty(r6.A07) && !r6.A09 && r6.A06 != AnonymousClass07B.A00) {
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A02("inbox_ad_impression_timespent", C04970Xc.A04), 271);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0I(r6.A08);
                uSLEBaseShape0S0000000.A0O("messenger_inbox_ads");
                uSLEBaseShape0S0000000.A0B("ui_format", Integer.valueOf(r6.A03));
                uSLEBaseShape0S0000000.A0D("impression_type", C74123hJ.A00(r6.A06));
                uSLEBaseShape0S0000000.A0B("ad_position", Integer.valueOf(r6.A02));
                uSLEBaseShape0S0000000.A0D("item_id", r6.A07);
                uSLEBaseShape0S0000000.A0C("time_on_screen", Long.valueOf(((AnonymousClass069) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBa, this.A00)).now() - r6.A04));
                uSLEBaseShape0S0000000.A0D("end_point", A01(this));
                uSLEBaseShape0S0000000.A06();
            }
        }
    }

    public void A05(C74283hZ r6) {
        if (!r6.A09) {
            ((AnonymousClass069) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBa, this.A00)).now();
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_first_pixel_impression_timespent"), 268);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0B("ad_position", Integer.valueOf(r6.A02));
                USLEBaseShape0S0000000 A0I = uSLEBaseShape0S0000000.A0I(r6.A08);
                A0I.A0A("max_visible_dp", Float.valueOf(r6.A00));
                A0I.A0A("max_visible_percent", Float.valueOf(r6.A01));
                A0I.A0C("time_on_screen", Long.valueOf(((AnonymousClass069) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBa, this.A00)).now() - r6.A04));
                A0I.A0D("item_id", r6.A07);
                A0I.A0O("messenger_inbox_ads");
                A0I.A06();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e9, code lost:
        if (r0 == false) goto L_0x00fc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(com.facebook.messaging.business.inboxads.common.InboxAdsData r13, int r14, X.C72423eG r15, X.AnonymousClass5SI r16) {
        /*
            r12 = this;
            int r2 = X.AnonymousClass1Y3.Ap7
            X.0UN r1 = r12.A00
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1ZE r2 = (X.AnonymousClass1ZE) r2
            X.0Xc r1 = X.C04970Xc.A04
            java.lang.String r0 = "inbox_ad_link_click"
            X.0bW r1 = r2.A02(r0, r1)
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r2 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000
            r0 = 272(0x110, float:3.81E-43)
            r2.<init>(r1, r0)
            com.google.common.collect.ImmutableList r0 = r13.A07()
            java.lang.Object r5 = r0.get(r14)
            com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo r5 = (com.facebook.messaging.business.inboxads.common.InboxAdsMediaInfo) r5
            boolean r0 = r2.A0G()
            if (r0 == 0) goto L_0x0084
            java.lang.String r0 = "messenger_inbox_ads"
            r2.A0O(r0)
            r3 = 5
            int r1 = X.AnonymousClass1Y3.Ayy
            X.0UN r0 = r12.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.3hI r1 = (X.C74113hI) r1
            com.google.common.collect.ImmutableList r0 = r13.A07()
            int r0 = r1.A01(r0)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "preclick_indicator_type"
            r2.A0B(r0, r1)
            java.lang.String r0 = r5.AcF()
            r2.A0I(r0)
            java.lang.Integer r1 = r13.A0D
            java.lang.String r0 = "ui_format"
            r2.A0B(r0, r1)
            java.lang.String r1 = r16.toString()
            java.lang.String r0 = "tap_surface"
            r2.A0D(r0, r1)
            java.lang.String r1 = r15.toString()
            java.lang.String r0 = "tap_context"
            r2.A0D(r0, r1)
            int r0 = r5.Ac9()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = "ad_position"
            r2.A0B(r0, r1)
            java.lang.String r1 = r5.Ac7()
            java.lang.String r0 = "item_id"
            r2.A0D(r0, r1)
            r2.A06()
        L_0x0084:
            int r1 = X.AnonymousClass1Y3.Adg
            X.0UN r3 = r12.A00
            r0 = 6
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.4tQ r2 = (X.C101884tQ) r2
            int r1 = X.AnonymousClass1Y3.B1q
            r0 = 3
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r1, r3)
            X.4t1 r0 = (X.C101714t1) r0
            X.4tP r6 = new X.4tP
            r6.<init>(r2, r0)
            if (r5 != 0) goto L_0x00a6
            r1 = 0
        L_0x00a0:
            if (r1 == 0) goto L_0x00ca
            r4 = -2
            if (r5 == 0) goto L_0x00c2
            goto L_0x00ba
        L_0x00a6:
            X.53Z r4 = r6.A00
            java.lang.String r3 = "LCF"
            X.53a r1 = new X.53a
            r0 = 0
            r1.<init>(r0, r3, r5, r4)
            X.4t1 r0 = r6.A01
            com.fasterxml.jackson.databind.JsonNode r0 = r0.A02(r5)
            r1.A05(r0)
            goto L_0x00a0
        L_0x00ba:
            java.lang.String r0 = r5.Ac7()     // Catch:{ NumberFormatException -> 0x00c2 }
            int r4 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x00c2 }
        L_0x00c2:
            r2 = -2
            r0 = 86
            if (r4 != r2) goto L_0x012f
            X.C1055753d.A00(r1, r0)
        L_0x00ca:
            if (r1 == 0) goto L_0x0116
            r7 = 0
            com.facebook.messaging.business.common.calltoaction.model.AdCallToAction r4 = r5.A04
            if (r4 == 0) goto L_0x0128
            android.net.Uri r0 = r4.A01
            if (r0 == 0) goto L_0x0128
        L_0x00d5:
            java.lang.String r7 = r0.toString()
        L_0x00d9:
            r3 = 2
            if (r4 == 0) goto L_0x00e6
            X.0xJ r2 = X.C16550xJ.A09
            X.0xJ r0 = r4.A08
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x00eb
        L_0x00e6:
            if (r7 != 0) goto L_0x0117
            r0 = 0
        L_0x00e9:
            if (r0 == 0) goto L_0x00fc
        L_0x00eb:
            if (r1 == 0) goto L_0x00fc
            r0 = 187(0xbb, float:2.62E-43)
            java.lang.String r4 = X.C99084oO.$const$string(r0)
            r2 = 1
            X.4tR r0 = new X.4tR
            r0.<init>(r4, r2)
            r1.A04(r0)
        L_0x00fc:
            java.lang.String r5 = "messenger_inbox_ads"
            java.lang.String r6 = "url"
            r9 = r7
            X.53b r4 = new X.53b
            r8 = 1
            r10 = 0
            r11 = 0
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
            int r2 = X.AnonymousClass1Y3.ATX
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r2, r0)
            X.3Qh r0 = (X.C67813Qh) r0
            r0.A01(r1, r4)
        L_0x0116:
            return
        L_0x0117:
            android.net.Uri r0 = android.net.Uri.parse(r7)     // Catch:{ Exception -> 0x0126 }
            java.lang.String r2 = "fbrpc"
            java.lang.String r0 = r0.getScheme()     // Catch:{ Exception -> 0x0126 }
            boolean r0 = r2.equals(r0)     // Catch:{ Exception -> 0x0126 }
            goto L_0x00e9
        L_0x0126:
            r0 = 0
            goto L_0x00e9
        L_0x0128:
            if (r4 == 0) goto L_0x00d9
            android.net.Uri r0 = r4.A00
            if (r0 == 0) goto L_0x00d9
            goto L_0x00d5
        L_0x012f:
            r3 = 86
            r0 = 0
            if (r1 == 0) goto L_0x00ca
            X.53e r2 = new X.53e
            r2.<init>(r3, r4, r0)
            java.lang.String r0 = "type"
            r1.A06(r0, r2)
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14500tS.A06(com.facebook.messaging.business.inboxads.common.InboxAdsData, int, X.3eG, X.5SI):void");
    }

    public void A07(InboxAdsMediaInfo inboxAdsMediaInfo, String str) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_no_effect_video_click"), AnonymousClass1Y3.A28);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0B("ad_position", Integer.valueOf(inboxAdsMediaInfo.Ac9()));
            uSLEBaseShape0S0000000.A0I(inboxAdsMediaInfo.AcF());
            uSLEBaseShape0S0000000.A0O("messenger_inbox_ads");
            uSLEBaseShape0S0000000.A0D("reason", str);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public void A08(InboxAdsMediaInfo inboxAdsMediaInfo, boolean z) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_sound_toggle_click"), AnonymousClass1Y3.A2D);
        if (uSLEBaseShape0S0000000.A0G()) {
            USLEBaseShape0S0000000 A0I = uSLEBaseShape0S0000000.A0I(inboxAdsMediaInfo.AcF());
            A0I.A08("mute", Boolean.valueOf(z));
            A0I.A0B("ad_position", Integer.valueOf(inboxAdsMediaInfo.Ac9()));
            A0I.A0O("messenger_inbox_ads");
            A0I.A06();
        }
    }

    public void A09(String str, int i, C72423eG r6, AnonymousClass5SI r7, int i2) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_non_click_target_tap"), 276);
        if (uSLEBaseShape0S0000000.A0G()) {
            USLEBaseShape0S0000000 A0I = uSLEBaseShape0S0000000.A0I(str);
            A0I.A0B("ad_position", Integer.valueOf(i2));
            A0I.A0O("messenger_inbox_ads");
            A0I.A0D("tap_context", r6.toString());
            A0I.A0D("tap_surface", r7.toString());
            A0I.A0B("ui_format", Integer.valueOf(i));
            A0I.A06();
        }
    }

    public void A0A(String str, C72423eG r5, AnonymousClass5SI r6, int i) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_open_media_viewer"), AnonymousClass1Y3.A29);
        if (uSLEBaseShape0S0000000.A0G()) {
            USLEBaseShape0S0000000 A0I = uSLEBaseShape0S0000000.A0I(str);
            A0I.A0B("ad_position", Integer.valueOf(i));
            A0I.A0O("messenger_inbox_ads");
            A0I.A0D("tap_context", r5.toString());
            A0I.A0D("tap_surface", r6.toString());
            A0I.A06();
        }
    }

    public void A0B(String str, String str2, C72423eG r6) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_see_more_click"), AnonymousClass1Y3.A2B);
        if (uSLEBaseShape0S0000000.A0G()) {
            USLEBaseShape0S0000000 A0I = uSLEBaseShape0S0000000.A0I(str);
            A0I.A0D("see_more_source", str2);
            A0I.A0O("messenger_inbox_ads");
            A0I.A0D("tap_context", r6.toString());
            A0I.A06();
        }
    }

    public void A0C(String str, String str2, Integer num) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, this.A00)).A01("inbox_ad_error"), AnonymousClass1Y3.A23);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("ad_id", str);
            uSLEBaseShape0S0000000.A0D("error_message", str2);
            uSLEBaseShape0S0000000.A0B("error_type", Integer.valueOf(C111495Ss.A00(num)));
            uSLEBaseShape0S0000000.A0O("messenger_inbox_ads");
            uSLEBaseShape0S0000000.A06();
        }
    }

    private C14500tS(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(7, r3);
    }
}
