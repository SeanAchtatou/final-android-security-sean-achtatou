package X;

import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Spannable;
import com.facebook.ui.emoji.model.Emoji;
import java.util.List;

/* renamed from: X.1Kz  reason: invalid class name and case insensitive filesystem */
public interface C22351Kz {
    public static final String A00 = new String(Character.toChars(983040));
    public static final String A01 = new String(Character.toChars(128077));

    boolean AMy(Spannable spannable);

    boolean AMz(Spannable spannable, int i, int i2);

    boolean ANh(Spannable spannable, int i);

    boolean ANi(Spannable spannable, int i, int i2, int i3, boolean z);

    List AVF();

    List AVG(int i);

    Drawable AeN(int i);

    Drawable AeO(Emoji emoji);

    Drawable AeP(String str);

    Drawable Al9(Emoji emoji, int i);

    Drawable AlA(String str, int i);

    Emoji AlB(String str);

    List AlC(Emoji emoji);

    List AlD(String str);

    int B3C(Emoji emoji, int i);

    Drawable B3I(String str);

    boolean BBp(Emoji emoji);

    void BDB(Editable editable, Emoji emoji);

    boolean BH9(CharSequence charSequence);

    boolean BHQ(String str);

    CharSequence BKQ(CharSequence charSequence, float f);

    CharSequence BKR(CharSequence charSequence, int i);
}
