package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.facebook.push.fbns.ipc.IFbnsAIDLService;

/* renamed from: X.0Hs  reason: invalid class name and case insensitive filesystem */
public final class C03050Hs implements ServiceConnection {
    public final /* synthetic */ C03040Hr A00;

    public C03050Hs(C03040Hr r1) {
        this.A00 = r1;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        IFbnsAIDLService proxy;
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            C010708t.A0I("FbnsAIDLClientManager", "This operation should be run on UI thread");
        }
        C03040Hr r2 = this.A00;
        if (iBinder == null) {
            proxy = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.facebook.push.fbns.ipc.IFbnsAIDLService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IFbnsAIDLService)) {
                proxy = new IFbnsAIDLService.Stub.Proxy(iBinder);
            } else {
                proxy = (IFbnsAIDLService) queryLocalInterface;
            }
        }
        synchronized (r2) {
            r2.A01 = proxy;
            r2.A02 = AnonymousClass07B.A0C;
            r2.notifyAll();
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            C010708t.A0I("FbnsAIDLClientManager", "This operation should be run on UI thread");
        }
        C03040Hr.A01(this.A00);
    }
}
