package X;

import com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.model.messages.Message;

/* renamed from: X.22c  reason: invalid class name and case insensitive filesystem */
public final class C405722c {
    public static GSTModelShape1S0000000 A00(Message message) {
        if (message == null || message.BAB() == null || message.BAB().B46() == null) {
            return null;
        }
        return (GSTModelShape1S0000000) message.BAB().B46().A0J(2144758284, GSTModelShape1S0000000.class, 2047713190);
    }

    public static boolean A01(Message message) {
        GraphQLMessengerXMAGroupingType graphQLMessengerXMAGroupingType;
        GSTModelShape1S0000000 A00 = A00(message);
        if (A00 != null) {
            graphQLMessengerXMAGroupingType = (GraphQLMessengerXMAGroupingType) A00.A0O(-2114235146, GraphQLMessengerXMAGroupingType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE);
        } else {
            graphQLMessengerXMAGroupingType = GraphQLMessengerXMAGroupingType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE;
        }
        return GraphQLMessengerXMAGroupingType.GAME.equals(graphQLMessengerXMAGroupingType);
    }
}
