package X;

import com.facebook.common.dextricks.DalvikConstants;

/* renamed from: X.1nJ  reason: invalid class name and case insensitive filesystem */
public final class C33271nJ {
    public static final C33271nJ A03 = new C33271nJ(Integer.MAX_VALUE, true, true);
    public int A00;
    public boolean A01;
    public boolean A02;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C33271nJ)) {
            return false;
        }
        C33271nJ r4 = (C33271nJ) obj;
        return this.A00 == r4.A00 && this.A02 == r4.A02 && this.A01 == r4.A01;
    }

    public int hashCode() {
        int i = this.A00;
        int i2 = 0;
        int i3 = 0;
        if (this.A02) {
            i3 = 4194304;
        }
        int i4 = i ^ i3;
        if (this.A01) {
            i2 = DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE;
        }
        return i4 ^ i2;
    }

    public C33271nJ(int i, boolean z, boolean z2) {
        this.A00 = i;
        this.A02 = z;
        this.A01 = z2;
    }
}
