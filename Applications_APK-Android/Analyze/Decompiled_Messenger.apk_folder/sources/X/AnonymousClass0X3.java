package X;

import java.io.File;

/* renamed from: X.0X3  reason: invalid class name */
public final class AnonymousClass0X3 {
    public final AnonymousClass0WC A00;
    public final File A01;

    public static AnonymousClass0WH A00(AnonymousClass0X3 r4, String str) {
        File file = new File(new File(r4.A01, "users"), str);
        if (file.exists() || file.mkdirs()) {
            return new AnonymousClass0WH(r4.A00, file);
        }
        return null;
    }

    public AnonymousClass0X3(AnonymousClass0WC r1, File file) {
        this.A00 = r1;
        this.A01 = file;
    }
}
