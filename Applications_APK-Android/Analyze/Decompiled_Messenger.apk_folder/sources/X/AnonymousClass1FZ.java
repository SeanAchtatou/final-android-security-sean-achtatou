package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FZ  reason: invalid class name */
public final class AnonymousClass1FZ {
    private static volatile AnonymousClass1FZ A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass1FZ A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1FZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1FZ(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static boolean A01(AnonymousClass1FZ r3) {
        if (((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, r3.A00)).Aer(2306125729730987865L, AnonymousClass0XE.A07)) {
            return false;
        }
        return true;
    }

    private AnonymousClass1FZ(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public boolean A02() {
        if (!A01(this)) {
            return false;
        }
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aer(282720517490523L, AnonymousClass0XE.A07);
    }
}
