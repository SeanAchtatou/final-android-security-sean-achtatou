package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.google.common.base.Supplier;

/* renamed from: X.0aX  reason: invalid class name and case insensitive filesystem */
public abstract class C05910aX {
    public static final AnonymousClass0W6 A02 = new AnonymousClass0W6("key", "TEXT PRIMARY KEY");
    public static final AnonymousClass0W6 A03 = new AnonymousClass0W6("value", "TEXT");
    private static final String[] A04 = {"value"};
    public final Supplier A00;
    private final String A01;

    public String A03(AnonymousClass063 r12) {
        Cursor query = ((SQLiteDatabase) this.A00.get()).query(this.A01, A04, "key = ?", new String[]{r12.A05()}, null, null, null);
        try {
            if (query.moveToNext()) {
                return query.getString(0);
            }
            query.close();
            return null;
        } finally {
            query.close();
        }
    }

    public void A04(AnonymousClass063 r5) {
        ((SQLiteDatabase) this.A00.get()).delete(this.A01, "key = ?", new String[]{r5.A05()});
    }

    public void A06(AnonymousClass063 r5, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", r5.A05());
        contentValues.put("value", str);
        String str2 = this.A01;
        C007406x.A00(354932196);
        ((SQLiteDatabase) this.A00.get()).replaceOrThrow(str2, null, contentValues);
        C007406x.A00(-1318522462);
    }

    public void A07(AnonymousClass063 r2, boolean z) {
        String str;
        if (z) {
            str = "1";
        } else {
            str = "0";
        }
        A06(r2, str);
    }

    public C05910aX(Supplier supplier, String str) {
        this.A00 = supplier;
        this.A01 = str;
    }

    public long A02(AnonymousClass063 r3, long j) {
        String A032 = A03(r3);
        if (A032 == null) {
            return j;
        }
        try {
            return Long.parseLong(A032);
        } catch (NumberFormatException unused) {
            return j;
        }
    }

    public void A05(AnonymousClass063 r2, long j) {
        A06(r2, Long.toString(j));
    }

    public boolean A08(AnonymousClass063 r7, boolean z) {
        String A032 = A03(r7);
        if (A032 == null) {
            return z;
        }
        try {
            if (Long.parseLong(A032) != 0) {
                return true;
            }
            return false;
        } catch (NumberFormatException unused) {
            return z;
        }
    }
}
