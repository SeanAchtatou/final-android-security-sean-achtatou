package X;

import android.os.Bundle;
import com.facebook.contacts.upload.ContactsUploadRunner;
import com.facebook.fbservice.service.OperationResult;
import com.facebook.messaging.phoneconfirmation.protocol.CheckConfirmationCodeResult;
import com.facebook.messaging.phoneconfirmation.protocol.PhoneNumberParam;
import com.facebook.messaging.phoneconfirmation.protocol.RecoveredAccount;
import com.facebook.messaging.zombification.PhoneReconfirmationConfirmNumberFragment;
import com.facebook.messaging.zombification.PhoneReconfirmationLoginFragment;
import com.facebook.messaging.zombification.PhoneReconfirmationReactivatingAccountFragment;
import com.facebook.user.model.User;

/* renamed from: X.20w  reason: invalid class name and case insensitive filesystem */
public abstract class C402520w {
    public void A00(OperationResult operationResult) {
        RecoveredAccount recoveredAccount;
        if (this instanceof C56582qL) {
            PhoneReconfirmationReactivatingAccountFragment.A04(((C56582qL) this).A00);
        } else if (this instanceof AnonymousClass2L4) {
            AnonymousClass2L4 r2 = (AnonymousClass2L4) this;
            if (r2.A00.A01 != null) {
                CheckConfirmationCodeResult checkConfirmationCodeResult = (CheckConfirmationCodeResult) operationResult.A07();
                PhoneReconfirmationConfirmNumberFragment phoneReconfirmationConfirmNumberFragment = r2.A00.A01.A00;
                CheckConfirmationCodeResult checkConfirmationCodeResult2 = checkConfirmationCodeResult;
                if (checkConfirmationCodeResult == null || (recoveredAccount = checkConfirmationCodeResult.A00) == null) {
                    PhoneReconfirmationConfirmNumberFragment.A00(phoneReconfirmationConfirmNumberFragment, PhoneReconfirmationReactivatingAccountFragment.class, PhoneReconfirmationReactivatingAccountFragment.A00(phoneReconfirmationConfirmNumberFragment.A07, checkConfirmationCodeResult.A02));
                } else {
                    C07220cv r22 = new C07220cv();
                    r22.A03(C25651aB.A03, recoveredAccount.A01);
                    r22.A0h = recoveredAccount.A02;
                    r22.A0j = recoveredAccount.A04;
                    r22.A11 = recoveredAccount.A06;
                    User A02 = r22.A02();
                    PhoneNumberParam phoneNumberParam = phoneReconfirmationConfirmNumberFragment.A07;
                    String str = checkConfirmationCodeResult2.A02;
                    Bundle bundle = new Bundle();
                    PhoneReconfirmationLoginFragment.A05(A02, true, phoneNumberParam, str, bundle);
                    PhoneReconfirmationConfirmNumberFragment.A00(phoneReconfirmationConfirmNumberFragment, PhoneReconfirmationLoginFragment.class, bundle);
                }
                phoneReconfirmationConfirmNumberFragment.A0B.A03();
                phoneReconfirmationConfirmNumberFragment.A09.A06(phoneReconfirmationConfirmNumberFragment.Act(), "phone_reconfirmation_send_code_result", null);
            }
        } else if (this instanceof C50342dp) {
            C50342dp r3 = (C50342dp) this;
            ((ContactsUploadRunner) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BJ8, r3.A00.A01)).A04();
            C202229fy r0 = r3.A01;
            if (r0 != null) {
                r0.A00.A2Y(null, null);
            }
        } else if (!(this instanceof C43862Fz)) {
            C402520w r02 = ((AnonymousClass2SJ) this).A00.A02;
            if (r02 != null) {
                r02.A00(operationResult);
            }
        } else {
            C402120s r1 = ((C43862Fz) this).A00;
            if (r1.A02) {
                C402120s.A00(r1, operationResult);
                return;
            }
            r1.A00 = operationResult;
            r1.A04 = true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(com.facebook.fbservice.service.ServiceException r6) {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C56582qL
            if (r0 != 0) goto L_0x00b4
            boolean r0 = r5 instanceof X.AnonymousClass2L4
            if (r0 != 0) goto L_0x003d
            boolean r0 = r5 instanceof X.C50342dp
            if (r0 != 0) goto L_0x001d
            boolean r0 = r5 instanceof X.C43862Fz
            if (r0 != 0) goto L_0x00a1
            r0 = r5
            X.2SJ r0 = (X.AnonymousClass2SJ) r0
            X.20u r0 = r0.A00
            X.20w r0 = r0.A02
            if (r0 == 0) goto L_0x001c
            r0.A01(r6)
        L_0x001c:
            return
        L_0x001d:
            r0 = r5
            X.2dp r0 = (X.C50342dp) r0
            X.9fy r0 = r0.A01
            if (r0 == 0) goto L_0x001c
            int r2 = X.AnonymousClass1Y3.AI4
            com.facebook.messaging.neue.nux.messenger.NeueNuxContactImportFragment r0 = r0.A00
            X.0UN r1 = r0.A03
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.3Ph r2 = (X.C67553Ph) r2
            X.2fR r1 = new X.2fR
            r0 = 2131823257(0x7f110a99, float:1.9279309E38)
            r1.<init>(r0)
            r2.A04(r1)
            return
        L_0x003d:
            r0 = r5
            X.2L4 r0 = (X.AnonymousClass2L4) r0
            X.9kM r3 = r0.A00
            com.facebook.fbservice.service.OperationResult r0 = r6.result
            java.lang.Object r4 = r0.A08()
            com.facebook.http.protocol.ApiErrorResult r4 = (com.facebook.http.protocol.ApiErrorResult) r4
            if (r4 == 0) goto L_0x0097
            java.lang.Class r2 = X.C204379kM.A03
            int r0 = r4.A02()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = r4.A05()
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Confirmation Failure %s %s"
            X.C010708t.A0B(r2, r0, r1)
            int r1 = r4.A02()
            r0 = 3301(0xce5, float:4.626E-42)
            if (r1 != r0) goto L_0x0097
            X.2p2 r1 = r3.A02
            r0 = 2131829643(0x7f11238b, float:1.929226E38)
            X.4yK r0 = r1.A03(r0)
            r1.A02(r0)
        L_0x0077:
            X.9kR r0 = r3.A01
            if (r0 == 0) goto L_0x001c
            com.facebook.messaging.zombification.PhoneReconfirmationConfirmNumberFragment r4 = r0.A00
            java.lang.String r3 = "phone_reconfirmation_send_code_result"
            com.facebook.widget.splitinput.SplitFieldCodeInputView r0 = r4.A0B
            r0.A03()
            X.9k6 r1 = r4.A09
            java.lang.String r0 = r4.Act()
            r1.A04(r0, r3, r6)
            X.2p2 r1 = r4.A0A
            X.4yK r0 = r1.A04(r6)
            r1.A02(r0)
            return
        L_0x0097:
            X.2p2 r1 = r3.A02
            X.4yK r0 = r1.A04(r6)
            r1.A02(r0)
            goto L_0x0077
        L_0x00a1:
            r0 = r5
            X.2Fz r0 = (X.C43862Fz) r0
            X.20s r1 = r0.A00
            boolean r0 = r1.A02
            if (r0 == 0) goto L_0x00ae
            X.C402120s.A01(r1, r6)
            return
        L_0x00ae:
            r1.A01 = r6
            r0 = 1
            r1.A03 = r0
            return
        L_0x00b4:
            r0 = r5
            X.2qL r0 = (X.C56582qL) r0
            com.facebook.messaging.zombification.PhoneReconfirmationReactivatingAccountFragment r4 = r0.A00
            X.0uI r1 = r6.errorCode
            X.0uI r0 = X.C14880uI.A01
            if (r1 != r0) goto L_0x00d6
            com.facebook.fbservice.service.OperationResult r0 = r6.result
            java.lang.Object r0 = r0.A08()
            com.facebook.http.protocol.ApiErrorResult r0 = (com.facebook.http.protocol.ApiErrorResult) r0
            if (r0 == 0) goto L_0x00d6
            int r1 = r0.A02()
            r0 = 2018032(0x1ecaf0, float:2.827865E-39)
            if (r1 != r0) goto L_0x00d6
            com.facebook.messaging.zombification.PhoneReconfirmationReactivatingAccountFragment.A04(r4)
            return
        L_0x00d6:
            X.9k6 r2 = r4.A04
            java.lang.String r1 = r4.Act()
            java.lang.String r0 = "phone_reconfirmation_reactivate_account_result"
            r2.A04(r1, r0, r6)
            X.0US r0 = r4.A01
            java.lang.Object r1 = r0.get()
            X.2p3 r1 = (X.C55972p3) r1
            r3 = 0
            r0 = 1
            java.lang.String r1 = r1.A01(r6, r3, r0)
            X.0rX r2 = new X.0rX
            android.content.Context r0 = r4.A1j()
            r2.<init>(r0)
            r2.A0C(r1)
            r1 = 2131834209(0x7f113561, float:1.9301522E38)
            X.9ju r0 = new X.9ju
            r0.<init>(r4)
            r2.A02(r1, r0)
            r2.A0E(r3)
            X.3At r0 = r2.A06()
            r0.show()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C402520w.A01(com.facebook.fbservice.service.ServiceException):void");
    }
}
