package X;

/* renamed from: X.12z  reason: invalid class name and case insensitive filesystem */
public final class C184212z {
    public static final Class[] NO_CLASSES = new Class[0];
    public final Class[] _argTypes;
    public final String _name;

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj != null && obj.getClass() == getClass()) {
                C184212z r9 = (C184212z) obj;
                if (this._name.equals(r9._name)) {
                    Class[] clsArr = r9._argTypes;
                    int length = this._argTypes.length;
                    if (clsArr.length == length) {
                        int i = 0;
                        while (i < length) {
                            Class cls = clsArr[i];
                            Class cls2 = this._argTypes[i];
                            if (cls == cls2 || cls.isAssignableFrom(cls2) || cls2.isAssignableFrom(cls)) {
                                i++;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this._name.hashCode() + this._argTypes.length;
    }

    public String toString() {
        return AnonymousClass08S.A0M(this._name, "(", this._argTypes.length, "-args)");
    }

    public C184212z(String str, Class[] clsArr) {
        this._name = str;
        this._argTypes = clsArr == null ? NO_CLASSES : clsArr;
    }
}
