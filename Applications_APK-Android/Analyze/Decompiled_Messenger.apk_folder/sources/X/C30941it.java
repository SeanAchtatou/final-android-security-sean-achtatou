package X;

import java.util.List;

/* renamed from: X.1it  reason: invalid class name and case insensitive filesystem */
public final class C30941it {
    public C30961iv A00;
    public AnonymousClass0ZZ A01;
    public AnonymousClass13z A02;
    public String A03;
    public List A04;

    public C31001iz A00() {
        String str = this.A03;
        if (str != null) {
            return new C31001iz(str, this.A02, this.A00, this.A01, this.A04);
        }
        throw new IllegalStateException("Cache name must not be null");
    }
}
