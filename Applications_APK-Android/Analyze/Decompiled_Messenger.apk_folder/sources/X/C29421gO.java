package X;

import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.Omnistore;
import com.facebook.omnistore.QueueIdentifier;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1gO  reason: invalid class name and case insensitive filesystem */
public final class C29421gO implements Omnistore.CollectionIndexerFunction, Omnistore.DeltaClusterCallback, Omnistore.DeltaReceivedCallback, Omnistore.SnapshotStateChangedCallback {
    public final HashMap A00 = new HashMap();
    private final AnonymousClass09P A01;

    public IndexedFields getIndexedFields(CollectionName collectionName, String str, String str2, ByteBuffer byteBuffer) {
        C14430tJ r0;
        try {
            synchronized (this) {
                r0 = (C14430tJ) this.A00.get(collectionName);
            }
            if (r0 == null) {
                return new IndexedFields();
            }
            return r0.BCR(str, str2, byteBuffer);
        } catch (Throwable th) {
            this.A01.softReport(AnonymousClass08S.A0J("Exception thrown while indexing omnistore object for collection ", collectionName.getLabel()), th);
            return new IndexedFields();
        }
    }

    public void onDeltaReceived(Delta[] deltaArr) {
        C14430tJ r1;
        HashMap hashMap = new HashMap();
        for (Delta delta : deltaArr) {
            int status = delta.getStatus();
            if (status == 1 || status == 4) {
                CollectionName collectionName = delta.getCollectionName();
                ArrayList arrayList = (ArrayList) hashMap.get(collectionName);
                if (arrayList == null) {
                    arrayList = new ArrayList(r5);
                    hashMap.put(collectionName, arrayList);
                }
                arrayList.add(delta);
            }
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            synchronized (this) {
                r1 = (C14430tJ) this.A00.get(entry.getKey());
            }
            if (r1 != null) {
                r1.BVs((List) entry.getValue());
            }
        }
    }

    public void onSnapshotStateChanged(CollectionName collectionName, int i) {
        C14430tJ r0;
        synchronized (this) {
            r0 = (C14430tJ) this.A00.get(collectionName);
        }
        if (r0 != null) {
            r0.Boz(i);
        }
    }

    public static final C29421gO A00(AnonymousClass1XY r2) {
        return new C29421gO(C04750Wa.A01(r2));
    }

    private List A01(QueueIdentifier queueIdentifier) {
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            for (Map.Entry entry : this.A00.entrySet()) {
                if (((CollectionName) entry.getKey()).getQueueIdentifier().equals(queueIdentifier)) {
                    arrayList.add(entry.getValue());
                }
            }
        }
        return arrayList;
    }

    public C29421gO(AnonymousClass09P r2) {
        this.A01 = r2;
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
        queueIdentifier.toString();
        for (C14430tJ onDeltaClusterEnded : A01(queueIdentifier)) {
            onDeltaClusterEnded.onDeltaClusterEnded(i, queueIdentifier);
        }
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
        queueIdentifier.toString();
        for (C14430tJ onDeltaClusterStarted : A01(queueIdentifier)) {
            onDeltaClusterStarted.onDeltaClusterStarted(i, j, queueIdentifier);
        }
    }
}
