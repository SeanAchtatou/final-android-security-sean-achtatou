package X;

/* renamed from: X.0G2  reason: invalid class name */
public final class AnonymousClass0G2 extends C007907e {
    public final AnonymousClass04b A00;

    /* renamed from: A05 */
    public C02760Gg A03() {
        C02760Gg r4 = new C02760Gg();
        int size = this.A00.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass04b r0 = this.A00;
            r4.A0C((Class) r0.A07(i), ((C007907e) r0.A09(i)).A03());
        }
        return r4;
    }

    /* renamed from: A06 */
    public boolean A04(C02760Gg r8) {
        boolean z;
        C02740Gd.A00(r8, "Null value passed to getSnapshot!");
        AnonymousClass04b r6 = r8.mMetricsMap;
        int size = r6.size();
        boolean z2 = false;
        for (int i = 0; i < size; i++) {
            Class cls = (Class) r6.A07(i);
            C007907e r1 = (C007907e) this.A00.get(cls);
            if (r1 != null) {
                z = r1.A04(r8.A09(cls));
            } else {
                z = false;
            }
            r8.A0D(cls, z);
            z2 |= z;
        }
        return z2;
    }

    public AnonymousClass0G2(AnonymousClass0FK r3) {
        AnonymousClass04b r1 = new AnonymousClass04b();
        this.A00 = r1;
        r1.A0B(r3.A00);
    }
}
