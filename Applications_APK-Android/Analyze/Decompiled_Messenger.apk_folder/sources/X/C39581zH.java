package X;

/* renamed from: X.1zH  reason: invalid class name and case insensitive filesystem */
public final class C39581zH implements AnonymousClass1YQ {
    public static boolean A01;
    private final C25051Yd A00;

    public String getSimpleName() {
        return "ScrollStateHandler";
    }

    public static final C39581zH A00(AnonymousClass1XY r1) {
        return new C39581zH(r1);
    }

    private C39581zH(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }

    public void init() {
        int A03 = C000700l.A03(1853007859);
        A01 = this.A00.Aem(286474318650205L);
        C000700l.A09(-1560605692, A03);
    }
}
