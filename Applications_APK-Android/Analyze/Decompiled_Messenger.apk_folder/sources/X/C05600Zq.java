package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Zq  reason: invalid class name and case insensitive filesystem */
public final class C05600Zq extends AnonymousClass0UW {
    private static volatile C05600Zq A00;

    private C05600Zq(AnonymousClass1Y6 r2) {
        super(r2, "FbSharedPreferences Init Lock Held");
    }

    public static final C05600Zq A01(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (C05600Zq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new C05600Zq(AnonymousClass0UX.A07(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
