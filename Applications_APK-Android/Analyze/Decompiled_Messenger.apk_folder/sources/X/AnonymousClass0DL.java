package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0DL  reason: invalid class name */
public final class AnonymousClass0DL implements AnonymousClass1YQ {
    private static volatile AnonymousClass0DL A04;
    public boolean A00;
    private AnonymousClass0UN A01;
    private boolean A02;
    private final C25051Yd A03;

    public String getSimpleName() {
        return "DynamicAnalysisConfigSync";
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void init() {
        /*
            r14 = this;
            monitor-enter(r14)
            r0 = 1159707641(0x451fbbf9, float:2555.7483)
            int r4 = X.C000700l.A03(r0)     // Catch:{ all -> 0x0093 }
            X.1Yd r2 = r14.A03     // Catch:{ all -> 0x0093 }
            r0 = 281844344095485(0x10056000302fd, double:1.392496078922383E-309)
            boolean r7 = r2.Aem(r0)     // Catch:{ all -> 0x0093 }
            X.1Yd r3 = r14.A03     // Catch:{ all -> 0x0093 }
            r1 = 563319320871265(0x2005600040161, double:2.78316724081114E-309)
            X.0XE r0 = X.AnonymousClass0XE.A07     // Catch:{ all -> 0x0093 }
            long r0 = r3.At4(r1, r0)     // Catch:{ all -> 0x0093 }
            int r6 = (int) r0     // Catch:{ all -> 0x0093 }
            r3 = 1
            r2 = 0
            if (r7 == 0) goto L_0x0041
            int r1 = com.facebook.redex.dynamicanalysis.DynamicAnalysis.sNumStaticallyInstrumented     // Catch:{ all -> 0x0093 }
            r0 = 0
            if (r1 <= 0) goto L_0x002b
            r0 = 1
        L_0x002b:
            if (r0 == 0) goto L_0x0041
            if (r6 <= r3) goto L_0x003f
            int r1 = X.AnonymousClass1Y3.A8J     // Catch:{ all -> 0x0093 }
            X.0UN r0 = r14.A01     // Catch:{ all -> 0x0093 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x0093 }
            java.util.Random r0 = (java.util.Random) r0     // Catch:{ all -> 0x0093 }
            int r0 = r0.nextInt(r6)     // Catch:{ all -> 0x0093 }
            if (r0 != 0) goto L_0x0041
        L_0x003f:
            r0 = 1
            goto L_0x0042
        L_0x0041:
            r0 = 0
        L_0x0042:
            r14.A00 = r0     // Catch:{ all -> 0x0093 }
            X.1Yd r2 = r14.A03     // Catch:{ all -> 0x0093 }
            r0 = 281844343964411(0x10056000102fb, double:1.39249607827479E-309)
            X.0XE r3 = X.AnonymousClass0XE.A07     // Catch:{ all -> 0x0093 }
            boolean r0 = r2.Aer(r0, r3)     // Catch:{ all -> 0x0093 }
            r14.A02 = r0     // Catch:{ all -> 0x0093 }
            X.1Yd r2 = r14.A03     // Catch:{ all -> 0x0093 }
            r0 = 281844344029948(0x10056000202fc, double:1.392496078598587E-309)
            boolean r5 = r2.Aer(r0, r3)     // Catch:{ all -> 0x0093 }
            java.lang.String r3 = "DYNA|DynamicAnalysisConfigSync"
            java.lang.String r2 = "ColdStartUploadEnabled: %s (Instrumented: %s, GK: %s, Sampling: 1/%d), DebugColdStart: %s, DebugScroll: %s"
            boolean r0 = r14.A00     // Catch:{ all -> 0x0093 }
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0093 }
            int r1 = com.facebook.redex.dynamicanalysis.DynamicAnalysis.sNumStaticallyInstrumented     // Catch:{ all -> 0x0093 }
            r0 = 0
            if (r1 <= 0) goto L_0x006e
            r0 = 1
        L_0x006e:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0093 }
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x0093 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0093 }
            boolean r0 = r14.A02     // Catch:{ all -> 0x0093 }
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0093 }
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r5)     // Catch:{ all -> 0x0093 }
            java.lang.Object[] r0 = new java.lang.Object[]{r8, r9, r10, r11, r12, r13}     // Catch:{ all -> 0x0093 }
            X.C010708t.A0P(r3, r2, r0)     // Catch:{ all -> 0x0093 }
            r0 = 1148838392(0x4479e1f8, float:999.53076)
            X.C000700l.A09(r0, r4)     // Catch:{ all -> 0x0093 }
            monitor-exit(r14)
            return
        L_0x0093:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DL.init():void");
    }

    public static final AnonymousClass0US A00(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.BPT, r1);
    }

    public static final AnonymousClass0DL A02(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (AnonymousClass0DL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new AnonymousClass0DL(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    private AnonymousClass0DL(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(1, r3);
        this.A03 = AnonymousClass0WT.A00(r3);
    }

    public static final AnonymousClass0DL A01(AnonymousClass1XY r0) {
        return A02(r0);
    }
}
