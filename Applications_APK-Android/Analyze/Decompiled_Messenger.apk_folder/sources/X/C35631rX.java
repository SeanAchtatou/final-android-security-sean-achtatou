package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.montage.omnistore.MontageOmnistoreComponent;
import com.facebook.messaging.montage.omnistore.storedprocedures.MontageMoreBucketsStoredProcedureComponent;
import com.facebook.omnistore.CollectionName;
import io.card.payment.BuildConfig;
import java.util.concurrent.Executor;
import javax.inject.Singleton;
import org.json.JSONException;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1rX  reason: invalid class name and case insensitive filesystem */
public final class C35631rX {
    private static volatile C35631rX A03;
    private int A00 = 0;
    private long A01;
    private AnonymousClass0UN A02;

    public synchronized void A01(int i) {
        String str;
        if (this.A00 == 0) {
            this.A00 = 1;
            this.A01 = ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A02)).now();
            MontageMoreBucketsStoredProcedureComponent montageMoreBucketsStoredProcedureComponent = (MontageMoreBucketsStoredProcedureComponent) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BDE, this.A02);
            if (montageMoreBucketsStoredProcedureComponent.A01 == null) {
                C010708t.A0I("com.facebook.messaging.montage.omnistore.storedprocedures.MontageMoreBucketsStoredProcedureComponent", " no sender avaiable");
            } else {
                String str2 = BuildConfig.FLAVOR;
                try {
                    JSONObject put = new JSONObject().put("collection_name", ((MontageOmnistoreComponent) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ANr, montageMoreBucketsStoredProcedureComponent.A00)).A01);
                    if (montageMoreBucketsStoredProcedureComponent.A02 == null) {
                        montageMoreBucketsStoredProcedureComponent.A02 = ((C33451nb) AnonymousClass1XX.A02(3, AnonymousClass1Y3.A6K, montageMoreBucketsStoredProcedureComponent.A00)).A01("OmnistoreMontageSingleQuery.params.json", "query_doc_id", "com.facebook.messaging.montage.omnistore.storedprocedures.MontageMoreBucketsStoredProcedureComponent");
                    }
                    str2 = StringFormatUtil.formatStrLocaleSafe(put.put("doc_id", montageMoreBucketsStoredProcedureComponent.A02).put("flat_buffer_idl", ((MontageOmnistoreComponent) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ANr, montageMoreBucketsStoredProcedureComponent.A00)).A03()).put("page_num", i).toString());
                } catch (JSONException e) {
                    C010708t.A0T("com.facebook.messaging.montage.omnistore.storedprocedures.MontageMoreBucketsStoredProcedureComponent", e, " error while building request");
                } catch (NumberFormatException e2) {
                    C010708t.A0T("com.facebook.messaging.montage.omnistore.storedprocedures.MontageMoreBucketsStoredProcedureComponent", e2, " error while building request");
                }
                if (!C06850cB.A0B(str2)) {
                    String valueOf = String.valueOf(((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, montageMoreBucketsStoredProcedureComponent.A00)).now() / 1000);
                    CollectionName collectionName = ((MontageOmnistoreComponent) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ANr, montageMoreBucketsStoredProcedureComponent.A00)).A01;
                    if (collectionName != null) {
                        str = collectionName.getQueueIdentifier().toString();
                    } else {
                        str = null;
                    }
                    AnonymousClass07A.A04((Executor) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A1n, montageMoreBucketsStoredProcedureComponent.A00), new AnonymousClass52U(montageMoreBucketsStoredProcedureComponent, str, str2, valueOf), -499509585);
                }
            }
        }
    }

    public synchronized boolean A02() {
        boolean z;
        z = false;
        if (this.A00 == 1 && ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A02)).now() - this.A01 > 3000) {
            this.A00 = 0;
        }
        if (this.A00 == 1) {
            z = true;
        }
        return z;
    }

    public static final C35631rX A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C35631rX.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C35631rX(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C35631rX(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(2, r3);
    }
}
