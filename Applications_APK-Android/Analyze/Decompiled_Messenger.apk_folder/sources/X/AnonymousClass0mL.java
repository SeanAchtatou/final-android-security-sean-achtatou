package X;

import com.google.common.base.Preconditions;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0mL  reason: invalid class name */
public final class AnonymousClass0mL {
    public final ReentrantReadWriteLock A00 = new ReentrantReadWriteLock();
    private final C11200mM A01 = new C11200mM(this);

    public C11200mM A00() {
        this.A00.writeLock().lock();
        return this.A01;
    }

    public void A01() {
        Preconditions.checkState(this.A00.writeLock().isHeldByCurrentThread());
    }
}
