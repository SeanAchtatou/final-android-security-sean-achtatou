package X;

import android.content.Context;
import java.util.regex.Pattern;

/* renamed from: X.1jw  reason: invalid class name and case insensitive filesystem */
public final class C31591jw {
    public static C31691k6 A02;
    public static final Pattern A03 = Pattern.compile("^[0-9]+L$");
    public final C013209t A00;
    private final String A01;

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0033, code lost:
        if (X.C31591jw.A03.matcher(r2).matches() == false) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C31691k6 A00() {
        /*
            r8 = this;
            java.lang.String r2 = r8.A01
            java.lang.String r1 = "com.facebook.versioncontrol.revision"
            X.09t r0 = r8.A00
            java.lang.String r3 = r0.A00(r1, r2)
            if (r3 != 0) goto L_0x000e
            java.lang.String r3 = ""
        L_0x000e:
            java.lang.String r1 = "com.facebook.versioncontrol.branch"
            X.09t r0 = r8.A00
            java.lang.String r4 = r0.A00(r1, r2)
            if (r4 != 0) goto L_0x001a
            java.lang.String r4 = ""
        L_0x001a:
            java.lang.String r1 = "com.facebook.build_time"
            X.09t r0 = r8.A00
            java.lang.String r2 = r0.A00(r1, r2)
            if (r2 != 0) goto L_0x0026
            java.lang.String r2 = ""
        L_0x0026:
            if (r2 == 0) goto L_0x0035
            java.util.regex.Pattern r0 = X.C31591jw.A03
            java.util.regex.Matcher r0 = r0.matcher(r2)
            boolean r1 = r0.matches()
            r0 = 1
            if (r1 != 0) goto L_0x0036
        L_0x0035:
            r0 = 0
        L_0x0036:
            if (r0 == 0) goto L_0x0067
            int r0 = r2.length()
            int r1 = r0 + -1
            r0 = 0
            java.lang.String r0 = r2.substring(r0, r1)
            long r5 = java.lang.Long.parseLong(r0)
            java.util.Locale r2 = java.util.Locale.US
            r1 = 1
            r0 = 0
            java.text.DateFormat r1 = java.text.DateFormat.getDateTimeInstance(r1, r0, r2)
            java.lang.String r0 = "PST8PDT"
            java.util.TimeZone r0 = java.util.TimeZone.getTimeZone(r0)
            r1.setTimeZone(r0)
            java.util.Date r0 = new java.util.Date
            r0.<init>(r5)
            java.lang.String r7 = r1.format(r0)
        L_0x0061:
            X.1k6 r2 = new X.1k6
            r2.<init>(r3, r4, r5, r7)
            return r2
        L_0x0067:
            r5 = 0
            java.lang.String r7 = ""
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31591jw.A00():X.1k6");
    }

    public C31591jw(Context context, C013209t r3) {
        if (r3 != null) {
            this.A00 = r3;
            this.A01 = context.getPackageName();
            return;
        }
        throw new NullPointerException();
    }
}
