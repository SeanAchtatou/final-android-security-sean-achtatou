package X;

import com.facebook.video.engine.api.VideoPlayerParams;

/* renamed from: X.20c  reason: invalid class name and case insensitive filesystem */
public final class C400520c implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.video.engine.playerclient.FbHeroPlayerLogger$14";
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass223 A02;

    public C400520c(AnonymousClass223 r1, int i, int i2) {
        this.A02 = r1;
        this.A01 = i;
        this.A00 = i2;
    }

    public void run() {
        AnonymousClass223 r1 = this.A02;
        C400420b r2 = r1.A0c;
        VideoPlayerParams videoPlayerParams = r1.A0b;
        r2.A0Q(videoPlayerParams, videoPlayerParams.A0K, r1.A0G, C53752lJ.A0w.value, this.A01, this.A00, videoPlayerParams.A0Q, r1.A0H, videoPlayerParams.A0g, videoPlayerParams.A0a, r1.A0d.A0A);
    }
}
