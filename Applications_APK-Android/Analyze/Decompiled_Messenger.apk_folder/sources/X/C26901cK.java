package X;

import android.content.res.Resources;
import com.facebook.graphql.querybuilder.common.ScaleInputPixelRatio;

/* renamed from: X.1cK  reason: invalid class name and case insensitive filesystem */
public final class C26901cK {
    public static int A00() {
        return (94 * A02()) / 160;
    }

    public static int A01() {
        return (40 * A02()) / 160;
    }

    private static int A02() {
        if (Resources.getSystem() == null || Resources.getSystem().getDisplayMetrics() == null) {
            return 0;
        }
        return Resources.getSystem().getDisplayMetrics().densityDpi;
    }

    public static ScaleInputPixelRatio A03() {
        int A02 = A02();
        if (A02 > 480) {
            return ScaleInputPixelRatio.NUMBER_4;
        }
        if (A02 > 320) {
            return ScaleInputPixelRatio.NUMBER_3;
        }
        if (A02 > 240) {
            return ScaleInputPixelRatio.NUMBER_2;
        }
        if (A02 > 160) {
            return ScaleInputPixelRatio.NUMBER_1_5;
        }
        return ScaleInputPixelRatio.NUMBER_1;
    }
}
