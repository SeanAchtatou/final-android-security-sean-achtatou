package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1xU  reason: invalid class name and case insensitive filesystem */
public final class C38471xU extends RuntimeException {
    private static String A00(String str) {
        StringBuilder sb = new StringBuilder();
        if (str != null) {
            sb.append(str);
        } else {
            sb.append("Failure to provision.");
        }
        sb.append("\n");
        List list = (List) BM4.A00.get();
        ArrayList<BM1> arrayList = new ArrayList<>();
        for (int i = 0; i < list.size(); i += 2) {
            arrayList.add(new BM1((C38511xY) list.get(i), (C22916BKm) list.get(i + 1)));
        }
        Collections.reverse(arrayList);
        for (BM1 bm1 : arrayList) {
            C38511xY r1 = bm1.A00;
            if (r1 == C38511xY.INSTANCE_GET) {
                sb.append(" while trying to get instance of ");
            } else if (r1 == C38511xY.INJECT_COMPONENT) {
                sb.append(" while trying to inject component of ");
            } else {
                sb.append(" while trying to get provider of ");
            }
            sb.append(bm1.A01);
            sb.append("\n");
        }
        sb.append("If this is an instrumentation/screenshot test then you likely need to pass the ");
        sb.append("relevant DI module to the test rule, see https://fburl.com/wiki/24nviijj for ");
        sb.append("more details.\n");
        return sb.toString();
    }

    public C38471xU() {
        super(A00(null));
    }

    public C38471xU(String str) {
        super(A00(str));
    }
}
