package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import java.util.ArrayList;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0gd  reason: invalid class name and case insensitive filesystem */
public final class C09160gd {
    public final AnonymousClass1ZJ A00;
    public final C39851zi A01;
    public final C09170ge A02;
    public final C26601bi A03;
    public final C09220gk A04;
    public final AnonymousClass0Td[] A05;

    public void A00(C04270Tg r17) {
        ArrayList arrayList;
        int size;
        String str;
        C09220gk r4 = this.A04;
        C04270Tg r5 = r17;
        long A022 = r4.A02(r5.A02);
        if (A022 != 0 && r4.A03 != null) {
            if (r5.A0O) {
                arrayList = r5.A0X.A05;
                size = arrayList.size() - 1;
            } else {
                arrayList = r5.A0Y;
                size = arrayList.size() - 2;
            }
            String str2 = (String) arrayList.get(size);
            if (r5.A0O) {
                AnonymousClass0Tj r6 = r5.A0X;
                int i = r6.A03;
                boolean z = true;
                int i2 = i - 1;
                if (i2 < 0) {
                    throw new IndexOutOfBoundsException("Attempting to get last annotation value from empty list");
                } else if (i2 != r6.A01 || (str = r6.A04) == null) {
                    r6.A01 = i2;
                    byte b = r6.A07[i - 1];
                    switch (b) {
                        case 1:
                            ArrayList arrayList2 = r6.A06;
                            str = (String) arrayList2.get(arrayList2.size() - 1);
                            break;
                        case 2:
                            str = Integer.toString((int) r6.A09[r6.A02 - 1]);
                            break;
                        case 3:
                            str = Long.toString(r6.A09[r6.A02 - 1]);
                            break;
                        case 4:
                            ArrayList arrayList3 = r6.A06;
                            str = AnonymousClass36y.A03((String[]) arrayList3.get(arrayList3.size() - 1));
                            break;
                        case 5:
                            ArrayList arrayList4 = r6.A06;
                            str = AnonymousClass36y.A01((int[]) arrayList4.get(arrayList4.size() - 1));
                            break;
                        case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                            str = Double.toString(r6.A08[r6.A00 - 1]);
                            break;
                        case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                            ArrayList arrayList5 = r6.A06;
                            str = AnonymousClass36y.A00((double[]) arrayList5.get(arrayList5.size() - 1));
                            break;
                        case 8:
                            if (r6.A09[r6.A02 - 1] == 0) {
                                z = false;
                            }
                            str = Boolean.toString(z);
                            break;
                        case Process.SIGKILL:
                            ArrayList arrayList6 = r6.A06;
                            str = AnonymousClass36y.A04((boolean[]) arrayList6.get(arrayList6.size() - 1));
                            break;
                        case AnonymousClass1Y3.A01 /*10*/:
                            ArrayList arrayList7 = r6.A06;
                            str = AnonymousClass36y.A02((long[]) arrayList7.get(arrayList7.size() - 1));
                            break;
                        default:
                            throw new UnsupportedOperationException(AnonymousClass08S.A0A("Type ", b, " is not supported yet"));
                    }
                    r6.A04 = str;
                }
            } else {
                ArrayList arrayList8 = r5.A0Y;
                str = (String) arrayList8.get(arrayList8.size() - 1);
            }
            long j = 1;
            int i3 = 0;
            int i4 = 0;
            while (true) {
                AnonymousClass0Td[] r3 = r4.A03;
                if (i3 < r3.length) {
                    if ((A022 & j) != 0) {
                        r5.A04 = i3;
                        r3[i3].BeL(r5, str2, str);
                        i4++;
                    }
                    i3++;
                    j <<= 1;
                } else {
                    AnonymousClass1ZJ r0 = r4.A02;
                    if (r0 != null && i4 != 0) {
                        r0.A05.getAndAdd(i4);
                        return;
                    }
                    return;
                }
            }
        }
    }

    public void A01(C04270Tg r22, String str, AnonymousClass0lr r24, long j, boolean z, int i) {
        C09220gk r8 = this.A04;
        C04270Tg r14 = r22;
        long A022 = r8.A02(r14.A02);
        if (A022 != 0 && r8.A03 != null) {
            long j2 = 1;
            int i2 = 0;
            int i3 = 0;
            while (true) {
                AnonymousClass0Td[] r3 = r8.A03;
                if (i3 >= r3.length) {
                    break;
                }
                if ((A022 & j2) != 0) {
                    r14.A04 = i3;
                    r3[i3].BeQ(r14, str, r24, j, z, i);
                    i2++;
                }
                i3++;
                j2 <<= 1;
            }
            AnonymousClass1ZJ r0 = r8.A02;
            if (r0 != null && i2 != 0) {
                r0.A05.getAndAdd(i2);
            }
        }
    }

    public boolean A02(int i) {
        boolean z = false;
        if (this.A02.A02(i) != 0) {
            z = true;
        }
        if (!z) {
            boolean z2 = false;
            if (this.A04.A02(i) != 0) {
                z2 = true;
            }
            if (!z2) {
                boolean z3 = false;
                if (this.A03.A02(i) != 0) {
                    z3 = true;
                }
                if (z3) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    public boolean A03(int i) {
        if (this.A03.A02(i) != 0) {
            return true;
        }
        return false;
    }

    public C09160gd(AnonymousClass0Td[] r2, C39851zi r3, C12210op r4, AnonymousClass1ZJ r5) {
        this.A05 = r2;
        this.A01 = r3;
        this.A00 = r5;
        if (r4 != null && r4.Bwp()) {
            r2 = new AnonymousClass0Td[0];
        }
        this.A02 = new C09170ge(r2, r3, r5);
        this.A04 = new C09220gk(r2, r3, r4, r5);
        this.A03 = new C26601bi(r2, r3);
    }
}
