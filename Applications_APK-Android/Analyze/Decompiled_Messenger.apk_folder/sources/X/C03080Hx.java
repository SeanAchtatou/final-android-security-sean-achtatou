package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Hx  reason: invalid class name and case insensitive filesystem */
public enum C03080Hx {
    GET_PREF_BASED_CONFIG(0, true),
    SET_PREF_BASED_CONFIG(1, false),
    GET_APPS_STATISTICS(2, true),
    GET_ANALYTICS_CONFIG(3, true),
    SET_ANALYTICS_CONFIG(4, false),
    GET_FLYTRAP_REPORT(5, true),
    GET_PREF_IDS(6, true),
    SET_PREF_IDS(7, false),
    NOT_EXIST(Integer.MAX_VALUE, false);
    
    public static final Map A00 = new HashMap();
    public final boolean mHasReturn;
    public final int mOperationType;

    /* access modifiers changed from: public */
    static {
        for (C03080Hx r2 : values()) {
            A00.put(Integer.valueOf(r2.mOperationType), r2);
        }
    }

    private C03080Hx(int i, boolean z) {
        this.mOperationType = i;
        this.mHasReturn = z;
    }
}
