package X;

import android.net.Uri;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.resources.impl.loading.LanguagePackInfo;
import com.google.common.collect.RegularImmutableMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/* renamed from: X.0iF  reason: invalid class name and case insensitive filesystem */
public final class C09650iF implements C09660iG {
    private static final String A01 = AnonymousClass08S.A0J("i18n", C09650iF.class.getName());
    private AnonymousClass0UN A00;

    public static final C09650iF A00(AnonymousClass1XY r1) {
        return new C09650iF(r1);
    }

    public InputStream AqA(C09770if r15) {
        File file;
        Integer num;
        C09670iH r3 = (C09670iH) AnonymousClass1XX.A02(5, AnonymousClass1Y3.B1n, this.A00);
        File[] listFiles = r3.A03.A04(r3.A00).listFiles(new AnonymousClass0iP(new C09710iL(r3)));
        if (listFiles != null) {
            for (File delete : listFiles) {
                delete.delete();
            }
        }
        C10790kr r5 = (C10790kr) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AMn, this.A00);
        int i = AnonymousClass1Y3.APr;
        ((AnonymousClass1Y6) AnonymousClass1XX.A02(3, i, r5.A00)).AOx();
        C09640iE r32 = r15.A04;
        switch (r32.ordinal()) {
            case 0:
                ((AnonymousClass1Y6) AnonymousClass1XX.A02(3, i, r5.A00)).AOx();
                C35871rx r33 = r15.A00;
                if (r33 != null) {
                    try {
                        r33.A01("LanguagePackDownloader:downloadLangpackFileIfNeeded:Started");
                    } catch (Exception e) {
                        e = e;
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C12120oZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ADC, r5.A00)).A00)).markerEnd(4456451, 3);
                        if (r33 != null) {
                            r33.A01("LanguagePackDownloader:downloadLangpackFileIfNeeded:DownloadFailed");
                        }
                        throw e;
                    }
                }
                C09700iK r6 = (C09700iK) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AEl, r5.A00);
                r6.A07(r15.A02);
                C10560kP A03 = r6.A03(r15);
                if (A03 == null) {
                    file = null;
                } else {
                    file = r6.A05(r15.A02, A03);
                }
                if (file != null) {
                    if (r33 != null) {
                        r33.A02(true, 0);
                        r33.A01("LanguagePackDownloader:downloadLangpackFileIfNeeded:FoundInCache");
                        break;
                    }
                } else {
                    if (r33 != null) {
                        r33.A02(false, 0);
                    }
                    LanguagePackInfo A012 = C10790kr.A01(r5, r15);
                    File file2 = new File(((C09700iK) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AEl, r5.A00)).A04(r15.A02), C10560kP.A01(A012.locale, (long) A012.releaseNumber, A012.contentChecksum, AnonymousClass07B.A01));
                    C191208vC r8 = new C191208vC(Uri.parse(A012.downloadUrl), new C191088uq(r5, A012.downloadChecksum, file2), CallerContext.A04(r5.getClass()), r15.A00(), RegularImmutableMap.A03);
                    int i2 = AnonymousClass1Y3.BBd;
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, i2, ((C12120oZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ADC, r5.A00)).A00)).markerStart(4456451);
                    if (r33 != null) {
                        r33.A01("LanguagePackDownloader:downloadLangpackFileIfNeeded:DownloadStarted");
                    }
                    ((AnonymousClass29m) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B4S, r5.A00)).A02(r8);
                    file2.getName();
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, i2, ((C12120oZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ADC, r5.A00)).A00)).markerEnd(4456451, 2);
                    if (r33 != null) {
                        r33.A01("LanguagePackDownloader:downloadLangpackFileIfNeeded:DownloadSucceed");
                    }
                    C09700iK r4 = (C09700iK) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AEl, r5.A00);
                    r4.A07(r15.A02);
                    C10560kP A032 = r4.A03(r15);
                    if (A032 != null) {
                        file = r4.A05(r15.A02, A032);
                        break;
                    }
                    file = null;
                    break;
                }
                break;
            case 1:
                ((AnonymousClass1Y6) AnonymousClass1XX.A02(3, i, r5.A00)).AOx();
                C35871rx r34 = r15.A00;
                try {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C12120oZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ADC, r5.A00)).A00)).markerStart(4456459);
                    if (r34 != null) {
                        r34.A01("LanguagePackDownloader:downloadFbtLanguagePackUsingDod:Started");
                    }
                    if (((AnonymousClass1YI) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AcD, r5.A00)).AbO(AnonymousClass1Y3.A4X, false)) {
                        C30458Ewq ewq = (C30458Ewq) AnonymousClass1XX.A03(AnonymousClass1Y3.B4v, r5.A00);
                        if (r34 != null) {
                            r34.A01("LanguagePackDownloader:downloadFbtLanguagePackUsingLazyDod:InjectCompleted");
                        }
                        ewq.fetchLPResourceFile("fbt_language_pack.bin", r15.A07.toString());
                    } else {
                        C30460Ews ews = (C30460Ews) AnonymousClass1XX.A03(AnonymousClass1Y3.AOp, r5.A00);
                        if (r34 != null) {
                            r34.A01("LanguagePackDownloader:downloadFbtLanguagePackUsingDefaultDod:InjectCompleted");
                        }
                        String locale = r15.A07.toString();
                        if (((AnonymousClass1YI) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AcD, r5.A00)).AbO(534, false)) {
                            num = AnonymousClass07B.A0C;
                        } else {
                            num = AnonymousClass07B.A01;
                        }
                        ews.fetchOptionalResourceFile$REDEX$VQIYq5RxeY9("fbt_language_pack.bin", locale, num);
                    }
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C12120oZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ADC, r5.A00)).A00)).markerEnd(4456459, 2);
                    if (r34 != null) {
                        r34.A02(false, 0);
                        r34.A01("LanguagePackDownloader:downloadFbtLanguagePackUsingDod:Succeed");
                    }
                    file = null;
                    break;
                } catch (AnonymousClass3AI e2) {
                    if (r34 != null) {
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, r34.A00)).markerAnnotate(30474250, "failure_category", e2.mCategory.text);
                    }
                    throw e2;
                } catch (Exception e3) {
                    e = e3;
                    C010708t.A0L(r5.A01, "fetching fbt language pack using dod failed with exception", e);
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C12120oZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.ADC, r5.A00)).A00)).markerEnd(4456459, 3);
                    if (r34 != null) {
                        r34.A01("LanguagePackDownloader:downloadFbtLanguagePackUsingDod:Failed");
                    }
                    throw e;
                }
            default:
                throw new RuntimeException("Request is for unrecognized language file format : " + r32);
        }
        if (file != null) {
            file.getName();
        }
        return new FileInputStream(file);
    }

    public void BYb(C09770if r4) {
        File file;
        C10560kP A03;
        C010708t.A0I(A01, "onFailure() called");
        C09700iK r2 = (C09700iK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AEl, this.A00);
        if (r4.A04 != C09640iE.LANGPACK || (A03 = r2.A03(r4)) == null) {
            file = null;
        } else {
            file = r2.A05(r4.A02, A03);
        }
        if (file != null && file.exists()) {
            file.delete();
            C010708t.A0P(C09700iK.A00, "Deleted potentially corrupted lang file: %s", file.getName());
        }
        ((C35871rx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AhK, this.A00)).A01("DownloadableLanguagePackLoaderDelegate:onFailure");
    }

    public void Bhq() {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C12120oZ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADC, this.A00)).A00)).markerEnd(4456450, 2);
        ((C35871rx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AhK, this.A00)).A01("DownloadableLanguagePackLoaderDelegate:onParseDone");
    }

    public void Bhr() {
        C010708t.A0I(A01, "onParseFailure() called");
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C12120oZ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADC, this.A00)).A00)).markerEnd(4456450, 3);
        ((C35871rx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AhK, this.A00)).A01("DownloadableLanguagePackLoaderDelegate:onParseFailure");
    }

    public void Bhs() {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C12120oZ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADC, this.A00)).A00)).markerStart(4456450);
        ((C35871rx) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AhK, this.A00)).A01("DownloadableLanguagePackLoaderDelegate:onParseStart");
    }

    private C09650iF(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(6, r3);
    }
}
