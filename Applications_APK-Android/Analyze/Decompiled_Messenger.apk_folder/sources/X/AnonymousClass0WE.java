package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0WE  reason: invalid class name */
public final class AnonymousClass0WE extends AnonymousClass0UW {
    private static volatile AnonymousClass0WE A00;

    private AnonymousClass0WE(AnonymousClass1Y6 r2) {
        super(r2, "Gatekeeper Init Lock Held");
    }

    public static final AnonymousClass0WE A01(AnonymousClass1XY r4) {
        if (A00 == null) {
            synchronized (AnonymousClass0WE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r4);
                if (A002 != null) {
                    try {
                        A00 = new AnonymousClass0WE(AnonymousClass0UX.A07(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
