package X;

import android.app.Activity;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import javax.inject.Singleton;
import org.json.JSONObject;

@Singleton
/* renamed from: X.1eH  reason: invalid class name and case insensitive filesystem */
public final class C28111eH {
    private static volatile C28111eH A01;
    public AnonymousClass0UN A00;

    public static final C28111eH A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C28111eH.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C28111eH(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static void A01(C28111eH r3, JSONObject jSONObject, Activity activity) {
        if (jSONObject != null) {
            USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Ap7, r3.A00)).A01("android_security_fb4a_intent_logging"), 34);
            if (uSLEBaseShape0S0000000.A0G()) {
                uSLEBaseShape0S0000000.A0D("activity_name", activity.getClass().getName());
                uSLEBaseShape0S0000000.A0D("intent", jSONObject.toString());
                uSLEBaseShape0S0000000.A0D("caller_identity", C06620bo.A00(C13360rJ.A00(activity.getIntent(), null)));
                uSLEBaseShape0S0000000.A06();
            }
        }
    }

    private C28111eH(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(5, r3);
    }
}
