package X;

import android.os.SystemClock;

/* renamed from: X.01h  reason: invalid class name and case insensitive filesystem */
public final class C001801h {
    public long A00;
    private long A01 = 0;
    private long A02 = 0;
    private boolean A03;

    public synchronized long A00() {
        long j;
        if (this.A03) {
            j = 0;
        } else {
            j = SystemClock.uptimeMillis() - this.A01;
        }
        return j;
    }

    public synchronized long A01() {
        return this.A02 + A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0007, code lost:
        if (r7 != false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A02(boolean r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r2 = r6.A03     // Catch:{ all -> 0x0033 }
            r1 = 1
            if (r2 == 0) goto L_0x0009
            r0 = 1
            if (r7 == 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r2 != 0) goto L_0x0011
            if (r7 == 0) goto L_0x0011
        L_0x000e:
            if (r1 == 0) goto L_0x0020
            goto L_0x0013
        L_0x0011:
            r1 = 0
            goto L_0x000e
        L_0x0013:
            long r4 = r6.A02     // Catch:{ all -> 0x0033 }
            long r2 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0033 }
            long r0 = r6.A01     // Catch:{ all -> 0x0033 }
            long r2 = r2 - r0
            long r4 = r4 + r2
            r6.A02 = r4     // Catch:{ all -> 0x0033 }
            goto L_0x002f
        L_0x0020:
            if (r0 == 0) goto L_0x002f
            long r0 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0033 }
            r6.A01 = r0     // Catch:{ all -> 0x0033 }
            long r2 = r6.A00     // Catch:{ all -> 0x0033 }
            r0 = 1
            long r2 = r2 + r0
            r6.A00 = r2     // Catch:{ all -> 0x0033 }
        L_0x002f:
            r6.A03 = r7     // Catch:{ all -> 0x0033 }
            monitor-exit(r6)
            return
        L_0x0033:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C001801h.A02(boolean):void");
    }

    public C001801h(boolean z) {
        long j = 0;
        this.A03 = z;
        this.A01 = SystemClock.uptimeMillis();
        this.A00 = !z ? 1 : j;
    }
}
