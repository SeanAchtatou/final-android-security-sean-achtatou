package X;

import com.facebook.common.util.TriState;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.1bj  reason: invalid class name and case insensitive filesystem */
public final class C26611bj implements C26171b1 {
    public final C08510fT A00;
    public final QuickPerformanceLogger A01;

    private static int A00(int i) {
        if (i == 1) {
            return 27328525;
        }
        if (i == 5) {
            return 27328516;
        }
        if (i == 10) {
            return 27328518;
        }
        if (i == 15) {
            return 27328517;
        }
        if (i == 20) {
            return 27328524;
        }
        if (i == 25) {
            return 27328519;
        }
        if (i == 30) {
            return 27328522;
        }
        if (i != 35) {
            return i != 40 ? 27328523 : 27328526;
        }
        return 27328520;
    }

    public C26611bj(QuickPerformanceLogger quickPerformanceLogger, C08510fT r2) {
        this.A01 = quickPerformanceLogger;
        this.A00 = r2;
    }

    public void BJ9(C26461bU r13) {
        Map emptyMap;
        Map emptyMap2;
        Map emptyMap3;
        Map emptyMap4;
        Map emptyMap5;
        r13.toString();
        if (r13.A0A) {
            this.A01.markEvent(A00(r13.A05), r13.A03, 7);
            return;
        }
        int A002 = A00(r13.A05);
        if (this.A00 != null) {
            C26471bV r0 = r13.A09;
            if (r0 != null) {
                emptyMap4 = r0.A01;
            } else {
                emptyMap4 = Collections.emptyMap();
            }
            Long l = (Long) emptyMap4.get("trigger_source_id");
            if (l != null) {
                r13.A09.A02.put("trigger_source_name", C03020Ho.A00(l.intValue()));
            }
            if (r13.A05 == 40) {
                C26471bV r02 = r13.A09;
                if (r02 != null) {
                    emptyMap5 = r02.A01;
                } else {
                    emptyMap5 = Collections.emptyMap();
                }
                Long l2 = (Long) emptyMap5.get("overlapping_id");
                if (l2 != null) {
                    r13.A09.A02.put("overlapping_name", C03020Ho.A00(l2.intValue()));
                }
            }
        }
        this.A01.markerStart(A002, r13.A06, r13.A07);
        C21061Ew withMarker = this.A01.withMarker(A002, r13.A06);
        C26471bV r03 = r13.A09;
        if (r03 != null) {
            emptyMap = r03.A02;
        } else {
            emptyMap = Collections.emptyMap();
        }
        for (Map.Entry entry : emptyMap.entrySet()) {
            if (entry.getValue() != null) {
                withMarker.A08((String) entry.getKey(), (String) entry.getValue());
            }
        }
        C26471bV r04 = r13.A09;
        if (r04 != null) {
            emptyMap2 = r04.A01;
        } else {
            emptyMap2 = Collections.emptyMap();
        }
        for (Map.Entry entry2 : emptyMap2.entrySet()) {
            if (entry2.getValue() != null) {
                withMarker.A06((String) entry2.getKey(), ((Long) entry2.getValue()).longValue());
            }
        }
        C26471bV r05 = r13.A09;
        if (r05 != null) {
            emptyMap3 = r05.A00;
        } else {
            emptyMap3 = Collections.emptyMap();
        }
        for (Map.Entry entry3 : emptyMap3.entrySet()) {
            if (entry3.getValue() != null) {
                withMarker.A04((String) entry3.getKey(), ((Double) entry3.getValue()).doubleValue());
            }
        }
        withMarker.A06("duration_microseconds", (r13.A02 - r13.A08) / 1000);
        withMarker.BK9();
        this.A01.markerEnd(A002, r13.A06, (short) r13.A00, r13.A01, TriState.NO);
    }
}
