package X;

/* renamed from: X.09y  reason: invalid class name and case insensitive filesystem */
public final class C013709y implements AnonymousClass084 {
    public boolean A00;
    public boolean A01;

    public String toString() {
        return "[ " + getClass().getSimpleName() + " disableGcConcurrent: " + false + " concurrentGcThreshold: " + 0 + " disableNativeGcLogic: " + this.A00 + " growHeapAggresivly: " + this.A01 + " disableMinConcurrent: " + false + " ]";
    }
}
