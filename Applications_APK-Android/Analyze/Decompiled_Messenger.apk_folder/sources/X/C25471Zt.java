package X;

import android.content.Context;
import com.facebook.auth.viewercontext.ViewerContext;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.1Zt  reason: invalid class name and case insensitive filesystem */
public final class C25471Zt extends C24831Xg implements C24851Xi, AnonymousClass062 {
    public AnonymousClass0XN A00;
    public AnonymousClass09P A01;
    private C05630a0 A02;
    private final Context A03;
    private final C24921Xq A04;
    private final ViewerContext A05;
    private final ReentrantLock A06 = new ReentrantLock();

    public Object AYf() {
        return this.A04.A03(this);
    }

    public void AZH(Object obj) {
        C24811Xe r1 = (C24811Xe) obj;
        r1.A03();
        r1.A02();
    }

    public Context Aq3() {
        return this.A03;
    }

    /* JADX INFO: finally extract failed */
    public C05920aY B9T() {
        this.A06.lock();
        try {
            if (this.A02 == null) {
                this.A02 = new C05630a0(this.A00, this.A05, this.A01);
            }
            this.A06.unlock();
            return this.A02;
        } catch (Throwable th) {
            this.A06.unlock();
            throw th;
        }
    }

    public C25471Zt(C24921Xq r3, Context context, AnonymousClass1XX r5, ViewerContext viewerContext) {
        super(r5);
        this.A04 = r3;
        this.A03 = context;
        this.A05 = viewerContext;
        AnonymousClass1XX r1 = AnonymousClass1XX.get(context);
        this.A00 = C25481Zu.A00(r1);
        this.A01 = C04750Wa.A01(r1);
    }
}
