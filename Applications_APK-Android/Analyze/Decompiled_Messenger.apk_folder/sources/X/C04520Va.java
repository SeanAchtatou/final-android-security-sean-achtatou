package X;

import java.util.Comparator;

/* renamed from: X.0Va  reason: invalid class name and case insensitive filesystem */
public final class C04520Va implements Comparator {
    public static C04520Va A00 = new C04520Va();

    public int compare(Object obj, Object obj2) {
        C07690e0 r7 = (C07690e0) obj;
        C07690e0 r8 = (C07690e0) obj2;
        long A002 = r7.A00() - r8.A00();
        if (A002 == 0) {
            return C04530Vb.A00(r7, r8);
        }
        if (A002 < 0) {
            return -1;
        }
        return 1;
    }
}
