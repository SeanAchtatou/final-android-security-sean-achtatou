package X;

import com.facebook.compactdisk.common.PrivacyGuard;
import com.facebook.compactdisk.current.Scope;

/* renamed from: X.1iE  reason: invalid class name and case insensitive filesystem */
public final class C30541iE {
    private Scope A00 = null;
    private final PrivacyGuard A01;

    public synchronized Scope A00() {
        if (this.A00 == null) {
            this.A00 = new Scope(this.A01.getUUID());
        }
        return this.A00;
    }

    public synchronized void A01() {
        Scope scope = this.A00;
        if (scope != null) {
            scope.invalidate();
            this.A00 = null;
        }
    }

    public C30541iE(PrivacyGuard privacyGuard) {
        this.A01 = privacyGuard;
    }
}
