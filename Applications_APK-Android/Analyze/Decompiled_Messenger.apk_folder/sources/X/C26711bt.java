package X;

import com.facebook.acra.ACRA;

/* renamed from: X.1bt  reason: invalid class name and case insensitive filesystem */
public final class C26711bt extends Thread {
    public static final String __redex_internal_original_name = "com.facebook.analytics.anrwatchdog.ANRDetectorController$1";
    public final /* synthetic */ C05620Zs A00;

    public C26711bt(C05620Zs r1) {
        this.A00 = r1;
    }

    public void run() {
        C05620Zs r3 = this.A00;
        synchronized (r3) {
            if (((C09740ic) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJc, r3.A00)).shouldANRDetectorRun()) {
                if (!r3.A02) {
                    r3.A02 = true;
                    ACRA.setANRDetectorCheckIntervalMs(r3.A03.At0(566690869937890L));
                }
                ACRA.startANRDetector();
            } else {
                ACRA.stopANRDetector();
            }
        }
    }
}
