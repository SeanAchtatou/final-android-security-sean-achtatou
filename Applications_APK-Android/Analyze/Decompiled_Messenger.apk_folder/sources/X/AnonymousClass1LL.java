package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1LL  reason: invalid class name */
public final class AnonymousClass1LL extends C35531rN {
    private static volatile AnonymousClass1LL A05;
    public AnonymousClass0UN A00;
    public Boolean A01;
    public Boolean A02;
    public final C04310Tq A03;
    public final C04310Tq A04;

    public static final AnonymousClass1LL A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (AnonymousClass1LL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new AnonymousClass1LL(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private AnonymousClass1LL(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
        this.A03 = AnonymousClass0VG.A00(AnonymousClass1Y3.AgD, r3);
        this.A04 = AnonymousClass0VG.A00(AnonymousClass1Y3.Af4, r3);
    }
}
