package X;

/* renamed from: X.0Gn  reason: invalid class name and case insensitive filesystem */
public final class C02820Gn implements C02780Gi {
    public void C2x(AnonymousClass0FM r4, C02910Gx r5) {
        AnonymousClass0FZ r42 = (AnonymousClass0FZ) r4;
        r5.AMT("battery_pct", (double) r42.batteryLevelPct);
        r5.AMV("battery_realtime_ms", r42.batteryRealtimeMs);
        r5.AMV("charging_realtime_ms", r42.chargingRealtimeMs);
    }
}
