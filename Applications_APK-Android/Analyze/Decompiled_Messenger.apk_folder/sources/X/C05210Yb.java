package X;

import android.os.Handler;
import android.os.Looper;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;

/* renamed from: X.0Yb  reason: invalid class name and case insensitive filesystem */
public final class C05210Yb implements Comparator {
    private final Looper A00 = Looper.getMainLooper();
    public final /* synthetic */ AnonymousClass0Y6 A01;

    public C05210Yb(AnonymousClass0Y6 r2) {
        this.A01 = r2;
    }

    private boolean A00(ExecutorService executorService) {
        Handler handler;
        if (executorService == ((ExecutorService) AnonymousClass1XX.A02(9, AnonymousClass1Y3.ADs, this.A01.A05))) {
            return true;
        }
        if ((executorService instanceof AnonymousClass0VJ) && (handler = ((AnonymousClass0VJ) executorService).getHandler()) != null && handler.getLooper() == this.A00) {
            return true;
        }
        return false;
    }

    /* renamed from: A01 */
    public int compare(AnonymousClass0XY r5, AnonymousClass0XY r6) {
        boolean A002;
        int ordinal = r5.A02.ordinal();
        int ordinal2 = r6.A02.ordinal();
        if (ordinal >= ordinal2) {
            if (ordinal <= ordinal2) {
                if (!this.A01.A0E || (A002 = A00(r5.A05)) == A00(r6.A05)) {
                    int i = r5.A01;
                    int i2 = r6.A01;
                    if (i >= i2) {
                        if (i <= i2) {
                            int i3 = r5.A00;
                            int i4 = r6.A00;
                            if (i3 >= i4) {
                                if (i3 <= i4) {
                                    return 0;
                                }
                            }
                        }
                    }
                } else if (A002) {
                    return 1;
                }
            }
            return 1;
        }
        return -1;
    }
}
