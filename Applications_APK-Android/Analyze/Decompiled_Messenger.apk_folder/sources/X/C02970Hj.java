package X;

/* renamed from: X.0Hj  reason: invalid class name and case insensitive filesystem */
public final class C02970Hj extends C02320Ea {
    public long A00;
    public C02980Hk A01 = new C02980Hk();
    public C02350Ed A02;
    public String A03;

    public C02320Ea A00(String str, int i) {
        this.A01.A00(str, Integer.toString(i));
        return this;
    }

    public C02320Ea A01(String str, long j) {
        this.A01.A00(str, Long.toString(j));
        return this;
    }

    public C02320Ea A02(String str, Object obj) {
        this.A01.A00(str, String.valueOf(obj));
        return this;
    }

    public void A03() {
        this.A02.AaI(this.A00, this.A03, this.A01);
    }
}
