package X;

import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import java.lang.reflect.Field;

/* renamed from: X.1rM  reason: invalid class name and case insensitive filesystem */
public final class C35521rM {
    public static int A00 = Integer.MIN_VALUE;

    public static int A00() {
        int i;
        if (A00 == Integer.MIN_VALUE) {
            if (Build.VERSION.SDK_INT < 24) {
                Class<C35521rM> cls = C35521rM.class;
                try {
                    Field field = DisplayMetrics.class.getField("DENSITY_DEVICE");
                    field.setAccessible(true);
                    i = field.getInt(null);
                } catch (NoSuchFieldException e) {
                    C010708t.A0A(cls, "Unable to find DENSITY_DEVICE field", e);
                } catch (IllegalAccessException e2) {
                    C010708t.A0A(cls, "Unable to access DENSITY_DEVICE field", e2);
                }
                A00 = i;
            } else {
                A00 = DisplayMetrics.DENSITY_DEVICE_STABLE;
            }
        }
        return A00;
        i = Resources.getSystem().getDisplayMetrics().densityDpi;
        A00 = i;
        return A00;
    }

    private C35521rM() {
    }
}
