package X;

import android.graphics.ColorSpace;
import android.util.Pair;

/* renamed from: X.1SL  reason: invalid class name */
public final class AnonymousClass1SL {
    public final ColorSpace A00;
    public final Pair A01;

    public AnonymousClass1SL(int i, int i2, ColorSpace colorSpace) {
        Pair pair;
        if (i == -1 || i2 == -1) {
            pair = null;
        } else {
            pair = new Pair(Integer.valueOf(i), Integer.valueOf(i2));
        }
        this.A01 = pair;
        this.A00 = colorSpace;
    }
}
