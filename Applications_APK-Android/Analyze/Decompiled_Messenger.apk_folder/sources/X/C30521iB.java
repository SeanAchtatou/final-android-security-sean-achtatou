package X;

import com.facebook.crypto.module.LoggedInUserCrypto;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1iB  reason: invalid class name and case insensitive filesystem */
public final class C30521iB extends AnonymousClass0UV {
    private static volatile LoggedInUserCrypto A00;

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r7.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0062, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.facebook.crypto.module.LoggedInUserCrypto A00(X.AnonymousClass1XY r9) {
        /*
            com.facebook.crypto.module.LoggedInUserCrypto r0 = X.C30521iB.A00
            if (r0 != 0) goto L_0x006b
            java.lang.Class<com.facebook.crypto.module.LoggedInUserCrypto> r8 = com.facebook.crypto.module.LoggedInUserCrypto.class
            monitor-enter(r8)
            com.facebook.crypto.module.LoggedInUserCrypto r0 = X.C30521iB.A00     // Catch:{ all -> 0x0068 }
            X.0WD r7 = X.AnonymousClass0WD.A00(r0, r9)     // Catch:{ all -> 0x0068 }
            if (r7 == 0) goto L_0x0066
            X.1XY r0 = r9.getApplicationInjector()     // Catch:{ all -> 0x005e }
            X.18A r6 = X.AnonymousClass18A.A00(r0)     // Catch:{ all -> 0x005e }
            X.0XQ r2 = X.AnonymousClass0XP.A00(r0)     // Catch:{ all -> 0x005e }
            X.09P r5 = X.C04750Wa.A01(r0)     // Catch:{ all -> 0x005e }
            X.0Tq r4 = X.AnonymousClass0XJ.A0K(r0)     // Catch:{ all -> 0x005e }
            X.AnonymousClass0WT.A00(r0)     // Catch:{ all -> 0x005e }
            java.lang.Object r1 = r4.get()     // Catch:{ all -> 0x005e }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x005e }
            com.facebook.crypto.module.LightSharedPreferencesPersistence r0 = new com.facebook.crypto.module.LightSharedPreferencesPersistence     // Catch:{ all -> 0x005e }
            r0.<init>(r2, r5)     // Catch:{ all -> 0x005e }
            X.18x r3 = new X.18x     // Catch:{ all -> 0x005e }
            r3.<init>(r0, r6, r1, r5)     // Catch:{ all -> 0x005e }
            X.192 r2 = new X.192     // Catch:{ all -> 0x005e }
            X.18C r1 = r6.A00     // Catch:{ all -> 0x005e }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x005e }
            r2.<init>(r3, r1, r0)     // Catch:{ all -> 0x005e }
            java.lang.Class<com.facebook.crypto.module.LoggedInUserCryptoHybrid> r1 = com.facebook.crypto.module.LoggedInUserCryptoHybrid.class
            monitor-enter(r1)     // Catch:{ all -> 0x005e }
            com.facebook.crypto.module.LoggedInUserCryptoHybrid r0 = com.facebook.crypto.module.LoggedInUserCryptoHybrid.sInstance     // Catch:{ all -> 0x005b }
            if (r0 != 0) goto L_0x004d
            com.facebook.crypto.module.LoggedInUserCryptoHybrid r0 = new com.facebook.crypto.module.LoggedInUserCryptoHybrid     // Catch:{ all -> 0x005b }
            r0.<init>()     // Catch:{ all -> 0x005b }
            com.facebook.crypto.module.LoggedInUserCryptoHybrid.sInstance = r0     // Catch:{ all -> 0x005b }
        L_0x004d:
            com.facebook.crypto.module.LoggedInUserCryptoHybrid r0 = com.facebook.crypto.module.LoggedInUserCryptoHybrid.sInstance     // Catch:{ all -> 0x005b }
            monitor-exit(r1)     // Catch:{ all -> 0x005e }
            r0.setKeyChain(r3)     // Catch:{ all -> 0x005e }
            com.facebook.crypto.module.LoggedInUserCrypto r0 = new com.facebook.crypto.module.LoggedInUserCrypto     // Catch:{ all -> 0x005e }
            r0.<init>(r2, r3, r5, r4)     // Catch:{ all -> 0x005e }
            X.C30521iB.A00 = r0     // Catch:{ all -> 0x005e }
            goto L_0x0063
        L_0x005b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            r7.A01()     // Catch:{ all -> 0x0068 }
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x0063:
            r7.A01()     // Catch:{ all -> 0x0068 }
        L_0x0066:
            monitor-exit(r8)     // Catch:{ all -> 0x0068 }
            goto L_0x006b
        L_0x0068:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0068 }
            throw r0
        L_0x006b:
            com.facebook.crypto.module.LoggedInUserCrypto r0 = X.C30521iB.A00
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30521iB.A00(X.1XY):com.facebook.crypto.module.LoggedInUserCrypto");
    }
}
