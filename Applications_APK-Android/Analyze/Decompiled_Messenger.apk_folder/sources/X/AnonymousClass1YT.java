package X;

import java.util.concurrent.ConcurrentHashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1YT  reason: invalid class name */
public final class AnonymousClass1YT {
    private static volatile AnonymousClass1YT A06;
    public AnonymousClass0UN A00;
    public final AnonymousClass1YW A01;
    public final AnonymousClass1YW A02;
    private final AnonymousClass1YU A03 = new AnonymousClass1YU(this);
    private final AnonymousClass1YV A04;
    private final ConcurrentHashMap A05 = new ConcurrentHashMap();

    public static final AnonymousClass1YT A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (AnonymousClass1YT.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new AnonymousClass1YT(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0021, code lost:
        if (r1.contains(X.AnonymousClass1ZW.A02) == false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass1ZX r4) {
        /*
            r3 = this;
            int r2 = X.AnonymousClass1Y3.BTd
            X.0UN r1 = r3.A00
            r0 = 4
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1YY r1 = (X.AnonymousClass1YY) r1
            int r0 = r4.A02
            java.util.concurrent.ConcurrentSkipListSet r1 = r1.A03
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.remove(r0)
            java.util.EnumSet r1 = r4.A01
            if (r1 == 0) goto L_0x0023
            X.1ZW r0 = X.AnonymousClass1ZW.A02
            boolean r1 = r1.contains(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 == 0) goto L_0x0038
            X.1YW r0 = r3.A02
        L_0x0028:
            X.1Zb r2 = r0.A02(r4)
            java.util.concurrent.ConcurrentHashMap r1 = r3.A05
            int r0 = r4.A02
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.put(r0, r2)
            return
        L_0x0038:
            X.1YW r0 = r3.A01
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YT.A01(X.1ZX):void");
    }

    private AnonymousClass1YT(AnonymousClass1XY r4) {
        this.A00 = new AnonymousClass0UN(5, r4);
        AnonymousClass1YV r2 = new AnonymousClass1YV(r4);
        this.A04 = r2;
        this.A02 = new AnonymousClass1YW(r2, true);
        this.A01 = new AnonymousClass1YW(this.A04, false);
        ((AnonymousClass1YY) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTd, this.A00)).A00 = this.A03;
    }
}
