package X;

import android.content.Context;
import android.os.Handler;

/* renamed from: X.0p1  reason: invalid class name and case insensitive filesystem */
public final class C12290p1 {
    public static final C12290p1 A08 = new C12290p1();
    public final Handler A00 = new Handler(C07840eF.A00);
    public final AnonymousClass04a A01 = new AnonymousClass04a();
    public final AnonymousClass04a A02 = new AnonymousClass04a();
    public final AnonymousClass04a A03 = new AnonymousClass04a();
    public final AnonymousClass04a A04 = new AnonymousClass04a();
    public final AnonymousClass04a A05 = new AnonymousClass04a();
    public final Object A06 = new Object();
    public volatile C50422dy A07 = new EF3();

    public static Runnable A03(C12290p1 r4, C27581dQ r5, long j) {
        if (j < 0) {
            return null;
        }
        synchronized (r4.A06) {
            if (!r4.A05.containsKey(r5)) {
                return null;
            }
            AnonymousClass04a r1 = r4.A04;
            r1.put(r5, Integer.valueOf(((Integer) r1.getOrDefault(r5, 0)).intValue() + 1));
            EF1 ef1 = new EF1(r4, r5);
            AnonymousClass00S.A05(r4.A00, ef1, j, 1084503336);
            return ef1;
        }
    }

    public static C73183fd A00(C12290p1 r5, Context context, C12260oy r7, long j) {
        r5.A07.BpZ(r7, 3);
        C73183fd A002 = r7.A02(context).A00();
        C27591dR r2 = (C27591dR) C27591dR.A03.A00();
        if (r2 == null) {
            r2 = new C27591dR();
        }
        r2.A02 = A002;
        r2.A01 = C27591dR.A04.getAndIncrement();
        r2.A00 = 3;
        synchronized (r5.A06) {
            r5.A02.put(r7, r2);
        }
        A002.AZY(3);
        r5.A07.BjO(r2.A01, r7);
        A04(r5, r7, j);
        return A002;
    }

    public static C27591dR A01(C12290p1 r2, C12260oy r3) {
        C27591dR r0;
        synchronized (r2.A06) {
            r0 = (C27591dR) r2.A02.get(r3);
        }
        return r0;
    }

    public static Long A02(C12290p1 r2, C12260oy r3) {
        Long l;
        synchronized (r2.A06) {
            l = (Long) r2.A03.remove(r3);
        }
        return l;
    }

    public static void A04(C12290p1 r3, C12260oy r4, long j) {
        if (j >= 0) {
            synchronized (r3.A06) {
                if (r3.A02.containsKey(r4) || r3.A03.containsKey(r4)) {
                    AnonymousClass04a r1 = r3.A01;
                    r1.put(r4, Integer.valueOf(((Integer) r1.getOrDefault(r4, 0)).intValue() + 1));
                    AnonymousClass00S.A05(r3.A00, new EF2(r3, r4, j), j, -1786485736);
                }
            }
        }
    }

    public C27591dR A05(C12260oy r3) {
        C27591dR r0;
        synchronized (this.A06) {
            r0 = (C27591dR) this.A02.remove(r3);
        }
        return r0;
    }

    public C12320p7 A06(C27581dQ r3) {
        C12320p7 r0;
        synchronized (this.A06) {
            r0 = (C12320p7) this.A05.remove(r3);
        }
        return r0;
    }

    private C12290p1() {
    }
}
