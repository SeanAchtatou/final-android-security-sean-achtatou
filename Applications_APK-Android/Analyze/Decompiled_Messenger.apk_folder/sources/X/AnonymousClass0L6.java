package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0L6  reason: invalid class name */
public final class AnonymousClass0L6<A, B> extends LinkedHashMap<A, B> {
    private final int maxEntries;
    public final /* synthetic */ AnonymousClass0L7 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0L6(AnonymousClass0L7 r4, int i) {
        super(i + 1, 1.0f, false);
        this.this$0 = r4;
        this.maxEntries = i;
    }

    public boolean removeEldestEntry(Map.Entry entry) {
        if (size() > this.maxEntries) {
            return true;
        }
        return false;
    }
}
