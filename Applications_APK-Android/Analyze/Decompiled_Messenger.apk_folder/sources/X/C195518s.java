package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.18s  reason: invalid class name and case insensitive filesystem */
public final class C195518s implements C05460Za {
    private static volatile C195518s A01;
    public AnonymousClass0UN A00;

    public void clearUserData() {
    }

    public static final C195518s A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C195518s.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C195518s(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public boolean A01() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282501479007734L);
    }

    private C195518s(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
