package X;

/* renamed from: X.017  reason: invalid class name */
public final class AnonymousClass017 implements Comparable {
    public int A00;
    public AnonymousClass015 A01;

    public int compareTo(Object obj) {
        int i = this.A00;
        int i2 = ((AnonymousClass017) obj).A00;
        if (i == i2) {
            return 0;
        }
        if (i > i2) {
            return 1;
        }
        return -1;
    }
}
