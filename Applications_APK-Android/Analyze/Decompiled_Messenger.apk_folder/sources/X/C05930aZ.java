package X;

import android.util.LruCache;
import com.google.common.collect.ImmutableSet;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0aZ  reason: invalid class name and case insensitive filesystem */
public class C05930aZ {
    public static final Locale A05 = Locale.ENGLISH;
    public static final Locale A06 = Locale.US;
    public static final Locale A07 = new Locale("fb", "HA");
    public final C06940cL A00;
    public final C04310Tq A01;
    public final LruCache A02 = new LruCache(5);
    public final LruCache A03 = new LruCache(5);
    public final AtomicReference A04 = new AtomicReference();

    private Locale A02(Locale locale) {
        Locale locale2 = (Locale) this.A02.get(locale);
        if (locale2 != null) {
            return locale2;
        }
        Locale locale3 = new Locale(locale.getLanguage(), locale.getCountry());
        this.A02.put(locale, locale3);
        return locale3;
    }

    public String A03() {
        Locale locale = (Locale) this.A04.get();
        if (locale == null) {
            locale = A08(A06());
        }
        return AnonymousClass1KT.A00(locale);
    }

    public String A04() {
        Locale locale = (Locale) this.A04.get();
        if (locale == null) {
            locale = (Locale) this.A01.get();
        }
        return AnonymousClass1KT.A00(locale);
    }

    public Locale A06() {
        Locale locale = (Locale) this.A01.get();
        ImmutableSet immutableSet = (ImmutableSet) this.A00.A02.get();
        if (immutableSet.isEmpty() || immutableSet.contains(locale.getLanguage()) || immutableSet.contains(A02(locale).toString()) || locale.toString().equals(A07.toString())) {
            return locale;
        }
        return A06;
    }

    public Locale A07() {
        Locale locale = (Locale) this.A04.get();
        if (locale == null) {
            locale = A06();
        }
        String language = locale.getLanguage();
        char c = 65535;
        int hashCode = language.hashCode();
        if (hashCode != 3260) {
            if (hashCode == 3625 && language.equals("qz")) {
                c = 1;
            }
        } else if (language.equals("fb")) {
            c = 0;
        }
        if (c == 0 || c == 1) {
            return new Locale("en", locale.getCountry(), locale.getVariant());
        }
        return locale;
    }

    public Locale A08(Locale locale) {
        ImmutableSet immutableSet = (ImmutableSet) this.A00.A02.get();
        if (immutableSet.isEmpty()) {
            return locale;
        }
        Locale A022 = A02(locale);
        if (immutableSet.contains(A022.toString())) {
            return A022;
        }
        String language = locale.getLanguage();
        if (!immutableSet.contains(language)) {
            return A05;
        }
        Locale locale2 = (Locale) this.A03.get(language);
        if (locale2 != null) {
            return locale2;
        }
        Locale locale3 = new Locale(language);
        this.A03.put(language, locale3);
        return locale3;
    }

    public C05930aZ(C06940cL r3, C04310Tq r4) {
        this.A00 = r3;
        this.A01 = r4;
    }

    public Locale A05() {
        Locale A08 = A08(A06());
        if ("fil".equals(A08.getLanguage())) {
            return new Locale("tl", A08.getCountry());
        }
        return A08;
    }
}
