package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0xQ  reason: invalid class name and case insensitive filesystem */
public final class C16620xQ {
    public C16620xQ A00;
    public final C181810n A01;
    public final /* synthetic */ C16610xP A02;

    public int A02(int i, Object obj) {
        while (i > 2) {
            int i2 = (((i - 1) >> 1) - 1) >> 1;
            Object obj2 = this.A02.A02[i2];
            if (this.A01.compare(obj2, obj) <= 0) {
                break;
            }
            this.A02.A02[i] = obj2;
            i = i2;
        }
        this.A02.A02[i] = obj;
        return i;
    }

    public C16620xQ(C16610xP r1, C181810n r2) {
        this.A02 = r1;
        this.A01 = r2;
    }

    public static int A00(C16620xQ r5, int i, int i2) {
        int i3 = r5.A02.A01;
        if (i >= i3) {
            return -1;
        }
        boolean z = false;
        if (i > 0) {
            z = true;
        }
        Preconditions.checkState(z);
        int min = Math.min(i, i3 - i2) + i2;
        for (int i4 = i + 1; i4 < min; i4++) {
            C181810n r2 = r5.A01;
            Object[] objArr = r5.A02.A02;
            if (r2.compare(objArr[i4], objArr[i]) < 0) {
                i = i4;
            }
        }
        return i;
    }

    public static int A01(C16620xQ r7, int i, Object obj) {
        int i2;
        if (i == 0) {
            r7.A02.A02[0] = obj;
            return 0;
        }
        int i3 = (i - 1) >> 1;
        C16610xP r6 = r7.A02;
        Object[] objArr = r6.A02;
        Object obj2 = objArr[i3];
        if (!(i3 == 0 || (i2 = (((i3 - 1) >> 1) << 1) + 2) == i3 || (i2 << 1) + 1 < r6.A01)) {
            Object obj3 = objArr[i2];
            if (r7.A01.compare(obj3, obj2) < 0) {
                i3 = i2;
                obj2 = obj3;
            }
        }
        if (r7.A01.compare(obj2, obj) < 0) {
            Object[] objArr2 = r7.A02.A02;
            objArr2[i] = obj2;
            objArr2[i3] = obj;
            return i3;
        }
        r7.A02.A02[i] = obj;
        return i;
    }
}
