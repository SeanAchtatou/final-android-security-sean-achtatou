package X;

import android.content.Context;

/* renamed from: X.0Y5  reason: invalid class name */
public final class AnonymousClass0Y5 {
    private static AnonymousClass0Y5 A06;
    public final int A00;
    public final boolean A01;
    public final boolean A02;
    public final boolean A03;
    public final boolean A04;
    public final boolean A05;

    public static synchronized AnonymousClass0Y5 A00(Context context, C001500z r8) {
        AnonymousClass0Y5 r0;
        AnonymousClass0Y5 r2;
        synchronized (AnonymousClass0Y5.class) {
            r0 = A06;
            if (r0 == null) {
                if (r8 == C001500z.A03) {
                    AnonymousClass00K A012 = AnonymousClass00J.A01(context);
                    r2 = new AnonymousClass0Y5(true, A012.A0O, A012.A1e, A012.A1c, A012.A1f, A012.A1d);
                } else if (r8 == C001500z.A07) {
                    AnonymousClass00K A013 = AnonymousClass00J.A01(context);
                    r2 = new AnonymousClass0Y5(A013.A15, 1, false, A013.A1k, A013.A1m, A013.A1l);
                } else {
                    AnonymousClass00K A014 = AnonymousClass00J.A01(context);
                    r2 = new AnonymousClass0Y5(A014.A1u, 1, false, A014.A1t, A014.A1v, true);
                }
                A06 = r2;
                r0 = A06;
            }
        }
        return r0;
    }

    public String toString() {
        return "LightweightAppChoreographerConfig{enabled=" + this.A02 + ", tasks=" + this.A00 + ", parallelizeHighPriTasks=" + this.A04 + ", earlyCancel=" + this.A01 + ", staySubscribed=" + this.A05 + ", fixInitialLoadComplete=" + this.A03 + '}';
    }

    private AnonymousClass0Y5(boolean z, int i, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.A02 = z;
        this.A00 = i;
        this.A04 = z2;
        this.A01 = z3;
        this.A05 = z4;
        this.A03 = z5;
    }
}
