package X;

import android.view.View;

/* renamed from: X.0zp  reason: invalid class name and case insensitive filesystem */
public final class C17970zp implements View.OnTouchListener {
    public AnonymousClass10N A00;

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002f, code lost:
        if (((java.lang.Boolean) r2).booleanValue() == false) goto L_0x0031;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r4, android.view.MotionEvent r5) {
        /*
            r3 = this;
            X.10N r2 = r3.A00
            if (r2 == 0) goto L_0x0035
            X.2Zp r0 = X.C33181nA.A0C
            if (r0 != 0) goto L_0x000f
            X.2Zp r0 = new X.2Zp
            r0.<init>()
            X.C33181nA.A0C = r0
        L_0x000f:
            X.2Zp r1 = X.C33181nA.A0C
            r1.A01 = r4
            r1.A00 = r5
            X.0zV r0 = r2.A00
            X.0zT r0 = r0.Alq()
            java.lang.Object r2 = r0.AXa(r2, r1)
            X.2Zp r1 = X.C33181nA.A0C
            r0 = 0
            r1.A01 = r0
            r1.A00 = r0
            if (r2 == 0) goto L_0x0031
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r0 = r2.booleanValue()
            r1 = 1
            if (r0 != 0) goto L_0x0032
        L_0x0031:
            r1 = 0
        L_0x0032:
            r0 = 1
            if (r1 != 0) goto L_0x0036
        L_0x0035:
            r0 = 0
        L_0x0036:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17970zp.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
