package X;

/* renamed from: X.158  reason: invalid class name */
public final class AnonymousClass158 {
    public static final AnonymousClass1Y8 A00;
    public static final AnonymousClass1Y8 A01;
    public static final AnonymousClass1Y8 A02;
    public static final AnonymousClass1Y8 A03;
    public static final AnonymousClass1Y8 A04;
    public static final AnonymousClass1Y8 A05;
    public static final AnonymousClass1Y8 A06;
    public static final AnonymousClass1Y8 A07;
    public static final AnonymousClass1Y8 A08;
    public static final AnonymousClass1Y8 A09;
    public static final AnonymousClass1Y8 A0A;
    public static final AnonymousClass1Y8 A0B;

    static {
        AnonymousClass1Y8 A0D = C04350Ue.A0A.A09("dialtone/").A09("clearable/");
        A00 = A0D;
        A09 = A0D.A09("dialtone_upgrade_interstitial_impression");
        AnonymousClass1Y8 r1 = A00;
        A05 = r1.A09("cleared_cache");
        r1.A09("switcher_info_banner_impression_count");
        A06 = r1.A09("switcher_free_info_tooltip_impression_count");
        A08 = r1.A09("switcher_paid_info_tooltip_impression_count");
        A07 = r1.A09("switcher_nux_interstitial_seen");
        r1.A09("switch_to_dialtone_once_already");
        r1.A09("dialtone_switcher_zero_balance_reminder_tooltip_impression");
        r1.A09("dialtone_switcher_zero_balance_reminder_tooltip_displaying");
        r1.A09("dialtone_switcher_info_banner_explicitly_dismissed");
        r1.A09("dialtone_optout_tooltip_shown_already");
        A02 = r1.A09("dialtone_enter_data_mode_timestamp");
        r1.A09("dialtone_optout_tooltip_displaying");
        r1.A09("dialtone_last_time_zb_shown");
        r1.A09("dialtone_zb_impressions");
        A03 = r1.A09("dialtone_last_opt_out_time");
        r1.A09("dialtone_qp_zb_toggle_time");
        A01 = r1.A09("dialtone_disable_free_to_paid_timeout_and_refresh");
        A04 = r1.A09("dialtone_optout_upgrade_dialog");
        A0B = r1.A09("messenger_flex_nux_free_seen");
        A0A = r1.A09("messenger_flex_nux_data_seen");
        r1.A09("messenger_toggle_info_tooltip_impression_count");
    }
}
