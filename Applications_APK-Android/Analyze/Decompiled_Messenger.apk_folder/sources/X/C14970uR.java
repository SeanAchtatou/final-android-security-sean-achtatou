package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0uR  reason: invalid class name and case insensitive filesystem */
public final class C14970uR {
    private static volatile C14970uR A02;
    public C25051Yd A00;
    private AnonymousClass0UN A01;

    public static final C14970uR A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C14970uR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C14970uR(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public AnonymousClass230 A01() {
        if (((AnonymousClass10R) AnonymousClass1XX.A03(AnonymousClass1Y3.BO4, this.A01)).A03(true)) {
            return AnonymousClass230.A07;
        }
        return AnonymousClass230.A09;
    }

    private C14970uR(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(0, r3);
        this.A00 = AnonymousClass0WT.A00(r3);
    }
}
