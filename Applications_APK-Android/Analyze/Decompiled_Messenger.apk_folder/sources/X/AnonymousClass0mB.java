package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0mB  reason: invalid class name */
public final class AnonymousClass0mB {
    private static volatile AnonymousClass0mB A01;
    public final Comparator A00 = new C11160mC();

    public static MessagesCollection A00(AnonymousClass0mB r8, MessagesCollection messagesCollection, MessagesCollection messagesCollection2, boolean z) {
        ImmutableList copyOf;
        boolean z2;
        Message message;
        boolean equal = Objects.equal(messagesCollection.A00, messagesCollection2.A00);
        boolean z3 = false;
        Object[] objArr = {messagesCollection.A00, messagesCollection2.A00};
        if (equal) {
            if (!messagesCollection2.A08()) {
                if (messagesCollection.A08()) {
                    return messagesCollection2;
                }
                HashMap hashMap = new HashMap(messagesCollection2.A04());
                HashMap hashMap2 = new HashMap(messagesCollection2.A04());
                C24971Xv it = messagesCollection2.A01.iterator();
                while (it.hasNext()) {
                    Message message2 = (Message) it.next();
                    hashMap.put(message2.A0q, message2);
                    String str = message2.A0w;
                    if (str != null) {
                        hashMap2.put(str, message2.A0q);
                    }
                }
                C24971Xv it2 = messagesCollection.A01.iterator();
                while (it2.hasNext()) {
                    Message message3 = (Message) it2.next();
                    String str2 = message3.A0q;
                    if (!hashMap.containsKey(str2) && (str2 = (String) hashMap2.get(message3.A0w)) == null) {
                        str2 = message3.A0q;
                    }
                    if (!message3.A14 || (message = (Message) hashMap.get(str2)) == null || message.A14) {
                        hashMap.put(str2, message3);
                    }
                }
                if (z) {
                    if (messagesCollection2.A04() + messagesCollection.A04() != hashMap.size()) {
                        boolean z4 = false;
                        if (messagesCollection2.A04() + messagesCollection.A04() > hashMap.size()) {
                            z4 = true;
                        }
                        AnonymousClass064.A04(z4);
                    }
                }
                Collection values = hashMap.values();
                if (values.isEmpty()) {
                    copyOf = RegularImmutableList.A02;
                } else {
                    ArrayList arrayList = new ArrayList(values);
                    Collections.sort(arrayList, r8.A00);
                    copyOf = ImmutableList.copyOf((Collection) arrayList);
                }
                if (messagesCollection.A02 || messagesCollection2.A02) {
                    z3 = true;
                }
                Message A05 = messagesCollection.A05();
                AnonymousClass064.A00(A05);
                long j = A05.A03;
                Message A052 = messagesCollection2.A05();
                AnonymousClass064.A00(A052);
                if (j >= A052.A03) {
                    z2 = messagesCollection.A03;
                } else {
                    z2 = messagesCollection2.A03;
                }
                C33881oI r1 = new C33881oI();
                r1.A00 = messagesCollection.A00;
                r1.A01(copyOf);
                r1.A03 = z3;
                r1.A04 = z2;
                r1.A02 = true;
                return r1.A00();
            }
            return messagesCollection;
        }
        throw new IllegalArgumentException(StringFormatUtil.formatStrLocaleSafe("ThreadKeys mismatch in new messages(ThreadKey: %s) and old messages(ThreadKey: %s).", objArr));
    }

    public static final AnonymousClass0mB A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0mB.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0mB(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private AnonymousClass0mB(AnonymousClass1XY r3) {
        new AnonymousClass0UN(2, r3);
    }

    public boolean A02(MessagesCollection messagesCollection, MessagesCollection messagesCollection2) {
        boolean z;
        if ((messagesCollection.A08() && messagesCollection2.A08()) || messagesCollection.A08() || messagesCollection2.A08()) {
            return true;
        }
        Message A06 = messagesCollection.A06();
        C24971Xv it = messagesCollection2.A01.iterator();
        while (it.hasNext()) {
            Message message = (Message) it.next();
            if (Objects.equal(message.A0q, A06.A0q) || ((!C06850cB.A0B(message.A0w)) && (!C06850cB.A0B(A06.A0w)) && Objects.equal(message.A0w, A06.A0w))) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }
}
