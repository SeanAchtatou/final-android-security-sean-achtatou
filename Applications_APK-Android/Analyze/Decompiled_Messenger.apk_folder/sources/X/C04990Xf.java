package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0Xf  reason: invalid class name and case insensitive filesystem */
public final class C04990Xf extends AnonymousClass0UV {
    private static final Object A00 = new Object();
    private static volatile C05000Xg A01;
    private static volatile C05000Xg A02;

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r3.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002e, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C05000Xg A00(X.AnonymousClass1XY r5) {
        /*
            X.0Xg r0 = X.C04990Xf.A01
            if (r0 != 0) goto L_0x0037
            java.lang.Class<X.0Xg> r4 = X.C05000Xg.class
            monitor-enter(r4)
            X.0Xg r0 = X.C04990Xf.A01     // Catch:{ all -> 0x0034 }
            X.0WD r3 = X.AnonymousClass0WD.A00(r0, r5)     // Catch:{ all -> 0x0034 }
            if (r3 == 0) goto L_0x0032
            X.1XY r0 = r5.getApplicationInjector()     // Catch:{ all -> 0x002a }
            X.0WB r2 = X.AnonymousClass0WA.A03(r0)     // Catch:{ all -> 0x002a }
            java.util.concurrent.Executor r1 = X.AnonymousClass0UX.A0T(r0)     // Catch:{ all -> 0x002a }
            X.0Xg r0 = new X.0Xg     // Catch:{ all -> 0x002a }
            r0.<init>(r1)     // Catch:{ all -> 0x002a }
            monitor-enter(r2)     // Catch:{ all -> 0x002a }
            r2.A00 = r0     // Catch:{ all -> 0x0027 }
            monitor-exit(r2)     // Catch:{ all -> 0x002a }
            X.C04990Xf.A01 = r0     // Catch:{ all -> 0x002a }
            goto L_0x002f
        L_0x0027:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x002a }
            throw r0     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
            r3.A01()     // Catch:{ all -> 0x0034 }
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x002f:
            r3.A01()     // Catch:{ all -> 0x0034 }
        L_0x0032:
            monitor-exit(r4)     // Catch:{ all -> 0x0034 }
            goto L_0x0037
        L_0x0034:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0034 }
            throw r0
        L_0x0037:
            X.0Xg r0 = X.C04990Xf.A01
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04990Xf.A00(X.1XY):X.0Xg");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r3.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002e, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C05000Xg A01(X.AnonymousClass1XY r5) {
        /*
            X.0Xg r0 = X.C04990Xf.A02
            if (r0 != 0) goto L_0x0037
            java.lang.Object r4 = X.C04990Xf.A00
            monitor-enter(r4)
            X.0Xg r0 = X.C04990Xf.A02     // Catch:{ all -> 0x0034 }
            X.0WD r3 = X.AnonymousClass0WD.A00(r0, r5)     // Catch:{ all -> 0x0034 }
            if (r3 == 0) goto L_0x0032
            X.1XY r0 = r5.getApplicationInjector()     // Catch:{ all -> 0x002a }
            X.0WB r2 = com.facebook.gk.sessionless.GkSessionlessModule.A03(r0)     // Catch:{ all -> 0x002a }
            java.util.concurrent.Executor r1 = X.AnonymousClass0UX.A0T(r0)     // Catch:{ all -> 0x002a }
            X.0Xg r0 = new X.0Xg     // Catch:{ all -> 0x002a }
            r0.<init>(r1)     // Catch:{ all -> 0x002a }
            monitor-enter(r2)     // Catch:{ all -> 0x002a }
            r2.A00 = r0     // Catch:{ all -> 0x0027 }
            monitor-exit(r2)     // Catch:{ all -> 0x002a }
            X.C04990Xf.A02 = r0     // Catch:{ all -> 0x002a }
            goto L_0x002f
        L_0x0027:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x002a }
            throw r0     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
            r3.A01()     // Catch:{ all -> 0x0034 }
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x002f:
            r3.A01()     // Catch:{ all -> 0x0034 }
        L_0x0032:
            monitor-exit(r4)     // Catch:{ all -> 0x0034 }
            goto L_0x0037
        L_0x0034:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0034 }
            throw r0
        L_0x0037:
            X.0Xg r0 = X.C04990Xf.A02
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04990Xf.A01(X.1XY):X.0Xg");
    }
}
