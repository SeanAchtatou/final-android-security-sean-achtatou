package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.19K  reason: invalid class name */
public final class AnonymousClass19K extends AnonymousClass0W4 {
    private static final C06060am A00 = new C06050al(ImmutableList.of(AnonymousClass182.A05));
    private static final ImmutableList A01 = ImmutableList.of(AnonymousClass182.A05, AnonymousClass182.A00, AnonymousClass182.A02, AnonymousClass182.A03, AnonymousClass182.A04, AnonymousClass182.A01);

    public void A0C(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public AnonymousClass19K() {
        super("thread_participants", A01, A00);
    }
}
