package X;

import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.1dp  reason: invalid class name and case insensitive filesystem */
public final class C27831dp implements C05700aB {
    public static final AnonymousClass1Y7 A00;
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06 = ((AnonymousClass1Y7) C04350Ue.A05.A09("fb_android/"));
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y8 A09 = C04350Ue.A0A.A09("fb_android/");
    public static final AnonymousClass1Y8 A0A = A09.A09("login_broadcasted_cross_app");

    static {
        AnonymousClass1Y7 r1 = A06;
        r1.A09("kvm/");
        r1.A09("uvm/");
        r1.A09("events/");
        r1.A09("client_time_offset_via_login_approvals/");
        r1.A09("login_approvals_secret/");
        r1.A09("video_stall_bug_report");
        r1.A09("video_spec_display");
        r1.A09("photo_360_spec_display");
        r1.A09("video_inline_unmute");
        r1.A09(TurboLoader.Locator.$const$string(AnonymousClass1Y3.A1O));
        r1.A09("video_force_autoplay");
        r1.A09("video_home_force_prefetch");
        r1.A09("video_home_data_fetch_toast");
        r1.A09("video_home_debug_overlay");
        r1.A09("watch_growth_debug_overlay");
        r1.A09("in_progress_login_timestamp");
        r1.A09("last_login_time");
        r1.A09("last_username");
        r1.A09("hashed_uid");
        r1.A09("last_logout_time");
        A07 = (AnonymousClass1Y7) r1.A09("intern_settings_history");
        r1.A09("jewel_footer_promo_times_shown_since_last_reset");
        r1.A09("jewel_footer_promo_times_shown_total");
        r1.A09("jewel_footer_promo_last_shown_secs");
        r1.A09("insights_context_capture");
        r1.A09("insights_context_capture_manual");
        r1.A09("in_app_browser_profiling");
        r1.A09("in_app_browser_debug_overlay");
        AnonymousClass1Y7 r12 = A06;
        A05 = (AnonymousClass1Y7) r12.A09("facecast_debug_overlay");
        r12.A09("facecast_broadcast_established_debug_toast");
        r12.A09("first_boot_page_seen_v4");
        A08 = (AnonymousClass1Y7) r12.A09("scroll_away_threshold");
        r12.A09("gltf_scene_overlay");
        r12.A09("show_autodownload_notification");
        r12.A09("video_viewability_logging_debug");
        r12.A09("enable_notifications_debug_mode");
        r12.A09("enable_news_feed_startup_debug_mode");
        r12.A09("disable_mark_as_seen_read_mutations");
        r12.A09("disable_notif_preprocess");
        r12.A09("fb_emoji_debug_mode");
        r12.A09("fb_emoji_highlight_multiple_parsing");
        r12.A09("fbui_view_highlighter");
        r12.A09("fbui_view_highlighter_highlight_views");
        r12.A09("fbui_view_highlighter_highlight_components");
        r12.A09("fbui_view_highlighter_highlight_type");
        r12.A09("fbui_ui_tracker");
        r12.A09("zero_header_optin_state");
        r12.A09("zero_carrier_name");
        r12.A09("zb_ping_url");
        r12.A09(ECX.$const$string(AnonymousClass1Y3.A1I));
        r12.A09("zb_request_delay_in_ms");
        r12.A09("componentscript_debug_overlay");
        r12.A09("navigation_button_overal");
        r12.A09("inbox_trial_enabled");
        A02 = (AnonymousClass1Y7) r12.A09("dark_mode");
        A03 = (AnonymousClass1Y7) r12.A09("dark_mode_toggle_enabled");
        A04 = (AnonymousClass1Y7) r12.A09("dark_pink_mode");
        r12.A09("attribution_id_overlay");
        r12.A09("redblock_background");
        r12.A09("redblock_visibility");
        r12.A09("start_chat_tooltip_seen");
        A01 = (AnonymousClass1Y7) r12.A09("color_detection_enabled");
        A00 = (AnonymousClass1Y7) r12.A09("color_detection_carousel_enabled");
    }
}
