package X;

import com.facebook.acra.ACRA;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.21B  reason: invalid class name */
public final class AnonymousClass21B {
    public static String A00(Integer num) {
        int i;
        switch (num.intValue()) {
            case 1:
                return "home_nested";
            case 2:
                return "pinned_groups";
            case 3:
                return "montage";
            case 4:
                return "people";
            case 5:
                return "me";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                i = AnonymousClass1Y3.A1G;
                break;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "games";
            case 8:
                return "active_now";
            case Process.SIGKILL:
                i = AnonymousClass1Y3.A19;
                break;
            case AnonymousClass1Y3.A01:
                return C05360Yq.$const$string(AnonymousClass1Y3.AKI);
            default:
                return "recents";
        }
        return C99084oO.$const$string(i);
    }
}
