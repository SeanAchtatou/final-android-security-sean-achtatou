package X;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1QK  reason: invalid class name */
public class AnonymousClass1QK {
    public Integer A00 = AnonymousClass07B.A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    private C23561Pz A04;
    public final SparseArray A05 = new SparseArray();
    public final AnonymousClass1PL A06;
    public final AnonymousClass1MN A07;
    public final C23531Pw A08;
    public final AnonymousClass1Q0 A09;
    public final Object A0A;
    public final String A0B;
    public final String A0C;
    public final List A0D;

    public synchronized C23561Pz A03() {
        return this.A04;
    }

    public synchronized List A04(C23561Pz r3) {
        if (r3 == this.A04) {
            return null;
        }
        this.A04 = r3;
        return new ArrayList(this.A0D);
    }

    public void A05() {
        ArrayList<C23811Qy> arrayList;
        synchronized (this) {
            if (this.A01) {
                arrayList = null;
            } else {
                this.A01 = true;
                arrayList = new ArrayList<>(this.A0D);
            }
        }
        if (arrayList != null) {
            for (C23811Qy A002 : arrayList) {
                A002.A00();
            }
        }
    }

    public void A06(C23811Qy r2) {
        boolean z;
        synchronized (this) {
            this.A0D.add(r2);
            z = this.A01;
        }
        if (z) {
            r2.A00();
        }
    }

    public synchronized boolean A07() {
        return this.A02;
    }

    public synchronized boolean A08() {
        return this.A03;
    }

    public static void A00(List list) {
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ((C23811Qy) it.next()).A01();
            }
        }
    }

    public static void A01(List list) {
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ((C23811Qy) it.next()).A02();
            }
        }
    }

    public static void A02(List list) {
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                ((C23811Qy) it.next()).A03();
            }
        }
    }

    public AnonymousClass1QK(AnonymousClass1Q0 r2, String str, String str2, AnonymousClass1MN r5, Object obj, C23531Pw r7, boolean z, boolean z2, C23561Pz r10, AnonymousClass1PL r11) {
        this.A09 = r2;
        this.A0B = str;
        this.A0C = str2;
        this.A07 = r5;
        this.A0A = obj;
        this.A08 = r7;
        this.A03 = z;
        this.A04 = r10;
        this.A02 = z2;
        this.A01 = false;
        this.A0D = new ArrayList();
        this.A06 = r11;
    }
}
