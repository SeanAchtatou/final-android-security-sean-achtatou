package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.21M  reason: invalid class name */
public final class AnonymousClass21M extends AnonymousClass11I {
    @Comparable(type = 3)
    public boolean hasBothBeenShown;
    @Comparable(type = 3)
    public boolean showErrorMessage;
    @Comparable(type = 3)
    public boolean showLeftAd;

    public void applyStateUpdate(C61322yh r7) {
        Object obj;
        C23871Rg r1;
        boolean z;
        Object[] objArr = r7.A01;
        int i = r7.A00;
        if (i != 0) {
            if (i == 1) {
                r1 = new C23871Rg();
                r1.A00(Boolean.valueOf(this.showErrorMessage));
                z = true;
            } else if (i == 2) {
                r1 = new C23871Rg();
                r1.A00(Boolean.valueOf(this.showErrorMessage));
                z = false;
            } else {
                return;
            }
            r1.A00(Boolean.valueOf(z));
            obj = r1.A00;
        } else {
            C23871Rg r4 = new C23871Rg();
            r4.A00(Boolean.valueOf(this.showLeftAd));
            C23871Rg r3 = new C23871Rg();
            r3.A00(Boolean.valueOf(this.hasBothBeenShown));
            C23871Rg r2 = new C23871Rg();
            r2.A00(Boolean.valueOf(this.showErrorMessage));
            boolean booleanValue = ((Boolean) objArr[0]).booleanValue();
            if (!booleanValue) {
                r3.A00(true);
                r2.A00(false);
            }
            r4.A00(Boolean.valueOf(booleanValue));
            this.showLeftAd = ((Boolean) r4.A00).booleanValue();
            this.hasBothBeenShown = ((Boolean) r3.A00).booleanValue();
            obj = r2.A00;
        }
        this.showErrorMessage = ((Boolean) obj).booleanValue();
    }
}
