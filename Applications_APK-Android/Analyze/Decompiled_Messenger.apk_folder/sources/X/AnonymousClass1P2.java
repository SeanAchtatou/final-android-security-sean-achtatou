package X;

import java.util.List;

/* renamed from: X.1P2  reason: invalid class name */
public final class AnonymousClass1P2 implements C23301Oz {
    private final AnonymousClass1P1 A00;

    public int Avn(int i) {
        List B1w = this.A00.B1w();
        if (B1w == null || B1w.isEmpty()) {
            return i + 1;
        }
        for (int i2 = 0; i2 < B1w.size(); i2++) {
            if (((Integer) B1w.get(i2)).intValue() > i) {
                return ((Integer) B1w.get(i2)).intValue();
            }
        }
        return Integer.MAX_VALUE;
    }

    public C33271nJ Azp(int i) {
        boolean z = false;
        if (i >= this.A00.Ao3()) {
            z = true;
        }
        return new C33271nJ(i, z, false);
    }

    public AnonymousClass1P2() {
        this(new AnonymousClass4Jp());
    }

    public AnonymousClass1P2(AnonymousClass1P1 r1) {
        C05520Zg.A02(r1);
        this.A00 = r1;
    }
}
