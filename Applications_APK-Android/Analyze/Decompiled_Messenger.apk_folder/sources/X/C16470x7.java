package X;

import com.facebook.common.perftest.PerfTestConfig;
import com.facebook.common.perftest.base.PerfTestConfigBase;
import java.util.Random;

/* renamed from: X.0x7  reason: invalid class name and case insensitive filesystem */
public final class C16470x7 {
    private final int A00;
    private final Boolean A01;
    private final Random A02 = AnonymousClass0W9.A01();

    public C16470x7(AnonymousClass1XY r2, int i) {
        PerfTestConfig.A01(r2);
        this.A01 = AnonymousClass0UU.A07(r2);
        this.A00 = i;
    }

    public boolean A00() {
        if (PerfTestConfigBase.A00()) {
            return PerfTestConfigBase.A01;
        }
        if (this.A01.booleanValue() || this.A02.nextInt(this.A00) == 0) {
            return true;
        }
        return false;
    }
}
