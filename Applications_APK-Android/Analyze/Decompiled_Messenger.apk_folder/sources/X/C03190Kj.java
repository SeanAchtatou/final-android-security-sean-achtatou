package X;

import com.facebook.common.dextricks.DexStore;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.nio.channels.FileLockInterruptionException;

/* renamed from: X.0Kj  reason: invalid class name and case insensitive filesystem */
public final class C03190Kj {
    public RandomAccessFile A00;
    private FileLock A01;
    private final AnonymousClass0DQ A02;
    private final DataOutputStream A03 = new DataOutputStream(new BufferedOutputStream(new C03180Ki(this), DexStore.LOAD_RESULT_OATMEAL_QUICKENED));
    private final File A04;

    public static C02760Gg A00() {
        C02760Gg r2 = new C02760Gg();
        r2.A0C(AnonymousClass0FV.class, new AnonymousClass0FV());
        r2.A0C(C02570Fl.class, new C02570Fl());
        r2.A0C(C02600Fo.class, new C02600Fo());
        return r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r1 == false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A01() {
        /*
            r2 = this;
            java.io.RandomAccessFile r0 = r2.A00
            if (r0 == 0) goto L_0x000f
            java.nio.channels.FileLock r0 = r2.A01
            if (r0 == 0) goto L_0x000f
            boolean r1 = r0.isValid()
            r0 = 1
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            if (r0 == 0) goto L_0x0013
            return
        L_0x0013:
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.String r0 = "Must acquire the file lock before attempting writes!"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03190Kj.A01():void");
    }

    public String A03() {
        String[] split = this.A04.getName().split("_", 2);
        if (split.length >= 2) {
            return split[1].replace("_", ":");
        }
        return this.A04.getName();
    }

    public void A04() {
        this.A04.getParentFile().mkdirs();
        int i = 0;
        while (i < 3) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(this.A04, "rw");
                this.A00 = randomAccessFile;
                this.A01 = randomAccessFile.getChannel().lock();
                return;
            } catch (FileLockInterruptionException e) {
                if (i != 2) {
                    i++;
                } else {
                    Thread.currentThread().interrupt();
                    throw new IOException("Failed to lock file after 3 attempts", e);
                }
            }
        }
    }

    public void A05() {
        FileLock fileLock = this.A01;
        if (fileLock != null) {
            try {
                fileLock.release();
            } catch (IOException unused) {
            }
        }
        RandomAccessFile randomAccessFile = this.A00;
        if (randomAccessFile != null) {
            try {
                randomAccessFile.close();
            } catch (IOException unused2) {
            }
        }
        this.A01 = null;
        this.A00 = null;
    }

    public C03190Kj(File file) {
        AnonymousClass0DQ r2 = new AnonymousClass0DQ();
        r2.A04(AnonymousClass0FV.class, new C03510Og());
        r2.A04(C02570Fl.class, new C02140Dc());
        r2.A04(C02600Fo.class, new AnonymousClass0P5());
        this.A02 = r2;
        this.A04 = file;
    }

    public C02760Gg A02() {
        A01();
        if (this.A00.length() != 0) {
            C02760Gg A002 = A00();
            if (this.A02.A02(A002, this.A00)) {
                return A002;
            }
        }
        return null;
    }

    public void A06(C02760Gg r5) {
        A01();
        this.A00.setLength(0);
        if (r5 != null) {
            AnonymousClass0DQ r3 = this.A02;
            DataOutputStream dataOutputStream = this.A03;
            dataOutputStream.writeShort(AnonymousClass1Y3.A1z);
            dataOutputStream.writeShort(2);
            dataOutputStream.writeLong(r3.A00());
            r3.A01(r5, dataOutputStream);
            this.A03.flush();
        }
    }
}
