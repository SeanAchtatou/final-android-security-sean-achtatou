package X;

/* renamed from: X.0GM  reason: invalid class name */
public final class AnonymousClass0GM {
    private static Boolean A00;

    /* JADX WARNING: Can't wrap try/catch for region: R(6:2|3|(3:5|6|7)|8|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0022 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A00(android.content.Context r3) {
        /*
            java.lang.Class<X.0GM> r2 = X.AnonymousClass0GM.class
            monitor-enter(r2)
            java.lang.Boolean r0 = X.AnonymousClass0GM.A00     // Catch:{ all -> 0x002a }
            if (r0 != 0) goto L_0x0022
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ IOException -> 0x0022 }
            X.AnonymousClass0GM.A00 = r0     // Catch:{ IOException -> 0x0022 }
            android.content.res.AssetManager r1 = r3.getAssets()     // Catch:{ IOException -> 0x0022 }
            java.lang.String r0 = "app_modules.json"
            java.io.InputStream r0 = r1.open(r0)     // Catch:{ IOException -> 0x0022 }
            r0.close()     // Catch:{ IOException -> 0x0022 }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ IOException -> 0x0022 }
            X.AnonymousClass0GM.A00 = r0     // Catch:{ IOException -> 0x0022 }
        L_0x0022:
            java.lang.Boolean r0 = X.AnonymousClass0GM.A00     // Catch:{ all -> 0x002a }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x002a }
            monitor-exit(r2)
            return r0
        L_0x002a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0GM.A00(android.content.Context):boolean");
    }

    private AnonymousClass0GM() {
    }
}
