package X;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

/* renamed from: X.1rl  reason: invalid class name and case insensitive filesystem */
public final class C35751rl extends TypefaceSpan {
    public final int A00;
    public final Typeface A01;

    public C35751rl(String str, Typeface typeface, int i) {
        super(str);
        this.A01 = typeface;
        this.A00 = i;
    }

    public void updateDrawState(TextPaint textPaint) {
        super.updateDrawState(textPaint);
        textPaint.setTypeface(this.A01);
        textPaint.setAlpha(255);
        int i = this.A00;
        if (i > 0) {
            textPaint.setTextSize((float) i);
        }
    }

    public void updateMeasureState(TextPaint textPaint) {
        super.updateMeasureState(textPaint);
        textPaint.setTypeface(this.A01);
        textPaint.setAlpha(255);
        int i = this.A00;
        if (i > 0) {
            textPaint.setTextSize((float) i);
        }
    }
}
