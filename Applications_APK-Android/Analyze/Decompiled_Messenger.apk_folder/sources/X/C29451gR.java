package X;

import androidx.lifecycle.LiveData$LifecycleBoundObserver;
import java.util.Map;

/* renamed from: X.1gR  reason: invalid class name and case insensitive filesystem */
public abstract class C29451gR {
    public static final Object A09 = new Object();
    public int A00;
    public int A01;
    private AnonymousClass1XK A02;
    private boolean A03;
    private boolean A04;
    public final Object A05;
    private final Runnable A06;
    public volatile Object A07;
    public volatile Object A08;

    public void A02() {
    }

    public void A03() {
    }

    private void A00(E4B e4b) {
        if (!e4b.A01) {
            return;
        }
        if (!e4b.A02()) {
            e4b.A01(false);
            return;
        }
        int i = e4b.A00;
        int i2 = this.A01;
        if (i < i2) {
            e4b.A00 = i2;
            e4b.A02.BRq(this.A08);
        }
    }

    public void A04(C11410n6 r4, C155357Fy r5) {
        A01("observe");
        if (r4.AsK().A05() != AnonymousClass1XL.DESTROYED) {
            LiveData$LifecycleBoundObserver liveData$LifecycleBoundObserver = new LiveData$LifecycleBoundObserver(this, r4, r5);
            E4B e4b = (E4B) this.A02.A03(r5, liveData$LifecycleBoundObserver);
            if (e4b != null && !e4b.A03(r4)) {
                throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
            } else if (e4b == null) {
                r4.AsK().A06(liveData$LifecycleBoundObserver);
            }
        }
    }

    public void A05(E4B e4b) {
        if (this.A04) {
            this.A03 = true;
            return;
        }
        this.A04 = true;
        do {
            this.A03 = false;
            if (e4b == null) {
                AnonymousClass1XK r0 = this.A02;
                C17390yp r2 = new C17390yp(r0);
                r0.A03.put(r2, false);
                while (r2.hasNext()) {
                    A00((E4B) ((Map.Entry) r2.next()).getValue());
                    if (this.A03) {
                        break;
                    }
                }
            } else {
                A00(e4b);
                e4b = null;
            }
        } while (this.A03);
        this.A04 = false;
    }

    public void A06(C155357Fy r4) {
        A01("observeForever");
        E4C e4c = new E4C(this, r4);
        E4B e4b = (E4B) this.A02.A03(r4, e4c);
        if (e4b instanceof LiveData$LifecycleBoundObserver) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        } else if (e4b == null) {
            e4c.A01(true);
        }
    }

    public void A07(C155357Fy r3) {
        A01("removeObserver");
        E4B e4b = (E4B) this.A02.A02(r3);
        if (e4b != null) {
            e4b.A00();
            e4b.A01(false);
        }
    }

    public void A08(Object obj) {
        boolean z;
        synchronized (this.A05) {
            z = false;
            if (this.A07 == A09) {
                z = true;
            }
            this.A07 = obj;
        }
        if (z) {
            C31671k4.A00().A01(this.A06);
        }
    }

    public void A09(Object obj) {
        A01("setValue");
        this.A01++;
        this.A08 = obj;
        A05(null);
    }

    private static void A01(String str) {
        if (!C31671k4.A00().A02()) {
            throw new IllegalStateException(AnonymousClass08S.A0P("Cannot invoke ", str, " on a background thread"));
        }
    }

    public C29451gR() {
        this.A05 = new Object();
        this.A02 = new AnonymousClass1XK();
        this.A00 = 0;
        this.A07 = A09;
        this.A06 = new C16210wg(this);
        this.A08 = A09;
        this.A01 = -1;
    }

    public C29451gR(Object obj) {
        this.A05 = new Object();
        this.A02 = new AnonymousClass1XK();
        this.A00 = 0;
        this.A07 = A09;
        this.A06 = new C16210wg(this);
        this.A08 = obj;
        this.A01 = 0;
    }
}
