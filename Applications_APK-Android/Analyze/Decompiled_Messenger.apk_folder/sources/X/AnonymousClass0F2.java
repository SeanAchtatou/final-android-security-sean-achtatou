package X;

import android.os.Build;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0F2  reason: invalid class name */
public final class AnonymousClass0F2 {
    private static final int A01 = Build.VERSION.SDK_INT;
    private static volatile AnonymousClass0F2 A02;
    private final C25051Yd A00;

    public static final AnonymousClass0F2 A01(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (AnonymousClass0F2.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new AnonymousClass0F2(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public double A02(long j, long j2, long j3, long j4, long j5, double d) {
        switch (A01) {
            case AnonymousClass1Y3.A05 /*21*/:
            case AnonymousClass1Y3.A06 /*22*/:
                if (j != Long.MIN_VALUE) {
                    return this.A00.Akj(j, d);
                }
                break;
            case 23:
                if (j2 != Long.MIN_VALUE) {
                    return this.A00.Akj(j2, d);
                }
                break;
            case AnonymousClass1Y3.A07 /*24*/:
            case 25:
                if (j3 != Long.MIN_VALUE) {
                    return this.A00.Akj(j3, d);
                }
                break;
            case AnonymousClass1Y3.A08 /*26*/:
            case AnonymousClass1Y3.A09 /*27*/:
                if (j4 != Long.MIN_VALUE) {
                    return this.A00.Akj(j4, d);
                }
                break;
            case 28:
                if (j5 != Long.MIN_VALUE) {
                    return this.A00.Akj(j5, d);
                }
                break;
        }
        return d;
    }

    public int A03(long j, long j2, long j3, long j4, long j5, int i) {
        switch (A01) {
            case AnonymousClass1Y3.A05 /*21*/:
            case AnonymousClass1Y3.A06 /*22*/:
                if (j != Long.MIN_VALUE) {
                    return this.A00.AqL(j, i);
                }
                break;
            case 23:
                if (j2 != Long.MIN_VALUE) {
                    return this.A00.AqL(j2, i);
                }
                break;
            case AnonymousClass1Y3.A07 /*24*/:
            case 25:
                if (j3 != Long.MIN_VALUE) {
                    return this.A00.AqL(j3, i);
                }
                break;
            case AnonymousClass1Y3.A08 /*26*/:
            case AnonymousClass1Y3.A09 /*27*/:
                if (j4 != Long.MIN_VALUE) {
                    return this.A00.AqL(j4, i);
                }
                break;
            case 28:
                if (j5 != Long.MIN_VALUE) {
                    return this.A00.AqL(j5, i);
                }
                break;
        }
        return i;
    }

    public long A04(long j, long j2, long j3, long j4, long j5, long j6) {
        switch (A01) {
            case AnonymousClass1Y3.A05 /*21*/:
            case AnonymousClass1Y3.A06 /*22*/:
                if (j != Long.MIN_VALUE) {
                    return this.A00.At1(j, j6);
                }
                break;
            case 23:
                if (j2 != Long.MIN_VALUE) {
                    return this.A00.At1(j2, j6);
                }
                break;
            case AnonymousClass1Y3.A07 /*24*/:
            case 25:
                if (j3 != Long.MIN_VALUE) {
                    return this.A00.At1(j3, j6);
                }
                break;
            case AnonymousClass1Y3.A08 /*26*/:
            case AnonymousClass1Y3.A09 /*27*/:
                if (j4 != Long.MIN_VALUE) {
                    return this.A00.At1(j4, j6);
                }
                break;
            case 28:
                if (j5 != Long.MIN_VALUE) {
                    return this.A00.At1(j5, j6);
                }
                break;
        }
        return j6;
    }

    public boolean A05(long j, long j2, long j3, long j4, long j5, boolean z) {
        switch (A01) {
            case AnonymousClass1Y3.A05 /*21*/:
            case AnonymousClass1Y3.A06 /*22*/:
                if (j != Long.MIN_VALUE) {
                    return this.A00.Aeo(j, z);
                }
                break;
            case 23:
                if (j2 != Long.MIN_VALUE) {
                    return this.A00.Aeo(j2, z);
                }
                break;
            case AnonymousClass1Y3.A07 /*24*/:
            case 25:
                if (j3 != Long.MIN_VALUE) {
                    return this.A00.Aeo(j3, z);
                }
                break;
            case AnonymousClass1Y3.A08 /*26*/:
            case AnonymousClass1Y3.A09 /*27*/:
                if (j4 != Long.MIN_VALUE) {
                    return this.A00.Aeo(j4, z);
                }
                break;
            case 28:
                if (j5 != Long.MIN_VALUE) {
                    return this.A00.Aeo(j5, z);
                }
                break;
        }
        return z;
    }

    private AnonymousClass0F2(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0WT.A00(r2);
    }

    public static final AnonymousClass0F2 A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
