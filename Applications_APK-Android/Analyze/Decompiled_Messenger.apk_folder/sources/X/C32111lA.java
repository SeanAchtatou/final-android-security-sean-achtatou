package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1lA  reason: invalid class name and case insensitive filesystem */
public final class C32111lA implements C22571Lz {
    private float A00;
    private float A01;
    private int A02;
    private int A03;
    private C17770zR A04;
    private C17730zN A05;
    private C17730zN A06;
    private C17730zN A07;
    private C17730zN A08;
    private final List A09 = new ArrayList(4);

    public void AMg(C22571Lz r2) {
        this.A09.add(r2);
    }

    public C17730zN Adz() {
        return this.A05;
    }

    public C17730zN Aev() {
        return this.A06;
    }

    public C22571Lz Ah2(int i) {
        return (C22571Lz) this.A09.get(i);
    }

    public int Ah4() {
        return this.A09.size();
    }

    public C17770zR Ahm() {
        return this.A04;
    }

    public C17730zN AiP() {
        return this.A07;
    }

    public C17730zN AnN() {
        return this.A08;
    }

    public int Ary() {
        return this.A02;
    }

    public float As1() {
        return this.A00;
    }

    public float As2() {
        return this.A01;
    }

    public int AsD() {
        return this.A03;
    }

    public void C6H(C17730zN r1) {
        this.A05 = r1;
    }

    public void C6O(C17730zN r1) {
        this.A06 = r1;
    }

    public void C70(C17770zR r1) {
        this.A04 = r1;
    }

    public void C78(C17730zN r1) {
        this.A07 = r1;
    }

    public void C84(C17730zN r1) {
        this.A08 = r1;
    }

    public void C8J(C17730zN r1) {
    }

    public void C8l(int i) {
        this.A02 = i;
    }

    public void C8m(float f) {
        this.A00 = f;
    }

    public void C8n(float f) {
        this.A01 = f;
    }

    public void C8r(int i) {
        this.A03 = i;
    }

    public void CDF(AnonymousClass117 r1) {
    }
}
