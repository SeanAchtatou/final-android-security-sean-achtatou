package X;

import com.google.common.base.Function;
import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.16A  reason: invalid class name */
public final class AnonymousClass16A extends AnonymousClass169 {
    public static final String __redex_internal_original_name = "com.google.common.util.concurrent.AbstractTransformFuture$TransformFuture";

    public AnonymousClass16A(ListenableFuture listenableFuture, Function function) {
        super(listenableFuture, function);
    }
}
