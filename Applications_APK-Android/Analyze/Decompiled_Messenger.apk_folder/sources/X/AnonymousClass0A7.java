package X;

import com.facebook.acra.config.StartupBlockingConfig;

/* renamed from: X.0A7  reason: invalid class name */
public final class AnonymousClass0A7 implements C004405h {
    public int AfE() {
        return -1;
    }

    public long B7l() {
        return 416;
    }

    public long B7m() {
        return StartupBlockingConfig.BLOCKING_UPLOAD_MAX_WAIT_MILLIS;
    }

    public boolean BFq() {
        return false;
    }

    public long B7n() {
        return C004105a.A00;
    }
}
