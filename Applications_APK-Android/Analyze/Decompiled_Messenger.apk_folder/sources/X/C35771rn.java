package X;

import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.DalvikInternals;
import com.facebook.forker.Process;
import io.card.payment.BuildConfig;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1rn  reason: invalid class name and case insensitive filesystem */
public final class C35771rn extends C35781ro {
    public static final C36241sk A06 = new C36241sk(BuildConfig.FLAVOR, (byte) 0, 0);
    public static final C36231sj A07 = new C36231sj(BuildConfig.FLAVOR);
    public static final byte[] A08;
    public C36361sw A00 = new C36361sw(15);
    public C36241sk A01 = null;
    public Boolean A02 = null;
    public short A03 = 0;
    public final byte[] A04 = new byte[10];
    private final long A05;

    public static int A01(C35771rn r5) {
        int i = 0;
        int i2 = 0;
        while (true) {
            byte A09 = r5.A09();
            i |= (A09 & Byte.MAX_VALUE) << i2;
            if ((A09 & 128) != 128) {
                return i;
            }
            i2 += 7;
        }
    }

    public static void A05(C35771rn r5, int i) {
        int i2 = 0;
        while ((i & -128) != 0) {
            r5.A04[i2] = (byte) ((i & 127) | 128);
            i >>>= 7;
            i2++;
        }
        byte[] bArr = r5.A04;
        bArr[i2] = (byte) i;
        r5.A00.A02(bArr, 0, i2 + 1);
    }

    public static void A06(C35771rn r2, C36241sk r3, byte b) {
        int i;
        if (b == -1) {
            b = A08[r3.A00];
        }
        short s = r3.A02;
        short s2 = r2.A03;
        if (s <= s2 || (i = s - s2) > 15) {
            A02(r2, b);
            r2.A0k(r3.A02);
        } else {
            A02(r2, (byte) (b | (i << 4)));
        }
        r2.A03 = r3.A02;
    }

    static {
        byte[] bArr = new byte[20];
        A08 = bArr;
        bArr[0] = 0;
        bArr[2] = 1;
        bArr[3] = 3;
        bArr[6] = 4;
        bArr[8] = 5;
        bArr[10] = 6;
        bArr[4] = 7;
        bArr[11] = 8;
        bArr[15] = 9;
        bArr[14] = 10;
        bArr[13] = 11;
        bArr[12] = 12;
        bArr[19] = DalvikInternals.IOPRIO_CLASS_SHIFT;
    }

    public static byte A00(byte b) {
        byte b2 = (byte) (b & 15);
        switch (b2) {
            case 0:
                return 0;
            case 1:
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 6;
            case 5:
                return 8;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return 10;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return 4;
            case 8:
                return 11;
            case Process.SIGKILL:
                return 15;
            case AnonymousClass1Y3.A01:
                return 14;
            case AnonymousClass1Y3.A02:
                return DalvikInternals.IOPRIO_CLASS_SHIFT;
            case AnonymousClass1Y3.A03:
                return 12;
            case 13:
                return 19;
            default:
                throw new C22334Aw1(AnonymousClass08S.A09("don't know what type: ", b2));
        }
    }

    public static void A02(C35771rn r4, byte b) {
        byte[] bArr = r4.A04;
        bArr[0] = b;
        r4.A00.A02(bArr, 0, 1);
    }

    public static void A03(C35771rn r2, byte b, int i) {
        if (i <= 14) {
            A02(r2, (byte) (A08[b] | (i << 4)));
            return;
        }
        A02(r2, (byte) (A08[b] | 240));
        A05(r2, i);
    }

    public static void A04(C35771rn r4, int i) {
        if (i >= 0) {
            long j = r4.A05;
            if (j != -1 && ((long) i) > j) {
                throw new C22334Aw1(AnonymousClass08S.A09("Length exceeded max allowed: ", i));
            }
            return;
        }
        throw new C22334Aw1(AnonymousClass08S.A09("Negative length: ", i));
    }

    public C35771rn(C35761rm r3, long j) {
        super(r3);
        this.A05 = j;
    }
}
