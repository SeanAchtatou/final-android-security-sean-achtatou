package X;

/* renamed from: X.1zU  reason: invalid class name and case insensitive filesystem */
public final class C39711zU implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.IntervalBasedEventThrottler$1";
    public final /* synthetic */ C1064257g A00;

    public C39711zU(C1064257g r1) {
        this.A00 = r1;
    }

    public void run() {
        synchronized (C1064257g.A08) {
            C1064257g r4 = this.A00;
            double d = (double) (r4.A02 / 1000);
            for (C1064357h r5 : r4.A05) {
                String str = r5.A03;
                String A0P = AnonymousClass08S.A0P(str, " ", "Event Throttled");
                int i = r5.A00;
                C010708t.A0Q(A0P, "PIGEON EVENT IS THROTTLED (DROPPED):  %s. There were %d calls to log this event in %f seconds (%.2f times a second) of which %d calls were throttled. Events logged more than %d times a second are dropped.  Avoid logging event in a tight loop, consolidate calls into a single event or update the sample rate to something reasonable for all users.", str, Integer.valueOf(i), Double.valueOf(d), Double.valueOf(((double) i) / d), Integer.valueOf(r5.A01), Integer.valueOf(r4.A01));
                r4.A06.add(r5.A03);
            }
            this.A00.A00.clear();
            this.A00.A05.clear();
        }
        C1064257g r0 = this.A00;
        AnonymousClass00S.A05(r0.A03, r0.A04, r0.A02, -293651275);
    }
}
