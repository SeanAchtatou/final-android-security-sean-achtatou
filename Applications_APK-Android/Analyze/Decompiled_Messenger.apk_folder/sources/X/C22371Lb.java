package X;

import com.google.common.base.Objects;
import java.io.File;
import java.util.Arrays;

/* renamed from: X.1Lb  reason: invalid class name and case insensitive filesystem */
public final class C22371Lb {
    public final File A00;
    public final boolean A01;
    private final String A02;
    private final String A03;

    public boolean equals(Object obj) {
        if (!(obj instanceof C22371Lb)) {
            return super.equals(obj);
        }
        C22371Lb r4 = (C22371Lb) obj;
        if (!Objects.equal(this.A03, r4.A03) || !Objects.equal(this.A02, r4.A02) || !Objects.equal(this.A00, r4.A00) || this.A01 != r4.A01) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, this.A02, this.A00, Boolean.valueOf(this.A01)});
    }

    public C22371Lb(String str, String str2, File file, boolean z) {
        this.A03 = str;
        this.A02 = str2;
        this.A01 = z;
        this.A00 = file;
    }
}
