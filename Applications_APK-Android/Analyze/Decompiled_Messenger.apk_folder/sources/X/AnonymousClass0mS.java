package X;

/* renamed from: X.0mS  reason: invalid class name */
public class AnonymousClass0mS implements AnonymousClass1Q3 {
    public final AnonymousClass1Q3 A00;
    public final AnonymousClass1PR A01;

    public AnonymousClass0mS(AnonymousClass1Q3 r1, AnonymousClass1PR r2) {
        C05520Zg.A02(r1);
        this.A00 = r1;
        this.A01 = r2;
    }

    public void ByS(C23581Qb r13, AnonymousClass1QK r14) {
        boolean z;
        String str;
        try {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A02("ThreadHandoffProducer#produceResults");
            }
            AnonymousClass1MN r9 = r14.A07;
            C23581Qb r5 = r13;
            C23621Qf r3 = new C23621Qf(this, r5, r9, r14, "BackgroundThreadHandoffProducer", r9, r14, r13);
            r14.A06(new C23721Qp(this, r3));
            AnonymousClass1PR r2 = this.A01;
            if (C27491dH.A00 == null) {
                z = false;
            } else {
                z = C25231Yv.A01();
            }
            if (z) {
                str = AnonymousClass08S.A0J("ThreadHandoffProducer_produceResults_", r14.A0B);
            } else {
                str = null;
            }
            r2.ANo(C27491dH.A02(r3, str));
        } finally {
            if (AnonymousClass1NB.A03()) {
                AnonymousClass1NB.A01();
            }
        }
    }
}
