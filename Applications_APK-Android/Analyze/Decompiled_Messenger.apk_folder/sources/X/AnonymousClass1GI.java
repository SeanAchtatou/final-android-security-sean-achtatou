package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.inbox2.morefooter.InboxMoreThreadsItem;

/* renamed from: X.1GI  reason: invalid class name */
public final class AnonymousClass1GI implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxMoreThreadsItem(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxMoreThreadsItem[i];
    }
}
