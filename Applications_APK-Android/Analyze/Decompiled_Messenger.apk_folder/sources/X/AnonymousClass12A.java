package X;

/* renamed from: X.12A  reason: invalid class name */
public abstract class AnonymousClass12A {
    public void A00() {
    }

    public void A01() {
        if (!(this instanceof AnonymousClass16V)) {
            AnonymousClass11W r3 = ((C182711a) this).A00;
            if (!r3.A02.A0E() && !r3.A02.A0N() && !r3.A02.A04()) {
                AnonymousClass0r6 r2 = r3.A04;
                C13430rQ r1 = r2.A03;
                if (r1 == C13430rQ.PRESENCE_MAP_RECEIVED || r1 == C13430rQ.TP_FULL_LIST_RECEIVED) {
                    int size = AnonymousClass0r6.A02(r2, -1, -1).size();
                    if (!r3.A02.A04()) {
                        AnonymousClass1XX.A03(AnonymousClass1Y3.Aay, r3.A00);
                        r3.A03(new C15400vE(size, AnonymousClass10O.ACTIVE_NOW));
                        return;
                    }
                    return;
                }
                r3.A03(C15400vE.A02);
                return;
            }
            return;
        }
        AnonymousClass16V r32 = (AnonymousClass16V) this;
        ((C20921Ei) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BMw, r32.A00.A01)).A06(5505198, "presence_changed");
        AnonymousClass16S r33 = r32.A00;
        if (((AnonymousClass0r6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.Auh, r33.A01)).A03 == C13430rQ.MQTT_DISCONNECTED) {
            r33.A02.ARp();
            r33.A0A = false;
            return;
        }
        AnonymousClass16S.A03(r33);
    }
}
