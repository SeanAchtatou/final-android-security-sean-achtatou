package X;

/* renamed from: X.05x  reason: invalid class name */
public final class AnonymousClass05x {
    static {
        C001000r.A00(new AnonymousClass05y());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: boolean
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static void A00() {
        /*
            r4 = 64
            boolean r0 = X.C001000r.A02(r4)
            if (r0 == 0) goto L_0x0024
            int r3 = android.os.Process.myTid()
            java.lang.Thread r2 = java.lang.Thread.currentThread()
            java.lang.String r1 = r2.getName()
            java.lang.String r0 = "thread_name"
            X.AnonymousClass08Z.A04(r4, r0, r1, r3)
            r0 = 0
            if (r0 == 0) goto L_0x0024
            r1 = 0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            r1.put(r2, r0)
        L_0x0024:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass05x.A00():void");
    }
}
