package X;

import android.view.View;
import android.view.ViewGroup;
import com.facebook.graphql.enums.GraphQLProfileSellingContextActions;
import com.facebook.graphql.enums.GraphQLProfileSellingInvoiceActions;
import com.facebook.graphql.enums.GraphQLProfileSellingTrustActions;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.litho.LithoView;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.14k  reason: invalid class name and case insensitive filesystem */
public final class C186114k extends C32501lr {
    public C13060qW A00;
    public AnonymousClass0YJ A01;
    public AnonymousClass0YJ A02;
    public GSTModelShape1S0000000 A03;
    public GSTModelShape1S0000000 A04;
    public AnonymousClass0UN A05;
    public LithoView A06;
    public AnonymousClass2FB A07;
    public ThreadSummary A08;
    public User A09;
    public boolean A0A;
    public boolean A0B;
    private C140426gp A0C;
    private C133816Nv A0D;
    private boolean A0E;
    public final AnonymousClass3KV A0F = new AnonymousClass3KV();
    public final AnonymousClass3KU A0G = new AnonymousClass3KU();
    public final C66533Kp A0H;
    public final AnonymousClass3KG A0I;
    public final C66403Kb A0J;
    public final AnonymousClass3KX A0K;
    public final AnonymousClass3KQ A0L;
    public final AnonymousClass3KS A0M;
    public final C66503Km A0N;
    public final C66523Ko A0O;
    public final C66513Kn A0P;
    public final AnonymousClass3KT A0Q;
    public final C66493Kl A0R;
    public final C66553Kr A0S;
    public final AnonymousClass3KW A0T;
    public final C66413Kc A0U;
    public final AnonymousClass3KN A0V;
    public final AnonymousClass3KZ A0W;
    public final C25051Yd A0X;
    public final AnonymousClass3KI A0Y;
    public final String A0Z;
    private final AnonymousClass0Ud A0a;
    private final AnonymousClass3D4 A0b;
    private final AnonymousClass3KM A0c;
    private final AnonymousClass3Ka A0d;
    private final AnonymousClass3KH A0e;
    private final C66213Jh A0f;

    private synchronized C140426gp A03() {
        if (this.A0C == null) {
            this.A0C = new C140426gp(this.A0e, this);
        }
        return this.A0C;
    }

    public static void A0A(C186114k r4) {
        r4.A03 = null;
        r4.A04 = null;
        if (r4.A0B) {
            r4.A0B = false;
            C140426gp A032 = r4.A03();
            C140426gp.A00(A032);
            ((AnonymousClass7NO) A032.A01.get()).A01();
        }
        AnonymousClass3KI r2 = r4.A0Y;
        r2.A03.A02.A04();
        AnonymousClass3KJ r1 = r2.A02;
        A18 a18 = r2.A00;
        if (a18 != null) {
            r1.A02.A04(Collections.singleton(a18));
        }
        r2.A00 = null;
        AnonymousClass0YJ r22 = r4.A01;
        if (r22 != null && r4.A02 != null) {
            ((AnonymousClass5Z6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADR, r4.A05)).A02("rate_buyer_action", r22);
            ((AnonymousClass5Z6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ADR, r4.A05)).A02("rate_seller_action", r4.A02);
        }
    }

    public C186114k(AnonymousClass1XY r6, UserKey userKey) {
        super("MarketplaceBannerNotification");
        this.A05 = new AnonymousClass0UN(8, r6);
        this.A0I = new AnonymousClass3KG(AnonymousClass0WT.A00(r6));
        this.A0e = new AnonymousClass3KH(r6);
        this.A0Y = new AnonymousClass3KI(r6);
        this.A0a = AnonymousClass0Ud.A00(r6);
        this.A0c = new AnonymousClass3KM(r6);
        this.A0V = new AnonymousClass3KN(r6);
        this.A0L = new AnonymousClass3KQ(r6);
        this.A0M = new AnonymousClass3KS(r6);
        this.A0Q = new AnonymousClass3KT(r6);
        this.A0T = new AnonymousClass3KW(r6);
        this.A0K = new AnonymousClass3KX(r6);
        this.A0W = new AnonymousClass3KZ(r6);
        this.A0d = new AnonymousClass3Ka(r6);
        this.A0J = new C66403Kb(r6);
        this.A0U = new C66413Kc(r6);
        this.A0R = new C66493Kl(r6);
        this.A0N = new C66503Km(r6);
        this.A0b = AnonymousClass3D4.A00(r6);
        this.A0P = new C66513Kn(r6);
        this.A0O = new C66523Ko(r6);
        this.A0H = new C66533Kp(r6);
        this.A0S = new C66553Kr(r6);
        this.A0X = AnonymousClass0WT.A00(r6);
        this.A0f = C66213Jh.A01();
        String str = (userKey == null || userKey.type != C25651aB.A03 || (str = userKey.id) == null) ? null : str;
        this.A0Z = str;
        this.A0V.A02 = str;
        this.A0L.A04 = str;
        this.A0M.A04 = str;
        AnonymousClass3KU r2 = this.A0G;
        C66563Ks r1 = new C66563Ks(this.A0d, "OUT_OF_STOCK");
        AnonymousClass3KN r0 = this.A0V;
        r2.A00 = r1;
        r2.A01 = r0;
        AnonymousClass3KV r3 = this.A0F;
        AnonymousClass3KQ r22 = this.A0L;
        C66563Ks r02 = new C66563Ks(this.A0d, "OUT_OF_STOCK");
        r3.A01 = r22;
        r3.A00 = r02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01f2, code lost:
        if (r1.getBooleanValue(-560137149) == false) goto L_0x01f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0226, code lost:
        if (r1.getBooleanValue(-560137149) == false) goto L_0x0228;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0260, code lost:
        if (r1 == null) goto L_0x0262;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x02b3, code lost:
        if (r14.A0A == false) goto L_0x02b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0346, code lost:
        if (r0.A2S() == null) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0393, code lost:
        if (r11 == false) goto L_0x0395;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0092, code lost:
        if (r0.A2S() == null) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x03a1, code lost:
        if (r0.A05 == false) goto L_0x03a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ba, code lost:
        if (r7 == false) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c2, code lost:
        if (r7 == false) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f5, code lost:
        if (r5.getBooleanValue(39050024) == false) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f8, code lost:
        if (r0 != false) goto L_0x00fa;
     */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x0424  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x0431  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x0492  */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x04a6  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x04ba  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C17770zR A00(X.C186114k r19, X.AnonymousClass0p4 r20) {
        /*
            r14 = r19
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            boolean r0 = X.C113275aH.A04(r0)
            if (r0 == 0) goto L_0x0076
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            com.google.common.collect.ImmutableList r0 = A05(r0)
            java.util.List r5 = r14.A08(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            com.google.common.collect.ImmutableList r0 = A04(r0)
            java.util.List r4 = r14.A07(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            com.google.common.collect.ImmutableList r0 = A06(r0)
            java.util.List r2 = r14.A09(r0)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            boolean r0 = r5.isEmpty()
            r1 = 0
            if (r0 != 0) goto L_0x0068
            java.lang.Object r0 = r5.get(r1)
            r3.add(r0)
        L_0x003b:
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x0048
            java.lang.Object r0 = r4.get(r1)
            r3.add(r0)
        L_0x0048:
            int r0 = r3.size()
            X.3KP[] r2 = new X.AnonymousClass3KP[r0]
            r3.toArray(r2)
        L_0x0051:
            X.3KM r7 = r14.A0c
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            int r3 = r2.length
            r1 = 0
        L_0x005a:
            if (r1 >= r3) goto L_0x0255
            r0 = r2[r1]
            X.6pQ r0 = r0.Asv()
            r6.add(r0)
            int r1 = r1 + 1
            goto L_0x005a
        L_0x0068:
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x003b
            java.lang.Object r0 = r2.get(r1)
            r3.add(r0)
            goto L_0x003b
        L_0x0076:
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            com.facebook.messaging.model.threads.ThreadSummary r1 = r14.A08
            java.lang.String r0 = r14.A0Z
            boolean r16 = X.AnonymousClass6YF.A01(r1, r0)
            boolean r9 = X.AnonymousClass6YF.A00(r1, r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            r8 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0094
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A2S()
            r3 = 1
            if (r0 != 0) goto L_0x0095
        L_0x0094:
            r3 = 0
        L_0x0095:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r14.A08
            r1 = 1
            if (r0 == 0) goto L_0x0252
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r0.A0W
            if (r0 == 0) goto L_0x0252
            boolean r0 = r0.A08
            if (r0 != r8) goto L_0x0252
        L_0x00a2:
            r5 = 0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            if (r0 == 0) goto L_0x00ab
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r5 = r0.A1a()
        L_0x00ab:
            if (r3 != 0) goto L_0x01b1
            X.3KG r0 = r14.A0I
            boolean r0 = r0.A03(r5)
            boolean r7 = A0C(r5)
            if (r16 == 0) goto L_0x00bc
            r6 = 1
            if (r7 != 0) goto L_0x00bd
        L_0x00bc:
            r6 = 0
        L_0x00bd:
            if (r9 == 0) goto L_0x00c4
            if (r1 != 0) goto L_0x00c4
            r4 = 1
            if (r7 != 0) goto L_0x00c5
        L_0x00c4:
            r4 = 0
        L_0x00c5:
            if (r0 == 0) goto L_0x00d5
            r3 = 5
            int r1 = X.AnonymousClass1Y3.BRh
            X.0UN r0 = r14.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            X.3VT r0 = (X.AnonymousClass3VT) r0
            r15.add(r0)
        L_0x00d5:
            if (r6 == 0) goto L_0x0237
            X.3KQ r0 = r14.A0L
            r15.add(r0)
        L_0x00dc:
            X.3KG r0 = r14.A0I
            X.1Yd r3 = r0.A00
            r0 = 284752037286592(0x102fb000812c0, double:1.40686199206611E-309)
            boolean r4 = r3.Aem(r0)
            if (r9 == 0) goto L_0x0234
            if (r5 == 0) goto L_0x00f7
            r0 = 39050024(0x253db28, float:1.5564727E-37)
            boolean r1 = r5.getBooleanValue(r0)
            r0 = 1
            if (r1 != 0) goto L_0x00f8
        L_0x00f7:
            r0 = 0
        L_0x00f8:
            if (r0 == 0) goto L_0x0234
        L_0x00fa:
            if (r9 == 0) goto L_0x01e4
            X.3KG r3 = r14.A0I
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            if (r1 == 0) goto L_0x01e1
            r0 = 2082228937(0x7c1c4ac9, float:3.2460605E36)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x01e1
            X.1Yd r3 = r3.A00
            r0 = 284752037614275(0x102fb000d12c3, double:1.406861993685077E-309)
            boolean r0 = r3.Aem(r0)
        L_0x0116:
            if (r0 == 0) goto L_0x01e4
            X.3Kb r0 = r14.A0J
            r15.add(r0)
        L_0x011d:
            if (r9 == 0) goto L_0x015b
            X.3KG r3 = r14.A0I
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            if (r0 == 0) goto L_0x01de
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A1J()
            if (r0 == 0) goto L_0x01de
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r0.A1b()
            if (r1 == 0) goto L_0x01de
            r0 = -1824631729(0xffffffff933e544f, float:-2.4022939E-27)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x01de
            X.1Yd r3 = r3.A00
            r0 = 284752037679812(0x102fb000e12c4, double:1.406861994008873E-309)
            boolean r0 = r3.Aem(r0)
        L_0x0145:
            if (r0 == 0) goto L_0x015b
            X.3KG r0 = r14.A0I
            X.1Yd r3 = r0.A00
            r0 = 284752036893370(0x102fb000212ba, double:1.406861990123333E-309)
            boolean r0 = r3.Aem(r0)
            if (r0 == 0) goto L_0x01d7
            X.3KZ r0 = r14.A0W
            r15.add(r0)
        L_0x015b:
            if (r16 == 0) goto L_0x0176
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            if (r1 == 0) goto L_0x0176
            r3 = 0
            r0 = -2111595429(0xffffffff82239c5b, float:-1.202022E-37)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x01c9
            java.lang.String r3 = "IN_STOCK"
        L_0x016d:
            if (r3 == 0) goto L_0x0176
            X.3KO r0 = r14.A01(r3)
            r15.add(r2, r0)
        L_0x0176:
            com.facebook.messaging.model.threads.ThreadSummary r3 = r14.A08
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A04
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            r17 = r3
            r18 = r1
            r19 = r0
            r14.A0B(r15, r16, r17, r18, r19)
            if (r16 == 0) goto L_0x019b
            if (r7 == 0) goto L_0x019b
            X.1Yd r3 = r14.A0X
            r0 = 284747742253751(0x102fa000712b7, double:1.406840771784363E-309)
            boolean r0 = r3.Aem(r0)
            if (r0 == 0) goto L_0x019b
            X.3KT r0 = r14.A0Q
            r15.add(r2, r0)
        L_0x019b:
            if (r9 == 0) goto L_0x01ac
            X.3KG r1 = r14.A0I
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            boolean r0 = r1.A02(r0)
            if (r0 == 0) goto L_0x01ac
            X.3Kp r0 = r14.A0H
            r15.add(r0)
        L_0x01ac:
            X.3Kr r0 = r14.A0S
            r15.add(r0)
        L_0x01b1:
            r1 = 2
            int r0 = r15.size()
            int r0 = java.lang.Math.min(r1, r0)
            java.util.List r1 = r15.subList(r2, r0)
            int r0 = r1.size()
            X.3KP[] r2 = new X.AnonymousClass3KP[r0]
            r1.toArray(r2)
            goto L_0x0051
        L_0x01c9:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            r0 = 997081158(0x3b6e4046, float:0.0036354228)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x016d
            java.lang.String r3 = "OUT_OF_STOCK"
            goto L_0x016d
        L_0x01d7:
            X.3KX r0 = r14.A0K
            r15.add(r0)
            goto L_0x015b
        L_0x01de:
            r0 = 0
            goto L_0x0145
        L_0x01e1:
            r0 = 0
            goto L_0x0116
        L_0x01e4:
            if (r16 == 0) goto L_0x020d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            if (r1 == 0) goto L_0x01f4
            r0 = -560137149(0xffffffffde9cfc43, float:-5.655995E18)
            boolean r1 = r1.getBooleanValue(r0)
            r0 = 1
            if (r1 != 0) goto L_0x01f5
        L_0x01f4:
            r0 = 0
        L_0x01f5:
            if (r0 == 0) goto L_0x020d
            X.3KG r0 = r14.A0I
            X.1Yd r3 = r0.A00
            r0 = 284752037155518(0x102fb000612be, double:1.406861991418516E-309)
            boolean r0 = r3.Aem(r0)
            if (r0 == 0) goto L_0x020d
            X.3Kl r0 = r14.A0R
            r15.add(r0)
            goto L_0x011d
        L_0x020d:
            if (r4 != 0) goto L_0x0211
            if (r8 == 0) goto L_0x011d
        L_0x0211:
            if (r4 == 0) goto L_0x021a
            X.3KW r0 = r14.A0T
            r15.add(r0)
            goto L_0x011d
        L_0x021a:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            if (r1 == 0) goto L_0x0228
            r0 = -560137149(0xffffffffde9cfc43, float:-5.655995E18)
            boolean r1 = r1.getBooleanValue(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0229
        L_0x0228:
            r0 = 0
        L_0x0229:
            if (r0 == 0) goto L_0x011d
            if (r8 == 0) goto L_0x011d
            X.3Kc r0 = r14.A0U
            r15.add(r0)
            goto L_0x011d
        L_0x0234:
            r8 = 0
            goto L_0x00fa
        L_0x0237:
            if (r4 == 0) goto L_0x00dc
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            if (r1 == 0) goto L_0x024b
            r0 = 1493797969(0x59098c51, float:2.41977196E15)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x024b
            X.3KS r1 = r14.A0M
            r0 = 1
            r1.A02 = r0
        L_0x024b:
            X.3KS r0 = r14.A0M
            r15.add(r0)
            goto L_0x00dc
        L_0x0252:
            r1 = 0
            goto L_0x00a2
        L_0x0255:
            com.facebook.messaging.model.threads.ThreadSummary r8 = r14.A08
            if (r8 == 0) goto L_0x0262
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r8.A0W
            if (r0 == 0) goto L_0x0262
            java.lang.String r1 = r0.A04
            r0 = 1
            if (r1 != 0) goto L_0x0263
        L_0x0262:
            r0 = 0
        L_0x0263:
            r5 = 0
            if (r0 == 0) goto L_0x026a
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r8.A0W
            java.lang.String r5 = r0.A04
        L_0x026a:
            r1 = 0
            if (r8 == 0) goto L_0x0281
            com.facebook.messaging.model.threads.MarketplaceThreadData r4 = r8.A0W
            if (r4 == 0) goto L_0x0281
            java.lang.String r3 = r14.A0Z
            if (r3 == 0) goto L_0x0281
            boolean r0 = X.AnonymousClass6YF.A01(r8, r3)
            if (r0 == 0) goto L_0x0531
            com.facebook.messaging.model.threads.MarketplaceThreadUserData r0 = r4.A00
            if (r0 == 0) goto L_0x0531
        L_0x027f:
            java.lang.String r1 = r0.A08
        L_0x0281:
            r7.A04(r6, r5, r1)
            r6 = 3
            java.lang.String r3 = "actions"
            r0 = 185(0xb9, float:2.59E-43)
            java.lang.String r1 = X.C99084oO.$const$string(r0)
            java.lang.String r0 = "viewerId"
            java.lang.String[] r5 = new java.lang.String[]{r3, r1, r0}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r6)
            X.5Ox r3 = new X.5Ox
            r6 = r20
            android.content.Context r0 = r6.A09
            r3.<init>(r0)
            X.0zR r0 = r6.A04
            if (r0 == 0) goto L_0x02a9
            java.lang.String r0 = r0.A06
            r3.A07 = r0
        L_0x02a9:
            r4.clear()
            boolean r0 = r14.A0E
            if (r0 == 0) goto L_0x02b5
            boolean r1 = r14.A0A
            r0 = 1
            if (r1 != 0) goto L_0x02b6
        L_0x02b5:
            r0 = 0
        L_0x02b6:
            r3.A07 = r0
            r7 = 1
            r4.set(r7)
            r3.A08 = r2
            r6 = 0
            r4.set(r6)
            java.lang.String r0 = r14.A0Z
            r3.A06 = r0
            r0 = 2
            r4.set(r0)
            X.3Kr r0 = r14.A0S
            r3.A03 = r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            boolean r0 = X.C113275aH.A04(r0)
            if (r0 == 0) goto L_0x032a
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            com.google.common.collect.ImmutableList r0 = A05(r0)
            java.util.List r6 = r14.A08(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            com.google.common.collect.ImmutableList r0 = A04(r0)
            java.util.List r2 = r14.A07(r0)
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            com.google.common.collect.ImmutableList r0 = A06(r0)
            java.util.List r0 = r14.A09(r0)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r1.addAll(r6)
            r1.addAll(r2)
            r1.addAll(r0)
            int r0 = r1.size()
            X.3KP[] r0 = new X.AnonymousClass3KP[r0]
            r1.toArray(r0)
        L_0x030b:
            r3.A09 = r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            r3.A00 = r0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            r3.A01 = r0
            X.6Nv r0 = r14.A0D
            r3.A05 = r0
            X.3Jh r1 = r14.A0f
            X.2FB r0 = r14.A07
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r1.A03(r0)
            if (r0 == 0) goto L_0x0325
            r3.A04 = r0
        L_0x0325:
            r0 = 3
            X.AnonymousClass11F.A0C(r0, r4, r5)
            return r3
        L_0x032a:
            X.3KG r0 = r14.A0I
            X.1Yd r2 = r0.A00
            r0 = 284752036827833(0x102fb000112b9, double:1.406861989799537E-309)
            boolean r0 = r2.Aem(r0)
            r13 = 0
            if (r0 != 0) goto L_0x033d
            X.3KP[] r0 = new X.AnonymousClass3KP[r6]
            goto L_0x030b
        L_0x033d:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            if (r0 == 0) goto L_0x0348
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A2S()
            r8 = 1
            if (r0 != 0) goto L_0x0349
        L_0x0348:
            r8 = 0
        L_0x0349:
            com.facebook.messaging.model.threads.ThreadSummary r1 = r14.A08
            java.lang.String r0 = r14.A0Z
            boolean r16 = X.AnonymousClass6YF.A01(r1, r0)
            boolean r12 = X.AnonymousClass6YF.A00(r1, r0)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r6 = 0
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A04
            if (r0 == 0) goto L_0x0363
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r6 = r0.A1a()
        L_0x0363:
            if (r8 == 0) goto L_0x0387
            if (r12 == 0) goto L_0x0387
            com.facebook.messaging.model.threads.ThreadSummary r8 = r14.A08
            if (r8 == 0) goto L_0x0387
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r8.A0W
            if (r0 == 0) goto L_0x0387
            X.3Ko r7 = r14.A0O
            com.facebook.user.model.User r6 = r14.A09
            X.0qW r1 = r14.A00
            X.4FS r0 = new X.4FS
            r0.<init>(r7, r8, r6, r1)
            r2.add(r0)
        L_0x037d:
            int r0 = r2.size()
            X.3KP[] r0 = new X.AnonymousClass3KP[r0]
            r2.toArray(r0)
            goto L_0x030b
        L_0x0387:
            X.3Kr r0 = r14.A0S
            r2.add(r0)
            boolean r11 = A0C(r6)
            if (r16 == 0) goto L_0x0395
            r8 = 1
            if (r11 != 0) goto L_0x0396
        L_0x0395:
            r8 = 0
        L_0x0396:
            com.facebook.messaging.model.threads.ThreadSummary r1 = r14.A08
            if (r1 == 0) goto L_0x03a3
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r1.A0W
            if (r0 == 0) goto L_0x03a3
            boolean r0 = r0.A05
            r10 = 1
            if (r0 != 0) goto L_0x03a4
        L_0x03a3:
            r10 = 0
        L_0x03a4:
            if (r8 == 0) goto L_0x04c0
            X.3KQ r0 = r14.A0L
            r2.add(r0)
            com.facebook.messaging.model.threads.ThreadSummary r8 = r14.A08
            if (r8 == 0) goto L_0x03bd
            if (r10 == 0) goto L_0x03bd
            X.3Km r7 = r14.A0N
            X.0qW r1 = r14.A00
            X.5aC r0 = new X.5aC
            r0.<init>(r7, r8, r1)
            r2.add(r0)
        L_0x03bd:
            X.3KG r0 = r14.A0I
            X.1Yd r7 = r0.A00
            r0 = 284752037286592(0x102fb000812c0, double:1.40686199206611E-309)
            boolean r0 = r7.Aem(r0)
            if (r0 == 0) goto L_0x03d1
            X.3KW r0 = r14.A0T
            r2.add(r0)
        L_0x03d1:
            X.3KG r0 = r14.A0I
            X.1Yd r7 = r0.A00
            r0 = 286749196557455(0x104cc00001c8f, double:1.416729269916176E-309)
            boolean r0 = r7.Aem(r0)
            if (r0 == 0) goto L_0x03f6
            X.3KG r0 = r14.A0I
            boolean r0 = r0.A03(r6)
            if (r0 != 0) goto L_0x03f6
            r7 = 5
            int r1 = X.AnonymousClass1Y3.BRh
            X.0UN r0 = r14.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.3VT r0 = (X.AnonymousClass3VT) r0
            r2.add(r0)
        L_0x03f6:
            X.3KG r7 = r14.A0I
            if (r6 == 0) goto L_0x04bd
            java.lang.Class<com.facebook.graphservice.modelutil.GSTModelShape1S0000000> r8 = com.facebook.graphservice.modelutil.GSTModelShape1S0000000.class
            r1 = -692475982(0xffffffffd6b9a7b2, float:-1.02064949E14)
            r0 = -1454998187(0xffffffffa9467d55, float:-4.407354E-14)
            com.google.common.collect.ImmutableList r0 = r6.A0M(r1, r8, r0)
            if (r0 == 0) goto L_0x04bd
            X.1Xv r6 = r0.iterator()
        L_0x040c:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x04bd
            java.lang.Object r1 = r6.next()
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = (com.facebook.graphservice.modelutil.GSTModelShape1S0000000) r1
            r0 = -958911557(0xffffffffc6d82bbb, float:-27669.865)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x040c
            r0 = 1
        L_0x0422:
            if (r0 == 0) goto L_0x04ba
            X.1Yd r6 = r7.A00
            r0 = 286749196622992(0x104cc00011c90, double:1.41672927023997E-309)
            boolean r0 = r6.Aem(r0)
        L_0x042f:
            if (r0 == 0) goto L_0x043f
            r6 = 7
            int r1 = X.AnonymousClass1Y3.BIL
            X.0UN r0 = r14.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.3VU r0 = (X.AnonymousClass3VU) r0
            r2.add(r0)
        L_0x043f:
            if (r12 == 0) goto L_0x0450
            X.3KG r1 = r14.A0I
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            boolean r0 = r1.A02(r0)
            if (r0 == 0) goto L_0x0450
            X.3Kp r0 = r14.A0H
            r2.add(r0)
        L_0x0450:
            com.facebook.messaging.model.threads.ThreadSummary r6 = r14.A08
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A04
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            r15 = r2
            r17 = r6
            r18 = r1
            r19 = r0
            r14.A0B(r15, r16, r17, r18, r19)
            if (r16 == 0) goto L_0x04af
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            if (r1 == 0) goto L_0x04af
            r0 = 731566377(0x2b9ad129, float:1.1000412E-12)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x0487
            X.3KG r0 = r14.A0I
            X.1Yd r6 = r0.A00
            r0 = 286882340543761(0x104eb00001d11, double:1.417387088612017E-309)
            boolean r0 = r6.Aem(r0)
            if (r0 == 0) goto L_0x0487
            java.lang.String r0 = "PENDING"
            X.3KO r0 = r14.A01(r0)
            r2.add(r0)
        L_0x0487:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            r0 = -2111595429(0xffffffff82239c5b, float:-1.202022E-37)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x049b
            java.lang.String r0 = "IN_STOCK"
            X.3KO r0 = r14.A01(r0)
            r2.add(r0)
        L_0x049b:
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            r0 = 997081158(0x3b6e4046, float:0.0036354228)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x04af
            java.lang.String r0 = "OUT_OF_STOCK"
            X.3KO r0 = r14.A01(r0)
            r2.add(r0)
        L_0x04af:
            if (r16 == 0) goto L_0x037d
            if (r11 == 0) goto L_0x037d
            X.3KT r0 = r14.A0Q
            r2.add(r0)
            goto L_0x037d
        L_0x04ba:
            r0 = 0
            goto L_0x042f
        L_0x04bd:
            r0 = 0
            goto L_0x0422
        L_0x04c0:
            if (r12 == 0) goto L_0x03bd
            r9 = 1
            if (r1 == 0) goto L_0x052f
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r1.A0W
            if (r0 == 0) goto L_0x052f
            boolean r0 = r0.A08
            if (r0 != r7) goto L_0x052f
        L_0x04cd:
            X.3KG r8 = r14.A0I
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r14.A03
            if (r0 == 0) goto L_0x052d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r0 = r0.A1J()
            if (r0 == 0) goto L_0x052d
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r0.A1b()
            if (r1 == 0) goto L_0x052d
            r0 = -1824631729(0xffffffff933e544f, float:-2.4022939E-27)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x052d
            X.1Yd r8 = r8.A00
            r0 = 284752037745349(0x102fb000f12c5, double:1.40686199433267E-309)
            boolean r0 = r8.Aem(r0)
        L_0x04f3:
            if (r0 == 0) goto L_0x04fa
            X.3KX r0 = r14.A0K
            r2.add(r0)
        L_0x04fa:
            if (r12 == 0) goto L_0x0501
            if (r9 != 0) goto L_0x0501
            if (r11 == 0) goto L_0x0501
            r13 = 1
        L_0x0501:
            if (r13 == 0) goto L_0x0519
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r14.A03
            if (r1 == 0) goto L_0x0514
            r0 = 1493797969(0x59098c51, float:2.41977196E15)
            boolean r0 = r1.getBooleanValue(r0)
            if (r0 == 0) goto L_0x0514
            X.3KS r0 = r14.A0M
            r0.A02 = r7
        L_0x0514:
            X.3KS r0 = r14.A0M
            r2.add(r0)
        L_0x0519:
            com.facebook.messaging.model.threads.ThreadSummary r8 = r14.A08
            if (r8 == 0) goto L_0x03bd
            if (r10 == 0) goto L_0x03bd
            X.3Kn r7 = r14.A0P
            X.0qW r1 = r14.A00
            X.5aB r0 = new X.5aB
            r0.<init>(r7, r8, r1)
            r2.add(r0)
            goto L_0x03bd
        L_0x052d:
            r0 = 0
            goto L_0x04f3
        L_0x052f:
            r9 = 0
            goto L_0x04cd
        L_0x0531:
            boolean r0 = X.AnonymousClass6YF.A00(r8, r3)
            if (r0 == 0) goto L_0x0281
            com.facebook.messaging.model.threads.MarketplaceThreadUserData r0 = r4.A01
            if (r0 == 0) goto L_0x0281
            goto L_0x027f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C186114k.A00(X.14k, X.0p4):X.0zR");
    }

    private AnonymousClass3KO A01(String str) {
        boolean equalsIgnoreCase = str.equalsIgnoreCase("OUT_OF_STOCK");
        if (equalsIgnoreCase && this.A0X.Aem(286744901590158L) && this.A03.getBooleanValue(-1600906823)) {
            return this.A0G;
        }
        if (equalsIgnoreCase && this.A0X.Aem(285572375516915L)) {
            return this.A0F;
        }
        C66563Ks r1 = new C66563Ks(this.A0d, str);
        r1.A00 = C113275aH.A03(this.A03);
        return r1;
    }

    public static final C186114k A02(AnonymousClass1XY r2) {
        return new C186114k(r2, AnonymousClass0XJ.A09(r2));
    }

    public static ImmutableList A04(GSTModelShape1S0000000 gSTModelShape1S0000000) {
        GSTModelShape1S0000000 A1a;
        GSTModelShape1S0000000 gSTModelShape1S00000002;
        if (gSTModelShape1S0000000 == null || (A1a = gSTModelShape1S0000000.A1a()) == null || (gSTModelShape1S00000002 = (GSTModelShape1S0000000) A1a.A0J(-2130493681, GSTModelShape1S0000000.class, -168492989)) == null) {
            return ImmutableList.builder().build();
        }
        return gSTModelShape1S00000002.A0N(1590298221, GraphQLProfileSellingContextActions.UNSET_OR_UNRECOGNIZED_ENUM_VALUE);
    }

    public static ImmutableList A05(GSTModelShape1S0000000 gSTModelShape1S0000000) {
        GSTModelShape1S0000000 A1a;
        GSTModelShape1S0000000 gSTModelShape1S00000002;
        if (gSTModelShape1S0000000 == null || (A1a = gSTModelShape1S0000000.A1a()) == null || (gSTModelShape1S00000002 = (GSTModelShape1S0000000) A1a.A0J(-2130493681, GSTModelShape1S0000000.class, -168492989)) == null) {
            return ImmutableList.builder().build();
        }
        return gSTModelShape1S00000002.A0N(2134554091, GraphQLProfileSellingInvoiceActions.UNSET_OR_UNRECOGNIZED_ENUM_VALUE);
    }

    public static ImmutableList A06(GSTModelShape1S0000000 gSTModelShape1S0000000) {
        GSTModelShape1S0000000 A1a;
        GSTModelShape1S0000000 gSTModelShape1S00000002;
        if (gSTModelShape1S0000000 == null || (A1a = gSTModelShape1S0000000.A1a()) == null || (gSTModelShape1S00000002 = (GSTModelShape1S0000000) A1a.A0J(-2130493681, GSTModelShape1S0000000.class, -168492989)) == null) {
            return ImmutableList.builder().build();
        }
        return gSTModelShape1S00000002.A0N(-1068476906, GraphQLProfileSellingTrustActions.UNSET_OR_UNRECOGNIZED_ENUM_VALUE);
    }

    private List A07(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            switch (((GraphQLProfileSellingContextActions) it.next()).ordinal()) {
                case 1:
                    arrayList.add((AnonymousClass3VV) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ari, this.A05));
                    break;
                case 2:
                    arrayList.add(this.A0S);
                    break;
            }
        }
        return arrayList;
    }

    private List A08(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            int i = C145316pX.A00[((GraphQLProfileSellingInvoiceActions) it.next()).ordinal()];
            if (i == 1) {
                arrayList.add((AnonymousClass3VS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A14, this.A05));
            } else if (i == 2) {
                arrayList.add((AnonymousClass3VQ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgX, this.A05));
            } else if (i == 3) {
                arrayList.add((AnonymousClass3VR) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AVf, this.A05));
            }
        }
        return arrayList;
    }

    private List A09(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            switch (((GraphQLProfileSellingTrustActions) it.next()).ordinal()) {
                case 1:
                    arrayList.add(this.A0L);
                    break;
                case 2:
                    arrayList.add(this.A0M);
                    break;
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        if (r1.getBooleanValue(-2107392789) == false) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r1 == false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0B(java.util.List r5, boolean r6, com.facebook.messaging.model.threads.ThreadSummary r7, com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r8, com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r9) {
        /*
            r4 = this;
            if (r6 == 0) goto L_0x0047
            if (r7 == 0) goto L_0x000d
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r7.A0W
            if (r0 == 0) goto L_0x000d
            boolean r1 = r0.A07
            r0 = 1
            if (r1 != 0) goto L_0x000e
        L_0x000d:
            r0 = 0
        L_0x000e:
            if (r0 == 0) goto L_0x0047
            if (r8 == 0) goto L_0x0022
            com.facebook.graphservice.modelutil.GSTModelShape1S0000000 r1 = r8.A1a()
            if (r1 == 0) goto L_0x0022
            r0 = -2107392789(0xffffffff8263bceb, float:-1.6731543E-37)
            boolean r1 = r1.getBooleanValue(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0023
        L_0x0022:
            r0 = 0
        L_0x0023:
            if (r0 != 0) goto L_0x0047
            boolean r0 = X.C113275aH.A03(r9)
            if (r0 == 0) goto L_0x0047
            X.1Yd r2 = r4.A0X
            r0 = 284747741794992(0x102fa000012b0, double:1.40684076951779E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x0047
            r3 = 0
            r2 = 1
            int r1 = X.AnonymousClass1Y3.A14
            X.0UN r0 = r4.A05
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.3VS r0 = (X.AnonymousClass3VS) r0
            r5.add(r3, r0)
        L_0x0047:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C186114k.A0B(java.util.List, boolean, com.facebook.messaging.model.threads.ThreadSummary, com.facebook.graphservice.modelutil.GSTModelShape1S0000000, com.facebook.graphservice.modelutil.GSTModelShape1S0000000):void");
    }

    private static boolean A0C(GSTModelShape1S0000000 gSTModelShape1S0000000) {
        GSTModelShape1S0000000 gSTModelShape1S00000002;
        if (gSTModelShape1S0000000 == null || (gSTModelShape1S00000002 = (GSTModelShape1S0000000) gSTModelShape1S0000000.A0J(-316248209, GSTModelShape1S0000000.class, 66669804)) == null || !gSTModelShape1S00000002.getBooleanValue(375518257)) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r1 == null) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D() {
        /*
            r8 = this;
            com.facebook.messaging.model.threads.ThreadSummary r0 = r8.A08
            if (r0 == 0) goto L_0x000d
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r0.A0W
            if (r0 == 0) goto L_0x000d
            java.lang.String r1 = r0.A04
            r0 = 1
            if (r1 != 0) goto L_0x000e
        L_0x000d:
            r0 = 0
        L_0x000e:
            if (r0 == 0) goto L_0x0166
            X.0Ud r0 = r8.A0a
            boolean r0 = r0.A0G()
            if (r0 == 0) goto L_0x001c
            boolean r0 = r8.A0A
            if (r0 == 0) goto L_0x0166
        L_0x001c:
            X.0xM r0 = r8.A00
            r0.A06(r8)
            r0 = 1
            r8.A0B = r0
            X.6gp r5 = r8.A03()
            com.facebook.messaging.model.threads.ThreadSummary r6 = r8.A08
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r6.A0W
            java.lang.String r2 = r0.A04
            X.C140426gp.A00(r5)
            com.facebook.graphql.calls.GQLCallInputCInputShape2S0000000 r1 = new com.facebook.graphql.calls.GQLCallInputCInputShape2S0000000
            r0 = 2
            r1.<init>(r0)
            java.lang.String r0 = "for_sale_item_id"
            r1.A09(r0, r2)
            X.3wL r2 = new X.3wL
            r2.<init>()
            java.lang.String r0 = "input"
            r2.A04(r0, r1)
            X.0US r0 = r5.A02     // Catch:{ 3KF -> 0x0057 }
            java.lang.Object r1 = r0.get()     // Catch:{ 3KF -> 0x0057 }
            com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl r1 = (com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl) r1     // Catch:{ 3KF -> 0x0057 }
            X.0Wg r0 = com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl.A03     // Catch:{ 3KF -> 0x0057 }
            X.A18 r0 = r1.A03(r2, r0)     // Catch:{ 3KF -> 0x0057 }
            r5.A00 = r0     // Catch:{ 3KF -> 0x0057 }
            goto L_0x005a
        L_0x0057:
            r0 = 0
            r5.A00 = r0
        L_0x005a:
            X.0US r0 = r5.A01
            java.lang.Object r0 = r0.get()
            X.7NO r0 = (X.AnonymousClass7NO) r0
            r0.A01()
            com.facebook.graphql.query.GQSQStringShape3S0000000_I3 r2 = new com.facebook.graphql.query.GQSQStringShape3S0000000_I3
            r0 = 130(0x82, float:1.82E-43)
            r2.<init>(r0)
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r6.A0W
            java.lang.String r1 = r0.A04
            java.lang.String r0 = "messageable_item_id"
            r2.A09(r0, r1)
            X.1cN r7 = X.C26931cN.A00(r2)
            com.facebook.graphql.query.GQSQStringShape3S0000000_I3 r2 = new com.facebook.graphql.query.GQSQStringShape3S0000000_I3
            r0 = 129(0x81, float:1.81E-43)
            r2.<init>(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r6.A0S
            long r0 = r0.A0G()
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            r0 = 12
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            r2.A07(r0, r1)
            X.1cN r4 = X.C26931cN.A00(r2)
            X.0US r0 = r5.A01
            java.lang.Object r3 = r0.get()
            X.7NO r3 = (X.AnonymousClass7NO) r3
            java.lang.String r1 = "item:"
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = r6.A0W
            java.lang.String r0 = r0.A04
            java.lang.String r2 = X.AnonymousClass08S.A0J(r1, r0)
            X.0Wg r1 = r5.A04
            java.util.concurrent.ExecutorService r0 = r5.A06
            r3.A02(r2, r7, r1, r0)
            X.0US r0 = r5.A01
            java.lang.Object r3 = r0.get()
            X.7NO r3 = (X.AnonymousClass7NO) r3
            java.lang.String r2 = "thread:"
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r6.A0S
            long r0 = r0.A0G()
            java.lang.String r2 = X.AnonymousClass08S.A0G(r2, r0)
            X.0Wg r1 = r5.A05
            java.util.concurrent.ExecutorService r0 = r5.A06
            r3.A02(r2, r4, r1, r0)
            com.facebook.messaging.model.threads.ThreadSummary r3 = r8.A08
            if (r3 == 0) goto L_0x0136
            X.3KI r6 = r8.A0Y
            java.lang.String r1 = "C2C_THREAD_BANNER"
            X.5QE r2 = new X.5QE
            r2.<init>()
            r2.A00 = r1
            java.lang.String r0 = "displayType"
            X.C28931fb.A06(r1, r0)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0S
            long r0 = r0.A0G()
            java.lang.String r1 = java.lang.Long.toString(r0)
            r2.A01 = r1
            java.lang.String r0 = "threadId"
            X.C28931fb.A06(r1, r0)
            X.5QD r7 = new X.5QD
            r7.<init>(r2)
            r6.A01 = r8
            X.2nL r3 = r6.A03
            X.4It r2 = new X.4It
            r2.<init>(r6, r7)
            X.0Wg r1 = r6.A04
            java.lang.String r0 = "fetch_campaign_view_key"
            r3.A03(r0, r2, r1)
            X.3KJ r5 = r6.A02
            X.0Wg r4 = r6.A04
            X.43K r3 = new X.43K
            r3.<init>()
            com.facebook.graphql.calls.GQLCallInputCInputShape2S0000000 r2 = new com.facebook.graphql.calls.GQLCallInputCInputShape2S0000000
            r0 = 15
            r2.<init>(r0)
            com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000 r1 = X.AnonymousClass3KJ.A00(r5, r7)
            java.lang.String r0 = "display_params"
            r2.A05(r0, r1)
            java.lang.String r0 = "input"
            r3.A04(r0, r2)
            com.facebook.graphql.subscriptions.core.GraphQLSubscriptionConnectorImpl r1 = r5.A02     // Catch:{ 3KF -> 0x012f }
            X.4DP r0 = new X.4DP     // Catch:{ 3KF -> 0x012f }
            r0.<init>(r4)     // Catch:{ 3KF -> 0x012f }
            X.A18 r0 = r1.A03(r3, r0)     // Catch:{ 3KF -> 0x012f }
            goto L_0x0134
        L_0x012f:
            r0 = move-exception
            r4.BYh(r0)
            r0 = 0
        L_0x0134:
            r6.A00 = r0
        L_0x0136:
            X.6gm r3 = new X.6gm
            r3.<init>(r8)
            r8.A01 = r3
            X.6gr r0 = new X.6gr
            r0.<init>(r8)
            r8.A02 = r0
            int r1 = X.AnonymousClass1Y3.ADR
            X.0UN r0 = r8.A05
            r2 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.5Z6 r1 = (X.AnonymousClass5Z6) r1
            java.lang.String r0 = "rate_buyer_action"
            r1.A01(r0, r3)
            int r1 = X.AnonymousClass1Y3.ADR
            X.0UN r0 = r8.A05
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.5Z6 r2 = (X.AnonymousClass5Z6) r2
            X.0YJ r1 = r8.A02
            java.lang.String r0 = "rate_seller_action"
            r2.A01(r0, r1)
            return
        L_0x0166:
            X.0xM r0 = r8.A00
            r0.A05(r8)
            A0A(r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C186114k.A0D():void");
    }

    public void A0E(C133816Nv r4) {
        this.A0D = r4;
        LithoView lithoView = this.A06;
        if (lithoView != null) {
            this.A06.A0a(A00(this, new AnonymousClass0p4(lithoView.getContext())));
        }
    }

    public void A0F(boolean z) {
        this.A0E = z;
        LithoView lithoView = this.A06;
        if (lithoView != null && this.A0A) {
            lithoView.A0a(A00(this, lithoView.A0H));
        }
    }

    public View B90(ViewGroup viewGroup) {
        AnonymousClass0p4 r3 = new AnonymousClass0p4(viewGroup.getContext());
        ((AnonymousClass3VQ) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AgX, this.A05)).A03 = C188215g.A00().toString();
        LithoView A002 = LithoView.A00(viewGroup.getContext(), A00(this, r3), false);
        this.A06 = A002;
        return A002;
    }

    public void onResume() {
        this.A0E = this.A0b.A03;
        A0D();
    }
}
