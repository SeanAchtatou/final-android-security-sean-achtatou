package X;

import java.io.Serializable;

/* renamed from: X.0Aq  reason: invalid class name and case insensitive filesystem */
public abstract class C01540Aq implements Serializable {
    private static final long serialVersionUID = 0;

    public abstract Object A01();

    public abstract boolean A02();

    public abstract boolean equals(Object obj);

    public abstract int hashCode();

    public abstract String toString();

    public static C01540Aq A00(Object obj) {
        AnonymousClass0A1.A00(obj);
        return new C01550Ar(obj);
    }
}
