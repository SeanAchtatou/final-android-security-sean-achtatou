package X;

import com.facebook.common.stringformat.StringFormatUtil;
import com.facebook.payments.auth.pin.model.CheckPaymentPasswordParams;
import com.facebook.payments.auth.pin.model.PaymentPin;
import com.facebook.tigon.iface.TigonRequest;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

/* renamed from: X.1y5  reason: invalid class name and case insensitive filesystem */
public final class C38841y5 implements C10810kt {
    public static final String __redex_internal_original_name = "com.facebook.payments.auth.pin.protocol.method.CheckPaymentPasswordMethod";

    public static final C38841y5 A00() {
        return new C38841y5();
    }

    public C47702Xm B0t(Object obj) {
        CheckPaymentPasswordParams checkPaymentPasswordParams = (CheckPaymentPasswordParams) obj;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("password", checkPaymentPasswordParams.A01));
        arrayList.add(new BasicNameValuePair("format", "json"));
        AnonymousClass2U7 A00 = C47702Xm.A00();
        A00.A0B = "check_payment_pins_with_password";
        A00.A0C = TigonRequest.GET;
        A00.A0D = StringFormatUtil.formatStrLocaleSafe("/%d", Long.valueOf(checkPaymentPasswordParams.A00));
        A00.A0H = arrayList;
        A00.A05 = AnonymousClass07B.A0C;
        return A00.A01();
    }

    public Object B1B(Object obj, AnonymousClass2U8 r4) {
        r4.A04();
        return Boolean.valueOf(((PaymentPin) r4.A00().readValueAs(PaymentPin.class)).A00().isPresent());
    }
}
