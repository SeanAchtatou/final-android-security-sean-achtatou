package X;

import java.util.LinkedList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Ml  reason: invalid class name and case insensitive filesystem */
public final class C22691Ml implements C22701Mm {
    public static final Runtime A06 = Runtime.getRuntime();
    private static volatile C22691Ml A07;
    public C23311Pa A00;
    public C23311Pa A01;
    public List A02;
    private long A03;
    public final C22721Mo A04;
    private final AnonymousClass06B A05 = AnonymousClass067.A02();

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00c3, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00c8, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A01() {
        /*
            r7 = this;
            monitor-enter(r7)
            X.06B r0 = r7.A05     // Catch:{ all -> 0x00dc }
            long r3 = r0.now()     // Catch:{ all -> 0x00dc }
            long r0 = r7.A03     // Catch:{ all -> 0x00dc }
            long r3 = r3 - r0
            r1 = 300000(0x493e0, double:1.482197E-318)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00da
            X.06B r0 = r7.A05     // Catch:{ all -> 0x00dc }
            long r0 = r0.now()     // Catch:{ all -> 0x00dc }
            r7.A03 = r0     // Catch:{ all -> 0x00dc }
            r5 = r7
            monitor-enter(r5)     // Catch:{ all -> 0x00dc }
            X.1Pa r0 = r7.A00     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x0061
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "bitmap_memory_cache_entries"
            int r0 = r0.getCount()     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "bitmap_memory_cache_size"
            X.1Pa r0 = r7.A00     // Catch:{ all -> 0x00c3 }
            int r0 = r0.getSizeInBytes()     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
            X.1Pa r4 = r7.A00     // Catch:{ all -> 0x00c3 }
            boolean r0 = r4 instanceof X.AnonymousClass1PZ     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x0061
            X.1PZ r4 = (X.AnonymousClass1PZ) r4     // Catch:{ all -> 0x00c3 }
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "bitmap_memory_cache_lru_entries"
            monitor-enter(r4)     // Catch:{ all -> 0x00c3 }
            X.1Pb r0 = r4.A03     // Catch:{ all -> 0x00aa }
            int r0 = r0.A00()     // Catch:{ all -> 0x00aa }
            monitor-exit(r4)     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "bitmap_memory_cache_lru_size"
            monitor-enter(r4)     // Catch:{ all -> 0x00c3 }
            X.1Pb r0 = r4.A03     // Catch:{ all -> 0x00aa }
            int r0 = r0.A01()     // Catch:{ all -> 0x00aa }
            monitor-exit(r4)     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
        L_0x0061:
            monitor-exit(r5)     // Catch:{ all -> 0x00dc }
            monitor-enter(r5)     // Catch:{ all -> 0x00dc }
            X.1Pa r0 = r7.A01     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x00ad
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "memory_cache_entries"
            int r0 = r0.getCount()     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "memory_cache_size"
            X.1Pa r0 = r7.A01     // Catch:{ all -> 0x00c3 }
            int r0 = r0.getSizeInBytes()     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
            X.1Pa r4 = r7.A01     // Catch:{ all -> 0x00c3 }
            boolean r0 = r4 instanceof X.AnonymousClass1PZ     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x00ad
            X.1PZ r4 = (X.AnonymousClass1PZ) r4     // Catch:{ all -> 0x00c3 }
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "memory_cache_lru_entries"
            monitor-enter(r4)     // Catch:{ all -> 0x00c3 }
            X.1Pb r0 = r4.A03     // Catch:{ all -> 0x00aa }
            int r0 = r0.A00()     // Catch:{ all -> 0x00aa }
            monitor-exit(r4)     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
            X.1Mo r3 = r7.A04     // Catch:{ all -> 0x00c3 }
            java.lang.String r2 = "memory_cache_lru_size"
            monitor-enter(r4)     // Catch:{ all -> 0x00c3 }
            X.1Pb r0 = r4.A03     // Catch:{ all -> 0x00aa }
            int r0 = r0.A01()     // Catch:{ all -> 0x00aa }
            monitor-exit(r4)     // Catch:{ all -> 0x00c3 }
            long r0 = (long) r0     // Catch:{ all -> 0x00c3 }
            r3.A05(r2, r0)     // Catch:{ all -> 0x00c3 }
            goto L_0x00ad
        L_0x00aa:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00c3 }
            throw r0     // Catch:{ all -> 0x00c3 }
        L_0x00ad:
            monitor-exit(r5)     // Catch:{ all -> 0x00dc }
            r6 = r7
            monitor-enter(r6)     // Catch:{ all -> 0x00dc }
            X.1Mo r5 = r7.A04     // Catch:{ all -> 0x00c6 }
            java.lang.String r4 = "memory_usage"
            java.lang.Runtime r0 = X.C22691Ml.A06     // Catch:{ all -> 0x00c6 }
            long r2 = r0.totalMemory()     // Catch:{ all -> 0x00c6 }
            long r0 = r0.freeMemory()     // Catch:{ all -> 0x00c6 }
            long r2 = r2 - r0
            r5.A05(r4, r2)     // Catch:{ all -> 0x00c6 }
            goto L_0x00c9
        L_0x00c3:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00dc }
            goto L_0x00c8
        L_0x00c6:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00dc }
        L_0x00c8:
            throw r0     // Catch:{ all -> 0x00dc }
        L_0x00c9:
            monitor-exit(r6)     // Catch:{ all -> 0x00dc }
            java.util.List r1 = r7.A02     // Catch:{ all -> 0x00dc }
            X.1Mo r0 = r7.A04     // Catch:{ all -> 0x00dc }
            com.fasterxml.jackson.databind.JsonNode r0 = r0.A02()     // Catch:{ all -> 0x00dc }
            r1.add(r0)     // Catch:{ all -> 0x00dc }
            X.1Mo r0 = r7.A04     // Catch:{ all -> 0x00dc }
            r0.A03()     // Catch:{ all -> 0x00dc }
        L_0x00da:
            monitor-exit(r7)     // Catch:{ all -> 0x00dc }
            return
        L_0x00dc:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00dc }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22691Ml.A01():void");
    }

    public void BWU(C23601Qd r1) {
    }

    public synchronized void C0P(C23311Pa r2) {
        this.A00 = r2;
    }

    public synchronized void C0W(C23311Pa r2) {
        this.A01 = r2;
    }

    public static final C22691Ml A00(AnonymousClass1XY r5) {
        if (A07 == null) {
            synchronized (C22691Ml.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r5);
                if (A002 != null) {
                    try {
                        A07 = new C22691Ml(new C22711Mn(r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    private C22691Ml(C22711Mn r4) {
        this.A04 = new C22721Mo(C22741Mq.A01(r4), "image_cache_stats_counter");
        this.A02 = new LinkedList();
    }

    public void BQ0(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("bitmap_cache_hit", 1);
        }
    }

    public void BQ1(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("bitmap_cache_miss", 1);
        }
    }

    public void BQ2(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("bitmap_cache_put", 1);
        }
    }

    public void BWR(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("disk_cache_get_fail", 1);
        }
    }

    public void BWS(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("disk_cache_hit", 1);
        }
    }

    public void BWT(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("disk_cache_miss", 1);
        }
    }

    public void Bel(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("memory_cache_hit", 1);
        }
    }

    public void Bem(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("memory_cache_miss", 1);
        }
    }

    public void Ben(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("memory_cache_put", 1);
        }
    }

    public void BpN(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("staging_area_hit", 1);
        }
    }

    public void BpO(C23601Qd r5) {
        A01();
        synchronized (this) {
            this.A04.A04("staging_area_miss", 1);
        }
    }
}
