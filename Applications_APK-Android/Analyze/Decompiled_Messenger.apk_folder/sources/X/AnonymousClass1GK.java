package X;

import com.google.common.base.Function;

/* renamed from: X.1GK  reason: invalid class name */
public final class AnonymousClass1GK implements Function {
    public final /* synthetic */ C09510hU A00;
    public final /* synthetic */ C25611a7 A01;

    public AnonymousClass1GK(C09510hU r1, C25611a7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public Object apply(Object obj) {
        AnonymousClass100 r4 = (AnonymousClass100) obj;
        if (r4 == null) {
            return AnonymousClass1EO.A00(null);
        }
        if (this.A00 == C09510hU.STALE_DATA_OKAY) {
            AnonymousClass101 r2 = r4.A01;
            if (r2.A01 == AnonymousClass102.FROM_CACHE_STALE) {
                return AnonymousClass1EO.A01(r2);
            }
        }
        if (this.A01 == C25611a7.TOP) {
            return AnonymousClass1EO.A01(r4.A01);
        }
        return AnonymousClass1EO.A00(r4.A01);
    }
}
