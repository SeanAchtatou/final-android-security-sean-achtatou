package X;

import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.Logger;
import com.facebook.systrace.TraceDirect;

/* renamed from: X.00C  reason: invalid class name */
public final class AnonymousClass00C {
    public static void A00(long j, int i) {
        int i2 = AnonymousClass00n.A08;
        Logger.writeStandardEntry(i2, 6, 23, 0, 0, i, 0, 0);
        if (!TraceEvents.isEnabled(i2) && AnonymousClass08Z.A05(j)) {
            if (TraceDirect.checkNative()) {
                TraceDirect.nativeEndSection();
            } else {
                AnonymousClass0HL.A00("E");
            }
        }
    }

    public static void A01(long j, String str, int i) {
        if (!TraceEvents.isEnabled(AnonymousClass00n.A08)) {
            AnonymousClass08Z.A00(j, str);
            return;
        }
        int i2 = AnonymousClass00n.A08;
        Logger.writeBytesEntry(i2, 1, 83, Logger.writeStandardEntry(i2, 7, 22, 0, 0, i, 0, 0), str);
    }
}
