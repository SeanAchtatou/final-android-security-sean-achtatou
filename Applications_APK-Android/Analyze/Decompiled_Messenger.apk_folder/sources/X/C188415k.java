package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.business.mdotme.model.PlatformRefParams;

/* renamed from: X.15k  reason: invalid class name and case insensitive filesystem */
public final class C188415k implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new PlatformRefParams(parcel);
    }

    public Object[] newArray(int i) {
        return new PlatformRefParams[i];
    }
}
