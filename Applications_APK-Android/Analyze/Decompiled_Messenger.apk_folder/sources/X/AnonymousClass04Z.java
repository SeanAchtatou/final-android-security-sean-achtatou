package X;

import android.app.Application;

/* renamed from: X.04Z  reason: invalid class name */
public interface AnonymousClass04Z {
    void registerActivityLifecycleCallbacks(AnonymousClass002 r1, Application.ActivityLifecycleCallbacks activityLifecycleCallbacks);

    void unregisterActivityLifecycleCallbacks(AnonymousClass002 r1, Application.ActivityLifecycleCallbacks activityLifecycleCallbacks);
}
