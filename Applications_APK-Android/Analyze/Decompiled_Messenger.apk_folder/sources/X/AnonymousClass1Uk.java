package X;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.contacts.graphql.Contact;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.CollectionName;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.user.model.User;
import com.facebook.user.model.UserKey;
import com.facebook.user.profilepic.PicSquare;
import com.google.common.base.Objects;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.SettableFuture;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Uk  reason: invalid class name */
public final class AnonymousClass1Uk implements OmnistoreComponent {
    private static final Class A0B = AnonymousClass1Uk.class;
    private static volatile AnonymousClass1Uk A0C;
    public CollectionName A00;
    private C22141Kb A01;
    private AnonymousClass0UN A02;
    private final AnonymousClass1Ul A03;
    private final C24541Un A04;
    private final AnonymousClass1Um A05;
    private final C04310Tq A06;
    private final C04310Tq A07;
    private final C04310Tq A08;
    private final C04310Tq A09;
    private final C04310Tq A0A;

    public String getCollectionLabel() {
        return "messenger_contacts_android_v2";
    }

    public synchronized void onCollectionInvalidated() {
        synchronized (this.A05) {
        }
        AnonymousClass25N r2 = (AnonymousClass25N) this.A07.get();
        synchronized (r2) {
            if (!r2.A01.isDone()) {
                r2.A01.cancel(false);
            }
            r2.A01 = SettableFuture.create();
            r2.A00 = null;
        }
    }

    public static final AnonymousClass1Uk A00(AnonymousClass1XY r4) {
        if (A0C == null) {
            synchronized (AnonymousClass1Uk.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0C, r4);
                if (A002 != null) {
                    try {
                        A0C = new AnonymousClass1Uk(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0C;
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        IndexedFields indexedFields;
        int i;
        C005505z.A05("ContactsOmnistoreComponent#indexObject", str, 824900421);
        try {
            String str3 = (String) this.A09.get();
            if (str3 == null) {
                C010708t.A05(A0B, "Trying to index contacts without a logged in users");
                indexedFields = new IndexedFields();
                i = -1955652553;
            } else {
                AnonymousClass7WG r3 = (AnonymousClass7WG) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BLO, this.A02);
                C005505z.A03("ContactCollectionIndexer#getAllIndexedFields", 1076993600);
                try {
                    indexedFields = new IndexedFields();
                    C005505z.A03("ContactCollectionIndexer#contactFromBlob", -644720043);
                    try {
                        Contact A042 = C75703kJ.A04(byteBuffer);
                        C005505z.A00(1850039060);
                        C005505z.A03("ContactCollectionIndexer#getAllIndexedFields:writeAllIndexesForContact", -1623750553);
                        r3.A00.A03(A042, new C158427Ux(indexedFields));
                        C005505z.A00(1421622191);
                        C005505z.A03("ContactCollectionIndexer#addOmnistoreSpecificIndexes", -624065920);
                        try {
                            indexedFields.addFieldValue("profile_type", Integer.toString(A042.mContactProfileType.A00()));
                            indexedFields.addFieldValue("link_type", Integer.toString(C157427Ps.A00(A042, str3).A01()));
                            indexedFields.addFieldValue("fbid", A042.mProfileFbid);
                            indexedFields.addFieldValue("pushable_tristate", Integer.toString(A042.mIsMobilePushable.getDbValue()));
                            String str4 = "1";
                            String str5 = "0";
                            if (A042.mIsMessengerUser) {
                                str5 = str4;
                            }
                            indexedFields.addFieldValue("messenger_user", str5);
                            String str6 = "0";
                            if (A042.mIsOnViewerContactList) {
                                str6 = str4;
                            }
                            indexedFields.addFieldValue("in_contact_list", str6);
                            String str7 = "0";
                            if (A042.mCommunicationRank == 0.0f) {
                                str7 = str4;
                            }
                            indexedFields.addFieldValue("zero_communication_rank", str7);
                            String str8 = "0";
                            if (A042.mIsMemorialized) {
                                str8 = str4;
                            }
                            indexedFields.addFieldValue("is_memorialized", str8);
                            indexedFields.addFieldValue("viewer_connection_status", A042.mViewerConnectionStatus.name());
                            String str9 = "0";
                            if (A042.mIsPartial) {
                                str9 = str4;
                            }
                            indexedFields.addFieldValue("is_partial", str9);
                            indexedFields.addFieldValue("add_source", Byte.toString(C75703kJ.A00(A042.mAddSource)));
                            indexedFields.addFieldValue("current_education_school_name", A042.mCurrentEducationSchoolName);
                            String str10 = "0";
                            if (A042.mContactProfileType == AnonymousClass2B4.A06) {
                                str10 = str4;
                            }
                            indexedFields.addFieldValue("is_parent_approved_user", str10);
                            if (!A042.mIsFavoriteMessengerContact) {
                                str4 = "0";
                            }
                            indexedFields.addFieldValue("is_favorite_messenger_contact", str4);
                            if (!C013509w.A02(A042.mCurrentWorkEmployerNames)) {
                                StringBuilder sb = new StringBuilder();
                                C24971Xv it = A042.mCurrentWorkEmployerNames.iterator();
                                while (it.hasNext()) {
                                    sb.append((String) it.next());
                                    sb.append("#");
                                }
                                indexedFields.addFieldValue("current_employer_name", sb.toString());
                            }
                            C005505z.A00(-1230629694);
                            C005505z.A00(-1111792902);
                            i = 2013404115;
                        } catch (Throwable th) {
                            th = th;
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        C005505z.A00(1598665874);
                        throw th;
                    }
                } catch (Throwable th3) {
                    C005505z.A00(-458532582);
                    throw th3;
                }
            }
            C005505z.A00(i);
            return indexedFields;
        } finally {
            C005505z.A00(2128014171);
        }
    }

    public void Boz(int i) {
        synchronized (this.A05) {
        }
    }

    public void onCollectionAvailable(Collection collection) {
        boolean z;
        int i;
        AnonymousClass25N r1 = (AnonymousClass25N) this.A07.get();
        synchronized (r1) {
            r1.A01.set(collection);
        }
        AnonymousClass1Ul r4 = this.A03;
        if (((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, r4.A00)).Aep(AnonymousClass1Ul.A02, false)) {
            C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(1, AnonymousClass1Y3.B6q, r4.A00)).edit();
            edit.putBoolean(AnonymousClass1Ul.A02, false);
            edit.commit();
            z = true;
        } else {
            z = false;
        }
        if (z) {
            ((AnonymousClass2C5) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AbX, this.A02)).A01();
        } else {
            AnonymousClass2C5 r2 = (AnonymousClass2C5) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AbX, this.A02);
            String A032 = ((C190116l) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B07, r2.A00)).A03(AnonymousClass2C6.A05);
            if (A032 == null) {
                i = -1;
            } else {
                try {
                    i = Integer.parseInt(A032);
                } catch (NumberFormatException unused) {
                    i = -1;
                }
            }
            if (i != 9) {
                r2.A01();
            }
        }
        AnonymousClass1Um r0 = this.A05;
        collection.getSnapshotState();
        synchronized (r0) {
        }
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
        Preconditions.checkNotNull(this.A01, "mDeltas should have been init in onDeltaClusterStarted()");
        ImmutableList copyOf = ImmutableList.copyOf(this.A01.values());
        this.A01 = null;
        A01(copyOf);
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
        this.A01 = new C22141Kb();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException} */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0207, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0208, code lost:
        if (r2 != null) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0238, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0239, code lost:
        if (r2 != null) goto L_0x023b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x020d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:52:0x023e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C32171lK provideSubscriptionInfo(com.facebook.omnistore.Omnistore r23) {
        /*
            r22 = this;
            r2 = r22
            X.1Ul r0 = r2.A03
            boolean r0 = r0.A01
            if (r0 != 0) goto L_0x000b
            X.1lK r0 = X.C32171lK.A03
            return r0
        L_0x000b:
            java.lang.String r0 = r2.getCollectionLabel()
            r1 = r23
            com.facebook.omnistore.CollectionName$Builder r1 = r1.createCollectionNameBuilder(r0)
            X.0Tq r0 = r2.A0A
            java.lang.Object r0 = r0.get()
            java.lang.String r0 = (java.lang.String) r0
            r1.addSegment(r0)
            r1.addDeviceId()
            com.facebook.omnistore.CollectionName r0 = r1.build()
            r2.A00 = r0
            X.0Tq r0 = r2.A07
            java.lang.Object r1 = r0.get()
            X.25N r1 = (X.AnonymousClass25N) r1
            com.facebook.omnistore.CollectionName r0 = r2.A00
            monitor-enter(r1)
            r1.A00 = r0     // Catch:{ all -> 0x0251 }
            monitor-exit(r1)
            com.facebook.omnistore.CollectionName r14 = r2.A00
            X.0Tq r0 = r2.A08
            java.lang.Object r7 = r0.get()
            X.2Bz r7 = (X.C42822Bz) r7
            java.lang.String r6 = "ContactsOmnistoreParamsBuilder"
            X.1lI r5 = new X.1lI
            r5.<init>()
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01d9 }
            r4.<init>()     // Catch:{ JSONException -> 0x01d9 }
            X.0kz r1 = r7.A04     // Catch:{ JSONException -> 0x01d9 }
            r0 = 50
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ JSONException -> 0x01d9 }
            int r0 = r1.A01(r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r18 = java.lang.Integer.toString(r0)     // Catch:{ JSONException -> 0x01d9 }
            X.0kz r1 = r7.A04     // Catch:{ JSONException -> 0x01d9 }
            r0 = 80
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ JSONException -> 0x01d9 }
            int r0 = r1.A01(r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r17 = java.lang.Integer.toString(r0)     // Catch:{ JSONException -> 0x01d9 }
            X.0kz r1 = r7.A04     // Catch:{ JSONException -> 0x01d9 }
            r0 = 320(0x140, float:4.48E-43)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ JSONException -> 0x01d9 }
            int r0 = r1.A01(r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r16 = java.lang.Integer.toString(r0)     // Catch:{ JSONException -> 0x01d9 }
            X.1cI r0 = r7.A05     // Catch:{ JSONException -> 0x01d9 }
            int r1 = r0.A09()     // Catch:{ JSONException -> 0x01d9 }
            X.1cI r0 = r7.A05     // Catch:{ JSONException -> 0x01d9 }
            int r0 = r0.A07()     // Catch:{ JSONException -> 0x01d9 }
            int r0 = java.lang.Math.min(r1, r0)     // Catch:{ JSONException -> 0x01d9 }
            int r0 = r0 / 2
            java.lang.String r3 = java.lang.Integer.toString(r0)     // Catch:{ JSONException -> 0x01d9 }
            X.2C0 r0 = r7.A06     // Catch:{ JSONException -> 0x01d9 }
            int r2 = X.AnonymousClass1Y3.AcD     // Catch:{ JSONException -> 0x01d9 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x01d9 }
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ JSONException -> 0x01d9 }
            X.1YI r2 = (X.AnonymousClass1YI) r2     // Catch:{ JSONException -> 0x01d9 }
            r1 = 442(0x1ba, float:6.2E-43)
            r0 = 0
            boolean r0 = r2.AbO(r1, r0)     // Catch:{ JSONException -> 0x01d9 }
            if (r0 == 0) goto L_0x011f
            X.2C3 r2 = X.AnonymousClass2C3.A02     // Catch:{ JSONException -> 0x01d9 }
        L_0x00ab:
            java.lang.String r8 = "render_object_query_id"
            r0 = 2486643598131274(0x8d596d459804a, double:1.228565175287729E-308)
            r4.put(r8, r0)     // Catch:{ JSONException -> 0x01d9 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01d9 }
            r1.<init>()     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r8 = "profile_id"
            java.lang.String r0 = "<ID>"
            r1.put(r8, r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = 737(0x2e1, float:1.033E-42)
            java.lang.String r12 = X.C99084oO.$const$string(r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = r18
            r1.put(r12, r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = 417(0x1a1, float:5.84E-43)
            java.lang.String r11 = X.C99084oO.$const$string(r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = r17
            r1.put(r11, r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = 515(0x203, float:7.22E-43)
            java.lang.String r10 = X.C99084oO.$const$string(r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = r16
            r1.put(r10, r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r9 = "low_res_cover_size"
            r1.put(r9, r3)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r8 = "media_type"
            r1.put(r8, r2)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r0 = "render_object_query_params"
            r4.put(r0, r1)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r13 = "render_object_list_query_id"
            r0 = 2671591162896704(0x97dcc4a3fe140, double:1.319941413320327E-308)
            r4.put(r13, r0)     // Catch:{ JSONException -> 0x01d9 }
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01d9 }
            r13.<init>()     // Catch:{ JSONException -> 0x01d9 }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ JSONException -> 0x01d9 }
            r1.<init>()     // Catch:{ JSONException -> 0x01d9 }
            X.2C2 r0 = r7.A02     // Catch:{ JSONException -> 0x01d9 }
            com.google.common.collect.ImmutableList r0 = r0.A01()     // Catch:{ JSONException -> 0x01d9 }
            X.1Xv r15 = r0.iterator()     // Catch:{ JSONException -> 0x01d9 }
        L_0x010f:
            boolean r0 = r15.hasNext()     // Catch:{ JSONException -> 0x01d9 }
            if (r0 == 0) goto L_0x0122
            java.lang.Object r0 = r15.next()     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ JSONException -> 0x01d9 }
            r1.put(r0)     // Catch:{ JSONException -> 0x01d9 }
            goto L_0x010f
        L_0x011f:
            X.2C3 r2 = X.AnonymousClass2C3.A01     // Catch:{ JSONException -> 0x01d9 }
            goto L_0x00ab
        L_0x0122:
            r0 = 688(0x2b0, float:9.64E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)     // Catch:{ JSONException -> 0x01d9 }
            r13.put(r0, r1)     // Catch:{ JSONException -> 0x01d9 }
            r19 = r13
            r20 = r12
            r21 = r18
            r19.put(r20, r21)     // Catch:{ JSONException -> 0x01d9 }
            r20 = r11
            r21 = r17
            r19.put(r20, r21)     // Catch:{ JSONException -> 0x01d9 }
            r20 = r10
            r21 = r16
            r19.put(r20, r21)     // Catch:{ JSONException -> 0x01d9 }
            r13.put(r9, r3)     // Catch:{ JSONException -> 0x01d9 }
            r13.put(r8, r2)     // Catch:{ JSONException -> 0x01d9 }
            X.1Ul r0 = r7.A03     // Catch:{ JSONException -> 0x01d9 }
            int r15 = X.AnonymousClass1Y3.AOJ     // Catch:{ JSONException -> 0x01d9 }
            X.0UN r1 = r0.A00     // Catch:{ JSONException -> 0x01d9 }
            r0 = 0
            java.lang.Object r15 = X.AnonymousClass1XX.A02(r0, r15, r1)     // Catch:{ JSONException -> 0x01d9 }
            X.1Yd r15 = (X.C25051Yd) r15     // Catch:{ JSONException -> 0x01d9 }
            r0 = 563246306165049(0x2004500000139, double:2.782806500231314E-309)
            long r0 = r15.At0(r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r15 = "first"
            r13.put(r15, r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r0 = "render_object_list_query_params"
            r4.put(r0, r13)     // Catch:{ JSONException -> 0x01d9 }
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01d9 }
            r13.<init>()     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r1 = "top_level_list_path"
            java.lang.String r0 = "viewer.messenger_contacts.edges"
            r13.put(r1, r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r1 = "object_path"
            java.lang.String r0 = "node"
            r13.put(r1, r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r1 = "primary_key_path"
            java.lang.String r0 = "represented_profile.id"
            r13.put(r1, r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r0 = "render_object_list_graphql_params"
            r4.put(r0, r13)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r1 = "page_info_path"
            java.lang.String r0 = "viewer.messenger_contacts.page_info"
            r13.put(r1, r0)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r1 = "after_param_name"
            java.lang.String r0 = "after"
            r13.put(r1, r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = 2579032528891185(0x9299dcdbfad31, double:1.2742113720322067E-308)
            java.lang.String r13 = "render_multi_objects_query_id"
            r4.put(r13, r0)     // Catch:{ JSONException -> 0x01d9 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01d9 }
            r1.<init>()     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r13 = "profile_ids"
            java.lang.String r0 = "<IDs>"
            r1.put(r13, r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = r18
            r1.put(r12, r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = r17
            r1.put(r11, r0)     // Catch:{ JSONException -> 0x01d9 }
            r0 = r16
            r1.put(r10, r0)     // Catch:{ JSONException -> 0x01d9 }
            r1.put(r9, r3)     // Catch:{ JSONException -> 0x01d9 }
            r1.put(r8, r2)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r0 = "render_multi_objects_query_params"
            r4.put(r0, r1)     // Catch:{ JSONException -> 0x01d9 }
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r1 = r0.toString()     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r0 = "locale"
            r4.put(r0, r1)     // Catch:{ JSONException -> 0x01d9 }
            java.lang.String r0 = r4.toString()     // Catch:{ JSONException -> 0x01d9 }
            r5.A02 = r0     // Catch:{ JSONException -> 0x01d9 }
            goto L_0x01e5
        L_0x01d9:
            r2 = move-exception
            java.lang.String r0 = ""
            r5.A02 = r0
            X.09P r1 = r7.A01
            java.lang.String r0 = "Failed to build subscription params"
            r1.softReport(r6, r0, r2)
        L_0x01e5:
            android.content.Context r0 = r7.A00     // Catch:{ IOException -> 0x020e }
            android.content.res.AssetManager r1 = r0.getAssets()     // Catch:{ IOException -> 0x020e }
            java.lang.String r0 = "ContactOmnistoreSchema.fbs"
            java.io.InputStream r2 = r1.open(r0)     // Catch:{ IOException -> 0x020e }
            int r0 = r2.available()     // Catch:{ all -> 0x0205 }
            byte[] r1 = new byte[r0]     // Catch:{ all -> 0x0205 }
            r2.read(r1)     // Catch:{ all -> 0x0205 }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0205 }
            r0.<init>(r1)     // Catch:{ all -> 0x0205 }
            r5.A03 = r0     // Catch:{ all -> 0x0205 }
            r2.close()     // Catch:{ IOException -> 0x020e }
            goto L_0x0216
        L_0x0205:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0207 }
        L_0x0207:
            r0 = move-exception
            if (r2 == 0) goto L_0x020d
            r2.close()     // Catch:{ all -> 0x020d }
        L_0x020d:
            throw r0     // Catch:{ IOException -> 0x020e }
        L_0x020e:
            r2 = move-exception
            X.09P r1 = r7.A01
            java.lang.String r0 = "Failed to read idl from file"
            r1.softReport(r6, r0, r2)
        L_0x0216:
            android.content.Context r0 = r7.A00     // Catch:{ IOException -> 0x023f }
            android.content.res.AssetManager r1 = r0.getAssets()     // Catch:{ IOException -> 0x023f }
            java.lang.String r0 = "ContactOmnistoreSchema.idna"
            java.io.InputStream r2 = r1.open(r0)     // Catch:{ IOException -> 0x023f }
            int r0 = r2.available()     // Catch:{ all -> 0x0236 }
            byte[] r1 = new byte[r0]     // Catch:{ all -> 0x0236 }
            r2.read(r1)     // Catch:{ all -> 0x0236 }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0236 }
            r0.<init>(r1)     // Catch:{ all -> 0x0236 }
            r5.A04 = r0     // Catch:{ all -> 0x0236 }
            r2.close()     // Catch:{ IOException -> 0x023f }
            goto L_0x0247
        L_0x0236:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0238 }
        L_0x0238:
            r0 = move-exception
            if (r2 == 0) goto L_0x023e
            r2.close()     // Catch:{ all -> 0x023e }
        L_0x023e:
            throw r0     // Catch:{ IOException -> 0x023f }
        L_0x023f:
            r2 = move-exception
            X.09P r1 = r7.A01
            java.lang.String r0 = "Failed to read idl dna from file"
            r1.softReport(r6, r0, r2)
        L_0x0247:
            X.1lJ r0 = new X.1lJ
            r0.<init>(r5)
            X.1lK r0 = X.C32171lK.A00(r14, r0)
            return r0
        L_0x0251:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1Uk.provideSubscriptionInfo(com.facebook.omnistore.Omnistore):X.1lK");
    }

    private AnonymousClass1Uk(AnonymousClass1XY r3) {
        this.A02 = new AnonymousClass0UN(4, r3);
        this.A0A = C10580kT.A04(r3);
        this.A08 = AnonymousClass0VG.A00(AnonymousClass1Y3.A5P, r3);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.Aq8, r3);
        this.A03 = AnonymousClass1Ul.A00(r3);
        this.A05 = AnonymousClass1Um.A00(r3);
        this.A09 = AnonymousClass0WY.A02(r3);
        this.A06 = AnonymousClass0VG.A00(AnonymousClass1Y3.AvG, r3);
        this.A04 = C24541Un.A00(r3);
    }

    private void A01(List list) {
        String[] strArr;
        String str;
        boolean z;
        ImmutableList.Builder builder = ImmutableList.builder();
        ImmutableList.Builder builder2 = ImmutableList.builder();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Delta delta = (Delta) it.next();
            int type = delta.getType();
            String primaryKey = delta.getPrimaryKey();
            Preconditions.checkNotNull(primaryKey);
            if (type == 2) {
                builder2.add((Object) UserKey.A01(primaryKey));
            } else {
                ByteBuffer blob = delta.getBlob();
                Preconditions.checkNotNull(blob);
                builder.add((Object) ((AnonymousClass2BF) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ad8, this.A02)).A01(primaryKey, blob));
            }
        }
        C24541Un r0 = this.A04;
        ImmutableList build = builder.build();
        ImmutableList build2 = builder2.build();
        for (DRG drg : (Set) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AVA, r0.A00)) {
            Preconditions.checkNotNull(build);
            Preconditions.checkNotNull(build2);
            Preconditions.checkNotNull(build);
            if (((AnonymousClass1B4) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ARk, drg.A00)).A00.Aem(284825051272207L)) {
                strArr = new String[]{"profile_pic_square", "aloha_proxy_users_owned", "is_message_ignored_by_viewer"};
            } else {
                strArr = new String[]{"profile_pic_square", "aloha_proxy_users_owned"};
            }
            ImmutableList.Builder builder3 = ImmutableList.builder();
            ImmutableList.Builder builder4 = ImmutableList.builder();
            C24971Xv it2 = build.iterator();
            while (it2.hasNext()) {
                User user = (User) it2.next();
                User A032 = ((C14300t0) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AxE, drg.A00)).A03(user.A0Q);
                if (A032 == null) {
                    builder4.add((Object) user);
                } else {
                    Preconditions.checkNotNull(A032);
                    Preconditions.checkNotNull(user);
                    int length = strArr.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            z = false;
                            break;
                        }
                        String str2 = strArr[i];
                        if ((!"profile_pic_square".equals(str2) || Objects.equal(user.A04(), A032.A04())) && ((!"aloha_proxy_users_owned".equals(str2) || Objects.equal(user.A0V, A032.A0V)) && (!"is_message_ignored_by_viewer".equals(str2) || user.A1W == A032.A1W))) {
                            i++;
                        }
                    }
                    z = true;
                    if (z) {
                        builder3.add((Object) user);
                        builder4.add((Object) user);
                    }
                }
            }
            ImmutableList build3 = builder3.build();
            ImmutableList build4 = builder4.build();
            C57962sv r13 = (C57962sv) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AiQ, drg.A00);
            Preconditions.checkNotNull(build4);
            if (!build4.isEmpty()) {
                SQLiteDatabase A062 = ((C25771aN) r13.A02.get()).A06();
                C007406x.A01(A062, 403055702);
                try {
                    ImmutableSet A0B2 = ImmutableSet.A0B(strArr);
                    ContentValues contentValues = new ContentValues();
                    C24971Xv it3 = build4.iterator();
                    while (it3.hasNext()) {
                        User user2 = (User) it3.next();
                        C25601a6 A002 = C06160ax.A00();
                        A002.A05(C06160ax.A03("user_key", user2.A0Q.A06()));
                        C12890qA r2 = new C12890qA();
                        if (A0B2.contains("profile_pic_square")) {
                            PicSquare A042 = user2.A04();
                            String str3 = null;
                            if (A042 != null) {
                                str3 = r13.A01.A08(A042).toString();
                            }
                            contentValues.put("profile_pic_square", str3);
                            if (Platform.stringIsNullOrEmpty(str3)) {
                                r2.A05(new AnonymousClass1CG("profile_pic_square"));
                            } else {
                                C12890qA r1 = new C12890qA();
                                r1.A05(new C12940qF(new AnonymousClass1CG("profile_pic_square")));
                                r1.A05(C06160ax.A05("profile_pic_square", str3));
                                r2.A05(r1);
                            }
                        }
                        if (A0B2.contains("is_message_ignored_by_viewer")) {
                            contentValues.put("is_message_ignored_by_viewer", Boolean.valueOf(user2.A1W));
                            C12890qA r12 = new C12890qA();
                            r12.A05(new C12940qF(new AnonymousClass1CG("is_message_ignored_by_viewer")));
                            if (user2.A1W) {
                                str = "1";
                            } else {
                                str = "0";
                            }
                            r12.A05(C06160ax.A05("is_message_ignored_by_viewer", str));
                            r2.A05(r12);
                        }
                        A002.A05(r2);
                        A062.update("thread_users", contentValues, A002.A02(), A002.A04());
                        contentValues.clear();
                    }
                    A062.setTransactionSuccessful();
                    C007406x.A02(A062, 1168488342);
                } catch (SQLException e) {
                    C010708t.A08(C57962sv.A06, AnonymousClass24B.$const$string(46), e);
                    throw e;
                } catch (Throwable th) {
                    C007406x.A02(A062, 307091165);
                    throw th;
                }
            }
            C14300t0 r22 = (C14300t0) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AxE, drg.A00);
            synchronized (r22) {
                Preconditions.checkNotNull(build3);
                if (!build3.isEmpty()) {
                    ImmutableSet A0B3 = ImmutableSet.A0B(strArr);
                    ImmutableList.Builder builder5 = ImmutableList.builder();
                    C24971Xv it4 = build3.iterator();
                    while (it4.hasNext()) {
                        User user3 = (User) it4.next();
                        User A033 = r22.A03(user3.A0Q);
                        if (A033 != null) {
                            C07220cv r14 = new C07220cv();
                            r14.A04(A033);
                            if (A0B3.contains("profile_pic_square")) {
                                r14.A0P = user3.A04();
                            }
                            if (A0B3.contains("aloha_proxy_users_owned")) {
                                r14.A0T = user3.A0V;
                            }
                            if (A0B3.contains("is_message_ignored_by_viewer")) {
                                r14.A1b = user3.A1W;
                            }
                            builder5.add((Object) r14.A02());
                        }
                    }
                    r22.A07(builder5.build(), true);
                }
            }
            Preconditions.checkNotNull(build3);
            if (!build3.isEmpty()) {
                ImmutableSet A0A2 = ImmutableSet.A0A(C04300To.A07(build3, DRG.A01));
                if (!A0A2.isEmpty()) {
                    ArrayList A034 = C04300To.A03(A0A2);
                    Intent intent = new Intent(C99084oO.$const$string(1));
                    intent.putParcelableArrayListExtra(C99084oO.$const$string(8), A034);
                    ((C156167Jx) AnonymousClass1XX.A02(3, AnonymousClass1Y3.BOj, drg.A00)).A00.C4x(intent);
                    C07410dQ A012 = ImmutableSet.A01();
                    for (ThreadKey A082 : ((C26681bq) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AuB, drg.A00)).A0D(AnonymousClass07B.A01)) {
                        ThreadSummary A083 = ((C26681bq) AnonymousClass1XX.A02(5, AnonymousClass1Y3.AuB, drg.A00)).A08(A082);
                        if (A083 != null) {
                            C24971Xv it5 = A083.A0m.iterator();
                            while (true) {
                                if (it5.hasNext()) {
                                    if (A0A2.contains(((ThreadParticipant) it5.next()).A00())) {
                                        A012.A01(A083.A0S);
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                    ImmutableList copyOf = ImmutableList.copyOf((java.util.Collection) A012.build());
                    if (!copyOf.isEmpty()) {
                        ((C189216c) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B7s, drg.A00)).A0K(copyOf, "ContactsOmnistoreListenerImpl");
                    }
                }
            }
            C07410dQ A013 = ImmutableSet.A01();
            A013.A00(C04300To.A07(build, DRG.A01));
            A013.A00(build2);
            ImmutableSet A043 = A013.build();
            ((C66333Jt) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BLW, drg.A00)).A00.BDN(A043);
            ((C189216c) AnonymousClass1XX.A02(4, AnonymousClass1Y3.B7s, drg.A00)).A0J(A043.asList(), "ContactsOmnistoreListenerImpl");
        }
    }

    public void BVs(List list) {
        list.size();
        if (((String) this.A09.get()) == null) {
            C010708t.A05(A0B, "Recevied contacts deltas without a logged in user.");
            return;
        }
        C158837Xo r3 = (C158837Xo) this.A06.get();
        Iterator it = list.iterator();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        while (it.hasNext()) {
            Delta delta = (Delta) it.next();
            if (delta.getType() == 1) {
                arrayList.add(delta.getPrimaryKey());
            } else if (delta.getType() == 2) {
                arrayList2.add(delta.getPrimaryKey());
            }
        }
        if (!arrayList.isEmpty()) {
            Intent intent = new Intent(C99084oO.$const$string(47));
            intent.putExtra("user_ids", arrayList);
            r3.A00.C4x(intent);
        }
        if (!arrayList2.isEmpty()) {
            Intent intent2 = new Intent(C99084oO.$const$string(143));
            intent2.putExtra("user_ids", arrayList2);
            r3.A00.C4x(intent2);
        }
        if (this.A01 == null) {
            A01(list);
            return;
        }
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            Delta delta2 = (Delta) it2.next();
            this.A01.put(delta2.getPrimaryKey(), delta2);
        }
    }
}
