package X;

import android.os.Process;
import android.os.SystemClock;
import com.facebook.common.dextricks.stats.ClassLoadingStats;

/* renamed from: X.00T  reason: invalid class name */
public final class AnonymousClass00T {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public AnonymousClass03b A0B;
    public ClassLoadingStats.SnapshotStats A0C;
    public String A0D;
    public boolean A0E;
    public boolean A0F;

    public static void A00(AnonymousClass00T r2) {
        r2.A0F = false;
        r2.A0E = false;
        r2.A02 = -1;
        r2.A00 = -1;
        r2.A01 = -1;
        r2.A0D = "not set";
        r2.A06 = -1;
        r2.A09 = -1;
        r2.A08 = -1;
        r2.A07 = -1;
        r2.A0A = -1;
        r2.A03 = -1;
        r2.A04 = -1;
        r2.A05 = -1;
        r2.A0B = null;
        r2.A0C = null;
    }

    public long A01() {
        if (this.A0B == null) {
            return -1;
        }
        AnonymousClass078 r1 = AnonymousClass077.A00;
        r1.A00.block();
        return r1.A03.get();
    }

    public AnonymousClass00T() {
        A00(this);
    }

    public void A02() {
        int myTid = Process.myTid();
        this.A02 = myTid;
        this.A00 = Process.getThreadPriority(myTid);
        this.A06 = Process.getElapsedCpuTime();
        this.A09 = SystemClock.currentThreadTimeMillis();
        long[] A012 = AnonymousClass00U.A01("/proc/self/stat");
        this.A08 = A012[0];
        this.A07 = A012[2];
        this.A0A = AnonymousClass00U.A00();
        AnonymousClass00Z A002 = AnonymousClass00Y.A00();
        this.A03 = A002.A00;
        this.A04 = A002.A02;
        this.A05 = A002.A04;
        this.A0C = ClassLoadingStats.A00().A01();
        this.A0F = true;
        this.A0E = false;
        this.A01 = -1;
    }
}
