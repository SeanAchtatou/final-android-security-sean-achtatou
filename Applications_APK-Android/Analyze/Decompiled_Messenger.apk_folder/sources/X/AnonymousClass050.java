package X;

import android.util.Log;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.logger.Logger;

/* renamed from: X.050  reason: invalid class name */
public final class AnonymousClass050 {
    public static volatile boolean A00;

    public static String A01(int i) {
        AnonymousClass054 r6;
        String str;
        if (A00 && (r6 = AnonymousClass054.A07) != null) {
            long j = (long) i;
            TraceContext A002 = AnonymousClass054.A00(r6, C003704n.A01, j, null);
            if (A002 == null) {
                str = null;
            } else {
                str = A002.A09;
            }
            if (str == null) {
                Log.w("Profilo/BlackBoxApi", AnonymousClass08S.A09("UNABLE TO STOP: No trace is active. Marker ID: ", i));
            } else {
                A04(i, null);
                Log.w("Profilo/BlackBoxApi", AnonymousClass08S.A0F("TRACE STOP. Marker ID: ", i, "; Trace ID: ", str));
                r6.A0B(C003704n.A01, null, j);
                return str;
            }
        }
        return null;
    }

    private static void A04(int i, String str) {
        int i2 = i;
        Logger.writeStandardEntry(0, 7, 46, 0, 0, i2, 0, 562949953421312L);
        String str2 = str;
        if (str != null) {
            Logger.writeBytesEntry(0, 1, 57, Logger.writeBytesEntry(0, 1, 56, Logger.writeStandardEntry(0, 7, 59, 0, 0, i2, 0, 562949953421312L), "type"), str2);
        }
    }

    public static AnonymousClass09N A00(int i, String str) {
        AnonymousClass054 r9;
        String str2;
        long j;
        AnonymousClass09N r1;
        if (!A00 || (r9 = AnonymousClass054.A07) == null) {
            return null;
        }
        long j2 = (long) i;
        TraceContext A002 = AnonymousClass054.A00(r9, C003704n.A01, j2, null);
        if (A002 == null) {
            str2 = null;
        } else {
            str2 = A002.A09;
        }
        TraceContext A003 = AnonymousClass054.A00(r9, C003704n.A01, j2, null);
        if (A003 == null) {
            j = 0;
        } else {
            j = A003.A05;
        }
        if (str2 == null || j == 0) {
            Log.w("Profilo/BlackBoxApi", AnonymousClass08S.A09("UNABLE TO STOP: No trace is active. Marker ID: ", i));
            return null;
        }
        A04(i, str);
        Log.w("Profilo/BlackBoxApi", AnonymousClass08S.A0F("TRACE STOP. Marker ID: ", i, "; Trace ID: ", str2));
        r9.A0B(C003704n.A01, null, j2);
        AnonymousClass0P7 r4 = AnonymousClass0P7.A01;
        synchronized (r4) {
            r1 = (AnonymousClass09N) r4.A00.get(j);
            if (r1 == null) {
                r1 = new AnonymousClass09N(j, str2);
                r4.A00.put(j, r1);
            }
        }
        return r1;
    }

    public static void A02() {
        AnonymousClass054 r4;
        if (A00 && (r4 = AnonymousClass054.A07) != null) {
            AnonymousClass072 r3 = (AnonymousClass072) r4.A06(C003704n.A01);
            if (r3 == null) {
                Log.w("Profilo/BlackBoxApi", "UNABLE TO ABORT: No config");
                return;
            }
            Log.w("Profilo/BlackBoxApi", AnonymousClass08S.A09("TRACE ABORT. Marker ID: ", r3.A05));
            AnonymousClass054.A05(r4, C003704n.A01, null, 0, (long) r3.A05, 2);
        }
    }

    public static void A03() {
        AnonymousClass054 r3;
        if (A00 && (r3 = AnonymousClass054.A07) != null) {
            AnonymousClass072 r1 = (AnonymousClass072) r3.A06(C003704n.A01);
            if (r1 == null) {
                Log.w("Profilo/BlackBoxApi", "UNABLE TO START: No config");
                return;
            }
            boolean A0A = r3.A0A(C003704n.A01, 2, null, (long) r1.A05);
            Logger.writeStandardEntry(0, 7, 43, 0, 0, r1.A05, 0, 0);
            Log.w("Profilo/BlackBoxApi", "TRACE START: success = " + A0A + "; markerID: " + r1.A05);
        }
    }

    public static boolean A05(int i) {
        AnonymousClass054 r4;
        long j;
        if (!A00 || (r4 = AnonymousClass054.A07) == null) {
            return false;
        }
        TraceContext A002 = AnonymousClass054.A00(r4, C003704n.A01, (long) i, null);
        if (A002 == null) {
            j = 0;
        } else {
            j = A002.A05;
        }
        if (j != 0) {
            return true;
        }
        return false;
    }
}
