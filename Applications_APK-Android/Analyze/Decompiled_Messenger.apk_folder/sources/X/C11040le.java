package X;

import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0le  reason: invalid class name and case insensitive filesystem */
public final class C11040le implements AnonymousClass0lf {
    private static volatile C11040le A01;
    private AnonymousClass0UN A00;

    public void BhH(Class cls, C11010lb r2) {
    }

    public void Bki(Class cls) {
    }

    public static final C11040le A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C11040le.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C11040le(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList
     arg types: [int, int]
     candidates:
      com.google.common.collect.ImmutableList.subList(int, int):java.util.List
      ClspMth{java.util.List.subList(int, int):java.util.List<E>}
      com.google.common.collect.ImmutableList.subList(int, int):com.google.common.collect.ImmutableList */
    public void BhF(Class cls, C11010lb r6, ImmutableList immutableList, long j, long j2, long j3) {
        AnonymousClass0ZF A05 = ((C06230bA) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BTX, this.A00)).A05("blue_service_queue_elapsed_time", false, AnonymousClass07B.A00, false);
        if (A05.A0G()) {
            A05.A0A(C22298Ase.$const$string(3), r6.A04);
            A05.A0A("caller_context", r6.A02.toString());
            A05.A0A("pending_operations", immutableList.subList(Math.max(immutableList.size() - 5, 0), immutableList.size()).toString());
            A05.A09("execution_time_ms", Long.valueOf(j3 - j2));
            A05.A09("queue_elapsed_time_ms", Long.valueOf(j2 - j));
            A05.A0A("queue_name", cls.toString());
            A05.A0E();
        }
    }

    private C11040le(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
