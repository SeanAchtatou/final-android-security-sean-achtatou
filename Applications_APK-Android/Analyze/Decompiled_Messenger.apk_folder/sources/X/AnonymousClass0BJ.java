package X;

import android.os.SystemClock;
import com.facebook.rti.common.time.RealtimeSinceBootClock;

/* renamed from: X.0BJ  reason: invalid class name */
public final class AnonymousClass0BJ implements AnonymousClass0BK {
    private double A00;
    private long A01;
    private final int A02;
    private final long A03;
    private final RealtimeSinceBootClock A04;

    public synchronized boolean ASA() {
        RealtimeSinceBootClock realtimeSinceBootClock = this.A04;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.A01 = elapsedRealtime;
        double d = this.A00;
        double d2 = (double) (elapsedRealtime - this.A01);
        double d3 = (double) this.A02;
        double d4 = (double) this.A03;
        Double.isNaN(d3);
        Double.isNaN(d4);
        Double.isNaN(d2);
        this.A00 = d + ((d2 * (d3 / d4)) / 1000.0d);
        if (this.A00 > ((double) this.A02)) {
            this.A00 = (double) this.A02;
        }
        if (this.A00 < 1.0d) {
            return false;
        }
        this.A00 -= 1.0d;
        return true;
    }

    public AnonymousClass0BJ(RealtimeSinceBootClock realtimeSinceBootClock, int i, long j) {
        this.A04 = realtimeSinceBootClock;
        this.A02 = i;
        this.A03 = j;
        this.A00 = (double) i;
    }
}
