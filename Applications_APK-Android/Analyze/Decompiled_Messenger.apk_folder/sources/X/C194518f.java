package X;

import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Queue;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.18f  reason: invalid class name and case insensitive filesystem */
public final class C194518f {
    private static volatile C194518f A04;
    public final SparseArray A00 = new SparseArray();
    private final AnonymousClass18d A01 = new AnonymousClass18d();
    private final ThreadLocal A02 = new ThreadLocal();
    private final ThreadLocal A03 = new ThreadLocal();

    public static final C194518f A00(AnonymousClass1XY r3) {
        if (A04 == null) {
            synchronized (C194518f.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A04 = new C194518f();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    public void A02(Object obj) {
        if (obj instanceof AnonymousClass2W7) {
            AnonymousClass2W7 r5 = (AnonymousClass2W7) obj;
            if (Boolean.TRUE.equals(this.A03.get())) {
                Object obj2 = (Queue) this.A02.get();
                if (obj2 == null) {
                    obj2 = new ArrayDeque();
                    this.A02.set(obj2);
                }
                obj2.add(r5);
                return;
            }
            this.A03.set(true);
            do {
                int Aax = r5.Aax();
                synchronized (this.A00) {
                    ArrayList arrayList = (ArrayList) this.A00.get(Aax);
                    if (arrayList != null) {
                        A01(arrayList);
                        ArrayList arrayList2 = new ArrayList(arrayList.size());
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            AnonymousClass0qy r0 = (AnonymousClass0qy) ((WeakReference) it.next()).get();
                            if (r0 != null) {
                                arrayList2.add(r0);
                            }
                        }
                        Iterator it2 = arrayList2.iterator();
                        while (it2.hasNext()) {
                            ((AnonymousClass0qy) it2.next()).Aaz(r5);
                        }
                    }
                }
                if (this.A02.get() != null) {
                    r5 = (AnonymousClass2W7) ((Queue) this.A02.get()).poll();
                    continue;
                } else {
                    r5 = null;
                    continue;
                }
            } while (r5 != null);
            this.A02.set(null);
            this.A03.set(false);
            return;
        }
        throw new ClassCastException("Events must be annotated with @BusEvent");
    }

    public void A03(Object obj) {
        boolean z;
        if (obj instanceof AnonymousClass0qy) {
            AnonymousClass0qy r9 = (AnonymousClass0qy) obj;
            synchronized (this.A01) {
                AnonymousClass18d r1 = this.A01;
                r1.A01 = 0;
                r9.Aay(r1);
                int i = this.A01.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    int i3 = this.A01.A00[i2];
                    synchronized (this.A00) {
                        ArrayList arrayList = (ArrayList) this.A00.get(i3);
                        if (arrayList == null) {
                            ArrayList arrayList2 = new ArrayList();
                            arrayList2.add(new WeakReference(r9));
                            this.A00.put(i3, arrayList2);
                        } else {
                            A01(arrayList);
                            Iterator it = arrayList.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (((WeakReference) it.next()).get() == r9) {
                                        z = true;
                                        break;
                                    }
                                } else {
                                    z = false;
                                    break;
                                }
                            }
                            if (!z) {
                                arrayList.add(new WeakReference(r9));
                            }
                        }
                    }
                }
            }
            return;
        }
        throw new ClassCastException("Subscribers must be annotated with @BusSubscriber");
    }

    public void A04(Object obj) {
        if (obj instanceof AnonymousClass0qy) {
            AnonymousClass0qy r10 = (AnonymousClass0qy) obj;
            synchronized (this.A01) {
                AnonymousClass18d r1 = this.A01;
                r1.A01 = 0;
                r10.Aay(r1);
                int i = this.A01.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    int i3 = this.A01.A00[i2];
                    synchronized (this.A00) {
                        ArrayList arrayList = (ArrayList) this.A00.get(i3);
                        if (arrayList != null) {
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                WeakReference weakReference = (WeakReference) it.next();
                                if (weakReference.get() == r10) {
                                    weakReference.clear();
                                }
                            }
                            A01(arrayList);
                            if (arrayList.isEmpty()) {
                                this.A00.remove(i3);
                            }
                        }
                    }
                }
            }
            return;
        }
        throw new ClassCastException("Subscribers must be annotated with @BusSubscriber");
    }

    private static void A01(ArrayList arrayList) {
        int size = arrayList.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            WeakReference weakReference = (WeakReference) arrayList.get(i2);
            if (weakReference.get() != null) {
                arrayList.set(i, weakReference);
                i++;
            }
        }
        for (int size2 = arrayList.size() - 1; size2 >= i; size2--) {
            arrayList.remove(size2);
        }
    }
}
