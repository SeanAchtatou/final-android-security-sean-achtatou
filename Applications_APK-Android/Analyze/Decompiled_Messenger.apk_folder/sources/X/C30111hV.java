package X;

import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1hV  reason: invalid class name and case insensitive filesystem */
public final class C30111hV extends AnonymousClass0UV {
    public static final BlueServiceOperationFactory A00(AnonymousClass1XY r3) {
        return new C09090gW(new C09100gX(r3), C10580kT.A01(r3));
    }
}
