package X;

import com.facebook.webpsupport.WebpBitmapFactoryImpl;

/* renamed from: X.1PJ  reason: invalid class name */
public final class AnonymousClass1PJ {
    public int A00;
    public int A01 = 0;
    public int A02 = 0;
    public int A03 = 2048;
    public long A04 = 0;
    public C23111Og A05;
    public C23111Og A06 = new AnonymousClass1PK(false);
    public AnonymousClass1PO A07;
    public C84013yd A08;
    public WebpBitmapFactoryImpl A09;
    public boolean A0A = false;
    public boolean A0B = false;
    public boolean A0C;
    public boolean A0D = true;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H = false;
    public boolean A0I = false;
    public boolean A0J = false;
    public boolean A0K = false;
    public final AnonymousClass1PI A0L;

    public AnonymousClass1PJ(AnonymousClass1PI r3) {
        this.A0L = r3;
    }
}
