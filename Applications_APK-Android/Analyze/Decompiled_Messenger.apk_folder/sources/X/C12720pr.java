package X;

import android.database.Cursor;
import com.facebook.user.model.UserKey;
import io.card.payment.BuildConfig;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0pr  reason: invalid class name and case insensitive filesystem */
public final class C12720pr {
    private static volatile C12720pr A02;
    private final C04310Tq A00;
    private final C04310Tq A01;

    public static final C12720pr A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C12720pr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C12720pr(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public Cursor A01(String str, boolean z, long j) {
        String str2;
        String str3;
        String str4;
        String[] strArr;
        AnonymousClass7W5[] r4 = AnonymousClass7VW.A09;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < r4.length; i++) {
            if (i != 0) {
                sb.append(',');
            }
            AnonymousClass7W5 r1 = r4[i];
            sb.append(r1.A01);
            sb.append(" as ");
            sb.append(r1.A00);
        }
        String replace = "select distinct {projection} from threads join thread_participants on thread_participants.thread_key = threads.thread_key join thread_users on thread_users.user_key = thread_participants.user_key where threads.folder = 'inbox' {thread-type-filter} and (thread_users.first_name like ?1 or thread_users.last_name like ?1 or thread_users.name like ?1) {timestamp-filter} {viewer-filter}union select distinct {projection} from threads where threads.folder = 'inbox' {thread-type-filter} and (threads.name like ?1 or threads.name like ?2) {timestamp-filter} order by threads_timestamp_ms desc".replace("{projection}", sb.toString());
        if (z) {
            str2 = "and (threads.thread_key like 'GROUP%' or threads.thread_key like 'ONE_TO_ONE%')";
        } else {
            str2 = "and threads.thread_key like 'GROUP%'";
        }
        String replace2 = replace.replace("{thread-type-filter}", str2);
        if (j > 0) {
            str3 = "and threads_timestamp_ms > ?3";
        } else {
            str3 = BuildConfig.FLAVOR;
        }
        String replace3 = replace2.replace("{timestamp-filter}", str3).replace("{viewer-filter}", "and not thread_users.user_key = ?4");
        if (j > 0) {
            str4 = String.valueOf(j);
        } else {
            str4 = null;
        }
        String A06 = ((UserKey) this.A00.get()).A06();
        String A0J = AnonymousClass08S.A0J(str, "%");
        String A0P = AnonymousClass08S.A0P("% ", str, "%");
        if (str4 != null) {
            strArr = new String[]{A0J, A0P, str4, A06};
        } else {
            strArr = new String[]{A0J, A0P};
        }
        return ((C25771aN) this.A01.get()).A06().rawQuery(replace3, strArr);
    }

    private C12720pr(AnonymousClass1XY r2) {
        this.A01 = C25771aN.A02(r2);
        this.A00 = AnonymousClass0XJ.A0I(r2);
    }
}
