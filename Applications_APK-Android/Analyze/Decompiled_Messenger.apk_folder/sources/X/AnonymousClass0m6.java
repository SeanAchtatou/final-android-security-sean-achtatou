package X;

import com.facebook.auth.annotations.LoggedInUser;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.MontageThreadPreview;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.user.model.User;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.0m6  reason: invalid class name */
public final class AnonymousClass0m6 implements C05460Za {
    public AnonymousClass0UN A00;
    public final AnonymousClass04b A01 = new AnonymousClass04b();
    public final C07380dK A02;
    public final AnonymousClass1YI A03;
    public final C11220mP A04;
    public final C11220mP A05;
    public final C11210mO A06;
    public final C11210mO A07;
    public final AnonymousClass0mN A08;
    public final AnonymousClass0m8 A09;
    public final AnonymousClass0mL A0A;
    public final AnonymousClass0m9 A0B;
    public final AnonymousClass0mB A0C;
    public final C04310Tq A0D;
    @LoggedInUser
    public final C04310Tq A0E;
    public final C04310Tq A0F;

    public static void A08(AnonymousClass0m6 r2, MessagesCollection messagesCollection) {
        if (C010708t.A0X(2)) {
            StringBuilder sb = new StringBuilder("  ");
            sb.append(r2.A09.logName);
            sb.append(" Messages:\n");
            A0L(sb, messagesCollection, 8);
        }
    }

    private void A0J(ThreadSummary threadSummary, ThreadSummary threadSummary2) {
        if (C010708t.A0X(2)) {
            String threadSummary3 = threadSummary.toString();
            if (threadSummary2 != null) {
                Objects.equal(threadSummary3, threadSummary2.toString());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0024, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (r2 != null) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C12560pa A00(X.AnonymousClass0m6 r3, X.C10950l8 r4) {
        /*
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.04b r0 = r3.A01     // Catch:{ all -> 0x0022 }
            java.lang.Object r1 = r0.get(r4)     // Catch:{ all -> 0x0022 }
            X.0pa r1 = (X.C12560pa) r1     // Catch:{ all -> 0x0022 }
            if (r1 != 0) goto L_0x001c
            X.0pa r1 = new X.0pa     // Catch:{ all -> 0x0022 }
            X.0mL r0 = r3.A0A     // Catch:{ all -> 0x0022 }
            r1.<init>(r4, r0)     // Catch:{ all -> 0x0022 }
            X.04b r0 = r3.A01     // Catch:{ all -> 0x0022 }
            r0.put(r4, r1)     // Catch:{ all -> 0x0022 }
        L_0x001c:
            if (r2 == 0) goto L_0x0021
            r2.close()
        L_0x0021:
            return r1
        L_0x0022:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0024 }
        L_0x0024:
            r0 = move-exception
            if (r2 == 0) goto L_0x002a
            r2.close()     // Catch:{ all -> 0x002a }
        L_0x002a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A00(X.0m6, X.0l8):X.0pa");
    }

    public static MessagesCollection A01(MessagesCollection messagesCollection, Message message, int i) {
        Message[] messageArr = (Message[]) messagesCollection.A01.toArray(new Message[0]);
        messageArr[i] = message;
        C33881oI r1 = new C33881oI();
        r1.A00 = messagesCollection.A00;
        r1.A01(ImmutableList.copyOf(messageArr));
        r1.A03 = messagesCollection.A02;
        r1.A04 = messagesCollection.A03;
        r1.A02 = true;
        return r1.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0047, code lost:
        if (r3 != null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004c, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.messaging.model.threads.ThreadSummary A02(X.AnonymousClass0m6 r4, com.facebook.messaging.model.threadkey.ThreadKey r5) {
        /*
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            X.0l8 r0 = X.C10950l8.A05     // Catch:{ all -> 0x0044 }
            X.0pa r1 = A00(r4, r0)     // Catch:{ all -> 0x0044 }
            X.0mL r0 = r1.A06     // Catch:{ all -> 0x0044 }
            r0.A01()     // Catch:{ all -> 0x0044 }
            X.07X r0 = r1.A05     // Catch:{ all -> 0x0044 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x0044 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0044 }
        L_0x001b:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0044 }
            if (r0 == 0) goto L_0x003d
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0044 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1     // Catch:{ all -> 0x0044 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0S     // Catch:{ all -> 0x0044 }
            boolean r0 = r0.A0N()     // Catch:{ all -> 0x0044 }
            if (r0 == 0) goto L_0x001b
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0R     // Catch:{ all -> 0x0044 }
            boolean r0 = r5.equals(r0)     // Catch:{ all -> 0x0044 }
            if (r0 == 0) goto L_0x001b
            if (r3 == 0) goto L_0x003c
            r3.close()
        L_0x003c:
            return r1
        L_0x003d:
            if (r3 == 0) goto L_0x0042
            r3.close()
        L_0x0042:
            r0 = 0
            return r0
        L_0x0044:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0046 }
        L_0x0046:
            r0 = move-exception
            if (r3 == 0) goto L_0x004c
            r3.close()     // Catch:{ all -> 0x004c }
        L_0x004c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A02(X.0m6, com.facebook.messaging.model.threadkey.ThreadKey):com.facebook.messaging.model.threads.ThreadSummary");
    }

    public static ThreadSummary A03(AnonymousClass0m6 r3, ThreadSummary threadSummary, MessagesCollection messagesCollection) {
        if (threadSummary.A0O != C10950l8.A06) {
            return threadSummary;
        }
        MontageThreadPreview A0F2 = ((AnonymousClass1TU) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8n, r3.A00)).A0F(messagesCollection.A01);
        C17920zh A002 = ThreadSummary.A00();
        A002.A02(threadSummary);
        A002.A0W = A0F2;
        ThreadSummary A003 = A002.A00();
        r3.A0b(A003);
        return A003;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0063, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0064, code lost:
        if (r8 != null) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0069, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A05(X.AnonymousClass0m6 r9) {
        /*
            X.0mL r0 = r9.A0A
            X.0mM r8 = r0.A00()
            X.04b r0 = r9.A01     // Catch:{ all -> 0x0061 }
            int r7 = r0.size()     // Catch:{ all -> 0x0061 }
            r5 = 0
            r6 = 0
        L_0x000e:
            if (r6 >= r7) goto L_0x0049
            X.04b r0 = r9.A01     // Catch:{ all -> 0x0061 }
            java.lang.Object r1 = r0.A09(r6)     // Catch:{ all -> 0x0061 }
            X.0pa r1 = (X.C12560pa) r1     // Catch:{ all -> 0x0061 }
            X.0mL r0 = r1.A06     // Catch:{ all -> 0x0061 }
            r0.A01()     // Catch:{ all -> 0x0061 }
            r1.A04 = r5     // Catch:{ all -> 0x0061 }
            X.0m9 r4 = r9.A0B     // Catch:{ all -> 0x0061 }
            X.0l8 r3 = r1.A07     // Catch:{ all -> 0x0061 }
            monitor-enter(r4)     // Catch:{ all -> 0x0061 }
            boolean r0 = r4.A0F()     // Catch:{ all -> 0x0046 }
            if (r0 == 0) goto L_0x0042
            X.0l8 r1 = X.C10950l8.A05     // Catch:{ all -> 0x0046 }
            r0 = 0
            if (r3 != r1) goto L_0x0030
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x0042
            java.lang.String r2 = "markAllThreadListsStaleInCache"
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x0046 }
            r0 = 0
            X.3bx r1 = X.AnonymousClass0m9.A01(r4, r0, r0, r2, r1)     // Catch:{ all -> 0x0046 }
            android.util.LruCache r0 = r4.A01     // Catch:{ all -> 0x0046 }
            r0.put(r1, r1)     // Catch:{ all -> 0x0046 }
        L_0x0042:
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            int r6 = r6 + 1
            goto L_0x000e
        L_0x0046:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            throw r0     // Catch:{ all -> 0x0061 }
        L_0x0049:
            X.0mP r1 = r9.A04     // Catch:{ all -> 0x0061 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0061 }
            r0.A01()     // Catch:{ all -> 0x0061 }
            r1.A01 = r5     // Catch:{ all -> 0x0061 }
            X.0mP r1 = r9.A05     // Catch:{ all -> 0x0061 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0061 }
            r0.A01()     // Catch:{ all -> 0x0061 }
            r1.A01 = r5     // Catch:{ all -> 0x0061 }
            if (r8 == 0) goto L_0x0060
            r8.close()
        L_0x0060:
            return
        L_0x0061:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r0 = move-exception
            if (r8 == 0) goto L_0x0069
            r8.close()     // Catch:{ all -> 0x0069 }
        L_0x0069:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A05(X.0m6):void");
    }

    public static void A06(AnonymousClass0m6 r3, C10950l8 r4, ThreadKey threadKey) {
        if (r4 != null && threadKey != null) {
            A00(r3, r4).A00(threadKey);
            AnonymousClass0m9 r32 = r3.A0B;
            synchronized (r32) {
                if (r32.A0F()) {
                    boolean z = false;
                    if (r4 == C10950l8.A05) {
                        z = true;
                    }
                    if (z) {
                        C71183bx A012 = AnonymousClass0m9.A01(r32, threadKey, null, "removeThreadFromFolderThreadListInCache", r4.toString());
                        r32.A01.put(A012, A012);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003b, code lost:
        if (r3 != null) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A09(X.AnonymousClass0m6 r4, com.facebook.messaging.model.messages.MessagesCollection r5, X.C11210mO r6, boolean r7) {
        /*
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r5.A00     // Catch:{ all -> 0x0038 }
            A0A(r4, r2)     // Catch:{ all -> 0x0038 }
            X.0Tq r0 = r4.A0E     // Catch:{ all -> 0x0038 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0038 }
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0     // Catch:{ all -> 0x0038 }
            r1 = 0
            r6.A04(r5, r0, r1, r7)     // Catch:{ all -> 0x0038 }
            X.0mN r0 = r4.A08     // Catch:{ all -> 0x0038 }
            X.1oR r0 = r0.A00(r2)     // Catch:{ all -> 0x0038 }
            r0.A00 = r1     // Catch:{ all -> 0x0038 }
            X.0mN r0 = r4.A08     // Catch:{ all -> 0x0038 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A01(r2)     // Catch:{ all -> 0x0038 }
            if (r0 != 0) goto L_0x002e
            A05(r4)     // Catch:{ all -> 0x0038 }
        L_0x002a:
            A08(r4, r5)     // Catch:{ all -> 0x0038 }
            goto L_0x0032
        L_0x002e:
            A03(r4, r0, r5)     // Catch:{ all -> 0x0038 }
            goto L_0x002a
        L_0x0032:
            if (r3 == 0) goto L_0x0037
            r3.close()
        L_0x0037:
            return
        L_0x0038:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003a }
        L_0x003a:
            r0 = move-exception
            if (r3 == 0) goto L_0x0040
            r3.close()     // Catch:{ all -> 0x0040 }
        L_0x0040:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A09(X.0m6, com.facebook.messaging.model.messages.MessagesCollection, X.0mO, boolean):void");
    }

    public static void A0B(AnonymousClass0m6 r2, ThreadKey threadKey) {
        r2.A08.A02(threadKey, "removeThread");
        C11220mP r1 = r2.A04;
        r1.A03.A01();
        List list = r1.A00;
        if (list != null) {
            list.remove(threadKey);
        }
        C11220mP r12 = r2.A05;
        r12.A03.A01();
        List list2 = r12.A00;
        if (list2 != null) {
            list2.remove(threadKey);
        }
        r2.A06.A05(threadKey);
        r2.A07.A05(threadKey);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        if (r3 != null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0044, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0D(X.AnonymousClass0m6 r4, com.facebook.messaging.model.threads.ThreadsCollection r5, X.C11220mP r6) {
        /*
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            com.google.common.collect.ImmutableList r1 = r5.A00     // Catch:{ all -> 0x003c }
            r4.A0K(r1)     // Catch:{ all -> 0x003c }
            com.google.common.base.Function r0 = X.C42782Bv.A01     // Catch:{ all -> 0x003c }
            java.util.Collection r2 = X.AnonymousClass0TH.A00(r1, r0)     // Catch:{ all -> 0x003c }
            boolean r1 = r5.A01     // Catch:{ all -> 0x003c }
            X.0mL r0 = r6.A03     // Catch:{ all -> 0x003c }
            r0.A01()     // Catch:{ all -> 0x003c }
            java.util.List r0 = r6.A00     // Catch:{ all -> 0x003c }
            if (r0 != 0) goto L_0x0032
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x003c }
            r0.<init>(r2)     // Catch:{ all -> 0x003c }
            r6.A00 = r0     // Catch:{ all -> 0x003c }
        L_0x0023:
            boolean r0 = r6.A02     // Catch:{ all -> 0x003c }
            if (r0 != 0) goto L_0x0029
            r6.A02 = r1     // Catch:{ all -> 0x003c }
        L_0x0029:
            r1 = 1
            X.0mL r0 = r6.A03     // Catch:{ all -> 0x003c }
            r0.A01()     // Catch:{ all -> 0x003c }
            r6.A01 = r1     // Catch:{ all -> 0x003c }
            goto L_0x0036
        L_0x0032:
            r0.addAll(r2)     // Catch:{ all -> 0x003c }
            goto L_0x0023
        L_0x0036:
            if (r3 == 0) goto L_0x003b
            r3.close()
        L_0x003b:
            return
        L_0x003c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x003e }
        L_0x003e:
            r0 = move-exception
            if (r3 == 0) goto L_0x0044
            r3.close()     // Catch:{ all -> 0x0044 }
        L_0x0044:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0D(X.0m6, com.facebook.messaging.model.threads.ThreadsCollection, X.0mP):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0031, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        if (r3 != null) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0E(X.AnonymousClass0m6 r4, com.facebook.messaging.model.threads.ThreadsCollection r5, X.C11220mP r6) {
        /*
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            com.google.common.collect.ImmutableList r1 = r5.A00     // Catch:{ all -> 0x002f }
            r4.A0K(r1)     // Catch:{ all -> 0x002f }
            com.google.common.base.Function r0 = X.C42782Bv.A01     // Catch:{ all -> 0x002f }
            java.util.Collection r2 = X.AnonymousClass0TH.A00(r1, r0)     // Catch:{ all -> 0x002f }
            boolean r1 = r5.A01     // Catch:{ all -> 0x002f }
            X.0mL r0 = r6.A03     // Catch:{ all -> 0x002f }
            r0.A01()     // Catch:{ all -> 0x002f }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x002f }
            r0.<init>(r2)     // Catch:{ all -> 0x002f }
            r6.A00 = r0     // Catch:{ all -> 0x002f }
            r6.A02 = r1     // Catch:{ all -> 0x002f }
            r1 = 1
            X.0mL r0 = r6.A03     // Catch:{ all -> 0x002f }
            r0.A01()     // Catch:{ all -> 0x002f }
            r6.A01 = r1     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x002e
            r3.close()
        L_0x002e:
            return
        L_0x002f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            if (r3 == 0) goto L_0x0037
            r3.close()     // Catch:{ all -> 0x0037 }
        L_0x0037:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0E(X.0m6, com.facebook.messaging.model.threads.ThreadsCollection, X.0mP):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004a, code lost:
        if (r5 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004f, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0F(X.AnonymousClass0m6 r6, java.lang.String r7, X.C22138ApO r8, boolean r9) {
        /*
            X.0mL r0 = r6.A0A
            X.0mM r5 = r0.A00()
            if (r9 == 0) goto L_0x000b
            X.0mO r4 = r6.A06     // Catch:{ all -> 0x0047 }
            goto L_0x000d
        L_0x000b:
            X.0mO r4 = r6.A07     // Catch:{ all -> 0x0047 }
        L_0x000d:
            com.facebook.messaging.model.messages.Message r3 = r4.A01(r7)     // Catch:{ all -> 0x0047 }
            if (r3 == 0) goto L_0x0041
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0U     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0041
            if (r9 == 0) goto L_0x001e
            com.facebook.messaging.model.messages.MessagesCollection r2 = r6.A0P(r0)     // Catch:{ all -> 0x0047 }
            goto L_0x0022
        L_0x001e:
            com.facebook.messaging.model.messages.MessagesCollection r2 = r6.A0Q(r0)     // Catch:{ all -> 0x0047 }
        L_0x0022:
            if (r2 == 0) goto L_0x0041
            com.google.common.collect.ImmutableList r0 = r2.A01     // Catch:{ all -> 0x0047 }
            int r1 = r0.indexOf(r3)     // Catch:{ all -> 0x0047 }
            r0 = -1
            if (r1 == r0) goto L_0x0041
            com.facebook.messaging.model.messages.Message r0 = r8.Au7(r3)     // Catch:{ all -> 0x0047 }
            com.facebook.messaging.model.messages.MessagesCollection r2 = A01(r2, r0, r1)     // Catch:{ all -> 0x0047 }
            X.0Tq r0 = r6.A0E     // Catch:{ all -> 0x0047 }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x0047 }
            com.facebook.user.model.User r1 = (com.facebook.user.model.User) r1     // Catch:{ all -> 0x0047 }
            r0 = 0
            r4.A04(r2, r1, r0, r0)     // Catch:{ all -> 0x0047 }
        L_0x0041:
            if (r5 == 0) goto L_0x0046
            r5.close()
        L_0x0046:
            return
        L_0x0047:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            if (r5 == 0) goto L_0x004f
            r5.close()     // Catch:{ all -> 0x004f }
        L_0x004f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0F(X.0m6, java.lang.String, X.ApO, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0043, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0044, code lost:
        if (r4 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0G(X.AnonymousClass0m6 r5, java.lang.String r6, X.AnonymousClass0V0 r7, X.C11210mO r8, com.facebook.messaging.model.messages.MessagesCollection r9) {
        /*
            X.0mL r0 = r5.A0A
            X.0mM r4 = r0.A00()
            com.facebook.messaging.model.messages.Message r3 = r8.A01(r6)     // Catch:{ all -> 0x0041 }
            if (r3 == 0) goto L_0x003b
            if (r9 == 0) goto L_0x003b
            com.google.common.collect.ImmutableList r0 = r9.A01     // Catch:{ all -> 0x0041 }
            int r2 = r0.indexOf(r3)     // Catch:{ all -> 0x0041 }
            if (r2 < 0) goto L_0x003b
            X.1TG r1 = com.facebook.messaging.model.messages.Message.A00()     // Catch:{ all -> 0x0041 }
            r1.A03(r3)     // Catch:{ all -> 0x0041 }
            com.google.common.base.Preconditions.checkNotNull(r7)     // Catch:{ all -> 0x0041 }
            com.google.common.collect.HashMultimap r0 = new com.google.common.collect.HashMultimap     // Catch:{ all -> 0x0041 }
            r0.<init>(r7)     // Catch:{ all -> 0x0041 }
            r1.A0h = r0     // Catch:{ all -> 0x0041 }
            com.facebook.messaging.model.messages.Message r0 = r1.A00()     // Catch:{ all -> 0x0041 }
            com.facebook.messaging.model.messages.MessagesCollection r2 = A01(r9, r0, r2)     // Catch:{ all -> 0x0041 }
            X.0Tq r0 = r5.A0E     // Catch:{ all -> 0x0041 }
            java.lang.Object r1 = r0.get()     // Catch:{ all -> 0x0041 }
            com.facebook.user.model.User r1 = (com.facebook.user.model.User) r1     // Catch:{ all -> 0x0041 }
            r0 = 0
            r8.A04(r2, r1, r0, r0)     // Catch:{ all -> 0x0041 }
        L_0x003b:
            if (r4 == 0) goto L_0x0040
            r4.close()
        L_0x0040:
            return
        L_0x0041:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r0 = move-exception
            if (r4 == 0) goto L_0x0049
            r4.close()     // Catch:{ all -> 0x0049 }
        L_0x0049:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0G(X.0m6, java.lang.String, X.0V0, X.0mO, com.facebook.messaging.model.messages.MessagesCollection):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0031, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        if (r3 != null) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0I(com.facebook.messaging.model.threads.ThreadSummary r5, X.C10950l8 r6) {
        /*
            r4 = this;
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0S     // Catch:{ all -> 0x002f }
            A0A(r4, r0)     // Catch:{ all -> 0x002f }
            X.0pa r2 = A00(r4, r6)     // Catch:{ all -> 0x002f }
            X.0mN r1 = r4.A08     // Catch:{ all -> 0x002f }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0S     // Catch:{ all -> 0x002f }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A01(r0)     // Catch:{ all -> 0x002f }
            r4.A0J(r5, r0)     // Catch:{ all -> 0x002f }
            X.0mN r1 = r4.A08     // Catch:{ all -> 0x002f }
            java.lang.String r0 = "updateThreadSummary"
            r1.A03(r5, r0)     // Catch:{ all -> 0x002f }
            r2.A03(r5)     // Catch:{ all -> 0x002f }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0S     // Catch:{ all -> 0x002f }
            r4.A0H(r0)     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x002e
            r3.close()
        L_0x002e:
            return
        L_0x002f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            if (r3 == 0) goto L_0x0037
            r3.close()     // Catch:{ all -> 0x0037 }
        L_0x0037:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0I(com.facebook.messaging.model.threads.ThreadSummary, X.0l8):void");
    }

    private static void A0L(StringBuilder sb, MessagesCollection messagesCollection, int i) {
        if (messagesCollection == null || messagesCollection.A08()) {
            sb.append("    none\n");
            return;
        }
        int i2 = 0;
        while (i2 < i && i2 < messagesCollection.A04()) {
            Message A072 = messagesCollection.A07(i2);
            sb.append("   ");
            sb.append(A072);
            sb.append("\n");
            i2++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (r2 != null) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A0M(X.C10950l8 r4) {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0pa r1 = A00(r3, r4)     // Catch:{ all -> 0x0017 }
            X.0mL r0 = r1.A06     // Catch:{ all -> 0x0017 }
            r0.A01()     // Catch:{ all -> 0x0017 }
            long r0 = r1.A00     // Catch:{ all -> 0x0017 }
            if (r2 == 0) goto L_0x0016
            r2.close()
        L_0x0016:
            return r0
        L_0x0017:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0019 }
        L_0x0019:
            r0 = move-exception
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ all -> 0x001f }
        L_0x001f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0M(X.0l8):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (r2 != null) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.folders.FolderCounts A0N(X.C10950l8 r4) {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0pa r1 = A00(r3, r4)     // Catch:{ all -> 0x0017 }
            X.0mL r0 = r1.A06     // Catch:{ all -> 0x0017 }
            r0.A01()     // Catch:{ all -> 0x0017 }
            com.facebook.messaging.model.folders.FolderCounts r0 = r1.A01     // Catch:{ all -> 0x0017 }
            if (r2 == 0) goto L_0x0016
            r2.close()
        L_0x0016:
            return r0
        L_0x0017:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0019 }
        L_0x0019:
            r0 = move-exception
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ all -> 0x001f }
        L_0x001f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0N(X.0l8):com.facebook.messaging.model.folders.FolderCounts");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0024, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (r3 != null) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.Message A0O(java.lang.String r5) {
        /*
            r4 = this;
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0022 }
            java.lang.String r0 = "getMessageById_total"
            r1.A03(r0)     // Catch:{ all -> 0x0022 }
            X.0mO r0 = r4.A06     // Catch:{ all -> 0x0022 }
            com.facebook.messaging.model.messages.Message r2 = r0.A01(r5)     // Catch:{ all -> 0x0022 }
            if (r2 == 0) goto L_0x001c
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0022 }
            java.lang.String r0 = "getMessageById_hit"
            r1.A03(r0)     // Catch:{ all -> 0x0022 }
        L_0x001c:
            if (r3 == 0) goto L_0x0021
            r3.close()
        L_0x0021:
            return r2
        L_0x0022:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0024 }
        L_0x0024:
            r0 = move-exception
            if (r3 == 0) goto L_0x002a
            r3.close()     // Catch:{ all -> 0x002a }
        L_0x002a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0O(java.lang.String):com.facebook.messaging.model.messages.Message");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r3 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.MessagesCollection A0P(com.facebook.messaging.model.threadkey.ThreadKey r5) {
        /*
            r4 = this;
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            A0A(r4, r5)     // Catch:{ all -> 0x0025 }
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = "getThreadMessagesByThreadKey_total"
            r1.A03(r0)     // Catch:{ all -> 0x0025 }
            X.0mO r0 = r4.A06     // Catch:{ all -> 0x0025 }
            com.facebook.messaging.model.messages.MessagesCollection r2 = r0.A02(r5)     // Catch:{ all -> 0x0025 }
            if (r2 == 0) goto L_0x001f
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = "getThreadMessagesByThreadKey_hit"
            r1.A03(r0)     // Catch:{ all -> 0x0025 }
        L_0x001f:
            if (r3 == 0) goto L_0x0024
            r3.close()
        L_0x0024:
            return r2
        L_0x0025:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            if (r3 == 0) goto L_0x002d
            r3.close()     // Catch:{ all -> 0x002d }
        L_0x002d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0P(com.facebook.messaging.model.threadkey.ThreadKey):com.facebook.messaging.model.messages.MessagesCollection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r3 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.MessagesCollection A0Q(com.facebook.messaging.model.threadkey.ThreadKey r5) {
        /*
            r4 = this;
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            A0A(r4, r5)     // Catch:{ all -> 0x0025 }
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = "getThreadMessagesContextByThreadKey_total"
            r1.A03(r0)     // Catch:{ all -> 0x0025 }
            X.0mO r0 = r4.A07     // Catch:{ all -> 0x0025 }
            com.facebook.messaging.model.messages.MessagesCollection r2 = r0.A02(r5)     // Catch:{ all -> 0x0025 }
            if (r2 == 0) goto L_0x001f
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = "getThreadMessagesContextByThreadKey_hit"
            r1.A03(r0)     // Catch:{ all -> 0x0025 }
        L_0x001f:
            if (r3 == 0) goto L_0x0024
            r3.close()
        L_0x0024:
            return r2
        L_0x0025:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            if (r3 == 0) goto L_0x002d
            r3.close()     // Catch:{ all -> 0x002d }
        L_0x002d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0Q(com.facebook.messaging.model.threadkey.ThreadKey):com.facebook.messaging.model.messages.MessagesCollection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r3 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.threads.ThreadSummary A0R(com.facebook.messaging.model.threadkey.ThreadKey r5) {
        /*
            r4 = this;
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            A0A(r4, r5)     // Catch:{ all -> 0x0025 }
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = "getThreadSummaryByKey_total"
            r1.A03(r0)     // Catch:{ all -> 0x0025 }
            X.0mN r0 = r4.A08     // Catch:{ all -> 0x0025 }
            com.facebook.messaging.model.threads.ThreadSummary r2 = r0.A01(r5)     // Catch:{ all -> 0x0025 }
            if (r2 == 0) goto L_0x001f
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = "getThreadSummaryByKey_hit"
            r1.A03(r0)     // Catch:{ all -> 0x0025 }
        L_0x001f:
            if (r3 == 0) goto L_0x0024
            r3.close()
        L_0x0024:
            return r2
        L_0x0025:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            if (r3 == 0) goto L_0x002d
            r3.close()     // Catch:{ all -> 0x002d }
        L_0x002d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0R(com.facebook.messaging.model.threadkey.ThreadKey):com.facebook.messaging.model.threads.ThreadSummary");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r4 != null) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.threads.ThreadsCollection A0S() {
        /*
            r5 = this;
            X.0mL r0 = r5.A0A
            X.0mM r4 = r0.A00()
            com.facebook.messaging.model.threads.ThreadsCollection r3 = new com.facebook.messaging.model.threads.ThreadsCollection     // Catch:{ all -> 0x0031 }
            X.0mP r1 = r5.A04     // Catch:{ all -> 0x0031 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0031 }
            r0.A01()     // Catch:{ all -> 0x0031 }
            java.util.List r0 = r1.A00     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x0026
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x0031 }
        L_0x0015:
            com.google.common.collect.ImmutableList r2 = r5.A04(r0)     // Catch:{ all -> 0x0031 }
            X.0mP r1 = r5.A04     // Catch:{ all -> 0x0031 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0031 }
            r0.A01()     // Catch:{ all -> 0x0031 }
            boolean r0 = r1.A02     // Catch:{ all -> 0x0031 }
            r3.<init>(r2, r0)     // Catch:{ all -> 0x0031 }
            goto L_0x002b
        L_0x0026:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x0031 }
            goto L_0x0015
        L_0x002b:
            if (r4 == 0) goto L_0x0030
            r4.close()
        L_0x0030:
            return r3
        L_0x0031:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r0 = move-exception
            if (r4 == 0) goto L_0x0039
            r4.close()     // Catch:{ all -> 0x0039 }
        L_0x0039:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0S():com.facebook.messaging.model.threads.ThreadsCollection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r4 != null) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.threads.ThreadsCollection A0T() {
        /*
            r5 = this;
            X.0mL r0 = r5.A0A
            X.0mM r4 = r0.A00()
            com.facebook.messaging.model.threads.ThreadsCollection r3 = new com.facebook.messaging.model.threads.ThreadsCollection     // Catch:{ all -> 0x0031 }
            X.0mP r1 = r5.A05     // Catch:{ all -> 0x0031 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0031 }
            r0.A01()     // Catch:{ all -> 0x0031 }
            java.util.List r0 = r1.A00     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x0026
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x0031 }
        L_0x0015:
            com.google.common.collect.ImmutableList r2 = r5.A04(r0)     // Catch:{ all -> 0x0031 }
            X.0mP r1 = r5.A05     // Catch:{ all -> 0x0031 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0031 }
            r0.A01()     // Catch:{ all -> 0x0031 }
            boolean r0 = r1.A02     // Catch:{ all -> 0x0031 }
            r3.<init>(r2, r0)     // Catch:{ all -> 0x0031 }
            goto L_0x002b
        L_0x0026:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x0031 }
            goto L_0x0015
        L_0x002b:
            if (r4 == 0) goto L_0x0030
            r4.close()
        L_0x0030:
            return r3
        L_0x0031:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r0 = move-exception
            if (r4 == 0) goto L_0x0039
            r4.close()     // Catch:{ all -> 0x0039 }
        L_0x0039:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0T():com.facebook.messaging.model.threads.ThreadsCollection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        if (r4 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0031, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.threads.ThreadsCollection A0U(X.C10950l8 r6) {
        /*
            r5 = this;
            X.0mL r0 = r5.A0A
            X.0mM r4 = r0.A00()
            X.0pa r3 = A00(r5, r6)     // Catch:{ all -> 0x0029 }
            com.facebook.messaging.model.threads.ThreadsCollection r2 = new com.facebook.messaging.model.threads.ThreadsCollection     // Catch:{ all -> 0x0029 }
            X.0mL r0 = r3.A06     // Catch:{ all -> 0x0029 }
            r0.A01()     // Catch:{ all -> 0x0029 }
            X.07X r0 = r3.A05     // Catch:{ all -> 0x0029 }
            java.util.List r0 = r0.A01     // Catch:{ all -> 0x0029 }
            com.google.common.collect.ImmutableList r1 = com.google.common.collect.ImmutableList.copyOf(r0)     // Catch:{ all -> 0x0029 }
            X.0mL r0 = r3.A06     // Catch:{ all -> 0x0029 }
            r0.A01()     // Catch:{ all -> 0x0029 }
            boolean r0 = r3.A02     // Catch:{ all -> 0x0029 }
            r2.<init>(r1, r0)     // Catch:{ all -> 0x0029 }
            if (r4 == 0) goto L_0x0028
            r4.close()
        L_0x0028:
            return r2
        L_0x0029:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            if (r4 == 0) goto L_0x0031
            r4.close()     // Catch:{ all -> 0x0031 }
        L_0x0031:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0U(X.0l8):com.facebook.messaging.model.threads.ThreadsCollection");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0085, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0086, code lost:
        if (r4 != null) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008b, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0V() {
        /*
            r11 = this;
            X.0mL r0 = r11.A0A
            X.0mM r4 = r0.A00()
            X.0mN r1 = r11.A08     // Catch:{ all -> 0x0083 }
            X.0mL r0 = r1.A02     // Catch:{ all -> 0x0083 }
            r0.A01()     // Catch:{ all -> 0x0083 }
            X.04b r0 = r1.A01     // Catch:{ all -> 0x0083 }
            r0.clear()     // Catch:{ all -> 0x0083 }
            X.04b r0 = r1.A00     // Catch:{ all -> 0x0083 }
            r0.clear()     // Catch:{ all -> 0x0083 }
            X.0m9 r1 = r1.A03     // Catch:{ all -> 0x0083 }
            java.lang.String r0 = "clearThreadSummariesFromCache"
            r3 = 0
            r1.A0C(r3, r0, r3)     // Catch:{ all -> 0x0083 }
            X.0mO r0 = r11.A06     // Catch:{ all -> 0x0083 }
            r0.A03()     // Catch:{ all -> 0x0083 }
            X.0mO r0 = r11.A07     // Catch:{ all -> 0x0083 }
            r0.A03()     // Catch:{ all -> 0x0083 }
            r2 = 0
            X.04b r0 = r11.A01     // Catch:{ all -> 0x0083 }
            int r1 = r0.size()     // Catch:{ all -> 0x0083 }
        L_0x0030:
            if (r2 >= r1) goto L_0x0040
            X.04b r0 = r11.A01     // Catch:{ all -> 0x0083 }
            java.lang.Object r0 = r0.A09(r2)     // Catch:{ all -> 0x0083 }
            X.0pa r0 = (X.C12560pa) r0     // Catch:{ all -> 0x0083 }
            r0.A01()     // Catch:{ all -> 0x0083 }
            int r2 = r2 + 1
            goto L_0x0030
        L_0x0040:
            X.04b r0 = r11.A01     // Catch:{ all -> 0x0083 }
            r0.clear()     // Catch:{ all -> 0x0083 }
            X.0mP r1 = r11.A04     // Catch:{ all -> 0x0083 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0083 }
            r0.A01()     // Catch:{ all -> 0x0083 }
            r1.A00 = r3     // Catch:{ all -> 0x0083 }
            r2 = 0
            r1.A01 = r2     // Catch:{ all -> 0x0083 }
            r1.A02 = r2     // Catch:{ all -> 0x0083 }
            X.0mP r1 = r11.A05     // Catch:{ all -> 0x0083 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0083 }
            r0.A01()     // Catch:{ all -> 0x0083 }
            r1.A00 = r3     // Catch:{ all -> 0x0083 }
            r1.A01 = r2     // Catch:{ all -> 0x0083 }
            r1.A02 = r2     // Catch:{ all -> 0x0083 }
            X.0m9 r2 = r11.A0B     // Catch:{ all -> 0x0083 }
            monitor-enter(r2)     // Catch:{ all -> 0x0083 }
            boolean r0 = r2.A0F()     // Catch:{ all -> 0x0080 }
            if (r0 == 0) goto L_0x0079
            r6 = 0
            r7 = 0
            java.lang.String r8 = "cacheClearAll"
            r9 = 0
            r10 = 1
            r5 = r2
            X.3bx r1 = X.AnonymousClass0m9.A02(r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0080 }
            android.util.LruCache r0 = r2.A01     // Catch:{ all -> 0x0080 }
            r0.put(r1, r1)     // Catch:{ all -> 0x0080 }
        L_0x0079:
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
            if (r4 == 0) goto L_0x007f
            r4.close()
        L_0x007f:
            return
        L_0x0080:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
            throw r0     // Catch:{ all -> 0x0083 }
        L_0x0083:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0085 }
        L_0x0085:
            r0 = move-exception
            if (r4 == 0) goto L_0x008b
            r4.close()     // Catch:{ all -> 0x008b }
        L_0x008b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0V():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        if (r3 != null) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0033, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0W(X.C10950l8 r5, com.facebook.messaging.model.folders.FolderCounts r6) {
        /*
            r4 = this;
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            X.0pa r2 = A00(r4, r5)     // Catch:{ all -> 0x002b }
            X.0mL r0 = r2.A06     // Catch:{ all -> 0x002b }
            r0.A01()     // Catch:{ all -> 0x002b }
            if (r6 != 0) goto L_0x0023
            X.0l8 r1 = r2.A07     // Catch:{ all -> 0x002b }
            X.0l8 r0 = X.C10950l8.A0B     // Catch:{ all -> 0x002b }
            if (r1 == r0) goto L_0x001e
            java.lang.String r1 = "FolderCacheData"
            java.lang.String r0 = "Passed in null folder counts!"
            X.C010708t.A0K(r1, r0)     // Catch:{ all -> 0x002b }
        L_0x001e:
            com.facebook.messaging.model.folders.FolderCounts r0 = com.facebook.messaging.model.folders.FolderCounts.A03     // Catch:{ all -> 0x002b }
            r2.A01 = r0     // Catch:{ all -> 0x002b }
            goto L_0x0025
        L_0x0023:
            r2.A01 = r6     // Catch:{ all -> 0x002b }
        L_0x0025:
            if (r3 == 0) goto L_0x002a
            r3.close()
        L_0x002a:
            return
        L_0x002b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002d }
        L_0x002d:
            r0 = move-exception
            if (r3 == 0) goto L_0x0033
            r3.close()     // Catch:{ all -> 0x0033 }
        L_0x0033:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0W(X.0l8, com.facebook.messaging.model.folders.FolderCounts):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        if (r2 != null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0X(X.C10950l8 r4, com.google.common.collect.ImmutableList r5) {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.1Xv r1 = r5.iterator()     // Catch:{ all -> 0x0026 }
        L_0x000a:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0026 }
            if (r0 == 0) goto L_0x0020
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0026 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = (com.facebook.messaging.model.threadkey.ThreadKey) r0     // Catch:{ all -> 0x0026 }
            A0A(r3, r0)     // Catch:{ all -> 0x0026 }
            A0B(r3, r0)     // Catch:{ all -> 0x0026 }
            A06(r3, r4, r0)     // Catch:{ all -> 0x0026 }
            goto L_0x000a
        L_0x0020:
            if (r2 == 0) goto L_0x0025
            r2.close()
        L_0x0025:
            return
        L_0x0026:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0028 }
        L_0x0028:
            r0 = move-exception
            if (r2 == 0) goto L_0x002e
            r2.close()     // Catch:{ all -> 0x002e }
        L_0x002e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0X(X.0l8, com.google.common.collect.ImmutableList):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        if (r1 != null) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Y(com.facebook.messaging.model.messages.Message r11, com.facebook.messaging.model.messages.MessagesCollection r12, long r13, X.C72153dk r15, java.lang.Boolean r16) {
        /*
            r10 = this;
            X.0mL r0 = r10.A0A
            X.0mM r1 = r0.A00()
            java.lang.Integer r7 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0018 }
            r2 = r10
            r4 = r12
            r3 = r11
            r5 = r13
            r9 = r16
            r8 = r15
            A07(r2, r3, r4, r5, r7, r8, r9)     // Catch:{ all -> 0x0018 }
            if (r1 == 0) goto L_0x0017
            r1.close()
        L_0x0017:
            return
        L_0x0018:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001a }
        L_0x001a:
            r0 = move-exception
            if (r1 == 0) goto L_0x0020
            r1.close()     // Catch:{ all -> 0x0020 }
        L_0x0020:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0Y(com.facebook.messaging.model.messages.Message, com.facebook.messaging.model.messages.MessagesCollection, long, X.3dk, java.lang.Boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00df, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e0, code lost:
        if (r11 != null) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00e5, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Z(com.facebook.messaging.model.messages.Message r21, boolean r22) {
        /*
            r20 = this;
            r2 = r20
            X.0mL r0 = r2.A0A
            X.0mM r11 = r0.A00()
            r13 = r21
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r13.A0U     // Catch:{ all -> 0x00dd }
            A0A(r2, r4)     // Catch:{ all -> 0x00dd }
            X.0mO r0 = r2.A06     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.messages.MessagesCollection r7 = r0.A02(r4)     // Catch:{ all -> 0x00dd }
            if (r7 == 0) goto L_0x00d7
            com.google.common.collect.ImmutableList$Builder r6 = com.google.common.collect.ImmutableList.builder()     // Catch:{ all -> 0x00dd }
            com.google.common.collect.ImmutableList r0 = r7.A01     // Catch:{ all -> 0x00dd }
            X.1Xv r10 = r0.iterator()     // Catch:{ all -> 0x00dd }
            r5 = 0
            r9 = 0
        L_0x0023:
            boolean r0 = r10.hasNext()     // Catch:{ all -> 0x00dd }
            if (r0 == 0) goto L_0x008e
            java.lang.Object r3 = r10.next()     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.messages.Message r3 = (com.facebook.messaging.model.messages.Message) r3     // Catch:{ all -> 0x00dd }
            java.lang.String r1 = r13.A0w     // Catch:{ all -> 0x00dd }
            java.lang.String r0 = r3.A0w     // Catch:{ all -> 0x00dd }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x00dd }
            if (r0 == 0) goto L_0x003e
            r6.add(r13)     // Catch:{ all -> 0x00dd }
            r9 = 1
            goto L_0x0023
        L_0x003e:
            if (r22 == 0) goto L_0x008a
            X.1V7 r1 = r3.A0D     // Catch:{ all -> 0x00dd }
            X.1V7 r0 = X.AnonymousClass1V7.A0J     // Catch:{ all -> 0x00dd }
            if (r1 != r0) goto L_0x008a
            com.facebook.messaging.model.send.PendingSendQueueKey r1 = r13.A0R     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.send.PendingSendQueueKey r0 = r3.A0R     // Catch:{ all -> 0x00dd }
            boolean r0 = com.google.common.base.Objects.equal(r1, r0)     // Catch:{ all -> 0x00dd }
            if (r0 == 0) goto L_0x008a
            X.1TG r8 = com.facebook.messaging.model.messages.Message.A00()     // Catch:{ all -> 0x00dd }
            r8.A03(r3)     // Catch:{ all -> 0x00dd }
            X.1V7 r0 = X.AnonymousClass1V7.A0A     // Catch:{ all -> 0x00dd }
            r8.A0C = r0     // Catch:{ all -> 0x00dd }
            X.9Nw r3 = new X.9Nw     // Catch:{ all -> 0x00dd }
            r3.<init>()     // Catch:{ all -> 0x00dd }
            X.1u4 r0 = X.C36891u4.A01     // Catch:{ all -> 0x00dd }
            r3.A02 = r0     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.send.SendError r1 = r13.A0S     // Catch:{ all -> 0x00dd }
            java.lang.String r0 = r1.A06     // Catch:{ all -> 0x00dd }
            r3.A06 = r0     // Catch:{ all -> 0x00dd }
            java.lang.String r0 = r1.A07     // Catch:{ all -> 0x00dd }
            r3.A07 = r0     // Catch:{ all -> 0x00dd }
            java.lang.String r0 = r1.A03     // Catch:{ all -> 0x00dd }
            r3.A03 = r0     // Catch:{ all -> 0x00dd }
            int r0 = r1.A00     // Catch:{ all -> 0x00dd }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00dd }
            r3.A00(r0)     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.send.SendError r0 = new com.facebook.messaging.model.send.SendError     // Catch:{ all -> 0x00dd }
            r0.<init>(r3)     // Catch:{ all -> 0x00dd }
            r8.A0R = r0     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.messages.Message r0 = r8.A00()     // Catch:{ all -> 0x00dd }
            r6.add(r0)     // Catch:{ all -> 0x00dd }
            goto L_0x0023
        L_0x008a:
            r6.add(r3)     // Catch:{ all -> 0x00dd }
            goto L_0x0023
        L_0x008e:
            X.1oI r1 = com.facebook.messaging.model.messages.MessagesCollection.A00(r7)     // Catch:{ all -> 0x00dd }
            com.google.common.collect.ImmutableList r0 = r6.build()     // Catch:{ all -> 0x00dd }
            r1.A01(r0)     // Catch:{ all -> 0x00dd }
            r0 = 1
            r1.A02 = r0     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.messages.MessagesCollection r3 = r1.A00()     // Catch:{ all -> 0x00dd }
            X.0mO r1 = r2.A06     // Catch:{ all -> 0x00dd }
            X.0Tq r0 = r2.A0E     // Catch:{ all -> 0x00dd }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x00dd }
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0     // Catch:{ all -> 0x00dd }
            r1.A04(r3, r0, r5, r5)     // Catch:{ all -> 0x00dd }
            if (r9 != 0) goto L_0x00be
            r14 = 0
            java.lang.Integer r17 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x00dd }
            X.3dk r18 = X.C72153dk.A02     // Catch:{ all -> 0x00dd }
            java.lang.Boolean r19 = java.lang.Boolean.valueOf(r5)     // Catch:{ all -> 0x00dd }
            r12 = r2
            r15 = -1
            A07(r12, r13, r14, r15, r17, r18, r19)     // Catch:{ all -> 0x00dd }
        L_0x00be:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.A0R(r4)     // Catch:{ all -> 0x00dd }
            if (r0 == 0) goto L_0x00d7
            X.0zh r1 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x00dd }
            r1.A02(r0)     // Catch:{ all -> 0x00dd }
            r0 = 1
            r1.A0z = r0     // Catch:{ all -> 0x00dd }
            r1.A0y = r0     // Catch:{ all -> 0x00dd }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A00()     // Catch:{ all -> 0x00dd }
            r2.A0b(r0)     // Catch:{ all -> 0x00dd }
        L_0x00d7:
            if (r11 == 0) goto L_0x00dc
            r11.close()
        L_0x00dc:
            return
        L_0x00dd:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00df }
        L_0x00df:
            r0 = move-exception
            if (r11 == 0) goto L_0x00e5
            r11.close()     // Catch:{ all -> 0x00e5 }
        L_0x00e5:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0Z(com.facebook.messaging.model.messages.Message, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005f, code lost:
        if (r7 != null) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0064, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0a(com.facebook.messaging.model.threadkey.ThreadKey r9, boolean r10) {
        /*
            r8 = this;
            java.lang.String r6 = "handleCanReplyUpdated"
            X.0mL r0 = r8.A0A
            X.0mM r7 = r0.A00()
            X.0mN r0 = r8.A08     // Catch:{ all -> 0x005c }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r0.A02(r9, r6)     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x0022
            X.0zh r0 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x005c }
            r0.A02(r1)     // Catch:{ all -> 0x005c }
            r0.A0x = r10     // Catch:{ all -> 0x005c }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r0.A00()     // Catch:{ all -> 0x005c }
            X.0mN r0 = r8.A08     // Catch:{ all -> 0x005c }
            r0.A03(r1, r6)     // Catch:{ all -> 0x005c }
        L_0x0022:
            r5 = 0
            X.04b r0 = r8.A01     // Catch:{ all -> 0x005c }
            int r4 = r0.size()     // Catch:{ all -> 0x005c }
        L_0x0029:
            if (r5 >= r4) goto L_0x0053
            X.04b r0 = r8.A01     // Catch:{ all -> 0x005c }
            java.lang.Object r3 = r0.A09(r5)     // Catch:{ all -> 0x005c }
            X.0pa r3 = (X.C12560pa) r3     // Catch:{ all -> 0x005c }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r3.A00(r9)     // Catch:{ all -> 0x005c }
            if (r1 == 0) goto L_0x0050
            X.0zh r0 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x005c }
            r0.A02(r1)     // Catch:{ all -> 0x005c }
            r0.A0x = r10     // Catch:{ all -> 0x005c }
            com.facebook.messaging.model.threads.ThreadSummary r2 = r0.A00()     // Catch:{ all -> 0x005c }
            r3.A02(r2)     // Catch:{ all -> 0x005c }
            X.0m9 r1 = r8.A0B     // Catch:{ all -> 0x005c }
            X.0l8 r0 = r3.A07     // Catch:{ all -> 0x005c }
            r1.A09(r0, r2, r6)     // Catch:{ all -> 0x005c }
        L_0x0050:
            int r5 = r5 + 1
            goto L_0x0029
        L_0x0053:
            r8.A0H(r9)     // Catch:{ all -> 0x005c }
            if (r7 == 0) goto L_0x005b
            r7.close()
        L_0x005b:
            return
        L_0x005c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
            if (r7 == 0) goto L_0x0064
            r7.close()     // Catch:{ all -> 0x0064 }
        L_0x0064:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0a(com.facebook.messaging.model.threadkey.ThreadKey, boolean):void");
    }

    public void A0b(ThreadSummary threadSummary) {
        A0I(threadSummary, threadSummary.A0O);
        if (((Boolean) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BAo, this.A00)).booleanValue() && threadSummary.A17) {
            A0I(threadSummary, C10950l8.A0B);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
        if (r5 != null) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0c(com.facebook.messaging.model.threads.ThreadSummary r7, com.facebook.messaging.model.threads.ThreadSummary r8) {
        /*
            r6 = this;
            X.0mL r0 = r6.A0A
            X.0mM r5 = r0.A00()
            if (r8 != 0) goto L_0x000e
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0S     // Catch:{ all -> 0x002e }
            com.facebook.messaging.model.threads.ThreadSummary r8 = r6.A0R(r0)     // Catch:{ all -> 0x002e }
        L_0x000e:
            if (r8 == 0) goto L_0x0025
            long r3 = r7.A07     // Catch:{ all -> 0x002e }
            long r1 = r8.A07     // Catch:{ all -> 0x002e }
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0025
            X.0zh r0 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x002e }
            r0.A02(r7)     // Catch:{ all -> 0x002e }
            r0.A06 = r1     // Catch:{ all -> 0x002e }
            com.facebook.messaging.model.threads.ThreadSummary r7 = r0.A00()     // Catch:{ all -> 0x002e }
        L_0x0025:
            r6.A0b(r7)     // Catch:{ all -> 0x002e }
            if (r5 == 0) goto L_0x002d
            r5.close()
        L_0x002d:
            return
        L_0x002e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0030 }
        L_0x0030:
            r0 = move-exception
            if (r5 == 0) goto L_0x0036
            r5.close()     // Catch:{ all -> 0x0036 }
        L_0x0036:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0c(com.facebook.messaging.model.threads.ThreadSummary, com.facebook.messaging.model.threads.ThreadSummary):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009c, code lost:
        if (r15 != null) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r15.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a1, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0d(com.facebook.messaging.service.model.MarkThreadFields r17, long r18) {
        /*
            r16 = this;
            r11 = r16
            X.0mL r0 = r11.A0A
            X.0mM r15 = r0.A00()
            r1 = r17
            com.facebook.messaging.model.threadkey.ThreadKey r10 = r1.A06     // Catch:{ all -> 0x0099 }
            A0A(r11, r10)     // Catch:{ all -> 0x0099 }
            boolean r0 = r1.A07     // Catch:{ all -> 0x0099 }
            r5 = 0
            if (r0 == 0) goto L_0x0016
            goto L_0x0019
        L_0x0016:
            r3 = 1
            goto L_0x001b
        L_0x0019:
            r3 = 0
        L_0x001b:
            if (r0 == 0) goto L_0x001f
            long r5 = r1.A04     // Catch:{ all -> 0x0099 }
        L_0x001f:
            com.facebook.messaging.model.threads.ThreadSummary r7 = r11.A0R(r10)     // Catch:{ all -> 0x0099 }
            if (r7 == 0) goto L_0x0030
            boolean r0 = r1.A07     // Catch:{ all -> 0x0099 }
            if (r0 == 0) goto L_0x0030
            long r0 = r7.A07     // Catch:{ all -> 0x0099 }
            int r2 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r2 >= 0) goto L_0x0030
            goto L_0x0093
        L_0x0030:
            r13 = -1
            if (r7 == 0) goto L_0x0052
            X.0zh r2 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x0099 }
            r2.A02(r7)     // Catch:{ all -> 0x0099 }
            r2.A0B = r3     // Catch:{ all -> 0x0099 }
            r2.A06 = r5     // Catch:{ all -> 0x0099 }
            int r0 = (r18 > r13 ? 1 : (r18 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x0046
            r0 = r18
        L_0x0045:
            goto L_0x0049
        L_0x0046:
            long r0 = r7.A09     // Catch:{ all -> 0x0099 }
            goto L_0x0045
        L_0x0049:
            r2.A08 = r0     // Catch:{ all -> 0x0099 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.A00()     // Catch:{ all -> 0x0099 }
            r11.A0b(r0)     // Catch:{ all -> 0x0099 }
        L_0x0052:
            r9 = 0
            X.04b r0 = r11.A01     // Catch:{ all -> 0x0099 }
            int r8 = r0.size()     // Catch:{ all -> 0x0099 }
        L_0x0059:
            if (r9 >= r8) goto L_0x0093
            X.04b r0 = r11.A01     // Catch:{ all -> 0x0099 }
            java.lang.Object r12 = r0.A09(r9)     // Catch:{ all -> 0x0099 }
            X.0pa r12 = (X.C12560pa) r12     // Catch:{ all -> 0x0099 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r12.A00(r10)     // Catch:{ all -> 0x0099 }
            if (r1 == 0) goto L_0x0090
            X.0zh r2 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x0099 }
            r2.A02(r1)     // Catch:{ all -> 0x0099 }
            r2.A0B = r3     // Catch:{ all -> 0x0099 }
            r2.A06 = r5     // Catch:{ all -> 0x0099 }
            int r0 = (r18 > r13 ? 1 : (r18 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x007b
            r0 = r18
        L_0x007a:
            goto L_0x007e
        L_0x007b:
            long r0 = r1.A09     // Catch:{ all -> 0x0099 }
            goto L_0x007a
        L_0x007e:
            r2.A08 = r0     // Catch:{ all -> 0x0099 }
            com.facebook.messaging.model.threads.ThreadSummary r7 = r2.A00()     // Catch:{ all -> 0x0099 }
            r12.A02(r7)     // Catch:{ all -> 0x0099 }
            X.0m9 r2 = r11.A0B     // Catch:{ all -> 0x0099 }
            X.0l8 r1 = r12.A07     // Catch:{ all -> 0x0099 }
            java.lang.String r0 = "noteNewReadState"
            r2.A09(r1, r7, r0)     // Catch:{ all -> 0x0099 }
        L_0x0090:
            int r9 = r9 + 1
            goto L_0x0059
        L_0x0093:
            if (r15 == 0) goto L_0x0098
            r15.close()
        L_0x0098:
            return
        L_0x0099:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x009b }
        L_0x009b:
            r0 = move-exception
            if (r15 == 0) goto L_0x00a1
            r15.close()     // Catch:{ all -> 0x00a1 }
        L_0x00a1:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0d(com.facebook.messaging.service.model.MarkThreadFields, long):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r2 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0e() {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0mP r1 = r3.A04     // Catch:{ all -> 0x0019 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0019 }
            r0.A01()     // Catch:{ all -> 0x0019 }
            java.util.List r1 = r1.A00     // Catch:{ all -> 0x0019 }
            r0 = 0
            if (r1 == 0) goto L_0x0013
            r0 = 1
        L_0x0013:
            if (r2 == 0) goto L_0x0018
            r2.close()
        L_0x0018:
            return r0
        L_0x0019:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001b }
        L_0x001b:
            r0 = move-exception
            if (r2 == 0) goto L_0x0021
            r2.close()     // Catch:{ all -> 0x0021 }
        L_0x0021:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0e():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r2 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0f() {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0mP r1 = r3.A04     // Catch:{ all -> 0x0015 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0015 }
            r0.A01()     // Catch:{ all -> 0x0015 }
            boolean r0 = r1.A01     // Catch:{ all -> 0x0015 }
            if (r2 == 0) goto L_0x0014
            r2.close()
        L_0x0014:
            return r0
        L_0x0015:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0017 }
        L_0x0017:
            r0 = move-exception
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ all -> 0x001d }
        L_0x001d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0f():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r2 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0g() {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0mP r1 = r3.A05     // Catch:{ all -> 0x0019 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0019 }
            r0.A01()     // Catch:{ all -> 0x0019 }
            java.util.List r1 = r1.A00     // Catch:{ all -> 0x0019 }
            r0 = 0
            if (r1 == 0) goto L_0x0013
            r0 = 1
        L_0x0013:
            if (r2 == 0) goto L_0x0018
            r2.close()
        L_0x0018:
            return r0
        L_0x0019:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001b }
        L_0x001b:
            r0 = move-exception
            if (r2 == 0) goto L_0x0021
            r2.close()     // Catch:{ all -> 0x0021 }
        L_0x0021:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0g():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r2 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0h() {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0mP r1 = r3.A05     // Catch:{ all -> 0x0015 }
            X.0mL r0 = r1.A03     // Catch:{ all -> 0x0015 }
            r0.A01()     // Catch:{ all -> 0x0015 }
            boolean r0 = r1.A01     // Catch:{ all -> 0x0015 }
            if (r2 == 0) goto L_0x0014
            r2.close()
        L_0x0014:
            return r0
        L_0x0015:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0017 }
        L_0x0017:
            r0 = move-exception
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ all -> 0x001d }
        L_0x001d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0h():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (r2 != null) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0i(X.C10950l8 r4) {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0pa r1 = A00(r3, r4)     // Catch:{ all -> 0x0017 }
            X.0mL r0 = r1.A06     // Catch:{ all -> 0x0017 }
            r0.A01()     // Catch:{ all -> 0x0017 }
            boolean r0 = r1.A03     // Catch:{ all -> 0x0017 }
            if (r2 == 0) goto L_0x0016
            r2.close()
        L_0x0016:
            return r0
        L_0x0017:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0019 }
        L_0x0019:
            r0 = move-exception
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ all -> 0x001f }
        L_0x001f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0i(X.0l8):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (r2 != null) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        r0 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0j(X.C10950l8 r4) {
        /*
            r3 = this;
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            X.0pa r1 = A00(r3, r4)     // Catch:{ all -> 0x0017 }
            X.0mL r0 = r1.A06     // Catch:{ all -> 0x0017 }
            r0.A01()     // Catch:{ all -> 0x0017 }
            boolean r0 = r1.A04     // Catch:{ all -> 0x0017 }
            if (r2 == 0) goto L_0x0016
            r2.close()
        L_0x0016:
            return r0
        L_0x0017:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0019 }
        L_0x0019:
            r0 = move-exception
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ all -> 0x001f }
        L_0x001f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0j(X.0l8):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x005d, code lost:
        if (r5 != null) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0062, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0k(com.facebook.messaging.model.threadkey.ThreadKey r7, int r8) {
        /*
            r6 = this;
            X.0mL r0 = r6.A0A
            X.0mM r5 = r0.A00()
            A0A(r6, r7)     // Catch:{ all -> 0x005a }
            X.0mN r0 = r6.A08     // Catch:{ all -> 0x005a }
            com.facebook.messaging.model.threads.ThreadSummary r4 = r0.A01(r7)     // Catch:{ all -> 0x005a }
            r3 = 0
            if (r8 != 0) goto L_0x0013
            goto L_0x0051
        L_0x0013:
            X.0mN r0 = r6.A08     // Catch:{ all -> 0x005a }
            X.1oR r0 = r0.A00(r7)     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0020
            boolean r0 = r0.A00     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0020
            goto L_0x0054
        L_0x0020:
            X.0mO r0 = r6.A06     // Catch:{ all -> 0x005a }
            com.facebook.messaging.model.messages.MessagesCollection r2 = r0.A02(r7)     // Catch:{ all -> 0x005a }
            if (r2 == 0) goto L_0x0054
            if (r4 == 0) goto L_0x0054
            X.0l8 r1 = r4.A0O     // Catch:{ all -> 0x005a }
            X.0l8 r0 = X.C10950l8.A06     // Catch:{ all -> 0x005a }
            if (r1 != r0) goto L_0x0047
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r4.A0X     // Catch:{ all -> 0x005a }
            if (r0 != 0) goto L_0x0047
            int r1 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x005a }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x005a }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x005a }
            X.1TU r1 = (X.AnonymousClass1TU) r1     // Catch:{ all -> 0x005a }
            com.google.common.collect.ImmutableList r0 = r2.A01     // Catch:{ all -> 0x005a }
            com.facebook.messaging.model.messages.Message r0 = r1.A0E(r0, r3)     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0047
            goto L_0x0054
        L_0x0047:
            boolean r0 = r2.A09(r8)     // Catch:{ all -> 0x005a }
            if (r5 == 0) goto L_0x0050
            r5.close()
        L_0x0050:
            return r0
        L_0x0051:
            if (r4 == 0) goto L_0x0054
            r3 = 1
        L_0x0054:
            if (r5 == 0) goto L_0x0059
            r5.close()
        L_0x0059:
            return r3
        L_0x005a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005c }
        L_0x005c:
            r0 = move-exception
            if (r5 == 0) goto L_0x0062
            r5.close()     // Catch:{ all -> 0x0062 }
        L_0x0062:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A0k(com.facebook.messaging.model.threadkey.ThreadKey, int):boolean");
    }

    public AnonymousClass0m6(AnonymousClass1XY r3, AnonymousClass0m8 r4, AnonymousClass0m9 r5) {
        this.A00 = new AnonymousClass0UN(6, r3);
        this.A0E = AnonymousClass0WY.A01(r3);
        this.A0D = AnonymousClass0VG.A00(AnonymousClass1Y3.BKW, r3);
        this.A0F = C10580kT.A04(r3);
        this.A03 = AnonymousClass0WA.A00(r3);
        this.A02 = C07380dK.A00(r3);
        this.A0C = AnonymousClass0mB.A01(r3);
        C11180mE.A02(r3);
        this.A09 = r4;
        AnonymousClass0mL r1 = new AnonymousClass0mL();
        this.A0A = r1;
        this.A08 = new AnonymousClass0mN(r1, r5);
        this.A06 = new C11210mO(r1, r5);
        this.A07 = new C11210mO(r1, r5);
        this.A0B = r5;
        this.A04 = new C11220mP(r1);
        this.A05 = new C11220mP(r1);
    }

    private ImmutableList A04(ImmutableList immutableList) {
        ImmutableList.Builder builder = ImmutableList.builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            ThreadSummary A0R = A0R((ThreadKey) it.next());
            if (A0R != null) {
                builder.add((Object) A0R);
            }
        }
        return builder.build();
    }

    public static void A0A(AnonymousClass0m6 r2, ThreadKey threadKey) {
        AnonymousClass0m8 r0;
        if (ThreadKey.A0E(threadKey)) {
            r0 = AnonymousClass0m8.SMS;
        } else if (ThreadKey.A0F(threadKey)) {
            r0 = AnonymousClass0m8.TINCAN;
        } else if (threadKey != null) {
            r0 = AnonymousClass0m8.FACEBOOK;
        } else {
            return;
        }
        AnonymousClass0m8 r22 = r2.A09;
        boolean z = false;
        if (r22 == r0) {
            z = true;
        }
        Preconditions.checkArgument(z, "Tried to use %s in %s cache", threadKey, r22);
    }

    public static void A0C(AnonymousClass0m6 r4, ThreadKey threadKey, Set set, MessagesCollection messagesCollection, C11210mO r8) {
        A0A(r4, threadKey);
        if (messagesCollection != null) {
            ImmutableList.Builder builder = ImmutableList.builder();
            C24971Xv it = messagesCollection.A01.iterator();
            while (it.hasNext()) {
                Message message = (Message) it.next();
                if (!set.contains(message.A0q)) {
                    builder.add((Object) message);
                }
            }
            C33881oI A002 = MessagesCollection.A00(messagesCollection);
            A002.A01(builder.build());
            A002.A02 = true;
            r8.A04(A002.A00(), (User) r4.A0E.get(), true, false);
            AnonymousClass0m9 r3 = r4.A0B;
            String str = "messageIdsToRemove: " + set;
            synchronized (r3) {
                if (r3.A0F() && AnonymousClass0m9.A05(threadKey)) {
                    C71183bx A012 = AnonymousClass0m9.A01(r3, threadKey, null, "updateAfterDeletedMessages", str);
                    r3.A01.put(A012, A012);
                }
            }
        }
    }

    private void A0H(ThreadKey threadKey) {
        if (threadKey.A0L() || threadKey.A0O()) {
            C11220mP r1 = this.A04;
            r1.A03.A01();
            r1.A01 = false;
            C11220mP r12 = this.A05;
            r12.A03.A01();
            r12.A01 = false;
        }
    }

    private void A0K(ImmutableList immutableList) {
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            A0A(this, ((ThreadSummary) it.next()).A0S);
        }
        AnonymousClass0mN r3 = this.A08;
        Iterator it2 = immutableList.iterator();
        while (it2.hasNext()) {
            r3.A03((ThreadSummary) it2.next(), "addGroupThreads");
        }
    }

    public void clearUserData() {
        A0V();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0086, code lost:
        if (r3.A03.AbO(202, false) == false) goto L_0x0088;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A07(X.AnonymousClass0m6 r14, com.facebook.messaging.model.messages.Message r15, com.facebook.messaging.model.messages.MessagesCollection r16, long r17, java.lang.Integer r19, X.C72153dk r20, java.lang.Boolean r21) {
        /*
            r12 = r15
            if (r15 != 0) goto L_0x0004
            return
        L_0x0004:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r15.A0U
            r3 = r14
            A0A(r14, r0)
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r15.A0U
            X.0mN r0 = r14.A08
            X.1oR r5 = r0.A00(r2)
            X.0mN r0 = r14.A08
            com.facebook.messaging.model.threads.ThreadSummary r11 = r0.A01(r2)
            if (r11 != 0) goto L_0x0028
            A05(r14)
            r0 = 1
            r5.A00 = r0
            X.0m9 r1 = r14.A0B
            java.lang.String r0 = "NullThreadSummary"
            r1.A0B(r2, r15, r0)
            return
        L_0x0028:
            com.facebook.messaging.model.messages.Publicity r1 = com.facebook.messaging.model.messages.Publicity.A02
            com.facebook.messaging.model.messages.Publicity r0 = r15.A0L
            boolean r7 = r1.equals(r0)
            X.0mO r0 = r14.A06
            com.facebook.messaging.model.messages.MessagesCollection r4 = r0.A02(r2)
            if (r4 != 0) goto L_0x01a8
            X.1oI r1 = new X.1oI
            r1.<init>()
            r1.A00 = r2
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            r1.A01(r0)
            r0 = 0
            r1.A03 = r0
            r0 = 1
            r1.A02 = r0
            com.facebook.messaging.model.messages.MessagesCollection r4 = r1.A00()
            A05(r14)
            r5.A00 = r0
            X.0m9 r1 = r14.A0B
            java.lang.String r0 = "NoMessages"
            r1.A0B(r2, r15, r0)
        L_0x005a:
            r6 = r16
            r8 = 0
            r14 = r17
            if (r7 != 0) goto L_0x01a5
            boolean r0 = r12.A14
            if (r0 != 0) goto L_0x01a5
            long r0 = r11.A09
            r9 = 1
            long r0 = r0 + r9
            int r7 = (r17 > r0 ? 1 : (r17 == r0 ? 0 : -1))
            if (r7 == 0) goto L_0x01a5
            X.0Tq r0 = r3.A0D
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r7 = 1
            if (r0 == 0) goto L_0x0088
            X.1YI r1 = r3.A03
            r0 = 202(0xca, float:2.83E-43)
            boolean r1 = r1.AbO(r0, r8)
            r0 = 1
            if (r1 != 0) goto L_0x0089
        L_0x0088:
            r0 = 0
        L_0x0089:
            if (r0 != 0) goto L_0x01a5
            if (r16 == 0) goto L_0x009d
            int r1 = X.AnonymousClass1Y3.BFT
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.0mB r0 = (X.AnonymousClass0mB) r0
            boolean r0 = r0.A02(r6, r4)
            if (r0 != 0) goto L_0x01a5
        L_0x009d:
            r0 = 1
        L_0x009e:
            if (r0 == 0) goto L_0x00a6
            A05(r3)
            r0 = 1
            r5.A00 = r0
        L_0x00a6:
            r7 = r6
            if (r16 != 0) goto L_0x00ad
            com.facebook.messaging.model.messages.MessagesCollection r7 = com.facebook.messaging.model.messages.MessagesCollection.A01(r12)
        L_0x00ad:
            r5 = 1
            int r1 = X.AnonymousClass1Y3.BFT
            X.0UN r0 = r3.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r1, r0)
            X.0mB r1 = (X.AnonymousClass0mB) r1
            com.facebook.messaging.model.messages.MessagesCollection r13 = X.AnonymousClass0mB.A00(r1, r7, r4, r8)
            r0 = 3
            boolean r0 = X.C010708t.A0X(r0)
            if (r0 == 0) goto L_0x010a
            X.0Tq r0 = r3.A0E
            java.lang.Object r0 = r0.get()
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0
            if (r0 == 0) goto L_0x010a
            boolean r0 = r0.A14
            if (r0 == 0) goto L_0x010a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            X.0m8 r0 = r3.A09
            java.lang.String r0 = r0.logName
            r5.append(r0)
            java.lang.String r0 = " Merged messages:\n"
            r5.append(r0)
            java.lang.String r0 = "  New Message:\n"
            r5.append(r0)
            com.facebook.messaging.model.messages.MessagesCollection r1 = com.facebook.messaging.model.messages.MessagesCollection.A01(r12)
            r0 = 1
            A0L(r5, r1, r0)
            java.lang.String r0 = "  Recent Messages:\n"
            r5.append(r0)
            r1 = 5
            A0L(r5, r6, r1)
            java.lang.String r0 = "  Loaded Messages:\n"
            r5.append(r0)
            A0L(r5, r4, r1)
            java.lang.String r0 = "  Result:\n"
            r5.append(r0)
            r0 = 8
            A0L(r5, r13, r0)
        L_0x010a:
            r4 = 2
            int r1 = X.AnonymousClass1Y3.AOk
            X.0UN r0 = r3.A00
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r4, r1, r0)
            X.2sw r10 = (X.C57972sw) r10
            r17 = r20
            r18 = r21
            r16 = r19
            com.facebook.messaging.model.threads.ThreadSummary r1 = r10.A06(r11, r12, r13, r14, r16, r17, r18)
            X.0l8 r0 = r1.A0O
            X.0pa r5 = A00(r3, r0)
            com.facebook.messaging.model.threads.ThreadSummary r6 = A03(r3, r1, r13)
            X.0mN r1 = r3.A08
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r11.A0S
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A01(r0)
            r3.A0J(r6, r0)
            X.0mN r1 = r3.A08
            java.lang.String r0 = "stitchInMessages"
            r1.A03(r6, r0)
            r5.A03(r6)
            X.0mO r1 = r3.A06
            X.0Tq r0 = r3.A0E
            java.lang.Object r0 = r0.get()
            com.facebook.user.model.User r0 = (com.facebook.user.model.User) r0
            r1.A04(r13, r0, r8, r8)
            com.google.common.base.Preconditions.checkNotNull(r12)
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r12.A0U
            if (r1 == 0) goto L_0x0183
            java.lang.String r0 = r12.A0q
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r0)
            if (r0 != 0) goto L_0x0183
            X.0mO r0 = r3.A07
            com.facebook.messaging.model.messages.MessagesCollection r7 = r0.A02(r1)
            if (r7 == 0) goto L_0x0183
            X.0mO r1 = r3.A07
            java.lang.String r0 = r12.A0q
            com.facebook.messaging.model.messages.Message r0 = r1.A01(r0)
            if (r0 == 0) goto L_0x0183
            X.0mO r5 = r3.A07
            X.0mB r4 = r3.A0C
            com.facebook.messaging.model.messages.MessagesCollection r1 = com.facebook.messaging.model.messages.MessagesCollection.A01(r12)
            com.facebook.messaging.model.messages.MessagesCollection r4 = X.AnonymousClass0mB.A00(r4, r1, r7, r8)
            X.0Tq r0 = r3.A0E
            java.lang.Object r1 = r0.get()
            com.facebook.user.model.User r1 = (com.facebook.user.model.User) r1
            r5.A04(r4, r1, r8, r8)
        L_0x0183:
            r4 = 5
            int r1 = X.AnonymousClass1Y3.BAo
            X.0UN r0 = r3.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x01a1
            boolean r0 = r6.A17
            if (r0 == 0) goto L_0x01a1
            X.0l8 r0 = X.C10950l8.A0B
            X.0pa r0 = A00(r3, r0)
            r0.A03(r6)
        L_0x01a1:
            r3.A0H(r2)
            return
        L_0x01a5:
            r0 = 0
            goto L_0x009e
        L_0x01a8:
            boolean r0 = r15.A14
            if (r0 == 0) goto L_0x005a
            if (r7 != 0) goto L_0x005a
            A05(r14)
            r0 = 1
            r5.A00 = r0
            X.0m9 r1 = r14.A0B
            java.lang.String r0 = "NonAuthoritativeMessage"
            r1.A0B(r2, r15, r0)
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m6.A07(X.0m6, com.facebook.messaging.model.messages.Message, com.facebook.messaging.model.messages.MessagesCollection, long, java.lang.Integer, X.3dk, java.lang.Boolean):void");
    }
}
