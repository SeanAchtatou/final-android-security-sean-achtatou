package X;

import android.util.JsonReader;
import android.util.JsonToken;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0GQ  reason: invalid class name */
public final class AnonymousClass0GQ {
    public static List A00(JsonReader jsonReader) {
        ArrayList arrayList = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext() && jsonReader.peek() == JsonToken.BEGIN_OBJECT) {
            jsonReader.beginObject();
            String str = null;
            String str2 = null;
            String str3 = null;
            String str4 = null;
            String str5 = null;
            while (jsonReader.hasNext()) {
                String nextName = jsonReader.nextName();
                char c = 65535;
                switch (nextName.hashCode()) {
                    case AnonymousClass1Y3.ARf /*3355*/:
                        if (nextName.equals("id")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 3195150:
                        if (nextName.equals("hash")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 3373707:
                        if (nextName.equals("name")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 3433509:
                        if (nextName.equals("path")) {
                            c = 4;
                            break;
                        }
                        break;
                    case 1109408053:
                        if (nextName.equals("download_uri")) {
                            c = 3;
                            break;
                        }
                        break;
                }
                if (c == 0) {
                    A01(str3, nextName);
                    str3 = jsonReader.nextString();
                } else if (c == 1) {
                    A01(str, nextName);
                    str = jsonReader.nextString();
                } else if (c == 2) {
                    A01(str2, nextName);
                    str2 = jsonReader.nextString();
                } else if (c == 3) {
                    A01(str4, nextName);
                    str4 = jsonReader.nextString();
                } else if (c == 4) {
                    A01(str5, nextName);
                    str5 = jsonReader.nextString();
                } else {
                    throw new RuntimeException(AnonymousClass08S.A0J("Unexpected name: ", nextName));
                }
            }
            jsonReader.endObject();
            arrayList.add(new AnonymousClass0GR(str, str2, str3, str4, str5));
        }
        jsonReader.endArray();
        return arrayList;
    }

    private static void A01(Object obj, String str) {
        if (obj != null) {
            throw new RuntimeException(AnonymousClass08S.A0J("Duplicate value for name ", str));
        }
    }
}
