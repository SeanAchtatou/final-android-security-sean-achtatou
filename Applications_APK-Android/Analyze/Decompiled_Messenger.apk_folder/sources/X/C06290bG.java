package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0bG  reason: invalid class name and case insensitive filesystem */
public final class C06290bG {
    private static volatile C06290bG A01;
    public final AnonymousClass00M A00;

    public static final C06290bG A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C06290bG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C06290bG(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C06290bG(AnonymousClass1XY r2) {
        this.A00 = C04900Wt.A00(r2);
    }
}
