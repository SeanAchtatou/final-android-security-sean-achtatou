package X;

import com.google.common.base.Preconditions;
import com.google.common.collect.AbstractMapBasedMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.LinkedListMultimap;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedMap;

/* renamed from: X.0Uz  reason: invalid class name and case insensitive filesystem */
public abstract class C04510Uz implements AnonymousClass0V0 {
    private transient AnonymousClass0UG A00;
    private transient Collection A01;
    private transient Collection A02;
    private transient Map A03;
    private transient Set A04;

    public Map A07() {
        if (this instanceof LinkedListMultimap) {
            return new C21121Fh((LinkedListMultimap) this);
        }
        if (!(this instanceof ImmutableMultimap)) {
            AbstractMapBasedMultimap abstractMapBasedMultimap = (AbstractMapBasedMultimap) this;
            Map map = abstractMapBasedMultimap.A01;
            return map instanceof NavigableMap ? new C29786EiY(abstractMapBasedMultimap, (NavigableMap) map) : map instanceof SortedMap ? new C29787EiZ(abstractMapBasedMultimap, (SortedMap) map) : new C409323m(abstractMapBasedMultimap, map);
        }
        throw new AssertionError("should never be called");
    }

    public Iterator A08() {
        if (!(this instanceof LinkedListMultimap)) {
            return !(this instanceof ImmutableMultimap) ? new AnonymousClass2V4((AbstractMapBasedMultimap) this) : new C80623so((ImmutableMultimap) this);
        }
        throw new AssertionError("should never be called");
    }

    public Set A02() {
        if (this instanceof LinkedListMultimap) {
            return new B8P((LinkedListMultimap) this);
        }
        if (!(this instanceof AbstractMapBasedMultimap)) {
            return new C33131n4(AOp());
        }
        AbstractMapBasedMultimap abstractMapBasedMultimap = (AbstractMapBasedMultimap) this;
        Map map = abstractMapBasedMultimap.A01;
        if (map instanceof NavigableMap) {
            return new C29785EiX(abstractMapBasedMultimap, (NavigableMap) map);
        }
        if (map instanceof SortedMap) {
            return new C29788Eia(abstractMapBasedMultimap, (SortedMap) map);
        }
        return new C21141Fj(abstractMapBasedMultimap, map);
    }

    public boolean A03(AnonymousClass0V0 r5) {
        if (!(this instanceof ImmutableMultimap)) {
            boolean z = false;
            for (Map.Entry entry : r5.AYj()) {
                z |= Byx(entry.getKey(), entry.getValue());
            }
            return z;
        }
        throw new UnsupportedOperationException();
    }

    public AnonymousClass0UG A04() {
        if (!(this instanceof ImmutableMultimap)) {
            return new C75113jA(this);
        }
        return new ImmutableMultimap.Keys();
    }

    public Collection A05() {
        if (this instanceof LinkedListMultimap) {
            return new B8X((LinkedListMultimap) this);
        }
        if (this instanceof ImmutableMultimap) {
            return new ImmutableMultimap.EntryCollection((ImmutableMultimap) this);
        }
        if (this instanceof C30141hY) {
            return new AnonymousClass2V1(this);
        }
        return new AnonymousClass2V2(this);
    }

    public Collection A06() {
        if (this instanceof LinkedListMultimap) {
            return new B8Y((LinkedListMultimap) this);
        }
        if (!(this instanceof ImmutableMultimap)) {
            return new C50372ds(this);
        }
        return new ImmutableMultimap.Values((ImmutableMultimap) this);
    }

    public Iterator A09() {
        if (this instanceof ImmutableMultimap) {
            return new C42992Cq((ImmutableMultimap) this);
        }
        if (!(this instanceof AbstractMapBasedMultimap)) {
            return AnonymousClass0TG.A05(AYj().iterator());
        }
        return new C50382dt((AbstractMapBasedMultimap) this);
    }

    public Map AOp() {
        if (this instanceof ImmutableMultimap) {
            return ((ImmutableMultimap) this).A01;
        }
        Map map = this.A03;
        if (map != null) {
            return map;
        }
        Map A07 = A07();
        this.A03 = A07;
        return A07;
    }

    public Collection AYj() {
        Collection collection = this.A01;
        if (collection != null) {
            return collection;
        }
        Collection A05 = A05();
        this.A01 = A05;
        return A05;
    }

    public AnonymousClass0UG BHh() {
        AnonymousClass0UG r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0UG A042 = A04();
        this.A00 = A042;
        return A042;
    }

    public boolean Byx(Object obj, Object obj2) {
        if (this instanceof LinkedListMultimap) {
            LinkedListMultimap.A00((LinkedListMultimap) this, obj, obj2, null);
            return true;
        } else if (this instanceof ImmutableMultimap) {
            throw new UnsupportedOperationException();
        } else if (!(this instanceof AbstractMapBasedMultimap)) {
            return AbK(obj).add(obj2);
        } else {
            AbstractMapBasedMultimap abstractMapBasedMultimap = (AbstractMapBasedMultimap) this;
            Collection collection = (Collection) abstractMapBasedMultimap.A01.get(obj);
            if (collection == null) {
                Collection A0C = abstractMapBasedMultimap.A0C(obj);
                if (A0C.add(obj2)) {
                    abstractMapBasedMultimap.A00++;
                    abstractMapBasedMultimap.A01.put(obj, A0C);
                    return true;
                }
                throw new AssertionError("New Collection violated the Collection spec");
            } else if (!collection.add(obj2)) {
                return false;
            } else {
                abstractMapBasedMultimap.A00++;
                return true;
            }
        }
    }

    public boolean Byz(Object obj, Iterable iterable) {
        if (!(this instanceof ImmutableMultimap)) {
            Preconditions.checkNotNull(iterable);
            if (iterable instanceof Collection) {
                Collection collection = (Collection) iterable;
                if (collection.isEmpty() || !AbK(obj).addAll(collection)) {
                    return false;
                }
                return true;
            }
            Iterator it = iterable.iterator();
            if (!it.hasNext() || !C24931Xr.A09(AbK(obj), it)) {
                return false;
            }
            return true;
        }
        throw new UnsupportedOperationException();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AnonymousClass0V0) {
            return AOp().equals(((AnonymousClass0V0) obj).AOp());
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isEmpty() {
        /*
            r2 = this;
            boolean r0 = r2 instanceof com.google.common.collect.LinkedListMultimap
            if (r0 != 0) goto L_0x000d
            int r1 = r2.size()
            r0 = 0
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            r0 = 1
        L_0x000c:
            return r0
        L_0x000d:
            r0 = r2
            com.google.common.collect.LinkedListMultimap r0 = (com.google.common.collect.LinkedListMultimap) r0
            X.B8h r1 = r0.A02
            r0 = 0
            if (r1 != 0) goto L_0x000c
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C04510Uz.isEmpty():boolean");
    }

    public Set keySet() {
        if (this instanceof ImmutableMultimap) {
            return ((ImmutableMultimap) this).A01.keySet();
        }
        Set set = this.A04;
        if (set != null) {
            return set;
        }
        Set A022 = A02();
        this.A04 = A022;
        return A022;
    }

    public boolean remove(Object obj, Object obj2) {
        if (!(this instanceof ImmutableMultimap)) {
            Collection collection = (Collection) AOp().get(obj);
            if (collection == null || !collection.remove(obj2)) {
                return false;
            }
            return true;
        }
        throw new UnsupportedOperationException();
    }

    public Collection values() {
        Collection collection = this.A02;
        if (collection != null) {
            return collection;
        }
        Collection A06 = A06();
        this.A02 = A06;
        return A06;
    }

    public boolean AUB(Object obj, Object obj2) {
        Collection collection = (Collection) AOp().get(obj);
        if (collection == null || !collection.contains(obj2)) {
            return false;
        }
        return true;
    }

    public Collection C2O(Object obj, Iterable iterable) {
        Preconditions.checkNotNull(iterable);
        Collection C1N = C1N(obj);
        Byz(obj, iterable);
        return C1N;
    }

    public boolean containsValue(Object obj) {
        for (Collection contains : AOp().values()) {
            if (contains.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return AOp().hashCode();
    }

    public String toString() {
        return AOp().toString();
    }
}
