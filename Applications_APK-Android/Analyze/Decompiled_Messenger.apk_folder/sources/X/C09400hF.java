package X;

/* renamed from: X.0hF  reason: invalid class name and case insensitive filesystem */
public abstract class C09400hF {
    public int A00() {
        return 1;
    }

    public int A01() {
        return ((AnonymousClass0hG) this).A00.versionCode;
    }

    public String A02() {
        return ((AnonymousClass0hG) this).A00.versionName;
    }

    public boolean A03() {
        if (A00() == A01()) {
            return true;
        }
        return false;
    }
}
