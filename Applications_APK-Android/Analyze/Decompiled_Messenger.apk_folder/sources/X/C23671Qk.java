package X;

import com.facebook.acra.ACRA;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.google.common.collect.ImmutableList;
import java.util.HashMap;

/* renamed from: X.1Qk  reason: invalid class name and case insensitive filesystem */
public final class C23671Qk extends C20831Dz {
    public GSTModelShape1S0000000 A00;
    public C84623zm A01;
    public ImmutableList A02;
    public String A03;
    public String A04;
    public HashMap A05 = new HashMap();
    public HashMap A06 = new HashMap();
    public boolean A07 = false;
    public boolean A08 = false;
    public boolean A09 = false;

    public static int A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return 6;
            default:
                return 0;
        }
    }

    public static final C23671Qk A01(AnonymousClass1XY r1) {
        return new C23671Qk(r1);
    }

    public int ArU() {
        return this.A02.size() + (this.A08 ? 1 : 0) + (this.A09 ? 1 : 0);
    }

    public C23671Qk(AnonymousClass1XY r3) {
        this.A01 = new C84623zm(r3);
    }
}
