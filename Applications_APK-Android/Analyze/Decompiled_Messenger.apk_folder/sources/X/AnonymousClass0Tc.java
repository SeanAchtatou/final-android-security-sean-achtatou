package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.fragment.app.Fragment;
import com.facebook.bugreporter.activity.chooser.ChooserFragment;
import com.facebook.bugreporter.activity.chooser.ChooserOption;
import com.facebook.common.util.TriState;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.google.common.base.Platform;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Tc  reason: invalid class name */
public final class AnonymousClass0Tc {
    public static C04240Tb A04;
    public static final String A05 = AnonymousClass08S.A0J("BugReporter", ".");
    private static volatile AnonymousClass0Tc A06;
    public AnonymousClass0UN A00;
    public Map A01;
    private String A02;
    public final C06330bK A03 = new C06330bK();

    public static ListenableFuture A01(AnonymousClass0Tc r11, Context context, int i, Callable callable) {
        String str = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(context.getPackageName(), 0));
            if (applicationLabel != null) {
                str = applicationLabel.toString();
            }
        } catch (PackageManager.NameNotFoundException e) {
            C010708t.A0J("BugReporter", e.getMessage());
        }
        if (Platform.stringIsNullOrEmpty(str)) {
            str = context.getString(2131822240);
        }
        String string = ((Resources) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AEg, r11.A00)).getString(i);
        ListenableFuture CIF = ((AnonymousClass0VL) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AZx, r11.A00)).CIF(callable);
        C121675od A002 = C121675od.A00(string, -1, true, false, false, false);
        Dialog dialog = A002.A09;
        if (dialog != null) {
            dialog.setTitle(str);
        }
        A002.A0G.putString("title", str.toString());
        A002.A1V(true);
        A002.A01 = new C134746Rx(r11, CIF);
        if (!(context instanceof Activity)) {
            int A003 = AnonymousClass8V7.A00(2002);
            Dialog dialog2 = A002.A09;
            if (dialog2 != null) {
                dialog2.getWindow().setType(A003);
            } else {
                A002.A0G.putInt("window_type", A003);
            }
        }
        if (context instanceof C11450nB) {
            A002.A27(((C11450nB) context).B4m().A0T(), "bug_report_in_progress", true);
        }
        C05350Yp.A08(CIF, new C123085r1(context, A002), (Executor) AnonymousClass1XX.A02(2, AnonymousClass1Y3.A6Z, r11.A00));
        A002.A26(true);
        return CIF;
    }

    public static final AnonymousClass0Tc A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (AnonymousClass0Tc.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new AnonymousClass0Tc(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static List A03(List list) {
        StrictMode.ThreadPolicy threadPolicy;
        ArrayList arrayList = new ArrayList();
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (Build.VERSION.SDK_INT >= 9) {
            threadPolicy = StrictMode.allowThreadDiskReads();
        } else {
            threadPolicy = null;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(BitmapFactory.decodeFile(((Uri) it.next()).getPath(), options));
        }
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.setThreadPolicy(threadPolicy);
        }
        return arrayList;
    }

    private void A04(C13060qW r6, ImmutableMap.Builder builder) {
        if (r6 != null && builder != null) {
            for (String A0Q : C84003yc.A00) {
                Fragment A0Q2 = r6.A0Q(A0Q);
                if (A0Q2 instanceof C12390pG) {
                    A05((C12390pG) A0Q2, builder);
                }
                if (A0Q2 != null) {
                    A04(A0Q2.A17(), builder);
                }
            }
        }
    }

    public void A06(Context context) {
        C44342Hv r0 = C44342Hv.A02;
        C134626Rk r1 = new C134626Rk();
        r1.A00(context);
        r1.A01(r0);
        A07(new AnonymousClass6Rm(r1));
    }

    public void A07(AnonymousClass6Rm r9) {
        boolean z;
        C21878Ajh markEventBuilder = ((QuickPerformanceLogger) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BBd, ((C47212Tt) AnonymousClass1XX.A02(19, AnonymousClass1Y3.B5b, this.A00)).A00)).markEventBuilder(30539800, "chooser_start");
        markEventBuilder.AOC("source", r9.A05.name);
        markEventBuilder.C2Q();
        Context context = r9.A04;
        ((AnonymousClass8WZ) AnonymousClass1XX.A02(6, AnonymousClass1Y3.As5, this.A00)).A04(Aj3.A0D);
        boolean Aem = ((C25051Yd) AnonymousClass1XX.A02(17, AnonymousClass1Y3.AOJ, this.A00)).Aem(286233800481415L);
        boolean Aep = ((FbSharedPreferences) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B6q, this.A00)).Aep(C71053bk.A00, false);
        TriState triState = TriState.YES;
        int i = AnonymousClass1Y3.Amd;
        AnonymousClass0UN r1 = this.A00;
        if (!triState.equals((TriState) AnonymousClass1XX.A02(7, i, r1))) {
            if (Aem && !Aep) {
                C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B6q, r1)).edit();
                edit.putBoolean(C71053bk.A00, true);
                edit.putBoolean(C71053bk.A01, true);
                edit.commit();
            } else if (!Aem && Aep) {
                ((FbSharedPreferences) AnonymousClass1XX.A02(12, AnonymousClass1Y3.B6q, r1)).ASu(ImmutableSet.A05(C71053bk.A00, C71053bk.A01));
            }
        }
        ((C21843Aix) AnonymousClass1XX.A02(18, AnonymousClass1Y3.BS4, this.A00)).A02(r9.A05, A02(this));
        ImmutableList AhC = ((AnonymousClass6SA) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Aqn, this.A00)).AhC();
        if (C013509w.A02(AhC) || (AhC.size() == 1 && ChooserOption.A07.equals(((ChooserOption) AhC.get(0)).A03))) {
            z = true;
        } else {
            z = false;
        }
        if (z || !(context instanceof C11450nB)) {
            A08(r9);
            return;
        }
        C13060qW B4m = ((C11450nB) context).B4m();
        if (B4m.A0Q("bug_reporter_chooser") == null) {
            C16290wo A0T = B4m.A0T();
            Preconditions.checkNotNull(AhC);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("CHOOSER_OPTIONS", C04300To.A03(AhC));
            ChooserFragment chooserFragment = new ChooserFragment();
            chooserFragment.A1P(bundle);
            chooserFragment.A01 = r9;
            chooserFragment.A2D(r9.A07);
            A0T.A0C(chooserFragment, "bug_reporter_chooser");
            A0T.A03();
            ((C21843Aix) AnonymousClass1XX.A02(18, AnonymousClass1Y3.BS4, this.A00)).A04("bug_report_menu");
        }
    }

    public void A08(AnonymousClass6Rm r4) {
        A09(r4, (AnonymousClass6SA) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Aqn, this.A00));
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x027d, code lost:
        if (r1.equals("bookmarks") == false) goto L_0x027f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0289, code lost:
        if (r2 == X.C44342Hv.A05) goto L_0x028b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x028e, code lost:
        if (r9 != false) goto L_0x020d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(X.AnonymousClass6Rm r22, X.AnonymousClass6SA r23) {
        /*
            r21 = this;
            r5 = r21
            int r1 = X.AnonymousClass1Y3.B5b
            X.0UN r0 = r5.A00
            r7 = 19
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.2Tt r0 = (X.C47212Tt) r0
            int r2 = X.AnonymousClass1Y3.BBd
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            com.facebook.quicklog.QuickPerformanceLogger r2 = (com.facebook.quicklog.QuickPerformanceLogger) r2
            r1 = 30539800(0x1d20018, float:7.714195E-38)
            java.lang.String r0 = "bug_report_start"
            X.Ajh r2 = r2.markEventBuilder(r1, r0)
            r15 = r22
            X.2Hv r0 = r15.A05
            java.lang.String r1 = r0.name
            java.lang.String r0 = "source"
            r2.AOC(r0, r1)
            r2.C2Q()
            X.46N r0 = r15.A06
            if (r0 == 0) goto L_0x003d
            X.46K r0 = r0.A00
            X.46A r1 = r0.A03
            X.45u r0 = X.C859245u.A01
            r1.A04(r0)
        L_0x003d:
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r5.A01 = r0
            X.0bK r0 = r5.A03     // Catch:{ all -> 0x03c8 }
            java.util.List r0 = r0.A00()     // Catch:{ all -> 0x03c8 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x03c8 }
        L_0x004e:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x03c8 }
            if (r0 == 0) goto L_0x006a
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x03c8 }
            X.3Dt r0 = (X.C64823Dt) r0     // Catch:{ all -> 0x03c8 }
            X.43q r2 = r0.AcO(r15)     // Catch:{ all -> 0x03c8 }
            if (r2 == 0) goto L_0x004e
            java.util.Map r1 = r5.A01     // Catch:{ all -> 0x03c8 }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x03c8 }
            r1.put(r0, r2)     // Catch:{ all -> 0x03c8 }
            goto L_0x004e
        L_0x006a:
            X.0bK r0 = r5.A03
            r0.A01()
            int r1 = X.AnonymousClass1Y3.BS4
            X.0UN r0 = r5.A00
            r6 = 18
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.Aix r2 = (X.C21843Aix) r2
            X.2Hv r1 = r15.A05
            java.lang.String r0 = A02(r5)
            r2.A02(r1, r0)
            r2 = 14
            int r1 = X.AnonymousClass1Y3.B2F
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2SU r0 = (X.AnonymousClass2SU) r0
            java.lang.String r0 = r0.A06()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            long r12 = r0.longValue()
            int r1 = X.AnonymousClass1Y3.ATL
            X.0UN r0 = r5.A00
            r8 = 15
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.2TN r9 = (X.AnonymousClass2TN) r9
            X.2Hv r0 = r15.A05
            java.lang.String r3 = r0.name
            int r1 = X.AnonymousClass1Y3.AQl
            X.0UN r0 = r9.A00
            r2 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14b r1 = (X.C185414b) r1
            X.0gA r0 = X.C08870g7.A0T
            r1.CH2(r0, r12)
            int r1 = X.AnonymousClass1Y3.AQl
            X.0UN r0 = r9.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.14b r2 = (X.C185414b) r2
            X.0gA r1 = X.C08870g7.A0T
            java.lang.String r0 = "source:"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r0, r3)
            r2.AMu(r1, r12, r0)
            com.google.common.collect.ImmutableMap$Builder r3 = new com.google.common.collect.ImmutableMap$Builder
            r3.<init>()
            android.content.Context r1 = r15.A04
            java.lang.Class<X.0pG> r0 = X.C12390pG.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            X.0pG r0 = (X.C12390pG) r0
            if (r0 == 0) goto L_0x00e5
            r5.A05(r0, r3)
        L_0x00e5:
            android.content.Context r1 = r15.A04
            java.lang.Class<X.0nB> r0 = X.C11450nB.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            X.0nB r0 = (X.C11450nB) r0
            if (r0 == 0) goto L_0x00f8
            X.0qW r0 = r0.B4m()
            r5.A04(r0, r3)
        L_0x00f8:
            android.content.Context r1 = r15.A04
            java.lang.Class<android.app.Activity> r0 = android.app.Activity.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            android.app.Activity r0 = (android.app.Activity) r0
            if (r0 == 0) goto L_0x011d
            android.content.Intent r1 = r0.getIntent()
            if (r1 == 0) goto L_0x011d
            android.os.Bundle r0 = r1.getExtras()
            if (r0 == 0) goto L_0x011d
            android.os.Bundle r0 = r1.getExtras()
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = "intent_extras"
            r3.put(r0, r1)
        L_0x011d:
            android.content.Context r1 = r15.A04
            java.lang.Class<X.0nI> r0 = X.C11510nI.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            X.0nI r0 = (X.C11510nI) r0
            if (r0 == 0) goto L_0x014c
            java.lang.String r1 = r0.Act()     // Catch:{ Exception -> 0x0135 }
            if (r1 == 0) goto L_0x014c
            java.lang.String r0 = "activity_analytics_tag"
            r3.put(r0, r1)     // Catch:{ Exception -> 0x0135 }
            goto L_0x014c
        L_0x0135:
            r9 = move-exception
            r2 = 3
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = X.AnonymousClass0Tc.A05
            java.lang.String r0 = "addActivityAnalyticsInfo"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.softReport(r0, r9)
        L_0x014c:
            int r2 = X.AnonymousClass1Y3.ATL
            X.0UN r1 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r8, r2, r1)
            X.2TN r2 = (X.AnonymousClass2TN) r2
            int r10 = X.AnonymousClass1Y3.AQl
            X.0UN r9 = r2.A00
            r2 = 0
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r2, r10, r9)
            X.14b r10 = (X.C185414b) r10
            X.0gA r9 = X.C08870g7.A0T
            java.lang.String r2 = "AddExtraDataFromUI"
            r10.AOH(r9, r12, r2)
            int r2 = X.AnonymousClass1Y3.BAm
            X.0UN r1 = r5.A00
            r0 = 20
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            java.util.Set r2 = (java.util.Set) r2
            r1 = r2
            X.0Tb r0 = X.AnonymousClass0Tc.A04
            if (r0 == 0) goto L_0x0181
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>(r1)
            r2.add(r0)
        L_0x0181:
            java.util.Iterator r10 = r2.iterator()
        L_0x0185:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x01b3
            java.lang.Object r0 = r10.next()
            X.0Tb r0 = (X.C04240Tb) r0
            com.google.common.collect.ImmutableMap r0 = r0.AmI()     // Catch:{ Exception -> 0x019b }
            if (r0 == 0) goto L_0x0185
            r3.putAll(r0)     // Catch:{ Exception -> 0x019b }
            goto L_0x0185
        L_0x019b:
            r9 = move-exception
            r2 = 3
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = X.AnonymousClass0Tc.A05
            java.lang.String r0 = "addExtraDataFromUI"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.softReport(r0, r9)
            goto L_0x0185
        L_0x01b3:
            com.google.common.collect.ImmutableMap r3 = r3.build()
            r16 = 0
            android.content.Context r1 = r15.A04
            java.lang.Class<android.app.Activity> r0 = android.app.Activity.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            if (r0 != 0) goto L_0x01cd
            android.content.Context r1 = r15.A04
            java.lang.Class<X.2h2> r0 = X.C51472h2.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            if (r0 == 0) goto L_0x02f1
        L_0x01cd:
            boolean r0 = r15.A0D
            r11 = 0
            if (r0 != 0) goto L_0x020d
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r5.A00
            r9 = 9
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r9, r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 509(0x1fd, float:7.13E-43)
            com.facebook.common.util.TriState r0 = r1.Ab9(r0)
            com.facebook.common.util.TriState r2 = com.facebook.common.util.TriState.YES
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x0290
            r10 = 7
            int r0 = X.AnonymousClass1Y3.Amd
            X.0UN r1 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r10, r0, r1)
            com.facebook.common.util.TriState r0 = (com.facebook.common.util.TriState) r0
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x026c
            int r0 = X.AnonymousClass1Y3.AcD
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r9, r0, r1)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            r0 = 373(0x175, float:5.23E-43)
            com.facebook.common.util.TriState r0 = r1.Ab9(r0)
            if (r0 == r2) goto L_0x026c
        L_0x020d:
            if (r11 == 0) goto L_0x02ad
            int r1 = X.AnonymousClass1Y3.ATL
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r8, r1, r0)
            X.2TN r0 = (X.AnonymousClass2TN) r0
            int r2 = X.AnonymousClass1Y3.AQl
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.14b r2 = (X.C185414b) r2
            X.0gA r1 = X.C08870g7.A0T
            java.lang.String r0 = "AddScreenshot"
            r2.AOH(r1, r12, r0)
            int r1 = X.AnonymousClass1Y3.B5b
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.2Tt r0 = (X.C47212Tt) r0
            int r8 = r0.A01()
            r2 = 11
            int r1 = X.AnonymousClass1Y3.B7Z
            X.0UN r0 = r5.A00
            java.lang.Object r10 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.84M r10 = (X.AnonymousClass84M) r10
            android.content.Context r9 = r15.A04
            java.lang.ref.WeakReference r2 = r15.A09
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Map r0 = r5.A01
            java.util.Collection r0 = r0.values()
            java.util.Iterator r11 = r0.iterator()
        L_0x0258:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x0293
            java.lang.Object r0 = r11.next()
            X.43q r0 = (X.C854943q) r0
            java.lang.ref.WeakReference r0 = r0.A01
            if (r0 == 0) goto L_0x0258
            r1.add(r0)
            goto L_0x0258
        L_0x026c:
            java.lang.String r0 = "activity_analytics_tag"
            java.lang.Object r1 = r3.get(r0)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x027f
            java.lang.String r0 = "bookmarks"
            boolean r0 = r1.equals(r0)
            r9 = 1
            if (r0 != 0) goto L_0x0280
        L_0x027f:
            r9 = 0
        L_0x0280:
            X.2Hv r2 = r15.A05
            X.2Hv r0 = X.C44342Hv.A0D
            if (r2 == r0) goto L_0x028b
            X.2Hv r1 = X.C44342Hv.A05
            r0 = 0
            if (r2 != r1) goto L_0x028c
        L_0x028b:
            r0 = 1
        L_0x028c:
            if (r0 == 0) goto L_0x0290
            if (r9 != 0) goto L_0x020d
        L_0x0290:
            r11 = 1
            goto L_0x020d
        L_0x0293:
            X.8BL r0 = r10.A00
            java.util.List r2 = r0.A08(r9, r2, r1)
            int r1 = X.AnonymousClass1Y3.B5b
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r7, r1, r0)
            X.2Tt r0 = (X.C47212Tt) r0
            r0.A02(r8)
            if (r2 == 0) goto L_0x02ad
            java.util.List r0 = r15.A0B
            r0.addAll(r2)
        L_0x02ad:
            java.util.List r0 = r15.A0A
            if (r0 == 0) goto L_0x02ba
            java.util.List r1 = A03(r0)
            java.util.List r0 = r15.A0B
            r0.addAll(r1)
        L_0x02ba:
            r2 = 9
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r5.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1YI r2 = (X.AnonymousClass1YI) r2
            r1 = 31
            r0 = 1
            boolean r0 = r2.AbO(r1, r0)
            if (r0 == 0) goto L_0x02f1
            r2 = 5
            int r1 = X.AnonymousClass1Y3.BFy
            X.0UN r0 = r5.A00
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2To r7 = (X.C47172To) r7
            android.content.Context r1 = r15.A04
            java.lang.Class<X.2h2> r0 = X.C51472h2.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            X.2h2 r0 = (X.C51472h2) r0
            if (r0 == 0) goto L_0x0318
            android.view.View r2 = r0.B1W()
        L_0x02ea:
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
            r0 = 0
            android.os.Bundle r16 = X.C47172To.A00(r7, r2, r1, r0)
        L_0x02f1:
            r17 = 0
            java.util.Map r2 = r15.A0C
            if (r2 != 0) goto L_0x02fc
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
        L_0x02fc:
            java.util.Map r0 = r5.A01
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
        L_0x0306:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x032c
            java.lang.Object r0 = r1.next()
            X.43q r0 = (X.C854943q) r0
            java.util.Map r0 = r0.A02
            r2.putAll(r0)
            goto L_0x0306
        L_0x0318:
            java.lang.Class<android.app.Activity> r0 = android.app.Activity.class
            java.lang.Object r0 = X.AnonymousClass065.A00(r1, r0)
            android.app.Activity r0 = (android.app.Activity) r0
            com.google.common.base.Preconditions.checkNotNull(r0)
            android.view.Window r0 = r0.getWindow()
            android.view.View r2 = r0.getDecorView()
            goto L_0x02ea
        L_0x032c:
            int r1 = X.AnonymousClass1Y3.BS4
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.Aix r0 = (X.C21843Aix) r0
            int r6 = X.AnonymousClass1Y3.AQl
            X.0UN r1 = r0.A00
            r0 = 0
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r6, r1)
            X.14b r1 = (X.C185414b) r1
            X.0gA r0 = X.C08870g7.A0S
            long r0 = r1.Ant(r0)
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "funnel_id"
            r2.put(r0, r1)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r6 = 13
            int r1 = X.AnonymousClass1Y3.AcV
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r6, r1, r0)
            X.0nZ r0 = (X.C11660nZ) r0
            boolean r0 = r0.A03()
            r7.append(r0)
            java.lang.String r1 = r7.toString()
            java.lang.String r0 = "is_dark_mode_currently_enabled"
            r2.put(r0, r1)
            com.google.common.collect.ImmutableSet r0 = r15.A08
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>(r0)
            java.util.Map r0 = r5.A01
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
        L_0x0382:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0396
            java.lang.Object r0 = r1.next()
            X.43q r0 = (X.C854943q) r0
            com.google.common.collect.ImmutableSet r0 = r0.A00
            if (r0 == 0) goto L_0x0382
            r6.addAll(r0)
            goto L_0x0382
        L_0x0396:
            com.google.common.collect.ImmutableSet r19 = com.google.common.collect.ImmutableSet.A0A(r6)
            android.content.Context r1 = r15.A04
            X.6Rv r10 = new X.6Rv
            r11 = r5
            r14 = r23
            r18 = r3
            r20 = r2
            r10.<init>(r11, r12, r14, r15, r16, r17, r18, r19, r20)
            r0 = 2131822211(0x7f110683, float:1.9277187E38)
            com.google.common.util.concurrent.ListenableFuture r4 = A01(r5, r1, r0, r10)
            X.6Ru r3 = new X.6Ru
            r7 = r5
            r6 = r3
            r8 = r14
            r9 = r15
            r10 = r12
            r6.<init>(r7, r8, r9, r10)
            r2 = 2
            int r1 = X.AnonymousClass1Y3.A6Z
            X.0UN r0 = r5.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.util.concurrent.Executor r0 = (java.util.concurrent.Executor) r0
            X.C05350Yp.A08(r4, r3, r0)
            return
        L_0x03c8:
            r1 = move-exception
            X.0bK r0 = r5.A03
            r0.A01()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Tc.A09(X.6Rm, X.6SA):void");
    }

    private AnonymousClass0Tc(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(21, r3);
    }

    public static String A02(AnonymousClass0Tc r2) {
        String A022 = AnonymousClass01P.A02();
        if (!A022.contains("BugReport")) {
            r2.A02 = A022;
        }
        return r2.A02;
    }

    private void A05(C12390pG r5, ImmutableMap.Builder builder) {
        try {
            Map Ajk = r5.Ajk();
            if (Ajk != null) {
                builder.putAll(Ajk);
            }
        } catch (Exception e) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Amr, this.A00)).softReport(AnonymousClass08S.A0J(A05, "addComponentDebugInfo"), e);
        }
    }
}
