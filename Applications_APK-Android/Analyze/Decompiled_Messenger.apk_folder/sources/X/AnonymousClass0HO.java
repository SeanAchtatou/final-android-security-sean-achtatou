package X;

import com.facebook.analytics.appstatelogger.AppStateLogFile;
import java.util.concurrent.Callable;

/* renamed from: X.0HO  reason: invalid class name */
public final class AnonymousClass0HO implements Callable {
    public final /* synthetic */ AppStateLogFile A00;

    public AnonymousClass0HO(AppStateLogFile appStateLogFile) {
        this.A00 = appStateLogFile;
    }

    public Object call() {
        return this.A00.getLogData();
    }
}
