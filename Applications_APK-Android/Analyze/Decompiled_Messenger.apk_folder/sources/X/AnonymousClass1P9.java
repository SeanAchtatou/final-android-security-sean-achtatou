package X;

import java.util.concurrent.Callable;

/* renamed from: X.1P9  reason: invalid class name */
public final class AnonymousClass1P9 implements Callable {
    public final Runtime A00;

    public Object call() {
        long maxMemory = this.A00.maxMemory();
        long freeMemory = this.A00.totalMemory() - this.A00.freeMemory();
        Double valueOf = Double.valueOf(-1.0d);
        if (maxMemory > 0 && maxMemory != Long.MAX_VALUE && freeMemory > 0 && freeMemory != Long.MAX_VALUE) {
            double d = ((double) freeMemory) / ((double) maxMemory);
            if (d < 1.0d) {
                return Double.valueOf(d);
            }
        }
        return valueOf;
    }

    public AnonymousClass1P9(Runtime runtime) {
        this.A00 = runtime;
    }
}
