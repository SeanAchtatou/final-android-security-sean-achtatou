package X;

import android.view.LayoutInflater;
import com.facebook.messaging.media.picker.MediaPickerPopupVideoView;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Dr  reason: invalid class name and case insensitive filesystem */
public final class C20751Dr extends C20831Dz {
    public C26296Cva A00;
    public C26268Cv6 A01;
    public ImmutableList A02 = RegularImmutableList.A02;
    public Integer A03;
    public boolean A04;
    public final LayoutInflater A05;
    public final C83333xS A06;
    public final C83343xT A07;
    public final C26324Cw3 A08 = new C26324Cw3(this);
    public final C26297Cvb A09 = new C26297Cvb(this);
    public final C64623Cp A0A;
    public final C1933294b A0B;
    public final C67553Ph A0C;
    public final List A0D = new ArrayList();

    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0240, code lost:
        if (r7 == X.C421828p.A0B) goto L_0x0242;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x02f8, code lost:
        if (r1 != false) goto L_0x02fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0305, code lost:
        if (r0 != false) goto L_0x0307;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        if (r4 == X.C421828p.A0B) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00aa, code lost:
        if (com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0) != false) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0192, code lost:
        if (r0 != false) goto L_0x0194;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0E(X.C33781o8 r12, int r13, java.util.List<X.C26326Cw5> r14) {
        /*
            r11 = this;
            boolean r0 = r11.A04
            if (r0 == 0) goto L_0x0008
            if (r13 == 0) goto L_0x035e
            int r13 = r13 + -1
        L_0x0008:
            if (r14 == 0) goto L_0x0010
            boolean r0 = r14.isEmpty()
            if (r0 == 0) goto L_0x0016
        L_0x0010:
            X.Cw5 r0 = X.C26326Cw5.A03
            java.util.List r14 = java.util.Collections.singletonList(r0)
        L_0x0016:
            X.Cur r12 = (X.C26257Cur) r12
            java.util.Iterator r10 = r14.iterator()
        L_0x001c:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x0322
            java.lang.Object r3 = r10.next()
            X.Cw5 r3 = (X.C26326Cw5) r3
            com.google.common.collect.ImmutableList r0 = r11.A02
            java.lang.Object r2 = r0.get(r13)
            com.facebook.ui.media.attachments.model.MediaResource r2 = (com.facebook.ui.media.attachments.model.MediaResource) r2
            boolean r0 = r3.A00
            if (r0 == 0) goto L_0x00ea
            X.28p r4 = r2.A0L
            X.28p r0 = X.C421828p.PHOTO
            if (r4 == r0) goto L_0x003f
            X.28p r1 = X.C421828p.VIDEO
            r0 = 0
            if (r4 != r1) goto L_0x0040
        L_0x003f:
            r0 = 1
        L_0x0040:
            com.google.common.base.Preconditions.checkArgument(r0)
            r12.A02 = r2
            X.Cv0 r5 = r12.A06
            X.Cvs r0 = r12.A03
            r5.A03 = r0
            com.facebook.ui.media.attachments.model.MediaResource r0 = r5.A04
            if (r0 == 0) goto L_0x0235
            if (r0 != r2) goto L_0x0235
        L_0x0051:
            X.Ctw r4 = r12.A07
            X.28p r1 = r2.A0L
            X.28p r0 = X.C421828p.VIDEO
            if (r1 == r0) goto L_0x0220
            android.widget.TextView r1 = r4.A00
            r0 = 8
            r1.setVisibility(r0)
        L_0x0060:
            X.2o5 r4 = r12.A04
            X.28p r0 = r2.A0L
            r4.A02 = r0
            boolean r0 = r4.A00()
            if (r0 == 0) goto L_0x009f
            com.facebook.messaging.media.picker.MediaPickerPopupVideoView r0 = r4.A00
            if (r0 != 0) goto L_0x007a
            android.view.ViewStub r0 = r4.A07
            android.view.View r0 = r0.inflate()
            com.facebook.messaging.media.picker.MediaPickerPopupVideoView r0 = (com.facebook.messaging.media.picker.MediaPickerPopupVideoView) r0
            r4.A00 = r0
        L_0x007a:
            com.facebook.messaging.media.picker.MediaPickerPopupVideoView r1 = r4.A00
            X.2o4 r0 = new X.2o4
            r0.<init>(r4)
            r1.A02 = r0
            android.animation.ValueAnimator r1 = r4.A06
            android.view.animation.LinearInterpolator r0 = new android.view.animation.LinearInterpolator
            r0.<init>()
            r1.setInterpolator(r0)
            android.animation.ValueAnimator r1 = r4.A06
            X.CvL r0 = new X.CvL
            r0.<init>(r4)
            r1.addUpdateListener(r0)
            r4.A03 = r2
            com.facebook.messaging.media.picker.MediaPickerPopupVideoView r1 = r4.A00
            r0 = 0
            r1.setVisibility(r0)
        L_0x009f:
            X.Cv6 r0 = r11.A01
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A02
            if (r0 == 0) goto L_0x00ac
            boolean r0 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0)
            r1 = 1
            if (r0 == 0) goto L_0x00ad
        L_0x00ac:
            r1 = 0
        L_0x00ad:
            boolean r0 = r2.A0k
            if (r0 != 0) goto L_0x00ea
            if (r1 == 0) goto L_0x00ea
            X.3Cp r0 = r11.A0A
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x00ea
            X.1Dt r6 = new X.1Dt
            r6.<init>(r11, r2, r13)
            X.94b r7 = r11.A0B
            android.net.Uri r0 = r2.A0D
            java.lang.String r5 = r0.getPath()
            int r4 = X.AnonymousClass1Y3.ALA
            X.0UN r1 = r7.A00
            r0 = 1
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r0, r4, r1)
            X.0VL r1 = (X.AnonymousClass0VL) r1
            X.94e r0 = new X.94e
            r0.<init>(r7, r5)
            com.google.common.util.concurrent.ListenableFuture r5 = r1.CIF(r0)
            int r4 = X.AnonymousClass1Y3.ADs
            X.0UN r1 = r7.A00
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r4, r1)
            java.util.concurrent.ExecutorService r0 = (java.util.concurrent.ExecutorService) r0
            X.C05350Yp.A08(r5, r6, r0)
        L_0x00ea:
            boolean r0 = r3.A01
            if (r0 == 0) goto L_0x0153
            java.util.List r0 = r11.A0D
            int r6 = r0.indexOf(r2)
            java.util.List r0 = r11.A0D
            boolean r4 = r0.contains(r2)
            X.Cv6 r0 = r12.A01
            if (r0 == 0) goto L_0x0153
            boolean r0 = r0.A08
            if (r0 == 0) goto L_0x0153
            X.Cv0 r5 = r12.A06
            java.lang.Integer r0 = r5.A05
            int r1 = r0.intValue()
            r0 = 0
            if (r1 == r0) goto L_0x01ff
            boolean r0 = r5.A0A
            if (r0 == r4) goto L_0x0127
            boolean r0 = r5.A07
            if (r0 != 0) goto L_0x0127
            r5.A0A = r4
            android.animation.ValueAnimator r0 = r5.A0C
            r0.cancel()
            android.animation.ValueAnimator r2 = r5.A0C
            boolean r0 = r5.A0A
            if (r0 == 0) goto L_0x01fb
            r0 = 100
        L_0x0124:
            r2.setCurrentPlayTime(r0)
        L_0x0127:
            X.Cvc r5 = r12.A05
            if (r4 == 0) goto L_0x01f2
            com.facebook.widget.text.BetterTextView r1 = r5.A01
            int r0 = r6 + 1
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r1.setText(r0)
            com.facebook.widget.text.BetterTextView r1 = r5.A01
            r0 = 0
            r1.setVisibility(r0)
        L_0x013c:
            r2 = 2131827160(0x7f1119d8, float:1.9287225E38)
            if (r4 == 0) goto L_0x0144
            r2 = 2131827159(0x7f1119d7, float:1.9287223E38)
        L_0x0144:
            android.widget.ImageView r1 = r5.A00
            com.facebook.widget.text.BetterTextView r0 = r5.A01
            android.content.Context r0 = r0.getContext()
            java.lang.String r0 = r0.getString(r2)
            r1.setContentDescription(r0)
        L_0x0153:
            boolean r0 = r3.A02
            if (r0 == 0) goto L_0x001c
            X.Cv6 r4 = r11.A01
            r12.A01 = r4
            X.Cv0 r2 = r12.A06
            boolean r1 = r4.A04
            r2.A07 = r1
            int r1 = r4.A00
            r0 = 1
            if (r1 == r0) goto L_0x01ec
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            r2.A05 = r0
        L_0x016a:
            X.Ctw r3 = r12.A07
            int r2 = r4.A01
            r0 = 2
            if (r2 == r0) goto L_0x01e9
            r0 = 3
            if (r2 == r0) goto L_0x01e6
            r0 = 4
            r1 = 85
            if (r2 == r0) goto L_0x017b
            r1 = 51
        L_0x017b:
            android.widget.TextView r0 = r3.A00
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.widget.FrameLayout$LayoutParams r0 = (android.widget.FrameLayout.LayoutParams) r0
            r0.gravity = r1
            X.Cv6 r2 = r12.A01
            if (r2 == 0) goto L_0x001c
            X.Cvc r1 = r12.A05
            boolean r0 = r2.A08
            if (r0 == 0) goto L_0x0194
            boolean r0 = r2.A0B
            r2 = 1
            if (r0 == 0) goto L_0x0195
        L_0x0194:
            r2 = 0
        L_0x0195:
            android.widget.ImageView r1 = r1.A00
            r0 = 8
            if (r2 == 0) goto L_0x019c
            r0 = 0
        L_0x019c:
            r1.setVisibility(r0)
            X.Cvc r4 = r12.A05
            X.Cv6 r0 = r12.A01
            boolean r0 = r0.A0B
            if (r0 == 0) goto L_0x001c
            com.facebook.widget.text.BetterTextView r0 = r4.A01
            android.content.Context r0 = r0.getContext()
            if (r0 == 0) goto L_0x001c
            com.facebook.widget.text.BetterTextView r0 = r4.A01
            android.content.Context r0 = r0.getContext()
            android.content.res.Resources r3 = r0.getResources()
            r0 = 2132148275(0x7f160033, float:1.9938523E38)
            int r2 = r3.getDimensionPixelSize(r0)
            com.facebook.widget.text.BetterTextView r0 = r4.A01
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            android.widget.FrameLayout$LayoutParams r1 = (android.widget.FrameLayout.LayoutParams) r1
            if (r1 == 0) goto L_0x001c
            r0 = 17
            r1.gravity = r0
            r1.width = r2
            r1.height = r2
            com.facebook.widget.text.BetterTextView r0 = r4.A01
            r0.setLayoutParams(r1)
            r0 = 2132148366(0x7f16008e, float:1.9938708E38)
            float r2 = r3.getDimension(r0)
            com.facebook.widget.text.BetterTextView r1 = r4.A01
            r0 = 0
            r1.setTextSize(r0, r2)
            goto L_0x001c
        L_0x01e6:
            r1 = 83
            goto L_0x017b
        L_0x01e9:
            r1 = 53
            goto L_0x017b
        L_0x01ec:
            java.lang.Integer r0 = X.AnonymousClass07B.A00
            r2.A05 = r0
            goto L_0x016a
        L_0x01f2:
            com.facebook.widget.text.BetterTextView r1 = r5.A01
            r0 = 8
            r1.setVisibility(r0)
            goto L_0x013c
        L_0x01fb:
            r0 = 0
            goto L_0x0124
        L_0x01ff:
            boolean r0 = r5.A0A
            if (r0 == r4) goto L_0x0127
            boolean r0 = r5.A07
            if (r0 != 0) goto L_0x0127
            r5.A0A = r4
            com.facebook.drawee.fbpipeline.FbDraweeView r2 = r5.A0H
            if (r4 == 0) goto L_0x021b
            android.content.res.Resources r1 = r5.A0D
            r0 = 2132083237(0x7f150225, float:1.980661E38)
            int r0 = r1.getColor(r0)
            r2.setColorFilter(r0)
            goto L_0x0127
        L_0x021b:
            r2.clearColorFilter()
            goto L_0x0127
        L_0x0220:
            android.widget.TextView r1 = r4.A00
            r0 = 0
            r1.setVisibility(r0)
            android.widget.TextView r5 = r4.A00
            X.3tN r4 = r4.A01
            long r0 = r2.A07
            java.lang.String r0 = r4.A01(r0)
            r5.setText(r0)
            goto L_0x0060
        L_0x0235:
            X.28p r7 = r2.A0L
            X.28p r0 = X.C421828p.PHOTO
            r4 = 1
            r6 = 0
            if (r7 == r0) goto L_0x0242
            X.28p r1 = X.C421828p.VIDEO
            r0 = 0
            if (r7 != r1) goto L_0x0243
        L_0x0242:
            r0 = 1
        L_0x0243:
            com.google.common.base.Preconditions.checkArgument(r0)
            r5.A04 = r2
            X.3hC r8 = r5.A0F
            java.lang.Integer r7 = X.AnonymousClass07B.A0q
            long r0 = r2.A05
            java.lang.String r9 = r8.A0D(r7, r0)
            android.content.res.Resources r8 = r5.A0D
            X.28p r7 = r2.A0L
            X.28p r0 = X.C421828p.PHOTO
            r1 = 2131834640(0x7f113710, float:1.9302396E38)
            if (r7 != r0) goto L_0x0260
            r1 = 2131830389(0x7f112675, float:1.9293774E38)
        L_0x0260:
            java.lang.Object[] r0 = new java.lang.Object[]{r9}
            java.lang.String r1 = r8.getString(r1, r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = r5.A0H
            r0.setContentDescription(r1)
            X.36w r0 = r5.A01
            if (r0 != 0) goto L_0x0281
            android.content.res.Resources r1 = r5.A0D
            r0 = 2132148293(0x7f160045, float:1.993856E38)
            int r1 = r1.getDimensionPixelSize(r0)
            X.36w r0 = new X.36w
            r0.<init>(r1, r1)
            r5.A01 = r0
        L_0x0281:
            r0 = 0
            r5.A06 = r0
            X.1Py r0 = new X.1Py
            r0.<init>()
            r0.A03(r4)
            r0.A01(r6)
            X.1Px r1 = r0.A00()
            android.net.Uri r0 = r2.A0D
            X.1Pv r7 = X.C23521Pv.A00(r0)
            X.A22 r0 = r5.A0K
            r7.A09 = r0
            X.36w r0 = r5.A01
            r7.A04 = r0
            r7.A0D = r4
            r7.A0E = r4
            X.3g8 r0 = r5.A0N
            boolean r0 = r0.A0E()
            if (r0 == 0) goto L_0x02af
            r7.A02 = r1
        L_0x02af:
            com.facebook.drawee.fbpipeline.FbDraweeView r4 = r5.A0H
            X.2zZ r1 = r5.A0G
            r1.A05 = r6
            com.facebook.common.callercontext.CallerContext r0 = X.C26265Cv0.A0S
            r1.A0Q(r0)
            X.2QL r0 = r5.A0L
            r1.A08(r0)
            X.1Q0 r0 = r7.A02()
            r1.A0A(r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = r5.A0H
            X.306 r0 = r0.A05()
            r1.A09(r0)
            X.303 r0 = r1.A0F()
            r4.A08(r0)
            android.widget.ImageView r4 = r5.A0E
            boolean r1 = r2.A04()
            r0 = 8
            if (r1 == 0) goto L_0x02e1
            r0 = 0
        L_0x02e1:
            r4.setVisibility(r0)
            com.facebook.drawee.fbpipeline.FbDraweeView r0 = r5.A0H
            r0.setVisibility(r6)
            X.0vt r0 = r5.A0O
            r0.A03()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r5.A0M
            r4 = 1
            if (r0 == 0) goto L_0x02fa
            boolean r1 = com.facebook.messaging.model.threadkey.ThreadKey.A0F(r0)
            r0 = 1
            if (r1 == 0) goto L_0x02fb
        L_0x02fa:
            r0 = 0
        L_0x02fb:
            if (r0 == 0) goto L_0x031b
            com.facebook.ui.media.attachments.model.MediaResource r0 = r5.A04
            com.facebook.spherical.photo.metadata.SphericalPhotoMetadata r1 = r0.A0H
            r0 = 0
            if (r1 == 0) goto L_0x0305
            r0 = 1
        L_0x0305:
            if (r0 == 0) goto L_0x031b
        L_0x0307:
            X.0vt r0 = r5.A0P
            if (r4 == 0) goto L_0x0317
            r0.A04()
        L_0x030e:
            X.0vt r0 = r5.A0Q
            if (r4 == 0) goto L_0x031d
            r0.A04()
            goto L_0x0051
        L_0x0317:
            r0.A03()
            goto L_0x030e
        L_0x031b:
            r4 = 0
            goto L_0x0307
        L_0x031d:
            r0.A03()
            goto L_0x0051
        L_0x0322:
            X.Cv0 r0 = r12.A06
            r0.A00 = r13
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x035e
            java.lang.Integer r0 = r11.A03
            if (r0 == 0) goto L_0x035e
            int r2 = r0.intValue()
            X.Cvc r0 = r12.A05
            com.facebook.widget.text.BetterTextView r0 = r0.A01
            android.graphics.drawable.Drawable r0 = r0.getBackground()
            android.graphics.drawable.Drawable r1 = r0.mutate()
            if (r1 == 0) goto L_0x035e
            boolean r0 = r1 instanceof android.graphics.drawable.InsetDrawable
            if (r0 == 0) goto L_0x035e
            android.graphics.drawable.InsetDrawable r1 = (android.graphics.drawable.InsetDrawable) r1
            android.graphics.drawable.Drawable r1 = r1.getDrawable()
            boolean r0 = r1 instanceof android.graphics.drawable.LayerDrawable
            if (r0 == 0) goto L_0x035e
            android.graphics.drawable.LayerDrawable r1 = (android.graphics.drawable.LayerDrawable) r1
            r0 = 2131296912(0x7f090290, float:1.8211754E38)
            android.graphics.drawable.Drawable r0 = r1.findDrawableByLayerId(r0)
            if (r0 == 0) goto L_0x035e
            r0.setTint(r2)
        L_0x035e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20751Dr.A0E(X.1o8, int, java.util.List):void");
    }

    public void A0F(C26268Cv6 cv6) {
        this.A01 = cv6;
        this.A0D.clear();
        C26326Cw5 cw5 = new C26326Cw5(false, true, true);
        this.A01.A04(0, ArU(), cw5);
    }

    public void A0G(ImmutableList immutableList) {
        this.A02 = immutableList;
        if (!C013509w.A02(this.A0D) && !immutableList.containsAll(this.A0D)) {
            ArrayList arrayList = new ArrayList();
            for (MediaResource mediaResource : this.A0D) {
                if (!immutableList.contains(mediaResource)) {
                    arrayList.add(mediaResource);
                }
            }
            this.A0D.removeAll(arrayList);
            boolean A012 = C013509w.A01(this.A0D);
            if (this.A00 != null) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    this.A00.BcA((MediaResource) it.next(), A012);
                }
            }
        }
        A04();
    }

    public int ArU() {
        if (this.A04) {
            return this.A02.size() + 1;
        }
        return this.A02.size();
    }

    public C20751Dr(AnonymousClass1XY r3, C26268Cv6 cv6) {
        this.A05 = C04490Ux.A0e(r3);
        this.A06 = new C83333xS(r3);
        this.A07 = new C83343xT(r3);
        this.A0A = C64623Cp.A00(r3);
        this.A0B = new C1933294b(r3, C1933394c.A00(r3));
        this.A0C = C67553Ph.A01(r3);
        this.A01 = cv6;
        this.A04 = cv6.A0A;
    }

    public void A0D(C33781o8 r6) {
        MediaPickerPopupVideoView mediaPickerPopupVideoView;
        super.A0D(r6);
        if (r6 instanceof C26257Cur) {
            C26257Cur cur = (C26257Cur) r6;
            C26265Cv0 cv0 = cur.A06;
            cv0.A06 = null;
            C26265Cv0.A01(cv0);
            C55442o5 r3 = cur.A04;
            if (r3.A00() && (mediaPickerPopupVideoView = r3.A00) != null) {
                AnonymousClass1G0 r1 = mediaPickerPopupVideoView.A01;
                if (r1 != null) {
                    r1.A01(true);
                    mediaPickerPopupVideoView.A01 = null;
                }
                MediaPickerPopupVideoView.A00(mediaPickerPopupVideoView, C53752lJ.A0L);
                mediaPickerPopupVideoView.A04 = null;
                r3.A00.setVisibility(8);
            }
        }
    }
}
