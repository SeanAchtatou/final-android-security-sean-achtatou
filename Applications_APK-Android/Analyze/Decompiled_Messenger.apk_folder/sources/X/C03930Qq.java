package X;

import java.io.Closeable;
import java.io.InputStream;

/* renamed from: X.0Qq  reason: invalid class name and case insensitive filesystem */
public final class C03930Qq implements Closeable {
    public final AnonymousClass0EV A00;
    public final InputStream A01;

    public void close() {
        this.A01.close();
    }

    public C03930Qq(AnonymousClass0EV r1, InputStream inputStream) {
        this.A00 = r1;
        this.A01 = inputStream;
    }
}
