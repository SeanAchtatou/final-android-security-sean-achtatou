package X;

/* renamed from: X.1sm  reason: invalid class name and case insensitive filesystem */
public enum C36261sm implements C36201sg {
    RAW(0),
    ZLIB(1),
    ZLIB_OPTIONAL(2);
    
    private final int value;

    public int getValue() {
        return this.value;
    }

    private C36261sm(int i) {
        this.value = i;
    }
}
