package X;

/* renamed from: X.0G8  reason: invalid class name */
public final class AnonymousClass0G8 {
    public final int A00;
    public final String A01;

    public String toString() {
        return AnonymousClass08S.A0Q("topic:", this.A01, " messageId:", this.A00);
    }

    public AnonymousClass0G8(String str, int i) {
        this.A01 = str;
        this.A00 = i;
    }
}
