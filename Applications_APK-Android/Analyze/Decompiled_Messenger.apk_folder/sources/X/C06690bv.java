package X;

import com.facebook.secure.intentswitchoff.FbReceiverSwitchOffDI;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0bv  reason: invalid class name and case insensitive filesystem */
public final class C06690bv extends AnonymousClass1ZS {
    private static volatile C06690bv A01;
    public AnonymousClass0UN A00;

    public static final C06690bv A00(AnonymousClass1XY r6) {
        if (A01 == null) {
            synchronized (C06690bv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        A01 = new C06690bv(applicationInjector, FbReceiverSwitchOffDI.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.B4h, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private C06690bv(AnonymousClass1XY r3, FbReceiverSwitchOffDI fbReceiverSwitchOffDI, AnonymousClass0US r5) {
        super(fbReceiverSwitchOffDI, r5);
        this.A00 = new AnonymousClass0UN(0, r3);
    }
}
