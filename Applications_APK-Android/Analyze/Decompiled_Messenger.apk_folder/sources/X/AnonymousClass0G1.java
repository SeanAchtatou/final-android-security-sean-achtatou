package X;

import android.os.Debug;
import com.facebook.acra.util.StatFsUtil;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0G1  reason: invalid class name */
public final class AnonymousClass0G1 extends C007907e {
    public boolean A00 = false;
    private final ThreadLocal A01 = new ThreadLocal();
    private final AtomicLong A02 = new AtomicLong();

    public AnonymousClass0FM A03() {
        return new AnonymousClass0G0();
    }

    public boolean A04(AnonymousClass0FM r8) {
        AnonymousClass0G0 r82 = (AnonymousClass0G0) r8;
        synchronized (this) {
            C02740Gd.A00(r82, "Null value passed to getSnapshot!");
            if (!this.A00) {
                return false;
            }
            r82.sequenceNumber = this.A02.incrementAndGet();
            Runtime runtime = Runtime.getRuntime();
            r82.javaHeapMaxSizeKb = runtime.maxMemory() / StatFsUtil.IN_KILO_BYTE;
            r82.javaHeapAllocatedKb = (runtime.totalMemory() - runtime.freeMemory()) / StatFsUtil.IN_KILO_BYTE;
            r82.nativeHeapSizeKb = Debug.getNativeHeapSize() / StatFsUtil.IN_KILO_BYTE;
            r82.nativeHeapAllocatedKb = Debug.getNativeHeapAllocatedSize() / StatFsUtil.IN_KILO_BYTE;
            r82.vmSizeKb = -1;
            r82.vmRssKb = -1;
            try {
                C02560Fk r4 = (C02560Fk) this.A01.get();
                if (r4 == null) {
                    r4 = new C02560Fk("/proc/self/statm", 512);
                    this.A01.set(r4);
                }
                r4.A04();
                if (r4.A01) {
                    r4.A06();
                    r82.vmSizeKb = r4.A03() * 4;
                    r4.A06();
                    r82.vmRssKb = r4.A03() * 4;
                }
            } catch (AnonymousClass0KX e) {
                AnonymousClass0KZ.A00("MemoryMetricsCollector", "Unable to parse memory (statm) field", e);
            }
            return true;
        }
    }
}
