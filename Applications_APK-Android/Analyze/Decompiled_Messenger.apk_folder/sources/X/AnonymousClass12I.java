package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.montage.model.BasicMontageThreadInfo;
import com.google.common.base.Optional;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@UserScoped
/* renamed from: X.12I  reason: invalid class name */
public final class AnonymousClass12I {
    private static C05540Zi A07;
    public long A00 = 0;
    public AnonymousClass0UN A01;
    private AnonymousClass12B A02;
    private Integer A03 = AnonymousClass07B.A00;
    public final Map A04 = new AnonymousClass04a();
    public final Set A05 = new C05180Xy();
    public final ReadWriteLock A06 = new ReentrantReadWriteLock();

    public static final AnonymousClass12I A00(AnonymousClass1XY r4) {
        AnonymousClass12I r0;
        synchronized (AnonymousClass12I.class) {
            C05540Zi A002 = C05540Zi.A00(A07);
            A07 = A002;
            try {
                if (A002.A03(r4)) {
                    A07.A00 = new AnonymousClass12I((AnonymousClass1XY) A07.A01());
                }
                C05540Zi r1 = A07;
                r0 = (AnonymousClass12I) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A07.A02();
                throw th;
            }
        }
        return r0;
    }

    private void A01() {
        long j;
        this.A04.clear();
        Optional A072 = ((C624531u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BUS, this.A01)).A07("montage_last_seen_timestamp");
        if (A072.isPresent()) {
            j = ((Long) A072.get()).longValue();
        } else {
            j = 0;
        }
        long j2 = this.A00;
        if (j2 > j) {
            ((C624531u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BUS, this.A01)).A09("montage_last_seen_timestamp", j2);
            this.A00 = 0;
        }
    }

    /* JADX INFO: finally extract failed */
    public static void A02(AnonymousClass12I r4) {
        AnonymousClass10O r1;
        if (AnonymousClass07B.A00.equals(r4.A03)) {
            r4.A03 = AnonymousClass07B.A01;
        }
        AnonymousClass12B r42 = r4.A02;
        if (r42 != null) {
            AnonymousClass12I r2 = r42.A00.A03;
            Lock readLock = r2.A06.readLock();
            readLock.lock();
            try {
                int size = r2.A04.size();
                readLock.unlock();
                AnonymousClass11W r22 = r42.A00;
                if (r22.A02.A0E()) {
                    r1 = AnonymousClass10O.RED_WITH_TEXT;
                } else if (r22.A02.A0N()) {
                    r1 = AnonymousClass10O.RED_DOT;
                } else {
                    r22.A03(C15400vE.A02);
                    return;
                }
                r22.A03(new C15400vE(size, r1));
            } catch (Throwable th) {
                readLock.unlock();
                throw th;
            }
        }
    }

    public void A03() {
        Lock writeLock = this.A06.writeLock();
        writeLock.lock();
        try {
            this.A05.clear();
            this.A05.addAll(this.A04.keySet());
            A01();
            A02(this);
        } finally {
            writeLock.unlock();
        }
    }

    public void A04() {
        Lock writeLock = this.A06.writeLock();
        writeLock.lock();
        try {
            this.A05.clear();
            A01();
            A02(this);
        } finally {
            writeLock.unlock();
        }
    }

    public void A05(AnonymousClass12B r3) {
        Lock writeLock = this.A06.writeLock();
        writeLock.lock();
        try {
            this.A02 = r3;
        } finally {
            writeLock.unlock();
        }
    }

    public void A06(List list) {
        long j;
        Lock writeLock = this.A06.writeLock();
        writeLock.lock();
        try {
            this.A04.clear();
            Optional A072 = ((C624531u) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BUS, this.A01)).A07("montage_last_seen_timestamp");
            if (A072.isPresent()) {
                j = ((Long) A072.get()).longValue();
            } else {
                j = 0;
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                BasicMontageThreadInfo basicMontageThreadInfo = (BasicMontageThreadInfo) it.next();
                if (basicMontageThreadInfo.A07 && basicMontageThreadInfo.A01.A03 > j) {
                    this.A04.put(Long.valueOf(basicMontageThreadInfo.A02.A00), basicMontageThreadInfo.A03);
                    this.A00 = Math.max(this.A00, basicMontageThreadInfo.A01.A03);
                }
            }
            A02(this);
        } finally {
            writeLock.unlock();
        }
    }

    public boolean A07() {
        Lock readLock = this.A06.readLock();
        readLock.lock();
        try {
            return AnonymousClass07B.A01.equals(this.A03);
        } finally {
            readLock.unlock();
        }
    }

    public boolean A08(long j) {
        Lock readLock = this.A06.readLock();
        readLock.lock();
        try {
            return this.A05.contains(Long.valueOf(j));
        } finally {
            readLock.unlock();
        }
    }

    private AnonymousClass12I(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(3, r3);
    }
}
