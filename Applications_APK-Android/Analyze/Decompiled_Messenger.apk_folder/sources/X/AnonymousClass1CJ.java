package X;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import java.util.Iterator;

/* renamed from: X.1CJ  reason: invalid class name */
public abstract class AnonymousClass1CJ implements Iterable {
    public final Optional A00;

    public static AnonymousClass1CJ A00(Iterable iterable) {
        if (iterable instanceof AnonymousClass1CJ) {
            return (AnonymousClass1CJ) iterable;
        }
        return new C146986sL(iterable, iterable);
    }

    public final Optional A02() {
        Iterator it = ((Iterable) this.A00.or(this)).iterator();
        if (it.hasNext()) {
            return Optional.of(it.next());
        }
        return Optional.absent();
    }

    public final AnonymousClass1CJ A03(int i) {
        Iterable iterable = (Iterable) this.A00.or(this);
        Preconditions.checkNotNull(iterable);
        boolean z = false;
        if (i >= 0) {
            z = true;
        }
        Preconditions.checkArgument(z, "limit is negative");
        return A00(new C186958mF(iterable, i));
    }

    public final AnonymousClass1CJ A04(Function function) {
        return A00(AnonymousClass0j4.A00((Iterable) this.A00.or(this), function));
    }

    public final AnonymousClass1CJ A05(Predicate predicate) {
        return A00(AnonymousClass0j4.A01((Iterable) this.A00.or(this), predicate));
    }

    public final ImmutableList A06() {
        return ImmutableList.copyOf((Iterable) this.A00.or(this));
    }

    public String toString() {
        return AnonymousClass0j4.A08((Iterable) this.A00.or(this));
    }

    public static AnonymousClass1CJ A01(Iterable iterable, Iterable iterable2) {
        ImmutableList of = ImmutableList.of(iterable, iterable2);
        Preconditions.checkNotNull(of);
        return new AnonymousClass1CD(of);
    }

    public AnonymousClass1CJ() {
        this.A00 = Optional.absent();
    }

    public AnonymousClass1CJ(Iterable iterable) {
        Preconditions.checkNotNull(iterable);
        this.A00 = Optional.fromNullable(this == iterable ? null : iterable);
    }
}
