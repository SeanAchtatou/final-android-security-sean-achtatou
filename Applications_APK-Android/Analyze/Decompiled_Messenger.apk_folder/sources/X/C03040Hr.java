package X;

import android.content.Context;
import android.content.ServiceConnection;
import com.facebook.push.fbns.ipc.FbnsAIDLRequest;
import com.facebook.push.fbns.ipc.IFbnsAIDLService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: X.0Hr  reason: invalid class name and case insensitive filesystem */
public final class C03040Hr {
    public int A00 = 0;
    public IFbnsAIDLService A01 = null;
    public Integer A02 = AnonymousClass07B.A00;
    public final Context A03;
    public final ServiceConnection A04 = new C03050Hs(this);
    public final ExecutorService A05 = Executors.newFixedThreadPool(5);

    public static void A00(C03040Hr r3) {
        synchronized (r3) {
            int i = r3.A00 - 1;
            r3.A00 = i;
            if (i == 0) {
                A01(r3);
                C006406k.A01(r3.A03, r3.A04, -193509335);
            }
        }
        Thread.currentThread().getId();
    }

    public static synchronized void A01(C03040Hr r1) {
        synchronized (r1) {
            r1.A01 = null;
            r1.A02 = AnonymousClass07B.A00;
        }
    }

    public static synchronized boolean A02(C03040Hr r3) {
        boolean z;
        synchronized (r3) {
            z = false;
            if (r3.A02 == AnonymousClass07B.A0C) {
                z = true;
            }
        }
        return z;
    }

    public void A03(FbnsAIDLRequest... fbnsAIDLRequestArr) {
        for (FbnsAIDLRequest r0 : fbnsAIDLRequestArr) {
            AnonymousClass07A.A03(this.A05, new C03090Hy(this, r0), -853626468);
        }
    }

    public C03040Hr(Context context) {
        this.A03 = context;
    }

    public void finalize() {
        int A032 = C000700l.A03(-159149713);
        this.A05.shutdown();
        C000700l.A09(1194522284, A032);
    }
}
