package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.profilo.multiprocess.ProfiloIPCParcelable;

/* renamed from: X.08A  reason: invalid class name */
public final class AnonymousClass08A implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new ProfiloIPCParcelable(parcel);
    }

    public Object[] newArray(int i) {
        return new ProfiloIPCParcelable[i];
    }
}
