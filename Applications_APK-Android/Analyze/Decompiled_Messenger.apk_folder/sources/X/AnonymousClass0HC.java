package X;

import android.content.SharedPreferences;
import android.os.Bundle;

/* renamed from: X.0HC  reason: invalid class name */
public final class AnonymousClass0HC extends AnonymousClass0HB {
    public Object A01(SharedPreferences sharedPreferences, String str, Object obj) {
        return sharedPreferences.getString(str, (String) obj);
    }

    public Object A02(Bundle bundle, String str, Object obj) {
        return bundle.getString(str, (String) obj);
    }

    public void A03(SharedPreferences.Editor editor, String str, Object obj) {
        editor.putString(str, (String) obj);
    }

    public void A04(Bundle bundle, String str, Object obj) {
        bundle.putString(str, (String) obj);
    }

    public Class A00() {
        return String.class;
    }
}
