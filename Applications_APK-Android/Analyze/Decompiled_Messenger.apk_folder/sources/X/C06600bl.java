package X;

import android.content.IntentFilter;
import android.os.Handler;
import java.util.Map;

/* renamed from: X.0bl  reason: invalid class name and case insensitive filesystem */
public final class C06600bl {
    public IntentFilter A00;
    private Handler A01;
    private final Map A02 = AnonymousClass0TG.A04();
    public final /* synthetic */ C04450Us A03;

    public synchronized C06790c5 A00() {
        C06790c5 r4;
        r4 = new C06790c5(this.A03, this.A02, this.A00, this.A01);
        for (AnonymousClass06U r1 : this.A02.values()) {
            if (r1 instanceof C06800c6) {
                ((C06800c6) r1).A00 = r4;
            }
        }
        return r4;
    }

    public synchronized void A01(Handler handler) {
        this.A01 = handler;
    }

    public synchronized void A02(String str, AnonymousClass06U r3) {
        this.A02.put(str, r3);
    }

    public C06600bl(C04450Us r2) {
        this.A03 = r2;
    }
}
