package X;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/* renamed from: X.1X2  reason: invalid class name */
public final class AnonymousClass1X2 {
    private Boolean A00;
    private final boolean A01;
    public final /* synthetic */ FirebaseInstanceId A02;

    public final synchronized boolean A00() {
        Boolean bool = this.A00;
        if (bool != null) {
            return bool.booleanValue();
        }
        if (this.A01) {
            AnonymousClass1WN r0 = this.A02.A02;
            AnonymousClass1WN.A02(r0);
            if (r0.A08.get()) {
                return true;
            }
        }
        return false;
    }

    public AnonymousClass1X2(FirebaseInstanceId firebaseInstanceId, AnonymousClass1Wh r8) {
        boolean z;
        Boolean bool;
        ApplicationInfo applicationInfo;
        Bundle bundle;
        this.A02 = firebaseInstanceId;
        try {
            Class.forName("com.google.firebase.messaging.FirebaseMessaging");
        } catch (ClassNotFoundException unused) {
            AnonymousClass1WN r0 = firebaseInstanceId.A02;
            AnonymousClass1WN.A02(r0);
            Context context = r0.A00;
            Intent intent = new Intent(AnonymousClass24B.$const$string(926));
            intent.setPackage(context.getPackageName());
            ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
            z = (resolveService == null || resolveService.serviceInfo == null) ? false : z;
        }
        z = true;
        this.A01 = z;
        AnonymousClass1WN r02 = this.A02.A02;
        AnonymousClass1WN.A02(r02);
        Context context2 = r02.A00;
        SharedPreferences sharedPreferences = context2.getSharedPreferences("com.google.firebase.messaging", 0);
        if (sharedPreferences.contains("auto_init")) {
            bool = Boolean.valueOf(sharedPreferences.getBoolean("auto_init", false));
        } else {
            try {
                PackageManager packageManager = context2.getPackageManager();
                if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(context2.getPackageName(), 128)) == null || (bundle = applicationInfo.metaData) == null || !bundle.containsKey("firebase_messaging_auto_init_enabled"))) {
                    bool = Boolean.valueOf(applicationInfo.metaData.getBoolean("firebase_messaging_auto_init_enabled"));
                }
            } catch (PackageManager.NameNotFoundException unused2) {
            }
            bool = null;
        }
        this.A00 = bool;
        if (bool == null && this.A01) {
            AnonymousClass1X3 r4 = new AnonymousClass1X3(this);
            Class<AnonymousClass1X4> cls = AnonymousClass1X4.class;
            Executor executor = r8.A02;
            synchronized (r8) {
                AnonymousClass09E.A01(cls);
                AnonymousClass09E.A01(r4);
                AnonymousClass09E.A01(executor);
                if (!r8.A01.containsKey(cls)) {
                    r8.A01.put(cls, new ConcurrentHashMap());
                }
                ((ConcurrentHashMap) r8.A01.get(cls)).put(r4, executor);
            }
        }
    }
}
