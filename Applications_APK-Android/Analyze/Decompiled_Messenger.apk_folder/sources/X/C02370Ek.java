package X;

import java.io.IOException;

/* renamed from: X.0Ek  reason: invalid class name and case insensitive filesystem */
public final class C02370Ek {
    private C02380El A00;

    public C02370Ek(C02380El r1) {
        this.A00 = r1;
    }

    public void A00(String str) {
        String A03 = C02420Er.A03(str);
        if (A03 != null) {
            try {
                this.A00.A03(A03);
            } catch (IOException e) {
                String format = String.format("Unable to load module %s for class %s", A03, str);
                C010708t.A0L("Voltron", format, e);
                throw new RuntimeException(format, e);
            }
        }
    }
}
