package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/* renamed from: X.0jA  reason: invalid class name */
public final class AnonymousClass0jA {
    public final ImmutableMap A00;

    public AnonymousClass0jA(ImmutableList immutableList) {
        ImmutableMap.Builder builder = ImmutableMap.builder();
        C24971Xv it = immutableList.iterator();
        while (it.hasNext()) {
            C09550i0 r1 = (C09550i0) it.next();
            builder.put(r1.A00, r1);
        }
        this.A00 = builder.build();
    }
}
