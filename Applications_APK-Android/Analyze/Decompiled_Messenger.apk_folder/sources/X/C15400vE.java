package X;

import java.util.Arrays;

/* renamed from: X.0vE  reason: invalid class name and case insensitive filesystem */
public final class C15400vE {
    public static final C15400vE A02 = new C15400vE(0, AnonymousClass10O.RED_WITH_TEXT);
    public final int A00;
    public final AnonymousClass10O A01;

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        C15400vE r4 = (C15400vE) obj;
        return this.A00 == r4.A00 && this.A01 == r4.A01;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), this.A01});
    }

    public C15400vE(int i, AnonymousClass10O r2) {
        this.A00 = i;
        this.A01 = r2;
    }
}
