package X;

import com.facebook.common.dextricks.DalvikConstants;
import com.facebook.common.dextricks.DexStore;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1AL  reason: invalid class name */
public final class AnonymousClass1AL implements C17480yy, C17500z0 {
    private float A00;
    private float A01;
    private float A02;
    private float A03;
    private float A04;
    private float A05;
    private float A06;
    private float A07;
    private float A08;
    private float A09;
    private float A0A;
    private int A0B;
    private int A0C;
    private int A0D;
    private int A0E;
    private int A0F;
    private int A0G;
    private int A0H;
    private int A0I;
    private AnonymousClass1IF A0J;
    private AnonymousClass1IF A0K;
    private AnonymousClass1IF A0L;
    private AnonymousClass1IF A0M;
    private AnonymousClass1IF A0N;
    private AnonymousClass1IF A0O;
    private C14940uO A0P;
    private C17660zG A0Q;
    private AnonymousClass1LC A0R;
    private List A0S;
    private boolean A0T;
    private boolean A0U;

    public void ANz(C14940uO r2) {
        this.A0H |= DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED;
        this.A0P = r2;
    }

    public void AOt(float f) {
        this.A0H |= DexStore.LOAD_RESULT_WITH_VDEX_ODEX;
        this.A00 = f;
    }

    public void AUX(Object obj) {
        C17480yy r8 = (C17480yy) obj;
        if (((long) (this.A0H & 1)) != 0) {
            r8.CNK(this.A0I);
        }
        if (((long) (this.A0H & 2)) != 0) {
            r8.CNJ(this.A0A);
        }
        if (((long) (this.A0H & 4)) != 0) {
            r8.BLA(this.A0G);
        }
        if (((long) (this.A0H & 8)) != 0) {
            r8.BL9(this.A09);
        }
        if (((long) (this.A0H & 16)) != 0) {
            r8.BKL(this.A0E);
        }
        if (((long) (this.A0H & 32)) != 0) {
            r8.BKK(this.A07);
        }
        if (((long) (this.A0H & 64)) != 0) {
            r8.BC2(this.A0C);
        }
        if (((long) (this.A0H & 128)) != 0) {
            r8.BC1(this.A05);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_OATMEAL_QUICKENED)) != 0) {
            r8.BL8(this.A0F);
        }
        if (((long) (this.A0H & 512)) != 0) {
            r8.BL7(this.A08);
        }
        if (((long) (this.A0H & 1024)) != 0) {
            r8.BKH(this.A0D);
        }
        if (((long) (this.A0H & 2048)) != 0) {
            r8.BKG(this.A06);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED)) != 0) {
            r8.BHv(this.A0Q);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_MIXED_MODE_ATTEMPTED)) != 0) {
            r8.ANz(this.A0P);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET)) != 0) {
            r8.Aa6(this.A01);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_PGO)) != 0) {
            r8.AaB(this.A03);
        }
        if (((long) (this.A0H & 65536)) != 0) {
            r8.AaD(this.A04);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP)) != 0) {
            r8.Aa8(this.A0B);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED)) != 0) {
            r8.Aa7(this.A02);
        }
        if (((long) (this.A0H & DexStore.LOAD_RESULT_WITH_VDEX_ODEX)) != 0) {
            r8.AOt(this.A00);
        }
        if (((long) (this.A0H & 1048576)) != 0) {
            r8.BxJ(this.A0R);
        }
        if (((long) (this.A0H & 2097152)) != 0) {
            for (int i = 0; i < AnonymousClass1IF.A03; i++) {
                float A002 = this.A0O.A00(i);
                if (!AnonymousClass1K2.A00(A002)) {
                    r8.BxI(AnonymousClass10G.A00(i), (int) A002);
                }
            }
        }
        if (((long) (this.A0H & 4194304)) != 0) {
            for (int i2 = 0; i2 < AnonymousClass1IF.A03; i2++) {
                float A003 = this.A0N.A00(i2);
                if (!AnonymousClass1K2.A00(A003)) {
                    r8.BxH(AnonymousClass10G.A00(i2), A003);
                }
            }
        }
        if (((long) (this.A0H & DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE)) != 0) {
            for (int i3 = 0; i3 < AnonymousClass1IF.A03; i3++) {
                float A004 = this.A0M.A00(i3);
                if (!AnonymousClass1K2.A00(A004)) {
                    r8.BwM(AnonymousClass10G.A00(i3), (int) A004);
                }
            }
        }
        if (((long) (this.A0H & 16777216)) != 0) {
            for (int i4 = 0; i4 < AnonymousClass1IF.A03; i4++) {
                float A005 = this.A0L.A00(i4);
                if (!AnonymousClass1K2.A00(A005)) {
                    r8.BwL(AnonymousClass10G.A00(i4), A005);
                }
            }
        }
        if (((long) (this.A0H & 33554432)) != 0) {
            for (int i5 = 0; i5 < AnonymousClass1IF.A03; i5++) {
                float A006 = this.A0K.A00(i5);
                if (!AnonymousClass1K2.A00(A006)) {
                    r8.BJx(AnonymousClass10G.A00(i5), (int) A006);
                }
            }
        }
        if (((long) (this.A0H & 67108864)) != 0) {
            for (int i6 = 0; i6 < AnonymousClass1IF.A03; i6++) {
                float A007 = this.A0J.A00(i6);
                if (!AnonymousClass1K2.A00(A007)) {
                    r8.BJv(AnonymousClass10G.A00(i6), A007);
                }
            }
        }
        if (((long) (this.A0H & 134217728)) != 0) {
            for (AnonymousClass10G BJt : this.A0S) {
                r8.BJt(BJt);
            }
        }
        if (((long) (this.A0H & 268435456)) != 0) {
            r8.BGL(this.A0T);
        }
        if (((long) (this.A0H & 536870912)) != 0) {
            r8.CLJ(this.A0U);
        }
    }

    public void Aa6(float f) {
        this.A0H |= DexStore.LOAD_RESULT_DEX2OAT_CLASSPATH_SET;
        this.A01 = f;
    }

    public void Aa7(float f) {
        this.A0H |= DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP_ATTEMPTED;
        this.A02 = f;
    }

    public void Aa8(int i) {
        this.A0H |= DexStore.LOAD_RESULT_DEX2OAT_TRY_PERIODIC_PGO_COMP;
        this.A0B = i;
    }

    public void AaB(float f) {
        this.A0H |= DexStore.LOAD_RESULT_PGO;
        this.A03 = f;
    }

    public void AaD(float f) {
        this.A0H |= 65536;
        this.A04 = f;
    }

    public void BC1(float f) {
        this.A0H |= 128;
        this.A05 = f;
    }

    public void BC2(int i) {
        this.A0H |= 64;
        this.A0C = i;
    }

    public void BGL(boolean z) {
        this.A0H |= 268435456;
        this.A0T = z;
    }

    public void BHv(C17660zG r2) {
        this.A0H |= DexStore.LOAD_RESULT_DEX2OAT_QUICKEN_ATTEMPTED;
        this.A0Q = r2;
    }

    public void BJt(AnonymousClass10G r3) {
        this.A0H |= 134217728;
        if (this.A0S == null) {
            this.A0S = new ArrayList(2);
        }
        this.A0S.add(r3);
    }

    public void BJv(AnonymousClass10G r3, float f) {
        this.A0H |= 67108864;
        if (this.A0J == null) {
            this.A0J = new AnonymousClass1IF();
        }
        this.A0J.A03(r3, f);
    }

    public void BJx(AnonymousClass10G r3, int i) {
        this.A0H |= 33554432;
        if (this.A0K == null) {
            this.A0K = new AnonymousClass1IF();
        }
        this.A0K.A03(r3, (float) i);
    }

    public void BKG(float f) {
        this.A0H |= 2048;
        this.A06 = f;
    }

    public void BKH(int i) {
        this.A0H |= 1024;
        this.A0D = i;
    }

    public void BKK(float f) {
        this.A0H |= 32;
        this.A07 = f;
    }

    public void BKL(int i) {
        this.A0H |= 16;
        this.A0E = i;
    }

    public void BL7(float f) {
        this.A0H |= 512;
        this.A08 = f;
    }

    public void BL8(int i) {
        this.A0H |= DexStore.LOAD_RESULT_OATMEAL_QUICKENED;
        this.A0F = i;
    }

    public void BL9(float f) {
        this.A0H |= 8;
        this.A09 = f;
    }

    public void BLA(int i) {
        this.A0H |= 4;
        this.A0G = i;
    }

    public void BwL(AnonymousClass10G r3, float f) {
        this.A0H |= 16777216;
        if (this.A0L == null) {
            this.A0L = new AnonymousClass1IF();
        }
        this.A0L.A03(r3, f);
    }

    public void BwM(AnonymousClass10G r3, int i) {
        this.A0H |= DalvikConstants.FB4A_LINEAR_ALLOC_BUFFER_SIZE;
        if (this.A0M == null) {
            this.A0M = new AnonymousClass1IF();
        }
        this.A0M.A03(r3, (float) i);
    }

    public void BxH(AnonymousClass10G r3, float f) {
        this.A0H |= 4194304;
        if (this.A0N == null) {
            this.A0N = new AnonymousClass1IF();
        }
        this.A0N.A03(r3, f);
    }

    public void BxI(AnonymousClass10G r3, int i) {
        this.A0H |= 2097152;
        if (this.A0O == null) {
            this.A0O = new AnonymousClass1IF();
        }
        this.A0O.A03(r3, (float) i);
    }

    public void BxJ(AnonymousClass1LC r3) {
        this.A0H |= 1048576;
        this.A0R = r3;
    }

    public void CLJ(boolean z) {
        this.A0H |= 536870912;
        this.A0U = z;
    }

    public void CNJ(float f) {
        this.A0H |= 2;
        this.A0A = f;
    }

    public void CNK(int i) {
        this.A0H |= 1;
        this.A0I = i;
    }
}
