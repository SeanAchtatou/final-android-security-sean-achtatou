package X;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import io.card.payment.BuildConfig;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Queue;

/* renamed from: X.0Ag  reason: invalid class name and case insensitive filesystem */
public final class C01440Ag {
    private int A00;
    private Notification.Builder A01;
    private String A02 = BuildConfig.FLAVOR;
    private Queue A03;
    public final Context A04;
    private final int A05;
    private final NotificationManager A06;
    private final String A07;
    private final String A08;
    private final boolean A09;

    private Notification.InboxStyle A00() {
        Notification.InboxStyle summaryText = new Notification.InboxStyle().setBigContentTitle(AnonymousClass08S.A0P("[", this.A07, "]")).setSummaryText(this.A08);
        for (CharSequence addLine : this.A03) {
            summaryText.addLine(addLine);
        }
        return summaryText;
    }

    public void A01(String str) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        if (!this.A09) {
            try {
                this.A06.cancel("MqttDiagnosticNotification", this.A05);
            } catch (RuntimeException unused) {
            }
        } else {
            synchronized (this) {
                this.A00 = 0;
                this.A02 = str;
                int i = -65536;
                int i2 = 17301608;
                if ("CONNECTED".equals(str)) {
                    i2 = 17301611;
                    i = -16711936;
                } else if ("CONNECTING".equals(str)) {
                    i2 = 17301607;
                    i = -256;
                }
                Notification.Builder contentText = new Notification.Builder(this.A04).setChannelId("debug_channel").setSmallIcon(i2).setContentIntent(PendingIntent.getActivity(this.A04, 0, new Intent().setPackage(this.A04.getPackageName()), 0)).setContentTitle(AnonymousClass08S.A0P("[", this.A07, "]")).setContentText(str);
                this.A01 = contentText;
                int i3 = Build.VERSION.SDK_INT;
                if (i3 >= 21) {
                    contentText.setColor(i);
                }
                if (i3 >= 16) {
                    this.A01.setStyle(A00());
                }
                this.A06.notify("MqttDiagnosticNotification", this.A05, this.A01.getNotification());
            }
        }
    }

    public void A02(String str) {
        int i = Build.VERSION.SDK_INT;
        if (i < 16) {
            return;
        }
        if (!this.A09) {
            try {
                this.A06.cancel("MqttDiagnosticNotification", this.A05);
            } catch (RuntimeException unused) {
            }
        } else if (this.A01 != null) {
            synchronized (this) {
                if (i >= 24) {
                    Notification.Builder builder = this.A01;
                    int i2 = this.A00 + 1;
                    this.A00 = i2;
                    builder.setSubText(String.valueOf(i2));
                } else {
                    Notification.Builder builder2 = this.A01;
                    int i3 = this.A00 + 1;
                    this.A00 = i3;
                    builder2.setContentInfo(String.valueOf(i3));
                }
                this.A03.add(AnonymousClass08S.A0P(new SimpleDateFormat("h:mm:ss a").format(new Date()), " ", str));
                if (this.A03.size() > 8) {
                    this.A03.poll();
                }
                this.A01.setContentText(this.A02);
                this.A01.setStyle(A00());
                this.A06.notify("MqttDiagnosticNotification", this.A05, this.A01.getNotification());
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C01440Ag(android.content.Context r7, java.lang.String r8, boolean r9, X.AnonymousClass0AW r10) {
        /*
            r6 = this;
            r6.<init>()
            java.lang.String r0 = ""
            r6.A02 = r0
            r2 = 0
            r6.A00 = r2
            r6.A07 = r8
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 >= r0) goto L_0x0016
            r0 = 0
            r6.A04 = r0
            return
        L_0x0016:
            r6.A04 = r7
            X.07y r1 = X.C009207y.A01
            java.lang.Class<android.app.NotificationManager> r0 = android.app.NotificationManager.class
            java.lang.String r4 = "notification"
            java.lang.Object r0 = r1.A01(r7, r4, r0)
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0
            r6.A06 = r0
            int r5 = android.os.Process.myPid()     // Catch:{ all -> 0x0053 }
            android.content.Context r1 = r6.A04     // Catch:{ all -> 0x0053 }
            java.lang.String r0 = "activity"
            java.lang.Object r0 = r1.getSystemService(r0)     // Catch:{ all -> 0x0053 }
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0     // Catch:{ all -> 0x0053 }
            java.util.List r0 = r0.getRunningAppProcesses()     // Catch:{ all -> 0x0053 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x0053 }
        L_0x003c:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0053 }
            if (r0 == 0) goto L_0x0053
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x0053 }
            android.app.ActivityManager$RunningAppProcessInfo r1 = (android.app.ActivityManager.RunningAppProcessInfo) r1     // Catch:{ all -> 0x0053 }
            int r0 = r1.pid     // Catch:{ all -> 0x0053 }
            if (r0 != r5) goto L_0x003c
            java.lang.String r0 = r1.processName     // Catch:{ all -> 0x0053 }
            int r0 = r0.hashCode()     // Catch:{ all -> 0x0053 }
            goto L_0x0055
        L_0x0053:
            r0 = 42
        L_0x0055:
            r6.A05 = r0
            java.lang.String r3 = "Started on "
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.lang.String r0 = "M/d h:mm:ss a"
            r1.<init>(r0)
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            java.lang.String r0 = r1.format(r0)
            java.lang.String r0 = X.AnonymousClass08S.A0J(r3, r0)
            r6.A08 = r0
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
            r6.A03 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A05
            X.0B0 r1 = r10.AbP(r0)
            if (r9 != 0) goto L_0x0086
            java.lang.String r0 = "is_on"
            boolean r0 = r1.getBoolean(r0, r2)
            if (r0 == 0) goto L_0x0087
        L_0x0086:
            r2 = 1
        L_0x0087:
            r6.A09 = r2
            if (r2 == 0) goto L_0x00a0
            android.app.NotificationChannel r3 = new android.app.NotificationChannel
            r2 = 2
            java.lang.String r1 = "debug_channel"
            java.lang.String r0 = "Debugging Information"
            r3.<init>(r1, r0, r2)
            android.content.Context r0 = r6.A04
            java.lang.Object r0 = r0.getSystemService(r4)
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0
            r0.createNotificationChannel(r3)
        L_0x00a0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C01440Ag.<init>(android.content.Context, java.lang.String, boolean, X.0AW):void");
    }
}
