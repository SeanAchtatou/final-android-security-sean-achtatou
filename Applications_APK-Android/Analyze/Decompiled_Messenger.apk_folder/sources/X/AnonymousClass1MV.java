package X;

import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.1MV  reason: invalid class name */
public abstract class AnonymousClass1MV {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "SIZE_20";
            case 2:
                return "SIZE_28";
            case 3:
                return "SIZE_32";
            case 4:
                return "SIZE_40";
            default:
                return "SIZE_16";
        }
    }

    public int A01(int i) {
        switch (i) {
            case 65539:
                return 2131231255;
            case 131075:
                return 2131231256;
            case 196611:
                return 2132346283;
            case 262147:
                return 2132346284;
            case 327683:
                return 2131231257;
            case 393219:
                return 2131231258;
            case 458755:
                return 2131231259;
            case 524291:
                return 2131231260;
            case 589827:
                return 2131231261;
            case 655363:
                return 2131231262;
            case 720899:
                return 2131231263;
            case 786435:
                return 2132346285;
            case 851971:
                return 2131231264;
            case 917507:
                return 2132346286;
            case 983043:
                return 2131231265;
            case 1048579:
                return 2131231266;
            case 1114115:
                return 2131231267;
            case 1179650:
                return 2132346287;
            case 1179651:
                return 2132346288;
            case 1245186:
                return 2132346289;
            case 1245187:
                return 2132346290;
            case 1310723:
                return 2131231268;
            case 1376258:
                return 2132346291;
            case 1376259:
                return 2131231269;
            case 1441795:
                return 2131231270;
            case 1507331:
                return 2131231271;
            case 1572867:
                return 2131231272;
            case 1638403:
                return 2131231273;
            case 1703939:
                return 2131231274;
            case 1769475:
                return 2131231275;
            case 1835011:
                return 2132346292;
            case 1900547:
                return 2132346293;
            case 1966083:
                return 2131231276;
            case 2031619:
                return 2131231277;
            case 2097155:
                return 2131231279;
            case 2162691:
                return 2132346294;
            case 2228227:
                return 2132346295;
            case 2293764:
                return 2132346296;
            case 2359299:
                return 2132346297;
            case 2424835:
                return 2131231280;
            case 2490371:
                return 2132346298;
            case 2555907:
                return 2131231281;
            case 2621443:
                return 2132346299;
            case 2686979:
                return 2132346300;
            case 2752515:
                return 2132346301;
            case 2818051:
                return 2132346302;
            case 2883587:
                return 2132346303;
            case 2949123:
                return 2132346304;
            case 3014659:
                return 2132346305;
            case 3080195:
                return 2131231282;
            case 3145731:
                return 2131231283;
            case 3211267:
                return 2131231284;
            case 3276803:
                return 2132346306;
            case 3342339:
                return 2131231285;
            case 3407875:
                return 2131231286;
            case 3473411:
                return 2132346307;
            case 3538947:
                return 2132346308;
            case 3604483:
                return 2131231287;
            case 3670019:
                return 2131231288;
            case 3735555:
                return 2132346309;
            case 3801091:
                return 2131231289;
            case 3866627:
                return 2131231290;
            case 3932163:
                return 2131231291;
            case 3997699:
                return 2131231292;
            case 4063235:
                return 2132346310;
            case 4128771:
                return 2132346311;
            case 4194307:
                return 2132346312;
            case 4259843:
                return 2131231293;
            case 4325379:
                return 2131231294;
            case 4390915:
                return 2131231295;
            case 4456451:
                return 2131231296;
            case 4521987:
                return 2132346313;
            case 4587523:
                return 2131231297;
            case 4653059:
                return 2132346314;
            case 4718595:
                return 2132346315;
            case 4784131:
                return 2132346316;
            case 4849667:
                return 2131231298;
            case 4915203:
                return 2131231299;
            case 4980739:
                return 2131231300;
            case 5046272:
                return 2131231301;
            case 5111811:
                return 2132346317;
            case 5177347:
                return 2132346318;
            case 5242880:
                return 2131231302;
            case 5242883:
                return 2132346319;
            case 5308419:
                return 2132346320;
            case 5373955:
                return 2131231303;
            case 5439491:
                return 2132346321;
            case 5505027:
                return 2131231304;
            case 5570563:
                return 2131231305;
            case 5636099:
                return 2131231306;
            case 5701635:
                return 2132346322;
            case 5767171:
                return 2132346323;
            case 5832704:
                return 2132346324;
            case 5832707:
                return 2131231307;
            case 5898243:
                return 2132346325;
            case 5963779:
                return 2131231308;
            case 6029315:
                return 2132346326;
            case 6094850:
                return 2132346327;
            case 6094851:
                return 2132346328;
            case 6160386:
                return 2132346329;
            case 6160387:
                return 2132346330;
            case 6225923:
                return 2131231309;
            case 6291459:
                return 2131231310;
            case 6356995:
                return 2132346331;
            case 6422531:
                return 2131231311;
            case 6488067:
                return 2131231312;
            case 6553603:
                return 2132346332;
            case 6619139:
                return 2131231313;
            case 6684675:
                return 2131231314;
            case 6750211:
                return 2131231315;
            case 6815747:
                return 2132346333;
            case 6881283:
                return 2132346334;
            case 6946818:
                return 2132346335;
            case 6946819:
                return 2132346336;
            case 7012355:
                return 2132346337;
            case 7077891:
                return 2131231316;
            case 7143427:
                return 2131231317;
            case 7208963:
                return 2131231318;
            case 7274499:
                return 2131231319;
            case 7340035:
                return 2132346338;
            case 7405571:
                return 2131231320;
            case 7471107:
                return 2131231321;
            case 7536643:
                return 2131231322;
            case 7602179:
                return 2131231323;
            case 7667715:
                return 2132346339;
            case 7733251:
                return 2132346340;
            case 7798787:
                return 2131231324;
            case 7864323:
                return 2132346341;
            case 7929859:
                return 2132346342;
            case 7995395:
                return 2131231325;
            case 8060931:
                return 2131231326;
            case 8126467:
                return 2131231327;
            case 8192003:
                return 2132346343;
            case 8257539:
                return 2131231328;
            case 8323075:
                return 2131231329;
            case 8388611:
                return 2131231330;
            case 8454147:
                return 2132346344;
            case 8519683:
                return 2131231331;
            case 8585217:
                return 2132346345;
            case 8650752:
                return 2132346346;
            case 8716288:
                return 2132346347;
            case 8781824:
                return 2132346348;
            case 8847360:
                return 2132346349;
            case 8912896:
                return 2132346350;
            case 8978435:
                return 2132346351;
            case 9043971:
                return 2131231332;
            case 9109507:
                return 2131231333;
            case 9175043:
                return 2131231334;
            case 9240579:
                return 2131231335;
            case 9306115:
                return 2131231336;
            case 9371651:
                return 2131231337;
            case 9437187:
                return 2132346352;
            case 9502723:
                return 2132346353;
            case 9568259:
                return 2132346354;
            case 9633795:
                return 2132346355;
            case 9699331:
                return 2131231338;
            default:
                return 0;
        }
    }

    public final int A02(C29631gj r10, Integer num) {
        String str;
        int ordinal = r10.ordinal() << 16;
        int A01 = A01(num.intValue() | ordinal);
        if (A01 == 0) {
            ArrayList arrayList = new ArrayList();
            for (Integer num2 : AnonymousClass07B.A00(5)) {
                r10.ordinal();
                boolean z = false;
                if (A01(num2.intValue() | ordinal) != 0) {
                    z = true;
                }
                if (z) {
                    arrayList.add(A00(num2));
                }
            }
            String arrays = Arrays.toString(arrayList.toArray());
            StringBuilder sb = new StringBuilder("MIGIconName '");
            sb.append(r10);
            sb.append("' is not present in size '");
            if (num != null) {
                str = A00(num);
            } else {
                str = "null";
            }
            sb.append(str);
            sb.append("'. The available sizes for this icon are: ");
            sb.append(arrays);
            sb.append(". Please find the icon in the Asset Manager and follow the ");
            sb.append("instructions there to add it to the asset bundle.");
            C010708t.A07(AnonymousClass1MV.class, sb.toString());
        }
        return A01;
    }
}
