package X;

import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.common.banner.BasicBannerNotificationView;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import io.card.payment.BuildConfig;
import java.util.Date;

/* renamed from: X.0xc  reason: invalid class name and case insensitive filesystem */
public final class C16690xc extends C32501lr {
    public final AnonymousClass0ZM A00 = new C16720xf(this);
    public final FbSharedPreferences A01;
    private final Context A02;
    private final Resources A03;
    private final LayoutInflater A04;

    private C16690xc(AnonymousClass1XY r2, Context context) {
        super("NetworkEmpathyStatusNotification");
        this.A04 = C04490Ux.A0e(r2);
        this.A01 = FbSharedPreferencesModule.A00(r2);
        this.A02 = context;
        this.A03 = context.getResources();
    }

    public static final C16690xc A00(AnonymousClass1XY r2) {
        return new C16690xc(r2, AnonymousClass1YA.A00(r2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View B90(ViewGroup viewGroup) {
        BasicBannerNotificationView basicBannerNotificationView = (BasicBannerNotificationView) this.A04.inflate(2132410497, viewGroup, false);
        long At2 = this.A01.At2(C26211b5.A04, 0);
        String str = BuildConfig.FLAVOR;
        if (At2 > 0) {
            str = AnonymousClass08S.A0J(AnonymousClass08S.A0J(str, " Until "), DateFormat.getTimeFormat(this.A02).format(new Date(At2)));
        }
        C46862Se r2 = new C46862Se();
        r2.A07 = AnonymousClass08S.A0J("[FB-ONLY] 2G Empathy Enabled", str);
        r2.A03 = this.A03.getDrawable(2132082872);
        r2.A02 = AnonymousClass01R.A00(this.A02, 2132082728);
        basicBannerNotificationView.A0T(r2.A00());
        return basicBannerNotificationView;
    }

    public void onResume() {
        FbSharedPreferences fbSharedPreferences = this.A01;
        AnonymousClass1Y7 r2 = C26211b5.A06;
        if (fbSharedPreferences.Aep(r2, false)) {
            this.A00.A06(this);
        } else {
            this.A00.A05(this);
        }
        this.A01.C0f(r2, this.A00);
    }
}
