package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0Cr  reason: invalid class name and case insensitive filesystem */
public enum C02060Cr implements AnonymousClass0C4 {
    PublishAcknowledgementMs("pub", r5),
    StackSendingLatencyMs("s", r5),
    StackReceivingLatencyMs("r", r5);
    
    private final String mJsonKey;
    private final Class mType;

    /* access modifiers changed from: public */
    static {
        Class<AtomicLong> cls = AtomicLong.class;
    }

    private C02060Cr(String str, Class cls) {
        this.mJsonKey = str;
        this.mType = cls;
    }

    public String Arl() {
        return this.mJsonKey;
    }

    public Class B8J() {
        return this.mType;
    }
}
