package X;

import com.facebook.messaging.montage.model.art.BaseItem;
import com.facebook.messaging.montage.model.art.PlaceholderItem;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1Ij  reason: invalid class name and case insensitive filesystem */
public final class C21701Ij extends C20831Dz {
    public int A00 = -1;
    public AnonymousClass0UN A01;
    public C26431CyL A02;
    public DAB A03;
    public DAA A04;
    public C26512Czy A05;
    public Integer A06;
    public boolean A07 = false;

    public int A0G() {
        int i = 0;
        while (true) {
            C26512Czy czy = this.A05;
            if (czy == null || i >= czy.A01.size()) {
                return 0;
            }
            if (!(this.A05.A01.get(i) instanceof PlaceholderItem)) {
                return i;
            }
            i++;
        }
        return 0;
    }

    public static int A00(C21701Ij r1) {
        C26512Czy czy = r1.A05;
        if (czy == null) {
            return 1;
        }
        return czy.A01.size() + 1;
    }

    public static final C21701Ij A01(AnonymousClass1XY r1) {
        return new C21701Ij(r1);
    }

    public static BaseItem A02(C21701Ij r2, int i) {
        Preconditions.checkNotNull(r2.A05);
        Preconditions.checkNotNull(r2.A02);
        int A002 = i % A00(r2);
        ImmutableList immutableList = r2.A05.A01;
        if (!r2.A02.A07) {
            A002--;
        }
        return (BaseItem) immutableList.get(A002);
    }

    public int A0F() {
        if (this.A05 == null) {
            return 0;
        }
        int ArU = ((ArU() >> 1) / A00(this)) * A00(this);
        C26431CyL cyL = this.A02;
        if (cyL == null || !cyL.A0B) {
            return ArU;
        }
        return ArU + 1;
    }

    public int ArU() {
        C26431CyL cyL;
        C26512Czy czy = this.A05;
        if (czy == null || (cyL = this.A02) == null) {
            return 0;
        }
        if (cyL.A0A) {
            return czy.A01.size();
        }
        return Integer.MAX_VALUE;
    }

    private C21701Ij(AnonymousClass1XY r3) {
        this.A01 = new AnonymousClass0UN(6, r3);
    }
}
