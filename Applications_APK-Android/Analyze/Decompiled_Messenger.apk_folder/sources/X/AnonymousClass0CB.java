package X;

import android.net.NetworkInfo;
import android.os.SystemClock;
import android.text.TextUtils;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0CB  reason: invalid class name */
public final class AnonymousClass0CB {
    public final /* synthetic */ AnonymousClass0C8 A00;

    public AnonymousClass0CB(AnonymousClass0C8 r1) {
        this.A00 = r1;
    }

    public void A00() {
        AnonymousClass0C8 r1 = this.A00;
        synchronized (r1) {
            r1.notifyAll();
        }
    }

    public void A01(AnonymousClass0CE r4, AnonymousClass0CG r5, Throwable th) {
        boolean z = false;
        if (this.A00.A0Z == AnonymousClass089.DISCONNECTED) {
            z = true;
        }
        if (!z) {
            AnonymousClass0C8.A03(this.A00, r4, r5, th);
        }
    }

    public void A02(AnonymousClass0CR r22) {
        boolean booleanValue;
        String str;
        AnonymousClass0CR r4 = r22;
        if (r4.A05) {
            AnonymousClass0C8 r2 = this.A00;
            if (!r2.A0M && (str = r2.A0E.A0G) != null) {
                r2.A0A.C73(str);
            }
            this.A00.A0R = SystemClock.elapsedRealtime();
        } else {
            AnonymousClass0C8 r3 = this.A00;
            if (r3.A0M) {
                C01540Aq r23 = r4.A04;
                if (r23.A02() && (r23.A01() == AnonymousClass0SB.A06 || r23.A01() == AnonymousClass0SB.A02)) {
                    r3.A0A.ASh();
                }
            }
        }
        AnonymousClass0C8 r0 = this.A00;
        AnonymousClass0B6 r6 = r0.A06;
        boolean z = r4.A05;
        long elapsedRealtime = SystemClock.elapsedRealtime() - r0.A0Q;
        C01540Aq r1 = r4.A04;
        String str2 = null;
        if (r1.A02()) {
            str2 = ((AnonymousClass0SB) r1.A01()).toString();
        }
        C01540Aq r20 = r4.A01;
        C01540Aq r19 = r4.A00;
        long j = this.A00.A0W;
        long A02 = this.A00.A09.A02();
        NetworkInfo networkInfo = this.A00.A0X;
        C01690Bg r02 = this.A00.A03;
        if (r02 == null) {
            booleanValue = false;
        } else {
            booleanValue = ((Boolean) r02.get()).booleanValue();
        }
        String valueOf = String.valueOf(z);
        String valueOf2 = String.valueOf(elapsedRealtime);
        Map A002 = C01740Bl.A00("connect_result", valueOf, "connect_duration_ms", valueOf2);
        if (str2 != null) {
            A002.put("failure_reason", str2);
        }
        if (r20.A02()) {
            A002.put("exception", ((Exception) r20.A01()).getClass().getCanonicalName());
            A002.put("error_message", ((Exception) r20.A01()).getMessage());
        }
        if (r19.A02()) {
            A002.put("conack_rc", ((Byte) r19.A01()).toString());
        }
        A002.put("fs", String.valueOf(booleanValue));
        A002.put("mqtt_session_id", Long.toString(j));
        A002.put("network_session_id", Long.toString(A02));
        AnonymousClass0B6.A00(r6, A002, networkInfo);
        r6.A07("mqtt_connect_attempt", A002);
        if (r6.A00 != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("connect_result", valueOf);
            hashMap.put("connect_duration_ms", valueOf2);
            if (str2 != null) {
                hashMap.put("failure_reason", str2);
            }
            if (r20.A02()) {
                hashMap.put("exception", ((Exception) r20.A01()).getClass().getCanonicalName());
            }
            r6.A00.BIq("mqtt_connect_attempt", hashMap);
        }
        AnonymousClass0CC r32 = this.A00.A0Y;
        if (r32 != null) {
            if (r4.A05) {
                AnonymousClass00S.A04(r32.A02.A05, new AnonymousClass0CS(r32, r4), -3035496);
            } else {
                AnonymousClass00S.A04(r32.A02.A05, new AnonymousClass0SL(r32, r4), -1300229591);
            }
        }
        if (r4.A05) {
            this.A00.A0D.BKs(0);
        }
    }

    public void A03(AnonymousClass089 r2) {
        this.A00.A0Z = r2;
    }

    public void A04(C02020Cn r15) {
        C01540Aq r0;
        String str;
        AnonymousClass0CU A002 = this.A00.A04.A00();
        AnonymousClass0C8 r02 = this.A00;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        AnonymousClass0CC r5 = r02.A0Y;
        AnonymousClass0CL r4 = r15.A00.A03;
        switch (r4.ordinal()) {
            case 2:
                C02040Cp r7 = (C02040Cp) r15;
                String AWi = this.A00.A0C.AWi(r7);
                int i = r7.A02().A00;
                int i2 = r7.A00.A02;
                Object BJK = this.A00.A02.BJK(AWi, r7.A03());
                if (r5 != null) {
                    byte[] A03 = r7.A03();
                    if ("/send_message_response".equals(AWi) || "/t_sm_rp".equals(AWi)) {
                        ((AtomicLong) ((AnonymousClass08I) r5.A02.A0A.A07(AnonymousClass08I.class)).A00(AnonymousClass08J.A08)).incrementAndGet();
                    }
                    if ("/push_notification".equals(AWi) || "/t_push".equals(AWi)) {
                        ((AtomicLong) ((AnonymousClass08I) r5.A02.A0A.A07(AnonymousClass08I.class)).A00(AnonymousClass08J.A05)).incrementAndGet();
                    }
                    if ("/fbns_msg".equals(AWi)) {
                        ((AtomicLong) ((AnonymousClass08I) r5.A02.A0A.A07(AnonymousClass08I.class)).A00(AnonymousClass08J.A03)).incrementAndGet();
                    }
                    ((AtomicLong) ((AnonymousClass08I) r5.A02.A0A.A07(AnonymousClass08I.class)).A00(AnonymousClass08J.A0D)).incrementAndGet();
                    if (r5.A02.A06 != null && AnonymousClass07g.A00.contains(AWi)) {
                        AnonymousClass07g r72 = r5.A02.A06;
                        r72.BIp("received", "PUBLISH(topic=" + AWi + ", msgId=" + i + ", time=" + elapsedRealtime + ")");
                    }
                    r5.A02.A0I.BkT(AWi, A03, i, elapsedRealtime, A002);
                    if (AWi.startsWith("/graphql")) {
                        this.A00.A0D.BKd(AWi);
                    }
                }
                if (i2 == AnonymousClass08G.A00(AnonymousClass07B.A01)) {
                    AnonymousClass0C8 r03 = this.A00;
                    AnonymousClass07A.A04(r03.A0G, new AnonymousClass0I8(r03, i, BJK), 1203309340);
                    break;
                }
                break;
            case 3:
                int i3 = ((C02010Cm) r15).A02().A00;
                this.A00.A0D.BKs(i3);
                this.A00.A0D.BKt(i3);
                break;
            case 8:
                this.A00.A0D.BKs(((AnonymousClass0P3) r15).A02().A00);
                break;
            case AnonymousClass1Y3.A01 /*10*/:
                this.A00.A0D.BKt(((AnonymousClass0P1) r15).A02().A00);
                break;
            case AnonymousClass1Y3.A02 /*11*/:
                AnonymousClass0C8 r04 = this.A00;
                AnonymousClass07A.A04(r04.A0G, new AnonymousClass0S4(r04), -1491068495);
                break;
        }
        if (r5 != null) {
            AnonymousClass00S.A04(r5.A02.A05, new C02030Co(r5, r15), 883057530);
        }
        this.A00.A0T = SystemClock.elapsedRealtime();
        C02020Cn r1 = r15;
        if (r15 == null || !(r15 instanceof C02040Cp)) {
            r0 = C01660Bc.A00;
        } else {
            r0 = C01540Aq.A00(((C02040Cp) r1).A02().A01);
        }
        C01440Ag r3 = this.A00.A07;
        String name = r4.name();
        if (r0.A02()) {
            str = AnonymousClass08S.A0J(" ", (String) r0.A01());
        } else {
            str = BuildConfig.FLAVOR;
        }
        r3.A02(String.format("I %s%s", name, str));
        AnonymousClass0C8 r2 = this.A00;
        r2.A0S = r2.A0T;
    }

    public void A05(String str) {
        AnonymousClass0C8 r6 = this.A00;
        AtomicReference atomicReference = r6.A0I;
        if (atomicReference != null) {
            Integer num = null;
            if (!TextUtils.isEmpty(str)) {
                try {
                    int parseInt = Integer.parseInt(str);
                    byte[] bytes = ((String) r6.A0E.A0D.first).getBytes();
                    int i = 0;
                    int i2 = 0;
                    while (i < bytes.length && i < 10) {
                        i2 = (i2 << 1) + bytes[i];
                        i++;
                    }
                    num = Integer.valueOf(((parseInt * i2) + i2) ^ ((int) r6.A0W));
                } catch (NumberFormatException unused) {
                }
            }
            atomicReference.set(num);
        }
    }

    public void A06(String str, String str2) {
        this.A00.A0U = SystemClock.elapsedRealtime();
        this.A00.A07.A02(String.format("O %s%s", str, str2));
        AnonymousClass0C8 r2 = this.A00;
        r2.A0S = r2.A0U;
        AnonymousClass0C8 r0 = this.A00;
        r0.A08.A08(str, str2, r0.A0b, false);
        this.A00.A05.A00();
    }

    public void A07(Throwable th, String str, String str2) {
        AnonymousClass0CC r0 = this.A00.A0Y;
        if (r0 != null) {
            r0.A02(str, str2, th);
        }
    }

    public void A08(boolean z, String str, Object obj) {
        if (!z) {
            this.A00.A02.BIu(obj, false, str);
        } else {
            this.A00.A02.BIu(obj, true, BuildConfig.FLAVOR);
        }
    }
}
