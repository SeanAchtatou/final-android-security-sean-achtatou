package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.contacts.ranking.logging.RankingLoggingItem;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import io.card.payment.BuildConfig;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.Executor;

@UserScoped
/* renamed from: X.1TR  reason: invalid class name */
public final class AnonymousClass1TR {
    private static C05540Zi A03;
    private static final Comparator A04 = new AnonymousClass1TS();
    public AnonymousClass0UN A00;
    public String A01;
    private final C31381jb A02 = new C32381lf(this);

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c3, code lost:
        return new com.facebook.messaging.model.threads.ThreadsCollection(r2.build(), r9.A01);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.messaging.model.threads.ThreadsCollection A00(X.AnonymousClass1TR r8, com.facebook.messaging.model.threads.ThreadsCollection r9, int r10) {
        /*
            int r2 = X.AnonymousClass1Y3.BEi
            X.0UN r1 = r8.A00
            r0 = 10
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.40K r3 = (X.AnonymousClass40K) r3
            r5 = 0
            if (r9 == 0) goto L_0x00c9
            com.google.common.collect.ImmutableList r0 = r9.A00
            int r4 = r0.size()
            com.google.common.collect.ImmutableList$Builder r2 = new com.google.common.collect.ImmutableList$Builder
            r2.<init>()
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            X.1Xv r6 = r0.iterator()
        L_0x0023:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0039
            java.lang.Object r1 = r6.next()
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r1.A0S
            java.lang.String r0 = r0.A0I()
            r7.put(r0, r1)
            goto L_0x0023
        L_0x0039:
            monitor-enter(r3)
            java.util.HashMap r0 = r3.A00     // Catch:{ all -> 0x00c6 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00c6 }
            if (r0 != 0) goto L_0x00c4
            java.util.HashMap r0 = r3.A01     // Catch:{ all -> 0x00c6 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00c6 }
            if (r0 != 0) goto L_0x00c4
            java.util.HashMap r0 = r3.A01     // Catch:{ all -> 0x00c6 }
            int r0 = r0.size()     // Catch:{ all -> 0x00c6 }
            int r4 = r4 + r10
            if (r0 < r4) goto L_0x00c4
            r6 = r10
        L_0x0054:
            if (r6 >= r4) goto L_0x009f
            java.util.HashMap r0 = r3.A01     // Catch:{ all -> 0x00c6 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x00c6 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00c4
            java.util.HashMap r0 = r3.A01     // Catch:{ all -> 0x00c6 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x00c6 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0     // Catch:{ all -> 0x00c6 }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r0.A0S     // Catch:{ all -> 0x00c6 }
            java.lang.String r8 = r0.A0I()     // Catch:{ all -> 0x00c6 }
            boolean r0 = r7.containsKey(r8)     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00c4
            java.lang.Object r1 = r7.remove(r8)     // Catch:{ all -> 0x00c6 }
            com.facebook.messaging.model.threads.ThreadSummary r1 = (com.facebook.messaging.model.threads.ThreadSummary) r1     // Catch:{ all -> 0x00c6 }
            java.util.HashMap r0 = r3.A00     // Catch:{ all -> 0x00c6 }
            boolean r0 = r0.containsKey(r8)     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00c4
            java.util.HashMap r0 = r3.A00     // Catch:{ all -> 0x00c6 }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00c4
            if (r1 == 0) goto L_0x00c4
            java.util.HashMap r0 = r3.A00     // Catch:{ all -> 0x00c6 }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x00c6 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = (com.facebook.messaging.model.threads.ThreadSummary) r0     // Catch:{ all -> 0x00c6 }
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00c4
            int r6 = r6 + 1
            goto L_0x0054
        L_0x009f:
            boolean r0 = r7.isEmpty()     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x00b7
        L_0x00a5:
            if (r10 >= r4) goto L_0x00b7
            java.util.HashMap r1 = r3.A01     // Catch:{ all -> 0x00c6 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x00c6 }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x00c6 }
            r2.add(r0)     // Catch:{ all -> 0x00c6 }
            int r10 = r10 + 1
            goto L_0x00a5
        L_0x00b7:
            monitor-exit(r3)     // Catch:{ all -> 0x00c6 }
            com.facebook.messaging.model.threads.ThreadsCollection r5 = new com.facebook.messaging.model.threads.ThreadsCollection
            com.google.common.collect.ImmutableList r1 = r2.build()
            boolean r0 = r9.A01
            r5.<init>(r1, r0)
            return r5
        L_0x00c4:
            monitor-exit(r3)     // Catch:{ all -> 0x00c6 }
            return r5
        L_0x00c6:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00c6 }
            throw r0
        L_0x00c9:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TR.A00(X.1TR, com.facebook.messaging.model.threads.ThreadsCollection, int):com.facebook.messaging.model.threads.ThreadsCollection");
    }

    public static C29607EeF A01(AnonymousClass1TR r19, ImmutableList immutableList, int i) {
        C17930zi r1;
        int i2 = i;
        ImmutableList.Builder builder = new ImmutableList.Builder();
        ImmutableList.Builder builder2 = new ImmutableList.Builder();
        ImmutableList.Builder builder3 = new ImmutableList.Builder();
        C21321Gd r9 = new C21321Gd();
        C21321Gd r8 = new C21321Gd();
        ImmutableList immutableList2 = immutableList;
        C24971Xv it = immutableList2.iterator();
        while (true) {
            AnonymousClass1TR r7 = r19;
            if (it.hasNext()) {
                ThreadSummary threadSummary = (ThreadSummary) it.next();
                if (!(threadSummary.A0C <= 0 || (r1 = threadSummary.A0i) == C17930zi.VIDEO_CALL || r1 == C17930zi.VOICE_CALL)) {
                    boolean z = false;
                    if (threadSummary.A06 > ((AnonymousClass06B) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AgK, r7.A00)).now() - (((long) ((int) ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, r7.A00)).A00.At0(563800360157782L))) * 3600000)) {
                        z = true;
                    }
                    if (z) {
                        r9.add(threadSummary.A0S.A0I());
                    }
                }
                if (i2 < ((int) ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, r7.A00)).A00.At0(563800359699027L)) || r7.A05(threadSummary)) {
                    r8.add(threadSummary.A0S.A0I());
                }
                if (!((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, r7.A00)).A00.Aem(282325382858054L) || (i2 >= ((int) ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, r7.A00)).A00.At0(563800359699027L)) && !r7.A05(threadSummary))) {
                    boolean z2 = false;
                    if (threadSummary.A0B > ((AnonymousClass06B) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AgK, r7.A00)).now() - (((long) ((int) ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, r7.A00)).A00.At0(563800360092245L))) * 3600000)) {
                        z2 = true;
                    }
                    if (!z2) {
                        builder3.add((Object) threadSummary);
                    } else {
                        builder.add((Object) threadSummary);
                    }
                } else {
                    builder2.add((Object) threadSummary);
                }
                i2++;
            } else {
                C75513jy r12 = (C75513jy) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AbB, r7.A00);
                synchronized (r12) {
                    r12.A00 = r9;
                }
                C75503jx r13 = (C75503jx) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AQS, r7.A00);
                synchronized (r13) {
                    r13.A00 = r8;
                }
                return new C29607EeF(builder2.build(), builder.build(), builder3.build(), immutableList2);
            }
        }
    }

    public static final AnonymousClass1TR A02(AnonymousClass1XY r4) {
        AnonymousClass1TR r0;
        synchronized (AnonymousClass1TR.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new AnonymousClass1TR((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (AnonymousClass1TR) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public static ImmutableList A03(AnonymousClass1TR r9, C29607EeF eeF, int i) {
        int i2 = AnonymousClass1Y3.B2l;
        AnonymousClass0UN r2 = r9.A00;
        C34801qC A012 = ((C76243lC) AnonymousClass1XX.A02(1, i2, r2)).A01(new C76233lB(eeF.A00, r9.A02, ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, r2)).A00.B4E(845275336671356L, BuildConfig.FLAVOR), true));
        AnonymousClass40K r4 = (AnonymousClass40K) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BEi, r9.A00);
        ImmutableList immutableList = eeF.A03;
        ImmutableList immutableList2 = A012.A00;
        ImmutableList immutableList3 = eeF.A02;
        int i3 = i;
        synchronized (r4) {
            C24971Xv it = immutableList.iterator();
            while (it.hasNext()) {
                ThreadSummary threadSummary = (ThreadSummary) it.next();
                r4.A00.put(threadSummary.A0S.A0I(), threadSummary);
                r4.A01.put(Integer.valueOf(i3), threadSummary);
                i3++;
            }
            C24971Xv it2 = immutableList2.iterator();
            while (it2.hasNext()) {
                ThreadSummary threadSummary2 = (ThreadSummary) it2.next();
                r4.A00.put(threadSummary2.A0S.A0I(), threadSummary2);
                r4.A01.put(Integer.valueOf(i3), threadSummary2);
                i3++;
            }
            C24971Xv it3 = immutableList3.iterator();
            while (it3.hasNext()) {
                ThreadSummary threadSummary3 = (ThreadSummary) it3.next();
                r4.A00.put(threadSummary3.A0S.A0I(), threadSummary3);
                r4.A01.put(Integer.valueOf(i3), threadSummary3);
                i3++;
            }
        }
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        for (int i4 = 0; i4 < eeF.A01.size(); i4++) {
            hashMap2.put(((ThreadSummary) eeF.A01.get(i4)).A0S.A0I(), String.valueOf(i + i4));
        }
        C24971Xv it4 = eeF.A03.iterator();
        while (it4.hasNext()) {
            hashMap.put(((ThreadSummary) it4.next()).A0S.A0I(), String.valueOf(i));
            i++;
        }
        C24971Xv it5 = A012.A00.iterator();
        while (it5.hasNext()) {
            hashMap2.put(((ThreadSummary) it5.next()).A0S.A0I(), String.valueOf(i));
            i++;
        }
        C24971Xv it6 = eeF.A02.iterator();
        while (it6.hasNext()) {
            hashMap2.put(((ThreadSummary) it6.next()).A0S.A0I(), String.valueOf(i));
            i++;
        }
        r9.A04(r9.A01, hashMap, hashMap2);
        return A012.A00;
    }

    private void A04(String str, HashMap hashMap, HashMap hashMap2) {
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(4, AnonymousClass1Y3.Ap7, this.A00)).A01("messenger_inbox_thread_list_ranking"), AnonymousClass1Y3.A36);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D(C99084oO.$const$string(193), str);
            uSLEBaseShape0S0000000.A0F("thread_list_order_after_ranking", hashMap2);
            uSLEBaseShape0S0000000.A0F("thread_list_order_before_ranking", hashMap);
            uSLEBaseShape0S0000000.A06();
        }
    }

    private boolean A05(ThreadSummary threadSummary) {
        if (threadSummary.A0B > ((AnonymousClass06B) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AgK, this.A00)).now() - (((long) ((int) ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, this.A00)).A00.At0(563800359633490L))) * 3600000)) {
            return true;
        }
        return false;
    }

    public ThreadsCollection A06(ThreadsCollection threadsCollection, int i) {
        C34801qC A012;
        ImmutableList.Builder builder = new ImmutableList.Builder();
        ImmutableList.Builder builder2 = new ImmutableList.Builder();
        ImmutableList.Builder builder3 = new ImmutableList.Builder();
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        ThreadsCollection threadsCollection2 = threadsCollection;
        C24971Xv it = threadsCollection2.A00.iterator();
        int i2 = i;
        while (it.hasNext()) {
            ThreadSummary threadSummary = (ThreadSummary) it.next();
            if (!((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, this.A00)).A00.Aem(282325382858054L) || (i2 >= ((int) ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, this.A00)).A00.At0(563800359699027L)) && !A05(threadSummary))) {
                boolean z = false;
                if (threadSummary.A0B > ((AnonymousClass06B) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AgK, this.A00)).now() - (((long) ((int) ((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, this.A00)).A00.At0(563800360092245L))) * 3600000)) {
                    z = true;
                }
                if (!z) {
                    builder2.add((Object) threadSummary);
                } else {
                    builder3.add((Object) threadSummary);
                }
            } else {
                builder.add((Object) threadSummary);
            }
            hashMap.put(threadSummary.A0S.A0I(), String.valueOf(i2));
            i2++;
        }
        ImmutableList build = builder3.build();
        ImmutableList.Builder builder4 = new ImmutableList.Builder();
        if (!build.isEmpty()) {
            if (((AnonymousClass2AI) AnonymousClass1XX.A02(3, AnonymousClass1Y3.B4N, this.A00)).A01(AnonymousClass230.A0B) == null) {
                AnonymousClass07A.A04((AnonymousClass0VL) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ASK, this.A00), new C144516o1(this), 206428684);
            }
            C31381jb r11 = this.A02;
            AnonymousClass230 r1 = AnonymousClass230.A0B;
            Comparator comparator = A04;
            AnonymousClass21D A013 = ((C406822n) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A4R, this.A00)).A00.A01(r1);
            if (A013 == null) {
                A012 = null;
            } else {
                A012 = C406822n.A01(A013, C49532cR.A01(build, r11), r11, comparator, C49532cR.A00(A013));
            }
            if (A012 != null) {
                boolean z2 = false;
                if (A012.A00.size() == A012.A01.size()) {
                    z2 = true;
                }
                Preconditions.checkArgument(z2, "Ranked items and logging items do not match");
                C24971Xv it2 = builder.build().iterator();
                while (it2.hasNext()) {
                    ThreadSummary threadSummary2 = (ThreadSummary) it2.next();
                    builder4.add((Object) threadSummary2);
                    hashMap2.put(threadSummary2.A0S.A0I(), String.valueOf(i));
                    i++;
                }
                for (int i3 = 0; i3 < A012.A00.size(); i3++) {
                    C17920zh A002 = ThreadSummary.A00();
                    A002.A02((ThreadSummary) A012.A00.get(i3));
                    A002.A0r = ((RankingLoggingItem) A012.A01.get(i3)).A03;
                    builder4.add((Object) A002.A00());
                    hashMap2.put(((ThreadSummary) A012.A00.get(i3)).A0S.A0I(), String.valueOf(i));
                    i++;
                }
                C24971Xv it3 = builder2.build().iterator();
                while (it3.hasNext()) {
                    ThreadSummary threadSummary3 = (ThreadSummary) it3.next();
                    builder4.add((Object) threadSummary3);
                    hashMap2.put(threadSummary3.A0S.A0I(), String.valueOf(i));
                    i++;
                }
                A04(A012.A02, hashMap, hashMap2);
                if (((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, this.A00)).A00.Aem(282325384299857L)) {
                    return threadsCollection2;
                }
                return new ThreadsCollection(builder4.build(), threadsCollection2.A01);
            }
        }
        return threadsCollection;
    }

    public void A07(ThreadsCollection threadsCollection, int i, AnonymousClass2TD r9) {
        ThreadsCollection threadsCollection2;
        ImmutableList immutableList = threadsCollection.A00;
        if (immutableList != null && immutableList.size() >= 1) {
            int i2 = AnonymousClass1Y3.ACN;
            if (((C35391r9) AnonymousClass1XX.A02(5, i2, this.A00)).A02()) {
                if (((C35391r9) AnonymousClass1XX.A02(5, i2, this.A00)).A00.Aem(282325383185737L)) {
                    threadsCollection2 = A00(this, threadsCollection, i);
                } else {
                    threadsCollection2 = null;
                }
                if (threadsCollection2 == null) {
                    C05350Yp.A08(((AnonymousClass0VL) AnonymousClass1XX.A02(2, AnonymousClass1Y3.ASK, this.A00)).CIF(new C29605EeD(this, A01(this, threadsCollection.A00, i), i, threadsCollection)), new EeG(this, r9, threadsCollection), (Executor) AnonymousClass1XX.A02(9, AnonymousClass1Y3.A6Z, this.A00));
                    return;
                }
                if (!((C35391r9) AnonymousClass1XX.A02(5, AnonymousClass1Y3.ACN, this.A00)).A00.Aem(282325384299857L)) {
                    threadsCollection = threadsCollection2;
                }
                r9.BqZ(threadsCollection);
                return;
            }
        }
        r9.BYW();
    }

    private AnonymousClass1TR(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(11, r3);
    }
}
