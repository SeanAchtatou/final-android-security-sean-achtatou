package X;

import java.util.ArrayList;

/* renamed from: X.0BP  reason: invalid class name */
public final class AnonymousClass0BP extends ArrayList<Long> {
    public AnonymousClass0BP() {
        add(86400000L);
        add(43200000L);
        add(3600000L);
        add(1800000L);
        add(900000L);
    }
}
