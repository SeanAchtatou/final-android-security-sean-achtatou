package X;

import android.os.Handler;

/* renamed from: X.1ze  reason: invalid class name and case insensitive filesystem */
public final class C39811ze extends EPJ {
    public final /* synthetic */ C29164EOk A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C39811ze(C29164EOk eOk, ERI eri, Handler handler, C29160EOg eOg, boolean z) {
        super(eri, handler, eOg, z);
        this.A00 = eOk;
    }

    public void A01(Exception exc) {
        this.A00.A01.A03(exc, "ErrorStateCallback.onError()");
    }
}
