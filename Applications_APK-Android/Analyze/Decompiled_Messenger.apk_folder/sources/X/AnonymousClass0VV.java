package X;

import java.util.concurrent.Callable;

/* renamed from: X.0VV  reason: invalid class name */
public interface AnonymousClass0VV {
    Callable onCreateCallable(Callable callable);

    Runnable onCreateRunnable(Runnable runnable);
}
