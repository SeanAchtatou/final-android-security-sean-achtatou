package X;

import android.content.Context;

/* renamed from: X.0dk  reason: invalid class name and case insensitive filesystem */
public final class C07550dk extends AnonymousClass0Wl {
    public AnonymousClass0UN A00;

    public String getSimpleName() {
        return "EarliestPossibleColdStartClassPreloadStarter";
    }

    public static final C07550dk A00(AnonymousClass1XY r1) {
        return new C07550dk(r1);
    }

    private C07550dk(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    public void init() {
        int A03 = C000700l.A03(1195173929);
        if (AnonymousClass08X.A07((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.A6S, this.A00), "messenger_preload_startup_classes_asap")) {
            new Thread(new C93604d4(this), "cold_start_class_preloader").start();
        }
        C000700l.A09(1199399185, A03);
    }
}
