package X;

import com.facebook.rti.mqtt.protocol.messages.SubscribeTopic;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1FR  reason: invalid class name */
public final class AnonymousClass1FR implements AnonymousClass1FI {
    private static volatile AnonymousClass1FR A02;
    public final boolean A00;
    public final boolean A01 = true;

    public static final AnonymousClass1FR A00(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (AnonymousClass1FR.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = new AnonymousClass1FR(applicationInjector, AnonymousClass0WA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public ImmutableMap get() {
        SubscribeTopic subscribeTopic;
        HashMap hashMap = new HashMap();
        if (this.A00 || this.A01) {
            subscribeTopic = new SubscribeTopic("/t_sp", 0);
        } else {
            subscribeTopic = new SubscribeTopic("/t_p", 0);
        }
        hashMap.put(subscribeTopic, AnonymousClass1FP.APP_USE);
        return ImmutableMap.copyOf(hashMap);
    }

    private AnonymousClass1FR(AnonymousClass1XY r3, AnonymousClass1YI r4) {
        AnonymousClass0WT.A00(r3);
        this.A00 = r4.AbO(593, false);
        r4.AbO(AnonymousClass1Y3.A23, false);
    }
}
