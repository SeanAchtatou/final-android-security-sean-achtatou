package X;

import android.net.Uri;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import java.io.IOException;
import java.util.Iterator;

/* renamed from: X.0aA  reason: invalid class name and case insensitive filesystem */
public final class C05690aA implements C05700aB {
    public static final AnonymousClass1Y7 A00 = ((AnonymousClass1Y7) A0l.A09("active_status_filtering/"));
    public static final AnonymousClass1Y7 A01;
    public static final AnonymousClass1Y7 A02;
    public static final AnonymousClass1Y7 A03;
    public static final AnonymousClass1Y7 A04;
    public static final AnonymousClass1Y7 A05;
    public static final AnonymousClass1Y7 A06;
    public static final AnonymousClass1Y7 A07;
    public static final AnonymousClass1Y7 A08;
    public static final AnonymousClass1Y7 A09;
    public static final AnonymousClass1Y7 A0A;
    public static final AnonymousClass1Y7 A0B;
    public static final AnonymousClass1Y7 A0C;
    public static final AnonymousClass1Y7 A0D = ((AnonymousClass1Y7) A0l.A09("auto_add_notice_shown"));
    public static final AnonymousClass1Y7 A0E;
    public static final AnonymousClass1Y7 A0F;
    public static final AnonymousClass1Y7 A0G;
    public static final AnonymousClass1Y7 A0H;
    public static final AnonymousClass1Y7 A0I;
    public static final AnonymousClass1Y7 A0J;
    public static final AnonymousClass1Y7 A0K;
    public static final AnonymousClass1Y7 A0L;
    public static final AnonymousClass1Y7 A0M;
    public static final AnonymousClass1Y7 A0N;
    public static final AnonymousClass1Y7 A0O;
    public static final AnonymousClass1Y7 A0P;
    public static final AnonymousClass1Y7 A0Q;
    public static final AnonymousClass1Y7 A0R;
    public static final AnonymousClass1Y7 A0S;
    public static final AnonymousClass1Y7 A0T;
    public static final AnonymousClass1Y7 A0U;
    public static final AnonymousClass1Y7 A0V = ((AnonymousClass1Y7) C04350Ue.A06.A09("external_browser_enabled"));
    public static final AnonymousClass1Y7 A0W;
    public static final AnonymousClass1Y7 A0X;
    public static final AnonymousClass1Y7 A0Y = ((AnonymousClass1Y7) A0t.A09("has_logged_app_install"));
    public static final AnonymousClass1Y7 A0Z = ((AnonymousClass1Y7) A1K.A09("show_approval_notice_dialog/"));
    public static final AnonymousClass1Y7 A0a = ((AnonymousClass1Y7) A1I.A09("show_add_group_members_alert_dialog"));
    public static final AnonymousClass1Y7 A0b;
    public static final AnonymousClass1Y7 A0c;
    public static final AnonymousClass1Y7 A0d = ((AnonymousClass1Y7) A0m.A09("in_app_notification_navigate_to_inbox/"));
    public static final AnonymousClass1Y7 A0e;
    public static final AnonymousClass1Y7 A0f;
    public static final AnonymousClass1Y7 A0g;
    public static final AnonymousClass1Y7 A0h;
    public static final AnonymousClass1Y7 A0i;
    public static final AnonymousClass1Y7 A0j;
    public static final AnonymousClass1Y7 A0k;
    public static final AnonymousClass1Y7 A0l = ((AnonymousClass1Y7) C04350Ue.A06.A09("messages/"));
    public static final AnonymousClass1Y7 A0m;
    public static final AnonymousClass1Y7 A0n = ((AnonymousClass1Y7) A0k.A09("missed_call_notifications"));
    public static final AnonymousClass1Y7 A0o;
    public static final AnonymousClass1Y7 A0p;
    public static final AnonymousClass1Y7 A0q = ((AnonymousClass1Y7) A0o.A09("region_hint_timestamp"));
    public static final AnonymousClass1Y7 A0r = ((AnonymousClass1Y7) A0m.A09("ms_queue_params_last_success_hash_code/"));
    public static final AnonymousClass1Y7 A0s = ((AnonymousClass1Y7) A0t.A09("nux_completed_timestamp"));
    public static final AnonymousClass1Y7 A0t = ((AnonymousClass1Y7) C04350Ue.A00.A09("neue/"));
    public static final AnonymousClass1Y7 A0u;
    public static final AnonymousClass1Y7 A0v;
    public static final AnonymousClass1Y7 A0w;
    public static final AnonymousClass1Y7 A0x;
    public static final AnonymousClass1Y7 A0y;
    public static final AnonymousClass1Y7 A0z;
    public static final AnonymousClass1Y7 A10;
    public static final AnonymousClass1Y7 A11;
    public static final AnonymousClass1Y7 A12;
    public static final AnonymousClass1Y7 A13;
    public static final AnonymousClass1Y7 A14;
    public static final AnonymousClass1Y7 A15;
    public static final AnonymousClass1Y7 A16;
    public static final AnonymousClass1Y7 A17;
    public static final AnonymousClass1Y7 A18;
    public static final AnonymousClass1Y7 A19;
    public static final AnonymousClass1Y7 A1A;
    public static final AnonymousClass1Y7 A1B;
    public static final AnonymousClass1Y7 A1C;
    public static final AnonymousClass1Y7 A1D;
    public static final AnonymousClass1Y7 A1E;
    public static final AnonymousClass1Y7 A1F;
    public static final AnonymousClass1Y7 A1G;
    public static final AnonymousClass1Y7 A1H = ((AnonymousClass1Y7) A0l.A09("canonical_recipients/"));
    public static final AnonymousClass1Y7 A1I;
    public static final AnonymousClass1Y7 A1J = ((AnonymousClass1Y7) A1K.A09("games_push_notification/"));
    public static final AnonymousClass1Y7 A1K;
    public static final AnonymousClass1Y7 A1L;
    public static final AnonymousClass1Y7 A1M;
    public static final AnonymousClass1Y7 A1N;
    public static final AnonymousClass1Y7 A1O;
    public static final AnonymousClass1Y7 A1P;
    public static final AnonymousClass1Y7 A1Q;
    public static final AnonymousClass1Y7 A1R;
    public static final AnonymousClass1Y7 A1S;
    public static final AnonymousClass1Y7 A1T;
    public static final AnonymousClass1Y7 A1U;
    public static final AnonymousClass1Y7 A1V;
    public static final AnonymousClass1Y7 A1W = ((AnonymousClass1Y7) A0m.A09("recent_locations"));
    public static final AnonymousClass1Y7 A1X;
    public static final AnonymousClass1Y7 A1Y;
    public static final AnonymousClass1Y7 A1Z = ((AnonymousClass1Y7) A0m.A09("rtc_vcl_warning_dialog_shown/"));
    public static final AnonymousClass1Y7 A1a;
    public static final AnonymousClass1Y7 A1b;
    public static final AnonymousClass1Y7 A1c;
    public static final AnonymousClass1Y7 A1d;
    public static final AnonymousClass1Y7 A1e = ((AnonymousClass1Y7) A0k.A09("temporary_receipts_info/"));
    public static final AnonymousClass1Y7 A1f;
    public static final AnonymousClass1Y8 A1g;
    public static final AnonymousClass1Y8 A1h;
    public static final AnonymousClass1Y8 A1i;
    public static final AnonymousClass1Y8 A1j = A1m.A09("has_seen_deactivations_nux");
    public static final AnonymousClass1Y8 A1k = A1m.A09("should_show_end_of_nux_survey");
    public static final AnonymousClass1Y8 A1l;
    public static final AnonymousClass1Y8 A1m = C04350Ue.A08.A09("neue/");
    public static final AnonymousClass1Y8 A1n;
    public static final AnonymousClass1Y8 A1o;
    public static final AnonymousClass1Y8 A1p;
    public static final AnonymousClass1Y8 A1q;
    private static final AnonymousClass1Y7 A1r;

    public static ThreadKey A00(AnonymousClass1Y7 r4) {
        if (r4 != null) {
            AnonymousClass1Y7 r2 = A13;
            if (r4.A07(r2) && r4.A05().endsWith("muted_until2")) {
                String A062 = r4.A06(r2);
                Splitter on = Splitter.on("/");
                Preconditions.checkNotNull(A062);
                Iterator it = new Iterable(A062) {
                    public final /* synthetic */ CharSequence val$sequence;

                    {
                        this.val$sequence = r2;
                    }

                    public Iterator iterator() {
                        Splitter splitter = Splitter.this;
                        return splitter.strategy.iterator(splitter, this.val$sequence);
                    }

                    public String toString() {
                        Joiner on = Joiner.on(", ");
                        StringBuilder sb = new StringBuilder("[");
                        try {
                            on.appendTo(sb, iterator());
                            sb.append(']');
                            return sb.toString();
                        } catch (IOException e) {
                            throw new AssertionError(e);
                        }
                    }
                }.iterator();
                if (it.hasNext()) {
                    return ThreadKey.A06(Uri.decode((String) it.next()));
                }
            }
        }
        return null;
    }

    static {
        AnonymousClass1Y7 r2 = C04350Ue.A05;
        String $const$string = AnonymousClass24B.$const$string(AnonymousClass1Y3.A27);
        A0m = (AnonymousClass1Y7) r2.A09($const$string);
        A1i = C04350Ue.A0A.A09($const$string);
        A0k = (AnonymousClass1Y7) r2.A09("messages/");
        AnonymousClass1Y7 r1 = A0m;
        A0E = (AnonymousClass1Y7) r1.A09("badging/");
        A0W = (AnonymousClass1Y7) r1.A09("first_install_time");
        A19 = (AnonymousClass1Y7) r1.A09("nux_completed");
        A0N = (AnonymousClass1Y7) r1.A09("device_density_dpi");
        A1S = (AnonymousClass1Y7) r1.A09("generic_extension_order/");
        AnonymousClass1Y7 r12 = (AnonymousClass1Y7) r2.A09("nux/");
        A1A = r12;
        A0G = (AnonymousClass1Y7) r12.A09("chat_head_first_head_nux_completed");
        AnonymousClass1Y7 r13 = A1A;
        A0F = (AnonymousClass1Y7) r13.A09("chat_head_close_nux_completed");
        r13.A09("message_requests_has_used_people_tab_entry");
        r13.A09("messenger_ads_should_show_nux");
        r13.A09("messenger_ads_nux_ad_id");
        AnonymousClass1Y7 r14 = A0m;
        A1L = (AnonymousClass1Y7) r14.A09("phone_confirm/");
        A02 = (AnonymousClass1Y7) r14.A09("app_icon_badge");
        A05 = (AnonymousClass1Y7) r14.A09("app_target_badge");
        A07 = (AnonymousClass1Y7) r14.A09("app_target_badge_read_sms");
        A0A = (AnonymousClass1Y7) r14.A09("app_target_badge_unread_sms");
        A08 = (AnonymousClass1Y7) r14.A09("app_target_badge_read_tincan");
        A0B = (AnonymousClass1Y7) r14.A09("app_target_badge_unread_tincan");
        A06 = (AnonymousClass1Y7) r14.A09("app_target_badge_read_pinned");
        A09 = (AnonymousClass1Y7) r14.A09("app_target_badge_unread_pinned");
        r14.A09("recents_tab_badge");
        AnonymousClass1Y7 r15 = A0k;
        r15.A09("activity_tab_badge");
        r15.A09("activity_tab_unseen_delta");
        A0K = (AnonymousClass1Y7) r15.A09("connections_tab_badge");
        r15.A09("tab_badging/");
        AnonymousClass1Y8 A0D2 = A1i.A09("contact_logs/");
        A1h = A0D2;
        A0D2.A09("nux_contact_logs_upload_screen_shown");
        AnonymousClass1Y8 r16 = A1m;
        A1g = r16.A09("confirmation_code_request_time");
        A1n = r16.A09("phone_confirmed");
        A1q = r16.A09("validated_phonenumber");
        A1p = r16.A09("validated_country");
        A1o = r16.A09("prefill_phone_infos");
        A1l = r16.A09("nux_ver_completed");
        AnonymousClass1Y7 r17 = A0t;
        A1O = (AnonymousClass1Y7) r17.A09("platform_reply_crypto_key");
        A1P = (AnonymousClass1Y7) r17.A09("platform_reply_iv_crypto_key");
        A1R = (AnonymousClass1Y7) r17.A09("platform_user_accepted_reply_dialog_key");
        A1M = (AnonymousClass1Y7) r17.A09("platform_last_time_reply_dialog_seen");
        A1Q = (AnonymousClass1Y7) r17.A09("platform_show_reply_dialog_wait_interval_days");
        A1N = (AnonymousClass1Y7) r17.A09("platform_menu_state_key");
        AnonymousClass1Y7 r18 = (AnonymousClass1Y7) A0m.A09("mqtt/");
        A0o = r18;
        A0p = (AnonymousClass1Y7) r18.A09("region_hint");
        AnonymousClass1Y7 r19 = (AnonymousClass1Y7) A0l.A09(AnonymousClass80H.$const$string(71));
        A17 = r19;
        r19.A09("enabled");
        AnonymousClass1Y7 r110 = A17;
        A0z = (AnonymousClass1Y7) r110.A09("muted_until2");
        A03 = (AnonymousClass1Y7) r110.A09("sound_enabled");
        A11 = (AnonymousClass1Y7) r110.A09("notif_pref_sound_switch");
        A0x = (AnonymousClass1Y7) r110.A09("in_app_sounds_enabled");
        A04 = (AnonymousClass1Y7) r110.A09("vibrate_enabled");
        A12 = (AnonymousClass1Y7) r110.A09("notif_pref_vib_switch");
        A0y = (AnonymousClass1Y7) r110.A09("led_enabled");
        A0u = (AnonymousClass1Y7) r110.A09("android_auto_groups");
        A18 = (AnonymousClass1Y7) r110.A09("unread_pill_enabled");
        A10 = (AnonymousClass1Y7) r110.A09("notif_bump_enabled");
        AnonymousClass1Y7 r111 = A0m;
        A0H = (AnonymousClass1Y7) r111.A09("composer_expression_latest_selected_option");
        A0I = (AnonymousClass1Y7) r111.A09("composer_expression_latest_selected_option_time");
        AnonymousClass1Y7 r112 = A17;
        r112.A09("use_system_sound");
        r112.A09("ringtone_uri");
        r112.A09("ringtone_uri2");
        A15 = (AnonymousClass1Y7) r112.A09("ringtone_uri_v3");
        A16 = (AnonymousClass1Y7) r112.A09("ringtone_uri_v3_in_use");
        A14 = (AnonymousClass1Y7) r112.A09(C99084oO.$const$string(78));
        A1d = (AnonymousClass1Y7) r112.A09("sms_preview");
        AnonymousClass1Y7 r113 = A0l;
        r113.A09("converted_prefs_to_threadkey");
        A1K = (AnonymousClass1Y7) r113.A09("threads/");
        A1I = (AnonymousClass1Y7) r113.A09("group_threads/");
        A1K.A09("/num_shortcut_banner_shown");
        AnonymousClass1Y7 r114 = (AnonymousClass1Y7) A17.A09("threads/");
        A13 = r114;
        A0w = (AnonymousClass1Y7) r114.A09("custom/");
        AnonymousClass1Y7 r115 = A0k;
        A1Y = (AnonymousClass1Y7) r115.A09("notifications/recent_threads/");
        A1X = (AnonymousClass1Y7) r115.A09("notifications/recent_page_threads/");
        A0v = (AnonymousClass1Y7) r115.A09("notification_channels");
        A0L = (AnonymousClass1Y7) ((AnonymousClass1Y7) r115.A09("contacts/")).A09("new_user_notifications");
        AnonymousClass1Y7 r116 = A0k;
        r116.A09("neue_nux/");
        A1c = (AnonymousClass1Y7) r116.A09("show_voip_nux_banner2");
        r116.A09("should_show_shortcut_banner");
        A1G = (AnonymousClass1Y7) r116.A09("page_subscription_nux_count");
        r116.A09("work_synced_group_thread_settings_nux_count");
        A0J = (AnonymousClass1Y7) r116.A09("confirm_delete_sms_thread");
        A1V = (AnonymousClass1Y7) r116.A09("quickcam_videos_taken");
        A1U = (AnonymousClass1Y7) r116.A09("quickcam_photos_taken");
        r116.A09("inbox_camera/");
        r116.A09("inbox_featured_art_last_tooltip_time");
        A0C = (AnonymousClass1Y7) r116.A09("audio_recording_first_time_press");
        r116.A09("hot_like_has_seen_nux_bubble");
        A0b = (AnonymousClass1Y7) r116.A09("hot_like_has_sent_larger_like");
        A1f = (AnonymousClass1Y7) r116.A09("wear_connected_nodes");
        A0j = (AnonymousClass1Y7) r116.A09("messages_collection_size_logged_timestamp_ms");
        A1b = (AnonymousClass1Y7) r116.A09("server_muted_until");
        r116.A09("num_threads_in_threadlist");
        ((AnonymousClass1Y7) A0k.A09("rideshare/")).A09("ride_providers");
        AnonymousClass1Y7 r117 = (AnonymousClass1Y7) A0k.A09("address_typahead/");
        A01 = r117;
        r117.A09("recent_selected_address");
        A01.A09("conversation_address");
        AnonymousClass1Y7 r118 = A0k;
        A0f = (AnonymousClass1Y7) r118.A09("last_send_failure_captive_portal_notified_ms");
        A0g = (AnonymousClass1Y7) r118.A09("last_send_failure_long_queued_message_last_notified_ms");
        A0e = (AnonymousClass1Y7) r118.A09("last_send_failure_backround_data_restriction_last_notified_ms");
        AnonymousClass1Y7 r119 = A0m;
        A0c = (AnonymousClass1Y7) r119.A09("inbox_has_top_unit");
        r119.A09("people_tab_last_visible_time");
        AnonymousClass1Y7 r120 = A1A;
        A1B = (AnonymousClass1Y7) r120.A09("one_line_composer_c2c_payments_buyer_button_nux_shown");
        A1C = (AnonymousClass1Y7) r120.A09("one_line_composer_c2c_payments_seller_button_nux_shown");
        r120.A09("one_line_composer_more_drawer_button_nux_shown");
        r120.A09("one_line_composer_cowatch_in_more_drawer_nux_show");
        r120.A09("one_line_composer_mfs_send_load_nux_show");
        A1D = (AnonymousClass1Y7) r120.A09("one_line_composer_mic_split_nux_num_times_shown");
        A1E = (AnonymousClass1Y7) r120.A09("one_line_composer_rich_text_intro_nux_show");
        A0M = (AnonymousClass1Y7) r120.A09("cowatch_rtc_cross_button_tooltip_show");
        A1a = (AnonymousClass1Y7) r120.A09("send_file_nux_shown");
        A0i = (AnonymousClass1Y7) r120.A09("mark_paid_nux_shown");
        A1T = (AnonymousClass1Y7) r120.A09("product_picker_nux_shown");
        AnonymousClass1Y7 r121 = A0m;
        r121.A09("gysc_drafts");
        AnonymousClass1Y7 r122 = (AnonymousClass1Y7) r121.A09("comment_pivot");
        A1r = r122;
        r122.A09("sticky_actions_count_v2");
        AnonymousClass1Y7 r123 = A1r;
        r123.A09("user_posts_default_opted_in_v2");
        r123.A09("page_posts_default_opted_in_v2");
        r123.A09("times_nux_shown_v2");
        r123.A09("times_post_send_one_time_modal_shown_v1");
        AnonymousClass1Y7 r124 = A1A;
        r124.A09("discover_tab_first_time_visit_nux");
        A0O = (AnonymousClass1Y7) r124.A09("discover_tab_m4_first_time_visit_nux");
        AnonymousClass1Y7 r125 = A0m;
        r125.A09("quicksilver_live_dont_show_privacy_message");
        A0X = (AnonymousClass1Y7) r125.A09("games_tab_image_nux_shown");
        A1F = (AnonymousClass1Y7) r125.A09("page_reply_first_time_init");
        r125.A09("group_thread_metadata");
        A0h = (AnonymousClass1Y7) r125.A09("/last_sequence_id");
        AnonymousClass1Y7 r126 = (AnonymousClass1Y7) A0m.A09("dual_sim/");
        A0U = r126;
        A0R = (AnonymousClass1Y7) r126.A09("preferred_sim/");
        AnonymousClass1Y7 r127 = A0U;
        A0P = (AnonymousClass1Y7) r127.A09("composer_button_tooltip/");
        A0S = (AnonymousClass1Y7) r127.A09("reply_in_kind/");
        A0T = (AnonymousClass1Y7) r127.A09("sub_id_column_available/");
        A0Q = (AnonymousClass1Y7) r127.A09("current_sims/");
    }

    public static final C05690aA A01() {
        return new C05690aA();
    }

    public ImmutableSet AzO() {
        return ImmutableSet.A04(A1h);
    }

    public static AnonymousClass1Y7 A02(ThreadKey threadKey) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A13.A09(Uri.encode(threadKey.toString()))).A09("/member_request_muted");
    }

    public static AnonymousClass1Y7 A03(ThreadKey threadKey) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A13.A09(Uri.encode(threadKey.toString()))).A09("/mentions_muted");
    }

    public static AnonymousClass1Y7 A04(ThreadKey threadKey) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A13.A09(Uri.encode(threadKey.toString()))).A09("/reactions_muted");
    }

    public static AnonymousClass1Y7 A05(ThreadKey threadKey) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A1X.A09("count/")).A09(Uri.encode(threadKey.toString()));
    }

    public static AnonymousClass1Y7 A06(ThreadKey threadKey) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A13.A09(Uri.encode(threadKey.toString()))).A09("/calls_muted_until");
    }

    public static AnonymousClass1Y7 A07(ThreadKey threadKey) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A0U.A09(Uri.encode(threadKey.toString()))).A09("/sub_id");
    }

    public static AnonymousClass1Y7 A08(ThreadKey threadKey) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A13.A09(Uri.encode(threadKey.toString()))).A09("/muted_until2");
    }

    public static AnonymousClass1Y7 A09(ThreadKey threadKey, int i) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) ((AnonymousClass1Y7) A1X.A09("content/")).A09(Uri.encode(threadKey.toString()))).A09(AnonymousClass08S.A0J("/", String.valueOf(i)));
    }

    public static AnonymousClass1Y7 A0A(String str) {
        return (AnonymousClass1Y7) ((AnonymousClass1Y7) A1K.A09(Uri.encode(str))).A09("/show_invite_banner");
    }
}
