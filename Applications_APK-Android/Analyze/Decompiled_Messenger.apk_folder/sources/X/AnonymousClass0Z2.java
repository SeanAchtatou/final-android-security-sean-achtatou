package X;

/* renamed from: X.0Z2  reason: invalid class name */
public final class AnonymousClass0Z2 extends AnonymousClass0Wl {
    private final AnonymousClass0WB A00;
    private final AnonymousClass0US A01;

    public String getSimpleName() {
        return "GatekeeperStoreInitializer";
    }

    public AnonymousClass0Z2(AnonymousClass0WB r1, AnonymousClass0US r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void init() {
        int A03 = C000700l.A03(-71262718);
        AnonymousClass0WB r1 = this.A00;
        synchronized (r1) {
            AnonymousClass0WB.A02(r1);
        }
        ((AnonymousClass0WE) this.A01.get()).A02();
        C000700l.A09(-1175049075, A03);
    }
}
