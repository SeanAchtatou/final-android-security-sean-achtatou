package X;

import com.facebook.inject.InjectorModule;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import io.card.payment.BuildConfig;
import java.util.Collection;

@InjectorModule
/* renamed from: X.1H1  reason: invalid class name */
public final class AnonymousClass1H1 extends AnonymousClass0UV {
    private static C04470Uu A00;

    public static final AnonymousClass511 A00(AnonymousClass1XY r13) {
        AnonymousClass511 r0;
        AnonymousClass511 r6;
        Boolean bool;
        Boolean valueOf;
        synchronized (AnonymousClass511.class) {
            C04470Uu A002 = C04470Uu.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r13)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    C04470Uu r2 = A00;
                    FbSharedPreferences A003 = FbSharedPreferencesModule.A00(r02);
                    boolean booleanValue = AnonymousClass0UU.A08(r02).booleanValue();
                    if (!booleanValue) {
                        r6 = null;
                    } else {
                        boolean Aep = A003.Aep(C44492Ik.A03, false);
                        ImmutableList A01 = A01(A003.B4F(C44492Ik.A04, BuildConfig.FLAVOR));
                        ImmutableList A012 = A01(A003.B4F(C44492Ik.A0B, BuildConfig.FLAVOR));
                        String str = null;
                        if (booleanValue) {
                            str = A003.B4F(C44492Ik.A05, null);
                        }
                        String str2 = null;
                        if (booleanValue) {
                            str2 = A003.B4F(C44492Ik.A06, null);
                        }
                        if (!booleanValue) {
                            bool = Boolean.FALSE;
                        } else {
                            bool = Boolean.valueOf(A003.Aep(C44492Ik.A08, Boolean.FALSE.booleanValue()));
                        }
                        boolean booleanValue2 = bool.booleanValue();
                        if (!booleanValue) {
                            valueOf = Boolean.FALSE;
                        } else {
                            valueOf = Boolean.valueOf(A003.Aep(C44492Ik.A07, Boolean.FALSE.booleanValue()));
                        }
                        r6 = new AnonymousClass511(str, str2, booleanValue2, Aep, valueOf.booleanValue(), A01, A012);
                    }
                    r2.A00 = r6;
                }
                C04470Uu r1 = A00;
                r0 = (AnonymousClass511) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static ImmutableList A01(String str) {
        ImmutableList copyOf = ImmutableList.copyOf((Collection) C06850cB.A08(str, ';'));
        if (copyOf.size() != 1 || !((String) copyOf.get(0)).equals(BuildConfig.FLAVOR)) {
            return copyOf;
        }
        return RegularImmutableList.A02;
    }

    public static final Boolean A02(AnonymousClass1XY r3) {
        FbSharedPreferences A002 = FbSharedPreferencesModule.A00(r3);
        if (!AnonymousClass0UU.A08(r3).booleanValue()) {
            return Boolean.FALSE;
        }
        return Boolean.valueOf(A002.Aep(C44492Ik.A08, Boolean.FALSE.booleanValue()));
    }

    public static final String A03(AnonymousClass1XY r3) {
        FbSharedPreferences A002 = FbSharedPreferencesModule.A00(r3);
        if (AnonymousClass0UU.A08(r3).booleanValue()) {
            return A002.B4F(C44492Ik.A06, null);
        }
        return null;
    }
}
