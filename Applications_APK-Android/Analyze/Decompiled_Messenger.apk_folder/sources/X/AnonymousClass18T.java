package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.18T  reason: invalid class name */
public final class AnonymousClass18T implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18T(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r5) {
        int A002 = AnonymousClass09Y.A00(1748132133);
        C193617v r0 = this.A00.A03;
        if (r0 != null) {
            r0.Bow();
        }
        AnonymousClass09Y.A01(32319401, A002);
    }
}
