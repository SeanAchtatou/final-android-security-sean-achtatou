package X;

import android.os.Bundle;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;

@UserScoped
/* renamed from: X.1G1  reason: invalid class name */
public final class AnonymousClass1G1 implements CallerContextable {
    private static C05540Zi A01 = null;
    public static final CallerContext A02 = CallerContext.A04(AnonymousClass1G1.class);
    public static final String __redex_internal_original_name = "com.facebook.messaging.montage.omnistore.util.MontageServiceFutureFactory";
    public final BlueServiceOperationFactory A00;

    public static final AnonymousClass1G1 A00(AnonymousClass1XY r4) {
        AnonymousClass1G1 r0;
        synchronized (AnonymousClass1G1.class) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r4)) {
                    A01.A00 = new AnonymousClass1G1((AnonymousClass1XY) A01.A01());
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass1G1) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public void A01(String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putString("REFRESH_STORY_STORY_ID", str);
        bundle.putString("REFRESH_STORY_STORY_TYPE", str2);
        this.A00.newInstance("refresh_story", bundle, 1, A02).CHd();
    }

    private AnonymousClass1G1(AnonymousClass1XY r3) {
        new C21321Gd();
        new AnonymousClass0UN(1, r3);
        this.A00 = C30111hV.A00(r3);
        C17350yl.A00(r3);
    }
}
