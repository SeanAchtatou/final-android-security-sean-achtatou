package X;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Arrays;

/* renamed from: X.1uj  reason: invalid class name and case insensitive filesystem */
public final class C37121uj implements C37131uk {
    public int A00;
    public int A01;
    public int A02;
    public int[] A03;

    public void A00(RecyclerView recyclerView, boolean z) {
        this.A00 = 0;
        int[] iArr = this.A03;
        if (iArr != null) {
            Arrays.fill(iArr, -1);
        }
        AnonymousClass19T r3 = recyclerView.A0L;
        if (recyclerView.A0J != null && r3 != null && r3.A0C) {
            if (z) {
                boolean z2 = false;
                if (recyclerView.A0G.A05.size() > 0) {
                    z2 = true;
                }
                if (!z2) {
                    r3.A13(recyclerView.A0J.ArU(), this);
                }
            } else if (!recyclerView.A1B()) {
                r3.A12(this.A01, this.A02, recyclerView.A0y, this);
            }
            int i = this.A00;
            if (i > r3.A03) {
                r3.A03 = i;
                r3.A0E = z;
                recyclerView.A0w.A06();
            }
        }
    }

    public void ANZ(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("Layout positions must be non-negative");
        } else if (i2 >= 0) {
            int i3 = this.A00 << 1;
            int[] iArr = this.A03;
            if (iArr == null) {
                int[] iArr2 = new int[4];
                this.A03 = iArr2;
                Arrays.fill(iArr2, -1);
            } else if (i3 >= iArr.length) {
                int[] iArr3 = new int[(i3 << 1)];
                this.A03 = iArr3;
                System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
            }
            int[] iArr4 = this.A03;
            iArr4[i3] = i;
            iArr4[i3 + 1] = i2;
            this.A00++;
        } else {
            throw new IllegalArgumentException("Pixel distance must be non-negative");
        }
    }
}
