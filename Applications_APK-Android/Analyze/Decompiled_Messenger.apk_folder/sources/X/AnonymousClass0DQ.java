package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0DQ  reason: invalid class name */
public final class AnonymousClass0DQ extends C03160Kg {
    private final AnonymousClass04b A00 = new AnonymousClass04b();
    private final AnonymousClass04b A01 = new AnonymousClass04b();
    private final AnonymousClass04b A02 = new AnonymousClass04b();

    public long A00() {
        return -3137023965338009377L;
    }

    public void A01(AnonymousClass0FM r7, DataOutput dataOutput) {
        C02760Gg r72 = (C02760Gg) r7;
        int size = this.A02.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            if (r72.A0E((Class) this.A02.A07(i2))) {
                i++;
            }
        }
        dataOutput.writeInt(i);
        for (int i3 = 0; i3 < size; i3++) {
            Class cls = (Class) this.A02.A07(i3);
            if (r72.A0E(cls)) {
                C03160Kg r2 = (C03160Kg) this.A02.A09(i3);
                dataOutput.writeLong(r2.A00());
                r2.A01(r72.A09(cls), dataOutput);
            }
        }
    }

    public boolean A03(AnonymousClass0FM r8, DataInput dataInput) {
        C02760Gg r82 = (C02760Gg) r8;
        AnonymousClass04b r3 = r82.mMetricsMap;
        int size = r3.size();
        for (int i = 0; i < size; i++) {
            r82.A0D((Class) r3.A07(i), false);
        }
        int readInt = dataInput.readInt();
        for (int i2 = 0; i2 < readInt; i2++) {
            long readLong = dataInput.readLong();
            AnonymousClass04b r0 = this.A01;
            Long valueOf = Long.valueOf(readLong);
            C03160Kg r2 = (C03160Kg) r0.get(valueOf);
            Class cls = (Class) this.A00.get(valueOf);
            if (r2 == null || cls == null || !r2.A03(r82.A09(cls), dataInput)) {
                return false;
            }
            r82.A0D(cls, true);
        }
        return true;
    }

    public void A04(Class cls, C03160Kg r6) {
        Class cls2 = (Class) this.A00.get(Long.valueOf(r6.A00()));
        if (cls2 == null || cls2 == cls) {
            this.A02.put(cls, r6);
            this.A01.put(Long.valueOf(r6.A00()), r6);
            this.A00.put(Long.valueOf(r6.A00()), cls);
            return;
        }
        throw new RuntimeException("Serializers " + cls2.getCanonicalName() + " and " + cls.getCanonicalName() + " have a conflicting tag: `" + r6.A00() + "`.");
    }
}
