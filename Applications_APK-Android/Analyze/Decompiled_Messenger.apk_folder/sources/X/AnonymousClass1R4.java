package X;

import android.util.Pair;
import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.1R4  reason: invalid class name */
public final class AnonymousClass1R4 {
    public float A00;
    public int A01;
    public AnonymousClass1QK A02;
    public AnonymousClass1RL A03;
    public Closeable A04;
    public final Object A05;
    public final CopyOnWriteArraySet A06 = new CopyOnWriteArraySet();
    public final /* synthetic */ AnonymousClass1Q9 A07;

    private synchronized C23561Pz A00() {
        C23561Pz r4;
        r4 = C23561Pz.LOW;
        Iterator it = this.A06.iterator();
        while (it.hasNext()) {
            C23561Pz A032 = ((AnonymousClass1QK) ((Pair) it.next()).second).A03();
            if (r4 != null) {
                if (A032 != null) {
                    if (r4.ordinal() > A032.ordinal()) {
                    }
                }
            }
            r4 = A032;
        }
        return r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x001f, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.util.List A01(X.AnonymousClass1R4 r3) {
        /*
            monitor-enter(r3)
            X.1QK r2 = r3.A02     // Catch:{ all -> 0x0023 }
            if (r2 != 0) goto L_0x0008
            r0 = 0
            monitor-exit(r3)
            return r0
        L_0x0008:
            boolean r1 = r3.A06()     // Catch:{ all -> 0x0023 }
            monitor-enter(r2)     // Catch:{ all -> 0x0023 }
            boolean r0 = r2.A02     // Catch:{ all -> 0x0020 }
            if (r1 != r0) goto L_0x0014
            r1 = 0
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x001e
        L_0x0014:
            r2.A02 = r1     // Catch:{ all -> 0x0020 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0020 }
            java.util.List r0 = r2.A0D     // Catch:{ all -> 0x0020 }
            r1.<init>(r0)     // Catch:{ all -> 0x0020 }
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
        L_0x001e:
            monitor-exit(r3)
            return r1
        L_0x0020:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1R4.A01(X.1R4):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x001f, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.util.List A02(X.AnonymousClass1R4 r3) {
        /*
            monitor-enter(r3)
            X.1QK r2 = r3.A02     // Catch:{ all -> 0x0023 }
            if (r2 != 0) goto L_0x0008
            r0 = 0
            monitor-exit(r3)
            return r0
        L_0x0008:
            boolean r1 = r3.A07()     // Catch:{ all -> 0x0023 }
            monitor-enter(r2)     // Catch:{ all -> 0x0023 }
            boolean r0 = r2.A03     // Catch:{ all -> 0x0020 }
            if (r1 != r0) goto L_0x0014
            r1 = 0
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x001e
        L_0x0014:
            r2.A03 = r1     // Catch:{ all -> 0x0020 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0020 }
            java.util.List r0 = r2.A0D     // Catch:{ all -> 0x0020 }
            r1.<init>(r0)     // Catch:{ all -> 0x0020 }
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
        L_0x001e:
            monitor-exit(r3)
            return r1
        L_0x0020:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1R4.A02(X.1R4):java.util.List");
    }

    public static synchronized List A03(AnonymousClass1R4 r2) {
        synchronized (r2) {
            AnonymousClass1QK r1 = r2.A02;
            if (r1 == null) {
                return null;
            }
            List A042 = r1.A04(r2.A00());
            return A042;
        }
    }

    public static void A04(AnonymousClass1R4 r13) {
        synchronized (r13) {
            boolean z = true;
            boolean z2 = false;
            if (r13.A02 == null) {
                z2 = true;
            }
            C05520Zg.A04(z2);
            if (r13.A03 != null) {
                z = false;
            }
            C05520Zg.A04(z);
            if (r13.A06.isEmpty()) {
                r13.A07.A02(r13.A05, r13);
                return;
            }
            AnonymousClass1QK r0 = (AnonymousClass1QK) ((Pair) r13.A06.iterator().next()).second;
            AnonymousClass1QK r2 = new AnonymousClass1QK(r0.A09, r0.A0B, null, r0.A07, r0.A0A, r0.A08, r13.A07(), r13.A06(), r13.A00(), r0.A06);
            r13.A02 = r2;
            AnonymousClass1RL r1 = new AnonymousClass1RL(r13);
            r13.A03 = r1;
            r13.A07.A00.ByS(r1, r2);
        }
    }

    private synchronized boolean A06() {
        Iterator it = this.A06.iterator();
        while (it.hasNext()) {
            if (((AnonymousClass1QK) ((Pair) it.next()).second).A07()) {
                return true;
            }
        }
        return false;
    }

    private synchronized boolean A07() {
        Iterator it = this.A06.iterator();
        while (it.hasNext()) {
            if (!((AnonymousClass1QK) ((Pair) it.next()).second).A08()) {
                return false;
            }
        }
        return true;
    }

    public AnonymousClass1R4(AnonymousClass1Q9 r2, Object obj) {
        this.A07 = r2;
        this.A05 = obj;
    }

    public static void A05(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
