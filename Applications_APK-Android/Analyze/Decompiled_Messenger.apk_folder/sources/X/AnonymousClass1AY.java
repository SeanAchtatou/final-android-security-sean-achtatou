package X;

import com.facebook.messaging.inbox2.items.InboxTrackableItem;
import java.util.ArrayList;
import java.util.Set;

/* renamed from: X.1AY  reason: invalid class name */
public final class AnonymousClass1AY implements AnonymousClass1AT {
    private boolean A00;
    public final C17610zB A01 = new C17610zB();
    public final C32871mT A02;
    private final Set A03;

    public void BMG(C24201Sr r5) {
        for (AnonymousClass1AZ r2 : this.A03) {
            if (r2.CEL() || r2.Apl() == ((InboxTrackableItem) r5.A05).A04) {
                r2.BMG(r5);
            }
        }
    }

    public void BMH(C24201Sr r5) {
        for (AnonymousClass1AZ r2 : this.A03) {
            if (r2.CEL() || r2.Apl() == ((InboxTrackableItem) r5.A05).A04) {
                r2.BMH(r5);
            }
        }
    }

    public void C8h(boolean z) {
        for (AnonymousClass1AZ C8h : this.A03) {
            C8h.C8h(z);
        }
    }

    public void CKX(C17610zB r11, boolean z) {
        if (z != this.A00) {
            this.A00 = z;
            int A012 = r11.A01();
            for (int i = 0; i < A012; i++) {
                C24201Sr r4 = (C24201Sr) r11.A06(i);
                for (AnonymousClass1AZ r2 : this.A03) {
                    if (r2.CEL() || r2.Apl() == ((InboxTrackableItem) r4.A05).A04) {
                        r2.CKW(r4, z);
                    }
                }
            }
            if (this.A00) {
                int A013 = r11.A01();
                for (int i2 = 0; i2 < A013; i2++) {
                    C24201Sr r1 = (C24201Sr) r11.A06(i2);
                    this.A01.A0D(((InboxTrackableItem) r1.A05).ArZ(), Long.valueOf(r1.A02));
                }
                return;
            }
            ArrayList<C24201Sr> arrayList = new ArrayList<>();
            int A014 = r11.A01();
            for (int i3 = 0; i3 < A014; i3++) {
                C24201Sr r5 = (C24201Sr) r11.A06(i3);
                Long l = (Long) this.A01.A07(((InboxTrackableItem) r5.A05).ArZ());
                Long valueOf = Long.valueOf(r5.A02);
                boolean z2 = true;
                if (l != null ? l.longValue() >= valueOf.longValue() : valueOf.longValue() <= 0) {
                    z2 = false;
                }
                if (z2) {
                    arrayList.add(r5);
                }
            }
            this.A01.A09();
            C32871mT r3 = this.A02;
            C05180Xy r52 = new C05180Xy();
            int i4 = Integer.MAX_VALUE;
            for (C24201Sr r22 : arrayList) {
                int i5 = ((InboxTrackableItem) r22.A05).A00;
                if (i5 != -1) {
                    r52.add(Integer.valueOf(i5));
                    i4 = Math.min(i4, ((InboxTrackableItem) r22.A05).A00);
                }
            }
            r52.size();
            C11670nb r23 = new C11670nb("inbox2_vr");
            r23.A0D("pigeon_reserved_keyword_module", "messenger_inbox_ads");
            r23.A09("p", i4);
            r23.A09("n", r52.size());
            r3.A01.A09(r23);
        }
    }

    public AnonymousClass1AY(Set set, C32871mT r4) {
        C05180Xy r1 = new C05180Xy();
        this.A03 = r1;
        r1.addAll(set);
        this.A02 = r4;
        this.A00 = false;
    }
}
