package X;

import com.facebook.acra.ACRA;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import java.util.BitSet;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.1NJ  reason: invalid class name */
public final class AnonymousClass1NJ {
    public static C17770zR A00(AnonymousClass0p4 r7, MigColorScheme migColorScheme, C21361Gm r9, AnonymousClass10N r10) {
        switch (r9.ordinal()) {
            case 0:
                String[] strArr = {"colorScheme"};
                BitSet bitSet = new BitSet(1);
                AnonymousClass1NK r3 = new AnonymousClass1NK();
                C17770zR r0 = r7.A04;
                if (r0 != null) {
                    r3.A07 = r0.A06;
                }
                bitSet.clear();
                r3.A00 = migColorScheme;
                bitSet.set(0);
                r3.A14().A0G(r10);
                AnonymousClass11F.A0C(1, bitSet, strArr);
                return r3;
            case 1:
                String[] strArr2 = {"colorScheme"};
                BitSet bitSet2 = new BitSet(1);
                AnonymousClass1vL r32 = new AnonymousClass1vL(r7.A09);
                C17770zR r02 = r7.A04;
                if (r02 != null) {
                    r32.A07 = r02.A06;
                }
                bitSet2.clear();
                r32.A01 = migColorScheme;
                bitSet2.set(0);
                r32.A14().A0G(r10);
                AnonymousClass11F.A0C(1, bitSet2, strArr2);
                return r32;
            case 2:
                String[] strArr3 = {"colorScheme"};
                BitSet bitSet3 = new BitSet(1);
                C23891Rj r33 = new C23891Rj();
                C17770zR r03 = r7.A04;
                if (r03 != null) {
                    r33.A07 = r03.A06;
                }
                bitSet3.clear();
                r33.A00 = migColorScheme;
                bitSet3.set(0);
                r33.A14().A0G(r10);
                AnonymousClass11F.A0C(1, bitSet3, strArr3);
                return r33;
            case 3:
                AnonymousClass1NL r34 = new AnonymousClass1NL(r7.A09);
                C17770zR r04 = r7.A04;
                if (r04 != null) {
                    r34.A07 = r04.A06;
                }
                r34.A01 = migColorScheme;
                r34.A14().A0G(r10);
                return r34;
            case 4:
                String[] strArr4 = {"colorScheme"};
                BitSet bitSet4 = new BitSet(1);
                AnonymousClass1M3 r35 = new AnonymousClass1M3(r7.A09);
                C17770zR r05 = r7.A04;
                if (r05 != null) {
                    r35.A07 = r05.A06;
                }
                bitSet4.clear();
                r35.A01 = migColorScheme;
                bitSet4.set(0);
                r35.A14().A0G(r10);
                AnonymousClass11F.A0C(1, bitSet4, strArr4);
                return r35;
            case 5:
                String[] strArr5 = {"colorScheme"};
                BitSet bitSet5 = new BitSet(1);
                AnonymousClass1M4 r36 = new AnonymousClass1M4();
                C17770zR r06 = r7.A04;
                if (r06 != null) {
                    r36.A07 = r06.A06;
                }
                bitSet5.clear();
                r36.A00 = migColorScheme;
                bitSet5.set(0);
                r36.A14().A0G(r10);
                AnonymousClass11F.A0C(1, bitSet5, strArr5);
                return r36;
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                String[] strArr6 = {"colorScheme"};
                BitSet bitSet6 = new BitSet(1);
                AnonymousClass1M5 r37 = new AnonymousClass1M5();
                C17770zR r07 = r7.A04;
                if (r07 != null) {
                    r37.A07 = r07.A06;
                }
                bitSet6.clear();
                r37.A00 = migColorScheme;
                bitSet6.set(0);
                r37.A14().A0G(r10);
                AnonymousClass11F.A0C(1, bitSet6, strArr6);
                return r37;
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                String[] strArr7 = {"colorScheme", "isPinThread"};
                BitSet bitSet7 = new BitSet(2);
                C113315aN r38 = new C113315aN(r7.A09);
                C17770zR r08 = r7.A04;
                if (r08 != null) {
                    r38.A07 = r08.A06;
                }
                bitSet7.clear();
                r38.A14().A0G(r10);
                r38.A01 = migColorScheme;
                bitSet7.set(0);
                r38.A02 = true;
                bitSet7.set(1);
                AnonymousClass11F.A0C(2, bitSet7, strArr7);
                return r38;
            case 8:
                String[] strArr8 = {"colorScheme", "isPinThread"};
                BitSet bitSet8 = new BitSet(2);
                C113315aN r39 = new C113315aN(r7.A09);
                C17770zR r09 = r7.A04;
                if (r09 != null) {
                    r39.A07 = r09.A06;
                }
                bitSet8.clear();
                r39.A14().A0G(r10);
                r39.A01 = migColorScheme;
                bitSet8.set(0);
                r39.A02 = false;
                bitSet8.set(1);
                AnonymousClass11F.A0C(2, bitSet8, strArr8);
                return r39;
            default:
                throw new RuntimeException("Unknown swipe action");
        }
    }
}
