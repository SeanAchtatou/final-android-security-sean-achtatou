package X;

/* renamed from: X.1oc  reason: invalid class name and case insensitive filesystem */
public final class C34081oc extends C34071ob {
    public final boolean A00;
    private final boolean A01;

    public String toString() {
        if (this.A01) {
            return "snapdragon_v3_auto";
        }
        return "snapdragon_v3";
    }

    public C34081oc(C26311bF r1, boolean z, boolean z2) {
        super(r1);
        this.A01 = z;
        this.A00 = z2;
    }
}
