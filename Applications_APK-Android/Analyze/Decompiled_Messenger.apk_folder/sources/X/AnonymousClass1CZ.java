package X;

/* renamed from: X.1CZ  reason: invalid class name */
public final class AnonymousClass1CZ implements C20841Ea {
    public final /* synthetic */ AnonymousClass1CX A00;
    public final /* synthetic */ C157047Nv A01;
    public final /* synthetic */ C16070wR A02;
    public final /* synthetic */ AnonymousClass1AC A03;
    public final /* synthetic */ boolean A04;
    public final /* synthetic */ boolean A05;

    public AnonymousClass1CZ(AnonymousClass1AC r1, C157047Nv r2, AnonymousClass1CX r3, boolean z, boolean z2, C16070wR r6) {
        this.A03 = r1;
        this.A01 = r2;
        this.A00 = r3;
        this.A04 = z;
        this.A05 = z2;
        this.A02 = r6;
    }

    public void BVN() {
        C157047Nv r4;
        String A0B;
        AnonymousClass1AC r6 = this.A03;
        C157037Nu r5 = r6.A0D;
        if (!(r5 == null || (r4 = this.A01) == null)) {
            C16070wR r3 = r6.A03;
            AnonymousClass1CX r2 = this.A00;
            AnonymousClass1GA r1 = r6.A0F;
            if (r1.A0B() == null) {
                A0B = AnonymousClass08S.A0J("SectionTree", r6.A0G);
            } else {
                A0B = r1.A0B();
            }
            r5.onChangesetApplied(r3, r2, A0B, r4);
        }
        if (this.A04) {
            if (this.A05) {
                C27041cY.A01("dataBound");
            }
            try {
                AnonymousClass1AC r12 = this.A03;
                C16070wR r0 = this.A02;
                if (r0 != null) {
                    r12.A02 = r0;
                    AnonymousClass1AC.A0G(r12, r0);
                }
            } finally {
                if (this.A05) {
                    C27041cY.A00();
                }
            }
        }
    }

    public void BVZ(boolean z, long j) {
        AnonymousClass1AC r0 = this.A03;
        C16070wR r1 = this.A02;
        boolean z2 = this.A04;
        AnonymousClass1CX r6 = this.A00;
        if (r1 != null) {
            AnonymousClass1AC.A0H(r0, r1, z2, z, j, r6, 0);
        }
    }
}
