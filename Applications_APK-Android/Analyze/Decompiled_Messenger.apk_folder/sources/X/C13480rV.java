package X;

import com.facebook.inject.ContextScoped;

@ContextScoped
/* renamed from: X.0rV  reason: invalid class name and case insensitive filesystem */
public final class C13480rV {
    private static C04470Uu A02;
    public AnonymousClass0UN A00;
    public final AnonymousClass04b A01 = new AnonymousClass04b();

    public static final C13480rV A00(AnonymousClass1XY r4) {
        C13480rV r0;
        synchronized (C13480rV.class) {
            C04470Uu A002 = C04470Uu.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new C13480rV((AnonymousClass1XY) A02.A01());
                }
                C04470Uu r1 = A02;
                r0 = (C13480rV) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    private C13480rV(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }
}
