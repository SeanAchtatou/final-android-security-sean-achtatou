package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16m  reason: invalid class name and case insensitive filesystem */
public final class C190216m extends AnonymousClass0W1 {
    private static volatile C190216m A00;

    public C190216m() {
        super("contacts", 83, ImmutableList.of(new C190316n(), new C190416o(), new C190816s(), new AnonymousClass179(), new AnonymousClass17J()));
    }

    public static final C190216m A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C190216m.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C190216m();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    private static void A01(SQLiteDatabase sQLiteDatabase) {
        String A002 = AnonymousClass0W4.A00("contact_summaries");
        C007406x.A00(1133912823);
        sQLiteDatabase.execSQL(A002);
        C007406x.A00(1283594089);
        String A003 = AnonymousClass0W4.A00("contact_details");
        C007406x.A00(-303237712);
        sQLiteDatabase.execSQL(A003);
        C007406x.A00(811210320);
        String A004 = AnonymousClass0W4.A00("contacts_db_properties");
        C007406x.A00(-1502710743);
        sQLiteDatabase.execSQL(A004);
        C007406x.A00(112419578);
        String A005 = AnonymousClass0W4.A00("contacts");
        C007406x.A00(2106372038);
        sQLiteDatabase.execSQL(A005);
        C007406x.A00(933718781);
        String A006 = AnonymousClass0W4.A00("contacts_indexed_data");
        C007406x.A00(641876285);
        sQLiteDatabase.execSQL(A006);
        C007406x.A00(-2051018404);
        String A007 = AnonymousClass0W4.A00("ephemeral_data");
        C007406x.A00(2079875175);
        sQLiteDatabase.execSQL(A007);
        C007406x.A00(2083277082);
        String A008 = AnonymousClass0W4.A00("favorite_sms_contacts");
        C007406x.A00(-1804364925);
        sQLiteDatabase.execSQL(A008);
        C007406x.A00(1165887775);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:64:0x02c6, code lost:
        if (r0 != false) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.database.sqlite.SQLiteDatabase r22, int r23, int r24) {
        /*
            r21 = this;
            r3 = r22
            r1 = r23
            r9 = r24
            if (r1 <= r9) goto L_0x000d
            A01(r3)
            r1 = 67
        L_0x000d:
            if (r1 >= r9) goto L_0x02d2
            r20 = r9
            int r11 = r1 + 1
            r0 = 68
            if (r1 != r0) goto L_0x0024
            X.17J r0 = new X.17J
            r0.<init>()
            r0.A09(r3)
        L_0x001f:
            r20 = r11
        L_0x0021:
            r1 = r20
            goto L_0x000d
        L_0x0024:
            r0 = 69
            if (r1 != r0) goto L_0x003a
            r0 = -1191262311(0xffffffffb8fec799, float:-1.2148842E-4)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN messenger_invite_priority REAL"
            r3.execSQL(r0)
            r0 = -1443395123(0xffffffffa9f789cd, float:-1.0992908E-13)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x003a:
            r0 = 70
            if (r1 != r0) goto L_0x0050
            r0 = -1395500876(0xffffffffacd258b4, float:-5.978407E-12)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN is_memorialized INTEGER"
            r3.execSQL(r0)
            r0 = -257140620(0xfffffffff0ac5874, float:-4.2670684E29)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x0050:
            r0 = 71
            if (r1 != r0) goto L_0x0066
            r0 = -1612207360(0xffffffff9fe7ab00, float:-9.81152E-20)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN viewer_connection_status TEXT"
            r3.execSQL(r0)
            r0 = 1358683002(0x50fbdb7a, float:3.38037187E10)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x0066:
            r0 = 72
            if (r1 != r0) goto L_0x007c
            r0 = -700079730(0xffffffffd645a18e, float:-5.4324416E13)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN is_broadcast_recipient_holdout INTEGER"
            r3.execSQL(r0)
            r0 = -164391435(0xfffffffff63395f5, float:-9.10608E32)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x007c:
            r0 = 73
            if (r1 != r0) goto L_0x0092
            r0 = -940301049(0xffffffffc7f42507, float:-125002.055)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN add_source TEXT"
            r3.execSQL(r0)
            r0 = -1511266755(0xffffffffa5ebe63d, float:-4.0922017E-16)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x0092:
            r0 = 74
            if (r1 != r0) goto L_0x00a9
            r0 = -1805479494(0xffffffff946291ba, float:-1.1438824E-26)
            X.C007406x.A00(r0)
            java.lang.String r0 = "DROP TABLE favorite_contacts"
            r3.execSQL(r0)
            r0 = -129137443(0xfffffffff84d84dd, float:-1.6673682E34)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x00a9:
            r0 = 75
            if (r1 != r0) goto L_0x00c0
            r0 = 1413756461(0x5444362d, float:3.37089004E12)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN is_aloha_proxy_confirmed INTEGER"
            r3.execSQL(r0)
            r0 = -2064769572(0xffffffff84ee1ddc, float:-5.5980953E-36)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x00c0:
            r0 = 76
            if (r1 != r0) goto L_0x00d7
            r0 = 1974723757(0x75b3e4ad, float:4.560836E32)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN aloha_proxy_user_owners TEXT"
            r3.execSQL(r0)
            r0 = 975094899(0x3a1ec473, float:6.056495E-4)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x00d7:
            r0 = 77
            if (r1 != r0) goto L_0x00ee
            r0 = -1054173997(0xffffffffc12a94d3, float:-10.661334)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN is_message_ignored_by_viewer INTEGER"
            r3.execSQL(r0)
            r0 = 442035338(0x1a58ec8a, float:4.4858828E-23)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x00ee:
            r0 = 78
            if (r1 != r0) goto L_0x0105
            r0 = 952118923(0x38c02e8b, float:9.163943E-5)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN favorite_color TEXT"
            r3.execSQL(r0)
            r0 = 1542117003(0x5bead68b, float:1.32202074E17)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x0105:
            r0 = 79
            if (r1 != r0) goto L_0x011c
            r0 = 1440622011(0x55de25bb, float:3.05317041E13)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN is_viewer_managing_parent INTEGER"
            r3.execSQL(r0)
            r0 = -1146082232(0xffffffffbbb02c48, float:-0.0053763725)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x011c:
            r0 = 80
            if (r1 != r0) goto L_0x0133
            r0 = -867018334(0xffffffffcc5259a2, float:-5.5142024E7)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN work_info TEXT"
            r3.execSQL(r0)
            r0 = -1050267095(0xffffffffc1663229, float:-14.387246)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x0133:
            r0 = 81
            if (r1 != r0) goto L_0x014a
            r0 = 1562138337(0x5d1c56e1, float:7.0408993E17)
            X.C007406x.A00(r0)
            java.lang.String r0 = "ALTER TABLE contacts ADD COLUMN is_managing_parent_approved_user INTEGER"
            r3.execSQL(r0)
            r0 = -1232097450(0xffffffffb68faf56, float:-4.282144E-6)
            X.C007406x.A00(r0)
            goto L_0x001f
        L_0x014a:
            r0 = 82
            if (r1 != r0) goto L_0x02c8
            X.0jE r2 = X.AnonymousClass0jE.getInstance()
            X.0W6 r0 = X.C190516p.A08
            java.lang.String r1 = r0.A00
            X.0W6 r0 = X.C190516p.A09
            java.lang.String r0 = r0.A00
            java.lang.String[] r14 = new java.lang.String[]{r1, r0}
            java.lang.String r13 = "contacts"
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r12 = r3
            android.database.Cursor r8 = r12.query(r13, r14, r15, r16, r17, r18, r19)
            X.0W6 r0 = X.C190516p.A08
            java.lang.String r0 = r0.A00
            int r7 = r8.getColumnIndexOrThrow(r0)
            X.0W6 r0 = X.C190516p.A09
            java.lang.String r0 = r0.A00
            int r6 = r8.getColumnIndexOrThrow(r0)
            r8.moveToFirst()
            r0 = -1731131319(0xffffffff98d10849, float:-5.40336E-24)
            X.C007406x.A01(r3, r0)     // Catch:{ all -> 0x02b1 }
        L_0x0187:
            boolean r0 = r8.isAfterLast()     // Catch:{ all -> 0x02b1 }
            if (r0 != 0) goto L_0x02ad
            java.lang.String r10 = r8.getString(r7)     // Catch:{ all -> 0x02b1 }
            java.lang.String r1 = r8.getString(r6)     // Catch:{ all -> 0x02b1 }
            if (r1 == 0) goto L_0x02a8
            if (r10 == 0) goto L_0x02a8
            java.lang.Class<com.facebook.contacts.graphql.FlatbufferContact> r0 = com.facebook.contacts.graphql.FlatbufferContact.class
            java.lang.Object r5 = r2.readValue(r1, r0)     // Catch:{ all -> 0x02b1 }
            com.facebook.contacts.graphql.FlatbufferContact r5 = (com.facebook.contacts.graphql.FlatbufferContact) r5     // Catch:{ all -> 0x02b1 }
            X.7Tl r4 = new X.7Tl     // Catch:{ all -> 0x02b1 }
            r4.<init>()     // Catch:{ all -> 0x02b1 }
            com.facebook.graphql.enums.GraphQLAccountClaimStatus r0 = r5.mAccountClaimStatus     // Catch:{ all -> 0x02b1 }
            r4.A0F = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mContactId     // Catch:{ all -> 0x02b1 }
            r4.A0X = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mProfileFbid     // Catch:{ all -> 0x02b1 }
            r4.A0i = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mGraphApiWriteId     // Catch:{ all -> 0x02b1 }
            r4.A0a = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.user.model.Name r0 = r5.mName     // Catch:{ all -> 0x02b1 }
            r4.A0M = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.user.model.Name r0 = r5.mPhoneticName     // Catch:{ all -> 0x02b1 }
            r4.A0N = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mSmallPictureUrl     // Catch:{ all -> 0x02b1 }
            r4.A0j = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mBigPictureUrl     // Catch:{ all -> 0x02b1 }
            r4.A0W = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mHugePictureUrl     // Catch:{ all -> 0x02b1 }
            r4.A0b = r0     // Catch:{ all -> 0x02b1 }
            int r0 = r5.mSmallPictureSize     // Catch:{ all -> 0x02b1 }
            r4.A08 = r0     // Catch:{ all -> 0x02b1 }
            int r0 = r5.mBigPictureSize     // Catch:{ all -> 0x02b1 }
            r4.A04 = r0     // Catch:{ all -> 0x02b1 }
            int r0 = r5.mHugePictureSize     // Catch:{ all -> 0x02b1 }
            r4.A05 = r0     // Catch:{ all -> 0x02b1 }
            float r0 = r5.mCommunicationRank     // Catch:{ all -> 0x02b1 }
            r4.A00 = r0     // Catch:{ all -> 0x02b1 }
            float r0 = r5.mWithTaggingRank     // Catch:{ all -> 0x02b1 }
            r4.A03 = r0     // Catch:{ all -> 0x02b1 }
            com.google.common.collect.ImmutableList<com.facebook.contacts.graphql.ContactPhone> r0 = r5.mPhones     // Catch:{ all -> 0x02b1 }
            r4.A0V = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsMessageBlockedByViewer     // Catch:{ all -> 0x02b1 }
            r4.A0m = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mCanMessage     // Catch:{ all -> 0x02b1 }
            r4.A0k = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.common.util.TriState r0 = r5.mIsMobilePushable     // Catch:{ all -> 0x02b1 }
            r4.A0D = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsMessengerUser     // Catch:{ all -> 0x02b1 }
            r4.A0o = r0     // Catch:{ all -> 0x02b1 }
            long r0 = r5.mMessengerInstallTimeInMS     // Catch:{ all -> 0x02b1 }
            r4.A0C = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsMemorialized     // Catch:{ all -> 0x02b1 }
            r4.A0l = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsBroadcastRecipientHoldout     // Catch:{ all -> 0x02b1 }
            r4.A0s = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsOnViewerContactList     // Catch:{ all -> 0x02b1 }
            r4.A0v = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.graphql.enums.GraphQLFriendshipStatus r0 = r5.mFriendshipStatus     // Catch:{ all -> 0x02b1 }
            r4.A0H = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.graphql.enums.GraphQLSubscribeStatus r0 = r5.mSubscribeStatus     // Catch:{ all -> 0x02b1 }
            r4.A0J = r0     // Catch:{ all -> 0x02b1 }
            X.2B4 r0 = r5.mContactProfileType     // Catch:{ all -> 0x02b1 }
            r4.A0E = r0     // Catch:{ all -> 0x02b1 }
            com.google.common.collect.ImmutableList<java.lang.String> r0 = r5.mNameSearchTokens     // Catch:{ all -> 0x02b1 }
            r4.A0U = r0     // Catch:{ all -> 0x02b1 }
            long r0 = r5.mAddedTimeInMS     // Catch:{ all -> 0x02b1 }
            r4.A09 = r0     // Catch:{ all -> 0x02b1 }
            int r1 = r5.mBirthdayMonth     // Catch:{ all -> 0x02b1 }
            int r0 = r5.mBirthdayDay     // Catch:{ all -> 0x02b1 }
            r4.A07 = r1     // Catch:{ all -> 0x02b1 }
            r4.A06 = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mCityName     // Catch:{ all -> 0x02b1 }
            r4.A0d = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsPartial     // Catch:{ all -> 0x02b1 }
            r4.A0w = r0     // Catch:{ all -> 0x02b1 }
            long r0 = r5.mLastFetchTime     // Catch:{ all -> 0x02b1 }
            r4.A0A = r0     // Catch:{ all -> 0x02b1 }
            long r0 = r5.mMontageThreadFBID     // Catch:{ all -> 0x02b1 }
            r4.A0B = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mCanSeeViewerMontageThread     // Catch:{ all -> 0x02b1 }
            r4.A0p = r0     // Catch:{ all -> 0x02b1 }
            float r0 = r5.mPhatRank     // Catch:{ all -> 0x02b1 }
            r4.A02 = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mUsername     // Catch:{ all -> 0x02b1 }
            r4.A0h = r0     // Catch:{ all -> 0x02b1 }
            float r0 = r5.mMessengerInvitePriority     // Catch:{ all -> 0x02b1 }
            r4.A01 = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mCanViewerSendMoney     // Catch:{ all -> 0x02b1 }
            r4.A0q = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.graphql.enums.GraphQLContactConnectionStatus r0 = r5.mViewerConnectionStatus     // Catch:{ all -> 0x02b1 }
            r4.A0G = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.graphql.enums.GraphQLMessengerContactCreationSource r0 = r5.mAddSource     // Catch:{ all -> 0x02b1 }
            r4.A0I = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.user.model.InstagramUser r0 = r5.mConnectedInstagramUser     // Catch:{ all -> 0x02b1 }
            r4.A0L = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsAlohaProxyConfirmed     // Catch:{ all -> 0x02b1 }
            r4.A0r = r0     // Catch:{ all -> 0x02b1 }
            com.google.common.collect.ImmutableList<com.facebook.user.model.AlohaUser> r0 = r5.mAlohaProxyUserOwners     // Catch:{ all -> 0x02b1 }
            r4.A0P = r0     // Catch:{ all -> 0x02b1 }
            com.google.common.collect.ImmutableList<com.facebook.user.model.AlohaProxyUser> r0 = r5.mAlohaProxyUsersOwned     // Catch:{ all -> 0x02b1 }
            r4.A0Q = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsMessageIgnoredByViewer     // Catch:{ all -> 0x02b1 }
            r4.A0n = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mFavoriteColor     // Catch:{ all -> 0x02b1 }
            r4.A0f = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.user.model.WorkUserInfo r0 = r5.mWorkUserInfo     // Catch:{ all -> 0x02b1 }
            r4.A0O = r0     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r5.mCurrentEducationSchoolName     // Catch:{ all -> 0x02b1 }
            r4.A0e = r0     // Catch:{ all -> 0x02b1 }
            com.google.common.collect.ImmutableList<java.lang.String> r0 = r5.mCurrentWorkEmployerNames     // Catch:{ all -> 0x02b1 }
            r4.A0R = r0     // Catch:{ all -> 0x02b1 }
            com.google.common.collect.ImmutableList<java.lang.String> r0 = r5.mFamilyRelationshipUserIds     // Catch:{ all -> 0x02b1 }
            r4.A0S = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsViewerManagingParent     // Catch:{ all -> 0x02b1 }
            r4.A0x = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.graphql.enums.GraphQLUnifiedStoriesParticipantConnectionType r0 = r5.mUnifiedStoriesConnectionType     // Catch:{ all -> 0x02b1 }
            r4.A0K = r0     // Catch:{ all -> 0x02b1 }
            boolean r0 = r5.mIsManagingParentApprovedUser     // Catch:{ all -> 0x02b1 }
            r4.A0u = r0     // Catch:{ all -> 0x02b1 }
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02     // Catch:{ all -> 0x02b1 }
            r4.A0T = r0     // Catch:{ all -> 0x02b1 }
            com.facebook.contacts.graphql.Contact r0 = new com.facebook.contacts.graphql.Contact     // Catch:{ all -> 0x02b1 }
            r0.<init>(r4)     // Catch:{ all -> 0x02b1 }
            java.lang.String r1 = r2.writeValueAsString(r0)     // Catch:{ all -> 0x02b1 }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ all -> 0x02b1 }
            r4.<init>()     // Catch:{ all -> 0x02b1 }
            X.0W6 r0 = X.C190516p.A09     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x02b1 }
            r4.put(r0, r1)     // Catch:{ all -> 0x02b1 }
            X.0W6 r0 = X.C190516p.A08     // Catch:{ all -> 0x02b1 }
            java.lang.String r1 = r0.A00     // Catch:{ all -> 0x02b1 }
            java.lang.String r0 = "=?"
            java.lang.String r1 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ all -> 0x02b1 }
            java.lang.String[] r0 = new java.lang.String[]{r10}     // Catch:{ all -> 0x02b1 }
            r3.update(r13, r4, r1, r0)     // Catch:{ all -> 0x02b1 }
        L_0x02a8:
            r8.moveToNext()     // Catch:{ all -> 0x02b1 }
            goto L_0x0187
        L_0x02ad:
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x02b1 }
            goto L_0x02bc
        L_0x02b1:
            r0 = -397373866(0xffffffffe8508e56, float:-3.9395115E24)
            X.C007406x.A02(r3, r0)
            r8.close()
            r0 = 0
            goto L_0x02c6
        L_0x02bc:
            r0 = -1947877776(0xffffffff8be5be70, float:-8.849413E-32)
            X.C007406x.A02(r3, r0)
            r8.close()
            r0 = 1
        L_0x02c6:
            if (r0 != 0) goto L_0x001f
        L_0x02c8:
            A01(r3)
            r0 = r21
            r0.A04(r3)
            goto L_0x0021
        L_0x02d2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C190216m.A08(android.database.sqlite.SQLiteDatabase, int, int):void");
    }
}
