package X;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1j2  reason: invalid class name and case insensitive filesystem */
public final class C31031j2 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.stash.di.CaskEvictionBridge$1";
    public final /* synthetic */ C31021j1 A00;
    public final /* synthetic */ C31001iz A01;
    public final /* synthetic */ File A02;

    public C31031j2(C31021j1 r1, File file, C31001iz r3) {
        this.A00 = r1;
        this.A02 = file;
        this.A01 = r3;
    }

    public void run() {
        C31021j1 r6 = this.A00;
        File file = this.A02;
        C31001iz r11 = this.A01;
        C31071j6 r4 = new C31071j6(r11.A03);
        r4.A00(r11.A02);
        C30961iv r10 = r11.A00;
        if (r10 == null && r11.A01 == null) {
            ((AnonymousClass09P) AnonymousClass1XX.A02(2, AnonymousClass1Y3.Amr, r6.A00)).CGY("FBStashFactory", AnonymousClass08S.A0P("Config for ", r11.A03, " didn't specify an eviction config. Is this what you want?"));
        } else {
            AnonymousClass0ZZ r8 = r11.A01;
            List list = r11.A04;
            AnonymousClass2L8 r7 = null;
            if (list != null && !list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (C30991iy r1 : r11.A04) {
                    if (r1 instanceof C30981ix) {
                        arrayList.add((C30981ix) r1);
                    }
                }
                if (!arrayList.isEmpty()) {
                    r7 = new AnonymousClass2L8(arrayList);
                }
            }
            r4.A00(new AnonymousClass2L6(r10, r8, r7));
        }
        ((AnonymousClass15S) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8B, r6.A00)).A04(file, r4);
    }
}
