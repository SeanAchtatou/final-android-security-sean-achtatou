package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.proxygen.LigerSamplePolicy;

/* renamed from: X.1hF  reason: invalid class name and case insensitive filesystem */
public final class C29951hF {
    public AnonymousClass1ZE A00;
    private AnonymousClass0Ud A01;
    private C29961hG A02;

    public void A00(String str, String str2, boolean z, boolean z2, String str3, String str4) {
        boolean z3 = !this.A02.A01();
        Boolean valueOf = Boolean.valueOf(this.A01.A0G());
        Long valueOf2 = Long.valueOf((long) LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT);
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(this.A00.A01("android_mgeoapi_module_use"), 25);
        if (uSLEBaseShape0S0000000.A0G()) {
            uSLEBaseShape0S0000000.A0D("module_name", str);
            uSLEBaseShape0S0000000.A0D("method_name", str2);
            uSLEBaseShape0S0000000.A08("is_app_in_background_no_delay", Boolean.valueOf(z3));
            uSLEBaseShape0S0000000.A08("is_blocked_by_failsafe", Boolean.valueOf(z));
            uSLEBaseShape0S0000000.A08("is_initiated_by_failsafe", Boolean.valueOf(z2));
            uSLEBaseShape0S0000000.A0D("token", str3);
            uSLEBaseShape0S0000000.A0D("caller_context", str4);
            uSLEBaseShape0S0000000.A08("is_app_in_background_with_delay", valueOf);
            uSLEBaseShape0S0000000.A0C("delayed_background_grace_period_ms", valueOf2);
            uSLEBaseShape0S0000000.A06();
        }
    }

    public C29951hF(AnonymousClass1ZE r1, C29961hG r2, AnonymousClass0Ud r3) {
        this.A00 = r1;
        this.A02 = r2;
        this.A01 = r3;
    }
}
