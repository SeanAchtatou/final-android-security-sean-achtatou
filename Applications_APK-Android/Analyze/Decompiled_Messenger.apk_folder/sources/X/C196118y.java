package X;

import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.18y  reason: invalid class name and case insensitive filesystem */
public final class C196118y {
    public C194318c A00;
    public boolean A01 = true;
    public final C20871Ed A02;

    public void A00(MigColorScheme migColorScheme) {
        int i;
        int i2;
        if (migColorScheme != null) {
            i = migColorScheme.B2D();
            i2 = migColorScheme.Aea();
        } else {
            i = -1;
            i2 = -16743169;
        }
        this.A02.A0A.setBackgroundColor(i);
        this.A02.A0D(i2);
    }

    public C196118y(C20871Ed r3) {
        this.A02 = r3;
        A00(null);
        this.A02.A0C = new AnonymousClass198(this);
    }
}
