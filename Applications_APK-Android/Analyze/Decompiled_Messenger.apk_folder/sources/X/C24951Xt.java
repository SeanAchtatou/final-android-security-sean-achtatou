package X;

import com.google.common.base.Preconditions;
import java.util.NoSuchElementException;

/* renamed from: X.1Xt  reason: invalid class name and case insensitive filesystem */
public abstract class C24951Xt extends C24961Xu {
    private int A00;
    private final int A01;

    public Object A00(int i) {
        if (!(this instanceof C24941Xs)) {
            return ((AnonymousClass0jB) this).A00.get(i);
        }
        C24941Xs r0 = (C24941Xs) this;
        return r0.A01[r0.A00 + i];
    }

    public final boolean hasNext() {
        if (this.A00 < this.A01) {
            return true;
        }
        return false;
    }

    public final boolean hasPrevious() {
        if (this.A00 > 0) {
            return true;
        }
        return false;
    }

    public final int nextIndex() {
        return this.A00;
    }

    public final int previousIndex() {
        return this.A00 - 1;
    }

    public C24951Xt(int i, int i2) {
        Preconditions.checkPositionIndex(i2, i);
        this.A01 = i;
        this.A00 = i2;
    }

    public final Object next() {
        if (hasNext()) {
            int i = this.A00;
            this.A00 = i + 1;
            return A00(i);
        }
        throw new NoSuchElementException();
    }

    public final Object previous() {
        if (hasPrevious()) {
            int i = this.A00 - 1;
            this.A00 = i;
            return A00(i);
        }
        throw new NoSuchElementException();
    }
}
