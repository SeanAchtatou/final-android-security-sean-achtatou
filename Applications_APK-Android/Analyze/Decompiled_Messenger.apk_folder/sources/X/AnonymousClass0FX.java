package X;

import android.util.SparseIntArray;
import java.util.Arrays;

/* renamed from: X.0FX  reason: invalid class name */
public final class AnonymousClass0FX extends AnonymousClass0FM {
    public final SparseIntArray[] timeInStateS;

    private void A00(AnonymousClass0FX r7) {
        int i = 0;
        while (true) {
            SparseIntArray[] sparseIntArrayArr = this.timeInStateS;
            if (i < sparseIntArrayArr.length) {
                SparseIntArray sparseIntArray = r7.timeInStateS[i];
                SparseIntArray sparseIntArray2 = sparseIntArrayArr[i];
                sparseIntArray2.clear();
                for (int i2 = 0; i2 < sparseIntArray.size(); i2++) {
                    sparseIntArray2.append(sparseIntArray.keyAt(i2), sparseIntArray.valueAt(i2));
                }
                i++;
            } else {
                return;
            }
        }
    }

    public static boolean A01(SparseIntArray sparseIntArray, SparseIntArray sparseIntArray2) {
        if (sparseIntArray != sparseIntArray2) {
            int size = sparseIntArray.size();
            if (size == sparseIntArray2.size()) {
                int i = 0;
                while (i < size) {
                    if (sparseIntArray.keyAt(i) == sparseIntArray2.keyAt(i) && sparseIntArray.valueAt(i) == sparseIntArray2.valueAt(i)) {
                        i++;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                AnonymousClass0FX r7 = (AnonymousClass0FX) obj;
                int length = this.timeInStateS.length;
                if (length == r7.timeInStateS.length) {
                    int i = 0;
                    while (i < length) {
                        if (A01(this.timeInStateS[i], r7.timeInStateS[i])) {
                            i++;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int i2 = 0;
        while (true) {
            SparseIntArray[] sparseIntArrayArr = this.timeInStateS;
            if (i >= sparseIntArrayArr.length) {
                return i2;
            }
            SparseIntArray sparseIntArray = sparseIntArrayArr[i];
            int size = sparseIntArray.size();
            for (int i3 = 0; i3 < size; i3++) {
                i2 += sparseIntArray.keyAt(i3) ^ sparseIntArray.valueAt(i3);
            }
            i++;
        }
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A00((AnonymousClass0FX) r1);
        return this;
    }

    public AnonymousClass0FM A07(AnonymousClass0FM r11, AnonymousClass0FM r12) {
        boolean z;
        AnonymousClass0FX r112 = (AnonymousClass0FX) r11;
        AnonymousClass0FX r122 = (AnonymousClass0FX) r12;
        if (r122 == null) {
            r122 = new AnonymousClass0FX();
        }
        if (r112 != null) {
            int i = 0;
            while (true) {
                SparseIntArray[] sparseIntArrayArr = this.timeInStateS;
                if (i >= sparseIntArrayArr.length) {
                    break;
                }
                SparseIntArray sparseIntArray = sparseIntArrayArr[i];
                SparseIntArray sparseIntArray2 = r112.timeInStateS[i];
                SparseIntArray sparseIntArray3 = r122.timeInStateS[i];
                int size = sparseIntArray.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        z = false;
                        break;
                    }
                    int keyAt = sparseIntArray.keyAt(i2);
                    int valueAt = sparseIntArray.valueAt(i2) - sparseIntArray2.get(keyAt, 0);
                    if (valueAt < 0) {
                        z = true;
                        break;
                    }
                    sparseIntArray3.put(keyAt, valueAt);
                    i2++;
                }
                if (z) {
                    sparseIntArray3.clear();
                    for (int i3 = 0; i3 < sparseIntArray.size(); i3++) {
                        sparseIntArray3.append(sparseIntArray.keyAt(i3), sparseIntArray.valueAt(i3));
                    }
                }
                i++;
            }
        } else {
            r122.A00(this);
        }
        return r122;
    }

    public AnonymousClass0FM A08(AnonymousClass0FM r10, AnonymousClass0FM r11) {
        AnonymousClass0FX r102 = (AnonymousClass0FX) r10;
        AnonymousClass0FX r112 = (AnonymousClass0FX) r11;
        if (r112 == null) {
            r112 = new AnonymousClass0FX();
        }
        if (r102 != null) {
            int i = 0;
            while (true) {
                SparseIntArray[] sparseIntArrayArr = this.timeInStateS;
                if (i >= sparseIntArrayArr.length) {
                    break;
                }
                SparseIntArray sparseIntArray = sparseIntArrayArr[i];
                SparseIntArray sparseIntArray2 = r102.timeInStateS[i];
                SparseIntArray sparseIntArray3 = r112.timeInStateS[i];
                for (int i2 = 0; i2 < sparseIntArray.size(); i2++) {
                    int keyAt = sparseIntArray.keyAt(i2);
                    sparseIntArray3.put(keyAt, sparseIntArray.valueAt(i2) + sparseIntArray2.get(keyAt, 0));
                }
                for (int i3 = 0; i3 < sparseIntArray2.size(); i3++) {
                    int keyAt2 = sparseIntArray2.keyAt(i3);
                    if (sparseIntArray.indexOfKey(keyAt2) < 0) {
                        sparseIntArray3.put(keyAt2, sparseIntArray2.valueAt(i3));
                    }
                }
                i++;
            }
        } else {
            r112.A00(this);
        }
        return r112;
    }

    public String toString() {
        return AnonymousClass08S.A0K("CpuFrequencyMetrics{timeInStateS=", Arrays.toString(this.timeInStateS), '}');
    }

    public AnonymousClass0FX() {
        int i = C03110Kb.A00;
        this.timeInStateS = new SparseIntArray[i];
        for (int i2 = 0; i2 < i; i2++) {
            this.timeInStateS[i2] = new SparseIntArray(0);
        }
    }
}
