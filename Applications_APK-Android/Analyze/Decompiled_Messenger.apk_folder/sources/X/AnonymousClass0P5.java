package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0P5  reason: invalid class name */
public final class AnonymousClass0P5 extends C03160Kg {
    public long A00() {
        return -7207777727314674541L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        C02600Fo r32 = (C02600Fo) r3;
        dataOutput.writeInt(r32.mqttFullPowerTimeS);
        dataOutput.writeInt(r32.mqttLowPowerTimeS);
        dataOutput.writeLong(r32.mqttTxBytes);
        dataOutput.writeLong(r32.mqttRxBytes);
        dataOutput.writeInt(r32.mqttRequestCount);
        dataOutput.writeInt(r32.mqttWakeupCount);
        dataOutput.writeInt(r32.ligerFullPowerTimeS);
        dataOutput.writeInt(r32.ligerLowPowerTimeS);
        dataOutput.writeLong(r32.ligerTxBytes);
        dataOutput.writeLong(r32.ligerRxBytes);
        dataOutput.writeInt(r32.ligerRequestCount);
        dataOutput.writeInt(r32.ligerWakeupCount);
        dataOutput.writeInt(r32.proxygenActiveRadioTimeS);
        dataOutput.writeInt(r32.proxygenTailRadioTimeS);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        C02600Fo r32 = (C02600Fo) r3;
        r32.mqttFullPowerTimeS = dataInput.readInt();
        r32.mqttLowPowerTimeS = dataInput.readInt();
        r32.mqttTxBytes = dataInput.readLong();
        r32.mqttRxBytes = dataInput.readLong();
        r32.mqttRequestCount = dataInput.readInt();
        r32.mqttWakeupCount = dataInput.readInt();
        r32.ligerFullPowerTimeS = dataInput.readInt();
        r32.ligerLowPowerTimeS = dataInput.readInt();
        r32.ligerTxBytes = dataInput.readLong();
        r32.ligerRxBytes = dataInput.readLong();
        r32.ligerRequestCount = dataInput.readInt();
        r32.ligerWakeupCount = dataInput.readInt();
        r32.proxygenActiveRadioTimeS = dataInput.readInt();
        r32.proxygenTailRadioTimeS = dataInput.readInt();
        return true;
    }
}
