package X;

import com.facebook.graphql.calls.GQLCallInputCInputShape1S0000000;
import com.facebook.graphql.enums.GraphQLMessengerInboxUnitType;
import com.facebook.graphservice.modelutil.GSMBuilderShape0S0000000;
import com.facebook.graphservice.modelutil.GSTModelShape1S0000000;
import com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper;
import com.facebook.messaging.inbox2.items.InboxUnitItem;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.Executor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.17O  reason: invalid class name */
public final class AnonymousClass17O extends AnonymousClass1EP {
    public long A00;
    public final AnonymousClass06B A01;
    public final C09470hQ A02;
    public final C05530Zh A03;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, int]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0D(C27161ck r8) {
        String str;
        C25511Zx r6 = this.A03.A06;
        GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MessengerInboxUnit", GSMBuilderShape0S0000000.class, -2129855990);
        gSMBuilderShape0S0000000.A0I(r8.A0U());
        gSMBuilderShape0S0000000.setInt("messenger_inbox_unit_hides_remaining", (Integer) 0);
        GSTModelShape1S0000000 gSTModelShape1S0000000 = (GSTModelShape1S0000000) gSMBuilderShape0S0000000.getResult(GSTModelShape1S0000000.class, -2129855990);
        C82573wC r4 = new C82573wC();
        GQLCallInputCInputShape1S0000000 gQLCallInputCInputShape1S0000000 = new GQLCallInputCInputShape1S0000000(154);
        gQLCallInputCInputShape1S0000000.A09("action_type", "HIDE");
        gQLCallInputCInputShape1S0000000.A09(C99084oO.$const$string(231), r8.A0U());
        gQLCallInputCInputShape1S0000000.A09("unit_logging_data", r8.A0P(392918208));
        GraphQLMessengerInboxUnitType A0Q = r8.A0Q();
        String str2 = null;
        if (A0Q == null) {
            C010708t.A0I("com.facebook.messaging.inbox2.data.graphql.InboxUnitGraphQLQueryExecutorHelper", "MessengerInboxUnitType is null");
        } else if (A0Q.ordinal() == 26) {
            LinkedList linkedList = new LinkedList();
            C24971Xv it = r8.A0T().iterator();
            while (it.hasNext()) {
                C16820xp A36 = ((GSTModelShape1S0000000) it.next()).A36();
                if (A36 != null) {
                    str = A36.Aby();
                } else {
                    str = BuildConfig.FLAVOR;
                }
                linkedList.add(str);
            }
            JSONArray jSONArray = new JSONArray((Collection) linkedList);
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("client_tokens", jSONArray.toString());
                str2 = jSONObject.toString();
            } catch (JSONException e) {
                throw new RuntimeException("A literal key was evaluated to null?", e);
            }
        }
        if (str2 != null) {
            gQLCallInputCInputShape1S0000000.A09("extra_data", str2);
        }
        r4.A04("input", gQLCallInputCInputShape1S0000000);
        C21899AkD A012 = C26931cN.A01(r4);
        A012.A00(gSTModelShape1S0000000);
        C25511Zx.A06(r6, r8, A012);
    }

    public void A0E(InboxUnitItem inboxUnitItem) {
        C25511Zx.A06(this.A03.A06, inboxUnitItem.A02, InboxUnitGraphQLQueryExecutorHelper.A00(inboxUnitItem.A02, inboxUnitItem.A01));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
     arg types: [java.lang.String, java.lang.Integer]
     candidates:
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(int, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI
      X.11R.setInt(int, java.lang.Integer):X.11R
      com.facebook.graphservice.tree.TreeBuilderJNI.setInt(java.lang.String, java.lang.Integer):com.facebook.graphservice.tree.TreeBuilderJNI */
    public void A0F(InboxUnitItem inboxUnitItem) {
        C25511Zx r3 = this.A03.A06;
        Preconditions.checkNotNull(inboxUnitItem.A01);
        C27161ck r5 = inboxUnitItem.A02;
        GSTModelShape1S0000000 gSTModelShape1S0000000 = inboxUnitItem.A01;
        C82583wD r4 = new C82583wD();
        GQLCallInputCInputShape1S0000000 gQLCallInputCInputShape1S0000000 = new GQLCallInputCInputShape1S0000000(AnonymousClass1Y3.A1C);
        gQLCallInputCInputShape1S0000000.A09("action_type", "CLICK");
        String A3m = gSTModelShape1S0000000.A3m();
        gQLCallInputCInputShape1S0000000.A09("item_id", A3m);
        gQLCallInputCInputShape1S0000000.A09("item_logging_data", r5.A0P(392918208));
        r4.A04("input", gQLCallInputCInputShape1S0000000);
        int intValue = gSTModelShape1S0000000.getIntValue(1050673705);
        int i = intValue;
        if (intValue > 0) {
            i = intValue - 1;
        }
        GSMBuilderShape0S0000000 gSMBuilderShape0S0000000 = (GSMBuilderShape0S0000000) C05850aR.A02().newTreeBuilder("MessengerInboxItem", GSMBuilderShape0S0000000.class, -923939885);
        gSMBuilderShape0S0000000.A0I(A3m);
        gSMBuilderShape0S0000000.setInt("messenger_inbox_item_hides_remaining", Integer.valueOf(gSTModelShape1S0000000.getIntValue(-1374423487)));
        gSMBuilderShape0S0000000.setInt("messenger_inbox_item_clicks_remaining", Integer.valueOf(i));
        C21899AkD A012 = C26931cN.A01(r4);
        A012.A00((GSTModelShape1S0000000) gSMBuilderShape0S0000000.getResult(GSTModelShape1S0000000.class, -923939885));
        C25511Zx.A06(r3, inboxUnitItem.A02, A012);
    }

    public AnonymousClass17O(C05530Zh r1, AnonymousClass06B r2, Executor executor, C09470hQ r4) {
        super(executor);
        this.A03 = r1;
        this.A01 = r2;
        this.A02 = r4;
    }
}
