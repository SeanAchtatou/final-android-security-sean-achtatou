package X;

import com.facebook.payments.logging.PaymentsLoggingSessionData;
import com.facebook.payments.model.PaymentItemType;

/* renamed from: X.1Cn  reason: invalid class name and case insensitive filesystem */
public final class C20461Cn extends C06020ai {
    public final /* synthetic */ PaymentsLoggingSessionData A00;
    public final /* synthetic */ PaymentItemType A01;
    public final /* synthetic */ C55122nZ A02;

    public C20461Cn(C55122nZ r1, PaymentsLoggingSessionData paymentsLoggingSessionData, PaymentItemType paymentItemType) {
        this.A02 = r1;
        this.A00 = paymentsLoggingSessionData;
        this.A01 = paymentItemType;
    }
}
