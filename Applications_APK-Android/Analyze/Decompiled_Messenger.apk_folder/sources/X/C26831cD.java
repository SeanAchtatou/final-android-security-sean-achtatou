package X;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/* renamed from: X.1cD  reason: invalid class name and case insensitive filesystem */
public final class C26831cD implements C26821cC, Serializable {
    private static final long serialVersionUID = 923268084968181479L;

    public C422628y findKeyDeserializer(C10030jR r4, C10490kF r5, C10120ja r6) {
        Class<Locale> cls = r4._class;
        Class<String> cls2 = String.class;
        if (cls == cls2 || cls == Object.class) {
            if (cls == cls2) {
                return C25054CUs.sString;
            }
            if (cls == Object.class) {
                return C25054CUs.sObject;
            }
            return new C25054CUs(cls);
        } else if (cls == UUID.class) {
            return new C25091CWm();
        } else {
            if (cls.isPrimitive()) {
                cls = C29081fq.wrapperType(cls);
            }
            if (cls == Integer.class) {
                return new C25052CUq();
            }
            if (cls == Long.class) {
                return new C25050CUo();
            }
            if (cls == Date.class) {
                return new C25053CUr();
            }
            if (cls == Calendar.class) {
                return new CUn();
            }
            if (cls == Boolean.class) {
                return new C25093CWo();
            }
            if (cls == Byte.class) {
                return new C25094CWp();
            }
            if (cls == Character.class) {
                return new C25092CWn();
            }
            if (cls == Short.class) {
                return new AnonymousClass2M3();
            }
            if (cls == Float.class) {
                return new C25055CUt();
            }
            if (cls == Double.class) {
                return new C43042Cv();
            }
            if (cls == Locale.class) {
                return new C25095CWq();
            }
            return null;
        }
    }
}
