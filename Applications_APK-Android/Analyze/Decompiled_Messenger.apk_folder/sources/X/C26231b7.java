package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import java.util.Locale;
import java.util.TreeMap;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1b7  reason: invalid class name and case insensitive filesystem */
public class C26231b7 {
    private static volatile C26231b7 A02;
    public final Context A00;
    public final AnonymousClass0ZS A01;

    public static final C26231b7 A01(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (C26231b7.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = new C26231b7(AnonymousClass1YA.A00(applicationInjector), AnonymousClass0ZS.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public C26231b7(Context context, AnonymousClass0ZS r2) {
        this.A00 = context;
        this.A01 = r2;
    }

    public ImmutableCollection A02() {
        Locale[] availableLocales = Locale.getAvailableLocales();
        String[] locales = Resources.getSystem().getAssets().getLocales();
        C07410dQ A012 = ImmutableSet.A01();
        for (Locale locale : availableLocales) {
            if (!locale.getLanguage().equals("gu") && !locale.getLanguage().equals("pa")) {
                A012.A01(locale.toString());
                A012.A01(locale.getLanguage());
            }
        }
        for (String A013 : locales) {
            Locale A014 = C007006q.A01(A013);
            A012.A01(A014.toString());
            A012.A01(A014.getLanguage());
        }
        ImmutableSet A04 = A012.build();
        TreeMap treeMap = new TreeMap();
        for (String A015 : (ImmutableSet) this.A01.A00.A02.get()) {
            Locale A016 = C007006q.A01(A015);
            if (!A04.contains(A016.toString())) {
                if (A04.contains(A016.getLanguage())) {
                    if (!A016.getLanguage().equals("zh")) {
                        treeMap.put(A016.getLanguage(), new Locale(A016.getLanguage()));
                    }
                } else if (!A016.getLanguage().equals("fb")) {
                    if (A016.getLanguage().equals("qz")) {
                        if (!A04.contains("my")) {
                        }
                    }
                }
            }
            treeMap.put(A016.toString(), A016);
        }
        return ImmutableMap.copyOf(treeMap).values();
    }

    public void A03(String str) {
        if (!C06850cB.A0B(str)) {
            Locale A012 = C007006q.A01(str);
            C005505z.A03("LanguageSwitcherCommon.setAppLocale", -674763843);
            try {
                Resources resources = this.A00.getResources();
                Configuration configuration = resources.getConfiguration();
                if (!A012.equals(configuration.locale)) {
                    configuration.locale = A012;
                    resources.updateConfiguration(configuration, resources.getDisplayMetrics());
                }
                Locale locale = A012;
                ApplicationInfo applicationInfo = this.A00.getApplicationInfo();
                if ((!((ImmutableSet) this.A01.A00.A02.get()).contains(A012.getLanguage()) && !((ImmutableSet) this.A01.A00.A02.get()).contains(A012.toString())) || (applicationInfo.flags & 4194304) == 0) {
                    locale = Locale.US;
                }
                if (Build.VERSION.SDK_INT >= 17) {
                    Resources resources2 = this.A00.getResources();
                    Configuration configuration2 = resources2.getConfiguration();
                    configuration2.setLayoutDirection(locale);
                    resources2.updateConfiguration(configuration2, resources2.getDisplayMetrics());
                }
                Locale.setDefault(A012);
            } finally {
                C005505z.A00(1669974185);
            }
        }
    }
}
