package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1OB  reason: invalid class name */
public final class AnonymousClass1OB extends AnonymousClass0UV {
    private static volatile AnonymousClass30C A00;
    private static volatile AnonymousClass1OC A01;

    public static final AnonymousClass30C A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass30C.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass30C();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final AnonymousClass1OC A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1OC.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1OC(new AnonymousClass1OD(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }
}
