package X;

import com.google.common.util.concurrent.ListenableFuture;

/* renamed from: X.0tD  reason: invalid class name and case insensitive filesystem */
public final class C14380tD implements C13970sO {
    public final /* synthetic */ C13450rS A00;

    public C14380tD(C13450rS r1) {
        this.A00 = r1;
    }

    public void BZh() {
        C13450rS r3 = this.A00;
        ListenableFuture listenableFuture = r3.A07;
        if (listenableFuture != null && !listenableFuture.isDone()) {
            r3.A07.cancel(true);
            r3.A07 = null;
        }
        C16130wX r4 = r3.A03;
        C24971Xv it = r4.A00.A00.iterator();
        while (it.hasNext()) {
            r4.A03.A01((C16040wO) it.next()).Ae6().CK1(r4.A02);
        }
        C16540xI r1 = r3.A05;
        C16540xI.A01(r1, r1.A00);
        r1.A01 = false;
        ((C15390vD) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AIz, r3.A02)).CK1(r3.A0A);
    }

    public void BZi() {
        C13450rS r5 = this.A00;
        C14410tG r4 = new C14410tG(r5);
        AnonymousClass108 r3 = (AnonymousClass108) AnonymousClass1XX.A03(AnonymousClass1Y3.BFn, r5.A02);
        r3.A02(r4);
        r3.A02 = "UpdateTabBar";
        r3.A03("ForUiThread");
        r5.A07 = ((AnonymousClass0g4) AnonymousClass1XX.A02(7, AnonymousClass1Y3.ASI, r5.A02)).A04(r3.A01(), "None");
    }
}
