package X;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Ub  reason: invalid class name and case insensitive filesystem */
public final class C04340Ub implements AnonymousClass1Y6 {
    private static final Thread A02 = Looper.getMainLooper().getThread();
    private static volatile C04340Ub A03;
    private final Handler A00;
    private volatile boolean A01;

    public void AOy(String str) {
        if (0 == 0) {
            Preconditions.checkState(!BHM(), str);
        }
    }

    public void AP0(String str) {
        if (0 == 0) {
            Preconditions.checkState(BHM(), str);
        }
    }

    public static final C04340Ub A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C04340Ub.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        r4.getApplicationInjector();
                        A03 = new C04340Ub(AnonymousClass0Uc.A00());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void AOx() {
        AOy("This operation can't be run on UI thread.");
    }

    public void AOz() {
        AP0("This operation must be run on UI thread.");
    }

    public AnonymousClass1GT AZ9(AnonymousClass1GT r2, Object... objArr) {
        r2.A01(AsyncTask.THREAD_POOL_EXECUTOR, objArr);
        return r2;
    }

    public boolean BHM() {
        if (A02 == Thread.currentThread()) {
            return true;
        }
        return false;
    }

    public void BxQ(Runnable runnable, long j) {
        AnonymousClass00S.A05(new Handler(), runnable, j, -1471740608);
    }

    public void BxR(Runnable runnable) {
        AnonymousClass00S.A04(this.A00, runnable, -1196278371);
    }

    public void BxS(Runnable runnable, long j) {
        AnonymousClass00S.A05(this.A00, runnable, j, 722338219);
    }

    public void C1d(Runnable runnable) {
        AnonymousClass00S.A02(this.A00, runnable);
    }

    private C04340Ub(Handler handler) {
        this.A00 = handler;
    }

    public void AMf(ListenableFuture listenableFuture, C04810Wg r4) {
        Preconditions.checkNotNull(listenableFuture);
        Preconditions.checkNotNull(r4);
        Preconditions.checkNotNull(Looper.myLooper(), "Must be called on a handler thread");
        C05350Yp.A08(listenableFuture, r4, new AnonymousClass0VI(new Handler()));
    }

    public void C4C(Runnable runnable) {
        if (BHM()) {
            runnable.run();
        } else {
            AnonymousClass00S.A04(this.A00, runnable, 313178383);
        }
    }

    public void CGN(long j) {
        Thread.sleep(j);
    }
}
