package X;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.0WJ  reason: invalid class name */
public final class AnonymousClass0WJ {
    public static AnonymousClass0VL A00(ExecutorService executorService) {
        if (executorService instanceof AnonymousClass0VL) {
            return (AnonymousClass0VL) executorService;
        }
        if (executorService instanceof ScheduledExecutorService) {
            return new AnonymousClass9KH((ScheduledExecutorService) executorService);
        }
        return new AnonymousClass9KK(executorService);
    }
}
