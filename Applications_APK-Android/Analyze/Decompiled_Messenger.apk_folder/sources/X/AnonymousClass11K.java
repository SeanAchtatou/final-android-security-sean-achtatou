package X;

/* renamed from: X.11K  reason: invalid class name */
public final class AnonymousClass11K {
    public byte[] _base64Buffer = null;
    public final AnonymousClass11L _bufferRecycler;
    public char[] _concatCBuffer = null;
    public C48392aM _encoding;
    public final boolean _managedResource;
    public char[] _nameCopyBuffer = null;
    public byte[] _readIOBuffer = null;
    public final Object _sourceRef;
    public char[] _tokenCBuffer = null;
    public byte[] _writeEncodingBuffer = null;

    public char[] allocConcatBuffer() {
        if (this._concatCBuffer == null) {
            char[] allocCharBuffer = this._bufferRecycler.allocCharBuffer(AnonymousClass11P.CONCAT_BUFFER, 0);
            this._concatCBuffer = allocCharBuffer;
            return allocCharBuffer;
        }
        throw new IllegalStateException("Trying to call same allocXxx() method second time");
    }

    public char[] allocNameCopyBuffer(int i) {
        if (this._nameCopyBuffer == null) {
            char[] allocCharBuffer = this._bufferRecycler.allocCharBuffer(AnonymousClass11P.NAME_COPY_BUFFER, i);
            this._nameCopyBuffer = allocCharBuffer;
            return allocCharBuffer;
        }
        throw new IllegalStateException("Trying to call same allocXxx() method second time");
    }

    public byte[] allocWriteEncodingBuffer() {
        if (this._writeEncodingBuffer == null) {
            byte[] allocByteBuffer = this._bufferRecycler.allocByteBuffer(AnonymousClass11M.WRITE_ENCODING_BUFFER);
            this._writeEncodingBuffer = allocByteBuffer;
            return allocByteBuffer;
        }
        throw new IllegalStateException("Trying to call same allocXxx() method second time");
    }

    public void releaseConcatBuffer(char[] cArr) {
        if (cArr == null) {
            return;
        }
        if (cArr == this._concatCBuffer) {
            this._concatCBuffer = null;
            this._bufferRecycler._charBuffers[AnonymousClass11P.CONCAT_BUFFER.ordinal()] = cArr;
            return;
        }
        throw new IllegalArgumentException("Trying to release buffer not owned by the context");
    }

    public void releaseReadIOBuffer(byte[] bArr) {
        if (bArr == null) {
            return;
        }
        if (bArr == this._readIOBuffer) {
            this._readIOBuffer = null;
            this._bufferRecycler._byteBuffers[AnonymousClass11M.READ_IO_BUFFER.ordinal()] = bArr;
            return;
        }
        throw new IllegalArgumentException("Trying to release buffer not owned by the context");
    }

    public void releaseWriteEncodingBuffer(byte[] bArr) {
        if (bArr == null) {
            return;
        }
        if (bArr == this._writeEncodingBuffer) {
            this._writeEncodingBuffer = null;
            this._bufferRecycler._byteBuffers[AnonymousClass11M.WRITE_ENCODING_BUFFER.ordinal()] = bArr;
            return;
        }
        throw new IllegalArgumentException("Trying to release buffer not owned by the context");
    }

    public AnonymousClass11K(AnonymousClass11L r2, Object obj, boolean z) {
        this._bufferRecycler = r2;
        this._sourceRef = obj;
        this._managedResource = z;
    }
}
