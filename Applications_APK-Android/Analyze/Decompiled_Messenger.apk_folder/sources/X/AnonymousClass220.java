package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.220  reason: invalid class name */
public final class AnonymousClass220 {
    private static volatile AnonymousClass220 A00;

    public static final AnonymousClass220 A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AnonymousClass220.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new AnonymousClass220();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
