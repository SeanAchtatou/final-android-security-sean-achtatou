package X;

/* renamed from: X.1Yt  reason: invalid class name and case insensitive filesystem */
public final class C25211Yt {
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        if (r7.equals("app_foregrounded") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        if (r7.equals("app_backgrounded") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        if (r7.equals("application_init") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        if (r7.equals("login_complete") == false) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0047, code lost:
        if (r7.equals("action_network_connectivity_changed") == false) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.String r7) {
        /*
            int r0 = r7.hashCode()
            r6 = 0
            r5 = 4
            r4 = 3
            r3 = 2
            r2 = 1
            switch(r0) {
                case -1466853626: goto L_0x0040;
                case -1210009265: goto L_0x0036;
                case 453279: goto L_0x002c;
                case 130218923: goto L_0x0022;
                case 566124160: goto L_0x0018;
                default: goto L_0x000c;
            }
        L_0x000c:
            r1 = -1
        L_0x000d:
            if (r1 == 0) goto L_0x0054
            if (r1 == r2) goto L_0x0051
            if (r1 == r3) goto L_0x004e
            if (r1 == r4) goto L_0x004b
            if (r1 == r5) goto L_0x004a
            return r6
        L_0x0018:
            java.lang.String r0 = "app_foregrounded"
            boolean r0 = r7.equals(r0)
            r1 = 3
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x0022:
            java.lang.String r0 = "app_backgrounded"
            boolean r0 = r7.equals(r0)
            r1 = 1
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x002c:
            java.lang.String r0 = "application_init"
            boolean r0 = r7.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x0036:
            java.lang.String r0 = "login_complete"
            boolean r0 = r7.equals(r0)
            r1 = 4
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x0040:
            java.lang.String r0 = "action_network_connectivity_changed"
            boolean r0 = r7.equals(r0)
            r1 = 0
            if (r0 != 0) goto L_0x000d
            goto L_0x000c
        L_0x004a:
            return r2
        L_0x004b:
            r0 = 58
            return r0
        L_0x004e:
            r0 = 25
            return r0
        L_0x0051:
            r0 = 51
            return r0
        L_0x0054:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25211Yt.A00(java.lang.String):int");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        if (r8.equals("app_locale_changed") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r8.equals("app_backgrounded") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        if (r8.equals("app_background_report_time_spent_only") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        if (r8.equals("app_foregrounded_immediate") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        if (r8.equals("device_locale_changed") == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0054, code lost:
        if (r8.equals("app_foreground_report_time_spent_only") == false) goto L_0x000d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(java.lang.String r8) {
        /*
            int r0 = r8.hashCode()
            r7 = 0
            r5 = 5
            r4 = 3
            r3 = 4
            r2 = 2
            r1 = 1
            switch(r0) {
                case -1814449310: goto L_0x004d;
                case -1561053480: goto L_0x0043;
                case -494931790: goto L_0x0039;
                case -197161193: goto L_0x002f;
                case 130218923: goto L_0x0025;
                case 757558189: goto L_0x001b;
                default: goto L_0x000d;
            }
        L_0x000d:
            r6 = -1
        L_0x000e:
            if (r6 == 0) goto L_0x005b
            if (r6 == r1) goto L_0x005a
            if (r6 == r2) goto L_0x0059
            if (r6 == r4) goto L_0x005b
            if (r6 == r3) goto L_0x0059
            if (r6 == r5) goto L_0x0057
            return r7
        L_0x001b:
            java.lang.String r0 = "app_locale_changed"
            boolean r0 = r8.equals(r0)
            r6 = 2
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0025:
            java.lang.String r0 = "app_backgrounded"
            boolean r0 = r8.equals(r0)
            r6 = 1
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x002f:
            java.lang.String r0 = "app_background_report_time_spent_only"
            boolean r0 = r8.equals(r0)
            r6 = 0
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0039:
            java.lang.String r0 = "app_foregrounded_immediate"
            boolean r0 = r8.equals(r0)
            r6 = 5
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0043:
            java.lang.String r0 = "device_locale_changed"
            boolean r0 = r8.equals(r0)
            r6 = 4
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x004d:
            java.lang.String r0 = "app_foreground_report_time_spent_only"
            boolean r0 = r8.equals(r0)
            r6 = 3
            if (r0 != 0) goto L_0x000e
            goto L_0x000d
        L_0x0057:
            r0 = 6
            return r0
        L_0x0059:
            return r2
        L_0x005a:
            return r3
        L_0x005b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25211Yt.A01(java.lang.String):int");
    }
}
