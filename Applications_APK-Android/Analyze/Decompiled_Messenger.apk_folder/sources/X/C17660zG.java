package X;

/* renamed from: X.0zG  reason: invalid class name and case insensitive filesystem */
public enum C17660zG {
    INHERIT(0),
    LTR(1),
    RTL(2);
    
    public final int mIntValue;

    public static C17660zG A00(int i) {
        if (i == 0) {
            return INHERIT;
        }
        if (i == 1) {
            return LTR;
        }
        if (i == 2) {
            return RTL;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", i));
    }

    private C17660zG(int i) {
        this.mIntValue = i;
    }
}
