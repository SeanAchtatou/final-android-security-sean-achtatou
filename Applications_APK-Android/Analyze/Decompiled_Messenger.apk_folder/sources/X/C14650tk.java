package X;

import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/* renamed from: X.0tk  reason: invalid class name and case insensitive filesystem */
public final class C14650tk {
    public int A00 = 0;
    public USLEBaseShape0S0000000 A01 = null;
    public Locale A02 = null;
    public Map A03 = null;
    public final AnonymousClass1ZE A04;
    public final Map A05 = new HashMap();
    private final C05930aZ A06;
    public volatile BlockingQueue A07;

    public static synchronized void A00(C14650tk r4) {
        Locale locale;
        synchronized (r4) {
            if (!(r4.A00 == 0 || (locale = r4.A02) == null)) {
                USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = r4.A01;
                Map map = r4.A03;
                if (!(uSLEBaseShape0S0000000 == null || map == null)) {
                    String A002 = AnonymousClass1KT.A00(r4.A06.A08(locale));
                    uSLEBaseShape0S0000000.A0F("impressions", map);
                    uSLEBaseShape0S0000000.A0D("string_locale", A002);
                    uSLEBaseShape0S0000000.A06();
                }
                r4.A01 = null;
                r4.A03 = null;
                r4.A05.clear();
                r4.A00 = 0;
            }
        }
    }

    public C14650tk(AnonymousClass1ZE r3, C05930aZ r4) {
        this.A04 = r3;
        this.A06 = r4;
    }
}
