package X;

import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.model.attribution.AttributionVisibility;
import com.facebook.messaging.model.attribution.ContentAppAttribution;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.0oG  reason: invalid class name and case insensitive filesystem */
public final class C11930oG {
    public final AnonymousClass0jD A00;

    public static final C11930oG A00(AnonymousClass1XY r2) {
        return new C11930oG(AnonymousClass0jD.A00(r2));
    }

    public static String A01(ContentAppAttribution contentAppAttribution) {
        if (contentAppAttribution == null) {
            return null;
        }
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        objectNode.put("attachment_fbid", contentAppAttribution.A08);
        objectNode.put("app_id", contentAppAttribution.A04);
        if (!C06850cB.A0B(contentAppAttribution.A06)) {
            objectNode.put("app_name", contentAppAttribution.A06);
        }
        if (!C06850cB.A0B(contentAppAttribution.A05)) {
            objectNode.put("app_key_hash", contentAppAttribution.A05);
        }
        if (!C06850cB.A0B(contentAppAttribution.A07)) {
            objectNode.put("app_package", contentAppAttribution.A07);
        }
        if (!C06850cB.A0B(contentAppAttribution.A0A)) {
            objectNode.put("metadata", contentAppAttribution.A0A);
        }
        objectNode.put("app_scoped_user_ids", JSONUtil.A0I(contentAppAttribution.A03));
        ObjectNode putObject = objectNode.putObject("visibility");
        putObject.put("hideAttribution", contentAppAttribution.A01.A01);
        putObject.put("hideInstallButton", contentAppAttribution.A01.A03);
        putObject.put("hideReplyButton", contentAppAttribution.A01.A04);
        putObject.put("hideAppIcon", contentAppAttribution.A01.A00);
        C21646AeL aeL = contentAppAttribution.A02;
        if (aeL != null) {
            objectNode.put("app_type", aeL.mValue);
        } else {
            objectNode.put("app_type", C21646AeL.UNRECOGNIZED.mValue);
        }
        if (!C06850cB.A0B(contentAppAttribution.A09)) {
            objectNode.put("icon_uri", contentAppAttribution.A09);
        }
        return objectNode.toString();
    }

    public C11930oG(AnonymousClass0jD r1) {
        this.A00 = r1;
    }

    public ContentAppAttribution A02(String str) {
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        C21646AeL aeL;
        String str7 = null;
        if (C06850cB.A0B(str)) {
            return null;
        }
        JsonNode A02 = this.A00.A02(str);
        if (A02.has("attachment_fbid")) {
            str2 = A02.get("attachment_fbid").textValue();
        } else {
            str2 = null;
        }
        String textValue = A02.get("app_id").textValue();
        if (A02.has("app_name")) {
            str3 = A02.get("app_name").textValue();
        } else {
            str3 = null;
        }
        if (A02.has("app_key_hash")) {
            str4 = A02.get("app_key_hash").textValue();
        } else {
            str4 = null;
        }
        if (A02.has("app_package")) {
            str5 = A02.get("app_package").textValue();
        } else {
            str5 = null;
        }
        if (A02.has("metadata")) {
            str6 = A02.get("metadata").textValue();
        } else {
            str6 = null;
        }
        AttributionVisibility attributionVisibility = AttributionVisibility.A05;
        if (A02.has("visibility")) {
            JsonNode jsonNode = A02.get("visibility");
            boolean asBoolean = jsonNode.get("hideAttribution").asBoolean();
            boolean asBoolean2 = jsonNode.get("hideInstallButton").asBoolean();
            boolean asBoolean3 = jsonNode.get("hideReplyButton").asBoolean();
            boolean A0U = JSONUtil.A0U(jsonNode.get("hideAppIcon"), false);
            BF1 bf1 = new BF1();
            bf1.A01 = asBoolean;
            bf1.A03 = asBoolean2;
            bf1.A04 = asBoolean3;
            bf1.A00 = A0U;
            attributionVisibility = new AttributionVisibility(bf1);
        }
        ImmutableMap.Builder builder = ImmutableMap.builder();
        if (A02.has("app_scoped_user_ids")) {
            Iterator fields = A02.get("app_scoped_user_ids").fields();
            while (fields.hasNext()) {
                Map.Entry entry = (Map.Entry) fields.next();
                builder.put(entry.getKey(), ((JsonNode) entry.getValue()).textValue());
            }
        }
        if (A02.has("app_type")) {
            aeL = C21646AeL.A00(A02.get("app_type").intValue());
        } else {
            aeL = C21646AeL.UNRECOGNIZED;
        }
        if (A02.has("icon_uri")) {
            str7 = A02.get("icon_uri").textValue();
        }
        C159737ae r1 = new C159737ae();
        r1.A08 = str2;
        r1.A04 = textValue;
        r1.A06 = str3;
        r1.A05 = str4;
        r1.A07 = str5;
        r1.A0A = str6;
        r1.A03 = ImmutableMap.copyOf(builder.build());
        r1.A01 = attributionVisibility;
        r1.A02 = aeL;
        r1.A09 = str7;
        return new ContentAppAttribution(r1);
    }
}
