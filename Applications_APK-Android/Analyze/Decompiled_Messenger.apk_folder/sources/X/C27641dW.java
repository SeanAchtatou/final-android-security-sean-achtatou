package X;

import java.util.ArrayDeque;
import java.util.Iterator;

/* renamed from: X.1dW  reason: invalid class name and case insensitive filesystem */
public final class C27641dW {
    public final ArrayDeque A00;
    private final Runnable A01;

    public void A00() {
        Iterator descendingIterator = this.A00.descendingIterator();
        while (descendingIterator.hasNext()) {
            C27751dh r1 = (C27751dh) descendingIterator.next();
            if (r1.A01) {
                r1.A00();
                return;
            }
        }
        Runnable runnable = this.A01;
        if (runnable != null) {
            runnable.run();
        }
    }

    public C27641dW() {
        this(null);
    }

    public C27641dW(Runnable runnable) {
        this.A00 = new ArrayDeque();
        this.A01 = runnable;
    }
}
