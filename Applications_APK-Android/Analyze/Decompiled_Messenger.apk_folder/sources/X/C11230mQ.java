package X;

import com.facebook.fbservice.results.DataFetchDisposition;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.model.threads.ThreadParticipant;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadsCollection;
import com.facebook.messaging.service.model.FetchThreadListResult;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;
import java.util.Collections;
import java.util.Iterator;

/* renamed from: X.0mQ  reason: invalid class name and case insensitive filesystem */
public final class C11230mQ {
    public AnonymousClass0UN A00;
    public final AnonymousClass0m6 A01;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static boolean A01(C11230mQ r3, ThreadSummary threadSummary, C09510hU r5, int i) {
        MessagesCollection A0P;
        if (threadSummary != null) {
            switch (r5.ordinal()) {
                case 0:
                case 2:
                    return r3.A01.A0k(threadSummary.A0S, i);
                case 3:
                    if (threadSummary == null) {
                        A0P = null;
                    } else {
                        A0P = r3.A01.A0P(threadSummary.A0S);
                    }
                    return A0P != null && A0P.A09(i);
            }
        }
    }

    public static FetchThreadResult A00(C11230mQ r6, ThreadSummary threadSummary) {
        int i;
        MessagesCollection A0P;
        DataFetchDisposition dataFetchDisposition;
        if (threadSummary == null) {
            return FetchThreadResult.A09;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = threadSummary.A06().iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            User A03 = ((C14300t0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxE, r6.A00)).A03(((ThreadParticipant) it.next()).A00());
            if (A03 != null) {
                builder.add((Object) A03);
            }
        }
        if (threadSummary == null) {
            A0P = null;
        } else {
            A0P = r6.A01.A0P(threadSummary.A0S);
        }
        if (A0P != null) {
            i = A0P.A04();
        }
        if (r6.A01.A0k(threadSummary.A0S, i)) {
            dataFetchDisposition = DataFetchDisposition.A0D;
        } else {
            dataFetchDisposition = DataFetchDisposition.A0C;
        }
        C61222yX A002 = FetchThreadResult.A00();
        A002.A01 = dataFetchDisposition;
        A002.A04 = threadSummary;
        A002.A02 = A0P;
        A002.A06 = builder.build();
        A002.A00 = -1;
        return A002.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        if (r3 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0030, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.messaging.model.messages.Message A02(com.facebook.messaging.model.threadkey.ThreadKey r6, java.lang.String r7) {
        /*
            r5 = this;
            if (r6 == 0) goto L_0x0031
            if (r7 == 0) goto L_0x0031
            X.0m6 r4 = r5.A01
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0028 }
            java.lang.String r0 = "getMessageByOfflineThreadingId_total"
            r1.A03(r0)     // Catch:{ all -> 0x0028 }
            X.0mO r0 = r4.A06     // Catch:{ all -> 0x0028 }
            com.facebook.messaging.model.messages.Message r2 = r0.A00(r6, r7)     // Catch:{ all -> 0x0028 }
            if (r2 == 0) goto L_0x0022
            X.0dK r1 = r4.A02     // Catch:{ all -> 0x0028 }
            java.lang.String r0 = "getMessageByOfflineThreadingId_hit"
            r1.A03(r0)     // Catch:{ all -> 0x0028 }
        L_0x0022:
            if (r3 == 0) goto L_0x0032
            r3.close()
            return r2
        L_0x0028:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
            if (r3 == 0) goto L_0x0030
            r3.close()     // Catch:{ all -> 0x0030 }
        L_0x0030:
            throw r0
        L_0x0031:
            r2 = 0
        L_0x0032:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11230mQ.A02(com.facebook.messaging.model.threadkey.ThreadKey, java.lang.String):com.facebook.messaging.model.messages.Message");
    }

    public FetchThreadListResult A04(C10950l8 r6) {
        DataFetchDisposition dataFetchDisposition;
        ThreadsCollection A0U = this.A01.A0U(r6);
        ImmutableList copyOf = ImmutableList.copyOf(Collections.unmodifiableCollection(((C14300t0) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AxE, this.A00)).A01.values()));
        if (this.A01.A0j(r6)) {
            dataFetchDisposition = DataFetchDisposition.A0D;
        } else {
            dataFetchDisposition = DataFetchDisposition.A0C;
        }
        FolderCounts A0N = this.A01.A0N(r6);
        AnonymousClass161 r1 = new AnonymousClass161();
        r1.A02 = dataFetchDisposition;
        r1.A04 = r6;
        r1.A06 = A0U;
        r1.A09 = copyOf;
        r1.A03 = A0N;
        return new FetchThreadListResult(r1);
    }

    public C11230mQ(AnonymousClass1XY r3, AnonymousClass0m6 r4) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = r4;
    }

    public ThreadSummary A03(ThreadCriteria threadCriteria) {
        ThreadKey A012 = threadCriteria.A01();
        if (A012 != null) {
            return this.A01.A0R(A012);
        }
        throw new IllegalArgumentException("No threadkey specified.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        if (r1.A01 != false) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if (r1.A02() >= r7) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if (r2 == false) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000f, code lost:
        if (r4.A01.A0i(r5) != false) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (r4.A01.A0j(r5) != false) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        if (r7 == 0) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        r1 = r4.A01.A0U(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A05(X.C10950l8 r5, X.C09510hU r6, int r7) {
        /*
            r4 = this;
            int r0 = r6.ordinal()
            r3 = 0
            switch(r0) {
                case 0: goto L_0x0012;
                case 1: goto L_0x0008;
                case 2: goto L_0x0012;
                case 3: goto L_0x0009;
                default: goto L_0x0008;
            }
        L_0x0008:
            return r3
        L_0x0009:
            X.0m6 r0 = r4.A01
            boolean r0 = r0.A0i(r5)
            if (r0 == 0) goto L_0x0008
            goto L_0x001a
        L_0x0012:
            X.0m6 r0 = r4.A01
            boolean r0 = r0.A0j(r5)
            if (r0 == 0) goto L_0x0008
        L_0x001a:
            r2 = 1
            if (r7 == 0) goto L_0x002e
            X.0m6 r0 = r4.A01
            com.facebook.messaging.model.threads.ThreadsCollection r1 = r0.A0U(r5)
            boolean r0 = r1.A01
            if (r0 != 0) goto L_0x002e
            int r0 = r1.A02()
            if (r0 >= r7) goto L_0x002e
            r2 = 0
        L_0x002e:
            if (r2 == 0) goto L_0x0008
            r3 = 1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11230mQ.A05(X.0l8, X.0hU, int):boolean");
    }
}
