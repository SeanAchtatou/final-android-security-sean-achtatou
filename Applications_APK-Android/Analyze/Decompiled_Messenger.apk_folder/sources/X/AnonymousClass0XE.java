package X;

import io.card.payment.BuildConfig;

/* renamed from: X.0XE  reason: invalid class name */
public final class AnonymousClass0XE implements Cloneable {
    public static final AnonymousClass0XE A05 = new AnonymousClass0XE().A02().A00();
    public static final AnonymousClass0XE A06 = new AnonymousClass0XE().A00();
    public static final AnonymousClass0XE A07 = new AnonymousClass0XE().A04().A00();
    public AnonymousClass0XF A00 = AnonymousClass0XF.A08;
    public boolean A01 = false;
    public boolean A02 = false;
    public boolean A03 = false;
    private boolean A04 = true;

    private AnonymousClass0XE A00() {
        if (!this.A04) {
            return this;
        }
        try {
            AnonymousClass0XE r1 = (AnonymousClass0XE) clone();
            r1.A04 = false;
            return r1;
        } catch (CloneNotSupportedException unused) {
            throw new RuntimeException(BuildConfig.FLAVOR);
        }
    }

    public static AnonymousClass0XE A01(AnonymousClass0XE r1) {
        if (r1.A04) {
            return r1;
        }
        try {
            AnonymousClass0XE r12 = (AnonymousClass0XE) r1.clone();
            r12.A04 = true;
            return r12;
        } catch (CloneNotSupportedException unused) {
            throw new RuntimeException(BuildConfig.FLAVOR);
        }
    }

    public AnonymousClass0XE A02() {
        AnonymousClass0XE A012 = A01(this);
        A012.A01 = true;
        return A012;
    }

    public AnonymousClass0XE A03() {
        AnonymousClass0XE A012 = A01(this);
        A012.A02 = true;
        return A012;
    }

    public AnonymousClass0XE A04() {
        AnonymousClass0XE A012 = A01(this);
        A012.A03 = true;
        return A012;
    }
}
