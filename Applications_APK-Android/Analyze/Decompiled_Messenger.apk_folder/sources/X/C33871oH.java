package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.messages.MessagesCollection;

/* renamed from: X.1oH  reason: invalid class name and case insensitive filesystem */
public final class C33871oH implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new MessagesCollection(parcel);
    }

    public Object[] newArray(int i) {
        return new MessagesCollection[i];
    }
}
