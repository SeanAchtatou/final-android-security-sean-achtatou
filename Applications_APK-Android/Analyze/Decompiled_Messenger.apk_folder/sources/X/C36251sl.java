package X;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1sl  reason: invalid class name and case insensitive filesystem */
public final class C36251sl implements C36221si, Serializable, Cloneable {
    private static final C36241sk A00 = new C36241sk("appId", (byte) 10, 16);
    private static final C36241sk A01 = new C36241sk("clientCapabilities", (byte) 10, 3);
    private static final C36241sk A02 = new C36241sk("clientIpAddress", (byte) 11, 13);
    private static final C36241sk A03 = new C36241sk("clientMqttSessionId", (byte) 10, 12);
    private static final C36241sk A04 = new C36241sk("clientStack", (byte) 3, 21);
    private static final C36241sk A05 = new C36241sk("clientType", (byte) 11, 15);
    private static final C36241sk A06 = new C36241sk("connectTokenHash", (byte) 11, 18);
    private static final C36241sk A07 = new C36241sk("deviceId", (byte) 11, 8);
    private static final C36241sk A08 = new C36241sk("deviceSecret", (byte) 11, 20);
    private static final C36241sk A09 = new C36241sk("endpointCapabilities", (byte) 10, 4);
    private static final C36241sk A0A = new C36241sk("fbnsConnectionKey", (byte) 10, 22);
    private static final C36241sk A0B = new C36241sk("fbnsConnectionSecret", (byte) 11, 23);
    private static final C36241sk A0C = new C36241sk("fbnsDeviceId", (byte) 11, 24);
    private static final C36241sk A0D = new C36241sk("fbnsDeviceSecret", (byte) 11, 25);
    private static final C36241sk A0E = new C36241sk("isInitiallyForeground", (byte) 2, 9);
    private static final C36241sk A0F = new C36241sk("luid", (byte) 10, 26);
    private static final C36241sk A0G = new C36241sk("makeUserAvailableInForeground", (byte) 2, 7);
    private static final C36241sk A0H = new C36241sk("networkSubtype", (byte) 8, 11);
    private static final C36241sk A0I = new C36241sk("networkType", (byte) 8, 10);
    private static final C36241sk A0J = new C36241sk("networkTypeInfo", (byte) 8, 27);
    private static final C36241sk A0K = new C36241sk("noAutomaticForeground", (byte) 2, 6);
    private static final C36241sk A0L = new C36241sk("overrideNectarLogging", (byte) 2, 17);
    private static final C36241sk A0M = new C36241sk("publishFormat", (byte) 8, 5);
    private static final C36241sk A0N = new C36241sk("regionPreference", (byte) 11, 19);
    private static final C36241sk A0O = new C36241sk("sslFingerprint", (byte) 11, 28);
    private static final C36241sk A0P = new C36241sk("subscribeTopics", (byte) 15, 14);
    private static final C36241sk A0Q = new C36241sk("tcpFingerprint", (byte) 11, 29);
    private static final C36241sk A0R = new C36241sk("userAgent", (byte) 11, 2);
    private static final C36241sk A0S = new C36241sk("userId", (byte) 10, 1);
    private static final C36231sj A0T = new C36231sj("ClientInfo");
    public final Long appId;
    public final Long clientCapabilities;
    public final String clientIpAddress;
    public final Long clientMqttSessionId;
    public final Byte clientStack;
    public final String clientType;
    public final byte[] connectTokenHash;
    public final String deviceId;
    public final String deviceSecret;
    public final Long endpointCapabilities;
    public final Long fbnsConnectionKey;
    public final String fbnsConnectionSecret;
    public final String fbnsDeviceId;
    public final String fbnsDeviceSecret;
    public final Boolean isInitiallyForeground;
    public final Long luid;
    public final Boolean makeUserAvailableInForeground;
    public final Integer networkSubtype;
    public final Integer networkType;
    public final C36191sf networkTypeInfo;
    public final Boolean noAutomaticForeground;
    public final Boolean overrideNectarLogging;
    public final C36261sm publishFormat;
    public final String regionPreference;
    public final String sslFingerprint;
    public final List subscribeTopics;
    public final String tcpFingerprint;
    public final String userAgent;
    public final Long userId;

    public boolean equals(Object obj) {
        C36251sl r5;
        if (obj == null || !(obj instanceof C36251sl) || (r5 = (C36251sl) obj) == null) {
            return false;
        }
        if (this == r5) {
            return true;
        }
        Long l = this.userId;
        boolean z = false;
        if (l != null) {
            z = true;
        }
        Long l2 = r5.userId;
        boolean z2 = false;
        if (l2 != null) {
            z2 = true;
        }
        if ((z || z2) && (!z || !z2 || !l.equals(l2))) {
            return false;
        }
        String str = this.userAgent;
        boolean z3 = false;
        if (str != null) {
            z3 = true;
        }
        String str2 = r5.userAgent;
        boolean z4 = false;
        if (str2 != null) {
            z4 = true;
        }
        if ((z3 || z4) && (!z3 || !z4 || !str.equals(str2))) {
            return false;
        }
        Long l3 = this.clientCapabilities;
        boolean z5 = false;
        if (l3 != null) {
            z5 = true;
        }
        Long l4 = r5.clientCapabilities;
        boolean z6 = false;
        if (l4 != null) {
            z6 = true;
        }
        if ((z5 || z6) && (!z5 || !z6 || !l3.equals(l4))) {
            return false;
        }
        Long l5 = this.endpointCapabilities;
        boolean z7 = false;
        if (l5 != null) {
            z7 = true;
        }
        Long l6 = r5.endpointCapabilities;
        boolean z8 = false;
        if (l6 != null) {
            z8 = true;
        }
        if ((z7 || z8) && (!z7 || !z8 || !l5.equals(l6))) {
            return false;
        }
        C36261sm r3 = this.publishFormat;
        boolean z9 = false;
        if (r3 != null) {
            z9 = true;
        }
        C36261sm r1 = r5.publishFormat;
        boolean z10 = false;
        if (r1 != null) {
            z10 = true;
        }
        if ((z9 || z10) && (!z9 || !z10 || !B36.A0C(r3, r1))) {
            return false;
        }
        Boolean bool = this.noAutomaticForeground;
        boolean z11 = false;
        if (bool != null) {
            z11 = true;
        }
        Boolean bool2 = r5.noAutomaticForeground;
        boolean z12 = false;
        if (bool2 != null) {
            z12 = true;
        }
        if ((z11 || z12) && (!z11 || !z12 || !bool.equals(bool2))) {
            return false;
        }
        Boolean bool3 = this.makeUserAvailableInForeground;
        boolean z13 = false;
        if (bool3 != null) {
            z13 = true;
        }
        Boolean bool4 = r5.makeUserAvailableInForeground;
        boolean z14 = false;
        if (bool4 != null) {
            z14 = true;
        }
        if ((z13 || z14) && (!z13 || !z14 || !bool3.equals(bool4))) {
            return false;
        }
        String str3 = this.deviceId;
        boolean z15 = false;
        if (str3 != null) {
            z15 = true;
        }
        String str4 = r5.deviceId;
        boolean z16 = false;
        if (str4 != null) {
            z16 = true;
        }
        if ((z15 || z16) && (!z15 || !z16 || !str3.equals(str4))) {
            return false;
        }
        Boolean bool5 = this.isInitiallyForeground;
        boolean z17 = false;
        if (bool5 != null) {
            z17 = true;
        }
        Boolean bool6 = r5.isInitiallyForeground;
        boolean z18 = false;
        if (bool6 != null) {
            z18 = true;
        }
        if ((z17 || z18) && (!z17 || !z18 || !bool5.equals(bool6))) {
            return false;
        }
        Integer num = this.networkType;
        boolean z19 = false;
        if (num != null) {
            z19 = true;
        }
        Integer num2 = r5.networkType;
        boolean z20 = false;
        if (num2 != null) {
            z20 = true;
        }
        if ((z19 || z20) && (!z19 || !z20 || !num.equals(num2))) {
            return false;
        }
        Integer num3 = this.networkSubtype;
        boolean z21 = false;
        if (num3 != null) {
            z21 = true;
        }
        Integer num4 = r5.networkSubtype;
        boolean z22 = false;
        if (num4 != null) {
            z22 = true;
        }
        if ((z21 || z22) && (!z21 || !z22 || !num3.equals(num4))) {
            return false;
        }
        Long l7 = this.clientMqttSessionId;
        boolean z23 = false;
        if (l7 != null) {
            z23 = true;
        }
        Long l8 = r5.clientMqttSessionId;
        boolean z24 = false;
        if (l8 != null) {
            z24 = true;
        }
        if ((z23 || z24) && (!z23 || !z24 || !l7.equals(l8))) {
            return false;
        }
        String str5 = this.clientIpAddress;
        boolean z25 = false;
        if (str5 != null) {
            z25 = true;
        }
        String str6 = r5.clientIpAddress;
        boolean z26 = false;
        if (str6 != null) {
            z26 = true;
        }
        if ((z25 || z26) && (!z25 || !z26 || !str5.equals(str6))) {
            return false;
        }
        List list = this.subscribeTopics;
        boolean z27 = false;
        if (list != null) {
            z27 = true;
        }
        List list2 = r5.subscribeTopics;
        boolean z28 = false;
        if (list2 != null) {
            z28 = true;
        }
        if ((z27 || z28) && (!z27 || !z28 || !B36.A0F(list, list2))) {
            return false;
        }
        String str7 = this.clientType;
        boolean z29 = false;
        if (str7 != null) {
            z29 = true;
        }
        String str8 = r5.clientType;
        boolean z30 = false;
        if (str8 != null) {
            z30 = true;
        }
        if ((z29 || z30) && (!z29 || !z30 || !str7.equals(str8))) {
            return false;
        }
        Long l9 = this.appId;
        boolean z31 = false;
        if (l9 != null) {
            z31 = true;
        }
        Long l10 = r5.appId;
        boolean z32 = false;
        if (l10 != null) {
            z32 = true;
        }
        if ((z31 || z32) && (!z31 || !z32 || !l9.equals(l10))) {
            return false;
        }
        Boolean bool7 = this.overrideNectarLogging;
        boolean z33 = false;
        if (bool7 != null) {
            z33 = true;
        }
        Boolean bool8 = r5.overrideNectarLogging;
        boolean z34 = false;
        if (bool8 != null) {
            z34 = true;
        }
        if ((z33 || z34) && (!z33 || !z34 || !bool7.equals(bool8))) {
            return false;
        }
        byte[] bArr = this.connectTokenHash;
        boolean z35 = false;
        if (bArr != null) {
            z35 = true;
        }
        byte[] bArr2 = r5.connectTokenHash;
        boolean z36 = false;
        if (bArr2 != null) {
            z36 = true;
        }
        if ((z35 || z36) && (!z35 || !z36 || !Arrays.equals(bArr, bArr2))) {
            return false;
        }
        String str9 = this.regionPreference;
        boolean z37 = false;
        if (str9 != null) {
            z37 = true;
        }
        String str10 = r5.regionPreference;
        boolean z38 = false;
        if (str10 != null) {
            z38 = true;
        }
        if ((z37 || z38) && (!z37 || !z38 || !str9.equals(str10))) {
            return false;
        }
        String str11 = this.deviceSecret;
        boolean z39 = false;
        if (str11 != null) {
            z39 = true;
        }
        String str12 = r5.deviceSecret;
        boolean z40 = false;
        if (str12 != null) {
            z40 = true;
        }
        if ((z39 || z40) && (!z39 || !z40 || !str11.equals(str12))) {
            return false;
        }
        Byte b = this.clientStack;
        boolean z41 = false;
        if (b != null) {
            z41 = true;
        }
        Byte b2 = r5.clientStack;
        boolean z42 = false;
        if (b2 != null) {
            z42 = true;
        }
        if ((z41 || z42) && (!z41 || !z42 || !b.equals(b2))) {
            return false;
        }
        Long l11 = this.fbnsConnectionKey;
        boolean z43 = false;
        if (l11 != null) {
            z43 = true;
        }
        Long l12 = r5.fbnsConnectionKey;
        boolean z44 = false;
        if (l12 != null) {
            z44 = true;
        }
        if ((z43 || z44) && (!z43 || !z44 || !l11.equals(l12))) {
            return false;
        }
        String str13 = this.fbnsConnectionSecret;
        boolean z45 = false;
        if (str13 != null) {
            z45 = true;
        }
        String str14 = r5.fbnsConnectionSecret;
        boolean z46 = false;
        if (str14 != null) {
            z46 = true;
        }
        if ((z45 || z46) && (!z45 || !z46 || !str13.equals(str14))) {
            return false;
        }
        String str15 = this.fbnsDeviceId;
        boolean z47 = false;
        if (str15 != null) {
            z47 = true;
        }
        String str16 = r5.fbnsDeviceId;
        boolean z48 = false;
        if (str16 != null) {
            z48 = true;
        }
        if ((z47 || z48) && (!z47 || !z48 || !str15.equals(str16))) {
            return false;
        }
        String str17 = this.fbnsDeviceSecret;
        boolean z49 = false;
        if (str17 != null) {
            z49 = true;
        }
        String str18 = r5.fbnsDeviceSecret;
        boolean z50 = false;
        if (str18 != null) {
            z50 = true;
        }
        if ((z49 || z50) && (!z49 || !z50 || !str17.equals(str18))) {
            return false;
        }
        Long l13 = this.luid;
        boolean z51 = false;
        if (l13 != null) {
            z51 = true;
        }
        Long l14 = r5.luid;
        boolean z52 = false;
        if (l14 != null) {
            z52 = true;
        }
        if ((z51 || z52) && (!z51 || !z52 || !l13.equals(l14))) {
            return false;
        }
        C36191sf r32 = this.networkTypeInfo;
        boolean z53 = false;
        if (r32 != null) {
            z53 = true;
        }
        C36191sf r12 = r5.networkTypeInfo;
        boolean z54 = false;
        if (r12 != null) {
            z54 = true;
        }
        if ((z53 || z54) && (!z53 || !z54 || !B36.A0C(r32, r12))) {
            return false;
        }
        String str19 = this.sslFingerprint;
        boolean z55 = false;
        if (str19 != null) {
            z55 = true;
        }
        String str20 = r5.sslFingerprint;
        boolean z56 = false;
        if (str20 != null) {
            z56 = true;
        }
        if ((z55 || z56) && (!z55 || !z56 || !str19.equals(str20))) {
            return false;
        }
        String str21 = this.tcpFingerprint;
        boolean z57 = false;
        if (str21 != null) {
            z57 = true;
        }
        String str22 = r5.tcpFingerprint;
        boolean z58 = false;
        if (str22 != null) {
            z58 = true;
        }
        if (!z57 && !z58) {
            return true;
        }
        if (!z57 || !z58 || !str21.equals(str22)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return CJ9(1, true);
    }

    public void CNX(C35781ro r5) {
        int value;
        int value2;
        r5.A0i(A0T);
        if (this.userId != null) {
            r5.A0e(A0S);
            r5.A0d(this.userId.longValue());
            r5.A0S();
        }
        String str = this.userAgent;
        if (str != null) {
            boolean z = false;
            if (str != null) {
                z = true;
            }
            if (z) {
                r5.A0e(A0R);
                r5.A0j(this.userAgent);
                r5.A0S();
            }
        }
        Long l = this.clientCapabilities;
        if (l != null) {
            boolean z2 = false;
            if (l != null) {
                z2 = true;
            }
            if (z2) {
                r5.A0e(A01);
                r5.A0d(this.clientCapabilities.longValue());
                r5.A0S();
            }
        }
        Long l2 = this.endpointCapabilities;
        if (l2 != null) {
            boolean z3 = false;
            if (l2 != null) {
                z3 = true;
            }
            if (z3) {
                r5.A0e(A09);
                r5.A0d(this.endpointCapabilities.longValue());
                r5.A0S();
            }
        }
        C36261sm r1 = this.publishFormat;
        int i = 0;
        if (r1 != null) {
            boolean z4 = false;
            if (r1 != null) {
                z4 = true;
            }
            if (z4) {
                r5.A0e(A0M);
                C36261sm r0 = this.publishFormat;
                if (r0 == null) {
                    value2 = 0;
                } else {
                    value2 = r0.getValue();
                }
                r5.A0c(value2);
                r5.A0S();
            }
        }
        Boolean bool = this.noAutomaticForeground;
        if (bool != null) {
            boolean z5 = false;
            if (bool != null) {
                z5 = true;
            }
            if (z5) {
                r5.A0e(A0K);
                r5.A0l(this.noAutomaticForeground.booleanValue());
                r5.A0S();
            }
        }
        Boolean bool2 = this.makeUserAvailableInForeground;
        if (bool2 != null) {
            boolean z6 = false;
            if (bool2 != null) {
                z6 = true;
            }
            if (z6) {
                r5.A0e(A0G);
                r5.A0l(this.makeUserAvailableInForeground.booleanValue());
                r5.A0S();
            }
        }
        String str2 = this.deviceId;
        if (str2 != null) {
            boolean z7 = false;
            if (str2 != null) {
                z7 = true;
            }
            if (z7) {
                r5.A0e(A07);
                r5.A0j(this.deviceId);
                r5.A0S();
            }
        }
        Boolean bool3 = this.isInitiallyForeground;
        if (bool3 != null) {
            boolean z8 = false;
            if (bool3 != null) {
                z8 = true;
            }
            if (z8) {
                r5.A0e(A0E);
                r5.A0l(this.isInitiallyForeground.booleanValue());
                r5.A0S();
            }
        }
        Integer num = this.networkType;
        if (num != null) {
            boolean z9 = false;
            if (num != null) {
                z9 = true;
            }
            if (z9) {
                r5.A0e(A0I);
                r5.A0c(this.networkType.intValue());
                r5.A0S();
            }
        }
        Integer num2 = this.networkSubtype;
        if (num2 != null) {
            boolean z10 = false;
            if (num2 != null) {
                z10 = true;
            }
            if (z10) {
                r5.A0e(A0H);
                r5.A0c(this.networkSubtype.intValue());
                r5.A0S();
            }
        }
        Long l3 = this.clientMqttSessionId;
        if (l3 != null) {
            boolean z11 = false;
            if (l3 != null) {
                z11 = true;
            }
            if (z11) {
                r5.A0e(A03);
                r5.A0d(this.clientMqttSessionId.longValue());
                r5.A0S();
            }
        }
        String str3 = this.clientIpAddress;
        if (str3 != null) {
            boolean z12 = false;
            if (str3 != null) {
                z12 = true;
            }
            if (z12) {
                r5.A0e(A02);
                r5.A0j(this.clientIpAddress);
                r5.A0S();
            }
        }
        List list = this.subscribeTopics;
        if (list != null) {
            boolean z13 = false;
            if (list != null) {
                z13 = true;
            }
            if (z13) {
                r5.A0e(A0P);
                r5.A0f(new C36381sy((byte) 8, this.subscribeTopics.size()));
                for (C56602qT r02 : this.subscribeTopics) {
                    if (r02 == null) {
                        value = 0;
                    } else {
                        value = r02.getValue();
                    }
                    r5.A0c(value);
                }
                r5.A0U();
                r5.A0S();
            }
        }
        String str4 = this.clientType;
        if (str4 != null) {
            boolean z14 = false;
            if (str4 != null) {
                z14 = true;
            }
            if (z14) {
                r5.A0e(A05);
                r5.A0j(this.clientType);
                r5.A0S();
            }
        }
        Long l4 = this.appId;
        if (l4 != null) {
            boolean z15 = false;
            if (l4 != null) {
                z15 = true;
            }
            if (z15) {
                r5.A0e(A00);
                r5.A0d(this.appId.longValue());
                r5.A0S();
            }
        }
        Boolean bool4 = this.overrideNectarLogging;
        if (bool4 != null) {
            boolean z16 = false;
            if (bool4 != null) {
                z16 = true;
            }
            if (z16) {
                r5.A0e(A0L);
                r5.A0l(this.overrideNectarLogging.booleanValue());
                r5.A0S();
            }
        }
        byte[] bArr = this.connectTokenHash;
        if (bArr != null) {
            boolean z17 = false;
            if (bArr != null) {
                z17 = true;
            }
            if (z17) {
                r5.A0e(A06);
                r5.A0m(this.connectTokenHash);
                r5.A0S();
            }
        }
        String str5 = this.regionPreference;
        if (str5 != null) {
            boolean z18 = false;
            if (str5 != null) {
                z18 = true;
            }
            if (z18) {
                r5.A0e(A0N);
                r5.A0j(this.regionPreference);
                r5.A0S();
            }
        }
        String str6 = this.deviceSecret;
        if (str6 != null) {
            boolean z19 = false;
            if (str6 != null) {
                z19 = true;
            }
            if (z19) {
                r5.A0e(A08);
                r5.A0j(this.deviceSecret);
                r5.A0S();
            }
        }
        Byte b = this.clientStack;
        if (b != null) {
            boolean z20 = false;
            if (b != null) {
                z20 = true;
            }
            if (z20) {
                r5.A0e(A04);
                r5.A0Z(this.clientStack.byteValue());
                r5.A0S();
            }
        }
        Long l5 = this.fbnsConnectionKey;
        if (l5 != null) {
            boolean z21 = false;
            if (l5 != null) {
                z21 = true;
            }
            if (z21) {
                r5.A0e(A0A);
                r5.A0d(this.fbnsConnectionKey.longValue());
                r5.A0S();
            }
        }
        String str7 = this.fbnsConnectionSecret;
        if (str7 != null) {
            boolean z22 = false;
            if (str7 != null) {
                z22 = true;
            }
            if (z22) {
                r5.A0e(A0B);
                r5.A0j(this.fbnsConnectionSecret);
                r5.A0S();
            }
        }
        String str8 = this.fbnsDeviceId;
        if (str8 != null) {
            boolean z23 = false;
            if (str8 != null) {
                z23 = true;
            }
            if (z23) {
                r5.A0e(A0C);
                r5.A0j(this.fbnsDeviceId);
                r5.A0S();
            }
        }
        String str9 = this.fbnsDeviceSecret;
        if (str9 != null) {
            boolean z24 = false;
            if (str9 != null) {
                z24 = true;
            }
            if (z24) {
                r5.A0e(A0D);
                r5.A0j(this.fbnsDeviceSecret);
                r5.A0S();
            }
        }
        Long l6 = this.luid;
        if (l6 != null) {
            boolean z25 = false;
            if (l6 != null) {
                z25 = true;
            }
            if (z25) {
                r5.A0e(A0F);
                r5.A0d(this.luid.longValue());
                r5.A0S();
            }
        }
        C36191sf r12 = this.networkTypeInfo;
        if (r12 != null) {
            boolean z26 = false;
            if (r12 != null) {
                z26 = true;
            }
            if (z26) {
                r5.A0e(A0J);
                C36191sf r03 = this.networkTypeInfo;
                if (r03 != null) {
                    i = r03.getValue();
                }
                r5.A0c(i);
                r5.A0S();
            }
        }
        String str10 = this.sslFingerprint;
        if (str10 != null) {
            boolean z27 = false;
            if (str10 != null) {
                z27 = true;
            }
            if (z27) {
                r5.A0e(A0O);
                r5.A0j(this.sslFingerprint);
                r5.A0S();
            }
        }
        String str11 = this.tcpFingerprint;
        if (str11 != null) {
            boolean z28 = false;
            if (str11 != null) {
                z28 = true;
            }
            if (z28) {
                r5.A0e(A0Q);
                r5.A0j(this.tcpFingerprint);
                r5.A0S();
            }
        }
        r5.A0T();
        r5.A0X();
    }

    public int hashCode() {
        Object[] objArr = new Object[29];
        Long l = this.userId;
        String str = this.userAgent;
        List list = this.subscribeTopics;
        String str2 = this.clientType;
        List list2 = list;
        String str3 = str2;
        Long l2 = l;
        String str4 = str;
        System.arraycopy(new Object[]{l2, str4, this.clientCapabilities, this.endpointCapabilities, this.publishFormat, this.noAutomaticForeground, this.makeUserAvailableInForeground, this.deviceId, this.isInitiallyForeground, this.networkType, this.networkSubtype, this.clientMqttSessionId, this.clientIpAddress, list2, str3, this.appId, this.overrideNectarLogging, this.connectTokenHash, this.regionPreference, this.deviceSecret, this.clientStack, this.fbnsConnectionKey, this.fbnsConnectionSecret, this.fbnsDeviceId, this.fbnsDeviceSecret, this.luid, this.networkTypeInfo}, 0, objArr, 0, 27);
        System.arraycopy(new Object[]{this.sslFingerprint, this.tcpFingerprint}, 0, objArr, 27, 2);
        return Arrays.deepHashCode(objArr);
    }

    public String CJ9(int i, boolean z) {
        return B36.A06(this, i, z);
    }

    public C36251sl(Long l, String str, Long l2, Long l3, C36261sm r6, Boolean bool, Boolean bool2, String str2, Boolean bool3, Integer num, Integer num2, Long l4, String str3, List list, String str4, Long l5, Boolean bool4, byte[] bArr, String str5, String str6, Byte b, Long l6, String str7, String str8, String str9, Long l7, C36191sf r28, String str10, String str11) {
        this.userId = l;
        this.userAgent = str;
        this.clientCapabilities = l2;
        this.endpointCapabilities = l3;
        this.publishFormat = r6;
        this.noAutomaticForeground = bool;
        this.makeUserAvailableInForeground = bool2;
        this.deviceId = str2;
        this.isInitiallyForeground = bool3;
        this.networkType = num;
        this.networkSubtype = num2;
        this.clientMqttSessionId = l4;
        this.clientIpAddress = str3;
        this.subscribeTopics = list;
        this.clientType = str4;
        this.appId = l5;
        this.overrideNectarLogging = bool4;
        this.connectTokenHash = bArr;
        this.regionPreference = str5;
        this.deviceSecret = str6;
        this.clientStack = b;
        this.fbnsConnectionKey = l6;
        this.fbnsConnectionSecret = str7;
        this.fbnsDeviceId = str8;
        this.fbnsDeviceSecret = str9;
        this.luid = l7;
        this.networkTypeInfo = r28;
        this.sslFingerprint = str10;
        this.tcpFingerprint = str11;
    }
}
