package X;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.StateSet;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0uQ  reason: invalid class name and case insensitive filesystem */
public final class C14960uQ extends StateListDrawable {
    private final List A00 = new ArrayList();
    private final List A01 = new ArrayList();

    public static C14960uQ A00(Drawable drawable, int i, float f) {
        int[][] iArr = {new int[]{16842910}, new int[]{-16842910}};
        int[] iArr2 = {i, C15970wH.A03(i, Math.round(((float) Color.alpha(i)) * f))};
        C14960uQ r2 = new C14960uQ();
        drawable.mutate();
        for (int i2 = 0; i2 < 2; i2++) {
            r2.A01(iArr[i2], Integer.valueOf(iArr2[i2]), drawable);
        }
        return r2;
    }

    public void addState(int[] iArr, Drawable drawable) {
        A01(iArr, null, drawable);
    }

    public void A01(int[] iArr, Integer num, Drawable drawable) {
        this.A01.add(iArr);
        this.A00.add(num);
        super.addState(iArr, drawable);
    }

    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        if (this.A01 != null) {
            clearColorFilter();
            int i = 0;
            while (true) {
                if (i >= this.A01.size()) {
                    break;
                } else if (!StateSet.stateSetMatches((int[]) this.A01.get(i), iArr)) {
                    i++;
                } else if (this.A00.get(i) != null) {
                    setColorFilter(((Integer) this.A00.get(i)).intValue(), PorterDuff.Mode.SRC_IN);
                }
            }
        }
        return onStateChange;
    }
}
