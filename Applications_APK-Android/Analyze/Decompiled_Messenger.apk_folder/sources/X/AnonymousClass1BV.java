package X;

/* renamed from: X.1BV  reason: invalid class name */
public abstract class AnonymousClass1BV implements C32821mO {
    public long Auk() {
        return !(this instanceof C22281Ks) ? 86400000 : 0;
    }

    public void C7z(long j) {
        if (this instanceof C22281Ks) {
            ((C22281Ks) this).A00.A00 = j;
        }
    }
}
