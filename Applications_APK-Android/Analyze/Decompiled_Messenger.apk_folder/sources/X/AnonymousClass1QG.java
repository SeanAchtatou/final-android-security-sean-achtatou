package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1QG  reason: invalid class name */
public final class AnonymousClass1QG extends C17770zR {
    @Comparable(type = 3)
    public int A00;
    @Comparable(type = 3)
    public long A01;
    @Comparable(type = 3)
    public long A02;
    @Comparable(type = 13)
    public C25998CqM A03;
    @Comparable(type = 13)
    public C204079jj A04;
    @Comparable(type = 13)
    public C203979jZ A05;
    @Comparable(type = 13)
    public C121605oW A06;
    @Comparable(type = 13)
    public String A07;
    @Comparable(type = 13)
    public String A08;

    public AnonymousClass1QG() {
        super("AccountRecPinRootComponent");
    }
}
