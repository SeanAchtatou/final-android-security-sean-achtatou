package X;

import android.view.MotionEvent;
import com.facebook.litho.LithoView;

/* renamed from: X.1HQ  reason: invalid class name */
public final class AnonymousClass1HQ extends LithoView {
    public C192917o A00;
    private int A01;
    private final C32781mK A02;

    public boolean post(Runnable runnable) {
        C192917o r0;
        if (this.A01 > 0) {
            C32781mK r2 = this.A02;
            if (r2.A00 == null) {
                r0 = null;
            } else {
                r0 = r2.A04;
            }
            if (r0 != null && runnable.getClass().getName().equals("android.view.View$PerformClick")) {
                return super.post(new EU6(this, r2, runnable));
            }
        }
        return super.post(runnable);
    }

    public AnonymousClass1HQ(AnonymousClass0p4 r1, C32781mK r2) {
        super(r1);
        this.A02 = r2;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int A05 = C000700l.A05(-1704025781);
        this.A01++;
        try {
            return super.onTouchEvent(motionEvent);
        } finally {
            this.A01--;
            C000700l.A0B(-31502172, A05);
        }
    }
}
