package X;

/* renamed from: X.1K8  reason: invalid class name */
public final class AnonymousClass1K8 {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r5 == X.C22111Jy.A05) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Path A00(android.content.Context r4, X.C22111Jy r5, X.AnonymousClass1J0 r6, float r7, boolean r8) {
        /*
            int r4 = X.C007106r.A00(r4, r7)
            X.1Jy r0 = X.C22111Jy.A04
            if (r5 == r0) goto L_0x0011
            X.1Jy r0 = X.C22111Jy.A03
            if (r5 == r0) goto L_0x0011
            X.1Jy r1 = X.C22111Jy.A05
            r0 = 1
            if (r5 != r1) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x0035
            int r0 = r6.A05
            float r3 = (float) r0
            r0 = 1056964608(0x3f000000, float:0.5)
            float r3 = r3 * r0
            float r3 = r3 / r7
            r0 = 1073741824(0x40000000, float:2.0)
            float r2 = r7 / r0
            int r0 = r6.A04
            int r0 = r0 >> 1
            float r1 = (float) r0
            if (r8 == 0) goto L_0x0032
            float r0 = r6.A01
        L_0x0028:
            float r1 = r1 + r0
            float r1 = r1 - r2
            float r1 = r1 / r7
            r0 = 1110704128(0x42340000, float:45.0)
            android.graphics.Path r0 = X.AnonymousClass1K7.A00(r3, r1, r0, r4)
            return r0
        L_0x0032:
            float r0 = r6.A00
            goto L_0x0028
        L_0x0035:
            android.graphics.Path r0 = X.AnonymousClass1K7.A01(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1K8.A00(android.content.Context, X.1Jy, X.1J0, float, boolean):android.graphics.Path");
    }
}
