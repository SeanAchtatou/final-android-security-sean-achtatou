package X;

import android.content.Context;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;

/* renamed from: X.1zY  reason: invalid class name and case insensitive filesystem */
public final class C39751zY {
    public C28468DvP A00 = new C28468DvP(this);
    public int A01;
    public LocationComponentOptions A02;
    public boolean A03;
    public boolean A04;
    private C28474DvV A05 = new C28474DvV(this);
    private C28391Du7 A06 = new C28391Du7(this);
    public final C28473DvU A07;
    public final C39541zD A08;
    public final C28501Dvy A09 = new C28454DvB(this);
    public final C28501Dvy A0A = new C28413DuV(this);
    public final C28501Dvy A0B = new C39881zl(this);
    public final C28501Dvy A0C = new C39761zZ(this);
    public final C28501Dvy A0D = new C39891zm(this);
    public final C28157Dph A0E;
    public final C28170Dpw A0F;
    private final C28423Dug A0G;
    private final C28423Dug A0H;
    private final C28429Dum A0I;

    public void A04(int i) {
        A05(i, null, 750, null, null, null, null);
    }

    public static void A00(C39751zY r3) {
        LocationComponentOptions locationComponentOptions = r3.A02;
        if (!locationComponentOptions.A0V) {
            return;
        }
        if (A03(r3)) {
            r3.A03 = true;
            r3.A0I.A00 = locationComponentOptions.A05;
            return;
        }
        r3.A0I.A00 = 0.0f;
    }

    public static void A01(C39751zY r10, float f) {
        if (!r10.A04) {
            r10.A0F.A04(r10.A0E, new C28191DqJ((double) f, null, -1.0d, -1.0d), null);
            r10.A07.A00.A0F.BQw();
        }
    }

    public static boolean A02(C39751zY r2) {
        int i = r2.A01;
        if (i == 16 || i == 32 || i == 22 || i == 34 || i == 36) {
            return true;
        }
        return false;
    }

    public static boolean A03(C39751zY r2) {
        int i = r2.A01;
        if (i == 24 || i == 32 || i == 34 || i == 36) {
            return true;
        }
        return false;
    }

    public void A06(LocationComponentOptions locationComponentOptions) {
        this.A02 = locationComponentOptions;
        boolean z = locationComponentOptions.A0V;
        C28157Dph dph = this.A0E;
        if (z) {
            C28155Dpd dpd = dph.A06.A00;
            C28419Duc duc = dpd.A04;
            C28423Dug dug = duc.A04;
            C28423Dug dug2 = this.A0H;
            if (dug != dug2) {
                Context context = dpd.getContext();
                C28419Duc.A05(duc, dug2, true);
                C28419Duc.A04(duc, context, true);
            }
            A00(this);
            return;
        }
        C28155Dpd dpd2 = dph.A06.A00;
        C28419Duc duc2 = dpd2.A04;
        C28423Dug dug3 = duc2.A04;
        C28423Dug dug4 = this.A0G;
        if (dug3 != dug4) {
            Context context2 = dpd2.getContext();
            C28419Duc.A05(duc2, dug4, true);
            C28419Duc.A04(duc2, context2, true);
        }
    }

    public C39751zY(Context context, C28157Dph dph, C28170Dpw dpw, C39541zD r6, LocationComponentOptions locationComponentOptions, C28473DvU dvU) {
        this.A0E = dph;
        this.A0F = dpw;
        this.A0G = dph.A06.A00.A04.A04;
        C28441Duy duy = new C28441Duy(this, context);
        this.A0H = duy;
        this.A0I = duy.A00;
        dph.A06.A00.A04.A0G.add(this.A05);
        dph.A06.A00.A04.A0C.add(this.A06);
        dph.A06.A00.A04.A0F.add(this.A00);
        this.A08 = r6;
        this.A07 = dvU;
        A06(locationComponentOptions);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00ac, code lost:
        if (r3 == 22) goto L_0x00ae;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(int r12, android.location.Location r13, long r14, java.lang.Double r16, java.lang.Double r17, java.lang.Double r18, X.C28418Dub r19) {
        /*
            r11 = this;
            boolean r2 = A03(r11)
            r11.A01 = r12
            r0 = 8
            if (r12 == r0) goto L_0x0011
            X.Dph r0 = r11.A0E
            X.Dpw r0 = r0.A09
            r0.A01()
        L_0x0011:
            A00(r11)
            X.1zD r1 = r11.A08
            int r0 = r11.A01
            r1.A01(r0)
            if (r2 == 0) goto L_0x0034
            boolean r0 = A03(r11)
            if (r0 != 0) goto L_0x0034
            X.Dph r0 = r11.A0E
            X.24f r0 = r0.A0A
            r1 = 0
            r0.A01 = r1
            X.Du3 r0 = r0.A0H
            r0.BZR(r1)
            X.1zD r0 = r11.A08
            r0.A00()
        L_0x0034:
            r10 = r17
            r3 = r18
            r8 = r19
            if (r2 != 0) goto L_0x00c1
            boolean r0 = A03(r11)
            if (r0 == 0) goto L_0x00c1
            if (r13 == 0) goto L_0x00c1
            r0 = 1
            r11.A04 = r0
            com.mapbox.mapboxsdk.geometry.LatLng r5 = new com.mapbox.mapboxsdk.geometry.LatLng
            r5.<init>(r13)
            X.1zC r4 = new X.1zC
            r4.<init>()
            r4.A03 = r5
            if (r16 == 0) goto L_0x005b
            double r0 = r16.doubleValue()
            r4.A02 = r0
        L_0x005b:
            if (r18 == 0) goto L_0x006f
            double r6 = r3.doubleValue()
            r2 = 0
            r0 = 4633641066610819072(0x404e000000000000, double:60.0)
            double r0 = java.lang.Math.min(r0, r6)
            double r0 = java.lang.Math.max(r2, r0)
            r4.A01 = r0
        L_0x006f:
            if (r17 == 0) goto L_0x009f
            double r0 = r10.doubleValue()
            r4.A01(r0)
        L_0x0078:
            com.mapbox.mapboxsdk.camera.CameraPosition r0 = r4.A00()
            X.DtU r4 = X.C28222Dqt.A00(r0)
            X.DvD r3 = new X.DvD
            r3.<init>(r11, r8)
            X.Dph r0 = r11.A0E
            com.mapbox.mapboxsdk.camera.CameraPosition r2 = r0.A01()
            X.Dph r0 = r11.A0E
            X.Dq0 r1 = r0.A08
            com.mapbox.mapboxsdk.geometry.LatLng r0 = r2.target
            boolean r0 = X.C28278Drs.A01(r1, r0, r5)
            if (r0 == 0) goto L_0x00c9
            X.Dpw r1 = r11.A0F
            X.Dph r0 = r11.A0E
            r1.A04(r0, r4, r3)
        L_0x009e:
            return
        L_0x009f:
            int r3 = r11.A01
            r0 = 34
            if (r3 == r0) goto L_0x00ae
            r0 = 36
            if (r3 == r0) goto L_0x00ae
            r2 = 22
            r0 = 0
            if (r3 != r2) goto L_0x00af
        L_0x00ae:
            r0 = 1
        L_0x00af:
            if (r0 == 0) goto L_0x0078
            r0 = 36
            if (r3 != r0) goto L_0x00bb
            r0 = 0
        L_0x00b7:
            r4.A01(r0)
            goto L_0x0078
        L_0x00bb:
            float r0 = r13.getBearing()
            double r0 = (double) r0
            goto L_0x00b7
        L_0x00c1:
            if (r19 == 0) goto L_0x009e
            int r0 = r11.A01
            r8.A01(r0)
            return
        L_0x00c9:
            X.Dpw r2 = r11.A0F
            X.Dph r1 = r11.A0E
            int r0 = (int) r14
            r2.A03(r1, r4, r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39751zY.A05(int, android.location.Location, long, java.lang.Double, java.lang.Double, java.lang.Double, X.Dub):void");
    }
}
