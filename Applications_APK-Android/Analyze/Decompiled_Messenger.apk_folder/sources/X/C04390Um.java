package X;

import android.os.Looper;

/* renamed from: X.0Um  reason: invalid class name and case insensitive filesystem */
public final class C04390Um implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.common.appstate.AppStateManager$3";
    public final /* synthetic */ AnonymousClass0Ud A00;

    public C04390Um(AnonymousClass0Ud r1) {
        this.A00 = r1;
    }

    public void run() {
        Looper.myQueue().addIdleHandler(this.A00.A08);
    }
}
