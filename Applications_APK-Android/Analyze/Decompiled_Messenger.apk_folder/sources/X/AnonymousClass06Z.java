package X;

import java.util.List;

/* renamed from: X.06Z  reason: invalid class name */
public final class AnonymousClass06Z {
    private static AnonymousClass06Z A02;
    public volatile C005606a A00;
    public volatile List A01;

    public static synchronized AnonymousClass06Z A00() {
        AnonymousClass06Z r0;
        synchronized (AnonymousClass06Z.class) {
            if (A02 == null) {
                A02 = new AnonymousClass06Z();
            }
            r0 = A02;
        }
        return r0;
    }

    public static void A01(AnonymousClass06Z r1, C005606a r2) {
        List<AnonymousClass06R> list = r1.A01;
        if (list != null) {
            for (AnonymousClass06R A0C : list) {
                A0C.A0C(r2);
            }
        }
    }

    private AnonymousClass06Z() {
    }
}
