package X;

import android.content.Context;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.mutators.DeleteThreadDialogFragment;
import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.user.model.User;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1Em  reason: invalid class name and case insensitive filesystem */
public final class C20961Em {
    public Context A00;
    public AnonymousClass0UN A01;
    public String A02;
    private C13060qW A03;

    public static void A00(C20961Em r3, ThreadSummary threadSummary) {
        if (((C185914h) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AGr, r3.A01)).A09(threadSummary.A0S)) {
            ((C185914h) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AGr, r3.A01)).A07(C138436dA.A04, new C98994oC(r3, threadSummary));
        } else {
            A01(r3, threadSummary);
        }
    }

    public static void A01(C20961Em r3, ThreadSummary threadSummary) {
        if (threadSummary != null && C16000wK.A01(r3.A03)) {
            User A05 = ((C42782Bv) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BFU, r3.A01)).A05(threadSummary);
            AnonymousClass2O9 r1 = new AnonymousClass2O9();
            r1.A01 = ImmutableList.of(threadSummary.A0S);
            r1.A00 = A05;
            DeleteThreadDialogFragment.A00(new AnonymousClass2OD(r1)).A25(r3.A03, "deleteThreadDialog");
        }
    }

    public void A02(ThreadSummary threadSummary) {
        ThreadKey threadKey = threadSummary.A0S;
        if (ThreadKey.A0C(threadKey)) {
            ((AnonymousClass2DP) AnonymousClass1XX.A02(3, AnonymousClass1Y3.ALD, this.A01)).A01(ImmutableList.of(threadKey), new C98984oB(this));
        } else if (!ThreadKey.A0E(threadKey)) {
            A01(this, threadSummary);
        } else if (threadSummary != null && ThreadKey.A0E(threadKey)) {
            if (!((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A01)).Aep(C05690aA.A0J, false)) {
                C21451Ha.A07((C21451Ha) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AFw, this.A01), this.A02, "show");
                C13500rX r3 = new C13500rX(this.A00);
                r3.A09(2131823856);
                r3.A08(2131823853);
                r3.A02(2131823854, new C99004oD(this, threadSummary));
                r3.A00(2131823855, new C99014oE(this));
                r3.A06().show();
                C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(0, AnonymousClass1Y3.B6q, this.A01)).edit();
                edit.putBoolean(C05690aA.A0J, true);
                edit.commit();
                return;
            }
            A00(this, threadSummary);
        }
    }

    public C20961Em(AnonymousClass1XY r3, CallerContext callerContext, Context context, C13060qW r6) {
        this.A01 = new AnonymousClass0UN(7, r3);
        this.A00 = context;
        this.A02 = callerContext.A0F();
        this.A03 = r6;
    }
}
