package X;

import com.facebook.prefs.shared.FbSharedPreferences;

/* renamed from: X.0yB  reason: invalid class name and case insensitive filesystem */
public final class C17010yB implements AnonymousClass0ZM {
    public final /* synthetic */ C16680xb A00;

    public C17010yB(C16680xb r1) {
        this.A00 = r1;
    }

    public void onSharedPreferenceChanged(FbSharedPreferences fbSharedPreferences, AnonymousClass1Y7 r3) {
        C16680xb.A01(this.A00);
    }
}
