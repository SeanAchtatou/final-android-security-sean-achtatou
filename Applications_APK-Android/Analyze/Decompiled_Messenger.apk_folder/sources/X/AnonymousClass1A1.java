package X;

import android.database.Observable;

/* renamed from: X.1A1  reason: invalid class name */
public final class AnonymousClass1A1 extends Observable {
    public void A00() {
        for (int size = this.mObservers.size() - 1; size >= 0; size--) {
            ((C15730vo) this.mObservers.get(size)).A04();
        }
    }

    public void A01(int i, int i2) {
        for (int size = this.mObservers.size() - 1; size >= 0; size--) {
            ((C15730vo) this.mObservers.get(size)).A02(i, i2, 1);
        }
    }

    public void A02(int i, int i2) {
        for (int size = this.mObservers.size() - 1; size >= 0; size--) {
            ((C15730vo) this.mObservers.get(size)).A05(i, i2);
        }
    }

    public void A03(int i, int i2) {
        for (int size = this.mObservers.size() - 1; size >= 0; size--) {
            ((C15730vo) this.mObservers.get(size)).A06(i, i2);
        }
    }

    public void A04(int i, int i2, Object obj) {
        for (int size = this.mObservers.size() - 1; size >= 0; size--) {
            ((C15730vo) this.mObservers.get(size)).A03(i, i2, obj);
        }
    }

    public boolean A05() {
        return !this.mObservers.isEmpty();
    }
}
