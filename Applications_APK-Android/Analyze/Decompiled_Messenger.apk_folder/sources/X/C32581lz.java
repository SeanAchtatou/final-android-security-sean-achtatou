package X;

/* renamed from: X.1lz  reason: invalid class name and case insensitive filesystem */
public final class C32581lz {
    public AnonymousClass0UN A00;
    private final C04310Tq A01;

    public static final C32581lz A00(AnonymousClass1XY r1) {
        return new C32581lz(r1);
    }

    public boolean A01() {
        if (((Boolean) this.A01.get()).booleanValue() || ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(282243775857926L)) {
            return true;
        }
        return false;
    }

    public boolean A02() {
        return ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(285031209637218L);
    }

    private C32581lz(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.AUm, r3);
    }
}
