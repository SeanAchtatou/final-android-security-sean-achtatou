package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0Or  reason: invalid class name and case insensitive filesystem */
public final class C03600Or extends C03160Kg {
    public long A00() {
        return 1224;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        C02660Fu r32 = (C02660Fu) r3;
        dataOutput.writeDouble(r32.total.powerMah);
        dataOutput.writeLong(r32.total.activeTimeMs);
        dataOutput.writeLong(r32.total.wakeUpTimeMs);
    }

    public boolean A03(AnonymousClass0FM r4, DataInput dataInput) {
        C02660Fu r42 = (C02660Fu) r4;
        r42.total.powerMah = dataInput.readDouble();
        r42.total.activeTimeMs = dataInput.readLong();
        r42.total.wakeUpTimeMs = dataInput.readLong();
        return true;
    }
}
