package X;

import com.facebook.common.time.AwakeTimeSinceBootClock;
import com.facebook.common.time.RealtimeSinceBootClock;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.067  reason: invalid class name */
public final class AnonymousClass067 extends AnonymousClass0UV {
    private static volatile AwakeTimeSinceBootClock A00;
    private static volatile AnonymousClass09O A01;
    private static volatile RealtimeSinceBootClock A02;
    private static volatile AnonymousClass06A A03;

    public static final AwakeTimeSinceBootClock A01(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (AwakeTimeSinceBootClock.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = AwakeTimeSinceBootClock.INSTANCE;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }

    public static final AnonymousClass09O A07(AnonymousClass1XY r3) {
        if (A01 == null) {
            synchronized (AnonymousClass09O.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A01 = AwakeTimeSinceBootClock.INSTANCE;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final RealtimeSinceBootClock A09(AnonymousClass1XY r3) {
        if (A02 == null) {
            synchronized (RealtimeSinceBootClock.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A02 = RealtimeSinceBootClock.A00;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    public static final AnonymousClass06A A0B(AnonymousClass1XY r3) {
        if (A03 == null) {
            synchronized (AnonymousClass06A.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A03 = AnonymousClass06A.A00;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public static final AnonymousClass0US A0C(AnonymousClass1XY r1) {
        return AnonymousClass0UQ.A00(AnonymousClass1Y3.AgK, r1);
    }

    public static final C04310Tq A0D(AnonymousClass1XY r1) {
        return AnonymousClass0VB.A00(AnonymousClass1Y3.AYB, r1);
    }

    public static final AwakeTimeSinceBootClock A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static final AnonymousClass06B A02() {
        return AnonymousClass06A.A00;
    }

    public static final AnonymousClass069 A03(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static final AnonymousClass069 A04(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static final AnonymousClass069 A05(AnonymousClass1XY r0) {
        return A09(r0);
    }

    public static final AnonymousClass09O A06(AnonymousClass1XY r0) {
        return A07(r0);
    }

    public static final RealtimeSinceBootClock A08(AnonymousClass1XY r0) {
        return A09(r0);
    }

    public static final AnonymousClass06A A0A(AnonymousClass1XY r0) {
        return A0B(r0);
    }
}
