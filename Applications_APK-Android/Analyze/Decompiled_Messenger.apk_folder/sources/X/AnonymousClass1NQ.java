package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1NQ  reason: invalid class name */
public final class AnonymousClass1NQ {
    private static volatile AnonymousClass1NQ A01;
    public final int A00;

    public static final AnonymousClass1NQ A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (AnonymousClass1NQ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1NQ(AnonymousClass0VG.A00(AnonymousClass1Y3.AOJ, r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r3.A0A() != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(com.facebook.messaging.model.threads.ThreadSummary r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.A0p
            boolean r0 = X.C06850cB.A0B(r0)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x001f
            com.google.common.collect.ImmutableList r0 = r3.A0m
            int r1 = r0.size()
            int r0 = r2.A00
            if (r1 > r0) goto L_0x001b
            boolean r0 = r3.A0A()
            r1 = 1
            if (r0 == 0) goto L_0x001c
        L_0x001b:
            r1 = 0
        L_0x001c:
            r0 = 1
            if (r1 != 0) goto L_0x0020
        L_0x001f:
            r0 = 0
        L_0x0020:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1NQ.A01(com.facebook.messaging.model.threads.ThreadSummary):boolean");
    }

    private AnonymousClass1NQ(C04310Tq r5) {
        this.A00 = ((C25051Yd) r5.get()).AqL(564981473477795L, 75);
    }
}
