package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.acra.LogCatCollector;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import java.io.UnsupportedEncodingException;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tf  reason: invalid class name and case insensitive filesystem */
public final class C36751tf extends C36471tB {
    public static final Class A02 = C36751tf.class;
    private static volatile C36751tf A03;
    private AnonymousClass0UN A00;
    private final C04310Tq A01;

    private C36751tf(AnonymousClass1XY r3, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        super(AnonymousClass1Y3.A1d, deprecatedAnalyticsLogger);
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A01 = C10580kT.A04(r3);
    }

    public static final C36751tf A00(AnonymousClass1XY r5) {
        if (A03 == null) {
            synchronized (C36751tf.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A03 = new C36751tf(applicationInjector, C06920cI.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    public void A0D(boolean z) {
        AwE awE;
        Class cls;
        String str;
        if (!A0C()) {
            cls = A02;
            str = "Stored procedure sender not available";
        } else if (((AnonymousClass18N) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BRm, this.A00)).A03()) {
            cls = A02;
            str = "Invalid device id";
        } else {
            C36511tF r3 = (C36511tF) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AZJ, this.A00);
            long parseLong = Long.parseLong((String) this.A01.get());
            C06140av A032 = C06160ax.A03(C197619o.A00.A00, ((AnonymousClass18N) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BRm, this.A00)).A02());
            SQLiteDatabase A012 = ((AnonymousClass183) r3.A03.get()).A01();
            if (A012 == null) {
                awE = null;
            } else {
                Cursor query = A012.query("other_devices", C197019i.A01, A032.A02(), A032.A04(), null, null, null);
                try {
                    if (query.moveToNext()) {
                        C22338Aw9 aw9 = new C22338Aw9(Long.valueOf(parseLong), C197619o.A00.A05(query));
                        byte[] A022 = C36511tF.A02(r3, query);
                        if (A022 != null) {
                            awE = new AwE(aw9, new String(A022, LogCatCollector.UTF_8_ENCODING));
                            query.close();
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    C010708t.A0S("OtherDevicesDAO", e, "Unable to get this device's PersonalDeviceInfo");
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
                query.close();
                awE = null;
            }
            C22348AwT awT = new C22348AwT(awE, null, Boolean.valueOf(z));
            C22036AnC anC = C22036AnC.A09;
            C22335Aw3 aw3 = new C22335Aw3();
            aw3.setField_ = 25;
            aw3.value_ = awT;
            A0B(C22030An2.A01(C22039AnF.A01(null, new C22338Aw9(Long.valueOf(Long.parseLong((String) this.A01.get())), ((AnonymousClass18N) AnonymousClass1XX.A02(0, AnonymousClass1Y3.BRm, this.A00)).A02()), ((AnonymousClass06A) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AYB, this.A00)).now() * 1000, anC, aw3, C36471tB.A05(), null)));
            return;
        }
        C010708t.A05(cls, str);
    }
}
