package X;

import com.facebook.profilo.ipc.TraceConfigData;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.multiprocess.ProfiloMultiProcessTraceServiceImpl;

/* renamed from: X.09e  reason: invalid class name and case insensitive filesystem */
public final class C011709e extends C011609d {
    private final C011809f A00;
    private final ProfiloMultiProcessTraceServiceImpl A01;

    public void BTJ() {
        ProfiloMultiProcessTraceServiceImpl profiloMultiProcessTraceServiceImpl = this.A01;
        int A03 = C000700l.A03(1109530352);
        TraceConfigData A002 = ProfiloMultiProcessTraceServiceImpl.A00();
        if (A002 == null) {
            C000700l.A09(-198181955, A03);
            return;
        }
        ProfiloMultiProcessTraceServiceImpl.A01(profiloMultiProcessTraceServiceImpl, 4, null, A002);
        C000700l.A09(-538982223, A03);
    }

    public void onTraceAbort(TraceContext traceContext) {
        if ((traceContext.A02 & AnonymousClass00n.A07) != 0) {
            this.A01.onTraceAbort(traceContext);
        }
    }

    public void onTraceStart(TraceContext traceContext) {
        if ((traceContext.A02 & AnonymousClass00n.A07) != 0) {
            this.A01.Bsd(traceContext);
            this.A01.Bsc(traceContext);
        }
    }

    public void onTraceStop(TraceContext traceContext) {
        if ((traceContext.A02 & AnonymousClass00n.A07) != 0) {
            this.A01.onTraceStop(traceContext);
        }
    }

    public C011709e() {
        ProfiloMultiProcessTraceServiceImpl profiloMultiProcessTraceServiceImpl = new ProfiloMultiProcessTraceServiceImpl();
        this.A01 = profiloMultiProcessTraceServiceImpl;
        this.A00 = new C011809f(profiloMultiProcessTraceServiceImpl);
    }

    public AnonymousClass07D A00() {
        return this.A00;
    }
}
