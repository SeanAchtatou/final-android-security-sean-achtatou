package X;

import android.content.Context;
import android.view.View;
import com.facebook.litho.ComponentHost;

/* renamed from: X.0zY  reason: invalid class name and case insensitive filesystem */
public final class C17830zY {
    public int A00 = 0;
    public int A01;
    public int A02;
    public int A03;
    public C17770zR A04;
    public ComponentHost A05;
    public C31401jd A06;
    public C17750zP A07;
    public C17650zF A08;
    public boolean A09;
    private String A0A;
    private boolean A0B;
    private final Object A0C;

    public Object A00() {
        if (!this.A0B) {
            return this.A0C;
        }
        throw new RuntimeException("Trying to access released mount content!");
    }

    public void A01(Context context, String str) {
        String str2;
        if (this.A0B) {
            C17770zR r0 = this.A04;
            String str3 = "<null>";
            if (r0 != null) {
                str2 = r0.A1A();
            } else {
                str2 = str3;
            }
            if (r0 != null) {
                str3 = r0.A06;
            }
            throw new C37781wF("Releasing released mount content! component: " + str2 + ", globalKey: " + str3 + ", transitionId: " + this.A07 + ", previousReleaseCause: " + this.A0A);
        }
        C17770zR r02 = this.A04;
        Object obj = this.A0C;
        C15690vh A002 = C15040ud.A00(context, r02);
        if (A002 != null) {
            A002.C0t(obj);
        }
        this.A0B = true;
        this.A0A = str;
    }

    public boolean A02() {
        C17770zR r3 = this.A04;
        if (r3 == null || this.A00 == 2) {
            return false;
        }
        C31401jd r0 = this.A06;
        if ((r0 == null || !r0.A02()) && !r3.A0x()) {
            return false;
        }
        return true;
    }

    public C17830zY(C17770zR r3, ComponentHost componentHost, Object obj, C31401jd r6, C17650zF r7, int i, int i2, int i3, C17750zP r11) {
        if (r3 != null) {
            this.A04 = r3;
            this.A0C = obj;
            this.A05 = componentHost;
            this.A01 = i;
            this.A00 = i2;
            this.A03 = i3;
            this.A07 = r11;
            if (r6 != null) {
                this.A06 = r6;
            }
            if (r7 != null) {
                this.A08 = r7;
            }
            if (obj instanceof View) {
                View view = (View) obj;
                if (view.isClickable()) {
                    this.A02 |= 1;
                }
                if (view.isLongClickable()) {
                    this.A02 |= 2;
                }
                if (view.isFocusable()) {
                    this.A02 |= 4;
                }
                if (view.isEnabled()) {
                    this.A02 |= 8;
                }
                if (view.isSelected()) {
                    this.A02 |= 16;
                    return;
                }
                return;
            }
            return;
        }
        throw new RuntimeException("Calling init() on a MountItem with a null Component!");
    }
}
