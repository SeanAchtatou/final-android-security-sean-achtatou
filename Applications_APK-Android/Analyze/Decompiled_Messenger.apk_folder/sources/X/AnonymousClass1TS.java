package X;

import com.facebook.messaging.model.threads.ThreadSummary;
import java.util.Comparator;

/* renamed from: X.1TS  reason: invalid class name */
public final class AnonymousClass1TS implements Comparator {
    public int compare(Object obj, Object obj2) {
        return (((ThreadSummary) obj2).A0B > ((ThreadSummary) obj).A0B ? 1 : (((ThreadSummary) obj2).A0B == ((ThreadSummary) obj).A0B ? 0 : -1));
    }
}
