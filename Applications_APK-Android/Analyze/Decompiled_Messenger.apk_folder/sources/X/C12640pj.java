package X;

import com.facebook.common.util.JSONUtil;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.business.common.calltoaction.model.NestedCallToAction;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0pj  reason: invalid class name and case insensitive filesystem */
public final class C12640pj {
    public static final C12640pj A00() {
        return new C12640pj();
    }

    public String A02(List list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            NestedCallToAction nestedCallToAction = (NestedCallToAction) it.next();
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("call_to_action", C29941hE.A01(nestedCallToAction.A00));
            objectNode.put("nested_call_to_action_list", A02(nestedCallToAction.A01));
            arrayNode.add(objectNode);
        }
        return arrayNode.toString();
    }

    public ImmutableList A01(String str) {
        NestedCallToAction nestedCallToAction;
        if (C06850cB.A0B(str)) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        try {
            Iterator it = AnonymousClass0jE.getInstance().readTree(str).iterator();
            while (it.hasNext()) {
                JsonNode jsonNode = (JsonNode) it.next();
                CallToAction A00 = C29941hE.A00(jsonNode.get("call_to_action"));
                ImmutableList A01 = A01(JSONUtil.A0N(jsonNode.get("nested_call_to_action_list")));
                if (A00 == null) {
                    nestedCallToAction = null;
                } else {
                    C30131hX r1 = new C30131hX();
                    r1.A00 = A00;
                    r1.A01 = A01;
                    nestedCallToAction = new NestedCallToAction(r1);
                }
                if (nestedCallToAction != null) {
                    builder.add((Object) nestedCallToAction);
                }
            }
            return builder.build();
        } catch (Exception unused) {
            return null;
        }
    }
}
