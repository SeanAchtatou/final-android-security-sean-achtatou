package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.0Xq  reason: invalid class name and case insensitive filesystem */
public class C05100Xq {
    public C06450bW A00;

    public void A06() {
        this.A00.A02();
    }

    public void A07(String str, C73033fO r3) {
        this.A00.A04(str, r3);
    }

    public void A08(String str, Boolean bool) {
        this.A00.A05(str, bool);
    }

    public void A09(String str, Double d) {
        this.A00.A06(str, d);
    }

    public void A0A(String str, Float f) {
        this.A00.A07(str, f);
    }

    public void A0B(String str, Integer num) {
        this.A00.A08(str, num);
    }

    public void A0C(String str, Long l) {
        this.A00.A09(str, l);
    }

    public void A0D(String str, String str2) {
        this.A00.A0B(str, str2);
    }

    public void A0E(String str, List list) {
        this.A00.A0C(str, list);
    }

    public void A0F(String str, Map map) {
        this.A00.A0D(str, map);
    }

    public boolean A0G() {
        return this.A00.A0F();
    }

    public C05100Xq(C06450bW r1) {
        this.A00 = r1;
    }
}
