package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.18Z  reason: invalid class name */
public final class AnonymousClass18Z implements AnonymousClass06U {
    public final /* synthetic */ C33741o4 A00;

    public AnonymousClass18Z(C33741o4 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r5) {
        int A002 = AnonymousClass09Y.A00(-1073268141);
        C33741o4.A01(this.A00, intent);
        AnonymousClass09Y.A01(-779838692, A002);
    }
}
