package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.21n  reason: invalid class name and case insensitive filesystem */
public final class C404221n extends AnonymousClass0UV {
    private static volatile C404521q A00;

    public static final C404521q A00(AnonymousClass1XY r3) {
        if (A00 == null) {
            synchronized (C404521q.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r3);
                if (A002 != null) {
                    try {
                        r3.getApplicationInjector();
                        A00 = new C404521q();
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
