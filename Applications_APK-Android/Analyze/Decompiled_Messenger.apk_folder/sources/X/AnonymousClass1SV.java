package X;

import java.io.InputStream;

/* renamed from: X.1SV  reason: invalid class name */
public final class AnonymousClass1SV extends InputStream {
    public int A00 = 0;
    public int A01 = 0;
    public final AnonymousClass1SS A02;

    public boolean markSupported() {
        return true;
    }

    public int available() {
        return this.A02.size() - this.A01;
    }

    public void mark(int i) {
        this.A00 = this.A01;
    }

    public void reset() {
        this.A01 = this.A00;
    }

    public long skip(long j) {
        boolean z = false;
        if (j >= 0) {
            z = true;
        }
        C05520Zg.A04(z);
        int min = Math.min((int) j, available());
        this.A01 += min;
        return (long) min;
    }

    public AnonymousClass1SV(AnonymousClass1SS r2) {
        C05520Zg.A04(!r2.isClosed());
        C05520Zg.A02(r2);
        this.A02 = r2;
    }

    public int read() {
        if (available() <= 0) {
            return -1;
        }
        AnonymousClass1SS r2 = this.A02;
        int i = this.A01;
        this.A01 = i + 1;
        return r2.read(i) & 255;
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            throw new ArrayIndexOutOfBoundsException(AnonymousClass08S.A0D("length=", bArr.length, "; regionStart=", i3, "; regionLength=", i4));
        }
        int available = available();
        if (available <= 0) {
            return -1;
        }
        if (i2 <= 0) {
            return 0;
        }
        int min = Math.min(available, i2);
        this.A02.read(this.A01, bArr, i, min);
        this.A01 += min;
        return min;
    }
}
