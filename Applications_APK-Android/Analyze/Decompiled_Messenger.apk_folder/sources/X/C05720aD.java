package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0aD  reason: invalid class name and case insensitive filesystem */
public final class C05720aD {
    private static volatile C05720aD A07;
    private AnonymousClass0UN A00;
    public final FbSharedPreferences A01;
    public final Map A02 = AnonymousClass0TG.A04();
    private final AnonymousClass1JA A03 = new AnonymousClass1JA(this);
    private final C28191eP A04;
    private final C04310Tq A05;
    private volatile boolean A06;

    public synchronized void A05(String str, boolean z) {
        AnonymousClass1Y7 A002 = C05730aE.A00(str);
        if (!this.A02.containsKey(A002) || z != ((Boolean) this.A02.get(A002)).booleanValue()) {
            AnonymousClass1JA r4 = this.A03;
            synchronized (r4) {
                try {
                    C30281hn edit = r4.A02.A01.edit();
                    edit.C1B(C05730aE.A0H.A09("count/").A09(str));
                    edit.commit();
                } catch (Throwable th) {
                    th = th;
                    throw th;
                }
            }
            C30281hn edit2 = this.A01.edit();
            edit2.putBoolean(A002, z);
            edit2.commit();
            Boolean valueOf = Boolean.valueOf(z);
            synchronized (this) {
                try {
                    A02(this);
                    this.A02.put(A002, valueOf);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public static final C05720aD A01(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C05720aD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C05720aD(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public static void A02(C05720aD r5) {
        if (!r5.A06) {
            synchronized (r5) {
                if (!r5.A06) {
                    for (String A002 : C28361eg.A00) {
                        AnonymousClass1Y7 A003 = C05730aE.A00(A002);
                        r5.A02.put(A003, Boolean.valueOf(r5.A01.Aep(A003, true)));
                    }
                    r5.A06 = true;
                }
            }
        }
    }

    private boolean A03() {
        if (this.A01.At2(C05730aE.A08, 0) > ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A00)).now()) {
            return false;
        }
        if (this.A01.AqN(C05730aE.A09, 0) < ((int) ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At0(566385927456412L))) {
            return true;
        }
        int At0 = (int) ((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).At0(566385927390875L);
        C30281hn edit = this.A01.edit();
        edit.Bz6(C05730aE.A09, 0);
        edit.BzA(C05730aE.A08, ((AnonymousClass06B) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AgK, this.A00)).now() + (((long) At0) * 3600000));
        edit.commit();
        return false;
    }

    public boolean A06() {
        if (A07("autoflex_enable_upsell") || !((Boolean) this.A05.get()).booleanValue()) {
            return false;
        }
        return A07("zero_balance_auto_switch");
    }

    public boolean A07(String str) {
        boolean z = false;
        if (this.A01.BFQ()) {
            A02(this);
            if (this.A04.A0J().contains(str)) {
                z = true;
                if (str.equals(AnonymousClass80H.$const$string(68))) {
                    return !A08("zero_state");
                }
            }
        }
        return z;
    }

    private C05720aD(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A01 = FbSharedPreferencesModule.A00(r3);
        this.A04 = C28191eP.A00(r3);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.Amx, r3);
    }

    public static final C05720aD A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public void A04(String str) {
        if (A08(str)) {
            AnonymousClass1JA r3 = this.A03;
            synchronized (r3) {
                if (r3.A01.containsKey(str)) {
                    AnonymousClass1Y8 A0D = C05730aE.A0H.A09("count/").A09(str);
                    int AqN = r3.A02.A01.AqN(A0D, 0);
                    C30281hn edit = r3.A02.A01.edit();
                    int i = AqN + 1;
                    edit.Bz6(A0D, i);
                    edit.commit();
                    if (i >= ((Integer) r3.A01.get(str)).intValue()) {
                        r3.A02.A05(str, false);
                    }
                }
            }
        }
    }

    public boolean A08(String str) {
        if (A07(str)) {
            boolean z = true;
            if (Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(284910950552722L)).booleanValue()) {
                if (Boolean.valueOf(((C25051Yd) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AOJ, this.A00)).Aem(284910950618259L)).booleanValue()) {
                    return A03();
                }
                if (!A07("advanced_upsell_for_all_show_again") || !this.A03.A00.contains(str) || !A03()) {
                    return false;
                }
                return true;
            } else if (A07("advanced_upsell_for_all_show_again") && this.A03.A00.contains(str)) {
                return true;
            } else {
                AnonymousClass1Y7 A002 = C05730aE.A00(str);
                synchronized (this) {
                    if (this.A02.containsKey(A002)) {
                        z = ((Boolean) this.A02.get(A002)).booleanValue();
                    }
                }
                return z;
            }
        }
        return false;
    }
}
