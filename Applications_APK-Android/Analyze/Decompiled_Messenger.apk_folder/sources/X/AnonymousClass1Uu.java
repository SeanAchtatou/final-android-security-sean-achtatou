package X;

import com.facebook.omnistore.module.OmnistoreComponent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Uu  reason: invalid class name */
public final class AnonymousClass1Uu {
    private static volatile AnonymousClass1Uu A06;
    public C06790c5 A00;
    public final AnonymousClass0XN A01;
    public final C04460Ut A02;
    public final List A03 = new ArrayList();
    public final ExecutorService A04;
    private final C24341Tf A05;

    public static final AnonymousClass1Uu A00(AnonymousClass1XY r7) {
        if (A06 == null) {
            synchronized (AnonymousClass1Uu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r7);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r7.getApplicationInjector();
                        A06 = new AnonymousClass1Uu(C24341Tf.A00(applicationInjector), AnonymousClass0XM.A00(applicationInjector), C04430Uq.A02(applicationInjector), AnonymousClass0UX.A0a(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public static void A01(AnonymousClass1Uu r3) {
        for (OmnistoreComponent A022 : r3.A03) {
            r3.A05.A02(A022);
        }
    }

    private AnonymousClass1Uu(C24341Tf r2, AnonymousClass0XN r3, C04460Ut r4, ExecutorService executorService) {
        this.A05 = r2;
        this.A01 = r3;
        this.A02 = r4;
        this.A04 = executorService;
    }
}
