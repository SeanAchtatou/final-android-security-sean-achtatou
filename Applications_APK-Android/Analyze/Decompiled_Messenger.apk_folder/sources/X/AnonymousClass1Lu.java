package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.RequestAppointmentData;

/* renamed from: X.1Lu  reason: invalid class name */
public final class AnonymousClass1Lu implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new RequestAppointmentData(parcel);
    }

    public Object[] newArray(int i) {
        return new RequestAppointmentData[i];
    }
}
