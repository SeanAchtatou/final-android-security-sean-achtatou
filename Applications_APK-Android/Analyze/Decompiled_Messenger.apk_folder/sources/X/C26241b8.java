package X;

import com.google.common.collect.ImmutableSet;

/* renamed from: X.1b8  reason: invalid class name and case insensitive filesystem */
public final class C26241b8 implements C05700aB {
    public static final AnonymousClass1Y7 A00 = ((AnonymousClass1Y7) A02.A09("application_locale"));
    public static final AnonymousClass1Y8 A01 = A03.A09("locale_last_time_synced");
    private static final AnonymousClass1Y7 A02 = ((AnonymousClass1Y7) C04350Ue.A05.A09("language_switcher/"));
    private static final AnonymousClass1Y8 A03 = C04350Ue.A0A.A09("language_switcher/");

    static {
        A02.A09("internal_settings/");
    }

    public static final C26241b8 A00() {
        return new C26241b8();
    }

    public ImmutableSet AzO() {
        return ImmutableSet.A04(A01);
    }
}
