package X;

import com.facebook.messaging.business.commerce.model.retail.CommerceData;
import com.facebook.messaging.model.attribution.ContentAppAttribution;
import com.facebook.messaging.model.messages.GenericAdminMessageInfo;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessageRepliedTo;
import com.facebook.messaging.model.messages.MontageBrandedCameraAttributionData;
import com.facebook.messaging.model.messages.ParticipantInfo;
import com.facebook.messaging.model.messages.Publicity;
import com.facebook.messaging.model.messages.montageattribution.MontageAttributionData;
import com.facebook.messaging.model.mms.MmsData;
import com.facebook.messaging.model.montagemetadata.MontageMetadata;
import com.facebook.messaging.model.payment.PaymentRequestData;
import com.facebook.messaging.model.payment.PaymentTransactionData;
import com.facebook.messaging.model.send.PendingSendQueueKey;
import com.facebook.messaging.model.send.SendError;
import com.facebook.messaging.model.share.SentShareAttachment;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.share.model.ComposerAppAttribution;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableList;
import com.google.common.collect.RegularImmutableMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1TG  reason: invalid class name */
public final class AnonymousClass1TG implements C36871u1 {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public CommerceData A05;
    public ContentAppAttribution A06;
    public C72033dY A07;
    public GenericAdminMessageInfo A08;
    public C36881u2 A09;
    public AnonymousClass1R6 A0A;
    public MessageRepliedTo A0B;
    public AnonymousClass1V7 A0C = AnonymousClass1V7.A0K;
    public MontageBrandedCameraAttributionData A0D;
    public C98894o1 A0E;
    public C117335hD A0F;
    public C117335hD A0G;
    public C21936Al6 A0H;
    public ParticipantInfo A0I;
    public ParticipantInfo A0J;
    public Publicity A0K;
    public MontageAttributionData A0L;
    public MmsData A0M;
    public MontageMetadata A0N;
    public PaymentRequestData A0O;
    public PaymentTransactionData A0P;
    public PendingSendQueueKey A0Q;
    public SendError A0R;
    public SentShareAttachment A0S;
    public ThreadKey A0T;
    public ComposerAppAttribution A0U;
    public ImmutableList A0V;
    public ImmutableList A0W;
    public ImmutableList A0X;
    public ImmutableList A0Y;
    public ImmutableList A0Z;
    public ImmutableList A0a;
    public ImmutableList A0b;
    public ImmutableList A0c;
    public ImmutableList A0d;
    public ImmutableMap A0e;
    public ImmutableMap A0f;
    public AnonymousClass0V0 A0g;
    public AnonymousClass0V0 A0h;
    public Integer A0i;
    public Long A0j;
    public String A0k;
    public String A0l;
    public String A0m;
    public String A0n;
    public String A0o;
    public String A0p;
    public String A0q;
    public String A0r;
    public String A0s;
    public String A0t;
    public String A0u;
    public String A0v;
    public String A0w;
    public String A0x;
    public String A0y;
    public Map A0z;
    public Map A10;
    public boolean A11;
    public boolean A12;
    public boolean A13;
    public boolean A14;
    public boolean A15;
    private C61192yU A16;

    public void A04(MmsData mmsData) {
        boolean z = false;
        if (mmsData != null) {
            z = true;
        }
        Preconditions.checkArgument(z);
        this.A0M = mmsData;
    }

    public Message A00() {
        return new Message(this);
    }

    public void A03(Message message) {
        this.A0m = message.A0q;
        this.A0T = message.A0U;
        this.A04 = message.A03;
        this.A03 = message.A02;
        this.A02 = message.A01;
        this.A0J = message.A0K;
        this.A0y = message.A10;
        this.A0v = message.A0x;
        this.A13 = message.A15;
        this.A0W = message.A0X;
        this.A0d = message.A0c;
        this.A0x = message.A0z;
        this.A0C = message.A0D;
        this.A0V = message.A0W;
        this.A0u = message.A0w;
        this.A12 = message.A14;
        this.A0w = message.A0y;
        this.A09 = message.A0A;
        this.A0A = message.A0B;
        this.A0c = message.A0b;
        this.A0S = message.A0T;
        this.A0z = new HashMap(message.A0g);
        this.A10 = new HashMap(message.A0h);
        this.A0R = message.A0S;
        this.A0K = message.A0L;
        this.A0l = message.A0p;
        this.A0k = message.A0o;
        this.A0e = message.A0f;
        this.A0Q = message.A0R;
        this.A0P = message.A0Q;
        this.A0O = message.A0P;
        this.A11 = message.A12;
        this.A0U = message.A0V;
        this.A06 = message.A07;
        this.A16 = message.A06;
        this.A05 = message.A05;
        this.A08 = message.A09;
        this.A0i = message.A0l;
        this.A0j = message.A0m;
        this.A0M = message.A0N;
        this.A15 = message.A16;
        this.A01 = message.A04;
        this.A0t = message.A11;
        this.A0n = message.A0n;
        this.A0p = message.A0s;
        this.A0E = message.A0F;
        this.A0F = message.A0G;
        this.A0G = message.A0H;
        this.A0q = message.A0t;
        this.A0H = message.A0I;
        this.A0r = message.A0u;
        this.A0X = message.A0Y;
        this.A0f = message.A0i;
        this.A14 = message.A13;
        this.A0h = message.A0k;
        this.A0g = message.A0j;
        this.A00 = message.A00;
        this.A0Y = message.A0a;
        this.A0b = message.A0Z;
        this.A0D = message.A0E;
        this.A0B = message.A0C;
        this.A0L = message.A0M;
        this.A07 = message.A08;
        this.A0N = message.A0O;
        this.A0a = message.A0e;
        this.A0I = message.A0J;
        this.A0Z = message.A0d;
        this.A0o = message.A0r;
        this.A0s = message.A0v;
    }

    public void A06(String str) {
        if (str != null && str.startsWith("m_mid")) {
            C010708t.A0K("MessageBuilder", "Encountered a legacy message id.");
        }
        this.A0m = str;
    }

    public void A07(String str, String str2) {
        this.A0z.put(str, str2);
    }

    public void A0B(Map map) {
        if (map == null) {
            this.A0f = RegularImmutableMap.A03;
        } else {
            this.A0f = ImmutableMap.copyOf(map);
        }
    }

    public void A0C(boolean z) {
        if (z) {
            this.A0z.put("message", "save");
        } else if (this.A0z.containsKey("message")) {
            this.A0z.remove("message");
        }
    }

    public CommerceData Ahe() {
        return this.A05;
    }

    public PaymentRequestData Axe() {
        return this.A0O;
    }

    public PaymentTransactionData Axg() {
        return this.A0P;
    }

    public SentShareAttachment B2Y() {
        return this.A0S;
    }

    public ImmutableList B2v() {
        return this.A0d;
    }

    public C61192yU BAB() {
        return this.A16;
    }

    public AnonymousClass1TG() {
        ImmutableList immutableList = RegularImmutableList.A02;
        this.A0W = immutableList;
        this.A0d = immutableList;
        this.A0V = immutableList;
        this.A09 = C36881u2.API;
        this.A0c = immutableList;
        this.A0z = AnonymousClass0TG.A04();
        this.A10 = AnonymousClass0TG.A04();
        this.A0R = SendError.A08;
        this.A0K = Publicity.A03;
        this.A0A = AnonymousClass1R6.UNKNOWN;
        this.A0M = MmsData.A04;
        this.A0X = immutableList;
        this.A0f = RegularImmutableMap.A03;
        this.A0h = new HashMultimap();
        this.A0g = new HashMultimap();
        this.A00 = -1;
        this.A0a = immutableList;
        this.A0Z = immutableList;
    }

    public void A01(C61192yU r2) {
        this.A16 = C58012t1.A02(r2, C05850aR.A02());
    }

    public void A02(AnonymousClass1R6 r1) {
        Preconditions.checkNotNull(r1);
        this.A0A = r1;
    }

    public void A05(AnonymousClass0V0 r2) {
        Preconditions.checkNotNull(r2);
        this.A0g = new HashMultimap(r2);
    }

    public void A08(List list) {
        this.A0W = ImmutableList.copyOf((Collection) list);
    }

    public void A09(List list) {
        this.A0c = ImmutableList.copyOf((Collection) list);
    }

    public void A0A(Map map) {
        Preconditions.checkNotNull(map);
        this.A0z = new HashMap(map);
    }
}
