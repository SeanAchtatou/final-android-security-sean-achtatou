package X;

import android.util.LruCache;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.messages.MessagesCollection;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0m9  reason: invalid class name */
public final class AnonymousClass0m9 implements AnonymousClass0mA {
    private static volatile AnonymousClass0m9 A05;
    public AnonymousClass0UN A00;
    public final LruCache A01 = new LruCache(300);
    public final LruCache A02 = new LruCache(300);
    public final Map A03 = new HashMap();
    public final Map A04 = new HashMap();

    public static C71183bx A01(AnonymousClass0m9 r6, ThreadKey threadKey, C196249Kq r8, String str, String str2) {
        return A02(r6, threadKey, r8, str, str2, false);
    }

    public static C71183bx A02(AnonymousClass0m9 r6, ThreadKey threadKey, C196249Kq r8, String str, String str2, boolean z) {
        String str3;
        if (z) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Math.min(stackTrace.length, 15); i++) {
                sb.append(stackTrace[i].toString());
                sb.append(10);
            }
            str3 = sb.toString();
        } else {
            str3 = null;
        }
        C71173bw r3 = new C71173bw();
        r3.A00 = ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, r6.A00)).now();
        r3.A04 = threadKey;
        Thread currentThread = Thread.currentThread();
        r3.A01 = currentThread.getId();
        r3.A08 = currentThread.getName();
        r3.A07 = str3;
        r3.A05 = str;
        r3.A03 = r8;
        r3.A06 = str2;
        return new C71183bx(r3);
    }

    public static synchronized void A04(AnonymousClass0m9 r5, MessagesCollection messagesCollection, String str) {
        Message message;
        synchronized (r5) {
            if (r5.A0F()) {
                ThreadKey threadKey = messagesCollection.A00;
                if (A05(threadKey)) {
                    C196249Kq A002 = A00(messagesCollection.A01, false);
                    r5.A03.put(threadKey, A002);
                    if (((C196249Kq) r5.A04.get(threadKey)) == null && (message = A002.A00) != null) {
                        r5.A04.put(threadKey, new C196249Kq(message, null));
                    }
                    C71183bx A012 = A01(r5, messagesCollection.A00, A00(messagesCollection.A01, true), str, null);
                    r5.A01.put(A012, A012);
                }
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:1|2|3|4|(2:7|5)|9|10|11) */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0037, code lost:
        if (r2 >= 300) goto L_0x0039;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0039 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String A06() {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x003f }
            r1.<init>()     // Catch:{ all -> 0x003f }
            android.util.LruCache r0 = r4.A01     // Catch:{ all -> 0x003f }
            java.util.Map r0 = r0.snapshot()     // Catch:{ all -> 0x003f }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x003f }
            r1.addAll(r0)     // Catch:{ all -> 0x003f }
            java.util.Collections.reverse(r1)     // Catch:{ all -> 0x003f }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ all -> 0x003f }
            r3.<init>()     // Catch:{ all -> 0x003f }
            r2 = 0
            java.util.Iterator r1 = r1.iterator()     // Catch:{ JSONException -> 0x0039 }
        L_0x0020:
            boolean r0 = r1.hasNext()     // Catch:{ JSONException -> 0x0039 }
            if (r0 == 0) goto L_0x0039
            java.lang.Object r0 = r1.next()     // Catch:{ JSONException -> 0x0039 }
            X.3bx r0 = (X.C71183bx) r0     // Catch:{ JSONException -> 0x0039 }
            org.json.JSONObject r0 = r0.A00()     // Catch:{ JSONException -> 0x0039 }
            r3.put(r0)     // Catch:{ JSONException -> 0x0039 }
            int r2 = r2 + 1
            r0 = 300(0x12c, float:4.2E-43)
            if (r2 < r0) goto L_0x0020
        L_0x0039:
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x003f }
            monitor-exit(r4)
            return r0
        L_0x003f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m9.A06():java.lang.String");
    }

    public synchronized void A07(long j, String str, ThreadKey threadKey, String str2, String str3) {
        if (A0F() && A05(threadKey)) {
            C71173bw r3 = new C71173bw();
            r3.A00 = ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A00)).now();
            r3.A04 = threadKey;
            Thread currentThread = Thread.currentThread();
            r3.A01 = currentThread.getId();
            r3.A08 = currentThread.getName();
            r3.A05 = str2;
            r3.A02 = j;
            r3.A09 = str;
            r3.A06 = str3;
            C71183bx r1 = new C71183bx(r3);
            this.A01.put(r1, r1);
        }
    }

    public synchronized void A08(long j, String str, String str2, String str3) {
        A07(j, str, null, str2, str3);
    }

    public synchronized void A09(C10950l8 r4, ThreadSummary threadSummary, String str) {
        if (A0F()) {
            boolean z = false;
            if (r4 == C10950l8.A05) {
                z = true;
            }
            if (z) {
                C71183bx A012 = A01(this, null, null, AnonymousClass08S.A0J("addThreadToFolderThreadListInCache-", str), threadSummary.toString());
                this.A01.put(A012, A012);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0048, code lost:
        if (r9.A03 == 0) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (r9.A03 == 0) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A0A(com.facebook.messaging.model.threadkey.ThreadKey r8, com.facebook.messaging.model.messages.Message r9) {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.A0F()     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0085
            boolean r0 = A05(r8)     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0085
            java.lang.String r1 = "updateLatestMessageInDb"
            java.lang.String r0 = com.facebook.messaging.model.messages.Message.A01(r9)     // Catch:{ all -> 0x0087 }
            r7.A0C(r8, r1, r0)     // Catch:{ all -> 0x0087 }
            boolean r0 = r9.A14     // Catch:{ all -> 0x0087 }
            if (r0 != 0) goto L_0x0023
            long r4 = r9.A03     // Catch:{ all -> 0x0087 }
            r2 = 0
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            r0 = 1
            if (r1 != 0) goto L_0x0024
        L_0x0023:
            r0 = 0
        L_0x0024:
            if (r0 == 0) goto L_0x0085
            java.util.Map r0 = r7.A04     // Catch:{ all -> 0x0087 }
            java.lang.Object r2 = r0.get(r8)     // Catch:{ all -> 0x0087 }
            X.9Kq r2 = (X.C196249Kq) r2     // Catch:{ all -> 0x0087 }
            if (r2 != 0) goto L_0x003c
            X.9Kq r2 = new X.9Kq     // Catch:{ all -> 0x0087 }
            r0 = 0
            r2.<init>(r9, r0)     // Catch:{ all -> 0x0087 }
        L_0x0036:
            java.util.Map r0 = r7.A04     // Catch:{ all -> 0x0087 }
            r0.put(r8, r2)     // Catch:{ all -> 0x0087 }
            goto L_0x0085
        L_0x003c:
            r6 = r9
            boolean r0 = r9.A14     // Catch:{ all -> 0x0087 }
            if (r0 != 0) goto L_0x004a
            long r0 = r9.A03     // Catch:{ all -> 0x0087 }
            r4 = 0
            int r3 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            r0 = 1
            if (r3 != 0) goto L_0x004b
        L_0x004a:
            r0 = 0
        L_0x004b:
            if (r0 == 0) goto L_0x0036
            java.lang.String r1 = r9.A0q     // Catch:{ all -> 0x0087 }
            if (r1 == 0) goto L_0x0065
            com.facebook.messaging.model.messages.Message r0 = r2.A00     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0065
            java.lang.String r0 = r0.A0q     // Catch:{ all -> 0x0087 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0065
            com.facebook.messaging.model.messages.Message r5 = r2.A01     // Catch:{ all -> 0x0087 }
        L_0x005f:
            X.9Kq r2 = new X.9Kq     // Catch:{ all -> 0x0087 }
            r2.<init>(r6, r5)     // Catch:{ all -> 0x0087 }
            goto L_0x0036
        L_0x0065:
            if (r1 == 0) goto L_0x0082
            com.facebook.messaging.model.messages.Message r5 = r2.A00     // Catch:{ all -> 0x0087 }
            if (r5 == 0) goto L_0x0082
            com.facebook.messaging.model.messages.Message r0 = r2.A01     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0082
            java.lang.String r0 = r0.A0q     // Catch:{ all -> 0x0087 }
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0082
            long r3 = r9.A03     // Catch:{ all -> 0x0087 }
            long r1 = r5.A03     // Catch:{ all -> 0x0087 }
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x005f
            r6 = r5
            r5 = r9
            goto L_0x005f
        L_0x0082:
            com.facebook.messaging.model.messages.Message r5 = r2.A00     // Catch:{ all -> 0x0087 }
            goto L_0x005f
        L_0x0085:
            monitor-exit(r7)
            return
        L_0x0087:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0m9.A0A(com.facebook.messaging.model.threadkey.ThreadKey, com.facebook.messaging.model.messages.Message):void");
    }

    public synchronized void A0B(ThreadKey threadKey, Message message, String str) {
        if (A0F() && A05(threadKey)) {
            C71183bx A012 = A01(this, threadKey, null, AnonymousClass08S.A0J("markThreadStale-", str), Message.A01(message));
            this.A01.put(A012, A012);
        }
    }

    public synchronized void A0C(ThreadKey threadKey, String str, String str2) {
        if (A0F() && A05(threadKey)) {
            C71183bx A012 = A01(this, threadKey, null, str, str2);
            this.A01.put(A012, A012);
        }
    }

    public synchronized void A0D(Object obj, C10950l8 r8, String str) {
        if (A0F()) {
            boolean z = false;
            if (r8 == C10950l8.A05) {
                z = true;
            }
            if (z) {
                C71183bx A022 = A02(this, null, null, AnonymousClass08S.A0J("updateFolderCacheData-", str), obj.toString(), false);
                this.A01.put(A022, A022);
            }
        }
    }

    public synchronized void A0E(String str, String str2, int i) {
        if (A0F()) {
            C71183bx A012 = A01(this, null, null, "markThreadReadDb", AnonymousClass24B.$const$string(330) + str + ", values: " + str2 + ", rows:" + i);
            this.A01.put(A012, A012);
        }
    }

    public String Aji() {
        return "messaging_debug_events.txt";
    }

    public static final AnonymousClass0m9 A03(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (AnonymousClass0m9.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new AnonymousClass0m9(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static boolean A05(ThreadKey threadKey) {
        if (threadKey == null) {
            return true;
        }
        if (threadKey.A0M() || threadKey.A05 == C28711fF.SMS) {
            return false;
        }
        return true;
    }

    public boolean A0F() {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, this.A00)).AbO(211, false);
    }

    private AnonymousClass0m9(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(2, r3);
    }

    public static C196249Kq A00(List list, boolean z) {
        Iterator it = list.iterator();
        Message message = null;
        Message message2 = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Message message3 = (Message) it.next();
            if (message3.A03 != 0 && (z || !message3.A14)) {
                if (message2 != null) {
                    message = message3;
                    break;
                }
                message2 = message3;
            }
        }
        return new C196249Kq(message2, message);
    }

    public String Ajh() {
        return A06();
    }
}
