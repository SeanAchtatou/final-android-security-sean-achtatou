package X;

import com.facebook.prefs.shared.FbSharedPreferences;
import com.facebook.prefs.shared.FbSharedPreferencesModule;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.13I  reason: invalid class name */
public final class AnonymousClass13I {
    public static AnonymousClass7Xn A0D;
    public static final Object A0E = new Object();
    private static volatile AnonymousClass13I A0F;
    public int A00;
    public boolean A01;
    private C06790c5 A02;
    public final AnonymousClass0US A03;
    public final AnonymousClass0US A04;
    public final AnonymousClass0US A05;
    public final AnonymousClass0US A06;
    public final AnonymousClass0US A07;
    public final AnonymousClass0US A08;
    public final AnonymousClass0US A09;
    public final FbSharedPreferences A0A;
    public final C04310Tq A0B;
    private final AnonymousClass0US A0C;

    public static final AnonymousClass13I A00(AnonymousClass1XY r14) {
        if (A0F == null) {
            synchronized (AnonymousClass13I.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0F, r14);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r14.getApplicationInjector();
                        A0F = new AnonymousClass13I(AnonymousClass0VB.A00(AnonymousClass1Y3.BOk, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AZq, applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.AWT, applicationInjector), FbSharedPreferencesModule.A00(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AT9, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AN0, applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.AGr, applicationInjector), AnonymousClass067.A0C(applicationInjector), AnonymousClass0VB.A00(AnonymousClass1Y3.APc, applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AKb, applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0F;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x000f, code lost:
        if (r4.A02 != null) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0011, code lost:
        r2 = ((X.C04460Ut) r4.A0C.get()).BMm();
        r2.A02(X.C06680bu.A0s, new X.AnonymousClass13K(r4, r5));
        r0 = r2.A00();
        r4.A02 = r0;
        r0.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        r5.CIG("Maybe update and check for SMS states change", new X.C32661m7(r4), X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE, X.AnonymousClass07B.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass0WP r5) {
        /*
            r4 = this;
            java.lang.Object r1 = X.AnonymousClass13I.A0E
            monitor-enter(r1)
            boolean r0 = r4.A01     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            return
        L_0x0009:
            r0 = 1
            r4.A01 = r0     // Catch:{ all -> 0x003f }
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            X.0c5 r0 = r4.A02
            if (r0 != 0) goto L_0x0030
            X.0US r0 = r4.A0C
            java.lang.Object r0 = r0.get()
            X.0Ut r0 = (X.C04460Ut) r0
            X.0bl r2 = r0.BMm()
            java.lang.String r1 = X.C06680bu.A0s
            X.13K r0 = new X.13K
            r0.<init>(r4, r5)
            r2.A02(r1, r0)
            X.0c5 r0 = r2.A00()
            r4.A02 = r0
            r0.A00()
        L_0x0030:
            X.1m7 r3 = new X.1m7
            r3.<init>(r4)
            X.0XV r2 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            java.lang.String r0 = "Maybe update and check for SMS states change"
            r5.CIG(r0, r3, r2, r1)
            return
        L_0x003f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13I.A01(X.0WP):void");
    }

    private AnonymousClass13I(AnonymousClass0US r1, AnonymousClass0US r2, C04310Tq r3, FbSharedPreferences fbSharedPreferences, AnonymousClass0US r5, AnonymousClass0US r6, AnonymousClass0US r7, AnonymousClass0US r8, AnonymousClass0US r9, AnonymousClass0US r10) {
        this.A06 = r1;
        this.A09 = r2;
        this.A0B = r3;
        this.A0A = fbSharedPreferences;
        this.A07 = r5;
        this.A04 = r6;
        this.A05 = r7;
        this.A03 = r8;
        this.A08 = r9;
        this.A0C = r10;
    }
}
