package X;

import com.facebook.litho.ComponentTree;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1KH  reason: invalid class name */
public final class AnonymousClass1KH {
    public final int A00;
    public final int A01;
    public final int A02;
    public final C17770zR A03;
    public final C188915v A04;
    public final String A05;
    public final FutureTask A06;
    public final AtomicInteger A07 = new AtomicInteger(0);
    public final AtomicInteger A08 = new AtomicInteger(-1);
    public final boolean A09;
    public final boolean A0A;
    private final AnonymousClass0p4 A0B;
    private final AnonymousClass1KE A0C;
    public volatile C188915v A0D;
    public volatile Object A0E;
    public volatile Object A0F;
    public volatile boolean A0G;
    public volatile boolean A0H = false;
    public final /* synthetic */ ComponentTree A0I;

    public synchronized void A01() {
        if (!this.A0H) {
            this.A0E = null;
            this.A0F = null;
            this.A0H = true;
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                AnonymousClass1KH r5 = (AnonymousClass1KH) obj;
                if (!(this.A02 == r5.A02 && this.A00 == r5.A00 && this.A0B.equals(r5.A0B) && this.A03.A00 == r5.A03.A00)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002f, code lost:
        if (r11 == 6) goto L_0x0031;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1KH(com.facebook.litho.ComponentTree r3, X.AnonymousClass0p4 r4, X.C17770zR r5, int r6, int r7, boolean r8, X.C188915v r9, X.AnonymousClass1KE r10, int r11, java.lang.String r12) {
        /*
            r2 = this;
            r2.A0I = r3
            r2.<init>()
            java.util.concurrent.atomic.AtomicInteger r1 = new java.util.concurrent.atomic.AtomicInteger
            r0 = -1
            r1.<init>(r0)
            r2.A08 = r1
            java.util.concurrent.atomic.AtomicInteger r1 = new java.util.concurrent.atomic.AtomicInteger
            r0 = 0
            r1.<init>(r0)
            r2.A07 = r1
            r2.A0H = r0
            r2.A0B = r4
            r2.A03 = r5
            r2.A02 = r6
            r2.A00 = r7
            r2.A09 = r8
            r2.A04 = r9
            r2.A0C = r10
            if (r11 == 0) goto L_0x0031
            r0 = 2
            if (r11 == r0) goto L_0x0031
            r0 = 4
            if (r11 == r0) goto L_0x0031
            r1 = 6
            r0 = 0
            if (r11 != r1) goto L_0x0032
        L_0x0031:
            r0 = 1
        L_0x0032:
            r2.A0A = r0
            r2.A01 = r11
            r2.A05 = r12
            java.util.concurrent.FutureTask r1 = new java.util.concurrent.FutureTask
            X.1KI r0 = new X.1KI
            r0.<init>(r2)
            r1.<init>(r0)
            r2.A06 = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1KH.<init>(com.facebook.litho.ComponentTree, X.0p4, X.0zR, int, int, boolean, X.15v, X.1KE, int, java.lang.String):void");
    }

    public static AnonymousClass0p4 A00(AnonymousClass1KH r6) {
        AnonymousClass0p4 r4;
        synchronized (r6.A0I) {
            r4 = new AnonymousClass0p4(r6.A0B, new AnonymousClass1JC(r6.A0I.A0E), r6.A0C, null);
        }
        return r4;
    }

    public int hashCode() {
        return (((((this.A0B.hashCode() * 31) + this.A03.A00) * 31) + this.A02) * 31) + this.A00;
    }
}
