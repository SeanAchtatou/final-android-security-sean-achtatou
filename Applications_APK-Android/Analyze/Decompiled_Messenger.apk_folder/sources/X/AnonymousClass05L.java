package X;

import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.ipc.TraceContext;
import com.facebook.profilo.provider.atrace.Atrace;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.TreeMap;

/* renamed from: X.05L  reason: invalid class name */
public final class AnonymousClass05L extends C000900o {
    public static final int A00 = ProvidersRegistry.A00.A02("atrace");

    public AnonymousClass05L() {
        super("profilo_atrace");
    }

    public void disable() {
        Method method;
        Boolean bool;
        int A03 = C000700l.A03(1420678510);
        TraceContext traceContext = this.A00;
        boolean z = false;
        if (traceContext != null) {
            boolean z2 = false;
            TreeMap treeMap = traceContext.A06.A00;
            if (!(treeMap == null || (bool = (Boolean) treeMap.get("provider.atrace.single_lib_optimization")) == null)) {
                z2 = bool.booleanValue();
            }
            if (z2) {
                z = true;
            }
        }
        if (Atrace.hasHacks(z)) {
            Atrace.restoreSystraceNative(z);
            Field field = C02960Hh.A00;
            if (!(field == null || (method = C02960Hh.A01) == null)) {
                try {
                    field.set(null, method.invoke(null, new Object[0]));
                } catch (IllegalAccessException | InvocationTargetException unused) {
                }
            }
        }
        C000700l.A09(-1450636393, A03);
    }

    public void enable() {
        Method method;
        Boolean bool;
        int A03 = C000700l.A03(2059097745);
        TraceContext traceContext = this.A00;
        boolean z = false;
        if (traceContext != null) {
            boolean z2 = false;
            TreeMap treeMap = traceContext.A06.A00;
            if (!(treeMap == null || (bool = (Boolean) treeMap.get("provider.atrace.single_lib_optimization")) == null)) {
                z2 = bool.booleanValue();
            }
            if (z2) {
                z = true;
            }
        }
        if (Atrace.hasHacks(z)) {
            Atrace.enableSystraceNative(z);
            Field field = C02960Hh.A00;
            if (!(field == null || (method = C02960Hh.A01) == null)) {
                try {
                    field.set(null, method.invoke(null, new Object[0]));
                } catch (IllegalAccessException | InvocationTargetException unused) {
                }
            }
        }
        C000700l.A09(-252517447, A03);
    }

    public int getSupportedProviders() {
        return A00;
    }

    public int getTracingProviders() {
        if (Atrace.isEnabled()) {
            return A00;
        }
        return 0;
    }
}
