package X;

import android.os.Bundle;
import com.facebook.auth.userscope.UserScoped;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

@UserScoped
/* renamed from: X.1lU  reason: invalid class name and case insensitive filesystem */
public final class C32271lU {
    private static C05540Zi A04;
    private AnonymousClass0UN A00;
    public final LinkedList A01 = new LinkedList();
    public final Map A02 = new HashMap();
    public final ConcurrentLinkedQueue A03 = new ConcurrentLinkedQueue();

    public synchronized C27211cp A02(AnonymousClass0lL r3) {
        if (!this.A01.isEmpty()) {
            C158937Ya r1 = new C158937Ya();
            this.A01.add(r3);
            this.A02.put(r3, r1);
            return r1;
        }
        this.A01.add(r3);
        return A00(r3);
    }

    public synchronized void A03(Bundle bundle) {
        if (!this.A01.isEmpty() && bundle.getLong("paramsId") == ((AnonymousClass0lL) this.A01.peek()).Ax6().getLong("paramsId")) {
            synchronized (this) {
                if (!this.A01.isEmpty()) {
                    this.A02.remove((AnonymousClass0lL) this.A01.pop());
                    if (!this.A01.isEmpty()) {
                        AnonymousClass0lL r2 = (AnonymousClass0lL) this.A01.peek();
                        ((C158937Ya) this.A02.get(r2)).A03(A00(r2));
                    }
                }
            }
        }
    }

    public static final C32271lU A01(AnonymousClass1XY r4) {
        C32271lU r0;
        synchronized (C32271lU.class) {
            C05540Zi A002 = C05540Zi.A00(A04);
            A04 = A002;
            try {
                if (A002.A03(r4)) {
                    A04.A00 = new C32271lU((AnonymousClass1XY) A04.A01());
                }
                C05540Zi r1 = A04;
                r0 = (C32271lU) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A04.A02();
                throw th;
            }
        }
        return r0;
    }

    private C32271lU(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
    }

    private C27211cp A00(AnonymousClass0lL r7) {
        C27211cp CGe = r7.CGe();
        this.A03.offer(new C93274cR(((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A00)).now(), r7.AwN()));
        CGe.addListener(new C93284cS(this, r7), C25141Ym.INSTANCE);
        return CGe;
    }
}
