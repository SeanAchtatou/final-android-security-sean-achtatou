package X;

/* renamed from: X.0Mv  reason: invalid class name and case insensitive filesystem */
public final class C03310Mv implements AnonymousClass01J {
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean ANg(java.io.Writer r7, X.AnonymousClass0HM r8) {
        /*
            r6 = this;
            boolean r0 = X.AnonymousClass0DC.A00
            if (r0 == 0) goto L_0x0042
            java.lang.String[] r2 = X.AnonymousClass0DC.A01
            int r0 = r2.length
            long[] r1 = new long[r0]
            java.lang.String r0 = "/proc/self/smaps_rollup"
            boolean r0 = X.AnonymousClass00V.A02(r0, r2, r1)
            r5 = 0
            if (r0 == 0) goto L_0x0040
            r3 = r1[r5]
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0040
            X.0DF r2 = new X.0DF
            r0 = 1
            r2.<init>(r0, r3)
        L_0x0020:
            boolean r0 = r2.A01
            if (r0 == 0) goto L_0x0048
            java.lang.String r0 = "\""
            java.io.Writer r1 = r7.append(r0)
            java.lang.String r0 = "private_dirty"
            java.io.Writer r1 = r1.append(r0)
            java.lang.String r0 = "\":"
            r1.append(r0)
            long r0 = r2.A00
            java.lang.String r0 = java.lang.Long.toString(r0)
            r7.append(r0)
            r0 = 1
            return r0
        L_0x0040:
            X.AnonymousClass0DC.A00 = r5
        L_0x0042:
            X.0DF r2 = new X.0DF
            r2.<init>()
            goto L_0x0020
        L_0x0048:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03310Mv.ANg(java.io.Writer, X.0HM):boolean");
    }
}
