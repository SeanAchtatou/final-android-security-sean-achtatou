package X;

import android.net.Uri;
import com.facebook.messaging.model.threads.GroupApprovalInfo;
import com.facebook.messaging.model.threads.JoinableInfo;

/* renamed from: X.1fC  reason: invalid class name and case insensitive filesystem */
public final class C28681fC {
    public Uri A00;
    public Uri A01;
    public GroupApprovalInfo A02 = new GroupApprovalInfo(new AnonymousClass103());
    public C17990zt A03 = C17990zt.PUBLIC;
    public String A04;
    public boolean A05;
    public boolean A06;

    public void A00(JoinableInfo joinableInfo) {
        this.A00 = joinableInfo.A00;
        this.A01 = joinableInfo.A01;
        this.A06 = joinableInfo.A06;
        this.A05 = joinableInfo.A05;
        this.A03 = joinableInfo.A03;
        this.A02 = joinableInfo.A02;
        this.A04 = joinableInfo.A04;
    }
}
