package X;

import android.app.Service;
import android.content.Intent;
import android.os.Looper;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: X.0AE  reason: invalid class name */
public abstract class AnonymousClass0AE extends Service {
    private boolean A00;
    private final Object A01 = new Object();
    private volatile AnonymousClass0AP A02;

    public abstract Looper A0B();

    public abstract void A0C();

    public abstract void A0D(Intent intent, int i, int i2);

    public abstract void A0E();

    public void onStart(Intent intent, int i) {
        onStartCommand(intent, -1, i);
    }

    public void A0A() {
        synchronized (this.A01) {
            if (!this.A00) {
                A0C();
                this.A00 = true;
            }
        }
    }

    public final void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        A0A();
        A0F(fileDescriptor, printWriter, strArr);
    }

    public void onCreate() {
        int A002 = AnonymousClass02C.A00(this, 113381559);
        super.onCreate();
        Looper A0B = A0B();
        if (A0B == null || A0B == Looper.getMainLooper()) {
            this.A02 = new AnonymousClass0PZ(this, Looper.getMainLooper());
        } else {
            this.A02 = new AnonymousClass0AP(this, A0B);
        }
        this.A02.A01();
        AnonymousClass02C.A02(-693646734, A002);
    }

    public void onDestroy() {
        int A04 = C000700l.A04(-176630759);
        this.A02.A00();
        super.onDestroy();
        C000700l.A0A(490435558, A04);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int A012 = AnonymousClass02C.A01(this, -1099767358);
        this.A02.A02(intent, i, i2);
        AnonymousClass02C.A02(-447248196, A012);
        return 1;
    }

    public void A0F(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(fileDescriptor, printWriter, strArr);
    }
}
