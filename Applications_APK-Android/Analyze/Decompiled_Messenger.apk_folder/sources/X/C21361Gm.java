package X;

/* renamed from: X.1Gm  reason: invalid class name and case insensitive filesystem */
public enum C21361Gm {
    MORE("More"),
    NOTIFICATION_OFF("Mute"),
    NOTIFICATION_ON("Unmute"),
    DELETE("Delete"),
    CAMERA("Camera"),
    AUDIO_CALL("Call"),
    VIDEO_CALL("VIDEO_CALL"),
    PIN_THREAD("PIN_THREAD"),
    UNPIN_THREAD("UNPIN_THREAD");
    
    public final String mLoggingTag;

    private C21361Gm(String str) {
        this.mLoggingTag = str;
    }
}
