package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.SparseArray;

/* renamed from: X.0OZ  reason: invalid class name */
public abstract class AnonymousClass0OZ extends AnonymousClass0K2 {
    private final Class A00;

    public AnonymousClass0OZ(Class cls) {
        if (cls != null) {
            this.A00 = cls;
            return;
        }
        throw new IllegalArgumentException("intentService cannot be null");
    }

    public void onReceive(Context context, Intent intent) {
        ComponentName startService;
        int A01 = C000700l.A01(252419707);
        intent.getAction();
        if (intent.getAction() == null) {
            C000700l.A0D(intent, -73478706, A01);
            return;
        }
        intent.setClass(context, this.A00);
        SparseArray sparseArray = AnonymousClass0K2.A01;
        synchronized (sparseArray) {
            int i = AnonymousClass0K2.A00;
            int i2 = i + 1;
            AnonymousClass0K2.A00 = i2;
            if (i2 <= 0) {
                AnonymousClass0K2.A00 = 1;
            }
            intent.putExtra("androidx.contentpager.content.wakelockid", i);
            startService = context.startService(intent);
            if (startService == null) {
                startService = null;
            } else {
                PowerManager.WakeLock A002 = C009007v.A00((PowerManager) context.getSystemService("power"), 1, AnonymousClass08S.A0J("wake:", startService.flattenToShortString()));
                C009007v.A03(A002, false);
                C009007v.A02(A002, 60000);
                sparseArray.put(i, A002);
            }
        }
        if (startService == null) {
            C010708t.A0O("FbnsCallbackReceiver", "service %s does not exist", this.A00.getClass().getCanonicalName());
        }
        C000700l.A0D(intent, -737601156, A01);
    }
}
