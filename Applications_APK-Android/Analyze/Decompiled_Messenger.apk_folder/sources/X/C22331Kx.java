package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.TextUtils;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Kx  reason: invalid class name and case insensitive filesystem */
public final class C22331Kx extends C17770zR {
    public static final int A0Y = A0a.getStyle();
    public static final ColorStateList A0Z = new ColorStateList(new int[][]{new int[]{0}}, new int[]{C15320v6.MEASURED_STATE_MASK});
    public static final Typeface A0a = Typeface.DEFAULT;
    public static final Typeface A0b = A0a;
    public static final C14920uM A0c = C14920uM.TOP;
    @Comparable(type = 0)
    public float A00;
    @Comparable(type = 0)
    public float A01;
    @Comparable(type = 0)
    public float A02;
    @Comparable(type = 0)
    public float A03;
    @Comparable(type = 0)
    public float A04 = 1.0f;
    @Comparable(type = 3)
    public int A05 = 0;
    @Comparable(type = 3)
    public int A06;
    @Comparable(type = 3)
    public int A07 = 0;
    @Comparable(type = 3)
    public int A08 = 0;
    @Comparable(type = 3)
    public int A09 = 0;
    @Comparable(type = 3)
    public int A0A = -1;
    @Comparable(type = 3)
    public int A0B = Integer.MAX_VALUE;
    @Comparable(type = 3)
    public int A0C = Integer.MAX_VALUE;
    @Comparable(type = 3)
    public int A0D = -1;
    @Comparable(type = 3)
    public int A0E = Integer.MIN_VALUE;
    @Comparable(type = 3)
    public int A0F = 0;
    @Comparable(type = 3)
    public int A0G = -7829368;
    @Comparable(type = 3)
    public int A0H = 0;
    @Comparable(type = 3)
    public int A0I = 13;
    @Comparable(type = 3)
    public int A0J = A0Y;
    @Comparable(type = 13)
    public ColorStateList A0K = A0Z;
    @Comparable(type = 13)
    public Typeface A0L = A0b;
    @Comparable(type = 13)
    public Layout.Alignment A0M;
    @Comparable(type = 13)
    public TextUtils.TruncateAt A0N;
    @Comparable(type = 13)
    public C22311Kv A0O;
    public AnonymousClass0UN A0P;
    @Comparable(type = 13)
    public AnonymousClass1JS A0Q;
    @Comparable(type = 13)
    public C14920uM A0R = A0c;
    @Comparable(type = 13)
    public CharSequence A0S;
    @Comparable(type = 3)
    public boolean A0T;
    @Comparable(type = 3)
    public boolean A0U = true;
    @Comparable(type = 3)
    public boolean A0V;
    @Comparable(type = 3)
    public boolean A0W = true;
    @Comparable(type = 3)
    public boolean A0X = true;

    public static ComponentBuilderCBuilderShape0_0S0300000 A00(AnonymousClass0p4 r4) {
        ComponentBuilderCBuilderShape0_0S0300000 componentBuilderCBuilderShape0_0S0300000 = new ComponentBuilderCBuilderShape0_0S0300000(0);
        ComponentBuilderCBuilderShape0_0S0300000.A00(componentBuilderCBuilderShape0_0S0300000, r4, 0, 0, new C22331Kx(r4.A09));
        return componentBuilderCBuilderShape0_0S0300000;
    }

    public C22331Kx(Context context) {
        super("FbText");
        this.A0P = new AnonymousClass0UN(2, AnonymousClass1XX.get(context));
    }
}
