package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.user.model.UserPhoneNumber;

/* renamed from: X.168  reason: invalid class name */
public final class AnonymousClass168 implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new UserPhoneNumber(parcel);
    }

    public Object[] newArray(int i) {
        return new UserPhoneNumber[i];
    }
}
