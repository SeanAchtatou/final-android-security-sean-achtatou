package X;

import android.os.Handler;
import android.os.Looper;
import java.io.File;
import java.lang.Thread;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0aI  reason: invalid class name and case insensitive filesystem */
public final class C05770aI {
    public static final Object A0A = new Object();
    public final int A00;
    public final AnonymousClass0XS A01;
    public final Object A02 = new Object();
    public final Thread A03;
    public final Map A04;
    public final CountDownLatch A05;
    public final Executor A06;
    public final AtomicBoolean A07 = new AtomicBoolean(false);
    private final Map A08;
    public volatile boolean A09 = false;

    public static synchronized void A02(C05770aI r7, Set set) {
        synchronized (r7) {
            Throwable th = new Throwable("commit stack");
            Iterator it = set.iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                Map map = (Map) r7.A08.get(str);
                if (map != null) {
                    for (Map.Entry entry : map.entrySet()) {
                        AnonymousClass00S.A04((Handler) entry.getValue(), new C29584Edg(r7, th, (C29585Edh) entry.getKey(), str), -1502616101);
                    }
                }
            }
        }
    }

    static {
        new Handler(Looper.getMainLooper());
    }

    public static RuntimeException A00(C05770aI r5, Exception exc, String str) {
        return new RuntimeException(AnonymousClass08S.A0T("LightSharedPreferences threw an exception for Key: ", str, "; ", "Raw file: ", r5.A01.A02()), exc);
    }

    public static void A01(C05770aI r2) {
        int priority;
        if (!r2.A09) {
            AnonymousClass06K.A01("LightSharedPreferences.waitIfNotLoaded", 1141194875);
            while (!r2.A09) {
                synchronized (r2) {
                    if (r2.A03.getState() != Thread.State.TERMINATED && (priority = Thread.currentThread().getPriority()) > r2.A03.getPriority()) {
                        r2.A03.setPriority(priority);
                    }
                }
                try {
                    r2.A05.await();
                    break;
                } catch (InterruptedException unused) {
                }
            }
            AnonymousClass06K.A00(319702124);
        }
    }

    public static /* synthetic */ void A03(Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
    }

    public C05770aI(File file, Executor executor, int i) {
        if (file != null) {
            this.A01 = new AnonymousClass0XS(file);
            this.A04 = new HashMap();
            this.A08 = new HashMap();
            if (executor != null) {
                this.A06 = executor;
                this.A00 = i;
                this.A05 = new CountDownLatch(1);
                Thread thread = new Thread(new C05780aJ(this), AnonymousClass08S.A0J("LSP-", file.getName()));
                this.A03 = thread;
                thread.start();
                return;
            }
        }
        throw new NullPointerException();
    }

    public int A04(String str, int i) {
        A01(this);
        synchronized (this.A02) {
            try {
                Integer num = (Integer) this.A04.get(str);
                if (num != null) {
                    i = num.intValue();
                }
            } catch (ClassCastException e) {
                throw A00(this, e, str);
            }
        }
        return i;
    }

    public long A05(String str, long j) {
        A01(this);
        synchronized (this.A02) {
            try {
                Long l = (Long) this.A04.get(str);
                if (l != null) {
                    j = l.longValue();
                }
            } catch (ClassCastException e) {
                throw A00(this, e, str);
            }
        }
        return j;
    }

    public AnonymousClass16O A06() {
        A01(this);
        return new AnonymousClass16O(this);
    }

    public String A07(String str, String str2) {
        A01(this);
        synchronized (this.A02) {
            try {
                String str3 = (String) this.A04.get(str);
                if (str3 != null) {
                    str2 = str3;
                }
            } catch (ClassCastException e) {
                throw A00(this, e, str);
            }
        }
        return str2;
    }

    public Map A08() {
        HashMap hashMap;
        A01(this);
        synchronized (this.A02) {
            hashMap = new HashMap(this.A04);
        }
        return hashMap;
    }

    public Set A09(String str, Set set) {
        A01(this);
        synchronized (this.A02) {
            try {
                Set set2 = (Set) this.A04.get(str);
                if (set2 != null) {
                    set = set2;
                }
            } catch (ClassCastException e) {
                throw A00(this, e, str);
            }
        }
        return set;
    }

    public boolean A0A(String str) {
        boolean containsKey;
        A01(this);
        synchronized (this.A02) {
            containsKey = this.A04.containsKey(str);
        }
        return containsKey;
    }

    public boolean A0B(String str, boolean z) {
        A01(this);
        synchronized (this.A02) {
            try {
                Boolean bool = (Boolean) this.A04.get(str);
                if (bool != null) {
                    z = bool.booleanValue();
                }
            } catch (ClassCastException e) {
                throw A00(this, e, str);
            }
        }
        return z;
    }
}
