package X;

import android.content.Context;
import android.content.IntentFilter;

/* renamed from: X.073  reason: invalid class name */
public final class AnonymousClass073 {
    public static void A00(Context context, boolean z, String str) {
        C011609d r3;
        synchronized (C011609d.class) {
            if (C011609d.A00 == null) {
                if (z) {
                    r3 = new C011709e();
                } else {
                    r3 = new AnonymousClass0Jr(str);
                }
                C011609d.A00 = r3;
            } else {
                throw new IllegalStateException("MultiProcessTraceManager already initialized");
            }
        }
        AnonymousClass07D A00 = r3.A00();
        context.registerReceiver(A00, new IntentFilter(A00.A01()));
        r3.A00().A03(context, str);
        C004505k.A00().A09.A00.add(r3);
    }
}
