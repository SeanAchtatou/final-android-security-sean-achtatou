package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0Cq  reason: invalid class name and case insensitive filesystem */
public final class C02050Cq extends C01860Bx {
    public C02050Cq() {
        super("lt");
    }

    public void A03(C02060Cr r12, long j) {
        long j2;
        long j3;
        AtomicLong atomicLong = (AtomicLong) A00(r12);
        if (j > 0) {
            do {
                j2 = atomicLong.get();
                if (j2 == 0) {
                    j3 = j;
                } else {
                    j3 = (long) ((((double) j2) * 0.8d) + (((double) j) * 0.2d));
                }
            } while (!atomicLong.compareAndSet(j2, j3));
        }
    }
}
