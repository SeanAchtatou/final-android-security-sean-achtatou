package X;

import java.util.EnumSet;

/* renamed from: X.1HH  reason: invalid class name */
public final class AnonymousClass1HH implements C33931oN {
    private final C04310Tq A00;

    public static final AnonymousClass1HH A00(AnonymousClass1XY r1) {
        return new AnonymousClass1HH(r1);
    }

    public EnumSet Aga() {
        if (!((Boolean) this.A00.get()).booleanValue()) {
            return EnumSet.noneOf(C33911oL.class);
        }
        return EnumSet.of(C33911oL.A07, C33911oL.A08, C33911oL.A06);
    }

    private AnonymousClass1HH(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0VG.A00(AnonymousClass1Y3.B0T, r2);
    }
}
