package X;

import android.telephony.TelephonyManager;
import com.facebook.inject.InjectorModule;
import com.google.common.base.Platform;
import io.card.payment.BuildConfig;
import java.util.Locale;

@InjectorModule
/* renamed from: X.0kv  reason: invalid class name and case insensitive filesystem */
public final class C10830kv extends AnonymousClass0UV {
    public static final String A00(AnonymousClass1XY r4) {
        TelephonyManager A0c = C04490Ux.A0c(r4);
        AnonymousClass0VG A00 = AnonymousClass0VG.A00(AnonymousClass1Y3.BQw, r4);
        String simCountryIso = A0c.getSimCountryIso();
        if (Platform.stringIsNullOrEmpty(simCountryIso)) {
            simCountryIso = A0c.getNetworkCountryIso();
        }
        if (Platform.stringIsNullOrEmpty(simCountryIso)) {
            simCountryIso = ((Locale) A00.get()).getCountry();
        }
        if (!Platform.stringIsNullOrEmpty(simCountryIso)) {
            return simCountryIso.toUpperCase(Locale.US);
        }
        C010708t.A06(C30375Ev8.A00, "No ISO country code detected!");
        return null;
    }

    public static final String A01(AnonymousClass1XY r2) {
        TelephonyManager A0c = C04490Ux.A0c(r2);
        String simCountryIso = A0c.getSimCountryIso();
        if (C06850cB.A0B(simCountryIso)) {
            simCountryIso = A0c.getNetworkCountryIso();
        }
        if (!C06850cB.A0B(simCountryIso)) {
            return simCountryIso.toUpperCase(Locale.US);
        }
        C010708t.A06(C30376Ev9.A00, "No ISO country code detected!");
        return BuildConfig.FLAVOR;
    }
}
