package X;

/* renamed from: X.1JQ  reason: invalid class name */
public enum AnonymousClass1JQ implements AnonymousClass1JM {
    XLARGE(20, 2132148243),
    LARGE(16, 2132148239),
    MEDIUM(12, 2132148247),
    SMALL(8, 2132148230),
    XSMALL(4, 2132148224),
    XXSMALL(2, 2132148233);
    
    private final int mSizeDip;
    public final int mSizeRes;

    public int B3A() {
        return this.mSizeDip;
    }

    private AnonymousClass1JQ(int i, int i2) {
        this.mSizeDip = i;
        this.mSizeRes = i2;
    }
}
