package X;

import com.google.common.base.Preconditions;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

/* renamed from: X.0rB  reason: invalid class name */
public final class AnonymousClass0rB implements Iterator {
    public int A00;
    private int A01 = -1;
    private int A02 = -1;
    private Object A03;
    private List A04;
    private Queue A05;
    private boolean A06;
    public final /* synthetic */ C16610xP A07;

    public AnonymousClass0rB(C16610xP r2) {
        this.A07 = r2;
        this.A00 = r2.A00;
    }

    private void A00(int i) {
        if (this.A02 < i) {
            if (this.A04 != null) {
                while (true) {
                    C16610xP r2 = this.A07;
                    if (i >= r2.size() || !A01(this.A04, r2.A02[i])) {
                        break;
                    }
                    i++;
                }
            }
            this.A02 = i;
        }
    }

    public boolean hasNext() {
        Queue queue;
        if (this.A07.A00 == this.A00) {
            A00(this.A01 + 1);
            if (this.A02 < this.A07.size() || ((queue = this.A05) != null && !queue.isEmpty())) {
                return true;
            }
            return false;
        }
        throw new ConcurrentModificationException();
    }

    public Object next() {
        if (this.A07.A00 == this.A00) {
            A00(this.A01 + 1);
            int i = this.A02;
            C16610xP r2 = this.A07;
            if (i < r2.size()) {
                this.A01 = i;
                this.A06 = true;
                return r2.A02[i];
            }
            Queue queue = this.A05;
            if (queue != null) {
                this.A01 = r2.size();
                Object poll = queue.poll();
                this.A03 = poll;
                if (poll != null) {
                    this.A06 = true;
                    return poll;
                }
            }
            throw new NoSuchElementException("iterator moved past last element in queue.");
        }
        throw new ConcurrentModificationException();
    }

    public void remove() {
        boolean z;
        C25001Xy.A04(this.A06);
        C16610xP r3 = this.A07;
        int i = r3.A00;
        int i2 = this.A00;
        if (i == i2) {
            this.A06 = false;
            this.A00 = i2 + 1;
            int i3 = this.A01;
            if (i3 < r3.size()) {
                C30362Euo A022 = r3.A02(i3);
                if (A022 != null) {
                    if (this.A05 == null) {
                        this.A05 = new ArrayDeque();
                        this.A04 = new ArrayList(3);
                    }
                    if (!A01(this.A04, A022.A01)) {
                        this.A05.add(A022.A01);
                    }
                    if (!A01(this.A05, A022.A00)) {
                        this.A04.add(A022.A00);
                    }
                }
                this.A01--;
                this.A02--;
                return;
            }
            Object obj = this.A03;
            int i4 = 0;
            while (true) {
                if (i4 >= r3.A01) {
                    z = false;
                    break;
                } else if (r3.A02[i4] == obj) {
                    r3.A02(i4);
                    z = true;
                    break;
                } else {
                    i4++;
                }
            }
            Preconditions.checkState(z);
            this.A03 = null;
            return;
        }
        throw new ConcurrentModificationException();
    }

    private static boolean A01(Iterable iterable, Object obj) {
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            if (it.next() == obj) {
                it.remove();
                return true;
            }
        }
        return false;
    }
}
