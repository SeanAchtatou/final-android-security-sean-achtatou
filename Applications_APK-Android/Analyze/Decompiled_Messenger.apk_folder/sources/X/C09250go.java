package X;

import com.facebook.auth.userscope.UserScoped;

@UserScoped
/* renamed from: X.0go  reason: invalid class name and case insensitive filesystem */
public final class C09250go extends C09260gp implements C05460Za {
    private static C05540Zi A00;

    public static final C09250go A00(AnonymousClass1XY r7) {
        C09250go r0;
        synchronized (C09250go.class) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r7)) {
                    AnonymousClass1XY r02 = (AnonymousClass1XY) A00.A01();
                    A00.A00 = new C09250go(AnonymousClass1YA.A00(r02), C04720Vx.A01(r02), C09280gs.A00(r02), new C09530hg());
                }
                C05540Zi r1 = A00;
                r0 = (C09250go) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C09250go(android.content.Context r7, X.C04740Vz r8, X.C09280gs r9, X.C09530hg r10) {
        /*
            r6 = this;
            com.google.common.collect.ImmutableList r4 = com.google.common.collect.ImmutableList.of(r10)
            java.lang.String r5 = "inbox_units_db"
            r0 = r6
            r2 = r8
            r3 = r9
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09250go.<init>(android.content.Context, X.0Vz, X.0gs, X.0hg):void");
    }

    public void A07() {
        super.A09();
    }

    public void clearUserData() {
        A07();
    }
}
