package X;

import java.util.concurrent.atomic.AtomicLong;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0C3  reason: invalid class name */
public final class AnonymousClass0C3 extends Enum implements AnonymousClass0C4 {
    private static final /* synthetic */ AnonymousClass0C3[] A00;
    public static final AnonymousClass0C3 A01;
    public static final AnonymousClass0C3 A02;
    public static final AnonymousClass0C3 A03;
    public static final AnonymousClass0C3 A04;
    public static final AnonymousClass0C3 A05;
    private final String mJsonKey;
    private final Class mType;

    static {
        Class<AtomicLong> cls = AtomicLong.class;
        AnonymousClass0C3 r4 = new AnonymousClass0C3("CountSuccessfulConnection", 0, "sc", cls);
        A03 = r4;
        AnonymousClass0C3 r5 = new AnonymousClass0C3("CountConnectAttempt", 1, "ca", cls);
        A02 = r5;
        AnonymousClass0C3 r6 = new AnonymousClass0C3("ConnectingMs", 2, "ce", cls);
        A01 = r6;
        Class<String> cls2 = String.class;
        AnonymousClass0C3 r7 = new AnonymousClass0C3("ConnectTriggerReason", 3, "tr", cls2);
        AnonymousClass0C3 r8 = new AnonymousClass0C3("LastConnectFailureReason", 4, "fr", cls2);
        A04 = r8;
        AnonymousClass0C3 r9 = new AnonymousClass0C3("LastDisconnectReason", 5, "dr", cls2);
        A05 = r9;
        A00 = new AnonymousClass0C3[]{r4, r5, r6, r7, r8, r9};
    }

    public static AnonymousClass0C3 valueOf(String str) {
        return (AnonymousClass0C3) Enum.valueOf(AnonymousClass0C3.class, str);
    }

    public static AnonymousClass0C3[] values() {
        return (AnonymousClass0C3[]) A00.clone();
    }

    private AnonymousClass0C3(String str, int i, String str2, Class cls) {
        this.mJsonKey = str2;
        this.mType = cls;
    }

    public String Arl() {
        return this.mJsonKey;
    }

    public Class B8J() {
        return this.mType;
    }
}
