package X;

/* renamed from: X.1fF  reason: invalid class name and case insensitive filesystem */
public enum C28711fF {
    ONE_TO_ONE,
    GROUP,
    TINCAN,
    TINCAN_MULTI_ENDPOINT,
    PENDING_THREAD,
    PENDING_GENERAL_THREAD,
    SMS,
    OPTIMISTIC_GROUP_THREAD,
    MONTAGE
}
