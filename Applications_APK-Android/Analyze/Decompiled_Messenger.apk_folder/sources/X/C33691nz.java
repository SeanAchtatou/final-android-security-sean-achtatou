package X;

import androidx.fragment.app.Fragment;

/* renamed from: X.1nz  reason: invalid class name and case insensitive filesystem */
public abstract class C33691nz extends C32321lZ implements C32331la {
    public void A00(C16040wO r3) {
        ((C33681ny) this).A00.A2T(r3, null);
    }

    public void A01(C16040wO r6, Integer num) {
        boolean z;
        C13450rS r4 = ((C33681ny) this).A00;
        if (r4.A2S() == r6) {
            C13060qW A17 = r4.A17();
            C16040wO A2S = r4.A2S();
            AnonymousClass13r A00 = C13450rS.A00(r4);
            Fragment A0Q = A17.A0Q(A2S.name());
            if (A0Q != null && !A00.BGe(A0Q)) {
                A00.C4d(A0Q);
            }
            z = true;
        } else {
            r4.A2T(r6, null);
            z = false;
        }
        if (num == AnonymousClass07B.A00 && r4.A06.A00.contains(r6)) {
            ((C13420rP) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ASO, r4.A02)).A01(r6).BST(z);
        }
    }
}
