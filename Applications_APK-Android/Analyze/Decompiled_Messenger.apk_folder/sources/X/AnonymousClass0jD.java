package X;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0jD  reason: invalid class name */
public final class AnonymousClass0jD {
    private static volatile AnonymousClass0jD A01;
    public final AnonymousClass0jJ A00;

    public static final AnonymousClass0jD A01(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass0jD.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        r4.getApplicationInjector();
                        A01 = new AnonymousClass0jD(AnonymousClass0jE.getInstance());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public JsonNode A02(String str) {
        try {
            return this.A00.readTree(str);
        } catch (IOException e) {
            throw new C21870AjW(e);
        }
    }

    private AnonymousClass0jD(AnonymousClass0jJ r1) {
        this.A00 = r1;
    }

    public static final AnonymousClass0jD A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
