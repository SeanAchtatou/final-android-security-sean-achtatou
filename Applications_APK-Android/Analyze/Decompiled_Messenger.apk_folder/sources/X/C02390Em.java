package X;

import android.content.Context;
import android.os.Build;
import com.facebook.common.dextricks.DexErrorRecoveryInfo;
import com.facebook.common.dextricks.DexStore;
import com.facebook.quicklog.QuickPerformanceLogger;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/* renamed from: X.0Em  reason: invalid class name and case insensitive filesystem */
public final class C02390Em extends C02400En {
    private static C02390Em A01;
    public QuickPerformanceLogger A00;

    public static synchronized C02390Em A00(Context context) {
        C02390Em r0;
        synchronized (C02390Em.class) {
            if (A01 == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                A01 = new C02390Em(context);
            }
            r0 = A01;
        }
        return r0;
    }

    public void A03(String str, String str2, File file, File file2) {
        RuntimeException runtimeException;
        Class<C002301n> cls;
        Class<C002301n> cls2;
        RuntimeException runtimeException2;
        DexStore findOpened = DexStore.findOpened(new File(AnonymousClass0Ep.A00(this.A01, str, str2), "dex"));
        if (findOpened != null) {
            int i = 0;
            if (Build.VERSION.SDK_INT >= 26) {
                i = 1;
            }
            DexErrorRecoveryInfo loadAll = findOpened.loadAll(i, AnonymousClass08Q.A00, this.A00);
            QuickPerformanceLogger quickPerformanceLogger = this.A00;
            if (quickPerformanceLogger != null) {
                int hashCode = str.hashCode();
                quickPerformanceLogger.markerAnnotate(11337730, hashCode, "scheme", loadAll.odexSchemeName);
                this.A00.markerAnnotate(11337730, hashCode, "result", loadAll.loadResult);
            }
        }
        File file3 = file;
        File file4 = file2;
        if (file == null || !file.exists()) {
            try {
                InputStream open = this.A00.getAssets().open(AnonymousClass08S.A0J(str, "/libs.txt"));
                if (open != null) {
                    open.close();
                    boolean z = false;
                    if (this.A00.getPackageName().contains("instagram")) {
                        z = true;
                    }
                    if (z) {
                        String A0J = AnonymousClass08S.A0J(str, "-lib-zstd");
                        String A0P = AnonymousClass08S.A0P("assets/", str, "/libs.zstd");
                        String A0P2 = AnonymousClass08S.A0P("assets/", str, "/libs.txt");
                        cls = C002301n.class;
                        synchronized (cls) {
                            if (C002301n.A00 == null) {
                                runtimeException = new RuntimeException(AnonymousClass08S.A0J("FbSoLoader.init() was not called prior to loading additional asset ", A0J));
                                throw runtimeException;
                            } else if (!C002301n.A01.contains(A0J)) {
                                AnonymousClass01q.A07(new AnonymousClass02N(C002301n.A00, file4, A0P, A0P2, AnonymousClass07B.A01));
                                C002301n.A01.add(A0J);
                            }
                        }
                    } else {
                        String A0J2 = AnonymousClass08S.A0J(str, "-lib-xzs");
                        String A0P3 = AnonymousClass08S.A0P("assets/", str, "/libs.xzs");
                        String A0P4 = AnonymousClass08S.A0P("assets/", str, "/libs.txt");
                        cls = C002301n.class;
                        synchronized (cls) {
                            if (C002301n.A00 == null) {
                                runtimeException = new RuntimeException(AnonymousClass08S.A0J("FbSoLoader.init() was not called prior to loading additional asset ", A0J2));
                                throw runtimeException;
                            } else if (!C002301n.A01.contains(A0J2)) {
                                AnonymousClass01q.A07(new AnonymousClass02N(C002301n.A00, file4, A0P3, A0P4, AnonymousClass07B.A00));
                                C002301n.A01.add(A0J2);
                            }
                        }
                    }
                }
            } catch (FileNotFoundException unused) {
            }
        } else {
            boolean z2 = false;
            if (this.A00.getPackageName().contains("instagram")) {
                z2 = true;
            }
            if (z2) {
                String A0J3 = AnonymousClass08S.A0J(str, "-lib-zstd");
                String str3 = File.separator;
                String A0T = AnonymousClass08S.A0T("assets", str3, str, str3, "libs.zstd");
                String A0T2 = AnonymousClass08S.A0T("assets", str3, str, str3, "libs.txt");
                cls2 = C002301n.class;
                synchronized (cls2) {
                    if (C002301n.A00 != null) {
                        if (!C002301n.A01.contains(A0J3)) {
                            AnonymousClass01q.A07(new AnonymousClass02N(C002301n.A00, file4, file3, A0T, A0T2, AnonymousClass07B.A01));
                            C002301n.A01.add(A0J3);
                        }
                        return;
                    }
                    runtimeException2 = new RuntimeException(AnonymousClass08S.A0J("FbSoLoader.init() was not called prior to loading additional asset ", A0J3));
                }
            } else {
                String A0J4 = AnonymousClass08S.A0J(str, "-lib-xzs");
                String str4 = File.separator;
                String A0T3 = AnonymousClass08S.A0T("assets", str4, str, str4, "libs.xzs");
                String A0T4 = AnonymousClass08S.A0T("assets", str4, str, str4, "libs.txt");
                cls2 = C002301n.class;
                synchronized (cls2) {
                    if (C002301n.A00 != null) {
                        if (!C002301n.A01.contains(A0J4)) {
                            AnonymousClass01q.A07(new AnonymousClass02N(C002301n.A00, file4, file3, A0T3, A0T4, AnonymousClass07B.A00));
                            C002301n.A01.add(A0J4);
                        }
                        return;
                    }
                    runtimeException2 = new RuntimeException(AnonymousClass08S.A0J("FbSoLoader.init() was not called prior to loading additional asset ", A0J4));
                }
            }
            throw runtimeException2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r14.exists() == false) goto L_0x000f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0097 A[SYNTHETIC, Splitter:B:35:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] A04(java.lang.String r12, java.lang.String r13, java.io.File r14) {
        /*
            r11 = this;
            java.util.LinkedList r5 = new java.util.LinkedList
            r5.<init>()
            r7 = 0
            if (r14 == 0) goto L_0x000f
            boolean r0 = r14.exists()
            r9 = 1
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            r9 = 0
        L_0x0010:
            android.content.Context r0 = r11.A00
            java.io.File r8 = X.AnonymousClass0GS.A00(r12, r0)
            if (r9 == 0) goto L_0x003d
            java.lang.String r3 = r14.getCanonicalPath()
        L_0x001c:
            com.facebook.quicklog.QuickPerformanceLogger r0 = r11.A00
            r4 = 11337729(0xad0001, float:1.5887542E-38)
            if (r0 == 0) goto L_0x0038
            int r2 = r12.hashCode()
            r0.markerStart(r4, r2)
            com.facebook.quicklog.QuickPerformanceLogger r1 = r11.A00
            java.lang.String r0 = "module"
            r1.markerAnnotate(r4, r2, r0, r12)
            com.facebook.quicklog.QuickPerformanceLogger r1 = r11.A00
            java.lang.String r0 = "path"
            r1.markerAnnotate(r4, r2, r0, r3)
        L_0x0038:
            r6 = 87
            if (r9 == 0) goto L_0x005f
            goto L_0x0057
        L_0x003d:
            android.content.Context r0 = r11.A00
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()
            java.lang.String r1 = r0.sourceDir
            java.lang.String r0 = r8.getCanonicalPath()
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0052
            java.lang.String r3 = "built-in"
            goto L_0x001c
        L_0x0052:
            java.lang.String r3 = r8.getCanonicalPath()
            goto L_0x001c
        L_0x0057:
            X.0Gb r3 = new X.0Gb     // Catch:{ all -> 0x00db }
            android.content.Context r0 = r11.A00     // Catch:{ all -> 0x00db }
            r3.<init>(r0, r12, r14)     // Catch:{ all -> 0x00db }
            goto L_0x0066
        L_0x005f:
            X.0Gb r3 = new X.0Gb     // Catch:{ all -> 0x00db }
            android.content.Context r0 = r11.A00     // Catch:{ all -> 0x00db }
            r3.<init>(r0, r12)     // Catch:{ all -> 0x00db }
        L_0x0066:
            java.lang.String r2 = "metadata.txt"
            java.lang.String r1 = r3.A01     // Catch:{ all -> 0x00db }
            java.lang.String r10 = java.io.File.separator     // Catch:{ all -> 0x00db }
            java.lang.String r9 = X.AnonymousClass08S.A0P(r1, r10, r2)     // Catch:{ all -> 0x00db }
            java.util.zip.ZipFile r2 = r3.A02     // Catch:{ all -> 0x00db }
            r0 = 0
            if (r2 == 0) goto L_0x0082
            java.lang.String r1 = "assets"
            java.lang.String r1 = X.AnonymousClass08S.A0P(r1, r10, r9)     // Catch:{ all -> 0x00db }
            java.util.zip.ZipEntry r1 = r2.getEntry(r1)     // Catch:{ all -> 0x00db }
            if (r1 == 0) goto L_0x0095
            goto L_0x0094
        L_0x0082:
            android.content.Context r1 = r3.A00     // Catch:{ IOException -> 0x0095 }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ IOException -> 0x0095 }
            java.io.InputStream r1 = r1.open(r9)     // Catch:{ IOException -> 0x0095 }
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0094
        L_0x0092:
            r0 = 1
            goto L_0x0095
        L_0x0094:
            r0 = 1
        L_0x0095:
            if (r0 == 0) goto L_0x00c2
            X.0Ep r0 = r11.A01     // Catch:{ all -> 0x00db }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x00db }
            java.io.File r1 = X.AnonymousClass0Ep.A00(r0, r12, r13)     // Catch:{ all -> 0x00db }
            java.lang.String r0 = "dex"
            r2.<init>(r1, r0)     // Catch:{ all -> 0x00db }
            com.facebook.common.dextricks.DexStore r0 = com.facebook.common.dextricks.DexStore.open(r2, r8, r3)     // Catch:{ all -> 0x00db }
            r0.loadManifest()     // Catch:{ all -> 0x00db }
            java.lang.String[] r3 = r0.getParentNames()     // Catch:{ all -> 0x00db }
            int r2 = r3.length     // Catch:{ all -> 0x00db }
        L_0x00b0:
            if (r7 >= r2) goto L_0x00c2
            r1 = r3[r7]     // Catch:{ all -> 0x00db }
            java.lang.String r0 = "dex"
            boolean r0 = r1.equals(r0)     // Catch:{ all -> 0x00db }
            if (r0 != 0) goto L_0x00bf
            r5.add(r1)     // Catch:{ all -> 0x00db }
        L_0x00bf:
            int r7 = r7 + 1
            goto L_0x00b0
        L_0x00c2:
            r2 = 2
            com.facebook.quicklog.QuickPerformanceLogger r1 = r11.A00
            if (r1 == 0) goto L_0x00ce
            int r0 = r12.hashCode()
            r1.markerEnd(r4, r0, r2)
        L_0x00ce:
            int r0 = r5.size()
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.Object[] r0 = r5.toArray(r0)
            java.lang.String[] r0 = (java.lang.String[]) r0
            return r0
        L_0x00db:
            r2 = move-exception
            com.facebook.quicklog.QuickPerformanceLogger r1 = r11.A00
            if (r1 == 0) goto L_0x00e7
            int r0 = r12.hashCode()
            r1.markerEnd(r4, r0, r6)
        L_0x00e7:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02390Em.A04(java.lang.String, java.lang.String, java.io.File):java.lang.String[]");
    }

    private C02390Em(Context context) {
        super(context, C02410Eo.A00(context));
    }

    public void A01(String str) {
        int hashCode = str.hashCode();
        QuickPerformanceLogger quickPerformanceLogger = this.A00;
        if (quickPerformanceLogger != null) {
            quickPerformanceLogger.markerStart(11337730, hashCode);
            this.A00.markerAnnotate(11337730, hashCode, "module", str);
        }
    }

    public void A02(String str, int i) {
        int hashCode = str.hashCode();
        QuickPerformanceLogger quickPerformanceLogger = this.A00;
        if (quickPerformanceLogger != null) {
            short s = 2;
            if (i == 1) {
                s = 25;
            } else if (i != 2) {
                s = 87;
            }
            quickPerformanceLogger.markerEnd(11337730, hashCode, s);
        }
    }
}
