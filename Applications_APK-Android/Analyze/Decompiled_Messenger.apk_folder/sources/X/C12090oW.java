package X;

import com.facebook.graphql.enums.GraphQLStoryAttachmentStyle;
import com.facebook.graphql.enums.GraphQLVideoBroadcastStatus;
import com.facebook.messaging.model.attachment.Attachment;
import com.facebook.messaging.model.messages.Message;
import com.facebook.ui.media.attachments.model.MediaResource;
import com.google.common.collect.ImmutableList;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0oW  reason: invalid class name and case insensitive filesystem */
public final class C12090oW {
    private static volatile C12090oW A01;
    public AnonymousClass0UN A00;

    public static final C12090oW A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C12090oW.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C12090oW(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static boolean A01(Message message) {
        C100354qi B46;
        C86874Cl A0Q;
        GraphQLVideoBroadcastStatus graphQLVideoBroadcastStatus;
        C61192yU r0 = message.A06;
        if (!(r0 == null || (B46 = r0.B46()) == null || (A0Q = B46.A0Q()) == null || !"Video".equals(A0Q.getTypeName()))) {
            ImmutableList B4T = B46.B4T();
            if (B4T.contains(GraphQLStoryAttachmentStyle.A18) || B4T.contains(GraphQLStoryAttachmentStyle.A4C) || B4T.contains(GraphQLStoryAttachmentStyle.A7L) || (((graphQLVideoBroadcastStatus = (GraphQLVideoBroadcastStatus) A0Q.A0O(-351684304, GraphQLVideoBroadcastStatus.A08)) != null && graphQLVideoBroadcastStatus != GraphQLVideoBroadcastStatus.A08 && graphQLVideoBroadcastStatus != GraphQLVideoBroadcastStatus.A09) || A0Q.A0P(752641086) == null || ((C101324sM) A0Q.A0J(-1967793586, C101324sM.class, 537206042)) == null || A0Q.getIntValue(113126854) <= 0 || A0Q.getIntValue(-1221029593) <= 0)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean A02(Message message) {
        boolean z;
        if ((message.A0X.size() != 1 || ((Attachment) message.A0X.get(0)).A04 == null) && (message.A04().size() != 1 || !AnonymousClass9QJ.A04((MediaResource) message.A04().get(0)))) {
            z = false;
        } else {
            z = true;
        }
        if (!z || !message.A0c.isEmpty() || message.A0T != null) {
            return false;
        }
        return true;
    }

    public boolean A03(Message message) {
        boolean z;
        C24971Xv it = message.A0X.iterator();
        while (true) {
            if (it.hasNext()) {
                if (!AnonymousClass18S.A03((Attachment) it.next())) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (message.A0X.size() == 0 || z) {
            if (!message.A04().isEmpty()) {
                C24971Xv it2 = message.A04().iterator();
                while (it2.hasNext()) {
                    if (!AnonymousClass9QJ.A03((MediaResource) it2.next())) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    private C12090oW(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public boolean A04(Message message) {
        if (A02(message) || A01(message)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        if (r0.A00 != X.C22652B5y.A03) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A05(X.C36871u1 r6) {
        /*
            r5 = this;
            X.2yU r0 = r6.BAB()
            r4 = 1
            r3 = 0
            if (r0 == 0) goto L_0x0021
            X.4qi r2 = r0.B46()
            if (r2 == 0) goto L_0x0021
            com.google.common.collect.ImmutableList r1 = r2.B4T()
            com.facebook.graphql.enums.GraphQLStoryAttachmentStyle r0 = com.facebook.graphql.enums.GraphQLStoryAttachmentStyle.A4t
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x0020
            boolean r0 = X.C28841fS.A0C(r2)
            if (r0 == 0) goto L_0x0021
        L_0x0020:
            return r3
        L_0x0021:
            com.facebook.messaging.model.share.SentShareAttachment r0 = r6.B2Y()
            if (r0 == 0) goto L_0x002e
            X.B5y r1 = r0.A00
            X.B5y r0 = X.C22652B5y.PAYMENT
            r2 = 1
            if (r1 == r0) goto L_0x002f
        L_0x002e:
            r2 = 0
        L_0x002f:
            int r1 = X.AnonymousClass1Y3.AcD
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A03(r1, r0)
            X.1YI r1 = (X.AnonymousClass1YI) r1
            if (r2 != 0) goto L_0x004f
            com.facebook.messaging.model.payment.PaymentTransactionData r0 = r6.Axg()
            if (r0 != 0) goto L_0x004f
            r0 = 753(0x2f1, float:1.055E-42)
            boolean r0 = r1.AbO(r0, r3)
            if (r0 == 0) goto L_0x0050
            com.facebook.messaging.model.payment.PaymentRequestData r0 = r6.Axe()
            if (r0 == 0) goto L_0x0050
        L_0x004f:
            return r4
        L_0x0050:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12090oW.A05(X.1u1):boolean");
    }
}
