package X;

import android.content.SharedPreferences;
import android.os.Bundle;
import com.facebook.rti.push.service.FbnsService;

/* renamed from: X.0Ns  reason: invalid class name and case insensitive filesystem */
public final class C03430Ns implements AnonymousClass0RR {
    public static final AnonymousClass0RR A01 = new C03430Ns(AnonymousClass07B.A01);
    public static final AnonymousClass0RR A02 = new C03430Ns(AnonymousClass07B.A06);
    public static final AnonymousClass0RR A03 = new C03430Ns(AnonymousClass07B.A0i);
    private static final String A04 = "SharedPreferencesBasedStateHelper";
    public final Integer A00;

    public Bundle AXc(FbnsService fbnsService, Bundle bundle) {
        SharedPreferences A002 = AnonymousClass0B2.A00(fbnsService, this.A00);
        Bundle bundle2 = new Bundle();
        for (String next : bundle.keySet()) {
            try {
                AnonymousClass0HA r0 = (AnonymousClass0HA) Enum.valueOf(AnonymousClass0HA.class, next);
                AnonymousClass0HB r3 = r0.mWrapper;
                try {
                    r3.A04(bundle2, r0.name(), r3.A01(A002, r0.mPrefKey, null));
                } catch (ClassCastException e) {
                    C010708t.A0L("KeyValueWrapper", "sharedPrefsToBundle got ClassCastException", e);
                }
            } catch (IllegalArgumentException e2) {
                C010708t.A0U(A04, e2, "aidlBundleKey: %s not exist in FbnsAIDLConstants", next);
            }
        }
        return bundle2;
    }

    public void AXk(FbnsService fbnsService, Bundle bundle) {
        SharedPreferences.Editor edit = AnonymousClass0B2.A00(fbnsService, this.A00).edit();
        for (String next : bundle.keySet()) {
            try {
                AnonymousClass0HA r0 = (AnonymousClass0HA) Enum.valueOf(AnonymousClass0HA.class, next);
                AnonymousClass0HB r3 = r0.mWrapper;
                String name = r0.name();
                try {
                    r3.A03(edit, r0.mPrefKey, r3.A02(bundle, name, null));
                } catch (ClassCastException e) {
                    C010708t.A0L("KeyValueWrapper", "bundleToSharedPrefs got ClassCastException", e);
                }
            } catch (IllegalArgumentException e2) {
                C010708t.A0U(A04, e2, "aidlBundleKey: %s not exist in FbnsAIDLConstants", next);
            }
        }
        AnonymousClass0B4.A00(edit);
    }

    private C03430Ns(Integer num) {
        this.A00 = num;
    }
}
