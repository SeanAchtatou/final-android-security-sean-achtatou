package X;

/* renamed from: X.0zi  reason: invalid class name and case insensitive filesystem */
public enum C17930zi {
    NONE(0),
    VOICE_CALL(1),
    VIDEO_CALL(2),
    RECENT_VOICE_CALL(3),
    RECENT_VIDEO_CALL(4);
    
    public static final C17930zi[] A00 = values();
    public int mValue;

    private C17930zi(int i) {
        this.mValue = i;
    }
}
