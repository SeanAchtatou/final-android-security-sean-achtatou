package X;

import com.facebook.inject.InjectorModule;
import java.security.SecureRandom;
import java.util.Random;

@InjectorModule
/* renamed from: X.0W9  reason: invalid class name */
public final class AnonymousClass0W9 extends AnonymousClass0UV {
    public static final Random A01() {
        return new Random();
    }

    public static final Random A02() {
        return new Random();
    }

    public static final SecureRandom A00() {
        return C30241hj.A00();
    }
}
