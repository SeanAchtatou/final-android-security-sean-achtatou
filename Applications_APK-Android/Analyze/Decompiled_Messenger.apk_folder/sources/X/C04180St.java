package X;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0St  reason: invalid class name and case insensitive filesystem */
public abstract class C04180St {
    public C04190Su A00;
    public AnonymousClass1WP A01;
    public AnonymousClass1WQ A02;

    public static boolean A01(Set set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                return set.size() == set2.size() && set.containsAll(set2);
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    public abstract int A02();

    public abstract int A03(Object obj);

    public abstract int A04(Object obj);

    public abstract Object A05(int i, int i2);

    public abstract Object A06(int i, Object obj);

    public abstract Map A07();

    public abstract void A08();

    public abstract void A09(int i);

    public abstract void A0A(Object obj, Object obj2);

    public static boolean A00(Map map, Collection collection) {
        int size = map.size();
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        if (size != map.size()) {
            return true;
        }
        return false;
    }

    public Object[] A0B(Object[] objArr, int i) {
        int A022 = A02();
        if (objArr.length < A022) {
            objArr = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), A022);
        }
        for (int i2 = 0; i2 < A022; i2++) {
            objArr[i2] = A05(i2, i);
        }
        if (objArr.length > A022) {
            objArr[A022] = null;
        }
        return objArr;
    }
}
