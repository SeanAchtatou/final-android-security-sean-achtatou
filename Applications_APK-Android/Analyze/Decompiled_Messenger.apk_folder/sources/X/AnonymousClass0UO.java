package X;

/* renamed from: X.0UO  reason: invalid class name */
public final class AnonymousClass0UO {
    private static final ThreadLocal A01 = new AnonymousClass0UP();
    public byte A00 = 0;

    private static String A01(byte b) {
        if (b == 1) {
            return "SingletonScope";
        }
        if (b == 4) {
            return "UserScope";
        }
        if (b == 8) {
            return "ContextScope";
        }
        throw new IllegalArgumentException(String.format("Invalid scope value %s", Integer.toBinaryString(b)));
    }

    public void A02(byte b, byte... bArr) {
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            byte b2 = bArr[i];
            boolean z = false;
            if ((b2 & this.A00) != 0) {
                z = true;
            }
            if (!z) {
                i++;
            } else {
                throw new C38471xU(AnonymousClass08S.A0S("Scope violation. Should not call inject ", A01(b), " into ", A01(b2)));
            }
        }
    }

    public static AnonymousClass0UO A00() {
        return (AnonymousClass0UO) A01.get();
    }
}
