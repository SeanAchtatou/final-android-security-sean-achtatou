package X;

import com.facebook.fbservice.service.OperationResult;

/* renamed from: X.1cy  reason: invalid class name and case insensitive filesystem */
public final class C27301cy implements C27311cz {
    private final AnonymousClass0mG A00;
    private final C27311cz A01;

    public OperationResult BAz(C11060ln r3) {
        return this.A00.BB0(r3, this.A01);
    }

    public C27301cy(AnonymousClass0mG r1, C27311cz r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
