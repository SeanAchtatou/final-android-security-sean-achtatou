package X;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1An  reason: invalid class name and case insensitive filesystem */
public final class C20001An {
    public ListenableFuture A00;
    public AnonymousClass0Y0 A01;
    public boolean A02;
    public final AnonymousClass0VK A03;
    public final Runnable A04;
    private final C06480bZ A05;

    public static void A00(C20001An r2) {
        if (r2.A00 == null) {
            AnonymousClass0Y0 r1 = r2.A01;
            if (r1 != null) {
                r1.cancel(false);
                r2.A01 = null;
            }
            r2.A00 = r2.A05.CIC(new C87884Gz(r2));
        }
    }

    public void A01() {
        if (!this.A02) {
            A00(this);
        } else if (this.A00 == null && this.A01 == null) {
            this.A01 = this.A03.C4Y(new C20261Bs(this), 5, TimeUnit.MINUTES);
        }
    }

    public C20001An(AnonymousClass1XY r2, Runnable runnable) {
        this.A03 = AnonymousClass0UX.A0R(r2);
        this.A05 = C06460bX.A00(r2);
        this.A04 = runnable;
    }
}
