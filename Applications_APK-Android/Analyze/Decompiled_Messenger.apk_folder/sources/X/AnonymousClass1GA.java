package X;

import android.content.Context;
import java.lang.ref.WeakReference;

/* renamed from: X.1GA  reason: invalid class name */
public final class AnonymousClass1GA extends AnonymousClass0p4 {
    public AnonymousClass10N A00;
    public AnonymousClass17T A01;
    public AnonymousClass1AC A02;
    public WeakReference A03;

    public C16070wR A0K() {
        return (C16070wR) this.A03.get();
    }

    public AnonymousClass1GA(AnonymousClass0p4 r5) {
        this(r5.A09, r5.A0B(), r5.A05(), r5.A08());
    }

    public AnonymousClass1GA(Context context) {
        this(context, null, null, null);
    }

    public AnonymousClass1GA(Context context, String str, C637038i r9, AnonymousClass1KE r10) {
        super(context, str, r9, null, null);
        this.A07 = r10;
        this.A01 = new AnonymousClass17T();
    }
}
