package X;

import android.os.Build;
import android.os.SystemClock;
import io.card.payment.BuildConfig;
import java.util.Map;

/* renamed from: X.1bU  reason: invalid class name and case insensitive filesystem */
public final class C26461bU {
    public int A00 = 2;
    public long A01;
    public long A02;
    public String A03 = BuildConfig.FLAVOR;
    public boolean A04 = false;
    public final int A05;
    public final int A06;
    public final long A07;
    public final long A08;
    public final C26471bV A09;
    public final boolean A0A;

    public void A00(int i) {
        long nanoTime;
        if (this.A04) {
            throw new RuntimeException(AnonymousClass08S.A0A("Event with action: ", i, " already ended"));
        } else if (i > 32767 || i < -32768) {
            throw new IllegalArgumentException("Action value must be between -32768 and 32767");
        } else {
            this.A00 = i;
            this.A04 = true;
            this.A01 = SystemClock.elapsedRealtime();
            if (Build.VERSION.SDK_INT >= 17) {
                nanoTime = SystemClock.elapsedRealtimeNanos();
            } else {
                nanoTime = System.nanoTime();
            }
            this.A02 = nanoTime;
        }
    }

    public void A01(String str) {
        this.A09.A02.put("event", str);
    }

    public void A02(String str, long j) {
        this.A09.A01.put(str, Long.valueOf(j));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Event--->");
        sb.append("\n\tStartTime: ");
        long j = this.A07;
        sb.append(j);
        sb.append("\n\tEndTime: ");
        long j2 = this.A01;
        sb.append(j2);
        sb.append("\n\tDuration(Millis): ");
        sb.append(j2 - j);
        sb.append("\n\tDuration(Micros): ");
        sb.append((this.A02 - this.A08) / 1000);
        sb.append("\n\tId: ");
        sb.append(this.A05);
        sb.append("\n\tUniqueKey: ");
        sb.append(this.A06);
        sb.append("\n\tAction: ");
        sb.append(this.A00);
        sb.append("\n\t- StringParams:");
        for (Map.Entry entry : this.A09.A02.entrySet()) {
            sb.append("\n\t\t");
            sb.append((String) entry.getKey());
            sb.append(": ");
            sb.append((String) entry.getValue());
        }
        sb.append("\n\t- DoubleParams:");
        for (Map.Entry entry2 : this.A09.A00.entrySet()) {
            sb.append("\n\t\t");
            sb.append((String) entry2.getKey());
            sb.append(": ");
            sb.append(entry2.getValue());
        }
        sb.append("\n\t- LongParams:");
        for (Map.Entry entry3 : this.A09.A01.entrySet()) {
            sb.append("\n\t\t");
            sb.append((String) entry3.getKey());
            sb.append(": ");
            sb.append(entry3.getValue());
        }
        return sb.toString();
    }

    public C26461bU(int i, Integer num, boolean z, C26471bV r6) {
        int intValue;
        long nanoTime;
        this.A09 = r6 == null ? new C26471bV() : r6;
        this.A05 = i;
        if (num == null) {
            intValue = System.identityHashCode(this);
        } else {
            intValue = num.intValue();
        }
        this.A06 = intValue;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.A01 = elapsedRealtime;
        this.A07 = elapsedRealtime;
        if (Build.VERSION.SDK_INT >= 17) {
            nanoTime = SystemClock.elapsedRealtimeNanos();
        } else {
            nanoTime = System.nanoTime();
        }
        this.A02 = nanoTime;
        this.A08 = nanoTime;
        this.A0A = z;
    }
}
