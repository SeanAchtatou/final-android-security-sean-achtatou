package X;

import java.util.Comparator;

/* renamed from: X.0Dv  reason: invalid class name and case insensitive filesystem */
public final class C02270Dv implements Comparator {
    public int compare(Object obj, Object obj2) {
        C02280Dw r7 = (C02280Dw) obj;
        C02280Dw r8 = (C02280Dw) obj2;
        long j = r7.A01 - r7.A02;
        long j2 = r8.A01 - r8.A02;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        return 0;
    }
}
