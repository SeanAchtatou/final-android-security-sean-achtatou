package X;

import com.facebook.messaging.model.share.Share;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0fm  reason: invalid class name and case insensitive filesystem */
public final class C08690fm {
    private final AnonymousClass0jD A00;
    private final C12470pQ A01 = new C12470pQ(new C11990oM());

    public static final C08690fm A00(AnonymousClass1XY r1) {
        return new C08690fm(r1);
    }

    public String A02(List list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayNode.add(this.A01.A02((Share) it.next()));
        }
        return arrayNode.toString();
    }

    public C08690fm(AnonymousClass1XY r3) {
        this.A00 = AnonymousClass0jD.A00(r3);
    }

    public ImmutableList A01(String str) {
        if (C06850cB.A0B(str)) {
            return RegularImmutableList.A02;
        }
        ImmutableList.Builder builder = ImmutableList.builder();
        Iterator it = this.A00.A02(str).iterator();
        while (it.hasNext()) {
            builder.add((Object) this.A01.A01((JsonNode) it.next()));
        }
        return builder.build();
    }
}
