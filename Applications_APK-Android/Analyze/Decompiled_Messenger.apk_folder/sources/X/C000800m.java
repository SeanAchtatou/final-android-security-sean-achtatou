package X;

import android.os.Build;
import android.os.Trace;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.00m  reason: invalid class name and case insensitive filesystem */
public final class C000800m {
    public static final long A00;
    public static final Method A01;
    public static final Method A02;
    public static volatile boolean A03 = true;

    public static Object A00(Method method, Object... objArr) {
        try {
            return method.invoke(null, objArr);
        } catch (IllegalAccessException unused) {
            A03 = false;
            return null;
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            } else if (!(targetException instanceof Error)) {
                return null;
            } else {
                throw ((Error) targetException);
            }
        }
    }

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 18) {
            z = true;
        }
        C000200g r5 = null;
        if (z) {
            Class<Trace> cls = Trace.class;
            try {
                Method method = cls.getMethod("isTagEnabled", Long.TYPE);
                Method method2 = cls.getMethod("setAppTracingAllowed", Boolean.TYPE);
                Field field = cls.getField("TRACE_TAG_APP");
                if (field.getType() == Long.TYPE) {
                    r5 = new C000200g(method, method2, field.getLong(null));
                }
            } catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException unused) {
            }
        }
        if (r5 != null) {
            A01 = r5.A01;
            A02 = r5.A02;
            A00 = r5.A00;
        }
    }
}
