package X;

import android.graphics.Bitmap;
import android.os.Build;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1RQ  reason: invalid class name */
public abstract class AnonymousClass1RQ extends AnonymousClass1RO {
    public boolean A00 = false;
    public final C23541Px A01;
    public final AnonymousClass1QK A02;
    public final AnonymousClass1RU A03;
    public final AnonymousClass1MN A04;
    public final /* synthetic */ AnonymousClass1QC A05;

    public static void A01(AnonymousClass1RQ r2, boolean z) {
        synchronized (r2) {
            if (z) {
                if (!r2.A00) {
                    r2.A00.BkJ(1.0f);
                    r2.A00 = true;
                    r2.A03.A03();
                }
            }
        }
    }

    public int A08(AnonymousClass1NY r2) {
        return ((AnonymousClass1RR) this).A02.A00;
    }

    public C33271nJ A09() {
        AnonymousClass1RR r0 = (AnonymousClass1RR) this;
        return r0.A01.Azp(r0.A02.A01);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass1RQ(AnonymousClass1QC r6, C23581Qb r7, AnonymousClass1QK r8, boolean z, int i) {
        super(r7);
        this.A05 = r6;
        this.A02 = r8;
        this.A04 = r8.A07;
        C23541Px r4 = r8.A09.A04;
        this.A01 = r4;
        this.A03 = new AnonymousClass1RU(r6.A02, new AnonymousClass1RS(this, r8, i), r4.A01);
        r8.A06(new AnonymousClass1SO(this, z));
    }

    public static Map A00(AnonymousClass1RQ r14, AnonymousClass1S9 r15, long j, C33271nJ r18, boolean z, String str, String str2, String str3, String str4) {
        HashMap hashMap;
        if (!r14.A04.C3Q(r14.A02, "DecodeProducer")) {
            return null;
        }
        String valueOf = String.valueOf(j);
        String valueOf2 = String.valueOf(r18.A02);
        String valueOf3 = String.valueOf(z);
        String str5 = str;
        String str6 = str2;
        String str7 = str4;
        String str8 = str3;
        if (r15 instanceof AnonymousClass1S5) {
            Bitmap A042 = ((AnonymousClass1S5) r15).A04();
            String A022 = AnonymousClass08S.A02(A042.getWidth(), "x", A042.getHeight());
            hashMap = new HashMap(8);
            hashMap.put("bitmapSize", A022);
            hashMap.put("queueTime", valueOf);
            hashMap.put("hasGoodQuality", valueOf2);
            hashMap.put("isFinal", valueOf3);
            hashMap.put("encodedImageSize", str6);
            hashMap.put("imageFormat", str5);
            hashMap.put("requestedImageSize", str8);
            hashMap.put("sampleSize", str7);
            if (Build.VERSION.SDK_INT >= 12) {
                StringBuilder sb = new StringBuilder();
                sb.append(A042.getByteCount());
                hashMap.put("byteCount", sb.toString());
            }
        } else {
            hashMap = new HashMap(7);
            hashMap.put("queueTime", valueOf);
            hashMap.put("hasGoodQuality", valueOf2);
            hashMap.put("isFinal", valueOf3);
            hashMap.put("encodedImageSize", str6);
            hashMap.put("imageFormat", str5);
            hashMap.put("requestedImageSize", str8);
            hashMap.put("sampleSize", str7);
        }
        return C22665B6t.A00(hashMap);
    }

    public boolean A0A(AnonymousClass1NY r2, int i) {
        return this.A03.A05(r2, i);
    }

    public void A05(float f) {
        super.A05(f * 0.99f);
    }
}
