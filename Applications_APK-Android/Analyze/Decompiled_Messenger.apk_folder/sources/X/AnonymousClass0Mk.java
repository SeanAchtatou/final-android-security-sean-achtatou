package X;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.Writer;

/* renamed from: X.0Mk  reason: invalid class name */
public final class AnonymousClass0Mk implements AnonymousClass01J {
    private int A00 = -1;

    public static int A00(Context context) {
        String[] strArr;
        if (Build.VERSION.SDK_INT < 21) {
            return -1;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo == null || (strArr = packageInfo.splitNames) == null) {
                return 0;
            }
            return strArr.length;
        } catch (PackageManager.NameNotFoundException unused) {
            return -1;
        }
    }

    public boolean ANg(Writer writer, AnonymousClass0HM r4) {
        if (Build.VERSION.SDK_INT < 21) {
            return false;
        }
        writer.append((CharSequence) "\"installedSplits\":");
        writer.append((CharSequence) Integer.toString(this.A00));
        return true;
    }

    public AnonymousClass0Mk(Context context) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.A00 = A00(context);
        }
    }
}
