package X;

import android.content.Intent;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.messaging.model.threads.ThreadUpdate;
import com.facebook.messaging.service.model.FetchThreadResult;
import com.facebook.messaging.service.model.MarkThreadFields;
import com.facebook.messaging.service.model.NewMessageResult;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0u9  reason: invalid class name and case insensitive filesystem */
public final class C14800u9 implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.cache.handlers.CacheInsertThreadsHandler";
    public final C189216c A00;
    public final AnonymousClass0m6 A01;
    public final C14300t0 A02;
    private final C05920aY A03;
    private final C17270yd A04;
    private final C28841fS A05;
    private final C13760s1 A06;
    private final AnonymousClass0r6 A07;
    private final C04310Tq A08;

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0093, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0094, code lost:
        if (r8 != null) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0099, code lost:
        throw r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0081 A[Catch:{ all -> 0x0093 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C12300p2 A00(com.facebook.messaging.model.threadkey.ThreadKey r10) {
        /*
            r9 = this;
            X.0m6 r7 = r9.A01
            X.0mL r0 = r7.A0A
            X.0mM r8 = r0.A00()
            com.facebook.messaging.model.threads.ThreadSummary r4 = X.AnonymousClass0m6.A02(r7, r10)     // Catch:{ all -> 0x0091 }
            r5 = 0
            if (r4 == 0) goto L_0x0089
            com.facebook.messaging.model.threads.MontageThreadPreview r3 = r4.A0X     // Catch:{ all -> 0x0091 }
            if (r3 == 0) goto L_0x0089
            boolean r0 = r3.A02()     // Catch:{ all -> 0x0091 }
            if (r0 == 0) goto L_0x0089
            X.0zh r2 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x0091 }
            r2.A02(r4)     // Catch:{ all -> 0x0091 }
            X.2VF r1 = new X.2VF     // Catch:{ all -> 0x0091 }
            r1.<init>(r3)     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = new com.facebook.messaging.model.threads.MontageThreadPreview     // Catch:{ all -> 0x0091 }
            r0.<init>(r1)     // Catch:{ all -> 0x0091 }
            r2.A0W = r0     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.ThreadSummary r2 = r2.A00()     // Catch:{ all -> 0x0091 }
            r7.A0b(r2)     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r4.A0X     // Catch:{ all -> 0x0091 }
            boolean r1 = r0.A02()     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r2.A0X     // Catch:{ all -> 0x0091 }
            boolean r0 = r0.A02()     // Catch:{ all -> 0x0091 }
            if (r1 == r0) goto L_0x0089
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r4.A0S     // Catch:{ all -> 0x0091 }
        L_0x0043:
            com.facebook.messaging.model.threads.ThreadSummary r6 = r7.A0R(r10)     // Catch:{ all -> 0x0091 }
            if (r6 == 0) goto L_0x0083
            X.0l8 r1 = r6.A0O     // Catch:{ all -> 0x0091 }
            X.0l8 r0 = X.C10950l8.A06     // Catch:{ all -> 0x0091 }
            if (r1 != r0) goto L_0x0083
            com.facebook.messaging.model.threads.MontageThreadPreview r3 = r6.A0X     // Catch:{ all -> 0x0091 }
            if (r3 == 0) goto L_0x0083
            boolean r0 = r3.A02()     // Catch:{ all -> 0x0091 }
            if (r0 == 0) goto L_0x0083
            X.0zh r2 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x0091 }
            r2.A02(r6)     // Catch:{ all -> 0x0091 }
            X.2VF r1 = new X.2VF     // Catch:{ all -> 0x0091 }
            r1.<init>(r3)     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = new com.facebook.messaging.model.threads.MontageThreadPreview     // Catch:{ all -> 0x0091 }
            r0.<init>(r1)     // Catch:{ all -> 0x0091 }
            r2.A0W = r0     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.ThreadSummary r2 = r2.A00()     // Catch:{ all -> 0x0091 }
            r7.A0b(r2)     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r2.A0X     // Catch:{ all -> 0x0091 }
            boolean r1 = r0.A02()     // Catch:{ all -> 0x0091 }
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = r6.A0X     // Catch:{ all -> 0x0091 }
            boolean r0 = r0.A02()     // Catch:{ all -> 0x0091 }
            if (r1 == r0) goto L_0x0083
            com.facebook.messaging.model.threadkey.ThreadKey r5 = r2.A0S     // Catch:{ all -> 0x0091 }
        L_0x0083:
            X.0p2 r0 = new X.0p2     // Catch:{ all -> 0x0091 }
            r0.<init>(r5, r4)     // Catch:{ all -> 0x0091 }
            goto L_0x008b
        L_0x0089:
            r4 = r5
            goto L_0x0043
        L_0x008b:
            if (r8 == 0) goto L_0x0090
            r8.close()
        L_0x0090:
            return r0
        L_0x0091:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0093 }
        L_0x0093:
            r0 = move-exception
            if (r8 == 0) goto L_0x0099
            r8.close()     // Catch:{ all -> 0x0099 }
        L_0x0099:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A00(com.facebook.messaging.model.threadkey.ThreadKey):X.0p2");
    }

    public Set A01(ImmutableMap immutableMap) {
        HashSet hashSet = new HashSet(immutableMap.size());
        C24971Xv it = immutableMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            entry.getValue();
            C12300p2 A002 = A00((ThreadKey) entry.getKey());
            Object obj = A002.A00;
            if (obj != null) {
                hashSet.add(obj);
            }
            Object obj2 = A002.A01;
            if (obj2 != null) {
                hashSet.add(obj2);
            }
        }
        return hashSet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d6, code lost:
        if (r1 == false) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x014f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0150, code lost:
        if (r12 != null) goto L_0x0152;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0155, code lost:
        throw r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00dd A[Catch:{ all -> 0x014f }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x010f A[Catch:{ all -> 0x014f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(int r14, com.facebook.messaging.model.threads.ThreadUpdate r15, boolean r16) {
        /*
            r13 = this;
            com.facebook.messaging.model.threads.ThreadSummary r4 = r15.A02
            com.google.common.collect.ImmutableList r1 = r15.A03
            if (r1 == 0) goto L_0x000b
            X.0t0 r0 = r13.A02
            r0.A06(r1)
        L_0x000b:
            if (r4 == 0) goto L_0x0172
            X.0m6 r0 = r13.A01
            r0.A0b(r4)
            if (r14 <= 0) goto L_0x001e
            X.0m6 r3 = r13.A01
            com.facebook.messaging.model.messages.MessagesCollection r2 = r15.A01
            X.0mO r1 = r3.A06
            r0 = 0
            X.AnonymousClass0m6.A09(r3, r2, r1, r0)
        L_0x001e:
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r4.A0S
            X.1fF r1 = r0.A05
            X.1fF r0 = X.C28711fF.ONE_TO_ONE
            if (r1 != r0) goto L_0x005a
            com.facebook.messaging.model.messages.MessagesCollection r0 = r15.A01
            com.google.common.collect.ImmutableList r0 = r0.A01
            X.1Xv r6 = r0.iterator()
        L_0x002e:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x005a
            java.lang.Object r5 = r6.next()
            com.facebook.messaging.model.messages.Message r5 = (com.facebook.messaging.model.messages.Message) r5
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r5.A0K
            com.facebook.user.model.UserKey r2 = r0.A01
            X.0aY r0 = r13.A03
            com.facebook.auth.viewercontext.ViewerContext r0 = r0.B9R()
            java.lang.String r1 = r0.mUserId
            com.facebook.user.model.UserKey r3 = new com.facebook.user.model.UserKey
            X.1aB r0 = X.C25651aB.A03
            r3.<init>(r0, r1)
            boolean r0 = com.google.common.base.Objects.equal(r2, r3)
            if (r0 != 0) goto L_0x002e
            X.0t0 r2 = r13.A02
            long r0 = r5.A03
            r2.A05(r3, r0)
        L_0x005a:
            com.facebook.messaging.model.threads.ThreadSummary r0 = r15.A02
            if (r0 == 0) goto L_0x015b
            X.0l8 r1 = r0.A0O
            X.0l8 r0 = X.C10950l8.A06
            if (r1 != r0) goto L_0x015b
            X.0m6 r5 = r13.A01
            X.0mL r0 = r5.A0A
            X.0mM r12 = r0.A00()
            com.facebook.messaging.model.threads.ThreadSummary r7 = r15.A02     // Catch:{ all -> 0x014d }
            r8 = 0
            if (r7 == 0) goto L_0x0156
            X.0l8 r1 = r7.A0O     // Catch:{ all -> 0x014d }
            X.0l8 r0 = X.C10950l8.A06     // Catch:{ all -> 0x014d }
            if (r1 != r0) goto L_0x0156
            X.0Tq r0 = r5.A0F     // Catch:{ all -> 0x014d }
            java.lang.Object r9 = r0.get()     // Catch:{ all -> 0x014d }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x014d }
            r6 = 0
            if (r9 == 0) goto L_0x0121
            com.google.common.collect.ImmutableList r0 = r7.A0m     // Catch:{ all -> 0x014d }
            int r1 = r0.size()     // Catch:{ all -> 0x014d }
            r0 = 2
            if (r1 != r0) goto L_0x0121
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r7.A0Q     // Catch:{ all -> 0x014d }
            if (r0 == 0) goto L_0x009b
            java.lang.String r0 = r0.A00()     // Catch:{ all -> 0x014d }
            boolean r0 = r9.equals(r0)     // Catch:{ all -> 0x014d }
            if (r0 == 0) goto L_0x009b
            goto L_0x0121
        L_0x009b:
            com.google.common.collect.ImmutableList r1 = r7.A0m     // Catch:{ all -> 0x014d }
            r0 = 0
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x014d }
            com.facebook.messaging.model.threads.ThreadParticipant r0 = (com.facebook.messaging.model.threads.ThreadParticipant) r0     // Catch:{ all -> 0x014d }
            java.lang.String r10 = r0.A01()     // Catch:{ all -> 0x014d }
            com.google.common.collect.ImmutableList r1 = r7.A0m     // Catch:{ all -> 0x014d }
            r0 = 1
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x014d }
            com.facebook.messaging.model.threads.ThreadParticipant r0 = (com.facebook.messaging.model.threads.ThreadParticipant) r0     // Catch:{ all -> 0x014d }
            java.lang.String r11 = r0.A01()     // Catch:{ all -> 0x014d }
            X.0Tq r0 = r5.A0F     // Catch:{ all -> 0x014d }
            java.lang.Object r3 = r0.get()     // Catch:{ all -> 0x014d }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x014d }
            if (r3 == 0) goto L_0x00d8
            if (r10 == 0) goto L_0x00d8
            if (r11 == 0) goto L_0x00d8
            boolean r1 = r3.equals(r10)     // Catch:{ all -> 0x014d }
            if (r1 == 0) goto L_0x00d0
            boolean r0 = r3.equals(r11)     // Catch:{ all -> 0x014d }
            if (r0 != 0) goto L_0x00d0
            goto L_0x00da
        L_0x00d0:
            boolean r0 = r3.equals(r11)     // Catch:{ all -> 0x014d }
            if (r0 == 0) goto L_0x00d8
            if (r1 == 0) goto L_0x00db
        L_0x00d8:
            r10 = r8
            goto L_0x00db
        L_0x00da:
            r10 = r11
        L_0x00db:
            if (r10 != 0) goto L_0x010f
            r2 = 4
            int r1 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x014d }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x014d }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x014d }
            X.09P r2 = (X.AnonymousClass09P) r2     // Catch:{ all -> 0x014d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x014d }
            r1.<init>()     // Catch:{ all -> 0x014d }
            java.lang.String r0 = "Got a null other user for thread key "
            r1.append(r0)     // Catch:{ all -> 0x014d }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0S     // Catch:{ all -> 0x014d }
            r1.append(r0)     // Catch:{ all -> 0x014d }
            java.lang.String r0 = " with participants "
            r1.append(r0)     // Catch:{ all -> 0x014d }
            com.google.common.collect.ImmutableList r0 = r7.A0m     // Catch:{ all -> 0x014d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x014d }
            r1.append(r0)     // Catch:{ all -> 0x014d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x014d }
            java.lang.String r0 = "montage-null-other-user-id"
            r2.CGS(r0, r1)     // Catch:{ all -> 0x014d }
            goto L_0x0121
        L_0x010f:
            long r2 = java.lang.Long.parseLong(r10)     // Catch:{ all -> 0x014d }
            long r0 = java.lang.Long.parseLong(r9)     // Catch:{ all -> 0x014d }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A04(r2, r0)     // Catch:{ all -> 0x014d }
            if (r0 == 0) goto L_0x0121
            com.facebook.messaging.model.threads.ThreadSummary r6 = r5.A0R(r0)     // Catch:{ all -> 0x014d }
        L_0x0121:
            if (r6 == 0) goto L_0x0156
            com.facebook.messaging.model.messages.MessagesCollection r3 = r15.A01     // Catch:{ all -> 0x014d }
            if (r3 == 0) goto L_0x0138
            r2 = 0
            int r1 = X.AnonymousClass1Y3.A8n     // Catch:{ all -> 0x014d }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x014d }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x014d }
            X.1TU r1 = (X.AnonymousClass1TU) r1     // Catch:{ all -> 0x014d }
            com.google.common.collect.ImmutableList r0 = r3.A01     // Catch:{ all -> 0x014d }
            com.facebook.messaging.model.threads.MontageThreadPreview r8 = r1.A0F(r0)     // Catch:{ all -> 0x014d }
        L_0x0138:
            X.0zh r1 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x014d }
            r1.A02(r6)     // Catch:{ all -> 0x014d }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r7.A0S     // Catch:{ all -> 0x014d }
            r1.A0Q = r0     // Catch:{ all -> 0x014d }
            r1.A0W = r8     // Catch:{ all -> 0x014d }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r1.A00()     // Catch:{ all -> 0x014d }
            r5.A0b(r0)     // Catch:{ all -> 0x014d }
            goto L_0x0156
        L_0x014d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x014f }
        L_0x014f:
            r0 = move-exception
            if (r12 == 0) goto L_0x0155
            r12.close()     // Catch:{ all -> 0x0155 }
        L_0x0155:
            throw r0
        L_0x0156:
            if (r12 == 0) goto L_0x015b
            r12.close()
        L_0x015b:
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            if (r16 == 0) goto L_0x0169
            X.2yZ r1 = X.C61242yZ.A01
            java.lang.String r0 = "broadcast_cause"
            r3.putSerializable(r0, r1)
        L_0x0169:
            X.16c r2 = r13.A00
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A0S
            java.lang.String r0 = "CacheInsertThreadsHandler"
            r2.A0D(r1, r3, r0)
        L_0x0172:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A02(int, com.facebook.messaging.model.threads.ThreadUpdate, boolean):void");
    }

    public void A03(int i, FetchThreadResult fetchThreadResult) {
        A02(i, new ThreadUpdate(fetchThreadResult.A05, fetchThreadResult.A03, fetchThreadResult.A07, fetchThreadResult.A01), true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0043, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        if (r5 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0049, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.C10950l8 r7) {
        /*
            r6 = this;
            X.0m6 r3 = r6.A01
            X.0mL r0 = r3.A0A
            X.0mM r5 = r0.A00()
            X.0pa r2 = X.AnonymousClass0m6.A00(r3, r7)     // Catch:{ all -> 0x0041 }
            r1 = 0
            X.0mL r0 = r2.A06     // Catch:{ all -> 0x0041 }
            r0.A01()     // Catch:{ all -> 0x0041 }
            r2.A04 = r1     // Catch:{ all -> 0x0041 }
            X.0m9 r4 = r3.A0B     // Catch:{ all -> 0x0041 }
            X.0l8 r3 = r2.A07     // Catch:{ all -> 0x0041 }
            monitor-enter(r4)     // Catch:{ all -> 0x0041 }
            boolean r0 = r4.A0F()     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x0037
            X.0l8 r1 = X.C10950l8.A05     // Catch:{ all -> 0x003e }
            r0 = 0
            if (r3 != r1) goto L_0x0025
            r0 = 1
        L_0x0025:
            if (r0 == 0) goto L_0x0037
            java.lang.String r2 = "markThreadListStaleForFolderInCache"
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x003e }
            r0 = 0
            X.3bx r1 = X.AnonymousClass0m9.A01(r4, r0, r0, r2, r1)     // Catch:{ all -> 0x003e }
            android.util.LruCache r0 = r4.A01     // Catch:{ all -> 0x003e }
            r0.put(r1, r1)     // Catch:{ all -> 0x003e }
        L_0x0037:
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            if (r5 == 0) goto L_0x003d
            r5.close()
        L_0x003d:
            return
        L_0x003e:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            throw r0     // Catch:{ all -> 0x0041 }
        L_0x0041:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r0 = move-exception
            if (r5 == 0) goto L_0x0049
            r5.close()     // Catch:{ all -> 0x0049 }
        L_0x0049:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A04(X.0l8):void");
    }

    public void A05(C10950l8 r5, FolderCounts folderCounts) {
        this.A01.A0W(r5, folderCounts);
        C189216c r3 = this.A00;
        Intent intent = new Intent(C06680bu.A0C);
        intent.putExtra("folder_name", r5.toString());
        C189216c.A04(r3, intent);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0057, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0058, code lost:
        if (r2 != null) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005d, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.C10950l8 r8, com.facebook.messaging.service.model.DeleteMessagesResult r9) {
        /*
            r7 = this;
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r9.A00
            if (r4 == 0) goto L_0x005e
            X.0m6 r0 = r7.A01
            com.facebook.messaging.model.messages.MessagesCollection r0 = r0.A0P(r4)
            com.google.common.collect.ImmutableSet r5 = r9.A03
            X.16c r6 = r7.A00
            com.google.common.collect.ImmutableSet r3 = r9.A04
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r0 = "MEDIA_UPLOAD_CACHE_PURGE"
            r2.<init>(r0)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r3)
            java.lang.String r0 = "media_fbids"
            r2.putStringArrayListExtra(r0, r1)
            X.C189216c.A04(r6, r2)
            X.0m6 r3 = r7.A01
            X.0mL r0 = r3.A0A
            X.0mM r2 = r0.A00()
            com.facebook.messaging.model.messages.MessagesCollection r1 = r3.A0P(r4)     // Catch:{ all -> 0x0055 }
            X.0mO r0 = r3.A06     // Catch:{ all -> 0x0055 }
            X.AnonymousClass0m6.A0C(r3, r4, r5, r1, r0)     // Catch:{ all -> 0x0055 }
            com.facebook.messaging.model.messages.MessagesCollection r1 = r3.A0Q(r4)     // Catch:{ all -> 0x0055 }
            X.0mO r0 = r3.A07     // Catch:{ all -> 0x0055 }
            X.AnonymousClass0m6.A0C(r3, r4, r5, r1, r0)     // Catch:{ all -> 0x0055 }
            if (r2 == 0) goto L_0x0043
            r2.close()
        L_0x0043:
            boolean r0 = r9.A05
            if (r0 == 0) goto L_0x004b
            r7.A06(r8, r4)
            return
        L_0x004b:
            com.facebook.messaging.model.threads.ThreadSummary r1 = r9.A01
            if (r1 == 0) goto L_0x005e
            X.0m6 r0 = r7.A01
            r0.A0b(r1)
            return
        L_0x0055:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0057 }
        L_0x0057:
            r0 = move-exception
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ all -> 0x005d }
        L_0x005d:
            throw r0
        L_0x005e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A07(X.0l8, com.facebook.messaging.service.model.DeleteMessagesResult):void");
    }

    public void A08(C10950l8 r3, ImmutableList immutableList) {
        this.A01.A0X(r3, immutableList);
        if (!immutableList.isEmpty()) {
            this.A00.A0L(immutableList, "CacheInsertThreadsHandler");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004e, code lost:
        if (r3 != null) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0053, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(com.facebook.messaging.model.messages.Message r13) {
        /*
            r12 = this;
            X.0m6 r4 = r12.A01
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            r5 = r13
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r13.A0U     // Catch:{ all -> 0x004b }
            X.AnonymousClass0m6.A0A(r4, r0)     // Catch:{ all -> 0x004b }
            X.0mO r2 = r4.A06     // Catch:{ all -> 0x004b }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r13.A0U     // Catch:{ all -> 0x004b }
            java.lang.String r0 = r13.A0w     // Catch:{ all -> 0x004b }
            com.facebook.messaging.model.messages.Message r0 = r2.A00(r1, r0)     // Catch:{ all -> 0x004b }
            boolean r0 = X.C57972sw.A04(r13, r0)     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0045
            r6 = 0
            java.lang.Integer r9 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x004b }
            X.3dk r10 = X.C72153dk.A02     // Catch:{ all -> 0x004b }
            r0 = 0
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x004b }
            r7 = -1
            X.AnonymousClass0m6.A07(r4, r5, r6, r7, r9, r10, r11)     // Catch:{ all -> 0x004b }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r13.A0U     // Catch:{ all -> 0x004b }
            com.facebook.messaging.model.threads.ThreadSummary r1 = r4.A0R(r0)     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x0045
            X.0zh r0 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x004b }
            r0.A02(r1)     // Catch:{ all -> 0x004b }
            r0.A0O = r6     // Catch:{ all -> 0x004b }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A00()     // Catch:{ all -> 0x004b }
            r4.A0b(r0)     // Catch:{ all -> 0x004b }
        L_0x0045:
            if (r3 == 0) goto L_0x004a
            r3.close()
        L_0x004a:
            return
        L_0x004b:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004d }
        L_0x004d:
            r0 = move-exception
            if (r3 == 0) goto L_0x0053
            r3.close()     // Catch:{ all -> 0x0053 }
        L_0x0053:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A09(com.facebook.messaging.model.messages.Message):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0087, code lost:
        if (r9 != null) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x008c, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(com.facebook.messaging.model.messages.MessagesCollection r11) {
        /*
            r10 = this;
            X.0m6 r6 = r10.A01
            X.0mO r8 = r6.A07
            X.0mL r0 = r6.A0A
            X.0mM r9 = r0.A00()
            com.facebook.messaging.model.threadkey.ThreadKey r5 = r11.A00     // Catch:{ all -> 0x0084 }
            X.AnonymousClass0m6.A0A(r6, r5)     // Catch:{ all -> 0x0084 }
            com.facebook.messaging.model.messages.MessagesCollection r2 = r8.A02(r5)     // Catch:{ all -> 0x0084 }
            if (r2 == 0) goto L_0x007e
            boolean r0 = r2.A08()     // Catch:{ all -> 0x0084 }
            if (r0 != 0) goto L_0x007e
            X.0mB r1 = r6.A0C     // Catch:{ all -> 0x0084 }
            r0 = 1
            com.facebook.messaging.model.messages.MessagesCollection r4 = X.AnonymousClass0mB.A00(r1, r2, r11, r0)     // Catch:{ all -> 0x0084 }
            int r1 = r2.A04()     // Catch:{ all -> 0x0084 }
            int r0 = r4.A04()     // Catch:{ all -> 0x0084 }
            if (r1 == r0) goto L_0x007e
            X.0mL r0 = r8.A02     // Catch:{ all -> 0x0084 }
            r0.A01()     // Catch:{ all -> 0x0084 }
            com.facebook.messaging.model.threadkey.ThreadKey r7 = r11.A00     // Catch:{ all -> 0x0084 }
            if (r7 == 0) goto L_0x006b
            com.google.common.collect.ImmutableList r0 = r11.A01     // Catch:{ all -> 0x0084 }
            X.1Xv r3 = r0.iterator()     // Catch:{ all -> 0x0084 }
        L_0x003b:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x0084 }
            if (r0 == 0) goto L_0x0059
            java.lang.Object r2 = r3.next()     // Catch:{ all -> 0x0084 }
            com.facebook.messaging.model.messages.Message r2 = (com.facebook.messaging.model.messages.Message) r2     // Catch:{ all -> 0x0084 }
            X.04b r1 = r8.A00     // Catch:{ all -> 0x0084 }
            java.lang.String r0 = r2.A0q     // Catch:{ all -> 0x0084 }
            boolean r0 = r1.containsKey(r0)     // Catch:{ all -> 0x0084 }
            if (r0 != 0) goto L_0x003b
            X.04b r1 = r8.A00     // Catch:{ all -> 0x0084 }
            java.lang.String r0 = r2.A0q     // Catch:{ all -> 0x0084 }
            r1.put(r0, r2)     // Catch:{ all -> 0x0084 }
            goto L_0x003b
        L_0x0059:
            X.04b r0 = r8.A01     // Catch:{ all -> 0x0084 }
            r0.put(r7, r4)     // Catch:{ all -> 0x0084 }
            X.0m9 r1 = r8.A03     // Catch:{ all -> 0x0084 }
            monitor-enter(r1)     // Catch:{ all -> 0x0084 }
            java.lang.String r0 = "addMessagesIntoCache"
            X.AnonymousClass0m9.A04(r1, r4, r0)     // Catch:{ all -> 0x0067 }
            goto L_0x006a
        L_0x0067:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0084 }
            throw r0     // Catch:{ all -> 0x0084 }
        L_0x006a:
            monitor-exit(r1)     // Catch:{ all -> 0x0084 }
        L_0x006b:
            X.0mN r0 = r6.A08     // Catch:{ all -> 0x0084 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r0.A01(r5)     // Catch:{ all -> 0x0084 }
            if (r0 != 0) goto L_0x007a
            X.AnonymousClass0m6.A05(r6)     // Catch:{ all -> 0x0084 }
        L_0x0076:
            X.AnonymousClass0m6.A08(r6, r11)     // Catch:{ all -> 0x0084 }
            goto L_0x007e
        L_0x007a:
            X.AnonymousClass0m6.A03(r6, r0, r11)     // Catch:{ all -> 0x0084 }
            goto L_0x0076
        L_0x007e:
            if (r9 == 0) goto L_0x0083
            r9.close()
        L_0x0083:
            return
        L_0x0084:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0086 }
        L_0x0086:
            r0 = move-exception
            if (r9 == 0) goto L_0x008c
            r9.close()     // Catch:{ all -> 0x008c }
        L_0x008c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A0A(com.facebook.messaging.model.messages.MessagesCollection):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        if (r3 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0B(com.facebook.messaging.model.threadkey.ThreadKey r6) {
        /*
            r5 = this;
            X.0m6 r4 = r5.A01
            if (r6 == 0) goto L_0x0036
            X.AnonymousClass0m6.A0A(r4, r6)
            X.0mL r0 = r4.A0A
            X.0mM r3 = r0.A00()
            X.AnonymousClass0m6.A0B(r4, r6)     // Catch:{ all -> 0x002d }
            r2 = 0
            X.04b r0 = r4.A01     // Catch:{ all -> 0x002d }
            int r1 = r0.size()     // Catch:{ all -> 0x002d }
        L_0x0017:
            if (r2 >= r1) goto L_0x0027
            X.04b r0 = r4.A01     // Catch:{ all -> 0x002d }
            java.lang.Object r0 = r0.A07(r2)     // Catch:{ all -> 0x002d }
            X.0l8 r0 = (X.C10950l8) r0     // Catch:{ all -> 0x002d }
            X.AnonymousClass0m6.A06(r4, r0, r6)     // Catch:{ all -> 0x002d }
            int r2 = r2 + 1
            goto L_0x0017
        L_0x0027:
            if (r3 == 0) goto L_0x0036
            r3.close()
            return
        L_0x002d:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002f }
        L_0x002f:
            r0 = move-exception
            if (r3 == 0) goto L_0x0035
            r3.close()     // Catch:{ all -> 0x0035 }
        L_0x0035:
            throw r0
        L_0x0036:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A0B(com.facebook.messaging.model.threadkey.ThreadKey):void");
    }

    public void A0C(ThreadKey threadKey, long j) {
        AnonymousClass0m6 r2 = this.A01;
        AnonymousClass3YC r1 = new AnonymousClass3YC();
        r1.A05 = threadKey;
        r2.A0d(new MarkThreadFields(r1), j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        if (r4 != null) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003b, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D(com.facebook.messaging.model.threadkey.ThreadKey r7, long r8, long r10) {
        /*
            r6 = this;
            X.0m6 r5 = r6.A01
            X.0mL r0 = r5.A0A
            X.0mM r4 = r0.A00()
            X.AnonymousClass0m6.A0A(r5, r7)     // Catch:{ all -> 0x0033 }
            com.facebook.messaging.model.threads.ThreadSummary r3 = r5.A0R(r7)     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x002d
            long r1 = r3.A0B     // Catch:{ all -> 0x0033 }
            int r0 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x002d
            X.0zh r2 = com.facebook.messaging.model.threads.ThreadSummary.A00()     // Catch:{ all -> 0x0033 }
            r2.A02(r3)     // Catch:{ all -> 0x0033 }
            r2.A08 = r10     // Catch:{ all -> 0x0033 }
            r2.A06 = r8     // Catch:{ all -> 0x0033 }
            r0 = 0
            r2.A0B = r0     // Catch:{ all -> 0x0033 }
            com.facebook.messaging.model.threads.ThreadSummary r0 = r2.A00()     // Catch:{ all -> 0x0033 }
            r5.A0b(r0)     // Catch:{ all -> 0x0033 }
        L_0x002d:
            if (r4 == 0) goto L_0x0032
            r4.close()
        L_0x0032:
            return
        L_0x0033:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0035 }
        L_0x0035:
            r0 = move-exception
            if (r4 == 0) goto L_0x003b
            r4.close()     // Catch:{ all -> 0x003b }
        L_0x003b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A0D(com.facebook.messaging.model.threadkey.ThreadKey, long, long):void");
    }

    public void A0E(ThreadSummary threadSummary) {
        this.A01.A0b(threadSummary);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0180, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0181, code lost:
        if (r18 != null) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        r18.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0186, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(com.facebook.messaging.service.model.FetchThreadListResult r20) {
        /*
            r19 = this;
            r6 = r20
            X.0l8 r4 = r6.A04
            com.google.common.collect.ImmutableList r1 = r6.A09
            r5 = r19
            X.0t0 r0 = r5.A02
            r0.A06(r1)
            X.0m6 r1 = r5.A01
            com.facebook.messaging.model.folders.FolderCounts r0 = r6.A03
            r1.A0W(r4, r0)
            X.0m6 r10 = r5.A01
            com.facebook.messaging.model.threads.ThreadsCollection r11 = r6.A06
            long r7 = r6.A00
            X.0mL r0 = r10.A0A
            X.0mM r18 = r0.A00()
            X.0pa r9 = X.AnonymousClass0m6.A00(r10, r4)     // Catch:{ all -> 0x017e }
            X.0mL r0 = r9.A06     // Catch:{ all -> 0x017e }
            r0.A01()     // Catch:{ all -> 0x017e }
            X.07X r0 = r9.A05     // Catch:{ all -> 0x017e }
            r0.clear()     // Catch:{ all -> 0x017e }
            com.google.common.collect.ImmutableList r0 = r11.A00     // Catch:{ all -> 0x017e }
            X.1Xv r17 = r0.iterator()     // Catch:{ all -> 0x017e }
        L_0x0034:
            boolean r0 = r17.hasNext()     // Catch:{ all -> 0x017e }
            if (r0 == 0) goto L_0x0066
            java.lang.Object r14 = r17.next()     // Catch:{ all -> 0x017e }
            com.facebook.messaging.model.threads.ThreadSummary r14 = (com.facebook.messaging.model.threads.ThreadSummary) r14     // Catch:{ all -> 0x017e }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r14.A0S     // Catch:{ all -> 0x017e }
            X.AnonymousClass0m6.A0A(r10, r0)     // Catch:{ all -> 0x017e }
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r14.A0S     // Catch:{ all -> 0x017e }
            com.facebook.messaging.model.threads.ThreadSummary r13 = r10.A0R(r0)     // Catch:{ all -> 0x017e }
            if (r13 == 0) goto L_0x005f
            long r2 = r14.A09     // Catch:{ all -> 0x017e }
            r15 = -1
            int r0 = (r2 > r15 ? 1 : (r2 == r15 ? 0 : -1))
            if (r0 == 0) goto L_0x005f
            long r0 = r13.A09     // Catch:{ all -> 0x017e }
            int r12 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r12 >= 0) goto L_0x005f
            r9.A02(r13)     // Catch:{ all -> 0x017e }
            goto L_0x0034
        L_0x005f:
            r9.A02(r14)     // Catch:{ all -> 0x017e }
            r10.A0b(r14)     // Catch:{ all -> 0x017e }
            goto L_0x0034
        L_0x0066:
            r1 = 1
            X.0mL r0 = r9.A06     // Catch:{ all -> 0x017e }
            r0.A01()     // Catch:{ all -> 0x017e }
            r9.A03 = r1     // Catch:{ all -> 0x017e }
            X.0mL r0 = r9.A06     // Catch:{ all -> 0x017e }
            r0.A01()     // Catch:{ all -> 0x017e }
            r9.A04 = r1     // Catch:{ all -> 0x017e }
            X.0mL r0 = r9.A06     // Catch:{ all -> 0x017e }
            r0.A01()     // Catch:{ all -> 0x017e }
            r9.A00 = r7     // Catch:{ all -> 0x017e }
            X.0mN r1 = r10.A08     // Catch:{ all -> 0x017e }
            X.0mL r0 = r1.A02     // Catch:{ all -> 0x017e }
            r0.A01()     // Catch:{ all -> 0x017e }
            X.04b r7 = r1.A00     // Catch:{ all -> 0x017e }
            int r3 = r7.size()     // Catch:{ all -> 0x017e }
            r2 = 0
        L_0x008a:
            if (r2 >= r3) goto L_0x0098
            java.lang.Object r1 = r7.A09(r2)     // Catch:{ all -> 0x017e }
            X.1oR r1 = (X.C33971oR) r1     // Catch:{ all -> 0x017e }
            r0 = 0
            r1.A00 = r0     // Catch:{ all -> 0x017e }
            int r2 = r2 + 1
            goto L_0x008a
        L_0x0098:
            boolean r1 = r11.A01     // Catch:{ all -> 0x017e }
            X.0mL r0 = r9.A06     // Catch:{ all -> 0x017e }
            r0.A01()     // Catch:{ all -> 0x017e }
            r9.A02 = r1     // Catch:{ all -> 0x017e }
            X.0m9 r2 = r10.A0B     // Catch:{ all -> 0x017e }
            X.0l8 r1 = r9.A07     // Catch:{ all -> 0x017e }
            java.lang.String r0 = "updateThreadList"
            r2.A0D(r9, r1, r0)     // Catch:{ all -> 0x017e }
            if (r18 == 0) goto L_0x00af
            r18.close()
        L_0x00af:
            X.0l8 r0 = X.C10950l8.A05
            if (r4 == r0) goto L_0x00b7
            X.0l8 r0 = X.C10950l8.A0B
            if (r4 != r0) goto L_0x017d
        L_0x00b7:
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
            X.0Tq r0 = r5.A08
            java.lang.Object r2 = r0.get()
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 282437049648530(0x100e000040592, double:1.39542443344097E-309)
            boolean r9 = r2.Aem(r0)
            X.0Tq r0 = r5.A08
            java.lang.Object r4 = r0.get()
            X.1Yd r4 = (X.C25051Yd) r4
            r1 = 563912026423918(0x200e00005026e, double:2.78609559532779E-309)
            r0 = 10
            int r4 = r4.AqL(r1, r0)
            com.facebook.messaging.model.threads.ThreadsCollection r0 = r6.A06
            com.google.common.collect.ImmutableList r0 = r0.A00
            X.1Xv r8 = r0.iterator()
        L_0x00e8:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x013f
            java.lang.Object r2 = r8.next()
            com.facebook.messaging.model.threads.ThreadSummary r2 = (com.facebook.messaging.model.threads.ThreadSummary) r2
            X.0yd r1 = r5.A04
            if (r9 == 0) goto L_0x0122
            com.google.common.collect.ImmutableList r0 = r1.A0A(r2)
            if (r0 == 0) goto L_0x00e8
            java.util.Iterator r7 = r0.iterator()
        L_0x0102:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x00e8
            java.lang.Object r0 = r7.next()
            com.facebook.messaging.model.messages.ParticipantInfo r0 = (com.facebook.messaging.model.messages.ParticipantInfo) r0
            com.facebook.user.model.UserKey r2 = r0.A01
            if (r2 == 0) goto L_0x011b
            X.1aB r1 = r2.type
            X.1aB r0 = X.C25651aB.A03
            if (r1 != r0) goto L_0x011b
            r3.add(r2)
        L_0x011b:
            int r0 = r3.size()
            if (r0 < r4) goto L_0x0102
            goto L_0x00e8
        L_0x0122:
            X.1fF r0 = X.C28711fF.ONE_TO_ONE
            com.facebook.messaging.model.threads.ThreadParticipant r0 = X.C17270yd.A04(r1, r2, r0)
            if (r0 == 0) goto L_0x0139
            com.facebook.user.model.UserKey r2 = r0.A00()
            if (r2 == 0) goto L_0x0139
            X.1aB r1 = r2.type
            X.1aB r0 = X.C25651aB.A03
            if (r1 != r0) goto L_0x0139
            r3.add(r2)
        L_0x0139:
            int r0 = r3.size()
            if (r0 < r4) goto L_0x00e8
        L_0x013f:
            X.0r6 r4 = r5.A07
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>(r3)
            java.util.Iterator r3 = r0.iterator()
            r2 = 0
        L_0x014b:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x016b
            java.lang.Object r1 = r3.next()
            com.facebook.user.model.UserKey r1 = (com.facebook.user.model.UserKey) r1
            X.1gw r0 = r4.A0F
            java.lang.Object r0 = r0.A05(r1, r1)
            com.facebook.user.model.UserKey r0 = (com.facebook.user.model.UserKey) r0
            if (r0 != 0) goto L_0x014b
            X.1gw r0 = r4.A0G
            java.lang.Object r0 = r0.A03(r1)
            if (r0 != 0) goto L_0x014b
            r2 = 1
            goto L_0x014b
        L_0x016b:
            if (r2 == 0) goto L_0x0170
            r4.A0R()
        L_0x0170:
            com.facebook.fbservice.results.DataFetchDisposition r0 = r6.A02
            X.0u3 r1 = r0.A07
            X.0u3 r0 = X.AnonymousClass0u3.SERVER
            if (r1 != r0) goto L_0x017d
            X.16c r0 = r5.A00
            r0.A08()
        L_0x017d:
            return
        L_0x017e:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0180 }
        L_0x0180:
            r0 = move-exception
            if (r18 == 0) goto L_0x0186
            r18.close()     // Catch:{ all -> 0x0186 }
        L_0x0186:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A0F(com.facebook.messaging.service.model.FetchThreadListResult):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005b, code lost:
        if (r4 != null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0060, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G(com.facebook.messaging.service.model.FetchThreadResult r14) {
        /*
            r13 = this;
            com.facebook.messaging.model.threads.ThreadSummary r3 = r14.A05
            com.google.common.collect.ImmutableList r1 = r14.A07
            if (r1 == 0) goto L_0x000b
            X.0t0 r0 = r13.A02
            r0.A06(r1)
        L_0x000b:
            X.0m6 r0 = r13.A01
            r0.A0b(r3)
            X.0m6 r5 = r13.A01
            com.facebook.messaging.model.messages.MessagesCollection r7 = r14.A03
            X.0mL r0 = r5.A0A
            X.0mM r4 = r0.A00()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0S     // Catch:{ all -> 0x0058 }
            X.AnonymousClass0m6.A0A(r5, r0)     // Catch:{ all -> 0x0058 }
            X.0mN r2 = r5.A08     // Catch:{ all -> 0x0058 }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r3.A0S     // Catch:{ all -> 0x0058 }
            X.0mL r0 = r2.A02     // Catch:{ all -> 0x0058 }
            r0.A01()     // Catch:{ all -> 0x0058 }
            X.04b r0 = r2.A01     // Catch:{ all -> 0x0058 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x0058 }
            if (r0 == 0) goto L_0x0043
            r0 = 0
            com.facebook.messaging.model.messages.Message r6 = r7.A07(r0)     // Catch:{ all -> 0x0058 }
            java.lang.Integer r10 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0058 }
            X.3dk r11 = X.C72153dk.A02     // Catch:{ all -> 0x0058 }
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0058 }
            r8 = -1
            X.AnonymousClass0m6.A07(r5, r6, r7, r8, r10, r11, r12)     // Catch:{ all -> 0x0058 }
            goto L_0x0049
        L_0x0043:
            X.0mO r1 = r5.A06     // Catch:{ all -> 0x0058 }
            r0 = 0
            X.AnonymousClass0m6.A09(r5, r7, r1, r0)     // Catch:{ all -> 0x0058 }
        L_0x0049:
            if (r4 == 0) goto L_0x004e
            r4.close()
        L_0x004e:
            X.16c r2 = r13.A00
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r3.A0S
            java.lang.String r0 = "CacheInsertThreadsHandler"
            r2.A0G(r1, r0)
            return
        L_0x0058:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x005a }
        L_0x005a:
            r0 = move-exception
            if (r4 == 0) goto L_0x0060
            r4.close()     // Catch:{ all -> 0x0060 }
        L_0x0060:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A0G(com.facebook.messaging.service.model.FetchThreadResult):void");
    }

    public void A0H(FetchThreadResult fetchThreadResult) {
        ThreadSummary threadSummary = fetchThreadResult.A05;
        this.A01.A0b(threadSummary);
        this.A02.A06(fetchThreadResult.A07);
        if (!this.A04.A0D(threadSummary)) {
            A06(threadSummary.A0O, threadSummary.A0S);
        }
        this.A00.A0G(threadSummary.A0S, "CacheInsertThreadsHandler");
    }

    public void A0I(NewMessageResult newMessageResult, long j) {
        A0J(newMessageResult, j, C72153dk.A02);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005d, code lost:
        if (r1 == false) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0J(com.facebook.messaging.service.model.NewMessageResult r11, long r12, X.C72153dk r14) {
        /*
            r10 = this;
            com.facebook.messaging.model.messages.Message r4 = r11.A01
            X.0m6 r3 = r10.A01
            com.facebook.messaging.model.messages.MessagesCollection r5 = r11.A02
            r0 = 0
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r0)
            r6 = r12
            r8 = r14
            r3.A0Y(r4, r5, r6, r8, r9)
            X.1fS r0 = r10.A05
            boolean r0 = r0.A1H(r4)
            if (r0 != 0) goto L_0x0037
            X.0t0 r3 = r10.A02
            com.facebook.messaging.model.messages.ParticipantInfo r0 = r4.A0K
            com.facebook.user.model.UserKey r2 = r0.A01
            long r0 = r4.A03
            r3.A05(r2, r0)
            X.16c r0 = r10.A00
            r0.A08()
            com.facebook.messaging.model.threads.ThreadSummary r0 = r11.A03
            if (r0 == 0) goto L_0x0037
            X.0l8 r1 = r0.A0O
            X.0l8 r0 = X.C10950l8.A06
            if (r1 != r0) goto L_0x0037
            X.16c r0 = r10.A00
            r0.A09()
        L_0x0037:
            boolean r0 = X.C28841fS.A0g(r4)
            if (r0 == 0) goto L_0x0050
            X.16c r3 = r10.A00
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r4.A0U
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r0 = X.C06680bu.A0v
            r1.<init>(r0)
            java.lang.String r0 = "thread_key"
            r1.putExtra(r0, r2)
            X.C189216c.A04(r3, r1)
        L_0x0050:
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r4.A09
            if (r0 == 0) goto L_0x005f
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r2 = r0.A0C
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r0 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A0t
            r1 = 0
            if (r2 != r0) goto L_0x005c
            r1 = 1
        L_0x005c:
            r0 = 1
            if (r1 != 0) goto L_0x0060
        L_0x005f:
            r0 = 0
        L_0x0060:
            if (r0 == 0) goto L_0x0075
            X.16c r3 = r10.A00
            com.facebook.messaging.model.threadkey.ThreadKey r2 = r4.A0U
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r0 = X.C06680bu.A0w
            r1.<init>(r0)
            java.lang.String r0 = "thread_key"
            r1.putExtra(r0, r2)
            X.C189216c.A04(r3, r1)
        L_0x0075:
            boolean r0 = X.C28841fS.A15(r4)
            if (r0 == 0) goto L_0x0094
            com.facebook.messaging.model.messages.GenericAdminMessageInfo r0 = r4.A09
            com.facebook.messaging.model.messages.GenericAdminMessageExtensibleData r0 = r0.A01()
            com.facebook.messaging.model.messages.GroupPollingInfoProperties r0 = (com.facebook.messaging.model.messages.GroupPollingInfoProperties) r0
            if (r0 == 0) goto L_0x0094
            X.0s1 r2 = r10.A06
            java.lang.String r1 = r0.A02
            monitor-enter(r2)
            X.0s2 r0 = r2.A00     // Catch:{ all -> 0x0090 }
            r0.remove(r1)     // Catch:{ all -> 0x0090 }
            goto L_0x0093
        L_0x0090:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0093:
            monitor-exit(r2)
        L_0x0094:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14800u9.A0J(com.facebook.messaging.service.model.NewMessageResult, long, X.3dk):void");
    }

    public C14800u9(AnonymousClass1XY r3, AnonymousClass0m6 r4) {
        new AnonymousClass0UN(1, r3);
        this.A07 = C14890uJ.A00(r3);
        this.A04 = C17270yd.A00(r3);
        this.A03 = C10580kT.A01(r3);
        this.A00 = C189216c.A02(r3);
        this.A05 = C28841fS.A04(r3);
        this.A08 = AnonymousClass0VG.A00(AnonymousClass1Y3.AOJ, r3);
        this.A02 = C14300t0.A00(r3);
        this.A06 = C13760s1.A00(r3);
        this.A01 = r4;
    }

    public void A06(C10950l8 r2, ThreadKey threadKey) {
        A08(r2, ImmutableList.of(threadKey));
    }
}
