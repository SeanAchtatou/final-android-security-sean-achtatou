package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Oq  reason: invalid class name and case insensitive filesystem */
public final class C03590Oq implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ C03550Om A00;

    public C03590Oq(C03550Om r1) {
        this.A00 = r1;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        C03550Om r2 = this.A00;
        r2.A00(r2.A01.getInterpolation(valueAnimator.getAnimatedFraction()));
    }
}
