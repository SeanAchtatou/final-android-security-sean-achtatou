package X;

import android.view.View;
import com.facebook.messaging.model.threadkey.ThreadKey;

/* renamed from: X.21P  reason: invalid class name */
public final class AnonymousClass21P implements C55672oT {
    public final /* synthetic */ C137666bm A00;
    public final /* synthetic */ ThreadKey A01;
    public final /* synthetic */ C59502vS A02;

    public AnonymousClass21P(C137666bm r1, ThreadKey threadKey, C59502vS r3) {
        this.A00 = r1;
        this.A01 = threadKey;
        this.A02 = r3;
    }

    public void onClick(View view) {
        C137656bl r2 = this.A00.A01;
        if (r2 != null) {
            r2.A00(this.A01, !this.A02.A02);
        }
    }
}
