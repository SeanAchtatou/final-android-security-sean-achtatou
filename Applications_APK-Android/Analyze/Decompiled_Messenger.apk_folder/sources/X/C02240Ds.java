package X;

/* renamed from: X.0Ds  reason: invalid class name and case insensitive filesystem */
public final class C02240Ds {
    public long A00 = -1;
    public final int A01;
    public final long A02;
    public final C02250Dt A03;
    private final String A04;

    public static long A00(C02240Ds r6, long j, boolean z) {
        AnonymousClass08Z.A02(32, r6.A04, r6.A01);
        long A012 = r6.A03.A01(r6.A01, j, z);
        if (A012 == -1) {
            A012 = C02300Dy.A00() - r6.A02;
        }
        r6.A00 = A012;
        return A012;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0026, code lost:
        if ((java.lang.System.nanoTime() - r19) <= 1000000) goto L_0x01be;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C02240Ds A01(java.lang.String r23, java.lang.Object[] r24) {
        /*
            java.lang.ThreadLocal r0 = X.C02250Dt.A05
            java.lang.Object r5 = r0.get()
            X.0Dt r5 = (X.C02250Dt) r5
            r22 = 1
            java.lang.String r4 = "ThreadTrace"
            long r19 = java.lang.System.nanoTime()
            r17 = 1000000(0xf4240, double:4.940656E-318)
            r21 = 0
            int r1 = r5.A00     // Catch:{ all -> 0x0222 }
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r1 = r1 - r0
            r9 = 2000(0x7d0, float:2.803E-42)
            if (r1 > r9) goto L_0x002a
            long r1 = java.lang.System.nanoTime()
            long r1 = r1 - r19
            int r0 = (r1 > r17 ? 1 : (r1 == r17 ? 0 : -1))
            if (r0 > 0) goto L_0x01b7
            goto L_0x01be
        L_0x002a:
            long r13 = X.C02300Dy.A00()     // Catch:{ all -> 0x0222 }
            r2 = 0
            r3 = 0
            r12 = 0
            r8 = 0
        L_0x0032:
            int r0 = r5.A00     // Catch:{ all -> 0x0222 }
            if (r3 >= r0) goto L_0x005e
            X.0Dw[] r0 = r5.A02     // Catch:{ all -> 0x0222 }
            r6 = r0[r3]     // Catch:{ all -> 0x0222 }
            if (r6 == 0) goto L_0x005b
            java.lang.Integer r1 = r6.A05     // Catch:{ all -> 0x0222 }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0222 }
            if (r1 != r0) goto L_0x0045
            int r12 = r12 + 1
            goto L_0x004b
        L_0x0045:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0222 }
            if (r1 != r0) goto L_0x004b
            int r12 = r12 + -1
        L_0x004b:
            long r0 = r6.A01     // Catch:{ all -> 0x0222 }
            long r10 = r13 - r0
            r6 = 180000000000(0x29e8d60800, double:8.89318162514E-313)
            int r0 = (r10 > r6 ? 1 : (r10 == r6 ? 0 : -1))
            if (r0 < 0) goto L_0x005e
            if (r12 != 0) goto L_0x005b
            r8 = r3
        L_0x005b:
            int r3 = r3 + 1
            goto L_0x0032
        L_0x005e:
            r3 = 0
            if (r8 <= 0) goto L_0x0080
        L_0x0061:
            int r0 = r8 + 1
            if (r2 >= r0) goto L_0x0080
            X.0Dw[] r0 = r5.A02     // Catch:{ all -> 0x0222 }
            r1 = r0[r2]     // Catch:{ all -> 0x0222 }
            if (r1 == 0) goto L_0x007d
            X.07J r0 = X.C02280Dw.A08     // Catch:{ all -> 0x0222 }
            r0.A02(r1)     // Catch:{ all -> 0x0222 }
            X.0Dw[] r1 = r5.A02     // Catch:{ all -> 0x0222 }
            r0 = 0
            r1[r2] = r0     // Catch:{ all -> 0x0222 }
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r0 = r0 + 1
            r5.A01 = r0     // Catch:{ all -> 0x0222 }
            int r3 = r3 + 1
        L_0x007d:
            int r2 = r2 + 1
            goto L_0x0061
        L_0x0080:
            int r21 = r21 + r3
            r8 = 1500(0x5dc, float:2.102E-42)
            int r1 = r5.A00     // Catch:{ all -> 0x0222 }
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r0 = r1 - r0
            if (r0 > r8) goto L_0x008f
            r6 = -1
            goto L_0x00d6
        L_0x008f:
            java.util.PriorityQueue r7 = new java.util.PriorityQueue     // Catch:{ all -> 0x0222 }
            int r1 = r1 / 2
            java.util.Comparator r0 = X.C02250Dt.A06     // Catch:{ all -> 0x0222 }
            r7.<init>(r1, r0)     // Catch:{ all -> 0x0222 }
            r6 = 0
        L_0x0099:
            int r3 = r5.A00     // Catch:{ all -> 0x0222 }
            if (r6 >= r3) goto L_0x00af
            X.0Dw[] r0 = r5.A02     // Catch:{ all -> 0x0222 }
            r2 = r0[r6]     // Catch:{ all -> 0x0222 }
            if (r2 == 0) goto L_0x00ac
            java.lang.Integer r1 = r2.A05     // Catch:{ all -> 0x0222 }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0222 }
            if (r1 != r0) goto L_0x00ac
            r7.add(r2)     // Catch:{ all -> 0x0222 }
        L_0x00ac:
            int r6 = r6 + 1
            goto L_0x0099
        L_0x00af:
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r3 = r3 - r0
            int r3 = r3 - r8
            java.util.Iterator r2 = r7.iterator()     // Catch:{ all -> 0x0222 }
            r6 = 0
        L_0x00b9:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0222 }
            if (r0 == 0) goto L_0x00cf
            if (r3 <= 0) goto L_0x00cf
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0222 }
            X.0Dw r0 = (X.C02280Dw) r0     // Catch:{ all -> 0x0222 }
            long r6 = r0.A01     // Catch:{ all -> 0x0222 }
            long r0 = r0.A02     // Catch:{ all -> 0x0222 }
            long r6 = r6 - r0
            int r3 = r3 + -2
            goto L_0x00b9
        L_0x00cf:
            if (r3 <= 0) goto L_0x00d6
            r6 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
        L_0x00d6:
            r1 = 0
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00df
            r16 = 0
            goto L_0x0136
        L_0x00df:
            r0 = 10
            int[] r12 = new int[r0]     // Catch:{ all -> 0x0222 }
            r11 = 0
            r16 = 0
            r10 = 0
        L_0x00e7:
            int r0 = r5.A00     // Catch:{ all -> 0x0222 }
            if (r11 >= r0) goto L_0x0136
            X.0Dw[] r14 = r5.A02     // Catch:{ all -> 0x0222 }
            r13 = r14[r11]     // Catch:{ all -> 0x0222 }
            if (r13 == 0) goto L_0x0133
            java.lang.Integer r1 = r13.A05     // Catch:{ all -> 0x0222 }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0222 }
            if (r1 != r0) goto L_0x0109
            int r0 = r12.length     // Catch:{ all -> 0x0222 }
            if (r10 < r0) goto L_0x0104
            int r0 = r0 * 3
            int r0 = r0 / 2
            int r0 = r0 + 1
            int[] r12 = java.util.Arrays.copyOf(r12, r0)     // Catch:{ all -> 0x0222 }
        L_0x0104:
            r12[r10] = r11     // Catch:{ all -> 0x0222 }
            int r10 = r10 + 1
            goto L_0x0133
        L_0x0109:
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0222 }
            if (r1 != r0) goto L_0x0133
            int r10 = r10 + -1
            r15 = r12[r10]     // Catch:{ all -> 0x0222 }
            long r2 = r13.A01     // Catch:{ all -> 0x0222 }
            long r0 = r13.A02     // Catch:{ all -> 0x0222 }
            long r2 = r2 - r0
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 > 0) goto L_0x0133
            r1 = r14[r15]     // Catch:{ all -> 0x0222 }
            X.07J r0 = X.C02280Dw.A08     // Catch:{ all -> 0x0222 }
            r0.A02(r13)     // Catch:{ all -> 0x0222 }
            r0.A02(r1)     // Catch:{ all -> 0x0222 }
            X.0Dw[] r1 = r5.A02     // Catch:{ all -> 0x0222 }
            r0 = 0
            r1[r11] = r0     // Catch:{ all -> 0x0222 }
            r1[r15] = r0     // Catch:{ all -> 0x0222 }
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r0 = r0 + 2
            r5.A01 = r0     // Catch:{ all -> 0x0222 }
            int r16 = r16 + 2
        L_0x0133:
            int r11 = r11 + 1
            goto L_0x00e7
        L_0x0136:
            int r21 = r21 + r16
            int r6 = r5.A00     // Catch:{ all -> 0x0222 }
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r6 = r6 - r0
            if (r6 <= r8) goto L_0x016d
            int r6 = r6 - r8
            r3 = 0
            r7 = 0
        L_0x0142:
            int r0 = r5.A00     // Catch:{ all -> 0x0222 }
            if (r3 >= r0) goto L_0x016b
            if (r6 <= 0) goto L_0x016b
            X.0Dw[] r0 = r5.A02     // Catch:{ all -> 0x0222 }
            r2 = r0[r3]     // Catch:{ all -> 0x0222 }
            if (r2 == 0) goto L_0x0168
            java.lang.Integer r1 = r2.A05     // Catch:{ all -> 0x0222 }
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y     // Catch:{ all -> 0x0222 }
            if (r1 != r0) goto L_0x0168
            X.07J r0 = X.C02280Dw.A08     // Catch:{ all -> 0x0222 }
            r0.A02(r2)     // Catch:{ all -> 0x0222 }
            X.0Dw[] r1 = r5.A02     // Catch:{ all -> 0x0222 }
            r0 = 0
            r1[r3] = r0     // Catch:{ all -> 0x0222 }
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r0 = r0 + 1
            r5.A01 = r0     // Catch:{ all -> 0x0222 }
            int r7 = r7 + 1
            int r6 = r6 + -1
        L_0x0168:
            int r3 = r3 + 1
            goto L_0x0142
        L_0x016b:
            int r21 = r21 + r7
        L_0x016d:
            int r1 = r5.A00     // Catch:{ all -> 0x0222 }
            int r0 = r5.A01     // Catch:{ all -> 0x0222 }
            int r1 = r1 - r0
            if (r1 <= r9) goto L_0x01a4
            java.lang.String r0 = "Resetting because hit couldn't get under hard limit after normal pruning"
            X.C010708t.A0K(r4, r0)     // Catch:{ all -> 0x0222 }
            r4 = 0
            r3 = 0
        L_0x017b:
            int r0 = r5.A00     // Catch:{ all -> 0x0222 }
            if (r3 >= r0) goto L_0x019a
            X.0Dw[] r0 = r5.A02     // Catch:{ all -> 0x0222 }
            r2 = r0[r3]     // Catch:{ all -> 0x0222 }
            if (r2 == 0) goto L_0x0197
            java.lang.Integer r1 = r2.A05     // Catch:{ all -> 0x0222 }
            java.lang.Integer r0 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0222 }
            if (r1 != r0) goto L_0x0192
            android.util.SparseArray r1 = r5.A03     // Catch:{ all -> 0x0222 }
            int r0 = r2.A00     // Catch:{ all -> 0x0222 }
            r1.remove(r0)     // Catch:{ all -> 0x0222 }
        L_0x0192:
            X.07J r0 = X.C02280Dw.A08     // Catch:{ all -> 0x0222 }
            r0.A02(r2)     // Catch:{ all -> 0x0222 }
        L_0x0197:
            int r3 = r3 + 1
            goto L_0x017b
        L_0x019a:
            r5.A00 = r4     // Catch:{ all -> 0x0222 }
            r5.A01 = r4     // Catch:{ all -> 0x0222 }
            android.util.SparseArray r0 = r5.A03     // Catch:{ all -> 0x0222 }
            r0.clear()     // Catch:{ all -> 0x0222 }
            goto L_0x01ab
        L_0x01a4:
            if (r1 <= r8) goto L_0x01ab
            java.lang.String r0 = "Couldn't get under soft limit after normal pruning"
            X.C010708t.A0J(r4, r0)     // Catch:{ all -> 0x0222 }
        L_0x01ab:
            long r1 = java.lang.System.nanoTime()
            long r1 = r1 - r19
            int r0 = (r1 > r17 ? 1 : (r1 == r17 ? 0 : -1))
            if (r0 > 0) goto L_0x01b7
            if (r21 <= 0) goto L_0x01be
        L_0x01b7:
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.getId()
        L_0x01be:
            java.util.concurrent.atomic.AtomicInteger r0 = X.C02250Dt.A07
            int r8 = r0.incrementAndGet()
            int r0 = X.C02250Dt.A04
            if (r8 == r0) goto L_0x01be
            if (r8 == 0) goto L_0x01be
            if (r22 == 0) goto L_0x021f
            java.lang.Integer r1 = X.AnonymousClass07B.A0C
        L_0x01ce:
            r6 = -1
            X.07J r0 = X.C02280Dw.A08
            java.lang.Object r2 = r0.A01()
            X.0Dw r2 = (X.C02280Dw) r2
            r2.A05 = r1
            r2.A00 = r8
            r0 = r24
            r2.A07 = r0
            r0 = r23
            r2.A06 = r0
            long r0 = android.os.SystemClock.currentThreadTimeMillis()
            r2.A03 = r0
            long r0 = X.C02300Dy.A00()
            r2.A01 = r0
            r2.A02 = r6
            r2.A04 = r6
            X.C02250Dt.A00(r5, r2)
            android.util.SparseArray r0 = r5.A03
            r0.append(r8, r2)
            int r1 = r2.A00
            android.util.SparseArray r0 = r5.A03
            java.lang.Object r0 = r0.get(r1)
            X.0Dw r0 = (X.C02280Dw) r0
            java.lang.String r4 = r0.A02()
            X.0Ds r3 = new X.0Ds
            long r9 = X.C02300Dy.A00()
            r6 = r3
            r7 = r5
            r8 = r1
            r11 = r4
            r6.<init>(r7, r8, r9, r11)
            int r2 = r3.A01
            r0 = 32
            X.AnonymousClass08Z.A01(r0, r4, r2)
            return r3
        L_0x021f:
            java.lang.Integer r1 = X.AnonymousClass07B.A00
            goto L_0x01ce
        L_0x0222:
            r3 = move-exception
            long r1 = java.lang.System.nanoTime()
            long r1 = r1 - r19
            int r0 = (r1 > r17 ? 1 : (r1 == r17 ? 0 : -1))
            if (r0 > 0) goto L_0x022f
            if (r21 <= 0) goto L_0x0236
        L_0x022f:
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.getId()
        L_0x0236:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02240Ds.A01(java.lang.String, java.lang.Object[]):X.0Ds");
    }

    private C02240Ds(C02250Dt r3, int i, long j, String str) {
        this.A03 = r3;
        this.A01 = i;
        this.A02 = j;
        this.A04 = str;
    }
}
