package X;

import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.math.RoundingMode;

/* renamed from: X.18G  reason: invalid class name */
public abstract class AnonymousClass18G {
    public static final AnonymousClass18G A00 = new AnonymousClass18L(new C30481i7("base16()", "0123456789ABCDEF".toCharArray()));
    public static final AnonymousClass18G A01 = new AnonymousClass18I(new C30481i7("base32()", "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".toCharArray()), '=');
    public static final AnonymousClass18G A02 = new AnonymousClass18H(new C30481i7("base64()", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray()), '=');
    public static final AnonymousClass18G A03 = new AnonymousClass18H(new C30481i7("base64Url()", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".toCharArray()), '=');

    public int A00(int i) {
        return (int) (((((long) ((AnonymousClass18I) this).A00.A01) * ((long) i)) + 7) / 8);
    }

    public int A01(int i) {
        C30481i7 r0 = ((AnonymousClass18I) this).A00;
        return r0.A03 * AnonymousClass18K.A00(i, r0.A02, RoundingMode.CEILING);
    }

    public int A02(byte[] bArr, CharSequence charSequence) {
        int i;
        C30481i7 r0;
        int i2;
        AnonymousClass18I r2 = (AnonymousClass18I) this;
        CharSequence charSequence2 = charSequence;
        if (r2 instanceof AnonymousClass18H) {
            AnonymousClass18H r22 = (AnonymousClass18H) r2;
            Preconditions.checkNotNull(bArr);
            String trimTrailingFrom = r22.A03().trimTrailingFrom(charSequence2);
            C30481i7 r02 = r22.A00;
            if (r02.A06[trimTrailingFrom.length() % r02.A03]) {
                int i3 = 0;
                int i4 = 0;
                while (i3 < trimTrailingFrom.length()) {
                    int i5 = i3 + 1;
                    i3 = i5 + 1;
                    int A002 = (r22.A00.A00(trimTrailingFrom.charAt(i3)) << 18) | (r22.A00.A00(trimTrailingFrom.charAt(i5)) << 12);
                    int i6 = i + 1;
                    bArr[i] = (byte) (A002 >>> 16);
                    if (i3 < trimTrailingFrom.length()) {
                        int i7 = i3 + 1;
                        int A003 = A002 | (r22.A00.A00(trimTrailingFrom.charAt(i3)) << 6);
                        i4 = i6 + 1;
                        bArr[i6] = (byte) ((A003 >>> 8) & 255);
                        if (i7 < trimTrailingFrom.length()) {
                            i3 = i7 + 1;
                            i6 = i4 + 1;
                            bArr[i4] = (byte) ((A003 | r22.A00.A00(trimTrailingFrom.charAt(i7))) & 255);
                        } else {
                            i3 = i7;
                        }
                    }
                    i4 = i6;
                }
            } else {
                throw new C37831wK(AnonymousClass08S.A09("Invalid input length ", trimTrailingFrom.length()));
            }
        } else if (!(r2 instanceof AnonymousClass18L)) {
            Preconditions.checkNotNull(bArr);
            String trimTrailingFrom2 = r2.A03().trimTrailingFrom(charSequence2);
            C30481i7 r03 = r2.A00;
            if (r03.A06[trimTrailingFrom2.length() % r03.A03]) {
                int i8 = 0;
                i = 0;
                while (i8 < trimTrailingFrom2.length()) {
                    long j = 0;
                    int i9 = 0;
                    int i10 = 0;
                    while (true) {
                        r0 = r2.A00;
                        i2 = r0.A03;
                        if (i9 >= i2) {
                            break;
                        }
                        j <<= r0.A01;
                        if (i8 + i9 < trimTrailingFrom2.length()) {
                            j |= (long) r2.A00.A00(trimTrailingFrom2.charAt(i10 + i8));
                            i10++;
                        }
                        i9++;
                    }
                    int i11 = r0.A02;
                    int i12 = (i11 << 3) - (i10 * r0.A01);
                    int i13 = (i11 - 1) << 3;
                    while (i13 >= i12) {
                        bArr[i] = (byte) ((int) ((j >>> i13) & 255));
                        i13 -= 8;
                        i++;
                    }
                    i8 += i2;
                }
            } else {
                throw new C37831wK(AnonymousClass08S.A09("Invalid input length ", trimTrailingFrom2.length()));
            }
        } else {
            AnonymousClass18L r23 = (AnonymousClass18L) r2;
            Preconditions.checkNotNull(bArr);
            if (charSequence2.length() % 2 != 1) {
                int i14 = 0;
                i = 0;
                while (i14 < charSequence2.length()) {
                    bArr[i] = (byte) ((r23.A00.A00(charSequence2.charAt(i14)) << 4) | r23.A00.A00(charSequence2.charAt(i14 + 1)));
                    i14 += 2;
                    i++;
                }
            } else {
                throw new C37831wK(AnonymousClass08S.A09("Invalid input length ", charSequence2.length()));
            }
        }
        return i;
    }

    public CharMatcher A03() {
        Character ch = ((AnonymousClass18I) this).A01;
        return ch == null ? CharMatcher.None.INSTANCE : new CharMatcher.Is(ch.charValue());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002c, code lost:
        if (r2 > 'z') goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004a, code lost:
        if (r2 > 'Z') goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        if (r2 > 'Z') goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass18G A04() {
        /*
            r9 = this;
            r7 = r9
            X.18I r7 = (X.AnonymousClass18I) r7
            X.18G r0 = r7.A02
            if (r0 != 0) goto L_0x0076
            X.1i7 r8 = r7.A00
            char[] r5 = r8.A05
            int r4 = r5.length
            r3 = 0
        L_0x000d:
            if (r3 >= r4) goto L_0x005f
            char r2 = r5[r3]
            r0 = 65
            if (r2 < r0) goto L_0x001a
            r1 = 90
            r0 = 1
            if (r2 <= r1) goto L_0x001b
        L_0x001a:
            r0 = 0
        L_0x001b:
            if (r0 == 0) goto L_0x005c
            r0 = 1
        L_0x001e:
            if (r0 == 0) goto L_0x006f
            r3 = 0
        L_0x0021:
            if (r3 >= r4) goto L_0x005a
            char r2 = r5[r3]
            r0 = 97
            if (r2 < r0) goto L_0x002e
            r1 = 122(0x7a, float:1.71E-43)
            r0 = 1
            if (r2 <= r1) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            if (r0 == 0) goto L_0x0057
            r0 = 1
        L_0x0032:
            r1 = r0 ^ 1
            java.lang.String r0 = "Cannot call lowerCase() on a mixed-case alphabet"
            com.google.common.base.Preconditions.checkState(r1, r0)
            char[] r6 = r8.A05
            int r5 = r6.length
            char[] r4 = new char[r5]
            r3 = 0
        L_0x003f:
            if (r3 >= r5) goto L_0x0061
            char r2 = r6[r3]
            r0 = 65
            if (r2 < r0) goto L_0x004c
            r1 = 90
            r0 = 1
            if (r2 <= r1) goto L_0x004d
        L_0x004c:
            r0 = 0
        L_0x004d:
            if (r0 == 0) goto L_0x0052
            r0 = r2 ^ 32
            char r2 = (char) r0
        L_0x0052:
            r4[r3] = r2
            int r3 = r3 + 1
            goto L_0x003f
        L_0x0057:
            int r3 = r3 + 1
            goto L_0x0021
        L_0x005a:
            r0 = 0
            goto L_0x0032
        L_0x005c:
            int r3 = r3 + 1
            goto L_0x000d
        L_0x005f:
            r0 = 0
            goto L_0x001e
        L_0x0061:
            X.1i7 r2 = new X.1i7
            java.lang.String r1 = r8.A04
            java.lang.String r0 = ".lowerCase()"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r1, r0)
            r2.<init>(r0, r4)
            r8 = r2
        L_0x006f:
            X.1i7 r0 = r7.A00
            if (r8 != r0) goto L_0x0077
            r0 = r7
        L_0x0074:
            r7.A02 = r0
        L_0x0076:
            return r0
        L_0x0077:
            java.lang.Character r0 = r7.A01
            X.18G r0 = r7.A09(r8, r0)
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18G.A04():X.18G");
    }

    public AnonymousClass18G A05() {
        AnonymousClass18I r2 = (AnonymousClass18I) this;
        return r2.A01 != null ? r2.A09(r2.A00, null) : r2;
    }

    public String A06(byte[] bArr) {
        int length = bArr.length;
        Preconditions.checkPositionIndexes(0, 0 + length, length);
        StringBuilder sb = new StringBuilder(A01(length));
        try {
            A07(sb, bArr, 0, length);
            return sb.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public void A07(Appendable appendable, byte[] bArr, int i, int i2) {
        AnonymousClass18I r4 = (AnonymousClass18I) this;
        if (r4 instanceof AnonymousClass18H) {
            AnonymousClass18H r42 = (AnonymousClass18H) r4;
            Preconditions.checkNotNull(appendable);
            int i3 = i + i2;
            Preconditions.checkPositionIndexes(i, i3, bArr.length);
            while (i2 >= 3) {
                int i4 = i + 1;
                int i5 = i4 + 1;
                i = i5 + 1;
                byte b = ((bArr[i] & 255) << 16) | ((bArr[i4] & 255) << 8) | (bArr[i5] & 255);
                appendable.append(r42.A00.A05[b >>> 18]);
                appendable.append(r42.A00.A05[(b >>> 12) & 63]);
                appendable.append(r42.A00.A05[(b >>> 6) & 63]);
                appendable.append(r42.A00.A05[b & 63]);
                i2 -= 3;
            }
            if (i < i3) {
                r42.A0A(appendable, bArr, i, i3 - i);
            }
        } else if (!(r4 instanceof AnonymousClass18L)) {
            Preconditions.checkNotNull(appendable);
            Preconditions.checkPositionIndexes(i, i + i2, bArr.length);
            for (int i6 = 0; i6 < i2; i6 += r4.A00.A02) {
                r4.A0A(appendable, bArr, i + i6, Math.min(r4.A00.A02, i2 - i6));
            }
        } else {
            AnonymousClass18L r43 = (AnonymousClass18L) r4;
            Preconditions.checkNotNull(appendable);
            Preconditions.checkPositionIndexes(i, i + i2, bArr.length);
            for (int i7 = 0; i7 < i2; i7++) {
                byte b2 = bArr[i + i7] & 255;
                appendable.append(r43.A00[b2]);
                appendable.append(r43.A00[b2 | 256]);
            }
        }
    }

    static {
        new AnonymousClass18I(new C30481i7("base32Hex()", "0123456789ABCDEFGHIJKLMNOPQRSTUV".toCharArray()), '=');
    }

    public final byte[] A08(CharSequence charSequence) {
        try {
            String trimTrailingFrom = A03().trimTrailingFrom(charSequence);
            int A002 = A00(trimTrailingFrom.length());
            byte[] bArr = new byte[A002];
            int A022 = A02(bArr, trimTrailingFrom);
            if (A022 == A002) {
                return bArr;
            }
            byte[] bArr2 = new byte[A022];
            System.arraycopy(bArr, 0, bArr2, 0, A022);
            return bArr2;
        } catch (C37831wK e) {
            throw new IllegalArgumentException(e);
        }
    }
}
