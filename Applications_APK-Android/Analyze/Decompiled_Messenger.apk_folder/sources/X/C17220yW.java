package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.contacts.interfaces.model.ContactsUploadState;

/* renamed from: X.0yW  reason: invalid class name and case insensitive filesystem */
public final class C17220yW implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C17220yW(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(282883323);
        if (AnonymousClass80H.$const$string(AnonymousClass1Y3.A12).equals(intent.getAction()) && ((ContactsUploadState) intent.getParcelableExtra("state")).A03 == AnonymousClass97t.A03) {
            this.A00.A0Q();
        }
        AnonymousClass09Y.A01(-1610326898, A002);
    }
}
