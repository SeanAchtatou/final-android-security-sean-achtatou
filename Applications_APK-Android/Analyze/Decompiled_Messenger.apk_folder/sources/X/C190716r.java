package X;

import com.facebook.auth.userscope.UserScoped;
import com.facebook.messaging.model.folders.FolderCounts;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.notify.DirectMessageStorySeenNotification;
import com.facebook.messaging.notify.EventReminderNotification;
import com.facebook.messaging.notify.FailedToSendMessageNotification;
import com.facebook.messaging.notify.FriendInstallNotification;
import com.facebook.messaging.notify.JoinRequestNotification;
import com.facebook.messaging.notify.LoggedOutMessageNotification;
import com.facebook.messaging.notify.MessageReactionNotification;
import com.facebook.messaging.notify.MessageRequestNotification;
import com.facebook.messaging.notify.MessengerLivingRoomCreateNotification;
import com.facebook.messaging.notify.MessengerRoomInviteReminderNotification;
import com.facebook.messaging.notify.MissedCallNotification;
import com.facebook.messaging.notify.MontageMessageNotification;
import com.facebook.messaging.notify.MultipleAccountsNewMessagesNotification;
import com.facebook.messaging.notify.PageMessageNotification;
import com.facebook.messaging.notify.PaymentNotification;
import com.facebook.messaging.notify.SimpleMessageNotification;
import com.facebook.messaging.notify.StaleNotification;
import com.facebook.messaging.notify.UriNotification;
import com.facebook.messaging.notify.VideoChatLinkJoinAttemptNotification;
import com.facebook.messaging.notify.type.NewMessageNotification;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

@UserScoped
/* renamed from: X.16r  reason: invalid class name and case insensitive filesystem */
public final class C190716r implements C191016u {
    private static C05540Zi A03;
    public AnonymousClass0UN A00;
    private final C001500z A01;
    private final AnonymousClass0US A02;

    public void ASx(ThreadKey threadKey, String str) {
        ThreadKey threadKey2 = threadKey;
        if (threadKey != null) {
            new AnonymousClass17E(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "clearThreadNotification", threadKey2, str).A01();
        }
    }

    public static final C190716r A00(AnonymousClass1XY r4) {
        C190716r r0;
        synchronized (C190716r.class) {
            C05540Zi A002 = C05540Zi.A00(A03);
            A03 = A002;
            try {
                if (A002.A03(r4)) {
                    A03.A00 = new C190716r((AnonymousClass1XY) A03.A01());
                }
                C05540Zi r1 = A03;
                r0 = (C190716r) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A03.A02();
                throw th;
            }
        }
        return r0;
    }

    public void ASY(String str) {
        new AnonymousClass17A(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "clearAllNotifications", str).A01();
    }

    public void ASm() {
        if (C001500z.A07.equals(this.A01)) {
            new C13780s3(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "clearFailedProfilePictureUploadNotifications").A01();
        }
    }

    public void ASn(String str) {
        new AnonymousClass17C(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "clearFriendInstall", str).A01();
    }

    public void ASr(ArrayList arrayList) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass17B(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "clearMultipleAccountsNewMessagesNotification", arrayList).A01();
        }
    }

    public void BM4(DirectMessageStorySeenNotification directMessageStorySeenNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass17H(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyDirectMessageStorySeenNotification", directMessageStorySeenNotification).A01();
        }
    }

    public void BM8(EventReminderNotification eventReminderNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new C30381hx(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyEventReminderNotification", eventReminderNotification).A01();
        }
    }

    public void BM9(FailedToSendMessageNotification failedToSendMessageNotification) {
        new AnonymousClass171(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyFailedToSendMessage", failedToSendMessageNotification).A01();
    }

    public void BMA() {
        if (C001500z.A07.equals(this.A01)) {
            new C30351hu(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyFailedToSetProfilePicture").A01();
        }
    }

    public void BMB(MontageMessageNotification montageMessageNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass177(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyFirstMontageMessage", montageMessageNotification).A01();
        }
    }

    public void BMC(FolderCounts folderCounts) {
        if (C001500z.A07.equals(this.A01)) {
            new C191116v(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyFolderCounts", folderCounts).A01();
        }
    }

    public void BME(SimpleMessageNotification simpleMessageNotification) {
        new AnonymousClass17G(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyInternalMessage", simpleMessageNotification).A01();
    }

    public void BMI(JoinRequestNotification joinRequestNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new C30391hy(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyJoinRequestNotification", joinRequestNotification).A01();
        }
    }

    public void BMK(LoggedOutMessageNotification loggedOutMessageNotification) {
        new BSO(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyLoggedOutMessage", loggedOutMessageNotification).A01();
    }

    public void BML(MessageReactionNotification messageReactionNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new C30331hs(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMessageReactionNotification", messageReactionNotification).A01();
        }
    }

    public void BMM(MessageRequestNotification messageRequestNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new C13790s4(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMessageRequestNotification", messageRequestNotification).A01();
        }
    }

    public void BMN(MessengerLivingRoomCreateNotification messengerLivingRoomCreateNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new C30361hv(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMessengerLivingRoomCreateNotification", messengerLivingRoomCreateNotification).A01();
        }
    }

    public void BMO(MessengerRoomInviteReminderNotification messengerRoomInviteReminderNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass17I(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notification", messengerRoomInviteReminderNotification).A01();
        }
    }

    public void BMP(StaleNotification staleNotification) {
        C001500z r0 = C001500z.A07;
        C001500z r1 = this.A01;
        if (r0.equals(r1) || C001500z.A0C.equals(r1)) {
            new C14230sp(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMessengerStaleNotification", staleNotification).A01();
        }
    }

    public void BMQ(UriNotification uriNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new C13800s5(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMessengerUriNotification", uriNotification).A01();
        }
    }

    public void BMR(MissedCallNotification missedCallNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new BSM(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMissedCall", missedCallNotification).A01();
        }
    }

    public void BMS(MontageMessageNotification montageMessageNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass174(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMontageDailyDigest", montageMessageNotification).A01();
        }
    }

    public void BMT(MontageMessageNotification montageMessageNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass176(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMontageMessageExpiring", montageMessageNotification).A01();
        }
    }

    public void BMU(MontageMessageNotification montageMessageNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass173(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMontageMessageReaction", montageMessageNotification).A01();
        }
    }

    public void BMV(MontageMessageNotification montageMessageNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass175(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyMontageMessageViewingStatus", montageMessageNotification).A01();
        }
    }

    public void BMW(MultipleAccountsNewMessagesNotification multipleAccountsNewMessagesNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new C30411i0(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notification", multipleAccountsNewMessagesNotification).A01();
        }
    }

    public void BMX(FriendInstallNotification friendInstallNotification) {
        new BSQ(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyNewFriendInstall", friendInstallNotification).A01();
    }

    public void BMY(NewMessageNotification newMessageNotification) {
        NewMessageNotification newMessageNotification2 = newMessageNotification;
        if (!C001500z.A0C.equals(this.A01) || !C28841fS.A0Y(newMessageNotification.A00)) {
            new C30421i1(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyNewMessage", newMessageNotification2).A01();
        }
    }

    public void BMb(PageMessageNotification pageMessageNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new BSL(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyPageMessage", pageMessageNotification).A01();
        }
    }

    public void BMc(PaymentNotification paymentNotification) {
        new BSP(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyPayment", paymentNotification).A01();
    }

    public void BMd(SimpleMessageNotification simpleMessageNotification) {
        new AnonymousClass172(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyPreRegPushMessage", simpleMessageNotification).A01();
    }

    public void BMg(VideoChatLinkJoinAttemptNotification videoChatLinkJoinAttemptNotification) {
        if (C001500z.A07.equals(this.A01)) {
            new AnonymousClass17F(this, (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AHh, this.A00), this.A02, "notifyVideoChatLinkJoinAttemptNotification", videoChatLinkJoinAttemptNotification).A01();
        }
    }

    private C190716r(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
        this.A02 = AnonymousClass0UQ.A00(AnonymousClass1Y3.A8E, r3);
        this.A01 = AnonymousClass0UU.A05(r3);
    }
}
