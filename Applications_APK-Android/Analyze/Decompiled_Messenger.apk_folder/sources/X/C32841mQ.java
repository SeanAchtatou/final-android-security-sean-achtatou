package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.widget.recyclerview.BetterRecyclerView;

/* renamed from: X.1mQ  reason: invalid class name and case insensitive filesystem */
public final class C32841mQ extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ BetterRecyclerView A00;

    public C32841mQ(BetterRecyclerView betterRecyclerView) {
        this.A00 = betterRecyclerView;
    }

    public void onLongPress(MotionEvent motionEvent) {
        int A02;
        C33781o8 A05;
        View A0Z = this.A00.A0Z(motionEvent.getX(), motionEvent.getY());
        if (A0Z != null && this.A00.A08 != null && (A02 = RecyclerView.A02(A0Z)) != -1) {
            BetterRecyclerView betterRecyclerView = this.A00;
            D6O d6o = betterRecyclerView.A08;
            C20831Dz r0 = betterRecyclerView.A0J;
            long j = -1;
            if (!(r0 == null || !r0.hasStableIds() || (A05 = RecyclerView.A05(A0Z)) == null)) {
                j = A05.A07;
            }
            if (d6o.Bc7(betterRecyclerView, A0Z, A02, j)) {
                this.A00.performHapticFeedback(0);
            }
        }
    }
}
