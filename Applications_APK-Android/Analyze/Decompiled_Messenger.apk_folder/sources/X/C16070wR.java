package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0wR  reason: invalid class name and case insensitive filesystem */
public abstract class C16070wR extends C16080wS implements C17810zV, AnonymousClass0zW, C17820zX, Cloneable {
    private static final AtomicInteger A0B = new AtomicInteger(0);
    public int A00;
    public AnonymousClass10N A01;
    public C16070wR A02;
    public AnonymousClass1GA A03;
    public String A04;
    public String A05;
    public List A06;
    public Map A07;
    public boolean A08;
    public final int A09 = A0B.getAndIncrement();
    public final String A0A;

    public static void A01(C16070wR r1) {
        r1.A08 = true;
        C16070wR r0 = r1.A02;
        if (r0 != null) {
            A01(r0);
        }
    }

    public AnonymousClass11I A0S() {
        if (this instanceof AnonymousClass1AO) {
            return ((AnonymousClass1AO) this).A07;
        }
        if (this instanceof C20271Bt) {
            return ((C20271Bt) this).A06;
        }
        if (!(this instanceof C20291Bv)) {
            return null;
        }
        return ((C20291Bv) this).A09;
    }

    public C17790zT Alq() {
        return this;
    }

    public static Map A00(C16070wR r8) {
        HashMap hashMap = new HashMap();
        if (r8 != null) {
            List list = r8.A06;
            if (list != null) {
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    C16070wR r3 = (C16070wR) list.get(i);
                    hashMap.put(r3.A04, new C12300p2(r3, Integer.valueOf(i)));
                }
            } else {
                throw new IllegalStateException("Children of current section " + r8 + " is null!");
            }
        }
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:224:0x024d, code lost:
        if (r1.equals(r10.A02) == false) goto L_0x024f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0044, code lost:
        if (r1.equals(r10.A04) == false) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x032f, code lost:
        if (r1 != null) goto L_0x0338;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x0336, code lost:
        if (r1 != null) goto L_0x0338;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:308:0x033c, code lost:
        if (r1.equals(r0) != false) goto L_0x0342;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x033e, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x033f, code lost:
        if (r0 == null) goto L_0x0342;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:311:0x0341, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:466:0x04c8, code lost:
        if (r1.equals(r10.A04) == false) goto L_0x04ca;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0U(X.C16070wR r10) {
        /*
            r9 = this;
            boolean r0 = r9 instanceof X.AnonymousClass1AO
            if (r0 != 0) goto L_0x040c
            boolean r0 = r9 instanceof X.C35291qz
            if (r0 != 0) goto L_0x0357
            boolean r0 = r9 instanceof X.C16060wQ
            if (r0 != 0) goto L_0x04a9
            boolean r0 = r9 instanceof X.AnonymousClass1FF
            if (r0 != 0) goto L_0x0343
            boolean r0 = r9 instanceof X.C20271Bt
            if (r0 != 0) goto L_0x022e
            boolean r0 = r9 instanceof X.C20291Bv
            if (r0 != 0) goto L_0x00b8
            boolean r0 = r9 instanceof X.AnonymousClass1GV
            if (r0 != 0) goto L_0x005d
            boolean r0 = r9 instanceof X.AnonymousClass1AM
            if (r0 != 0) goto L_0x0025
            boolean r0 = r9.equals(r10)
            return r0
        L_0x0025:
            r4 = r9
            X.1AM r4 = (X.AnonymousClass1AM) r4
            r3 = 1
            if (r4 == r10) goto L_0x005c
            r2 = 0
            if (r10 == 0) goto L_0x0046
            java.lang.Class r1 = r4.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 != r0) goto L_0x0046
            X.1AM r10 = (X.AnonymousClass1AM) r10
            java.util.List r1 = r4.A04
            if (r1 == 0) goto L_0x0047
            java.util.List r0 = r10.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x004c
        L_0x0046:
            return r2
        L_0x0047:
            java.util.List r0 = r10.A04
            if (r0 == 0) goto L_0x004c
            return r2
        L_0x004c:
            java.lang.Boolean r1 = r4.A03
            java.lang.Boolean r0 = r10.A03
            if (r1 == 0) goto L_0x0059
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x005c
            return r2
        L_0x0059:
            if (r0 == 0) goto L_0x005c
            return r2
        L_0x005c:
            return r3
        L_0x005d:
            r2 = r9
            X.1GV r2 = (X.AnonymousClass1GV) r2
            r4 = 1
            if (r2 == r10) goto L_0x055e
            r3 = 0
            if (r10 == 0) goto L_0x04ca
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 != r0) goto L_0x04ca
            X.1GV r10 = (X.AnonymousClass1GV) r10
            X.0zR r1 = r2.A00
            if (r1 == 0) goto L_0x007f
            X.0zR r0 = r10.A00
            boolean r0 = r1.A1L(r0)
            if (r0 != 0) goto L_0x0084
            return r3
        L_0x007f:
            X.0zR r0 = r10.A00
            if (r0 == 0) goto L_0x0084
            return r3
        L_0x0084:
            java.lang.Boolean r1 = r2.A01
            if (r1 == 0) goto L_0x0091
            java.lang.Boolean r0 = r10.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0096
            return r3
        L_0x0091:
            java.lang.Boolean r0 = r10.A01
            if (r0 == 0) goto L_0x0096
            return r3
        L_0x0096:
            java.lang.Integer r1 = r2.A03
            if (r1 == 0) goto L_0x00a3
            java.lang.Integer r0 = r10.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00a8
            return r3
        L_0x00a3:
            java.lang.Integer r0 = r10.A03
            if (r0 == 0) goto L_0x00a8
            return r3
        L_0x00a8:
            java.lang.Boolean r1 = r2.A02
            java.lang.Boolean r0 = r10.A02
            if (r1 == 0) goto L_0x00b5
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x055e
            return r3
        L_0x00b5:
            if (r0 == 0) goto L_0x055e
            return r3
        L_0x00b8:
            r2 = r9
            X.1Bv r2 = (X.C20291Bv) r2
            r8 = 1
            if (r2 == r10) goto L_0x0342
            r7 = 0
            if (r10 == 0) goto L_0x024f
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 != r0) goto L_0x024f
            X.1Bv r10 = (X.C20291Bv) r10
            boolean r1 = r2.A0F
            boolean r0 = r10.A0F
            if (r1 != r0) goto L_0x024f
            long r5 = r2.A03
            long r3 = r10.A03
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x024f
            X.AmJ r1 = r2.A05
            if (r1 == 0) goto L_0x00e8
            X.AmJ r0 = r10.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00ed
            return r7
        L_0x00e8:
            X.AmJ r0 = r10.A05
            if (r0 == 0) goto L_0x00ed
            return r7
        L_0x00ed:
            java.lang.Integer r1 = r2.A0C
            if (r1 == 0) goto L_0x00fa
            java.lang.Integer r0 = r10.A0C
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x00ff
            return r7
        L_0x00fa:
            java.lang.Integer r0 = r10.A0C
            if (r0 == 0) goto L_0x00ff
            return r7
        L_0x00ff:
            r1 = 0
            if (r1 != r1) goto L_0x024f
            X.Alj r1 = r2.A06
            if (r1 == 0) goto L_0x010f
            X.Alj r0 = r10.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0114
            return r7
        L_0x010f:
            X.Alj r0 = r10.A06
            if (r0 == 0) goto L_0x0114
            return r7
        L_0x0114:
            int r1 = r2.A00
            int r0 = r10.A00
            if (r1 != r0) goto L_0x024f
            java.lang.String r1 = r2.A0E
            if (r1 == 0) goto L_0x0127
            java.lang.String r0 = r10.A0E
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x012c
            return r7
        L_0x0127:
            java.lang.String r0 = r10.A0E
            if (r0 == 0) goto L_0x012c
            return r7
        L_0x012c:
            int r1 = r2.A01
            int r0 = r10.A01
            if (r1 != r0) goto L_0x024f
            int r1 = r2.A02
            int r0 = r10.A02
            if (r1 != r0) goto L_0x024f
            java.lang.Object r1 = r2.A0D
            if (r1 == 0) goto L_0x0145
            java.lang.Object r0 = r10.A0D
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x014a
            return r7
        L_0x0145:
            java.lang.Object r0 = r10.A0D
            if (r0 == 0) goto L_0x014a
            return r7
        L_0x014a:
            X.654 r1 = r2.A0B
            if (r1 == 0) goto L_0x0157
            X.654 r0 = r10.A0B
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x015c
            return r7
        L_0x0157:
            X.654 r0 = r10.A0B
            if (r0 == 0) goto L_0x015c
            return r7
        L_0x015c:
            X.3ik r0 = r2.A09
            X.3iZ r1 = r0.connectionData
            if (r1 == 0) goto L_0x016d
            X.3ik r0 = r10.A09
            X.3iZ r0 = r0.connectionData
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0174
            return r7
        L_0x016d:
            X.3ik r0 = r10.A09
            X.3iZ r0 = r0.connectionData
            if (r0 == 0) goto L_0x0174
            return r7
        L_0x0174:
            X.3ik r0 = r2.A09
            X.3iM r1 = r0.connectionHandler
            if (r1 == 0) goto L_0x0185
            X.3ik r0 = r10.A09
            X.3iM r0 = r0.connectionHandler
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x018c
            return r7
        L_0x0185:
            X.3ik r0 = r10.A09
            X.3iM r0 = r0.connectionHandler
            if (r0 == 0) goto L_0x018c
            return r7
        L_0x018c:
            X.3ik r3 = r2.A09
            X.7Na r1 = r3.dataSource
            if (r1 == 0) goto L_0x019d
            X.3ik r0 = r10.A09
            X.7Na r0 = r0.dataSource
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01a4
            return r7
        L_0x019d:
            X.3ik r0 = r10.A09
            X.7Na r0 = r0.dataSource
            if (r0 == 0) goto L_0x01a4
            return r7
        L_0x01a4:
            java.lang.Throwable r1 = r3.fetchError
            if (r1 == 0) goto L_0x01b3
            X.3ik r0 = r10.A09
            java.lang.Throwable r0 = r0.fetchError
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01ba
            return r7
        L_0x01b3:
            X.3ik r0 = r10.A09
            java.lang.Throwable r0 = r0.fetchError
            if (r0 == 0) goto L_0x01ba
            return r7
        L_0x01ba:
            X.3ik r3 = r2.A09
            X.7Ne r1 = r3.fetchState
            if (r1 == 0) goto L_0x01cb
            X.3ik r0 = r10.A09
            X.7Ne r0 = r0.fetchState
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01d2
            return r7
        L_0x01cb:
            X.3ik r0 = r10.A09
            X.7Ne r0 = r0.fetchState
            if (r0 == 0) goto L_0x01d2
            return r7
        L_0x01d2:
            java.util.concurrent.atomic.AtomicBoolean r1 = r3.isWaitingForOnDataBound
            if (r1 == 0) goto L_0x01e1
            X.3ik r0 = r10.A09
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.isWaitingForOnDataBound
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x01e8
            return r7
        L_0x01e1:
            X.3ik r0 = r10.A09
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.isWaitingForOnDataBound
            if (r0 == 0) goto L_0x01e8
            return r7
        L_0x01e8:
            X.3ik r3 = r2.A09
            java.lang.String r1 = r3.lastLocalCacheScopeUsed
            if (r1 == 0) goto L_0x01f9
            X.3ik r0 = r10.A09
            java.lang.String r0 = r0.lastLocalCacheScopeUsed
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0200
            return r7
        L_0x01f9:
            X.3ik r0 = r10.A09
            java.lang.String r0 = r0.lastLocalCacheScopeUsed
            if (r0 == 0) goto L_0x0200
            return r7
        L_0x0200:
            X.EfB r1 = r3.serviceListener
            if (r1 == 0) goto L_0x020f
            X.3ik r0 = r10.A09
            X.EfB r0 = r0.serviceListener
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0216
            return r7
        L_0x020f:
            X.3ik r0 = r10.A09
            X.EfB r0 = r0.serviceListener
            if (r0 == 0) goto L_0x0216
            return r7
        L_0x0216:
            X.3ik r0 = r2.A09
            X.23T r1 = r0.ttrcTraceHelper
            if (r1 == 0) goto L_0x0227
            X.3ik r0 = r10.A09
            X.23T r0 = r0.ttrcTraceHelper
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0332
            return r7
        L_0x0227:
            X.3ik r0 = r10.A09
            X.23T r0 = r0.ttrcTraceHelper
            if (r0 == 0) goto L_0x0332
            return r7
        L_0x022e:
            r2 = r9
            X.1Bt r2 = (X.C20271Bt) r2
            r8 = 1
            if (r2 == r10) goto L_0x0342
            r7 = 0
            if (r10 == 0) goto L_0x024f
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 != r0) goto L_0x024f
            X.1Bt r10 = (X.C20271Bt) r10
            X.0gx r1 = r2.A02
            if (r1 == 0) goto L_0x0250
            X.0gx r0 = r10.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0255
        L_0x024f:
            return r7
        L_0x0250:
            X.0gx r0 = r10.A02
            if (r0 == 0) goto L_0x0255
            return r7
        L_0x0255:
            long r5 = r2.A00
            long r3 = r10.A00
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x024f
            X.653 r1 = r2.A05
            if (r1 == 0) goto L_0x026a
            X.653 r0 = r10.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x026f
            return r7
        L_0x026a:
            X.653 r0 = r10.A05
            if (r0 == 0) goto L_0x026f
            return r7
        L_0x026f:
            X.6RZ r0 = r2.A06
            java.lang.Object r1 = r0.cachedModel
            if (r1 == 0) goto L_0x0280
            X.6RZ r0 = r10.A06
            java.lang.Object r0 = r0.cachedModel
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0287
            return r7
        L_0x0280:
            X.6RZ r0 = r10.A06
            java.lang.Object r0 = r0.cachedModel
            if (r0 == 0) goto L_0x0287
            return r7
        L_0x0287:
            X.6RZ r3 = r2.A06
            X.7Na r1 = r3.dataSource
            if (r1 == 0) goto L_0x0298
            X.6RZ r0 = r10.A06
            X.7Na r0 = r0.dataSource
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x029f
            return r7
        L_0x0298:
            X.6RZ r0 = r10.A06
            X.7Na r0 = r0.dataSource
            if (r0 == 0) goto L_0x029f
            return r7
        L_0x029f:
            java.lang.Throwable r1 = r3.fetchError
            if (r1 == 0) goto L_0x02ae
            X.6RZ r0 = r10.A06
            java.lang.Throwable r0 = r0.fetchError
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02b5
            return r7
        L_0x02ae:
            X.6RZ r0 = r10.A06
            java.lang.Throwable r0 = r0.fetchError
            if (r0 == 0) goto L_0x02b5
            return r7
        L_0x02b5:
            X.6RZ r3 = r2.A06
            X.7Ne r1 = r3.fetchState
            if (r1 == 0) goto L_0x02c6
            X.6RZ r0 = r10.A06
            X.7Ne r0 = r0.fetchState
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02cd
            return r7
        L_0x02c6:
            X.6RZ r0 = r10.A06
            X.7Ne r0 = r0.fetchState
            if (r0 == 0) goto L_0x02cd
            return r7
        L_0x02cd:
            java.lang.Object r1 = r3.responseModel
            if (r1 == 0) goto L_0x02dc
            X.6RZ r0 = r10.A06
            java.lang.Object r0 = r0.responseModel
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02e3
            return r7
        L_0x02dc:
            X.6RZ r0 = r10.A06
            java.lang.Object r0 = r0.responseModel
            if (r0 == 0) goto L_0x02e3
            return r7
        L_0x02e3:
            X.6RZ r0 = r2.A06
            X.7ND r1 = r0.serviceListener
            if (r1 == 0) goto L_0x02f4
            X.6RZ r0 = r10.A06
            X.7ND r0 = r0.serviceListener
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x02fb
            return r7
        L_0x02f4:
            X.6RZ r0 = r10.A06
            X.7ND r0 = r0.serviceListener
            if (r0 == 0) goto L_0x02fb
            return r7
        L_0x02fb:
            X.6RZ r0 = r2.A06
            com.facebook.graphservice.interfaces.Summary r1 = r0.summary
            if (r1 == 0) goto L_0x030c
            X.6RZ r0 = r10.A06
            com.facebook.graphservice.interfaces.Summary r0 = r0.summary
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0313
            return r7
        L_0x030c:
            X.6RZ r0 = r10.A06
            com.facebook.graphservice.interfaces.Summary r0 = r0.summary
            if (r0 == 0) goto L_0x0313
            return r7
        L_0x0313:
            X.6RZ r0 = r2.A06
            X.23T r1 = r0.ttrcTraceHelper
            if (r1 == 0) goto L_0x0324
            X.6RZ r0 = r10.A06
            X.23T r0 = r0.ttrcTraceHelper
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x032b
            return r7
        L_0x0324:
            X.6RZ r0 = r10.A06
            X.23T r0 = r0.ttrcTraceHelper
            if (r0 == 0) goto L_0x032b
            return r7
        L_0x032b:
            X.BiW r1 = r2.A01
            X.BiW r0 = r10.A01
            if (r1 == 0) goto L_0x033f
            goto L_0x0338
        L_0x0332:
            X.BiW r1 = r2.A04
            X.BiW r0 = r10.A04
            if (r1 == 0) goto L_0x033f
        L_0x0338:
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0342
            return r7
        L_0x033f:
            if (r0 == 0) goto L_0x0342
            return r7
        L_0x0342:
            return r8
        L_0x0343:
            r0 = r9
            X.1FF r0 = (X.AnonymousClass1FF) r0
            r2 = 1
            if (r0 == r10) goto L_0x0356
            if (r10 == 0) goto L_0x0355
            java.lang.Class r1 = r0.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 == r0) goto L_0x0356
        L_0x0355:
            r2 = 0
        L_0x0356:
            return r2
        L_0x0357:
            r2 = r9
            X.1qz r2 = (X.C35291qz) r2
            r4 = 1
            if (r2 == r10) goto L_0x055e
            r3 = 0
            if (r10 == 0) goto L_0x04ca
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 != r0) goto L_0x04ca
            X.1qz r10 = (X.C35291qz) r10
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r2.A04
            if (r1 == 0) goto L_0x0379
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x037e
            return r3
        L_0x0379:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A04
            if (r0 == 0) goto L_0x037e
            return r3
        L_0x037e:
            X.0vU r1 = r2.A07
            if (r1 == 0) goto L_0x038b
            X.0vU r0 = r10.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0390
            return r3
        L_0x038b:
            X.0vU r0 = r10.A07
            if (r0 == 0) goto L_0x0390
            return r3
        L_0x0390:
            X.0ki r1 = r2.A02
            if (r1 == 0) goto L_0x039d
            X.0ki r0 = r10.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03a2
            return r3
        L_0x039d:
            X.0ki r0 = r10.A02
            if (r0 == 0) goto L_0x03a2
            return r3
        L_0x03a2:
            X.0v8 r1 = r2.A01
            if (r1 == 0) goto L_0x03af
            X.0v8 r0 = r10.A01
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03b4
            return r3
        L_0x03af:
            X.0v8 r0 = r10.A01
            if (r0 == 0) goto L_0x03b4
            return r3
        L_0x03b4:
            com.google.common.collect.ImmutableList r1 = r2.A08
            if (r1 == 0) goto L_0x03c1
            com.google.common.collect.ImmutableList r0 = r10.A08
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03c6
            return r3
        L_0x03c1:
            com.google.common.collect.ImmutableList r0 = r10.A08
            if (r0 == 0) goto L_0x03c6
            return r3
        L_0x03c6:
            X.0x5 r1 = r2.A06
            if (r1 == 0) goto L_0x03d3
            X.0x5 r0 = r10.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03d8
            return r3
        L_0x03d3:
            X.0x5 r0 = r10.A06
            if (r0 == 0) goto L_0x03d8
            return r3
        L_0x03d8:
            java.lang.Integer r1 = r2.A09
            if (r1 == 0) goto L_0x03e5
            java.lang.Integer r0 = r10.A09
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03ea
            return r3
        L_0x03e5:
            java.lang.Integer r0 = r10.A09
            if (r0 == 0) goto L_0x03ea
            return r3
        L_0x03ea:
            X.17d r1 = r2.A05
            if (r1 == 0) goto L_0x03f7
            X.17d r0 = r10.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x03fc
            return r3
        L_0x03f7:
            X.17d r0 = r10.A05
            if (r0 == 0) goto L_0x03fc
            return r3
        L_0x03fc:
            X.0vK r1 = r2.A03
            X.0vK r0 = r10.A03
            if (r1 == 0) goto L_0x0409
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x055e
            return r3
        L_0x0409:
            if (r0 == 0) goto L_0x055e
            return r3
        L_0x040c:
            r2 = r9
            X.1AO r2 = (X.AnonymousClass1AO) r2
            r4 = 1
            if (r2 == r10) goto L_0x055e
            r3 = 0
            if (r10 == 0) goto L_0x04ca
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 != r0) goto L_0x04ca
            X.1AO r10 = (X.AnonymousClass1AO) r10
            X.248 r1 = r2.A06
            if (r1 == 0) goto L_0x042e
            X.248 r0 = r10.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0433
            return r3
        L_0x042e:
            X.248 r0 = r10.A06
            if (r0 == 0) goto L_0x0433
            return r3
        L_0x0433:
            X.654 r1 = r2.A04
            if (r1 == 0) goto L_0x0440
            X.654 r0 = r10.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0445
            return r3
        L_0x0440:
            X.654 r0 = r10.A04
            if (r0 == 0) goto L_0x0445
            return r3
        L_0x0445:
            X.5CT r0 = r2.A07
            X.4cY r1 = r0.ttrcRenderTracker
            if (r1 == 0) goto L_0x0456
            X.5CT r0 = r10.A07
            X.4cY r0 = r0.ttrcRenderTracker
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x045d
            return r3
        L_0x0456:
            X.5CT r0 = r10.A07
            X.4cY r0 = r0.ttrcRenderTracker
            if (r0 == 0) goto L_0x045d
            return r3
        L_0x045d:
            X.5CT r0 = r2.A07
            java.util.concurrent.atomic.AtomicReference r1 = r0.ttrcTraceRef
            if (r1 == 0) goto L_0x046e
            X.5CT r0 = r10.A07
            java.util.concurrent.atomic.AtomicReference r0 = r0.ttrcTraceRef
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0475
            return r3
        L_0x046e:
            X.5CT r0 = r10.A07
            java.util.concurrent.atomic.AtomicReference r0 = r0.ttrcTraceRef
            if (r0 == 0) goto L_0x0475
            return r3
        L_0x0475:
            X.1r0 r1 = r2.A03
            if (r1 == 0) goto L_0x0482
            X.1r0 r0 = r10.A03
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0487
            return r3
        L_0x0482:
            X.1r0 r0 = r10.A03
            if (r0 == 0) goto L_0x0487
            return r3
        L_0x0487:
            X.0oy r1 = r2.A05
            if (r1 == 0) goto L_0x0494
            X.0oy r0 = r10.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0499
            return r3
        L_0x0494:
            X.0oy r0 = r10.A05
            if (r0 == 0) goto L_0x0499
            return r3
        L_0x0499:
            X.BiW r1 = r2.A00
            X.BiW r0 = r10.A00
            if (r1 == 0) goto L_0x04a6
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x055e
            return r3
        L_0x04a6:
            if (r0 == 0) goto L_0x055e
            return r3
        L_0x04a9:
            r2 = r9
            X.0wQ r2 = (X.C16060wQ) r2
            r4 = 1
            if (r2 == r10) goto L_0x055e
            r3 = 0
            if (r10 == 0) goto L_0x04ca
            java.lang.Class r1 = r2.getClass()
            java.lang.Class r0 = r10.getClass()
            if (r1 != r0) goto L_0x04ca
            X.0wQ r10 = (X.C16060wQ) r10
            com.facebook.mig.scheme.interfaces.MigColorScheme r1 = r2.A04
            if (r1 == 0) goto L_0x04cb
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A04
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04d0
        L_0x04ca:
            return r3
        L_0x04cb:
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r10.A04
            if (r0 == 0) goto L_0x04d0
            return r3
        L_0x04d0:
            X.0vU r1 = r2.A07
            if (r1 == 0) goto L_0x04dd
            X.0vU r0 = r10.A07
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04e2
            return r3
        L_0x04dd:
            X.0vU r0 = r10.A07
            if (r0 == 0) goto L_0x04e2
            return r3
        L_0x04e2:
            X.0v8 r1 = r2.A02
            if (r1 == 0) goto L_0x04ef
            X.0v8 r0 = r10.A02
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x04f4
            return r3
        L_0x04ef:
            X.0v8 r0 = r10.A02
            if (r0 == 0) goto L_0x04f4
            return r3
        L_0x04f4:
            java.util.List r1 = r2.A09
            if (r1 == 0) goto L_0x0501
            java.util.List r0 = r10.A09
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0506
            return r3
        L_0x0501:
            java.util.List r0 = r10.A09
            if (r0 == 0) goto L_0x0506
            return r3
        L_0x0506:
            X.0x5 r1 = r2.A06
            if (r1 == 0) goto L_0x0513
            X.0x5 r0 = r10.A06
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0518
            return r3
        L_0x0513:
            X.0x5 r0 = r10.A06
            if (r0 == 0) goto L_0x0518
            return r3
        L_0x0518:
            java.lang.Integer r1 = r2.A08
            if (r1 == 0) goto L_0x0525
            java.lang.Integer r0 = r10.A08
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x052a
            return r3
        L_0x0525:
            java.lang.Integer r0 = r10.A08
            if (r0 == 0) goto L_0x052a
            return r3
        L_0x052a:
            X.17d r1 = r2.A05
            if (r1 == 0) goto L_0x0537
            X.17d r0 = r10.A05
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x053c
            return r3
        L_0x0537:
            X.17d r0 = r10.A05
            if (r0 == 0) goto L_0x053c
            return r3
        L_0x053c:
            X.0Uv r1 = r2.A00
            if (r1 == 0) goto L_0x0549
            X.0Uv r0 = r10.A00
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x054e
            return r3
        L_0x0549:
            X.0Uv r0 = r10.A00
            if (r0 == 0) goto L_0x054e
            return r3
        L_0x054e:
            X.0vK r1 = r2.A03
            X.0vK r0 = r10.A03
            if (r1 == 0) goto L_0x055b
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x055e
            return r3
        L_0x055b:
            if (r0 == 0) goto L_0x055e
            return r3
        L_0x055e:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16070wR.A0U(X.0wR):boolean");
    }

    public /* bridge */ /* synthetic */ boolean BEe(Object obj) {
        if (this instanceof AnonymousClass1AO) {
            return ((AnonymousClass1AO) this).A0U((C16070wR) obj);
        }
        if (this instanceof C35291qz) {
            return ((C35291qz) this).A0U((C16070wR) obj);
        }
        if (this instanceof C16060wQ) {
            return ((C16060wQ) this).A0U((C16070wR) obj);
        }
        if (this instanceof AnonymousClass1FF) {
            return ((AnonymousClass1FF) this).A0U((C16070wR) obj);
        }
        if (this instanceof C20271Bt) {
            return ((C20271Bt) this).A0U((C16070wR) obj);
        }
        if (this instanceof C20291Bv) {
            return ((C20291Bv) this).A0U((C16070wR) obj);
        }
        if (this instanceof AnonymousClass1GV) {
            return ((AnonymousClass1GV) this).A0U((C16070wR) obj);
        }
        if (!(this instanceof AnonymousClass1AM)) {
            return A0U((C16070wR) obj);
        }
        return ((AnonymousClass1AM) this).A0U((C16070wR) obj);
    }

    public C16070wR(String str) {
        this.A0A = str;
        this.A05 = str;
    }

    public C16070wR A0T(boolean z) {
        try {
            C16070wR r1 = (C16070wR) super.clone();
            if (!z) {
                if (r1.A06 != null) {
                    r1.A06 = new ArrayList();
                }
                r1.A00 = 0;
                r1.A08 = false;
                r1.A07 = null;
            }
            return r1;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
