package X;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0uw  reason: invalid class name and case insensitive filesystem */
public final class C15230uw {
    public Context A00;
    public AnonymousClass0UN A01;
    private NotificationManager A02;
    private C15240ux A03;
    public final C15090ui A04;
    private final C15100uj A05;

    public static Intent A00(Context context) {
        Intent intent = new Intent();
        int i = Build.VERSION.SDK_INT;
        if (i > 25) {
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
            return intent;
        } else if (i >= 21) {
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", context.getPackageName());
            intent.putExtra("app_uid", context.getApplicationInfo().uid);
            return intent;
        } else {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.addCategory(AnonymousClass24B.$const$string(54));
            intent.setData(Uri.parse(AnonymousClass08S.A0J("package:", context.getPackageName())));
            return intent;
        }
    }

    public static final C15230uw A01(AnonymousClass1XY r2) {
        return new C15230uw(r2, AnonymousClass1YA.A00(r2));
    }

    public static final C15230uw A02(AnonymousClass1XY r2) {
        return new C15230uw(r2, AnonymousClass1YA.A00(r2));
    }

    public int A03() {
        NotificationManager notificationManager;
        if (Build.VERSION.SDK_INT >= 26 && (notificationManager = this.A02) != null && this.A04.A03()) {
            for (NotificationChannel next : notificationManager.getNotificationChannels()) {
                String A012 = C15010uZ.A01(next.getId());
                if (A012 != null && A012.equals("messenger_orca_050_messaging")) {
                    return C15010uZ.A00(next);
                }
            }
        }
        return -1;
    }

    public Map A04() {
        HashMap hashMap = new HashMap();
        NotificationManager notificationManager = this.A02;
        if (notificationManager != null && this.A04.A03()) {
            HashMap hashMap2 = new HashMap();
            for (NotificationChannel next : notificationManager.getNotificationChannels()) {
                String A012 = C15010uZ.A01(next.getId());
                if (A012 != null) {
                    hashMap2.put(A012, Integer.toString(C15010uZ.A00(next)));
                }
            }
            hashMap.put("channels_mask", ((JsonNode) ((AnonymousClass0jJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AmL, this.A01)).convertValue(hashMap2, JsonNode.class)).toString());
            ImmutableMap copyOf = ImmutableMap.copyOf(((C32431lk) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Ady, this.A05.A00)).A09);
            ObjectNode createObjectNode = ((AnonymousClass0jJ) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AmL, this.A01)).createObjectNode();
            for (Map.Entry entry : copyOf.entrySet()) {
                createObjectNode.put(Integer.toString(((Integer) entry.getKey()).intValue()), (String) entry.getValue());
            }
            hashMap.put("channels_mapping", createObjectNode.toString());
        }
        return hashMap;
    }

    public boolean A05() {
        int i = Build.VERSION.SDK_INT;
        if (i < 19) {
            return true;
        }
        if (i > 24) {
            NotificationManager notificationManager = this.A02;
            if (notificationManager != null) {
                return notificationManager.areNotificationsEnabled();
            }
            return true;
        }
        C15240ux r0 = this.A03;
        if (r0 != null) {
            return r0.A04();
        }
        return true;
    }

    private C15230uw(AnonymousClass1XY r3, Context context) {
        this.A01 = new AnonymousClass0UN(1, r3);
        this.A05 = C15100uj.A03(r3);
        this.A04 = C15090ui.A00(r3);
        this.A00 = context;
        try {
            if (Build.VERSION.SDK_INT > 24) {
                this.A02 = (NotificationManager) context.getSystemService("notification");
            } else {
                this.A03 = new C15240ux(context);
            }
        } catch (NoClassDefFoundError unused) {
            this.A02 = null;
            this.A03 = null;
        }
    }
}
