package X;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0ja  reason: invalid class name and case insensitive filesystem */
public abstract class C10120ja {
    public final C10030jR _type;

    public abstract C28311eb bindingsForBeanType();

    public abstract C183512m findAnyGetter();

    public abstract C29141fw findAnySetter();

    public abstract Map findBackReferenceProperties();

    public abstract C29151fx findDefaultConstructor();

    public abstract C422428v findDeserializationConverter();

    public abstract CX6 findExpectedFormat(CX6 cx6);

    public abstract Method findFactoryMethod(Class... clsArr);

    public abstract Map findInjectables();

    public abstract C29141fw findJsonValueMethod();

    public abstract C29141fw findMethod(String str, Class[] clsArr);

    public abstract Class findPOJOBuilder();

    public abstract BM3 findPOJOBuilderConfig();

    public abstract List findProperties();

    public abstract C422428v findSerializationConverter();

    public abstract AnonymousClass0oC findSerializationInclusion(AnonymousClass0oC r1);

    public abstract Constructor findSingleArgConstructor(Class... clsArr);

    public abstract C10100jY getClassAnnotations();

    public abstract C10070jV getClassInfo();

    public abstract List getConstructors();

    public abstract List getFactoryMethods();

    public abstract Set getIgnoredPropertyNames();

    public abstract CZj getObjectIdInfo();

    public abstract boolean hasKnownClassAnnotations();

    public abstract Object instantiateBean(boolean z);

    public abstract C10030jR resolveType(Type type);

    public C10120ja(C10030jR r1) {
        this._type = r1;
    }
}
