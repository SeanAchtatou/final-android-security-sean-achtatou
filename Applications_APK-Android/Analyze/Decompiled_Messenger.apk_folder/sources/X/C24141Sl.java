package X;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import io.card.payment.BuildConfig;

/* renamed from: X.1Sl  reason: invalid class name and case insensitive filesystem */
public final class C24141Sl implements C35731rh {
    public final /* synthetic */ AnonymousClass0p4 A00;
    public final /* synthetic */ AnonymousClass1BB A01;

    public C24141Sl(AnonymousClass1BB r1, AnonymousClass0p4 r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public void Bbj(String str) {
        String B4K = this.A01.A03.A02.B4K(1153767093478097058L, BuildConfig.FLAVOR, AnonymousClass0XE.A06);
        String B4C = this.A01.A03.A02.B4C(1153767093478228131L);
        MigColorScheme migColorScheme = (MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, this.A01.A00);
        AnonymousClass1C0 r1 = this.A01.A04;
        Context context = this.A00.A09;
        migColorScheme.B9m();
        C80043rl A03 = r1.A03(context);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (!C06850cB.A0A(B4C)) {
            spannableStringBuilder.append((CharSequence) AnonymousClass08S.A0J(B4C, " "));
            spannableStringBuilder.setSpan(new ForegroundColorSpan(this.A00.A01(migColorScheme.Aeb().Aha())), 0, CoM.A00(B4C), 0);
        }
        spannableStringBuilder.append((CharSequence) B4K);
        A03.A0J(spannableStringBuilder);
        A03.A0K(AnonymousClass07B.A01);
        A03.A0O((int) this.A01.A03.A02.At0(564113894605522L));
        C108385Fy.A00(this.A00, new AnonymousClass5G0(A03, 0, 0), AnonymousClass14L.A01("column_layout_key", "row1_layout_key", "row2_layout_key", "tooltip_anchor_key"), 0, 0);
    }
}
