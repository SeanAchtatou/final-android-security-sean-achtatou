package X;

/* renamed from: X.1Hs  reason: invalid class name and case insensitive filesystem */
public final class C21541Hs {
    private final AnonymousClass10N A00;
    private final AnonymousClass1GA A01;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0020, code lost:
        if (X.AnonymousClass07c.enableRenderInfoDebugging == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C21681Ih A00(java.lang.Object r4, int r5) {
        /*
            r3 = this;
            X.10N r2 = r3.A00
            X.1IX r1 = new X.1IX
            r1.<init>()
            r1.A00 = r5
            r1.A01 = r4
            X.0zV r0 = r2.A00
            X.0zT r0 = r0.Alq()
            java.lang.Object r2 = r0.AXa(r2, r1)
            X.1Ih r2 = (X.C21681Ih) r2
            if (r2 == 0) goto L_0x0031
            boolean r0 = X.AnonymousClass07c.isDebugModeEnabled
            if (r0 == 0) goto L_0x0022
            boolean r1 = X.AnonymousClass07c.enableRenderInfoDebugging
            r0 = 1
            if (r1 != 0) goto L_0x0023
        L_0x0022:
            r0 = 0
        L_0x0023:
            if (r0 == 0) goto L_0x0030
            X.1GA r0 = r3.A01
            X.0wR r1 = r0.A0K()
            java.lang.String r0 = "SONAR_SECTIONS_DEBUG_INFO"
            r2.AMk(r0, r1)
        L_0x0030:
            return r2
        L_0x0031:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Method annotated with '@OnEvent(RenderEvent.class)' is not allowed to return 'null'."
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21541Hs.A00(java.lang.Object, int):X.1Ih");
    }

    public C21541Hs(AnonymousClass10N r1, AnonymousClass1GA r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
