package X;

import java.lang.annotation.Annotation;
import java.util.HashMap;

/* renamed from: X.0jX  reason: invalid class name and case insensitive filesystem */
public final class C10090jX implements C10100jY {
    public HashMap _annotations;

    public void add(Annotation annotation) {
        if (this._annotations == null) {
            this._annotations = new HashMap();
        }
        this._annotations.put(annotation.annotationType(), annotation);
    }

    public void addIfNotPresent(Annotation annotation) {
        HashMap hashMap = this._annotations;
        if (hashMap == null || !hashMap.containsKey(annotation.annotationType())) {
            if (this._annotations == null) {
                this._annotations = new HashMap();
            }
            this._annotations.put(annotation.annotationType(), annotation);
        }
    }

    public String toString() {
        HashMap hashMap = this._annotations;
        if (hashMap == null) {
            return "[null]";
        }
        return hashMap.toString();
    }

    public C10090jX() {
    }

    public C10090jX(HashMap hashMap) {
        this._annotations = hashMap;
    }
}
