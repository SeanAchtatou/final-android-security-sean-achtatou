package X;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Tw  reason: invalid class name and case insensitive filesystem */
public final class C24501Tw {
    public static final Class A0E = C24501Tw.class;
    private static volatile C24501Tw A0F;
    public AnonymousClass38R A00 = null;
    public boolean A01;
    public final AnonymousClass069 A02;
    public final C04700Vu A03;
    public final Object A04 = new Object();
    public final Collection A05 = new C05180Xy(4);
    public final List A06 = new CopyOnWriteArrayList();
    public final List A07 = new CopyOnWriteArrayList();
    public final Map A08 = new HashMap();
    public final Map A09 = new HashMap(16);
    private final ScheduledExecutorService A0A;
    public volatile long A0B = 0;
    public volatile boolean A0C;
    public volatile boolean A0D = false;

    public static void A03(C24501Tw r5, Long l) {
        long j;
        synchronized (r5) {
            if (!r5.A01 && r5.A0D) {
                SystemClock.elapsedRealtime();
                if (l != null) {
                    j = l.longValue();
                } else {
                    j = r5.A0B;
                }
                r5.A01 = true;
                r5.A0A.schedule(new AnonymousClass5AG(r5), j, TimeUnit.MILLISECONDS);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        java.lang.Thread.currentThread().interrupt();
        X.C010708t.A0E(X.C24501Tw.A0E, r3, "Error while trying to initialize shared prefs", new java.lang.Object[0]);
        com.google.common.base.Preconditions.checkState(r8.A0C, "Interrupted before FbSharedPreferencesCache initialized");
        r0 = 1557050446;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0073, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0074, code lost:
        X.C005505z.A00(-148160351);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Object A04(X.AnonymousClass1Y7 r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            java.lang.String r1 = "getInternal.blockUntilInitialized"
            r0 = 424379071(0x194b82bf, float:1.0521258E-23)
            X.C005505z.A03(r1, r0)     // Catch:{ InterruptedException -> 0x0036 }
            r7 = r8
            monitor-enter(r7)     // Catch:{ InterruptedException -> 0x0036 }
            X.069 r0 = r8.A02     // Catch:{ all -> 0x0033 }
            long r5 = r0.now()     // Catch:{ all -> 0x0033 }
        L_0x0011:
            boolean r0 = r8.A0C     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x002e
            r3 = 300000(0x493e0, double:1.482197E-318)
            r8.wait(r3)     // Catch:{ all -> 0x0033 }
            X.069 r0 = r8.A02     // Catch:{ all -> 0x0033 }
            long r1 = r0.now()     // Catch:{ all -> 0x0033 }
            long r1 = r1 - r5
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0011
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0033 }
            java.lang.String r0 = "Timed out waiting for shared prefs to initialize"
            r1.<init>(r0)     // Catch:{ all -> 0x0033 }
            throw r1     // Catch:{ all -> 0x0033 }
        L_0x002e:
            monitor-exit(r7)     // Catch:{ InterruptedException -> 0x0036 }
            r0 = 590563332(0x23334804, float:9.718859E-18)
            goto L_0x0052
        L_0x0033:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ InterruptedException -> 0x0036 }
            throw r0     // Catch:{ InterruptedException -> 0x0036 }
        L_0x0036:
            r3 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0073 }
            r0.interrupt()     // Catch:{ all -> 0x0073 }
            java.lang.Class r2 = X.C24501Tw.A0E     // Catch:{ all -> 0x0073 }
            java.lang.String r1 = "Error while trying to initialize shared prefs"
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0073 }
            X.C010708t.A0E(r2, r3, r1, r0)     // Catch:{ all -> 0x0073 }
            boolean r1 = r8.A0C     // Catch:{ all -> 0x0073 }
            java.lang.String r0 = "Interrupted before FbSharedPreferencesCache initialized"
            com.google.common.base.Preconditions.checkState(r1, r0)     // Catch:{ all -> 0x0073 }
            r0 = 1557050446(0x5cceb44e, float:4.65456738E17)
        L_0x0052:
            X.C005505z.A00(r0)     // Catch:{ all -> 0x007b }
            java.lang.String r1 = "getInternal.getCachedValue"
            r0 = -1482372455(0xffffffffa7a4ca99, float:-4.57388E-15)
            X.C005505z.A03(r1, r0)     // Catch:{ all -> 0x006b }
            java.util.Map r0 = r8.A08     // Catch:{ all -> 0x006b }
            java.lang.Object r1 = r0.get(r9)     // Catch:{ all -> 0x006b }
            r0 = 289765992(0x11457a68, float:1.5578279E-28)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x007b }
            monitor-exit(r8)
            return r1
        L_0x006b:
            r1 = move-exception
            r0 = -504749303(0xffffffffe1ea2309, float:-5.3988283E20)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x007b }
            goto L_0x007a
        L_0x0073:
            r1 = move-exception
            r0 = -148160351(0xfffffffff72b40a1, float:-3.4734125E33)
            X.C005505z.A00(r0)     // Catch:{ all -> 0x007b }
        L_0x007a:
            throw r1     // Catch:{ all -> 0x007b }
        L_0x007b:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24501Tw.A04(X.1Y7):java.lang.Object");
    }

    public static final C24501Tw A00(AnonymousClass1XY r8) {
        if (A0F == null) {
            synchronized (C24501Tw.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0F, r8);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r8.getApplicationInjector();
                        C04690Vt A003 = C04690Vt.A00(applicationInjector);
                        AnonymousClass0X5 r2 = new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A2K);
                        ScheduledExecutorService A0d = AnonymousClass0UX.A0d(applicationInjector);
                        AnonymousClass069 A032 = AnonymousClass067.A03(applicationInjector);
                        AnonymousClass0WA.A00(applicationInjector);
                        A0F = new C24501Tw(A003, r2, A0d, A032);
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0F;
    }

    public static void A02(C24501Tw r3, AnonymousClass1Y7 r4) {
        AnonymousClass38R r0 = r3.A00;
        if (r0 != null) {
            Iterator it = A01(r4).iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                if (r0.A03.containsKey(str)) {
                    r0 = (AnonymousClass38R) r0.A03.get(str);
                } else {
                    AnonymousClass38R r1 = new AnonymousClass38R();
                    r1.A02 = str;
                    r1.A01 = r0;
                    r0.A03.put(str, r1);
                    r0 = r1;
                }
            }
            r0.A00 = r4;
        }
    }

    private C24501Tw(C04700Vu r3, Set set, ScheduledExecutorService scheduledExecutorService, AnonymousClass069 r6) {
        this.A03 = r3;
        this.A07.addAll(set);
        this.A0A = scheduledExecutorService;
        this.A02 = r6;
    }

    public static ArrayList A01(AnonymousClass1Y7 r5) {
        String[] split = r5.A05().split("/");
        ArrayList arrayList = new ArrayList();
        for (String str : split) {
            if (!str.isEmpty()) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }
}
