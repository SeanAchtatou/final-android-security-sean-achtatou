package X;

import android.content.ContentValues;
import com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tE  reason: invalid class name and case insensitive filesystem */
public final class C36501tE {
    private static volatile C36501tE A07;
    public final AnonymousClass0m6 A00;
    private final C189216c A01;
    private final C36521tG A02;
    private final C36511tF A03;
    private final C193917y A04;
    private final C25051Yd A05;
    private final Map A06 = new HashMap();

    public boolean A04(ThreadKey threadKey) {
        if (threadKey == null || !this.A05.Aem(283326109124949L)) {
            return false;
        }
        this.A03.A03();
        ThreadSummary A0R = this.A00.A0R(threadKey);
        if (A0R != null && GraphQLMessageThreadCannotReplyReason.A06.equals(A0R.A0H)) {
            this.A06.remove(threadKey);
            AnonymousClass0m6 r2 = this.A00;
            C17920zh A002 = ThreadSummary.A00();
            A002.A02(A0R);
            A002.A0G = null;
            r2.A0b(A002.A00());
        }
        this.A01.A0G(threadKey, C22298Ase.$const$string(AnonymousClass1Y3.A1J));
        return true;
    }

    public boolean A05(ThreadKey threadKey, boolean z) {
        if (threadKey != null) {
            this.A06.put(threadKey, Boolean.valueOf(z));
            ThreadSummary A0R = this.A00.A0R(threadKey);
            if (A0R != null) {
                if (!A01(z, A0R)) {
                    return true;
                }
                C17920zh A002 = ThreadSummary.A00();
                A002.A02(A0R);
                if (!z) {
                    A002.A0G = null;
                } else {
                    A002.A0G = GraphQLMessageThreadCannotReplyReason.A06;
                }
                this.A00.A0b(A002.A00());
                this.A01.A0D(threadKey, null, "TincanDisabledUtil");
                return true;
            }
        }
        return false;
    }

    public static final C36501tE A00(AnonymousClass1XY r4) {
        if (A07 == null) {
            synchronized (C36501tE.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r4);
                if (A002 != null) {
                    try {
                        A07 = new C36501tE(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    private static boolean A01(boolean z, ThreadSummary threadSummary) {
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason;
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason2 = threadSummary.A0H;
        if (GraphQLMessageThreadCannotReplyReason.A01.equals(graphQLMessageThreadCannotReplyReason2) || ((graphQLMessageThreadCannotReplyReason2 != (graphQLMessageThreadCannotReplyReason = GraphQLMessageThreadCannotReplyReason.A06) && !z) || (graphQLMessageThreadCannotReplyReason2 == graphQLMessageThreadCannotReplyReason && z))) {
            return false;
        }
        return true;
    }

    public GraphQLMessageThreadCannotReplyReason A02(ThreadKey threadKey) {
        ThreadSummary A0R = this.A00.A0R(threadKey);
        if (this.A06.containsKey(threadKey)) {
            boolean booleanValue = ((Boolean) this.A06.get(threadKey)).booleanValue();
            if (A0R != null && !A01(booleanValue, A0R)) {
                return A0R.A0H;
            }
            if (booleanValue) {
                return GraphQLMessageThreadCannotReplyReason.A06;
            }
        } else if (A0R != null) {
            return A0R.A0H;
        }
        return GraphQLMessageThreadCannotReplyReason.A07;
    }

    public void A03(boolean z) {
        ImmutableSet A062 = this.A04.A06();
        if (!A062.isEmpty()) {
            C36521tG r2 = this.A02;
            ContentValues contentValues = new ContentValues();
            contentValues.put(C193717w.A01.A00, Integer.valueOf(z ? 1 : 0));
            ((AnonymousClass183) r2.A0A.get()).A01().update("threads", contentValues, null, null);
            C24971Xv it = A062.iterator();
            while (it.hasNext()) {
                this.A00.A0a((ThreadKey) it.next(), z);
            }
            this.A01.A0K(ImmutableList.copyOf((Collection) A062), "TincanDisabledUtil");
        }
    }

    private C36501tE(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0m2.A02(r2);
        this.A01 = C189216c.A02(r2);
        this.A05 = AnonymousClass0WT.A00(r2);
        this.A03 = C36511tF.A00(r2);
        this.A04 = C193917y.A00(r2);
        this.A02 = C36521tG.A03(r2);
    }
}
