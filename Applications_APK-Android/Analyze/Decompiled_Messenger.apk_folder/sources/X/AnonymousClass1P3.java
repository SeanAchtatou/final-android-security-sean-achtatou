package X;

import android.app.ActivityManager;
import java.util.Arrays;

/* renamed from: X.1P3  reason: invalid class name */
public final class AnonymousClass1P3 implements C23111Og {
    private AnonymousClass0UN A00;
    public final ActivityManager A01;
    public final AnonymousClass1P7 A02;
    public final AnonymousClass1P4 A03;
    public final C23271Ow A04;
    public final C25051Yd A05;

    public static final AnonymousClass1P3 A00(AnonymousClass1XY r3) {
        return new AnonymousClass1P3(r3, C04490Ux.A04(r3), AnonymousClass0WT.A00(r3));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01ca, code lost:
        if (r3.A02 == 0) goto L_0x01cc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get() {
        /*
            r22 = this;
            r0 = r22
            X.1Yd r5 = r0.A05
            r3 = 1126488317689910(0x4008900050036, double:5.565591782120847E-309)
            r1 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r10 = r5.Akj(r3, r1)
            X.1Ow r1 = r0.A04
            double r8 = r1.AvD()
            X.1P7 r1 = r0.A02
            X.1ik r5 = r1.get()
            X.1Yd r4 = r0.A05
            r1 = 844579548954640(0x3002400000010, double:4.17277740318583E-309)
            java.lang.String r3 = ""
            java.lang.String r2 = r4.B4E(r1, r3)
            java.lang.String r1 = "hardcoded_size"
            boolean r1 = r1.equals(r2)
            r4 = 0
            if (r1 == 0) goto L_0x0062
            X.1Yd r3 = r0.A05
            r1 = 563104572309696(0x20024000100c0, double:2.78210624194349E-309)
            int r15 = r3.AqL(r1, r4)
            if (r15 <= 0) goto L_0x018e
            r1 = 1048576(0x100000, float:1.469368E-39)
            int r15 = r15 * r1
            X.1ik r14 = new X.1ik
            int r1 = r5.A02
            if (r15 <= r1) goto L_0x0048
            r15 = r1
        L_0x0048:
            int r4 = r5.A04
            int r3 = r5.A03
            int r2 = r5.A01
            X.1Ow r1 = r0.A04
            long r20 = r1.Ago()
            r16 = 256(0x100, float:3.59E-43)
            r18 = r3
            r19 = r2
            r17 = r4
            r14.<init>(r15, r16, r17, r18, r19, r20)
        L_0x005f:
            if (r14 == 0) goto L_0x0191
            return r14
        L_0x0062:
            java.lang.String r1 = "messenger"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x00a2
            X.1Yd r3 = r0.A05
            r1 = 1126054525861908(0x4002400030014, double:5.56344856572422E-309)
            double r12 = r3.Aki(r1)
            r6 = 0
            r2 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r1 = (r12 > r6 ? 1 : (r12 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x0081
            int r1 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x0083
        L_0x0081:
            r12 = 4607182418800017408(0x3ff0000000000000, double:1.0)
        L_0x0083:
            X.1ik r14 = new X.1ik
            int r1 = r5.A02
            double r1 = (double) r1
            double r1 = r1 * r12
            int r15 = (int) r1
            int r4 = r5.A04
            int r3 = r5.A03
            int r2 = r5.A01
            X.1Ow r1 = r0.A04
            long r20 = r1.Ago()
            r16 = 256(0x100, float:3.59E-43)
            r18 = r3
            r19 = r2
            r17 = r4
            r14.<init>(r15, r16, r17, r18, r19, r20)
            goto L_0x005f
        L_0x00a2:
            java.lang.String r1 = "messenger_plus_fb4a"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x00b0
            r12 = 4603579539098121011(0x3fe3333333333333, double:0.6)
            goto L_0x0083
        L_0x00b0:
            java.lang.String r1 = "num_fullscreen_images"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x00fb
            X.1Yd r3 = r0.A05
            r1 = 563104572309696(0x20024000100c0, double:2.78210624194349E-309)
            int r3 = r3.AqL(r1, r4)
            android.content.res.Resources r1 = android.content.res.Resources.getSystem()
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()
            int r2 = r1.widthPixels
            android.content.res.Resources r1 = android.content.res.Resources.getSystem()
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()
            int r1 = r1.heightPixels
            int r2 = r2 * r1
            int r15 = r2 << 2
            int r15 = r15 * r3
            X.1ik r14 = new X.1ik
            int r1 = r5.A02
            if (r15 <= r1) goto L_0x00e2
            r15 = r1
        L_0x00e2:
            int r4 = r5.A04
            int r3 = r5.A03
            int r2 = r5.A01
            X.1Ow r1 = r0.A04
            long r20 = r1.Ago()
            r16 = 256(0x100, float:3.59E-43)
            r18 = r3
            r19 = r2
            r17 = r4
            r14.<init>(r15, r16, r17, r18, r19, r20)
            goto L_0x005f
        L_0x00fb:
            java.lang.String r1 = "total_memory_multiplier"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0159
            X.1Yd r3 = r0.A05
            r1 = 1126054525796371(0x4002400020013, double:5.563448565400427E-309)
            double r6 = r3.Aki(r1)
            r2 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            int r1 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x011c
            r6 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
        L_0x011c:
            r2 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            int r1 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x012a
            r6 = 4591870180066957722(0x3fb999999999999a, double:0.1)
        L_0x012a:
            android.app.ActivityManager$MemoryInfo r2 = new android.app.ActivityManager$MemoryInfo
            r2.<init>()
            android.app.ActivityManager r1 = r0.A01
            r1.getMemoryInfo(r2)
            long r3 = r2.totalMem
            double r1 = (double) r3
            double r1 = r1 * r6
            int r15 = (int) r1
            X.1ik r14 = new X.1ik
            int r1 = r5.A02
            if (r15 <= r1) goto L_0x0140
            r15 = r1
        L_0x0140:
            int r4 = r5.A04
            int r3 = r5.A03
            int r2 = r5.A01
            X.1Ow r1 = r0.A04
            long r20 = r1.Ago()
            r16 = 256(0x100, float:3.59E-43)
            r18 = r3
            r19 = r2
            r17 = r4
            r14.<init>(r15, r16, r17, r18, r19, r20)
            goto L_0x005f
        L_0x0159:
            java.lang.String r1 = "adaptive_experiment"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x018e
            X.1P4 r1 = r0.A03
            X.1ik r2 = r1.get()
            int r1 = r2.A02
            if (r1 != 0) goto L_0x0171
            X.1P7 r1 = r0.A02
            X.1ik r2 = r1.get()
        L_0x0171:
            X.1ik r14 = new X.1ik
            int r15 = r2.A02
            int r4 = r2.A04
            int r3 = r2.A03
            int r2 = r2.A01
            X.1Ow r1 = r0.A04
            long r20 = r1.Ago()
            r16 = 256(0x100, float:3.59E-43)
            r18 = r3
            r19 = r2
            r17 = r4
            r14.<init>(r15, r16, r17, r18, r19, r20)
            goto L_0x005f
        L_0x018e:
            r14 = 0
            goto L_0x005f
        L_0x0191:
            X.1Yd r3 = r0.A05
            r1 = 282063388607563(0x100890015044b, double:1.39357830260566E-309)
            boolean r1 = r3.Aem(r1)
            if (r1 == 0) goto L_0x01cc
            int r2 = X.AnonymousClass1Y3.BM4
            X.0UN r1 = r0.A00
            r4 = 0
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.8ot r3 = (X.C188418ot) r3
            X.1Yd r12 = r0.A05
            r13 = 1126488318869560(0x4008900170038, double:5.56559178794909E-309)
            r15 = 0
            X.0XE r17 = X.AnonymousClass0XE.A05
            double r1 = r12.Akl(r13, r15, r17)
            r3.A01 = r1
            int r2 = X.AnonymousClass1Y3.BM4
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.8ot r1 = (X.C188418ot) r1
            X.1ik r3 = r1.get()
            int r1 = r3.A02
            if (r1 != 0) goto L_0x01d2
        L_0x01cc:
            X.1P7 r1 = r0.A02
            X.1ik r3 = r1.get()
        L_0x01d2:
            X.1ik r4 = new X.1ik
            int r1 = r3.A02
            double r1 = (double) r1
            double r1 = r1 * r10
            double r1 = r1 * r8
            int r5 = (int) r1
            int r7 = r3.A04
            int r8 = r3.A03
            int r9 = r3.A01
            X.1Ow r0 = r0.A04
            long r10 = r0.Ago()
            r6 = 256(0x100, float:3.59E-43)
            r4.<init>(r5, r6, r7, r8, r9, r10)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1P3.get():java.lang.Object");
    }

    public AnonymousClass1P3(AnonymousClass1XY r8, ActivityManager activityManager, C25051Yd r10) {
        C23271Ow r4;
        this.A00 = new AnonymousClass0UN(1, r8);
        this.A03 = new AnonymousClass1P4(r8, C04490Ux.A04(r8));
        this.A01 = activityManager;
        this.A05 = r10;
        this.A02 = new AnonymousClass1P7(activityManager);
        C30801if r6 = new C30801if(r10, activityManager);
        switch (r6.A02.intValue()) {
            case 1:
                r4 = new C184738hl();
                break;
            case 2:
                r4 = new C184748hm(r6.A01, C30801if.A03, new AnonymousClass1P9(Runtime.getRuntime()));
                break;
            case 3:
                r4 = new C184748hm(r6.A01, C30801if.A04, new C188428ou(r6.A00));
                break;
            case 4:
                r4 = new C184728hk(Arrays.asList(new AnonymousClass1P8(r6.A01, new AnonymousClass1P9(Runtime.getRuntime())), new C184748hm(r6.A01, C30801if.A04, new C188428ou(r6.A00))));
                break;
            default:
                r4 = new AnonymousClass1P8(r6.A01, new AnonymousClass1P9(Runtime.getRuntime()));
                break;
        }
        this.A04 = r4;
    }
}
