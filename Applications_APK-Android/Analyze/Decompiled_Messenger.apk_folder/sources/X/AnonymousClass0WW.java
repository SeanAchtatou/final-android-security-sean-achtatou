package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0WW  reason: invalid class name */
public final class AnonymousClass0WW extends Enum {
    private static final /* synthetic */ AnonymousClass0WW[] A00;
    public static final AnonymousClass0WW A01;
    public static final AnonymousClass0WW A02;
    public static final AnonymousClass0WW A03;
    public static final AnonymousClass0WW A04;
    public static final AnonymousClass0WW A05;
    public static final AnonymousClass0WW A06;
    public static final AnonymousClass0WW A07;
    public static final AnonymousClass0WW A08;

    static {
        AnonymousClass0WW r2 = new AnonymousClass0WW("NO_DEPENDENCY", 0);
        A05 = r2;
        AnonymousClass0WW r3 = new AnonymousClass0WW("SHARED_PREFERENCES_INIT", 1);
        A06 = r3;
        AnonymousClass0WW r4 = new AnonymousClass0WW("GATEKEEPER_INIT", 2);
        A01 = r4;
        AnonymousClass0WW r5 = new AnonymousClass0WW("MOBILE_CONFIG_INIT", 3);
        A03 = r5;
        AnonymousClass0WW r6 = new AnonymousClass0WW("MOBILE_CONFIG_SESSIONLESS_INIT", 4);
        A04 = r6;
        AnonymousClass0WW r7 = new AnonymousClass0WW("STRING_RESOURCES_INIT", 5);
        A07 = r7;
        AnonymousClass0WW r8 = new AnonymousClass0WW("FRESH_FEED_CONFIG_INIT", 6);
        AnonymousClass0WW r9 = new AnonymousClass0WW("FEED_FETCH_INIT", 7);
        AnonymousClass0WW r10 = new AnonymousClass0WW("LANGUAGE_COMMON", 8);
        A02 = r10;
        AnonymousClass0WW r11 = new AnonymousClass0WW("LANGUAGE_SWITCHER", 9);
        AnonymousClass0WW r12 = new AnonymousClass0WW("UI_READY", 10);
        A08 = r12;
        A00 = new AnonymousClass0WW[]{r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12};
    }

    public static AnonymousClass0WW valueOf(String str) {
        return (AnonymousClass0WW) Enum.valueOf(AnonymousClass0WW.class, str);
    }

    public static AnonymousClass0WW[] values() {
        return (AnonymousClass0WW[]) A00.clone();
    }

    private AnonymousClass0WW(String str, int i) {
    }
}
