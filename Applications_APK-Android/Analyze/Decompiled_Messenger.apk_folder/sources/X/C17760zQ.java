package X;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

/* renamed from: X.0zQ  reason: invalid class name and case insensitive filesystem */
public abstract class C17760zQ {
    public static final Interpolator A00 = new AccelerateDecelerateInterpolator();
    public static final AnonymousClass1R9 A01;
    public static final Integer A02 = AnonymousClass07B.A01;

    static {
        C22451Lk r1 = new C22451Lk(C22461Ll.A02);
        A01 = r1;
    }

    public static AnonymousClass1R9 A00(int i) {
        return new AnonymousClass3Z8(i, A00);
    }

    public static C49302bz A01(Integer num, String str) {
        return new C49302bz(A03(num, true), str);
    }

    public static C49302bz A02(Integer num, String... strArr) {
        return new C49302bz(A03(num, false), strArr);
    }

    private static Integer A03(Integer num, boolean z) {
        if (num != AnonymousClass07B.A00) {
            Integer num2 = AnonymousClass07B.A01;
            if (num != num2) {
                throw new RuntimeException(AnonymousClass08S.A0J("Unhandled TransitionKeyType ", C21819AiH.A00(num)));
            } else if (!z) {
                return AnonymousClass07B.A0C;
            } else {
                return num2;
            }
        } else if (z) {
            return AnonymousClass07B.A0N;
        } else {
            return AnonymousClass07B.A0Y;
        }
    }
}
