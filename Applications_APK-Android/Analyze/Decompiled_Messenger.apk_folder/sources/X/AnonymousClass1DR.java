package X;

import com.facebook.payments.checkout.model.CheckoutCommonParams;
import com.facebook.payments.checkout.model.SimpleCheckoutData;

/* renamed from: X.1DR  reason: invalid class name */
public final class AnonymousClass1DR extends C06020ai {
    public final /* synthetic */ C54782mz A00;
    public final /* synthetic */ CheckoutCommonParams A01;
    public final /* synthetic */ SimpleCheckoutData A02;

    public AnonymousClass1DR(C54782mz r1, SimpleCheckoutData simpleCheckoutData, CheckoutCommonParams checkoutCommonParams) {
        this.A00 = r1;
        this.A02 = simpleCheckoutData;
        this.A01 = checkoutCommonParams;
    }
}
