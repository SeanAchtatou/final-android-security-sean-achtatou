package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/* renamed from: X.0Ak  reason: invalid class name and case insensitive filesystem */
public final class C01480Ak extends BroadcastReceiver {
    public final /* synthetic */ C01470Aj A00;

    public C01480Ak(C01470Aj r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(-1809977588);
        if (intent == null) {
            C000700l.A0D(intent, -1816910882, A01);
        } else if ("android.intent.action.SCREEN_ON".equals(intent.getAction()) || "android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            Boolean valueOf = Boolean.valueOf("android.intent.action.SCREEN_ON".equals(intent.getAction()));
            if (!valueOf.equals((Boolean) this.A00.A05.getAndSet(valueOf))) {
                this.A00.A04.set(SystemClock.elapsedRealtime());
                this.A00.A00.BLy(valueOf.booleanValue());
            }
            C000700l.A0D(intent, -620312679, A01);
        } else {
            C000700l.A0D(intent, 1277524002, A01);
        }
    }
}
