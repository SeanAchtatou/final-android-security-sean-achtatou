package X;

import java.util.Map;

/* renamed from: X.1MN  reason: invalid class name */
public interface AnonymousClass1MN {
    void Bk0(AnonymousClass1QK r1, String str, String str2);

    void Bk2(AnonymousClass1QK r1, String str, Map map);

    void Bk4(AnonymousClass1QK r1, String str, Throwable th, Map map);

    void Bk6(AnonymousClass1QK r1, String str, Map map);

    void Bk8(AnonymousClass1QK r1, String str);

    void Bt8(AnonymousClass1QK r1, String str, boolean z);

    boolean C3Q(AnonymousClass1QK r1, String str);
}
