package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.fbtrace.FbTraceNode;
import com.facebook.messaging.model.messages.Message;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.16c  reason: invalid class name and case insensitive filesystem */
public final class C189216c {
    private static volatile C189216c A05;
    public AnonymousClass0UN A00;
    public final C04310Tq A01;
    private final Context A02;
    private final AnonymousClass09P A03;
    private final C04310Tq A04;

    public static Bundle A00(C61242yZ r5, ThreadKey threadKey, long j, FbTraceNode fbTraceNode, long j2) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("broadcast_cause", r5);
        if (j >= 0) {
            bundle.putString("sound_trigger_identifier", r5.toString() + threadKey.toString() + ":" + j);
        }
        bundle.putParcelable("fbtrace_node", fbTraceNode);
        bundle.putLong("sequence_id", j2);
        return bundle;
    }

    public static Bundle A01(C61242yZ r3, String str) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("broadcast_cause", r3);
        if (str != null) {
            bundle.putString("sound_trigger_identifier", AnonymousClass08S.A0J(r3.toString(), str));
        }
        return bundle;
    }

    public static final C189216c A03(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C189216c.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C189216c(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    public static void A04(C189216c r2, Intent intent) {
        C04480Uv.A00(r2.A02).A04(intent);
        try {
            ((C20851Eb) r2.A04.get()).A02(intent, r2.A02);
        } catch (Throwable th) {
            r2.A03.softReport("MessagesBroadcaster", "broadcast to other processes failed", th);
        }
    }

    private void A06(String str, ArrayList arrayList, Bundle bundle, String str2) {
        Intent intent = new Intent();
        intent.setAction(str);
        if (!A07(this, arrayList, str)) {
            intent.putParcelableArrayListExtra("multiple_thread_keys", arrayList);
            if (bundle != null) {
                intent.putExtra("broadcast_extras", bundle);
            }
            intent.putExtra("calling_class", str2);
            A04(this, intent);
        }
    }

    public void A08() {
        A04(this, new Intent(C06680bu.A0H));
    }

    public void A09() {
        A04(this, new Intent(C06680bu.A0X));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0043, code lost:
        if (r0.A03 == null) goto L_0x0045;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(com.facebook.messaging.model.messages.Message r4, java.lang.String r5) {
        /*
            r3 = this;
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r0 = X.C06680bu.A0m
            r2.setAction(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A0U
            java.lang.String r0 = "thread_key"
            r2.putExtra(r0, r1)
            java.lang.String r0 = "calling_class"
            r2.putExtra(r0, r5)
            com.facebook.messaging.model.send.SendError r0 = r4.A0S
            java.lang.String r1 = r0.A06
            if (r1 == 0) goto L_0x0021
            java.lang.String r0 = "error_message"
            r2.putExtra(r0, r1)
        L_0x0021:
            com.facebook.messaging.model.send.SendError r0 = r4.A0S
            int r1 = r0.A00
            r0 = 150(0x96, float:2.1E-43)
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            r2.putExtra(r0, r1)
            java.lang.String r1 = r4.A0q
            java.lang.String r0 = "message_id"
            r2.putExtra(r0, r1)
            java.lang.String r1 = r4.A0w
            java.lang.String r0 = "offline_threading_id"
            r2.putExtra(r0, r1)
            com.facebook.messaging.model.share.SentShareAttachment r0 = r4.A0T
            if (r0 == 0) goto L_0x0045
            com.facebook.payments.p2p.service.model.transactions.SendPaymentMessageParams r0 = r0.A03
            r1 = 1
            if (r0 != 0) goto L_0x0046
        L_0x0045:
            r1 = 0
        L_0x0046:
            java.lang.String r0 = "is_sent_payment_message"
            r2.putExtra(r0, r1)
            A04(r3, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C189216c.A0A(com.facebook.messaging.model.messages.Message, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001f, code lost:
        if (r0.A03 == null) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0B(com.facebook.messaging.model.messages.Message r4, java.lang.String r5) {
        /*
            r3 = this;
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r0 = X.C06680bu.A0n
            r2.setAction(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r4.A0U
            java.lang.String r0 = "thread_key"
            r2.putExtra(r0, r1)
            java.lang.String r1 = r4.A0w
            java.lang.String r0 = "offline_threading_id"
            r2.putExtra(r0, r1)
            com.facebook.messaging.model.share.SentShareAttachment r0 = r4.A0T
            if (r0 == 0) goto L_0x0021
            com.facebook.payments.p2p.service.model.transactions.SendPaymentMessageParams r0 = r0.A03
            r1 = 1
            if (r0 != 0) goto L_0x0022
        L_0x0021:
            r1 = 0
        L_0x0022:
            java.lang.String r0 = "is_sent_payment_message"
            r2.putExtra(r0, r1)
            java.lang.String r0 = "calling_class"
            r2.putExtra(r0, r5)
            A04(r3, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C189216c.A0B(com.facebook.messaging.model.messages.Message, java.lang.String):void");
    }

    public void A0C(Message message, String str) {
        Intent intent = new Intent();
        intent.setAction(C06680bu.A0R);
        intent.putExtra(AnonymousClass24B.$const$string(AnonymousClass1Y3.A2J), message);
        intent.putExtra("calling_class", str);
        A04(this, intent);
    }

    public void A0E(ThreadKey threadKey, ThreadKey threadKey2, String str) {
        Intent intent = new Intent();
        intent.setAction(C06680bu.A0p);
        intent.putExtra("pending_thread_key", threadKey);
        intent.putExtra(C99084oO.$const$string(85), threadKey2);
        intent.putExtra("calling_class", str);
        A04(this, intent);
    }

    public void A0F(ThreadKey threadKey, String str) {
        Intent intent = new Intent();
        intent.setAction(C06680bu.A0q);
        intent.putExtra("pending_thread_key", threadKey);
        intent.putExtra("calling_class", str);
        A04(this, intent);
    }

    public void A0H(ThreadKey threadKey, String str, String str2) {
        Intent intent = new Intent();
        intent.setAction(C06680bu.A0o);
        intent.putExtra("thread_key", threadKey);
        intent.putExtra("offline_threading_id", str);
        intent.putExtra("calling_class", str2);
        A04(this, intent);
    }

    public void A0I(ThreadKey threadKey, Collection collection, Collection collection2, String str) {
        Intent intent = new Intent();
        intent.setAction(C06680bu.A0P);
        intent.putExtra("thread_key", threadKey);
        intent.putStringArrayListExtra(C99084oO.$const$string(16), C04300To.A03(collection));
        intent.putStringArrayListExtra("offline_threading_ids", C04300To.A03(collection2));
        intent.putExtra("calling_class", str);
        A04(this, intent);
    }

    public void A0J(ImmutableList immutableList, String str) {
        Intent intent = new Intent(C06680bu.A0a);
        intent.putParcelableArrayListExtra("multiple_user_keys", new ArrayList(immutableList));
        intent.putExtra("calling_class", str);
        A04(this, intent);
    }

    public void A0K(ImmutableList immutableList, String str) {
        A05(this, C06680bu.A0e, new ArrayList(immutableList), str);
    }

    public void A0L(ImmutableList immutableList, String str) {
        A05(this, C06680bu.A0c, new ArrayList(immutableList), str);
    }

    public void A0M(ImmutableList immutableList, String str) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_unread", true);
        A06(C06680bu.A0b, new ArrayList(immutableList), bundle, str);
    }

    public void A0N(String str) {
        Intent intent = new Intent(C06680bu.A0Z);
        intent.putExtra("calling_class", str);
        A04(this, intent);
    }

    public void A0P(String str, ArrayList arrayList, String str2) {
        Intent intent = new Intent("MONTAGE_MESSAGE_UPDATED_FOR_UI");
        intent.putExtra("multiple_thread_keys", arrayList);
        intent.putExtra("message_id", str);
        intent.putExtra("calling_class", str2);
        A04(this, intent);
    }

    private C189216c(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A02 = AnonymousClass1YA.A00(r3);
        this.A04 = AnonymousClass0VG.A00(AnonymousClass1Y3.AiL, r3);
        this.A01 = AnonymousClass0VG.A00(AnonymousClass1Y3.AFU, r3);
        this.A03 = C04750Wa.A01(r3);
        AnonymousClass0WT.A00(r3);
    }

    public static final C189216c A02(AnonymousClass1XY r0) {
        return A03(r0);
    }

    public static void A05(C189216c r2, String str, ArrayList arrayList, String str2) {
        if (!A07(r2, arrayList, str)) {
            Intent intent = new Intent();
            intent.setAction(str);
            intent.putParcelableArrayListExtra("multiple_thread_keys", arrayList);
            intent.putExtra("calling_class", str2);
            A04(r2, intent);
        }
    }

    public static boolean A07(C189216c r1, Collection collection, String str) {
        if (!collection.isEmpty()) {
            return false;
        }
        r1.A03.CGS("MessagesBroadcaster_NoThreadsUpdated", AnonymousClass08S.A0J("empty threadKeys, action=", str));
        return true;
    }

    public void A0D(ThreadKey threadKey, Bundle bundle, String str) {
        A06(C06680bu.A0e, new ArrayList(ImmutableList.of(threadKey)), bundle, str);
    }

    public void A0G(ThreadKey threadKey, String str) {
        A0K(ImmutableList.of(threadKey), str);
    }

    public void A0O(String str) {
        Preconditions.checkNotNull(str);
        Intent intent = new Intent();
        intent.setAction(C06680bu.A0z);
        intent.putExtra("calling_class", str);
        A04(this, intent);
    }
}
