package X;

import android.app.Activity;
import com.facebook.dialtone.activity.DialtoneUnsupportedCarrierInterstitialActivity;
import com.facebook.dialtone.activity.DialtoneWifiInterstitialActivity;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tr  reason: invalid class name and case insensitive filesystem */
public final class C14710tr {
    private static volatile C14710tr A07;
    public int A00 = 0;
    public Activity A01;
    public AnonymousClass1ES A02;
    public Set A03;
    public boolean A04 = false;
    public boolean A05 = true;
    public boolean A06 = false;

    public static final C14710tr A00(AnonymousClass1XY r5) {
        if (A07 == null) {
            synchronized (C14710tr.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A07 = new C14710tr(applicationInjector, C14730tt.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public boolean A01() {
        boolean z;
        Activity activity;
        if (this.A02.A0L()) {
            synchronized (this) {
                z = false;
                if (this.A00 > 0) {
                    z = true;
                }
            }
            if (!z || (activity = this.A01) == null || (activity instanceof DialtoneWifiInterstitialActivity) || (activity instanceof DialtoneUnsupportedCarrierInterstitialActivity)) {
                return true;
            }
            return false;
        }
        return true;
    }

    private C14710tr(AnonymousClass1XY r3, AnonymousClass1ES r4) {
        new AnonymousClass0UN(1, r3);
        this.A04 = r4.A0L();
        this.A02 = r4;
        r4.A0E(this);
        this.A03 = new HashSet();
    }
}
