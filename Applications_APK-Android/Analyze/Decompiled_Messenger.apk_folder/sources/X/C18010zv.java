package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.google.common.util.concurrent.ListenableFuture;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0zv  reason: invalid class name and case insensitive filesystem */
public final class C18010zv {
    private static volatile C18010zv A0A;
    public ListenableFuture A00;
    public final PackageManager A01;
    public final C001500z A02;
    public final C25501Zw A03;
    public final C12460pP A04;
    public final C04310Tq A05;
    private final Context A06;
    private final AnonymousClass0WP A07;
    private final AnonymousClass010 A08;
    private final C04310Tq A09;

    public static final C18010zv A00(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (C18010zv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new C18010zv(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0039, code lost:
        if (r5.A03.A09() == false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0015, code lost:
        if (r5.A04.A05() == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.common.util.concurrent.ListenableFuture A01(X.C18010zv r5, android.content.Intent r6) {
        /*
            X.0Tq r0 = r5.A05
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0017
            X.0pP r0 = r5.A04
            boolean r1 = r0.A05()
            r0 = 1
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            r3 = 0
            if (r0 == 0) goto L_0x0053
            java.lang.String r1 = X.C18030zx.A0P
            X.0Tq r0 = r5.A09
            java.lang.Object r0 = r0.get()
            java.lang.String r0 = (java.lang.String) r0
            r6.putExtra(r1, r0)
            X.00z r2 = r5.A02
            X.00z r1 = X.C001500z.A07
            r0 = 0
            if (r2 != r1) goto L_0x0030
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x003b
            X.1Zw r0 = r5.A03
            boolean r1 = r0.A09()
            r0 = 1
            if (r1 != 0) goto L_0x003c
        L_0x003b:
            r0 = 0
        L_0x003c:
            if (r0 == 0) goto L_0x0050
            X.0WP r4 = r5.A07
            X.16G r3 = new X.16G
            r3.<init>(r5, r6)
            X.0XV r2 = X.AnonymousClass0XV.APPLICATION_LOADED_UI_IDLE
            java.lang.Integer r1 = X.AnonymousClass07B.A01
            java.lang.String r0 = "ChatHeadsInitializer initAfterUiIdle"
            X.0XX r0 = r4.CIG(r0, r3, r2, r1)
            return r0
        L_0x0050:
            A02(r5, r6)
        L_0x0053:
            com.google.common.util.concurrent.ListenableFuture r0 = X.C05350Yp.A03(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18010zv.A01(X.0zv, android.content.Intent):com.google.common.util.concurrent.ListenableFuture");
    }

    public static void A02(C18010zv r2, Intent intent) {
        if (!r2.A01.queryBroadcastReceivers(intent, 0).isEmpty()) {
            r2.A06.sendOrderedBroadcast(intent, r2.A08.A00());
        }
    }

    public static void A03(C18010zv r3, boolean z, String str) {
        String str2;
        ListenableFuture listenableFuture = r3.A00;
        if (listenableFuture != null) {
            listenableFuture.cancel(false);
            r3.A00 = null;
        }
        if (z) {
            str2 = C18030zx.A0E;
        } else {
            str2 = C18030zx.A05;
        }
        Intent intent = new Intent(str2);
        if (str != null) {
            intent.putExtra(C18030zx.A0S, str);
        }
        ListenableFuture A012 = A01(r3, intent);
        if (!A012.isDone()) {
            r3.A00 = A012;
            C05350Yp.A08(A012, new AnonymousClass16H(r3), C25141Ym.INSTANCE);
        }
    }

    public void A04() {
        A01(this, new Intent(C18030zx.A04));
    }

    private C18010zv(AnonymousClass1XY r2) {
        this.A06 = AnonymousClass1YA.A00(r2);
        this.A09 = AnonymousClass0XJ.A0K(r2);
        this.A01 = C04490Ux.A0J(r2);
        this.A07 = C25091Yh.A00(r2);
        this.A02 = AnonymousClass0UU.A05(r2);
        this.A03 = C25501Zw.A00(r2);
        this.A08 = AnonymousClass0UU.A00(r2);
        this.A05 = AnonymousClass0VG.A00(AnonymousClass1Y3.AUX, r2);
        this.A04 = C12460pP.A01(r2);
    }
}
