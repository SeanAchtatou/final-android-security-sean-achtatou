package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.1c1  reason: invalid class name */
public final class AnonymousClass1c1 extends Enum implements C26781c0 {
    private static final /* synthetic */ AnonymousClass1c1[] $VALUES;
    public static final AnonymousClass1c1 ACCEPT_EMPTY_STRING_AS_NULL_OBJECT;
    public static final AnonymousClass1c1 ACCEPT_SINGLE_VALUE_AS_ARRAY;
    public static final AnonymousClass1c1 ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
    public static final AnonymousClass1c1 EAGER_DESERIALIZER_FETCH;
    public static final AnonymousClass1c1 FAIL_ON_INVALID_SUBTYPE;
    public static final AnonymousClass1c1 FAIL_ON_NULL_FOR_PRIMITIVES;
    public static final AnonymousClass1c1 FAIL_ON_NUMBERS_FOR_ENUMS;
    public static final AnonymousClass1c1 FAIL_ON_UNKNOWN_PROPERTIES;
    public static final AnonymousClass1c1 READ_DATE_TIMESTAMPS_AS_NANOSECONDS;
    public static final AnonymousClass1c1 READ_ENUMS_USING_TO_STRING;
    public static final AnonymousClass1c1 READ_UNKNOWN_ENUM_VALUES_AS_NULL;
    public static final AnonymousClass1c1 UNWRAP_ROOT_VALUE;
    public static final AnonymousClass1c1 USE_BIG_DECIMAL_FOR_FLOATS;
    public static final AnonymousClass1c1 USE_BIG_INTEGER_FOR_INTS;
    public static final AnonymousClass1c1 USE_JAVA_ARRAY_FOR_JSON_ARRAY;
    public static final AnonymousClass1c1 WRAP_EXCEPTIONS;
    private final boolean _defaultState;

    static {
        AnonymousClass1c1 r0 = new AnonymousClass1c1("USE_BIG_DECIMAL_FOR_FLOATS", 0, false);
        USE_BIG_DECIMAL_FOR_FLOATS = r0;
        AnonymousClass1c1 r15 = new AnonymousClass1c1("USE_BIG_INTEGER_FOR_INTS", 1, false);
        USE_BIG_INTEGER_FOR_INTS = r15;
        AnonymousClass1c1 r13 = new AnonymousClass1c1("USE_JAVA_ARRAY_FOR_JSON_ARRAY", 2, false);
        USE_JAVA_ARRAY_FOR_JSON_ARRAY = r13;
        AnonymousClass1c1 r12 = new AnonymousClass1c1("READ_ENUMS_USING_TO_STRING", 3, false);
        READ_ENUMS_USING_TO_STRING = r12;
        AnonymousClass1c1 r11 = new AnonymousClass1c1("FAIL_ON_UNKNOWN_PROPERTIES", 4, true);
        FAIL_ON_UNKNOWN_PROPERTIES = r11;
        AnonymousClass1c1 r10 = new AnonymousClass1c1("FAIL_ON_NULL_FOR_PRIMITIVES", 5, false);
        FAIL_ON_NULL_FOR_PRIMITIVES = r10;
        AnonymousClass1c1 r9 = new AnonymousClass1c1("FAIL_ON_NUMBERS_FOR_ENUMS", 6, false);
        FAIL_ON_NUMBERS_FOR_ENUMS = r9;
        AnonymousClass1c1 r8 = new AnonymousClass1c1("FAIL_ON_INVALID_SUBTYPE", 7, true);
        FAIL_ON_INVALID_SUBTYPE = r8;
        AnonymousClass1c1 r7 = new AnonymousClass1c1("WRAP_EXCEPTIONS", 8, true);
        WRAP_EXCEPTIONS = r7;
        AnonymousClass1c1 r6 = new AnonymousClass1c1("ACCEPT_SINGLE_VALUE_AS_ARRAY", 9, false);
        ACCEPT_SINGLE_VALUE_AS_ARRAY = r6;
        AnonymousClass1c1 r5 = new AnonymousClass1c1("UNWRAP_ROOT_VALUE", 10, false);
        UNWRAP_ROOT_VALUE = r5;
        AnonymousClass1c1 r4 = new AnonymousClass1c1("ACCEPT_EMPTY_STRING_AS_NULL_OBJECT", 11, false);
        ACCEPT_EMPTY_STRING_AS_NULL_OBJECT = r4;
        AnonymousClass1c1 r3 = new AnonymousClass1c1("READ_UNKNOWN_ENUM_VALUES_AS_NULL", 12, false);
        READ_UNKNOWN_ENUM_VALUES_AS_NULL = r3;
        AnonymousClass1c1 r2 = new AnonymousClass1c1("READ_DATE_TIMESTAMPS_AS_NANOSECONDS", 13, true);
        READ_DATE_TIMESTAMPS_AS_NANOSECONDS = r2;
        AnonymousClass1c1 r19 = new AnonymousClass1c1("ADJUST_DATES_TO_CONTEXT_TIME_ZONE", 14, true);
        ADJUST_DATES_TO_CONTEXT_TIME_ZONE = r19;
        AnonymousClass1c1 r192 = new AnonymousClass1c1("EAGER_DESERIALIZER_FETCH", 15, true);
        EAGER_DESERIALIZER_FETCH = r192;
        AnonymousClass1c1 r30 = r19;
        AnonymousClass1c1 r31 = r192;
        AnonymousClass1c1 r22 = r9;
        AnonymousClass1c1 r23 = r8;
        AnonymousClass1c1 r24 = r7;
        AnonymousClass1c1 r193 = r12;
        AnonymousClass1c1 r20 = r11;
        AnonymousClass1c1 r21 = r10;
        AnonymousClass1c1 r16 = r0;
        AnonymousClass1c1 r17 = r15;
        AnonymousClass1c1 r18 = r13;
        $VALUES = new AnonymousClass1c1[]{r16, r17, r18, r193, r20, r21, r22, r23, r24, r6, r5, r4, r3, r2, r30, r31};
    }

    public static AnonymousClass1c1 valueOf(String str) {
        return (AnonymousClass1c1) Enum.valueOf(AnonymousClass1c1.class, str);
    }

    public static AnonymousClass1c1[] values() {
        return (AnonymousClass1c1[]) $VALUES.clone();
    }

    public boolean enabledByDefault() {
        return this._defaultState;
    }

    private AnonymousClass1c1(String str, int i, boolean z) {
        this._defaultState = z;
    }

    public int getMask() {
        return 1 << ordinal();
    }
}
