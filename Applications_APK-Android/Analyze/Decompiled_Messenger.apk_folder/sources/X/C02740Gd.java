package X;

import android.util.SparseArray;

/* renamed from: X.0Gd  reason: invalid class name and case insensitive filesystem */
public abstract class C02740Gd {
    public static boolean A01(SparseArray sparseArray, SparseArray sparseArray2) {
        if (sparseArray != sparseArray2) {
            if (!(sparseArray == null || sparseArray2 == null || sparseArray.size() != sparseArray2.size())) {
                int i = 0;
                while (i < sparseArray.size()) {
                    int keyAt = sparseArray.keyAt(i);
                    if (sparseArray.get(keyAt).equals(sparseArray2.get(keyAt))) {
                        i++;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public static boolean A02(AnonymousClass04b r7, AnonymousClass04b r8) {
        if (r7 != r8) {
            int size = r7.size();
            if (size == r8.size()) {
                for (int i = 0; i < size; i++) {
                    Object A07 = r7.A07(i);
                    Object A09 = r7.A09(i);
                    Object obj = r8.get(A07);
                    if (A09 == null) {
                        if (obj == null && r8.containsKey(A07)) {
                        }
                    } else if (!A09.equals(obj)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public static void A00(Object obj, String str) {
        if (obj == null) {
            throw new IllegalArgumentException(str);
        }
    }
}
