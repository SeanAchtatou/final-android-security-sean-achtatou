package X;

import java.util.List;

/* renamed from: X.1iz  reason: invalid class name and case insensitive filesystem */
public final class C31001iz {
    public final C30961iv A00;
    public final AnonymousClass0ZZ A01;
    public final AnonymousClass13z A02;
    public final String A03;
    public final List A04;

    public C31001iz(String str, AnonymousClass13z r2, C30961iv r3, AnonymousClass0ZZ r4, List list) {
        this.A03 = str;
        this.A02 = r2;
        this.A00 = r3;
        this.A01 = r4;
        this.A04 = list;
    }
}
