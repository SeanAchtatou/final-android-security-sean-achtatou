package X;

/* renamed from: X.0nx  reason: invalid class name and case insensitive filesystem */
public final class C11790nx implements C11800ny {
    public final /* synthetic */ AnonymousClass0jJ this$0;
    public final /* synthetic */ AnonymousClass0jJ val$mapper;

    public C11790nx(AnonymousClass0jJ r1, AnonymousClass0jJ r2) {
        this.this$0 = r1;
        this.val$mapper = r2;
    }

    public void addBeanSerializerModifier(C11740ns r3) {
        AnonymousClass0jJ r1 = this.val$mapper;
        r1._serializerFactory = r1._serializerFactory.withSerializerModifier(r3);
    }

    public void addDeserializers(AnonymousClass1c7 r4) {
        AnonymousClass1c6 withAdditionalDeserializers = this.val$mapper._deserializationContext._factory.withAdditionalDeserializers(r4);
        AnonymousClass0jJ r1 = this.val$mapper;
        r1._deserializationContext = r1._deserializationContext.with(withAdditionalDeserializers);
    }

    public void addSerializers(C11730nr r3) {
        AnonymousClass0jJ r1 = this.val$mapper;
        r1._serializerFactory = r1._serializerFactory.withAdditionalSerializers(r3);
    }

    public void addTypeModifier(C11880o6 r6) {
        C10300js r4;
        C10490kF r0;
        C10450k8 r02;
        C10300js r1 = this.val$mapper._typeFactory;
        C11880o6[] r03 = r1._modifiers;
        if (r03 == null) {
            r4 = new C10300js(r1._parser, new C11880o6[]{r6});
        } else {
            r4 = new C10300js(r1._parser, (C11880o6[]) C11830o1.insertInListNoDup(r03, r6));
        }
        AnonymousClass0jJ r3 = this.val$mapper;
        r3._typeFactory = r4;
        C10490kF r2 = r3._deserializationConfig;
        C10290jr r04 = r2._base;
        C10290jr withTypeFactory = r04.withTypeFactory(r4);
        if (r04 == withTypeFactory) {
            r0 = r2;
        } else {
            r0 = new C10490kF(r2, withTypeFactory);
        }
        r3._deserializationConfig = r0;
        C10450k8 r22 = r3._serializationConfig;
        C10290jr r05 = r22._base;
        C10290jr withTypeFactory2 = r05.withTypeFactory(r4);
        if (r05 == withTypeFactory2) {
            r02 = r22;
        } else {
            r02 = new C10450k8(r22, withTypeFactory2);
        }
        r3._serializationConfig = r02;
    }
}
