package X;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import java.util.Set;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0tZ  reason: invalid class name */
public final class AnonymousClass0tZ {
    private static volatile AnonymousClass0tZ A01;
    private AnonymousClass0US A00;

    public static String A01(Integer num) {
        switch (num.intValue()) {
            case 1:
                return "ZR";
            case 2:
                return "DS";
            case 3:
                return "DI";
            case 4:
                return "LT";
            default:
                return "DT";
        }
    }

    public static final AnonymousClass0tZ A00(AnonymousClass1XY r5) {
        if (A01 == null) {
            synchronized (AnonymousClass0tZ.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass0tZ(AnonymousClass0UQ.A00(AnonymousClass1Y3.AZH, r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public String A02() {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (C14590te r1 : (Set) this.A00.get()) {
            if (r1.BEx()) {
                if (!z) {
                    sb.append("-");
                } else {
                    z = false;
                }
                sb.append(A01(r1.A01()));
            }
        }
        return sb.toString();
    }

    public void A03(C11670nb r5) {
        if (!((Set) this.A00.get()).isEmpty()) {
            for (C14590te r1 : (Set) this.A00.get()) {
                if (r1.BEx()) {
                    String A012 = A01(r1.A01());
                    if (r5.A03 == null) {
                        r5.A03 = new ArrayNode(JsonNodeFactory.instance);
                    }
                    r5.A03.add(A012);
                }
            }
        }
    }

    private AnonymousClass0tZ(AnonymousClass0US r1) {
        this.A00 = r1;
    }
}
