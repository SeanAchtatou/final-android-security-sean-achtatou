package X;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.0oQ  reason: invalid class name and case insensitive filesystem */
public final class C12030oQ {
    private final AnonymousClass0jD A00;

    public static final C12030oQ A00(AnonymousClass1XY r2) {
        return new C12030oQ(AnonymousClass0jD.A00(r2));
    }

    public static String A01(ImmutableMap immutableMap) {
        if (immutableMap == null || immutableMap.size() == 0) {
            return null;
        }
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        C24971Xv it = immutableMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            objectNode.put((String) entry.getKey(), (String) entry.getValue());
        }
        return objectNode.toString();
    }

    public C12030oQ(AnonymousClass0jD r1) {
        this.A00 = r1;
    }

    public ImmutableMap A02(String str) {
        if (C06850cB.A0B(str)) {
            return RegularImmutableMap.A03;
        }
        ImmutableMap.Builder builder = ImmutableMap.builder();
        Iterator fields = this.A00.A02(str).fields();
        while (fields.hasNext()) {
            Map.Entry entry = (Map.Entry) fields.next();
            builder.put(entry.getKey(), ((JsonNode) entry.getValue()).asText());
        }
        return builder.build();
    }
}
