package X;

/* renamed from: X.1q2  reason: invalid class name and case insensitive filesystem */
public abstract class C34701q2 implements C08290f0 {
    public final C26161b0 A00;

    public void BjG(AnonymousClass0f7 r2, int i, boolean z) {
        ((C34691q1) this).A00(r2, z);
    }

    public void BjH(AnonymousClass0f7 r2, boolean z, int i, int i2, Integer num) {
        ((C34691q1) this).A01(r2, z, i2, num);
    }

    public void BjL(AnonymousClass0f7 r1, int i, boolean z) {
    }

    public void BjM(AnonymousClass0f7 r1, int i) {
    }

    public void Bm4(AnonymousClass0f7 r1, int i, int i2, Integer num) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r3.A00 == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass0f7 r5, boolean r6) {
        /*
            r4 = this;
            X.1b0 r3 = r4.A00
            if (r3 == 0) goto L_0x0009
            boolean r1 = r3.A00
            r0 = 0
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 1
        L_0x000a:
            if (r0 != 0) goto L_0x003d
            boolean r0 = r5 instanceof X.C08240eu
            if (r0 != 0) goto L_0x003d
            boolean r0 = r5 instanceof X.AnonymousClass0f7
            if (r0 == 0) goto L_0x003d
            int r0 = r5.A02()
            java.lang.String r2 = X.C07210ct.A00(r0)
            X.1b2 r0 = r3.A01
            java.util.Map r0 = r0.A00
            java.lang.Object r1 = r0.get(r2)
            X.1bU r1 = (X.C26461bU) r1
            if (r1 == 0) goto L_0x003d
            r0 = 2
            if (r6 == 0) goto L_0x002c
            r0 = 4
        L_0x002c:
            r1.A00(r0)
            X.1b0 r0 = r4.A00
            r0.BJ9(r1)
            X.1b0 r0 = r4.A00
            X.1b2 r0 = r0.A01
            java.util.Map r0 = r0.A00
            r0.remove(r2)
        L_0x003d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34701q2.A00(X.0f7, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0007, code lost:
        if (r3.A00 == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass0f7 r8, boolean r9, int r10, java.lang.Integer r11) {
        /*
            r7 = this;
            X.1b0 r3 = r7.A00
            if (r3 == 0) goto L_0x0009
            boolean r1 = r3.A00
            r0 = 0
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 1
        L_0x000a:
            if (r0 != 0) goto L_0x0063
            boolean r0 = r8 instanceof X.C08240eu
            if (r0 != 0) goto L_0x0063
            boolean r0 = r8 instanceof X.AnonymousClass0f7
            if (r0 == 0) goto L_0x0063
            int r0 = r8.A02()
            java.lang.String r5 = X.C07210ct.A00(r0)
            r6 = 0
            if (r9 == 0) goto L_0x0064
            X.1bU r3 = new X.1bU
            r1 = 25
            r0 = 0
            r3.<init>(r1, r6, r0, r6)
            java.lang.String r0 = X.C26481bW.A00(r1)
            r3.A01(r0)
            long r0 = (long) r10
            java.lang.String r2 = "trigger_source_id"
            r3.A02(r2, r0)
            java.lang.String r1 = "booster"
            X.1bV r0 = r3.A09
            java.util.Map r0 = r0.A02
            r0.put(r1, r5)
            if (r11 == 0) goto L_0x0049
            int r0 = r11.intValue()
            long r1 = (long) r0
            java.lang.String r0 = "trigger_source_key"
            r3.A02(r0, r1)
        L_0x0049:
            X.1b0 r0 = r7.A00
            X.1b2 r2 = r0.A01
            java.util.Map r0 = r2.A00
            boolean r0 = r0.containsKey(r5)
            if (r0 != 0) goto L_0x005a
            java.util.Map r0 = r2.A00
            r0.put(r5, r3)
        L_0x005a:
            boolean r0 = r3.A04
            if (r0 != 0) goto L_0x0063
            java.util.Set r0 = X.C26621bk.A01
            X.C26161b0.A01(r3, r0)
        L_0x0063:
            return
        L_0x0064:
            X.1bU r4 = new X.1bU
            r2 = 25
            r0 = 0
            r4.<init>(r2, r6, r0, r6)
            java.lang.String r0 = X.C26481bW.A00(r2)
            r4.A01(r0)
            java.lang.String r1 = "booster"
            X.1bV r0 = r4.A09
            java.util.Map r0 = r0.A02
            r0.put(r1, r5)
            r0 = 3
            r4.A00(r0)
            long r0 = (long) r10
            java.lang.String r2 = "trigger_source_id"
            r4.A02(r2, r0)
            if (r11 == 0) goto L_0x0090
            int r0 = r11.intValue()
            long r0 = (long) r0
            r4.A02(r2, r0)
        L_0x0090:
            r3.BJ9(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34701q2.A01(X.0f7, boolean, int, java.lang.Integer):void");
    }

    public void Blz(AnonymousClass0f7 r8, int i, Integer num) {
        C26161b0 r4 = this.A00;
        if (r4 != null) {
            String A002 = C07210ct.A00(r8.A02());
            C26461bU r3 = new C26461bU(25, num, false, null);
            r3.A01(C26481bW.A00(25));
            r3.A02("trigger_source_id", (long) i);
            r3.A09.A02.put("booster", A002);
            if (num != null) {
                r3.A02("trigger_source_key", (long) num.intValue());
            }
            r3.A00(87);
            r4.BJ9(r3);
        }
    }

    public C34701q2(C26161b0 r1) {
        this.A00 = r1;
    }
}
