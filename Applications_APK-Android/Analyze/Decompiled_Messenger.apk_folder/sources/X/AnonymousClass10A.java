package X;

import com.facebook.common.util.TriState;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.10A  reason: invalid class name */
public final class AnonymousClass10A {
    private static volatile AnonymousClass10A A03;
    public TriState A00 = TriState.UNSET;
    public Map A01;
    public final C07030cV A02;

    public static final AnonymousClass10A A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (AnonymousClass10A.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new AnonymousClass10A(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private AnonymousClass10A(AnonymousClass1XY r2) {
        this.A02 = C07030cV.A00(r2);
    }
}
