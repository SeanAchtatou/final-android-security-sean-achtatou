package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1M3  reason: invalid class name */
public final class AnonymousClass1M3 extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public MigColorScheme A01;

    public AnonymousClass1M3(Context context) {
        super("M4SwipeCameraButton");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
