package X;

import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Rm  reason: invalid class name and case insensitive filesystem */
public final class C23921Rm {
    public static final int A01;
    public static final int A02;
    public static final C23921Rm A03 = new C23921Rm();
    public final Executor A00 = new C23931Rn();

    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        A01 = availableProcessors + 1;
        A02 = (availableProcessors << 1) + 1;
    }

    public static ExecutorService A00() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(A01, A02, 1, TimeUnit.SECONDS, new LinkedBlockingQueue());
        if (Build.VERSION.SDK_INT >= 9) {
            threadPoolExecutor.allowCoreThreadTimeOut(true);
        }
        return threadPoolExecutor;
    }

    private C23921Rm() {
    }
}
