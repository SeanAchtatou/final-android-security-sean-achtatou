package X;

import com.fasterxml.jackson.databind.JsonNode;

/* renamed from: X.13v  reason: invalid class name and case insensitive filesystem */
public abstract class C185113v extends JsonNode implements C10040jS {
    public abstract C182811d asToken();

    public C29501gW numberType() {
        return null;
    }

    public abstract void serialize(C11710np r1, C11260mU r2);

    public abstract void serializeWithType(C11710np r1, C11260mU r2, CY1 cy1);

    public final JsonNode findPath(String str) {
        JsonNode findValue = findValue(str);
        if (findValue == null) {
            return C29651gl.instance;
        }
        return findValue;
    }

    public C28271eX traverse() {
        return new C25099CXo(this, null);
    }

    public C28271eX traverse(C09980jK r2) {
        return new C25099CXo(this, r2);
    }
}
