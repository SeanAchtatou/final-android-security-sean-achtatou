package X;

/* renamed from: X.1SW  reason: invalid class name */
public final class AnonymousClass1SW extends AnonymousClass1PS {
    public /* bridge */ /* synthetic */ Object clone() {
        return this;
    }

    public void finalize() {
        boolean z;
        int A03 = C000700l.A03(-924156547);
        try {
            synchronized (this) {
                z = this.A00;
            }
            if (z) {
                super.finalize();
                C000700l.A09(788644167, A03);
                return;
            }
            Integer valueOf = Integer.valueOf(System.identityHashCode(this));
            AnonymousClass1SA r2 = this.A02;
            AnonymousClass02w.A0E("FinalizerCloseableReference", "Finalized without closing: %x %x (type = %s)", valueOf, Integer.valueOf(System.identityHashCode(r2)), r2.A01().getClass().getName());
            this.A02.A02();
            super.finalize();
            C000700l.A09(1346777502, A03);
        } catch (Throwable th) {
            super.finalize();
            C000700l.A09(1299523545, A03);
            throw th;
        }
    }

    public AnonymousClass1SW(Object obj, C22891Nf r2, C22991Nq r3, Throwable th) {
        super(obj, r2, r3, th);
    }
}
