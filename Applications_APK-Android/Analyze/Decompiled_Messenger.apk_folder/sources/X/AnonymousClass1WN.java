package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.util.Log;
import com.google.firebase.components.ComponentDiscoveryService;
import com.google.firebase.iid.Registrar;
import io.card.payment.BuildConfig;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1WN  reason: invalid class name */
public final class AnonymousClass1WN {
    public static final Object A0A = new Object();
    public static final Map A0B = new AnonymousClass04a();
    private static final List A0C = Arrays.asList("com.google.firebase.auth.FirebaseAuth", "com.google.firebase.iid.FirebaseInstanceId");
    private static final List A0D = Collections.singletonList("com.google.firebase.crash.FirebaseCrash");
    private static final List A0E = Arrays.asList("com.google.android.gms.measurement.AppMeasurement");
    private static final List A0F = Arrays.asList(new String[0]);
    private static final Set A0G = Collections.emptySet();
    private static final Executor A0H = new AnonymousClass1WO();
    public final Context A00;
    public final SharedPreferences A01;
    public final C04170Ss A02;
    public final AnonymousClass1WT A03;
    public final String A04;
    public final List A05 = new CopyOnWriteArrayList();
    public final List A06 = new CopyOnWriteArrayList();
    public final AtomicBoolean A07 = new AtomicBoolean(false);
    public final AtomicBoolean A08;
    public final AtomicBoolean A09 = new AtomicBoolean();

    public static AnonymousClass1WN A00() {
        AnonymousClass1WN r0;
        synchronized (A0A) {
            r0 = (AnonymousClass1WN) A0B.get("[DEFAULT]");
            if (r0 == null) {
                throw new IllegalStateException(AnonymousClass08S.A0P("Default FirebaseApp is not initialized in this process ", AnonymousClass8WS.A00(), ". Make sure to call FirebaseApp.initializeApp(Context) first."));
            }
        }
        return r0;
    }

    public static AnonymousClass1WX A01(String str, String str2) {
        C24561Wb r3 = new C24561Wb(str, str2);
        AnonymousClass1WY r2 = new AnonymousClass1WY(C24571Wc.class, new Class[0]);
        r2.A01 = 1;
        C24581Wd r1 = new C24581Wd(r3);
        AnonymousClass09E.A02(r1, "Null factory");
        r2.A02 = r1;
        return r2.A00();
    }

    public static void A02(AnonymousClass1WN r1) {
        AnonymousClass09E.A09(!r1.A09.get(), "FirebaseApp was deleted");
    }

    public static void A03(AnonymousClass1WN r9) {
        Queue<C29975Emo> queue;
        Set<Map.Entry> set;
        Class<AnonymousClass1WN> cls = AnonymousClass1WN.class;
        boolean A062 = AnonymousClass01R.A06(r9.A00);
        if (!A062) {
            AnonymousClass1WT r8 = r9.A03;
            A02(r9);
            boolean equals = "[DEFAULT]".equals(r9.A04);
            for (Map.Entry entry : r8.A01.entrySet()) {
                C24651Wo r3 = (C24651Wo) entry.getValue();
                int i = ((AnonymousClass1WX) entry.getKey()).A00;
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                if (!z) {
                    boolean z2 = false;
                    if (i == 2) {
                        z2 = true;
                    }
                    if (z2) {
                        if (!equals) {
                        }
                    }
                }
                r3.get();
            }
            AnonymousClass1Wh r4 = r8.A00;
            synchronized (r4) {
                try {
                    queue = r4.A00;
                    if (queue != null) {
                        r4.A00 = null;
                    } else {
                        queue = null;
                    }
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            if (queue != null) {
                for (C29975Emo emo : queue) {
                    AnonymousClass09E.A01(emo);
                    synchronized (r4) {
                        try {
                            Queue queue2 = r4.A00;
                            if (queue2 != null) {
                                queue2.add(emo);
                            } else {
                                synchronized (r4) {
                                    Map map = (Map) r4.A01.get(null);
                                    if (map == null) {
                                        set = Collections.emptySet();
                                    } else {
                                        set = map.entrySet();
                                    }
                                }
                                for (Map.Entry entry2 : set) {
                                    AnonymousClass07A.A04((Executor) entry2.getValue(), new C185588jP(entry2), -1719746353);
                                }
                            }
                        } catch (Throwable th2) {
                            while (true) {
                                th = th2;
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            Context context = r9.A00;
            if (AnonymousClass57V.A01.get() == null) {
                AnonymousClass57V r2 = new AnonymousClass57V(context);
                if (AnonymousClass57V.A01.compareAndSet(null, r2)) {
                    context.registerReceiver(r2, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }
        A04(cls, r9, A0C, A062);
        A02(r9);
        if ("[DEFAULT]".equals(r9.A04)) {
            A04(cls, r9, A0D, A062);
            A04(Context.class, r9.A00, A0E, A062);
            return;
        }
        return;
        throw th;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass1WN)) {
            return false;
        }
        String str = this.A04;
        AnonymousClass1WN r3 = (AnonymousClass1WN) obj;
        A02(r3);
        return str.equals(r3.A04);
    }

    public int hashCode() {
        return this.A04.hashCode();
    }

    public String toString() {
        E8B e8b = new E8B(this);
        e8b.A00("name", this.A04);
        e8b.A00(C99084oO.$const$string(AnonymousClass1Y3.A1a), this.A02);
        return e8b.toString();
    }

    public AnonymousClass1WN(Context context, String str, C04170Ss r17) {
        boolean z;
        ApplicationInfo applicationInfo;
        Bundle bundle;
        List<String> arrayList;
        AnonymousClass09E.A01(context);
        this.A00 = context;
        String str2 = str;
        AnonymousClass09E.A03(str2);
        this.A04 = str2;
        C04170Ss r5 = r17;
        AnonymousClass09E.A01(r5);
        this.A02 = r5;
        SharedPreferences sharedPreferences = context.getSharedPreferences(AnonymousClass08S.A0J("com.google.firebase.common.prefs:", str2), 0);
        this.A01 = sharedPreferences;
        if (sharedPreferences.contains("firebase_data_collection_default_enabled")) {
            z = this.A01.getBoolean("firebase_data_collection_default_enabled", true);
        } else {
            try {
                PackageManager packageManager = this.A00.getPackageManager();
                if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(this.A00.getPackageName(), 128)) == null || (bundle = applicationInfo.metaData) == null || !bundle.containsKey("firebase_data_collection_default_enabled"))) {
                    z = applicationInfo.metaData.getBoolean("firebase_data_collection_default_enabled");
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
            z = true;
        }
        this.A08 = new AtomicBoolean(z);
        new AnonymousClass1WS();
        Context context2 = (Context) new AnonymousClass1WR(context).A00;
        Bundle bundle2 = null;
        try {
            PackageManager packageManager2 = context2.getPackageManager();
            if (packageManager2 == null) {
                Log.w("ComponentDiscovery", "Context has no PackageManager.");
            } else {
                ServiceInfo serviceInfo = packageManager2.getServiceInfo(new ComponentName(context2, ComponentDiscoveryService.class), 128);
                if (serviceInfo == null) {
                    Log.w("ComponentDiscovery", "ComponentDiscoveryService has no service info.");
                } else {
                    bundle2 = serviceInfo.metaData;
                }
            }
        } catch (PackageManager.NameNotFoundException unused2) {
            Log.w("ComponentDiscovery", "Application info not found.");
        }
        if (bundle2 == null) {
            Log.w("ComponentDiscovery", "Could not retrieve metadata, returning empty list of registrars.");
            arrayList = Collections.emptyList();
        } else {
            arrayList = new ArrayList<>();
            for (String next : bundle2.keySet()) {
                if ("com.google.firebase.components.ComponentRegistrar".equals(bundle2.get(next)) && next.startsWith("com.google.firebase.components:")) {
                    arrayList.add(next.substring(31));
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (String str3 : arrayList) {
            try {
                Class<?> cls = Class.forName(str3);
                if (!Registrar.class.isAssignableFrom(cls)) {
                    Log.w("ComponentDiscovery", String.format("Class %s is not an instance of %s", str3, "com.google.firebase.components.ComponentRegistrar"));
                } else {
                    arrayList2.add((Registrar) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                }
            } catch (ClassNotFoundException e) {
                Log.w("ComponentDiscovery", String.format("Class %s is not an found.", str3), e);
            } catch (IllegalAccessException | InstantiationException e2) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", str3), e2);
            } catch (NoSuchMethodException | InvocationTargetException e3) {
                Log.w("ComponentDiscovery", String.format("Could not instantiate %s", str3), e3);
            }
        }
        Executor executor = A0H;
        AnonymousClass1WX A002 = AnonymousClass1WX.A00(context, Context.class, new Class[0]);
        AnonymousClass1WX A003 = AnonymousClass1WX.A00(this, AnonymousClass1WN.class, new Class[0]);
        AnonymousClass1WX A004 = AnonymousClass1WX.A00(r5, C04170Ss.class, new Class[0]);
        AnonymousClass1WX A012 = A01("fire-android", BuildConfig.FLAVOR);
        AnonymousClass1WX A013 = A01("fire-core", "16.1.0");
        AnonymousClass1WY r7 = new AnonymousClass1WY(C24591We.class, new Class[0]);
        r7.A01(new C24601Wf(C24571Wc.class, 2, 0));
        AnonymousClass1Wg r1 = AnonymousClass1Wg.A00;
        AnonymousClass09E.A02(r1, "Null factory");
        r7.A02 = r1;
        AnonymousClass1WT r3 = new AnonymousClass1WT(executor, arrayList2, A002, A003, A004, A012, A013, r7.A00());
        this.A03 = r3;
        r3.A02(AnonymousClass1Wi.class);
    }

    private static void A04(Class cls, Object obj, Iterable iterable, boolean z) {
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (z) {
                try {
                    if (!A0F.contains(str)) {
                    }
                } catch (ClassNotFoundException unused) {
                    if (A0G.contains(str)) {
                        throw new IllegalStateException(AnonymousClass08S.A0J(str, " is missing, but is required. Check if it has been removed by Proguard."));
                    }
                } catch (NoSuchMethodException unused2) {
                    throw new IllegalStateException(AnonymousClass08S.A0J(str, "#getInstance has been removed by Proguard. Add keep rule to prevent it."));
                } catch (InvocationTargetException e) {
                    Log.wtf("FirebaseApp", "Firebase API initialization failure.", e);
                } catch (IllegalAccessException e2) {
                    Log.wtf("FirebaseApp", AnonymousClass08S.A0J("Failed to initialize ", str), e2);
                }
            }
            Method method = Class.forName(str).getMethod("getInstance", cls);
            int modifiers = method.getModifiers();
            if (Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers)) {
                method.invoke(null, obj);
            }
        }
    }
}
