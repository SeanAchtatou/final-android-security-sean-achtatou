package X;

import java.io.DataInput;
import java.io.DataOutput;

/* renamed from: X.0DX  reason: invalid class name */
public final class AnonymousClass0DX extends C03160Kg {
    public long A00() {
        return -4085774432413599882L;
    }

    public void A01(AnonymousClass0FM r3, DataOutput dataOutput) {
        AnonymousClass0FL r32 = (AnonymousClass0FL) r3;
        dataOutput.writeInt(r32.bleScanCount);
        dataOutput.writeLong(r32.bleScanDurationMs);
        dataOutput.writeInt(r32.bleOpportunisticScanCount);
        dataOutput.writeLong(r32.bleOpportunisticScanDurationMs);
    }

    public boolean A03(AnonymousClass0FM r3, DataInput dataInput) {
        AnonymousClass0FL r32 = (AnonymousClass0FL) r3;
        r32.bleScanCount = dataInput.readInt();
        r32.bleScanDurationMs = dataInput.readLong();
        r32.bleOpportunisticScanCount = dataInput.readInt();
        r32.bleOpportunisticScanDurationMs = dataInput.readLong();
        return true;
    }
}
