package X;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import io.card.payment.BuildConfig;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.0Ai  reason: invalid class name and case insensitive filesystem */
public final class C01460Ai {
    public final Context A00;
    public final C01600Aw A01;
    public final String A02;
    public final String A03;
    private final String A04;

    private String A00(String str) {
        String str2;
        if (str != null) {
            int length = str.length();
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                if (charAt == '&') {
                    sb.append("&amp;");
                } else if (charAt < ' ' || charAt > '~') {
                    sb.append("&#");
                    sb.append(Integer.toString(charAt));
                    sb.append(";");
                } else {
                    sb.append(charAt);
                }
            }
            str2 = sb.toString();
        } else {
            str2 = BuildConfig.FLAVOR;
        }
        return str2.replace("/", "-").replace(";", "-");
    }

    public String A01() {
        String str;
        boolean z;
        String str2;
        WindowManager windowManager;
        String str3 = this.A04;
        if (str3 != null) {
            return str3;
        }
        String str4 = this.A02;
        C01600Aw r1 = this.A01;
        String str5 = r1.A01;
        String str6 = r1.A00;
        DisplayMetrics displayMetrics = this.A00.getResources().getDisplayMetrics();
        Point point = new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
        if (!(Build.VERSION.SDK_INT < 14 || (windowManager = (WindowManager) C009207y.A01.A01(this.A00, "window", WindowManager.class)) == null || windowManager.getDefaultDisplay() == null)) {
            windowManager.getDefaultDisplay().getSize(point);
        }
        String A002 = A00("{density=" + displayMetrics.density + ",width=" + point.x + ",height=" + point.y + "}");
        String str7 = this.A03;
        if (str7 == null) {
            str7 = Locale.getDefault().toString();
        }
        String format = String.format(null, "%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;", "FBAN", str4, "FBAV", str5, "FBBV", str6, "FBDM", A002, "FBLC", A00(str7));
        TelephonyManager telephonyManager = (TelephonyManager) C009207y.A01.A01(this.A00, "phone", TelephonyManager.class);
        if (telephonyManager != null) {
            str = telephonyManager.getNetworkOperatorName();
        } else {
            str = BuildConfig.FLAVOR;
        }
        String A003 = A00(str);
        String A004 = A00(Build.MANUFACTURER);
        String A005 = A00(Build.BRAND);
        String packageName = this.A00.getPackageName();
        String A006 = A00(Build.MODEL);
        String A007 = A00(Build.VERSION.RELEASE);
        try {
            z = this.A00.getPackageManager().hasSystemFeature("android.hardware.ram.low");
        } catch (Exception unused) {
            z = false;
        }
        if (z) {
            str2 = "1";
        } else {
            str2 = "0";
        }
        return AnonymousClass08S.A0U("[", format, String.format(null, "%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;", "FBCR", A003, "FBMF", A004, "FBBD", A005, "FBPN", packageName, "FBDV", A006, "FBSV", A007, "FBLR", A00(str2)), String.format(null, "%s/%s;", "FBBK", "1"), String.format(null, "%s/%s:%s;", "FBCA", A00(Build.CPU_ABI), A00(Build.CPU_ABI2)), "]");
    }

    public C01460Ai(Context context, C01600Aw r3, String str, Map map, String str2) {
        String str3;
        this.A00 = context;
        this.A01 = r3;
        this.A02 = str;
        if (map != null) {
            str3 = (String) map.get("User-Agent");
        } else {
            str3 = null;
        }
        this.A04 = str3;
        this.A03 = str2;
    }
}
