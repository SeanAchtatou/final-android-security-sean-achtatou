package X;

/* renamed from: X.0UR  reason: invalid class name */
public abstract class AnonymousClass0UR implements AnonymousClass0US {
    private Object A00;
    private final byte A01 = AnonymousClass0UO.A00().A00;
    private final AnonymousClass1XY A02;
    private volatile C24861Xj A03;

    public Object A01(AnonymousClass1XY r5) {
        AnonymousClass0UQ r1 = (AnonymousClass0UQ) this;
        try {
            return AnonymousClass1Y4.A00(r1.A00, r5);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format("Invalid binding id %d", Integer.valueOf(r1.A00)), e);
        }
    }

    public final Object get() {
        if (this.A03 != null) {
            synchronized (this) {
                if (this.A03 != null) {
                    C24861Xj r4 = this.A03;
                    AnonymousClass0UO A002 = AnonymousClass0UO.A00();
                    byte b = this.A01;
                    byte b2 = A002.A00;
                    A002.A00 = (byte) (b | b2);
                    Object AYf = r4.AYf();
                    try {
                        this.A00 = A01(this.A02.getScopeUnawareInjector());
                        this.A03 = null;
                        r4.AZH(AYf);
                        A002.A00 = b2;
                    } catch (Throwable th) {
                        r4.AZH(AYf);
                        A002.A00 = b2;
                        throw th;
                    }
                }
            }
        }
        return this.A00;
    }

    public AnonymousClass0UR(AnonymousClass1XY r2) {
        this.A02 = r2;
        this.A03 = r2.getScopeAwareInjector();
    }
}
