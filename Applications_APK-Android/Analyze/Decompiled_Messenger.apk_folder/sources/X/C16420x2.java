package X;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.widget.recyclerview.BetterRecyclerView;
import java.util.Iterator;

/* renamed from: X.0x2  reason: invalid class name and case insensitive filesystem */
public final class C16420x2 extends AnonymousClass1A9 {
    public final /* synthetic */ BetterRecyclerView A00;

    public C16420x2(BetterRecyclerView betterRecyclerView) {
        this.A00 = betterRecyclerView;
    }

    public void A07(RecyclerView recyclerView, int i) {
        BetterRecyclerView betterRecyclerView = this.A00;
        if (i != betterRecyclerView.A03) {
            betterRecyclerView.A03 = i;
            Iterator it = betterRecyclerView.A0L.iterator();
            while (it.hasNext()) {
                ((AnonymousClass1A9) it.next()).A07(recyclerView, i);
            }
        }
    }
}
