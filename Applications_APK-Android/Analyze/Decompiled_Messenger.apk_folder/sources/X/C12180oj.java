package X;

import android.database.Cursor;
import com.fasterxml.jackson.databind.JsonNode;

/* renamed from: X.0oj  reason: invalid class name and case insensitive filesystem */
public final class C12180oj {
    public static final String[] A0t;
    public AnonymousClass0UN A00;
    public AnonymousClass0V0 A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final C12160of A0A;
    public final C04310Tq A0B;
    private final int A0C;
    private final int A0D;
    private final int A0E;
    private final int A0F;
    private final int A0G;
    private final int A0H;
    private final int A0I;
    private final int A0J;
    private final int A0K;
    private final int A0L;
    private final int A0M;
    private final int A0N;
    private final int A0O;
    private final int A0P;
    private final int A0Q;
    private final int A0R;
    private final int A0S;
    private final int A0T;
    private final int A0U;
    private final int A0V;
    private final int A0W;
    private final int A0X;
    private final int A0Y;
    private final int A0Z;
    private final int A0a;
    private final int A0b;
    private final int A0c;
    private final int A0d;
    private final int A0e;
    private final int A0f;
    private final int A0g;
    private final int A0h;
    private final AnonymousClass0jE A0i = C05040Xk.A00();
    private final C28521ew A0j;
    private final C28531ex A0k;
    private final C28511ev A0l;
    private final C28501eu A0m;
    private final C28561f0 A0n;
    private final C28491et A0o;
    private final C28481es A0p;
    private final C28541ey A0q;
    private final C13710ru A0r;
    private final C28551ez A0s;

    static {
        String[] strArr = new String[41];
        System.arraycopy(new String[]{"name", "pic_hash", "pic", "missed_call_status", "custom_like_emoji", "outgoing_message_lifetime", "custom_nicknames", "last_message_id_if_sponsored", "group_chat_rank", "rtc_call_info", "last_message_commerce_message_type", "is_thread_queue_enabled", "media_preview", "booking_requests", "last_call_ms", "last_sponsored_message_call_to_action", "last_message_admin_text_type", "marketplace_data", "montage_thread_key", "inbox_to_montage_preview_message_id", "inbox_to_montage_preview_attachments", "inbox_to_montage_preview_text", "inbox_to_montage_preview_sticker_id", "inbox_to_montage_latest_message_timestamp_ms", "inbox_to_montage_thread_read_watermark_timestamp_ms", "inbox_to_montage_sender", "optimistic_group_state"}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{"ad_context_data", "use_existing_group", "games_push_notification_settings", "animated_thread_activity_banner", "last_message_breadcrumb_type", "last_message_breadcrumb_cta", "group_thread_subtype", "is_page_follow_up", "last_message_id", "ads_qp_update_data", "messenger_request_appointment_data", "related_page_thread_data", "thread_message_assigned_page_admin", "has_non_admin_message"}, 0, strArr, 27, 14);
        A0t = strArr;
    }

    public static String A00(JsonNode jsonNode, String str) {
        if (jsonNode == null || jsonNode.get(str) == null || jsonNode.get(str).getNodeType() == C11980oL.NULL) {
            return null;
        }
        return jsonNode.get(str).asText();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v291, resolved type: com.facebook.messaging.model.threads.ProfileSellingInvoice} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v315, resolved type: com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum} */
    /* JADX WARN: Type inference failed for: r0v290 */
    /* JADX WARN: Type inference failed for: r0v438 */
    /* JADX WARN: Type inference failed for: r0v439 */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0348, code lost:
        if (r6.get("will_expand_to_new_thread").booleanValue() == false) goto L_0x034a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x04f7, code lost:
        if (r4 != false) goto L_0x04f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x009e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a2, code lost:
        throw r0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.C17920zh r16, X.C13680rr r17) {
        /*
            r15 = this;
            X.0V0 r0 = r15.A01
            r4 = r17
            if (r0 != 0) goto L_0x00a6
            com.google.common.collect.ArrayListMultimap r0 = new com.google.common.collect.ArrayListMultimap
            r0.<init>()
            r15.A01 = r0
            com.google.common.collect.ImmutableList r1 = r4.A00()
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x00a6
            X.0Tq r0 = r15.A0B
            java.lang.Object r0 = r0.get()
            X.1aN r0 = (X.C25771aN) r0
            android.database.sqlite.SQLiteDatabase r7 = r0.A06()
            X.0of r0 = r15.A0A
            long r5 = r0.now()
            java.util.ArrayList r2 = new java.util.ArrayList
            int r0 = r1.size()
            r2.<init>(r0)
            X.1Xv r1 = r1.iterator()
        L_0x0036:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004a
            java.lang.Object r0 = r1.next()
            com.facebook.messaging.model.threadkey.ThreadKey r0 = (com.facebook.messaging.model.threadkey.ThreadKey) r0
            java.lang.String r0 = r0.A0J()
            r2.add(r0)
            goto L_0x0036
        L_0x004a:
            java.lang.String r0 = "thread_key"
            X.0av r3 = X.C06160ax.A04(r0, r2)
            r0 = 604800000(0x240c8400, double:2.988109026E-315)
            long r5 = r5 - r0
            r0 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r0
            java.lang.String r2 = java.lang.String.valueOf(r5)
            java.lang.String r1 = "event_reminder_timestamp"
            X.13P r0 = new X.13P
            r0.<init>(r1, r2)
            X.0av[] r0 = new X.C06140av[]{r3, r0}
            X.1a6 r0 = X.C06160ax.A01(r0)
            r9 = 0
            java.lang.String r10 = r0.A02()
            java.lang.String[] r11 = r0.A04()
            r12 = 0
            r13 = 0
            java.lang.String r8 = "event_reminders"
            java.lang.String r14 = "event_reminder_timestamp ASC"
            android.database.Cursor r0 = r7.query(r8, r9, r10, r11, r12, r13, r14)
            X.1g4 r5 = new X.1g4
            r5.<init>(r0)
            java.util.Iterator r3 = r5.iterator()     // Catch:{ all -> 0x009c }
        L_0x0086:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x00a3
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x009c }
            X.3AG r0 = (X.AnonymousClass3AG) r0     // Catch:{ all -> 0x009c }
            X.0V0 r2 = r15.A01     // Catch:{ all -> 0x009c }
            com.facebook.messaging.model.threadkey.ThreadKey r1 = r0.A00     // Catch:{ all -> 0x009c }
            com.facebook.messaging.model.threads.ThreadEventReminder r0 = r0.A01     // Catch:{ all -> 0x009c }
            r2.Byx(r1, r0)     // Catch:{ all -> 0x009c }
            goto L_0x0086
        L_0x009c:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x009e }
        L_0x009e:
            r0 = move-exception
            r5.close()     // Catch:{ all -> 0x00a2 }
        L_0x00a2:
            throw r0
        L_0x00a3:
            r5.close()
        L_0x00a6:
            android.database.Cursor r2 = r4.A00
            int r0 = r15.A0Z
            boolean r0 = r2.isNull(r0)
            r3 = r16
            if (r0 != 0) goto L_0x00ba
            int r0 = r15.A0Z
            java.lang.String r0 = r2.getString(r0)
            r3.A0p = r0
        L_0x00ba:
            int r0 = r15.A0d
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x00ce
            int r0 = r15.A0d
            java.lang.String r0 = r2.getString(r0)
            java.lang.String r0 = com.google.common.base.Strings.emptyToNull(r0)
            r3.A0q = r0
        L_0x00ce:
            int r0 = r15.A0c
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x00e2
            int r0 = r15.A0c
            java.lang.String r0 = r2.getString(r0)
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r3.A0D = r0
        L_0x00e2:
            int r0 = r15.A0W
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x0137
            int r0 = r15.A0W
            java.lang.String r5 = r2.getString(r0)
            X.1es r4 = r15.A0p
            boolean r0 = X.C06850cB.A0B(r5)
            r1 = 0
            if (r0 != 0) goto L_0x0135
            X.0jD r0 = r4.A00
            com.fasterxml.jackson.databind.JsonNode r4 = r0.A02(r5)
            java.lang.String r0 = "type"
            com.fasterxml.jackson.databind.JsonNode r5 = r4.get(r0)
            java.lang.String r0 = "thumbnail_uri"
            com.fasterxml.jackson.databind.JsonNode r4 = r4.get(r0)
            com.google.common.base.Preconditions.checkNotNull(r5)
            java.lang.String r0 = r5.textValue()
            X.37D r0 = X.AnonymousClass37D.valueOf(r0)
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x0135;
                case 1: goto L_0x0135;
                case 2: goto L_0x0135;
                case 3: goto L_0x0135;
                case 4: goto L_0x0135;
                case 5: goto L_0x0135;
                case 6: goto L_0x0123;
                default: goto L_0x011d;
            }
        L_0x011d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0123:
            com.google.common.base.Preconditions.checkNotNull(r4)
            java.lang.String r0 = r4.textValue()
            android.net.Uri r4 = android.net.Uri.parse(r0)
            com.facebook.messaging.model.threads.ThreadMediaPreview r1 = new com.facebook.messaging.model.threads.ThreadMediaPreview
            X.37D r0 = X.AnonymousClass37D.SPONSORED_MESSAGE_IMAGE
            r1.<init>(r0, r4)
        L_0x0135:
            r3.A0e = r1
        L_0x0137:
            int r0 = r15.A0Y
            int r7 = r2.getInt(r0)
            X.0zi[] r6 = X.C17930zi.A00
            int r5 = r6.length
            r4 = 0
        L_0x0141:
            if (r4 >= r5) goto L_0x014c
            r1 = r6[r4]
            int r0 = r1.mValue
            if (r0 == r7) goto L_0x014e
            int r4 = r4 + 1
            goto L_0x0141
        L_0x014c:
            X.0zi r1 = X.C17930zi.NONE
        L_0x014e:
            r3.A0h = r1
            X.1g5 r4 = new X.1g5
            r4.<init>()
            int r0 = r15.A0G
            java.lang.String r0 = r2.getString(r0)
            r4.A01 = r0
            com.facebook.messaging.model.threads.NicknamesMap r1 = new com.facebook.messaging.model.threads.NicknamesMap
            int r0 = r15.A02
            boolean r0 = r2.isNull(r0)
            if (r0 == 0) goto L_0x0364
            r0 = 0
        L_0x0168:
            r1.<init>(r0)
            r4.A00 = r1
            com.facebook.messaging.model.threads.ThreadCustomization r0 = new com.facebook.messaging.model.threads.ThreadCustomization
            r0.<init>(r4)
            r3.A0d = r0
            int r0 = r15.A0b
            int r0 = r2.getInt(r0)
            r3.A01 = r0
            int r0 = r15.A0T
            boolean r0 = r2.isNull(r0)
            r5 = 0
            if (r0 == 0) goto L_0x035c
            r0 = r5
        L_0x0186:
            r3.A0o = r0
            int r0 = r15.A0I
            float r0 = r2.getFloat(r0)
            r3.A00 = r0
            X.0V0 r1 = r15.A01
            com.facebook.messaging.model.threadkey.ThreadKey r0 = r3.A0R
            java.util.Collection r0 = r1.AbK(r0)
            if (r0 == 0) goto L_0x0358
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.ImmutableList.copyOf(r0)
        L_0x019e:
            r3.A0u = r0
            X.0ru r4 = r15.A0r
            int r0 = r15.A0f
            java.lang.String r1 = r2.getString(r0)
            r7 = 0
            if (r1 != 0) goto L_0x02ed
            r0 = r5
        L_0x01ac:
            r3.A0g = r0
            int r0 = r15.A0R
            boolean r0 = r2.isNull(r0)
            if (r0 == 0) goto L_0x02e5
            r0 = r5
        L_0x01b7:
            r3.A0m = r0
            int r0 = r15.A0M
            int r0 = r2.getInt(r0)
            if (r0 != 0) goto L_0x02df
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.UNSET
        L_0x01c3:
            r3.A0E = r0
            int r0 = r15.A0F
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x038d
            X.1et r10 = r15.A0o
            int r0 = r15.A0F
            java.lang.String r1 = r2.getString(r0)
            if (r1 == 0) goto L_0x036c
            java.lang.String r0 = "{}"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x036c
            X.0jD r0 = r10.A00
            com.fasterxml.jackson.databind.JsonNode r4 = r0.A02(r1)
            X.37F r1 = new X.37F
            r1.<init>()
            com.google.common.collect.ImmutableList r0 = com.facebook.messaging.model.threads.ThreadBookingRequests.A04
            X.1Xv r12 = r0.iterator()
        L_0x01f0:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x036e
            java.lang.Object r9 = r12.next()
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r9 = (com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus) r9
            java.lang.String r8 = r9.toString()
            java.util.Locale r7 = java.util.Locale.US
            java.lang.String r6 = r8.toLowerCase(r7)
            java.lang.String r0 = "_booking_request_count"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r6, r0)
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r0)
            int r0 = r0.asInt()
            java.util.HashMap r6 = r1.A02
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r6.put(r9, r0)
            r9.toString()
            java.lang.String r6 = r8.toLowerCase(r7)
            java.lang.String r0 = "_booking_request_detail"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r6, r0)
            java.lang.String r6 = X.C28491et.A01(r4, r0)
            if (r6 == 0) goto L_0x02dc
            java.lang.String r0 = "{}"
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x02dc
            X.0jD r0 = r10.A00
            com.fasterxml.jackson.databind.JsonNode r11 = r0.A02(r6)
            X.3fA r8 = new X.3fA
            r8.<init>()
            java.lang.String r0 = "request_id"
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A07 = r0
            r0 = 86
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A08 = r0
            r0 = 859(0x35b, float:1.204E-42)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            java.lang.String r6 = X.C28491et.A01(r11, r0)
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r0 = com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r6, r0)
            com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus r0 = (com.facebook.graphql.enums.GraphQLPagesPlatformNativeBookingStatus) r0
            r8.A01 = r0
            java.lang.String r0 = "localized_booking_status"
            com.fasterxml.jackson.databind.JsonNode r0 = r11.get(r0)
            java.lang.String r0 = r0.asText()
            r8.A02 = r0
            java.lang.String r7 = "start_time_sec"
            if (r11 == 0) goto L_0x02da
            com.fasterxml.jackson.databind.JsonNode r0 = r11.get(r7)
            if (r0 == 0) goto L_0x02da
            X.0oL r6 = r11.getNodeType()
            X.0oL r0 = X.C11980oL.NULL
            if (r6 == r0) goto L_0x02da
            com.fasterxml.jackson.databind.JsonNode r0 = r11.get(r7)
            long r6 = r0.asLong()
            java.lang.Long r0 = java.lang.Long.valueOf(r6)
        L_0x0295:
            long r6 = r0.longValue()
            r8.A00 = r6
            java.lang.String r0 = "page_name"
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A05 = r0
            java.lang.String r0 = "page_id"
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A04 = r0
            java.lang.String r0 = "localized_price"
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A03 = r0
            java.lang.String r0 = "profile_pic_url"
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A06 = r0
            java.lang.String r0 = "user_id"
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A09 = r0
            java.lang.String r0 = "user_name"
            java.lang.String r0 = X.C28491et.A01(r11, r0)
            r8.A0A = r0
            com.facebook.messaging.model.threads.BookingRequestDetail r0 = new com.facebook.messaging.model.threads.BookingRequestDetail
            r0.<init>(r8)
            r6 = r0
        L_0x02d1:
            if (r0 == 0) goto L_0x01f0
            java.util.HashMap r0 = r1.A03
            r0.put(r9, r6)
            goto L_0x01f0
        L_0x02da:
            r0 = r5
            goto L_0x0295
        L_0x02dc:
            r0 = 0
            r6 = r5
            goto L_0x02d1
        L_0x02df:
            com.facebook.common.util.TriState r0 = com.facebook.common.util.TriState.fromDbValue(r0)
            goto L_0x01c3
        L_0x02e5:
            int r0 = r15.A0R
            java.lang.String r0 = r2.getString(r0)
            goto L_0x01b7
        L_0x02ed:
            X.0jD r0 = r4.A00
            com.fasterxml.jackson.databind.JsonNode r6 = r0.A02(r1)
            X.1g7 r4 = new X.1g7
            r4.<init>()
            r0 = 201(0xc9, float:2.82E-43)
            java.lang.String r1 = X.AnonymousClass24B.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            if (r0 == 0) goto L_0x0356
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            java.lang.String r0 = r0.textValue()
        L_0x030c:
            r4.A00(r0)
            r0 = 221(0xdd, float:3.1E-43)
            java.lang.String r1 = X.C99084oO.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            if (r0 == 0) goto L_0x0354
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            java.lang.String r0 = r0.textValue()
        L_0x0323:
            r4.A02 = r0
            java.lang.String r1 = "initiator_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            if (r0 == 0) goto L_0x0335
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            java.lang.String r7 = r0.textValue()
        L_0x0335:
            r4.A01 = r7
            java.lang.String r1 = "will_expand_to_new_thread"
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            if (r0 == 0) goto L_0x034a
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r1)
            boolean r1 = r0.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x034b
        L_0x034a:
            r0 = 0
        L_0x034b:
            r4.A03 = r0
            com.facebook.messaging.model.threads.ThreadRtcCallInfoData r0 = new com.facebook.messaging.model.threads.ThreadRtcCallInfoData
            r0.<init>(r4)
            goto L_0x01ac
        L_0x0354:
            r0 = r5
            goto L_0x0323
        L_0x0356:
            r0 = r5
            goto L_0x030c
        L_0x0358:
            com.google.common.collect.ImmutableList r0 = com.google.common.collect.RegularImmutableList.A02
            goto L_0x019e
        L_0x035c:
            int r0 = r15.A0T
            java.lang.String r0 = r2.getString(r0)
            goto L_0x0186
        L_0x0364:
            int r0 = r15.A02
            java.lang.String r0 = r2.getString(r0)
            goto L_0x0168
        L_0x036c:
            r0 = r5
            goto L_0x038b
        L_0x036e:
            java.lang.String r0 = "page_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r0)
            java.lang.String r0 = r0.asText()
            r1.A00 = r0
            java.lang.String r0 = "page_name"
            com.fasterxml.jackson.databind.JsonNode r0 = r4.get(r0)
            java.lang.String r0 = r0.asText()
            r1.A01 = r0
            com.facebook.messaging.model.threads.ThreadBookingRequests r0 = new com.facebook.messaging.model.threads.ThreadBookingRequests
            r0.<init>(r1)
        L_0x038b:
            r3.A0b = r0
        L_0x038d:
            int r0 = r15.A0N
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x039d
            int r0 = r15.A0N
            long r0 = r2.getLong(r0)
            r3.A04 = r0
        L_0x039d:
            int r0 = r15.A0O
            java.lang.String r1 = r2.getString(r0)
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r0 = com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType.A3z
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r1, r0)
            com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType r0 = (com.facebook.graphql.enums.GraphQLExtensibleMessageAdminTextType) r0
            r3.A0F = r0
            int r0 = r15.A0U
            boolean r0 = r2.isNull(r0)
            r1 = 0
            if (r0 != 0) goto L_0x03d0
            int r0 = r15.A0U
            java.lang.String r0 = r2.getString(r0)
            com.google.common.collect.ImmutableList r4 = X.C29941hE.A02(r0)
            if (r4 == 0) goto L_0x03d0
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x03d0
            java.lang.Object r0 = r4.get(r1)
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = (com.facebook.messaging.business.common.calltoaction.model.CallToAction) r0
            r3.A0K = r0
        L_0x03d0:
            X.1ev r12 = r15.A0l
            int r0 = r15.A0V
            java.lang.String r7 = r2.getString(r0)
            if (r7 == 0) goto L_0x0579
            java.lang.String r0 = ""
            boolean r0 = r7.equals(r0)
            if (r0 != 0) goto L_0x0579
            int r4 = X.AnonymousClass1Y3.A1l
            X.0UN r0 = r12.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r4, r0)
            X.0jD r0 = (X.AnonymousClass0jD) r0
            com.fasterxml.jackson.databind.JsonNode r8 = r0.A02(r7)
            X.2cG r7 = new X.2cG
            r7.<init>()
            java.lang.String r0 = "buyer"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            com.facebook.messaging.model.threads.MarketplaceThreadUserData r0 = X.C28511ev.A01(r12, r0)
            if (r0 == 0) goto L_0x0407
            r7.A00 = r0
        L_0x0407:
            java.lang.String r0 = "seller"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            com.facebook.messaging.model.threads.MarketplaceThreadUserData r0 = X.C28511ev.A01(r12, r0)
            if (r0 == 0) goto L_0x0419
            r7.A01 = r0
        L_0x0419:
            java.lang.String r0 = "attached_invoice"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            java.lang.String r10 = com.facebook.common.util.JSONUtil.A0N(r0)
            r0 = 0
            if (r10 == 0) goto L_0x0474
            java.lang.String r11 = ""
            boolean r4 = r10.equals(r11)
            if (r4 != 0) goto L_0x0474
            int r6 = X.AnonymousClass1Y3.A1l
            X.0UN r4 = r12.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r6, r4)
            X.0jD r4 = (X.AnonymousClass0jD) r4
            com.fasterxml.jackson.databind.JsonNode r10 = r4.A02(r10)
            X.3f8 r6 = new X.3f8
            r6.<init>()
            java.lang.String r4 = "id"
            com.fasterxml.jackson.databind.JsonNode r4 = r10.get(r4)
            java.lang.String r4 = com.facebook.common.util.JSONUtil.A0N(r4)
            r6.A01 = r4
            java.lang.String r9 = "invoice_status"
            com.fasterxml.jackson.databind.JsonNode r4 = r10.get(r9)
            java.lang.String r4 = com.facebook.common.util.JSONUtil.A0N(r4)
            boolean r4 = r4.equals(r11)
            if (r4 != 0) goto L_0x046d
            com.fasterxml.jackson.databind.JsonNode r0 = r10.get(r9)
            java.lang.String r4 = com.facebook.common.util.JSONUtil.A0N(r0)
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r0 = com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum.A03
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r4, r0)
            com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum r0 = (com.facebook.graphql.enums.GraphQLPaymentInvoiceStatusEnum) r0
        L_0x046d:
            r6.A00 = r0
            com.facebook.messaging.model.threads.ProfileSellingInvoice r0 = new com.facebook.messaging.model.threads.ProfileSellingInvoice
            r0.<init>(r6)
        L_0x0474:
            if (r0 == 0) goto L_0x0478
            r7.A02 = r0
        L_0x0478:
            java.lang.String r0 = "for_sale_item_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r7.A04 = r0
            java.lang.String r0 = "is_from_marketplace"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r7.A09 = r0
            java.lang.String r0 = "conversation_id"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            java.lang.String r0 = com.facebook.common.util.JSONUtil.A0N(r0)
            r7.A03 = r0
            java.lang.String r0 = "is_seller_profile"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r7.A08 = r0
            java.lang.String r0 = "is_seller_page"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r7.A07 = r0
            r0 = 205(0xcd, float:2.87E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r7.A05 = r0
            java.lang.String r0 = "is_mark_as_paid_enabled"
            com.fasterxml.jackson.databind.JsonNode r0 = r8.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r7.A06 = r0
            com.facebook.messaging.model.threads.MarketplaceThreadData r0 = new com.facebook.messaging.model.threads.MarketplaceThreadData
            r0.<init>(r7)
        L_0x04d5:
            r3.A0V = r0
            int r0 = r15.A09
            boolean r0 = r2.isNull(r0)
            r7 = 0
            if (r0 != 0) goto L_0x0576
            int r0 = r15.A09
            java.lang.String r0 = r2.getString(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r0 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)
        L_0x04ea:
            r3.A0Q = r0
            if (r0 == 0) goto L_0x0561
            int r0 = r15.A05
            if (r0 < 0) goto L_0x04f9
            boolean r4 = r2.isNull(r0)
            r0 = 1
            if (r4 == 0) goto L_0x04fa
        L_0x04f9:
            r0 = 0
        L_0x04fa:
            if (r0 == 0) goto L_0x0561
            int r0 = r15.A03
            long r11 = r2.getLong(r0)
            int r0 = r15.A05
            java.lang.String r9 = r2.getString(r0)
            r6 = 1
            int r4 = X.AnonymousClass1Y3.AOI
            X.0UN r0 = r15.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r6, r4, r0)
            X.0jC r4 = (X.AnonymousClass0jC) r4
            int r0 = r15.A06
            java.lang.String r0 = r2.getString(r0)
            com.facebook.messaging.model.messages.ParticipantInfo r13 = r4.A03(r0)
            int r4 = X.AnonymousClass1Y3.A0X
            X.0UN r0 = r15.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r1, r4, r0)
            X.0oH r4 = (X.C11940oH) r4
            int r0 = r15.A04
            java.lang.String r0 = r2.getString(r0)
            com.google.common.collect.ImmutableList r4 = r4.A03(r0, r9)
            X.1V7 r0 = X.AnonymousClass1V7.A0K
            X.2Br r10 = com.facebook.messaging.model.threads.MontageThreadPreview.A00(r0)
            X.2VF r8 = new X.2VF
            r8.<init>(r9, r10, r11, r13)
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x0548
            java.lang.Object r7 = r4.get(r1)
            com.facebook.messaging.model.attachment.Attachment r7 = (com.facebook.messaging.model.attachment.Attachment) r7
        L_0x0548:
            r8.A01 = r7
            int r0 = r15.A07
            java.lang.String r0 = r2.getString(r0)
            r8.A06 = r0
            int r0 = r15.A08
            java.lang.String r0 = r2.getString(r0)
            r8.A07 = r0
            com.facebook.messaging.model.threads.MontageThreadPreview r0 = new com.facebook.messaging.model.threads.MontageThreadPreview
            r0.<init>(r8)
            r3.A0W = r0
        L_0x0561:
            int r0 = r15.A0a
            int r9 = r2.getInt(r0)
            X.105[] r8 = X.AnonymousClass105.A00
            int r7 = r8.length
            r6 = 0
        L_0x056b:
            if (r6 >= r7) goto L_0x057c
            r4 = r8[r6]
            int r0 = r4.dbValue
            if (r0 == r9) goto L_0x057e
            int r6 = r6 + 1
            goto L_0x056b
        L_0x0576:
            r0 = r5
            goto L_0x04ea
        L_0x0579:
            r0 = r5
            goto L_0x04d5
        L_0x057c:
            X.105 r4 = X.AnonymousClass105.A03
        L_0x057e:
            r3.A0Y = r4
            X.1ew r8 = r15.A0j
            int r0 = r15.A0C
            java.lang.String r7 = r2.getString(r0)
            boolean r0 = X.C06850cB.A0B(r7)
            r10 = 0
            if (r0 != 0) goto L_0x071f
            java.lang.String r0 = "{}"
            boolean r0 = r7.equals(r0)
            if (r0 != 0) goto L_0x071f
            int r4 = X.AnonymousClass1Y3.A1l
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r4, r0)
            X.0jD r0 = (X.AnonymousClass0jD) r0
            com.fasterxml.jackson.databind.JsonNode r9 = r0.A02(r7)
            X.13e r7 = new X.13e
            r7.<init>()
            java.lang.String r0 = "ad_id"
            com.fasterxml.jackson.databind.JsonNode r11 = r9.get(r0)
            r0 = 6
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r4 = r9.get(r0)
            java.lang.String r0 = "subtitle"
            com.fasterxml.jackson.databind.JsonNode r8 = r9.get(r0)
            java.lang.String r0 = "title"
            com.fasterxml.jackson.databind.JsonNode r6 = r9.get(r0)
            if (r11 == 0) goto L_0x071c
            java.lang.String r0 = r11.asText()
        L_0x05cb:
            r7.A01 = r0
            if (r4 == 0) goto L_0x0719
            java.lang.String r4 = r4.asText()
        L_0x05d3:
            boolean r0 = com.google.common.base.Platform.stringIsNullOrEmpty(r4)
            if (r0 == 0) goto L_0x0713
            r0 = 0
        L_0x05da:
            r7.A00 = r0
            if (r8 == 0) goto L_0x0710
            java.lang.String r0 = r8.asText()
        L_0x05e2:
            r7.A02 = r0
            if (r6 == 0) goto L_0x05ea
            java.lang.String r10 = r6.asText()
        L_0x05ea:
            r7.A03 = r10
            java.lang.String r0 = "pma_banner"
            com.fasterxml.jackson.databind.JsonNode r0 = r9.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r7.A04 = r0
            com.facebook.messaging.model.threads.AdContextData r0 = new com.facebook.messaging.model.threads.AdContextData
            r0.<init>(r7)
        L_0x05fd:
            r3.A0S = r0
            int r0 = r15.A0D
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x06cb
            X.1ex r8 = r15.A0k
            int r0 = r15.A0D
            java.lang.String r7 = r2.getString(r0)
            boolean r0 = X.C06850cB.A0B(r7)
            if (r0 != 0) goto L_0x070e
            java.lang.String r0 = "{}"
            boolean r0 = r7.equals(r0)
            if (r0 != 0) goto L_0x070e
            int r4 = X.AnonymousClass1Y3.A1l
            X.0UN r0 = r8.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r4, r0)
            X.0jD r0 = (X.AnonymousClass0jD) r0
            com.fasterxml.jackson.databind.JsonNode r4 = r0.A02(r7)
            java.lang.String r0 = "is_eligible"
            com.fasterxml.jackson.databind.JsonNode r13 = r4.get(r0)
            r0 = 10
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r12 = r4.get(r0)
            r0 = 9
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r7 = r4.get(r0)
            r0 = 53
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r6 = r4.get(r0)
            java.lang.String r0 = "timestamp"
            com.fasterxml.jackson.databind.JsonNode r11 = r4.get(r0)
            r0 = 199(0xc7, float:2.79E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r10 = r4.get(r0)
            r0 = 180(0xb4, float:2.52E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r9 = r4.get(r0)
            r0 = 181(0xb5, float:2.54E-43)
            java.lang.String r0 = X.C99084oO.$const$string(r0)
            com.fasterxml.jackson.databind.JsonNode r8 = r4.get(r0)
            X.1gc r4 = new X.1gc
            r4.<init>()
            if (r6 == 0) goto L_0x0685
            java.lang.String r6 = r6.asText()
            r4.A02 = r6
            java.lang.String r0 = "conversionType"
            X.C28931fb.A06(r6, r0)
        L_0x0685:
            if (r12 == 0) goto L_0x0692
            java.lang.String r6 = r12.asText()
            r4.A03 = r6
            java.lang.String r0 = "currencyCode"
            X.C28931fb.A06(r6, r0)
        L_0x0692:
            if (r7 == 0) goto L_0x069a
            double r6 = r7.doubleValue()
            r4.A00 = r6
        L_0x069a:
            if (r13 == 0) goto L_0x06a2
            boolean r0 = r13.booleanValue()
            r4.A07 = r0
        L_0x06a2:
            if (r11 == 0) goto L_0x06aa
            long r6 = r11.asLong()
            r4.A01 = r6
        L_0x06aa:
            if (r10 == 0) goto L_0x06b2
            java.lang.String r0 = r10.textValue()
            r4.A06 = r0
        L_0x06b2:
            if (r9 == 0) goto L_0x06ba
            java.lang.String r0 = r9.textValue()
            r4.A04 = r0
        L_0x06ba:
            if (r8 == 0) goto L_0x06c2
            java.lang.String r0 = r8.textValue()
            r4.A05 = r0
        L_0x06c2:
            com.facebook.messaging.model.threads.AdsConversionsQPData r0 = new com.facebook.messaging.model.threads.AdsConversionsQPData
            r0.<init>(r4)
        L_0x06c7:
            if (r0 == 0) goto L_0x06cb
            r3.A0T = r0
        L_0x06cb:
            int r0 = r15.A0h
            int r4 = r2.getInt(r0)
            r0 = 0
            if (r4 == 0) goto L_0x06d5
            r0 = 1
        L_0x06d5:
            r3.A17 = r0
            int r0 = r15.A0H
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x06ec
            X.1ez r7 = r15.A0s
            int r0 = r15.A0H
            java.lang.String r6 = r2.getString(r0)
            if (r6 != 0) goto L_0x06f5
            r0 = r5
        L_0x06ea:
            r3.A0M = r0
        L_0x06ec:
            int r0 = r15.A0E
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x073d
            goto L_0x0722
        L_0x06f5:
            X.0jE r4 = r7.A01     // Catch:{ IOException -> 0x0703 }
            X.1gd r0 = new X.1gd     // Catch:{ IOException -> 0x0703 }
            r0.<init>(r7)     // Catch:{ IOException -> 0x0703 }
            java.lang.Object r0 = r4.readValue(r6, r0)     // Catch:{ IOException -> 0x0703 }
            com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings r0 = (com.facebook.messaging.games.pushnotification.model.GamesPushNotificationSettings) r0     // Catch:{ IOException -> 0x0703 }
            goto L_0x06ea
        L_0x0703:
            X.09P r6 = r7.A00
            java.lang.String r4 = "GamesPushNotificationSettingSerializer"
            java.lang.String r0 = "Failed to deserialize InstantGameChannel"
            r6.CGS(r4, r0)
            r0 = r5
            goto L_0x06ea
        L_0x070e:
            r0 = r5
            goto L_0x06c7
        L_0x0710:
            r0 = r5
            goto L_0x05e2
        L_0x0713:
            android.net.Uri r0 = android.net.Uri.parse(r4)
            goto L_0x05da
        L_0x0719:
            r4 = r5
            goto L_0x05d3
        L_0x071c:
            r0 = r5
            goto L_0x05cb
        L_0x071f:
            r0 = r5
            goto L_0x05fd
        L_0x0722:
            X.0jE r6 = r15.A0i     // Catch:{ Exception -> 0x0735 }
            int r0 = r15.A0E     // Catch:{ Exception -> 0x0735 }
            java.lang.String r4 = r2.getString(r0)     // Catch:{ Exception -> 0x0735 }
            java.lang.Class<com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel> r0 = com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel.class
            java.lang.Object r0 = r6.readValue(r4, r0)     // Catch:{ Exception -> 0x0735 }
            com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel r0 = (com.facebook.messaging.threadview.notificationbanner.model.animated.AnimatedThreadActivityBannerDataModel) r0     // Catch:{ Exception -> 0x0735 }
            r3.A0i = r0     // Catch:{ Exception -> 0x0735 }
            goto L_0x073d
        L_0x0735:
            r6 = move-exception
            java.lang.String r4 = "InboxIterationResultBuilder"
            java.lang.String r0 = "Failed to deserialize Animated Thread Banner"
            X.C010708t.A0R(r4, r6, r0)
        L_0x073d:
            int r0 = r15.A0Q
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x0755
            int r0 = r15.A0Q
            java.lang.String r4 = r2.getString(r0)
            com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType r0 = com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r4, r0)
            com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType r0 = (com.facebook.graphql.enums.GraphQLMessengerXMAGroupingType) r0
            r3.A0I = r0
        L_0x0755:
            int r0 = r15.A0P
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x0777
            int r0 = r15.A0P
            java.lang.String r0 = r2.getString(r0)
            com.google.common.collect.ImmutableList r4 = X.C29941hE.A02(r0)
            if (r4 == 0) goto L_0x0777
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x0777
            java.lang.Object r0 = r4.get(r1)
            com.facebook.messaging.business.common.calltoaction.model.CallToAction r0 = (com.facebook.messaging.business.common.calltoaction.model.CallToAction) r0
            r3.A0J = r0
        L_0x0777:
            int r0 = r15.A0J
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x078f
            int r0 = r15.A0J
            java.lang.String r4 = r2.getString(r0)
            com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType r0 = com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType.A05
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r4, r0)
            com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType r0 = (com.facebook.graphql.enums.GraphQLMessengerGroupThreadSubType) r0
            r3.A0H = r0
        L_0x078f:
            int r0 = r15.A0L
            int r4 = r2.getInt(r0)
            r0 = 0
            if (r4 == 0) goto L_0x0799
            r0 = 1
        L_0x0799:
            r3.A14 = r0
            int r0 = r15.A0S
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x07a9
            int r0 = r15.A0S
            java.lang.String r5 = r2.getString(r0)
        L_0x07a9:
            r3.A0n = r5
            int r0 = r15.A0X
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x07ef
            X.1eu r5 = r15.A0m
            int r0 = r15.A0X
            java.lang.String r4 = r2.getString(r0)
            if (r4 == 0) goto L_0x0807
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x0807
            X.0jD r0 = r5.A00
            com.fasterxml.jackson.databind.JsonNode r5 = r0.A02(r4)
            X.1Lv r4 = new X.1Lv
            r4.<init>()
            java.lang.String r0 = "is_eligible"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r4.A00 = r0
            java.lang.String r0 = "should_show_nux"
            com.fasterxml.jackson.databind.JsonNode r0 = r5.get(r0)
            boolean r0 = com.facebook.common.util.JSONUtil.A0S(r0)
            r4.A01 = r0
            com.facebook.messaging.model.threads.RequestAppointmentData r0 = new com.facebook.messaging.model.threads.RequestAppointmentData
            r0.<init>(r4)
        L_0x07ed:
            r3.A0a = r0
        L_0x07ef:
            int r0 = r15.A0e
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x0900
            X.1f0 r8 = r15.A0n
            int r0 = r15.A0e
            java.lang.String r6 = r2.getString(r0)
            boolean r0 = X.C06850cB.A0B(r6)
            r7 = 0
            if (r0 != 0) goto L_0x08fe
            goto L_0x0809
        L_0x0807:
            r0 = 0
            goto L_0x07ed
        L_0x0809:
            int r4 = X.AnonymousClass1Y3.A1l     // Catch:{ Exception -> 0x08e9 }
            X.0UN r0 = r8.A00     // Catch:{ Exception -> 0x08e9 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r4, r0)     // Catch:{ Exception -> 0x08e9 }
            X.0jD r0 = (X.AnonymousClass0jD) r0     // Catch:{ Exception -> 0x08e9 }
            com.fasterxml.jackson.databind.JsonNode r6 = r0.A02(r6)     // Catch:{ Exception -> 0x08e9 }
            java.lang.String r5 = "page_message_customer_tags"
            if (r6 == 0) goto L_0x0851
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r5)     // Catch:{ Exception -> 0x08e9 }
            if (r0 == 0) goto L_0x0851
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r5)     // Catch:{ Exception -> 0x08e9 }
            X.0oL r4 = r0.getNodeType()     // Catch:{ Exception -> 0x08e9 }
            X.0oL r0 = X.C11980oL.NULL     // Catch:{ Exception -> 0x08e9 }
            if (r4 == r0) goto L_0x0851
            com.fasterxml.jackson.databind.JsonNode r0 = r6.get(r5)     // Catch:{ Exception -> 0x08e9 }
            java.lang.String r10 = r0.asText()     // Catch:{ Exception -> 0x08e9 }
        L_0x0835:
            X.37I r6 = new X.37I     // Catch:{ Exception -> 0x08e9 }
            r6.<init>()     // Catch:{ Exception -> 0x08e9 }
            r5 = 1
            int r4 = X.AnonymousClass1Y3.BMl     // Catch:{ Exception -> 0x08e9 }
            X.0UN r0 = r8.A00     // Catch:{ Exception -> 0x08e9 }
            java.lang.Object r11 = X.AnonymousClass1XX.A02(r5, r4, r0)     // Catch:{ Exception -> 0x08e9 }
            X.37J r11 = (X.AnonymousClass37J) r11     // Catch:{ Exception -> 0x08e9 }
            com.google.common.collect.ImmutableList$Builder r9 = new com.google.common.collect.ImmutableList$Builder     // Catch:{ Exception -> 0x08e9 }
            r9.<init>()     // Catch:{ Exception -> 0x08e9 }
            boolean r0 = X.C06850cB.A0B(r10)     // Catch:{ Exception -> 0x08e9 }
            if (r0 != 0) goto L_0x08d3
            goto L_0x0853
        L_0x0851:
            r10 = 0
            goto L_0x0835
        L_0x0853:
            int r4 = X.AnonymousClass1Y3.A1l     // Catch:{ Exception -> 0x08b1 }
            X.0UN r0 = r11.A00     // Catch:{ Exception -> 0x08b1 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r4, r0)     // Catch:{ Exception -> 0x08b1 }
            X.0jD r0 = (X.AnonymousClass0jD) r0     // Catch:{ Exception -> 0x08b1 }
            com.fasterxml.jackson.databind.JsonNode r0 = r0.A02(r10)     // Catch:{ Exception -> 0x08b1 }
            java.util.Iterator r13 = r0.iterator()     // Catch:{ Exception -> 0x08b1 }
        L_0x0865:
            boolean r0 = r13.hasNext()     // Catch:{ Exception -> 0x08b1 }
            if (r0 == 0) goto L_0x08d3
            java.lang.Object r5 = r13.next()     // Catch:{ Exception -> 0x08b1 }
            com.fasterxml.jackson.databind.JsonNode r5 = (com.fasterxml.jackson.databind.JsonNode) r5     // Catch:{ Exception -> 0x08b1 }
            r0 = 0
            if (r5 == 0) goto L_0x08ab
            java.lang.String r4 = "key_assigned_customer_tag_id"
            java.lang.String r12 = A00(r5, r4)     // Catch:{ Exception -> 0x08b1 }
            java.lang.String r4 = "key_assigned_customer_tag_name"
            java.lang.String r10 = A00(r5, r4)     // Catch:{ Exception -> 0x08b1 }
            java.lang.String r4 = "key_assigned_customer_tag_color"
            java.lang.String r5 = A00(r5, r4)     // Catch:{ Exception -> 0x08b1 }
            if (r12 == 0) goto L_0x08ab
            if (r10 == 0) goto L_0x08ab
            if (r5 == 0) goto L_0x08ab
            X.37K r4 = new X.37K     // Catch:{ Exception -> 0x08b1 }
            r4.<init>()     // Catch:{ Exception -> 0x08b1 }
            r4.A01 = r12     // Catch:{ Exception -> 0x08b1 }
            java.lang.String r0 = "customerTagId"
            X.C28931fb.A06(r12, r0)     // Catch:{ Exception -> 0x08b1 }
            r4.A02 = r10     // Catch:{ Exception -> 0x08b1 }
            java.lang.String r0 = "customerTagName"
            X.C28931fb.A06(r10, r0)     // Catch:{ Exception -> 0x08b1 }
            r4.A00 = r5     // Catch:{ Exception -> 0x08b1 }
            java.lang.String r0 = "customerTagColor"
            X.C28931fb.A06(r5, r0)     // Catch:{ Exception -> 0x08b1 }
            com.facebook.messaging.model.threads.ThreadPageMessageCustomerTag r0 = new com.facebook.messaging.model.threads.ThreadPageMessageCustomerTag     // Catch:{ Exception -> 0x08b1 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x08b1 }
        L_0x08ab:
            if (r0 == 0) goto L_0x0865
            r9.add(r0)     // Catch:{ Exception -> 0x08b1 }
            goto L_0x0865
        L_0x08b1:
            r10 = move-exception
            r5 = 1
            int r4 = X.AnonymousClass1Y3.Amr     // Catch:{ Exception -> 0x08e9 }
            X.0UN r0 = r11.A00     // Catch:{ Exception -> 0x08e9 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r4, r0)     // Catch:{ Exception -> 0x08e9 }
            X.09P r5 = (X.AnonymousClass09P) r5     // Catch:{ Exception -> 0x08e9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x08e9 }
            r4.<init>()     // Catch:{ Exception -> 0x08e9 }
            java.lang.String r0 = "Failed to deserialize ThreadPageMessageCustomerTag list with exception: "
            r4.append(r0)     // Catch:{ Exception -> 0x08e9 }
            r4.append(r10)     // Catch:{ Exception -> 0x08e9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x08e9 }
            java.lang.String r0 = "DbThreadPageMessageCustomerTagsSerialization"
            r5.CGS(r0, r4)     // Catch:{ Exception -> 0x08e9 }
        L_0x08d3:
            com.google.common.collect.ImmutableList r0 = r9.build()     // Catch:{ Exception -> 0x08e9 }
            r6.A00 = r0     // Catch:{ Exception -> 0x08e9 }
            java.lang.String r4 = "threadPageMessageCustomerTags"
            X.C28931fb.A06(r0, r4)     // Catch:{ Exception -> 0x08e9 }
            java.util.Set r0 = r6.A01     // Catch:{ Exception -> 0x08e9 }
            r0.add(r4)     // Catch:{ Exception -> 0x08e9 }
            com.facebook.messaging.model.threads.RelatedPageThreadData r0 = new com.facebook.messaging.model.threads.RelatedPageThreadData     // Catch:{ Exception -> 0x08e9 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x08e9 }
            goto L_0x08fd
        L_0x08e9:
            r6 = move-exception
            r5 = 2
            int r4 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r8.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.09P r5 = (X.AnonymousClass09P) r5
            java.lang.String r4 = "DbRelatedPageThreadDataSerialization"
            java.lang.String r0 = "unable to deserialize RelatedPageThreadData"
            r5.softReport(r4, r0, r6)
            goto L_0x08fe
        L_0x08fd:
            r7 = r0
        L_0x08fe:
            r3.A0Z = r7
        L_0x0900:
            int r0 = r15.A0g
            boolean r0 = r2.isNull(r0)
            if (r0 != 0) goto L_0x0960
            X.1ey r8 = r15.A0q
            int r0 = r15.A0g
            java.lang.String r6 = r2.getString(r0)
            boolean r0 = X.C06850cB.A0B(r6)
            r7 = 0
            if (r0 != 0) goto L_0x095e
            int r4 = X.AnonymousClass1Y3.A1l     // Catch:{ Exception -> 0x0949 }
            X.0UN r0 = r8.A00     // Catch:{ Exception -> 0x0949 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r1, r4, r0)     // Catch:{ Exception -> 0x0949 }
            X.0jD r0 = (X.AnonymousClass0jD) r0     // Catch:{ Exception -> 0x0949 }
            com.fasterxml.jackson.databind.JsonNode r6 = r0.A02(r6)     // Catch:{ Exception -> 0x0949 }
            X.37N r5 = new X.37N     // Catch:{ Exception -> 0x0949 }
            r5.<init>()     // Catch:{ Exception -> 0x0949 }
            java.lang.String r0 = "key_assigned_admin_id"
            java.lang.String r4 = X.C28541ey.A01(r6, r0)     // Catch:{ Exception -> 0x0949 }
            r5.A00 = r4     // Catch:{ Exception -> 0x0949 }
            r0 = 29
            java.lang.String r0 = X.C22298Ase.$const$string(r0)     // Catch:{ Exception -> 0x0949 }
            X.C28931fb.A06(r4, r0)     // Catch:{ Exception -> 0x0949 }
            java.lang.String r0 = "key_assigned_admin_picture_uri"
            java.lang.String r0 = X.C28541ey.A01(r6, r0)     // Catch:{ Exception -> 0x0949 }
            r5.A01 = r0     // Catch:{ Exception -> 0x0949 }
            com.facebook.messaging.model.threads.ThreadPageMessageAssignedAdmin r0 = new com.facebook.messaging.model.threads.ThreadPageMessageAssignedAdmin     // Catch:{ Exception -> 0x0949 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x0949 }
            goto L_0x095d
        L_0x0949:
            r6 = move-exception
            r5 = 1
            int r4 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r8.A00
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.09P r5 = (X.AnonymousClass09P) r5
            java.lang.String r4 = "DbThreadPageMessageAssignedAdminSerialization"
            java.lang.String r0 = "unable to deserialize ThreadPageMessageAssignedAdmin"
            r5.softReport(r4, r0, r6)
            goto L_0x095e
        L_0x095d:
            r7 = r0
        L_0x095e:
            r3.A0f = r7
        L_0x0960:
            int r0 = r15.A0K
            int r0 = r2.getInt(r0)
            if (r0 == 0) goto L_0x0969
            r1 = 1
        L_0x0969:
            r3.A10 = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C12180oj.A01(X.0zh, X.0rr):void");
    }

    public C12180oj(AnonymousClass1XY r3, Cursor cursor) {
        this.A00 = new AnonymousClass0UN(2, r3);
        this.A0r = new C13710ru(r3);
        this.A0p = C28481es.A00(r3);
        this.A0o = new C28491et(r3);
        this.A0m = new C28501eu(r3);
        this.A0B = C25771aN.A02(r3);
        this.A0A = C12160of.A00(r3);
        this.A0l = new C28511ev(r3);
        this.A0j = new C28521ew(r3);
        this.A0k = new C28531ex(r3);
        this.A0q = new C28541ey(r3);
        this.A0s = new C28551ez(r3);
        this.A0n = new C28561f0(r3);
        this.A0Z = cursor.getColumnIndexOrThrow("name");
        this.A0d = cursor.getColumnIndexOrThrow("pic_hash");
        this.A0c = cursor.getColumnIndexOrThrow("pic");
        this.A0W = cursor.getColumnIndexOrThrow("media_preview");
        this.A0Y = cursor.getColumnIndexOrThrow("missed_call_status");
        this.A0G = cursor.getColumnIndexOrThrow("custom_like_emoji");
        this.A0O = cursor.getColumnIndexOrThrow("last_message_admin_text_type");
        this.A0b = cursor.getColumnIndexOrThrow("outgoing_message_lifetime");
        this.A02 = cursor.getColumnIndexOrThrow("custom_nicknames");
        this.A0T = cursor.getColumnIndexOrThrow("last_message_id_if_sponsored");
        this.A0I = cursor.getColumnIndexOrThrow("group_chat_rank");
        this.A0f = cursor.getColumnIndexOrThrow("rtc_call_info");
        this.A0R = cursor.getColumnIndexOrThrow("last_message_commerce_message_type");
        this.A0M = cursor.getColumnIndexOrThrow("is_thread_queue_enabled");
        this.A0F = cursor.getColumnIndexOrThrow("booking_requests");
        this.A0N = cursor.getColumnIndexOrThrow("last_call_ms");
        this.A0U = cursor.getColumnIndexOrThrow("last_sponsored_message_call_to_action");
        this.A0V = cursor.getColumnIndexOrThrow("marketplace_data");
        this.A09 = cursor.getColumnIndexOrThrow("montage_thread_key");
        this.A05 = cursor.getColumnIndex("inbox_to_montage_preview_message_id");
        this.A08 = cursor.getColumnIndex("inbox_to_montage_preview_text");
        this.A07 = cursor.getColumnIndex("inbox_to_montage_preview_sticker_id");
        this.A04 = cursor.getColumnIndex("inbox_to_montage_preview_attachments");
        cursor.getColumnIndex("inbox_to_montage_thread_read_watermark_timestamp_ms");
        this.A03 = cursor.getColumnIndex("inbox_to_montage_latest_message_timestamp_ms");
        this.A06 = cursor.getColumnIndex("inbox_to_montage_sender");
        this.A0a = cursor.getColumnIndexOrThrow("optimistic_group_state");
        this.A0C = cursor.getColumnIndexOrThrow("ad_context_data");
        this.A0D = cursor.getColumnIndexOrThrow("ads_qp_update_data");
        this.A0h = cursor.getColumnIndexOrThrow("use_existing_group");
        this.A0H = cursor.getColumnIndexOrThrow("games_push_notification_settings");
        this.A0E = cursor.getColumnIndexOrThrow("animated_thread_activity_banner");
        this.A0Q = cursor.getColumnIndexOrThrow("last_message_breadcrumb_type");
        this.A0P = cursor.getColumnIndexOrThrow("last_message_breadcrumb_cta");
        this.A0J = cursor.getColumnIndexOrThrow("group_thread_subtype");
        this.A0L = cursor.getColumnIndexOrThrow("is_page_follow_up");
        this.A0S = cursor.getColumnIndexOrThrow("last_message_id");
        this.A0X = cursor.getColumnIndexOrThrow("messenger_request_appointment_data");
        this.A0e = cursor.getColumnIndexOrThrow("related_page_thread_data");
        this.A0g = cursor.getColumnIndexOrThrow("thread_message_assigned_page_admin");
        this.A0K = cursor.getColumnIndexOrThrow("has_non_admin_message");
    }
}
