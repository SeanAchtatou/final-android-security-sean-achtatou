package X;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

/* renamed from: X.169  reason: invalid class name */
public abstract class AnonymousClass169 extends C06720by implements Runnable {
    public static final String __redex_internal_original_name = "com.google.common.util.concurrent.AbstractTransformFuture";
    public ListenableFuture A00;
    public Object A01;

    public Object A04(Object obj, Object obj2) {
        if (this instanceof AnonymousClass16A) {
            return ((Function) obj).apply(obj2);
        }
        ListenableFuture AOW = ((AnonymousClass0YC) obj).AOW(obj2);
        Preconditions.checkNotNull(AOW, "AsyncFunction.apply returned null instead of a Future. Did you mean to return immediateFuture(null)?");
        return AOW;
    }

    public void A05(Object obj) {
        if (!(this instanceof AnonymousClass16A)) {
            ((AnonymousClass16B) this).setFuture((ListenableFuture) obj);
        } else {
            ((AnonymousClass16A) this).set(obj);
        }
    }

    public static ListenableFuture A02(ListenableFuture listenableFuture, AnonymousClass0YC r3) {
        AnonymousClass16B r1 = new AnonymousClass16B(listenableFuture, r3);
        listenableFuture.addListener(r1, C25141Ym.INSTANCE);
        return r1;
    }

    public final void afterDone() {
        maybePropagateCancellation(this.A00);
        this.A00 = null;
        this.A01 = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002e, code lost:
        setException(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0031, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0032, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0033, code lost:
        r0 = r0.getCause();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003b, code lost:
        cancel(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003e, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002d A[ExcHandler: Error | RuntimeException (r0v6 'e' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:10:0x001b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            com.google.common.util.concurrent.ListenableFuture r5 = r6.A00
            java.lang.Object r4 = r6.A01
            boolean r3 = r6.isCancelled()
            r2 = 1
            r1 = 0
            r0 = 0
            if (r5 != 0) goto L_0x000e
            r0 = 1
        L_0x000e:
            r3 = r3 | r0
            if (r4 == 0) goto L_0x0012
            r2 = 0
        L_0x0012:
            r3 = r3 | r2
            if (r3 == 0) goto L_0x0016
            return
        L_0x0016:
            r0 = 0
            r6.A00 = r0
            r6.A01 = r0
            java.lang.Object r0 = X.C05350Yp.A06(r5)     // Catch:{ CancellationException -> 0x003b, ExecutionException -> 0x0032, Error | RuntimeException -> 0x002d }
            java.lang.Object r0 = r6.A04(r4, r0)     // Catch:{ UndeclaredThrowableException -> 0x0027, Error | RuntimeException -> 0x002d }
            r6.A05(r0)
            return
        L_0x0027:
            r0 = move-exception
            java.lang.Throwable r0 = r0.getCause()
            goto L_0x0037
        L_0x002d:
            r0 = move-exception
            r6.setException(r0)
            return
        L_0x0032:
            r0 = move-exception
            java.lang.Throwable r0 = r0.getCause()
        L_0x0037:
            r6.setException(r0)
            return
        L_0x003b:
            r6.cancel(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass169.run():void");
    }

    public AnonymousClass169(ListenableFuture listenableFuture, Object obj) {
        Preconditions.checkNotNull(listenableFuture);
        this.A00 = listenableFuture;
        Preconditions.checkNotNull(obj);
        this.A01 = obj;
    }

    public static ListenableFuture A00(ListenableFuture listenableFuture, Function function) {
        Preconditions.checkNotNull(function);
        AnonymousClass16A r1 = new AnonymousClass16A(listenableFuture, function);
        listenableFuture.addListener(r1, C25141Ym.INSTANCE);
        return r1;
    }

    public static ListenableFuture A01(ListenableFuture listenableFuture, Function function, Executor executor) {
        Preconditions.checkNotNull(function);
        AnonymousClass16A r1 = new AnonymousClass16A(listenableFuture, function);
        Preconditions.checkNotNull(executor);
        Preconditions.checkNotNull(r1);
        if (executor != C25141Ym.INSTANCE) {
            executor = new C42522Au(executor, r1);
        }
        listenableFuture.addListener(r1, executor);
        return r1;
    }

    public static ListenableFuture A03(ListenableFuture listenableFuture, AnonymousClass0YC r3, Executor executor) {
        Preconditions.checkNotNull(executor);
        AnonymousClass16B r1 = new AnonymousClass16B(listenableFuture, r3);
        Preconditions.checkNotNull(executor);
        Preconditions.checkNotNull(r1);
        if (executor != C25141Ym.INSTANCE) {
            executor = new C42522Au(executor, r1);
        }
        listenableFuture.addListener(r1, executor);
        return r1;
    }
}
