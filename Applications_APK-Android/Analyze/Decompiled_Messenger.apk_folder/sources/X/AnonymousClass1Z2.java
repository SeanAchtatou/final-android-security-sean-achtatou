package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Z2  reason: invalid class name */
public abstract class AnonymousClass1Z2 extends C25261Yy {
    public final C25361Zi A00;
    public final AnonymousClass069 A01;
    public final AtomicInteger A02 = new AtomicInteger();

    public AnonymousClass0WR A06() {
        int i;
        AnonymousClass0UN r1;
        if (!(this instanceof AnonymousClass1Z1)) {
            i = AnonymousClass1Y3.AON;
            r1 = ((AnonymousClass1Z3) this).A00;
        } else {
            i = AnonymousClass1Y3.AH8;
            r1 = ((AnonymousClass1Z1) this).A00;
        }
        return (AnonymousClass0WR) AnonymousClass1XX.A02(0, i, r1);
    }

    public void A07() {
        if (!(this instanceof AnonymousClass1Z1)) {
            AnonymousClass1Z3 r4 = (AnonymousClass1Z3) this;
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r4.A00)).markerEnd(3997719, 2);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, r4.A00)).markerEnd(3997702, 2);
            return;
        }
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, ((AnonymousClass1Z1) this).A00)).markerEnd(3997701, 2);
    }

    public void A08() {
        if (!(this instanceof AnonymousClass1Z1)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, ((AnonymousClass1Z3) this).A00)).markerStart(3997702);
        } else {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, ((AnonymousClass1Z1) this).A00)).markerStart(3997701);
        }
    }

    public AnonymousClass1Z2(AnonymousClass1YS r2, String str, long j, AnonymousClass1XV r6, AnonymousClass069 r7, C25361Zi r8) {
        super(r2, str, j, r6);
        this.A01 = r7;
        this.A00 = r8;
    }
}
