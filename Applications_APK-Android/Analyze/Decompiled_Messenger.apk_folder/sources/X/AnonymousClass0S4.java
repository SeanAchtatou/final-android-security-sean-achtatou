package X;

/* renamed from: X.0S4  reason: invalid class name */
public final class AnonymousClass0S4 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.protocol.MqttClient$10";
    public final /* synthetic */ AnonymousClass0C8 A00;

    public AnonymousClass0S4(AnonymousClass0C8 r1) {
        this.A00 = r1;
    }

    public void run() {
        try {
            AnonymousClass0C8 r1 = this.A00;
            if (!r1.A0J) {
                r1.A0C.C5N();
            }
        } catch (Throwable th) {
            AnonymousClass0C8.A03(this.A00, AnonymousClass0CE.A01(th), AnonymousClass0CG.A04, th);
        }
    }
}
