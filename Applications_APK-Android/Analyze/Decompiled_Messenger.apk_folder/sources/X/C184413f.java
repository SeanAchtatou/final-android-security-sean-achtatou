package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.AdContextData;

/* renamed from: X.13f  reason: invalid class name and case insensitive filesystem */
public final class C184413f implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new AdContextData(parcel);
    }

    public Object[] newArray(int i) {
        return new AdContextData[i];
    }
}
