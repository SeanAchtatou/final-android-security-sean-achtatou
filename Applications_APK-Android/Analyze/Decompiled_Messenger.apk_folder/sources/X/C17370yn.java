package X;

import androidx.transition.FragmentTransitionSupport;
import java.util.ArrayList;

/* renamed from: X.0yn  reason: invalid class name and case insensitive filesystem */
public final class C17370yn implements C17160yQ {
    public final /* synthetic */ FragmentTransitionSupport A00;
    public final /* synthetic */ Object A01;
    public final /* synthetic */ Object A02;
    public final /* synthetic */ Object A03;
    public final /* synthetic */ ArrayList A04;
    public final /* synthetic */ ArrayList A05;
    public final /* synthetic */ ArrayList A06;

    public void Bss(C14030sU r1) {
    }

    public void Bst(C14030sU r1) {
    }

    public void Bsv(C14030sU r1) {
    }

    public void Bsw(C14030sU r1) {
    }

    public C17370yn(FragmentTransitionSupport fragmentTransitionSupport, Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3) {
        this.A00 = fragmentTransitionSupport;
        this.A01 = obj;
        this.A04 = arrayList;
        this.A02 = obj2;
        this.A05 = arrayList2;
        this.A03 = obj3;
        this.A06 = arrayList3;
    }

    public void Bsx(C14030sU r5) {
        Object obj = this.A01;
        if (obj != null) {
            this.A00.A0H(obj, this.A04, null);
        }
        Object obj2 = this.A02;
        if (obj2 != null) {
            this.A00.A0H(obj2, this.A05, null);
        }
        Object obj3 = this.A03;
        if (obj3 != null) {
            this.A00.A0H(obj3, this.A06, null);
        }
    }
}
