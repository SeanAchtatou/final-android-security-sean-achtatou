package X;

/* renamed from: X.1e3  reason: invalid class name and case insensitive filesystem */
public abstract class C27971e3 extends C27981e4 implements C27961e2 {
    public abstract C27941e0 A00(String str, Class cls);

    public C27941e0 AUd(Class cls) {
        throw new UnsupportedOperationException("create(String, Class<?>) must be called on implementaions of KeyedFactory");
    }
}
