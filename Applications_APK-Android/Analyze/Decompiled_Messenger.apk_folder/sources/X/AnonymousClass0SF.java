package X;

import android.content.SharedPreferences;

/* renamed from: X.0SF  reason: invalid class name */
public final class AnonymousClass0SF implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.NotificationDeliveryStoreSharedPreferences$1";
    public final /* synthetic */ SharedPreferences.Editor A00;

    public AnonymousClass0SF(SharedPreferences.Editor editor) {
        this.A00 = editor;
    }

    public void run() {
        this.A00.commit();
    }
}
