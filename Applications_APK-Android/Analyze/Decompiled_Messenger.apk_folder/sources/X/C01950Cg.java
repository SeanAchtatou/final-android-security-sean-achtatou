package X;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;

/* renamed from: X.0Cg  reason: invalid class name and case insensitive filesystem */
public interface C01950Cg extends Future {
    void addListener(Runnable runnable, Executor executor);
}
