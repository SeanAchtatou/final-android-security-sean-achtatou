package X;

/* renamed from: X.1wj  reason: invalid class name and case insensitive filesystem */
public final class C38001wj implements Runnable {
    public static final String __redex_internal_original_name = "com.google.android.exoplayer2.source.MediaSourceEventListener$EventDispatcher$9";
    public final /* synthetic */ C21439AEg A00;
    public final /* synthetic */ C38031wm A01;
    public final /* synthetic */ C38211x4 A02;

    public C38001wj(C21439AEg aEg, C38211x4 r2, C38031wm r3) {
        this.A00 = aEg;
        this.A02 = r2;
        this.A01 = r3;
    }

    public void run() {
        C38211x4 r3 = this.A02;
        C21439AEg aEg = this.A00;
        r3.BWv(aEg.A00, aEg.A01, this.A01);
    }
}
