package X;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: X.0B2  reason: invalid class name */
public final class AnonymousClass0B2 {
    public static volatile boolean A00 = true;

    public static SharedPreferences A00(Context context, Integer num) {
        return AnonymousClass0B4.A00.A01(context, AnonymousClass08S.A0J("rti.mqtt.", C01450Ah.A00(num)), C01450Ah.A01(num));
    }
}
