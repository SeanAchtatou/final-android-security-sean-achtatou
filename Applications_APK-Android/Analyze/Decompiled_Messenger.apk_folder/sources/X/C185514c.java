package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.funnellogger.FunnelLoggerImpl;

/* renamed from: X.14c  reason: invalid class name and case insensitive filesystem */
public final class C185514c extends Handler {
    public final /* synthetic */ FunnelLoggerImpl A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C185514c(FunnelLoggerImpl funnelLoggerImpl, Looper looper) {
        super(looper);
        this.A00 = funnelLoggerImpl;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:452:0x0c91, code lost:
        if (r1.A00 == null) goto L_0x0c96;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01f5, code lost:
        if (r3.A03 < 100) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x025b, code lost:
        if (r4.equals(((X.C415125v) r2.get(r1.size() - 1)).A02) != false) goto L_0x09eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x02ab, code lost:
        if (r3.A0B != false) goto L_0x02ad;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r25) {
        /*
            r24 = this;
            r0 = r25
            int r2 = r0.what
            r4 = 3
            r13 = r24
            switch(r2) {
                case 1: goto L_0x0016;
                case 2: goto L_0x00ff;
                case 3: goto L_0x0153;
                case 4: goto L_0x02ce;
                case 5: goto L_0x0371;
                case 6: goto L_0x042a;
                case 7: goto L_0x087d;
                case 8: goto L_0x094e;
                case 9: goto L_0x09e0;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unknown what="
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r2)
            r1.<init>(r0)
            throw r1
        L_0x0016:
            java.lang.Object r3 = r0.obj
            X.25X r3 = (X.AnonymousClass25X) r3
            com.facebook.funnellogger.FunnelLoggerImpl r14 = r13.A00
            java.lang.String r15 = com.facebook.funnellogger.FunnelLoggerImpl.A04(r3)
            X.0gA r0 = r3.A00
            java.lang.Long r1 = r3.A04
            if (r1 != 0) goto L_0x008c
            int r2 = X.AnonymousClass1Y3.A8J
            X.0UN r1 = r14.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)
            java.util.Random r1 = (java.util.Random) r1
            long r17 = r1.nextLong()
        L_0x0034:
            java.lang.Long r1 = r3.A05
            long r19 = r1.longValue()
            java.util.List r2 = r3.A0A
            java.lang.Long r1 = r3.A06
            long r21 = r1.longValue()
            r12 = 0
            boolean r1 = r14.A03
            boolean r1 = X.C413425b.A00(r1, r0)
            if (r1 == 0) goto L_0x0091
            r23 = 0
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r14)
            java.util.Map r1 = r14.A02
            java.lang.Object r1 = r1.get(r15)
            X.5Jm r1 = (X.C109245Jm) r1
            if (r1 == 0) goto L_0x006b
            X.8cg r4 = X.C182058cg.A05
            r7 = 0
            r8 = 0
            r1 = r14
            r2 = r15
            r3 = r0
            r5 = r19
            com.facebook.funnellogger.FunnelLoggerImpl.A0A(r1, r2, r3, r4, r5, r7, r8)
            java.util.Map r1 = r14.A02
            r1.remove(r15)
        L_0x006b:
            r16 = r0
            X.5Jm r2 = com.facebook.funnellogger.FunnelLoggerImpl.A02(r14, r15, r16, r17, r19, r21, r23)
            if (r2 == 0) goto L_0x09eb
            java.util.Map r1 = r14.A02
            r1.put(r15, r2)
            boolean r1 = r2.A05
            if (r1 != 0) goto L_0x09eb
            r3 = 4
            int r2 = X.AnonymousClass1Y3.BR6
            X.0UN r1 = r14.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.261 r1 = (X.AnonymousClass261) r1
            r1.A05(r0)
            goto L_0x09eb
        L_0x008c:
            long r17 = r1.longValue()
            goto L_0x0034
        L_0x0091:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r14)
            java.util.Map r1 = r14.A01
            java.lang.Object r5 = r1.get(r15)
            X.25u r5 = (X.C415025u) r5
            if (r5 == 0) goto L_0x00ac
            X.8cg r6 = X.C182058cg.A05
            r3 = r14
            r4 = r15
            r7 = r19
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r3, r4, r5, r6, r7)
            java.util.Map r1 = r14.A01
            r1.remove(r15)
        L_0x00ac:
            r3 = r14
            r4 = r0
            r5 = r17
            r7 = r19
            r9 = r2
            r10 = r21
            X.25u r5 = com.facebook.funnellogger.FunnelLoggerImpl.A00(r3, r4, r5, r7, r9, r10, r12)
            if (r5 == 0) goto L_0x09eb
            java.util.Map r1 = r14.A01
            r1.put(r15, r5)
            boolean r1 = r5.A0B
            if (r1 != 0) goto L_0x00d2
            r3 = 4
            int r2 = X.AnonymousClass1Y3.BR6
            X.0UN r1 = r14.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            X.261 r1 = (X.AnonymousClass261) r1
            r1.A05(r0)
        L_0x00d2:
            r3 = 10
            int r2 = X.AnonymousClass1Y3.Awn
            X.0UN r1 = r14.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r2, r1)
            java.util.Set r1 = (java.util.Set) r1
            java.util.Iterator r4 = r1.iterator()
        L_0x00e2:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x09eb
            java.lang.Object r3 = r4.next()
            X.EvX r3 = (X.C30396EvX) r3
            boolean r1 = r5.A0B
            if (r1 == 0) goto L_0x00f9
            r1 = -1
        L_0x00f3:
            java.lang.String r2 = r0.A0D
            r3.onStartFunnel(r2, r1)
            goto L_0x00e2
        L_0x00f9:
            X.C415025u.A00(r5)
            int r1 = r5.A05
            goto L_0x00f3
        L_0x00ff:
            java.lang.Object r1 = r0.obj
            X.25X r1 = (X.AnonymousClass25X) r1
            com.facebook.funnellogger.FunnelLoggerImpl r2 = r13.A00
            java.lang.String r15 = com.facebook.funnellogger.FunnelLoggerImpl.A04(r1)
            java.lang.Long r0 = r1.A05
            long r6 = r0.longValue()
            X.0gA r4 = r1.A00
            boolean r0 = r2.A03
            boolean r0 = X.C413425b.A00(r0, r4)
            if (r0 == 0) goto L_0x0135
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r2)
            java.util.Map r0 = r2.A02
            java.lang.Object r0 = r0.get(r15)
            X.5Jm r0 = (X.C109245Jm) r0
            if (r0 == 0) goto L_0x09eb
            X.8cg r5 = X.C182058cg.A02
            r8 = 0
            r9 = 0
            r3 = r15
            com.facebook.funnellogger.FunnelLoggerImpl.A0A(r2, r3, r4, r5, r6, r8, r9)
            java.util.Map r0 = r2.A02
            r0.remove(r15)
            goto L_0x09eb
        L_0x0135:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r2)
            java.util.Map r0 = r2.A01
            java.lang.Object r1 = r0.get(r15)
            X.25u r1 = (X.C415025u) r1
            if (r1 == 0) goto L_0x09eb
            java.util.Map r0 = r2.A01
            r0.remove(r15)
            X.8cg r17 = X.C182058cg.A02
            r14 = r2
            r18 = r6
            r16 = r1
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r14, r15, r16, r17, r18)
            goto L_0x09eb
        L_0x0153:
            java.lang.Object r3 = r0.obj
            X.25X r3 = (X.AnonymousClass25X) r3
            com.facebook.funnellogger.FunnelLoggerImpl r0 = r13.A00
            java.lang.String r10 = com.facebook.funnellogger.FunnelLoggerImpl.A04(r3)
            X.0gA r12 = r3.A00
            java.lang.String r4 = r3.A07
            java.lang.Boolean r1 = r3.A03
            boolean r2 = r1.booleanValue()
            java.lang.String r9 = r3.A08
            X.26K r11 = r3.A01
            java.lang.Long r1 = r3.A05
            long r7 = r1.longValue()
            boolean r1 = r0.A03
            boolean r1 = X.C413425b.A00(r1, r12)
            if (r1 == 0) goto L_0x0228
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r0)
            java.util.Map r1 = r0.A02
            java.lang.Object r3 = r1.get(r10)
            X.5Jm r3 = (X.C109245Jm) r3
            if (r3 == 0) goto L_0x09eb
            boolean r1 = r3.A05
            if (r1 != 0) goto L_0x09eb
            if (r2 == 0) goto L_0x0198
            java.lang.String r1 = r3.A02
            if (r1 == 0) goto L_0x0198
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x0198
            goto L_0x09eb
        L_0x0198:
            long r5 = r3.A01
            r14 = 13
            int r2 = X.AnonymousClass1Y3.AcI
            X.0UN r1 = r0.A00
            java.lang.Object r14 = X.AnonymousClass1XX.A02(r14, r2, r1)
            X.9Ja r14 = (X.C195929Ja) r14
            long r1 = r7 - r5
            int r5 = (int) r1
            if (r10 == 0) goto L_0x099f
            long r1 = (long) r5
            com.fasterxml.jackson.databind.node.ObjectNode r5 = new com.fasterxml.jackson.databind.node.ObjectNode     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            com.fasterxml.jackson.databind.node.JsonNodeFactory r6 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            r5.<init>(r6)     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            java.lang.String r6 = "name"
            r5.put(r6, r4)     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            java.lang.String r6 = "relative_time"
            r5.put(r6, r1)     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            if (r9 == 0) goto L_0x01c4
            java.lang.String r1 = "tag"
            r5.put(r1, r9)     // Catch:{ ConcurrentModificationException -> 0x01e0 }
        L_0x01c4:
            if (r11 == 0) goto L_0x01cd
            com.fasterxml.jackson.databind.node.ObjectNode r2 = r11.A00     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            java.lang.String r1 = "payload"
            r5.put(r1, r2)     // Catch:{ ConcurrentModificationException -> 0x01e0 }
        L_0x01cd:
            java.lang.String r22 = r5.toString()     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            r16 = 0
            r17 = 0
            r18 = 0
            r21 = 2
            r15 = r10
            r19 = r7
            X.C195929Ja.A01(r14, r15, r16, r17, r18, r19, r21, r22)     // Catch:{ ConcurrentModificationException -> 0x01e0 }
            goto L_0x01ec
        L_0x01e0:
            r6 = move-exception
            java.lang.String r5 = "FunnelLoggerDbManager"
            java.lang.Object[] r2 = new java.lang.Object[]{r10}
            java.lang.String r1 = "Failed to save action for funnel %s. Action payload is possibly being mutated."
            X.C010708t.A0W(r5, r6, r1, r2)
        L_0x01ec:
            boolean r1 = r3.A05
            if (r1 != 0) goto L_0x01f7
            short r5 = r3.A03
            r2 = 100
            r1 = 0
            if (r5 >= r2) goto L_0x01f8
        L_0x01f7:
            r1 = 1
        L_0x01f8:
            if (r1 != 0) goto L_0x021b
            X.8cg r17 = X.C182058cg.A01
            r5 = 2
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)
            X.06B r1 = (X.AnonymousClass06B) r1
            long r18 = r1.now()
            r20 = 0
            r21 = 0
            r15 = r10
            r14 = r0
            r16 = r12
            com.facebook.funnellogger.FunnelLoggerImpl.A0A(r14, r15, r16, r17, r18, r20, r21)
            java.util.Map r0 = r0.A02
            r0.remove(r10)
        L_0x021b:
            r3.A00 = r7
            r3.A02 = r4
            short r0 = r3.A03
            int r0 = r0 + 1
            short r0 = (short) r0
            r3.A03 = r0
            goto L_0x09eb
        L_0x0228:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r0)
            java.util.Map r1 = r0.A01
            java.lang.Object r3 = r1.get(r10)
            X.25u r3 = (X.C415025u) r3
            if (r3 == 0) goto L_0x09eb
            boolean r1 = r3.A0B
            if (r1 != 0) goto L_0x09eb
            if (r2 == 0) goto L_0x025f
            X.C415025u.A00(r3)
            java.util.List r1 = r3.A03
            r2 = r1
            if (r1 == 0) goto L_0x025f
            X.C415025u.A00(r3)
            X.C415025u.A00(r3)
            int r1 = r1.size()
            int r1 = r1 + -1
            java.lang.Object r1 = r2.get(r1)
            X.25v r1 = (X.C415125v) r1
            java.lang.String r1 = r1.A02
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x025f
            goto L_0x09eb
        L_0x025f:
            X.25v r14 = new X.25v
            long r5 = r3.A06
            long r1 = r7 - r5
            int r5 = (int) r1
            r14.<init>(r4, r5, r9, r11)
            r3.A01(r14, r7)
            com.facebook.funnellogger.FunnelLoggerImpl.A0B(r0, r10, r12, r14)
            r5 = 10
            int r2 = X.AnonymousClass1Y3.Awn
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r5, r2, r1)
            java.util.Set r1 = (java.util.Set) r1
            java.util.Iterator r6 = r1.iterator()
        L_0x027f:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L_0x029a
            java.lang.Object r5 = r6.next()
            X.EvX r5 = (X.C30396EvX) r5
            if (r11 != 0) goto L_0x0295
            java.lang.String r2 = ""
        L_0x028f:
            java.lang.String r1 = r12.A0D
            r5.onAppendAction(r1, r4, r9, r2)
            goto L_0x027f
        L_0x0295:
            java.lang.String r2 = r11.toString()
            goto L_0x028f
        L_0x029a:
            java.util.List r1 = r3.A03
            if (r1 == 0) goto L_0x02ad
            int r2 = r1.size()
            X.0gA r1 = r3.A09
            int r1 = r1.A01
            if (r2 < r1) goto L_0x02ad
            boolean r2 = r3.A0B
            r1 = 0
            if (r2 == 0) goto L_0x02ae
        L_0x02ad:
            r1 = 1
        L_0x02ae:
            if (r1 != 0) goto L_0x09eb
            X.8cg r7 = X.C182058cg.A01
            r4 = 2
            int r2 = X.AnonymousClass1Y3.AgK
            X.0UN r1 = r0.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r4, r2, r1)
            X.06B r1 = (X.AnonymousClass06B) r1
            long r8 = r1.now()
            r5 = r10
            r4 = r0
            r6 = r3
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r4, r5, r6, r7, r8)
            java.util.Map r0 = r0.A01
            r0.remove(r10)
            goto L_0x09eb
        L_0x02ce:
            java.lang.Object r0 = r0.obj
            X.25X r0 = (X.AnonymousClass25X) r0
            com.facebook.funnellogger.FunnelLoggerImpl r6 = r13.A00
            java.lang.String r15 = com.facebook.funnellogger.FunnelLoggerImpl.A04(r0)
            X.0gA r4 = r0.A00
            java.lang.String r3 = r0.A09
            java.lang.Long r0 = r0.A05
            long r0 = r0.longValue()
            boolean r2 = r6.A03
            boolean r2 = X.C413425b.A00(r2, r4)
            if (r2 == 0) goto L_0x0338
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r6)
            java.util.Map r2 = r6.A02
            java.lang.Object r2 = r2.get(r15)
            X.5Jm r2 = (X.C109245Jm) r2
            if (r2 == 0) goto L_0x09eb
            boolean r2 = r2.A05
            if (r2 != 0) goto L_0x09eb
            if (r3 == 0) goto L_0x09eb
            java.util.Map r2 = r6.A02
            java.lang.Object r2 = r2.get(r15)
            if (r2 == 0) goto L_0x030f
            java.util.Map r2 = r6.A02
            java.lang.Object r2 = r2.get(r15)
            X.5Jm r2 = (X.C109245Jm) r2
            r2.A00 = r0
        L_0x030f:
            java.util.Map r2 = r6.A02
            java.lang.Object r2 = r2.get(r15)
            X.5Jm r2 = (X.C109245Jm) r2
            r2.A00 = r0
            r5 = 13
            int r4 = X.AnonymousClass1Y3.AcI
            X.0UN r2 = r6.A00
            java.lang.Object r14 = X.AnonymousClass1XX.A02(r5, r4, r2)
            X.9Ja r14 = (X.C195929Ja) r14
            if (r15 == 0) goto L_0x099f
            r16 = 0
            r17 = 0
            r18 = 0
            r21 = 3
            r19 = r0
            r22 = r3
            X.C195929Ja.A01(r14, r15, r16, r17, r18, r19, r21, r22)
            goto L_0x09eb
        L_0x0338:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r6)
            java.util.Map r2 = r6.A01
            java.lang.Object r5 = r2.get(r15)
            X.25u r5 = (X.C415025u) r5
            if (r5 == 0) goto L_0x09eb
            boolean r2 = r5.A0B
            if (r2 != 0) goto L_0x09eb
            boolean r0 = r5.A02(r3, r0)
            if (r0 == 0) goto L_0x09eb
            r2 = 10
            int r1 = X.AnonymousClass1Y3.Awn
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.util.Set r0 = (java.util.Set) r0
            java.util.Iterator r2 = r0.iterator()
        L_0x035f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x09eb
            java.lang.Object r1 = r2.next()
            X.EvX r1 = (X.C30396EvX) r1
            java.lang.String r0 = r4.A0D
            r1.onAddTag(r0, r3)
            goto L_0x035f
        L_0x0371:
            java.lang.Object r3 = r0.obj
            X.25X r3 = (X.AnonymousClass25X) r3
            com.facebook.funnellogger.FunnelLoggerImpl r6 = r13.A00
            java.lang.String r7 = com.facebook.funnellogger.FunnelLoggerImpl.A04(r3)
            java.lang.Long r0 = r3.A05
            long r1 = r0.longValue()
            X.0gA r3 = r3.A00
            boolean r0 = r6.A03
            boolean r0 = X.C413425b.A00(r0, r3)
            if (r0 == 0) goto L_0x03c2
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r6)
            java.util.Map r0 = r6.A02
            java.lang.Object r3 = r0.get(r7)
            X.5Jm r3 = (X.C109245Jm) r3
            if (r3 == 0) goto L_0x03ac
            boolean r0 = r3.A05
            if (r0 != 0) goto L_0x03ac
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r6.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.261 r1 = (X.AnonymousClass261) r1
            X.0gA r0 = r3.A04
            r1.A03(r0)
        L_0x03ac:
            r2 = 13
            int r1 = X.AnonymousClass1Y3.AcI
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.9Ja r0 = (X.C195929Ja) r0
            r0.A02(r7)
            java.util.Map r0 = r6.A02
            r0.remove(r7)
            goto L_0x09eb
        L_0x03c2:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r6)
            java.util.Map r0 = r6.A01
            java.lang.Object r3 = r0.get(r7)
            X.25u r3 = (X.C415025u) r3
            if (r3 == 0) goto L_0x03e3
            boolean r0 = r3.A0B
            if (r0 != 0) goto L_0x03e3
            r5 = 4
            int r4 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r6.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r5, r4, r0)
            X.261 r4 = (X.AnonymousClass261) r4
            X.0gA r0 = r3.A09
            r4.A03(r0)
        L_0x03e3:
            java.util.Map r0 = r6.A01
            r0.remove(r7)
            r5 = 6
            int r4 = X.AnonymousClass1Y3.Aaz     // Catch:{ IOException -> 0x0420 }
            X.0UN r0 = r6.A00     // Catch:{ IOException -> 0x0420 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r5, r4, r0)     // Catch:{ IOException -> 0x0420 }
            X.25c r0 = (X.C413525c) r0     // Catch:{ IOException -> 0x0420 }
            X.25f r0 = r0.A01()     // Catch:{ IOException -> 0x0420 }
            r0.Bzw(r7, r1)     // Catch:{ IOException -> 0x0420 }
            if (r3 == 0) goto L_0x09eb
            r2 = 10
            int r1 = X.AnonymousClass1Y3.Awn     // Catch:{ IOException -> 0x0420 }
            X.0UN r0 = r6.A00     // Catch:{ IOException -> 0x0420 }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IOException -> 0x0420 }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ IOException -> 0x0420 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ IOException -> 0x0420 }
        L_0x040c:
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x0420 }
            if (r0 == 0) goto L_0x09eb
            java.lang.Object r1 = r2.next()     // Catch:{ IOException -> 0x0420 }
            X.EvX r1 = (X.C30396EvX) r1     // Catch:{ IOException -> 0x0420 }
            X.0gA r0 = r3.A09     // Catch:{ IOException -> 0x0420 }
            java.lang.String r0 = r0.A0D     // Catch:{ IOException -> 0x0420 }
            r1.onCancel(r0)     // Catch:{ IOException -> 0x0420 }
            goto L_0x040c
        L_0x0420:
            r2 = move-exception
            java.lang.String r1 = "FunnelLoggerImpl"
            java.lang.String r0 = "Failed to write cancel funnel operation to changelog"
            X.C010708t.A0N(r1, r0, r2)
            goto L_0x09eb
        L_0x042a:
            com.facebook.funnellogger.FunnelLoggerImpl r15 = r13.A00
            boolean r0 = r15.A03
            if (r0 == 0) goto L_0x0436
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r15)
            com.facebook.funnellogger.FunnelLoggerImpl.A07(r15)
        L_0x0436:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r15)
            java.lang.String r8 = "FunnelLoggerImpl"
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Map r0 = r15.A01
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r14 = r0.iterator()
        L_0x044a:
            boolean r0 = r14.hasNext()
            if (r0 == 0) goto L_0x05cb
            java.lang.Object r1 = r14.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r4 = r1.getValue()
            X.25u r4 = (X.C415025u) r4
            java.lang.Object r0 = r1.getKey()
            java.lang.String r0 = (java.lang.String) r0
            X.0gA r2 = r4.A09     // Catch:{ NullPointerException -> 0x05bb }
            boolean r2 = r2.A0B     // Catch:{ NullPointerException -> 0x05bb }
            if (r2 == 0) goto L_0x0482
            X.8cg r18 = X.C182058cg.A06     // Catch:{ NullPointerException -> 0x05bb }
            r6 = 2
            int r3 = X.AnonymousClass1Y3.AgK     // Catch:{ NullPointerException -> 0x05bb }
            X.0UN r2 = r15.A00     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            X.06B r2 = (X.AnonymousClass06B) r2     // Catch:{ NullPointerException -> 0x05bb }
            long r19 = r2.now()     // Catch:{ NullPointerException -> 0x05bb }
            r16 = r0
            r17 = r4
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r15, r16, r17, r18, r19)     // Catch:{ NullPointerException -> 0x05bb }
            r2 = 1
            goto L_0x0483
        L_0x0482:
            r2 = 0
        L_0x0483:
            if (r2 != 0) goto L_0x05b6
            int r3 = X.AnonymousClass1Y3.AgK     // Catch:{ NullPointerException -> 0x05bb }
            X.0UN r2 = r15.A00     // Catch:{ NullPointerException -> 0x05bb }
            r7 = 2
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            X.06B r2 = (X.AnonymousClass06B) r2     // Catch:{ NullPointerException -> 0x05bb }
            long r11 = r2.now()     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = r4.A00     // Catch:{ NullPointerException -> 0x05bb }
            long r11 = r11 - r2
            X.0gA r2 = r4.A09     // Catch:{ NullPointerException -> 0x05bb }
            int r3 = r2.A04     // Catch:{ NullPointerException -> 0x05bb }
            r2 = -1
            if (r3 != r2) goto L_0x049f
            goto L_0x04a7
        L_0x049f:
            r2 = 86400(0x15180, float:1.21072E-40)
            int r2 = java.lang.Math.min(r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            goto L_0x04a9
        L_0x04a7:
            r2 = 600(0x258, float:8.41E-43)
        L_0x04a9:
            long r2 = (long) r2     // Catch:{ NullPointerException -> 0x05bb }
            r9 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r9
            int r6 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
            if (r6 <= 0) goto L_0x04ca
            X.8cg r18 = X.C182058cg.A07     // Catch:{ NullPointerException -> 0x05bb }
            int r3 = X.AnonymousClass1Y3.AgK     // Catch:{ NullPointerException -> 0x05bb }
            X.0UN r2 = r15.A00     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            X.06B r2 = (X.AnonymousClass06B) r2     // Catch:{ NullPointerException -> 0x05bb }
            long r19 = r2.now()     // Catch:{ NullPointerException -> 0x05bb }
            r16 = r0
            r17 = r4
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r15, r16, r17, r18, r19)     // Catch:{ NullPointerException -> 0x05bb }
            r2 = 1
            goto L_0x04cb
        L_0x04ca:
            r2 = 0
        L_0x04cb:
            if (r2 != 0) goto L_0x05b6
            int r3 = X.AnonymousClass1Y3.AgK     // Catch:{ NullPointerException -> 0x05bb }
            X.0UN r2 = r15.A00     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            X.06B r2 = (X.AnonymousClass06B) r2     // Catch:{ NullPointerException -> 0x05bb }
            long r11 = r2.now()     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = r4.A06     // Catch:{ NullPointerException -> 0x05bb }
            long r11 = r11 - r2
            X.0gA r2 = r4.A09     // Catch:{ NullPointerException -> 0x05bb }
            int r2 = r2.A02()     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = (long) r2     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = r2 * r9
            int r6 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
            if (r6 <= 0) goto L_0x0503
            X.8cg r18 = X.C182058cg.A08     // Catch:{ NullPointerException -> 0x05bb }
            int r3 = X.AnonymousClass1Y3.AgK     // Catch:{ NullPointerException -> 0x05bb }
            X.0UN r2 = r15.A00     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            X.06B r2 = (X.AnonymousClass06B) r2     // Catch:{ NullPointerException -> 0x05bb }
            long r19 = r2.now()     // Catch:{ NullPointerException -> 0x05bb }
            r16 = r0
            r17 = r4
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r15, r16, r17, r18, r19)     // Catch:{ NullPointerException -> 0x05bb }
            r2 = 1
            goto L_0x0504
        L_0x0503:
            r2 = 0
        L_0x0504:
            if (r2 != 0) goto L_0x05b6
            boolean r7 = r4.A0B     // Catch:{ NullPointerException -> 0x05bb }
            if (r7 != 0) goto L_0x044a
            X.0gA r3 = r4.A09     // Catch:{ NullPointerException -> 0x05bb }
            boolean r2 = r3.A08     // Catch:{ NullPointerException -> 0x05bb }
            if (r2 == 0) goto L_0x044a
            X.25t r6 = new X.25t     // Catch:{ NullPointerException -> 0x05bb }
            r6.<init>()     // Catch:{ NullPointerException -> 0x05bb }
            r6.A06 = r3     // Catch:{ NullPointerException -> 0x05bb }
            X.C415025u.A00(r4)     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = r4.A07     // Catch:{ NullPointerException -> 0x05bb }
            r6.A02 = r2     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = r4.A06     // Catch:{ NullPointerException -> 0x05bb }
            r6.A01 = r2     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = r4.A00     // Catch:{ NullPointerException -> 0x05bb }
            r6.A03 = r2     // Catch:{ NullPointerException -> 0x05bb }
            X.C415025u.A00(r4)     // Catch:{ NullPointerException -> 0x05bb }
            int r3 = r4.A05     // Catch:{ NullPointerException -> 0x05bb }
            r6.A00 = r3     // Catch:{ NullPointerException -> 0x05bb }
            X.C415025u.A00(r4)     // Catch:{ NullPointerException -> 0x05bb }
            boolean r3 = r4.A04     // Catch:{ NullPointerException -> 0x05bb }
            r6.A0A = r3     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.String r3 = r4.A0A     // Catch:{ NullPointerException -> 0x05bb }
            r6.A07 = r3     // Catch:{ NullPointerException -> 0x05bb }
            long r2 = r4.A08     // Catch:{ NullPointerException -> 0x05bb }
            r6.A04 = r2     // Catch:{ NullPointerException -> 0x05bb }
            r6.A09 = r7     // Catch:{ NullPointerException -> 0x05bb }
            if (r7 != 0) goto L_0x0551
            X.C415025u.A00(r4)     // Catch:{ NullPointerException -> 0x05bb }
            X.0Xy r3 = r4.A01     // Catch:{ NullPointerException -> 0x05bb }
            if (r3 == 0) goto L_0x0551
            X.0Xy r2 = new X.0Xy     // Catch:{ NullPointerException -> 0x05bb }
            X.C415025u.A00(r4)     // Catch:{ NullPointerException -> 0x05bb }
            r2.<init>(r3)     // Catch:{ NullPointerException -> 0x05bb }
            r6.A05 = r2     // Catch:{ NullPointerException -> 0x05bb }
        L_0x0551:
            boolean r2 = r4.A0B     // Catch:{ NullPointerException -> 0x05bb }
            if (r2 != 0) goto L_0x0597
            X.C415025u.A00(r4)     // Catch:{ NullPointerException -> 0x05bb }
            java.util.List r2 = r4.A03     // Catch:{ NullPointerException -> 0x05bb }
            if (r2 == 0) goto L_0x0597
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ NullPointerException -> 0x05bb }
            r9.<init>()     // Catch:{ NullPointerException -> 0x05bb }
            X.C415025u.A00(r4)     // Catch:{ NullPointerException -> 0x05bb }
            java.util.Iterator r11 = r2.iterator()     // Catch:{ NullPointerException -> 0x05bb }
        L_0x0568:
            boolean r2 = r11.hasNext()     // Catch:{ NullPointerException -> 0x05bb }
            if (r2 == 0) goto L_0x0595
            java.lang.Object r2 = r11.next()     // Catch:{ NullPointerException -> 0x05bb }
            X.25v r2 = (X.C415125v) r2     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.String r7 = r2.A03     // Catch:{ NullPointerException -> 0x05bb }
            if (r7 == 0) goto L_0x0587
            X.25v r10 = new X.25v     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.String r4 = r2.A02     // Catch:{ NullPointerException -> 0x05bb }
            int r3 = r2.A00     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.String r2 = r2.A04     // Catch:{ NullPointerException -> 0x05bb }
            r10.<init>(r4, r3, r2, r7)     // Catch:{ NullPointerException -> 0x05bb }
        L_0x0583:
            r9.add(r10)     // Catch:{ NullPointerException -> 0x05bb }
            goto L_0x0568
        L_0x0587:
            X.25v r10 = new X.25v     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.String r7 = r2.A02     // Catch:{ NullPointerException -> 0x05bb }
            int r4 = r2.A00     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.String r3 = r2.A04     // Catch:{ NullPointerException -> 0x05bb }
            X.26K r2 = r2.A01     // Catch:{ NullPointerException -> 0x05bb }
            r10.<init>(r7, r4, r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            goto L_0x0583
        L_0x0595:
            r6.A08 = r9     // Catch:{ NullPointerException -> 0x05bb }
        L_0x0597:
            X.25u r7 = new X.25u     // Catch:{ NullPointerException -> 0x05bb }
            r7.<init>(r6)     // Catch:{ NullPointerException -> 0x05bb }
            X.8cg r18 = X.C182058cg.A04     // Catch:{ NullPointerException -> 0x05bb }
            r4 = 2
            int r3 = X.AnonymousClass1Y3.AgK     // Catch:{ NullPointerException -> 0x05bb }
            X.0UN r2 = r15.A00     // Catch:{ NullPointerException -> 0x05bb }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ NullPointerException -> 0x05bb }
            X.06B r2 = (X.AnonymousClass06B) r2     // Catch:{ NullPointerException -> 0x05bb }
            long r19 = r2.now()     // Catch:{ NullPointerException -> 0x05bb }
            r16 = r0
            r17 = r7
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r15, r16, r17, r18, r19)     // Catch:{ NullPointerException -> 0x05bb }
            goto L_0x044a
        L_0x05b6:
            r5.add(r0)     // Catch:{ NullPointerException -> 0x05bb }
            goto L_0x044a
        L_0x05bb:
            r2 = move-exception
            java.lang.Object r0 = r1.getKey()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "NPE for key: %s"
            X.C010708t.A0W(r8, r2, r0, r1)
            goto L_0x044a
        L_0x05cb:
            java.util.Iterator r2 = r5.iterator()
        L_0x05cf:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x05e1
            java.lang.Object r1 = r2.next()
            java.lang.String r1 = (java.lang.String) r1
            java.util.Map r0 = r15.A01
            r0.remove(r1)
            goto L_0x05cf
        L_0x05e1:
            r2 = 6
            int r1 = X.AnonymousClass1Y3.Aaz     // Catch:{ IOException -> 0x07b4 }
            X.0UN r0 = r15.A00     // Catch:{ IOException -> 0x07b4 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ IOException -> 0x07b4 }
            X.25c r6 = (X.C413525c) r6     // Catch:{ IOException -> 0x07b4 }
            java.util.Map r8 = r15.A01     // Catch:{ IOException -> 0x07b4 }
            X.25d r5 = r6.A02     // Catch:{ IOException -> 0x07b4 }
            java.io.File r4 = X.C413625d.A01(r5)     // Catch:{ IOException -> 0x07b4 }
            boolean r0 = r8.isEmpty()     // Catch:{ IOException -> 0x07b4 }
            if (r0 == 0) goto L_0x0608
            java.io.File r0 = X.C413625d.A01(r5)     // Catch:{ IOException -> 0x07b4 }
            r0.delete()     // Catch:{ IOException -> 0x07b4 }
        L_0x0601:
            X.25f r0 = r6.A01     // Catch:{ IOException -> 0x07b4 }
            r0.clear()     // Catch:{ IOException -> 0x07b4 }
            goto L_0x09eb
        L_0x0608:
            java.lang.String r1 = r4.getName()     // Catch:{ IOException -> 0x07b4 }
            java.lang.String r0 = "."
            java.lang.String r2 = X.AnonymousClass08S.A0J(r1, r0)     // Catch:{ IOException -> 0x07b4 }
            java.io.File r1 = r4.getParentFile()     // Catch:{ IOException -> 0x07b4 }
            java.lang.String r0 = ".tmp"
            java.io.File r3 = java.io.File.createTempFile(r2, r0, r1)     // Catch:{ IOException -> 0x07b4 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x07b4 }
            java.io.BufferedOutputStream r7 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x07b4 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x07b4 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x07b4 }
            r0 = 1024(0x400, float:1.435E-42)
            r7.<init>(r1, r0)     // Catch:{ IOException -> 0x07b4 }
            r2.<init>(r7)     // Catch:{ IOException -> 0x07b4 }
            r0 = 1
            r2.writeByte(r0)     // Catch:{ all -> 0x07af }
            int r0 = r8.size()     // Catch:{ all -> 0x07af }
            r2.writeInt(r0)     // Catch:{ all -> 0x07af }
            java.util.Set r0 = r8.entrySet()     // Catch:{ all -> 0x07af }
            java.util.Iterator r11 = r0.iterator()     // Catch:{ all -> 0x07af }
        L_0x0640:
            boolean r0 = r11.hasNext()     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0791
            java.lang.Object r9 = r11.next()     // Catch:{ all -> 0x07af }
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ all -> 0x07af }
            java.lang.Object r0 = r9.getValue()     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0780
            java.lang.Object r0 = r9.getValue()     // Catch:{ all -> 0x07af }
            X.25u r0 = (X.C415025u) r0     // Catch:{ all -> 0x07af }
            X.0gA r0 = r0.A09     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0780
            java.lang.Object r0 = r9.getKey()     // Catch:{ all -> 0x07af }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
            java.lang.Object r7 = r9.getValue()     // Catch:{ all -> 0x07af }
            X.25u r7 = (X.C415025u) r7     // Catch:{ all -> 0x07af }
            boolean r0 = r7.A0B     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0693
            r0 = 1
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            X.0gA r0 = r7.A09     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r0.A0D     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
            r0 = 5
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            long r0 = r7.A00     // Catch:{ all -> 0x07af }
            r2.writeLong(r0)     // Catch:{ all -> 0x07af }
            r0 = 10
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            boolean r0 = r7.A0B     // Catch:{ all -> 0x07af }
            r2.writeBoolean(r0)     // Catch:{ all -> 0x07af }
        L_0x068d:
            r0 = 9
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            goto L_0x0640
        L_0x0693:
            r0 = 1
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            X.0gA r0 = r7.A09     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r0.A0D     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
            r0 = 2
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            X.C415025u.A00(r7)     // Catch:{ all -> 0x07af }
            long r0 = r7.A07     // Catch:{ all -> 0x07af }
            r2.writeLong(r0)     // Catch:{ all -> 0x07af }
            r0 = 3
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            X.C415025u.A00(r7)     // Catch:{ all -> 0x07af }
            int r0 = r7.A05     // Catch:{ all -> 0x07af }
            r2.writeInt(r0)     // Catch:{ all -> 0x07af }
            r0 = 4
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            long r0 = r7.A06     // Catch:{ all -> 0x07af }
            r2.writeLong(r0)     // Catch:{ all -> 0x07af }
            r0 = 5
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            long r0 = r7.A00     // Catch:{ all -> 0x07af }
            r2.writeLong(r0)     // Catch:{ all -> 0x07af }
            X.C415025u.A00(r7)     // Catch:{ all -> 0x07af }
            X.0Xy r1 = r7.A01     // Catch:{ all -> 0x07af }
            if (r1 == 0) goto L_0x06ee
            r0 = 6
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            int r0 = r1.size()     // Catch:{ all -> 0x07af }
            r2.writeInt(r0)     // Catch:{ all -> 0x07af }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x07af }
        L_0x06de:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x06ee
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x07af }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
            goto L_0x06de
        L_0x06ee:
            X.C415025u.A00(r7)     // Catch:{ all -> 0x07af }
            java.util.List r1 = r7.A03     // Catch:{ all -> 0x07af }
            if (r1 == 0) goto L_0x0759
            r0 = 7
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            int r0 = r1.size()     // Catch:{ all -> 0x07af }
            r2.writeInt(r0)     // Catch:{ all -> 0x07af }
            java.util.Iterator r10 = r1.iterator()     // Catch:{ all -> 0x07af }
        L_0x0704:
            boolean r0 = r10.hasNext()     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0759
            java.lang.Object r9 = r10.next()     // Catch:{ all -> 0x07af }
            X.25v r9 = (X.C415125v) r9     // Catch:{ all -> 0x07af }
            r0 = 701(0x2bd, float:9.82E-43)
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r9.A02     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
            r0 = 702(0x2be, float:9.84E-43)
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            int r0 = r9.A00     // Catch:{ all -> 0x07af }
            r2.writeInt(r0)     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r9.A04     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0732
            r0 = 703(0x2bf, float:9.85E-43)
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r9.A04     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
        L_0x0732:
            X.26K r0 = r9.A01     // Catch:{ all -> 0x07af }
            r1 = 704(0x2c0, float:9.87E-43)
            if (r0 == 0) goto L_0x074c
            r2.writeShort(r1)     // Catch:{ all -> 0x07af }
            X.26K r0 = r9.A01     // Catch:{ all -> 0x07af }
            com.fasterxml.jackson.databind.node.ObjectNode r0 = r0.A00     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
        L_0x0746:
            r0 = 705(0x2c1, float:9.88E-43)
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            goto L_0x0704
        L_0x074c:
            java.lang.String r0 = r9.A03     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0746
            r2.writeShort(r1)     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r9.A03     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
            goto L_0x0746
        L_0x0759:
            r0 = 8
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            X.C415025u.A00(r7)     // Catch:{ all -> 0x07af }
            boolean r0 = r7.A04     // Catch:{ all -> 0x07af }
            r2.writeBoolean(r0)     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r7.A0A     // Catch:{ all -> 0x07af }
            if (r0 == 0) goto L_0x0774
            r0 = 11
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            java.lang.String r0 = r7.A0A     // Catch:{ all -> 0x07af }
            r2.writeUTF(r0)     // Catch:{ all -> 0x07af }
        L_0x0774:
            r0 = 12
            r2.writeShort(r0)     // Catch:{ all -> 0x07af }
            long r0 = r7.A08     // Catch:{ all -> 0x07af }
            r2.writeLong(r0)     // Catch:{ all -> 0x07af }
            goto L_0x068d
        L_0x0780:
            java.lang.String r7 = "FunnelBackupStorageFileImpl"
            java.lang.String r1 = "null FunnelDefinition for key %s"
            java.lang.Object r0 = r9.getKey()     // Catch:{ all -> 0x07af }
            java.lang.Object[] r0 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x07af }
            X.C010708t.A0Q(r7, r1, r0)     // Catch:{ all -> 0x07af }
            goto L_0x0640
        L_0x0791:
            r2.close()     // Catch:{ IOException -> 0x07b4 }
            monitor-enter(r5)     // Catch:{ IOException -> 0x07b4 }
            boolean r0 = r3.renameTo(r4)     // Catch:{ all -> 0x07ac }
            if (r0 == 0) goto L_0x07a1
            monitor-exit(r5)     // Catch:{ all -> 0x07ac }
            r8.size()     // Catch:{ IOException -> 0x07b4 }
            goto L_0x0601
        L_0x07a1:
            r3.delete()     // Catch:{ all -> 0x07ac }
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x07ac }
            java.lang.String r0 = "Failed to replace the current preference file!"
            r1.<init>(r0)     // Catch:{ all -> 0x07ac }
            throw r1     // Catch:{ all -> 0x07ac }
        L_0x07ac:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x07ac }
            goto L_0x07b3
        L_0x07af:
            r0 = move-exception
            r2.close()     // Catch:{ IOException -> 0x07b4 }
        L_0x07b3:
            throw r0     // Catch:{ IOException -> 0x07b4 }
        L_0x07b4:
            r3 = move-exception
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.util.Map r0 = r15.A01
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r6 = r0.iterator()
            r5 = 3
        L_0x07c5:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0818
            if (r5 <= 0) goto L_0x0818
            java.lang.Object r2 = r6.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r0 = r2.getValue()
            if (r0 == 0) goto L_0x0815
            java.lang.Object r0 = r2.getValue()
            X.25u r0 = (X.C415025u) r0
            boolean r0 = r0.A0B
            if (r0 != 0) goto L_0x0815
            java.lang.Object r0 = r2.getValue()
            X.25u r0 = (X.C415025u) r0
            X.C415025u.A00(r0)
            java.util.List r0 = r0.A03
            if (r0 == 0) goto L_0x0815
            java.lang.Object r0 = r2.getValue()
            X.25u r0 = (X.C415025u) r0
            X.C415025u.A00(r0)
            java.util.List r0 = r0.A03
            int r1 = r0.size()
            java.lang.String r0 = "Funnel key: "
            r4.append(r0)
            java.lang.Object r0 = r2.getKey()
            java.lang.String r0 = (java.lang.String) r0
            r4.append(r0)
            java.lang.String r0 = ", funnel action count: "
            r4.append(r0)
            r4.append(r1)
        L_0x0815:
            int r5 = r5 + -1
            goto L_0x07c5
        L_0x0818:
            java.lang.String r2 = "FunnelLoggerImpl"
            java.util.Map r0 = r15.A01
            int r0 = r0.size()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = r4.toString()
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r0}
            java.lang.String r0 = "Failed to save funnels! Funnels size = %d, report 3 funnel info: %s. "
            X.C010708t.A0W(r2, r3, r0, r1)
            java.util.Map r0 = r15.A01
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r6 = r0.iterator()
        L_0x083b:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x09eb
            java.lang.Object r5 = r6.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            java.lang.Object r4 = r5.getValue()
            X.25u r4 = (X.C415025u) r4
            java.lang.Object r3 = r5.getKey()
            java.lang.String r3 = (java.lang.String) r3
            X.8cg r18 = X.C182058cg.A03     // Catch:{ NullPointerException -> 0x086c }
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AgK     // Catch:{ NullPointerException -> 0x086c }
            X.0UN r0 = r15.A00     // Catch:{ NullPointerException -> 0x086c }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ NullPointerException -> 0x086c }
            X.06B r0 = (X.AnonymousClass06B) r0     // Catch:{ NullPointerException -> 0x086c }
            long r19 = r0.now()     // Catch:{ NullPointerException -> 0x086c }
            r16 = r3
            r17 = r4
            com.facebook.funnellogger.FunnelLoggerImpl.A09(r15, r16, r17, r18, r19)     // Catch:{ NullPointerException -> 0x086c }
            goto L_0x083b
        L_0x086c:
            r3 = move-exception
            java.lang.String r2 = "FunnelLoggerImpl"
            java.lang.Object r0 = r5.getKey()
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "NPE for key: %s"
            X.C010708t.A0W(r2, r3, r0, r1)
            goto L_0x083b
        L_0x087d:
            java.lang.Object r2 = r0.obj
            X.25X r2 = (X.AnonymousClass25X) r2
            com.facebook.funnellogger.FunnelLoggerImpl r14 = r13.A00
            java.lang.String r15 = com.facebook.funnellogger.FunnelLoggerImpl.A04(r2)
            X.0gA r3 = r2.A00
            java.lang.Long r0 = r2.A04
            if (r0 != 0) goto L_0x08e4
            int r1 = X.AnonymousClass1Y3.A8J
            X.0UN r0 = r14.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r4, r1, r0)
            java.util.Random r0 = (java.util.Random) r0
            long r17 = r0.nextLong()
        L_0x089b:
            java.lang.Long r0 = r2.A05
            long r19 = r0.longValue()
            java.util.List r1 = r2.A0A
            java.lang.Long r0 = r2.A06
            long r21 = r0.longValue()
            r11 = 0
            boolean r0 = r3.A0A
            if (r0 == 0) goto L_0x0946
            boolean r0 = r14.A03
            boolean r0 = X.C413425b.A00(r0, r3)
            if (r0 == 0) goto L_0x08e9
            r23 = 0
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r14)
            java.util.Map r0 = r14.A02
            boolean r0 = r0.containsKey(r15)
            if (r0 != 0) goto L_0x09eb
            r16 = r3
            X.5Jm r1 = com.facebook.funnellogger.FunnelLoggerImpl.A02(r14, r15, r16, r17, r19, r21, r23)
            if (r1 == 0) goto L_0x09eb
            java.util.Map r0 = r14.A02
            r0.put(r15, r1)
            boolean r0 = r1.A05
            if (r0 != 0) goto L_0x09eb
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r14.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.261 r0 = (X.AnonymousClass261) r0
            r0.A05(r3)
            goto L_0x09eb
        L_0x08e4:
            long r17 = r0.longValue()
            goto L_0x089b
        L_0x08e9:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r14)
            java.util.Map r0 = r14.A01
            boolean r0 = r0.containsKey(r15)
            if (r0 != 0) goto L_0x09eb
            r2 = r14
            r4 = r17
            r6 = r19
            r8 = r1
            r9 = r21
            X.25u r5 = com.facebook.funnellogger.FunnelLoggerImpl.A00(r2, r3, r4, r6, r8, r9, r11)
            if (r5 == 0) goto L_0x09eb
            java.util.Map r0 = r14.A01
            r0.put(r15, r5)
            boolean r0 = r5.A0B
            if (r0 != 0) goto L_0x0919
            r2 = 4
            int r1 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r14.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.261 r0 = (X.AnonymousClass261) r0
            r0.A05(r3)
        L_0x0919:
            r2 = 10
            int r1 = X.AnonymousClass1Y3.Awn
            X.0UN r0 = r14.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.util.Set r0 = (java.util.Set) r0
            java.util.Iterator r4 = r0.iterator()
        L_0x0929:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x09eb
            java.lang.Object r2 = r4.next()
            X.EvX r2 = (X.C30396EvX) r2
            boolean r0 = r5.A0B
            if (r0 == 0) goto L_0x0940
            r0 = -1
        L_0x093a:
            java.lang.String r1 = r3.A0D
            r2.onStartFunnelIfNotStarted(r1, r0)
            goto L_0x0929
        L_0x0940:
            X.C415025u.A00(r5)
            int r0 = r5.A05
            goto L_0x093a
        L_0x0946:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = "Must enable noop funnels in the FunnelDefinition to use startFunnelIfNotStarted()"
            r1.<init>(r0)
            throw r1
        L_0x094e:
            java.lang.Object r0 = r0.obj
            X.25X r0 = (X.AnonymousClass25X) r0
            com.facebook.funnellogger.FunnelLoggerImpl r6 = r13.A00
            java.lang.String r15 = com.facebook.funnellogger.FunnelLoggerImpl.A04(r0)
            X.0gA r4 = r0.A00
            X.26K r5 = r0.A02
            java.lang.Long r0 = r0.A05
            long r0 = r0.longValue()
            boolean r2 = r6.A03
            boolean r2 = X.C413425b.A00(r2, r4)
            if (r2 == 0) goto L_0x09a7
            com.facebook.funnellogger.FunnelLoggerImpl.A0C(r6)
            java.util.Map r2 = r6.A02
            java.lang.Object r3 = r2.get(r15)
            X.5Jm r3 = (X.C109245Jm) r3
            if (r3 == 0) goto L_0x09eb
            boolean r2 = r3.A05
            if (r2 != 0) goto L_0x09eb
            if (r5 == 0) goto L_0x09eb
            r3.A00 = r0
            r4 = 13
            int r3 = X.AnonymousClass1Y3.AcI
            X.0UN r2 = r6.A00
            java.lang.Object r14 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.9Ja r14 = (X.C195929Ja) r14
            if (r15 == 0) goto L_0x099f
            java.lang.String r22 = r5.toString()
            r16 = 0
            r17 = 0
            r18 = 0
            r21 = 4
            r19 = r0
            X.C195929Ja.A01(r14, r15, r16, r17, r18, r19, r21, r22)
            goto L_0x09eb
        L_0x099f:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Funnel key is null, expecting non-null value"
            r1.<init>(r0)
            throw r1
        L_0x09a7:
            com.facebook.funnellogger.FunnelLoggerImpl.A08(r6)
            java.util.Map r2 = r6.A01
            java.lang.Object r3 = r2.get(r15)
            X.25u r3 = (X.C415025u) r3
            if (r3 == 0) goto L_0x09eb
            boolean r2 = r3.A0B
            if (r2 != 0) goto L_0x09eb
            r3.A00 = r0
            if (r2 != 0) goto L_0x09be
            r3.A02 = r5
        L_0x09be:
            r2 = 10
            int r1 = X.AnonymousClass1Y3.Awn
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            java.util.Set r0 = (java.util.Set) r0
            java.util.Iterator r2 = r0.iterator()
        L_0x09ce:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x09eb
            java.lang.Object r1 = r2.next()
            X.EvX r1 = (X.C30396EvX) r1
            java.lang.String r0 = r4.A0D
            r1.onAddPayload(r0)
            goto L_0x09ce
        L_0x09e0:
            com.facebook.funnellogger.FunnelLoggerImpl r1 = r13.A00
            boolean r0 = com.facebook.funnellogger.FunnelLoggerImpl.A0C(r1)
            if (r0 != 0) goto L_0x09eb
            com.facebook.funnellogger.FunnelLoggerImpl.A07(r1)
        L_0x09eb:
            com.facebook.funnellogger.FunnelLoggerImpl r3 = r13.A00
            int r1 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r3.A00
            r2 = 4
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.261 r0 = (X.AnonymousClass261) r0
            boolean r0 = r0.A06()
            if (r0 == 0) goto L_0x0df8
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            boolean r0 = r3.A03
            if (r0 == 0) goto L_0x0a60
            java.util.Map r0 = r3.A02
            java.util.Collection r0 = r0.values()
            java.util.Iterator r5 = r0.iterator()
            r14 = 0
        L_0x0a12:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0a61
            java.lang.Object r4 = r5.next()
            X.5Jm r4 = (X.C109245Jm) r4
            boolean r0 = r4.A05
            if (r0 != 0) goto L_0x0a12
            X.0gA r0 = r4.A04
            java.lang.String r1 = r0.A0D
            com.google.common.collect.ImmutableSet r0 = X.C413425b.A00
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x0a12
            int r14 = r14 + 1
            X.0gA r1 = r4.A04
            boolean r0 = r1.A07
            if (r0 == 0) goto L_0x0a12
            int r0 = r1.hashCode()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            boolean r0 = r8.containsKey(r1)
            if (r0 == 0) goto L_0x0a5e
            java.lang.Object r0 = r8.get(r1)
            if (r0 == 0) goto L_0x0a5e
            java.lang.Object r0 = r8.get(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
        L_0x0a54:
            int r0 = r0 + 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r8.put(r1, r0)
            goto L_0x0a12
        L_0x0a5e:
            r0 = 0
            goto L_0x0a54
        L_0x0a60:
            r14 = 0
        L_0x0a61:
            java.util.Map r0 = r3.A01
            java.util.Collection r0 = r0.values()
            java.util.Iterator r5 = r0.iterator()
        L_0x0a6b:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0ab9
            java.lang.Object r4 = r5.next()
            X.25u r4 = (X.C415025u) r4
            boolean r0 = r4.A0B
            if (r0 != 0) goto L_0x0a6b
            X.0gA r0 = r4.A09
            java.lang.String r1 = r0.A0D
            com.google.common.collect.ImmutableSet r0 = X.C413425b.A00
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x0a6b
            int r14 = r14 + 1
            X.0gA r1 = r4.A09
            boolean r0 = r1.A07
            if (r0 == 0) goto L_0x0a6b
            int r0 = r1.hashCode()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            boolean r0 = r8.containsKey(r1)
            if (r0 == 0) goto L_0x0ab7
            java.lang.Object r0 = r8.get(r1)
            if (r0 == 0) goto L_0x0ab7
            java.lang.Object r0 = r8.get(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
        L_0x0aad:
            int r0 = r0 + 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r8.put(r1, r0)
            goto L_0x0a6b
        L_0x0ab7:
            r0 = 0
            goto L_0x0aad
        L_0x0ab9:
            int r1 = X.AnonymousClass1Y3.BR6
            X.0UN r0 = r3.A00
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.261 r4 = (X.AnonymousClass261) r4
            boolean r0 = r4.A06()
            if (r0 == 0) goto L_0x0df8
            monitor-enter(r4)
            r12 = 0
            X.263 r0 = r4.A06     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r0.A01()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.263 r7 = r4.A06     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r0 = r7.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r0 == 0) goto L_0x0c6e
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r7.A01     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r5.<init>(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r6 = 4
        L_0x0ade:
            java.nio.ByteBuffer r0 = r7.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r0.limit()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r6 >= r0) goto L_0x0b09
            java.nio.ByteBuffer r0 = r7.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r3 = r0.getInt(r6)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r3 == 0) goto L_0x0b09
            java.nio.ByteBuffer r1 = r7.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r6 + 4
            int r2 = r1.getInt(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r1 = r7.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r6 + 8
            int r1 = r1.getInt(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvY r0 = new X.EvY     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r0.<init>(r3, r2, r1)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r5.add(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r6 = r6 + 12
            goto L_0x0ade
        L_0x0b09:
            java.util.HashMap r9 = new java.util.HashMap     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r9.<init>()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r5.size()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r7.<init>(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.util.Iterator r11 = r5.iterator()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0b1b:
            boolean r0 = r11.hasNext()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r10 = 3
            r6 = 2
            r5 = 1
            if (r0 == 0) goto L_0x0ba0
            java.lang.Object r2 = r11.next()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvY r2 = (X.C30397EvY) r2     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r2.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Object r0 = r9.get(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r0 != 0) goto L_0x0b45
            int r0 = r2.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvZ r1 = new X.EvZ     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r0 = 0
            r1.<init>(r0, r0, r0, r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r9.put(r3, r1)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0b45:
            int r3 = r2.A01     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r3 == r5) goto L_0x0b83
            if (r3 == r6) goto L_0x0b72
            if (r3 == r10) goto L_0x0b61
            r0 = 4
            if (r3 != r0) goto L_0x0b94
            int r0 = r2.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Object r1 = r9.get(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvZ r1 = (X.C30398EvZ) r1     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r2.A02     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r1.A02 = r0     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            goto L_0x0b1b
        L_0x0b61:
            int r0 = r2.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Object r1 = r9.get(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvZ r1 = (X.C30398EvZ) r1     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r2.A02     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r1.A00 = r0     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            goto L_0x0b1b
        L_0x0b72:
            int r0 = r2.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Object r1 = r9.get(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvZ r1 = (X.C30398EvZ) r1     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r2.A02     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r1.A01 = r0     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            goto L_0x0b1b
        L_0x0b83:
            int r0 = r2.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Object r1 = r9.get(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvZ r1 = (X.C30398EvZ) r1     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r2.A02     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r1.A03 = r0     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            goto L_0x0b1b
        L_0x0b94:
            X.1wH r1 = new X.1wH     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.String r0 = "Unexpected secondary id "
            java.lang.String r0 = X.AnonymousClass08S.A09(r0, r3)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            throw r1     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0ba0:
            java.util.Set r0 = r9.keySet()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.util.Iterator r11 = r0.iterator()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0ba8:
            boolean r0 = r11.hasNext()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r0 == 0) goto L_0x0bf1
            java.lang.Object r2 = r11.next()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvY r0 = new X.EvY     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r3 = r2.intValue()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r1 = 0
            r0.<init>(r3, r5, r1)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r7.add(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvY r0 = new X.EvY     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r0.<init>(r3, r6, r1)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r7.add(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvY r0 = new X.EvY     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r0.<init>(r3, r10, r1)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r7.add(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r3 != r5) goto L_0x0bd5
            r2 = r14
            goto L_0x0be5
        L_0x0bd5:
            boolean r0 = r8.containsKey(r2)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r0 == 0) goto L_0x0bef
            java.lang.Object r0 = r8.get(r2)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r2 = r0.intValue()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0be5:
            X.EvY r1 = new X.EvY     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r0 = 4
            r1.<init>(r3, r0, r2)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r7.add(r1)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            goto L_0x0ba8
        L_0x0bef:
            r2 = 0
            goto L_0x0be5
        L_0x0bf1:
            X.263 r6 = r4.A06     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r0 = r6.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r0 == 0) goto L_0x0c66
            int r1 = r7.size()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r6.A01     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r1 > r0) goto L_0x0c52
            java.util.Iterator r10 = r7.iterator()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r5 = 4
        L_0x0c04:
            boolean r0 = r10.hasNext()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r0 == 0) goto L_0x0c2c
            java.lang.Object r3 = r10.next()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            X.EvY r3 = (X.C30397EvY) r3     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r1 = r6.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r3.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r1.putInt(r5, r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r2 = r6.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r1 = r5 + 4
            int r0 = r3.A01     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r2.putInt(r1, r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r2 = r6.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r1 = r5 + 8
            int r0 = r3.A02     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r2.putInt(r1, r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r5 = r5 + 12
            goto L_0x0c04
        L_0x0c2c:
            int r3 = r7.size()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0c30:
            int r0 = r6.A01     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            if (r3 >= r0) goto L_0x0c4d
            java.nio.ByteBuffer r0 = r6.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r2 = 0
            r0.putInt(r5, r2)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r1 = r6.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r5 + 4
            r1.putInt(r0, r2)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.nio.ByteBuffer r1 = r6.A00     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r0 = r5 + 8
            r1.putInt(r0, r2)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            int r5 = r5 + 12
            int r3 = r3 + 1
            goto L_0x0c30
        L_0x0c4d:
            r7.size()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r12 = r9
            goto L_0x0c96
        L_0x0c52:
            X.1y6 r5 = new X.1y6     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.String r3 = "Trying to write "
            int r2 = r7.size()     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.String r1 = " counters to storage with capacity for "
            int r0 = r6.A01     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.String r0 = X.AnonymousClass08S.A0B(r3, r2, r1, r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            r5.<init>(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            throw r5     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0c66:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.String r0 = "ByteBuffer not properly created"
            r1.<init>(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            throw r1     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0c6e:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            java.lang.String r0 = "ByteBuffer not properly created"
            r1.<init>(r0)     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
            throw r1     // Catch:{ IOException -> 0x0c85, 1wH -> 0x0c76 }
        L_0x0c76:
            r2 = move-exception
            java.lang.String r1 = "FunnelReliabilityStatsCollector"
            java.lang.String r0 = "Got invalid counters"
            X.C010708t.A0T(r1, r2, r0)     // Catch:{ all -> 0x0df5 }
            X.263 r1 = r4.A06     // Catch:{ all -> 0x0df5 }
            java.nio.ByteBuffer r0 = r1.A00     // Catch:{ all -> 0x0df5 }
            if (r0 == 0) goto L_0x0c96
            goto L_0x0c93
        L_0x0c85:
            r2 = move-exception
            java.lang.String r1 = "FunnelReliabilityStatsCollector"
            java.lang.String r0 = "Failed to read/update stats on report"
            X.C010708t.A0T(r1, r2, r0)     // Catch:{ all -> 0x0df5 }
            X.263 r1 = r4.A06     // Catch:{ all -> 0x0df5 }
            java.nio.ByteBuffer r0 = r1.A00     // Catch:{ all -> 0x0df5 }
            if (r0 == 0) goto L_0x0c96
        L_0x0c93:
            X.AnonymousClass263.A00(r1)     // Catch:{ all -> 0x0df5 }
        L_0x0c96:
            monitor-exit(r4)     // Catch:{ all -> 0x0df5 }
            if (r12 == 0) goto L_0x0dd2
            int r0 = r12.size()
            if (r0 <= 0) goto L_0x0dd2
            com.facebook.analytics.DeprecatedAnalyticsLogger r2 = r4.A03
            r1 = 0
            java.lang.String r0 = "funnel_analytics_data_loss"
            X.1La r6 = r2.A04(r0, r1)
            boolean r0 = r6.A0B()
            java.lang.String r3 = "FunnelReliabilityStatsCollector"
            if (r0 == 0) goto L_0x0dd2
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.Class<X.0g7> r0 = X.C08870g7.class
            java.lang.reflect.Field[] r10 = r0.getDeclaredFields()
            int r9 = r10.length
            r5 = 0
        L_0x0cbd:
            if (r5 >= r9) goto L_0x0ce0
            r1 = r10[r5]
            r0 = 0
            java.lang.Object r2 = r1.get(r0)     // Catch:{ IllegalAccessException -> 0x0cdd }
            boolean r0 = r2 instanceof X.C08900gA     // Catch:{ IllegalAccessException -> 0x0cdd }
            if (r0 == 0) goto L_0x0cdd
            X.0gA r2 = (X.C08900gA) r2     // Catch:{ IllegalAccessException -> 0x0cdd }
            boolean r0 = r2.A07     // Catch:{ IllegalAccessException -> 0x0cdd }
            if (r0 == 0) goto L_0x0cdd
            int r0 = r2.hashCode()     // Catch:{ IllegalAccessException -> 0x0cdd }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch:{ IllegalAccessException -> 0x0cdd }
            java.lang.String r0 = r2.A0D     // Catch:{ IllegalAccessException -> 0x0cdd }
            r7.put(r1, r0)     // Catch:{ IllegalAccessException -> 0x0cdd }
        L_0x0cdd:
            int r5 = r5 + 1
            goto L_0x0cbd
        L_0x0ce0:
            com.fasterxml.jackson.databind.node.ObjectNode r5 = new com.fasterxml.jackson.databind.node.ObjectNode
            com.fasterxml.jackson.databind.node.JsonNodeFactory r0 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance
            r5.<init>(r0)
            r2 = 1
            java.util.Set r0 = r12.entrySet()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.util.Iterator r13 = r0.iterator()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
        L_0x0cf0:
            boolean r0 = r13.hasNext()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            if (r0 == 0) goto L_0x0db9
            java.lang.Object r9 = r13.next()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Object r0 = r9.getKey()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.intValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            if (r0 != r2) goto L_0x0d0a
            r12 = r14
            goto L_0x0d22
        L_0x0d0a:
            java.lang.Object r0 = r9.getKey()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            boolean r0 = r8.containsKey(r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            if (r0 == 0) goto L_0x0d9e
            java.lang.Object r0 = r9.getKey()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Object r0 = r8.get(r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r12 = r0.intValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
        L_0x0d22:
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r11 = r0.A02     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.A03     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r11 = r11 + r0
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.A01     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r11 = r11 - r0
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.A00     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r11 = r11 - r0
            int r11 = r11 - r12
            com.fasterxml.jackson.databind.node.ObjectNode r1 = new com.fasterxml.jackson.databind.node.ObjectNode     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            com.fasterxml.jackson.databind.node.JsonNodeFactory r0 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            r1.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.String r10 = "start"
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.A03     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            r1.put(r10, r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.String r10 = "end"
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.A01     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            r1.put(r10, r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.String r10 = "cancel"
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.A00     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            r1.put(r10, r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.String r10 = "prev"
            java.lang.Object r0 = r9.getValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            X.EvZ r0 = (X.C30398EvZ) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.A02     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            r1.put(r10, r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.String r0 = "ongoing"
            r1.put(r0, r12)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.String r0 = "loss"
            r1.put(r0, r11)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Object r0 = r9.getKey()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            int r0 = r0.intValue()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            if (r0 != r2) goto L_0x0da0
            java.lang.String r0 = "overall"
            r6.A04(r0, r1)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            goto L_0x0cf0
        L_0x0d9e:
            r12 = 0
            goto L_0x0d22
        L_0x0da0:
            java.lang.Object r0 = r9.getKey()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            boolean r0 = r7.containsKey(r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            if (r0 == 0) goto L_0x0cf0
            java.lang.Object r0 = r9.getKey()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.Object r0 = r7.get(r0)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            r5.put(r0, r1)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            goto L_0x0cf0
        L_0x0db9:
            int r0 = r5.size()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            if (r0 <= 0) goto L_0x0dc4
            java.lang.String r0 = "funnel_level_data_loss"
            r6.A04(r0, r5)     // Catch:{ IllegalArgumentException -> 0x0dc8 }
        L_0x0dc4:
            r6.A0A()     // Catch:{ IllegalArgumentException -> 0x0dc8 }
            goto L_0x0dd2
        L_0x0dc8:
            r2 = move-exception
            java.lang.Object[] r1 = new java.lang.Object[]{r5}
            java.lang.String r0 = "Failed to report funnel lib data loss event with event payload = %s"
            X.C010708t.A0W(r3, r2, r0, r1)
        L_0x0dd2:
            X.0aI r0 = r4.A00
            if (r0 != 0) goto L_0x0de0
            X.0XQ r1 = r4.A05
            java.lang.String r0 = "funnel_reliability_stats"
            X.0aI r0 = r1.A00(r0)
            r4.A00 = r0
        L_0x0de0:
            X.0aI r0 = r4.A00
            X.16O r3 = r0.A06()
            X.06B r0 = r4.A04
            long r1 = r0.now()
            java.lang.String r0 = "reliability_stats_last_flush_timestamp"
            r3.A0A(r0, r1)
            r3.A05()
            return
        L_0x0df5:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0df5 }
            throw r0
        L_0x0df8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C185514c.handleMessage(android.os.Message):void");
    }
}
