package X;

import android.graphics.Color;
import com.google.common.collect.ImmutableSet;

/* renamed from: X.0pT  reason: invalid class name and case insensitive filesystem */
public final class C12500pT implements C12510pU {
    public static final C12520pV A00 = new C12520pV("Delta", "Deltas from the sync protocol", Color.argb(255, 0, (int) AnonymousClass1Y3.A1c, 0));
    public static final C12520pV A01 = new C12520pV("Sync Exception", "Uncaught exceptions from the sync protocol", -65536);
    public static final C12520pV A02 = new C12520pV("Sync network", "Sync connection events (i.e. get_diffs)", -16776961);
    public static final C12520pV A03 = new C12520pV("Threads Fetch Cache", "Thread operations Cache level", -7829368);
    public static final C12520pV A04 = new C12520pV("Threads Fetch Caller", "Thread operations' callers", C15320v6.MEASURED_STATE_MASK);
    public static final C12520pV A05 = new C12520pV("Threads Fetch DB", "Thread operations at DB level", -65281);
    public static final C12520pV A06 = new C12520pV("Threads Fetch Web", "FQL/GQL fetches related to threads/messages", Color.argb(255, 255, 165, 0));

    public static final C12500pT A00() {
        return new C12500pT();
    }

    public ImmutableSet B5I() {
        return ImmutableSet.A09(A06, A05, A03, A04, A00, A01, A02);
    }
}
