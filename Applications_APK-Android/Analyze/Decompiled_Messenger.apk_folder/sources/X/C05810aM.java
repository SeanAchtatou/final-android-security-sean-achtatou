package X;

import android.accounts.Account;
import android.content.Context;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.prefs.shared.FbSharedPreferences;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0aM  reason: invalid class name and case insensitive filesystem */
public final class C05810aM {
    private static volatile C05810aM A01;
    public AnonymousClass0UN A00;

    public static final C05810aM A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (C05810aM.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new C05810aM(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    private boolean A01() {
        int i = AnonymousClass1Y3.AOJ;
        if (((C25051Yd) AnonymousClass1XX.A02(0, i, this.A00)).BFd(false)) {
            return ((C25051Yd) AnonymousClass1XX.A02(0, i, this.A00)).Aer(286379829369585L, AnonymousClass0XE.A05);
        }
        return false;
    }

    public void A03(boolean z) {
        C30281hn edit = ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A00)).edit();
        edit.putBoolean(C10730kl.A0I, z);
        edit.commit();
    }

    private C05810aM(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public void A02() {
        boolean A012 = A01();
        String $const$string = TurboLoader.Locator.$const$string(16);
        if (A012 && !((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A00)).Aep(C10730kl.A0I, false)) {
            int i = AnonymousClass1Y3.BCt;
            Account A002 = C17430yt.A00((Context) AnonymousClass1XX.A02(1, i, this.A00), $const$string);
            if (A002 != null) {
                C30291ho r2 = new C30291ho();
                r2.A02("session_permanence_experiment_metadata", "enabled");
                r2.A01((Context) AnonymousClass1XX.A02(1, i, this.A00), A002);
                A03(true);
            }
        } else if (!A01() && ((FbSharedPreferences) AnonymousClass1XX.A02(2, AnonymousClass1Y3.B6q, this.A00)).Aep(C10730kl.A0I, false)) {
            int i2 = AnonymousClass1Y3.BCt;
            Account A003 = C17430yt.A00((Context) AnonymousClass1XX.A02(1, i2, this.A00), $const$string);
            if (A003 != null) {
                C30291ho r22 = new C30291ho();
                r22.A01.remove("session_permanence_experiment_metadata");
                r22.A01((Context) AnonymousClass1XX.A02(1, i2, this.A00), A003);
                A03(false);
            }
        }
    }
}
