package X;

import java.util.ArrayList;

/* renamed from: X.1we  reason: invalid class name and case insensitive filesystem */
public final class C37951we extends E76 {
    public AnonymousClass11D A00;

    public AnonymousClass11F A2x() {
        return this;
    }

    public /* bridge */ /* synthetic */ C17770zR A31() {
        return this.A00;
    }

    public void A32(C17770zR r1) {
        this.A00 = (AnonymousClass11D) r1;
    }

    public /* bridge */ /* synthetic */ E76 A38(AnonymousClass6I0 r2) {
        this.A00.A03 = r2;
        return this;
    }

    public void A39(AnonymousClass11F r2) {
        if (r2 != null) {
            A3A(r2.A31());
        }
    }

    public void A3A(C17770zR r3) {
        if (r3 != null) {
            AnonymousClass11D r1 = this.A00;
            if (r1.A04 == null) {
                r1.A04 = new ArrayList();
            }
            r1.A04.add(r3);
        }
    }

    public void A3C(C14940uO r2) {
        this.A00.A00 = r2;
    }

    public void A3D(C14940uO r2) {
        this.A00.A01 = r2;
    }

    public void A3E(C14950uP r2) {
        this.A00.A02 = r2;
    }

    public void A3B(AnonymousClass0p4 r1, int i, int i2, AnonymousClass11D r4) {
        super.A2L(r1, i, i2, r4);
        this.A00 = r4;
    }

    public /* bridge */ /* synthetic */ E76 A33(AnonymousClass11F r1) {
        A39(r1);
        return this;
    }

    public /* bridge */ /* synthetic */ E76 A34(C17770zR r1) {
        A3A(r1);
        return this;
    }

    public /* bridge */ /* synthetic */ E76 A35(C14940uO r1) {
        A3C(r1);
        return this;
    }

    public /* bridge */ /* synthetic */ E76 A36(C14940uO r1) {
        A3D(r1);
        return this;
    }

    public /* bridge */ /* synthetic */ E76 A37(C14950uP r1) {
        A3E(r1);
        return this;
    }
}
