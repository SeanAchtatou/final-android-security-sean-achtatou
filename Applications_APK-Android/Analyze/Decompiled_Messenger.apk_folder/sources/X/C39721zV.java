package X;

import android.animation.TypeEvaluator;
import com.mapbox.mapboxsdk.geometry.LatLng;

/* renamed from: X.1zV  reason: invalid class name and case insensitive filesystem */
public final class C39721zV extends C28411DuT {
    public TypeEvaluator A00() {
        return new C28280Dru();
    }

    public C39721zV(LatLng latLng, LatLng latLng2, C28501Dvy dvy, int i) {
        super(latLng, latLng2, dvy, i);
    }
}
