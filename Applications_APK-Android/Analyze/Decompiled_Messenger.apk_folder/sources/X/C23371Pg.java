package X;

import java.lang.reflect.InvocationTargetException;

/* renamed from: X.1Pg  reason: invalid class name and case insensitive filesystem */
public final class C23371Pg implements C23351Pe {
    public final int A00;
    public final C23351Pe A01;
    public final Integer A02;
    private final boolean A03;

    private AnonymousClass1T5 A00(AnonymousClass1O3 r6, boolean z) {
        try {
            return ((C23351Pe) Class.forName("com.facebook.imagepipeline.nativecode.NativeJpegTranscoderFactory").getConstructor(Integer.TYPE, Boolean.TYPE).newInstance(Integer.valueOf(this.A00), Boolean.valueOf(this.A03))).createImageTranscoder(r6, z);
        } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            throw new RuntimeException("Dependency ':native-imagetranscoder' is needed to use the default native image transcoder.", e);
        }
    }

    public AnonymousClass1T5 createImageTranscoder(AnonymousClass1O3 r3, boolean z) {
        AnonymousClass1T5 createImageTranscoder;
        C23351Pe r0 = this.A01;
        if (r0 == null) {
            createImageTranscoder = null;
        } else {
            createImageTranscoder = r0.createImageTranscoder(r3, z);
        }
        if (createImageTranscoder == null) {
            Integer num = this.A02;
            if (num == null) {
                createImageTranscoder = null;
            } else {
                int intValue = num.intValue();
                if (intValue == 0) {
                    createImageTranscoder = A00(r3, z);
                } else if (intValue == 1) {
                    createImageTranscoder = new C49642cc(this.A00).createImageTranscoder(r3, z);
                } else {
                    throw new IllegalArgumentException("Invalid ImageTranscoderType");
                }
            }
        }
        if (createImageTranscoder == null && C30601iL.A00) {
            createImageTranscoder = A00(r3, z);
        }
        if (createImageTranscoder == null) {
            return new C49642cc(this.A00).createImageTranscoder(r3, z);
        }
        return createImageTranscoder;
    }

    public C23371Pg(int i, boolean z, C23351Pe r3, Integer num) {
        this.A00 = i;
        this.A03 = z;
        this.A01 = r3;
        this.A02 = num;
    }
}
