package X;

import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.1de  reason: invalid class name and case insensitive filesystem */
public final class C27721de {
    public final ArrayList A00 = new ArrayList();
    public final HashMap A01 = new HashMap();

    public Fragment A00(String str) {
        C012909q r0 = (C012909q) this.A01.get(str);
        if (r0 != null) {
            return r0.A03();
        }
        return null;
    }

    public List A01() {
        ArrayList arrayList;
        if (this.A00.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.A00) {
            arrayList = new ArrayList(this.A00);
        }
        return arrayList;
    }

    public List A02() {
        ArrayList arrayList = new ArrayList();
        for (C012909q r0 : this.A01.values()) {
            if (r0 != null) {
                arrayList.add(r0.A03());
            } else {
                arrayList.add(null);
            }
        }
        return arrayList;
    }

    public void A03(Fragment fragment) {
        synchronized (this.A00) {
            this.A00.remove(fragment);
        }
        fragment.A0W = false;
    }

    public void A04(Fragment fragment) {
        if (!this.A00.contains(fragment)) {
            synchronized (this.A00) {
                this.A00.add(fragment);
            }
            fragment.A0W = true;
            return;
        }
        throw new IllegalStateException("Fragment already added: " + fragment);
    }
}
