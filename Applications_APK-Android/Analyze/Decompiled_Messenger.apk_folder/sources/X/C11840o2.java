package X;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.datatype.guava.ser.GuavaOptionalSerializer;
import com.fasterxml.jackson.datatype.guava.ser.MultimapSerializer;
import com.google.common.base.Optional;

/* renamed from: X.0o2  reason: invalid class name and case insensitive filesystem */
public final class C11840o2 extends C11860o4 {
    public JsonSerializer findMapLikeSerializer(C10450k8 r8, C21991Jm r9, C10120ja r10, JsonSerializer jsonSerializer, CY1 cy1, JsonSerializer jsonSerializer2) {
        C21991Jm r2 = r9;
        if (!AnonymousClass0V0.class.isAssignableFrom(r9._class)) {
            return null;
        }
        return new MultimapSerializer(r8, r2, r10, jsonSerializer, cy1, jsonSerializer2);
    }

    public JsonSerializer findSerializer(C10450k8 r3, C10030jR r4, C10120ja r5) {
        if (Optional.class.isAssignableFrom(r4._class)) {
            return new GuavaOptionalSerializer(r4);
        }
        return super.findSerializer(r3, r4, r5);
    }
}
