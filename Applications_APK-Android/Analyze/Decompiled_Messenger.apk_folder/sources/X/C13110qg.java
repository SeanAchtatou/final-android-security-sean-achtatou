package X;

import android.os.Bundle;
import android.os.SystemClock;
import com.facebook.quicklog.QuickPerformanceLogger;

/* renamed from: X.0qg  reason: invalid class name and case insensitive filesystem */
public abstract class C13110qg extends C12980qK implements AnonymousClass0nG {
    public static final String __redex_internal_original_name = "com.facebook.messenger.neue.MainActivityDelegate";
    public AnonymousClass0UN A00;
    public boolean A01 = false;

    public abstract void A0H(Bundle bundle);

    public final void A08(Bundle bundle) {
        C07620dr.A00(this.A00);
        long uptimeMillis = SystemClock.uptimeMillis();
        super.A08(bundle);
        AnonymousClass0UN r1 = new AnonymousClass0UN(0, AnonymousClass1XX.get(this.A00));
        this.A00 = r1;
        QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A03(AnonymousClass1Y3.BBd, r1);
        quickPerformanceLogger.markerStart(5505068, 0, uptimeMillis);
        A0H(bundle);
        quickPerformanceLogger.markerEnd(5505068, 2);
    }

    public void A07() {
        super.A07();
        this.A01 = false;
    }

    public void A0D() {
        super.A0D();
        this.A01 = true;
    }
}
