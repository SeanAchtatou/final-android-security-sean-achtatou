package X;

import android.content.Context;
import android.os.Build;

/* renamed from: X.0Fi  reason: invalid class name and case insensitive filesystem */
public abstract class C02540Fi {
    public abstract boolean A02();

    public abstract boolean A03(long[] jArr);

    public static C02540Fi A00(Context context) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            return new AnonymousClass0HR(context);
        }
        if (i >= 14) {
            C02550Fj r1 = new C02550Fj();
            if (r1.A03(new long[8])) {
                return r1;
            }
        }
        return new AnonymousClass0Jq(context);
    }
}
