package X;

import java.util.Random;

/* renamed from: X.0HU  reason: invalid class name */
public final class AnonymousClass0HU {
    public static final Random A00 = new Random();

    public static long A00(String str) {
        long j = 0;
        int i = 0;
        while (i < str.length()) {
            long j2 = j << 6;
            int indexOf = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".indexOf(str.charAt(i));
            if (indexOf >= 0) {
                j = j2 + ((long) indexOf);
                i++;
            } else {
                throw new IllegalArgumentException(AnonymousClass08S.A0J("Invalid encoded integer ", str));
            }
        }
        return j;
    }

    public static String A01() {
        long abs;
        do {
            abs = Math.abs(A00.nextLong());
        } while (abs <= 0);
        return A02(abs);
    }

    public static String A02(long j) {
        if (j < 0) {
            throw new IllegalArgumentException(AnonymousClass08S.A0G("Cannot internalEncode negative integer ", j));
        } else if (j <= (1 << Math.min(63, 66)) - 1) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 11; i++) {
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((int) (j % 64)));
                j >>= 6;
            }
            if (j <= 0) {
                sb.reverse();
                return sb.toString();
            }
            throw new IllegalArgumentException("Number won't fit in string");
        } else {
            throw new IllegalArgumentException("Cannot internalEncode integer " + j + " in " + 11 + " chars");
        }
    }
}
