package X;

import android.animation.ValueAnimator;
import com.facebook.interstitial.triggers.InterstitialTrigger;
import com.facebook.interstitial.triggers.InterstitialTriggerContext;
import com.facebook.litho.LithoView;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.facebook.quickpromotion.model.QuickPromotionDefinition;
import io.card.payment.BuildConfig;

/* renamed from: X.0x3  reason: invalid class name and case insensitive filesystem */
public final class C16430x3 {
    public int A00;
    public InterstitialTriggerContext A01;
    public LithoView A02;
    public C136596Zu A03;
    public MigColorScheme A04;
    public QuickPromotionDefinition A05;
    public C148426ul A06;
    public C15750vt A07;
    public Integer A08;
    public boolean A09;
    public boolean A0A;
    private AnonymousClass0UN A0B;

    public static final C16430x3 A00(AnonymousClass1XY r1) {
        return new C16430x3(r1);
    }

    private boolean A01(QuickPromotionDefinition quickPromotionDefinition, InterstitialTrigger interstitialTrigger) {
        InterstitialTriggerContext interstitialTriggerContext;
        if (interstitialTrigger != null) {
            interstitialTriggerContext = interstitialTrigger.A00;
        } else {
            interstitialTriggerContext = null;
        }
        if (quickPromotionDefinition == null || quickPromotionDefinition.equals(this.A05) || interstitialTriggerContext == null || interstitialTriggerContext.equals(this.A01)) {
            return false;
        }
        return true;
    }

    public void A02() {
        LithoView lithoView = this.A02;
        if (lithoView != null) {
            lithoView.setVisibility(8);
        }
    }

    public void A03() {
        String str;
        this.A06.A04();
        QuickPromotionDefinition.Creative A082 = this.A05.A08();
        C148426ul r4 = this.A06;
        C148596v2 r3 = new C148596v2();
        r3.A04 = AnonymousClass8VO.A00(A082.title, this.A01);
        r3.A00 = AnonymousClass8VO.A00(A082.content, this.A01);
        QuickPromotionDefinition.Action action = A082.primaryAction;
        String str2 = BuildConfig.FLAVOR;
        if (action != null) {
            str = AnonymousClass8VO.A00(action.title, this.A01);
        } else {
            str = str2;
        }
        r3.A01 = str;
        QuickPromotionDefinition.Action action2 = A082.secondaryAction;
        if (action2 != null) {
            str2 = AnonymousClass8VO.A00(action2.title, this.A01);
        }
        r3.A02 = str2;
        r4.A08(r3);
    }

    public void A04() {
        LithoView lithoView = this.A02;
        if (lithoView != null) {
            if (this.A03 != null) {
                ValueAnimator ofFloat = ValueAnimator.ofFloat(-1.0f, 0.0f);
                ofFloat.setDuration(300L);
                ofFloat.addUpdateListener(new C136606Zv(this, lithoView));
                ofFloat.start();
            }
            this.A02.setVisibility(0);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0064 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(java.lang.Integer r10, X.C22281Ks r11, X.C136596Zu r12) {
        /*
            r9 = this;
            X.0vt r1 = r9.A07
            java.lang.String r0 = "setLithoBannerViewStub() must be called before showing the banner"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            X.0vt r0 = r9.A07
            android.view.View r1 = r0.A02()
            java.lang.String r0 = "getViewOrViewStub() returned null, check your stub is set up correctly"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            r9.A03 = r12
            int r2 = X.AnonymousClass1Y3.AQy
            X.0UN r1 = r9.A0B
            r0 = 2
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.A5v r4 = (X.C21282A5v) r4
            X.0vt r0 = r9.A07
            android.view.View r0 = r0.A02()
            android.content.Context r0 = r0.getContext()
            android.content.Intent r3 = r11.A03(r0)
            r5 = 0
            r2 = 0
            if (r3 != 0) goto L_0x0067
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "ThreadListFragment_QPBanner_NullIntent"
            java.lang.String r0 = "Null intent to present from QP Banner Controller"
        L_0x003f:
            r2.CGY(r1, r0)
        L_0x0042:
            X.0vt r0 = r9.A07
            android.view.View r0 = r0.A02()
            android.content.Context r0 = r0.getContext()
            android.content.Intent r0 = r11.A03(r0)
            if (r0 == 0) goto L_0x0065
            android.os.Bundle r1 = r0.getExtras()
            java.lang.String r0 = "qp_trigger"
            java.lang.Object r8 = r1.get(r0)
            com.facebook.interstitial.triggers.InterstitialTrigger r8 = (com.facebook.interstitial.triggers.InterstitialTrigger) r8
        L_0x005e:
            boolean r0 = r9.A01(r5, r8)
            if (r0 != 0) goto L_0x00a4
            return
        L_0x0065:
            r8 = 0
            goto L_0x005e
        L_0x0067:
            r0 = 22
            java.lang.String r1 = X.C99084oO.$const$string(r0)
            boolean r0 = r3.hasExtra(r1)
            if (r0 != 0) goto L_0x0082
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "ThreadListFragment_QPBanner_MissingKey"
            java.lang.String r0 = "Intent missing QP_DEFINITION_KEY"
            goto L_0x003f
        L_0x0082:
            java.lang.String r0 = "qp_trigger"
            boolean r0 = r3.hasExtra(r0)
            if (r0 != 0) goto L_0x0099
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r4.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r1 = "ThreadListFragment_QPBanner_MissingTrigger"
            java.lang.String r0 = "Intent missing QP_TRIGGER"
            goto L_0x003f
        L_0x0099:
            android.os.Bundle r0 = r3.getExtras()
            java.lang.Object r5 = r0.get(r1)
            com.facebook.quickpromotion.model.QuickPromotionDefinition r5 = (com.facebook.quickpromotion.model.QuickPromotionDefinition) r5
            goto L_0x0042
        L_0x00a4:
            r2 = 0
            int r1 = X.AnonymousClass1Y3.AHe
            X.0UN r0 = r9.A0B
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.2Hf r1 = (X.C44182Hf) r1
            X.6Y1 r6 = new X.6Y1
            r6.<init>(r9, r1, r12)
            java.lang.String r7 = r11.Aqc()
            r3 = r9
            r4 = r10
            r3.A06(r4, r5, r6, r7, r8)
            r9.A03()
            r9.A04()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16430x3.A05(java.lang.Integer, X.1Ks, X.6Zu):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00ba, code lost:
        if (r12.primaryAction == null) goto L_0x00bc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(java.lang.Integer r15, com.facebook.quickpromotion.model.QuickPromotionDefinition r16, android.view.View.OnClickListener r17, java.lang.String r18, com.facebook.interstitial.triggers.InterstitialTrigger r19) {
        /*
            r14 = this;
            r13 = r19
            if (r19 == 0) goto L_0x0174
            com.facebook.interstitial.triggers.InterstitialTriggerContext r1 = r13.A00
        L_0x0006:
            r2 = r16
            boolean r0 = r14.A01(r2, r13)
            if (r0 == 0) goto L_0x0115
            r14.A05 = r2
            com.facebook.quickpromotion.model.QuickPromotionDefinition$Creative r12 = r2.A08()
            r14.A01 = r1
            r2 = 1
            int r1 = X.AnonymousClass1Y3.B0A
            X.0UN r0 = r14.A0B
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.6ui r9 = (X.C148396ui) r9
            com.facebook.quickpromotion.model.QuickPromotionDefinition r10 = r14.A05
            X.6ul r8 = new X.6ul
            r11 = r18
            r8.<init>(r9, r10, r11, r12, r13)
            r14.A06 = r8
            X.0vt r1 = r14.A07
            java.lang.String r0 = "setLithoBannerViewStub() must be called before showing the banner"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            X.0vt r0 = r14.A07
            android.view.View r1 = r0.A02()
            java.lang.String r0 = "getViewOrViewStub() returned null, check your stub is set up correctly"
            com.google.common.base.Preconditions.checkNotNull(r1, r0)
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r14.A04
            if (r0 != 0) goto L_0x0048
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = X.C17190yT.A00()
            r14.A04 = r0
        L_0x0048:
            com.facebook.litho.LithoView r0 = r14.A02
            if (r0 != 0) goto L_0x007a
            X.0vt r0 = r14.A07
            android.view.View r2 = r0.A02()
            androidx.appcompat.widget.ViewStubCompat r2 = (androidx.appcompat.widget.ViewStubCompat) r2
            r0 = 2132411920(0x7f1a0610, float:2.0473259E38)
            r2.A01 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            boolean r1 = r15.equals(r0)
            r0 = 2131300042(0x7f090eca, float:1.8218102E38)
            if (r1 == 0) goto L_0x0067
            r0 = 2131300037(0x7f090ec5, float:1.8218092E38)
        L_0x0067:
            r2.A00 = r0
            android.view.View r1 = r2.A00()
            com.facebook.resources.ui.FbFrameLayout r1 = (com.facebook.resources.ui.FbFrameLayout) r1
            r0 = 2131298829(0x7f090a0d, float:1.8215642E38)
            android.view.View r0 = r1.findViewById(r0)
            com.facebook.litho.LithoView r0 = (com.facebook.litho.LithoView) r0
            r14.A02 = r0
        L_0x007a:
            com.facebook.litho.LithoView r6 = r14.A02
            X.0p4 r2 = r6.A0H
            r10 = 0
            r1 = 1
            java.lang.String r0 = "model"
            java.lang.String[] r5 = new java.lang.String[]{r0}
            java.util.BitSet r4 = new java.util.BitSet
            r4.<init>(r1)
            X.5Pa r3 = new X.5Pa
            r3.<init>()
            X.0zR r0 = r2.A04
            if (r0 == 0) goto L_0x0098
            java.lang.String r0 = r0.A06
            r3.A07 = r0
        L_0x0098:
            r4.clear()
            X.6uz r9 = new X.6uz
            r9.<init>()
            r11 = r17
            r9.A00 = r11
            com.facebook.quickpromotion.model.QuickPromotionDefinition r8 = r14.A05
            r9.A02 = r8
            com.facebook.interstitial.triggers.InterstitialTriggerContext r0 = r14.A01
            r9.A01 = r0
            X.6ul r7 = r14.A06
            r9.A03 = r7
            boolean r0 = com.facebook.quickpromotion.model.QuickPromotionDefinition.A06(r8)
            r2 = 1
            if (r0 == 0) goto L_0x00bc
            com.facebook.quickpromotion.model.QuickPromotionDefinition$Action r1 = r12.primaryAction
            r0 = 1
            if (r1 != 0) goto L_0x00bd
        L_0x00bc:
            r0 = 0
        L_0x00bd:
            r9.A04 = r0
            if (r17 == 0) goto L_0x0187
            if (r7 == 0) goto L_0x017f
            if (r8 == 0) goto L_0x0177
            X.6ut r0 = new X.6ut
            r0.<init>(r9)
            r3.A01 = r0
            r4.set(r10)
            boolean r0 = r14.A0A
            r3.A03 = r0
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            if (r15 == r0) goto L_0x00d8
            r2 = 0
        L_0x00d8:
            r3.A04 = r2
            int r0 = r14.A00
            r3.A00 = r0
            com.facebook.mig.scheme.interfaces.MigColorScheme r0 = r14.A04
            r3.A02 = r0
            r0 = 1
            X.AnonymousClass11F.A0C(r0, r4, r5)
            r6.A0a(r3)
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            boolean r0 = r15.equals(r0)
            if (r0 == 0) goto L_0x0115
            com.facebook.quickpromotion.model.QuickPromotionDefinition r1 = r14.A05
            java.lang.String r0 = "composer_trigger_banner_pointer_location"
            java.lang.String r1 = X.C21282A5v.A01(r1, r0)
            java.lang.String r0 = "text"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0116
            java.lang.Integer r0 = X.AnonymousClass07B.A00
        L_0x0103:
            r14.A08 = r0
            com.facebook.quickpromotion.model.QuickPromotionDefinition r1 = r14.A05
            java.lang.String r0 = "composer_banner_pointer_overflows_to_more_tab"
            java.lang.String r1 = X.C21282A5v.A01(r1, r0)
            java.lang.String r0 = "true"
            boolean r0 = r0.equals(r1)
            r14.A09 = r0
        L_0x0115:
            return
        L_0x0116:
            java.lang.String r0 = "quick_cam"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0121
            java.lang.Integer r0 = X.AnonymousClass07B.A01
            goto L_0x0103
        L_0x0121:
            r0 = 67
            java.lang.String r0 = X.AnonymousClass80H.$const$string(r0)
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0130
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            goto L_0x0103
        L_0x0130:
            java.lang.String r0 = "media_tray"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x013b
            java.lang.Integer r0 = X.AnonymousClass07B.A0N
            goto L_0x0103
        L_0x013b:
            java.lang.String r0 = "stickers"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0146
            java.lang.Integer r0 = X.AnonymousClass07B.A0Y
            goto L_0x0103
        L_0x0146:
            java.lang.String r0 = "payments"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0151
            java.lang.Integer r0 = X.AnonymousClass07B.A0i
            goto L_0x0103
        L_0x0151:
            java.lang.String r0 = "voice_clips"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x015c
            java.lang.Integer r0 = X.AnonymousClass07B.A0n
            goto L_0x0103
        L_0x015c:
            java.lang.String r0 = "games"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0167
            java.lang.Integer r0 = X.AnonymousClass07B.A0o
            goto L_0x0103
        L_0x0167:
            java.lang.String r0 = "more"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0172
            java.lang.Integer r0 = X.AnonymousClass07B.A0p
            goto L_0x0103
        L_0x0172:
            r0 = 0
            goto L_0x0103
        L_0x0174:
            r1 = 0
            goto L_0x0006
        L_0x0177:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "QuickPromotionDefinition is a mandatory param."
            r1.<init>(r0)
            throw r1
        L_0x017f:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "QuickPromotionViewHelper is a mandatory param."
            r1.<init>(r0)
            throw r1
        L_0x0187:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "ClickListener is a mandatory param."
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16430x3.A06(java.lang.Integer, com.facebook.quickpromotion.model.QuickPromotionDefinition, android.view.View$OnClickListener, java.lang.String, com.facebook.interstitial.triggers.InterstitialTrigger):void");
    }

    public boolean A07() {
        LithoView lithoView = this.A02;
        if (lithoView == null || lithoView.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    private C16430x3(AnonymousClass1XY r3) {
        this.A0B = new AnonymousClass0UN(3, r3);
    }
}
