package X;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0C6  reason: invalid class name */
public final class AnonymousClass0C6 {
    public static final byte[] A0U = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
    public String A00;
    public String A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final C01690Bg A0B;
    public final C01460Ai A0C;
    public final C01830Bu A0D;
    public final AnonymousClass0C5 A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final List A0I;
    public final Map A0J;
    public final AtomicInteger A0K;
    public final boolean A0L;
    public final boolean A0M;
    public final boolean A0N;
    public final boolean A0O;
    public final boolean A0P;
    public final boolean A0Q;
    public final boolean A0R;
    public final boolean A0S;
    public final boolean A0T;

    /* JADX WARNING: Can't wrap try/catch for region: R(3:17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00b0, code lost:
        throw new X.AnonymousClass0Rz();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x009f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x00ab */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0C6(java.lang.String r10, java.lang.String r11, int r12, int r13, boolean r14, X.C01830Bu r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, X.C01460Ai r19, java.util.concurrent.atomic.AtomicInteger r20, int r21, int r22, int r23, int r24, int r25, int r26, X.C01690Bg r27, java.util.List r28, boolean r29, boolean r30, boolean r31, java.util.Map r32, boolean r33, java.lang.String r34, boolean r35, boolean r36, int r37, boolean r38, boolean r39) {
        /*
            r9 = this;
            java.lang.String r4 = " "
            r9.<init>()
            r9.A00 = r10
            r9.A01 = r11
            r9.A07 = r12
            r9.A02 = r13
            r9.A0T = r14
            r9.A0D = r15
            X.0C5 r2 = new X.0C5
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r3 = r17
            r7 = r16
            r2.<init>(r7, r3, r0)
            r9.A0E = r2
            r0 = r18
            r9.A0F = r0
            r1 = r19
            r9.A0C = r1
            r0 = r20
            r9.A0K = r0
            r0 = r21
            r9.A03 = r0
            r0 = r22
            r9.A09 = r0
            r0 = r23
            r9.A05 = r0
            r0 = r24
            r9.A08 = r0
            r0 = r25
            r9.A06 = r0
            r0 = r26
            r9.A0A = r0
            r0 = r27
            r9.A0B = r0
            r0 = r28
            r9.A0I = r0
            r0 = r29
            r9.A0R = r0
            r0 = r30
            r9.A0Q = r0
            java.lang.String r3 = r1.A01()     // Catch:{ 0Rz -> 0x00b1 }
            java.lang.Object r5 = r15.second     // Catch:{ 0Rz -> 0x00b1 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ 0Rz -> 0x00b1 }
            r6 = r4
            r8 = r4
            java.lang.String r1 = X.AnonymousClass08S.A0U(r3, r4, r5, r6, r7, r8)     // Catch:{ 0Rz -> 0x00b1 }
            java.lang.String r0 = "utf-8"
            byte[] r3 = r1.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x00ab }
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r2 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r1 = 0
            int r0 = r3.length     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r2.update(r3, r1, r0)     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            byte[] r6 = r2.digest()     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            int r4 = r6.length     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r5.<init>(r4)     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r3 = 0
        L_0x007f:
            if (r3 >= r4) goto L_0x009a
            byte r0 = r6[r3]     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r2 = r0 & 255(0xff, float:3.57E-43)
            byte[] r1 = X.AnonymousClass0C6.A0U     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            int r0 = r2 >>> 4
            byte r0 = r1[r0]     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            char r0 = (char) r0     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r5.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r0 = r2 & 15
            byte r0 = r1[r0]     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            char r0 = (char) r0     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            r5.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            int r3 = r3 + 1
            goto L_0x007f
        L_0x009a:
            java.lang.String r0 = r5.toString()     // Catch:{ NoSuchAlgorithmException -> 0x00a5, UnsupportedEncodingException -> 0x009f }
            goto L_0x00b2
        L_0x009f:
            X.0Rz r0 = new X.0Rz     // Catch:{ UnsupportedEncodingException -> 0x00ab }
            r0.<init>()     // Catch:{ UnsupportedEncodingException -> 0x00ab }
            goto L_0x00aa
        L_0x00a5:
            X.0Rz r0 = new X.0Rz     // Catch:{ UnsupportedEncodingException -> 0x00ab }
            r0.<init>()     // Catch:{ UnsupportedEncodingException -> 0x00ab }
        L_0x00aa:
            throw r0     // Catch:{ UnsupportedEncodingException -> 0x00ab }
        L_0x00ab:
            X.0Rz r0 = new X.0Rz     // Catch:{ 0Rz -> 0x00b1 }
            r0.<init>()     // Catch:{ 0Rz -> 0x00b1 }
            throw r0     // Catch:{ 0Rz -> 0x00b1 }
        L_0x00b1:
            r0 = 0
        L_0x00b2:
            r9.A0G = r0
            r0 = r31
            r9.A0S = r0
            r0 = r32
            r9.A0J = r0
            r0 = r33
            r9.A0P = r0
            r0 = r34
            r9.A0H = r0
            r0 = r35
            r9.A0L = r0
            r0 = r36
            r9.A0M = r0
            r0 = r37
            r9.A04 = r0
            r0 = r38
            r9.A0N = r0
            r0 = r39
            r9.A0O = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0C6.<init>(java.lang.String, java.lang.String, int, int, boolean, X.0Bu, java.lang.String, java.lang.String, java.lang.String, X.0Ai, java.util.concurrent.atomic.AtomicInteger, int, int, int, int, int, int, X.0Bg, java.util.List, boolean, boolean, boolean, java.util.Map, boolean, java.lang.String, boolean, boolean, int, boolean, boolean):void");
    }
}
