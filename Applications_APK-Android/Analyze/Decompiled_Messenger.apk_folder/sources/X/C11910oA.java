package X;

import java.io.Writer;

/* renamed from: X.0oA  reason: invalid class name and case insensitive filesystem */
public final class C11910oA implements AnonymousClass0oB {
    private static C11910oA A00;

    public static synchronized C11910oA A00() {
        C11910oA r0;
        synchronized (C11910oA.class) {
            if (A00 == null) {
                A00 = new C11910oA();
            }
            r0 = A00;
        }
        return r0;
    }

    private void A01(Writer writer, C16910xz r6) {
        int size = r6.A00.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                writer.write(44);
            }
            A03(writer, null, r6.A00.get(i));
        }
    }

    private void A02(Writer writer, C12240os r9) {
        int i = r9.A00;
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 > 0) {
                writer.write(44);
            }
            String A0H = r9.A0H(i2);
            writer.write(34);
            int length = A0H.length();
            for (int i3 = 0; i3 < length; i3++) {
                AnonymousClass32r.A00(writer, A0H.charAt(i3));
            }
            writer.write(34);
            writer.write(58);
            A03(writer, A0H, r9.A0G(i2));
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A03(java.io.Writer r8, java.lang.String r9, java.lang.Object r10) {
        /*
            r7 = this;
            if (r10 != 0) goto L_0x0008
            java.lang.String r0 = "null"
            r8.write(r0)
            return
        L_0x0008:
            boolean r0 = r10 instanceof java.lang.String
            if (r0 == 0) goto L_0x0028
            java.lang.String r10 = (java.lang.String) r10
            r3 = 34
            r8.write(r3)
            int r2 = r10.length()
            r1 = 0
        L_0x0018:
            if (r1 >= r2) goto L_0x0024
            char r0 = r10.charAt(r1)
            X.AnonymousClass32r.A00(r8, r0)
            int r1 = r1 + 1
            goto L_0x0018
        L_0x0024:
            r8.write(r3)
            return
        L_0x0028:
            boolean r0 = r10 instanceof java.lang.Number
            if (r0 == 0) goto L_0x003a
            java.lang.Number r10 = (java.lang.Number) r10
            java.lang.ThreadLocal r0 = X.C60102wa.A01
            java.lang.Object r0 = r0.get()
            X.2wa r0 = (X.C60102wa) r0
            r0.A00(r8, r10)
            return
        L_0x003a:
            boolean r0 = r10 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x004f
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r0 = r10.booleanValue()
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = "true"
        L_0x0048:
            r8.write(r0)
            return
        L_0x004c:
            java.lang.String r0 = "false"
            goto L_0x0048
        L_0x004f:
            boolean r0 = r10 instanceof X.C12250ot
            if (r0 == 0) goto L_0x00ca
            X.0ot r10 = (X.C12250ot) r10
            java.lang.Class<X.0oA> r6 = X.C11910oA.class
            X.3g1 r3 = r10.A03
            if (r3 == 0) goto L_0x006d
            r1 = 0
        L_0x005c:
            int r0 = r3.A00
            if (r1 >= r0) goto L_0x006f
            java.lang.Object[] r0 = r3.A02
            r0 = r0[r1]
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x0070
            int r1 = r1 + 1
            goto L_0x005c
        L_0x006d:
            r4 = 0
            goto L_0x0076
        L_0x006f:
            r1 = -1
        L_0x0070:
            if (r1 < 0) goto L_0x006d
            int[] r0 = r3.A01
            r4 = r0[r1]
        L_0x0076:
            r1 = r7
            X.0oB r0 = r10.A02
            if (r0 == 0) goto L_0x007c
            r1 = r0
        L_0x007c:
            java.lang.Class r3 = r1.getClass()
            boolean r0 = r3.equals(r6)
            if (r0 == 0) goto L_0x008c
            if (r4 != 0) goto L_0x008c
            r10.A0D(r8, r7)
            return
        L_0x008c:
            r0 = r4 & 1
            if (r0 == 0) goto L_0x00a9
            r0 = 34
            r8.write(r0)
            X.32r r1 = new X.32r
            r1.<init>(r8)
            r10.A0D(r1, r7)     // Catch:{ all -> 0x00a4 }
            r1.flush()
            r8.write(r0)
            return
        L_0x00a4:
            r0 = move-exception
            r1.flush()
            throw r0
        L_0x00a9:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = "Unsupported encoder="
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = ", flags="
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = " combination"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x00ca:
            if (r9 == 0) goto L_0x00ea
            java.lang.String r1 = " (found in key '"
            java.lang.String r0 = "')"
            java.lang.String r4 = X.AnonymousClass08S.A0P(r1, r9, r0)
        L_0x00d4:
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "The type "
            java.lang.Class r0 = r10.getClass()
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = " is not supported"
            java.lang.String r0 = X.AnonymousClass08S.A0S(r2, r1, r0, r4)
            r3.<init>(r0)
            throw r3
        L_0x00ea:
            java.lang.String r4 = ""
            goto L_0x00d4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11910oA.A03(java.io.Writer, java.lang.String, java.lang.Object):void");
    }

    public void A04(Writer writer, C12250ot r3) {
        if (r3 instanceof C12240os) {
            A02(writer, (C12240os) r3);
        } else {
            A01(writer, (C16910xz) r3);
        }
    }

    public void AYP(Writer writer, C12250ot r3) {
        if (r3 instanceof C12240os) {
            writer.write(123);
            A02(writer, (C12240os) r3);
            writer.write((int) AnonymousClass1Y3.A0w);
            return;
        }
        writer.write(91);
        A01(writer, (C16910xz) r3);
        writer.write(93);
    }
}
