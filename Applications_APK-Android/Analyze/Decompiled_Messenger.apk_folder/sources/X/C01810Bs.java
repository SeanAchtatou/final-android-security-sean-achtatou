package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.0Bs  reason: invalid class name and case insensitive filesystem */
public final class C01810Bs extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0AH A00;

    public C01810Bs(AnonymousClass0AH r1) {
        this.A00 = r1;
    }

    public void onReceive(Context context, Intent intent) {
        int A01 = C000700l.A01(486491527);
        if (intent == null) {
            C000700l.A0D(intent, 1585398037, A01);
            return;
        }
        intent.getAction();
        AnonymousClass0AH.A03(this.A00, intent);
        C000700l.A0D(intent, -1292855840, A01);
    }
}
