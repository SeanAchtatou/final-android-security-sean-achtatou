package X;

import java.util.Collections;
import java.util.Set;

/* renamed from: X.22H  reason: invalid class name */
public final class AnonymousClass22H {
    private static volatile C38371xK A03;
    public final String A00;
    private final C38371xK A01;
    private final Set A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass22H) {
                AnonymousClass22H r5 = (AnonymousClass22H) obj;
                if (!C28931fb.A07(this.A00, r5.A00) || A00() != r5.A00()) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public C38371xK A00() {
        if (this.A02.contains(C99084oO.$const$string(AnonymousClass1Y3.A3t))) {
            return this.A01;
        }
        if (A03 == null) {
            synchronized (this) {
                if (A03 == null) {
                    A03 = C38371xK.UNKNOWN;
                }
            }
        }
        return A03;
    }

    public int hashCode() {
        int ordinal;
        int A032 = C28931fb.A03(1, this.A00);
        C38371xK A002 = A00();
        if (A002 == null) {
            ordinal = -1;
        } else {
            ordinal = A002.ordinal();
        }
        return (A032 * 31) + ordinal;
    }

    public AnonymousClass22H(C140646hC r3) {
        String str = r3.A01;
        C28931fb.A06(str, C99084oO.$const$string(468));
        this.A00 = str;
        this.A01 = r3.A00;
        this.A02 = Collections.unmodifiableSet(r3.A02);
    }
}
