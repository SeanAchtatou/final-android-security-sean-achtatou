package X;

/* renamed from: X.1xw  reason: invalid class name and case insensitive filesystem */
public final class C38751xw implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.analytics.reporters.FBAppStateReporter$ReportAppStateRunnable";
    public final /* synthetic */ C38801y1 A00;

    public C38751xw(C38801y1 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:252|253|254|255|256|257) */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02fe, code lost:
        if (r9.A00 != 'm') goto L_0x0300;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x0611, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:?, code lost:
        throw r5;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:256:0x0617 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:265:0x0620 */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x02b7 A[Catch:{ 0Cz -> 0x04e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x02f8 A[Catch:{ 0Cz -> 0x04e9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x04f2 A[SYNTHETIC, Splitter:B:191:0x04f2] */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0528  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x053d  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x05d5  */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x05dc  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x05e3  */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x05ea  */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x05f1  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x05f8  */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x05ff  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r87 = this;
            r4 = r87
            X.1y1 r0 = r4.A00
            X.0Xj r5 = r0.A01
            java.lang.String r3 = "fbandroid_cold_start"
            r33 = 0
            r2 = 0
            r1 = r33
            com.facebook.flexiblesampling.SamplingResult r1 = r5.A03(r3, r1, r1, r2)
            X.1y1 r0 = r4.A00
            X.0Tq r0 = r0.A03
            java.lang.Object r0 = r0.get()
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x0031
            java.lang.String r1 = "User not logged in"
        L_0x001f:
            r0 = 0
            if (r1 == 0) goto L_0x0023
            r0 = 1
        L_0x0023:
            if (r0 == 0) goto L_0x003a
            r2 = 1
            X.1y1 r0 = r4.A00
            java.util.concurrent.ScheduledExecutorService r1 = r0.A02
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MINUTES
            r1.schedule(r4, r2, r0)
            return
        L_0x0031:
            boolean r0 = r1.A01
            if (r0 != 0) goto L_0x0038
            java.lang.String r1 = "Sampling config not available"
            goto L_0x001f
        L_0x0038:
            r1 = 0
            goto L_0x001f
        L_0x003a:
            X.1y1 r4 = r4.A00
            java.lang.Object r22 = X.AnonymousClass0NH.A03
            monitor-enter(r22)
            X.07M r0 = r4.A02     // Catch:{ all -> 0x06ab }
            if (r0 != 0) goto L_0x004d
            java.lang.String r1 = "AppStateReporter"
            java.lang.String r0 = "Unable to create parser"
            X.C010708t.A0I(r1, r0)     // Catch:{ all -> 0x06ab }
        L_0x004a:
            monitor-exit(r22)     // Catch:{ all -> 0x06ab }
            goto L_0x069b
        L_0x004d:
            java.lang.Object r2 = X.AnonymousClass01P.A0Z     // Catch:{ all -> 0x06ab }
            monitor-enter(r2)     // Catch:{ all -> 0x06ab }
            X.01P r0 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x06a8 }
            if (r0 == 0) goto L_0x069c
            monitor-exit(r2)     // Catch:{ all -> 0x06a8 }
            X.01P r0 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x06ab }
            java.io.File r0 = r0.A0E     // Catch:{ all -> 0x06ab }
            r86 = r0
            java.io.File r1 = r86.getParentFile()     // Catch:{ all -> 0x06ab }
            r1.getAbsolutePath()     // Catch:{ all -> 0x06ab }
            X.0Jx r6 = new X.0Jx     // Catch:{ all -> 0x06ab }
            r6.<init>()     // Catch:{ all -> 0x06ab }
            X.0Jw r7 = new X.0Jw     // Catch:{ all -> 0x06ab }
            r7.<init>()     // Catch:{ all -> 0x06ab }
            X.0Ju r8 = new X.0Ju     // Catch:{ all -> 0x06ab }
            r8.<init>()     // Catch:{ all -> 0x06ab }
            X.0Jz r9 = new X.0Jz     // Catch:{ all -> 0x06ab }
            r9.<init>()     // Catch:{ all -> 0x06ab }
            X.0D9 r10 = new X.0D9     // Catch:{ all -> 0x06ab }
            r10.<init>()     // Catch:{ all -> 0x06ab }
            X.0Jy r2 = new X.0Jy     // Catch:{ all -> 0x06ab }
            r2.<init>()     // Catch:{ all -> 0x06ab }
            X.0Jv r0 = new X.0Jv     // Catch:{ all -> 0x06ab }
            r0.<init>()     // Catch:{ all -> 0x06ab }
            X.0K0 r5 = new X.0K0     // Catch:{ all -> 0x06ab }
            r11 = r2
            r12 = r0
            java.io.FilenameFilter[] r3 = new java.io.FilenameFilter[]{r6, r7, r8, r9, r10, r11, r12}     // Catch:{ all -> 0x06ab }
            r5.<init>(r3)     // Catch:{ all -> 0x06ab }
            java.io.File[] r21 = r1.listFiles(r5)     // Catch:{ all -> 0x06ab }
            if (r21 != 0) goto L_0x00a6
            java.lang.String r2 = "AppStateReporter"
            java.lang.String r0 = r1.getAbsolutePath()     // Catch:{ all -> 0x06ab }
            java.lang.Object[] r1 = new java.lang.Object[]{r0}     // Catch:{ all -> 0x06ab }
            java.lang.String r0 = "No app state log files found in app state log directory: %s"
            X.C010708t.A0O(r2, r0, r1)     // Catch:{ all -> 0x06ab }
            goto L_0x004a
        L_0x00a6:
            java.io.File[] r31 = r1.listFiles(r6)     // Catch:{ all -> 0x06ab }
            java.io.File[] r30 = r1.listFiles(r7)     // Catch:{ all -> 0x06ab }
            java.io.File[] r29 = r1.listFiles(r8)     // Catch:{ all -> 0x06ab }
            java.io.File[] r28 = r1.listFiles(r9)     // Catch:{ all -> 0x06ab }
            java.io.File[] r27 = r1.listFiles(r10)     // Catch:{ all -> 0x06ab }
            java.io.File[] r26 = r1.listFiles(r2)     // Catch:{ all -> 0x06ab }
            java.io.File[] r25 = r1.listFiles(r0)     // Catch:{ all -> 0x06ab }
            java.util.Calendar r0 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x06ab }
            long r19 = r0.getTimeInMillis()     // Catch:{ all -> 0x06ab }
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x06ab }
            long r19 = r19 - r0
            r17 = 1000(0x3e8, double:4.94E-321)
            long r19 = r19 / r17
            android.content.Context r0 = r4.A00     // Catch:{ all -> 0x06ab }
            X.0K4 r0 = X.AnonymousClass0K4.A00(r0)     // Catch:{ all -> 0x06ab }
            android.content.SharedPreferences r3 = r0.A00     // Catch:{ all -> 0x06ab }
            java.lang.String r2 = "frameworkStartTime"
            r0 = 0
            long r23 = r3.getLong(r2, r0)     // Catch:{ all -> 0x06ab }
            android.content.Context r2 = r4.A00     // Catch:{ all -> 0x06ab }
            X.0K4 r10 = X.AnonymousClass0K4.A00(r2)     // Catch:{ all -> 0x06ab }
            android.content.SharedPreferences r2 = r10.A00     // Catch:{ all -> 0x06ab }
            java.lang.String r8 = "deviceShutdown"
            long r2 = r2.getLong(r8, r0)     // Catch:{ all -> 0x06ab }
            java.lang.String r9 = "previousShutdown"
            java.lang.String r7 = "deviceBootTime"
            int r11 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r11 == 0) goto L_0x063b
            android.content.SharedPreferences r5 = r10.A00     // Catch:{ all -> 0x06ab }
            android.content.SharedPreferences$Editor r5 = r5.edit()     // Catch:{ all -> 0x06ab }
            android.content.SharedPreferences$Editor r9 = r5.putLong(r9, r2)     // Catch:{ all -> 0x06ab }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x06ab }
            long r10 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x06ab }
            long r5 = r5 - r10
            long r5 = r5 / r17
            android.content.SharedPreferences$Editor r5 = r9.putLong(r7, r5)     // Catch:{ all -> 0x06ab }
            android.content.SharedPreferences$Editor r5 = r5.remove(r8)     // Catch:{ all -> 0x06ab }
            r5.apply()     // Catch:{ all -> 0x06ab }
        L_0x011a:
            java.util.HashSet r32 = new java.util.HashSet     // Catch:{ all -> 0x06ab }
            r5 = r32
            r5.<init>()     // Catch:{ all -> 0x06ab }
            r5 = r86
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ all -> 0x06ab }
            r6 = r32
            r6.add(r5)     // Catch:{ all -> 0x06ab }
            r5 = r21
            int r5 = r5.length     // Catch:{ all -> 0x06ab }
            r85 = r5
            r35 = 0
            r34 = 0
        L_0x0135:
            java.lang.String r45 = "_lib"
            r6 = r85
            r5 = r35
            if (r5 >= r6) goto L_0x0663
            r10 = r21[r35]     // Catch:{ all -> 0x06ab }
            r5 = r86
            boolean r5 = r5.equals(r10)     // Catch:{ all -> 0x06ab }
            if (r5 == 0) goto L_0x014e
            r5 = r86
            r5.getAbsolutePath()     // Catch:{ all -> 0x06ab }
            goto L_0x0637
        L_0x014e:
            java.io.RandomAccessFile r36 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0621 }
            java.lang.String r5 = "rw"
            r6 = r36
            r6.<init>(r10, r5)     // Catch:{ Exception -> 0x0621 }
            java.io.FileInputStream r37 = new java.io.FileInputStream     // Catch:{ all -> 0x0618 }
            java.io.FileDescriptor r5 = r6.getFD()     // Catch:{ all -> 0x0618 }
            r6 = r37
            r6.<init>(r5)     // Catch:{ all -> 0x0618 }
            r5 = r36
            java.nio.channels.FileChannel r5 = r5.getChannel()     // Catch:{ all -> 0x060f }
            r8 = 0
            java.nio.channels.FileLock r8 = r5.tryLock()     // Catch:{ IOException -> 0x016e }
            goto L_0x0187
        L_0x016e:
            r7 = move-exception
            java.lang.Throwable r5 = r7.getCause()     // Catch:{ Exception -> 0x017f }
            if (r5 == 0) goto L_0x017e
            int r6 = X.AnonymousClass0KA.A00(r5)     // Catch:{ Exception -> 0x017f }
            r5 = 11
            if (r6 != r5) goto L_0x017e
            goto L_0x0187
        L_0x017e:
            throw r7     // Catch:{ Exception -> 0x017f }
        L_0x017f:
            r7 = move-exception
            java.lang.String r6 = "AppStateReporter"
            java.lang.String r5 = "Error acquiring lock"
            X.C010708t.A0R(r6, r7, r5)     // Catch:{ all -> 0x060f }
        L_0x0187:
            if (r8 != 0) goto L_0x0197
            r10.getAbsolutePath()     // Catch:{ all -> 0x060f }
            java.lang.String r5 = r10.getAbsolutePath()     // Catch:{ all -> 0x060f }
            r6 = r32
            r6.add(r5)     // Catch:{ all -> 0x060f }
            goto L_0x0604
        L_0x0197:
            r10.getAbsolutePath()     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "_native"
            r7 = r31
            java.io.File r43 = X.AnonymousClass0NH.A00(r10, r7, r5)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "_lib"
            r7 = r30
            java.io.File r42 = X.AnonymousClass0NH.A00(r10, r7, r5)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "_anr"
            r7 = r29
            java.io.File r41 = X.AnonymousClass0NH.A00(r10, r7, r5)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "_wrotedump"
            r7 = r28
            java.io.File r40 = X.AnonymousClass0NH.A00(r10, r7, r5)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "_memorytimeline"
            r7 = r27
            java.io.File r44 = X.AnonymousClass0NH.A00(r10, r7, r5)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "_static"
            r7 = r26
            java.io.File r39 = X.AnonymousClass0NH.A00(r10, r7, r5)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "_entity"
            r7 = r25
            java.io.File r38 = X.AnonymousClass0NH.A00(r10, r7, r5)     // Catch:{ all -> 0x060f }
            boolean r46 = r4.A0D()     // Catch:{ all -> 0x060f }
            boolean r61 = r4.A0B()     // Catch:{ all -> 0x060f }
            android.content.Context r9 = r4.A00     // Catch:{ 0Cz -> 0x04e9 }
            X.07M r6 = r4.A02     // Catch:{ 0Cz -> 0x04e9 }
            boolean r60 = r4.A0A()     // Catch:{ 0Cz -> 0x04e9 }
            r49 = r37
            r14 = r41
            r13 = r40
            r7 = r44
            r11 = r43
            r8 = r39
            r64 = r38
            r54 = r19
            r56 = r23
            r58 = r2
            java.lang.String r50 = X.AnonymousClass07N.A02(r10)     // Catch:{ 0Cz -> 0x04e9 }
            java.lang.String r5 = r10.getName()     // Catch:{ 0Cz -> 0x04e9 }
            java.lang.Integer r51 = X.AnonymousClass01V.A00(r5)     // Catch:{ 0Cz -> 0x04e9 }
            long r52 = r10.lastModified()     // Catch:{ 0Cz -> 0x04e9 }
            r5 = 0
            java.util.Properties r62 = X.AnonymousClass07N.A04(r8, r5)     // Catch:{ 0Cz -> 0x04e9 }
            r5 = 1
            java.util.Properties r63 = X.AnonymousClass07N.A04(r7, r5)     // Catch:{ 0Cz -> 0x04e9 }
            r47 = r6
            r48 = r9
            X.07N r7 = r47.A01(r48, r49, r50, r51, r52, r54, r56, r58, r60, r61, r62, r63, r64)     // Catch:{ 0Cz -> 0x04e9 }
            X.01k r5 = X.C002101k.A0D     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            r50 = r5
            r6 = 0
            if (r43 == 0) goto L_0x0236
            long r8 = r11.length()     // Catch:{ 0Cz -> 0x04e9 }
            int r5 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r5 <= 0) goto L_0x0236
            X.0K3 r8 = X.AnonymousClass07N.A01(r11, r4)     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == 0) goto L_0x0233
            char r5 = r8.A00     // Catch:{ 0Cz -> 0x04e9 }
            r50 = r5
        L_0x0233:
            if (r40 == 0) goto L_0x0260
            goto L_0x0239
        L_0x0236:
            r9 = r6
            r8 = r6
            goto L_0x0245
        L_0x0239:
            long r11 = r13.length()     // Catch:{ 0Cz -> 0x04e9 }
            int r5 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r5 <= 0) goto L_0x0260
            X.0K3 r9 = X.AnonymousClass07N.A01(r13, r4)     // Catch:{ 0Cz -> 0x04e9 }
        L_0x0245:
            X.01k r5 = X.C002101k.A0D     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            r51 = r5
            if (r41 == 0) goto L_0x0262
            long r11 = r14.length()     // Catch:{ 0Cz -> 0x04e9 }
            int r5 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r5 <= 0) goto L_0x0262
            X.0K3 r6 = X.AnonymousClass07N.A01(r14, r4)     // Catch:{ 0Cz -> 0x04e9 }
            if (r6 == 0) goto L_0x0262
            char r5 = r6.A00     // Catch:{ 0Cz -> 0x04e9 }
            r51 = r5
            goto L_0x0262
        L_0x0260:
            r9 = r6
            goto L_0x0245
        L_0x0262:
            if (r6 == 0) goto L_0x0269
            if (r8 != 0) goto L_0x0269
        L_0x0266:
            if (r6 == 0) goto L_0x0335
            goto L_0x026b
        L_0x0269:
            r6 = r8
            goto L_0x0266
        L_0x026b:
            char r8 = r7.A05     // Catch:{ 0Cz -> 0x04e9 }
            char r11 = r6.A00     // Catch:{ 0Cz -> 0x04e9 }
            X.01k r5 = X.C002101k.A0E     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r11 != r5) goto L_0x027c
            X.01k r5 = X.C002101k.A0B     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 != r5) goto L_0x027c
            goto L_0x02b2
        L_0x027c:
            X.01k r5 = X.C002101k.A01     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == r5) goto L_0x02ac
            X.01k r5 = X.C002101k.A02     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == r5) goto L_0x02ac
            X.01k r5 = X.C002101k.A00     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == r5) goto L_0x02ac
            X.01k r5 = X.C002101k.A05     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == r5) goto L_0x02ac
            X.01k r5 = X.C002101k.A04     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == r5) goto L_0x02ac
            X.01k r5 = X.C002101k.A06     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == r5) goto L_0x02ac
            X.01k r5 = X.C002101k.A03     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 == r5) goto L_0x02ac
            X.01k r5 = X.C002101k.A0B     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r8 != r5) goto L_0x02b4
        L_0x02ac:
            X.01k r5 = X.C002101k.A07     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r5.mSymbol     // Catch:{ 0Cz -> 0x04e9 }
            if (r11 != r5) goto L_0x02b4
        L_0x02b2:
            r5 = 0
            goto L_0x02b5
        L_0x02b4:
            r5 = 1
        L_0x02b5:
            if (r5 == 0) goto L_0x02b8
            r8 = r11
        L_0x02b8:
            X.07N r47 = new X.07N     // Catch:{ 0Cz -> 0x04e9 }
            char r5 = r7.A02     // Catch:{ 0Cz -> 0x04e9 }
            r49 = r5
            char r5 = r7.A00     // Catch:{ 0Cz -> 0x04e9 }
            r52 = r5
            char r5 = r7.A03     // Catch:{ 0Cz -> 0x04e9 }
            r53 = r5
            int r5 = r7.A06     // Catch:{ 0Cz -> 0x04e9 }
            r54 = r5
            int r5 = r7.A09     // Catch:{ 0Cz -> 0x04e9 }
            r55 = r5
            int r5 = r7.A0A     // Catch:{ 0Cz -> 0x04e9 }
            r56 = r5
            java.lang.String r5 = r7.A0L     // Catch:{ 0Cz -> 0x04e9 }
            r57 = r5
            java.lang.String r5 = r7.A0K     // Catch:{ 0Cz -> 0x04e9 }
            r58 = r5
            java.lang.String r5 = r6.A02     // Catch:{ 0Cz -> 0x04e9 }
            r59 = r5
            long r5 = r6.A01     // Catch:{ 0Cz -> 0x04e9 }
            r62 = r5
            long r5 = r7.A0E     // Catch:{ 0Cz -> 0x04e9 }
            r64 = r5
            long r5 = r7.A0D     // Catch:{ 0Cz -> 0x04e9 }
            r66 = r5
            long r15 = r7.A0H     // Catch:{ 0Cz -> 0x04e9 }
            boolean r5 = r7.A0R     // Catch:{ 0Cz -> 0x04e9 }
            r68 = r5
            byte[] r5 = r7.A0T     // Catch:{ 0Cz -> 0x04e9 }
            r69 = r5
            r70 = 1
            if (r9 == 0) goto L_0x0300
            char r6 = r9.A00     // Catch:{ 0Cz -> 0x04e9 }
            r5 = 109(0x6d, float:1.53E-43)
            r71 = 1
            if (r6 == r5) goto L_0x0302
        L_0x0300:
            r71 = 0
        L_0x0302:
            long r13 = r7.A0C     // Catch:{ 0Cz -> 0x04e9 }
            long r11 = r7.A0B     // Catch:{ 0Cz -> 0x04e9 }
            int r5 = r7.A08     // Catch:{ 0Cz -> 0x04e9 }
            r76 = r5
            int r5 = r7.A07     // Catch:{ 0Cz -> 0x04e9 }
            r77 = r5
            java.lang.String r5 = r7.A0M     // Catch:{ 0Cz -> 0x04e9 }
            r78 = r5
            long r5 = r7.A0F     // Catch:{ 0Cz -> 0x04e9 }
            java.util.Properties r9 = r7.A0P     // Catch:{ 0Cz -> 0x04e9 }
            r81 = r9
            java.util.Properties r9 = r7.A0O     // Catch:{ 0Cz -> 0x04e9 }
            java.io.File r7 = r7.A0J     // Catch:{ 0Cz -> 0x04e9 }
            r48 = r8
            r60 = r62
            r62 = r64
            r64 = r66
            r66 = r15
            r72 = r13
            r74 = r11
            r79 = r5
            r82 = r9
            r83 = r7
            r47.<init>(r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r62, r64, r66, r68, r69, r70, r71, r72, r74, r76, r77, r78, r79, r81, r82, r83)     // Catch:{ 0Cz -> 0x04e9 }
            r7 = r47
        L_0x0335:
            boolean r5 = r4.A0F(r7)     // Catch:{ 0Cz -> 0x04e5 }
            if (r5 == 0) goto L_0x04d9
            X.0Jp r5 = r4.A01     // Catch:{ 0Cz -> 0x04e5 }
            X.0Jo r8 = r5.ALd()     // Catch:{ 0Cz -> 0x04e5 }
            java.lang.Boolean r13 = r4.A07()     // Catch:{ 0Cz -> 0x04ee }
            char r6 = r7.A05     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "status"
            r8.ANL(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            char r6 = r7.A02     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "appStateStatus"
            r8.ANL(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            char r6 = r7.A04     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "nativeStatus"
            r8.ANL(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            char r6 = r7.A01     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "anrNativeStatus"
            r8.ANL(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r6 = r7.A0L     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "checksum"
            r8.ANO(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r6 = r7.A0K     // Catch:{ 0Cz -> 0x04ee }
            r5 = 150(0x96, float:2.1E-43)
            java.lang.String r5 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r5)     // Catch:{ 0Cz -> 0x04ee }
            r8.ANO(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r6 = r7.A0N     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "reportId"
            r8.ANO(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            long r5 = r7.A0G     // Catch:{ 0Cz -> 0x04ee }
            long r5 = r5 / r17
            java.lang.String r11 = "reportTime"
            r47 = r8
            r48 = r11
            r49 = r5
            r47.ANN(r48, r49)     // Catch:{ 0Cz -> 0x04ee }
            long r5 = r7.A0E     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r11 = "rebootTime"
            r48 = r11
            r49 = r5
            r47.ANN(r48, r49)     // Catch:{ 0Cz -> 0x04ee }
            long r5 = r7.A0D     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r11 = "frameworkStartTime"
            r48 = r11
            r49 = r5
            r47.ANN(r48, r49)     // Catch:{ 0Cz -> 0x04ee }
            long r5 = r7.A0H     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r11 = "shutdownTime"
            r48 = r11
            r49 = r5
            r47.ANN(r48, r49)     // Catch:{ 0Cz -> 0x04ee }
            boolean r6 = r7.A0Q     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "fromNative"
            r8.ANP(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            boolean r6 = r7.A0S     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "wroteDump"
            r8.ANP(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            boolean r6 = r4.A0E(r7)     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "splitsInstalledChanged"
            r8.ANP(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            int r6 = r7.A06     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "nextPendingStopCode"
            r8.ANM(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            int r6 = r7.A09     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "pendingStopCount"
            r8.ANM(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r6 = r7.A06()     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "entityName"
            r8.ANO(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            int r6 = r7.A0A     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = "processImportance"
            r8.ANM(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
            long r5 = r7.A0B     // Catch:{ 0Cz -> 0x04ee }
            r11 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            int r9 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r9 == 0) goto L_0x03ef
            java.lang.String r9 = "anrDiffCount"
            r8.ANN(r9, r5)     // Catch:{ 0Cz -> 0x04ee }
        L_0x03ef:
            long r5 = r7.A0C     // Catch:{ 0Cz -> 0x04ee }
            int r9 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r9 == 0) goto L_0x03fa
            java.lang.String r9 = "crashDiffCount"
            r8.ANN(r9, r5)     // Catch:{ 0Cz -> 0x04ee }
        L_0x03fa:
            int r6 = r7.A08     // Catch:{ 0Cz -> 0x04ee }
            r9 = -1
            if (r6 <= r9) goto L_0x0404
            java.lang.String r5 = "watchCrashStatus"
            r8.ANM(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
        L_0x0404:
            int r6 = r7.A07     // Catch:{ 0Cz -> 0x04ee }
            if (r6 <= r9) goto L_0x040d
            java.lang.String r5 = "watchCrashReason"
            r8.ANM(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
        L_0x040d:
            long r5 = r7.A0F     // Catch:{ 0Cz -> 0x04ee }
            int r9 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r9 <= 0) goto L_0x0418
            java.lang.String r9 = "watchTimeMs"
            r8.ANN(r9, r5)     // Catch:{ 0Cz -> 0x04ee }
        L_0x0418:
            char r5 = r7.A00     // Catch:{ 0Cz -> 0x04ee }
            r16 = r5
            X.01Y[] r12 = X.AnonymousClass01Y.values()     // Catch:{ 0Cz -> 0x04ee }
            int r5 = r12.length     // Catch:{ 0Cz -> 0x04ee }
            r14 = r5
            r11 = 0
        L_0x0423:
            if (r11 >= r14) goto L_0x0430
            r9 = r12[r11]     // Catch:{ 0Cz -> 0x04ee }
            char r6 = r9.mLogSymbol     // Catch:{ 0Cz -> 0x04ee }
            r5 = r16
            if (r6 == r5) goto L_0x0432
            int r11 = r11 + 1
            goto L_0x0423
        L_0x0430:
            r9 = r33
        L_0x0432:
            if (r9 == 0) goto L_0x044b
            X.01Y r5 = X.AnonymousClass01Y.BYTE_NOT_PRESENT     // Catch:{ 0Cz -> 0x04ee }
            boolean r5 = r5.equals(r9)     // Catch:{ 0Cz -> 0x04ee }
            if (r5 != 0) goto L_0x044b
            X.01Y r5 = X.AnonymousClass01Y.BYTE_NOT_USED     // Catch:{ 0Cz -> 0x04ee }
            boolean r5 = r5.equals(r9)     // Catch:{ 0Cz -> 0x04ee }
            if (r5 != 0) goto L_0x044b
            java.lang.String r5 = "activityState"
            r14 = r8
            r15 = r5
            r14.ANL(r15, r16)     // Catch:{ 0Cz -> 0x04ee }
        L_0x044b:
            java.lang.String r6 = r7.A0M     // Catch:{ 0Cz -> 0x04ee }
            if (r6 == 0) goto L_0x0454
            java.lang.String r5 = "watchRusage"
            r8.ANO(r5, r6)     // Catch:{ 0Cz -> 0x04ee }
        L_0x0454:
            java.lang.String r6 = "app_started_in_background"
            if (r13 == 0) goto L_0x0487
            boolean r5 = r13.booleanValue()     // Catch:{ 0Cz -> 0x04ee }
            r8.ANP(r6, r5)     // Catch:{ 0Cz -> 0x04ee }
        L_0x045f:
            java.util.Properties r5 = r7.A0P     // Catch:{ 0Cz -> 0x04ee }
            if (r5 == 0) goto L_0x048d
            java.util.Set r5 = r5.entrySet()     // Catch:{ 0Cz -> 0x04ee }
            java.util.Iterator r9 = r5.iterator()     // Catch:{ 0Cz -> 0x04ee }
        L_0x046b:
            boolean r5 = r9.hasNext()     // Catch:{ 0Cz -> 0x04ee }
            if (r5 == 0) goto L_0x048d
            java.lang.Object r5 = r9.next()     // Catch:{ 0Cz -> 0x04ee }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ 0Cz -> 0x04ee }
            java.lang.Object r6 = r5.getKey()     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ 0Cz -> 0x04ee }
            java.lang.Object r5 = r5.getValue()     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ 0Cz -> 0x04ee }
            r8.ANO(r6, r5)     // Catch:{ 0Cz -> 0x04ee }
            goto L_0x046b
        L_0x0487:
            java.lang.String r5 = "unknown"
            r8.ANO(r6, r5)     // Catch:{ 0Cz -> 0x04ee }
            goto L_0x045f
        L_0x048d:
            java.util.Properties r5 = r7.A0O     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r13 = "memorySnapshot"
            if (r5 == 0) goto L_0x04d5
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ 0Cz -> 0x04ee }
            r6.<init>()     // Catch:{ 0Cz -> 0x04ee }
            r12 = 1
            java.util.Set r5 = r5.entrySet()     // Catch:{ 0Cz -> 0x04ee }
            java.util.Iterator r11 = r5.iterator()     // Catch:{ 0Cz -> 0x04ee }
        L_0x04a1:
            boolean r5 = r11.hasNext()     // Catch:{ 0Cz -> 0x04ee }
            if (r5 == 0) goto L_0x04cd
            java.lang.Object r9 = r11.next()     // Catch:{ 0Cz -> 0x04ee }
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ 0Cz -> 0x04ee }
            if (r12 != 0) goto L_0x04b4
            r5 = 44
            r6.append(r5)     // Catch:{ 0Cz -> 0x04ee }
        L_0x04b4:
            r12 = 0
            java.lang.Object r5 = r9.getKey()     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ 0Cz -> 0x04ee }
            r6.append(r5)     // Catch:{ 0Cz -> 0x04ee }
            r5 = 58
            r6.append(r5)     // Catch:{ 0Cz -> 0x04ee }
            java.lang.Object r5 = r9.getValue()     // Catch:{ 0Cz -> 0x04ee }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ 0Cz -> 0x04ee }
            r6.append(r5)     // Catch:{ 0Cz -> 0x04ee }
            goto L_0x04a1
        L_0x04cd:
            java.lang.String r5 = r6.toString()     // Catch:{ 0Cz -> 0x04ee }
            r12 = r13
            r8.ANO(r12, r5)     // Catch:{ 0Cz -> 0x04ee }
        L_0x04d5:
            r7.A07()     // Catch:{ 0Cz -> 0x04ee }
            goto L_0x0512
        L_0x04d9:
            r10.getAbsolutePath()     // Catch:{ 0Cz -> 0x04e0 }
            r8 = r33
            r11 = 0
            goto L_0x0513
        L_0x04e0:
            r9 = move-exception
            r8 = r33
            r11 = 0
            goto L_0x04f0
        L_0x04e5:
            r9 = move-exception
            r8 = r33
            goto L_0x04ef
        L_0x04e9:
            r9 = move-exception
            r8 = r33
            r7 = r8
            goto L_0x04ef
        L_0x04ee:
            r9 = move-exception
        L_0x04ef:
            r11 = 1
        L_0x04f0:
            if (r8 != 0) goto L_0x04f8
            X.0Jp r5 = r4.A01     // Catch:{ all -> 0x060f }
            X.0Jo r8 = r5.ALd()     // Catch:{ all -> 0x060f }
        L_0x04f8:
            java.io.StringWriter r6 = new java.io.StringWriter     // Catch:{ all -> 0x060f }
            r6.<init>()     // Catch:{ all -> 0x060f }
            java.io.PrintWriter r5 = new java.io.PrintWriter     // Catch:{ all -> 0x060f }
            r5.<init>(r6)     // Catch:{ all -> 0x060f }
            r9.printStackTrace(r5)     // Catch:{ all -> 0x060f }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x060f }
            java.lang.String r5 = "logParseError"
            r8.ANO(r5, r6)     // Catch:{ all -> 0x060f }
            X.AnonymousClass0NH.A01(r8, r10)     // Catch:{ all -> 0x060f }
            goto L_0x0513
        L_0x0512:
            r11 = 1
        L_0x0513:
            if (r11 == 0) goto L_0x051a
            if (r8 == 0) goto L_0x051a
            r8.BIw()     // Catch:{ all -> 0x060f }
        L_0x051a:
            if (r7 == 0) goto L_0x05d0
            if (r46 == 0) goto L_0x05d0
            if (r34 != 0) goto L_0x05d0
            char r6 = r7.A05     // Catch:{ all -> 0x060f }
            X.01k r5 = X.C002101k.A09     // Catch:{ all -> 0x060f }
            char r5 = r5.mSymbol     // Catch:{ all -> 0x060f }
            if (r6 == r5) goto L_0x0535
            X.01k r5 = X.C002101k.A0A     // Catch:{ all -> 0x060f }
            char r5 = r5.mSymbol     // Catch:{ all -> 0x060f }
            if (r6 == r5) goto L_0x0535
            X.01k r5 = X.C002101k.A0F     // Catch:{ all -> 0x060f }
            char r5 = r5.mSymbol     // Catch:{ all -> 0x060f }
            if (r6 == r5) goto L_0x0535
            goto L_0x053a
        L_0x0535:
            boolean r5 = r7.A08()     // Catch:{ all -> 0x060f }
            goto L_0x053b
        L_0x053a:
            r5 = 0
        L_0x053b:
            if (r5 == 0) goto L_0x05d0
            boolean r5 = r4.A0C()     // Catch:{ all -> 0x060f }
            if (r5 == 0) goto L_0x05cb
            X.0K5 r16 = new X.0K5     // Catch:{ all -> 0x060f }
            r5 = r16
            r5.<init>()     // Catch:{ all -> 0x060f }
            java.io.File r6 = new java.io.File     // Catch:{ all -> 0x060f }
            r5 = r86
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ all -> 0x060f }
            r9 = r45
            java.lang.String r5 = X.AnonymousClass08S.A0J(r5, r9)     // Catch:{ all -> 0x060f }
            r6.<init>(r5)     // Catch:{ all -> 0x060f }
            r5 = r42
            java.util.List r15 = X.AnonymousClass0K5.A00(r5)     // Catch:{ all -> 0x060f }
            java.util.List r14 = X.AnonymousClass0K5.A00(r6)     // Catch:{ all -> 0x060f }
            int r13 = r15.size()     // Catch:{ all -> 0x060f }
            int r9 = r14.size()     // Catch:{ all -> 0x060f }
            r8 = 0
            r6 = 0
        L_0x056f:
            if (r8 >= r13) goto L_0x05c2
            if (r6 >= r9) goto L_0x05c2
            java.lang.Object r5 = r14.get(r6)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x060f }
            java.lang.Object r11 = r15.get(r8)     // Catch:{ all -> 0x060f }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x060f }
            int r5 = r11.compareTo(r5)     // Catch:{ all -> 0x060f }
            if (r5 >= 0) goto L_0x058d
            r5 = r16
            java.util.List r5 = r5.A00     // Catch:{ all -> 0x060f }
            r5.add(r11)     // Catch:{ all -> 0x060f }
            goto L_0x05bc
        L_0x058d:
            if (r5 != 0) goto L_0x05bf
            int r5 = r8 + 1
            r45 = r15
            r46 = r5
            java.lang.Object r12 = r45.get(r46)     // Catch:{ all -> 0x060f }
            r5 = r12
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x060f }
            r12 = r5
            int r5 = r6 + 1
            r45 = r14
            r46 = r5
            java.lang.Object r5 = r45.get(r46)     // Catch:{ all -> 0x060f }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x060f }
            r45 = r12
            r46 = r5
            int r5 = r45.compareTo(r46)     // Catch:{ all -> 0x060f }
            if (r5 == 0) goto L_0x05ba
            r5 = r16
            java.util.List r5 = r5.A00     // Catch:{ all -> 0x060f }
            r5.add(r11)     // Catch:{ all -> 0x060f }
        L_0x05ba:
            int r6 = r6 + 2
        L_0x05bc:
            int r8 = r8 + 2
            goto L_0x056f
        L_0x05bf:
            int r6 = r6 + 2
            goto L_0x056f
        L_0x05c2:
            X.0K6 r5 = new X.0K6     // Catch:{ all -> 0x060f }
            r6 = r16
            r5.<init>(r6)     // Catch:{ all -> 0x060f }
            r7.A0I = r5     // Catch:{ all -> 0x060f }
        L_0x05cb:
            r4.A08(r7)     // Catch:{ all -> 0x060f }
            r34 = 1
        L_0x05d0:
            X.AnonymousClass0NH.A02(r4, r10)     // Catch:{ all -> 0x060f }
            if (r43 == 0) goto L_0x05da
            r6 = r43
            X.AnonymousClass0NH.A02(r4, r6)     // Catch:{ all -> 0x060f }
        L_0x05da:
            if (r42 == 0) goto L_0x05e1
            r6 = r42
            X.AnonymousClass0NH.A02(r4, r6)     // Catch:{ all -> 0x060f }
        L_0x05e1:
            if (r41 == 0) goto L_0x05e8
            r6 = r41
            X.AnonymousClass0NH.A02(r4, r6)     // Catch:{ all -> 0x060f }
        L_0x05e8:
            if (r40 == 0) goto L_0x05ef
            r6 = r40
            X.AnonymousClass0NH.A02(r4, r6)     // Catch:{ all -> 0x060f }
        L_0x05ef:
            if (r44 == 0) goto L_0x05f6
            r6 = r44
            X.AnonymousClass0NH.A02(r4, r6)     // Catch:{ all -> 0x060f }
        L_0x05f6:
            if (r39 == 0) goto L_0x05fd
            r6 = r39
            X.AnonymousClass0NH.A02(r4, r6)     // Catch:{ all -> 0x060f }
        L_0x05fd:
            if (r38 == 0) goto L_0x0604
            r6 = r38
            X.AnonymousClass0NH.A02(r4, r6)     // Catch:{ all -> 0x060f }
        L_0x0604:
            r5 = r37
            r5.close()     // Catch:{ all -> 0x0618 }
            r5 = r36
            r5.close()     // Catch:{ Exception -> 0x0621 }
            goto L_0x0637
        L_0x060f:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x0611 }
        L_0x0611:
            r5 = move-exception
            r6 = r37
            r6.close()     // Catch:{ all -> 0x0617 }
        L_0x0617:
            throw r5     // Catch:{ all -> 0x0618 }
        L_0x0618:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x061a }
        L_0x061a:
            r5 = move-exception
            r6 = r36
            r6.close()     // Catch:{ all -> 0x0620 }
        L_0x0620:
            throw r5     // Catch:{ Exception -> 0x0621 }
        L_0x0621:
            r8 = move-exception
            java.lang.String r7 = "AppStateReporter"
            java.lang.String r5 = r10.getAbsolutePath()     // Catch:{ all -> 0x06ab }
            java.lang.Object[] r6 = new java.lang.Object[]{r5}     // Catch:{ all -> 0x06ab }
            java.lang.String r5 = "Error reporting on app state log file: %s"
            X.C010708t.A0U(r7, r8, r5, r6)     // Catch:{ all -> 0x06ab }
            X.AnonymousClass0NH.A03(r4, r8, r10)     // Catch:{ all -> 0x06ab }
            X.AnonymousClass0NH.A02(r4, r10)     // Catch:{ all -> 0x06ab }
        L_0x0637:
            int r35 = r35 + 1
            goto L_0x0135
        L_0x063b:
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x06ab }
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x06ab }
            long r11 = r11 - r2
            long r11 = r11 / r17
            android.content.SharedPreferences r2 = r10.A00     // Catch:{ all -> 0x06ab }
            long r2 = r2.getLong(r7, r0)     // Catch:{ all -> 0x06ab }
            long r11 = r11 - r2
            long r11 = java.lang.Math.abs(r11)     // Catch:{ all -> 0x06ab }
            r7 = 2
            int r2 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r2 >= 0) goto L_0x065f
            android.content.SharedPreferences r2 = r10.A00     // Catch:{ all -> 0x06ab }
            long r2 = r2.getLong(r9, r0)     // Catch:{ all -> 0x06ab }
            goto L_0x011a
        L_0x065f:
            r2 = 0
            goto L_0x011a
        L_0x0663:
            java.lang.String r2 = "_native"
            r1 = r31
            r0 = r32
            X.AnonymousClass0NH.A04(r4, r1, r2, r0)     // Catch:{ all -> 0x06ab }
            r2 = r30
            r1 = r0
            r0 = r45
            X.AnonymousClass0NH.A04(r4, r2, r0, r1)     // Catch:{ all -> 0x06ab }
            java.lang.String r2 = "_anr"
            r1 = r29
            r0 = r32
            X.AnonymousClass0NH.A04(r4, r1, r2, r0)     // Catch:{ all -> 0x06ab }
            java.lang.String r2 = "_wrotedump"
            r1 = r28
            X.AnonymousClass0NH.A04(r4, r1, r2, r0)     // Catch:{ all -> 0x06ab }
            java.lang.String r2 = "_memorytimeline"
            r1 = r27
            X.AnonymousClass0NH.A04(r4, r1, r2, r0)     // Catch:{ all -> 0x06ab }
            java.lang.String r2 = "_static"
            r1 = r26
            X.AnonymousClass0NH.A04(r4, r1, r2, r0)     // Catch:{ all -> 0x06ab }
            java.lang.String r2 = "_entity"
            r1 = r25
            X.AnonymousClass0NH.A04(r4, r1, r2, r0)     // Catch:{ all -> 0x06ab }
            goto L_0x004a
        L_0x069b:
            return
        L_0x069c:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x06a8 }
            r0 = 119(0x77, float:1.67E-43)
            java.lang.String r0 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r0)     // Catch:{ all -> 0x06a8 }
            r1.<init>(r0)     // Catch:{ all -> 0x06a8 }
            throw r1     // Catch:{ all -> 0x06a8 }
        L_0x06a8:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x06a8 }
            throw r0     // Catch:{ all -> 0x06ab }
        L_0x06ab:
            r0 = move-exception
            monitor-exit(r22)     // Catch:{ all -> 0x06ab }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38751xw.run():void");
    }
}
