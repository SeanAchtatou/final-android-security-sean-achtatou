package X;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1v5  reason: invalid class name */
public final class AnonymousClass1v5 extends C17770zR {
    public AnonymousClass0UN A00;
    public AnonymousClass5XZ A01;
    @Comparable(type = 13)
    public C73513gF A02;
    @Comparable(type = 13)
    public StoryCard A03;
    @Comparable(type = 13)
    public C88334Jw A04;
    @Comparable(type = 14)
    public AnonymousClass5CS A05 = new AnonymousClass5CS();
    @Comparable(type = 13)
    public C88584Ky A06;
    @Comparable(type = 13)
    public AnonymousClass46A A07;
    @Comparable(type = 13)
    public AnonymousClass463 A08;

    public int A0M() {
        return 3;
    }

    static {
        CallerContext.A09("FeelingsStickerOverlayComponentSpec");
    }

    public AnonymousClass1v5(Context context) {
        super("FeelingsStickerOverlayComponent");
        this.A00 = new AnonymousClass0UN(6, AnonymousClass1XX.get(context));
    }

    public static AnimatorSet A00(View view, boolean z) {
        AnimatorSet animatorSet = new AnimatorSet();
        float[] fArr = new float[1];
        float f = 1.2f;
        float f2 = 1.05f;
        if (z) {
            f2 = 1.2f;
        }
        fArr[0] = f2;
        ObjectAnimator duration = ObjectAnimator.ofFloat(view, "scaleX", fArr).setDuration(250L);
        float[] fArr2 = new float[1];
        if (!z) {
            f = 1.05f;
        }
        fArr2[0] = f;
        ObjectAnimator duration2 = ObjectAnimator.ofFloat(view, "scaleY", fArr2).setDuration(250L);
        ObjectAnimator duration3 = ObjectAnimator.ofFloat(view, "scaleX", 1.0f).setDuration(250L);
        ObjectAnimator duration4 = ObjectAnimator.ofFloat(view, "scaleY", 1.0f).setDuration(250L);
        animatorSet.play(duration).with(duration2);
        animatorSet.play(duration).before(duration3);
        animatorSet.play(duration3).with(duration4);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSet.setStartDelay(250);
        return animatorSet;
    }

    public C17770zR A16() {
        AnonymousClass1v5 r1 = (AnonymousClass1v5) super.A16();
        r1.A01 = null;
        r1.A05 = new AnonymousClass5CS();
        return r1;
    }
}
