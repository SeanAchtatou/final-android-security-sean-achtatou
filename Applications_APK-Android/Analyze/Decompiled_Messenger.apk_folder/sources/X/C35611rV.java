package X;

import com.facebook.inject.InjectorModule;
import java.util.concurrent.TimeUnit;

@InjectorModule
/* renamed from: X.1rV  reason: invalid class name and case insensitive filesystem */
public final class C35611rV extends AnonymousClass0UV {
    public static final long A00 = TimeUnit.DAYS.toSeconds(60);

    public static final AnonymousClass1LS A00(AnonymousClass1XY r0) {
        return C35641rY.A00(r0);
    }
}
