package X;

import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Br  reason: invalid class name and case insensitive filesystem */
public final class C20251Br implements AnonymousClass1LY {
    private static volatile C20251Br A04;
    public String A00;
    public String A01;
    public Map A02;
    private C32561lx A03;

    public static final C20251Br A00(AnonymousClass1XY r4) {
        if (A04 == null) {
            synchronized (C20251Br.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A04, r4);
                if (A002 != null) {
                    try {
                        A04 = new C20251Br(C32561lx.A00(r4.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A04;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        if (r1 == false) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void BgD(java.lang.String r7, java.lang.String r8, java.util.Map r9) {
        /*
            r6 = this;
            if (r7 != 0) goto L_0x0005
            if (r8 != 0) goto L_0x0005
            return
        L_0x0005:
            java.lang.String r5 = "not inspected"
            java.lang.String r2 = "click_point"
            if (r9 == 0) goto L_0x0071
            java.lang.Object r0 = r9.get(r2)
            if (r0 == 0) goto L_0x0071
            java.lang.Object r0 = r9.get(r2)
            java.lang.String r4 = r0.toString()
        L_0x0019:
            if (r9 == 0) goto L_0x006f
            java.lang.String r1 = "thread_key"
            java.lang.Object r0 = r9.get(r1)
            if (r0 == 0) goto L_0x006f
            java.lang.Object r0 = r9.get(r1)
            java.lang.String r3 = r0.toString()
        L_0x002b:
            java.lang.String r0 = r6.A00
            if (r0 == 0) goto L_0x0073
            if (r7 == 0) goto L_0x003a
            if (r0 == 0) goto L_0x003a
            boolean r1 = r7.equals(r0)
            r0 = 1
            if (r1 != 0) goto L_0x003b
        L_0x003a:
            r0 = 0
        L_0x003b:
            if (r0 == 0) goto L_0x004c
            X.1lx r1 = r6.A03
            java.lang.String r0 = r6.A01
            r1.A02(r0, r8, r4, r3)
        L_0x0044:
            r0 = 0
            r6.A00 = r0
            r6.A01 = r0
            r6.A02 = r0
            return
        L_0x004c:
            java.util.Map r0 = r6.A02
            if (r0 == 0) goto L_0x0060
            java.lang.Object r0 = r0.get(r2)
            if (r0 == 0) goto L_0x0060
            java.util.Map r0 = r6.A02
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r5 = r0.toString()
        L_0x0060:
            X.1lx r2 = r6.A03
            java.lang.String r1 = r6.A01
            java.lang.String r0 = r6.A00
            r2.A02(r1, r0, r5, r3)
            X.1lx r0 = r6.A03
            r0.A02(r7, r8, r4, r3)
            goto L_0x0044
        L_0x006f:
            r3 = 0
            goto L_0x002b
        L_0x0071:
            r4 = r5
            goto L_0x0019
        L_0x0073:
            if (r8 == 0) goto L_0x0084
            java.lang.String r0 = "module"
            boolean r0 = r8.endsWith(r0)
            if (r0 == 0) goto L_0x0084
            r6.A01 = r7
            r6.A00 = r8
            r6.A02 = r9
            return
        L_0x0084:
            if (r8 != 0) goto L_0x008f
            java.lang.String r0 = "neue_nux"
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x008f
            return
        L_0x008f:
            X.1lx r0 = r6.A03
            r0.A02(r7, r8, r4, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20251Br.BgD(java.lang.String, java.lang.String, java.util.Map):void");
    }

    private C20251Br(C32561lx r1) {
        this.A03 = r1;
    }
}
