package X;

import com.google.firebase.iid.Registrar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: X.1WT  reason: invalid class name */
public final class AnonymousClass1WT extends AnonymousClass1WU {
    public static final AnonymousClass1WW A04 = AnonymousClass1WV.A00;
    public final AnonymousClass1Wh A00;
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();
    public final Map A03 = new HashMap();

    public AnonymousClass1WT(Executor executor, Iterable iterable, AnonymousClass1WX... r15) {
        AnonymousClass1Wh r2 = new AnonymousClass1Wh(executor);
        this.A00 = r2;
        ArrayList<AnonymousClass1WX> arrayList = new ArrayList<>();
        Class<AnonymousClass1Wh> cls = AnonymousClass1Wh.class;
        arrayList.add(AnonymousClass1WX.A00(r2, cls, cls, AnonymousClass1Wi.class));
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            arrayList.addAll(((Registrar) it.next()).getComponents());
        }
        Collections.addAll(arrayList, r15);
        HashMap hashMap = new HashMap(arrayList.size());
        for (AnonymousClass1WX r8 : arrayList) {
            C24631Wm r4 = new C24631Wm(r8);
            Iterator it2 = r8.A04.iterator();
            while (true) {
                if (it2.hasNext()) {
                    Class cls2 = (Class) it2.next();
                    C24641Wn r22 = new C24641Wn(cls2, !(r8.A01 == 0));
                    if (!hashMap.containsKey(r22)) {
                        hashMap.put(r22, new HashSet());
                    }
                    Set set = (Set) hashMap.get(r22);
                    if (set.isEmpty() || r22.A00) {
                        set.add(r4);
                    } else {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", cls2));
                    }
                }
            }
        }
        for (Set<C24631Wm> it3 : hashMap.values()) {
            for (C24631Wm r82 : it3) {
                for (C24601Wf r23 : r82.A00.A03) {
                    if (r23.A00 == 0) {
                        Set<C24631Wm> set2 = (Set) hashMap.get(new C24641Wn(r23.A02, r23.A01 == 2));
                        if (set2 != null) {
                            for (C24631Wm r1 : set2) {
                                r82.A01.add(r1);
                                r1.A02.add(r82);
                            }
                        }
                    }
                }
            }
        }
        HashSet<C24631Wm> hashSet = new HashSet<>();
        for (Set addAll : hashMap.values()) {
            hashSet.addAll(addAll);
        }
        HashSet hashSet2 = new HashSet();
        for (C24631Wm r12 : hashSet) {
            if (r12.A02.isEmpty()) {
                hashSet2.add(r12);
            }
        }
        int i = 0;
        while (!hashSet2.isEmpty()) {
            C24631Wm r3 = (C24631Wm) hashSet2.iterator().next();
            hashSet2.remove(r3);
            i++;
            for (C24631Wm r13 : r3.A01) {
                r13.A02.remove(r3);
                if (r13.A02.isEmpty()) {
                    hashSet2.add(r13);
                }
            }
        }
        if (i == arrayList.size()) {
            for (AnonymousClass1WX r32 : arrayList) {
                this.A01.put(r32, new C24651Wo(new C24661Wp(this, r32)));
            }
            for (Map.Entry entry : this.A01.entrySet()) {
                AnonymousClass1WX r24 = (AnonymousClass1WX) entry.getKey();
                if (r24.A01 == 0) {
                    C24651Wo r33 = (C24651Wo) entry.getValue();
                    for (Class put : r24.A04) {
                        this.A02.put(put, r33);
                    }
                }
            }
            for (AnonymousClass1WX r42 : this.A01.keySet()) {
                Iterator it4 = r42.A03.iterator();
                while (true) {
                    if (it4.hasNext()) {
                        C24601Wf r34 = (C24601Wf) it4.next();
                        if ((r34.A01 != 1 ? false : true) && !this.A02.containsKey(r34.A02)) {
                            throw new AnonymousClass38Q(String.format("Unsatisfied dependency for component %s: %s", r42, r34.A02));
                        }
                    }
                }
            }
            HashMap hashMap2 = new HashMap();
            for (Map.Entry entry2 : this.A01.entrySet()) {
                AnonymousClass1WX r25 = (AnonymousClass1WX) entry2.getKey();
                if (!(r25.A01 == 0)) {
                    C24651Wo r35 = (C24651Wo) entry2.getValue();
                    for (Class cls3 : r25.A04) {
                        if (!hashMap2.containsKey(cls3)) {
                            hashMap2.put(cls3, new HashSet());
                        }
                        ((Set) hashMap2.get(cls3)).add(r35);
                    }
                }
            }
            for (Map.Entry entry3 : hashMap2.entrySet()) {
                this.A03.put((Class) entry3.getKey(), new C24651Wo(new C24671Wq((Set) entry3.getValue())));
            }
            return;
        }
        ArrayList arrayList2 = new ArrayList();
        for (C24631Wm r14 : hashSet) {
            if (!r14.A02.isEmpty() && !r14.A01.isEmpty()) {
                arrayList2.add(r14.A00);
            }
        }
        throw new AnonymousClass38O(arrayList2);
    }
}
