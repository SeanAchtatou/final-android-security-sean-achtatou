package X;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1ct  reason: invalid class name and case insensitive filesystem */
public final class C27251ct {
    private static volatile C27251ct A03;
    public AnonymousClass0UN A00;
    public final Map A01 = new HashMap();
    public final boolean A02;

    public static final C27251ct A00(AnonymousClass1XY r4) {
        if (A03 == null) {
            synchronized (C27251ct.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A03, r4);
                if (A002 != null) {
                    try {
                        A03 = new C27251ct(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A03;
    }

    private C27251ct(AnonymousClass1XY r4) {
        AnonymousClass0UN r2 = new AnonymousClass0UN(4, r4);
        this.A00 = r2;
        this.A02 = ((C25051Yd) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AOJ, r2)).Aem(282909496248317L);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0066, code lost:
        if (r1 == false) goto L_0x0068;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(android.content.Intent r7) {
        /*
            r6 = this;
            android.content.ComponentName r0 = r7.getComponent()
            r2 = 0
            r1 = 0
            if (r0 == 0) goto L_0x0009
            r1 = 1
        L_0x0009:
            java.lang.String r0 = "Bind intent must specify a component"
            com.google.common.base.Preconditions.checkState(r1, r0)
            int r1 = X.AnonymousClass1Y3.A6S
            X.0UN r0 = r6.A00
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            android.content.Context r0 = (android.content.Context) r0
            java.lang.String r1 = r0.getPackageName()
            android.content.ComponentName r0 = r7.getComponent()
            java.lang.String r0 = r0.getPackageName()
            boolean r1 = r1.equals(r0)
            java.lang.String r0 = "We can currently only bind to a BlueService that is part of our package."
            com.google.common.base.Preconditions.checkState(r1, r0)
            java.util.Map r4 = r6.A01
            monitor-enter(r4)
            java.util.Map r1 = r6.A01     // Catch:{ all -> 0x00bd }
            android.content.ComponentName r0 = r7.getComponent()     // Catch:{ all -> 0x00bd }
            java.lang.Object r5 = r1.get(r0)     // Catch:{ all -> 0x00bd }
            java.lang.Boolean r5 = (java.lang.Boolean) r5     // Catch:{ all -> 0x00bd }
            r3 = 0
            r2 = 1
            if (r5 != 0) goto L_0x007b
            int r1 = X.AnonymousClass1Y3.A6m     // Catch:{ all -> 0x00bd }
            X.0UN r0 = r6.A00     // Catch:{ all -> 0x00bd }
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)     // Catch:{ all -> 0x00bd }
            android.content.pm.PackageManager r0 = (android.content.pm.PackageManager) r0     // Catch:{ all -> 0x00bd }
            android.content.pm.ResolveInfo r2 = r0.resolveService(r7, r3)     // Catch:{ all -> 0x00bd }
            X.00M r0 = X.AnonymousClass00M.A00()     // Catch:{ all -> 0x00bd }
            java.lang.String r1 = r0.A01     // Catch:{ all -> 0x00bd }
            if (r2 != 0) goto L_0x005b
            X.00f r0 = X.C000100f.A01     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = r0.A00     // Catch:{ all -> 0x00bd }
            goto L_0x005f
        L_0x005b:
            android.content.pm.ServiceInfo r0 = r2.serviceInfo     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = r0.processName     // Catch:{ all -> 0x00bd }
        L_0x005f:
            if (r1 == 0) goto L_0x0068
            boolean r1 = r1.equals(r0)     // Catch:{ all -> 0x00bd }
            r0 = 1
            if (r1 != 0) goto L_0x0069
        L_0x0068:
            r0 = 0
        L_0x0069:
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x00bd }
            android.content.ComponentName r1 = r7.getComponent()     // Catch:{ all -> 0x00bd }
            java.util.Map r0 = r6.A01     // Catch:{ all -> 0x00bd }
            r0.put(r1, r5)     // Catch:{ all -> 0x00bd }
            if (r1 != 0) goto L_0x007d
            java.lang.String r3 = "<no name>"
            goto L_0x0081
        L_0x007b:
            r3 = 0
            goto L_0x0081
        L_0x007d:
            java.lang.String r3 = r1.flattenToString()     // Catch:{ all -> 0x00bd }
        L_0x0081:
            monitor-exit(r4)     // Catch:{ all -> 0x00bd }
            boolean r0 = r5.booleanValue()
            if (r0 == 0) goto L_0x008a
            r0 = 0
            return r0
        L_0x008a:
            if (r3 == 0) goto L_0x00bb
            boolean r0 = r6.A02
            if (r0 == 0) goto L_0x00bb
            r2 = 3
            int r1 = X.AnonymousClass1Y3.AOJ
            X.0UN r0 = r6.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 282909497165833(0x1014e00150809, double:1.397758634318596E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x00bb
            int r1 = X.AnonymousClass1Y3.Amr
            X.0UN r0 = r6.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A03(r1, r0)
            X.09P r2 = (X.AnonymousClass09P) r2
            java.lang.String r0 = "BlueService used out of process: "
            java.lang.String r1 = X.AnonymousClass08S.A0J(r0, r3)
            java.lang.String r0 = "BlueService out of process"
            r2.CGS(r0, r1)
        L_0x00bb:
            r0 = 1
            return r0
        L_0x00bd:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00bd }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27251ct.A01(android.content.Intent):boolean");
    }
}
