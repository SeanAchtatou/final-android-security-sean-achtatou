package X;

import android.content.Context;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0Dq  reason: invalid class name and case insensitive filesystem */
public final class C02220Dq {
    private static volatile C02220Dq A02;
    public final C012309k A00;
    private final C04460Ut A01;

    public static final C02220Dq A01(AnonymousClass1XY r5) {
        if (A02 == null) {
            synchronized (C02220Dq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A02 = new C02220Dq(applicationInjector, AnonymousClass1YA.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(java.lang.Runnable r5) {
        /*
            r4 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 14
            if (r1 < r0) goto L_0x0016
            java.lang.String r3 = "android.intent.action.PACKAGE_FULLY_REMOVED"
        L_0x0008:
            android.content.IntentFilter r2 = new android.content.IntentFilter
            r2.<init>()
            java.lang.String r0 = "package"
            r2.addDataScheme(r0)
            r2.addAction(r3)
            goto L_0x0019
        L_0x0016:
            java.lang.String r3 = "android.intent.action.PACKAGE_REMOVED"
            goto L_0x0008
        L_0x0019:
            X.0Ut r0 = r4.A01     // Catch:{ SecurityException -> 0x0036 }
            X.0bl r1 = r0.BMm()     // Catch:{ SecurityException -> 0x0036 }
            X.0Dr r0 = new X.0Dr     // Catch:{ SecurityException -> 0x0036 }
            r0.<init>(r4, r5)     // Catch:{ SecurityException -> 0x0036 }
            r1.A02(r3, r0)     // Catch:{ SecurityException -> 0x0036 }
            monitor-enter(r1)     // Catch:{ SecurityException -> 0x0036 }
            r1.A00 = r2     // Catch:{ all -> 0x0033 }
            monitor-exit(r1)     // Catch:{ SecurityException -> 0x0036 }
            X.0c5 r0 = r1.A00()     // Catch:{ SecurityException -> 0x0036 }
            r0.A00()     // Catch:{ SecurityException -> 0x0036 }
            return
        L_0x0033:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ SecurityException -> 0x0036 }
            throw r0     // Catch:{ SecurityException -> 0x0036 }
        L_0x0036:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02220Dq.A02(java.lang.Runnable):void");
    }

    private C02220Dq(AnonymousClass1XY r3, Context context) {
        this.A01 = C07510dg.A00(r3);
        this.A00 = new C012309k(context, C04750Wa.A01(r3));
    }

    public static final C02220Dq A00(AnonymousClass1XY r0) {
        return A01(r0);
    }
}
