package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.0V3  reason: invalid class name */
public final class AnonymousClass0V3 extends Handler {
    public final /* synthetic */ AnonymousClass0V2 A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0V3(AnonymousClass0V2 r1, Looper looper) {
        super(looper);
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        if (r6 >= r8) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0028, code lost:
        r5 = r7[r6];
        r4 = r5.A01.size();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        if (r3 >= r4) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0033, code lost:
        r1 = (X.C06580bj) r5.A01.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003d, code lost:
        if (r1.A01 != false) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
        r1.A02.onReceive(r9.A00, r5.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0048, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004b, code lost:
        r6 = r6 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r11) {
        /*
            r10 = this;
            int r1 = r11.what
            r0 = 1
            if (r1 == r0) goto L_0x0009
            super.handleMessage(r11)
            return
        L_0x0009:
            X.0V2 r9 = r10.A00
        L_0x000b:
            java.util.HashMap r1 = r9.A04
            monitor-enter(r1)
            java.util.ArrayList r0 = r9.A02     // Catch:{ all -> 0x004f }
            int r8 = r0.size()     // Catch:{ all -> 0x004f }
            if (r8 > 0) goto L_0x0018
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            goto L_0x004e
        L_0x0018:
            X.1Fi[] r7 = new X.C21131Fi[r8]     // Catch:{ all -> 0x004f }
            java.util.ArrayList r0 = r9.A02     // Catch:{ all -> 0x004f }
            r0.toArray(r7)     // Catch:{ all -> 0x004f }
            java.util.ArrayList r0 = r9.A02     // Catch:{ all -> 0x004f }
            r0.clear()     // Catch:{ all -> 0x004f }
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            r6 = 0
        L_0x0026:
            if (r6 >= r8) goto L_0x000b
            r5 = r7[r6]
            java.util.ArrayList r0 = r5.A01
            int r4 = r0.size()
            r3 = 0
        L_0x0031:
            if (r3 >= r4) goto L_0x004b
            java.util.ArrayList r0 = r5.A01
            java.lang.Object r1 = r0.get(r3)
            X.0bj r1 = (X.C06580bj) r1
            boolean r0 = r1.A01
            if (r0 != 0) goto L_0x0048
            android.content.BroadcastReceiver r2 = r1.A02
            android.content.Context r1 = r9.A00
            android.content.Intent r0 = r5.A00
            r2.onReceive(r1, r0)
        L_0x0048:
            int r3 = r3 + 1
            goto L_0x0031
        L_0x004b:
            int r6 = r6 + 1
            goto L_0x0026
        L_0x004e:
            return
        L_0x004f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0V3.handleMessage(android.os.Message):void");
    }
}
