package X;

import android.content.Context;
import android.os.Build;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1hD  reason: invalid class name and case insensitive filesystem */
public final class C29931hD extends AnonymousClass0UV {
    private static volatile A98 A00;
    private static volatile C29961hG A01;
    private static volatile C29951hF A02;

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0036, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r3.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003a, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.A98 A00(X.AnonymousClass1XY r5) {
        /*
            X.A98 r0 = X.C29931hD.A00
            if (r0 != 0) goto L_0x0043
            java.lang.Class<X.A98> r4 = X.A98.class
            monitor-enter(r4)
            X.A98 r0 = X.C29931hD.A00     // Catch:{ all -> 0x0040 }
            X.0WD r3 = X.AnonymousClass0WD.A00(r0, r5)     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x003e
            X.1XY r0 = r5.getApplicationInjector()     // Catch:{ all -> 0x0036 }
            android.content.Context r2 = X.AnonymousClass1YA.A00(r0)     // Catch:{ all -> 0x0036 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0036 }
            r0 = 29
            if (r1 >= r0) goto L_0x001f
            r0 = 0
            goto L_0x0030
        L_0x001f:
            java.lang.Class<X.A98> r1 = X.A98.class
            monitor-enter(r1)     // Catch:{ all -> 0x0036 }
            X.A98 r0 = X.A98.A01     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x002d
            X.A98 r0 = new X.A98     // Catch:{ all -> 0x0033 }
            r0.<init>(r2)     // Catch:{ all -> 0x0033 }
            X.A98.A01 = r0     // Catch:{ all -> 0x0033 }
        L_0x002d:
            X.A98 r0 = X.A98.A01     // Catch:{ all -> 0x0033 }
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
        L_0x0030:
            X.C29931hD.A00 = r0     // Catch:{ all -> 0x0036 }
            goto L_0x003b
        L_0x0033:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0036 }
            throw r0     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            r3.A01()     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x003b:
            r3.A01()     // Catch:{ all -> 0x0040 }
        L_0x003e:
            monitor-exit(r4)     // Catch:{ all -> 0x0040 }
            goto L_0x0043
        L_0x0040:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0040 }
            throw r0
        L_0x0043:
            X.A98 r0 = X.C29931hD.A00
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29931hD.A00(X.1XY):X.A98");
    }

    public static final C29961hG A01(AnonymousClass1XY r5) {
        C29961hG r0;
        if (A01 == null) {
            synchronized (C29961hG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r5);
                if (A002 != null) {
                    try {
                        Context A003 = AnonymousClass1YA.A00(r5.getApplicationInjector());
                        if (Build.VERSION.SDK_INT < 29) {
                            r0 = null;
                        } else {
                            r0 = C29961hG.A00(A003);
                        }
                        A01 = r0;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static final C29951hF A02(AnonymousClass1XY r6) {
        C29951hF r0;
        if (A02 == null) {
            synchronized (C29951hF.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r6);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r6.getApplicationInjector();
                        AnonymousClass1ZE A003 = AnonymousClass1ZD.A00(applicationInjector);
                        C29961hG A012 = A01(applicationInjector);
                        AnonymousClass0Ud A004 = AnonymousClass0Ud.A00(applicationInjector);
                        if (A012 == null) {
                            r0 = null;
                        } else {
                            r0 = new C29951hF(A003, A012, A004);
                        }
                        A02 = r0;
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }
}
