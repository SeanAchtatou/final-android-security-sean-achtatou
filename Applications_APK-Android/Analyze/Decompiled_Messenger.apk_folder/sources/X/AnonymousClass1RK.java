package X;

import java.util.List;

/* renamed from: X.1RK  reason: invalid class name */
public final class AnonymousClass1RK extends AnonymousClass1ID {
    public final AnonymousClass10N A00;
    public final AnonymousClass10N A01;
    public final List A02;
    public final List A03;
    private final AnonymousClass1GA A04;

    public AnonymousClass1RK(AnonymousClass1GA r3, List list, List list2) {
        AnonymousClass10N r0;
        AnonymousClass10N r02;
        this.A04 = r3;
        if (r3.A0K() == null) {
            r0 = null;
        } else {
            r0 = ((AnonymousClass1AM) r3.A0K()).A01;
        }
        this.A01 = r0;
        AnonymousClass1GA r1 = this.A04;
        if (r1.A0K() == null) {
            r02 = null;
        } else {
            r02 = ((AnonymousClass1AM) r1.A0K()).A00;
        }
        this.A00 = r02;
        this.A03 = list;
        this.A02 = list2;
    }
}
