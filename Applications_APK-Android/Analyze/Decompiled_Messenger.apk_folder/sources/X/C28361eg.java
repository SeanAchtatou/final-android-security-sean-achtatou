package X;

import com.facebook.common.dextricks.DexStore;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.RegularImmutableSet;
import io.card.payment.BuildConfig;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1eg  reason: invalid class name and case insensitive filesystem */
public final class C28361eg {
    public static final Set A00;

    static {
        String[] strArr = new String[151];
        System.arraycopy(new String[]{BuildConfig.FLAVOR, "promo_banner", "zero_state", "url_interstitial", AnonymousClass24B.$const$string(DexStore.LOAD_RESULT_OATMEAL_QUICKENED), C99084oO.$const$string(89), AnonymousClass80H.$const$string(AnonymousClass1Y3.A4P), AnonymousClass80H.$const$string(645), "voip_incoming_interstitial", AnonymousClass24B.$const$string(1), "native_optin_interstitial", "native_url_interstitial", "ussd_upsell", "dialog_when_leaving_app", "checkin_interstitial", "buy_confirm_interstitial", "optin_group_interstitial", "upsell_use_data_or_stay_free_screen", "optout_upgrade_dialog_interstitial", AnonymousClass24B.$const$string(32), AnonymousClass24B.$const$string(0), AnonymousClass24B.$const$string(91), AnonymousClass24B.$const$string(AnonymousClass1Y3.A2e), C99084oO.$const$string(20), AnonymousClass24B.$const$string(1458), AnonymousClass24B.$const$string(974), AnonymousClass24B.$const$string(AnonymousClass1Y3.A1m)}, 0, strArr, 0, 27);
        System.arraycopy(new String[]{AnonymousClass24B.$const$string(AnonymousClass1Y3.ABD), AnonymousClass24B.$const$string(1365), AnonymousClass24B.$const$string(1366), "go_live_video_interstitial", "zero_rated_interstitial", AnonymousClass24B.$const$string(321), "dialtone_optin", "photo_dialtone", AnonymousClass80H.$const$string(AnonymousClass1Y3.A3n), AnonymousClass80H.$const$string(68), "dialtone_sticky_mode", "dialtone_toggle_nux", "dialtone_switch_megaphone", "dialtone_switcher_zero_balance_reminder", "dialtone_optout_reminder", "switch_to_dialtone_mode", "dialtone_toggle_bookmark", "flex_plus", "video_screencap", "data_saving_mode", "dialtone_photo_interstitial", "dialtone_video_interstitial", "dialtone_toggle_interstitial", "dialtone_show_interstitial_with_cap", "instant_article_setting", AnonymousClass80H.$const$string(AnonymousClass1Y3.A5G), "loyalty_topup"}, 0, strArr, 27, 27);
        System.arraycopy(new String[]{AnonymousClass80H.$const$string(AnonymousClass1Y3.A3o), "dialtone_sticky_on_login", "video_preview", "video_preview_test_qe", "video_preview_for_flex_plus", "paid_video_preview", "video_whitelist", AnonymousClass80H.$const$string(485), "messenger_zero_balance_detection", "zero_messenger_block_prefetch", "free_messenger_admin_message", "free_messenger_gif_interstitial", "free_messenger_nux", "messenger_photo_size_limit", C99084oO.$const$string(5), "free_messenger_sending_free_tooltip", AnonymousClass80H.$const$string(62), AnonymousClass80H.$const$string(484), "free_messenger_gif_placeholder", "free_messenger_video_placeholder", "free_messenger_paid_photo", "free_messenger_open_camera_interstitial", "free_messenger_send_video_interstitial", "auto_free_messenger_snack_bar", AnonymousClass80H.$const$string(73), AnonymousClass80H.$const$string(AnonymousClass1Y3.A1V), AnonymousClass80H.$const$string(AnonymousClass1Y3.A1U)}, 0, strArr, 54, 27);
        System.arraycopy(new String[]{"semi_free_messenger_admin_text", C99084oO.$const$string(220), "semi_free_messenger_send_gifs_interstitial", "semi_free_messenger_open_camera_interstitial", AnonymousClass24B.$const$string(1201), AnonymousClass24B.$const$string(AnonymousClass1Y3.A9m), "messenger_dialtone_my_day_interstitial", "messenger_dialtone_location_interstitial", "messenger_dialtone_gif_interstitial", "messenger_dialtone_link_interstitial", "messenger_dialtone_sticker_interstitial", "messenger_dialtone_voip_call_interstitial", "messenger_enable_dialtone_on_cold_start", AnonymousClass80H.$const$string(634), AnonymousClass80H.$const$string(198), "open_carrier_portal_from_placeholder", "header_ping_before_fetch_upsell", "free_data_persistent_notification", "iorg_external_url", "fbs_open_platform", "fbs_system_notifications", "fbs_content-notifications", "zero_balance_detection", "zero_balance_background_detection", "zero_balance_auto_switch", "autoflex_placeholder", AnonymousClass80H.$const$string(651)}, 0, strArr, 81, 27);
        System.arraycopy(new String[]{"autoflex_settings_bookmark", ECX.$const$string(33), "carrier_signal_detection", "autoflex_enable_upsell", "autoflex_free_to_paid_interstitial", AnonymousClass80H.$const$string(AnonymousClass1Y3.A0y), "autoflex_paid_to_free_interstitial", "autoflex_optin_test_one", "autoflex_optin_test_two", "autoflex_optin_test_three", "autoflex_optin_test_four", "autoflex_upgrade_disable_detection", "autoflex_optin_tos_nt", AnonymousClass80H.$const$string(405), "new_res_expiration_notice", "paid_balance_detection", "pbd_auto_mode", "auto_new_res_eligible", AnonymousClass24B.$const$string(AnonymousClass1Y3.A8Y), "free_data_policy_sensitive_carrier", "messenger_hide_ads", AnonymousClass24B.$const$string(AnonymousClass1Y3.ACP), "zero_camera_stories_block_prefetch", "stories_interstitial", "block_camera_effect", "zero_whitelist_story_rectangular_thumbnail", "block_rtc_live"}, 0, strArr, 108, 27);
        System.arraycopy(new String[]{C22298Ase.$const$string(81), "zero_gql_rewrite_whitelist", AnonymousClass80H.$const$string(AnonymousClass1Y3.A5H), "zero_fup_interstitial", "disable_fb4a_free_upgrade", AnonymousClass24B.$const$string(6), AnonymousClass80H.$const$string(547), "advanced_upsell_for_all_show_again", "native_template_enable_cache", AnonymousClass24B.$const$string(230), "zero_carrier_page", "zero_carrier_page_upsell_dialog", "zero_carrier_page_reconsider_dialog", "zero_carrier_page_by_intent_mapper", "zero_carrier_page_by_screen_resolver", "zero_carrier_page_hide_banner"}, 0, strArr, AnonymousClass1Y3.A11, 16);
        A00 = new HashSet(Arrays.asList(strArr));
    }

    public static ImmutableSet A00(Iterable iterable) {
        if (iterable == null) {
            return RegularImmutableSet.A05;
        }
        HashSet hashSet = new HashSet();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            hashSet.add(A01((String) it.next()));
        }
        return ImmutableSet.A0A(hashSet);
    }

    public static String A01(String str) {
        if (str == null || !A00.contains(str)) {
            return BuildConfig.FLAVOR;
        }
        return str;
    }
}
