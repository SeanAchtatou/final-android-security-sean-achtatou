package X;

import android.content.Context;
import java.io.IOException;

/* renamed from: X.0Km  reason: invalid class name and case insensitive filesystem */
public final class C03210Km {
    public static final C03210Km A05 = new C03210Km();
    public Context A00;
    public C02730Gc A01;
    public C03190Kj A02;
    public Thread A03 = new AnonymousClass0Kk(this, "BatteryMetricsLogger");
    public boolean A04 = false;

    public synchronized void A00() {
        C02760Gg r5;
        C03190Kj r0;
        C02760Gg A022;
        if (this.A04 && (r5 = (C02760Gg) this.A01.A01()) != null) {
            try {
                this.A02.A04();
                try {
                    A022 = this.A02.A02();
                } catch (IOException e) {
                    AnonymousClass0KZ.A00("BatteryMetricsLogger", AnonymousClass08S.A0J("Unable to read existing logs for ", this.A02.A03()), e);
                }
                if (A022 != null) {
                    r5.A0B(A022, r5);
                }
                this.A02.A06(r5);
                r0 = this.A02;
            } catch (IOException e2) {
                AnonymousClass0KZ.A00("BatteryMetricsLogger", AnonymousClass08S.A0J("Unable to update log file for ", this.A02.A03()), e2);
                r0 = this.A02;
            } catch (Throwable th) {
                this.A02.A05();
                throw th;
            }
            r0.A05();
        }
    }

    private C03210Km() {
    }
}
