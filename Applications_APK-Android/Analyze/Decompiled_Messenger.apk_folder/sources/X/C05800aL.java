package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.0aL  reason: invalid class name and case insensitive filesystem */
public final class C05800aL extends C25941ae {
    private static volatile C05800aL A00;

    private C05800aL(AnonymousClass0US r3) {
        super(r3, C26211b5.A06, AnonymousClass07B.A00);
    }

    public static final C05800aL A00(AnonymousClass1XY r5) {
        if (A00 == null) {
            synchronized (C05800aL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A00, r5);
                if (A002 != null) {
                    try {
                        A00 = new C05800aL(AnonymousClass0VB.A00(AnonymousClass1Y3.AUr, r5.getApplicationInjector()));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A00;
    }
}
