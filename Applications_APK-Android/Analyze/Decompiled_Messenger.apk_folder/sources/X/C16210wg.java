package X;

/* renamed from: X.0wg  reason: invalid class name and case insensitive filesystem */
public final class C16210wg implements Runnable {
    public static final String __redex_internal_original_name = "androidx.lifecycle.LiveData$1";
    public final /* synthetic */ C29451gR A00;

    public C16210wg(C29451gR r1) {
        this.A00 = r1;
    }

    public void run() {
        Object obj;
        synchronized (this.A00.A05) {
            obj = this.A00.A07;
            this.A00.A07 = C29451gR.A09;
        }
        this.A00.A09(obj);
    }
}
