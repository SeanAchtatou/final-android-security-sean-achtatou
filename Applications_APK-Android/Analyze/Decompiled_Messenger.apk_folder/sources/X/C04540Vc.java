package X;

import com.google.common.base.Preconditions;

/* renamed from: X.0Vc  reason: invalid class name and case insensitive filesystem */
public final class C04540Vc {
    public int A00;
    public final int A01;

    public void A00() {
        int i = this.A00;
        boolean z = false;
        if (i < this.A01) {
            z = true;
        }
        Preconditions.checkState(z);
        this.A00 = i + 1;
    }

    public C04540Vc(int i) {
        Preconditions.checkArgument(i > 0);
        this.A01 = i;
    }
}
