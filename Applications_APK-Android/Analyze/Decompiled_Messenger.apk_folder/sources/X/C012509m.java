package X;

import android.graphics.Color;
import java.util.regex.Pattern;

/* renamed from: X.09m  reason: invalid class name and case insensitive filesystem */
public final class C012509m {
    static {
        Pattern.compile("^([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})$");
    }

    public static int A01(int i, float f) {
        return C15970wH.A03(i, (int) (f * 255.0f));
    }

    public static int A00(int i, float f) {
        return C15970wH.A03(i, Math.round(((float) Color.alpha(i)) * f));
    }

    public static int A02(int i, int i2) {
        float alpha = ((float) Color.alpha(i)) / 255.0f;
        float f = 1.0f - alpha;
        return Color.rgb((int) ((((float) Color.red(i2)) * f) + (((float) Color.red(i)) * alpha)), (int) ((((float) Color.green(i2)) * f) + (((float) Color.green(i)) * alpha)), (int) ((f * ((float) Color.blue(i2))) + (alpha * ((float) Color.blue(i)))));
    }

    public static int A03(String str, int i) {
        try {
            if (!C06850cB.A0A(str) && !str.startsWith("#")) {
                str = AnonymousClass08S.A00('#', str);
            }
            return Color.parseColor(str);
        } catch (Exception unused) {
            return i;
        }
    }

    public static int A04(String str, int i) {
        if (C06850cB.A0B(str)) {
            return i;
        }
        try {
            if (str.charAt(0) != '#') {
                str = AnonymousClass08S.A0J("#", str);
            }
            return Color.parseColor(str);
        } catch (IllegalArgumentException e) {
            C010708t.A0L("ColorUtil", "color", e);
            return i;
        }
    }
}
