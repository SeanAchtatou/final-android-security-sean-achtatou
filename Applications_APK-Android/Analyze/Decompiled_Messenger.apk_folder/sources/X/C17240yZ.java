package X;

import android.content.Context;
import android.content.Intent;
import java.util.Iterator;

/* renamed from: X.0yZ  reason: invalid class name and case insensitive filesystem */
public final class C17240yZ implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass0r6 A00;

    public C17240yZ(AnonymousClass0r6 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r9) {
        int A002 = AnonymousClass09Y.A00(1574591937);
        switch (AnonymousClass195.A00(intent.getIntExtra("event", -1)).ordinal()) {
            case 1:
                AnonymousClass0r6 r3 = this.A00;
                C005505z.A03("PresenceManager:onMqttConnected", -754905976);
                try {
                    r3.A0R();
                    r3.A03 = C13430rQ.MQTT_CONNECTED_WAITING_FOR_PRESENCE;
                    C05180Xy r2 = new C05180Xy();
                    for (AnonymousClass12A add : r3.A0M.keySet()) {
                        r2.add(add);
                    }
                    Iterator it = r2.iterator();
                    while (it.hasNext()) {
                        ((AnonymousClass12A) it.next()).A00();
                    }
                    break;
                } finally {
                    C005505z.A00(690996121);
                }
            case 2:
                AnonymousClass0r6 r4 = this.A00;
                r4.A0I.A03 = ((AnonymousClass06B) AnonymousClass1XX.A02(7, AnonymousClass1Y3.AgK, r4.A02)).now();
                AnonymousClass0r6.A03(r4);
                r4.A03 = C13430rQ.MQTT_DISCONNECTED;
                AnonymousClass0r6.A0C(r4, true);
                break;
        }
        AnonymousClass09Y.A01(-1319095329, A002);
    }
}
