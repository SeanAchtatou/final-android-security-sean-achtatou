package X;

import com.facebook.graphql.calls.GQLCallInputCInputShape0S0000000;
import com.facebook.graphql.calls.GraphQlCallInput;
import com.facebook.graphql.query.GraphQlQueryParamSet;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0l0  reason: invalid class name and case insensitive filesystem */
public class C10880l0 {
    public GraphQlQueryParamSet A00 = new GraphQlQueryParamSet();
    public final int A01;
    public final int A02;
    public final long A03;
    public final Class A04;
    public final String A05;
    public final String A06;
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;
    public volatile long A0A;
    public volatile boolean A0B;
    public volatile boolean A0C;

    public void A0A(String str, List list) {
        String str2;
        if (list == null || list.isEmpty() || (list.get(0) instanceof String) || (list.get(0) instanceof GraphQlCallInput)) {
            GraphQlQueryParamSet graphQlQueryParamSet = this.A00;
            if (str == null) {
                str = String.valueOf((Object) null);
            }
            if (list != null) {
                graphQlQueryParamSet.A00.A0A(str, list);
                return;
            }
            return;
        }
        ArrayList A002 = C04300To.A00();
        for (Object next : list) {
            if (next != null) {
                str2 = next.toString();
            } else {
                str2 = null;
            }
            A002.add(str2);
        }
        GraphQlQueryParamSet graphQlQueryParamSet2 = this.A00;
        if (str == null) {
            str = String.valueOf((Object) null);
        }
        if (A002 != null) {
            graphQlQueryParamSet2.A00.A0A(str, A002);
        }
    }

    public boolean A0B() {
        return this instanceof C07450dZ;
    }

    public String A01() {
        if (C05850aR.A00 == null) {
            C05850aR.A00 = C05850aR.A01();
        }
        return Long.toString(C05850aR.A00.getLegacyPersistIdForQueryNameHash(this.A0A));
    }

    public String A02() {
        if (C05850aR.A00 == null) {
            C05850aR.A00 = C05850aR.A01();
        }
        return Long.toString(C05850aR.A00.getOssPersistIdForQueryNameHash(this.A0A));
    }

    public void A03(GraphQlQueryParamSet graphQlQueryParamSet) {
        if (graphQlQueryParamSet == null) {
            graphQlQueryParamSet = new GraphQlQueryParamSet();
        }
        this.A00 = graphQlQueryParamSet;
    }

    public void A04(String str, GraphQlCallInput graphQlCallInput) {
        this.A00.A01(str, graphQlCallInput);
    }

    public void A05(String str, Boolean bool) {
        GraphQlQueryParamSet graphQlQueryParamSet = this.A00;
        if (bool != null) {
            graphQlQueryParamSet.A00.A06(str, bool);
        }
    }

    public void A06(String str, Enum enumR) {
        GraphQlQueryParamSet graphQlQueryParamSet = this.A00;
        if (enumR != null) {
            graphQlQueryParamSet.A00.A03().A0K(str, enumR.toString());
        }
    }

    public void A07(String str, Number number) {
        GraphQlQueryParamSet graphQlQueryParamSet = this.A00;
        if (number != null) {
            graphQlQueryParamSet.A00.A03().A0J(str, number);
        }
    }

    public void A08(String str, Object obj) {
        GraphQlQueryParamSet graphQlQueryParamSet = this.A00;
        if (obj != null) {
            GQLCallInputCInputShape0S0000000 gQLCallInputCInputShape0S0000000 = graphQlQueryParamSet.A00;
            if (obj == null) {
                return;
            }
            if (obj instanceof String) {
                gQLCallInputCInputShape0S0000000.A09(str, (String) obj);
            } else if (obj instanceof Number) {
                gQLCallInputCInputShape0S0000000.A03().A0J(str, (Number) obj);
            } else if (obj instanceof Boolean) {
                gQLCallInputCInputShape0S0000000.A06(str, (Boolean) obj);
            } else if (obj instanceof Enum) {
                gQLCallInputCInputShape0S0000000.A03().A0K(str, ((Enum) obj).toString());
            } else if (obj instanceof List) {
                gQLCallInputCInputShape0S0000000.A0A(str, (List) obj);
            } else if (obj instanceof GraphQlCallInput) {
                gQLCallInputCInputShape0S0000000.A05(str, (GraphQlCallInput) obj);
            } else {
                throw new IllegalArgumentException("Value is not type that can be added. Actual value type: " + obj.getClass());
            }
        }
    }

    public void A09(String str, String str2) {
        GraphQlQueryParamSet graphQlQueryParamSet = this.A00;
        if (str2 != null) {
            graphQlQueryParamSet.A00.A09(str, str2);
        }
    }

    public C10880l0(Class cls, int i, long j, boolean z, boolean z2, int i2, String str, String str2, long j2) {
        this.A01 = i2;
        this.A06 = str;
        this.A08 = z;
        this.A07 = z2;
        this.A0C = true;
        this.A09 = true;
        this.A0A = j2;
        this.A04 = cls;
        this.A05 = str2;
        this.A02 = i;
        this.A03 = j;
    }

    public boolean A0C(String str) {
        return !str.equals(str);
    }
}
