package X;

import com.facebook.common.callercontext.CallerContext;
import com.facebook.messaging.service.model.FetchThreadListParams;

/* renamed from: X.0li  reason: invalid class name */
public final class AnonymousClass0li extends AnonymousClass0lj {
    public final /* synthetic */ CallerContext A00;
    public final /* synthetic */ FetchThreadListParams A01;
    public final /* synthetic */ C09080gV A02;

    public AnonymousClass0li(C09080gV r1, FetchThreadListParams fetchThreadListParams, CallerContext callerContext) {
        this.A02 = r1;
        this.A01 = fetchThreadListParams;
        this.A00 = callerContext;
    }
}
