package X;

import android.content.Context;
import androidx.fragment.app.Fragment;
import com.facebook.auth.userscope.UserScoped;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

@UserScoped
/* renamed from: X.22N  reason: invalid class name */
public final class AnonymousClass22N implements AnonymousClass13r {
    private static C05540Zi A02;
    private AnonymousClass0UN A00;
    private final C15390vD A01;

    public void BST(boolean z) {
    }

    public static final AnonymousClass22N A00(AnonymousClass1XY r4) {
        AnonymousClass22N r0;
        synchronized (AnonymousClass22N.class) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r4)) {
                    A02.A00 = new AnonymousClass22N((AnonymousClass1XY) A02.A01());
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass22N) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public /* bridge */ /* synthetic */ void AP7(Context context, Fragment fragment, C33691nz r4) {
        C138276cr r0 = new C138276cr(r4);
        Preconditions.checkNotNull(r0);
        ((C138856dw) fragment).A09 = r0;
    }

    public Fragment AV6() {
        return new C138856dw();
    }

    public AnonymousClass10Z B5D(Context context) {
        return new AnonymousClass10Z(AnonymousClass01R.A03(context, ((AnonymousClass1MT) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AJa, this.A00)).A03(C29631gj.A0R, AnonymousClass07B.A0N)), 20, -1, 26, -1);
    }

    public boolean BGe(Fragment fragment) {
        C138856dw r3 = (C138856dw) fragment;
        C148706vE r0 = (C148706vE) r3.A0H.get(r3.A05);
        if (r0 == null) {
            return true;
        }
        return r0.A0G;
    }

    public void C4d(Fragment fragment) {
        C138856dw r4 = (C138856dw) fragment;
        C148706vE r0 = (C148706vE) r4.A0H.get(r4.A05);
        if (r0 != null) {
            r0.A06.A0L.A01(0, true);
        }
    }

    public void CBh(Fragment fragment, C33761o6 r3) {
        ((C138856dw) fragment).A07 = new C111035Qt(r3);
    }

    private AnonymousClass22N(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(1, r3);
        this.A01 = AnonymousClass22O.A00(r3);
    }

    public C15390vD Ae6() {
        return this.A01;
    }

    public Class AiR() {
        return C138856dw.class;
    }

    public String B5E(Context context) {
        return context.getString(2131823829);
    }

    public String B6U(Context context) {
        return context.getString(2131834123);
    }

    public ImmutableList B6e() {
        return RegularImmutableList.A02;
    }
}
