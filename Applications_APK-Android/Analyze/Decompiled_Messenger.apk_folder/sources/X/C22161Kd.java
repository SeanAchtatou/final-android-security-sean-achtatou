package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;
import com.facebook.messaging.model.threads.ThreadSummary;
import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.1Kd  reason: invalid class name and case insensitive filesystem */
public final class C22161Kd extends C17770zR {
    public AnonymousClass0UN A00;
    @Comparable(type = 13)
    public ThreadSummary A01;
    @Comparable(type = 13)
    public C16400x0 A02;
    @Comparable(type = 13)
    public MigColorScheme A03;

    public C22161Kd(Context context) {
        super("M4ThreadItemThreadNameComponent");
        this.A00 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
