package X;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.1sr  reason: invalid class name and case insensitive filesystem */
public final class C36311sr extends C35761rm {
    private static final C36321ss A02;
    public InputStream A00 = null;
    public OutputStream A01 = null;

    static {
        C36341su r1;
        String name = C36311sr.class.getName();
        try {
            Object invoke = C36321ss.A01.invoke(null, name);
            Class<?> cls = invoke.getClass();
            Class<String> cls2 = String.class;
            Method declaredMethod = cls.getDeclaredMethod("error", cls2);
            Method declaredMethod2 = cls.getDeclaredMethod("warn", cls2);
            r1 = new C72213ds(declaredMethod, invoke, name);
            new C72223dt(declaredMethod2, invoke, name);
        } catch (ExceptionInInitializerError | IllegalAccessException | IllegalArgumentException | NoSuchMethodException | NullPointerException | InvocationTargetException unused) {
            r1 = new C36331st(name);
            new C36351sv(name);
        }
        A02 = new C36321ss(r1);
    }

    public void A03() {
        InputStream inputStream = this.A00;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                A02.A00.AOZ(AnonymousClass08S.A0J("Error closing input stream.", e.toString()));
            }
            this.A00 = null;
        }
        OutputStream outputStream = this.A01;
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e2) {
                A02.A00.AOZ(AnonymousClass08S.A0J("Error closing output stream.", e2.toString()));
            }
            this.A01 = null;
        }
    }

    public C36311sr() {
    }

    public C36311sr(InputStream inputStream) {
        this.A00 = inputStream;
    }

    public C36311sr(OutputStream outputStream) {
        this.A01 = outputStream;
    }
}
