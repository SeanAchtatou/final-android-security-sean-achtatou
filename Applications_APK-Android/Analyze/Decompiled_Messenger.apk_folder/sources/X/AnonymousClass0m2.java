package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.0m2  reason: invalid class name */
public final class AnonymousClass0m2 extends AnonymousClass0UV {
    private static C05540Zi A00;
    private static C05540Zi A01;
    private static C05540Zi A02;
    private static final Object A03 = new Object();
    private static final Object A04 = new Object();
    private static final Object A05 = new Object();

    public static final AnonymousClass0m6 A00(AnonymousClass1XY r6) {
        AnonymousClass0m6 r0;
        synchronized (A03) {
            C05540Zi A002 = C05540Zi.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r6)) {
                    C05540Zi r4 = A00;
                    AnonymousClass0m7 r3 = new AnonymousClass0m7((AnonymousClass1XY) A00.A01());
                    r4.A00 = new AnonymousClass0m6(r3, AnonymousClass0m8.FACEBOOK, AnonymousClass0m9.A03(r3));
                }
                C05540Zi r1 = A00;
                r0 = (AnonymousClass0m6) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final AnonymousClass0m6 A01(AnonymousClass1XY r6) {
        AnonymousClass0m6 r0;
        synchronized (A04) {
            C05540Zi A002 = C05540Zi.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r6)) {
                    C05540Zi r4 = A01;
                    AnonymousClass0m7 r3 = new AnonymousClass0m7((AnonymousClass1XY) A01.A01());
                    r4.A00 = new AnonymousClass0m6(r3, AnonymousClass0m8.SMS, AnonymousClass0m9.A03(r3));
                }
                C05540Zi r1 = A01;
                r0 = (AnonymousClass0m6) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final AnonymousClass0m6 A02(AnonymousClass1XY r6) {
        AnonymousClass0m6 r0;
        synchronized (A05) {
            C05540Zi A002 = C05540Zi.A00(A02);
            A02 = A002;
            try {
                if (A002.A03(r6)) {
                    C05540Zi r4 = A02;
                    AnonymousClass0m7 r3 = new AnonymousClass0m7((AnonymousClass1XY) A02.A01());
                    r4.A00 = new AnonymousClass0m6(r3, AnonymousClass0m8.TINCAN, AnonymousClass0m9.A03(r3));
                }
                C05540Zi r1 = A02;
                r0 = (AnonymousClass0m6) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A02.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final Boolean A03(AnonymousClass1XY r2) {
        boolean z = false;
        if (AnonymousClass0UU.A05(r2) != C001500z.A03) {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
