package X;

/* renamed from: X.0SL  reason: invalid class name */
public final class AnonymousClass0SL implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$CallbackHandler$3";
    public final /* synthetic */ AnonymousClass0CC A00;
    public final /* synthetic */ AnonymousClass0CR A01;

    public AnonymousClass0SL(AnonymousClass0CC r1, AnonymousClass0CR r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void run() {
        AnonymousClass0SB r3 = (AnonymousClass0SB) this.A01.A04.A01();
        if (r3.equals(AnonymousClass0SB.A05) || r3.equals(AnonymousClass0SB.A04)) {
            AnonymousClass0BL r1 = this.A00.A02.A0P;
            synchronized (r1) {
                r1.A07 = true;
            }
        }
        AnonymousClass0C8 r2 = this.A00.A02.A0n;
        AnonymousClass0CC r12 = this.A00;
        if (r2 == r12.A00) {
            if (r3.equals(AnonymousClass0SB.A03)) {
                r12.A02.A0E.clear();
            }
            AnonymousClass0AH.A05(this.A00.A02, AnonymousClass0FG.CONNECT_FAILED, C01540Aq.A00(r3));
        } else {
            AnonymousClass0C8 r22 = r12.A02.A0o;
            AnonymousClass0CC r13 = this.A00;
            if (r22 == r13.A00) {
                AnonymousClass0AH.A01(r13.A02);
            }
        }
        ((C01850Bw) this.A00.A02.A0A.A07(C01850Bw.class)).A02(AnonymousClass0C3.A04, r3.name());
    }
}
