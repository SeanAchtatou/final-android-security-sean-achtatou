package X;

/* renamed from: X.0K7  reason: invalid class name */
public final class AnonymousClass0K7 implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.common.analytics.defaultlogger.DefaultAnalyticsLogger$UploadRunnable";
    public final /* synthetic */ AnonymousClass0QV A00;

    public AnonymousClass0K7(AnonymousClass0QV r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:88:0x019b */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01bf A[LOOP:0: B:8:0x0026->B:101:0x01bf, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0023 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0171 A[Catch:{ IOException -> 0x0179 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0198 A[SYNTHETIC, Splitter:B:86:0x0198] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01bc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r15 = this;
            X.0QV r0 = r15.A00
            X.0KU r7 = r0.A03
            java.lang.String r6 = "AnalyticsUploader"
            java.io.File r0 = r7.A01
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0023
            java.io.File r0 = r7.A01
            java.io.File[] r5 = r0.listFiles()
            if (r5 != 0) goto L_0x0024
            java.io.File r0 = r7.A01
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x01c3
            java.lang.String r0 = "directory_not_found"
            X.C010708t.A0I(r6, r0)
        L_0x0023:
            return
        L_0x0024:
            int r4 = r5.length
            r3 = 0
        L_0x0026:
            if (r3 >= r4) goto L_0x0023
            r14 = r5[r3]
            r13 = 0
            r2 = 0
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ all -> 0x0192 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x0192 }
            r1.<init>(r14)     // Catch:{ all -> 0x0192 }
            java.lang.String r0 = "UTF-8"
            r9.<init>(r1, r0)     // Catch:{ all -> 0x0192 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0195 }
            r8.<init>()     // Catch:{ all -> 0x0195 }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r2 = new char[r0]     // Catch:{ all -> 0x0195 }
        L_0x0041:
            int r1 = r9.read(r2)     // Catch:{ all -> 0x0195 }
            r0 = -1
            if (r1 == r0) goto L_0x004d
            r0 = 0
            r8.append(r2, r0, r1)     // Catch:{ all -> 0x0195 }
            goto L_0x0041
        L_0x004d:
            java.lang.String r12 = r8.toString()     // Catch:{ all -> 0x0195 }
            r9.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0054:
            X.0MT r9 = r7.A00
            java.lang.String r8 = "message"
            java.util.HashMap r11 = new java.util.HashMap
            r11.<init>()
            java.lang.String r1 = "method"
            java.lang.String r0 = "logging.clientevent"
            r11.put(r1, r0)
            java.lang.String r1 = "format"
            java.lang.String r0 = "json"
            r11.put(r1, r0)
            java.lang.String r1 = r9.A01
            java.lang.String r0 = "access_token"
            r11.put(r0, r1)
            if (r12 != 0) goto L_0x007b
            java.lang.String r1 = "AnalyticsHttpClient"
            java.lang.String r0 = "Json data cannot be null"
            X.C010708t.A0J(r1, r0)     // Catch:{ IOException -> 0x00a5 }
        L_0x007b:
            java.lang.String r0 = "UTF-8"
            byte[] r2 = r12.getBytes(r0)     // Catch:{ IOException -> 0x00a5 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x00a5 }
            r1.<init>()     // Catch:{ IOException -> 0x00a5 }
            java.util.zip.DeflaterOutputStream r0 = new java.util.zip.DeflaterOutputStream     // Catch:{ IOException -> 0x00a5 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x00a5 }
            r0.write(r2)     // Catch:{ IOException -> 0x00a5 }
            r0.close()     // Catch:{ IOException -> 0x00a5 }
            byte[] r1 = r1.toByteArray()     // Catch:{ IOException -> 0x00a5 }
            r0 = 2
            java.lang.String r0 = android.util.Base64.encodeToString(r1, r0)     // Catch:{ IOException -> 0x00a5 }
            r11.put(r8, r0)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r1 = "compressed"
            java.lang.String r0 = "1"
            r11.put(r1, r0)     // Catch:{ IOException -> 0x00a5 }
            goto L_0x00b0
        L_0x00a5:
            r2 = move-exception
            java.lang.String r1 = "AnalyticsHttpClient"
            java.lang.String r0 = "Unable to compress message to Json object, using original message"
            X.C010708t.A0R(r1, r2, r0)
            r11.put(r8, r12)
        L_0x00b0:
            X.0DT r0 = r9.A00
            java.lang.String r2 = r9.A02
            java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0188 }
            X.0Bg r0 = r0.A00     // Catch:{ MalformedURLException -> 0x0188 }
            java.lang.Object r0 = r0.get()     // Catch:{ MalformedURLException -> 0x0188 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ MalformedURLException -> 0x0188 }
            r1.<init>(r0)     // Catch:{ MalformedURLException -> 0x0188 }
            java.net.URLConnection r9 = r1.openConnection()     // Catch:{ IOException -> 0x0182 }
            java.net.HttpURLConnection r9 = (java.net.HttpURLConnection) r9     // Catch:{ IOException -> 0x0182 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r9.setConnectTimeout(r0)
            r0 = 1
            r9.setDoOutput(r0)     // Catch:{ IOException -> 0x0179 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r0 = "application/x-www-form-urlencoded; charset=UTF-8"
            r9.setRequestProperty(r1, r0)     // Catch:{ IOException -> 0x0179 }
            java.lang.String r0 = "User-Agent"
            r9.setRequestProperty(r0, r2)     // Catch:{ IOException -> 0x0179 }
            java.lang.String r1 = "X-FB-HTTP-Engine"
            java.lang.String r0 = "MQTT Analytics"
            r9.setRequestProperty(r1, r0)     // Catch:{ IOException -> 0x0179 }
            java.lang.String r2 = "UTF-8"
            java.lang.String r10 = "Unable to create output stream"
            java.io.DataOutputStream r8 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0166, SecurityException -> 0x015f }
            r0 = 133641169(0x7f733d1, float:3.7194884E-34)
            java.io.OutputStream r0 = X.AnonymousClass0EN.A01(r9, r0)     // Catch:{ IOException -> 0x0166, SecurityException -> 0x015f }
            r8.<init>(r0)     // Catch:{ IOException -> 0x0166, SecurityException -> 0x015f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0179 }
            r1.<init>()     // Catch:{ IOException -> 0x0179 }
            java.util.Set r0 = r11.entrySet()     // Catch:{ IOException -> 0x0179 }
            java.util.Iterator r11 = r0.iterator()     // Catch:{ IOException -> 0x0179 }
        L_0x0100:
            boolean r0 = r11.hasNext()     // Catch:{ IOException -> 0x0179 }
            if (r0 == 0) goto L_0x0137
            java.lang.Object r10 = r11.next()     // Catch:{ IOException -> 0x0179 }
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10     // Catch:{ IOException -> 0x0179 }
            int r0 = r1.length()     // Catch:{ IOException -> 0x0179 }
            if (r0 == 0) goto L_0x0117
            java.lang.String r0 = "&"
            r1.append(r0)     // Catch:{ IOException -> 0x0179 }
        L_0x0117:
            java.lang.Object r0 = r10.getKey()     // Catch:{ UnsupportedEncodingException -> 0x014e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ UnsupportedEncodingException -> 0x014e }
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r2)     // Catch:{ UnsupportedEncodingException -> 0x014e }
            r1.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x014e }
            java.lang.String r0 = "="
            r1.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x014e }
            java.lang.Object r0 = r10.getValue()     // Catch:{ UnsupportedEncodingException -> 0x014e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ UnsupportedEncodingException -> 0x014e }
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r2)     // Catch:{ UnsupportedEncodingException -> 0x014e }
            r1.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x014e }
            goto L_0x0100
        L_0x0137:
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x0145 }
            r8.writeBytes(r0)     // Catch:{ IOException -> 0x0145 }
            r8.flush()     // Catch:{ IOException -> 0x0145 }
            r8.close()     // Catch:{ IOException -> 0x0179 }
            goto L_0x016e
        L_0x0145:
            r2 = move-exception
            java.lang.String r1 = "AnalyticsService"
            java.lang.String r0 = "Failed to write to output stream"
            X.C010708t.A0R(r1, r2, r0)     // Catch:{ all -> 0x015a }
            goto L_0x0156
        L_0x014e:
            r2 = move-exception
            java.lang.String r1 = "AnalyticsService"
            java.lang.String r0 = ""
            X.C010708t.A0R(r1, r2, r0)     // Catch:{ IOException -> 0x0179 }
        L_0x0156:
            r8.close()     // Catch:{ IOException -> 0x0179 }
            goto L_0x016c
        L_0x015a:
            r0 = move-exception
            r8.close()     // Catch:{ IOException -> 0x0179 }
            throw r0     // Catch:{ IOException -> 0x0179 }
        L_0x015f:
            r1 = move-exception
            java.lang.String r0 = "AnalyticsService"
            X.C010708t.A0R(r0, r1, r10)     // Catch:{ IOException -> 0x0179 }
            goto L_0x016c
        L_0x0166:
            r1 = move-exception
            java.lang.String r0 = "AnalyticsService"
            X.C010708t.A0R(r0, r1, r10)     // Catch:{ IOException -> 0x0179 }
        L_0x016c:
            r0 = 0
            goto L_0x016f
        L_0x016e:
            r0 = 1
        L_0x016f:
            if (r0 == 0) goto L_0x01a3
            int r8 = r9.getResponseCode()     // Catch:{ IOException -> 0x0179 }
            r9.disconnect()
            goto L_0x01a7
        L_0x0179:
            r2 = move-exception
            java.lang.String r1 = "AnalyticsService"
            java.lang.String r0 = ""
            X.C010708t.A0R(r1, r2, r0)     // Catch:{ all -> 0x01d7 }
            goto L_0x01a3
        L_0x0182:
            r2 = move-exception
            java.lang.String r1 = "AnalyticsService"
            java.lang.String r0 = "Failed to open http connection"
            goto L_0x018d
        L_0x0188:
            r2 = move-exception
            java.lang.String r1 = "AnalyticsService"
            java.lang.String r0 = "Logging end point malformed"
        L_0x018d:
            X.C010708t.A0R(r1, r2, r0)
            r8 = -1
            goto L_0x01a7
        L_0x0192:
            r0 = move-exception
            r9 = r2
            goto L_0x0196
        L_0x0195:
            r0 = move-exception
        L_0x0196:
            if (r9 == 0) goto L_0x019b
            r9.close()     // Catch:{ IOException -> 0x019b }
        L_0x019b:
            throw r0     // Catch:{ IOException -> 0x019c }
        L_0x019c:
            r1 = move-exception
            java.lang.String r0 = "Unable to read file"
            X.C010708t.A0R(r6, r1, r0)
            goto L_0x01bd
        L_0x01a3:
            r9.disconnect()
            r8 = -1
        L_0x01a7:
            r2 = 200(0xc8, float:2.8E-43)
            if (r8 != r2) goto L_0x01ba
            boolean r0 = r14.delete()
            if (r0 != 0) goto L_0x01ba
            java.lang.Object[] r1 = new java.lang.Object[]{r14}
            java.lang.String r0 = "File %s was not deleted"
            X.C010708t.A0P(r6, r0, r1)
        L_0x01ba:
            if (r8 != r2) goto L_0x01bd
            r13 = 1
        L_0x01bd:
            if (r13 == 0) goto L_0x0023
            int r3 = r3 + 1
            goto L_0x0026
        L_0x01c3:
            java.io.File r0 = r7.A01
            boolean r0 = r0.isFile()
            if (r0 == 0) goto L_0x01d1
            java.lang.String r0 = "directory_is_file"
            X.C010708t.A0I(r6, r0)
            return
        L_0x01d1:
            java.lang.String r0 = "directory_unknown_error"
            X.C010708t.A0I(r6, r0)
            return
        L_0x01d7:
            r0 = move-exception
            r9.disconnect()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0K7.run():void");
    }
}
