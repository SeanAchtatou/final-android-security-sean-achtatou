package X;

import android.content.Context;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

/* renamed from: X.1zh  reason: invalid class name and case insensitive filesystem */
public final class C39841zh implements C54292mB {
    public C24440Bzl A00;
    public ListenableFuture A01;
    public String A02;
    public final Context A03;
    public final C183148eW A04;
    public final Executor A05;

    public static final C39841zh A00(AnonymousClass1XY r1) {
        return new C39841zh(r1);
    }

    public void CAK(C24440Bzl bzl) {
        this.A00 = bzl;
    }

    private C39841zh(AnonymousClass1XY r2) {
        this.A03 = AnonymousClass1YA.A00(r2);
        this.A04 = C183148eW.A00(r2);
        this.A05 = AnonymousClass0UX.A0U(r2);
    }
}
