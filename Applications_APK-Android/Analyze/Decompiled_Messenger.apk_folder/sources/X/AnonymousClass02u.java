package X;

import android.util.Log;

/* renamed from: X.02u  reason: invalid class name */
public abstract class AnonymousClass02u implements AnonymousClass02v {
    private int A00;

    public abstract void CNm(String str, String str2);

    public abstract void CNn(String str, String str2, Throwable th);

    public boolean BFj(int i) {
        if (this.A00 <= i) {
            return true;
        }
        return false;
    }

    public int Aum() {
        return this.A00;
    }

    public void C9Z(int i) {
        this.A00 = i;
    }

    public void AWW(String str, String str2) {
    }

    public void AY3(String str, String str2) {
        Log.e(str, str2);
    }

    public void CLT(String str, String str2) {
    }

    public void CMp(String str, String str2) {
        Log.w(str, str2);
    }

    public void AWX(String str, String str2, Throwable th) {
    }

    public void AY4(String str, String str2, Throwable th) {
        Log.e(str, str2, th);
    }

    public void BIm(int i, String str, String str2) {
        Log.println(i, str, str2);
    }

    public void CLU(String str, String str2, Throwable th) {
    }

    public void CMq(String str, String str2, Throwable th) {
        Log.w(str, str2, th);
    }
}
