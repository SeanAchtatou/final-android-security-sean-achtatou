package X;

/* renamed from: X.1XY  reason: invalid class name */
public interface AnonymousClass1XY extends AnonymousClass0UH {
    AnonymousClass1XY getApplicationInjector();

    C24811Xe getInjectorThreadStack();

    C24851Xi getScopeAwareInjector();

    C24781Xb getScopeUnawareInjector();
}
