package X;

/* renamed from: X.0hN  reason: invalid class name and case insensitive filesystem */
public final class C09440hN implements AnonymousClass0Z1 {
    public final /* synthetic */ C09350h5 A00;

    public String Amj() {
        return "analytics_session_id";
    }

    public C09440hN(C09350h5 r1) {
        this.A00 = r1;
    }

    public String getCustomData(Throwable th) {
        return ((C06230bA) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BTX, this.A00.A00)).A06();
    }
}
