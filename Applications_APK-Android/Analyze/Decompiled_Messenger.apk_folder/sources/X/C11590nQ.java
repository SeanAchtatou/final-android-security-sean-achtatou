package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

/* renamed from: X.0nQ  reason: invalid class name and case insensitive filesystem */
public final class C11590nQ {
    public boolean A00 = false;
    public Activity A01;
    private int A02 = 0;
    private List A03;
    private List A04;
    public final PriorityQueue A05 = new PriorityQueue(1, new C27861ds());
    public final Set A06 = new HashSet(1);
    private final AnonymousClass1ZJ A07;

    public static synchronized void A03(C11590nQ r1) {
        synchronized (r1) {
            r1.A02++;
        }
    }

    public static synchronized void A04(C11590nQ r4) {
        synchronized (r4) {
            int i = r4.A02;
            boolean z = false;
            if (i > 0) {
                z = true;
            }
            Preconditions.checkState(z);
            int i2 = i - 1;
            r4.A02 = i2;
            if (i2 <= 0) {
                List<C13180qr> list = r4.A04;
                if (list != null) {
                    for (C13180qr r1 : list) {
                        r4.A06.remove(r1);
                        r4.A05.remove(r1);
                    }
                    r4.A04 = null;
                }
                List<C13180qr> list2 = r4.A03;
                if (list2 != null) {
                    for (C13180qr r12 : list2) {
                        r4.A05.add(r12);
                        if (r12 instanceof C13200qt) {
                            r4.A06.add((C13200qt) r12);
                        }
                    }
                    r4.A03 = null;
                }
            }
        }
    }

    public void A0E() {
        this.A00 = false;
        A03(this);
        C005505z.A03("FbActivityListeners.onPause", -498395143);
        try {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                ((C13180qr) it.next()).Bhz(this.A01);
            }
            A02(this);
        } finally {
            C005505z.A00(-223417005);
            A04(this);
        }
    }

    public synchronized void A0Q(C13180qr r2) {
        if (this.A02 == 0) {
            this.A06.remove(r2);
            this.A05.remove(r2);
        } else {
            if (this.A04 == null) {
                this.A04 = C04300To.A01(1);
            }
            this.A04.add(r2);
        }
    }

    public static final C11590nQ A00(AnonymousClass1XY r3) {
        return new C11590nQ(new C27851dr(r3), AnonymousClass1ZI.A00(r3));
    }

    private void A01() {
        if (!this.A00) {
            this.A00 = true;
            A03(this);
            C005505z.A03("FbActivityListeners.onActivated", -1793612652);
            try {
                for (C13200qt BN7 : this.A06) {
                    BN7.BN7(this.A01);
                }
                A02(this);
            } finally {
                C005505z.A00(649230325);
                A04(this);
            }
        }
    }

    public static void A02(C11590nQ r2) {
        r2.A07.A00(r2.A06.size() - 1);
    }

    public static void A05(C11590nQ r1, C13180qr r2) {
        if (r1.A02 == 0) {
            r1.A05.add(r2);
            if (r2 instanceof C13200qt) {
                r1.A06.add((C13200qt) r2);
                return;
            }
            return;
        }
        if (r1.A03 == null) {
            r1.A03 = C04300To.A02(1);
        }
        r1.A03.add(r2);
    }

    private C11590nQ(C27851dr r4, AnonymousClass1ZJ r5) {
        this.A07 = r5;
        synchronized (this) {
            A05(this, r4);
        }
    }

    /* JADX INFO: finally extract failed */
    public Dialog A06(int i) {
        A03(this);
        C005505z.A03("FbActivityListeners.onCreateDialog", 1492285008);
        try {
            Dialog dialog = null;
            int i2 = 0;
            for (C13200qt BUl : this.A06) {
                dialog = BUl.BUl(this.A01, i);
                i2++;
                if (dialog != null) {
                    break;
                }
            }
            C005505z.A00(-1149808638);
            A04(this);
            this.A07.A00(i2 - 1);
            return dialog;
        } catch (Throwable th) {
            C005505z.A00(571105069);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public Optional A07() {
        Optional absent = Optional.absent();
        A03(this);
        C005505z.A03("FbActivityListeners.onSearchRequested", 57309764);
        try {
            int i = 0;
            for (C13200qt BnW : this.A06) {
                absent = BnW.BnW(this.A01);
                i++;
                if (absent.isPresent()) {
                    break;
                }
            }
            C005505z.A00(-1589946190);
            A04(this);
            this.A07.A00(i - 1);
            return absent;
        } catch (Throwable th) {
            C005505z.A00(1269075241);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public Optional A08(int i, KeyEvent keyEvent) {
        Optional absent = Optional.absent();
        A03(this);
        C005505z.A03("FbActivityListeners.onKeyDown", -1567947330);
        try {
            int i2 = 0;
            for (C13200qt BcM : this.A06) {
                absent = BcM.BcM(this.A01, i, keyEvent);
                i2++;
                if (absent.isPresent()) {
                    break;
                }
            }
            C005505z.A00(-2121597775);
            A04(this);
            this.A07.A00(i2 - 1);
            return absent;
        } catch (Throwable th) {
            C005505z.A00(1181275041);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public Optional A09(int i, KeyEvent keyEvent) {
        Optional absent = Optional.absent();
        A03(this);
        C005505z.A03("FbActivityListeners.onKeyUp", -207701531);
        try {
            int i2 = 0;
            for (C13200qt BcN : this.A06) {
                absent = BcN.BcN(this.A01, i, keyEvent);
                i2++;
                if (absent.isPresent()) {
                    break;
                }
            }
            C005505z.A00(-1947753749);
            A04(this);
            this.A07.A00(i2 - 1);
            return absent;
        } catch (Throwable th) {
            C005505z.A00(5152869);
            A04(this);
            throw th;
        }
    }

    public void A0A() {
        A03(this);
        C005505z.A03("FbActivityListeners.finish", -280541786);
        try {
            for (C13200qt AZv : this.A06) {
                AZv.AZv(this.A01);
            }
            A02(this);
        } finally {
            C005505z.A00(-2036062952);
            A04(this);
        }
    }

    /* JADX INFO: finally extract failed */
    public void A0B() {
        A03(this);
        C005505z.A03("FbActivityListeners.onActivityCreate", -1490131782);
        try {
            Iterator it = this.A05.iterator();
            int i = 0;
            while (it.hasNext()) {
                C13180qr r2 = (C13180qr) it.next();
                i++;
                if (this.A01.isFinishing()) {
                    break;
                }
                C005505z.A03(C05260Yg.A00(r2.getClass()), 177647799);
                r2.BNA(this.A01);
                C005505z.A00(712909321);
            }
            C005505z.A00(-741485139);
            A04(this);
            this.A07.A00(i - 1);
        } catch (Throwable th) {
            C005505z.A00(-551060310);
            A04(this);
            throw th;
        }
    }

    public void A0C() {
        A03(this);
        C005505z.A03("FbActivityListeners.onContentChanged", -680664483);
        try {
            for (C13200qt BTy : this.A06) {
                BTy.BTy(this.A01);
            }
            A02(this);
        } finally {
            C005505z.A00(-823849659);
            A04(this);
        }
    }

    public void A0D() {
        A03(this);
        C005505z.A03("FbActivityListeners.onDestroy", 616308016);
        try {
            for (C13200qt BVx : this.A06) {
                BVx.BVx(this.A01);
            }
            A02(this);
        } finally {
            C005505z.A00(-186406746);
            A04(this);
        }
    }

    /* JADX INFO: finally extract failed */
    public void A0F() {
        A01();
        A03(this);
        C005505z.A03("FbActivityListeners.onResume", -2017871193);
        try {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                C13180qr r2 = (C13180qr) it.next();
                C005505z.A03(C05260Yg.A00(r2.getClass()), 1212224750);
                r2.BmN(this.A01);
                C005505z.A00(-582653294);
            }
            A02(this);
            C005505z.A00(1627240335);
            A04(this);
        } catch (Throwable th) {
            C005505z.A00(82520664);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void A0G() {
        A01();
        A03(this);
        C005505z.A03("FbActivityListeners.onStart", -31554435);
        try {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                C13180qr r2 = (C13180qr) it.next();
                C005505z.A03(C05260Yg.A00(r2.getClass()), 1254224912);
                r2.BpP(this.A01);
                C005505z.A00(-1977841759);
            }
            A02(this);
            C005505z.A00(2111051030);
            A04(this);
        } catch (Throwable th) {
            C005505z.A00(-192568119);
            A04(this);
            throw th;
        }
    }

    public void A0H() {
        A03(this);
        C005505z.A03("FbActivityListeners.onStop", 1552892959);
        try {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                ((C13180qr) it.next()).Bq4(this.A01);
            }
            A02(this);
        } finally {
            C005505z.A00(-781311731);
            A04(this);
        }
    }

    public void A0I() {
        A03(this);
        C005505z.A03("FbActivityListeners.onUserInteraction", -1764298469);
        try {
            for (C13200qt Btv : this.A06) {
                Btv.Btv(this.A01);
            }
            A02(this);
        } finally {
            C005505z.A00(-1763314711);
            A04(this);
        }
    }

    public void A0J() {
        A03(this);
        C005505z.A03("FbActivityListeners.onUserLeaveHint", -965952027);
        try {
            for (C13200qt Bty : this.A06) {
                Bty.Bty(this.A01);
            }
            A02(this);
        } finally {
            C005505z.A00(-26044321);
            A04(this);
        }
    }

    public void A0K(int i, int i2, Intent intent) {
        A03(this);
        C005505z.A03("FbActivityListeners.onActivityResult", -1955364585);
        try {
            for (C13200qt BNJ : this.A06) {
                BNJ.BNJ(this.A01, i, i2, intent);
            }
            A02(this);
        } finally {
            C005505z.A00(-2118536266);
            A04(this);
        }
    }

    public void A0L(Intent intent) {
        A01();
        A03(this);
        C005505z.A03("FbActivityListeners.onNewIntent", -1627367807);
        try {
            for (C13200qt BgX : this.A06) {
                BgX.BgX(this.A01, intent);
            }
            A02(this);
        } finally {
            C005505z.A00(1519403155);
            A04(this);
        }
    }

    public void A0M(Configuration configuration) {
        A03(this);
        C005505z.A03("FbActivityListeners.onConfigurationChanged", 1137321572);
        try {
            for (C13200qt BTK : this.A06) {
                BTK.BTK(this.A01, configuration);
            }
            A02(this);
        } finally {
            C005505z.A00(-1017783488);
            A04(this);
        }
    }

    public void A0N(Bundle bundle) {
        A03(this);
        C005505z.A03("FbActivityListeners.onPostCreate", -1537007441);
        try {
            for (C13200qt BjC : this.A06) {
                BjC.BjC(this.A01, bundle);
            }
            A02(this);
        } finally {
            C005505z.A00(-1751831682);
            A04(this);
        }
    }

    public void A0O(Menu menu) {
        A03(this);
        C005505z.A03("FbActivityListeners.onCreateOptionsMenu", -200224186);
        try {
            for (C13200qt BUs : this.A06) {
                BUs.BUs(menu);
            }
            A02(this);
        } finally {
            C005505z.A00(-1149111797);
            A04(this);
        }
    }

    public void A0P(Menu menu) {
        A03(this);
        C005505z.A03("FbActivityListeners.onPrepareOptionsMenu", -1186039755);
        try {
            for (C13200qt BjX : this.A06) {
                BjX.BjX(menu);
            }
            A02(this);
        } finally {
            C005505z.A00(596647941);
            A04(this);
        }
    }

    public void A0R(CharSequence charSequence, int i) {
        A03(this);
        C005505z.A03("FbActivityListeners.onTitleChanged", 1469503793);
        try {
            for (C13200qt BsC : this.A06) {
                BsC.BsC(charSequence, i);
            }
            A02(this);
        } finally {
            C005505z.A00(394197727);
            A04(this);
        }
    }

    public void A0S(boolean z) {
        A03(this);
        C005505z.A03("FbActivityListeners.onWindowFocusChanged", 136529153);
        try {
            for (C13200qt Bv9 : this.A06) {
                Bv9.Bv9(this.A01, z);
            }
            A02(this);
        } finally {
            C005505z.A00(1834476569);
            A04(this);
        }
    }

    public void A0T(boolean z, Configuration configuration) {
        A03(this);
        C005505z.A03("FbActivityListeners.onPictureInPictureModeChanged", 1661928356);
        try {
            for (C13200qt Bif : this.A06) {
                Bif.Bif(this.A01, z, configuration);
            }
            A02(this);
        } finally {
            C005505z.A00(322313087);
            A04(this);
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean A0U() {
        A03(this);
        C005505z.A03("FbActivityListeners.onBackPressed", -397774479);
        try {
            Iterator it = this.A06.iterator();
            boolean z = false;
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                i++;
                if (((C13200qt) it.next()).BPC(this.A01)) {
                    z = true;
                    break;
                }
            }
            C005505z.A00(1511811288);
            A04(this);
            this.A07.A00(i - 1);
            return z;
        } catch (Throwable th) {
            C005505z.A00(-1300432862);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean A0V(int i, Dialog dialog) {
        A03(this);
        C005505z.A03("FbActivityListeners.onPrepareDialog", 687284057);
        try {
            Iterator it = this.A06.iterator();
            boolean z = false;
            int i2 = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                i2++;
                if (((C13200qt) it.next()).BjS(this.A01, i, dialog)) {
                    z = true;
                    break;
                }
            }
            C005505z.A00(577836229);
            A04(this);
            this.A07.A00(i2 - 1);
            return z;
        } catch (Throwable th) {
            C005505z.A00(-1874991970);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean A0W(Bundle bundle) {
        boolean z;
        A01();
        A03(this);
        C005505z.A03("FbActivityListeners.onBeforeActivityCreate", 1745674735);
        try {
            Iterator it = this.A06.iterator();
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                C13200qt r2 = (C13200qt) it.next();
                i++;
                if (this.A01.isFinishing()) {
                    z = true;
                    break;
                }
                C005505z.A03(C05260Yg.A00(r2.getClass()), -1012081346);
                r2.BPY(this.A01, bundle);
                C005505z.A00(636126452);
            }
            C005505z.A00(-301663138);
            A04(this);
            this.A07.A00(i - 1);
            if (z || this.A01.isFinishing()) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            C005505z.A00(162431495);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean A0X(Bundle bundle) {
        A01();
        A03(this);
        C005505z.A03("FbActivityListeners.onBeforeSuperOnCreate", 823374683);
        try {
            Iterator it = this.A06.iterator();
            boolean z = false;
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ((C13200qt) it.next()).BPo(this.A01, bundle);
                i++;
                if (this.A01.isFinishing()) {
                    z = true;
                    break;
                }
            }
            C005505z.A00(-1388445208);
            A04(this);
            this.A07.A00(i - 1);
            return z;
        } catch (Throwable th) {
            C005505z.A00(-1084125077);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean A0Y(MenuItem menuItem) {
        A03(this);
        C005505z.A03("FbActivityListeners.onOptionsItemSelected", 1623552787);
        try {
            Iterator it = this.A06.iterator();
            boolean z = false;
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                i++;
                if (((C13200qt) it.next()).BhL(menuItem)) {
                    z = true;
                    break;
                }
            }
            C005505z.A00(466917668);
            A04(this);
            this.A07.A00(i - 1);
            return z;
        } catch (Throwable th) {
            C005505z.A00(-903211423);
            A04(this);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean A0Z(Throwable th) {
        A03(this);
        C005505z.A03("FbActivityListeners.handleServiceException", -1058640876);
        try {
            Iterator it = this.A06.iterator();
            boolean z = false;
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                i++;
                if (((C13200qt) it.next()).Bo7(this.A01, th)) {
                    z = true;
                    break;
                }
            }
            C005505z.A00(314532058);
            A04(this);
            this.A07.A00(i - 1);
            return z;
        } catch (Throwable th2) {
            C005505z.A00(-604380646);
            A04(this);
            throw th2;
        }
    }
}
