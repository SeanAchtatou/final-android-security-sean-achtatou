package X;

import android.content.Context;
import com.facebook.proxygen.EventBase;
import com.facebook.proxygen.HTTPClient;
import com.facebook.proxygen.HTTPThread;
import com.facebook.proxygen.ProxygenRadioMeter;
import com.facebook.proxygen.utils.BLogWrapper;
import com.facebook.proxygen.utils.CircularEventLog;
import com.facebook.proxygen.utils.EventBaseFuncLog;
import java.util.concurrent.Executor;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0ka  reason: invalid class name and case insensitive filesystem */
public final class C10630ka implements C10640kc {
    private static volatile C10630ka A0A;
    public C31731kG A00 = null;
    public AnonymousClass0UN A01;
    public final C181310h A02 = new C181310h();
    public final AnonymousClass1WC A03;
    public final HTTPClient A04;
    public final Executor A05;
    public final Executor A06;
    public final C04310Tq A07;
    private final C16840xr A08;
    private final HTTPThread A09;

    public void ASk() {
    }

    public String Ap0() {
        return "Liger";
    }

    private C10630ka(AnonymousClass1XY r18, AnonymousClass1Vu r19, C31541jr r20, AnonymousClass09P r21, C06230bA r22, AnonymousClass1WC r23, HTTPClient.Builder builder, C31531jq r25) {
        AnonymousClass1XY r2 = r18;
        this.A01 = new AnonymousClass0UN(13, r2);
        this.A07 = AnonymousClass0VG.A00(AnonymousClass1Y3.A43, r2);
        C005505z.A03("LigerRequestExecutor", 1763526810);
        try {
            r25.A01();
            new BLogWrapper(r21).init();
            AnonymousClass1WC r0 = r23;
            this.A03 = r0;
            int i = r0.A00;
            HTTPThread hTTPThread = new HTTPThread();
            Thread thread = new Thread(new C31711kE(hTTPThread, i), "Liger-EventBase");
            thread.setPriority(7);
            thread.start();
            hTTPThread.waitForInitialization();
            this.A09 = hTTPThread;
            HTTPClient.Builder builder2 = builder;
            builder2.mEventBase = hTTPThread.getEventBase();
            CircularEventLog circularEventLog = new CircularEventLog(this.A09.getEventBase(), 100);
            r19.A00 = circularEventLog;
            C10660ke.A01 = circularEventLog;
            C10660ke.A02 = new EventBaseFuncLog(this.A09.getEventBase());
            r20.A00 = builder2.mPersistentDNSCacheSettings;
            int i2 = AnonymousClass1Y3.Ar4;
            AnonymousClass0UN r4 = this.A01;
            if (((C16810xo) AnonymousClass1XX.A02(3, i2, r4)).isEnabled()) {
                EventBase eventBase = this.A09.getEventBase();
                int i3 = AnonymousClass1Y3.Ar4;
                AnonymousClass0UN r42 = this.A01;
                C16810xo r02 = (C16810xo) AnonymousClass1XX.A02(3, i3, r42);
                C31731kG r6 = new C31731kG((Context) AnonymousClass1XX.A02(5, AnonymousClass1Y3.BCt, r4), eventBase, r02.B9x(), r02.AfY(), (AnonymousClass125) AnonymousClass1XX.A02(7, AnonymousClass1Y3.At6, r42), (C09340h3) AnonymousClass1XX.A02(6, AnonymousClass1Y3.AeN, r42), (C04460Ut) AnonymousClass1XX.A02(8, AnonymousClass1Y3.AKb, r42), (C31631k0) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AO4, r42), (AnonymousClass0Ud) AnonymousClass1XX.A02(10, AnonymousClass1Y3.B5j, r42), (ProxygenRadioMeter) AnonymousClass1XX.A02(11, AnonymousClass1Y3.ADv, r42));
                this.A00 = r6;
                C16830xq r43 = new C16830xq(r6);
                C06600bl BMm = r6.A00.BMm();
                BMm.A02("com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED", r43);
                BMm.A00().A00();
                ((C16810xo) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Ar4, this.A01)).C7c(this.A00);
            }
            this.A06 = ((AnonymousClass0VT) AnonymousClass1XX.A02(12, AnonymousClass1Y3.ALd, this.A01)).A01(4, AnonymousClass0VS.URGENT, "PersistentCaches", null);
            C04650Vo A012 = ((AnonymousClass0VT) AnonymousClass1XX.A02(12, AnonymousClass1Y3.ALd, this.A01)).A01(4, AnonymousClass0VS.SUPER_HIGH, "DnsResolver", null);
            this.A05 = A012;
            builder2.mPersistentCachesExecutor = this.A06;
            builder2.mDNSResolverExecutor = A012;
            this.A08 = new C16840xr(r22);
            A01();
            builder2.mProxyFallbackEnabled = true;
            builder2.mEnableTransportCallbacks = true;
            HTTPClient build = builder2.build();
            this.A04 = build;
            build.mNetworkStatusMonitor = this.A00;
            build.mAnalyticsLogger = this.A08;
            this.A02.A01(build, build.mIsSandbox);
            this.A04.init();
            C10660ke.A01.init();
            C005505z.A00(1807771740);
        } catch (Throwable th) {
            C005505z.A00(2111973879);
            throw th;
        }
    }

    public static final C10630ka A00(AnonymousClass1XY r11) {
        if (A0A == null) {
            synchronized (C10630ka.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        C10660ke.A00(applicationInjector);
                        AnonymousClass1Vu A003 = C10690kh.A00(applicationInjector);
                        C31541jr A004 = C31541jr.A00(applicationInjector);
                        AnonymousClass09P A012 = C04750Wa.A01(applicationInjector);
                        C06230bA A022 = AnonymousClass1ZF.A02(applicationInjector);
                        AnonymousClass0VG.A00(AnonymousClass1Y3.AcD, applicationInjector);
                        A0A = new C10630ka(applicationInjector, A003, A004, A012, A022, C10710kj.A00(applicationInjector), C10710kj.A01(applicationInjector), new C31531jq(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x009f, code lost:
        if (r1 >= 65536) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b3, code lost:
        if (r1 >= 65536) goto L_0x00b5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean A01() {
        /*
            r7 = this;
            int r2 = X.AnonymousClass1Y3.AOJ
            X.0UN r1 = r7.A01
            r0 = 1
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.1Yd r2 = (X.C25051Yd) r2
            r0 = 285928858261465(0x1040d000717d9, double:1.41267626021601E-309)
            boolean r0 = r2.Aem(r0)
            if (r0 == 0) goto L_0x003e
            X.10h r3 = r7.A02
            int r2 = X.AnonymousClass1Y3.BFE
            X.0UN r1 = r7.A01
            r0 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r2, r1)
            X.EWY r0 = (X.EWY) r0
            X.EWa r2 = r0.A01()
            X.EWa r1 = r3.A02
            r0 = 1
            if (r1 == 0) goto L_0x002d
            r0 = 0
        L_0x002d:
            if (r0 == 0) goto L_0x003c
            if (r1 == 0) goto L_0x0037
            boolean r0 = r2.equals(r1)
            if (r0 != 0) goto L_0x003c
        L_0x0037:
            r3.A00(r2)
            r0 = 1
            return r0
        L_0x003c:
            r0 = 0
            return r0
        L_0x003e:
            X.10h r6 = r7.A02
            r2 = 2
            int r1 = X.AnonymousClass1Y3.AwY
            X.0UN r0 = r7.A01
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r2, r1, r0)
            X.1jx r0 = (X.C31601jx) r0
            org.apache.http.HttpHost r1 = r0.A01
            java.lang.String r5 = ""
            if (r1 == 0) goto L_0x006d
            java.lang.String r0 = r1.getHostName()
            r6.A04 = r0
            int r0 = r1.getPort()
            r6.A00 = r0
            java.lang.String r0 = r1.getHostName()
            r6.A05 = r0
            int r0 = r1.getPort()
            r6.A01 = r0
            r6.A03 = r5
            r0 = 1
            return r0
        L_0x006d:
            java.lang.String r0 = "http.proxyHost"
            java.lang.String r0 = java.lang.System.getProperty(r0)
            r6.A04 = r0
            java.lang.String r0 = "https.proxyHost"
            java.lang.String r0 = java.lang.System.getProperty(r0)
            r6.A05 = r0
            java.lang.String r0 = "http.nonProxyHosts"
            java.lang.String r0 = java.lang.System.getProperty(r0)
            r6.A03 = r0
            java.lang.String r0 = "http.proxyPort"
            java.lang.String r1 = java.lang.System.getProperty(r0)
            java.lang.String r0 = "https.proxyPort"
            java.lang.String r4 = java.lang.System.getProperty(r0)
            r3 = 65536(0x10000, float:9.18355E-41)
            r2 = 0
            if (r1 == 0) goto L_0x00a1
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x00a1 }
            r6.A00 = r1     // Catch:{ NumberFormatException -> 0x00a1 }
            if (r1 < 0) goto L_0x00a1
            r0 = 1
            if (r1 < r3) goto L_0x00a2
        L_0x00a1:
            r0 = 0
        L_0x00a2:
            if (r0 != 0) goto L_0x00a8
            r6.A04 = r5
            r6.A00 = r2
        L_0x00a8:
            if (r4 == 0) goto L_0x00b5
            int r1 = java.lang.Integer.parseInt(r4)     // Catch:{ NumberFormatException -> 0x00b5 }
            r6.A01 = r1     // Catch:{ NumberFormatException -> 0x00b5 }
            if (r1 < 0) goto L_0x00b5
            r0 = 1
            if (r1 < r3) goto L_0x00b6
        L_0x00b5:
            r0 = 0
        L_0x00b6:
            if (r0 != 0) goto L_0x00bc
            r6.A05 = r5
            r6.A01 = r2
        L_0x00bc:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10630ka.A01():boolean");
    }

    public void A02() {
        synchronized (this.A04) {
            boolean z = ((C31601jx) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AwY, this.A01)).A02;
            if (A01() || z != this.A04.mIsSandbox) {
                this.A02.A01(this.A04, z);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x015f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.http.HttpResponse AZ6(org.apache.http.client.methods.HttpUriRequest r18, X.AnonymousClass2Y2 r19, org.apache.http.protocol.HttpContext r20, X.AnonymousClass2ZJ r21) {
        /*
            r17 = this;
            r4 = r18
            r4.getURI()
            r3 = 0
        L_0x0006:
            int r3 = r3 + 1
            r5 = r17
            r5.A02()     // Catch:{ HttpNetworkException -> 0x0148 }
            X.0Tq r0 = r5.A07     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.Object r6 = r0.get()     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ HttpNetworkException -> 0x0148 }
            java.net.URI r0 = r4.getURI()     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r2 = r0.getHost()     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r1 = "Host"
            org.apache.http.Header r0 = r4.getFirstHeader(r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            if (r0 != 0) goto L_0x0028
            r4.setHeader(r1, r2)     // Catch:{ HttpNetworkException -> 0x0148 }
        L_0x0028:
            java.lang.String r1 = "User-Agent"
            org.apache.http.Header r0 = r4.getFirstHeader(r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            if (r0 != 0) goto L_0x0033
            r4.setHeader(r1, r6)     // Catch:{ HttpNetworkException -> 0x0148 }
        L_0x0033:
            org.apache.http.params.HttpParams r2 = r4.getParams()     // Catch:{ HttpNetworkException -> 0x0148 }
            r0 = r19
            com.facebook.http.interfaces.RequestPriority r0 = r0.A04     // Catch:{ HttpNetworkException -> 0x0148 }
            int r1 = r0.requestPriority     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r0 = "priority"
            r2.setIntParameter(r0, r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            X.2ZH r0 = X.AnonymousClass2ZH.A00(r20)     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.common.callercontext.CallerContext r0 = r0.A01     // Catch:{ HttpNetworkException -> 0x0148 }
            if (r0 == 0) goto L_0x0055
            org.apache.http.params.HttpParams r2 = r4.getParams()     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r1 = r0.A02     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r0 = "fb_request_call_path"
            r2.setParameter(r0, r1)     // Catch:{ HttpNetworkException -> 0x0148 }
        L_0x0055:
            boolean r0 = r4 instanceof org.apache.http.HttpEntityEnclosingRequest     // Catch:{ ProtocolException -> 0x0141 }
            if (r0 == 0) goto L_0x00af
            r6 = r4
            org.apache.http.HttpEntityEnclosingRequest r6 = (org.apache.http.HttpEntityEnclosingRequest) r6     // Catch:{ ProtocolException -> 0x0141 }
            org.apache.http.HttpEntity r9 = r6.getEntity()     // Catch:{ ProtocolException -> 0x0141 }
            if (r9 == 0) goto L_0x009e
            boolean r0 = r9.isChunked()     // Catch:{ ProtocolException -> 0x0141 }
            if (r0 != 0) goto L_0x00a7
            long r7 = r9.getContentLength()     // Catch:{ ProtocolException -> 0x0141 }
            r1 = 0
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x00a7
            long r1 = r9.getContentLength()     // Catch:{ ProtocolException -> 0x0141 }
            int r0 = (int) r1     // Catch:{ ProtocolException -> 0x0141 }
            java.lang.String r1 = java.lang.String.valueOf(r0)     // Catch:{ ProtocolException -> 0x0141 }
            r0 = 458(0x1ca, float:6.42E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)     // Catch:{ ProtocolException -> 0x0141 }
            r4.setHeader(r0, r1)     // Catch:{ ProtocolException -> 0x0141 }
        L_0x0084:
            org.apache.http.Header r0 = r9.getContentEncoding()     // Catch:{ ProtocolException -> 0x0141 }
            if (r0 == 0) goto L_0x0091
            org.apache.http.Header r0 = r9.getContentEncoding()     // Catch:{ ProtocolException -> 0x0141 }
            r4.setHeader(r0)     // Catch:{ ProtocolException -> 0x0141 }
        L_0x0091:
            org.apache.http.Header r0 = r9.getContentType()     // Catch:{ ProtocolException -> 0x0141 }
            if (r0 == 0) goto L_0x009e
            org.apache.http.Header r0 = r9.getContentType()     // Catch:{ ProtocolException -> 0x0141 }
            r4.setHeader(r0)     // Catch:{ ProtocolException -> 0x0141 }
        L_0x009e:
            org.apache.http.impl.client.EntityEnclosingRequestWrapper r7 = new org.apache.http.impl.client.EntityEnclosingRequestWrapper     // Catch:{ ProtocolException -> 0x0141 }
            r7.<init>(r6)     // Catch:{ ProtocolException -> 0x0141 }
        L_0x00a3:
            r7.resetHeaders()     // Catch:{ ProtocolException -> 0x0141 }
            goto L_0x00b5
        L_0x00a7:
            java.lang.String r1 = "Transfer-Encoding"
            java.lang.String r0 = "chunked"
            r4.setHeader(r1, r0)     // Catch:{ ProtocolException -> 0x0141 }
            goto L_0x0084
        L_0x00af:
            org.apache.http.impl.client.RequestWrapper r7 = new org.apache.http.impl.client.RequestWrapper     // Catch:{ ProtocolException -> 0x0141 }
            r7.<init>(r4)     // Catch:{ ProtocolException -> 0x0141 }
            goto L_0x00a3
        L_0x00b5:
            X.1WC r8 = r5.A03     // Catch:{ HttpNetworkException -> 0x0148 }
            int r2 = X.AnonymousClass1Y3.Amr     // Catch:{ HttpNetworkException -> 0x0148 }
            X.0UN r1 = r5.A01     // Catch:{ HttpNetworkException -> 0x0148 }
            r0 = 4
            java.lang.Object r12 = X.AnonymousClass1XX.A02(r0, r2, r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            X.09P r12 = (X.AnonymousClass09P) r12     // Catch:{ HttpNetworkException -> 0x0148 }
            X.1W9 r0 = r8.A01     // Catch:{ HttpNetworkException -> 0x0148 }
            r14 = r21
            com.facebook.proxygen.HTTPTransportCallback r6 = r0.AbD(r14)     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.HTTPClient r5 = r5.A04     // Catch:{ HttpNetworkException -> 0x0148 }
            X.0Tq r0 = r8.A03     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.Object r2 = r0.get()     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.SamplePolicy r2 = (com.facebook.proxygen.SamplePolicy) r2     // Catch:{ HttpNetworkException -> 0x0148 }
            X.1W7 r8 = r8.A02     // Catch:{ HttpNetworkException -> 0x0148 }
            java.net.URI r0 = r4.getURI()     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r1 = r0.getHost()     // Catch:{ HttpNetworkException -> 0x0148 }
            r0 = r20
            com.facebook.proxygen.TraceEventHandler r11 = r8.AbE(r1, r0, r14, r2)     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.NativeReadBuffer r10 = new com.facebook.proxygen.NativeReadBuffer     // Catch:{ HttpNetworkException -> 0x0148 }
            r10.<init>()     // Catch:{ HttpNetworkException -> 0x0148 }
            r10.init()     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.RequestStatsObserver r13 = new com.facebook.proxygen.RequestStatsObserver     // Catch:{ HttpNetworkException -> 0x0148 }
            r13.<init>()     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.TraceEventObserver[] r0 = new com.facebook.proxygen.TraceEventObserver[]{r13}     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.TraceEventContext r1 = new com.facebook.proxygen.TraceEventContext     // Catch:{ HttpNetworkException -> 0x0148 }
            r1.<init>(r0, r2)     // Catch:{ HttpNetworkException -> 0x0148 }
            java.net.URI r0 = r4.getURI()     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r9 = r0.getHost()     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.LigerHttpResponseHandler r8 = new com.facebook.proxygen.LigerHttpResponseHandler     // Catch:{ HttpNetworkException -> 0x0148 }
            int r15 = r1.mParentID     // Catch:{ HttpNetworkException -> 0x0148 }
            X.2ZH r0 = X.AnonymousClass2ZH.A00(r20)     // Catch:{ HttpNetworkException -> 0x0148 }
            java.lang.String r0 = r0.A04     // Catch:{ HttpNetworkException -> 0x0148 }
            r16 = r0
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16)     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.HTTPRequestHandler r2 = new com.facebook.proxygen.HTTPRequestHandler     // Catch:{ HttpNetworkException -> 0x0148 }
            r2.<init>()     // Catch:{ HttpNetworkException -> 0x0148 }
            com.facebook.proxygen.JniHandler r0 = new com.facebook.proxygen.JniHandler     // Catch:{ HttpNetworkException -> 0x0148 }
            r0.<init>(r2, r8, r6)     // Catch:{ HttpNetworkException -> 0x0148 }
            r5.make(r0, r10, r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            r2.executeWithDefragmentation(r7)     // Catch:{ HttpNetworkException -> 0x0148 }
            X.2Sb r1 = new X.2Sb     // Catch:{ HttpNetworkException -> 0x0148 }
            r1.<init>(r2)     // Catch:{ HttpNetworkException -> 0x0148 }
            com.google.common.base.Preconditions.checkNotNull(r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            boolean r0 = r4 instanceof org.apache.http.client.methods.AbortableHttpRequest     // Catch:{ HttpNetworkException -> 0x0148 }
            if (r0 == 0) goto L_0x013c
            r0 = r4
            org.apache.http.client.methods.AbortableHttpRequest r0 = (org.apache.http.client.methods.AbortableHttpRequest) r0     // Catch:{ HttpNetworkException -> 0x0148 }
            r0.setReleaseTrigger(r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            boolean r0 = r4.isAborted()     // Catch:{ HttpNetworkException -> 0x0148 }
            if (r0 == 0) goto L_0x013c
            r2.cancel()     // Catch:{ HttpNetworkException -> 0x0148 }
        L_0x013c:
            org.apache.http.HttpResponse r0 = r8.getResponse()     // Catch:{ HttpNetworkException -> 0x0148 }
            return r0
        L_0x0141:
            r1 = move-exception
            org.apache.http.client.ClientProtocolException r0 = new org.apache.http.client.ClientProtocolException     // Catch:{ HttpNetworkException -> 0x0148 }
            r0.<init>(r1)     // Catch:{ HttpNetworkException -> 0x0148 }
            throw r0     // Catch:{ HttpNetworkException -> 0x0148 }
        L_0x0148:
            r5 = move-exception
            boolean r0 = r4 instanceof org.apache.http.HttpEntityEnclosingRequest
            if (r0 == 0) goto L_0x0160
            r0 = r4
            org.apache.http.HttpEntityEnclosingRequest r0 = (org.apache.http.HttpEntityEnclosingRequest) r0
            org.apache.http.HttpEntity r0 = r0.getEntity()
            if (r0 == 0) goto L_0x0160
            boolean r0 = r0.isRepeatable()
            if (r0 != 0) goto L_0x0160
        L_0x015c:
            r2 = 0
        L_0x015d:
            if (r2 != 0) goto L_0x0006
            throw r5
        L_0x0160:
            com.facebook.proxygen.HTTPRequestError r0 = r5.mError
            r2 = 1
            if (r0 == 0) goto L_0x015c
            com.facebook.proxygen.HTTPRequestError$ProxygenError r1 = com.facebook.proxygen.HTTPRequestError.ProxygenError.StreamUnacknowledged
            com.facebook.proxygen.HTTPRequestError$ProxygenError r0 = r0.mErrCode
            if (r1 != r0) goto L_0x015c
            if (r3 > r2) goto L_0x015c
            goto L_0x015d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C10630ka.AZ6(org.apache.http.client.methods.HttpUriRequest, X.2Y2, org.apache.http.protocol.HttpContext, X.2ZJ):org.apache.http.HttpResponse");
    }
}
