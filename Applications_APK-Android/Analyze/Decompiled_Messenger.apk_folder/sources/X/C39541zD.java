package X;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import java.util.Iterator;

/* renamed from: X.1zD  reason: invalid class name and case insensitive filesystem */
public final class C39541zD {
    public final /* synthetic */ C28394DuB A00;

    public C39541zD(C28394DuB duB) {
        this.A00 = duB;
    }

    public void A00() {
        Iterator it = this.A00.A0U.iterator();
        while (it.hasNext()) {
            ((C39541zD) it.next()).A00();
        }
    }

    public void A01(int i) {
        C28396DuD.A00(this.A00.A05, 7);
        C28396DuD.A00(this.A00.A05, 8);
        C28394DuB.A04(this.A00);
        C28394DuB duB = this.A00;
        C28396DuD duD = duB.A05;
        CameraPosition A01 = duB.A0S.A01();
        boolean z = false;
        if (this.A00.A06.A01 == 36) {
            z = true;
        }
        duD.A04(A01, z);
        Iterator it = this.A00.A0U.iterator();
        while (it.hasNext()) {
            ((C39541zD) it.next()).A01(i);
        }
    }
}
