package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1U3  reason: invalid class name */
public final class AnonymousClass1U3 {
    private static volatile AnonymousClass1U3 A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass1U3 A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1U3.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1U3(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public B6D A01(String str, C186038kQ r12) {
        int i = AnonymousClass1Y3.AKb;
        AnonymousClass0UN r2 = this.A00;
        return new C186028kP(str, (C04460Ut) AnonymousClass1XX.A02(0, i, r2), (AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, r2), r12, null, null);
    }

    public B6D A02(String str, C186038kQ r12, C36991uR r13, AnonymousClass09P r14) {
        int i = AnonymousClass1Y3.AKb;
        AnonymousClass0UN r2 = this.A00;
        return new C186028kP(str, (C04460Ut) AnonymousClass1XX.A02(0, i, r2), (AnonymousClass069) AnonymousClass1XX.A02(2, AnonymousClass1Y3.BBa, r2), r12, r13, r14);
    }

    private AnonymousClass1U3(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }
}
