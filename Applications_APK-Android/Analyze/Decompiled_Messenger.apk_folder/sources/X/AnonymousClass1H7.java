package X;

import android.util.Pair;
import java.util.Comparator;

/* renamed from: X.1H7  reason: invalid class name */
public final class AnonymousClass1H7 implements Comparator {
    public int compare(Object obj, Object obj2) {
        return ((Long) ((Pair) obj).second).compareTo((Long) ((Pair) obj2).second);
    }
}
