package X;

import android.content.Context;
import com.facebook.account.recovery.common.model.AccountCandidateModel;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0100000;
import com.facebook.litho.ComponentBuilderCBuilderShape0_0S0300000;
import com.facebook.litho.annotations.Comparable;
import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1v7  reason: invalid class name and case insensitive filesystem */
public final class C37321v7 extends C17770zR {
    public static final CallerContext A07 = CallerContext.A09("AccountLoginRecSelectMethodRootComponentSpec");
    public static final MigColorScheme A08 = C17190yT.A00();
    @Comparable(type = 13)
    public C25998CqM A00;
    @Comparable(type = 13)
    public AccountCandidateModel A01;
    public AnonymousClass0UN A02;
    @Comparable(type = 13)
    public C203069hi A03;
    @Comparable(type = 14)
    public AnonymousClass5CI A04;
    @Comparable(type = 13)
    public C121605oW A05;
    @Comparable(type = 13)
    public MigColorScheme A06 = A08;

    public static C17770zR A00(AnonymousClass0p4 r11, AnonymousClass9ZN r12, int i, float f, float f2) {
        AnonymousClass10N A0E;
        ImmutableList A002;
        String string;
        String str;
        if (i == 1 && !r12.A01.A02().isEmpty()) {
            A0E = C17780zS.A0E(C37321v7.class, r11, 1469135926, new Object[]{r11});
            A002 = r12.A01.A02();
            string = r11.A09.getString(2131821091);
            str = "sms_item";
        } else if (i != 2 || r12.A01.A00().isEmpty()) {
            return C16980y8.A00(r11).A00;
        } else {
            A0E = C17780zS.A0E(C37321v7.class, r11, 1089119481, new Object[]{r11});
            A002 = r12.A01.A00();
            string = r11.A09.getString(2131821090);
            str = "email_item";
        }
        Context context = r11.A09;
        int i2 = 2131821070;
        if (r12.A00 == i) {
            i2 = 2131821069;
        }
        String string2 = context.getString(i2, string);
        int i3 = 2131231351;
        if (r12.A00 == i) {
            i3 = 2131231352;
        }
        C37941wd A003 = C16980y8.A00(r11);
        A003.A20(100.0f);
        A003.A1t(70.0f);
        A003.A2X(AnonymousClass10G.TOP, f);
        A003.A2X(AnonymousClass10G.BOTTOM, f2);
        C37951we A004 = AnonymousClass11D.A00(r11);
        A004.A3E(C14950uP.CENTER);
        A004.A1q(100.0f);
        A004.A2X(AnonymousClass10G.RIGHT, (float) AnonymousClass1JQ.MEDIUM.B3A());
        ComponentBuilderCBuilderShape0_0S0100000 A005 = C22291Kt.A00(r11);
        A005.A36(i3);
        A005.A1p(30.0f);
        A005.A1z(30.0f);
        A005.A33(AnonymousClass1JZ.A02.AhV());
        A005.A1o(0.0f);
        A004.A3A((C22291Kt) A005.A00);
        A003.A3A(A004.A00);
        C37951we A006 = AnonymousClass11D.A00(r11);
        C14950uP r0 = C14950uP.CENTER;
        A006.A3E(r0);
        A006.A3E(r0);
        A006.A1q(100.0f);
        ComponentBuilderCBuilderShape0_0S0300000 A007 = C17030yD.A00(r11);
        A007.A3l(string);
        A007.A3f(AnonymousClass10J.A01);
        A006.A3A(A007.A39());
        StringBuilder sb = new StringBuilder((String) A002.get(0));
        AnonymousClass10J r6 = AnonymousClass10J.A0B;
        if (A002.size() > 1) {
            sb.append("\n");
            sb.append((String) A002.get(1));
        }
        ComponentBuilderCBuilderShape0_0S0300000 A008 = C17030yD.A00(r11);
        A008.A3l(sb.toString());
        A008.A3f(r6);
        A006.A3A(A008.A39());
        A003.A3A(A006.A00);
        A003.A2y(A0E);
        A003.A2p(str);
        A003.A2z(string2);
        A003.A2H(AnonymousClass1M2.A01(0.0f, 0, AnonymousClass1KA.A01(r12.A04.AfS())));
        return A003.A00;
    }

    public C37321v7(Context context) {
        super("AccountLoginRecSelectMethodRootComponent");
        this.A02 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
        this.A04 = new AnonymousClass5CI();
    }

    public static void A01(AnonymousClass0p4 r3, int i) {
        if (r3.A04 != null) {
            r3.A0F(new C61322yh(0, Integer.valueOf(i)), "updateState:AccountLoginRecSelectMethodRootComponent.setSelectedRecoveryType");
        }
    }

    public C17770zR A16() {
        C37321v7 r1 = (C37321v7) super.A16();
        r1.A04 = new AnonymousClass5CI();
        return r1;
    }
}
