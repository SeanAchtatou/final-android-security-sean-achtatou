package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.montage.inboxunit.InboxMontageItem;

/* renamed from: X.1HO  reason: invalid class name */
public final class AnonymousClass1HO implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new InboxMontageItem(parcel);
    }

    public Object[] newArray(int i) {
        return new InboxMontageItem[i];
    }
}
