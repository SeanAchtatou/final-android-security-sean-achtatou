package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1WP  reason: invalid class name */
public final class AnonymousClass1WP implements Set {
    public final /* synthetic */ C04180St A00;

    public AnonymousClass1WP(C04180St r1) {
        this.A00 = r1;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.A00.A08();
    }

    public boolean contains(Object obj) {
        if (this.A00.A03(obj) >= 0) {
            return true;
        }
        return false;
    }

    public boolean containsAll(Collection collection) {
        Map A07 = this.A00.A07();
        for (Object containsKey : collection) {
            if (!A07.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int hashCode;
        int i = 0;
        for (int A02 = this.A00.A02() - 1; A02 >= 0; A02--) {
            Object A05 = this.A00.A05(A02, 0);
            if (A05 == null) {
                hashCode = 0;
            } else {
                hashCode = A05.hashCode();
            }
            i += hashCode;
        }
        return i;
    }

    public boolean isEmpty() {
        if (this.A00.A02() == 0) {
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new C47662Xi(this.A00, 0);
    }

    public boolean remove(Object obj) {
        int A03 = this.A00.A03(obj);
        if (A03 < 0) {
            return false;
        }
        this.A00.A09(A03);
        return true;
    }

    public boolean removeAll(Collection collection) {
        Map A07 = this.A00.A07();
        int size = A07.size();
        for (Object remove : collection) {
            A07.remove(remove);
        }
        if (size != A07.size()) {
            return true;
        }
        return false;
    }

    public boolean retainAll(Collection collection) {
        return C04180St.A00(this.A00.A07(), collection);
    }

    public int size() {
        return this.A00.A02();
    }

    public boolean equals(Object obj) {
        return C04180St.A01(this, obj);
    }

    public Object[] toArray() {
        C04180St r5 = this.A00;
        int A02 = r5.A02();
        Object[] objArr = new Object[A02];
        for (int i = 0; i < A02; i++) {
            objArr[i] = r5.A05(i, 0);
        }
        return objArr;
    }

    public Object[] toArray(Object[] objArr) {
        return this.A00.A0B(objArr, 0);
    }
}
