package X;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.os.SystemClock;
import java.util.ArrayList;

/* renamed from: X.0Fw  reason: invalid class name and case insensitive filesystem */
public final class C02680Fw extends C007907e {
    public final AnonymousClass04b A00 = new AnonymousClass04b();
    private final C02660Fu A01 = new C02660Fu(true);
    private final ArrayList A02 = new ArrayList();
    public volatile boolean A03 = true;

    public synchronized void A05(SensorEventListener sensorEventListener, Sensor sensor) {
        if (this.A03) {
            this.A02.add(new C02710Fz(sensorEventListener, sensor));
            C02690Fx r1 = (C02690Fx) this.A00.get(sensor);
            if (r1 == null) {
                this.A00.put(sensor, new C02690Fx(SystemClock.elapsedRealtime(), 1));
            } else {
                r1.A00++;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a8, code lost:
        if (r2.isWakeUpSensor() == false) goto L_0x00aa;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A06(android.hardware.SensorEventListener r13, android.hardware.Sensor r14) {
        /*
            r12 = this;
            monitor-enter(r12)
            boolean r0 = r12.A03     // Catch:{ all -> 0x00bf }
            if (r0 == 0) goto L_0x00bd
            long r10 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x00bf }
            java.util.ArrayList r0 = r12.A02     // Catch:{ all -> 0x00bf }
            java.util.Iterator r9 = r0.iterator()     // Catch:{ all -> 0x00bf }
        L_0x000f:
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x00bf }
            if (r0 == 0) goto L_0x00bd
            java.lang.Object r7 = r9.next()     // Catch:{ all -> 0x00bf }
            X.0Fz r7 = (X.C02710Fz) r7     // Catch:{ all -> 0x00bf }
            android.hardware.SensorEventListener r0 = r7.A01     // Catch:{ all -> 0x00bf }
            if (r13 != r0) goto L_0x000f
            if (r14 == 0) goto L_0x0026
            android.hardware.Sensor r0 = r7.A00     // Catch:{ all -> 0x00bf }
            if (r14 == r0) goto L_0x0026
            goto L_0x000f
        L_0x0026:
            r9.remove()     // Catch:{ all -> 0x00bf }
            X.04b r1 = r12.A00     // Catch:{ all -> 0x00bf }
            android.hardware.Sensor r0 = r7.A00     // Catch:{ all -> 0x00bf }
            java.lang.Object r3 = r1.get(r0)     // Catch:{ all -> 0x00bf }
            X.0Fx r3 = (X.C02690Fx) r3     // Catch:{ all -> 0x00bf }
            if (r3 == 0) goto L_0x000f
            int r1 = r3.A00     // Catch:{ all -> 0x00bf }
            if (r1 == 0) goto L_0x000f
            r0 = 1
            if (r1 <= r0) goto L_0x0040
            int r1 = r1 - r0
            r3.A00 = r1     // Catch:{ all -> 0x00bf }
            goto L_0x000f
        L_0x0040:
            X.04b r1 = r12.A00     // Catch:{ all -> 0x00bf }
            android.hardware.Sensor r0 = r7.A00     // Catch:{ all -> 0x00bf }
            r1.remove(r0)     // Catch:{ all -> 0x00bf }
            android.hardware.Sensor r0 = r7.A00     // Catch:{ all -> 0x00bf }
            int r2 = r0.getType()     // Catch:{ all -> 0x00bf }
            X.0Fu r0 = r12.A01     // Catch:{ all -> 0x00bf }
            android.util.SparseArray r1 = r0.sensorConsumption     // Catch:{ all -> 0x00bf }
            r0 = 0
            java.lang.Object r6 = r1.get(r2, r0)     // Catch:{ all -> 0x00bf }
            X.0Fy r6 = (X.C02700Fy) r6     // Catch:{ all -> 0x00bf }
            if (r6 != 0) goto L_0x0066
            X.0Fy r6 = new X.0Fy     // Catch:{ all -> 0x00bf }
            r6.<init>()     // Catch:{ all -> 0x00bf }
            X.0Fu r0 = r12.A01     // Catch:{ all -> 0x00bf }
            android.util.SparseArray r0 = r0.sensorConsumption     // Catch:{ all -> 0x00bf }
            r0.put(r2, r6)     // Catch:{ all -> 0x00bf }
        L_0x0066:
            long r0 = r3.A01     // Catch:{ all -> 0x00bf }
            long r4 = r10 - r0
            long r0 = r6.activeTimeMs     // Catch:{ all -> 0x00bf }
            long r0 = r0 + r4
            r6.activeTimeMs = r0     // Catch:{ all -> 0x00bf }
            X.0Fu r0 = r12.A01     // Catch:{ all -> 0x00bf }
            X.0Fy r2 = r0.total     // Catch:{ all -> 0x00bf }
            long r0 = r2.activeTimeMs     // Catch:{ all -> 0x00bf }
            long r0 = r0 + r4
            r2.activeTimeMs = r0     // Catch:{ all -> 0x00bf }
            android.hardware.Sensor r0 = r7.A00     // Catch:{ all -> 0x00bf }
            float r0 = r0.getPower()     // Catch:{ all -> 0x00bf }
            double r2 = (double) r0     // Catch:{ all -> 0x00bf }
            double r0 = (double) r4     // Catch:{ all -> 0x00bf }
            double r2 = r2 * r0
            r0 = 4660134898793709568(0x40ac200000000000, double:3600.0)
            double r2 = r2 / r0
            r0 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r2 = r2 / r0
            double r0 = r6.powerMah     // Catch:{ all -> 0x00bf }
            double r0 = r0 + r2
            r6.powerMah = r0     // Catch:{ all -> 0x00bf }
            X.0Fu r0 = r12.A01     // Catch:{ all -> 0x00bf }
            X.0Fy r8 = r0.total     // Catch:{ all -> 0x00bf }
            double r0 = r8.powerMah     // Catch:{ all -> 0x00bf }
            double r0 = r0 + r2
            r8.powerMah = r0     // Catch:{ all -> 0x00bf }
            android.hardware.Sensor r2 = r7.A00     // Catch:{ all -> 0x00bf }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x00bf }
            r0 = 21
            if (r1 < r0) goto L_0x00aa
            boolean r1 = r2.isWakeUpSensor()     // Catch:{ all -> 0x00bf }
            r0 = 1
            if (r1 != 0) goto L_0x00ab
        L_0x00aa:
            r0 = 0
        L_0x00ab:
            if (r0 == 0) goto L_0x000f
            long r0 = r6.wakeUpTimeMs     // Catch:{ all -> 0x00bf }
            long r0 = r0 + r4
            r6.wakeUpTimeMs = r0     // Catch:{ all -> 0x00bf }
            X.0Fu r0 = r12.A01     // Catch:{ all -> 0x00bf }
            X.0Fy r2 = r0.total     // Catch:{ all -> 0x00bf }
            long r0 = r2.wakeUpTimeMs     // Catch:{ all -> 0x00bf }
            long r0 = r0 + r4
            r2.wakeUpTimeMs = r0     // Catch:{ all -> 0x00bf }
            goto L_0x000f
        L_0x00bd:
            monitor-exit(r12)
            return
        L_0x00bf:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02680Fw.A06(android.hardware.SensorEventListener, android.hardware.Sensor):void");
    }

    public AnonymousClass0FM A03() {
        return new C02660Fu(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0060, code lost:
        if (r7.isWakeUpSensor() == false) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(X.AnonymousClass0FM r14) {
        /*
            r13 = this;
            X.0Fu r14 = (X.C02660Fu) r14
            monitor-enter(r13)
            java.lang.String r0 = "Null value passed to getSnapshot!"
            X.C02740Gd.A00(r14, r0)     // Catch:{ all -> 0x009f }
            boolean r0 = r13.A03     // Catch:{ all -> 0x009f }
            r6 = 0
            if (r0 != 0) goto L_0x000f
            monitor-exit(r13)
            return r6
        L_0x000f:
            long r11 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x009f }
            X.0Fu r0 = r13.A01     // Catch:{ all -> 0x009f }
            r14.A09(r0)     // Catch:{ all -> 0x009f }
            X.04b r0 = r13.A00     // Catch:{ all -> 0x009f }
            int r8 = r0.size()     // Catch:{ all -> 0x009f }
        L_0x001e:
            if (r6 >= r8) goto L_0x009c
            X.04b r0 = r13.A00     // Catch:{ all -> 0x009f }
            java.lang.Object r7 = r0.A07(r6)     // Catch:{ all -> 0x009f }
            android.hardware.Sensor r7 = (android.hardware.Sensor) r7     // Catch:{ all -> 0x009f }
            java.lang.Object r1 = r0.A09(r6)     // Catch:{ all -> 0x009f }
            X.0Fx r1 = (X.C02690Fx) r1     // Catch:{ all -> 0x009f }
            int r0 = r1.A00     // Catch:{ all -> 0x009f }
            if (r0 <= 0) goto L_0x0099
            long r0 = r1.A01     // Catch:{ all -> 0x009f }
            long r4 = r11 - r0
            float r0 = r7.getPower()     // Catch:{ all -> 0x009f }
            double r2 = (double) r0     // Catch:{ all -> 0x009f }
            double r0 = (double) r4     // Catch:{ all -> 0x009f }
            double r2 = r2 * r0
            r0 = 4660134898793709568(0x40ac200000000000, double:3600.0)
            double r2 = r2 / r0
            r0 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r2 = r2 / r0
            X.0Fy r9 = r14.total     // Catch:{ all -> 0x009f }
            long r0 = r9.activeTimeMs     // Catch:{ all -> 0x009f }
            long r0 = r0 + r4
            r9.activeTimeMs = r0     // Catch:{ all -> 0x009f }
            double r0 = r9.powerMah     // Catch:{ all -> 0x009f }
            double r0 = r0 + r2
            r9.powerMah = r0     // Catch:{ all -> 0x009f }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x009f }
            r0 = 21
            if (r1 < r0) goto L_0x0062
            boolean r0 = r7.isWakeUpSensor()     // Catch:{ all -> 0x009f }
            r10 = 1
            if (r0 != 0) goto L_0x0063
        L_0x0062:
            r10 = 0
        L_0x0063:
            if (r10 == 0) goto L_0x006c
            X.0Fy r9 = r14.total     // Catch:{ all -> 0x009f }
            long r0 = r9.wakeUpTimeMs     // Catch:{ all -> 0x009f }
            long r0 = r0 + r4
            r9.wakeUpTimeMs = r0     // Catch:{ all -> 0x009f }
        L_0x006c:
            boolean r0 = r14.isAttributionEnabled     // Catch:{ all -> 0x009f }
            if (r0 == 0) goto L_0x0099
            int r1 = r7.getType()     // Catch:{ all -> 0x009f }
            android.util.SparseArray r0 = r14.sensorConsumption     // Catch:{ all -> 0x009f }
            java.lang.Object r7 = r0.get(r1)     // Catch:{ all -> 0x009f }
            X.0Fy r7 = (X.C02700Fy) r7     // Catch:{ all -> 0x009f }
            if (r7 != 0) goto L_0x0088
            X.0Fy r7 = new X.0Fy     // Catch:{ all -> 0x009f }
            r7.<init>()     // Catch:{ all -> 0x009f }
            android.util.SparseArray r0 = r14.sensorConsumption     // Catch:{ all -> 0x009f }
            r0.put(r1, r7)     // Catch:{ all -> 0x009f }
        L_0x0088:
            long r0 = r7.activeTimeMs     // Catch:{ all -> 0x009f }
            long r0 = r0 + r4
            r7.activeTimeMs = r0     // Catch:{ all -> 0x009f }
            double r0 = r7.powerMah     // Catch:{ all -> 0x009f }
            double r0 = r0 + r2
            r7.powerMah = r0     // Catch:{ all -> 0x009f }
            if (r10 == 0) goto L_0x0099
            long r0 = r7.wakeUpTimeMs     // Catch:{ all -> 0x009f }
            long r0 = r0 + r4
            r7.wakeUpTimeMs = r0     // Catch:{ all -> 0x009f }
        L_0x0099:
            int r6 = r6 + 1
            goto L_0x001e
        L_0x009c:
            monitor-exit(r13)
            r0 = 1
            return r0
        L_0x009f:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02680Fw.A04(X.0FM):boolean");
    }
}
