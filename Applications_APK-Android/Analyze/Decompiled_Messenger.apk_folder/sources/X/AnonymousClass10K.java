package X;

import com.facebook.mig.scheme.interfaces.MigColorScheme;

/* renamed from: X.10K  reason: invalid class name */
public interface AnonymousClass10K {
    boolean AcY();

    C16930y3 B5c();

    C16930y3 B5d(MigColorScheme migColorScheme);

    AnonymousClass1JL B5i();

    AnonymousClass10M B7S();

    String name();
}
