package X;

import com.google.common.base.Preconditions;
import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.0xP  reason: invalid class name and case insensitive filesystem */
public final class C16610xP<E> extends AbstractQueue<E> {
    public int A00;
    public int A01;
    public Object[] A02;
    public final int A03 = Integer.MAX_VALUE;
    private final C16620xQ A04;
    private final C16620xQ A05;

    private C16620xQ A01(int i) {
        boolean z = true;
        int i2 = ((i + 1) ^ -1) ^ -1;
        boolean z2 = false;
        if (i2 > 0) {
            z2 = true;
        }
        Preconditions.checkState(z2, "negative index");
        if ((1431655765 & i2) <= (i2 & -1431655766)) {
            z = false;
        }
        if (z) {
            return this.A05;
        }
        return this.A04;
    }

    public void clear() {
        for (int i = 0; i < this.A01; i++) {
            this.A02[i] = null;
        }
        this.A01 = 0;
    }

    private int A00() {
        int i = this.A01;
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        C16620xQ r0 = this.A04;
        C181810n r3 = r0.A01;
        Object[] objArr = r0.A02.A02;
        if (r3.compare(objArr[1], objArr[2]) > 0) {
            return 2;
        }
        return 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C30362Euo A02(int r13) {
        /*
            r12 = this;
            int r0 = r12.A01
            com.google.common.base.Preconditions.checkPositionIndex(r13, r0)
            int r0 = r12.A00
            int r0 = r0 + 1
            r12.A00 = r0
            int r0 = r12.A01
            int r1 = r0 + -1
            r12.A01 = r1
            r5 = 0
            if (r1 != r13) goto L_0x0019
            java.lang.Object[] r0 = r12.A02
            r0[r1] = r5
            return r5
        L_0x0019:
            java.lang.Object[] r0 = r12.A02
            r4 = r0[r1]
            X.0xQ r3 = r12.A01(r1)
            X.0xP r7 = r3.A02
            int r2 = r7.A01
            int r0 = r2 + -1
            int r1 = r0 >> 1
            if (r1 == 0) goto L_0x005a
            int r0 = r1 + -1
            int r0 = r0 >> 1
            int r0 = r0 << 1
            int r6 = r0 + 2
            if (r6 == r1) goto L_0x005a
            int r0 = r6 << 1
            int r0 = r0 + 1
            if (r0 < r2) goto L_0x005a
            java.lang.Object[] r0 = r7.A02
            r2 = r0[r6]
            X.10n r0 = r3.A01
            int r0 = r0.compare(r2, r4)
            if (r0 >= 0) goto L_0x005a
            X.0xP r0 = r3.A02
            java.lang.Object[] r1 = r0.A02
            r1[r6] = r4
            int r0 = r0.A01
            r1[r0] = r2
        L_0x0051:
            if (r6 != r13) goto L_0x005f
            java.lang.Object[] r1 = r12.A02
            int r0 = r12.A01
            r1[r0] = r5
            return r5
        L_0x005a:
            X.0xP r0 = r3.A02
            int r6 = r0.A01
            goto L_0x0051
        L_0x005f:
            int r1 = r12.A01
            java.lang.Object[] r0 = r12.A02
            r3 = r0[r1]
            r0[r1] = r5
            X.0xQ r9 = r12.A01(r13)
            r10 = r13
        L_0x006c:
            int r0 = r10 << 1
            int r11 = r0 + 1
            if (r11 >= 0) goto L_0x007f
            r0 = -1
        L_0x0073:
            if (r0 <= 0) goto L_0x0089
            X.0xP r1 = r9.A02
            java.lang.Object[] r7 = r1.A02
            r1 = r7[r0]
            r7[r10] = r1
            r10 = r0
            goto L_0x006c
        L_0x007f:
            int r0 = r11 << 1
            int r1 = r0 + 1
            r0 = 4
            int r0 = X.C16620xQ.A00(r9, r1, r0)
            goto L_0x0073
        L_0x0089:
            int r0 = r9.A02(r10, r3)
            if (r0 != r10) goto L_0x00dd
            r0 = 2
            int r8 = X.C16620xQ.A00(r9, r11, r0)
            if (r8 <= 0) goto L_0x00d8
            X.10n r11 = r9.A01
            X.0xP r0 = r9.A02
            java.lang.Object[] r0 = r0.A02
            r0 = r0[r8]
            int r0 = r11.compare(r0, r3)
            if (r0 >= 0) goto L_0x00d8
            X.0xP r0 = r9.A02
            java.lang.Object[] r1 = r0.A02
            r0 = r1[r8]
            r1[r10] = r0
            r1[r8] = r3
        L_0x00ae:
            if (r8 == r10) goto L_0x00c5
            X.0xP r2 = r9.A02
            if (r8 >= r13) goto L_0x00cf
            java.lang.Object[] r0 = r2.A02
            r1 = r0[r13]
        L_0x00b8:
            X.0xQ r0 = r9.A00
            int r0 = r0.A02(r8, r3)
            if (r0 >= r13) goto L_0x00c5
            X.Euo r5 = new X.Euo
            r5.<init>(r3, r1)
        L_0x00c5:
            if (r6 >= r13) goto L_0x00ef
            X.Euo r1 = new X.Euo
            if (r5 != 0) goto L_0x00e9
            r1.<init>(r4, r3)
            return r1
        L_0x00cf:
            int r0 = r13 + -1
            int r1 = r0 >> 1
            java.lang.Object[] r0 = r2.A02
            r1 = r0[r1]
            goto L_0x00b8
        L_0x00d8:
            int r8 = X.C16620xQ.A01(r9, r10, r3)
            goto L_0x00ae
        L_0x00dd:
            if (r0 >= r13) goto L_0x00c5
            X.Euo r5 = new X.Euo
            java.lang.Object[] r0 = r12.A02
            r0 = r0[r13]
            r5.<init>(r3, r0)
            goto L_0x00c5
        L_0x00e9:
            java.lang.Object r0 = r5.A00
            r1.<init>(r4, r0)
            return r1
        L_0x00ef:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16610xP.A02(int):X.Euo");
    }

    public Iterator iterator() {
        return new AnonymousClass0rB(this);
    }

    public int size() {
        return this.A01;
    }

    public Object[] toArray() {
        int i = this.A01;
        Object[] objArr = new Object[i];
        System.arraycopy(this.A02, 0, objArr, 0, i);
        return objArr;
    }

    public C16610xP(C33101mv r4, int i) {
        C181810n A002 = C181810n.A00(r4.A00);
        this.A05 = new C16620xQ(this, A002);
        C16620xQ r1 = new C16620xQ(this, A002.A04());
        this.A04 = r1;
        C16620xQ r0 = this.A05;
        r0.A00 = r1;
        r1.A00 = r0;
        this.A02 = new Object[i];
    }

    public Object A03() {
        if (isEmpty()) {
            return null;
        }
        return this.A02[A00()];
    }

    public boolean add(Object obj) {
        offer(obj);
        return true;
    }

    public boolean addAll(Collection collection) {
        boolean z = false;
        for (Object offer : collection) {
            offer(offer);
            z = true;
        }
        return z;
    }

    public boolean offer(Object obj) {
        Object obj2;
        Preconditions.checkNotNull(obj);
        this.A00++;
        int i = this.A01;
        int i2 = i + 1;
        this.A01 = i2;
        Object[] objArr = this.A02;
        int length = objArr.length;
        if (i2 > length) {
            int i3 = (length + 1) << 1;
            if (length >= 64) {
                long j = ((long) (length >> 1)) * ((long) 3);
                i3 = (int) j;
                boolean z = false;
                if (j == ((long) i3)) {
                    z = true;
                }
                if (!z) {
                    throw new ArithmeticException("overflow");
                }
            }
            Object[] objArr2 = new Object[(Math.min(i3 - 1, this.A03) + 1)];
            System.arraycopy(objArr, 0, objArr2, 0, length);
            this.A02 = objArr2;
        }
        C16620xQ A012 = A01(i);
        int A013 = C16620xQ.A01(A012, i, obj);
        if (A013 == i) {
            A013 = i;
        } else {
            A012 = A012.A00;
        }
        A012.A02(A013, obj);
        if (this.A01 <= this.A03) {
            return true;
        }
        if (isEmpty()) {
            obj2 = null;
        } else {
            int A002 = A00();
            obj2 = this.A02[A002];
            A02(A002);
        }
        if (obj2 == obj) {
            return false;
        }
        return true;
    }

    public Object peek() {
        if (isEmpty()) {
            return null;
        }
        return this.A02[0];
    }

    public Object poll() {
        if (isEmpty()) {
            return null;
        }
        Object obj = this.A02[0];
        A02(0);
        return obj;
    }
}
