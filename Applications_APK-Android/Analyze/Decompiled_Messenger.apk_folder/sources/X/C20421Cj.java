package X;

import com.facebook.payments.logging.PaymentsLoggingSessionData;
import com.facebook.payments.model.PaymentItemType;
import com.facebook.payments.paymentmethods.model.PayPalBillingAgreement;
import com.facebook.payments.ui.ctabutton.PrimaryCtaButtonView;

/* renamed from: X.1Cj  reason: invalid class name and case insensitive filesystem */
public final class C20421Cj extends C06020ai {
    public final /* synthetic */ PaymentsLoggingSessionData A00;
    public final /* synthetic */ PaymentItemType A01;
    public final /* synthetic */ PayPalBillingAgreement A02;
    public final /* synthetic */ C55152nc A03;
    public final /* synthetic */ PrimaryCtaButtonView A04;

    public C20421Cj(C55152nc r1, PrimaryCtaButtonView primaryCtaButtonView, PaymentsLoggingSessionData paymentsLoggingSessionData, PaymentItemType paymentItemType, PayPalBillingAgreement payPalBillingAgreement) {
        this.A03 = r1;
        this.A04 = primaryCtaButtonView;
        this.A00 = paymentsLoggingSessionData;
        this.A01 = paymentItemType;
        this.A02 = payPalBillingAgreement;
    }
}
