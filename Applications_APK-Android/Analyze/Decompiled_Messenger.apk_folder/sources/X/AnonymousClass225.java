package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.facebook.common.build.BuildConstants;
import com.facebook.common.util.TriState;
import com.facebook.zero.activity.ZeroIntentInterstitialActivity;
import com.google.common.base.Preconditions;
import io.card.payment.BuildConfig;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.225  reason: invalid class name */
public final class AnonymousClass225 extends C403921k {
    private final AnonymousClass0US A00;
    private final C28141eK A01;
    private final C28141eK A02;
    private final C05720aD A03;
    private final Set A04;
    private final C04310Tq A05;

    private static Intent A00(Context context, Intent intent, int i, boolean z) {
        Intent intent2 = new Intent();
        intent2.setClass(context, ZeroIntentInterstitialActivity.class);
        intent2.putExtra("destination_intent", intent);
        intent2.putExtra("request_code", i);
        intent2.putExtra("start_for_result", z);
        intent2.putExtra("zero_feature_key_string", "url_interstitial");
        intent2.addFlags(intent.getFlags());
        intent2.addFlags(65536);
        return intent2;
    }

    private AnonymousClass461 A03(Intent intent) {
        if (((Boolean) this.A05.get()).booleanValue() && this.A03.A07("url_interstitial") && !intent.getBooleanExtra("zero_dialog_shown", false)) {
            Iterator it = this.A04.iterator();
            while (true) {
                if (it.hasNext()) {
                    TriState BFV = ((C98344n2) it.next()).BFV(intent);
                    if (BFV != TriState.YES) {
                        if (BFV == TriState.NO) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else if (intent.getComponent() == null || BuildConstants.A01().equals(intent.getComponent().getPackageName())) {
                    if (intent.getData() != null && C006306i.A07(intent.getData())) {
                        Uri A012 = C006306i.A01(intent.getData());
                        if (((C47812Yd) this.A00.get()).A05(A012)) {
                            Uri parse = Uri.parse(C47812Yd.A01((C47812Yd) this.A00.get(), A012.toString(), BuildConfig.FLAVOR));
                            Uri data = intent.getData();
                            Preconditions.checkArgument(C006306i.A07(data));
                            intent.setData(A01(data, "u", parse.toString()));
                            return AnonymousClass461.HANDLE_AFTER_REWRITE;
                        }
                    } else if ((intent.getData() == null || intent.getComponent() != null || !this.A03.A08(C05360Yq.$const$string(AnonymousClass1Y3.ABW))) && intent.getData() != null && ((C47812Yd) this.A00.get()).A05(intent.getData())) {
                        return AnonymousClass461.HANDLE_AFTER_REWRITE;
                    }
                }
            }
            return AnonymousClass461.HANDLE_BEHIND_DIALOG;
        }
        return AnonymousClass461.DONT_HANDLE;
    }

    public static final AnonymousClass225 A04(AnonymousClass1XY r7) {
        AnonymousClass1YA.A00(r7);
        return new AnonymousClass225(AnonymousClass0UQ.A00(AnonymousClass1Y3.AGU, r7), AnonymousClass0VG.A00(AnonymousClass1Y3.A7j, r7), new AnonymousClass0X5(r7, AnonymousClass0X6.A2x), C05720aD.A00(r7), AnonymousClass0WT.A00(r7), C04750Wa.A01(r7));
    }

    private AnonymousClass225(AnonymousClass0US r4, C04310Tq r5, Set set, C05720aD r7, C25051Yd r8, AnonymousClass09P r9) {
        this.A00 = r4;
        this.A05 = r5;
        this.A04 = set;
        this.A03 = r7;
        AnonymousClass21E r2 = new AnonymousClass21E(r9, "ZeroAwareExternalIntentHandler");
        this.A02 = new C14070sZ(C008807t.A00(r8.At0(566587790853843L)), r2);
        this.A01 = new C14130sf(C008807t.A00(r8.At0(566587790788306L)), r2);
    }

    public static Uri A01(Uri uri, String str, String str2) {
        Set<String> queryParameterNames = uri.getQueryParameterNames();
        Uri.Builder buildUpon = uri.buildUpon();
        buildUpon.query(null);
        boolean z = false;
        for (String next : queryParameterNames) {
            if (next.equals(str)) {
                z = true;
                buildUpon.appendQueryParameter(next, str2);
            } else {
                List<String> queryParameters = uri.getQueryParameters(next);
                if (queryParameters != null) {
                    for (String appendQueryParameter : queryParameters) {
                        buildUpon.appendQueryParameter(next, appendQueryParameter);
                    }
                }
            }
        }
        if (!z) {
            buildUpon.appendQueryParameter(str, str2);
        }
        return buildUpon.build();
    }

    private void A05(Intent intent) {
        if (intent.getData() != null) {
            intent.setDataAndType(Uri.parse(C47812Yd.A01((C47812Yd) this.A00.get(), intent.getData().toString(), BuildConfig.FLAVOR)), intent.getType());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r0 == null) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A09(android.content.Intent r6, int r7, android.app.Activity r8) {
        /*
            r5 = this;
            android.content.ComponentName r4 = r6.getComponent()
            X.461 r0 = r5.A03(r6)     // Catch:{ all -> 0x0045 }
            int r0 = r0.ordinal()     // Catch:{ all -> 0x0045 }
            r3 = 0
            r2 = 1
            switch(r0) {
                case 0: goto L_0x0012;
                case 1: goto L_0x001e;
                default: goto L_0x0011;
            }     // Catch:{ all -> 0x0045 }
        L_0x0011:
            goto L_0x002a
        L_0x0012:
            r5.A05(r6)     // Catch:{ all -> 0x0045 }
            X.1eK r0 = r5.A01     // Catch:{ all -> 0x0045 }
            android.content.Intent r0 = r0.A0A(r6, r8)     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0036
            goto L_0x002a
        L_0x001e:
            android.content.Intent r1 = A00(r8, r6, r7, r2)     // Catch:{ all -> 0x0045 }
            X.1eK r0 = r5.A02     // Catch:{ all -> 0x0045 }
            android.content.Intent r0 = r0.A0A(r1, r8)     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0036
        L_0x002a:
            android.content.ComponentName r0 = r6.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            return r3
        L_0x0036:
            r8.startActivityForResult(r0, r7)     // Catch:{ all -> 0x0045 }
            android.content.ComponentName r0 = r6.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            return r2
        L_0x0045:
            r1 = move-exception
            android.content.ComponentName r0 = r6.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass225.A09(android.content.Intent, int, android.app.Activity):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        if (r0 == null) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0A(android.content.Intent r7, int r8, androidx.fragment.app.Fragment r9) {
        /*
            r6 = this;
            android.content.ComponentName r4 = r7.getComponent()
            android.content.Context r5 = r9.A1j()
            X.461 r0 = r6.A03(r7)     // Catch:{ all -> 0x0049 }
            int r0 = r0.ordinal()     // Catch:{ all -> 0x0049 }
            r3 = 0
            r2 = 1
            switch(r0) {
                case 0: goto L_0x0016;
                case 1: goto L_0x0022;
                default: goto L_0x0015;
            }     // Catch:{ all -> 0x0049 }
        L_0x0015:
            goto L_0x002e
        L_0x0016:
            r6.A05(r7)     // Catch:{ all -> 0x0049 }
            X.1eK r0 = r6.A01     // Catch:{ all -> 0x0049 }
            android.content.Intent r0 = r0.A0A(r7, r5)     // Catch:{ all -> 0x0049 }
            if (r0 != 0) goto L_0x003a
            goto L_0x002e
        L_0x0022:
            android.content.Intent r1 = A00(r5, r7, r8, r2)     // Catch:{ all -> 0x0049 }
            X.1eK r0 = r6.A02     // Catch:{ all -> 0x0049 }
            android.content.Intent r0 = r0.A0A(r1, r5)     // Catch:{ all -> 0x0049 }
            if (r0 != 0) goto L_0x003a
        L_0x002e:
            android.content.ComponentName r0 = r7.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            return r3
        L_0x003a:
            r9.startActivityForResult(r0, r8)     // Catch:{ all -> 0x0049 }
            android.content.ComponentName r0 = r7.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            return r2
        L_0x0049:
            r1 = move-exception
            android.content.ComponentName r0 = r7.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass225.A0A(android.content.Intent, int, androidx.fragment.app.Fragment):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r0 == null) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0B(android.content.Intent r6, android.content.Context r7) {
        /*
            r5 = this;
            android.content.ComponentName r4 = r6.getComponent()
            X.461 r0 = r5.A03(r6)     // Catch:{ all -> 0x0045 }
            int r0 = r0.ordinal()     // Catch:{ all -> 0x0045 }
            r3 = 1
            r2 = 0
            switch(r0) {
                case 0: goto L_0x0012;
                case 1: goto L_0x001e;
                default: goto L_0x0011;
            }     // Catch:{ all -> 0x0045 }
        L_0x0011:
            goto L_0x002a
        L_0x0012:
            r5.A05(r6)     // Catch:{ all -> 0x0045 }
            X.1eK r0 = r5.A01     // Catch:{ all -> 0x0045 }
            android.content.Intent r0 = r0.A0A(r6, r7)     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0036
            goto L_0x002a
        L_0x001e:
            android.content.Intent r1 = A00(r7, r6, r2, r2)     // Catch:{ all -> 0x0045 }
            X.1eK r0 = r5.A02     // Catch:{ all -> 0x0045 }
            android.content.Intent r0 = r0.A0A(r1, r7)     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0036
        L_0x002a:
            android.content.ComponentName r0 = r6.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            return r2
        L_0x0036:
            r7.startActivity(r0)     // Catch:{ all -> 0x0045 }
            android.content.ComponentName r0 = r6.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            return r3
        L_0x0045:
            r1 = move-exception
            android.content.ComponentName r0 = r6.getComponent()
            boolean r0 = com.google.common.base.Objects.equal(r4, r0)
            com.google.common.base.Preconditions.checkState(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass225.A0B(android.content.Intent, android.content.Context):boolean");
    }
}
