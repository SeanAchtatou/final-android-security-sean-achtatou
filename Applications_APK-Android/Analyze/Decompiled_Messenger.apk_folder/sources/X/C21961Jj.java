package X;

import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1Jj  reason: invalid class name and case insensitive filesystem */
public final class C21961Jj extends C17770zR {
    @Comparable(type = 13)
    public C25998CqM A00;
    @Comparable(type = 13)
    public C204039jf A01;
    @Comparable(type = 13)
    public C203999jb A02;
    @Comparable(type = 13)
    public C121605oW A03;
    @Comparable(type = 13)
    public String A04;
    @Comparable(type = 13)
    public String A05;

    public C21961Jj() {
        super("AccountRegTwoFacComponent");
    }
}
