package X;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.0q9  reason: invalid class name and case insensitive filesystem */
public final class C12880q9 {
    public static boolean A00(ConnectivityManager connectivityManager) {
        if (Build.VERSION.SDK_INT >= 16) {
            return connectivityManager.isActiveNetworkMetered();
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            switch (activeNetworkInfo.getType()) {
                case 1:
                case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                case Process.SIGKILL:
                    return false;
            }
        }
        return true;
    }
}
