package X;

import com.facebook.common.connectionstatus.FbDataConnectionManager;
import com.facebook.proxygen.SamplePolicy;
import com.facebook.proxygen.TraceEventHandler;
import org.apache.http.protocol.HttpContext;

/* renamed from: X.1WB  reason: invalid class name */
public final class AnonymousClass1WB implements AnonymousClass1W7 {
    public final /* synthetic */ C06230bA A00;
    public final /* synthetic */ AnonymousClass0Ud A01;
    public final /* synthetic */ FbDataConnectionManager A02;
    public final /* synthetic */ AnonymousClass069 A03;
    public final /* synthetic */ AnonymousClass1W1 A04;
    public final /* synthetic */ AnonymousClass1W0 A05;
    public final /* synthetic */ AnonymousClass0US A06;

    public AnonymousClass1WB(C06230bA r1, FbDataConnectionManager fbDataConnectionManager, AnonymousClass1W1 r3, AnonymousClass069 r4, AnonymousClass1W0 r5, AnonymousClass0US r6, AnonymousClass0Ud r7) {
        this.A00 = r1;
        this.A02 = fbDataConnectionManager;
        this.A04 = r3;
        this.A03 = r4;
        this.A05 = r5;
        this.A06 = r6;
        this.A01 = r7;
    }

    public TraceEventHandler AbE(String str, HttpContext httpContext, AnonymousClass2ZJ r15, SamplePolicy samplePolicy) {
        C06230bA r2 = this.A00;
        this.A02.A08();
        return new A7Y(r2, r15, httpContext, samplePolicy, this.A04, this.A02, this.A03, this.A05, this.A06, this.A01);
    }
}
