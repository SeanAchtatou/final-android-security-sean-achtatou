package X;

import android.content.Context;
import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1YA  reason: invalid class name */
public final class AnonymousClass1YA extends AnonymousClass0UV {
    public static final Context A00(AnonymousClass1XY r0) {
        return A01(r0);
    }

    public static final Context A01(AnonymousClass1XY r2) {
        Context Aq3 = r2.getScopeAwareInjector().Aq3();
        if (Aq3 != null) {
            if (Aq3 != Aq3.getApplicationContext()) {
                boolean z = false;
                if ((1 & AnonymousClass0UO.A00().A00) != 0) {
                    z = true;
                }
                if (z) {
                    throw new C38471xU("Should not call getContext in singleton creation. Can lead to memory leaks.");
                }
            }
            return Aq3;
        }
        throw new RuntimeException();
    }

    public static final Context A02(AnonymousClass1XY r0) {
        Context Aq3 = r0.getScopeAwareInjector().Aq3();
        if (Aq3 != null) {
            return Aq3.getApplicationContext();
        }
        throw new RuntimeException();
    }

    public static final Context A03(AnonymousClass1XY r0) {
        Context Aq3 = r0.getScopeAwareInjector().Aq3();
        if (Aq3 != null) {
            return Aq3.getApplicationContext();
        }
        throw new RuntimeException();
    }
}
