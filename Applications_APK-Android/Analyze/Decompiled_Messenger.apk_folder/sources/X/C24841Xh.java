package X;

/* renamed from: X.1Xh  reason: invalid class name and case insensitive filesystem */
public abstract class C24841Xh extends C24791Xc {
    private final AnonymousClass1XX A00;

    public abstract Object getInstance(C22916BKm bKm);

    public abstract AnonymousClass0US getLazy(C22916BKm bKm);

    public abstract C04310Tq getProvider(C22916BKm bKm);

    public AnonymousClass1XY getApplicationInjector() {
        return this.A00.getApplicationInjector();
    }

    public C24811Xe getInjectorThreadStack() {
        return this.A00.getInjectorThreadStack();
    }

    public C24891Xn getScope(Class cls) {
        return this.A00.getScope(cls);
    }

    public C24851Xi getScopeAwareInjector() {
        if (!(this instanceof C24831Xg)) {
            return this.A00.getScopeAwareInjector();
        }
        return (C24831Xg) this;
    }

    public C24781Xb getScopeUnawareInjector() {
        return this.A00.getScopeUnawareInjector();
    }

    public C24841Xh(AnonymousClass1XX r1) {
        this.A00 = r1;
    }
}
