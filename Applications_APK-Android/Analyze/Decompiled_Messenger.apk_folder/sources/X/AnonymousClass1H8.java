package X;

import com.facebook.user.model.LastActive;
import com.facebook.user.model.UserKey;
import com.google.common.collect.ImmutableList;

/* renamed from: X.1H8  reason: invalid class name */
public final class AnonymousClass1H8 {
    public AnonymousClass0UN A00;

    public static final AnonymousClass1H8 A00(AnonymousClass1XY r1) {
        return new AnonymousClass1H8(r1);
    }

    public String A01(UserKey userKey) {
        AnonymousClass0r6 r1 = (AnonymousClass0r6) AnonymousClass1XX.A02(0, AnonymousClass1Y3.Auh, this.A00);
        LastActive A0M = r1.A0M(userKey);
        if (A0M == null) {
            return null;
        }
        return r1.A0O(A0M);
    }

    private AnonymousClass1H8(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(3, r3);
    }

    public String A02(ImmutableList immutableList) {
        if (immutableList.isEmpty()) {
            return null;
        }
        ImmutableList A02 = ((AnonymousClass1H6) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A8q, this.A00)).A02(immutableList, true);
        if (!A02.isEmpty()) {
            return A01((UserKey) A02.get(0));
        }
        return null;
    }
}
