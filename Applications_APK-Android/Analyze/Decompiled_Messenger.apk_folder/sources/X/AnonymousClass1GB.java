package X;

import com.facebook.inject.InjectorModule;

@InjectorModule
/* renamed from: X.1GB  reason: invalid class name */
public final class AnonymousClass1GB extends AnonymousClass0UV {
    private static C04470Uu A00;
    private static C04470Uu A01;

    public static final AnonymousClass1GC A00(AnonymousClass1XY r4) {
        AnonymousClass1GC r0;
        synchronized (AnonymousClass1GC.class) {
            C04470Uu A002 = C04470Uu.A00(A00);
            A00 = A002;
            try {
                if (A002.A03(r4)) {
                    A00.A00 = new AnonymousClass1GC((AnonymousClass1XY) A00.A01());
                }
                C04470Uu r1 = A00;
                r0 = (AnonymousClass1GC) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A00.A02();
                throw th;
            }
        }
        return r0;
    }

    public static final C24291Ta A01(AnonymousClass1XY r3) {
        C24291Ta r0;
        synchronized (C24291Ta.class) {
            C04470Uu A002 = C04470Uu.A00(A01);
            A01 = A002;
            try {
                if (A002.A03(r3)) {
                    A01.A00 = C24291Ta.A01((AnonymousClass1XY) A01.A01());
                }
                C04470Uu r1 = A01;
                r0 = (C24291Ta) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A01.A02();
                throw th;
            }
        }
        return r0;
    }
}
