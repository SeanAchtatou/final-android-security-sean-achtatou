package X;

import com.google.common.base.Predicate;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0UA  reason: invalid class name */
public final class AnonymousClass0UA extends AnonymousClass0UB<E> {
    public final /* synthetic */ Set A00;
    public final /* synthetic */ Predicate A01;
    public final /* synthetic */ Set A02;

    public AnonymousClass0UA(Set set, Predicate predicate, Set set2) {
        this.A02 = set;
        this.A01 = predicate;
        this.A00 = set2;
    }

    public boolean contains(Object obj) {
        if (!this.A02.contains(obj) || this.A00.contains(obj)) {
            return false;
        }
        return true;
    }

    public boolean isEmpty() {
        return this.A00.containsAll(this.A02);
    }

    public /* bridge */ /* synthetic */ Iterator iterator() {
        return iterator();
    }

    public int size() {
        return C24931Xr.A00(iterator());
    }
}
