package X;

import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/* renamed from: X.00J  reason: invalid class name */
public final class AnonymousClass00J {
    public static String A00;
    public static volatile AnonymousClass00K A01;

    /* JADX WARNING: Can't wrap try/catch for region: R(4:64|(2:66|67)|68|69) */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00e6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00e7, code lost:
        if (r2 != null) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x010e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:68:0x00ec */
    /* JADX WARNING: Missing exception handler attribute for start block: B:76:0x00f3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x0112 */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass00K A01(android.content.Context r12) {
        /*
            if (r12 != 0) goto L_0x0014
            java.lang.NullPointerException r2 = new java.lang.NullPointerException
            r2.<init>()
            r0 = 0
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r0 = "context is null"
            A03(r2, r0, r1)
            X.00K r0 = X.AnonymousClass0FJ.A00()
            return r0
        L_0x0014:
            X.00K r0 = X.AnonymousClass00J.A01
            if (r0 == 0) goto L_0x001b
            X.00K r0 = X.AnonymousClass00J.A01
            return r0
        L_0x001b:
            java.lang.Class<X.00J> r11 = X.AnonymousClass00J.class
            monitor-enter(r11)
            X.00K r0 = X.AnonymousClass00J.A01     // Catch:{ all -> 0x012c }
            if (r0 == 0) goto L_0x0026
            X.00K r0 = X.AnonymousClass00J.A01     // Catch:{ all -> 0x012c }
            monitor-exit(r11)     // Catch:{ all -> 0x012c }
            return r0
        L_0x0026:
            java.lang.String r5 = "startup_experiments"
            java.io.File r6 = r12.getFileStreamPath(r5)     // Catch:{ all -> 0x012c }
            boolean r0 = r6.exists()     // Catch:{ all -> 0x012c }
            if (r0 == 0) goto L_0x0124
            long r7 = r6.length()     // Catch:{ all -> 0x012c }
            r10 = 1
            r0 = 0
            r9 = 0
            r2 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r1 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x0053
            long r1 = r6.length()     // Catch:{ all -> 0x012c }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x012c }
            java.lang.Object[] r2 = new java.lang.Object[]{r1, r5}     // Catch:{ all -> 0x012c }
            java.lang.String r1 = "File too large: %d %s"
            A04(r1, r2)     // Catch:{ all -> 0x012c }
            goto L_0x0121
        L_0x0053:
            long r3 = r6.length()     // Catch:{ all -> 0x012c }
            int r2 = (int) r3     // Catch:{ all -> 0x012c }
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0113 }
            r7.<init>(r6)     // Catch:{ Exception -> 0x0113 }
            byte[] r8 = new byte[r2]     // Catch:{ all -> 0x010c }
            int r4 = java.lang.Math.min(r2, r2)     // Catch:{ all -> 0x010c }
            r3 = 0
            r2 = 0
        L_0x0065:
            if (r3 >= r4) goto L_0x0071
            int r1 = r4 - r3
            int r2 = r7.read(r8, r3, r1)     // Catch:{ all -> 0x010c }
            if (r2 < 0) goto L_0x0071
            int r3 = r3 + r2
            goto L_0x0065
        L_0x0071:
            r1 = -1
            if (r2 != r1) goto L_0x0079
            if (r3 != 0) goto L_0x0079
        L_0x0076:
            if (r1 > 0) goto L_0x008d
            goto L_0x007b
        L_0x0079:
            r1 = r3
            goto L_0x0076
        L_0x007b:
            java.lang.String r2 = "Unable to slurp file: %d %s"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x010c }
            java.lang.Object[] r1 = new java.lang.Object[]{r1, r5}     // Catch:{ all -> 0x010c }
            A04(r2, r1)     // Catch:{ all -> 0x010c }
            r7.close()     // Catch:{ Exception -> 0x0113 }
            goto L_0x0121
        L_0x008d:
            java.nio.ByteBuffer r1 = java.nio.ByteBuffer.wrap(r8, r9, r1)     // Catch:{ all -> 0x010c }
            X.00k r4 = A00(r1)     // Catch:{ all -> 0x010c }
            r7.close()     // Catch:{ Exception -> 0x0113 }
            int r2 = r4.A01     // Catch:{ all -> 0x012c }
            r1 = 3
            if (r2 < r1) goto L_0x0102
            java.lang.String r3 = "GKBOOTSTRAP_CRASH_DETECT"
            int r5 = X.AnonymousClass08X.A01(r12, r3, r9, r9)     // Catch:{ Exception -> 0x00f4 }
            if (r5 <= 0) goto L_0x0102
            int r2 = r4.A00     // Catch:{ Exception -> 0x00f4 }
            if (r5 > r2) goto L_0x00bb
            java.lang.String r3 = "Exceeded crash count: crashCount=%d maxCrashCount=%d"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x00f4 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00f4 }
            java.lang.Object[] r1 = new java.lang.Object[]{r2, r1}     // Catch:{ Exception -> 0x00f4 }
            A04(r3, r1)     // Catch:{ Exception -> 0x00f4 }
            goto L_0x0104
        L_0x00bb:
            r1 = 8
            java.nio.ByteBuffer r5 = java.nio.ByteBuffer.allocate(r1)     // Catch:{ Exception -> 0x00f4 }
            int r1 = r4.A00     // Catch:{ Exception -> 0x00f4 }
            int r1 = r1 + r10
            A06(r5, r4, r1)     // Catch:{ Exception -> 0x00f4 }
            r5.flip()     // Catch:{ Exception -> 0x00f4 }
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x00f4 }
            java.lang.String r1 = "rws"
            r3.<init>(r6, r1)     // Catch:{ Exception -> 0x00f4 }
            r1 = 0
            r3.seek(r1)     // Catch:{ all -> 0x00ed }
            java.nio.channels.FileChannel r2 = r3.getChannel()     // Catch:{ all -> 0x00ed }
            r2.write(r5)     // Catch:{ all -> 0x00e4 }
            r2.close()     // Catch:{ all -> 0x00ed }
            r3.close()     // Catch:{ Exception -> 0x00f4 }
            goto L_0x0102
        L_0x00e4:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x00e6 }
        L_0x00e6:
            r1 = move-exception
            if (r2 == 0) goto L_0x00ec
            r2.close()     // Catch:{ all -> 0x00ec }
        L_0x00ec:
            throw r1     // Catch:{ all -> 0x00ed }
        L_0x00ed:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x00ef }
        L_0x00ef:
            r1 = move-exception
            r3.close()     // Catch:{ all -> 0x00f3 }
        L_0x00f3:
            throw r1     // Catch:{ Exception -> 0x00f4 }
        L_0x00f4:
            r3 = move-exception
            java.lang.String r1 = r3.getMessage()     // Catch:{ all -> 0x012c }
            java.lang.Object[] r2 = new java.lang.Object[]{r1}     // Catch:{ all -> 0x012c }
            java.lang.String r1 = "Cannot handleCrashCount: %s"
            A03(r3, r1, r2)     // Catch:{ all -> 0x012c }
        L_0x0102:
            r1 = 0
            goto L_0x0105
        L_0x0104:
            r1 = 1
        L_0x0105:
            if (r1 != 0) goto L_0x0121
            java.lang.Object r0 = r4.A02     // Catch:{ all -> 0x012c }
            X.00K r0 = (X.AnonymousClass00K) r0     // Catch:{ all -> 0x012c }
            goto L_0x0121
        L_0x010c:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x010e }
        L_0x010e:
            r1 = move-exception
            r7.close()     // Catch:{ all -> 0x0112 }
        L_0x0112:
            throw r1     // Catch:{ Exception -> 0x0113 }
        L_0x0113:
            r3 = move-exception
            java.lang.String r1 = r3.getMessage()     // Catch:{ all -> 0x012c }
            java.lang.Object[] r2 = new java.lang.Object[]{r1, r5}     // Catch:{ all -> 0x012c }
            java.lang.String r1 = "Cannot read file: %s %s"
            A03(r3, r1, r2)     // Catch:{ all -> 0x012c }
        L_0x0121:
            if (r0 == 0) goto L_0x0124
            goto L_0x0128
        L_0x0124:
            X.00K r0 = X.AnonymousClass0FJ.A00()     // Catch:{ all -> 0x012c }
        L_0x0128:
            X.AnonymousClass00J.A01 = r0     // Catch:{ all -> 0x012c }
            monitor-exit(r11)     // Catch:{ all -> 0x012c }
            return r0
        L_0x012c:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x012c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00J.A01(android.content.Context):X.00K");
    }

    private static synchronized void A03(Exception exc, String str, Object... objArr) {
        synchronized (AnonymousClass00J.class) {
            String format = String.format(str, objArr);
            Log.e("FbColdStartExperimentsLoader", format, exc);
            if (A00 == null) {
                A00 = format;
            }
        }
    }

    private static synchronized void A04(String str, Object... objArr) {
        synchronized (AnonymousClass00J.class) {
            String format = String.format(str, objArr);
            Log.e("FbColdStartExperimentsLoader", format);
            if (A00 == null) {
                A00 = format;
            }
        }
    }

    public static void A06(ByteBuffer byteBuffer, C000600k r3, int i) {
        int i2 = r3.A01;
        if (i2 >= 3) {
            byteBuffer.putInt(i2);
            byteBuffer.putInt(i);
            return;
        }
        throw new IllegalArgumentException("Version to early to crash header");
    }

    public static String A02(ByteBuffer byteBuffer, Charset charset) {
        int i = byteBuffer.getInt();
        if (byteBuffer.hasArray()) {
            String str = new String(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), i, charset);
            byteBuffer.position(byteBuffer.position() + i);
            return str;
        }
        byte[] bArr = new byte[i];
        byteBuffer.get(bArr);
        return new String(bArr, charset);
    }

    public static void A05(ByteBuffer byteBuffer) {
        byteBuffer.position(byteBuffer.position() + byteBuffer.getInt());
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 1195 */
    /* JADX WARNING: Removed duplicated region for block: B:684:0x0ac3  */
    /* JADX WARNING: Removed duplicated region for block: B:721:0x0b44  */
    /* JADX WARNING: Removed duplicated region for block: B:816:0x0c83  */
    /* JADX WARNING: Removed duplicated region for block: B:866:0x0ccf A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:867:0x0ccf A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:874:0x0ccf A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C000600k A00(java.nio.ByteBuffer r194) {
        /*
            r7 = r194
            int r12 = r7.getInt()
            r3 = 1
            if (r12 < r3) goto L_0x0cd2
            r4 = 3
            if (r12 > r4) goto L_0x0cd2
            if (r12 < r4) goto L_0x0044
            int r22 = r7.getInt()
        L_0x0012:
            java.lang.String r0 = "UTF-8"
            java.nio.charset.Charset r11 = java.nio.charset.Charset.forName(r0)
            int r8 = r7.position()
            int r0 = r7.limit()
            r5 = 2
            int r0 = r0 + -2
            r7.position(r0)
            short r6 = r7.getShort()
            int r0 = r7.limit()
            int r1 = r0 + -2
            int r0 = r6 << 3
            int r1 = r1 - r0
            r7.position(r1)
            long[] r10 = new long[r6]
            r2 = 0
        L_0x0039:
            if (r2 >= r6) goto L_0x0047
            long r0 = r7.getLong()
            r10[r2] = r0
            int r2 = r2 + 1
            goto L_0x0039
        L_0x0044:
            r22 = 0
            goto L_0x0012
        L_0x0047:
            r7.position(r8)
            r24 = 0
            r25 = 1
            r28 = 0
            r29 = 0
            r30 = 0
            r32 = 0
            r37 = 0
            r40 = 0
            r41 = 0
            r42 = 0
            r43 = 1
            r45 = 0
            r46 = 0
            r47 = 0
            r48 = 0
            r49 = 0
            r51 = 0
            r52 = 0
            r53 = 0
            r54 = 0
            r55 = 0
            r56 = 0
            r57 = 0
            r58 = 0
            r59 = 0
            r60 = 0
            r61 = 0
            r62 = 0
            r65 = 0
            r66 = 0
            r67 = 0
            r68 = 0
            r69 = 0
            r70 = 0
            r71 = 0
            r72 = 0
            r73 = 1
            r76 = 1
            r77 = 1
            r78 = 1
            r81 = 1
            r82 = 1
            r86 = 0
            r94 = 0
            r95 = 0
            r97 = 0
            r100 = 0
            r103 = 0
            r105 = 0
            r106 = 0
            r107 = 0
            r108 = 0
            r109 = 0
            r110 = 0
            r111 = 0
            r112 = 0
            r113 = 0
            r114 = 0
            r115 = 0
            r116 = 0
            r117 = 0
            r118 = 0
            r119 = 0
            r120 = 0
            r121 = 0
            r122 = 0
            r123 = 0
            r125 = 0
            r128 = 0
            r129 = 0
            r133 = 0
            r134 = 0
            r135 = 0
            r136 = 0
            r137 = 0
            r138 = 0
            r139 = 0
            r140 = 0
            r141 = 0
            r142 = 0
            r143 = 0
            r145 = 0
            r147 = 0
            r149 = 0
            r150 = 0
            r151 = 0
            r152 = 0
            r153 = 0
            r156 = 0
            r157 = 0
            r158 = 0
            r162 = 0
            r163 = 0
            r169 = 0
            r170 = 0
            r171 = 0
            r172 = 0
            r176 = 0
            r177 = 0
            r178 = 0
            r179 = 0
            r180 = 0
            r181 = 0
            r185 = 0
            r186 = 0
            r189 = 0
            r190 = 0
            r191 = 0
        L_0x0122:
            short r1 = r7.getShort()
            r18 = -2086642317469965836(0xe30ac1de25e7a1f4, double:-1.2622628693082689E169)
            r16 = -2568163647383178380(0xdc5c0cc4f8a77374, double:-8.155095756380556E136)
            r13 = -4776474633669163571(0xbdb68da0a57c45cd, double:-2.0512045484748224E-11)
            r9 = 5
            r8 = 6
            r6 = 4
            r0 = -1
            if (r1 != r0) goto L_0x0529
            r26 = 20000(0x4e20, float:2.8026E-41)
            r27 = -1
            r33 = 0
            r34 = 0
            r35 = 0
            r36 = 0
            r38 = 0
            r39 = 0
            r44 = 0
            r50 = 0
            r85 = -1
            r91 = 0
            r96 = 1
            r98 = 0
            r99 = 0
            r101 = 0
            r102 = 0
            r104 = 3
            r124 = 0
            r126 = 0
            r127 = 0
            r130 = -1
            r131 = -1
            r132 = 0
            r144 = 19
            r146 = 0
            r148 = 0
            r154 = 0
            r155 = 0
            r159 = 0
            r166 = 1
            r167 = 0
            r168 = 0
            r173 = 0
            r174 = 0
            r175 = 0
            r182 = 0
        L_0x0185:
            short r1 = r7.getShort()
            if (r1 != r0) goto L_0x0303
            r74 = 1
            r79 = 1
            r63 = 60000(0xea60, double:2.9644E-319)
        L_0x0192:
            short r1 = r7.getShort()
            if (r1 != r0) goto L_0x02a9
            r89 = 0
            r92 = 0
            r183 = 0
            r160 = 0
            r164 = 4608083138725491507(0x3ff3333333333333, double:1.2)
            r83 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r87 = -4616189618054758400(0xbff0000000000000, double:-1.0)
        L_0x01a9:
            short r1 = r7.getShort()
            if (r1 != r0) goto L_0x0239
            java.lang.String r187 = ""
            if (r12 < r5) goto L_0x022e
            r6 = r187
            r188 = r6
            r192 = r6
            r193 = r6
            r194 = r6
        L_0x01bd:
            short r1 = r7.getShort()
            if (r1 != r0) goto L_0x01d6
            r31 = r187
            r187 = r6
        L_0x01c7:
            X.00K r0 = new X.00K
            r23 = r0
            r23.<init>(r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r65, r66, r67, r68, r69, r70, r71, r72, r73, r74, r76, r77, r78, r79, r81, r82, r83, r85, r86, r87, r89, r91, r92, r94, r95, r96, r97, r98, r99, r100, r101, r102, r103, r104, r105, r106, r107, r108, r109, r110, r111, r112, r113, r114, r115, r116, r117, r118, r119, r120, r121, r122, r123, r124, r125, r126, r127, r128, r129, r130, r131, r132, r133, r134, r135, r136, r137, r138, r139, r140, r141, r142, r143, r144, r145, r146, r147, r148, r149, r150, r151, r152, r153, r154, r155, r156, r157, r158, r159, r160, r162, r163, r164, r166, r167, r168, r169, r170, r171, r172, r173, r174, r175, r176, r177, r178, r179, r180, r181, r182, r183, r185, r186, r187, r188, r189, r190, r191, r192, r193, r194)
            X.00k r2 = new X.00k
            r1 = r22
            r2.<init>(r12, r1, r0)
            return r2
        L_0x01d6:
            r1 = r10[r1]
            r4 = 8401887318822129211(0x7499800059408e3b, double:4.6738700945639036E253)
            int r8 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r8 == 0) goto L_0x021d
            int r4 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r4 != 0) goto L_0x0213
        L_0x01e5:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x01bd
            r2 = 16
            if (r1 == r2) goto L_0x020e
            r2 = 17
            if (r1 == r2) goto L_0x0209
            switch(r1) {
                case 22: goto L_0x01fa;
                case 23: goto L_0x01ff;
                case 24: goto L_0x0204;
                default: goto L_0x01f6;
            }
        L_0x01f6:
            A05(r7)
            goto L_0x01e5
        L_0x01fa:
            java.lang.String r192 = A02(r7, r11)
            goto L_0x01e5
        L_0x01ff:
            java.lang.String r193 = A02(r7, r11)
            goto L_0x01e5
        L_0x0204:
            java.lang.String r194 = A02(r7, r11)
            goto L_0x01e5
        L_0x0209:
            java.lang.String r188 = A02(r7, r11)
            goto L_0x01e5
        L_0x020e:
            java.lang.String r6 = A02(r7, r11)
            goto L_0x01e5
        L_0x0213:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x01bd
            A05(r7)
            goto L_0x0213
        L_0x021d:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x01bd
            if (r1 == r3) goto L_0x0229
            A05(r7)
            goto L_0x021d
        L_0x0229:
            java.lang.String r187 = A02(r7, r11)
            goto L_0x021d
        L_0x022e:
            r188 = r187
            r192 = r187
            r193 = r187
            r31 = r187
            r194 = r187
            goto L_0x01c7
        L_0x0239:
            r1 = r10[r1]
            int r4 = (r1 > r16 ? 1 : (r1 == r16 ? 0 : -1))
            if (r4 == 0) goto L_0x0280
            int r4 = (r1 > r18 ? 1 : (r1 == r18 ? 0 : -1))
            if (r4 == 0) goto L_0x0264
            int r4 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r4 != 0) goto L_0x025a
        L_0x0247:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x01a9
            r2 = 13
            if (r1 == r2) goto L_0x0255
            r7.getDouble()
            goto L_0x0247
        L_0x0255:
            double r183 = r7.getDouble()
            goto L_0x0247
        L_0x025a:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x01a9
            r7.getDouble()
            goto L_0x025a
        L_0x0264:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x01a9
            r2 = 14
            if (r1 == r2) goto L_0x027b
            r2 = 17
            if (r1 == r2) goto L_0x0276
            r7.getDouble()
            goto L_0x0264
        L_0x0276:
            double r164 = r7.getDouble()
            goto L_0x0264
        L_0x027b:
            double r160 = r7.getDouble()
            goto L_0x0264
        L_0x0280:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x01a9
            if (r1 == r3) goto L_0x02a4
            r2 = 9
            if (r1 == r2) goto L_0x029f
            if (r1 == r8) goto L_0x029a
            r2 = 7
            if (r1 == r2) goto L_0x0295
            r7.getDouble()
            goto L_0x0280
        L_0x0295:
            double r89 = r7.getDouble()
            goto L_0x0280
        L_0x029a:
            double r87 = r7.getDouble()
            goto L_0x0280
        L_0x029f:
            double r92 = r7.getDouble()
            goto L_0x0280
        L_0x02a4:
            double r83 = r7.getDouble()
            goto L_0x0280
        L_0x02a9:
            r1 = r10[r1]
            r20 = -256995823427616551(0xfc6ef7a903d680d9, double:-2.4142980677819806E291)
            int r4 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r4 == 0) goto L_0x02f2
            r20 = 3497953107691115171(0x308b39d188e712a3, double:7.524065915390022E-75)
            int r4 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r4 == 0) goto L_0x02e1
            r20 = 7077860196793471229(0x62399c594fc050fd, double:1.4748224193642524E165)
            int r4 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r4 != 0) goto L_0x02d7
        L_0x02c6:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0192
            if (r1 == r5) goto L_0x02d2
            r7.getLong()
            goto L_0x02c6
        L_0x02d2:
            long r79 = r7.getLong()
            goto L_0x02c6
        L_0x02d7:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0192
            r7.getLong()
            goto L_0x02d7
        L_0x02e1:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0192
            if (r1 == r5) goto L_0x02ed
            r7.getLong()
            goto L_0x02e1
        L_0x02ed:
            long r74 = r7.getLong()
            goto L_0x02e1
        L_0x02f2:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0192
            if (r1 == r8) goto L_0x02fe
            r7.getLong()
            goto L_0x02f2
        L_0x02fe:
            long r63 = r7.getLong()
            goto L_0x02f2
        L_0x0303:
            r1 = r10[r1]
            r20 = 8448581745870077061(0x753f6455da6f7c85, double:5.8918870106918075E256)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0511
            r20 = -210475169871049954(0xfd143df2b9cbcb1e, double:-3.2319815608578734E294)
            r15 = 8
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x04dd
            r20 = 4892948164530689018(0x43e73e6e834053fa, double:1.3399180971419357E19)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x04cc
            r20 = -8078472953883366716(0x8fe37fa6682e6ac4, double:-3.9247998572796246E-232)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x04bb
            int r20 = (r1 > r16 ? 1 : (r1 == r16 ? 0 : -1))
            if (r20 == 0) goto L_0x04a3
            r20 = 1248908708094389307(0x115503da5edbac3b, double:3.5484092001748626E-225)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x048b
            r20 = 4495419830547305445(0x3e62f084a404d3e5, double:3.527761238895735E-8)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x046c
            r20 = -151458577822322484(0xfde5e939d6b6f0cc, double:-2.8659505206888374E298)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x045b
            r20 = -5918643744588913870(0xaddcc0df63b0a332, double:-9.033802913171843E-88)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x043c
            r20 = 2467489591076646141(0x223e48b5b318ecfd, double:9.700980725302096E-144)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x0422
            r20 = -6754720465236801728(0xa2426986607fc740, double:-1.1796085676194033E-143)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x0411
            r20 = 6777071730784209483(0x5e0cfed52abc0a4b, double:1.1314559164265081E145)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x03fe
            r20 = -5994192921154690785(0xacd0594bfb69e11f, double:-7.837682035766678E-93)
            int r23 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r23 == 0) goto L_0x03ed
            int r20 = (r1 > r18 ? 1 : (r1 == r18 ? 0 : -1))
            if (r20 == 0) goto L_0x03b1
            int r15 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r15 != 0) goto L_0x03a7
        L_0x037f:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r3) goto L_0x03a2
            if (r1 == r5) goto L_0x039d
            if (r1 == r4) goto L_0x0398
            r2 = 12
            if (r1 == r2) goto L_0x0393
            r7.getInt()
            goto L_0x037f
        L_0x0393:
            int r182 = r7.getInt()
            goto L_0x037f
        L_0x0398:
            int r175 = r7.getInt()
            goto L_0x037f
        L_0x039d:
            int r173 = r7.getInt()
            goto L_0x037f
        L_0x03a2:
            int r174 = r7.getInt()
            goto L_0x037f
        L_0x03a7:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            r7.getInt()
            goto L_0x03a7
        L_0x03b1:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r5) goto L_0x03e8
            r2 = 13
            if (r1 == r2) goto L_0x03e3
            if (r1 == r15) goto L_0x03de
            r2 = 9
            if (r1 == r2) goto L_0x03d9
            switch(r1) {
                case 18: goto L_0x03ca;
                case 19: goto L_0x03cf;
                case 20: goto L_0x03d4;
                default: goto L_0x03c6;
            }
        L_0x03c6:
            r7.getInt()
            goto L_0x03b1
        L_0x03ca:
            int r166 = r7.getInt()
            goto L_0x03b1
        L_0x03cf:
            int r167 = r7.getInt()
            goto L_0x03b1
        L_0x03d4:
            int r168 = r7.getInt()
            goto L_0x03b1
        L_0x03d9:
            int r155 = r7.getInt()
            goto L_0x03b1
        L_0x03de:
            int r154 = r7.getInt()
            goto L_0x03b1
        L_0x03e3:
            int r159 = r7.getInt()
            goto L_0x03b1
        L_0x03e8:
            int r148 = r7.getInt()
            goto L_0x03b1
        L_0x03ed:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r4) goto L_0x03f9
            r7.getInt()
            goto L_0x03ed
        L_0x03f9:
            int r146 = r7.getInt()
            goto L_0x03ed
        L_0x03fe:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            r2 = 11
            if (r1 == r2) goto L_0x040c
            r7.getInt()
            goto L_0x03fe
        L_0x040c:
            int r144 = r7.getInt()
            goto L_0x03fe
        L_0x0411:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r3) goto L_0x041d
            r7.getInt()
            goto L_0x0411
        L_0x041d:
            int r132 = r7.getInt()
            goto L_0x0411
        L_0x0422:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r4) goto L_0x0437
            r2 = 10
            if (r1 == r2) goto L_0x0432
            r7.getInt()
            goto L_0x0422
        L_0x0432:
            int r131 = r7.getInt()
            goto L_0x0422
        L_0x0437:
            int r130 = r7.getInt()
            goto L_0x0422
        L_0x043c:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r4) goto L_0x0456
            if (r1 == r9) goto L_0x0451
            if (r1 == r8) goto L_0x044c
            r7.getInt()
            goto L_0x043c
        L_0x044c:
            int r127 = r7.getInt()
            goto L_0x043c
        L_0x0451:
            int r126 = r7.getInt()
            goto L_0x043c
        L_0x0456:
            int r124 = r7.getInt()
            goto L_0x043c
        L_0x045b:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r5) goto L_0x0467
            r7.getInt()
            goto L_0x045b
        L_0x0467:
            int r104 = r7.getInt()
            goto L_0x045b
        L_0x046c:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r3) goto L_0x0486
            if (r1 == r4) goto L_0x0481
            if (r1 == r6) goto L_0x047c
            r7.getInt()
            goto L_0x046c
        L_0x047c:
            int r102 = r7.getInt()
            goto L_0x046c
        L_0x0481:
            int r101 = r7.getInt()
            goto L_0x046c
        L_0x0486:
            int r99 = r7.getInt()
            goto L_0x046c
        L_0x048b:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r5) goto L_0x049e
            if (r1 == r6) goto L_0x0499
            r7.getInt()
            goto L_0x048b
        L_0x0499:
            int r98 = r7.getInt()
            goto L_0x048b
        L_0x049e:
            int r96 = r7.getInt()
            goto L_0x048b
        L_0x04a3:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r5) goto L_0x04b6
            if (r1 == r15) goto L_0x04b1
            r7.getInt()
            goto L_0x04a3
        L_0x04b1:
            int r91 = r7.getInt()
            goto L_0x04a3
        L_0x04b6:
            int r85 = r7.getInt()
            goto L_0x04a3
        L_0x04bb:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r6) goto L_0x04c7
            r7.getInt()
            goto L_0x04bb
        L_0x04c7:
            int r50 = r7.getInt()
            goto L_0x04bb
        L_0x04cc:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r3) goto L_0x04d8
            r7.getInt()
            goto L_0x04cc
        L_0x04d8:
            int r44 = r7.getInt()
            goto L_0x04cc
        L_0x04dd:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r5) goto L_0x050c
            if (r1 == r4) goto L_0x0507
            if (r1 == r6) goto L_0x0502
            if (r1 == r9) goto L_0x04fd
            if (r1 == r8) goto L_0x04f8
            if (r1 == r15) goto L_0x04f3
            r7.getInt()
            goto L_0x04dd
        L_0x04f3:
            int r39 = r7.getInt()
            goto L_0x04dd
        L_0x04f8:
            int r33 = r7.getInt()
            goto L_0x04dd
        L_0x04fd:
            int r35 = r7.getInt()
            goto L_0x04dd
        L_0x0502:
            int r38 = r7.getInt()
            goto L_0x04dd
        L_0x0507:
            int r34 = r7.getInt()
            goto L_0x04dd
        L_0x050c:
            int r36 = r7.getInt()
            goto L_0x04dd
        L_0x0511:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0185
            if (r1 == r5) goto L_0x0524
            if (r1 == r4) goto L_0x051f
            r7.getInt()
            goto L_0x0511
        L_0x051f:
            int r27 = r7.getInt()
            goto L_0x0511
        L_0x0524:
            int r26 = r7.getInt()
            goto L_0x0511
        L_0x0529:
            r1 = r10[r1]
            r20 = 8448581745870077061(0x753f6455da6f7c85, double:5.8918870106918075E256)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0cab
            r20 = -7810065979679914640(0x939d125ef8d0c570, double:-3.3732959535495874E-214)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0c7d
            r20 = 8401887318822129211(0x7499800059408e3b, double:4.6738700945639036E253)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0c66
            r20 = -210475169871049954(0xfd143df2b9cbcb1e, double:-3.2319815608578734E294)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0c31
            r20 = -583631376941802297(0xf7e6865ad99c9cc7, double:-3.718669127529469E269)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0c1a
            r20 = -5316278372034984862(0xb638c8fccbc34462, double:-1.6958658633674277E-47)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0c03
            r20 = 4892948164530689018(0x43e73e6e834053fa, double:1.3399180971419357E19)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0bdf
            r20 = -8078472953883366716(0x8fe37fa6682e6ac4, double:-3.9247998572796246E-232)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0bae
            r20 = -137829240979293785(0xfe1655091bc479a7, double:-2.3368274045961628E299)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0b3e
            r20 = -256995823427616551(0xfc6ef7a903d680d9, double:-2.4142980677819806E291)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0abd
            r20 = 9212754848718578216(0x7fda47b940a0e228, double:7.381825320554681E307)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0aa6
            r20 = 3497953107691115171(0x308b39d188e712a3, double:7.524065915390022E-75)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0a75
            r20 = 7077860196793471229(0x62399c594fc050fd, double:1.4748224193642524E165)
            int r15 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r15 == 0) goto L_0x0a44
            int r15 = (r1 > r16 ? 1 : (r1 == r16 ? 0 : -1))
            if (r15 == 0) goto L_0x0a2d
            r15 = 1530191552554774366(0x153c551c1854af5e, double:2.2062121228840596E-206)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x0a16
            r15 = 1248908708094389307(0x115503da5edbac3b, double:3.5484092001748626E-225)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x09f2
            r15 = 4495419830547305445(0x3e62f084a404d3e5, double:3.527761238895735E-8)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x09db
            r15 = 8867944669082615948(0x7b1144b1307d008c, double:6.419574187948456E284)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x09c4
            r15 = -151458577822322484(0xfde5e939d6b6f0cc, double:-2.8659505206888374E298)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x0986
            r15 = -905490430449379174(0xf36f0d40418c989a, double:-1.0855571537905258E248)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x0948
            r15 = -2596169838136401405(0xdbf88d498d4be203, double:-1.1153296415712993E135)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x0917
            r15 = 9222332083119924944(0x7ffc4e2ae70d26d0, double:NaN)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x08e6
            r15 = -3523316742381704115(0xcf1aaa171326ec4d, double:-1.1777985664727822E73)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x08b5
            r15 = -5918643744588913870(0xaddcc0df63b0a332, double:-9.033802913171843E-88)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x0884
            r15 = 9147141664028029706(0x7ef12ce13a90f30a, double:2.944559093972289E303)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x0860
            r15 = 6518910297612815478(0x5a77d26083d90c76, double:6.450189637774505E127)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x0849
            r15 = 6777071730784209483(0x5e0cfed52abc0a4b, double:1.1314559164265081E145)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x07ce
            r15 = -5994192921154690785(0xacd0594bfb69e11f, double:-7.837682035766678E-93)
            int r17 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x07b7
            int r15 = (r1 > r18 ? 1 : (r1 == r18 ? 0 : -1))
            if (r15 == 0) goto L_0x06fb
            r15 = -3829005244898204780(0xcadca40446bc2394, double:-4.2863017673258575E52)
            int r4 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r4 == 0) goto L_0x06e4
            r15 = 4942223800506374986(0x44964e5c8da9db4a, double:2.6334394458566012E22)
            int r4 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r4 == 0) goto L_0x06cd
            int r4 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r4 != 0) goto L_0x06c3
        L_0x063c:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            switch(r1) {
                case 4: goto L_0x0649;
                case 5: goto L_0x0654;
                case 6: goto L_0x065f;
                case 7: goto L_0x066a;
                case 8: goto L_0x0645;
                case 9: goto L_0x0645;
                case 10: goto L_0x0675;
                case 11: goto L_0x0680;
                case 12: goto L_0x0645;
                case 13: goto L_0x0645;
                case 14: goto L_0x068b;
                case 15: goto L_0x0696;
                case 16: goto L_0x0645;
                case 17: goto L_0x0645;
                case 18: goto L_0x06a1;
                case 19: goto L_0x06ac;
                case 20: goto L_0x06b7;
                default: goto L_0x0645;
            }
        L_0x0645:
            r7.get()
            goto L_0x063c
        L_0x0649:
            byte r1 = r7.get()
            r176 = 0
            if (r1 == 0) goto L_0x063c
            r176 = 1
            goto L_0x063c
        L_0x0654:
            byte r1 = r7.get()
            r177 = 0
            if (r1 == 0) goto L_0x063c
            r177 = 1
            goto L_0x063c
        L_0x065f:
            byte r1 = r7.get()
            r178 = 0
            if (r1 == 0) goto L_0x063c
            r178 = 1
            goto L_0x063c
        L_0x066a:
            byte r1 = r7.get()
            r179 = 0
            if (r1 == 0) goto L_0x063c
            r179 = 1
            goto L_0x063c
        L_0x0675:
            byte r1 = r7.get()
            r180 = 0
            if (r1 == 0) goto L_0x063c
            r180 = 1
            goto L_0x063c
        L_0x0680:
            byte r1 = r7.get()
            r181 = 0
            if (r1 == 0) goto L_0x063c
            r181 = 1
            goto L_0x063c
        L_0x068b:
            byte r1 = r7.get()
            r185 = 0
            if (r1 == 0) goto L_0x063c
            r185 = 1
            goto L_0x063c
        L_0x0696:
            byte r1 = r7.get()
            r186 = 0
            if (r1 == 0) goto L_0x063c
            r186 = 1
            goto L_0x063c
        L_0x06a1:
            byte r1 = r7.get()
            r189 = 0
            if (r1 == 0) goto L_0x063c
            r189 = 1
            goto L_0x063c
        L_0x06ac:
            byte r1 = r7.get()
            r190 = 0
            if (r1 == 0) goto L_0x063c
            r190 = 1
            goto L_0x063c
        L_0x06b7:
            byte r1 = r7.get()
            r191 = 0
            if (r1 == 0) goto L_0x063c
            r191 = 1
            goto L_0x063c
        L_0x06c3:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            r7.get()
            goto L_0x06c3
        L_0x06cd:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r8) goto L_0x06d9
            r7.get()
            goto L_0x06cd
        L_0x06d9:
            byte r1 = r7.get()
            r172 = 0
            if (r1 == 0) goto L_0x06cd
            r172 = 1
            goto L_0x06cd
        L_0x06e4:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r5) goto L_0x06f0
            r7.get()
            goto L_0x06e4
        L_0x06f0:
            byte r1 = r7.get()
            r171 = 0
            if (r1 == 0) goto L_0x06e4
            r171 = 1
            goto L_0x06e4
        L_0x06fb:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x07ab
            if (r1 == r4) goto L_0x079f
            if (r1 == r6) goto L_0x0794
            if (r1 == r9) goto L_0x0789
            if (r1 == r8) goto L_0x077e
            r2 = 7
            if (r1 == r2) goto L_0x0773
            r2 = 15
            if (r1 == r2) goto L_0x0768
            r2 = 16
            if (r1 == r2) goto L_0x075d
            r2 = 21
            if (r1 == r2) goto L_0x0752
            r2 = 22
            if (r1 == r2) goto L_0x0747
            switch(r1) {
                case 10: goto L_0x0726;
                case 11: goto L_0x0731;
                case 12: goto L_0x073c;
                default: goto L_0x0721;
            }
        L_0x0721:
            r7.get()
        L_0x0724:
            r4 = 3
            goto L_0x06fb
        L_0x0726:
            byte r1 = r7.get()
            r156 = 0
            if (r1 == 0) goto L_0x0724
            r156 = 1
            goto L_0x0724
        L_0x0731:
            byte r1 = r7.get()
            r157 = 0
            if (r1 == 0) goto L_0x0724
            r157 = 1
            goto L_0x0724
        L_0x073c:
            byte r1 = r7.get()
            r158 = 0
            if (r1 == 0) goto L_0x0724
            r158 = 1
            goto L_0x0724
        L_0x0747:
            byte r1 = r7.get()
            r170 = 0
            if (r1 == 0) goto L_0x0724
            r170 = 1
            goto L_0x0724
        L_0x0752:
            byte r1 = r7.get()
            r169 = 0
            if (r1 == 0) goto L_0x0724
            r169 = 1
            goto L_0x0724
        L_0x075d:
            byte r1 = r7.get()
            r163 = 0
            if (r1 == 0) goto L_0x0724
            r163 = 1
            goto L_0x0724
        L_0x0768:
            byte r1 = r7.get()
            r162 = 0
            if (r1 == 0) goto L_0x0724
            r162 = 1
            goto L_0x0724
        L_0x0773:
            byte r1 = r7.get()
            r153 = 0
            if (r1 == 0) goto L_0x0724
            r153 = 1
            goto L_0x0724
        L_0x077e:
            byte r1 = r7.get()
            r152 = 0
            if (r1 == 0) goto L_0x0724
            r152 = 1
            goto L_0x0724
        L_0x0789:
            byte r1 = r7.get()
            r151 = 0
            if (r1 == 0) goto L_0x0724
            r151 = 1
            goto L_0x0724
        L_0x0794:
            byte r1 = r7.get()
            r150 = 0
            if (r1 == 0) goto L_0x0724
            r150 = 1
            goto L_0x0724
        L_0x079f:
            byte r1 = r7.get()
            r149 = 0
            if (r1 == 0) goto L_0x0724
            r149 = 1
            goto L_0x0724
        L_0x07ab:
            byte r1 = r7.get()
            r147 = 0
            if (r1 == 0) goto L_0x0724
            r147 = 1
            goto L_0x0724
        L_0x07b7:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x07c3
            r7.get()
            goto L_0x07b7
        L_0x07c3:
            byte r1 = r7.get()
            r145 = 0
            if (r1 == 0) goto L_0x07b7
            r145 = 1
            goto L_0x07b7
        L_0x07ce:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            switch(r1) {
                case 1: goto L_0x07db;
                case 2: goto L_0x07e6;
                case 3: goto L_0x07f1;
                case 4: goto L_0x07fc;
                case 5: goto L_0x0807;
                case 6: goto L_0x0812;
                case 7: goto L_0x081d;
                case 8: goto L_0x0828;
                case 9: goto L_0x0833;
                case 10: goto L_0x083e;
                default: goto L_0x07d7;
            }
        L_0x07d7:
            r7.get()
            goto L_0x07ce
        L_0x07db:
            byte r1 = r7.get()
            r134 = 0
            if (r1 == 0) goto L_0x07ce
            r134 = 1
            goto L_0x07ce
        L_0x07e6:
            byte r1 = r7.get()
            r135 = 0
            if (r1 == 0) goto L_0x07ce
            r135 = 1
            goto L_0x07ce
        L_0x07f1:
            byte r1 = r7.get()
            r136 = 0
            if (r1 == 0) goto L_0x07ce
            r136 = 1
            goto L_0x07ce
        L_0x07fc:
            byte r1 = r7.get()
            r137 = 0
            if (r1 == 0) goto L_0x07ce
            r137 = 1
            goto L_0x07ce
        L_0x0807:
            byte r1 = r7.get()
            r138 = 0
            if (r1 == 0) goto L_0x07ce
            r138 = 1
            goto L_0x07ce
        L_0x0812:
            byte r1 = r7.get()
            r139 = 0
            if (r1 == 0) goto L_0x07ce
            r139 = 1
            goto L_0x07ce
        L_0x081d:
            byte r1 = r7.get()
            r140 = 0
            if (r1 == 0) goto L_0x07ce
            r140 = 1
            goto L_0x07ce
        L_0x0828:
            byte r1 = r7.get()
            r141 = 0
            if (r1 == 0) goto L_0x07ce
            r141 = 1
            goto L_0x07ce
        L_0x0833:
            byte r1 = r7.get()
            r142 = 0
            if (r1 == 0) goto L_0x07ce
            r142 = 1
            goto L_0x07ce
        L_0x083e:
            byte r1 = r7.get()
            r143 = 0
            if (r1 == 0) goto L_0x07ce
            r143 = 1
            goto L_0x07ce
        L_0x0849:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0855
            r7.get()
            goto L_0x0849
        L_0x0855:
            byte r1 = r7.get()
            r133 = 0
            if (r1 == 0) goto L_0x0849
            r133 = 1
            goto L_0x0849
        L_0x0860:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0879
            if (r1 == r5) goto L_0x086e
            r7.get()
            goto L_0x0860
        L_0x086e:
            byte r1 = r7.get()
            r129 = 0
            if (r1 == 0) goto L_0x0860
            r129 = 1
            goto L_0x0860
        L_0x0879:
            byte r1 = r7.get()
            r128 = 0
            if (r1 == 0) goto L_0x0860
            r128 = 1
            goto L_0x0860
        L_0x0884:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x08aa
            if (r1 == r5) goto L_0x089f
            if (r1 == r6) goto L_0x0894
            r7.get()
            goto L_0x0884
        L_0x0894:
            byte r1 = r7.get()
            r125 = 0
            if (r1 == 0) goto L_0x0884
            r125 = 1
            goto L_0x0884
        L_0x089f:
            byte r1 = r7.get()
            r123 = 0
            if (r1 == 0) goto L_0x0884
            r123 = 1
            goto L_0x0884
        L_0x08aa:
            byte r1 = r7.get()
            r122 = 0
            if (r1 == 0) goto L_0x0884
            r122 = 1
            goto L_0x0884
        L_0x08b5:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x08db
            if (r1 == r5) goto L_0x08d0
            if (r1 == r4) goto L_0x08c5
            r7.get()
            goto L_0x08b5
        L_0x08c5:
            byte r1 = r7.get()
            r121 = 0
            if (r1 == 0) goto L_0x08b5
            r121 = 1
            goto L_0x08b5
        L_0x08d0:
            byte r1 = r7.get()
            r120 = 0
            if (r1 == 0) goto L_0x08b5
            r120 = 1
            goto L_0x08b5
        L_0x08db:
            byte r1 = r7.get()
            r119 = 0
            if (r1 == 0) goto L_0x08b5
            r119 = 1
            goto L_0x08b5
        L_0x08e6:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x090c
            if (r1 == r5) goto L_0x0901
            if (r1 == r4) goto L_0x08f6
            r7.get()
            goto L_0x08e6
        L_0x08f6:
            byte r1 = r7.get()
            r118 = 0
            if (r1 == 0) goto L_0x08e6
            r118 = 1
            goto L_0x08e6
        L_0x0901:
            byte r1 = r7.get()
            r117 = 0
            if (r1 == 0) goto L_0x08e6
            r117 = 1
            goto L_0x08e6
        L_0x090c:
            byte r1 = r7.get()
            r116 = 0
            if (r1 == 0) goto L_0x08e6
            r116 = 1
            goto L_0x08e6
        L_0x0917:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x093d
            if (r1 == r5) goto L_0x0932
            if (r1 == r4) goto L_0x0927
            r7.get()
            goto L_0x0917
        L_0x0927:
            byte r1 = r7.get()
            r115 = 0
            if (r1 == 0) goto L_0x0917
            r115 = 1
            goto L_0x0917
        L_0x0932:
            byte r1 = r7.get()
            r114 = 0
            if (r1 == 0) goto L_0x0917
            r114 = 1
            goto L_0x0917
        L_0x093d:
            byte r1 = r7.get()
            r113 = 0
            if (r1 == 0) goto L_0x0917
            r113 = 1
            goto L_0x0917
        L_0x0948:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x097b
            if (r1 == r5) goto L_0x0970
            if (r1 == r4) goto L_0x0965
            if (r1 == r6) goto L_0x095a
            r7.get()
            goto L_0x0948
        L_0x095a:
            byte r1 = r7.get()
            r112 = 0
            if (r1 == 0) goto L_0x0948
            r112 = 1
            goto L_0x0948
        L_0x0965:
            byte r1 = r7.get()
            r111 = 0
            if (r1 == 0) goto L_0x0948
            r111 = 1
            goto L_0x0948
        L_0x0970:
            byte r1 = r7.get()
            r110 = 0
            if (r1 == 0) goto L_0x0948
            r110 = 1
            goto L_0x0948
        L_0x097b:
            byte r1 = r7.get()
            r109 = 0
            if (r1 == 0) goto L_0x0948
            r109 = 1
            goto L_0x0948
        L_0x0986:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x09b9
            if (r1 == r4) goto L_0x09ae
            if (r1 == r6) goto L_0x09a3
            if (r1 == r9) goto L_0x0998
            r7.get()
            goto L_0x0986
        L_0x0998:
            byte r1 = r7.get()
            r108 = 0
            if (r1 == 0) goto L_0x0986
            r108 = 1
            goto L_0x0986
        L_0x09a3:
            byte r1 = r7.get()
            r107 = 0
            if (r1 == 0) goto L_0x0986
            r107 = 1
            goto L_0x0986
        L_0x09ae:
            byte r1 = r7.get()
            r106 = 0
            if (r1 == 0) goto L_0x0986
            r106 = 1
            goto L_0x0986
        L_0x09b9:
            byte r1 = r7.get()
            r105 = 0
            if (r1 == 0) goto L_0x0986
            r105 = 1
            goto L_0x0986
        L_0x09c4:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x09d0
            r7.get()
            goto L_0x09c4
        L_0x09d0:
            byte r1 = r7.get()
            r103 = 0
            if (r1 == 0) goto L_0x09c4
            r103 = 1
            goto L_0x09c4
        L_0x09db:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r5) goto L_0x09e7
            r7.get()
            goto L_0x09db
        L_0x09e7:
            byte r1 = r7.get()
            r100 = 0
            if (r1 == 0) goto L_0x09db
            r100 = 1
            goto L_0x09db
        L_0x09f2:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0a0b
            if (r1 == r4) goto L_0x0a00
            r7.get()
            goto L_0x09f2
        L_0x0a00:
            byte r1 = r7.get()
            r97 = 0
            if (r1 == 0) goto L_0x09f2
            r97 = 1
            goto L_0x09f2
        L_0x0a0b:
            byte r1 = r7.get()
            r95 = 0
            if (r1 == 0) goto L_0x09f2
            r95 = 1
            goto L_0x09f2
        L_0x0a16:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0a22
            r7.get()
            goto L_0x0a16
        L_0x0a22:
            byte r1 = r7.get()
            r94 = 0
            if (r1 == 0) goto L_0x0a16
            r94 = 1
            goto L_0x0a16
        L_0x0a2d:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r4) goto L_0x0a39
            r7.get()
            goto L_0x0a2d
        L_0x0a39:
            byte r1 = r7.get()
            r86 = 0
            if (r1 == 0) goto L_0x0a2d
            r86 = 1
            goto L_0x0a2d
        L_0x0a44:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0a6a
            if (r1 == r4) goto L_0x0a5f
            if (r1 == r6) goto L_0x0a54
            r7.get()
            goto L_0x0a44
        L_0x0a54:
            byte r1 = r7.get()
            r81 = 0
            if (r1 == 0) goto L_0x0a44
            r81 = 1
            goto L_0x0a44
        L_0x0a5f:
            byte r1 = r7.get()
            r82 = 0
            if (r1 == 0) goto L_0x0a44
            r82 = 1
            goto L_0x0a44
        L_0x0a6a:
            byte r1 = r7.get()
            r78 = 0
            if (r1 == 0) goto L_0x0a44
            r78 = 1
            goto L_0x0a44
        L_0x0a75:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0a9b
            if (r1 == r4) goto L_0x0a90
            if (r1 == r6) goto L_0x0a85
            r7.get()
            goto L_0x0a75
        L_0x0a85:
            byte r1 = r7.get()
            r76 = 0
            if (r1 == 0) goto L_0x0a75
            r76 = 1
            goto L_0x0a75
        L_0x0a90:
            byte r1 = r7.get()
            r77 = 0
            if (r1 == 0) goto L_0x0a75
            r77 = 1
            goto L_0x0a75
        L_0x0a9b:
            byte r1 = r7.get()
            r73 = 0
            if (r1 == 0) goto L_0x0a75
            r73 = 1
            goto L_0x0a75
        L_0x0aa6:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0ab2
            r7.get()
            goto L_0x0aa6
        L_0x0ab2:
            byte r1 = r7.get()
            r72 = 0
            if (r1 == 0) goto L_0x0aa6
            r72 = 1
            goto L_0x0aa6
        L_0x0abd:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0b33
            if (r1 == r6) goto L_0x0b28
            if (r1 == r9) goto L_0x0b1d
            switch(r1) {
                case 7: goto L_0x0ad0;
                case 8: goto L_0x0adb;
                case 9: goto L_0x0ae6;
                case 10: goto L_0x0af1;
                case 11: goto L_0x0afc;
                case 12: goto L_0x0b07;
                case 13: goto L_0x0b12;
                default: goto L_0x0acc;
            }
        L_0x0acc:
            r7.get()
            goto L_0x0abd
        L_0x0ad0:
            byte r1 = r7.get()
            r65 = 0
            if (r1 == 0) goto L_0x0abd
            r65 = 1
            goto L_0x0abd
        L_0x0adb:
            byte r1 = r7.get()
            r66 = 0
            if (r1 == 0) goto L_0x0abd
            r66 = 1
            goto L_0x0abd
        L_0x0ae6:
            byte r1 = r7.get()
            r67 = 0
            if (r1 == 0) goto L_0x0abd
            r67 = 1
            goto L_0x0abd
        L_0x0af1:
            byte r1 = r7.get()
            r68 = 0
            if (r1 == 0) goto L_0x0abd
            r68 = 1
            goto L_0x0abd
        L_0x0afc:
            byte r1 = r7.get()
            r69 = 0
            if (r1 == 0) goto L_0x0abd
            r69 = 1
            goto L_0x0abd
        L_0x0b07:
            byte r1 = r7.get()
            r70 = 0
            if (r1 == 0) goto L_0x0abd
            r70 = 1
            goto L_0x0abd
        L_0x0b12:
            byte r1 = r7.get()
            r71 = 0
            if (r1 == 0) goto L_0x0abd
            r71 = 1
            goto L_0x0abd
        L_0x0b1d:
            byte r1 = r7.get()
            r62 = 0
            if (r1 == 0) goto L_0x0abd
            r62 = 1
            goto L_0x0abd
        L_0x0b28:
            byte r1 = r7.get()
            r61 = 0
            if (r1 == 0) goto L_0x0abd
            r61 = 1
            goto L_0x0abd
        L_0x0b33:
            byte r1 = r7.get()
            r60 = 0
            if (r1 == 0) goto L_0x0abd
            r60 = 1
            goto L_0x0abd
        L_0x0b3e:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            switch(r1) {
                case 1: goto L_0x0b4b;
                case 2: goto L_0x0b56;
                case 3: goto L_0x0b61;
                case 4: goto L_0x0b6c;
                case 5: goto L_0x0b77;
                case 6: goto L_0x0b82;
                case 7: goto L_0x0b8d;
                case 8: goto L_0x0b98;
                case 9: goto L_0x0ba3;
                default: goto L_0x0b47;
            }
        L_0x0b47:
            r7.get()
            goto L_0x0b3e
        L_0x0b4b:
            byte r1 = r7.get()
            r51 = 0
            if (r1 == 0) goto L_0x0b3e
            r51 = 1
            goto L_0x0b3e
        L_0x0b56:
            byte r1 = r7.get()
            r53 = 0
            if (r1 == 0) goto L_0x0b3e
            r53 = 1
            goto L_0x0b3e
        L_0x0b61:
            byte r1 = r7.get()
            r54 = 0
            if (r1 == 0) goto L_0x0b3e
            r54 = 1
            goto L_0x0b3e
        L_0x0b6c:
            byte r1 = r7.get()
            r55 = 0
            if (r1 == 0) goto L_0x0b3e
            r55 = 1
            goto L_0x0b3e
        L_0x0b77:
            byte r1 = r7.get()
            r52 = 0
            if (r1 == 0) goto L_0x0b3e
            r52 = 1
            goto L_0x0b3e
        L_0x0b82:
            byte r1 = r7.get()
            r57 = 0
            if (r1 == 0) goto L_0x0b3e
            r57 = 1
            goto L_0x0b3e
        L_0x0b8d:
            byte r1 = r7.get()
            r56 = 0
            if (r1 == 0) goto L_0x0b3e
            r56 = 1
            goto L_0x0b3e
        L_0x0b98:
            byte r1 = r7.get()
            r58 = 0
            if (r1 == 0) goto L_0x0b3e
            r58 = 1
            goto L_0x0b3e
        L_0x0ba3:
            byte r1 = r7.get()
            r59 = 0
            if (r1 == 0) goto L_0x0b3e
            r59 = 1
            goto L_0x0b3e
        L_0x0bae:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0bd4
            if (r1 == r5) goto L_0x0bc9
            if (r1 == r4) goto L_0x0bbe
            r7.get()
            goto L_0x0bae
        L_0x0bbe:
            byte r1 = r7.get()
            r49 = 0
            if (r1 == 0) goto L_0x0bae
            r49 = 1
            goto L_0x0bae
        L_0x0bc9:
            byte r1 = r7.get()
            r48 = 0
            if (r1 == 0) goto L_0x0bae
            r48 = 1
            goto L_0x0bae
        L_0x0bd4:
            byte r1 = r7.get()
            r47 = 0
            if (r1 == 0) goto L_0x0bae
            r47 = 1
            goto L_0x0bae
        L_0x0bdf:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r5) goto L_0x0bf8
            if (r1 == r4) goto L_0x0bed
            r7.get()
            goto L_0x0bdf
        L_0x0bed:
            byte r1 = r7.get()
            r46 = 0
            if (r1 == 0) goto L_0x0bdf
            r46 = 1
            goto L_0x0bdf
        L_0x0bf8:
            byte r1 = r7.get()
            r45 = 0
            if (r1 == 0) goto L_0x0bdf
            r45 = 1
            goto L_0x0bdf
        L_0x0c03:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0c0f
            r7.get()
            goto L_0x0c03
        L_0x0c0f:
            byte r1 = r7.get()
            r43 = 0
            if (r1 == 0) goto L_0x0c03
            r43 = 1
            goto L_0x0c03
        L_0x0c1a:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0c26
            r7.get()
            goto L_0x0c1a
        L_0x0c26:
            byte r1 = r7.get()
            r42 = 0
            if (r1 == 0) goto L_0x0c1a
            r42 = 1
            goto L_0x0c1a
        L_0x0c31:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0c5b
            r2 = 10
            if (r1 == r2) goto L_0x0c50
            r2 = 11
            if (r1 == r2) goto L_0x0c45
            r7.get()
            goto L_0x0c31
        L_0x0c45:
            byte r1 = r7.get()
            r41 = 0
            if (r1 == 0) goto L_0x0c31
            r41 = 1
            goto L_0x0c31
        L_0x0c50:
            byte r1 = r7.get()
            r40 = 0
            if (r1 == 0) goto L_0x0c31
            r40 = 1
            goto L_0x0c31
        L_0x0c5b:
            byte r1 = r7.get()
            r37 = 0
            if (r1 == 0) goto L_0x0c31
            r37 = 1
            goto L_0x0c31
        L_0x0c66:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r5) goto L_0x0c72
            r7.get()
            goto L_0x0c66
        L_0x0c72:
            byte r1 = r7.get()
            r32 = 0
            if (r1 == 0) goto L_0x0c66
            r32 = 1
            goto L_0x0c66
        L_0x0c7d:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            switch(r1) {
                case 26: goto L_0x0c8a;
                case 27: goto L_0x0c95;
                case 28: goto L_0x0ca0;
                default: goto L_0x0c86;
            }
        L_0x0c86:
            r7.get()
            goto L_0x0c7d
        L_0x0c8a:
            byte r1 = r7.get()
            r28 = 0
            if (r1 == 0) goto L_0x0c7d
            r28 = 1
            goto L_0x0c7d
        L_0x0c95:
            byte r1 = r7.get()
            r29 = 0
            if (r1 == 0) goto L_0x0c7d
            r29 = 1
            goto L_0x0c7d
        L_0x0ca0:
            byte r1 = r7.get()
            r30 = 0
            if (r1 == 0) goto L_0x0c7d
            r30 = 1
            goto L_0x0c7d
        L_0x0cab:
            short r1 = r7.getShort()
            if (r1 == r0) goto L_0x0ccf
            if (r1 == r3) goto L_0x0cc4
            if (r1 == r6) goto L_0x0cb9
            r7.get()
            goto L_0x0cab
        L_0x0cb9:
            byte r1 = r7.get()
            r25 = 0
            if (r1 == 0) goto L_0x0cab
            r25 = 1
            goto L_0x0cab
        L_0x0cc4:
            byte r1 = r7.get()
            r24 = 0
            if (r1 == 0) goto L_0x0cab
            r24 = 1
            goto L_0x0cab
        L_0x0ccf:
            r4 = 3
            goto L_0x0122
        L_0x0cd2:
            java.io.IOException r2 = new java.io.IOException
            java.lang.Integer r0 = java.lang.Integer.valueOf(r12)
            java.lang.Object[] r1 = new java.lang.Object[]{r0}
            java.lang.String r0 = "Version incorrect: %d not between 1 and 3"
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00J.A00(java.nio.ByteBuffer):X.00k");
    }
}
