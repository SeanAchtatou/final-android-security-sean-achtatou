package X;

import android.content.SharedPreferences;
import android.os.Bundle;

/* renamed from: X.0HE  reason: invalid class name */
public final class AnonymousClass0HE extends AnonymousClass0HB {
    public Object A01(SharedPreferences sharedPreferences, String str, Object obj) {
        boolean booleanValue;
        Boolean bool = (Boolean) obj;
        if (bool == null) {
            booleanValue = false;
        } else {
            booleanValue = bool.booleanValue();
        }
        return Boolean.valueOf(sharedPreferences.getBoolean(str, booleanValue));
    }

    public Object A02(Bundle bundle, String str, Object obj) {
        boolean booleanValue;
        Boolean bool = (Boolean) obj;
        if (bool == null) {
            booleanValue = false;
        } else {
            booleanValue = bool.booleanValue();
        }
        return Boolean.valueOf(bundle.getBoolean(str, booleanValue));
    }

    public void A03(SharedPreferences.Editor editor, String str, Object obj) {
        editor.putBoolean(str, ((Boolean) obj).booleanValue());
    }

    public void A04(Bundle bundle, String str, Object obj) {
        bundle.putBoolean(str, ((Boolean) obj).booleanValue());
    }

    public Class A00() {
        return Boolean.class;
    }
}
