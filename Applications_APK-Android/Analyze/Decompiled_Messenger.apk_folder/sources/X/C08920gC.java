package X;

import com.facebook.quicklog.PerformanceLoggingEvent;

/* renamed from: X.0gC  reason: invalid class name and case insensitive filesystem */
public abstract class C08920gC {
    private int A00 = 0;
    private int A01 = 0;
    private int A02 = 0;
    private int A03 = 0;
    private C04280Th A04 = null;
    private final int A05;

    public C04280Th A00() {
        return !(this instanceof C08910gB) ? new PerformanceLoggingEvent() : new C04270Tg();
    }

    public synchronized C04280Th A01() {
        this.A00++;
        C04280Th r1 = this.A04;
        if (r1 == null) {
            this.A01++;
            return A00();
        }
        this.A04 = (C04280Th) r1.Avh();
        r1.clear();
        this.A03--;
        return r1;
    }

    public synchronized void A02(C04280Th r3) {
        this.A02++;
        if (this.A03 < this.A05) {
            r3.BqE();
            r3.C9o(this.A04);
            this.A04 = r3;
            this.A03++;
        }
    }

    public C08920gC(int i) {
        this.A05 = i;
    }
}
