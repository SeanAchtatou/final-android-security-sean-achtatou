package X;

import android.content.Context;
import java.io.File;
import java.io.IOException;

/* renamed from: X.02N  reason: invalid class name */
public final class AnonymousClass02N extends AnonymousClass02E {
    public Integer A00;
    public final File A01;
    public final String A02;
    public final String A03;
    private final File A04;

    public final AnonymousClass0EL A0D() {
        return new AnonymousClass0NV(this, this);
    }

    public byte[] A0E() {
        return C002401o.A01(this.A04, this.A03);
    }

    public String toString() {
        String str;
        try {
            str = String.valueOf(this.A00.getCanonicalPath());
        } catch (IOException unused) {
            str = this.A00.getName();
        }
        return getClass().getName() + "[root = " + str + " flags = " + this.A01 + " zipSource = " + this.A01.getPath() + " compressedPath = " + this.A03 + ']';
    }

    public AnonymousClass02N(Context context, File file, File file2, String str, String str2, Integer num) {
        super(context, file);
        this.A04 = new File(context.getApplicationInfo().sourceDir);
        this.A01 = file2;
        this.A00 = num;
        this.A03 = str;
        this.A02 = str2;
    }

    public AnonymousClass02N(Context context, File file, String str, String str2, Integer num) {
        super(context, file);
        File file2 = new File(context.getApplicationInfo().sourceDir);
        this.A04 = file2;
        this.A01 = file2;
        this.A00 = num;
        this.A03 = str;
        this.A02 = str2;
    }

    public AnonymousClass02N(Context context, Integer num) {
        super(context, AnonymousClass02J.A00(num));
        String str;
        File file = new File(this.A03.getApplicationInfo().sourceDir);
        this.A04 = file;
        this.A01 = file;
        this.A00 = num;
        switch (num.intValue()) {
            case 1:
                str = "assets/lib/libs.zstd";
                break;
            case 2:
                str = "assets/lib/libs.spk.xz";
                break;
            case 3:
                str = "assets/lib/libs.spk.zst";
                break;
            default:
                str = "assets/lib/libs.xzs";
                break;
        }
        this.A03 = str;
        this.A02 = "assets/lib/metadata.txt";
    }
}
