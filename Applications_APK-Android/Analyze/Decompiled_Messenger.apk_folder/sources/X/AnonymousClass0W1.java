package X;

import android.database.sqlite.SQLiteDatabase;
import com.google.common.collect.ImmutableList;

/* renamed from: X.0W1  reason: invalid class name */
public class AnonymousClass0W1 extends AnonymousClass0W2 {
    public final ImmutableList A00;
    private final C30356Eug A01;
    private final boolean A02;

    public void A07(SQLiteDatabase sQLiteDatabase) {
        C24971Xv it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass0W4) it.next()).A0B(sQLiteDatabase);
        }
        if (this.A01 == null && this.A02) {
            C007406x.A00(244469499);
            sQLiteDatabase.execSQL("PRAGMA synchronous=OFF");
            C007406x.A00(461448166);
        }
    }

    public void A08(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        C24971Xv it = this.A00.iterator();
        while (it.hasNext()) {
            ((AnonymousClass0W4) it.next()).A0C(sQLiteDatabase, i, i2);
        }
    }

    public AnonymousClass0W1(String str, int i, ImmutableList immutableList) {
        this(str, i, immutableList, true, null);
    }

    private AnonymousClass0W1(String str, int i, ImmutableList immutableList, boolean z, C30356Eug eug) {
        super(str, i);
        this.A00 = immutableList;
        this.A02 = z;
        this.A01 = eug;
    }
}
