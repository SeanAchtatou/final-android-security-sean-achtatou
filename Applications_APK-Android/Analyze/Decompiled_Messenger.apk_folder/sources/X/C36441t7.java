package X;

import android.content.Context;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.messaging.business.common.calltoaction.model.CallToAction;
import com.facebook.messaging.business.common.calltoaction.model.NestedCallToAction;
import com.facebook.omnistore.Collection;
import com.facebook.omnistore.Delta;
import com.facebook.omnistore.IndexedFields;
import com.facebook.omnistore.QueueIdentifier;
import com.facebook.omnistore.module.OmnistoreComponent;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1t7  reason: invalid class name and case insensitive filesystem */
public final class C36441t7 implements OmnistoreComponent {
    private static volatile C36441t7 A0A;
    public Collection A00;
    public final Context A01;
    public final AnonymousClass09P A02;
    public final AnonymousClass0ZS A03;
    public final C24341Tf A04;
    public final Set A05;
    public final ExecutorService A06;
    private final AnonymousClass06B A07;
    private final C36451t8 A08;
    private final C04310Tq A09;

    public String getCollectionLabel() {
        return "messenger_platform_menu";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bd, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00be, code lost:
        if (r2 != null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00c3 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0009 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onCollectionAvailable(com.facebook.omnistore.Collection r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r9.A00 = r10     // Catch:{ all -> 0x00e4 }
            java.util.Set r0 = r9.A05     // Catch:{ all -> 0x00e4 }
            java.util.Iterator r8 = r0.iterator()     // Catch:{ all -> 0x00e4 }
        L_0x0009:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x00e4 }
            if (r0 == 0) goto L_0x00e2
            java.lang.Object r4 = r8.next()     // Catch:{ all -> 0x00e4 }
            X.2UI r4 = (X.AnonymousClass2UI) r4     // Catch:{ all -> 0x00e4 }
            X.2UJ r5 = r4.A04     // Catch:{ all -> 0x00e4 }
            com.facebook.prefs.shared.FbSharedPreferences r3 = r5.A01     // Catch:{ all -> 0x00e4 }
            X.1Y7 r0 = X.AnonymousClass2UJ.A02     // Catch:{ all -> 0x00e4 }
            r1 = -1
            long r6 = r3.At2(r0, r1)     // Catch:{ all -> 0x00e4 }
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x003a
            com.facebook.prefs.shared.FbSharedPreferences r0 = r5.A01     // Catch:{ all -> 0x00e4 }
            X.1hn r3 = r0.edit()     // Catch:{ all -> 0x00e4 }
            X.1Y7 r2 = X.AnonymousClass2UJ.A02     // Catch:{ all -> 0x00e4 }
            X.06B r0 = r5.A00     // Catch:{ all -> 0x00e4 }
            long r0 = r0.now()     // Catch:{ all -> 0x00e4 }
            r3.BzA(r2, r0)     // Catch:{ all -> 0x00e4 }
            r3.commit()     // Catch:{ all -> 0x00e4 }
            goto L_0x005e
        L_0x003a:
            r0 = 604800000(0x240c8400, double:2.988109026E-315)
            long r6 = r6 + r0
            X.06B r0 = r5.A00     // Catch:{ all -> 0x00e4 }
            long r1 = r0.now()     // Catch:{ all -> 0x00e4 }
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x005e
            com.facebook.prefs.shared.FbSharedPreferences r0 = r5.A01     // Catch:{ all -> 0x00e4 }
            X.1hn r3 = r0.edit()     // Catch:{ all -> 0x00e4 }
            X.1Y7 r2 = X.AnonymousClass2UJ.A02     // Catch:{ all -> 0x00e4 }
            X.06B r0 = r5.A00     // Catch:{ all -> 0x00e4 }
            long r0 = r0.now()     // Catch:{ all -> 0x00e4 }
            r3.BzA(r2, r0)     // Catch:{ all -> 0x00e4 }
            r3.commit()     // Catch:{ all -> 0x00e4 }
            r0 = 1
            goto L_0x005f
        L_0x005e:
            r0 = 0
        L_0x005f:
            java.lang.String r3 = "UserPlatformMenuUpdater"
            if (r0 == 0) goto L_0x0009
            X.2UK r0 = r4.A03     // Catch:{ all -> 0x00e4 }
            com.facebook.analytics.DeprecatedAnalyticsLogger r2 = r0.A00     // Catch:{ all -> 0x00e4 }
            java.lang.String r1 = "omnistore_bot_menus_periodic_dump"
            r0 = 0
            X.1La r2 = r2.A04(r1, r0)     // Catch:{ all -> 0x00e4 }
            boolean r0 = r2.A0B()     // Catch:{ all -> 0x00e4 }
            if (r0 == 0) goto L_0x007e
            java.lang.String r1 = "pigeon_reserved_keyword_module"
            java.lang.String r0 = "omnistore_bot_menus_module"
            r2.A06(r1, r0)     // Catch:{ all -> 0x00e4 }
            r2.A0A()     // Catch:{ all -> 0x00e4 }
        L_0x007e:
            X.1t7 r5 = r4.A02     // Catch:{ Exception -> 0x00d8 }
            monitor-enter(r5)     // Catch:{ Exception -> 0x00d8 }
            r2 = r5
            monitor-enter(r2)     // Catch:{ all -> 0x00d5 }
            com.facebook.omnistore.Collection r1 = r5.A00     // Catch:{ all -> 0x00d2 }
            r0 = 0
            if (r1 == 0) goto L_0x0089
            r0 = 1
        L_0x0089:
            monitor-exit(r2)     // Catch:{ all -> 0x00d5 }
            if (r0 != 0) goto L_0x0092
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x00d5 }
            r6.<init>()     // Catch:{ all -> 0x00d5 }
            goto L_0x00cc
        L_0x0092:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x00d5 }
            r6.<init>()     // Catch:{ all -> 0x00d5 }
            com.facebook.omnistore.Collection r7 = r5.A00     // Catch:{ OmnistoreIOException -> 0x00c4 }
            java.lang.String r2 = ""
            r1 = -1
            r0 = 1
            com.facebook.omnistore.Cursor r2 = r7.query(r2, r1, r0)     // Catch:{ OmnistoreIOException -> 0x00c4 }
        L_0x00a1:
            boolean r0 = r2.step()     // Catch:{ all -> 0x00bb }
            if (r0 == 0) goto L_0x00b7
            java.lang.String r1 = r2.getPrimaryKey()     // Catch:{ all -> 0x00bb }
            java.nio.ByteBuffer r0 = r2.getBlob()     // Catch:{ all -> 0x00bb }
            X.3wW r0 = A01(r1, r0)     // Catch:{ all -> 0x00bb }
            r6.add(r0)     // Catch:{ all -> 0x00bb }
            goto L_0x00a1
        L_0x00b7:
            r2.close()     // Catch:{ OmnistoreIOException -> 0x00c4 }
            goto L_0x00cc
        L_0x00bb:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00bd }
        L_0x00bd:
            r0 = move-exception
            if (r2 == 0) goto L_0x00c3
            r2.close()     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            throw r0     // Catch:{ OmnistoreIOException -> 0x00c4 }
        L_0x00c4:
            r2 = move-exception
            java.lang.String r1 = "PlatformMenuOmnistoreManagerImpl"
            java.lang.String r0 = "IO error while getting platform menus"
            X.C010708t.A0R(r1, r2, r0)     // Catch:{ all -> 0x00d5 }
        L_0x00cc:
            monitor-exit(r5)     // Catch:{ Exception -> 0x00d8 }
            X.AnonymousClass2UI.A01(r4, r6)     // Catch:{ all -> 0x00e4 }
            goto L_0x0009
        L_0x00d2:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00d5 }
            throw r0     // Catch:{ all -> 0x00d5 }
        L_0x00d5:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ Exception -> 0x00d8 }
            throw r0     // Catch:{ Exception -> 0x00d8 }
        L_0x00d8:
            r2 = move-exception
            X.09P r1 = r4.A01     // Catch:{ all -> 0x00e4 }
            java.lang.String r0 = "Exception retrieving full menu list from Omnistore"
            r1.softReport(r3, r0, r2)     // Catch:{ all -> 0x00e4 }
            goto L_0x0009
        L_0x00e2:
            monitor-exit(r9)
            return
        L_0x00e4:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36441t7.onCollectionAvailable(com.facebook.omnistore.Collection):void");
    }

    public synchronized void onCollectionInvalidated() {
        this.A00 = null;
    }

    public void onDeltaClusterEnded(int i, QueueIdentifier queueIdentifier) {
    }

    public void onDeltaClusterStarted(int i, long j, QueueIdentifier queueIdentifier) {
    }

    public static NestedCallToAction A00(AnonymousClass84q r7) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        int i;
        String str7;
        float f;
        C188515m r5 = new C188515m();
        int A022 = r7.A02(4);
        if (A022 != 0) {
            str = r7.A05(A022 + r7.A00);
        } else {
            str = null;
        }
        r5.A0B = str;
        int A023 = r7.A02(6);
        if (A023 != 0) {
            str2 = r7.A05(A023 + r7.A00);
        } else {
            str2 = null;
        }
        r5.A0E = str2;
        int A024 = r7.A02(8);
        if (A024 != 0) {
            str3 = r7.A05(A024 + r7.A00);
        } else {
            str3 = null;
        }
        r5.A01(str3);
        int A025 = r7.A02(14);
        if (A025 != 0) {
            str4 = r7.A05(A025 + r7.A00);
        } else {
            str4 = null;
        }
        r5.A02(str4);
        int A026 = r7.A02(10);
        if (A026 != 0) {
            str5 = r7.A05(A026 + r7.A00);
        } else {
            str5 = null;
        }
        r5.A08 = C16550xJ.A00(str5);
        int A027 = r7.A02(12);
        boolean z = false;
        if (!(A027 == 0 || r7.A01.get(A027 + r7.A00) == 0)) {
            z = true;
        }
        r5.A0G = z;
        int A028 = r7.A02(16);
        boolean z2 = false;
        if (!(A028 == 0 || r7.A01.get(A028 + r7.A00) == 0)) {
            z2 = true;
        }
        r5.A0F = z2;
        int A029 = r7.A02(24);
        if (A029 != 0) {
            str6 = r7.A05(A029 + r7.A00);
        } else {
            str6 = null;
        }
        r5.A0C = str6;
        int A0210 = r7.A02(26);
        boolean z3 = false;
        if (!(A0210 == 0 || r7.A01.get(A0210 + r7.A00) == 0)) {
            z3 = true;
        }
        r5.A0H = z3;
        if (r7.A06() != null) {
            C30041hO r6 = new C30041hO();
            C1748384r A062 = r7.A06();
            int A0211 = A062.A02(8);
            if (A0211 != 0) {
                str7 = A062.A05(A0211 + A062.A00);
            } else {
                str7 = null;
            }
            r6.A01 = C30051hP.A00(str7);
            C1748384r A063 = r7.A06();
            int A0212 = A063.A02(4);
            if (A0212 != 0) {
                f = A063.A01.getFloat(A0212 + A063.A00);
            } else {
                f = 0.0f;
            }
            r6.A00 = (double) f;
            C1748384r A064 = r7.A06();
            int A0213 = A064.A02(6);
            boolean z4 = false;
            if (!(A0213 == 0 || A064.A01.get(A0213 + A064.A00) == 0)) {
                z4 = true;
            }
            r6.A07 = z4;
            r5.A02 = r6.A00();
        }
        CallToAction A002 = r5.A00();
        ArrayList arrayList = new ArrayList();
        int A0214 = r7.A02(20);
        if (A0214 != 0) {
            i = r7.A04(A0214);
        } else {
            i = 0;
        }
        for (int i2 = 0; i2 < i; i2++) {
            AnonymousClass84q r2 = new AnonymousClass84q();
            int A0215 = r7.A02(20);
            if (A0215 != 0) {
                int A012 = r7.A01(r7.A03(A0215) + (i2 << 2));
                ByteBuffer byteBuffer = r7.A01;
                r2.A00 = A012;
                r2.A01 = byteBuffer;
            } else {
                r2 = null;
            }
            arrayList.add(A00(r2));
        }
        C30131hX r1 = new C30131hX();
        r1.A00 = A002;
        r1.A01 = ImmutableList.copyOf((java.util.Collection) arrayList);
        return new NestedCallToAction(r1);
    }

    public static final C36441t7 A02(AnonymousClass1XY r13) {
        if (A0A == null) {
            synchronized (C36441t7.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r13);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r13.getApplicationInjector();
                        A0A = new C36441t7(AnonymousClass0XJ.A0L(applicationInjector), AnonymousClass0ZS.A00(applicationInjector), AnonymousClass1YA.A00(applicationInjector), C04750Wa.A01(applicationInjector), AnonymousClass0UX.A0Z(applicationInjector), C36451t8.A00(applicationInjector), AnonymousClass067.A02(), new AnonymousClass0X5(applicationInjector, AnonymousClass0X6.A1J), C24341Tf.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public IndexedFields BCR(String str, String str2, ByteBuffer byteBuffer) {
        return new IndexedFields();
    }

    public void BVs(List list) {
        long now = this.A07.now();
        list.size();
        C36451t8 r0 = this.A08;
        Preconditions.checkNotNull(list);
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(r0.A00.A01("omnistore_bot_menus_delta_received"), AnonymousClass1Y3.A3v);
        if (uSLEBaseShape0S0000000.A0G()) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Iterator it = list.iterator();
            int i = 0;
            int i2 = 0;
            while (it.hasNext()) {
                Delta delta = (Delta) it.next();
                int type = delta.getType();
                if (type == 1) {
                    i2++;
                    if (arrayList.size() < 10) {
                        arrayList.add(delta.getPrimaryKey());
                    }
                } else if (type == 2) {
                    if (arrayList2.size() < 10) {
                        arrayList2.add(delta.getPrimaryKey());
                    }
                    i++;
                }
            }
            uSLEBaseShape0S0000000.A0C("delta_event_ts", Long.valueOf(now));
            uSLEBaseShape0S0000000.A0C("num_delete_deltas", Long.valueOf((long) i));
            uSLEBaseShape0S0000000.A0C("num_save_deltas", Long.valueOf((long) i2));
            uSLEBaseShape0S0000000.A0D(C99084oO.$const$string(19), null);
            uSLEBaseShape0S0000000.A0D(C99084oO.$const$string(7), null);
            USLEBaseShape0S0000000 A0O = uSLEBaseShape0S0000000.A0O("omnistore_bot_menus_module");
            A0O.A0E("delete_delta_ids", arrayList2);
            A0O.A0E("save_delta_ids", arrayList);
            A0O.A06();
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            Delta delta2 = (Delta) it2.next();
            if (delta2.getType() == 1) {
                C82773wW r5 = null;
                try {
                    r5 = A01(delta2.getPrimaryKey(), delta2.getBlob());
                } catch (Exception e) {
                    this.A02.softReport("PlatformMenuOmnistoreManager", "Error deserializing flatbuffer menu into platform menu", e);
                }
                if (r5 != null) {
                    arrayList3.add(r5);
                }
            }
        }
        C36451t8 r02 = this.A08;
        Preconditions.checkNotNull(arrayList3);
        USLEBaseShape0S0000000 uSLEBaseShape0S00000002 = new USLEBaseShape0S0000000(r02.A00.A01("omnistore_bot_menus_menus_to_notify"), 473);
        if (uSLEBaseShape0S00000002.A0G()) {
            ArrayList arrayList4 = new ArrayList();
            for (int i3 = 0; i3 < Math.min(10, arrayList3.size()); i3++) {
                arrayList4.add(((C82773wW) arrayList3.get(i3)).A00);
            }
            uSLEBaseShape0S00000002.A0C("delta_event_ts", Long.valueOf(now));
            uSLEBaseShape0S00000002.A0C("num_menus", Long.valueOf((long) arrayList3.size()));
            uSLEBaseShape0S00000002.A0D(C99084oO.$const$string(19), null);
            uSLEBaseShape0S00000002.A0D(C99084oO.$const$string(7), null);
            USLEBaseShape0S0000000 A0O2 = uSLEBaseShape0S00000002.A0O("omnistore_bot_menus_module");
            A0O2.A0E("menu_ids", arrayList4);
            A0O2.A06();
        }
        if (!arrayList3.isEmpty()) {
            for (AnonymousClass2UI r03 : this.A05) {
                arrayList3.size();
                AnonymousClass2UI.A01(r03, arrayList3);
                AnonymousClass00S.A04(AnonymousClass2UI.A09, new C107765Dn(r03), 1728397257);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:26|27|28|29|30) */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x008f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x0098 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C32171lK provideSubscriptionInfo(com.facebook.omnistore.Omnistore r9) {
        /*
            r8 = this;
            X.0Tq r0 = r8.A09
            java.lang.Object r2 = r0.get()
            java.lang.String r2 = (java.lang.String) r2
            if (r2 == 0) goto L_0x00b1
            java.lang.String r1 = "messenger_platform_menu"
            java.lang.String r0 = "messenger_android_device_flash_sq"
            com.facebook.omnistore.CollectionName$Builder r0 = r9.createCollectionNameWithDomainBuilder(r1, r0)
            r0.addSegment(r2)
            r0.addDeviceId()
            com.facebook.omnistore.CollectionName r5 = r0.build()
            X.1lI r4 = new X.1lI
            r4.<init>()
            X.0ZS r0 = r8.A03
            java.util.Locale r0 = r0.A06()
            java.lang.String r3 = r0.toString()
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0044 }
            r2.<init>()     // Catch:{ JSONException -> 0x0044 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0044 }
            r1.<init>()     // Catch:{ JSONException -> 0x0044 }
            java.lang.String r0 = "user_locale"
            r1.put(r0, r3)     // Catch:{ JSONException -> 0x0044 }
            java.lang.String r0 = "render_object_list_query_params"
            r2.put(r0, r1)     // Catch:{ JSONException -> 0x0044 }
            java.lang.String r0 = r2.toString()     // Catch:{ JSONException -> 0x0044 }
            goto L_0x0050
        L_0x0044:
            r3 = move-exception
            X.09P r2 = r8.A02
            java.lang.String r1 = "PlatformMenuOmnistoreManager"
            java.lang.String r0 = "JSON exception building collection params"
            r2.softReport(r1, r0, r3)
            java.lang.String r0 = ""
        L_0x0050:
            r4.A02 = r0
            android.content.Context r0 = r8.A01     // Catch:{ IOException -> 0x0099 }
            android.content.res.AssetManager r1 = r0.getAssets()     // Catch:{ IOException -> 0x0099 }
            java.lang.String r0 = "OmnistorePlatformMenu.fbs"
            java.io.InputStream r6 = r1.open(r0)     // Catch:{ IOException -> 0x0099 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ all -> 0x0090 }
            java.nio.charset.Charset r0 = java.nio.charset.Charset.defaultCharset()     // Catch:{ all -> 0x0090 }
            r7.<init>(r6, r0)     // Catch:{ all -> 0x0090 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0089 }
            r3.<init>()     // Catch:{ all -> 0x0089 }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r2 = new char[r0]     // Catch:{ all -> 0x0089 }
        L_0x0070:
            int r1 = r7.read(r2)     // Catch:{ all -> 0x0089 }
            r0 = -1
            if (r1 == r0) goto L_0x007c
            r0 = 0
            r3.append(r2, r0, r1)     // Catch:{ all -> 0x0089 }
            goto L_0x0070
        L_0x007c:
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0089 }
            r7.close()     // Catch:{ all -> 0x0090 }
            if (r6 == 0) goto L_0x00a5
            r6.close()     // Catch:{ IOException -> 0x0099 }
            goto L_0x00a5
        L_0x0089:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x008b }
        L_0x008b:
            r0 = move-exception
            r7.close()     // Catch:{ all -> 0x008f }
        L_0x008f:
            throw r0     // Catch:{ all -> 0x0090 }
        L_0x0090:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0092 }
        L_0x0092:
            r0 = move-exception
            if (r6 == 0) goto L_0x0098
            r6.close()     // Catch:{ all -> 0x0098 }
        L_0x0098:
            throw r0     // Catch:{ IOException -> 0x0099 }
        L_0x0099:
            r3 = move-exception
            X.09P r2 = r8.A02
            java.lang.String r1 = "PlatformMenuOmnistoreManager"
            java.lang.String r0 = "IOException reading IDL file"
            r2.softReport(r1, r0, r3)
            java.lang.String r0 = ""
        L_0x00a5:
            r4.A03 = r0
            X.1lJ r0 = new X.1lJ
            r0.<init>(r4)
            X.1lK r0 = X.C32171lK.A00(r5, r0)
            return r0
        L_0x00b1:
            X.1lK r0 = X.C32171lK.A03
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36441t7.provideSubscriptionInfo(com.facebook.omnistore.Omnistore):X.1lK");
    }

    private C36441t7(C04310Tq r1, AnonymousClass0ZS r2, Context context, AnonymousClass09P r4, ExecutorService executorService, C36451t8 r6, AnonymousClass06B r7, Set set, C24341Tf r9) {
        this.A09 = r1;
        this.A03 = r2;
        this.A01 = context;
        this.A02 = r4;
        this.A06 = executorService;
        this.A04 = r9;
        this.A08 = r6;
        this.A07 = r7;
        this.A05 = set;
        set.size();
    }

    public static C82773wW A01(String str, ByteBuffer byteBuffer) {
        int i;
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(byteBuffer);
        C158727We r6 = new C158727We();
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        r6.A00 = byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position();
        r6.A01 = byteBuffer;
        ArrayList arrayList = new ArrayList();
        int A022 = r6.A02(4);
        if (A022 != 0) {
            i = r6.A04(A022);
        } else {
            i = 0;
        }
        for (int i2 = 0; i2 < i; i2++) {
            AnonymousClass84q r2 = new AnonymousClass84q();
            int A023 = r6.A02(4);
            if (A023 != 0) {
                int A012 = r6.A01(r6.A03(A023) + (i2 << 2));
                ByteBuffer byteBuffer2 = r6.A01;
                r2.A00 = A012;
                r2.A01 = byteBuffer2;
            } else {
                r2 = null;
            }
            arrayList.add(A00(r2));
        }
        int A024 = r6.A02(6);
        boolean z = false;
        if (!(A024 == 0 || r6.A01.get(A024 + r6.A00) == 0)) {
            z = true;
        }
        return new C82773wW(str, z, arrayList);
    }

    public void Boz(int i) {
    }
}
