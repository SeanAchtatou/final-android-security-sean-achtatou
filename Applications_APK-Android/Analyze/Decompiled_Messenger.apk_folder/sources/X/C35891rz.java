package X;

import com.samsung.android.os.SemPerfManager;
import io.card.payment.BuildConfig;
import org.json.JSONObject;

/* renamed from: X.1rz  reason: invalid class name and case insensitive filesystem */
public final class C35891rz implements C26381bM {
    private static boolean A00;

    public static boolean A00() {
        Class<SemPerfManager> cls = SemPerfManager.class;
        try {
            cls.toString();
            Class cls2 = Boolean.TYPE;
            if (!C34061oa.A01(cls, "onScrollEvent", cls2)) {
                return false;
            }
            A00 = C34061oa.A01(cls, "onSmoothScrollEvent", cls2);
            return true;
        } catch (Error | Exception unused) {
            return false;
        }
    }

    public AnonymousClass0f7 AUy(C26401bO r4, C26501bY r5) {
        int[] Aet = r4.Aet(r5);
        if (Aet == null || Aet.length == 0) {
            return null;
        }
        boolean z = false;
        if (Aet[0] > 50 && A00) {
            z = true;
        }
        return new C35901s0(r5.A01, z);
    }

    public int AyN() {
        return 3;
    }

    public int AyO() {
        return 2;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", "samsung");
            jSONObject.put("framework", "SemPerfManager");
            jSONObject.put("extra", A00 ? "partial" : BuildConfig.FLAVOR);
            return jSONObject.toString();
        } catch (Exception unused) {
            return BuildConfig.FLAVOR;
        }
    }
}
