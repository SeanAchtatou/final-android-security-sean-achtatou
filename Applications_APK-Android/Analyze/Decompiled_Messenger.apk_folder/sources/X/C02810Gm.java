package X;

import android.util.SparseIntArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0Gm  reason: invalid class name and case insensitive filesystem */
public final class C02810Gm implements C02780Gi {
    public void C2x(AnonymousClass0FM r12, C02910Gx r13) {
        JSONObject jSONObject;
        AnonymousClass0FX r122 = (AnonymousClass0FX) r12;
        int length = r122.timeInStateS.length;
        if (length == 0) {
            jSONObject = null;
        } else {
            boolean[] zArr = new boolean[length];
            jSONObject = new JSONObject();
            int length2 = r122.timeInStateS.length;
            for (int i = 0; i < length2; i++) {
                SparseIntArray sparseIntArray = r122.timeInStateS[i];
                if (sparseIntArray.size() != 0 && !zArr[i]) {
                    int i2 = 1 << i;
                    for (int i3 = i + 1; i3 < length2; i3++) {
                        if (AnonymousClass0FX.A01(sparseIntArray, r122.timeInStateS[i3])) {
                            i2 |= 1 << i3;
                            zArr[i3] = true;
                        }
                    }
                    try {
                        String hexString = Integer.toHexString(i2);
                        JSONObject jSONObject2 = new JSONObject();
                        int size = sparseIntArray.size();
                        for (int i4 = 0; i4 < size; i4++) {
                            jSONObject2.put(Integer.toString(sparseIntArray.keyAt(i4)), sparseIntArray.valueAt(i4));
                        }
                        jSONObject.put(hexString, jSONObject2);
                    } catch (JSONException e) {
                        AnonymousClass0KZ.A00("CpuFrequencyMetricsReporter", "Unable to store event", e);
                    }
                }
            }
        }
        if (jSONObject != null && jSONObject.length() != 0) {
            r13.AMW("cpu_time_in_state_s", jSONObject.toString());
        }
    }
}
