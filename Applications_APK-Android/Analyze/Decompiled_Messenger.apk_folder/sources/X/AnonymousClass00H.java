package X;

import android.os.Build;
import java.lang.reflect.Method;

/* renamed from: X.00H  reason: invalid class name */
public final class AnonymousClass00H {
    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|18|19|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0028 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x002f */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0028=Splitter:B:19:0x0028, B:8:0x001b=Splitter:B:8:0x001b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00() {
        /*
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ IOException -> 0x0030 }
            java.lang.String r0 = "/proc/self/cmdline"
            r4.<init>(r0)     // Catch:{ IOException -> 0x0030 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x0029 }
            r3.<init>(r4)     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = r3.readLine()     // Catch:{ all -> 0x0022 }
            r1 = 0
            int r0 = r2.indexOf(r1)     // Catch:{ all -> 0x0022 }
            if (r0 < 0) goto L_0x001b
            java.lang.String r2 = r2.substring(r1, r0)     // Catch:{ all -> 0x0022 }
        L_0x001b:
            r3.close()     // Catch:{ all -> 0x0029 }
            r4.close()     // Catch:{ IOException -> 0x0030 }
            return r2
        L_0x0022:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0024 }
        L_0x0024:
            r0 = move-exception
            r3.close()     // Catch:{ all -> 0x0028 }
        L_0x0028:
            throw r0     // Catch:{ all -> 0x0029 }
        L_0x0029:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            r4.close()     // Catch:{ all -> 0x002f }
        L_0x002f:
            throw r0     // Catch:{ IOException -> 0x0030 }
        L_0x0030:
            r1 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00H.A00():java.lang.String");
    }

    public static boolean A01() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 18) {
            long j = C000800m.A00;
            if (!C000800m.A03) {
                return false;
            }
            Method method = C000800m.A01;
            C000300h.A01(method);
            Boolean bool = (Boolean) C000800m.A00(method, Long.valueOf(j));
            if (bool != null) {
                return bool.booleanValue();
            }
            return false;
        } else if (i < 16) {
            return false;
        } else {
            if (AnonymousClass0FH.A00 == null) {
                AnonymousClass0FH.A00();
            }
            return AnonymousClass0FH.A00.booleanValue();
        }
    }
}
