package X;

import java.math.BigDecimal;
import java.math.BigInteger;

/* renamed from: X.0to  reason: invalid class name and case insensitive filesystem */
public abstract class C14680to extends C14610tg {
    public abstract String asText();

    public abstract BigInteger bigIntegerValue();

    public abstract boolean canConvertToInt();

    public abstract BigDecimal decimalValue();

    public abstract double doubleValue();

    public abstract int intValue();

    public abstract long longValue();

    public abstract C29501gW numberType();

    public abstract Number numberValue();

    public final C11980oL getNodeType() {
        return C11980oL.NUMBER;
    }

    public final double asDouble() {
        return doubleValue();
    }

    public final double asDouble(double d) {
        return doubleValue();
    }

    public final int asInt() {
        return intValue();
    }

    public final int asInt(int i) {
        return intValue();
    }

    public final long asLong() {
        return longValue();
    }

    public final long asLong(long j) {
        return longValue();
    }
}
