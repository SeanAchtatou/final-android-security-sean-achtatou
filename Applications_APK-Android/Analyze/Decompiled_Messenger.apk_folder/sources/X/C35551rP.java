package X;

import com.facebook.litho.ComponentTree;
import java.util.List;

/* renamed from: X.1rP  reason: invalid class name and case insensitive filesystem */
public final class C35551rP implements C35561rQ {
    public final /* synthetic */ AnonymousClass1IK A00;
    public final /* synthetic */ C35541rO A01;
    public final /* synthetic */ AnonymousClass1Ri A02;

    public C35551rP(AnonymousClass1Ri r1, C35541rO r2, AnonymousClass1IK r3) {
        this.A02 = r1;
        this.A01 = r2;
        this.A00 = r3;
    }

    public void BoH(int i, int i2) {
        AnonymousClass1Ri.A0O(this.A02, this.A01);
        AnonymousClass1IK r2 = this.A00;
        synchronized (r2) {
            ComponentTree componentTree = r2.A01;
            if (!(componentTree == null || this == null)) {
                synchronized (componentTree) {
                    List list = componentTree.A0J;
                    if (list != null) {
                        list.remove(this);
                    }
                }
            }
        }
    }
}
