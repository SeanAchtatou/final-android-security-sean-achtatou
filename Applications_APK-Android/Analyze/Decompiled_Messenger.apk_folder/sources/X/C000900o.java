package X;

import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.ipc.TraceContext;

/* renamed from: X.00o  reason: invalid class name and case insensitive filesystem */
public abstract class C000900o {
    public TraceContext A00;
    public String A01;
    public boolean A02;
    private int A03;

    public void A07(TraceContext traceContext, C004505k r2) {
    }

    public boolean A08() {
        return false;
    }

    public abstract void disable();

    public abstract void enable();

    public abstract int getSupportedProviders();

    public abstract int getTracingProviders();

    public void onTraceStarted(TraceContext traceContext, C004505k r2) {
    }

    private final void A00() {
        if (!this.A02) {
            synchronized (this) {
                if (!this.A02) {
                    AnonymousClass01q.A08(this.A01);
                    this.A02 = true;
                }
            }
        }
    }

    private void A01(TraceContext traceContext) {
        int supportedProviders = getSupportedProviders() & TraceEvents.sProviders;
        int i = this.A03;
        if (i == 0 || !TraceEvents.isEnabled(i)) {
            if (this.A03 != 0) {
                disable();
                this.A00 = null;
            }
            if (supportedProviders != 0) {
                this.A00 = traceContext;
                enable();
            }
            this.A03 = supportedProviders;
        }
    }

    public final void A05(TraceContext traceContext, C004505k r4) {
        int A032 = C000700l.A03(565695267);
        if (this.A03 == 0) {
            C000700l.A09(1976702663, A032);
            return;
        }
        A00();
        A07(traceContext, r4);
        A01(traceContext);
        C000700l.A09(-886804755, A032);
    }

    public final void A06(TraceContext traceContext, C004505k r5) {
        int A032 = C000700l.A03(-968538987);
        if ((getSupportedProviders() & TraceEvents.sProviders) == 0) {
            C000700l.A09(1275317900, A032);
            return;
        }
        A00();
        A01(traceContext);
        onTraceStarted(traceContext, r5);
        C000700l.A09(1881992226, A032);
    }

    public C000900o() {
        this(null);
    }

    public C000900o(String str) {
        this.A01 = str;
        this.A02 = str == null;
    }
}
