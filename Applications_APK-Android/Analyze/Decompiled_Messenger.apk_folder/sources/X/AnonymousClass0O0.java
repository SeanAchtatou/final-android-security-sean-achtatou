package X;

import android.os.Bundle;
import com.facebook.rti.push.service.FbnsService;

/* renamed from: X.0O0  reason: invalid class name */
public final class AnonymousClass0O0 implements AnonymousClass0RR {
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0095, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x0099 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.Bundle AXc(com.facebook.rti.push.service.FbnsService r9, android.os.Bundle r10) {
        /*
            r8 = this;
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            X.0OO r7 = r9.A02
            X.0Bk r0 = r7.A01
            if (r0 == 0) goto L_0x0023
            java.util.Map r1 = r0.B5A()
            java.lang.String r0 = "DumpSys"
            r7.BIq(r0, r1)
        L_0x0014:
            java.util.concurrent.ScheduledExecutorService r2 = r7.A06
            X.0RQ r1 = new X.0RQ
            r1.<init>(r7)
            r0 = -972433988(0xffffffffc609d5bc, float:-8821.434)
            java.util.concurrent.Future r0 = X.AnonymousClass07A.A02(r2, r1, r0)
            goto L_0x0029
        L_0x0023:
            java.lang.String r0 = "SystemDumper not connected"
            r7.BIo(r0)
            goto L_0x0014
        L_0x0029:
            r0.get()     // Catch:{ InterruptedException | ExecutionException -> 0x002c }
        L_0x002c:
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            int r0 = r7.A00
            r5 = 0
            if (r0 != 0) goto L_0x0037
            r5 = 1
        L_0x0037:
            java.io.File r2 = new java.io.File
            android.content.Context r0 = r7.A04
            java.io.File r1 = r0.getCacheDir()
            java.lang.String r3 = "fbnslite_log"
            java.lang.String r0 = X.AnonymousClass08S.A09(r3, r5)
            r2.<init>(r1, r0)
            boolean r0 = r2.exists()
            if (r0 == 0) goto L_0x0051
            r6.add(r2)
        L_0x0051:
            java.io.File r2 = new java.io.File
            android.content.Context r0 = r7.A04
            java.io.File r1 = r0.getCacheDir()
            int r0 = r7.A00
            java.lang.String r0 = X.AnonymousClass08S.A09(r3, r0)
            r2.<init>(r1, r0)
            boolean r0 = r2.exists()
            if (r0 == 0) goto L_0x006b
            r6.add(r2)
        L_0x006b:
            java.util.Iterator r5 = r6.iterator()
        L_0x006f:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x00af
            java.lang.Object r2 = r5.next()
            java.io.File r2 = (java.io.File) r2
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x009a }
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ IOException -> 0x009a }
            r0.<init>(r2)     // Catch:{ IOException -> 0x009a }
            r1.<init>(r0)     // Catch:{ IOException -> 0x009a }
        L_0x0085:
            java.lang.String r0 = r1.readLine()     // Catch:{ all -> 0x0093 }
            if (r0 == 0) goto L_0x008f
            r4.add(r0)     // Catch:{ all -> 0x0093 }
            goto L_0x0085
        L_0x008f:
            r1.close()     // Catch:{ IOException -> 0x009a }
            goto L_0x006f
        L_0x0093:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0095 }
        L_0x0095:
            r0 = move-exception
            r1.close()     // Catch:{ all -> 0x0099 }
        L_0x0099:
            throw r0     // Catch:{ IOException -> 0x009a }
        L_0x009a:
            r0 = move-exception
            java.lang.String r3 = "Error reading file "
            java.lang.String r2 = r2.getName()
            java.lang.String r1 = " - "
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = X.AnonymousClass08S.A0S(r3, r2, r1, r0)
            r4.add(r0)
            goto L_0x006f
        L_0x00af:
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            java.lang.String r0 = "flytrap"
            r1.putStringArrayList(r0, r4)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0O0.AXc(com.facebook.rti.push.service.FbnsService, android.os.Bundle):android.os.Bundle");
    }

    public void AXk(FbnsService fbnsService, Bundle bundle) {
        throw new IllegalArgumentException("not implemented for FlytrapReportFetcher");
    }
}
