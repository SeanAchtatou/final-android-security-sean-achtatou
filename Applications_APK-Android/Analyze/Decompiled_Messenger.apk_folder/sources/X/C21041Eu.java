package X;

import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Eu  reason: invalid class name and case insensitive filesystem */
public final class C21041Eu {
    private static volatile C21041Eu A05;
    private Long A00;
    private Long A01;
    private Long A02;
    private Long A03;
    public final C13850sB A04;

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0005, code lost:
        if (r2.A00 == null) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long A01(boolean r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 != 0) goto L_0x0007
            java.lang.Long r0 = r2.A00     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x0011
        L_0x0007:
            long r0 = X.C50402dv.A00()     // Catch:{ all -> 0x0019 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0019 }
            r2.A00 = r0     // Catch:{ all -> 0x0019 }
        L_0x0011:
            java.lang.Long r0 = r2.A00     // Catch:{ all -> 0x0019 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0019 }
            monitor-exit(r2)
            return r0
        L_0x0019:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21041Eu.A01(boolean):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0005, code lost:
        if (r2.A01 == null) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long A02(boolean r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 != 0) goto L_0x0007
            java.lang.Long r0 = r2.A01     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x0011
        L_0x0007:
            long r0 = X.C50402dv.A00()     // Catch:{ all -> 0x0019 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0019 }
            r2.A01 = r0     // Catch:{ all -> 0x0019 }
        L_0x0011:
            java.lang.Long r0 = r2.A01     // Catch:{ all -> 0x0019 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0019 }
            monitor-exit(r2)
            return r0
        L_0x0019:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21041Eu.A02(boolean):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0005, code lost:
        if (r2.A02 == null) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long A03(boolean r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 != 0) goto L_0x0007
            java.lang.Long r0 = r2.A02     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x0011
        L_0x0007:
            long r0 = X.C50402dv.A00()     // Catch:{ all -> 0x0019 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0019 }
            r2.A02 = r0     // Catch:{ all -> 0x0019 }
        L_0x0011:
            java.lang.Long r0 = r2.A02     // Catch:{ all -> 0x0019 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0019 }
            monitor-exit(r2)
            return r0
        L_0x0019:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21041Eu.A03(boolean):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0005, code lost:
        if (r2.A03 == null) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long A04(boolean r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            if (r3 != 0) goto L_0x0007
            java.lang.Long r0 = r2.A03     // Catch:{ all -> 0x0019 }
            if (r0 != 0) goto L_0x0011
        L_0x0007:
            long r0 = X.C50402dv.A00()     // Catch:{ all -> 0x0019 }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ all -> 0x0019 }
            r2.A03 = r0     // Catch:{ all -> 0x0019 }
        L_0x0011:
            java.lang.Long r0 = r2.A03     // Catch:{ all -> 0x0019 }
            long r0 = r0.longValue()     // Catch:{ all -> 0x0019 }
            monitor-exit(r2)
            return r0
        L_0x0019:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21041Eu.A04(boolean):long");
    }

    public static final C21041Eu A00(AnonymousClass1XY r4) {
        if (A05 == null) {
            synchronized (C21041Eu.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A05, r4);
                if (A002 != null) {
                    try {
                        A05 = new C21041Eu(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A05;
    }

    private C21041Eu(AnonymousClass1XY r2) {
        C28801fO.A00(r2);
        this.A04 = new C13850sB(r2);
    }
}
