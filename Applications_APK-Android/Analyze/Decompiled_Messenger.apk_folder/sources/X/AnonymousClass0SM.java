package X;

import android.util.Pair;
import java.util.List;

/* renamed from: X.0SM  reason: invalid class name */
public final class AnonymousClass0SM implements Runnable {
    public static final String __redex_internal_original_name = "com.facebook.rti.mqtt.manager.FbnsConnectionManager$6";
    public final /* synthetic */ Pair A00;
    public final /* synthetic */ AnonymousClass0AH A01;

    public AnonymousClass0SM(AnonymousClass0AH r1, Pair pair) {
        this.A01 = r1;
        this.A00 = pair;
    }

    public void run() {
        this.A01.A0d((List) this.A00.first);
        this.A01.A0e((List) this.A00.second);
    }
}
