package X;

import android.content.Context;
import com.facebook.auth.userscope.UserScoped;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.0Zi  reason: invalid class name and case insensitive filesystem */
public final class C05540Zi {
    public Object A00;
    private C24921Xq A01;
    private AnonymousClass0Zu A02;
    private C24811Xe A03;
    private AnonymousClass0UO A04;
    private Byte A05;

    public final void A02() {
        try {
            C24811Xe r0 = this.A03;
            if (r0 != null) {
                r0.A03();
                r0.A02();
            }
            AnonymousClass0Zu r1 = this.A02;
            if (r1 != null) {
                if (this.A03 != null) {
                    ConcurrentMap concurrentMap = r1.A01;
                    Object obj = this.A00;
                    if (obj == null) {
                        obj = C24921Xq.A08;
                    }
                    if (concurrentMap.putIfAbsent(this, obj) != null) {
                        throw new IllegalStateException("Some other thread put a value in the ConcurrentMap even though we locked on the key");
                    }
                }
                AnonymousClass0Zu.A02.A02(this.A02);
            }
            Byte b = this.A05;
            if (b != null) {
                this.A04.A00 = b.byteValue();
            }
        } finally {
            this.A03 = null;
            this.A01 = null;
            this.A02 = null;
            this.A05 = null;
            this.A04 = null;
            this.A00 = null;
        }
    }

    public static C05540Zi A00(C05540Zi r1) {
        if (r1 == null) {
            r1 = new C05540Zi();
        }
        if (r1.A04 == null) {
            return r1;
        }
        throw new UnsupportedOperationException("reentrant injection or failed cleanup detected");
    }

    public final C24851Xi A01() {
        return this.A03.A01();
    }

    public final boolean A03(AnonymousClass1XY r4) {
        AnonymousClass0UO A002 = AnonymousClass0UO.A00();
        this.A04 = A002;
        byte b = A002.A00;
        A002.A00 = (byte) (4 | b);
        this.A05 = Byte.valueOf(b);
        Context Aq3 = r4.getScopeAwareInjector().Aq3();
        if (Aq3 != null) {
            C24921Xq r0 = (C24921Xq) r4.getScope(UserScoped.class);
            this.A01 = r0;
            AnonymousClass0Zu A022 = r0.A02(Aq3);
            this.A02 = A022;
            Object obj = A022.A01.get(this);
            if (obj != null) {
                if (obj == C24921Xq.A08) {
                    obj = null;
                }
                this.A00 = obj;
                return false;
            }
            this.A03 = this.A01.A04(this.A02);
            return true;
        }
        throw new C38471xU("Called user scoped provider outside of context scope");
    }
}
