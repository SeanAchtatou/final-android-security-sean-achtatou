package X;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import java.util.Collection;

/* renamed from: X.0TH  reason: invalid class name */
public final class AnonymousClass0TH {
    public static Collection A00(Collection collection, Function function) {
        return new AnonymousClass15Z(collection, function);
    }

    public static Collection A01(Collection collection, Predicate predicate) {
        if (collection instanceof C33011mh) {
            C33011mh r3 = (C33011mh) collection;
            return new C33011mh(r3.A01, Predicates.and(r3.A00, predicate));
        }
        Preconditions.checkNotNull(collection);
        Preconditions.checkNotNull(predicate);
        return new C33011mh(collection, predicate);
    }
}
