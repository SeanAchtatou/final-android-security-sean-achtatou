package X;

import com.facebook.acra.info.ExternalProcessInfo;

/* renamed from: X.06E  reason: invalid class name */
public abstract class AnonymousClass06E implements AnonymousClass09P {
    public void CGS(String str, String str2) {
        CGQ(AnonymousClass06F.A00(str, str2));
    }

    public void CGT(String str, String str2, int i) {
        CGQ(AnonymousClass06F.A01(str, str2, i));
    }

    public void CGU(String str, String str2, Throwable th, int i) {
        AnonymousClass06G A02 = AnonymousClass06F.A02(str, str2);
        A02.A03 = th;
        A02.A00 = i;
        CGQ(A02.A00());
    }

    public void CGV(String str, Throwable th, int i) {
        CGU(str, th.getMessage(), th, i);
    }

    public void CGY(String str, String str2) {
        AnonymousClass06G A02 = AnonymousClass06F.A02(str, str2);
        A02.A04 = true;
        CGQ(A02.A00());
    }

    public void CGZ(String str, String str2, Throwable th) {
        AnonymousClass06G A02 = AnonymousClass06F.A02(str, str2);
        A02.A04 = true;
        A02.A03 = th;
        CGQ(A02.A00());
    }

    public void CGa(String str, Throwable th) {
        CGZ(str, th.getMessage(), th);
    }

    public void CGR(AnonymousClass06F r1, ExternalProcessInfo externalProcessInfo) {
        CGQ(r1);
    }

    public void softReport(String str, String str2, Throwable th) {
        AnonymousClass06G A02 = AnonymousClass06F.A02(str, str2);
        A02.A03 = th;
        CGQ(A02.A00());
    }

    public void softReport(String str, Throwable th) {
        softReport(str, th.getMessage(), th);
    }
}
