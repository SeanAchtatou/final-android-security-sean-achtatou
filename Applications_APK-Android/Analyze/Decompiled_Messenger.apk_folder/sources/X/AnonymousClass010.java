package X;

/* renamed from: X.010  reason: invalid class name */
public enum AnonymousClass010 {
    DEBUG,
    IN_HOUSE,
    PROD;

    public String A00() {
        return String.format("com.facebook.permission.%s.FB_APP_COMMUNICATION", name().toLowerCase());
    }
}
