package X;

import java.io.Serializable;
import java.util.Arrays;

/* renamed from: X.0jx  reason: invalid class name and case insensitive filesystem */
public final class C10350jx implements Serializable {
    private static final long serialVersionUID = 1;
    public final transient int[] _asciiToBase64;
    public final transient byte[] _base64ToAsciiB;
    public final transient char[] _base64ToAsciiC;
    public final transient int _maxLineLength;
    public final String _name;
    public final transient char _paddingChar;
    public final transient boolean _usesPadding;

    public String encode(byte[] bArr, boolean z) {
        char c;
        int length = bArr.length;
        StringBuilder sb = new StringBuilder((length >> 2) + length + (length >> 3));
        if (z) {
            sb.append('\"');
        }
        int i = this._maxLineLength >> 2;
        int i2 = i;
        int i3 = 0;
        int i4 = length - 3;
        while (i3 <= i4) {
            int i5 = i3 + 1;
            int i6 = i5 + 1;
            int i7 = ((bArr[i3] << 8) | (bArr[i5] & 255)) << 8;
            i3 = i6 + 1;
            byte b = i7 | (bArr[i6] & 255);
            char[] cArr = this._base64ToAsciiC;
            sb.append(cArr[(b >> 18) & 63]);
            sb.append(cArr[(b >> 12) & 63]);
            sb.append(cArr[(b >> 6) & 63]);
            sb.append(cArr[b & 63]);
            i--;
            if (i <= 0) {
                sb.append('\\');
                sb.append('n');
                i = i2;
            }
        }
        int i8 = length - i3;
        if (i8 > 0) {
            int i9 = i3 + 1;
            int i10 = bArr[i3] << 16;
            if (i8 == 2) {
                i10 |= (bArr[i9] & 255) << 8;
            }
            char[] cArr2 = this._base64ToAsciiC;
            sb.append(cArr2[(i10 >> 18) & 63]);
            sb.append(cArr2[(i10 >> 12) & 63]);
            if (this._usesPadding) {
                if (i8 == 2) {
                    c = cArr2[(i10 >> 6) & 63];
                } else {
                    c = this._paddingChar;
                }
                sb.append(c);
                sb.append(this._paddingChar);
            } else if (i8 == 2) {
                sb.append(cArr2[(i10 >> 6) & 63]);
            }
        }
        if (z) {
            sb.append('\"');
        }
        return sb.toString();
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public static void _reportInvalidBase64(C10350jx r4, char c, int i, String str) {
        String A0P;
        if (c <= ' ') {
            A0P = AnonymousClass08S.A0R("Illegal white space character (code 0x", Integer.toHexString(c), ") as character #", i + 1, " of 4-char base64 unit: can only used between units");
        } else {
            char c2 = r4._paddingChar;
            boolean z = false;
            if (c == c2) {
                z = true;
            }
            if (z) {
                A0P = "Unexpected padding character ('" + c2 + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
            } else if (!Character.isDefined(c) || Character.isISOControl(c)) {
                A0P = AnonymousClass08S.A0P("Illegal character (code 0x", Integer.toHexString(c), ") in base64 content");
            } else {
                A0P = "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
            }
        }
        if (str != null) {
            A0P = AnonymousClass08S.A0P(A0P, ": ", str);
        }
        throw new IllegalArgumentException(A0P);
    }

    public int decodeBase64Char(char c) {
        if (c <= 127) {
            return this._asciiToBase64[c];
        }
        return -1;
    }

    public int hashCode() {
        return this._name.hashCode();
    }

    public Object readResolve() {
        String A0P;
        String str = this._name;
        C10350jx r1 = C10340jw.MIME;
        if (!r1._name.equals(str)) {
            r1 = C10340jw.MIME_NO_LINEFEEDS;
            if (!r1._name.equals(str)) {
                r1 = C10340jw.PEM;
                if (!r1._name.equals(str)) {
                    r1 = C10340jw.MODIFIED_FOR_URL;
                    if (!r1._name.equals(str)) {
                        if (str == null) {
                            A0P = "<null>";
                        } else {
                            A0P = AnonymousClass08S.A0P("'", str, "'");
                        }
                        throw new IllegalArgumentException(AnonymousClass08S.A0J("No Base64Variant with name ", A0P));
                    }
                }
            }
        }
        return r1;
    }

    public String toString() {
        return this._name;
    }

    public C10350jx(C10350jx r5, String str, boolean z, char c, int i) {
        this._asciiToBase64 = new int[128];
        this._base64ToAsciiC = new char[64];
        byte[] bArr = new byte[64];
        this._base64ToAsciiB = bArr;
        this._name = str;
        byte[] bArr2 = r5._base64ToAsciiB;
        System.arraycopy(bArr2, 0, bArr, 0, bArr2.length);
        char[] cArr = r5._base64ToAsciiC;
        System.arraycopy(cArr, 0, this._base64ToAsciiC, 0, cArr.length);
        int[] iArr = r5._asciiToBase64;
        System.arraycopy(iArr, 0, this._asciiToBase64, 0, iArr.length);
        this._usesPadding = z;
        this._paddingChar = c;
        this._maxLineLength = i;
    }

    public C10350jx(String str, String str2, boolean z, char c, int i) {
        this._asciiToBase64 = new int[128];
        char[] cArr = new char[64];
        this._base64ToAsciiC = cArr;
        this._base64ToAsciiB = new byte[64];
        this._name = str;
        this._usesPadding = z;
        this._paddingChar = c;
        this._maxLineLength = i;
        int length = str2.length();
        if (length == 64) {
            str2.getChars(0, length, cArr, 0);
            Arrays.fill(this._asciiToBase64, -1);
            for (int i2 = 0; i2 < length; i2++) {
                char c2 = this._base64ToAsciiC[i2];
                this._base64ToAsciiB[i2] = (byte) c2;
                this._asciiToBase64[c2] = i2;
            }
            if (z) {
                this._asciiToBase64[c] = -2;
                return;
            }
            return;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A0A("Base64Alphabet length must be exactly 64 (was ", length, ")"));
    }
}
