package X;

import com.facebook.mig.scheme.interfaces.MigColorScheme;
import com.google.common.base.Preconditions;

/* renamed from: X.0y4  reason: invalid class name and case insensitive filesystem */
public final class C16940y4 implements C17100yK {
    public final /* synthetic */ C13450rS A00;

    public C16940y4(C13450rS r1) {
        this.A00 = r1;
    }

    public void C2A() {
        C13450rS r3 = this.A00;
        Preconditions.checkNotNull(r3.A04);
        C15980wI.A00(r3.A04.A01, ((MigColorScheme) AnonymousClass1XX.A03(AnonymousClass1Y3.Anh, r3.A02)).B9m());
        C13450rS r2 = this.A00;
        boolean z = false;
        if (r2.A04 != null) {
            z = true;
        }
        if (z) {
            C13450rS.A06(r2);
            C13450rS.A05(r2);
        }
    }
}
