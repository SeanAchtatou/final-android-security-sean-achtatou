package X;

import java.io.InputStream;

/* renamed from: X.1rg  reason: invalid class name and case insensitive filesystem */
public final class C35721rg implements C24011Rv {
    private final long A00;
    private final InputStream A01;

    public InputStream openStream() {
        return this.A01;
    }

    public long size() {
        return this.A00;
    }

    public C35721rg(InputStream inputStream, long j) {
        this.A01 = inputStream;
        this.A00 = j;
    }
}
