package X;

import com.facebook.ipc.stories.model.StoryBucket;
import com.facebook.ipc.stories.model.StoryCard;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1vR  reason: invalid class name */
public final class AnonymousClass1vR extends C17770zR {
    @Comparable(type = 13)
    public StoryBucket A00;
    @Comparable(type = 13)
    public StoryCard A01;
    @Comparable(type = 13)
    public C855643z A02;
    @Comparable(type = 13)
    public C88334Jw A03;
    @Comparable(type = 13)
    public AnonymousClass466 A04;
    @Comparable(type = 13)
    public C854143i A05;
    @Comparable(type = 13)
    public AnonymousClass4O9 A06;
    @Comparable(type = 13)
    public AnonymousClass463 A07;
    @Comparable(type = 3)
    public boolean A08;
    @Comparable(type = 3)
    public boolean A09;

    public AnonymousClass1vR() {
        super("StoryViewerFeedbackOverlayComponent");
    }

    public static boolean A00(StoryCard storyCard) {
        if (storyCard.A0T() == null || storyCard.A0T().isEmpty()) {
            return false;
        }
        return true;
    }
}
