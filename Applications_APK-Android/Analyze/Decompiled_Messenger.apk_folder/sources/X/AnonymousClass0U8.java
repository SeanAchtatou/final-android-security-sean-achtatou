package X;

import com.google.common.base.Preconditions;
import java.lang.reflect.Array;
import java.util.Collection;

/* renamed from: X.0U8  reason: invalid class name */
public final class AnonymousClass0U8 {
    public static Object[] A03(Object[] objArr, Object[] objArr2, Class cls) {
        int length = objArr.length;
        int length2 = objArr2.length;
        Object[] objArr3 = (Object[]) Array.newInstance(cls, length + length2);
        System.arraycopy(objArr, 0, objArr3, 0, length);
        System.arraycopy(objArr2, 0, objArr3, length, length2);
        return objArr3;
    }

    public static void A00(Object obj, int i) {
        if (obj == null) {
            throw new NullPointerException(AnonymousClass08S.A09("at index ", i));
        }
    }

    public static Object[] A02(Object[] objArr, int i, int i2, Object[] objArr2) {
        Preconditions.checkPositionIndexes(i, i + i2, objArr.length);
        int length = objArr2.length;
        if (length < i2) {
            objArr2 = (Object[]) Array.newInstance(objArr2.getClass().getComponentType(), i2);
        } else if (length > i2) {
            objArr2[i2] = null;
        }
        System.arraycopy(objArr, i, objArr2, 0, i2);
        return objArr2;
    }

    public static Object[] A01(Collection collection, Object[] objArr) {
        int size = collection.size();
        if (objArr.length < size) {
            objArr = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), size);
        }
        int i = 0;
        for (Object obj : collection) {
            objArr[i] = obj;
            i++;
        }
        if (objArr.length > size) {
            objArr[size] = null;
        }
        return objArr;
    }
}
