package X;

import android.util.SparseArray;

/* renamed from: X.0iv  reason: invalid class name and case insensitive filesystem */
public final class C09930iv extends SparseArray {
    public final Object A00;

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0021, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get(int r4) {
        /*
            r3 = this;
            java.lang.Object r2 = r3.A00
            monitor-enter(r2)
            java.lang.Object r1 = super.get(r4)     // Catch:{ all -> 0x0022 }
            android.util.SparseArray r1 = (android.util.SparseArray) r1     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x0020
            boolean r0 = r1 instanceof X.AnonymousClass38F     // Catch:{ all -> 0x0022 }
            if (r0 != 0) goto L_0x0020
            X.38F r0 = new X.38F     // Catch:{ all -> 0x0022 }
            r0.<init>(r1)     // Catch:{ all -> 0x0022 }
            java.lang.Object r1 = r3.A00     // Catch:{ all -> 0x0022 }
            monitor-enter(r1)     // Catch:{ all -> 0x0022 }
            super.put(r4, r0)     // Catch:{ all -> 0x001d }
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            monitor-exit(r2)     // Catch:{ all -> 0x0022 }
            return r0
        L_0x001d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            throw r0     // Catch:{ all -> 0x0022 }
        L_0x0020:
            monitor-exit(r2)     // Catch:{ all -> 0x0022 }
            return r1
        L_0x0022:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0022 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C09930iv.get(int):java.lang.Object");
    }

    public /* bridge */ /* synthetic */ void put(int i, Object obj) {
        SparseArray sparseArray = (SparseArray) obj;
        synchronized (this.A00) {
            super.put(i, sparseArray);
        }
    }

    public C09930iv(Object obj, int i) {
        super(i);
        this.A00 = obj;
    }
}
