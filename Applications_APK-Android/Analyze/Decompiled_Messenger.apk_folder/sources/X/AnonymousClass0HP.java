package X;

import java.util.Properties;
import java.util.concurrent.Callable;

/* renamed from: X.0HP  reason: invalid class name */
public final class AnonymousClass0HP implements Callable {
    public final /* synthetic */ Properties A00;

    public AnonymousClass0HP(Properties properties) {
        this.A00 = properties;
    }

    public Object call() {
        return this.A00;
    }
}
