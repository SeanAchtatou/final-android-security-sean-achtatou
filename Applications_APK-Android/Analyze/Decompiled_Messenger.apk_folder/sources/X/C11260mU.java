package X;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ser.impl.FailingSerializer;
import com.fasterxml.jackson.databind.ser.impl.TypeWrappedSerializer;
import com.fasterxml.jackson.databind.ser.impl.UnknownSerializer;
import com.fasterxml.jackson.databind.ser.std.NullSerializer;
import java.text.DateFormat;
import java.util.Date;

/* renamed from: X.0mU  reason: invalid class name and case insensitive filesystem */
public abstract class C11260mU extends C11270mV {
    public static final JsonSerializer DEFAULT_NULL_KEY_SERIALIZER = new FailingSerializer("Null key for a Map not allowed in JSON (use a converting NullKeySerializer?)");
    public static final JsonSerializer DEFAULT_UNKNOWN_SERIALIZER = new UnknownSerializer();
    public static final C10030jR TYPE_OBJECT = new C10020jP(Object.class);
    public final C10450k8 _config;
    public DateFormat _dateFormat;
    public JsonSerializer _keySerializer;
    public final C44662Jb _knownSerializers;
    public JsonSerializer _nullKeySerializer;
    public JsonSerializer _nullValueSerializer;
    public final C10440k7 _rootNames;
    public final Class _serializationView;
    public final C11390mq _serializerCache;
    public final C26861cG _serializerFactory;
    public JsonSerializer _unknownTypeSerializer;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.0mU.findTypedValueSerializer(java.lang.Class, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer
     arg types: [java.lang.Class<?>, int, ?[OBJECT, ARRAY]]
     candidates:
      X.0mU.findTypedValueSerializer(X.0jR, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer
      X.0mU.findTypedValueSerializer(java.lang.Class, boolean, X.CX9):com.fasterxml.jackson.databind.JsonSerializer */
    public final void defaultSerializeValue(Object obj, C11710np r5) {
        if (obj == null) {
            this._nullValueSerializer.serialize(null, r5, this);
        } else {
            findTypedValueSerializer((Class) obj.getClass(), true, (CX9) null).serialize(obj, r5, this);
        }
    }

    public abstract CZm findObjectId(Object obj, C25128CaN caN);

    public abstract JsonSerializer serializerInstance(C10080jW r1, Object obj);

    public static final DateFormat _dateFormat(C11260mU r1) {
        DateFormat dateFormat = r1._dateFormat;
        if (dateFormat != null) {
            return dateFormat;
        }
        DateFormat dateFormat2 = (DateFormat) r1._config._base._dateFormat.clone();
        r1._dateFormat = dateFormat2;
        return dateFormat2;
    }

    public void defaultSerializeDateKey(Date date, C11710np r4) {
        if (isEnabled(C10480kE.WRITE_DATE_KEYS_AS_TIMESTAMPS)) {
            r4.writeFieldName(String.valueOf(date.getTime()));
        } else {
            r4.writeFieldName(_dateFormat(this).format(date));
        }
    }

    public final void defaultSerializeDateValue(Date date, C11710np r4) {
        if (isEnabled(C10480kE.WRITE_DATES_AS_TIMESTAMPS)) {
            r4.writeNumber(date.getTime());
        } else {
            r4.writeString(_dateFormat(this).format(date));
        }
    }

    public final void defaultSerializeNull(C11710np r3) {
        this._nullValueSerializer.serialize(null, r3, this);
    }

    public JsonSerializer findKeySerializer(C10030jR r4, CX9 cx9) {
        JsonSerializer createKeySerializer = this._serializerFactory.createKeySerializer(this._config, r4, this._keySerializer);
        if (createKeySerializer instanceof AnonymousClass2HD) {
            ((AnonymousClass2HD) createKeySerializer).resolve(this);
        }
        if (createKeySerializer instanceof C11690nn) {
            return ((C11690nn) createKeySerializer).createContextual(this, cx9);
        }
        return createKeySerializer;
    }

    public final C10140jc getAnnotationIntrospector() {
        return this._config.getAnnotationIntrospector();
    }

    public /* bridge */ /* synthetic */ C10470kA getConfig() {
        return this._config;
    }

    public final C10300js getTypeFactory() {
        return this._config._base._typeFactory;
    }

    public final boolean isEnabled(C10480kE r2) {
        return this._config.isEnabled(r2);
    }

    public C11260mU() {
        this._unknownTypeSerializer = DEFAULT_UNKNOWN_SERIALIZER;
        this._nullValueSerializer = NullSerializer.instance;
        this._nullKeySerializer = DEFAULT_NULL_KEY_SERIALIZER;
        this._config = null;
        this._serializerFactory = null;
        this._serializerCache = new C11390mq();
        this._knownSerializers = null;
        this._rootNames = new C10440k7();
        this._serializationView = null;
    }

    public C11260mU(C11260mU r5, C10450k8 r6, C26861cG r7) {
        C44662Jb r2;
        this._unknownTypeSerializer = DEFAULT_UNKNOWN_SERIALIZER;
        this._nullValueSerializer = NullSerializer.instance;
        this._nullKeySerializer = DEFAULT_NULL_KEY_SERIALIZER;
        if (r6 != null) {
            this._serializerFactory = r7;
            this._config = r6;
            C11390mq r3 = r5._serializerCache;
            this._serializerCache = r3;
            this._unknownTypeSerializer = r5._unknownTypeSerializer;
            this._keySerializer = r5._keySerializer;
            this._nullValueSerializer = r5._nullValueSerializer;
            this._nullKeySerializer = r5._nullKeySerializer;
            this._rootNames = r5._rootNames;
            synchronized (r3) {
                r2 = r3._readOnlyMap;
                if (r2 == null) {
                    r2 = new C44662Jb(new C48772b6(r3._sharedMap));
                    r3._readOnlyMap = r2;
                }
            }
            this._knownSerializers = new C44662Jb(r2._map);
            this._serializationView = r6._view;
            return;
        }
        throw new NullPointerException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.4Gr.<init>(X.0jR, boolean):void
     arg types: [X.0jR, int]
     candidates:
      X.4Gr.<init>(java.lang.Class, boolean):void
      X.4Gr.<init>(X.0jR, boolean):void */
    public JsonSerializer findTypedValueSerializer(C10030jR r6, boolean z, CX9 cx9) {
        C44662Jb r2 = this._knownSerializers;
        C87814Gr r1 = r2._cacheKey;
        if (r1 == null) {
            r2._cacheKey = new C87814Gr(r6, true);
        } else {
            r1._type = r6;
            r1._class = null;
            r1._isTyped = true;
            r1._hashCode = (r6.hashCode() - 1) - 1;
        }
        JsonSerializer find = r2._map.find(r2._cacheKey);
        if (find == null) {
            C11390mq r3 = this._serializerCache;
            synchronized (r3) {
                try {
                    find = (JsonSerializer) r3._sharedMap.get(new C87814Gr(r6, true));
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            if (find == null) {
                find = findValueSerializer(r6, cx9);
                CY1 createTypeSerializer = this._serializerFactory.createTypeSerializer(this._config, r6);
                if (createTypeSerializer != null) {
                    find = new TypeWrappedSerializer(createTypeSerializer.forProperty(cx9), find);
                }
                if (z) {
                    C11390mq r32 = this._serializerCache;
                    synchronized (r32) {
                        try {
                            if (r32._sharedMap.put(new C87814Gr(r6, true), find) == null) {
                                r32._readOnlyMap = null;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            throw th;
                        }
                    }
                    return find;
                }
            }
        }
        return find;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.4Gr.<init>(java.lang.Class, boolean):void
     arg types: [java.lang.Class, int]
     candidates:
      X.4Gr.<init>(X.0jR, boolean):void
      X.4Gr.<init>(java.lang.Class, boolean):void */
    public JsonSerializer findTypedValueSerializer(Class cls, boolean z, CX9 cx9) {
        C44662Jb r2 = this._knownSerializers;
        C87814Gr r1 = r2._cacheKey;
        if (r1 == null) {
            r2._cacheKey = new C87814Gr(cls, true);
        } else {
            r1._type = null;
            r1._class = cls;
            r1._isTyped = true;
            r1._hashCode = cls.getName().hashCode() + 1;
        }
        JsonSerializer find = r2._map.find(r2._cacheKey);
        if (find == null) {
            C11390mq r3 = this._serializerCache;
            synchronized (r3) {
                try {
                    find = (JsonSerializer) r3._sharedMap.get(new C87814Gr(cls, true));
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            if (find == null) {
                find = findValueSerializer(cls, cx9);
                C26861cG r22 = this._serializerFactory;
                C10450k8 r12 = this._config;
                CY1 createTypeSerializer = r22.createTypeSerializer(r12, r12.constructType(cls));
                if (createTypeSerializer != null) {
                    find = new TypeWrappedSerializer(createTypeSerializer.forProperty(cx9), find);
                }
                if (z) {
                    C11390mq r32 = this._serializerCache;
                    synchronized (r32) {
                        try {
                            if (r32._sharedMap.put(new C87814Gr(cls, true), find) == null) {
                                r32._readOnlyMap = null;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            throw th;
                        }
                    }
                    return find;
                }
            }
        }
        return find;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.4Gr.<init>(X.0jR, boolean):void
     arg types: [X.0jR, int]
     candidates:
      X.4Gr.<init>(java.lang.Class, boolean):void
      X.4Gr.<init>(X.0jR, boolean):void */
    public JsonSerializer findValueSerializer(C10030jR r8, CX9 cx9) {
        C44662Jb r2 = this._knownSerializers;
        C87814Gr r1 = r2._cacheKey;
        if (r1 == null) {
            r2._cacheKey = new C87814Gr(r8, false);
        } else {
            r1._type = r8;
            r1._class = null;
            r1._isTyped = false;
            r1._hashCode = r8.hashCode() - 1;
        }
        JsonSerializer find = r2._map.find(r2._cacheKey);
        if (find == null && (find = this._serializerCache.untypedValueSerializer(r8)) == null) {
            try {
                JsonSerializer createSerializer = this._serializerFactory.createSerializer(this, r8);
                if (createSerializer != null) {
                    C11390mq r5 = this._serializerCache;
                    JsonSerializer jsonSerializer = createSerializer;
                    synchronized (r5) {
                        if (r5._sharedMap.put(new C87814Gr(r8, false), createSerializer) == null) {
                            r5._readOnlyMap = null;
                        }
                        if (createSerializer instanceof AnonymousClass2HD) {
                            ((AnonymousClass2HD) jsonSerializer).resolve(this);
                        }
                    }
                }
                find = createSerializer;
                if (createSerializer == null) {
                    return this._unknownTypeSerializer;
                }
            } catch (IllegalArgumentException e) {
                throw new C37701w6(e.getMessage(), null, e);
            }
        }
        if (find instanceof C11690nn) {
            return ((C11690nn) find).createContextual(this, cx9);
        }
        return find;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: X.4Gr.<init>(java.lang.Class, boolean):void
     arg types: [java.lang.Class, int]
     candidates:
      X.4Gr.<init>(X.0jR, boolean):void
      X.4Gr.<init>(java.lang.Class, boolean):void */
    public JsonSerializer findValueSerializer(Class cls, CX9 cx9) {
        C44662Jb r2 = this._knownSerializers;
        C87814Gr r1 = r2._cacheKey;
        if (r1 == null) {
            r2._cacheKey = new C87814Gr(cls, false);
        } else {
            r1._type = null;
            r1._class = cls;
            r1._isTyped = false;
            r1._hashCode = cls.getName().hashCode();
        }
        JsonSerializer find = r2._map.find(r2._cacheKey);
        if (find == null) {
            C11390mq r3 = this._serializerCache;
            synchronized (r3) {
                try {
                    find = (JsonSerializer) r3._sharedMap.get(new C87814Gr(cls, false));
                } catch (Throwable th) {
                    while (true) {
                        th = th;
                    }
                }
            }
            if (find == null && (find = this._serializerCache.untypedValueSerializer(this._config.constructType(cls))) == null) {
                try {
                    JsonSerializer createSerializer = this._serializerFactory.createSerializer(this, this._config.constructType(cls));
                    if (createSerializer != null) {
                        C11390mq r5 = this._serializerCache;
                        JsonSerializer jsonSerializer = createSerializer;
                        synchronized (r5) {
                            try {
                                if (r5._sharedMap.put(new C87814Gr(cls, false), createSerializer) == null) {
                                    r5._readOnlyMap = null;
                                }
                                if (createSerializer instanceof AnonymousClass2HD) {
                                    ((AnonymousClass2HD) jsonSerializer).resolve(this);
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                throw th;
                            }
                        }
                    }
                    find = createSerializer;
                    if (createSerializer == null) {
                        return this._unknownTypeSerializer;
                    }
                } catch (IllegalArgumentException e) {
                    throw new C37701w6(e.getMessage(), null, e);
                }
            }
        }
        if (find instanceof C11690nn) {
            return ((C11690nn) find).createContextual(this, cx9);
        }
        return find;
    }
}
