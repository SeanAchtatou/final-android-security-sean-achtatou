package X;

import android.content.Context;
import android.content.pm.PackageManager;

/* renamed from: X.0ZP  reason: invalid class name */
public final class AnonymousClass0ZP {
    public static volatile long A00 = -1;
    private static volatile int A01;

    public static int A00(Context context) {
        if (A01 == 0) {
            try {
                A01 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return A01;
    }
}
