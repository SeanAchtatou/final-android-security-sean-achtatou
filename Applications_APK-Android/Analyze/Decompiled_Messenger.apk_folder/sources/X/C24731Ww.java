package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.facebook.acra.LogCatCollector;
import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Properties;

/* renamed from: X.1Ww  reason: invalid class name and case insensitive filesystem */
public final class C24731Ww {
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0084, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0085, code lost:
        if (r8 != null) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        A09(r1, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        A08(r1, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0091, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C72203dr A00(android.content.Context r10, java.lang.String r11, X.C72203dr r12, boolean r13) {
        /*
            r9 = 3
            java.lang.String r3 = "FirebaseInstanceId"
            java.util.Properties r7 = new java.util.Properties
            r7.<init>()
            java.security.KeyPair r0 = r12.A01
            java.security.PublicKey r0 = r0.getPublic()
            byte[] r1 = r0.getEncoded()
            r0 = 11
            java.lang.String r1 = android.util.Base64.encodeToString(r1, r0)
            java.lang.String r0 = "pub"
            r7.setProperty(r0, r1)
            java.security.KeyPair r0 = r12.A01
            java.security.PrivateKey r0 = r0.getPrivate()
            byte[] r1 = r0.getEncoded()
            r0 = 11
            java.lang.String r1 = android.util.Base64.encodeToString(r1, r0)
            java.lang.String r0 = "pri"
            r7.setProperty(r0, r1)
            long r0 = r12.A00
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "cre"
            r7.setProperty(r0, r1)
            java.io.File r1 = A04(r10, r11)
            r4 = 0
            r1.createNewFile()     // Catch:{ IOException -> 0x0092 }
            java.io.RandomAccessFile r6 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0092 }
            java.lang.String r0 = "rw"
            r6.<init>(r1, r0)     // Catch:{ IOException -> 0x0092 }
            java.nio.channels.FileChannel r8 = r6.getChannel()     // Catch:{ all -> 0x008b }
            r8.lock()     // Catch:{ all -> 0x0082 }
            r0 = 0
            if (r13 == 0) goto L_0x0071
            long r10 = r8.size()     // Catch:{ all -> 0x0082 }
            int r2 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0071
            r8.position(r0)     // Catch:{ 1wX | IOException -> 0x0067 }
            X.3dr r12 = A03(r8)     // Catch:{ 1wX | IOException -> 0x0067 }
            goto L_0x007b
        L_0x0067:
            r5 = move-exception
            boolean r2 = android.util.Log.isLoggable(r3, r9)     // Catch:{ all -> 0x0082 }
            if (r2 == 0) goto L_0x0071
            java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0082 }
        L_0x0071:
            r8.position(r0)     // Catch:{ all -> 0x0082 }
            java.io.OutputStream r0 = java.nio.channels.Channels.newOutputStream(r8)     // Catch:{ all -> 0x0082 }
            r7.store(r0, r4)     // Catch:{ all -> 0x0082 }
        L_0x007b:
            A09(r4, r8)     // Catch:{ all -> 0x008b }
            A08(r4, r6)     // Catch:{ IOException -> 0x0092 }
            return r12
        L_0x0082:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0084 }
        L_0x0084:
            r0 = move-exception
            if (r8 == 0) goto L_0x008a
            A09(r1, r8)     // Catch:{ all -> 0x008b }
        L_0x008a:
            throw r0     // Catch:{ all -> 0x008b }
        L_0x008b:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x008d }
        L_0x008d:
            r0 = move-exception
            A08(r1, r6)     // Catch:{ IOException -> 0x0092 }
            throw r0     // Catch:{ IOException -> 0x0092 }
        L_0x0092:
            r0 = move-exception
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r0 = r2.length()
            int r0 = r0 + 21
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Failed to write key: "
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            android.util.Log.w(r3, r0)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24731Ww.A00(android.content.Context, java.lang.String, X.3dr, boolean):X.3dr");
    }

    public static C72203dr A01(SharedPreferences sharedPreferences, String str) {
        long j;
        String string = sharedPreferences.getString(C24721Wv.A00(str, "|P|"), null);
        String string2 = sharedPreferences.getString(C24721Wv.A00(str, "|K|"), null);
        if (string == null || string2 == null) {
            return null;
        }
        KeyPair A05 = A05(string, string2);
        String string3 = sharedPreferences.getString(C24721Wv.A00(str, "cre"), null);
        if (string3 != null) {
            try {
                j = Long.parseLong(string3);
            } catch (NumberFormatException unused) {
            }
            return new C72203dr(A05, j);
        }
        j = 0;
        return new C72203dr(A05, j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r3 != null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        A09(r1, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0028, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002c, code lost:
        A07(r1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002f, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final X.C72203dr A02(java.io.File r8) {
        /*
            java.io.FileInputStream r2 = new java.io.FileInputStream
            r2.<init>(r8)
            java.nio.channels.FileChannel r3 = r2.getChannel()     // Catch:{ all -> 0x0029 }
            r4 = 0
            r6 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r8 = 1
            r3.lock(r4, r6, r8)     // Catch:{ all -> 0x0020 }
            X.3dr r1 = A03(r3)     // Catch:{ all -> 0x0020 }
            r0 = 0
            A09(r0, r3)     // Catch:{ all -> 0x0029 }
            A07(r0, r2)
            return r1
        L_0x0020:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0022 }
        L_0x0022:
            r0 = move-exception
            if (r3 == 0) goto L_0x0028
            A09(r1, r3)     // Catch:{ all -> 0x0029 }
        L_0x0028:
            throw r0     // Catch:{ all -> 0x0029 }
        L_0x0029:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
            A07(r1, r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C24731Ww.A02(java.io.File):X.3dr");
    }

    private static C72203dr A03(FileChannel fileChannel) {
        Properties properties = new Properties();
        properties.load(Channels.newInputStream(fileChannel));
        String property = properties.getProperty("pub");
        String property2 = properties.getProperty("pri");
        if (property == null || property2 == null) {
            throw new C37901wX("Invalid properties file");
        }
        try {
            return new C72203dr(A05(property, property2), Long.parseLong(properties.getProperty("cre")));
        } catch (NumberFormatException e) {
            throw new C37901wX(e);
        }
    }

    private static KeyPair A05(String str, String str2) {
        try {
            byte[] decode = Base64.decode(str, 8);
            byte[] decode2 = Base64.decode(str2, 8);
            try {
                KeyFactory instance = KeyFactory.getInstance("RSA");
                return new KeyPair(instance.generatePublic(new X509EncodedKeySpec(decode)), instance.generatePrivate(new PKCS8EncodedKeySpec(decode2)));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(valueOf.length() + 19);
                sb.append("Invalid key stored ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                throw new C37901wX(e);
            }
        } catch (IllegalArgumentException e2) {
            throw new C37901wX(e2);
        }
    }

    public static final void A06(Context context, String str, C72203dr r5) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.appid", 0);
        try {
            if (r5.equals(A01(sharedPreferences, str))) {
                return;
            }
        } catch (C37901wX unused) {
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(C24721Wv.A00(str, "|P|"), Base64.encodeToString(r5.A01.getPublic().getEncoded(), 11));
        edit.putString(C24721Wv.A00(str, "|K|"), Base64.encodeToString(r5.A01.getPrivate().getEncoded(), 11));
        edit.putString(C24721Wv.A00(str, "cre"), String.valueOf(r5.A00));
        edit.commit();
    }

    private static /* synthetic */ void A07(Throwable th, FileInputStream fileInputStream) {
        if (th != null) {
            try {
                fileInputStream.close();
            } catch (Throwable th2) {
                C196259Kr.A00.A00(th, th2);
            }
        } else {
            fileInputStream.close();
        }
    }

    private static /* synthetic */ void A08(Throwable th, RandomAccessFile randomAccessFile) {
        if (th != null) {
            try {
                randomAccessFile.close();
            } catch (Throwable th2) {
                C196259Kr.A00.A00(th, th2);
            }
        } else {
            randomAccessFile.close();
        }
    }

    private static /* synthetic */ void A09(Throwable th, FileChannel fileChannel) {
        if (th != null) {
            try {
                fileChannel.close();
            } catch (Throwable th2) {
                C196259Kr.A00.A00(th, th2);
            }
        } else {
            fileChannel.close();
        }
    }

    public final C72203dr A0A(Context context, String str) {
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
            instance.initialize(2048);
            C72203dr r3 = new C72203dr(instance.generateKeyPair(), System.currentTimeMillis());
            C72203dr A00 = A00(context, str, r3, true);
            if (A00 != null && !A00.equals(r3)) {
                return A00;
            }
            A06(context, str, r3);
            return r3;
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static File A04(Context context, String str) {
        String sb;
        if (TextUtils.isEmpty(str)) {
            sb = "com.google.InstanceId.properties";
        } else {
            try {
                String encodeToString = Base64.encodeToString(str.getBytes(LogCatCollector.UTF_8_ENCODING), 11);
                StringBuilder sb2 = new StringBuilder(String.valueOf(encodeToString).length() + 33);
                sb2.append("com.google.InstanceId_");
                sb2.append(encodeToString);
                sb2.append(".properties");
                sb = sb2.toString();
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
        File A04 = AnonymousClass01R.A04(context);
        if (A04 == null || !A04.isDirectory()) {
            Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
            A04 = context.getFilesDir();
        }
        return new File(A04, sb);
    }
}
