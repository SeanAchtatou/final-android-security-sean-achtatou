package X;

import java.util.List;

/* renamed from: X.1Qg  reason: invalid class name and case insensitive filesystem */
public final class C23631Qg extends C20831Dz {
    public C25675Ck7 A00;
    public List A01;

    public static final C23631Qg A00() {
        return new C23631Qg();
    }

    public int ArU() {
        List list = this.A01;
        if (list == null) {
            return 0;
        }
        return list.size();
    }
}
