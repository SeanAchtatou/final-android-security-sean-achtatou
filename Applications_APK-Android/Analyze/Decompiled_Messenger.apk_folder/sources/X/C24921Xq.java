package X;

import android.content.Context;
import com.facebook.auth.userscope.UserScoped;
import com.facebook.auth.viewercontext.ViewerContext;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

@UserScoped
/* renamed from: X.1Xq  reason: invalid class name and case insensitive filesystem */
public final class C24921Xq implements C24891Xn {
    public static final Object A07 = new Object();
    public static final Object A08 = new Object();
    private static final Class A09 = C24921Xq.class;
    public long A00 = -1;
    public C05560Zk A01;
    public final AnonymousClass1XX A02;
    public final Object A03 = new Object();
    public final Map A04 = AnonymousClass0TG.A04();
    private final Context A05;
    private final Map A06 = AnonymousClass0TG.A04();

    private void A00() {
        synchronized (this.A03) {
            if (this.A01 == null) {
                this.A01 = new C05560Zk(this.A05);
            }
        }
    }

    public Context A01() {
        return this.A02.getInjectorThreadStack().A00();
    }

    public C24811Xe A03(C25471Zt r4) {
        C24811Xe injectorThreadStack = this.A02.getInjectorThreadStack();
        injectorThreadStack.A01.add(injectorThreadStack.A00);
        injectorThreadStack.A02.add(r4);
        return injectorThreadStack;
    }

    public C04310Tq C4b(C22916BKm bKm, C04310Tq r3) {
        return new EIE(this, r3);
    }

    public C24921Xq(AnonymousClass1XX r3) {
        this.A02 = r3;
        this.A05 = r3.getInjectorThreadStack().A00();
    }

    public AnonymousClass0Zu A02(Context context) {
        ConcurrentMap concurrentMap;
        A00();
        A00();
        synchronized (this.A03) {
            try {
                if (this.A00 != -1 && this.A01.A01.now() > this.A00 + 30000) {
                    this.A04.clear();
                    this.A00 = -1;
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        C05920aY A002 = AnonymousClass0XL.A00(this.A02, context);
        ViewerContext B9R = A002.B9R();
        if (B9R != null) {
            String str = B9R.mUserId;
            synchronized (this.A03) {
                try {
                    ConcurrentMap concurrentMap2 = (ConcurrentMap) this.A06.get(str);
                    if (concurrentMap2 == null) {
                        concurrentMap2 = AnonymousClass0TG.A07();
                        this.A06.put(str, concurrentMap2);
                    }
                } catch (Throwable th2) {
                    while (true) {
                        th = th2;
                        break;
                    }
                }
            }
        } else {
            ViewerContext Awc = A002.Awc();
            synchronized (this.A03) {
                if (Awc == null) {
                    try {
                        C010708t.A06(A09, "Called user scoped provider with no viewer. ViewerContextManager was created with no ViewerContext. Using EmptyViewerContextManager to return fake logged in instance.");
                        String str2 = this.A01.A00.B9R().mUserId;
                        concurrentMap = (ConcurrentMap) this.A06.get(str2);
                        if (concurrentMap == null) {
                            concurrentMap = AnonymousClass0TG.A07();
                            this.A06.put(str2, concurrentMap);
                        }
                        A002 = this.A01.A00;
                    } catch (Throwable th3) {
                        th = th3;
                        throw th;
                    }
                } else {
                    String str3 = Awc.mUserId;
                    if (this.A04.containsKey(str3)) {
                        concurrentMap = (ConcurrentMap) this.A04.get(str3);
                    } else if (this.A06.containsKey(str3)) {
                        concurrentMap = (ConcurrentMap) this.A06.get(str3);
                    } else {
                        C010708t.A06(A09, "Called user scoped provider with no viewer. ViewerContextManager was created with no ViewerContext. Using EmptyViewerContextManager to return fake logged in instance.");
                        String str4 = this.A01.A00.B9R().mUserId;
                        ConcurrentMap concurrentMap3 = (ConcurrentMap) this.A06.get(str4);
                        if (concurrentMap3 == null) {
                            concurrentMap3 = AnonymousClass0TG.A07();
                            this.A06.put(str4, concurrentMap3);
                        }
                        A002 = this.A01.A00;
                    }
                }
            }
        }
        AnonymousClass0Zu r0 = (AnonymousClass0Zu) AnonymousClass0Zu.A02.A01();
        r0.A00 = A002;
        r0.A01 = concurrentMap;
        return r0;
    }

    public C24811Xe A04(AnonymousClass0Zu r6) {
        A00();
        ConcurrentMap concurrentMap = r6.A01;
        C25471Zt r0 = (C25471Zt) concurrentMap.get(A07);
        if (r0 == null) {
            C25471Zt r3 = new C25471Zt(this, this.A01.A02.A00, this.A02, r6.A00.B9R());
            r0 = (C25471Zt) concurrentMap.putIfAbsent(A07, r3);
            if (r0 == null) {
                r0 = r3;
            }
        }
        C24811Xe injectorThreadStack = this.A02.getInjectorThreadStack();
        injectorThreadStack.A01.add(injectorThreadStack.A00);
        injectorThreadStack.A02.add(r0);
        return injectorThreadStack;
    }

    public void A05() {
        A00();
        synchronized (this.A03) {
            for (Map values : this.A06.values()) {
                for (Object next : values.values()) {
                    if (next instanceof C05460Za) {
                        try {
                            ((C05460Za) next).clearUserData();
                        } catch (Exception e) {
                            ((AnonymousClass09P) this.A01.A03.get()).CGZ("UserScope", AnonymousClass08S.A0J(next.getClass().getName(), ".clearUserData() failure"), e);
                        }
                    }
                }
            }
            this.A04.putAll(this.A06);
            this.A00 = this.A01.A01.now();
            this.A06.clear();
        }
    }
}
