package X;

import io.card.payment.BuildConfig;

/* renamed from: X.0RN  reason: invalid class name */
public final class AnonymousClass0RN {
    public String A00 = BuildConfig.FLAVOR;
    public String A01 = BuildConfig.FLAVOR;
    public String A02 = BuildConfig.FLAVOR;
    public String A03 = BuildConfig.FLAVOR;
    public String A04 = BuildConfig.FLAVOR;
    public String A05 = BuildConfig.FLAVOR;
    public String A06 = BuildConfig.FLAVOR;

    public String toString() {
        return "FbnsNotificationMessage{mToken'=" + this.A06 + '\'' + ", mConnectionKey='" + this.A01 + '\'' + ", mPackageName='" + this.A04 + '\'' + ", mCollapseKey='" + this.A00 + '\'' + ", mPayload='" + this.A05 + '\'' + ", mNotifId='" + this.A03 + '\'' + ", mIsBuffered='" + this.A02 + '\'' + '}';
    }
}
