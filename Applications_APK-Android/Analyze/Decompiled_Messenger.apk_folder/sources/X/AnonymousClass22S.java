package X;

import com.facebook.auth.userscope.UserScoped;
import java.util.HashSet;
import java.util.Set;

@UserScoped
/* renamed from: X.22S  reason: invalid class name */
public final class AnonymousClass22S {
    private static C05540Zi A05;
    public long A00;
    public Set A01 = new HashSet();
    public boolean A02;
    public boolean A03;
    public final AnonymousClass069 A04;

    public synchronized void A01(boolean z) {
        this.A03 = z;
    }

    public static final AnonymousClass22S A00(AnonymousClass1XY r4) {
        AnonymousClass22S r0;
        synchronized (AnonymousClass22S.class) {
            C05540Zi A002 = C05540Zi.A00(A05);
            A05 = A002;
            try {
                if (A002.A03(r4)) {
                    A05.A00 = new AnonymousClass22S((AnonymousClass1XY) A05.A01());
                }
                C05540Zi r1 = A05;
                r0 = (AnonymousClass22S) r1.A00;
                r1.A02();
            } catch (Throwable th) {
                A05.A02();
                throw th;
            }
        }
        return r0;
    }

    public boolean A02(String str) {
        boolean z = false;
        if (Long.valueOf(this.A04.now()).longValue() - this.A00 > 60000) {
            z = true;
        }
        if (z) {
            return false;
        }
        boolean contains = this.A01.contains(str);
        if (contains) {
            A01(true);
        }
        return contains;
    }

    private AnonymousClass22S(AnonymousClass1XY r2) {
        AnonymousClass22R.A00(r2);
        this.A04 = AnonymousClass067.A03(r2);
    }
}
