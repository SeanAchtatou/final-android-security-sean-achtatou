package X;

import android.content.Intent;

/* renamed from: X.0SE  reason: invalid class name */
public abstract class AnonymousClass0SE {
    public final Intent A00;
    public final String A01;

    public AnonymousClass0SE(Intent intent, String str) {
        this.A00 = intent;
        this.A01 = str;
    }
}
