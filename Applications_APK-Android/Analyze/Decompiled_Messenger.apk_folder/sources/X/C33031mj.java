package X;

import java.util.Set;

/* renamed from: X.1mj  reason: invalid class name and case insensitive filesystem */
public abstract class C33031mj<E> extends C21221Fs<E> implements AnonymousClass0UG<E> {
    public abstract AnonymousClass0UG A06();

    public boolean equals(Object obj) {
        if (obj == this || A06().equals(obj)) {
            return true;
        }
        return false;
    }

    public int AMN(Object obj, int i) {
        return A06().AMN(obj, i);
    }

    public int AUa(Object obj) {
        return A06().AUa(obj);
    }

    public Set AYC() {
        return A06().AYC();
    }

    public int C18(Object obj, int i) {
        return A06().C18(obj, i);
    }

    public int C7L(Object obj, int i) {
        return A06().C7L(obj, i);
    }

    public boolean C7M(Object obj, int i, int i2) {
        return A06().C7M(obj, i, i2);
    }

    public Set entrySet() {
        return A06().entrySet();
    }

    public int hashCode() {
        return A06().hashCode();
    }
}
