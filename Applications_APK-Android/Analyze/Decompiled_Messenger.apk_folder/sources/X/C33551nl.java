package X;

import android.content.Context;
import android.content.Intent;
import com.facebook.common.dextricks.turboloader.TurboLoader;

/* renamed from: X.1nl  reason: invalid class name and case insensitive filesystem */
public final class C33551nl implements AnonymousClass06U {
    public final /* synthetic */ AnonymousClass1U0 A00;

    public C33551nl(AnonymousClass1U0 r1) {
        this.A00 = r1;
    }

    public void Bl1(Context context, Intent intent, AnonymousClass06Y r6) {
        int A002 = AnonymousClass09Y.A00(-608032881);
        if (TurboLoader.Locator.$const$string(12).equals(intent.getAction())) {
            AnonymousClass1U0.A03(this.A00, intent);
        }
        AnonymousClass09Y.A01(806641855, A002);
    }
}
