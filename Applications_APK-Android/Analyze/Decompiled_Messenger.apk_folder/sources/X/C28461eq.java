package X;

import android.content.Context;
import android.os.Handler;
import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1eq  reason: invalid class name and case insensitive filesystem */
public final class C28461eq {
    private static volatile C28461eq A0A;
    public AnonymousClass0UN A00;
    public Runnable A01;
    public Runnable A02;
    public Runnable A03;
    public boolean A04 = false;
    public final Object A05 = new Object();
    public final Object A06 = new Object();
    public final WeakHashMap A07 = new WeakHashMap();
    public final WeakHashMap A08 = new WeakHashMap();
    private final WeakHashMap A09 = new WeakHashMap();

    public static final C28461eq A00(AnonymousClass1XY r4) {
        if (A0A == null) {
            synchronized (C28461eq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0A, r4);
                if (A002 != null) {
                    try {
                        A0A = new C28461eq(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0A;
    }

    public static void A01(C28461eq r2, C25101Yi r3) {
        synchronized (r2.A06) {
            A02(r2, new AnonymousClass4LL(r2, r3));
        }
    }

    public static void A02(C28461eq r4, Runnable runnable) {
        C25121Yk r2 = (C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, r4.A00);
        if (r2.A02 == null) {
            r2.A02 = Boolean.valueOf(AnonymousClass00J.A01((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, r2.A00)).A29);
        }
        if (r2.A02.booleanValue()) {
            runnable.run();
            return;
        }
        C25121Yk r22 = (C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, r4.A00);
        if (r22.A01 == null) {
            r22.A01 = Boolean.valueOf(AnonymousClass00J.A01((Context) AnonymousClass1XX.A03(AnonymousClass1Y3.BCt, r22.A00)).A28);
        }
        if (r22.A01.booleanValue()) {
            AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(7, AnonymousClass1Y3.BSz, r4.A00), runnable, 453531512);
        } else {
            AnonymousClass07A.A04((ExecutorService) AnonymousClass1XX.A02(6, AnonymousClass1Y3.BNC, r4.A00), runnable, 430735496);
        }
    }

    private void A03(boolean z) {
        if (z) {
            if (this.A02 == null) {
                this.A02 = new C35821rs(this);
            }
            A02(this, this.A02);
            return;
        }
        if (this.A03 == null) {
            this.A03 = new C55602oM(this);
        }
        A02(this, this.A03);
    }

    public void A04(C25101Yi r4) {
        synchronized (this.A06) {
            A02(this, new C35791rp(this, r4, A0A()));
        }
    }

    public void A05(ListenableFuture listenableFuture) {
        Object obj = new Object();
        A07(obj);
        listenableFuture.addListener(new C35931s3(this, obj), (ExecutorService) AnonymousClass1XX.A02(1, AnonymousClass1Y3.ADs, this.A00));
    }

    public boolean A0A() {
        boolean z;
        synchronized (this.A06) {
            z = false;
            if (!this.A09.isEmpty()) {
                z = true;
            }
        }
        return z;
    }

    private C28461eq(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(8, r3);
    }

    public void A06(Object obj) {
        Preconditions.checkNotNull(obj);
        obj.toString();
        if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A05()) {
            A08(obj);
        } else {
            ((AnonymousClass0WP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8a, this.A00)).C25(obj);
        }
    }

    public void A07(Object obj) {
        Preconditions.checkNotNull(obj);
        obj.toString();
        if (((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, this.A00)).A05()) {
            A09(obj);
        } else {
            ((AnonymousClass0WP) AnonymousClass1XX.A02(0, AnonymousClass1Y3.A8a, this.A00)).ANq(obj);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0057, code lost:
        X.C005505z.A00(703416270);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005d, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(java.lang.Object r6) {
        /*
            r5 = this;
            com.google.common.base.Preconditions.checkNotNull(r6)
            r6.toString()
            java.lang.String r1 = "BusySignalHandler.endSignalInternal"
            r0 = -2136690258(0xffffffff80a4b1ae, float:-1.5124761E-38)
            X.C005505z.A03(r1, r0)
            java.lang.Object r2 = r5.A06     // Catch:{ all -> 0x0061 }
            monitor-enter(r2)     // Catch:{ all -> 0x0061 }
            java.util.WeakHashMap r0 = r5.A09     // Catch:{ all -> 0x005e }
            java.lang.Object r4 = r0.remove(r6)     // Catch:{ all -> 0x005e }
            X.1rr r4 = (X.C35811rr) r4     // Catch:{ all -> 0x005e }
            if (r4 != 0) goto L_0x0023
            monitor-exit(r2)     // Catch:{ all -> 0x005e }
            r0 = 26084149(0x18e0335, float:5.2167164E-38)
            X.C005505z.A00(r0)
            return
        L_0x0023:
            r3 = 5
            int r1 = X.AnonymousClass1Y3.Abm     // Catch:{ all -> 0x005e }
            X.0UN r0 = r5.A00     // Catch:{ all -> 0x005e }
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)     // Catch:{ all -> 0x005e }
            android.os.Handler r1 = (android.os.Handler) r1     // Catch:{ all -> 0x005e }
            X.1rq r0 = r4.A00     // Catch:{ all -> 0x005e }
            X.AnonymousClass00S.A02(r1, r0)     // Catch:{ all -> 0x005e }
            boolean r0 = r5.A04     // Catch:{ all -> 0x005e }
            if (r0 != 0) goto L_0x004a
            r0 = 1
            r5.A04 = r0     // Catch:{ all -> 0x005e }
            java.lang.Runnable r0 = r5.A01     // Catch:{ all -> 0x005e }
            if (r0 != 0) goto L_0x0045
            X.1rt r0 = new X.1rt     // Catch:{ all -> 0x005e }
            r0.<init>(r5)     // Catch:{ all -> 0x005e }
            r5.A01 = r0     // Catch:{ all -> 0x005e }
        L_0x0045:
            java.lang.Runnable r0 = r5.A01     // Catch:{ all -> 0x005e }
            A02(r5, r0)     // Catch:{ all -> 0x005e }
        L_0x004a:
            java.util.WeakHashMap r0 = r5.A09     // Catch:{ all -> 0x005e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x0056
            r0 = 0
            r5.A03(r0)     // Catch:{ all -> 0x005e }
        L_0x0056:
            monitor-exit(r2)     // Catch:{ all -> 0x005e }
            r0 = 703416270(0x29ed47ce, float:1.05373704E-13)
            X.C005505z.A00(r0)
            return
        L_0x005e:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x0061 }
        L_0x0061:
            r1 = move-exception
            r0 = -725681483(0xffffffffd4befab5, float:-6.5619996E12)
            X.C005505z.A00(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28461eq.A08(java.lang.Object):void");
    }

    public void A09(Object obj) {
        C35801rq r5;
        C35801rq r2;
        Preconditions.checkNotNull(obj);
        obj.toString();
        C005505z.A03("BusySignalHandler.startSignalInternal", 415674150);
        try {
            synchronized (this.A06) {
                boolean isEmpty = this.A09.isEmpty();
                C35811rr r52 = (C35811rr) this.A09.get(obj);
                if (r52 == null || (r2 = r52.A00) == null) {
                    r5 = new C35801rq(this, obj);
                } else {
                    AnonymousClass00S.A02((Handler) AnonymousClass1XX.A02(5, AnonymousClass1Y3.Abm, this.A00), r2);
                    r5 = r52.A00;
                }
                int i = AnonymousClass1Y3.Abm;
                AnonymousClass0UN r22 = this.A00;
                AnonymousClass00S.A05((Handler) AnonymousClass1XX.A02(5, i, r22), r5, ((C25121Yk) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BTs, r22)).A01(), 984465244);
                this.A09.put(obj, new C35811rr(r5));
                if (isEmpty) {
                    A03(true);
                }
            }
            C005505z.A00(795622778);
        } catch (Throwable th) {
            C005505z.A00(-139054739);
            throw th;
        }
    }
}
