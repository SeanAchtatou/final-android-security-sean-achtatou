package X;

import android.app.Activity;
import android.content.Context;
import android.net.NetworkInfo;
import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000;
import com.facebook.common.time.RealtimeSinceBootClock;
import com.facebook.device.resourcemonitor.DataUsageBytes;
import com.facebook.proxygen.LigerSamplePolicy;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.RegularImmutableMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.146  reason: invalid class name */
public final class AnonymousClass146 {
    private static volatile AnonymousClass146 A0R;
    public C31201jJ A00 = new C31201jJ(null);
    public C31201jJ A01 = new C31201jJ(null);
    public C06790c5 A02;
    public AnonymousClass0UN A03;
    public Runnable A04 = null;
    public String A05;
    public String A06 = null;
    public String A07;
    public boolean A08 = true;
    public boolean A09 = false;
    public boolean A0A = false;
    private int A0B = 0;
    private AnonymousClass14E A0C;
    private C31201jJ A0D = new C31201jJ(null);
    private C31201jJ A0E = new C31201jJ(null);
    private C31201jJ A0F = new C31201jJ(null);
    private C31201jJ A0G = new C31201jJ(null);
    private C31201jJ A0H = new C31201jJ(null);
    private C31201jJ A0I = new C31201jJ(null);
    private C31201jJ A0J = new C31201jJ(null);
    private C31201jJ A0K = new C31201jJ(null);
    private DataUsageBytes A0L;
    public final Map A0M = Collections.synchronizedMap(new HashMap());
    public final Map A0N = new HashMap();
    public final Set A0O = C25011Xz.A03();
    private final List A0P = new ArrayList();
    private volatile C32411li A0Q;

    public synchronized String A09() {
        return (String) this.A01.A00();
    }

    public void A0A(Activity activity) {
        Activity activity2;
        synchronized (this) {
            activity2 = activity;
            this.A0O.add(activity);
            Runnable runnable = this.A04;
            if (runnable != null) {
                ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, this.A03)).A03(runnable);
                this.A04 = null;
            }
            synchronized (this) {
                if (this.A08) {
                    this.A08 = false;
                    RealtimeSinceBootClock.A00.now();
                    C09390hE r3 = (C09390hE) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BP0, this.A03);
                    synchronized (r3) {
                        try {
                            boolean AbO = ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, r3.A02)).AbO(AnonymousClass1Y3.A2z, false);
                            r3.A03 = AbO;
                            if (AbO) {
                                r3.A01 = ((AnonymousClass069) AnonymousClass1XX.A02(4, AnonymousClass1Y3.BBa, r3.A02)).now();
                                r3.A06 = C09390hE.A01(r3);
                                if (r3.A06 != null) {
                                    r3.A06.A00("app_foregrounded");
                                }
                            }
                        } catch (Throwable th) {
                            th = th;
                            throw th;
                        }
                    }
                    if (A09() == null) {
                        A0D("foreground");
                    }
                    boolean AbO2 = ((AnonymousClass1YI) AnonymousClass1XX.A02(18, AnonymousClass1Y3.AcD, this.A03)).AbO(326, false);
                    if (AbO2) {
                        C06260bD r1 = (C06260bD) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AuX, this.A03);
                        synchronized (r1) {
                            try {
                                r1.A02.A01();
                            } catch (Throwable th2) {
                                th = th2;
                                throw th;
                            }
                        }
                    }
                    ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, this.A03)).A02(new C32181lL(this, AbO2));
                }
            }
        }
        if (!(activity instanceof AnonymousClass14D)) {
            A0C(activity2, null, null, null, null);
        }
        if (!((Context) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BCt, this.A03)).getString(2131821487).equalsIgnoreCase("messenger")) {
            if (this.A02 == null) {
                C06600bl BMm = ((C04460Ut) AnonymousClass1XX.A02(24, AnonymousClass1Y3.A2N, this.A03)).BMm();
                BMm.A02("chat_heads_status_change", new C406722m(this));
                this.A02 = BMm.A00();
            }
            this.A02.A00();
        }
    }

    public synchronized void A0B(Activity activity) {
        if (this.A0O.remove(activity) && this.A0O.isEmpty()) {
            long now = ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, this.A03)).now();
            if (this.A04 != null) {
                C010708t.A0K("NavigationLogger", "Previous sendToBackgroundDetector is still alive");
                ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, this.A03)).A03(this.A04);
                this.A04 = null;
            }
            if (0 == 0) {
                C35191qp r7 = new C35191qp(this);
                this.A04 = r7;
                ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, this.A03)).A04(r7, LigerSamplePolicy.CERT_DATA_SAMPLE_WEIGHT);
            }
            String A012 = ((AnonymousClass144) AnonymousClass1XX.A02(4, AnonymousClass1Y3.AUN, this.A03)).A01(activity);
            if (0 != 0) {
                C11670nb r5 = new C11670nb("session_end");
                r5.A0D("pigeon_reserved_keyword_uuid", A012);
                C11670nb.A00(r5, true).put("session_timeout", "1");
                r5.A02 = now;
                AnonymousClass13b r3 = (AnonymousClass13b) AnonymousClass1XX.A02(12, AnonymousClass1Y3.A7w, this.A03);
                synchronized (r3) {
                    AnonymousClass13b.A03(r3);
                    ((AnonymousClass2ZS) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AhD, r3.A04)).AMh(r5);
                    r3.A05(r5);
                }
            }
        }
        C06790c5 r0 = this.A02;
        if (r0 != null && r0.A02()) {
            this.A0A = false;
            this.A02.A01();
        }
    }

    public void A0C(Activity activity, String str, String str2, String str3, Map map) {
        Map map2 = map;
        Activity activity2 = activity;
        if (map == null) {
            map2 = RegularImmutableMap.A03;
        }
        if (activity != null && !(activity instanceof C11510nI)) {
            map2 = A02(activity, map2);
            activity2 = null;
        }
        A08(this, false, (C11510nI) activity2, activity2, str, str2, str3, map2, true);
    }

    public synchronized void A0E(String str, String str2, String str3, Map map) {
        ((C09390hE) AnonymousClass1XX.A02(10, AnonymousClass1Y3.BP0, this.A03)).A02(str, str2);
        ((DeprecatedAnalyticsLogger) AnonymousClass1XX.A02(14, AnonymousClass1Y3.AR0, this.A03)).A09(C1056253i.A00(str, str2, str3, map));
    }

    public static final AnonymousClass146 A00(AnonymousClass1XY r4) {
        if (A0R == null) {
            synchronized (AnonymousClass146.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0R, r4);
                if (A002 != null) {
                    try {
                        A0R = new AnonymousClass146(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0R;
    }

    public static USLEBaseShape0S0000000 A01(AnonymousClass146 r6, Integer num) {
        String str;
        String str2;
        USLEBaseShape0S0000000 uSLEBaseShape0S0000000 = new USLEBaseShape0S0000000(((AnonymousClass1ZE) AnonymousClass1XX.A02(21, AnonymousClass1Y3.Ap7, r6.A03)).A01("app_state"), 50);
        NetworkInfo A0E2 = ((C16330ws) AnonymousClass1XX.A02(3, AnonymousClass1Y3.Au3, r6.A03)).A05.A0E();
        String str3 = null;
        if (!uSLEBaseShape0S0000000.A0G()) {
            return null;
        }
        switch (num.intValue()) {
            case 1:
                str = "launch";
                break;
            case 2:
                str = "active";
                break;
            case 3:
                str = "resign";
                break;
            case 4:
                str = "foreground";
                break;
            case 5:
                str = "background";
                break;
            default:
                str = "init";
                break;
        }
        uSLEBaseShape0S0000000.A0D("state", str);
        uSLEBaseShape0S0000000.A0D("visitation_id", ((C07790eA) AnonymousClass1XX.A02(11, AnonymousClass1Y3.APk, r6.A03)).A05());
        if (A0E2 != null) {
            str2 = A0E2.getTypeName();
        } else {
            str2 = "null";
        }
        uSLEBaseShape0S0000000.A0D("connection", str2);
        JsonNode A032 = ((AnonymousClass14M) AnonymousClass1XX.A02(20, AnonymousClass1Y3.A86, r6.A03)).A03();
        if (A032 != null) {
            str3 = A032.toString();
        }
        uSLEBaseShape0S0000000.A0D("nav_attribution_id", str3);
        return uSLEBaseShape0S0000000;
    }

    public static ImmutableMap A02(Object obj, Map map) {
        Object activeContentFragment;
        while (true) {
            boolean z = obj instanceof AnonymousClass14G;
            if (!z && !(obj instanceof AnonymousClass14H)) {
                break;
            }
            if (obj instanceof AnonymousClass2HR) {
                activeContentFragment = ((AnonymousClass2HR) obj).getTopActiveContentFragment();
            } else if (z) {
                activeContentFragment = ((AnonymousClass14G) obj).getActiveContentFragment();
            } else {
                activeContentFragment = ((AnonymousClass14H) obj).getActiveContentFragment();
            }
            if (activeContentFragment == null) {
                break;
            }
            obj = activeContentFragment;
        }
        if (obj == null) {
            return RegularImmutableMap.A03;
        }
        ImmutableMap.Builder builder = ImmutableMap.builder();
        if (map == null) {
            map = RegularImmutableMap.A03;
        }
        builder.putAll(map);
        builder.put("dest_module_class", AnonymousClass07V.A02(obj));
        return builder.build();
    }

    public static String A03(Context context) {
        String str;
        if (context instanceof C11510nI) {
            str = ((C11510nI) context).Act();
        } else {
            str = null;
        }
        if (str == null) {
            return "unknown";
        }
        return str;
    }

    public static void A04(AnonymousClass146 r3, USLEBaseShape0S0000000 uSLEBaseShape0S0000000) {
        C06260bD r1 = (C06260bD) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AuX, r3.A03);
        synchronized (r1) {
            r1.A02.A00();
        }
        if (uSLEBaseShape0S0000000 != null) {
            uSLEBaseShape0S0000000.A06();
        }
    }

    public static void A05(AnonymousClass146 r3, USLEBaseShape0S0000000 uSLEBaseShape0S0000000) {
        C06260bD r1 = (C06260bD) AnonymousClass1XX.A02(16, AnonymousClass1Y3.AuX, r3.A03);
        synchronized (r1) {
            r1.A02.A01();
        }
        if (uSLEBaseShape0S0000000 != null) {
            uSLEBaseShape0S0000000.A06();
        }
        ((AnonymousClass06B) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AgK, r3.A03)).now();
    }

    public static void A06(AnonymousClass146 r10, String str, Map map) {
        String str2;
        String str3;
        AnonymousClass146 r4 = r10;
        AnonymousClass14J r2 = (AnonymousClass14J) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Asq, r10.A03);
        synchronized (r2.A02) {
            try {
                str2 = str;
                if (!r2.A01.empty() && str != null && ((AnonymousClass15L) r2.A01.peek()).A01 != null && ((AnonymousClass15L) r2.A01.peek()).A01.equals(str)) {
                    r2.A01.pop();
                }
                if (!r2.A01.isEmpty()) {
                    r2.A01.peek();
                }
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        HashMap hashMap = new HashMap();
        AnonymousClass15L A012 = ((AnonymousClass14J) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Asq, r10.A03)).A01();
        if (A012 != null) {
            str3 = A012.A01;
            hashMap.put("dest_module_class", A012.A00);
            Map map2 = A012.A02;
            if (map2 != null) {
                hashMap.putAll(map2);
            }
        } else {
            str3 = null;
        }
        if (map != null) {
            hashMap.putAll(map);
        }
        int i = AnonymousClass1Y3.Asq;
        Stack A032 = ((AnonymousClass14J) AnonymousClass1XX.A02(7, i, r10.A03)).A03();
        A08(r4, false, null, null, str2, str3, null, hashMap, false);
        AnonymousClass14J r0 = (AnonymousClass14J) AnonymousClass1XX.A02(7, i, r4.A03);
        synchronized (r0.A02) {
            try {
                r0.A01 = A032;
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public static void A07(AnonymousClass146 r13, String str, boolean z, Map map) {
        String str2;
        AnonymousClass146 r5 = r13;
        AnonymousClass15L A012 = ((AnonymousClass14J) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Asq, r13.A03)).A01();
        String str3 = null;
        if (A012 != null) {
            str2 = A012.A01;
            str3 = A012.A00;
        } else {
            str2 = null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("is_modal", Boolean.valueOf(z));
        hashMap.put("source_module_class", str3);
        Map map2 = map;
        if (map != null) {
            hashMap.putAll(map2);
        }
        int i = AnonymousClass1Y3.Asq;
        Stack A032 = ((AnonymousClass14J) AnonymousClass1XX.A02(7, i, r13.A03)).A03();
        A08(r5, false, null, null, str2, str, null, hashMap, false);
        AnonymousClass14J r0 = (AnonymousClass14J) AnonymousClass1XX.A02(7, i, r5.A03);
        synchronized (r0.A02) {
            try {
                r0.A01 = A032;
            } catch (Throwable th) {
                while (true) {
                    th = th;
                }
            }
        }
        AnonymousClass14J r2 = (AnonymousClass14J) AnonymousClass1XX.A02(7, AnonymousClass1Y3.Asq, r5.A03);
        synchronized (r2.A02) {
            try {
                String str4 = null;
                if (!r2.A01.isEmpty()) {
                    r2.A01.peek();
                }
                Stack stack = r2.A01;
                if (map != null) {
                    str4 = (String) map2.get("dest_module_class");
                }
                stack.push(new AnonymousClass15L(str, str4, map2));
            } catch (Throwable th2) {
                while (true) {
                    th = th2;
                    break;
                }
            }
        }
        return;
        throw th;
    }

    public void A0D(String str) {
        if (str == null) {
            Object obj = this.A01.A01;
        }
        this.A01 = new C31201jJ(str);
    }

    public void A0F(String str, Map map) {
        if (((C25051Yd) AnonymousClass1XX.A02(22, AnonymousClass1Y3.AOJ, this.A03)).Aem(283832913759752L)) {
            ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, this.A03)).A02(new AnonymousClass21R(this, str, map));
        } else {
            A06(this, str, map);
        }
    }

    public void A0G(String str, boolean z, Map map) {
        if (((C25051Yd) AnonymousClass1XX.A02(22, AnonymousClass1Y3.AOJ, this.A03)).Aem(283832913759752L)) {
            ((AnonymousClass0ia) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A9n, this.A03)).A02(new AnonymousClass4PK(this, str, z, map));
        } else {
            A07(this, str, z, map);
        }
    }

    private AnonymousClass146(AnonymousClass1XY r4) {
        this.A03 = new AnonymousClass0UN(25, r4);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0376, code lost:
        if (r5 == false) goto L_0x0378;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:417:0x0b96, code lost:
        if (r2 == false) goto L_0x0b9c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:587:0x0f3f, code lost:
        r1 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x03c3 A[Catch:{ all -> 0x0f42 }] */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x06ab A[Catch:{ all -> 0x0f42 }] */
    /* JADX WARNING: Removed duplicated region for block: B:348:0x0a02 A[Catch:{ all -> 0x0f3f }] */
    /* JADX WARNING: Removed duplicated region for block: B:362:0x0a5e A[Catch:{ all -> 0x0f3f }] */
    /* JADX WARNING: Removed duplicated region for block: B:363:0x0a75 A[Catch:{ all -> 0x0f3f }] */
    /* JADX WARNING: Removed duplicated region for block: B:364:0x0a91 A[Catch:{ all -> 0x0f3f }] */
    /* JADX WARNING: Removed duplicated region for block: B:366:0x0a9a A[Catch:{ all -> 0x0f3f }] */
    /* JADX WARNING: Removed duplicated region for block: B:450:0x0c34 A[Catch:{ all -> 0x0f3c, all -> 0x0f48 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A08(X.AnonymousClass146 r41, boolean r42, X.C11510nI r43, android.content.Context r44, java.lang.String r45, java.lang.String r46, java.lang.String r47, java.util.Map r48, boolean r49) {
        /*
            r1 = r46
            r17 = r45
            r0 = r41
            monitor-enter(r0)
            X.14E r2 = r0.A0C     // Catch:{ all -> 0x0f48 }
            r26 = r2
            X.14E r25 = new X.14E     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A00     // Catch:{ all -> 0x0f48 }
            java.lang.Object r6 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.Long r6 = (java.lang.Long) r6     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A01     // Catch:{ all -> 0x0f48 }
            java.lang.Object r5 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0F     // Catch:{ all -> 0x0f48 }
            java.lang.Object r4 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ all -> 0x0f48 }
            r18 = r43
            if (r46 != 0) goto L_0x002c
            if (r43 == 0) goto L_0x002c
            goto L_0x002e
        L_0x002c:
            r3 = r1
            goto L_0x0032
        L_0x002e:
            java.lang.String r3 = r18.Act()     // Catch:{ all -> 0x0f48 }
        L_0x0032:
            r2 = r25
            r2.<init>(r6, r5, r4, r3)     // Catch:{ all -> 0x0f48 }
            r3 = r48
            java.util.HashMap r16 = new java.util.HashMap     // Catch:{ all -> 0x0f48 }
            r16.<init>()     // Catch:{ all -> 0x0f48 }
            java.util.Map r2 = r0.A0N     // Catch:{ all -> 0x0f48 }
            r4 = r16
            r4.putAll(r2)     // Catch:{ all -> 0x0f48 }
            if (r48 == 0) goto L_0x004a
            r4.putAll(r3)     // Catch:{ all -> 0x0f48 }
        L_0x004a:
            int r4 = X.AnonymousClass1Y3.AgK     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)     // Catch:{ all -> 0x0f48 }
            X.06B r2 = (X.AnonymousClass06B) r2     // Catch:{ all -> 0x0f48 }
            long r21 = r2.now()     // Catch:{ all -> 0x0f48 }
            if (r43 == 0) goto L_0x0071
            if (r46 == 0) goto L_0x0065
            java.lang.String r2 = "unknown"
            boolean r2 = r2.equals(r1)     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x0069
        L_0x0065:
            java.lang.String r1 = r18.Act()     // Catch:{ all -> 0x0f48 }
        L_0x0069:
            r2 = r18
            r3 = r16
            com.google.common.collect.ImmutableMap r16 = A02(r2, r3)     // Catch:{ all -> 0x0f48 }
        L_0x0071:
            int r4 = X.AnonymousClass1Y3.Asq     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 7
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)     // Catch:{ all -> 0x0f48 }
            X.14J r2 = (X.AnonymousClass14J) r2     // Catch:{ all -> 0x0f48 }
            X.15L r3 = r2.A01()     // Catch:{ all -> 0x0f48 }
            if (r45 != 0) goto L_0x0085
            if (r3 == 0) goto L_0x0085
            goto L_0x0088
        L_0x0085:
            r20 = 0
            goto L_0x0090
        L_0x0088:
            java.lang.String r2 = r3.A01     // Catch:{ all -> 0x0f48 }
            r17 = r2
            java.lang.String r2 = r3.A00     // Catch:{ all -> 0x0f48 }
            r20 = r2
        L_0x0090:
            X.1jJ r3 = r0.A00     // Catch:{ all -> 0x0f48 }
            java.lang.Object r19 = r3.A00()     // Catch:{ all -> 0x0f48 }
            r2 = r19
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x0f48 }
            r19 = r2
            java.lang.Object r2 = r3.A00()     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x00c7
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ all -> 0x0f48 }
            r4.<init>()     // Catch:{ all -> 0x0f48 }
            if (r16 == 0) goto L_0x00ae
            r3 = r16
            r4.putAll(r3)     // Catch:{ all -> 0x0f48 }
        L_0x00ae:
            X.1jJ r2 = r0.A00     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = "bookmark_id"
            r4.put(r2, r3)     // Catch:{ all -> 0x0f48 }
            X.1jJ r3 = new X.1jJ     // Catch:{ all -> 0x0f48 }
            r2 = 0
            r3.<init>(r2)     // Catch:{ all -> 0x0f48 }
            r0.A00 = r3     // Catch:{ all -> 0x0f48 }
            r16 = r4
        L_0x00c7:
            r13 = 11
            if (r19 == 0) goto L_0x0226
            int r3 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r28 = X.AnonymousClass1XX.A02(r13, r3, r2)     // Catch:{ all -> 0x0f48 }
            r2 = r28
            X.0eA r2 = (X.C07790eA) r2     // Catch:{ all -> 0x0f48 }
            r28 = r2
            r2 = r19
            java.lang.String r27 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0F     // Catch:{ all -> 0x0f48 }
            java.lang.Object r24 = r2.A00()     // Catch:{ all -> 0x0f48 }
            r2 = r24
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0f48 }
            r24 = r2
            X.1jJ r2 = r0.A0G     // Catch:{ all -> 0x0f48 }
            java.lang.Object r12 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.Boolean r12 = (java.lang.Boolean) r12     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A01     // Catch:{ all -> 0x0f48 }
            java.lang.Object r11 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0K     // Catch:{ all -> 0x0f48 }
            java.lang.Object r10 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0D     // Catch:{ all -> 0x0f48 }
            java.lang.Object r9 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0E     // Catch:{ all -> 0x0f48 }
            java.lang.Object r8 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0I     // Catch:{ all -> 0x0f48 }
            java.lang.Object r7 = r2.A00()     // Catch:{ all -> 0x0f48 }
            X.4mI r7 = (X.C97964mI) r7     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0J     // Catch:{ all -> 0x0f48 }
            java.lang.Object r6 = r2.A00()     // Catch:{ all -> 0x0f48 }
            X.46i r6 = (X.C860146i) r6     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A0H     // Catch:{ all -> 0x0f48 }
            java.lang.Object r5 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.Long r5 = (java.lang.Long) r5     // Catch:{ all -> 0x0f48 }
            java.util.Map r14 = r0.A0M     // Catch:{ all -> 0x0f48 }
            monitor-enter(r28)     // Catch:{ all -> 0x0f48 }
            r2 = r28
            java.util.LinkedList r2 = r2.A08     // Catch:{ all -> 0x0f45 }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0f45 }
            if (r2 != 0) goto L_0x01e6
            r2 = r28
            java.util.LinkedList r2 = r2.A08     // Catch:{ all -> 0x0f45 }
            java.lang.Object r4 = r2.getFirst()     // Catch:{ all -> 0x0f45 }
            X.0pA r4 = (X.C12330pA) r4     // Catch:{ all -> 0x0f45 }
            X.4mH r2 = r4.A03     // Catch:{ all -> 0x0f45 }
            if (r2 != 0) goto L_0x017d
            r2 = r27
            r4.A09 = r2     // Catch:{ all -> 0x0f45 }
            r4.A0C = r10     // Catch:{ all -> 0x0f45 }
            r2 = r24
            r4.A08 = r2     // Catch:{ all -> 0x0f45 }
            r4.A04 = r9     // Catch:{ all -> 0x0f45 }
            r4.A07 = r8     // Catch:{ all -> 0x0f45 }
            r4.A01 = r7     // Catch:{ all -> 0x0f45 }
            r4.A02 = r6     // Catch:{ all -> 0x0f45 }
            r4.A05 = r5     // Catch:{ all -> 0x0f45 }
            java.lang.String r2 = r4.A0A     // Catch:{ all -> 0x0f45 }
            if (r2 != 0) goto L_0x0160
            r4.A0A = r11     // Catch:{ all -> 0x0f45 }
        L_0x0160:
            if (r12 == 0) goto L_0x017a
            boolean r2 = r12.booleanValue()     // Catch:{ all -> 0x0f45 }
            if (r2 == 0) goto L_0x017a
            int r2 = r4.A00     // Catch:{ all -> 0x0f45 }
            int r2 = r2 + 1
            r4.A00 = r2     // Catch:{ all -> 0x0f45 }
            X.06A r2 = X.AnonymousClass06A.A00     // Catch:{ all -> 0x0f45 }
            long r2 = r2.now()     // Catch:{ all -> 0x0f45 }
            java.lang.String r2 = X.C12350pC.A04(r2)     // Catch:{ all -> 0x0f45 }
            r4.A0B = r2     // Catch:{ all -> 0x0f45 }
        L_0x017a:
            r4.A0D = r14     // Catch:{ all -> 0x0f45 }
            goto L_0x01e6
        L_0x017d:
            r3 = 0
            if (r2 != 0) goto L_0x0183
        L_0x0180:
            if (r3 == 0) goto L_0x01e6
            goto L_0x01ab
        L_0x0183:
            if (r2 == 0) goto L_0x01a8
            java.lang.String r15 = r2.A07     // Catch:{ all -> 0x0f45 }
            if (r15 != 0) goto L_0x01a6
            java.lang.Class<X.Ev6> r23 = X.C30373Ev6.class
            java.lang.Class r15 = r2.A0D     // Catch:{ all -> 0x0f45 }
            r29 = r23
            r30 = r15
            boolean r15 = r29.isAssignableFrom(r30)     // Catch:{ all -> 0x0f45 }
            if (r15 != 0) goto L_0x01a6
            java.lang.Integer r2 = r2.A0E     // Catch:{ all -> 0x0f45 }
            if (r2 == 0) goto L_0x01a4
            java.util.Map r15 = r4.A0I     // Catch:{ all -> 0x0f45 }
            java.lang.Object r2 = r15.get(r2)     // Catch:{ all -> 0x0f45 }
            X.4mH r2 = (X.C97954mH) r2     // Catch:{ all -> 0x0f45 }
            goto L_0x0183
        L_0x01a4:
            r2 = r3
            goto L_0x0183
        L_0x01a6:
            r3 = r2
            goto L_0x0180
        L_0x01a8:
            X.4mH r3 = r4.A03     // Catch:{ all -> 0x0f45 }
            goto L_0x0180
        L_0x01ab:
            r2 = r27
            r3.A07 = r2     // Catch:{ all -> 0x0f45 }
            r3.A0A = r10     // Catch:{ all -> 0x0f45 }
            r2 = r24
            r3.A06 = r2     // Catch:{ all -> 0x0f45 }
            r3.A03 = r9     // Catch:{ all -> 0x0f45 }
            r3.A05 = r8     // Catch:{ all -> 0x0f45 }
            r3.A01 = r7     // Catch:{ all -> 0x0f45 }
            r3.A02 = r6     // Catch:{ all -> 0x0f45 }
            r3.A04 = r5     // Catch:{ all -> 0x0f45 }
            java.lang.String r2 = r3.A08     // Catch:{ all -> 0x0f45 }
            if (r2 != 0) goto L_0x01c5
            r3.A08 = r11     // Catch:{ all -> 0x0f45 }
        L_0x01c5:
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x0f45 }
            r2.<init>(r14)     // Catch:{ all -> 0x0f45 }
            r3.A0B = r2     // Catch:{ all -> 0x0f45 }
            if (r12 == 0) goto L_0x01e6
            boolean r2 = r12.booleanValue()     // Catch:{ all -> 0x0f45 }
            if (r2 == 0) goto L_0x01e6
            int r2 = r3.A00     // Catch:{ all -> 0x0f45 }
            int r2 = r2 + 1
            r3.A00 = r2     // Catch:{ all -> 0x0f45 }
            X.06A r2 = X.AnonymousClass06A.A00     // Catch:{ all -> 0x0f45 }
            long r4 = r2.now()     // Catch:{ all -> 0x0f45 }
            java.lang.String r2 = X.C12350pC.A04(r4)     // Catch:{ all -> 0x0f45 }
            r3.A09 = r2     // Catch:{ all -> 0x0f45 }
        L_0x01e6:
            monitor-exit(r28)     // Catch:{ all -> 0x0f48 }
            int r3 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r13, r3, r2)     // Catch:{ all -> 0x0f48 }
            X.0eA r2 = (X.C07790eA) r2     // Catch:{ all -> 0x0f48 }
            r2.A09()     // Catch:{ all -> 0x0f48 }
            X.1jJ r3 = r0.A0G     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = r3.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x0207
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x0207
            r2 = 0
            r3.A01 = r2     // Catch:{ all -> 0x0f48 }
        L_0x0207:
            X.1jJ r3 = r0.A0F     // Catch:{ all -> 0x0f48 }
            r2 = 0
            r3.A01 = r2     // Catch:{ all -> 0x0f48 }
            X.1jJ r3 = r0.A0D     // Catch:{ all -> 0x0f48 }
            r3.A01 = r2     // Catch:{ all -> 0x0f48 }
            X.1jJ r3 = r0.A0E     // Catch:{ all -> 0x0f48 }
            r3.A01 = r2     // Catch:{ all -> 0x0f48 }
            X.1jJ r3 = r0.A0I     // Catch:{ all -> 0x0f48 }
            r3.A01 = r2     // Catch:{ all -> 0x0f48 }
            X.1jJ r3 = r0.A0J     // Catch:{ all -> 0x0f48 }
            r3.A01 = r2     // Catch:{ all -> 0x0f48 }
            X.1jJ r3 = r0.A0H     // Catch:{ all -> 0x0f48 }
            r3.A01 = r2     // Catch:{ all -> 0x0f48 }
            java.util.Map r2 = r0.A0M     // Catch:{ all -> 0x0f48 }
            r2.clear()     // Catch:{ all -> 0x0f48 }
            goto L_0x0237
        L_0x0226:
            X.1jJ r2 = r0.A01     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x0237
            int r3 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r13, r3, r2)     // Catch:{ all -> 0x0f48 }
            X.0eA r2 = (X.C07790eA) r2     // Catch:{ all -> 0x0f48 }
            r2.A09()     // Catch:{ all -> 0x0f48 }
        L_0x0237:
            int r4 = X.AnonymousClass1Y3.A86     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 20
            java.lang.Object r24 = X.AnonymousClass1XX.A02(r2, r4, r3)     // Catch:{ all -> 0x0f48 }
            r2 = r24
            X.14M r2 = (X.AnonymousClass14M) r2     // Catch:{ all -> 0x0f48 }
            r24 = r2
            X.1jJ r2 = r0.A01     // Catch:{ all -> 0x0f48 }
            java.lang.Object r3 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0f48 }
            r6 = r24
            r5 = r19
            monitor-enter(r24)     // Catch:{ all -> 0x0f48 }
            r10 = 4748854339(0x11b0dc443, double:2.346245786E-314)
            r7 = 0
            r2 = 0
            if (r19 == 0) goto L_0x0357
            int r6 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            r4 = r24
            X.0UN r4 = r4.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r7, r6, r4)     // Catch:{ all -> 0x0f42 }
            X.0eA r4 = (X.C07790eA) r4     // Catch:{ all -> 0x0f42 }
            java.lang.String r12 = r5.toString()     // Catch:{ all -> 0x0f42 }
            X.0pC r4 = r4.A04(r12)     // Catch:{ all -> 0x0f42 }
            java.lang.Long r8 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0f42 }
            boolean r29 = r5.equals(r8)     // Catch:{ all -> 0x0f42 }
            if (r29 == 0) goto L_0x0310
            r5 = r24
            java.util.LinkedList r5 = r5.A07     // Catch:{ all -> 0x0f42 }
            r5.clear()     // Catch:{ all -> 0x0f42 }
            if (r4 == 0) goto L_0x02ab
            r5 = r19
            long r5 = r5.longValue()     // Catch:{ all -> 0x0f42 }
            java.lang.String r6 = java.lang.Long.toString(r5)     // Catch:{ all -> 0x0f42 }
            X.2wE r6 = X.AnonymousClass2wE.A00(r4, r6, r7)     // Catch:{ all -> 0x0f42 }
            r5 = r24
            java.util.LinkedList r5 = r5.A07     // Catch:{ all -> 0x0f42 }
            r5.add(r6)     // Catch:{ all -> 0x0f42 }
            r5 = r24
            r5.A01 = r6     // Catch:{ all -> 0x0f42 }
            X.43N r7 = new X.43N     // Catch:{ all -> 0x0f42 }
            java.lang.String r6 = r4.A0G()     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r4.A0C()     // Catch:{ all -> 0x0f42 }
            r7.<init>(r12, r6, r5)     // Catch:{ all -> 0x0f42 }
            goto L_0x02c3
        L_0x02ab:
            r5 = r24
            X.2wE r7 = r5.A01     // Catch:{ all -> 0x0f42 }
            if (r7 == 0) goto L_0x02c9
            java.util.LinkedList r5 = r5.A07     // Catch:{ all -> 0x0f42 }
            r5.add(r7)     // Catch:{ all -> 0x0f42 }
            X.43N r7 = new X.43N     // Catch:{ all -> 0x0f42 }
            r5 = r24
            X.2wE r5 = r5.A01     // Catch:{ all -> 0x0f42 }
            java.lang.String r6 = r5.A08     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r5.A06     // Catch:{ all -> 0x0f42 }
            r7.<init>(r12, r6, r5)     // Catch:{ all -> 0x0f42 }
        L_0x02c3:
            r5 = r24
            r5.A00 = r7     // Catch:{ all -> 0x0f42 }
            goto L_0x0353
        L_0x02c9:
            X.2wE r27 = new X.2wE     // Catch:{ all -> 0x0f42 }
            java.lang.String r28 = java.lang.Long.toString(r10)     // Catch:{ all -> 0x0f42 }
            r23 = 0
            X.06A r5 = X.AnonymousClass06A.A00     // Catch:{ all -> 0x0f42 }
            long r7 = r5.now()     // Catch:{ all -> 0x0f42 }
            java.lang.String r31 = X.C12350pC.A04(r7)     // Catch:{ all -> 0x0f42 }
            r33 = 0
            r34 = 0
            r35 = 0
            r36 = 0
            java.lang.String r41 = "Feed"
            r5 = r27
            r38 = r2
            r39 = 0
            r40 = 0
            r29 = r2
            r30 = 0
            r32 = r3
            r37 = r2
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41)     // Catch:{ all -> 0x0f42 }
            r2 = r24
            java.util.LinkedList r2 = r2.A07     // Catch:{ all -> 0x0f42 }
            r2.add(r5)     // Catch:{ all -> 0x0f42 }
            r2 = r24
            r2.A01 = r5     // Catch:{ all -> 0x0f42 }
            X.43N r7 = new X.43N     // Catch:{ all -> 0x0f42 }
            r5 = r23
            r7.<init>(r12, r5, r5)     // Catch:{ all -> 0x0f42 }
            r2 = r24
            r2.A00 = r7     // Catch:{ all -> 0x0f42 }
            goto L_0x03fd
        L_0x0310:
            if (r4 != 0) goto L_0x0608
            r9 = 3
            int r8 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x0f42 }
            r5 = r24
            X.0UN r5 = r5.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r9, r8, r5)     // Catch:{ all -> 0x0f42 }
            X.1ZE r8 = (X.AnonymousClass1ZE) r8     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = "fb4a_attribution_id_null_hierarchical_session"
            X.0bW r9 = r8.A01(r5)     // Catch:{ all -> 0x0f42 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r8 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x0f42 }
            r5 = 149(0x95, float:2.09E-43)
            r8.<init>(r9, r5)     // Catch:{ all -> 0x0f42 }
            boolean r5 = r8.A0G()     // Catch:{ all -> 0x0f42 }
            if (r5 == 0) goto L_0x0353
            int r9 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            r5 = r24
            X.0UN r5 = r5.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r7, r9, r5)     // Catch:{ all -> 0x0f42 }
            X.0eA r5 = (X.C07790eA) r5     // Catch:{ all -> 0x0f42 }
            java.util.List r5 = r5.A07()     // Catch:{ all -> 0x0f42 }
            java.lang.String r7 = r5.toString()     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = "session_path"
            r8.A0D(r5, r7)     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = "surface_link_id"
            r8.A0D(r5, r12)     // Catch:{ all -> 0x0f42 }
            r8.A06()     // Catch:{ all -> 0x0f42 }
        L_0x0353:
            r23 = r2
            goto L_0x03fd
        L_0x0357:
            r23 = r2
            java.util.LinkedList r4 = r6.A07     // Catch:{ all -> 0x0f42 }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x0f42 }
            if (r4 != 0) goto L_0x03d9
            java.util.LinkedList r4 = r6.A07     // Catch:{ all -> 0x0f42 }
            java.lang.Object r4 = r4.get(r7)     // Catch:{ all -> 0x0f42 }
            X.2wE r4 = (X.AnonymousClass2wE) r4     // Catch:{ all -> 0x0f42 }
            if (r4 == 0) goto L_0x0378
            java.lang.String r5 = r4.A0A     // Catch:{ all -> 0x0f42 }
            java.lang.String r4 = java.lang.Long.toString(r10)     // Catch:{ all -> 0x0f42 }
            boolean r5 = r5.equals(r4)     // Catch:{ all -> 0x0f42 }
            r4 = 1
            if (r5 != 0) goto L_0x0379
        L_0x0378:
            r4 = 0
        L_0x0379:
            if (r4 != 0) goto L_0x03d9
            int r5 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            X.0UN r4 = r6.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r7, r5, r4)     // Catch:{ all -> 0x0f42 }
            X.0eA r4 = (X.C07790eA) r4     // Catch:{ all -> 0x0f42 }
            java.util.List r4 = r4.A08()     // Catch:{ all -> 0x0f42 }
            boolean r5 = r4.isEmpty()     // Catch:{ all -> 0x0f42 }
            if (r5 != 0) goto L_0x0395
            java.lang.Object r2 = r4.get(r7)     // Catch:{ all -> 0x0f42 }
            X.0pC r2 = (X.C12350pC) r2     // Catch:{ all -> 0x0f42 }
        L_0x0395:
            if (r2 == 0) goto L_0x03be
            java.lang.String r4 = r2.A0D()     // Catch:{ all -> 0x0f42 }
            if (r4 == 0) goto L_0x03ab
            java.lang.String r5 = r2.A0D()     // Catch:{ all -> 0x0f42 }
            java.lang.String r4 = java.lang.Long.toString(r10)     // Catch:{ all -> 0x0f42 }
            boolean r4 = r5.equals(r4)     // Catch:{ all -> 0x0f42 }
            if (r4 != 0) goto L_0x03c0
        L_0x03ab:
            java.lang.String r4 = r2.A0E()     // Catch:{ all -> 0x0f42 }
            if (r4 == 0) goto L_0x03be
            java.lang.String r5 = r2.A0E()     // Catch:{ all -> 0x0f42 }
            java.lang.String r4 = "native_newsfeed"
            boolean r4 = r5.equals(r4)     // Catch:{ all -> 0x0f42 }
            if (r4 == 0) goto L_0x03be
            goto L_0x03c0
        L_0x03be:
            r4 = 0
            goto L_0x03c1
        L_0x03c0:
            r4 = 1
        L_0x03c1:
            if (r4 == 0) goto L_0x03d9
            java.lang.String r5 = java.lang.Long.toString(r10)     // Catch:{ all -> 0x0f42 }
            r4 = 0
            X.2wE r4 = X.AnonymousClass2wE.A00(r2, r5, r4)     // Catch:{ all -> 0x0f42 }
            java.util.LinkedList r2 = r6.A07     // Catch:{ all -> 0x0f42 }
            r2.clear()     // Catch:{ all -> 0x0f42 }
            java.util.LinkedList r2 = r6.A07     // Catch:{ all -> 0x0f42 }
            r2.add(r4)     // Catch:{ all -> 0x0f42 }
            r6.A01 = r4     // Catch:{ all -> 0x0f42 }
            goto L_0x03de
        L_0x03d9:
            if (r3 == 0) goto L_0x03de
            X.AnonymousClass14M.A02(r6, r3)     // Catch:{ all -> 0x0f42 }
        L_0x03de:
            r4 = r23
            goto L_0x03fd
        L_0x03e1:
            java.lang.String r6 = "tap_bookmark"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x0463
            r6 = 986244814899307(0x380fc03da886b, double:4.87269681430807E-309)
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0f42 }
            r7 = r19
            boolean r6 = r7.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x0409
            X.AnonymousClass14M.A01(r9)     // Catch:{ all -> 0x0f42 }
        L_0x03fd:
            r2 = r24
            java.util.LinkedList r2 = r2.A07     // Catch:{ all -> 0x0f42 }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0f42 }
            if (r2 == 0) goto L_0x07ff
            goto L_0x06b3
        L_0x0409:
            X.43N r8 = r9.A00     // Catch:{ all -> 0x0f42 }
            if (r8 == 0) goto L_0x043c
            java.lang.String r7 = r8.A02     // Catch:{ all -> 0x0f42 }
            if (r7 == 0) goto L_0x0420
            r9 = 281710865595635(0x10036ec12bcf3, double:1.39183660751004E-309)
            java.lang.String r6 = java.lang.Long.toString(r9)     // Catch:{ all -> 0x0f42 }
            boolean r6 = r7.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 != 0) goto L_0x042c
        L_0x0420:
            java.lang.String r7 = r8.A00     // Catch:{ all -> 0x0f42 }
            if (r7 == 0) goto L_0x043c
            java.lang.String r6 = "FolderBookmark"
            boolean r6 = r7.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x043c
        L_0x042c:
            r6 = r24
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            r7 = r27
            r6.addFirst(r7)     // Catch:{ all -> 0x0f42 }
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            r6.<init>(r12, r5, r2)     // Catch:{ all -> 0x0f42 }
            goto L_0x063e
        L_0x043c:
            r6 = 1
            int r5 = X.AnonymousClass1Y3.Amr     // Catch:{ all -> 0x0f42 }
            r2 = r24
            X.0UN r2 = r2.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r6, r5, r2)     // Catch:{ all -> 0x0f42 }
            X.09P r6 = (X.AnonymousClass09P) r6     // Catch:{ all -> 0x0f42 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0f42 }
            r5.<init>()     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "Invalid current surface link id: "
            r5.append(r2)     // Catch:{ all -> 0x0f42 }
            r8 = r19
            r5.append(r8)     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "attribution_id_manager_bookmark_select_from_outside_more_menu"
            r6.CGS(r2, r5)     // Catch:{ all -> 0x0f42 }
            goto L_0x06a9
        L_0x0463:
            if (r3 == 0) goto L_0x048e
            r8 = 391724414624676(0x16445729563a4, double:1.93537575903325E-309)
            java.lang.Long r6 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x0f42 }
            r8 = r19
            boolean r6 = r8.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x048e
            java.lang.String r6 = "tap_search_bar"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x048e
            r6 = r24
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            r7 = r27
            r6.addFirst(r7)     // Catch:{ all -> 0x0f42 }
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            r6.<init>(r12, r5, r2)     // Catch:{ all -> 0x0f42 }
            goto L_0x063e
        L_0x048e:
            if (r3 == 0) goto L_0x0695
            java.lang.String r6 = "tap_back_button"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 != 0) goto L_0x04a0
            java.lang.String r6 = "tap_top_left_nav"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x0695
        L_0x04a0:
            if (r29 == 0) goto L_0x04a4
            goto L_0x05e8
        L_0x04a4:
            r6 = r24
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            java.util.Iterator r8 = r6.iterator()     // Catch:{ all -> 0x0f42 }
        L_0x04ac:
            boolean r6 = r8.hasNext()     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x04be
            java.lang.Object r6 = r8.next()     // Catch:{ all -> 0x0f42 }
            X.2wE r6 = (X.AnonymousClass2wE) r6     // Catch:{ all -> 0x0f42 }
            java.lang.String r6 = r6.A0A     // Catch:{ all -> 0x0f42 }
            if (r6 != 0) goto L_0x04ac
            r6 = 1
            goto L_0x04bf
        L_0x04be:
            r6 = 0
        L_0x04bf:
            r8 = 3
            if (r6 == 0) goto L_0x0509
            int r9 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x0f42 }
            r6 = r24
            X.0UN r6 = r6.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r8, r9, r6)     // Catch:{ all -> 0x0f42 }
            X.1ZE r9 = (X.AnonymousClass1ZE) r9     // Catch:{ all -> 0x0f42 }
            java.lang.String r6 = "fb4a_attribution_id_back_contains_placeholder"
            X.0bW r10 = r9.A01(r6)     // Catch:{ all -> 0x0f42 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r6 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x0f42 }
            r9 = 147(0x93, float:2.06E-43)
            r6.<init>(r10, r9)     // Catch:{ all -> 0x0f42 }
            boolean r9 = r6.A0G()     // Catch:{ all -> 0x0f42 }
            if (r9 == 0) goto L_0x0509
            r2 = r24
            java.lang.String r5 = r2.A03     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "placeholder_session_path"
            r6.A0D(r2, r5)     // Catch:{ all -> 0x0f42 }
            int r5 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            r2 = r24
            X.0UN r2 = r2.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r5, r2)     // Catch:{ all -> 0x0f42 }
            X.0eA r2 = (X.C07790eA) r2     // Catch:{ all -> 0x0f42 }
            java.util.List r2 = r2.A07()     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "session_path"
            r6.A0D(r2, r5)     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "surface_link_id"
            r6.A0D(r2, r12)     // Catch:{ all -> 0x0f42 }
            goto L_0x0558
        L_0x0509:
            int r9 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            r6 = r24
            X.0UN r6 = r6.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r7, r9, r6)     // Catch:{ all -> 0x0f42 }
            X.0eA r6 = (X.C07790eA) r6     // Catch:{ all -> 0x0f42 }
            X.0pC r9 = r6.A04(r12)     // Catch:{ all -> 0x0f42 }
            if (r9 != 0) goto L_0x055d
            int r5 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x0f42 }
            r2 = r24
            X.0UN r2 = r2.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r8, r5, r2)     // Catch:{ all -> 0x0f42 }
            X.1ZE r5 = (X.AnonymousClass1ZE) r5     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "fb4a_attribution_id_null_hierarchical_session_on_back"
            X.0bW r5 = r5.A01(r2)     // Catch:{ all -> 0x0f42 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r6 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x0f42 }
            r2 = 150(0x96, float:2.1E-43)
            r6.<init>(r5, r2)     // Catch:{ all -> 0x0f42 }
            boolean r2 = r6.A0G()     // Catch:{ all -> 0x0f42 }
            if (r2 == 0) goto L_0x03fd
            int r5 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            r2 = r24
            X.0UN r2 = r2.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r7, r5, r2)     // Catch:{ all -> 0x0f42 }
            X.0eA r2 = (X.C07790eA) r2     // Catch:{ all -> 0x0f42 }
            java.util.List r2 = r2.A07()     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "session_path"
            r6.A0D(r2, r5)     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "surface_link_id"
            r6.A0D(r2, r12)     // Catch:{ all -> 0x0f42 }
        L_0x0558:
            r6.A06()     // Catch:{ all -> 0x0f42 }
            goto L_0x03fd
        L_0x055d:
            r6 = r24
            java.util.LinkedList r13 = r6.A07     // Catch:{ all -> 0x0f42 }
            java.lang.String r15 = r9.A0D()     // Catch:{ all -> 0x0f42 }
            java.lang.String r14 = r9.A0G()     // Catch:{ all -> 0x0f42 }
            java.lang.String r28 = r9.A0C()     // Catch:{ all -> 0x0f42 }
            java.util.Map r11 = r6.A04     // Catch:{ all -> 0x0f42 }
            r10 = 0
        L_0x0570:
            int r6 = r13.size()     // Catch:{ all -> 0x0f42 }
            if (r10 >= r6) goto L_0x05e2
            java.lang.Object r9 = r13.get(r10)     // Catch:{ all -> 0x0f42 }
            X.2wE r9 = (X.AnonymousClass2wE) r9     // Catch:{ all -> 0x0f42 }
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            r30 = r6
            r31 = r15
            r32 = r14
            r33 = r28
            r30.<init>(r31, r32, r33)     // Catch:{ all -> 0x0f42 }
            java.lang.Object r8 = r11.get(r6)     // Catch:{ all -> 0x0f42 }
            X.43N r8 = (X.AnonymousClass43N) r8     // Catch:{ all -> 0x0f42 }
            java.lang.String r7 = r9.A0A     // Catch:{ all -> 0x0f42 }
            if (r7 == 0) goto L_0x05df
            boolean r6 = r7.equals(r15)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x05bd
            java.lang.String r6 = r9.A08     // Catch:{ all -> 0x0f42 }
            boolean r6 = r6.equals(r14)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x05bd
            java.util.LinkedList r7 = new java.util.LinkedList     // Catch:{ all -> 0x0f42 }
            int r6 = r13.size()     // Catch:{ all -> 0x0f42 }
            java.util.List r6 = r13.subList(r10, r6)     // Catch:{ all -> 0x0f42 }
            r7.<init>(r6)     // Catch:{ all -> 0x0f42 }
        L_0x05ae:
            r6 = r24
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            r6.clear()     // Catch:{ all -> 0x0f42 }
            r6 = r24
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            r6.addAll(r7)     // Catch:{ all -> 0x0f42 }
            goto L_0x05fa
        L_0x05bd:
            if (r8 == 0) goto L_0x05df
            java.lang.String r6 = r8.A02     // Catch:{ all -> 0x0f42 }
            boolean r6 = r7.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x05df
            java.lang.String r6 = r9.A08     // Catch:{ all -> 0x0f42 }
            java.lang.String r7 = r8.A01     // Catch:{ all -> 0x0f42 }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x05df
            java.util.LinkedList r7 = new java.util.LinkedList     // Catch:{ all -> 0x0f42 }
            int r6 = r13.size()     // Catch:{ all -> 0x0f42 }
            java.util.List r6 = r13.subList(r10, r6)     // Catch:{ all -> 0x0f42 }
            r7.<init>(r6)     // Catch:{ all -> 0x0f42 }
            goto L_0x05ae
        L_0x05df:
            int r10 = r10 + 1
            goto L_0x0570
        L_0x05e2:
            java.util.LinkedList r7 = new java.util.LinkedList     // Catch:{ all -> 0x0f42 }
            r7.<init>()     // Catch:{ all -> 0x0f42 }
            goto L_0x05ae
        L_0x05e8:
            r6 = r24
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            r6.clear()     // Catch:{ all -> 0x0f42 }
            r6 = r24
            X.2wE r7 = r6.A01     // Catch:{ all -> 0x0f42 }
            if (r7 == 0) goto L_0x0600
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            r6.add(r7)     // Catch:{ all -> 0x0f42 }
        L_0x05fa:
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            r6.<init>(r12, r5, r2)     // Catch:{ all -> 0x0f42 }
            goto L_0x063e
        L_0x0600:
            java.util.LinkedList r6 = r6.A07     // Catch:{ all -> 0x0f42 }
            r7 = r27
            r6.add(r7)     // Catch:{ all -> 0x0f42 }
            goto L_0x05fa
        L_0x0608:
            r23 = r2
            r9 = r24
            java.lang.String r5 = r4.A0G()     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = r4.A0C()     // Catch:{ all -> 0x0f42 }
            r6 = r19
            long r6 = r6.longValue()     // Catch:{ all -> 0x0f42 }
            java.lang.String r7 = java.lang.Long.toString(r6)     // Catch:{ all -> 0x0f42 }
            r6 = 0
            X.2wE r27 = X.AnonymousClass2wE.A00(r4, r7, r6)     // Catch:{ all -> 0x0f42 }
            if (r3 == 0) goto L_0x0643
            java.lang.String r6 = "cold_start"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x0643
            java.util.LinkedList r6 = r9.A07     // Catch:{ all -> 0x0f42 }
            r6.clear()     // Catch:{ all -> 0x0f42 }
            java.util.LinkedList r6 = r9.A07     // Catch:{ all -> 0x0f42 }
            r7 = r27
            r6.add(r7)     // Catch:{ all -> 0x0f42 }
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            r6.<init>(r12, r5, r2)     // Catch:{ all -> 0x0f42 }
        L_0x063e:
            r2 = r24
            r2.A00 = r6     // Catch:{ all -> 0x0f42 }
            goto L_0x06a9
        L_0x0643:
            r7 = 0
            if (r3 == 0) goto L_0x0666
            java.lang.String r6 = "login"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x0666
            java.util.LinkedList r6 = r9.A07     // Catch:{ all -> 0x0f42 }
            r6.clear()     // Catch:{ all -> 0x0f42 }
            java.util.LinkedList r6 = r9.A07     // Catch:{ all -> 0x0f42 }
            r9 = r27
            r6.add(r9)     // Catch:{ all -> 0x0f42 }
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            r6.<init>(r12, r5, r2)     // Catch:{ all -> 0x0f42 }
            r2 = r24
            r2.A00 = r6     // Catch:{ all -> 0x0f42 }
            r2.A05 = r7     // Catch:{ all -> 0x0f42 }
            goto L_0x06a9
        L_0x0666:
            if (r3 == 0) goto L_0x0691
            java.lang.String r6 = "tap_top_jewel_bar"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 != 0) goto L_0x0678
            java.lang.String r6 = "tab_swipe"
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x0691
        L_0x0678:
            java.util.LinkedList r6 = r9.A07     // Catch:{ all -> 0x0f42 }
            r6.clear()     // Catch:{ all -> 0x0f42 }
            java.util.LinkedList r6 = r9.A07     // Catch:{ all -> 0x0f42 }
            r7 = r27
            r6.add(r7)     // Catch:{ all -> 0x0f42 }
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            r6.<init>(r12, r5, r2)     // Catch:{ all -> 0x0f42 }
            r9.A00 = r6     // Catch:{ all -> 0x0f42 }
            java.util.Map r2 = r9.A04     // Catch:{ all -> 0x0f42 }
            r2.clear()     // Catch:{ all -> 0x0f42 }
            goto L_0x06a9
        L_0x0691:
            if (r3 == 0) goto L_0x0463
            goto L_0x03e1
        L_0x0695:
            r5 = r24
            X.43N r8 = r5.A00     // Catch:{ all -> 0x0f42 }
            if (r8 == 0) goto L_0x06a9
            java.util.Map r7 = r5.A04     // Catch:{ all -> 0x0f42 }
            X.43N r6 = new X.43N     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r4.A0G()     // Catch:{ all -> 0x0f42 }
            r6.<init>(r12, r5, r2)     // Catch:{ all -> 0x0f42 }
            r7.put(r6, r8)     // Catch:{ all -> 0x0f42 }
        L_0x06a9:
            if (r29 == 0) goto L_0x03fd
            r5 = r27
            r2 = r24
            r2.A01 = r5     // Catch:{ all -> 0x0f42 }
            goto L_0x03fd
        L_0x06b3:
            if (r19 == 0) goto L_0x06f0
            if (r3 == 0) goto L_0x06f0
            if (r4 == 0) goto L_0x06bb
            goto L_0x07d9
        L_0x06bb:
            X.2wE r2 = new X.2wE     // Catch:{ all -> 0x0f42 }
            r4 = r19
            long r4 = r4.longValue()     // Catch:{ all -> 0x0f42 }
            java.lang.String r28 = java.lang.Long.toString(r4)     // Catch:{ all -> 0x0f42 }
            r29 = 0
            r30 = 0
            X.06A r4 = X.AnonymousClass06A.A00     // Catch:{ all -> 0x0f42 }
            long r4 = r4.now()     // Catch:{ all -> 0x0f42 }
            java.lang.String r31 = X.C12350pC.A04(r4)     // Catch:{ all -> 0x0f42 }
            r33 = 0
            r34 = 0
            r35 = 0
            r36 = 0
            r37 = 0
            r38 = 0
            r39 = 1
            r40 = 0
            java.lang.String r41 = "unknown; fallback"
            r27 = r2
            r32 = r3
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41)     // Catch:{ all -> 0x0f42 }
            goto L_0x07e8
        L_0x06f0:
            int r5 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            r2 = r24
            X.0UN r2 = r2.A02     // Catch:{ all -> 0x0f42 }
            r4 = 0
            java.lang.Object r6 = X.AnonymousClass1XX.A02(r4, r5, r2)     // Catch:{ all -> 0x0f42 }
            X.0eA r6 = (X.C07790eA) r6     // Catch:{ all -> 0x0f42 }
            monitor-enter(r6)     // Catch:{ all -> 0x0f42 }
            java.util.LinkedList r5 = new java.util.LinkedList     // Catch:{ all -> 0x07d6 }
            java.util.LinkedList r2 = r6.A08     // Catch:{ all -> 0x07d6 }
            r5.<init>(r2)     // Catch:{ all -> 0x07d6 }
            monitor-exit(r6)     // Catch:{ all -> 0x07d6 }
            java.util.Iterator r7 = r5.iterator()     // Catch:{ all -> 0x0f42 }
        L_0x070a:
            boolean r2 = r7.hasNext()     // Catch:{ all -> 0x0f42 }
            if (r2 == 0) goto L_0x0732
            java.lang.Object r2 = r7.next()     // Catch:{ all -> 0x0f42 }
            X.0pA r2 = (X.C12330pA) r2     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r2.A0D()     // Catch:{ all -> 0x0f42 }
            if (r5 != 0) goto L_0x0737
            X.4mH r5 = r2.A03     // Catch:{ all -> 0x0f42 }
        L_0x071e:
            if (r5 == 0) goto L_0x0734
            java.lang.String r6 = r5.A07     // Catch:{ all -> 0x0f42 }
            if (r6 == 0) goto L_0x0725
            goto L_0x0730
        L_0x0725:
            java.util.Map r6 = r2.A0I     // Catch:{ all -> 0x0f42 }
            java.lang.Integer r5 = r5.A0E     // Catch:{ all -> 0x0f42 }
            java.lang.Object r5 = r6.get(r5)     // Catch:{ all -> 0x0f42 }
            X.4mH r5 = (X.C97954mH) r5     // Catch:{ all -> 0x0f42 }
            goto L_0x071e
        L_0x0730:
            r2 = r5
            goto L_0x0735
        L_0x0732:
            r2 = 0
            goto L_0x0737
        L_0x0734:
            r2 = 0
        L_0x0735:
            if (r2 == 0) goto L_0x070a
        L_0x0737:
            if (r2 == 0) goto L_0x0799
            X.2wE r27 = new X.2wE     // Catch:{ all -> 0x0f42 }
            java.lang.String r28 = r2.A0D()     // Catch:{ all -> 0x0f42 }
            java.lang.String r29 = r2.A0G()     // Catch:{ all -> 0x0f42 }
            int r30 = r2.A05()     // Catch:{ all -> 0x0f42 }
            X.06A r4 = X.AnonymousClass06A.A00     // Catch:{ all -> 0x0f42 }
            long r4 = r4.now()     // Catch:{ all -> 0x0f42 }
            java.lang.String r31 = X.C12350pC.A04(r4)     // Catch:{ all -> 0x0f42 }
            java.lang.String r33 = r2.A0C()     // Catch:{ all -> 0x0f42 }
            java.lang.Integer r34 = r2.A09()     // Catch:{ all -> 0x0f42 }
            java.lang.String r35 = r2.A0B()     // Catch:{ all -> 0x0f42 }
            X.4mI r36 = r2.A06()     // Catch:{ all -> 0x0f42 }
            X.46i r37 = r2.A07()     // Catch:{ all -> 0x0f42 }
            java.lang.Long r38 = r2.A0A()     // Catch:{ all -> 0x0f42 }
            r39 = 1
            java.util.Map r40 = r2.A0J()     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r2.A0I()     // Catch:{ all -> 0x0f42 }
            java.lang.String r4 = "; fallback"
            java.lang.String r41 = X.AnonymousClass08S.A0J(r5, r4)     // Catch:{ all -> 0x0f42 }
            r4 = r27
            r32 = r3
            r27.<init>(r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41)     // Catch:{ all -> 0x0f42 }
            r3 = r24
            java.util.LinkedList r3 = r3.A07     // Catch:{ all -> 0x0f42 }
            r3.add(r4)     // Catch:{ all -> 0x0f42 }
            X.43N r4 = new X.43N     // Catch:{ all -> 0x0f42 }
            java.lang.String r5 = r2.A0D()     // Catch:{ all -> 0x0f42 }
            java.lang.String r3 = r2.A0G()     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = r2.A0C()     // Catch:{ all -> 0x0f42 }
            r4.<init>(r5, r3, r2)     // Catch:{ all -> 0x0f42 }
            goto L_0x07fb
        L_0x0799:
            r5 = 3
            int r3 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x0f42 }
            r2 = r24
            X.0UN r2 = r2.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r5, r3, r2)     // Catch:{ all -> 0x0f42 }
            X.1ZE r3 = (X.AnonymousClass1ZE) r3     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "fb4a_attribution_id_no_fallback"
            X.0bW r3 = r3.A01(r2)     // Catch:{ all -> 0x0f42 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r5 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x0f42 }
            r2 = 148(0x94, float:2.07E-43)
            r5.<init>(r3, r2)     // Catch:{ all -> 0x0f42 }
            boolean r2 = r5.A0G()     // Catch:{ all -> 0x0f42 }
            if (r2 == 0) goto L_0x07ff
            int r3 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f42 }
            r2 = r24
            X.0UN r2 = r2.A02     // Catch:{ all -> 0x0f42 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x0f42 }
            X.0eA r2 = (X.C07790eA) r2     // Catch:{ all -> 0x0f42 }
            java.util.List r2 = r2.A07()     // Catch:{ all -> 0x0f42 }
            java.lang.String r3 = r2.toString()     // Catch:{ all -> 0x0f42 }
            java.lang.String r2 = "session_path"
            r5.A0D(r2, r3)     // Catch:{ all -> 0x0f42 }
            r5.A06()     // Catch:{ all -> 0x0f42 }
            goto L_0x07ff
        L_0x07d6:
            r1 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x07d6 }
            throw r1     // Catch:{ all -> 0x0f42 }
        L_0x07d9:
            r2 = r19
            long r2 = r2.longValue()     // Catch:{ all -> 0x0f42 }
            java.lang.String r3 = java.lang.Long.toString(r2)     // Catch:{ all -> 0x0f42 }
            r2 = 1
            X.2wE r2 = X.AnonymousClass2wE.A00(r4, r3, r2)     // Catch:{ all -> 0x0f42 }
        L_0x07e8:
            r3 = r24
            java.util.LinkedList r3 = r3.A07     // Catch:{ all -> 0x0f42 }
            r3.add(r2)     // Catch:{ all -> 0x0f42 }
            X.43N r4 = new X.43N     // Catch:{ all -> 0x0f42 }
            r2 = r19
            java.lang.String r3 = r2.toString()     // Catch:{ all -> 0x0f42 }
            r2 = 0
            r4.<init>(r3, r2, r2)     // Catch:{ all -> 0x0f42 }
        L_0x07fb:
            r2 = r24
            r2.A00 = r4     // Catch:{ all -> 0x0f42 }
        L_0x07ff:
            r2 = r24
            X.1YL r2 = r2.A06     // Catch:{ all -> 0x0f42 }
            com.google.common.collect.ImmutableList r5 = X.AnonymousClass14M.A08     // Catch:{ all -> 0x0f42 }
            boolean r2 = r2.A05(r5)     // Catch:{ all -> 0x0f42 }
            if (r2 == 0) goto L_0x0816
            r2 = r24
            X.1YL r4 = r2.A06     // Catch:{ all -> 0x0f42 }
            X.1Ym r3 = X.C25141Ym.INSTANCE     // Catch:{ all -> 0x0f42 }
            r2 = r23
            r4.A04(r5, r2, r3)     // Catch:{ all -> 0x0f42 }
        L_0x0816:
            monitor-exit(r24)     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = "unknown"
            r2 = r17
            if (r2 != r3) goto L_0x081f
            r17 = 0
        L_0x081f:
            if (r1 != r3) goto L_0x0822
            r1 = 0
        L_0x0822:
            r0.A05 = r1     // Catch:{ all -> 0x0f48 }
            com.facebook.common.time.RealtimeSinceBootClock r2 = com.facebook.common.time.RealtimeSinceBootClock.A00     // Catch:{ all -> 0x0f48 }
            r2.now()     // Catch:{ all -> 0x0f48 }
            X.0nb r5 = new X.0nb     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = "navigation"
            r5.<init>(r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = "pigeon_reserved_keyword_module"
            if (r17 == 0) goto L_0x0837
            r2 = r17
            goto L_0x0839
        L_0x0837:
            java.lang.String r2 = "unknown"
        L_0x0839:
            r5.A0D(r3, r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = "source_module"
            r2 = r17
            r5.A0D(r3, r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = "source_module_class"
            r7 = r20
            r5.A0D(r2, r7)     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = "dest_module"
            r5.A0D(r2, r1)     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = "seq"
            int r2 = r0.A0B     // Catch:{ all -> 0x0f48 }
            r5.A09(r3, r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r6 = "visitation_id"
            int r4 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 11
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)     // Catch:{ all -> 0x0f48 }
            X.0eA r2 = (X.C07790eA) r2     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = r2.A05()     // Catch:{ all -> 0x0f48 }
            r5.A0D(r6, r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r6 = "nav_attribution_id"
            int r4 = X.AnonymousClass1Y3.A86     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 20
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r4, r3)     // Catch:{ all -> 0x0f48 }
            X.14M r2 = (X.AnonymousClass14M) r2     // Catch:{ all -> 0x0f48 }
            com.fasterxml.jackson.databind.JsonNode r2 = r2.A03()     // Catch:{ all -> 0x0f48 }
            r5.A0B(r6, r2)     // Catch:{ all -> 0x0f48 }
            r4 = 17
            int r3 = X.AnonymousClass1Y3.BFX     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x0f48 }
            X.0tZ r2 = (X.AnonymousClass0tZ) r2     // Catch:{ all -> 0x0f48 }
            r2.A03(r5)     // Catch:{ all -> 0x0f48 }
            r10 = r44
            if (r44 == 0) goto L_0x08a7
            java.lang.String r6 = "pigeon_reserved_keyword_uuid"
            r4 = 4
            int r3 = X.AnonymousClass1Y3.AUN     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)     // Catch:{ all -> 0x0f48 }
            X.144 r2 = (X.AnonymousClass144) r2     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = r2.A01(r10)     // Catch:{ all -> 0x0f48 }
            r5.A0D(r6, r2)     // Catch:{ all -> 0x0f48 }
        L_0x08a7:
            r2 = r18
            boolean r2 = r2 instanceof X.AnonymousClass151     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x08bc
            r2 = r18
            X.151 r2 = (X.AnonymousClass151) r2     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = r2.getContentUri()     // Catch:{ all -> 0x0f48 }
            if (r3 == 0) goto L_0x08bc
            java.lang.String r2 = "dest_module_uri"
            r5.A0D(r2, r3)     // Catch:{ all -> 0x0f48 }
        L_0x08bc:
            r2 = r18
            boolean r2 = r2 instanceof X.C10890l1     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x08dc
            r4 = r18
            X.0l1 r4 = (X.C10890l1) r4     // Catch:{ all -> 0x0f48 }
            java.lang.Integer r2 = r4.getObjectType$REDEX$2GJxpwjwFN8()     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x08dc
            java.lang.String r3 = "pigeon_reserved_keyword_obj_type"
            java.lang.String r2 = "pages"
            r5.A0D(r3, r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = "pigeon_reserved_keyword_obj_id"
            java.lang.String r3 = r4.getObjectId()     // Catch:{ all -> 0x0f48 }
            r5.A0D(r2, r3)     // Catch:{ all -> 0x0f48 }
        L_0x08dc:
            r2 = r18
            boolean r2 = r2 instanceof X.C11570nO     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x090f
            r2 = r18
            X.0nO r2 = (X.C11570nO) r2     // Catch:{ all -> 0x0f48 }
            java.util.Map r3 = r2.Acr()     // Catch:{ all -> 0x0f48 }
            if (r3 != 0) goto L_0x08f8
            if (r16 != 0) goto L_0x08f1
            com.google.common.collect.ImmutableMap r16 = com.google.common.collect.RegularImmutableMap.A03     // Catch:{ all -> 0x0f48 }
            goto L_0x090f
        L_0x08f1:
            r2 = r16
            com.google.common.collect.ImmutableMap r16 = com.google.common.collect.ImmutableMap.copyOf(r2)     // Catch:{ all -> 0x0f48 }
            goto L_0x090f
        L_0x08f8:
            if (r16 != 0) goto L_0x08ff
            com.google.common.collect.ImmutableMap r16 = com.google.common.collect.ImmutableMap.copyOf(r3)     // Catch:{ all -> 0x0f48 }
            goto L_0x090f
        L_0x08ff:
            com.google.common.collect.ImmutableMap$Builder r2 = com.google.common.collect.ImmutableMap.builder()     // Catch:{ all -> 0x0f48 }
            r7 = r16
            r2.putAll(r7)     // Catch:{ all -> 0x0f48 }
            r2.putAll(r3)     // Catch:{ all -> 0x0f48 }
            com.google.common.collect.ImmutableMap r16 = r2.build()     // Catch:{ all -> 0x0f48 }
        L_0x090f:
            r3 = r47
            if (r47 == 0) goto L_0x0918
            java.lang.String r2 = "pigeon_reserved_keyword_src_interface"
            r5.A0D(r2, r3)     // Catch:{ all -> 0x0f48 }
        L_0x0918:
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ all -> 0x0f48 }
            r4.<init>()     // Catch:{ all -> 0x0f48 }
            if (r16 == 0) goto L_0x0924
            r3 = r16
            r4.putAll(r3)     // Catch:{ all -> 0x0f48 }
        L_0x0924:
            java.lang.String r3 = r0.A09()     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = "click_point"
            r4.put(r2, r3)     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = r0.A07     // Catch:{ all -> 0x0f48 }
            java.lang.String r2 = "last_tracking_code"
            r4.put(r2, r3)     // Catch:{ all -> 0x0f48 }
            boolean r2 = r4.isEmpty()     // Catch:{ all -> 0x0f48 }
            if (r2 != 0) goto L_0x093e
            r2 = 0
            X.C11670nb.A02(r5, r4, r2)     // Catch:{ all -> 0x0f48 }
        L_0x093e:
            r6 = 10
            int r3 = X.AnonymousClass1Y3.BP0     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ all -> 0x0f48 }
            X.0hE r2 = (X.C09390hE) r2     // Catch:{ all -> 0x0f48 }
            X.EVQ r7 = X.C09390hE.A01(r2)     // Catch:{ all -> 0x0f48 }
            if (r7 == 0) goto L_0x095d
            r2 = r17
            java.lang.Object[] r6 = new java.lang.Object[]{r2, r1}     // Catch:{ all -> 0x0f48 }
            java.lang.String r3 = "navigation"
            java.lang.String r2 = "source: %s, dest: %s"
            r7.A01(r3, r2, r6)     // Catch:{ all -> 0x0f48 }
        L_0x095d:
            int r6 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 11
            java.lang.Object r9 = X.AnonymousClass1XX.A02(r2, r6, r3)     // Catch:{ all -> 0x0f48 }
            X.0eA r9 = (X.C07790eA) r9     // Catch:{ all -> 0x0f48 }
            X.1jJ r2 = r0.A01     // Catch:{ all -> 0x0f48 }
            java.lang.Object r8 = r2.A00()     // Catch:{ all -> 0x0f48 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x0f48 }
            r2 = r19
            java.lang.String r32 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0f48 }
            r7 = r10
            monitor-enter(r9)     // Catch:{ all -> 0x0f48 }
            r29 = 0
            if (r44 == 0) goto L_0x09af
            if (r1 == 0) goto L_0x09af
            boolean r2 = r10 instanceof android.app.Activity     // Catch:{ all -> 0x0f3f }
            if (r2 == 0) goto L_0x09af
            android.app.Activity r10 = (android.app.Activity) r10     // Catch:{ all -> 0x0f3f }
            int r3 = r10.hashCode()     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.util.Iterator r11 = r2.iterator()     // Catch:{ all -> 0x0f3f }
            r6 = 0
        L_0x0990:
            boolean r2 = r11.hasNext()     // Catch:{ all -> 0x0f3f }
            if (r2 == 0) goto L_0x09a3
            java.lang.Object r2 = r11.next()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            int r2 = r2.A00     // Catch:{ all -> 0x0f3f }
            if (r2 == r3) goto L_0x09a4
            int r6 = r6 + 1
            goto L_0x0990
        L_0x09a3:
            r6 = -1
        L_0x09a4:
            r3 = 0
        L_0x09a5:
            if (r3 >= r6) goto L_0x09b2
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            r2.removeFirst()     // Catch:{ all -> 0x0f3f }
            int r3 = r3 + 1
            goto L_0x09a5
        L_0x09af:
            java.lang.Integer r10 = X.AnonymousClass07B.A0i     // Catch:{ all -> 0x0f3f }
            goto L_0x09fb
        L_0x09b2:
            X.0l2 r6 = new X.0l2     // Catch:{ all -> 0x0f3f }
            java.lang.Class r3 = r10.getClass()     // Catch:{ all -> 0x0f3f }
            int r2 = r10.hashCode()     // Catch:{ all -> 0x0f3f }
            r6.<init>(r3, r2, r1)     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0f3f }
            if (r2 != 0) goto L_0x0a5b
            java.lang.Class r3 = r6.A01     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            java.lang.Class r2 = r2.A01     // Catch:{ all -> 0x0f3f }
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x0f3f }
            if (r2 == 0) goto L_0x0a5b
            int r3 = r6.A00     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            int r2 = r2.A00     // Catch:{ all -> 0x0f3f }
            if (r3 == r2) goto L_0x0a12
            java.lang.String r3 = r6.A02     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            java.lang.String r2 = r2.A02     // Catch:{ all -> 0x0f3f }
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x0f3f }
            if (r2 != 0) goto L_0x0a12
            java.lang.Integer r10 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0f3f }
        L_0x09fb:
            int r2 = r10.intValue()     // Catch:{ all -> 0x0f3f }
            switch(r2) {
                case 0: goto L_0x0a5e;
                case 1: goto L_0x0a5e;
                case 2: goto L_0x0a75;
                case 3: goto L_0x0a75;
                case 4: goto L_0x0a91;
                case 5: goto L_0x0a98;
                default: goto L_0x0a02;
            }     // Catch:{ all -> 0x0f3f }
        L_0x0a02:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0f3f }
            java.lang.String r2 = "Unrecognized navigation status "
            java.lang.String r1 = X.C73203fg.A00(r10)     // Catch:{ all -> 0x0f3f }
            java.lang.String r1 = X.AnonymousClass08S.A0J(r2, r1)     // Catch:{ all -> 0x0f3f }
            r3.<init>(r1)     // Catch:{ all -> 0x0f3f }
            throw r3     // Catch:{ all -> 0x0f3f }
        L_0x0a12:
            int r3 = r6.A00     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            int r2 = r2.A00     // Catch:{ all -> 0x0f3f }
            if (r3 == r2) goto L_0x0a35
            java.lang.String r3 = r6.A02     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            java.lang.String r2 = r2.A02     // Catch:{ all -> 0x0f3f }
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x0f3f }
            if (r2 == 0) goto L_0x0a35
            java.lang.Integer r10 = X.AnonymousClass07B.A0N     // Catch:{ all -> 0x0f3f }
            goto L_0x09fb
        L_0x0a35:
            int r3 = r6.A00     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            int r2 = r2.A00     // Catch:{ all -> 0x0f3f }
            if (r3 != r2) goto L_0x0a58
            java.lang.String r3 = r6.A02     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x0f3f }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0f3f }
            java.lang.String r2 = r2.A02     // Catch:{ all -> 0x0f3f }
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x0f3f }
            if (r2 != 0) goto L_0x0a58
            java.lang.Integer r10 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x0f3f }
            goto L_0x09fb
        L_0x0a58:
            java.lang.Integer r10 = X.AnonymousClass07B.A0Y     // Catch:{ all -> 0x0f3f }
            goto L_0x09fb
        L_0x0a5b:
            java.lang.Integer r10 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0f3f }
            goto L_0x09fb
        L_0x0a5e:
            X.0l2 r6 = new X.0l2     // Catch:{ all -> 0x0f3f }
            android.app.Activity r7 = (android.app.Activity) r7     // Catch:{ all -> 0x0f3f }
            java.lang.Class r3 = r7.getClass()     // Catch:{ all -> 0x0f3f }
            int r2 = r7.hashCode()     // Catch:{ all -> 0x0f3f }
            r6.<init>(r3, r2, r1)     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            r2.addFirst(r6)     // Catch:{ all -> 0x0f3f }
            java.lang.Integer r29 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0f3f }
            goto L_0x0a98
        L_0x0a75:
            X.0l2 r6 = new X.0l2     // Catch:{ all -> 0x0f3f }
            android.app.Activity r7 = (android.app.Activity) r7     // Catch:{ all -> 0x0f3f }
            java.lang.Class r3 = r7.getClass()     // Catch:{ all -> 0x0f3f }
            int r2 = r7.hashCode()     // Catch:{ all -> 0x0f3f }
            r6.<init>(r3, r2, r1)     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            r2.removeFirst()     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            r2.addFirst(r6)     // Catch:{ all -> 0x0f3f }
            java.lang.Integer r29 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0f3f }
            goto L_0x0a98
        L_0x0a91:
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0f3f }
            r2.peek()     // Catch:{ all -> 0x0f3f }
            java.lang.Integer r29 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0f3f }
        L_0x0a98:
            if (r29 == 0) goto L_0x0ad5
            X.1YL r2 = r9.A06     // Catch:{ all -> 0x0f3f }
            com.google.common.collect.ImmutableList r7 = X.C07790eA.A0B     // Catch:{ all -> 0x0f3f }
            boolean r2 = r2.A05(r7)     // Catch:{ all -> 0x0f3f }
            if (r2 == 0) goto L_0x0ad5
            X.1YL r6 = r9.A06     // Catch:{ all -> 0x0f3f }
            X.3yS r3 = new X.3yS     // Catch:{ all -> 0x0f3f }
            java.util.List r28 = r9.A06()     // Catch:{ all -> 0x0f3f }
            monitor-enter(r9)     // Catch:{ all -> 0x0f3f }
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0ad2 }
            boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0ad2 }
            if (r2 == 0) goto L_0x0ab8
            r2 = 0
            monitor-exit(r9)     // Catch:{ all -> 0x0f3f }
            goto L_0x0ac3
        L_0x0ab8:
            java.util.LinkedList r2 = r9.A09     // Catch:{ all -> 0x0ad2 }
            java.lang.Object r2 = r2.getFirst()     // Catch:{ all -> 0x0ad2 }
            X.0l2 r2 = (X.C10900l2) r2     // Catch:{ all -> 0x0ad2 }
            java.lang.String r2 = r2.A03     // Catch:{ all -> 0x0ad2 }
            monitor-exit(r9)     // Catch:{ all -> 0x0f3f }
        L_0x0ac3:
            r27 = r3
            r30 = r2
            r31 = r8
            r27.<init>(r28, r29, r30, r31, r32)     // Catch:{ all -> 0x0f3f }
            X.1Ym r2 = X.C25141Ym.INSTANCE     // Catch:{ all -> 0x0f3f }
            r6.A04(r7, r3, r2)     // Catch:{ all -> 0x0f3f }
            goto L_0x0ad5
        L_0x0ad2:
            r1 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0f3f }
            throw r1     // Catch:{ all -> 0x0f3f }
        L_0x0ad5:
            monitor-exit(r9)     // Catch:{ all -> 0x0f48 }
            com.facebook.device.resourcemonitor.DataUsageBytes r2 = r0.A0L     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x0b0a
            int r7 = android.os.Process.myUid()     // Catch:{ all -> 0x0f48 }
            int r6 = X.AnonymousClass1Y3.BN4     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 15
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r6, r3)     // Catch:{ all -> 0x0f48 }
            X.0f9 r3 = (X.AnonymousClass0f9) r3     // Catch:{ all -> 0x0f48 }
            r2 = 0
            com.facebook.device.resourcemonitor.DataUsageBytes r9 = r3.A04(r7, r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r8 = "bytes_rx"
            long r2 = r9.A00     // Catch:{ all -> 0x0f48 }
            com.facebook.device.resourcemonitor.DataUsageBytes r6 = r0.A0L     // Catch:{ all -> 0x0f48 }
            long r6 = r6.A00     // Catch:{ all -> 0x0f48 }
            long r2 = r2 - r6
            r5.A0A(r8, r2)     // Catch:{ all -> 0x0f48 }
            java.lang.String r8 = "bytes_tx"
            long r2 = r9.A01     // Catch:{ all -> 0x0f48 }
            com.facebook.device.resourcemonitor.DataUsageBytes r6 = r0.A0L     // Catch:{ all -> 0x0f48 }
            long r6 = r6.A01     // Catch:{ all -> 0x0f48 }
            long r2 = r2 - r6
            r5.A0A(r8, r2)     // Catch:{ all -> 0x0f48 }
            r2 = 0
            r0.A0L = r2     // Catch:{ all -> 0x0f48 }
        L_0x0b0a:
            r6 = 9
            int r3 = X.AnonymousClass1Y3.A8J     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ all -> 0x0f48 }
            java.util.Random r2 = (java.util.Random) r2     // Catch:{ all -> 0x0f48 }
            int r2 = r2.nextInt()     // Catch:{ all -> 0x0f48 }
            int r2 = r2 % 1000
            if (r2 != 0) goto L_0x0b35
            int r7 = android.os.Process.myUid()     // Catch:{ all -> 0x0f48 }
            int r6 = X.AnonymousClass1Y3.BN4     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 15
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r2, r6, r3)     // Catch:{ all -> 0x0f48 }
            X.0f9 r3 = (X.AnonymousClass0f9) r3     // Catch:{ all -> 0x0f48 }
            r2 = 0
            com.facebook.device.resourcemonitor.DataUsageBytes r2 = r3.A04(r7, r2)     // Catch:{ all -> 0x0f48 }
            r0.A0L = r2     // Catch:{ all -> 0x0f48 }
        L_0x0b35:
            r6 = 19
            int r3 = X.AnonymousClass1Y3.Aoq     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r8 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ all -> 0x0f48 }
            X.0kx r8 = (X.C10850kx) r8     // Catch:{ all -> 0x0f48 }
            monitor-enter(r8)     // Catch:{ all -> 0x0f48 }
            X.2Of r2 = r8.A00     // Catch:{ all -> 0x0f3c }
            if (r2 == 0) goto L_0x0c60
            java.lang.String r6 = r5.A05     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "media_metrics_open_application"
            boolean r7 = r6.equals(r2)     // Catch:{ all -> 0x0f3c }
            if (r7 != 0) goto L_0x0b59
            java.lang.String r2 = "navigation"
            boolean r2 = r6.equals(r2)     // Catch:{ all -> 0x0f3c }
            if (r2 != 0) goto L_0x0b59
            goto L_0x0b98
        L_0x0b59:
            if (r7 != 0) goto L_0x0b93
            java.lang.String r2 = "source_module"
            java.lang.String r7 = r5.A07(r2)     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "dest_module"
            java.lang.String r6 = r5.A07(r2)     // Catch:{ all -> 0x0f3c }
            if (r6 == 0) goto L_0x0b98
            java.lang.String r2 = "webview"
            boolean r9 = r6.equals(r2)     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "dest_module_uri"
            java.lang.String r3 = r5.A07(r2)     // Catch:{ all -> 0x0f3c }
            if (r9 == 0) goto L_0x0b9c
            if (r3 == 0) goto L_0x0b91
            r2 = 4
            java.lang.String r2 = X.ECX.$const$string(r2)     // Catch:{ all -> 0x0f3c }
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x0f3c }
            if (r2 != 0) goto L_0x0b95
            r2 = 154(0x9a, float:2.16E-43)
            java.lang.String r2 = X.AnonymousClass80H.$const$string(r2)     // Catch:{ all -> 0x0f3c }
            boolean r2 = r3.equals(r2)     // Catch:{ all -> 0x0f3c }
            if (r2 == 0) goto L_0x0b91
            goto L_0x0b95
        L_0x0b91:
            r2 = 0
            goto L_0x0b96
        L_0x0b93:
            r2 = 1
            goto L_0x0b99
        L_0x0b95:
            r2 = 1
        L_0x0b96:
            if (r2 == 0) goto L_0x0b9c
        L_0x0b98:
            r2 = 0
        L_0x0b99:
            if (r2 == 0) goto L_0x0c60
            goto L_0x0bfd
        L_0x0b9c:
            java.lang.String r2 = "deprecated_full_screen_video_player"
            boolean r2 = r2.equals(r6)     // Catch:{ all -> 0x0f3c }
            if (r2 != 0) goto L_0x0b98
            java.lang.String r3 = "zero_intent_interstitial"
            boolean r2 = r3.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0f3c }
            if (r2 != 0) goto L_0x0b98
            java.lang.String r2 = "infrastructure"
            boolean r2 = r2.equals(r6)     // Catch:{ all -> 0x0f3c }
            if (r2 != 0) goto L_0x0b98
            boolean r3 = r3.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "native_newsfeed"
            if (r3 == 0) goto L_0x0bc3
            boolean r3 = r2.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0f3c }
            if (r3 == 0) goto L_0x0bc3
            goto L_0x0b98
        L_0x0bc3:
            boolean r3 = r2.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0f3c }
            r9 = 1
            if (r3 == 0) goto L_0x0bd0
            boolean r2 = r2.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0f3c }
            r2 = r2 ^ r9
            goto L_0x0b99
        L_0x0bd0:
            java.lang.String r2 = "story_view"
            boolean r3 = r2.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0f3c }
            if (r3 == 0) goto L_0x0bde
            boolean r2 = r2.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0f3c }
            r2 = r2 ^ r9
            goto L_0x0b99
        L_0x0bde:
            java.lang.String r2 = "story_feedback_flyout"
            boolean r3 = r2.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0f3c }
            if (r3 == 0) goto L_0x0bec
            boolean r2 = r2.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0f3c }
            r2 = r2 ^ r9
            goto L_0x0b99
        L_0x0bec:
            java.lang.String r2 = "webview"
            boolean r2 = r2.equals(r7)     // Catch:{ all -> 0x0f3c }
            if (r2 != 0) goto L_0x0b93
            java.lang.String r2 = "tab_INBOX"
            boolean r2 = r2.equals(r6)     // Catch:{ all -> 0x0f3c }
            if (r2 == 0) goto L_0x0b93
            goto L_0x0b98
        L_0x0bfd:
            boolean r2 = X.C10850kx.A04(r8)     // Catch:{ all -> 0x0f3c }
            if (r2 == 0) goto L_0x0c60
            X.1lP r3 = r8.A02     // Catch:{ all -> 0x0f3c }
            java.lang.Runnable r2 = r8.A03     // Catch:{ all -> 0x0f3c }
            X.AnonymousClass00S.A02(r3, r2)     // Catch:{ all -> 0x0f3c }
            java.lang.String r3 = X.C10850kx.A01()     // Catch:{ all -> 0x0f3c }
            X.2Of r2 = r8.A00     // Catch:{ all -> 0x0f3c }
            r2.A02(r3)     // Catch:{ all -> 0x0f3c }
            X.C10850kx.A03(r8)     // Catch:{ all -> 0x0f3c }
            com.fasterxml.jackson.databind.node.ObjectNode r6 = new com.fasterxml.jackson.databind.node.ObjectNode     // Catch:{ all -> 0x0f3c }
            com.fasterxml.jackson.databind.node.JsonNodeFactory r2 = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance     // Catch:{ all -> 0x0f3c }
            r6.<init>(r2)     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "event_trace_id"
            r6.put(r2, r3)     // Catch:{ all -> 0x0f3c }
            java.lang.String r3 = "tracking"
            X.2Of r2 = r8.A00     // Catch:{ all -> 0x0f3c }
            com.fasterxml.jackson.databind.JsonNode r2 = r2.A00()     // Catch:{ all -> 0x0f3c }
            r5.A0B(r3, r2)     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "last_event_context"
            r5.A0B(r2, r6)     // Catch:{ all -> 0x0f3c }
            if (r10 == 0) goto L_0x0c3d
            java.lang.String r3 = "navigation"
            java.lang.String r2 = X.C73203fg.A00(r10)     // Catch:{ all -> 0x0f3c }
            r5.A0D(r3, r2)     // Catch:{ all -> 0x0f3c }
        L_0x0c3d:
            java.lang.String r6 = "dest_module_uri"
            java.lang.String r2 = r5.A07(r6)     // Catch:{ all -> 0x0f3c }
            if (r2 != 0) goto L_0x0c5a
            X.2Of r3 = r8.A00     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "ads_navigation_url"
            java.lang.String r2 = r3.A01(r2)     // Catch:{ all -> 0x0f3c }
            if (r2 == 0) goto L_0x0c5a
            X.2Of r3 = r8.A00     // Catch:{ all -> 0x0f3c }
            java.lang.String r2 = "ads_navigation_url"
            java.lang.String r2 = r3.A01(r2)     // Catch:{ all -> 0x0f3c }
            r5.A0D(r6, r2)     // Catch:{ all -> 0x0f3c }
        L_0x0c5a:
            r2 = 0
            r8.A00 = r2     // Catch:{ all -> 0x0f3c }
            monitor-exit(r8)     // Catch:{ all -> 0x0f48 }
            r6 = 1
            goto L_0x0c62
        L_0x0c60:
            monitor-exit(r8)     // Catch:{ all -> 0x0f48 }
            r6 = 0
        L_0x0c62:
            r2 = r21
            r5.A02 = r2     // Catch:{ all -> 0x0f48 }
            r8 = 1
            if (r6 == 0) goto L_0x0c79
            r6 = 14
            int r3 = X.AnonymousClass1Y3.AR0     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ all -> 0x0f48 }
            com.facebook.analytics.DeprecatedAnalyticsLogger r2 = (com.facebook.analytics.DeprecatedAnalyticsLogger) r2     // Catch:{ all -> 0x0f48 }
            r2.A06(r5)     // Catch:{ all -> 0x0f48 }
            goto L_0x0ca8
        L_0x0c79:
            r6 = 13
            int r3 = X.AnonymousClass1Y3.BTX     // Catch:{ all -> 0x0f48 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f48 }
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r6, r3, r2)     // Catch:{ all -> 0x0f48 }
            X.0bA r7 = (X.C06230bA) r7     // Catch:{ all -> 0x0f48 }
            java.lang.String r6 = "navigation"
            java.lang.Integer r3 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0f48 }
            r2 = 0
            X.0ZF r6 = r7.A05(r6, r8, r3, r2)     // Catch:{ all -> 0x0f48 }
            boolean r2 = r6.A0G()     // Catch:{ all -> 0x0f48 }
            if (r2 == 0) goto L_0x0ca8
            X.C11670nb.A01(r5)     // Catch:{ all -> 0x0f48 }
            com.fasterxml.jackson.databind.node.ObjectNode r3 = r5.A04     // Catch:{ all -> 0x0f48 }
            X.0os r2 = r6.A0B()     // Catch:{ all -> 0x0f48 }
            X.C11960oJ.A01(r3, r2)     // Catch:{ all -> 0x0f48 }
            r10 = r21
            r6.A06(r10)     // Catch:{ all -> 0x0f48 }
            r6.A0E()     // Catch:{ all -> 0x0f48 }
        L_0x0ca8:
            if (r49 == 0) goto L_0x0cdc
            int r5 = X.AnonymousClass1Y3.Asq     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 7
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r5, r3)     // Catch:{ all -> 0x0f48 }
            X.14J r5 = (X.AnonymousClass14J) r5     // Catch:{ all -> 0x0f48 }
            java.lang.Object r6 = r5.A02     // Catch:{ all -> 0x0f48 }
            monitor-enter(r6)     // Catch:{ all -> 0x0f48 }
            java.util.Stack r2 = r5.A01     // Catch:{ all -> 0x0cd8 }
            r2.clear()     // Catch:{ all -> 0x0cd8 }
            java.util.Stack r7 = r5.A01     // Catch:{ all -> 0x0cd8 }
            X.15L r3 = new X.15L     // Catch:{ all -> 0x0cd8 }
            java.lang.String r2 = "dest_module_class"
            java.lang.Object r2 = r4.get(r2)     // Catch:{ all -> 0x0cd8 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0cd8 }
            r3.<init>(r1, r2, r4)     // Catch:{ all -> 0x0cd8 }
            r7.push(r3)     // Catch:{ all -> 0x0cd8 }
            monitor-exit(r6)     // Catch:{ all -> 0x0cd8 }
            X.15M r3 = new X.15M     // Catch:{ all -> 0x0f48 }
            r2 = r17
            r3.<init>(r5, r2, r1, r4)     // Catch:{ all -> 0x0f48 }
            goto L_0x0cee
        L_0x0cd8:
            r1 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0cd8 }
            goto L_0x0f47
        L_0x0cdc:
            int r5 = X.AnonymousClass1Y3.Asq     // Catch:{ all -> 0x0f48 }
            X.0UN r3 = r0.A03     // Catch:{ all -> 0x0f48 }
            r2 = 7
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r2, r5, r3)     // Catch:{ all -> 0x0f48 }
            X.14J r5 = (X.AnonymousClass14J) r5     // Catch:{ all -> 0x0f48 }
            X.15M r3 = new X.15M     // Catch:{ all -> 0x0f48 }
            r2 = r17
            r3.<init>(r5, r2, r1, r4)     // Catch:{ all -> 0x0f48 }
        L_0x0cee:
            r5 = 0
            if (r42 != 0) goto L_0x0cfb
            r0.A0D(r5)     // Catch:{ all -> 0x0f48 }
            r0.A07 = r5     // Catch:{ all -> 0x0f48 }
            int r2 = r0.A0B     // Catch:{ all -> 0x0f48 }
            int r2 = r2 + r8
            r0.A0B = r2     // Catch:{ all -> 0x0f48 }
        L_0x0cfb:
            monitor-exit(r0)     // Catch:{ all -> 0x0f48 }
            X.14J r2 = r3.A03
            java.lang.String r7 = r3.A01
            java.lang.String r6 = r3.A00
            java.util.Map r4 = r3.A02
            int r8 = X.AnonymousClass1Y3.BFb
            X.0UN r3 = r2.A00
            r2 = 0
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r2, r8, r3)
            java.util.Set r2 = (java.util.Set) r2
            java.util.Iterator r3 = r2.iterator()
        L_0x0d13:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x0d23
            java.lang.Object r2 = r3.next()
            X.1LY r2 = (X.AnonymousClass1LY) r2
            r2.BgD(r7, r6, r4)
            goto L_0x0d13
        L_0x0d23:
            if (r16 == 0) goto L_0x0d2f
            java.lang.String r3 = "dest_module_class"
            r2 = r16
            java.lang.Object r5 = r2.get(r3)
            java.lang.String r5 = (java.lang.String) r5
        L_0x0d2f:
            r4 = 23
            int r3 = X.AnonymousClass1Y3.A4K
            X.0UN r2 = r0.A03
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)
            java.util.Set r2 = (java.util.Set) r2
            java.util.Iterator r6 = r2.iterator()
        L_0x0d3f:
            boolean r2 = r6.hasNext()
            if (r2 == 0) goto L_0x0d6c
            java.lang.Object r2 = r6.next()
            X.1og r2 = (X.C34121og) r2
            if (r1 == 0) goto L_0x0d3f
            r4 = 0
            int r3 = X.AnonymousClass1Y3.AIJ
            X.0UN r2 = r2.A00
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.1f5 r2 = (X.C28611f5) r2
            X.1fB r2 = r2.A01()
            X.1qk r4 = r2.A04()
            if (r4 == 0) goto L_0x0d3f
            r3 = 4
            X.1ql r2 = new X.1ql
            r2.<init>(r1)
            r4.A00(r3, r2)
            goto L_0x0d3f
        L_0x0d6c:
            r4 = 18
            int r3 = X.AnonymousClass1Y3.AcD
            X.0UN r2 = r0.A03
            java.lang.Object r3 = X.AnonymousClass1XX.A02(r4, r3, r2)
            X.1YI r3 = (X.AnonymousClass1YI) r3
            r2 = 88
            r6 = 0
            boolean r8 = r3.AbO(r2, r6)
            java.lang.Object r3 = X.AnonymousClass01P.A0Z
            monitor-enter(r3)
            X.01P r2 = X.AnonymousClass01P.A0Y     // Catch:{ all -> 0x0f39 }
            if (r2 != 0) goto L_0x0d98
            r1 = 47
            java.lang.String r2 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r1)     // Catch:{ all -> 0x0f39 }
            r1 = 19
            java.lang.String r1 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r1)     // Catch:{ all -> 0x0f39 }
            X.C010708t.A0J(r2, r1)     // Catch:{ all -> 0x0f39 }
            monitor-exit(r3)     // Catch:{ all -> 0x0f39 }
            goto L_0x0e5d
        L_0x0d98:
            monitor-exit(r3)     // Catch:{ all -> 0x0f39 }
            X.01P r7 = X.AnonymousClass01P.A0Y
            com.facebook.analytics.appstatelogger.AppState r4 = r7.A09
            if (r1 != 0) goto L_0x0da1
            java.lang.String r1 = ""
        L_0x0da1:
            java.lang.String r2 = r4.A0N
            boolean r2 = r1.equals(r2)
            if (r2 != 0) goto L_0x0db1
            r4.A0N = r1
            long r2 = android.os.SystemClock.uptimeMillis()
            r4.A08 = r2
        L_0x0db1:
            if (r5 != 0) goto L_0x0db5
            java.lang.String r5 = ""
        L_0x0db5:
            java.lang.String r2 = r4.A0I
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x0dbf
            r4.A0I = r5
        L_0x0dbf:
            if (r8 == 0) goto L_0x0df8
            java.lang.String r5 = "@"
            r3 = 44
            r2 = 95
            java.lang.String r2 = r1.replace(r3, r2)
            java.lang.String r3 = X.AnonymousClass08S.A0J(r5, r2)
            java.lang.StringBuilder r5 = r7.A0K
            monitor-enter(r5)
            java.util.HashSet r2 = r7.A0L     // Catch:{ all -> 0x0df4 }
            boolean r2 = r2.add(r3)     // Catch:{ all -> 0x0df4 }
            if (r2 == 0) goto L_0x0df2
            java.lang.StringBuilder r8 = r7.A0K     // Catch:{ all -> 0x0df4 }
            r8.append(r3)     // Catch:{ all -> 0x0df4 }
            r2 = 44
            r8.append(r2)     // Catch:{ all -> 0x0df4 }
            int r2 = r8.length()     // Catch:{ all -> 0x0df4 }
            int r2 = r2 + -1
            java.lang.String r3 = r8.substring(r6, r2)     // Catch:{ all -> 0x0df4 }
            com.facebook.analytics.appstatelogger.AppState r2 = r7.A09     // Catch:{ all -> 0x0df4 }
            r2.A0K = r3     // Catch:{ all -> 0x0df4 }
        L_0x0df2:
            monitor-exit(r5)     // Catch:{ all -> 0x0df4 }
            goto L_0x0df8
        L_0x0df4:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0df4 }
            goto L_0x0f3b
        L_0x0df8:
            boolean r2 = com.facebook.breakpad.BreakpadManager.isActive()
            if (r2 == 0) goto L_0x0e14
            java.lang.Object[] r3 = new java.lang.Object[r6]
            r2 = 84
            java.lang.String r2 = com.facebook.common.dextricks.turboloader.TurboLoader.Locator.$const$string(r2)
            com.facebook.breakpad.BreakpadManager.setCustomData(r2, r1, r3)
            java.lang.String r3 = r4.A00()
            java.lang.Object[] r2 = new java.lang.Object[r6]
            java.lang.String r1 = "endpoint"
            com.facebook.breakpad.BreakpadManager.setCustomData(r1, r3, r2)
        L_0x0e14:
            X.01P r3 = X.AnonymousClass01P.A0Y
            X.01i r8 = r3.A0B
            android.app.ActivityManager r2 = r3.A06
            r7 = 0
            if (r2 == 0) goto L_0x0e32
            android.app.ActivityManager$MemoryInfo r1 = r3.A05
            r2.getMemoryInfo(r1)
            android.app.ActivityManager$MemoryInfo r2 = r3.A05
            boolean r1 = r2.lowMemory
            if (r1 != 0) goto L_0x0e31
            long r1 = r2.availMem
            r4 = 157286400(0x9600000, double:7.7709807E-316)
            int r3 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r3 >= 0) goto L_0x0e32
        L_0x0e31:
            r7 = 1
        L_0x0e32:
            r8.A07(r7, r6)
            boolean r1 = com.facebook.breakpad.BreakpadManager.isActive()
            if (r1 == 0) goto L_0x0e5d
            X.01P r3 = X.AnonymousClass01P.A0Y
            android.app.ActivityManager r2 = r3.A06
            r7 = 0
            if (r2 == 0) goto L_0x0e52
            android.app.ActivityManager$MemoryInfo r1 = r3.A05
            r2.getMemoryInfo(r1)
            android.app.ActivityManager$MemoryInfo r1 = r3.A05
            long r3 = r1.availMem
            long r1 = r1.threshold
            int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r5 >= 0) goto L_0x0e52
            r7 = 1
        L_0x0e52:
            java.lang.String r3 = java.lang.Boolean.toString(r7)
            java.lang.Object[] r2 = new java.lang.Object[r6]
            java.lang.String r1 = "no_device_memory"
            com.facebook.breakpad.BreakpadManager.setCustomData(r1, r3, r2)
        L_0x0e5d:
            monitor-enter(r0)
            java.util.List r1 = r0.A0P     // Catch:{ all -> 0x0f36 }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ all -> 0x0f36 }
        L_0x0e64:
            boolean r1 = r5.hasNext()     // Catch:{ all -> 0x0f36 }
            if (r1 == 0) goto L_0x0f2b
            java.lang.Object r4 = r5.next()     // Catch:{ all -> 0x0f36 }
            X.EvK r4 = (X.C30386EvK) r4     // Catch:{ all -> 0x0f36 }
            r3 = r26
            X.EvJ r2 = r4.A00()     // Catch:{ all -> 0x0f36 }
            r1 = r25
            boolean r1 = r2.A00(r3, r1)     // Catch:{ all -> 0x0f36 }
            if (r1 == 0) goto L_0x0e84
            r1 = r25
            r4.A01(r3, r1)     // Catch:{ all -> 0x0f36 }
            goto L_0x0e64
        L_0x0e84:
            r3 = 21
            int r2 = X.AnonymousClass1Y3.Ap7     // Catch:{ all -> 0x0f36 }
            X.0UN r1 = r0.A03     // Catch:{ all -> 0x0f36 }
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r3, r2, r1)     // Catch:{ all -> 0x0f36 }
            X.1ZE r2 = (X.AnonymousClass1ZE) r2     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "fb4a_end_of_navigation_predicate_fail"
            X.0bW r2 = r2.A01(r1)     // Catch:{ all -> 0x0f36 }
            com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000 r3 = new com.facebook.analytics.structuredlogger.base.USLEBaseShape0S0000000     // Catch:{ all -> 0x0f36 }
            r1 = 153(0x99, float:2.14E-43)
            r3.<init>(r2, r1)     // Catch:{ all -> 0x0f36 }
            boolean r1 = r3.A0G()     // Catch:{ all -> 0x0f36 }
            if (r1 == 0) goto L_0x0e64
            if (r26 == 0) goto L_0x0e64
            if (r43 == 0) goto L_0x0eac
            java.lang.String r2 = X.AnonymousClass07V.A02(r18)     // Catch:{ all -> 0x0f36 }
            goto L_0x0eae
        L_0x0eac:
            java.lang.String r2 = ""
        L_0x0eae:
            java.lang.String r1 = "activity_class_name"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r25
            java.lang.String r2 = r1.A01     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "dest_bookmark_type"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r25
            java.lang.String r2 = r1.A02     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "dest_last_navigation_tap_point"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r25
            java.lang.String r2 = r1.A03     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "dest_module_name"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r25
            java.lang.Long r1 = r1.A00     // Catch:{ all -> 0x0f36 }
            if (r1 == 0) goto L_0x0f28
            java.lang.String r2 = r1.toString()     // Catch:{ all -> 0x0f36 }
        L_0x0ed8:
            java.lang.String r1 = "dest_surface_link_id"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r26
            java.lang.String r2 = r1.A01     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "source_bookmark_type"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r26
            java.lang.String r2 = r1.A02     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "source_last_navigation_tap_point"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r26
            java.lang.String r2 = r1.A03     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "source_module_name"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r1 = r26
            java.lang.Long r1 = r1.A00     // Catch:{ all -> 0x0f36 }
            if (r1 == 0) goto L_0x0f25
            java.lang.String r2 = r1.toString()     // Catch:{ all -> 0x0f36 }
        L_0x0f02:
            java.lang.String r1 = "source_surface_link_id"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            int r4 = X.AnonymousClass1Y3.APk     // Catch:{ all -> 0x0f36 }
            X.0UN r2 = r0.A03     // Catch:{ all -> 0x0f36 }
            r1 = 11
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r1, r4, r2)     // Catch:{ all -> 0x0f36 }
            X.0eA r1 = (X.C07790eA) r1     // Catch:{ all -> 0x0f36 }
            java.util.List r1 = r1.A07()     // Catch:{ all -> 0x0f36 }
            java.lang.String r2 = r1.toString()     // Catch:{ all -> 0x0f36 }
            java.lang.String r1 = "hsm_path"
            r3.A0D(r1, r2)     // Catch:{ all -> 0x0f36 }
            r3.A06()     // Catch:{ all -> 0x0f36 }
            goto L_0x0e64
        L_0x0f25:
            java.lang.String r2 = ""
            goto L_0x0f02
        L_0x0f28:
            java.lang.String r2 = ""
            goto L_0x0ed8
        L_0x0f2b:
            java.util.List r1 = r0.A0P     // Catch:{ all -> 0x0f36 }
            r1.clear()     // Catch:{ all -> 0x0f36 }
            r1 = r25
            r0.A0C = r1     // Catch:{ all -> 0x0f36 }
            monitor-exit(r0)     // Catch:{ all -> 0x0f36 }
            return
        L_0x0f36:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0f36 }
            goto L_0x0f4a
        L_0x0f39:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0f39 }
        L_0x0f3b:
            throw r0
        L_0x0f3c:
            r1 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0f48 }
            goto L_0x0f47
        L_0x0f3f:
            r1 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0f48 }
            goto L_0x0f47
        L_0x0f42:
            r1 = move-exception
            monitor-exit(r24)     // Catch:{ all -> 0x0f48 }
            goto L_0x0f47
        L_0x0f45:
            r1 = move-exception
            monitor-exit(r28)     // Catch:{ all -> 0x0f48 }
        L_0x0f47:
            throw r1     // Catch:{ all -> 0x0f48 }
        L_0x0f48:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0f48 }
        L_0x0f4a:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass146.A08(X.146, boolean, X.0nI, android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.util.Map, boolean):void");
    }
}
