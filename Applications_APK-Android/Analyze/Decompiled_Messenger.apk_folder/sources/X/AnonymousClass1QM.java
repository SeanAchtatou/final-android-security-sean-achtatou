package X;

/* renamed from: X.1QM  reason: invalid class name */
public abstract class AnonymousClass1QM extends AnonymousClass1QN {
    private final AnonymousClass1MK A00;
    private final AnonymousClass1QJ A01;

    public AnonymousClass1QM(AnonymousClass1Q3 r3, AnonymousClass1QJ r4, AnonymousClass1MK r5) {
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("AbstractProducerToDataSourceAdapter()");
        }
        this.A01 = r4;
        this.A00 = r5;
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("AbstractProducerToDataSourceAdapter()->onRequestStart");
        }
        this.A00.Bm6(this.A01);
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A02("AbstractProducerToDataSourceAdapter()->produceResult");
        }
        r3.ByS(new AnonymousClass1QQ(this), r4);
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
        if (AnonymousClass1NB.A03()) {
            AnonymousClass1NB.A01();
        }
    }

    public static void A01(AnonymousClass1QM r2, Throwable th) {
        if (super.A09(th)) {
            r2.A00.Bm1(r2.A01, th);
        }
    }

    public void A0A(Object obj, int i) {
        boolean A03 = AnonymousClass1QU.A03(i);
        if (super.A08(obj, A03) && A03) {
            this.A00.Bm8(this.A01);
        }
    }

    public boolean AT6() {
        if (!super.AT6()) {
            return false;
        }
        if (super.BEz()) {
            return true;
        }
        this.A00.Blr(this.A01);
        this.A01.A05();
        return true;
    }
}
