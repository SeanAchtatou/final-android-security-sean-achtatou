package X;

/* renamed from: X.0P9  reason: invalid class name */
public final class AnonymousClass0P9 extends C02020Cn {
    public /* bridge */ /* synthetic */ Object A00() {
        return (AnonymousClass0CQ) super.A00();
    }

    public /* bridge */ /* synthetic */ Object A01() {
        return (C04100Rw) super.A01();
    }

    public AnonymousClass0CQ A02() {
        return (AnonymousClass0CQ) super.A00();
    }

    public C04100Rw A03() {
        return (C04100Rw) super.A01();
    }

    public String toString() {
        return AnonymousClass08S.A0P(super.toString(), " returnCode:", Byte.toString(((C04100Rw) super.A01()).A00));
    }

    public AnonymousClass0P9(C01990Ck r1, C04100Rw r2, AnonymousClass0CQ r3) {
        super(r1, r2, r3);
    }
}
