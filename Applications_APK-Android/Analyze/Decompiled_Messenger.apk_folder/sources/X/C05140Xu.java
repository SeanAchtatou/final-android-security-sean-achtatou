package X;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0Xu  reason: invalid class name and case insensitive filesystem */
public final class C05140Xu implements Iterator {
    private final AtomicInteger A00 = new AtomicInteger(0);
    public final /* synthetic */ AnonymousClass0X5 A01;

    public C05140Xu(AnonymousClass0X5 r3) {
        this.A01 = r3;
    }

    public boolean hasNext() {
        if (this.A00.get() < this.A01.size()) {
            return true;
        }
        return false;
    }

    public Object next() {
        int andIncrement = this.A00.getAndIncrement();
        AnonymousClass0X5 r1 = this.A01;
        if (andIncrement < r1.size()) {
            synchronized (r1.A03) {
                try {
                    AnonymousClass0X5 r3 = this.A01;
                    int i = r3.A00;
                    if (andIncrement >= i) {
                        r3.A03[andIncrement] = AnonymousClass0X5.A04;
                        r3.A00 = i + 1;
                        try {
                            AnonymousClass0X5 r2 = this.A01;
                            Object A002 = AnonymousClass1Y4.A00(r2.A02[andIncrement], r2.A01);
                            synchronized (this.A01.A03) {
                                try {
                                    Object[] objArr = this.A01.A03;
                                    objArr[andIncrement] = A002;
                                    objArr.notifyAll();
                                } catch (Throwable th) {
                                    th = th;
                                    throw th;
                                }
                            }
                            return A002;
                        } catch (IllegalArgumentException e) {
                            throw new IllegalArgumentException(String.format("Invalid binding id %d", Integer.valueOf(this.A01.A02[andIncrement])), e);
                        } catch (Throwable th2) {
                            synchronized (this.A01.A03) {
                                try {
                                    Object[] objArr2 = this.A01.A03;
                                    objArr2[andIncrement] = null;
                                    objArr2.notifyAll();
                                    throw th2;
                                } catch (Throwable th3) {
                                    while (true) {
                                        th = th3;
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        while (true) {
                            Object[] objArr3 = this.A01.A03;
                            Object obj = objArr3[andIncrement];
                            if (obj != AnonymousClass0X5.A04) {
                                return obj;
                            }
                            objArr3.wait();
                        }
                    }
                } catch (InterruptedException e2) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e2);
                } catch (Throwable th4) {
                    while (true) {
                        th = th4;
                    }
                }
            }
        } else {
            throw new NoSuchElementException();
        }
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
