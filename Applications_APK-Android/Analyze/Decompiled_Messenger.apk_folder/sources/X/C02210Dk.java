package X;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.RegularImmutableList;

/* renamed from: X.0Dk  reason: invalid class name and case insensitive filesystem */
public final class C02210Dk {
    public static ImmutableList A00(ImmutableList immutableList) {
        if (immutableList == null) {
            return RegularImmutableList.A02;
        }
        return immutableList;
    }
}
