package X;

import com.facebook.orca.threadview.ThreadViewFragment;

/* renamed from: X.13F  reason: invalid class name */
public final class AnonymousClass13F implements AnonymousClass13G {
    public final /* synthetic */ C32301lX A00;

    public String B53() {
        return "thread";
    }

    public AnonymousClass13F(C32301lX r1) {
        this.A00 = r1;
    }

    public void AXh() {
        C13150qn A002 = C32301lX.A00(this.A00);
        if (A002.A04 == null) {
            ThreadViewFragment A05 = ThreadViewFragment.A05(null);
            C16290wo r1 = A002.A05;
            r1.A08(2131301002, A05);
            r1.A0H(A05);
        }
        A002.A01();
        this.A00.A03.A07 = "preloaded";
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x011d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x015e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AXi() {
        /*
            r28 = this;
            r2 = r28
            X.1lX r0 = r2.A00
            r1 = 2131300895(0x7f09121f, float:1.8219833E38)
            X.0qW r0 = r0.A01
            androidx.fragment.app.Fragment r4 = r0.A0O(r1)
            X.1lX r0 = r2.A00
            r1 = 2131301002(0x7f09128a, float:1.822005E38)
            X.0qW r0 = r0.A01
            androidx.fragment.app.Fragment r0 = r0.A0O(r1)
            if (r4 == 0) goto L_0x024b
            if (r0 == 0) goto L_0x024b
            android.view.View r3 = r4.A0I
            if (r3 == 0) goto L_0x024b
            boolean r1 = r0.A0b
            if (r1 == 0) goto L_0x024b
            int r5 = r3.getWidth()
            android.view.View r1 = r4.A0I
            int r4 = r1.getHeight()
            com.facebook.orca.threadview.ThreadViewFragment r0 = (com.facebook.orca.threadview.ThreadViewFragment) r0
            r0.A01 = r5
            r0.A00 = r4
            com.facebook.orca.threadview.ThreadViewMessagesFragment r1 = r0.A0c
            if (r1 == 0) goto L_0x01b8
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r0.A0H
            if (r3 != 0) goto L_0x01b8
            android.view.View r3 = r0.A05
            if (r3 == 0) goto L_0x01b8
            if (r5 <= 0) goto L_0x01b8
            if (r4 <= 0) goto L_0x01b8
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r1.A0Y
            if (r3 != 0) goto L_0x019b
            r3 = 0
            r1.A1c = r3
            com.facebook.widget.listview.EmptyListViewItem r4 = r1.A1F
            r3 = 8
            r4.setVisibility(r3)
            r5 = 46
            int r4 = X.AnonymousClass1Y3.AmP
            X.0UN r3 = r1.A0M
            java.lang.Object r7 = X.AnonymousClass1XX.A02(r5, r4, r3)
            X.5n7 r7 = (X.AnonymousClass5n7) r7
            X.1bq r6 = r7.A00
            X.0l8 r5 = X.C10950l8.A05
            X.0ki r4 = X.C10700ki.ALL
            com.google.common.collect.RegularImmutableSet r3 = com.google.common.collect.RegularImmutableSet.A05
            com.facebook.messaging.model.threads.ThreadsCollection r8 = r6.A09(r5, r4, r3)
            com.google.common.collect.ImmutableList r3 = r8.A00
            boolean r3 = r3.isEmpty()
            r11 = 0
            if (r3 != 0) goto L_0x016a
            com.google.common.collect.ImmutableList r9 = r8.A00
            int r4 = r9.size()
            r3 = 5
            int r6 = java.lang.Math.min(r3, r4)
            r5 = 0
            r4 = 0
        L_0x0080:
            if (r4 >= r6) goto L_0x0091
            java.lang.Object r10 = r9.get(r4)
            com.facebook.messaging.model.threads.ThreadSummary r10 = (com.facebook.messaging.model.threads.ThreadSummary) r10
            boolean r3 = r10.A0B()
            if (r3 != 0) goto L_0x0098
            int r4 = r4 + 1
            goto L_0x0080
        L_0x0091:
            r8.A03(r5)
            com.facebook.messaging.model.threads.ThreadSummary r10 = r8.A03(r5)
        L_0x0098:
            X.1bq r4 = r7.A00
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r10.A0S
            com.facebook.messaging.model.messages.MessagesCollection r6 = r4.A07(r3)
            if (r6 == 0) goto L_0x00a8
            boolean r3 = r6.A08()
            if (r3 == 0) goto L_0x00d6
        L_0x00a8:
            com.google.common.collect.ImmutableList r3 = r8.A00
            X.1Xv r8 = r3.iterator()
        L_0x00ae:
            boolean r3 = r8.hasNext()
            if (r3 == 0) goto L_0x00d6
            java.lang.Object r5 = r8.next()
            com.facebook.messaging.model.threads.ThreadSummary r5 = (com.facebook.messaging.model.threads.ThreadSummary) r5
            com.facebook.messaging.model.threadkey.ThreadKey r4 = r10.A0S
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r5.A0S
            boolean r3 = com.google.common.base.Objects.equal(r4, r3)
            if (r3 != 0) goto L_0x00ae
            X.1bq r4 = r7.A00
            com.facebook.messaging.model.threadkey.ThreadKey r3 = r5.A0S
            com.facebook.messaging.model.messages.MessagesCollection r4 = r4.A07(r3)
            if (r6 == 0) goto L_0x00ae
            boolean r3 = r6.A08()
            if (r3 != 0) goto L_0x00ae
            r10 = r5
            r6 = r4
        L_0x00d6:
            if (r6 == 0) goto L_0x016a
            boolean r3 = r6.A08()
            if (r3 != 0) goto L_0x016a
            X.2Me r3 = r7.A01
            com.google.common.collect.ImmutableList r4 = r6.A01
            java.util.Set r11 = java.util.Collections.EMPTY_SET
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            com.google.common.collect.ImmutableList r15 = com.google.common.collect.RegularImmutableList.A02
            java.util.Set r16 = java.util.Collections.EMPTY_SET
            java.util.Set r20 = java.util.Collections.EMPTY_SET
            java.util.Set r21 = java.util.Collections.EMPTY_SET
            r17 = 0
            r22 = 0
            r23 = 0
            r27 = 0
            r12 = 0
            r13 = 1
            r18 = 0
            r24 = 0
            r26 = 0
            X.3Wb r8 = new X.3Wb
            java.util.List r9 = X.C04300To.A05(r4)
            r19 = 1
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r26, r27)
            java.util.List r3 = X.C45462Me.A02(r3, r8)
            java.util.List r11 = X.C04300To.A05(r3)
            com.google.common.collect.ImmutableList$Builder r8 = com.google.common.collect.ImmutableList.builder()
            java.util.Iterator r10 = r11.iterator()
            r9 = 0
        L_0x011d:
            boolean r3 = r10.hasNext()
            if (r3 == 0) goto L_0x0164
            java.lang.Object r7 = r10.next()
            X.3Wm r7 = (X.C69293Wm) r7
            boolean r3 = r7 instanceof X.C43332Dy
            r6 = 1
            if (r3 != 0) goto L_0x0153
            boolean r3 = r7 instanceof X.C69353Ws
            if (r3 != 0) goto L_0x0153
            boolean r3 = r7 instanceof X.AnonymousClass2DD
            if (r3 != 0) goto L_0x0153
            boolean r3 = r7 instanceof X.C69283Wl
            if (r3 == 0) goto L_0x015c
            r5 = r7
            X.3Wl r5 = (X.C69283Wl) r5
            com.facebook.messaging.model.messages.Message r4 = r5.A04
            boolean r3 = X.C28841fS.A0J(r4)
            if (r3 != 0) goto L_0x014d
            java.lang.String r3 = r4.A0z
            boolean r3 = X.C72043dZ.A03(r3)
            if (r3 == 0) goto L_0x015c
        L_0x014d:
            X.3Wo r4 = r5.A05
            monitor-enter(r4)
            r4.A04 = r13     // Catch:{ all -> 0x0161 }
            goto L_0x0157
        L_0x0153:
            r8.add(r7)
            goto L_0x015b
        L_0x0157:
            monitor-exit(r4)
            r8.add(r7)
        L_0x015b:
            r6 = 0
        L_0x015c:
            if (r6 == 0) goto L_0x011d
            int r9 = r9 + 1
            goto L_0x011d
        L_0x0161:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0164:
            if (r9 == 0) goto L_0x016a
            com.google.common.collect.ImmutableList r11 = r8.build()
        L_0x016a:
            if (r11 == 0) goto L_0x0172
            boolean r3 = r11.isEmpty()
            if (r3 == 0) goto L_0x0174
        L_0x0172:
            com.google.common.collect.ImmutableList r11 = com.google.common.collect.RegularImmutableList.A02
        L_0x0174:
            java.util.List r4 = X.C04300To.A05(r11)
            java.lang.String r3 = "ThreadViewMessagesFragment.preRenderMessageList"
            X.C27041cY.A01(r3)
            X.3EO r5 = r1.A17
            com.facebook.messaging.model.threadkey.ThreadKey r6 = r1.A0Y
            X.2sU r7 = r1.A0O
            com.google.common.collect.ImmutableList r8 = com.google.common.collect.ImmutableList.copyOf(r4)
            r9 = 0
            X.2JL r3 = r1.A0y
            r10 = 0
            if (r3 != 0) goto L_0x018e
            r10 = 1
        L_0x018e:
            r5.A07(r6, r7, r8, r9, r10)
            X.C27041cY.A00()
            com.facebook.widget.listview.EmptyListViewItem r3 = r1.A1E
            r1 = 8
            r3.setVisibility(r1)
        L_0x019b:
            android.view.View r5 = r0.A05
            int r1 = r0.A01
            r4 = 1073741824(0x40000000, float:2.0)
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r4)
            int r1 = r0.A00
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r4)
            r5.measure(r3, r1)
            android.view.View r4 = r0.A05
            int r3 = r0.A01
            int r1 = r0.A00
            r0 = 0
            r4.layout(r0, r0, r3, r1)
        L_0x01b8:
            X.1lX r2 = r2.A00
            X.0fv r1 = r2.A03
            java.lang.String r0 = "prerendered"
            r1.A07 = r0
            X.1mk r5 = r2.A06
            r4 = 2
            X.0p4 r8 = new X.0p4
            int r1 = X.AnonymousClass1Y3.A6S
            X.0UN r0 = r5.A00
            r3 = 0
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r3, r1, r0)
            android.content.Context r0 = (android.content.Context) r0
            r8.<init>(r0)
            r1 = 7
            r0 = 1048(0x418, float:1.469E-42)
            java.lang.String r9 = X.AnonymousClass24B.$const$string(r0)
            java.lang.String r10 = "listener"
            java.lang.String r11 = "message"
            java.lang.String r12 = "needsUserRequestToLoad"
            java.lang.String r13 = "onLoadClickListener"
            java.lang.String r14 = "videoAttachmentData"
            r0 = 780(0x30c, float:1.093E-42)
            java.lang.String r15 = X.C99084oO.$const$string(r0)
            java.lang.String[] r7 = new java.lang.String[]{r9, r10, r11, r12, r13, r14, r15}
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>(r1)
            X.6aH r6 = new X.6aH
            android.content.Context r0 = r8.A09
            r6.<init>(r0)
            X.0zR r0 = r8.A04
            if (r0 == 0) goto L_0x0202
            java.lang.String r0 = r0.A06
            r6.A07 = r0
        L_0x0202:
            r2.clear()
            r1 = 0
            r6.A01 = r1
            r2.set(r3)
            r6.A06 = r1
            r0 = 1
            r2.set(r0)
            r6.A04 = r1
            r2.set(r4)
            r6.A09 = r3
            r0 = 3
            r2.set(r0)
            r6.A00 = r1
            r0 = 4
            r2.set(r0)
            r6.A03 = r1
            r0 = 5
            r2.set(r0)
            r6.A05 = r1
            r0 = 6
            r2.set(r0)
            r0 = 7
            X.AnonymousClass11F.A0C(r0, r2, r7)
            r2 = 0
        L_0x0233:
            if (r2 >= r4) goto L_0x024b
            int r1 = X.AnonymousClass1Y3.A6S
            X.0UN r0 = r5.A00
            java.lang.Object r1 = X.AnonymousClass1XX.A02(r3, r1, r0)
            android.content.Context r1 = (android.content.Context) r1
            X.0vh r0 = X.C15040ud.A00(r1, r6)
            if (r0 == 0) goto L_0x0248
            r0.BKc(r1, r6)
        L_0x0248:
            int r2 = r2 + 1
            goto L_0x0233
        L_0x024b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13F.AXi():void");
    }
}
