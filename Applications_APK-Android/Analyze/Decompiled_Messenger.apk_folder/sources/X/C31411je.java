package X;

import android.view.View;

/* renamed from: X.1je  reason: invalid class name and case insensitive filesystem */
public final class C31411je implements View.OnFocusChangeListener {
    public AnonymousClass10N A00;

    public void onFocusChange(View view, boolean z) {
        AnonymousClass10N r2 = this.A00;
        if (r2 != null) {
            if (C33181nA.A02 == null) {
                C33181nA.A02 = new C124875uA();
            }
            C124875uA r1 = C33181nA.A02;
            r1.A00 = view;
            r1.A01 = z;
            r2.A00.Alq().AXa(r2, r1);
            C33181nA.A02.A00 = null;
        }
    }
}
