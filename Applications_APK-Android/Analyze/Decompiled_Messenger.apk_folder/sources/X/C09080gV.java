package X;

import android.os.Bundle;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.CallerContextable;
import com.facebook.fbservice.ops.BlueServiceOperationFactory;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadCriteria;
import com.facebook.messaging.service.model.FetchThreadParams;

/* renamed from: X.0gV  reason: invalid class name and case insensitive filesystem */
public final class C09080gV implements CallerContextable {
    public static final String __redex_internal_original_name = "com.facebook.messaging.startup.ThreadsPrefetcher";
    public AnonymousClass0UN A00;

    public static final C09080gV A00(AnonymousClass1XY r1) {
        return new C09080gV(r1);
    }

    public void A01(ThreadKey threadKey, String str) {
        if (((AnonymousClass1YI) AnonymousClass1XX.A02(1, AnonymousClass1Y3.AcD, this.A00)).AbO(AnonymousClass1Y3.A1x, false)) {
            C60292wu r2 = new C60292wu();
            r2.A03 = ThreadCriteria.A00(threadKey);
            r2.A01 = C09510hU.STALE_DATA_OKAY;
            r2.A00 = 20;
            r2.A06 = true;
            FetchThreadParams fetchThreadParams = new FetchThreadParams(r2);
            Bundle bundle = new Bundle();
            bundle.putParcelable("fetchThreadParams", fetchThreadParams);
            AnonymousClass0lL A01 = AnonymousClass07W.A01((BlueServiceOperationFactory) AnonymousClass1XX.A03(AnonymousClass1Y3.B1W, this.A00), "fetch_thread", bundle, CallerContext.A07(getClass(), str), -1597587471);
            A01.C81(true);
            A01.CHd();
        }
    }

    private C09080gV(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
