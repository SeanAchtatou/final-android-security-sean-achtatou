package X;

import android.content.Context;
import com.facebook.litho.annotations.Comparable;

/* renamed from: X.1JX  reason: invalid class name */
public final class AnonymousClass1JX extends C17770zR {
    @Comparable(type = 3)
    public long A00;
    @Comparable(type = 3)
    public long A01;
    public AnonymousClass0UN A02;
    public AnonymousClass10N A03;
    @Comparable(type = 13)
    public String A04;
    @Comparable(type = 13)
    public String A05;

    public int A0M() {
        return 3;
    }

    public AnonymousClass1JX(Context context) {
        super("CountDownComponent");
        this.A02 = new AnonymousClass0UN(1, AnonymousClass1XX.get(context));
    }
}
