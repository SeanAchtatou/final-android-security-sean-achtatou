package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.facebook.messaging.appupdate2.AppUpdateNTActivity;
import com.facebook.selfupdate2.SelfUpdateActivity;

/* renamed from: X.0h2  reason: invalid class name */
public final class AnonymousClass0h2 implements C186714q {
    private final AnonymousClass14P A00;

    public boolean CE5() {
        return true;
    }

    public static final AnonymousClass0h2 A00(AnonymousClass1XY r1) {
        return new AnonymousClass0h2(r1);
    }

    public Integer AeZ() {
        return AnonymousClass07B.A0C;
    }

    public Intent AqX(Activity activity) {
        Intent intent = new Intent(activity, AppUpdateNTActivity.class);
        intent.setFlags(268468224);
        return intent;
    }

    public boolean BDw(Context context) {
        AnonymousClass14P r7 = this.A00;
        long At0 = r7.A02.At0(566364452488856L);
        if (At0 <= -1 || !AnonymousClass14P.A01(r7, At0)) {
            return false;
        }
        if (r7.A00.now() - r7.A03.At2(AnonymousClass5Z4.A00, -1) <= At0) {
            return false;
        }
        r7.A01.A00.CH1(AnonymousClass14Q.A01);
        r7.A01.A00.AOI(AnonymousClass14Q.A01, "eligibility_determined");
        return true;
    }

    private AnonymousClass0h2(AnonymousClass1XY r2) {
        this.A00 = new AnonymousClass14P(r2);
    }

    public boolean CEE(Activity activity) {
        Class<?> cls = activity.getClass();
        if (!(cls == AppUpdateNTActivity.class || cls == SelfUpdateActivity.class)) {
            String simpleName = cls.getSimpleName();
            if (simpleName.equals("SecureIntentHandlerActivity") || simpleName.equals("MobileConfigPreferenceActivity")) {
                return false;
            }
            return true;
        }
        return false;
    }
}
