package X;

import android.database.Cursor;
import com.google.common.base.Preconditions;

/* renamed from: X.1fJ  reason: invalid class name and case insensitive filesystem */
public abstract class C28751fJ implements C008607r {
    public Object A00 = null;
    public boolean A01 = true;
    public final Cursor A02;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v3, resolved type: com.facebook.user.model.UserKey} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v5, resolved type: java.lang.Integer} */
    /* JADX WARN: Type inference failed for: r14v2 */
    /* JADX WARN: Type inference failed for: r14v7 */
    /* JADX WARN: Type inference failed for: r14v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A00(android.database.Cursor r24) {
        /*
            r23 = this;
            r1 = r23
            boolean r0 = r1 instanceof X.C181410j
            r2 = r24
            if (r0 != 0) goto L_0x03af
            boolean r0 = r1 instanceof X.C181610l
            if (r0 != 0) goto L_0x03a8
            boolean r0 = r1 instanceof X.C28911fZ
            if (r0 != 0) goto L_0x0253
            boolean r0 = r1 instanceof X.C28741fI
            if (r0 != 0) goto L_0x00d6
            r6 = r1
            X.13X r6 = (X.AnonymousClass13X) r6
            android.database.Cursor r1 = r6.A02
            int r0 = r6.A09
            java.lang.String r0 = r1.getString(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r4 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)
            int r0 = r6.A08
            java.lang.String r0 = r2.getString(r0)
            com.facebook.graphql.enums.GraphQLLightweightEventType r8 = com.facebook.graphql.enums.GraphQLLightweightEventType.A00(r0)
            com.facebook.graphql.enums.GraphQLLightweightEventType r0 = com.facebook.graphql.enums.GraphQLLightweightEventType.A08
            if (r8 != r0) goto L_0x0033
            com.facebook.graphql.enums.GraphQLLightweightEventType r8 = com.facebook.graphql.enums.GraphQLLightweightEventType.A03
        L_0x0033:
            X.38l r3 = new X.38l
            r3.<init>()
            int r0 = r6.A03
            java.lang.String r1 = r2.getString(r0)
            java.lang.Class<java.util.Map> r0 = java.util.Map.class
            java.lang.Object r9 = r3.A07(r1, r0)
            java.util.Map r9 = (java.util.Map) r9
            com.google.common.collect.ImmutableMap$Builder r5 = com.google.common.collect.ImmutableMap.builder()
            if (r9 == 0) goto L_0x007c
            java.util.Set r0 = r9.keySet()
            java.util.Iterator r7 = r0.iterator()
        L_0x0054:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x007c
            java.lang.Object r1 = r7.next()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r9.get(r1)
            if (r0 == 0) goto L_0x0054
            com.facebook.user.model.UserKey r3 = com.facebook.user.model.UserKey.A02(r1)
            java.lang.Object r1 = r9.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            com.facebook.graphql.enums.GraphQLLightweightEventGuestStatus r0 = com.facebook.graphql.enums.GraphQLLightweightEventGuestStatus.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r1, r0)
            com.facebook.graphql.enums.GraphQLLightweightEventGuestStatus r0 = (com.facebook.graphql.enums.GraphQLLightweightEventGuestStatus) r0
            r5.put(r3, r0)
            goto L_0x0054
        L_0x007c:
            X.2lN r3 = new X.2lN
            r3.<init>()
            int r0 = r6.A04
            java.lang.String r0 = r2.getString(r0)
            r3.A06 = r0
            r3.A02 = r8
            int r0 = r6.A06
            long r0 = r2.getLong(r0)
            r3.A01 = r0
            int r0 = r6.A02
            long r0 = r2.getLong(r0)
            r3.A00 = r0
            int r0 = r6.A07
            java.lang.String r0 = r2.getString(r0)
            r3.A09 = r0
            int r0 = r6.A01
            int r1 = r2.getInt(r0)
            r0 = 1
            if (r1 == r0) goto L_0x00ad
            r0 = 0
        L_0x00ad:
            r3.A0A = r0
            int r0 = r6.A05
            java.lang.String r0 = r2.getString(r0)
            r3.A07 = r0
            r3.A03 = r4
            int r0 = r6.A00
            java.lang.String r0 = r2.getString(r0)
            com.facebook.user.model.UserKey r0 = com.facebook.user.model.UserKey.A02(r0)
            r3.A04 = r0
            com.google.common.collect.ImmutableMap r0 = r5.build()
            r3.A05 = r0
            X.3AG r1 = new X.3AG
            com.facebook.messaging.model.threads.ThreadEventReminder r0 = new com.facebook.messaging.model.threads.ThreadEventReminder
            r0.<init>(r3)
            r1.<init>(r4, r0)
            return r1
        L_0x00d6:
            r2 = r1
            X.1fI r2 = (X.C28741fI) r2
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0F
            java.lang.String r0 = r1.getString(r0)
            com.facebook.messaging.model.threadkey.ThreadKey r9 = com.facebook.messaging.model.threadkey.ThreadKey.A06(r0)
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0H
            java.lang.String r0 = r1.getString(r0)
            com.facebook.user.model.UserKey r10 = com.facebook.user.model.UserKey.A02(r0)
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0G
            java.lang.String r0 = r1.getString(r0)
            java.lang.Integer r3 = X.C181710m.A00(r0)
            java.lang.Integer r0 = X.AnonymousClass07B.A0C
            r14 = 0
            if (r3 != r0) goto L_0x0132
            X.115 r8 = new X.115
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0D
            long r11 = r1.getLong(r0)
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A02
            java.lang.String r0 = r1.getString(r0)
            com.facebook.user.model.UserKey r13 = com.facebook.user.model.UserKey.A02(r0)
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0C
            boolean r0 = r1.isNull(r0)
            if (r0 != 0) goto L_0x012e
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0C
            int r0 = r1.getInt(r0)
            java.lang.Integer r14 = java.lang.Integer.valueOf(r0)
        L_0x012e:
            r8.<init>(r9, r10, r11, r13, r14)
            return r8
        L_0x0132:
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0A
            java.lang.String r6 = r1.getString(r0)
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0B
            java.lang.String r19 = r1.getString(r0)
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A0E
            java.lang.String r20 = r1.getString(r0)
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A04
            int r0 = r1.getInt(r0)
            r5 = 0
            r4 = 1
            r21 = 0
            if (r0 == 0) goto L_0x015a
            r21 = 1
        L_0x015a:
            android.database.Cursor r1 = r2.A02
            int r0 = r2.A09
            java.lang.String r0 = r1.getString(r0)
            if (r0 != 0) goto L_0x0167
            X.1aC r22 = X.C25661aC.UNSET
            goto L_0x016b
        L_0x0167:
            X.1aC r22 = X.C25661aC.valueOf(r0)
        L_0x016b:
            com.facebook.messaging.model.messages.ParticipantInfo r15 = new com.facebook.messaging.model.messages.ParticipantInfo     // Catch:{ RuntimeException -> 0x0210 }
            r18 = 0
            r16 = r10
            r17 = r6
            r15.<init>(r16, r17, r18, r19, r20, r21, r22)     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            boolean r0 = r1.isNull(r0)     // Catch:{ RuntimeException -> 0x0210 }
            if (r0 != 0) goto L_0x018c
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ RuntimeException -> 0x0210 }
            com.facebook.user.model.UserKey r14 = com.facebook.user.model.UserKey.A02(r0)     // Catch:{ RuntimeException -> 0x0210 }
        L_0x018c:
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A0C     // Catch:{ RuntimeException -> 0x0210 }
            boolean r0 = r1.isNull(r0)     // Catch:{ RuntimeException -> 0x0210 }
            if (r0 == 0) goto L_0x0198
            r8 = -1
            goto L_0x01a0
        L_0x0198:
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A0C     // Catch:{ RuntimeException -> 0x0210 }
            int r8 = r1.getInt(r0)     // Catch:{ RuntimeException -> 0x0210 }
        L_0x01a0:
            X.1fR r7 = new X.1fR     // Catch:{ RuntimeException -> 0x0210 }
            r7.<init>()     // Catch:{ RuntimeException -> 0x0210 }
            r7.A04 = r15     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A06     // Catch:{ RuntimeException -> 0x0210 }
            long r0 = r1.getLong(r0)     // Catch:{ RuntimeException -> 0x0210 }
            r7.A01 = r0     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A07     // Catch:{ RuntimeException -> 0x0210 }
            long r0 = r1.getLong(r0)     // Catch:{ RuntimeException -> 0x0210 }
            r7.A02 = r0     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A08     // Catch:{ RuntimeException -> 0x0210 }
            long r0 = r1.getLong(r0)     // Catch:{ RuntimeException -> 0x0210 }
            r7.A03 = r0     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A05     // Catch:{ RuntimeException -> 0x0210 }
            java.lang.String r1 = r1.getString(r0)     // Catch:{ RuntimeException -> 0x0210 }
            r7.A07 = r1     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A03     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r1.getInt(r0)     // Catch:{ RuntimeException -> 0x0210 }
            r1 = 0
            if (r0 != r4) goto L_0x01db
            r1 = 1
        L_0x01db:
            r7.A09 = r1     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A00     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r1.getInt(r0)     // Catch:{ RuntimeException -> 0x0210 }
            X.114 r1 = X.AnonymousClass114.A00(r0)     // Catch:{ RuntimeException -> 0x0210 }
            r7.A05 = r1     // Catch:{ RuntimeException -> 0x0210 }
            android.database.Cursor r1 = r2.A02     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r2.A01     // Catch:{ RuntimeException -> 0x0210 }
            int r0 = r1.getInt(r0)     // Catch:{ RuntimeException -> 0x0210 }
            if (r0 != r4) goto L_0x01f6
            r5 = 1
        L_0x01f6:
            r7.A08 = r5     // Catch:{ RuntimeException -> 0x0210 }
            r7.A06 = r14     // Catch:{ RuntimeException -> 0x0210 }
            if (r8 == 0) goto L_0x0202
            r1 = 1
            r0 = 1
            if (r8 == r1) goto L_0x0203
            r0 = -1
            goto L_0x0203
        L_0x0202:
            r0 = 0
        L_0x0203:
            r7.A00 = r0     // Catch:{ RuntimeException -> 0x0210 }
            X.115 r8 = new X.115     // Catch:{ RuntimeException -> 0x0210 }
            com.facebook.messaging.model.threads.ThreadParticipant r0 = new com.facebook.messaging.model.threads.ThreadParticipant     // Catch:{ RuntimeException -> 0x0210 }
            r0.<init>(r7)     // Catch:{ RuntimeException -> 0x0210 }
            r8.<init>(r9, r0, r3)     // Catch:{ RuntimeException -> 0x0210 }
            return r8
        L_0x0210:
            r0 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = r0.getMessage()
            r1.append(r0)
            java.lang.String r0 = ", thread key: "
            r1.append(r0)
            r1.append(r9)
            java.lang.String r0 = ", type: "
            r1.append(r0)
            if (r3 == 0) goto L_0x0250
            int r0 = r3.intValue()
            switch(r0) {
                case 1: goto L_0x024a;
                case 2: goto L_0x024d;
                default: goto L_0x0235;
            }
        L_0x0235:
            java.lang.String r0 = "PARTICIPANT"
        L_0x0237:
            r1.append(r0)
            java.lang.String r0 = ", name: "
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            throw r2
        L_0x024a:
            java.lang.String r0 = "BOT"
            goto L_0x0237
        L_0x024d:
            java.lang.String r0 = "REQUEST"
            goto L_0x0237
        L_0x0250:
            java.lang.String r0 = "null"
            goto L_0x0237
        L_0x0253:
            r3 = r1
            X.1fZ r3 = (X.C28911fZ) r3
            X.12U r8 = com.facebook.messaging.customthreads.model.ThreadThemeInfo.A00()
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A00
            long r0 = r1.getLong(r0)
            r8.A01 = r0
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A09
            java.lang.String r1 = r1.getString(r0)
            com.facebook.graphql.enums.GraphQLMessengerThreadViewMode r0 = com.facebook.graphql.enums.GraphQLMessengerThreadViewMode.UNSET_OR_UNRECOGNIZED_ENUM_VALUE
            java.lang.Enum r0 = com.facebook.graphql.enums.EnumHelper.A00(r1, r0)
            com.facebook.graphql.enums.GraphQLMessengerThreadViewMode r0 = (com.facebook.graphql.enums.GraphQLMessengerThreadViewMode) r0
            r8.A05 = r0
            java.lang.String r1 = "threadViewMode"
            X.C28931fb.A06(r0, r1)
            java.util.Set r0 = r8.A09
            r0.add(r1)
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A01
            java.lang.String r1 = r1.getString(r0)
            boolean r0 = X.C06850cB.A0B(r1)
            if (r0 != 0) goto L_0x0395
            android.net.Uri r0 = android.net.Uri.parse(r1)
        L_0x0292:
            r8.A02 = r0
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A08
            java.lang.String r1 = r1.getString(r0)
            boolean r0 = X.C06850cB.A0B(r1)
            if (r0 != 0) goto L_0x0392
            android.net.Uri r0 = android.net.Uri.parse(r1)
        L_0x02a6:
            r8.A04 = r0
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A02
            java.lang.String r1 = r1.getString(r0)
            boolean r0 = X.C06850cB.A0B(r1)
            if (r0 != 0) goto L_0x038f
            android.net.Uri r0 = android.net.Uri.parse(r1)
        L_0x02ba:
            r8.A03 = r0
            X.12W r2 = r3.A0A
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A03
            java.lang.String r0 = r1.getString(r0)
            com.google.common.collect.ImmutableList r10 = r2.A02(r0)
            X.12W r2 = r3.A0A
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A04
            java.lang.String r0 = r1.getString(r0)
            com.google.common.collect.ImmutableList r9 = r2.A02(r0)
            X.12W r2 = r3.A0A
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A05
            java.lang.String r0 = r1.getString(r0)
            com.google.common.collect.ImmutableList r7 = r2.A02(r0)
            X.12W r2 = r3.A0A
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A07
            java.lang.String r0 = r1.getString(r0)
            com.google.common.collect.ImmutableList r6 = r2.A02(r0)
            X.12W r2 = r3.A0A
            android.database.Cursor r1 = r3.A02
            int r0 = r3.A06
            java.lang.String r0 = r1.getString(r0)
            com.google.common.collect.ImmutableList r5 = r2.A02(r0)
            int r4 = r10.size()
            int r0 = r9.size()
            if (r0 != r4) goto L_0x03a3
            int r0 = r7.size()
            if (r0 != r4) goto L_0x03a3
            int r0 = r6.size()
            if (r0 != r4) goto L_0x03a3
            int r0 = r5.size()
            if (r0 != r4) goto L_0x03a3
            com.google.common.collect.ImmutableList$Builder r3 = com.google.common.collect.ImmutableList.builder()
            r2 = 0
        L_0x0323:
            if (r2 >= r4) goto L_0x0398
            java.lang.Object r15 = r10.get(r2)
            java.lang.String r15 = (java.lang.String) r15
            java.lang.Object r14 = r9.get(r2)
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r13 = r7.get(r2)
            java.lang.String r13 = (java.lang.String) r13
            java.lang.Object r12 = r6.get(r2)
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r11 = r5.get(r2)
            java.lang.String r11 = (java.lang.String) r11
            X.1vq r1 = new X.1vq
            r1.<init>()
            r1.A03 = r15
            java.lang.String r0 = "id"
            X.C28931fb.A06(r15, r0)
            r1.A04 = r14
            r0 = 40
            java.lang.String r0 = X.C22298Ase.$const$string(r0)
            X.C28931fb.A06(r14, r0)
            boolean r0 = X.C06850cB.A0B(r13)
            if (r0 != 0) goto L_0x038d
            android.net.Uri r0 = android.net.Uri.parse(r13)
        L_0x0364:
            r1.A00 = r0
            boolean r0 = X.C06850cB.A0B(r12)
            if (r0 != 0) goto L_0x038b
            android.net.Uri r0 = android.net.Uri.parse(r12)
        L_0x0370:
            r1.A02 = r0
            boolean r0 = X.C06850cB.A0B(r11)
            if (r0 != 0) goto L_0x0389
            android.net.Uri r0 = android.net.Uri.parse(r11)
        L_0x037c:
            r1.A01 = r0
            com.facebook.messaging.customthreads.model.ThreadThemeReactionAssetInfo r0 = new com.facebook.messaging.customthreads.model.ThreadThemeReactionAssetInfo
            r0.<init>(r1)
            r3.add(r0)
            int r2 = r2 + 1
            goto L_0x0323
        L_0x0389:
            r0 = 0
            goto L_0x037c
        L_0x038b:
            r0 = 0
            goto L_0x0370
        L_0x038d:
            r0 = 0
            goto L_0x0364
        L_0x038f:
            r0 = 0
            goto L_0x02ba
        L_0x0392:
            r0 = 0
            goto L_0x02a6
        L_0x0395:
            r0 = 0
            goto L_0x0292
        L_0x0398:
            com.google.common.collect.ImmutableList r1 = r3.build()
            r8.A07 = r1
            java.lang.String r0 = "reactionAssets"
            X.C28931fb.A06(r1, r0)
        L_0x03a3:
            com.facebook.messaging.customthreads.model.ThreadThemeInfo r0 = r8.A00()
            return r0
        L_0x03a8:
            X.0W6 r0 = X.AnonymousClass377.A05
            java.lang.String r0 = r0.A05(r2)
            return r0
        L_0x03af:
            r5 = r1
            X.10j r5 = (X.C181410j) r5
            int r1 = r5.A00
            r0 = -1
            if (r1 != r0) goto L_0x03cf
            java.lang.String r0 = "fbid"
            int r0 = r2.getColumnIndexOrThrow(r0)
            r5.A00 = r0
            java.lang.String r0 = "is_mobile_pushable"
            int r0 = r2.getColumnIndexOrThrow(r0)
            r5.A01 = r0
            java.lang.String r0 = "is_messenger_user"
            int r0 = r2.getColumnIndexOrThrow(r0)
            r5.A02 = r0
        L_0x03cf:
            com.facebook.user.model.UserKey r4 = new com.facebook.user.model.UserKey
            X.1aB r1 = X.C25651aB.A03
            int r0 = r5.A00
            java.lang.String r0 = r2.getString(r0)
            r4.<init>(r1, r0)
            int r0 = r5.A01
            int r0 = r2.getInt(r0)
            com.facebook.common.util.TriState r3 = com.facebook.common.util.TriState.fromDbValue(r0)
            int r0 = r5.A02
            java.lang.String r0 = r2.getString(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            boolean r2 = r0.booleanValue()
            X.3AQ r1 = new X.3AQ
            r0 = 0
            boolean r0 = r3.asBoolean(r0)
            r1.<init>(r4, r0, r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28751fJ.A00(android.database.Cursor):java.lang.Object");
    }

    public void close() {
        this.A02.close();
    }

    public boolean hasNext() {
        Object obj;
        if (this.A01) {
            this.A01 = false;
            if (this.A02.moveToNext()) {
                obj = A00(this.A02);
            } else {
                obj = null;
            }
            this.A00 = obj;
        }
        if (this.A00 != null) {
            return true;
        }
        return false;
    }

    public Object next() {
        Object obj;
        if (this.A01) {
            this.A01 = false;
            if (this.A02.moveToNext()) {
                obj = A00(this.A02);
            } else {
                obj = null;
            }
            this.A00 = obj;
        }
        this.A01 = true;
        return this.A00;
    }

    public void remove() {
        throw new UnsupportedOperationException(AnonymousClass08S.A0J(getClass().toString(), C99084oO.$const$string(239)));
    }

    public C28751fJ(Cursor cursor) {
        Preconditions.checkNotNull(cursor);
        this.A02 = cursor;
    }
}
