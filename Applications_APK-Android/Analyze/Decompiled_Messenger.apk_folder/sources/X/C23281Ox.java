package X;

import com.facebook.acra.ACRA;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.connectionstatus.FbDataConnectionManager;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Ox  reason: invalid class name and case insensitive filesystem */
public abstract class C23281Ox {
    public boolean A03(C50152dV r4) {
        AnonymousClass1Vy A08;
        return !(this instanceof C23261Ov) || (A08 = ((FbDataConnectionManager) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B3e, ((C23261Ov) this).A00)).A08()) == AnonymousClass1Vy.POOR || A08 == AnonymousClass1Vy.A04;
    }

    public C50152dV A04(C23581Qb r5, AnonymousClass1QK r6) {
        return new C50142dU(r5, r6, ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, ((C23261Ov) this).A00)).now());
    }

    public Map A05(C50152dV r11, int i) {
        String str;
        if (!(this instanceof C23261Ov)) {
            return null;
        }
        C23261Ov r3 = (C23261Ov) this;
        C50142dU r112 = (C50142dU) r11;
        long j = r112.A00;
        long j2 = r112.A03;
        long j3 = j - j2;
        long j4 = r112.A01 - j2;
        double A03 = ((FbDataConnectionManager) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B3e, r3.A00)).A03();
        Integer num = r112.A02;
        ImmutableMap.Builder builder = ImmutableMap.builder();
        builder.put("responseLatency", String.valueOf(j3));
        builder.put(C99084oO.$const$string(710), String.valueOf(i));
        builder.put("rtt_ms", String.valueOf(((FbDataConnectionManager) AnonymousClass1XX.A02(6, AnonymousClass1Y3.B3e, r3.A00)).A04()));
        builder.put("average_bandwidth_kbit", String.valueOf(A03));
        builder.put("dropped_bytes", String.valueOf((long) 0));
        boolean z = false;
        if (r112.A01 != -1) {
            z = true;
        }
        if (z) {
            builder.put("cancellation_time_ms", String.valueOf(j4));
        }
        if (num != AnonymousClass07B.A0n) {
            switch (num.intValue()) {
                case 1:
                    str = "FB_CDN_CACHE_MISS";
                    break;
                case 2:
                    str = "AKAMAI_CDN_CACHE_EDGE_HIT";
                    break;
                case 3:
                    str = "AKAMAI_CDN_CACHE_MIDGRESS_HIT";
                    break;
                case 4:
                    str = "AKAMAI_CDN_CACHE_MISS";
                    break;
                case 5:
                    str = "NO_HEADER";
                    break;
                case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                    str = "NOT_IN_GK";
                    break;
                default:
                    str = "FB_CDN_CACHE_HIT";
                    break;
            }
            builder.put("cdnHeaderResponse", str);
        }
        return builder.build();
    }

    public void A06(C50152dV r13, int i) {
        if (this instanceof C23261Ov) {
            C23261Ov r3 = (C23261Ov) this;
            C50142dU r132 = (C50142dU) r13;
            ((AnonymousClass069) AnonymousClass1XX.A02(8, AnonymousClass1Y3.BBa, r3.A00)).now();
            for (AnonymousClass1OQ r6 : (Set) AnonymousClass1XX.A02(9, AnonymousClass1Y3.AY6, r3.A00)) {
                AnonymousClass1QK r0 = r132.A03;
                AnonymousClass1Q0 r7 = r0.A09;
                CallerContext callerContext = (CallerContext) r0.A0A;
                boolean A08 = r0.A08();
                boolean z = false;
                if (r132.A01 != -1) {
                    z = true;
                }
                r6.BgO(r7, callerContext, i, A08, z);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: X.2Zr} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: X.2Zr} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: X.2Zr} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r25v0, resolved type: X.2Zs} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v2, resolved type: com.facebook.common.callercontext.CallerContext} */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0067, code lost:
        if (r3.getQueryParameter("se") != null) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006c, code lost:
        if (r0 == false) goto L_0x006e;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.C50152dV r27, X.C50162dW r28) {
        /*
            r26 = this;
            r8 = r27
            r7 = r26
            X.1Ov r7 = (X.C23261Ov) r7
            X.2dU r8 = (X.C50142dU) r8
            X.1QK r6 = r8.A03
            X.1Q0 r2 = r6.A09
            android.net.Uri r3 = r2.A02
            X.9Mo r5 = r2.A03
            X.1Pz r9 = r6.A03()
            int r4 = X.AnonymousClass1Y3.BKO
            X.0UN r1 = r7.A00
            r0 = 10
            java.lang.Object r0 = X.AnonymousClass1XX.A02(r0, r4, r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x006e
            if (r5 != 0) goto L_0x006e
            X.1Pz r5 = X.C23561Pz.MEDIUM
            r4 = r5
            if (r9 == 0) goto L_0x003a
            if (r5 == 0) goto L_0x0039
            int r1 = r9.ordinal()
            int r0 = r5.ordinal()
            if (r1 <= r0) goto L_0x003a
        L_0x0039:
            r5 = r9
        L_0x003a:
            if (r5 != r4) goto L_0x006e
            java.lang.String r1 = r3.getHost()
            java.lang.String r0 = ".fbcdn.net"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x0069
            r0 = 234(0xea, float:3.28E-43)
            java.lang.String r0 = X.AnonymousClass24B.$const$string(r0)
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x0069
            java.lang.String r1 = r3.getLastPathSegment()
            java.lang.String r0 = "jpg"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x0069
            java.lang.String r0 = "se"
            java.lang.String r1 = r3.getQueryParameter(r0)
            r0 = 1
            if (r1 == 0) goto L_0x006a
        L_0x0069:
            r0 = 0
        L_0x006a:
            r23 = 1
            if (r0 != 0) goto L_0x0070
        L_0x006e:
            r23 = 0
        L_0x0070:
            if (r23 == 0) goto L_0x0086
            android.net.Uri r0 = r2.A02
            android.net.Uri$Builder r3 = r0.buildUpon()
            java.lang.String r1 = X.AnonymousClass24L.A00
            java.lang.String r0 = "se"
            android.net.Uri$Builder r0 = r3.appendQueryParameter(r0, r1)
            android.net.Uri r14 = r0.build()
        L_0x0084:
            r13 = 0
            goto L_0x0089
        L_0x0086:
            android.net.Uri r14 = r2.A02
            goto L_0x0084
        L_0x0089:
            r15 = r28
            java.lang.Object r1 = r6.A0A     // Catch:{ Exception -> 0x014a }
            boolean r0 = r1 instanceof com.facebook.common.callercontext.CallerContext     // Catch:{ Exception -> 0x014a }
            if (r0 == 0) goto L_0x0094
            r13 = r1
            com.facebook.common.callercontext.CallerContext r13 = (com.facebook.common.callercontext.CallerContext) r13     // Catch:{ Exception -> 0x014a }
        L_0x0094:
            X.1Pz r0 = r6.A03()     // Catch:{ Exception -> 0x014a }
            com.facebook.http.interfaces.RequestPriority r20 = X.C23261Ov.A01(r0)     // Catch:{ Exception -> 0x014a }
            X.9Mo r0 = r2.A03     // Catch:{ Exception -> 0x014a }
            r16 = r0
            r17 = r7
            r22 = r8
            r24 = r15
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ Exception -> 0x014a }
            X.0UN r0 = r7.A00     // Catch:{ Exception -> 0x014a }
            r4 = 11
            java.lang.Object r2 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ Exception -> 0x014a }
            X.1Yd r2 = (X.C25051Yd) r2     // Catch:{ Exception -> 0x014a }
            r0 = 566914208237343(0x2039b0000071f, double:2.80092834429362E-309)
            long r2 = r2.At0(r0)     // Catch:{ Exception -> 0x014a }
            r9 = 0
            int r0 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r0 <= 0) goto L_0x013e
            java.util.Random r0 = r7.A01     // Catch:{ Exception -> 0x014a }
            if (r0 != 0) goto L_0x00cc
            java.util.Random r0 = new java.util.Random     // Catch:{ Exception -> 0x014a }
            r0.<init>()     // Catch:{ Exception -> 0x014a }
            r7.A01 = r0     // Catch:{ Exception -> 0x014a }
        L_0x00cc:
            int r1 = X.AnonymousClass1Y3.AOJ     // Catch:{ Exception -> 0x014a }
            X.0UN r0 = r7.A00     // Catch:{ Exception -> 0x014a }
            java.lang.Object r4 = X.AnonymousClass1XX.A02(r4, r1, r0)     // Catch:{ Exception -> 0x014a }
            X.1Yd r4 = (X.C25051Yd) r4     // Catch:{ Exception -> 0x014a }
            r0 = 1129864161722669(0x4039b0001012d, double:5.58227066774389E-309)
            long r11 = r4.At0(r0)     // Catch:{ Exception -> 0x014a }
            java.util.Random r1 = r7.A01     // Catch:{ Exception -> 0x014a }
            r0 = 2147483647(0x7fffffff, float:NaN)
            int r0 = r1.nextInt(r0)     // Catch:{ Exception -> 0x014a }
            double r9 = (double) r0     // Catch:{ Exception -> 0x014a }
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r0 = (double) r11     // Catch:{ Exception -> 0x014a }
            double r4 = java.lang.Math.min(r4, r0)     // Catch:{ Exception -> 0x014a }
            r0 = 4746794007244308480(0x41dfffffffc00000, double:2.147483647E9)
            double r4 = r4 * r0
            int r0 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x013e
            X.2Zs r4 = new X.2Zs     // Catch:{ Exception -> 0x014a }
            r4.<init>()     // Catch:{ Exception -> 0x014a }
            r5 = 12
            int r1 = X.AnonymousClass1Y3.Azd     // Catch:{ Exception -> 0x014a }
            X.0UN r0 = r7.A00     // Catch:{ Exception -> 0x014a }
            java.lang.Object r5 = X.AnonymousClass1XX.A02(r5, r1, r0)     // Catch:{ Exception -> 0x014a }
            java.util.concurrent.ScheduledExecutorService r5 = (java.util.concurrent.ScheduledExecutorService) r5     // Catch:{ Exception -> 0x014a }
            X.9Mw r1 = new X.9Mw     // Catch:{ Exception -> 0x014a }
            r21 = r16
            r25 = r4
            r18 = r14
            r19 = r13
            r16 = r1
            r16.<init>(r17, r18, r19, r20, r21, r22, r23, r24, r25)     // Catch:{ Exception -> 0x014a }
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ Exception -> 0x014a }
            r5.schedule(r1, r2, r0)     // Catch:{ Exception -> 0x014a }
        L_0x011f:
            com.google.common.util.concurrent.ListenableFuture r3 = r4.A00()     // Catch:{ Exception -> 0x014a }
            X.2io r2 = new X.2io     // Catch:{ Exception -> 0x014a }
            r2.<init>(r15)     // Catch:{ Exception -> 0x014a }
            X.2dj r1 = X.C50292dj.A00     // Catch:{ Exception -> 0x014a }
            com.google.common.base.Preconditions.checkNotNull(r2)     // Catch:{ Exception -> 0x014a }
            X.2dl r0 = new X.2dl     // Catch:{ Exception -> 0x014a }
            r0.<init>(r3, r2)     // Catch:{ Exception -> 0x014a }
            r3.addListener(r0, r1)     // Catch:{ Exception -> 0x014a }
            X.2ip r0 = new X.2ip     // Catch:{ Exception -> 0x014a }
            r0.<init>(r7, r8, r4, r6)     // Catch:{ Exception -> 0x014a }
            r6.A06(r0)     // Catch:{ Exception -> 0x014a }
            goto L_0x0149
        L_0x013e:
            r21 = r16
            r18 = r14
            r19 = r13
            X.2Zr r4 = X.C23261Ov.A00(r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x014a }
            goto L_0x011f
        L_0x0149:
            return
        L_0x014a:
            r0 = move-exception
            r15.A02(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C23281Ox.A07(X.2dV, X.2dW):void");
    }
}
