package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.1PB  reason: invalid class name */
public final class AnonymousClass1PB implements C23111Og {
    private static final long A00 = TimeUnit.MINUTES.toMillis(5);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* renamed from: A00 */
    public C30851ik get() {
        int i;
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min < 16777216) {
            i = 1048576;
        } else {
            i = 4194304;
            if (min < 33554432) {
                i = 2097152;
            }
        }
        return new C30851ik(i, Integer.MAX_VALUE, i, Integer.MAX_VALUE, i >> 3, A00);
    }
}
