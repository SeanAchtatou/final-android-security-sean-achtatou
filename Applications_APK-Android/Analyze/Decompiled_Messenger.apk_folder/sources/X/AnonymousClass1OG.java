package X;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/* renamed from: X.1OG  reason: invalid class name */
public final class AnonymousClass1OG {
    private static AnonymousClass1OG A03;
    public int A00;
    public List A01;
    public final AnonymousClass1O4 A02 = new AnonymousClass1OJ();

    /* JADX WARNING: Can't wrap try/catch for region: R(6:10|9|11|12|13|14) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0018, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1O3 A02(java.lang.String r3) {
        /*
            r2 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0012 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0012 }
            X.1O3 r0 = A00(r1)     // Catch:{ IOException -> 0x0011, all -> 0x000e }
            X.AnonymousClass2R2.A01(r1)
            return r0
        L_0x000e:
            r0 = move-exception
            r2 = r1
            goto L_0x0019
        L_0x0011:
            r2 = r1
        L_0x0012:
            X.1O3 r0 = X.AnonymousClass1O3.A02     // Catch:{ all -> 0x0018 }
            X.AnonymousClass2R2.A01(r2)
            return r0
        L_0x0018:
            r0 = move-exception
        L_0x0019:
            X.AnonymousClass2R2.A01(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1OG.A02(java.lang.String):X.1O3");
    }

    public static synchronized AnonymousClass1OG A03() {
        AnonymousClass1OG r0;
        synchronized (AnonymousClass1OG.class) {
            if (A03 == null) {
                A03 = new AnonymousClass1OG();
            }
            r0 = A03;
        }
        return r0;
    }

    public static void A04(AnonymousClass1OG r3) {
        r3.A00 = r3.A02.Aoc();
        List<AnonymousClass1O4> list = r3.A01;
        if (list != null) {
            for (AnonymousClass1O4 Aoc : list) {
                r3.A00 = Math.max(r3.A00, Aoc.Aoc());
            }
        }
    }

    private AnonymousClass1OG() {
        A04(this);
    }

    public static AnonymousClass1O3 A00(InputStream inputStream) {
        int i;
        AnonymousClass1OG A032 = A03();
        C05520Zg.A02(inputStream);
        int i2 = A032.A00;
        byte[] bArr = new byte[i2];
        C05520Zg.A02(inputStream);
        C05520Zg.A02(bArr);
        boolean z = false;
        if (bArr.length >= i2) {
            z = true;
        }
        C05520Zg.A04(z);
        if (inputStream.markSupported()) {
            try {
                inputStream.mark(i2);
                C05520Zg.A02(inputStream);
                C05520Zg.A02(bArr);
                if (i2 >= 0) {
                    i = 0;
                    while (i < i2) {
                        int read = inputStream.read(bArr, 0 + i, i2 - i);
                        if (read == -1) {
                            break;
                        }
                        i += read;
                    }
                } else {
                    throw new IndexOutOfBoundsException("len is negative");
                }
            } finally {
                inputStream.reset();
            }
        } else {
            C05520Zg.A02(inputStream);
            C05520Zg.A02(bArr);
            if (i2 >= 0) {
                int i3 = 0;
                while (i < i2) {
                    int read2 = inputStream.read(bArr, 0 + i, i2 - i);
                    if (read2 == -1) {
                        break;
                    }
                    i3 = i + read2;
                }
            } else {
                throw new IndexOutOfBoundsException("len is negative");
            }
        }
        AnonymousClass1O3 AXH = A032.A02.AXH(bArr, i);
        if (AXH != null && AXH != AnonymousClass1O3.A02) {
            return AXH;
        }
        List<AnonymousClass1O4> list = A032.A01;
        if (list != null) {
            for (AnonymousClass1O4 AXH2 : list) {
                AnonymousClass1O3 AXH3 = AXH2.AXH(bArr, i);
                if (AXH3 != null && AXH3 != AnonymousClass1O3.A02) {
                    return AXH3;
                }
            }
        }
        return AnonymousClass1O3.A02;
    }

    public static AnonymousClass1O3 A01(InputStream inputStream) {
        try {
            return A00(inputStream);
        } catch (IOException e) {
            throw AnonymousClass95E.A00(e);
        }
    }
}
