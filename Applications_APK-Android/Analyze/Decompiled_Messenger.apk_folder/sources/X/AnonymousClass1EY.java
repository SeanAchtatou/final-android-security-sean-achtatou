package X;

import android.app.Activity;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1EY  reason: invalid class name */
public final class AnonymousClass1EY {
    private static volatile AnonymousClass1EY A01;
    public AnonymousClass0UN A00;

    public static final AnonymousClass1EY A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1EY.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1EY(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public static void A01(AnonymousClass1EY r3) {
        int i = AnonymousClass1Y3.A5w;
        if (((AnonymousClass146) AnonymousClass1XX.A02(0, i, r3.A00)).A09() == null) {
            ((AnonymousClass146) AnonymousClass1XX.A02(0, i, r3.A00)).A0D("tap_back_button");
        }
    }

    public static void A02(AnonymousClass1EY r3, Activity activity) {
        C005505z.A03("AnalyticsActivityListener#onResume", -262069465);
        try {
            int i = AnonymousClass1Y3.A5w;
            ((AnonymousClass146) AnonymousClass1XX.A02(0, i, r3.A00)).A0A(activity);
            if (!(activity instanceof AnonymousClass14D)) {
                ((AnonymousClass146) AnonymousClass1XX.A02(0, i, r3.A00)).A0D(null);
            }
        } finally {
            C005505z.A00(-2022699951);
        }
    }

    public static boolean A03(AnonymousClass1EY r3) {
        return ((AnonymousClass1YI) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AcD, r3.A00)).AbO(12, false);
    }

    private AnonymousClass1EY(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }
}
