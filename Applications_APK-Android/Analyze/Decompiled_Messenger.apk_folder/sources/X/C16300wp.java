package X;

import android.view.animation.Animation;

/* renamed from: X.0wp  reason: invalid class name and case insensitive filesystem */
public final class C16300wp implements Animation.AnimationListener {
    public final /* synthetic */ C20871Ed A00;

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public C16300wp(C20871Ed r1) {
        this.A00 = r1;
    }

    public void onAnimationEnd(Animation animation) {
        AnonymousClass199 r0;
        C20871Ed r1 = this.A00;
        if (r1.A0D) {
            r1.A0B.setAlpha(255);
            this.A00.A0B.start();
            C20871Ed r12 = this.A00;
            if (r12.A02 && (r0 = r12.A0C) != null) {
                r0.BlK();
            }
            C20871Ed r13 = this.A00;
            r13.A00 = r13.A0A.getTop();
            return;
        }
        r1.A08();
    }
}
