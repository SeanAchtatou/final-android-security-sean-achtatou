package X;

import com.facebook.graphql.enums.GraphQLMessageThreadCannotReplyReason;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.messaging.model.threads.ThreadSummary;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1tL  reason: invalid class name and case insensitive filesystem */
public final class C36571tL {
    private static volatile C36571tL A06;
    private final C189216c A00;
    private final AnonymousClass0m6 A01;
    private final C36581tM A02;
    private final C36521tG A03;
    private final C193917y A04;
    private final Map A05 = new HashMap();

    public boolean A02(ThreadKey threadKey, boolean z, boolean z2) {
        GraphQLMessageThreadCannotReplyReason graphQLMessageThreadCannotReplyReason;
        if (threadKey == null) {
            return false;
        }
        if (this.A04.A09(threadKey)) {
            this.A03.A0K(threadKey, !z);
        }
        ThreadSummary A0R = this.A01.A0R(threadKey);
        if (A0R == null) {
            this.A05.put(threadKey, Boolean.valueOf(z));
            this.A00.A0D(threadKey, null, "TincanBlockUtil");
            return false;
        }
        this.A05.remove(threadKey);
        C17920zh A002 = ThreadSummary.A00();
        A002.A02(A0R);
        if (z) {
            graphQLMessageThreadCannotReplyReason = null;
        } else {
            graphQLMessageThreadCannotReplyReason = GraphQLMessageThreadCannotReplyReason.A01;
        }
        A002.A0G = graphQLMessageThreadCannotReplyReason;
        this.A01.A0b(A002.A00());
        if (z2) {
            this.A02.A05(A0R.A0S);
            return true;
        }
        this.A00.A0D(threadKey, null, "TincanBlockUtil");
        return true;
    }

    public static final C36571tL A00(AnonymousClass1XY r4) {
        if (A06 == null) {
            synchronized (C36571tL.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A06, r4);
                if (A002 != null) {
                    try {
                        A06 = new C36571tL(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A06;
    }

    public boolean A01(ThreadKey threadKey) {
        if (this.A05.containsKey(threadKey)) {
            return !((Boolean) this.A05.get(threadKey)).booleanValue();
        }
        return false;
    }

    private C36571tL(AnonymousClass1XY r2) {
        this.A01 = AnonymousClass0m2.A02(r2);
        this.A04 = C193917y.A00(r2);
        this.A03 = C36521tG.A03(r2);
        this.A02 = C36581tM.A00(r2);
        AnonymousClass0WT.A00(r2);
        this.A00 = C189216c.A02(r2);
    }
}
