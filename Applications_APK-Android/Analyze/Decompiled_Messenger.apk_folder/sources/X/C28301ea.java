package X;

/* renamed from: X.1ea  reason: invalid class name and case insensitive filesystem */
public abstract class C28301ea {
    public int _index;
    public int _type;

    public final String getTypeDesc() {
        int i = this._type;
        if (i == 0) {
            return "ROOT";
        }
        if (i == 1) {
            return "ARRAY";
        }
        if (i != 2) {
            return "?";
        }
        return "OBJECT";
    }

    public final boolean inObject() {
        if (this._type == 2) {
            return true;
        }
        return false;
    }
}
