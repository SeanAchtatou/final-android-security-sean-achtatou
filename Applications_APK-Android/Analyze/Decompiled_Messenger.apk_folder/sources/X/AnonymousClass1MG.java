package X;

import com.facebook.acra.ACRA;
import com.facebook.common.callercontext.CallerContext;
import com.facebook.common.callercontext.ContextChain;
import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.proxygen.TraceFieldType;
import com.facebook.quicklog.QuickPerformanceLogger;
import io.card.payment.BuildConfig;
import java.util.Map;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1MG  reason: invalid class name */
public final class AnonymousClass1MG implements AnonymousClass1MK {
    private static volatile AnonymousClass1MG A01;
    private AnonymousClass0UN A00;

    public static final AnonymousClass1MG A00(AnonymousClass1XY r4) {
        if (A01 == null) {
            synchronized (AnonymousClass1MG.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A01, r4);
                if (A002 != null) {
                    try {
                        A01 = new AnonymousClass1MG(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A01;
    }

    public void Bk0(AnonymousClass1QK r6, String str, String str2) {
        int hashCode = r6.A0B.hashCode();
        int i = AnonymousClass1Y3.BBd;
        if (((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).isMarkerOn(41222145, hashCode)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerPoint(41222145, hashCode, AnonymousClass08S.A0P(str, "_event_", str2));
        }
    }

    public void Bk2(AnonymousClass1QK r7, String str, Map map) {
        A01(r7.A0B, str, "cancel", map, null);
    }

    public void Bk4(AnonymousClass1QK r7, String str, Throwable th, Map map) {
        String str2 = str;
        A01(r7.A0B, str2, TurboLoader.Locator.$const$string(152), map, th);
    }

    public void Bk6(AnonymousClass1QK r7, String str, Map map) {
        A01(r7.A0B, str, OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, map, null);
    }

    public void Bk8(AnonymousClass1QK r6, String str) {
        int hashCode = r6.A0B.hashCode();
        int i = AnonymousClass1Y3.BBd;
        if (((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).isMarkerOn(41222145, hashCode)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerPoint(41222145, hashCode, AnonymousClass08S.A0J(str, "_start"));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void Blr(AnonymousClass1QK r5) {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(41222145, r5.A0B.hashCode(), (short) 4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void Bm1(AnonymousClass1QK r7, Throwable th) {
        String str;
        int hashCode = r7.A0B.hashCode();
        int i = AnonymousClass1Y3.BBd;
        if (((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).isMarkerOn(41222145, hashCode)) {
            QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00);
            if (th != null) {
                str = th.getMessage();
            } else {
                str = "unknown";
            }
            quickPerformanceLogger.markerAnnotate(41222145, hashCode, "failure_reason", str);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(41222145, hashCode, (short) 3);
        }
    }

    public void Bm6(AnonymousClass1QK r12) {
        CallerContext callerContext;
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(103, false)) {
            return;
        }
        if (!((C25051Yd) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AOJ, this.A00)).Aem(283897338334832L) || C004004z.A03("qpl")) {
            String str = r12.A0B;
            int hashCode = str.hashCode();
            int i = AnonymousClass1Y3.BBd;
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerStart(41222145, hashCode);
            if (((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).isMarkerOn(41222145, hashCode)) {
                boolean A08 = r12.A08();
                AnonymousClass1Q0 r8 = r12.A09;
                Object obj = r12.A0A;
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerAnnotate(41222145, hashCode, "prefetch", A08);
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "request_level", r12.A08.mValue);
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, TraceFieldType.Uri, String.valueOf(r8.A02));
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "uri_hash", Math.abs(r8.A02.hashCode()));
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "ui_component_id", r12.A0C);
                if (((C25051Yd) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AOJ, this.A00)).Aem(283897338400369L)) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "normalized_url", C006306i.A00(r8.A02).toString());
                }
                if (obj instanceof CallerContext) {
                    callerContext = (CallerContext) obj;
                } else {
                    callerContext = CallerContext.A06;
                }
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "module_analytics_tag", callerContext.A0H());
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "analytics_tag", callerContext.A0F());
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "feature_tag", callerContext.A0G());
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "calling_class", callerContext.A02);
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, TraceFieldType.RequestID, str);
                int i2 = AnonymousClass1Y3.BBd;
                AnonymousClass0UN r1 = this.A00;
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i2, r1)).markerAnnotate(41222145, hashCode, "network_type", ((C09340h3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AeN, r1)).A0K());
                int i3 = AnonymousClass1Y3.BBd;
                AnonymousClass0UN r13 = this.A00;
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i3, r13)).markerAnnotate(41222145, hashCode, "network_subtype", ((C09340h3) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AeN, r13)).A0J());
                ContextChain contextChain = callerContext.A00;
                if (contextChain != null) {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "context_chain", contextChain.A01());
                    Map map = contextChain.A00;
                    if (C004004z.A03("qpl") && map != null) {
                        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "extra_data", map.toString());
                    }
                } else {
                    ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "context_chain", "unset");
                }
                ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, ACRA.SESSION_ID_KEY, AnonymousClass01P.A05());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void Bm8(AnonymousClass1QK r7) {
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, r7.A0B.hashCode(), "image_origin_from_producer_context", (String) r7.A05.get(1, BuildConfig.FLAVOR));
        ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerEnd(41222145, r7.A0B.hashCode(), (short) 2);
    }

    public void Bt8(AnonymousClass1QK r6, String str, boolean z) {
        int hashCode = r6.A0B.hashCode();
        int i = AnonymousClass1Y3.BBd;
        if (((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).isMarkerOn(41222145, hashCode)) {
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).markerAnnotate(41222145, hashCode, "ultimate_producer", str);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "ultimate_successful", z);
            ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).markerAnnotate(41222145, hashCode, "image_origin", C621030a.A01(C621030a.A00(str)));
        }
    }

    public boolean C3Q(AnonymousClass1QK r5, String str) {
        if (!((AnonymousClass1YI) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AcD, this.A00)).AbO(103, false) || !((QuickPerformanceLogger) AnonymousClass1XX.A02(1, AnonymousClass1Y3.BBd, this.A00)).isMarkerOn(41222145, r5.A0B.hashCode())) {
            return false;
        }
        return true;
    }

    private AnonymousClass1MG(AnonymousClass1XY r3) {
        this.A00 = new AnonymousClass0UN(4, r3);
    }

    private void A01(String str, String str2, String str3, Map map, Throwable th) {
        int hashCode = str.hashCode();
        int i = AnonymousClass1Y3.BBd;
        if (((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).isMarkerOn(41222145, hashCode)) {
            String A0J = AnonymousClass08S.A0J(str2, "_end");
            C21061Ew withMarker = ((QuickPerformanceLogger) AnonymousClass1XX.A02(1, i, this.A00)).withMarker(41222145, hashCode);
            C31011j0 A0D = withMarker.A0D(A0J);
            A0D.ANT("status", str3);
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    A0D.ANT((String) entry.getKey(), (String) entry.getValue());
                }
            }
            if (th != null) {
                A0D.ANT("failure_reason", th.getMessage());
            }
            A0D.BK9();
            withMarker.BK9();
        }
    }
}
