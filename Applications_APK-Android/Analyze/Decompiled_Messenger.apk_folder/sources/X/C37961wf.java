package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.account.recovery.common.protocol.OpenIDConnectAccountRecoveryMethodResult;

/* renamed from: X.1wf  reason: invalid class name and case insensitive filesystem */
public final class C37961wf implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new OpenIDConnectAccountRecoveryMethodResult(parcel);
    }

    public Object[] newArray(int i) {
        return new OpenIDConnectAccountRecoveryMethodResult[i];
    }
}
