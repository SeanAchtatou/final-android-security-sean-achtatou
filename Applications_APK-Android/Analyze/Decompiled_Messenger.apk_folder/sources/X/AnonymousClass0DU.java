package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.0DU  reason: invalid class name */
public final class AnonymousClass0DU extends Handler {
    public final /* synthetic */ AnonymousClass0QV A00;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnonymousClass0DU(AnonymousClass0QV r1, Looper looper) {
        super(looper);
        this.A00 = r1;
    }

    public void handleMessage(Message message) {
        if (message.what == 1) {
            AnonymousClass0QV r1 = this.A00;
            AnonymousClass0QV.A01(r1, new AnonymousClass0K9(r1));
            AnonymousClass0QV r12 = this.A00;
            AnonymousClass0QV.A01(r12, new AnonymousClass0K7(r12));
        }
    }
}
