package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.model.threads.SyncedGroupData;

/* renamed from: X.0xs  reason: invalid class name and case insensitive filesystem */
public final class C16850xs implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new SyncedGroupData(parcel);
    }

    public Object[] newArray(int i) {
        return new SyncedGroupData[i];
    }
}
