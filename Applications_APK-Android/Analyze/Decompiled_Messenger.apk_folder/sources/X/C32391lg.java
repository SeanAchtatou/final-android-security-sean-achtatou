package X;

import com.facebook.analytics.DeprecatedAnalyticsLogger;
import com.facebook.messaging.model.threadkey.ThreadKey;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1lg  reason: invalid class name and case insensitive filesystem */
public final class C32391lg extends C36471tB {
    public static final Class A06 = C32391lg.class;
    private static volatile C32391lg A07;
    public final C04310Tq A00;
    private final AnonymousClass06A A01;
    private final AnonymousClass0US A02;
    private final C194718h A03;
    private final AnonymousClass18N A04;
    private final C04310Tq A05;

    public static synchronized void A01(C32391lg r9, C22338Aw9 aw9, C22338Aw9 aw92, C22036AnC anC, C22335Aw3 aw3, byte[] bArr, Long l, byte[] bArr2) {
        C32391lg r1 = r9;
        synchronized (r1) {
            C22338Aw9 aw93 = aw9;
            C22338Aw9 aw94 = aw92;
            C22036AnC anC2 = anC;
            r1.A02(aw93, aw94, anC2, C22076Anw.A00.intValue(), aw3, bArr, l, bArr2);
        }
    }

    private synchronized void A02(C22338Aw9 aw9, C22338Aw9 aw92, C22036AnC anC, int i, C22335Aw3 aw3, byte[] bArr, Long l, byte[] bArr2) {
        if (!A0C()) {
            this.A02.get();
            C010708t.A05(A06, "No stored procedure available to use for send");
        } else if (this.A04.A03()) {
            C010708t.A05(A06, "Invalid device id");
        } else {
            C22338Aw9 aw93 = aw9;
            A0B(C22030An2.A01(new C22337Aw7(Integer.valueOf(i), aw93, aw92, Long.valueOf(this.A01.now() * 1000), anC, aw3, bArr, l, bArr2)));
        }
    }

    public synchronized void A0F(C22338Aw9 aw9, C22036AnC anC, int i, byte[] bArr, Long l, byte[] bArr2) {
        C22335Aw3 aw3;
        C22338Aw9 A002 = C22039AnF.A00(Long.parseLong((String) this.A05.get()), this.A04.A02());
        byte[] A052 = C36471tB.A05();
        synchronized (this) {
            byte[] bArr3 = bArr;
            Long l2 = l;
            if (l != null) {
                C22350AwV awV = new C22350AwV(bArr3, true);
                aw3 = new C22335Aw3();
                aw3.setField_ = 23;
                aw3.value_ = awV;
            } else {
                aw3 = new C22335Aw3();
                if (bArr != null) {
                    aw3.setField_ = 8;
                    aw3.value_ = bArr3;
                } else {
                    throw new NullPointerException();
                }
            }
            A02(aw9, A002, anC, i, aw3, A052, l2, bArr2);
        }
    }

    private C32391lg(AnonymousClass18N r2, C04310Tq r3, C04310Tq r4, AnonymousClass06A r5, AnonymousClass0US r6, C194718h r7, DeprecatedAnalyticsLogger deprecatedAnalyticsLogger) {
        super(11, deprecatedAnalyticsLogger);
        this.A04 = r2;
        this.A05 = r3;
        this.A00 = r4;
        this.A01 = r5;
        this.A02 = r6;
        this.A03 = r7;
    }

    public static final C32391lg A00(AnonymousClass1XY r11) {
        if (A07 == null) {
            synchronized (C32391lg.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A07, r11);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r11.getApplicationInjector();
                        A07 = new C32391lg(AnonymousClass18N.A00(applicationInjector), C10580kT.A04(applicationInjector), AnonymousClass0VG.A00(AnonymousClass1Y3.BDe, applicationInjector), AnonymousClass067.A0A(applicationInjector), AnonymousClass0UQ.A00(AnonymousClass1Y3.AVl, applicationInjector), C194718h.A06(applicationInjector), C06920cI.A00(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A07;
    }

    public synchronized void A0E(ThreadKey threadKey, long j, String str, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        byte[] C5Z;
        long j2 = j;
        ThreadKey threadKey2 = threadKey;
        String str2 = str;
        C22056Anc anc = new C22056Anc(this.A03, C22047AnO.A00(threadKey2, j2, str2));
        synchronized (C22056Anc.A02) {
            try {
                C22107Aog A0C = anc.A00.A0C(anc.A01);
                AnonymousClass20P A002 = A0C.A00();
                AnI A003 = A002.A00();
                C22050AnU anU = new C22050AnU(A003.A00, AnI.A00(AnI.A02, A003.A01));
                byte[] bArr4 = anU.A02;
                byte[] bArr5 = anU.A01;
                IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr4);
                Cipher instance = Cipher.getInstance(C22298Ase.$const$string(15));
                instance.init(1, new SecretKeySpec(bArr5, "AES"), ivParameterSpec);
                byte[] doFinal = instance.doFinal(bArr);
                C22218Aqu aqu = A002.A00;
                C22112Aol aol = new C22112Aol(aqu.senderKeyId_, anU.A00, doFinal, new C49002bU(aqu.senderSigningKey_.private_.A0B()));
                A002.A01(A002.A00().A01());
                anc.A00.A0H(anc.A01, A0C);
                C5Z = aol.C5Z();
            } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
                throw new AssertionError(e);
            } catch (C22069Anp e2) {
                throw new C22068Ano(e2);
            }
        }
        try {
            A01(this, null, C22039AnF.A00(j2, str2), C22036AnC.A0G, C22335Aw3.A00(new Aw4(C5Z, bArr2, null, false, true, 1, null, null)), bArr3, Long.valueOf(threadKey2.A03), this.A03.A0K(threadKey2));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e3) {
            C010708t.A08(A06, "Unable to calculate participant checksum for thread", e3);
        }
        return;
    }

    public synchronized void A0D(ThreadKey threadKey, long j, String str, long j2, String str2, byte[] bArr, byte[] bArr2, byte[] bArr3, boolean z, boolean z2, boolean z3) {
        Long l;
        byte[] bArr4 = bArr3;
        synchronized (this) {
            long j3 = j2;
            String str3 = str2;
            C22063Anj A022 = ((C22026Amx) this.A00.get()).A02(C22054Ana.A00(j3, str3));
            if (A022 == null) {
                C010708t.A05(A06, C22298Ase.$const$string(AnonymousClass1Y3.A1A));
            } else {
                C22064Ank A002 = C22027Amy.A00(A022, bArr, (C22026Amx) this.A00.get());
                C22335Aw3 A003 = C22335Aw3.A00(new Aw4(A002.A01, bArr2, null, Boolean.valueOf(A002.A00), Boolean.valueOf(z), Boolean.valueOf(z2), Boolean.valueOf(z3), false));
                try {
                    ThreadKey threadKey2 = threadKey;
                    byte[] A0K = this.A03.A0K(threadKey2);
                    C22338Aw9 A004 = C22039AnF.A00(j3, str3);
                    C22338Aw9 A005 = C22039AnF.A00(j, str);
                    C22036AnC anC = C22036AnC.A0G;
                    if (bArr3 == null) {
                        bArr4 = C36471tB.A05();
                    }
                    if (threadKey != null) {
                        l = Long.valueOf(threadKey2.A03);
                    } else {
                        l = null;
                    }
                    A01(this, A004, A005, anC, A003, bArr4, l, A0K);
                } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
                    C010708t.A08(A06, "Unable to calculate participant checksum for thread", e);
                }
            }
        }
        return;
    }
}
