package X;

import java.util.Locale;

/* renamed from: X.11O  reason: invalid class name */
public abstract class AnonymousClass11O implements C22311Kv {
    public final C22221Kj A00;

    public boolean A00() {
        return !(this instanceof C17090yJ) ? ((C22191Kg) this).A00 : AnonymousClass8WC.A00(Locale.getDefault()) == 1;
    }

    public boolean BGQ(CharSequence charSequence, int i, int i2) {
        if (charSequence == null || i < 0 || i2 < 0 || charSequence.length() - i2 < i) {
            throw new IllegalArgumentException();
        }
        C22221Kj r0 = this.A00;
        if (r0 == null) {
            return A00();
        }
        int ASF = r0.ASF(charSequence, i, i2);
        if (ASF == 0) {
            return true;
        }
        if (ASF != 1) {
            return A00();
        }
        return false;
    }

    public AnonymousClass11O(C22221Kj r1) {
        this.A00 = r1;
    }
}
