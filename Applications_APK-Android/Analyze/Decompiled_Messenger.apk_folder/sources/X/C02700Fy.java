package X;

/* renamed from: X.0Fy  reason: invalid class name and case insensitive filesystem */
public final class C02700Fy extends AnonymousClass0FM {
    public long activeTimeMs;
    public double powerMah;
    public long wakeUpTimeMs;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C02700Fy r7 = (C02700Fy) obj;
            if (!(Double.compare(r7.powerMah, this.powerMah) == 0 && this.activeTimeMs == r7.activeTimeMs && this.wakeUpTimeMs == r7.wakeUpTimeMs)) {
                return false;
            }
        }
        return true;
    }

    public /* bridge */ /* synthetic */ AnonymousClass0FM A06(AnonymousClass0FM r1) {
        A0B((C02700Fy) r1);
        return this;
    }

    /* renamed from: A09 */
    public C02700Fy A07(C02700Fy r5, C02700Fy r6) {
        if (r6 == null) {
            r6 = new C02700Fy();
        }
        if (r5 == null) {
            r6.A0B(this);
            return r6;
        }
        r6.powerMah = this.powerMah - r5.powerMah;
        r6.activeTimeMs = this.activeTimeMs - r5.activeTimeMs;
        r6.wakeUpTimeMs = this.wakeUpTimeMs - r5.wakeUpTimeMs;
        return r6;
    }

    /* renamed from: A0A */
    public C02700Fy A08(C02700Fy r5, C02700Fy r6) {
        if (r6 == null) {
            r6 = new C02700Fy();
        }
        if (r5 == null) {
            r6.A0B(this);
            return r6;
        }
        r6.powerMah = r5.powerMah + this.powerMah;
        r6.activeTimeMs = r5.activeTimeMs + this.activeTimeMs;
        r6.wakeUpTimeMs = r5.wakeUpTimeMs + this.wakeUpTimeMs;
        return r6;
    }

    public void A0B(C02700Fy r3) {
        this.powerMah = r3.powerMah;
        this.activeTimeMs = r3.activeTimeMs;
        this.wakeUpTimeMs = r3.wakeUpTimeMs;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.powerMah);
        long j = this.activeTimeMs;
        long j2 = this.wakeUpTimeMs;
        return (((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "Consumption{powerMah=" + this.powerMah + ", activeTimeMs=" + this.activeTimeMs + ", wakeUpTimeMs=" + this.wakeUpTimeMs + '}';
    }
}
