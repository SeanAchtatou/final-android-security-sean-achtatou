package X;

import android.content.Context;
import android.content.Intent;
import android.preference.Preference;

/* renamed from: X.1y7  reason: invalid class name and case insensitive filesystem */
public final class C38861y7 implements Preference.OnPreferenceClickListener {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ Intent A01;
    public final /* synthetic */ AnonymousClass4Z4 A02;

    public C38861y7(AnonymousClass4Z4 r1, Intent intent, Context context) {
        this.A02 = r1;
        this.A01 = intent;
        this.A00 = context;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.A02.A00.startFacebookActivity(this.A01, this.A00);
        return true;
    }
}
