package X;

import java.util.Arrays;

/* renamed from: X.07B  reason: invalid class name */
public final class AnonymousClass07B {
    public static final Integer A00 = 0;
    public static final Integer A01 = 1;
    public static final Integer A02 = 10;
    public static final Integer A03 = 11;
    public static final Integer A04 = 12;
    public static final Integer A05 = 13;
    public static final Integer A06 = 14;
    public static final Integer A07 = 15;
    public static final Integer A08 = 16;
    public static final Integer A09 = 17;
    public static final Integer A0A = 18;
    public static final Integer A0B = 19;
    public static final Integer A0C = 2;
    public static final Integer A0D = 20;
    public static final Integer A0E = 21;
    public static final Integer A0F = 22;
    public static final Integer A0G = 23;
    public static final Integer A0H = 24;
    public static final Integer A0I = 25;
    public static final Integer A0J = 26;
    public static final Integer A0K = 27;
    public static final Integer A0L = 28;
    public static final Integer A0M = 29;
    public static final Integer A0N = 3;
    public static final Integer A0O = 30;
    public static final Integer A0P = 31;
    public static final Integer A0Q = 32;
    public static final Integer A0R = 33;
    public static final Integer A0S = 34;
    public static final Integer A0T = 35;
    public static final Integer A0U = 36;
    public static final Integer A0V = 37;
    public static final Integer A0W = 38;
    public static final Integer A0X = 39;
    public static final Integer A0Y = 4;
    public static final Integer A0Z = 40;
    public static final Integer A0a = 41;
    public static final Integer A0b = 42;
    public static final Integer A0c = 43;
    public static final Integer A0d = 44;
    public static final Integer A0e = 45;
    public static final Integer A0f = 46;
    public static final Integer A0g = 47;
    public static final Integer A0h = 48;
    public static final Integer A0i = 5;
    public static final Integer A0j = 50;
    public static final Integer A0k = 51;
    public static final Integer A0l = 52;
    public static final Integer A0m = 53;
    public static final Integer A0n = 6;
    public static final Integer A0o = 7;
    public static final Integer A0p = 8;
    public static final Integer A0q = 9;
    private static final Integer[] A0r;

    static {
        Integer[] numArr = new Integer[93];
        System.arraycopy(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26}, 0, numArr, 0, 27);
        System.arraycopy(new Integer[]{27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53}, 0, numArr, 27, 27);
        System.arraycopy(new Integer[]{54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80}, 0, numArr, 54, 27);
        System.arraycopy(new Integer[]{81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92}, 0, numArr, 81, 12);
        A0r = numArr;
    }

    public static Integer[] A00(int i) {
        return (Integer[]) Arrays.copyOfRange(A0r, 0, i);
    }
}
