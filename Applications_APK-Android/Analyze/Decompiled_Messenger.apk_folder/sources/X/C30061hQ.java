package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.messaging.browser.model.MessengerWebViewParams;

/* renamed from: X.1hQ  reason: invalid class name and case insensitive filesystem */
public final class C30061hQ implements Parcelable.Creator {
    public Object createFromParcel(Parcel parcel) {
        return new MessengerWebViewParams(parcel);
    }

    public Object[] newArray(int i) {
        return new MessengerWebViewParams[i];
    }
}
