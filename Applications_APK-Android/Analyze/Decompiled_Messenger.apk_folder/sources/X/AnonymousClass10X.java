package X;

/* renamed from: X.10X  reason: invalid class name */
public enum AnonymousClass10X {
    COLUMN(0),
    COLUMN_REVERSE(1),
    ROW(2),
    ROW_REVERSE(3);
    
    public final int mIntValue;

    public static AnonymousClass10X A00(int i) {
        if (i == 0) {
            return COLUMN;
        }
        if (i == 1) {
            return COLUMN_REVERSE;
        }
        if (i == 2) {
            return ROW;
        }
        if (i == 3) {
            return ROW_REVERSE;
        }
        throw new IllegalArgumentException(AnonymousClass08S.A09("Unknown enum value: ", i));
    }

    private AnonymousClass10X(int i) {
        this.mIntValue = i;
    }
}
