package X;

import android.net.Uri;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.0ar  reason: invalid class name and case insensitive filesystem */
public final class C06110ar {
    public static final Pattern A01 = Pattern.compile("\\{([#!]?)([^ }]+)(?: ([^}]+))?\\}");
    public static final Pattern A02 = Pattern.compile("\\{([#]?)([^ }]+)\\}");
    private static final Pattern A03 = Pattern.compile("&?([^=]+)=([^&]+)");
    private final List A00 = C04300To.A00();

    public static String[] A01(String str) {
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != '?') {
                if (charAt == '{') {
                    z = true;
                } else if (charAt == '}') {
                    z = false;
                }
            } else if (!z) {
                return new String[]{str.substring(0, i), str.substring(i + 1)};
            }
        }
        return new String[]{str, BuildConfig.FLAVOR};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0206, code lost:
        r1 = new X.C189818s6("Query parameter does not match templating syntax");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x020e, code lost:
        r1 = new X.C189818s6("Duplicate template key");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00ce, code lost:
        r4.A02 = java.util.regex.Pattern.compile(X.AnonymousClass08S.A0J(r12, "[/]?"));
        r14 = A00(r16).entrySet().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ea, code lost:
        if (r14.hasNext() == false) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ec, code lost:
        r3 = (java.util.Map.Entry) r14.next();
        r12 = X.C06110ar.A01.matcher((java.lang.CharSequence) r3.getValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0102, code lost:
        if (r12.matches() == false) goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0104, code lost:
        r5 = X.AnonymousClass07B.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0110, code lost:
        if ("#".equals(r12.group(1)) == false) goto L_0x0134;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0112, code lost:
        r5 = X.AnonymousClass07B.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0114, code lost:
        r13 = r12.group(2);
        r12 = r12.group(3);
        r3 = (java.lang.String) r3.getKey();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0127, code lost:
        if (r9.add(r3) == false) goto L_0x020e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0129, code lost:
        r4.A01.put(r3, new X.C30416Evr(r13, r5, r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x013e, code lost:
        if ("!".equals(r12.group(1)) == false) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0140, code lost:
        r5 = X.AnonymousClass07B.A0C;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        r1 = new X.C189818s6("Duplicate template key");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C55562oH A02(java.lang.String r20) {
        /*
            r19 = this;
            r6 = r20
            if (r20 == 0) goto L_0x021e
            r0 = r19
            java.util.List r0 = r0.A00
            java.util.Iterator r18 = r0.iterator()
        L_0x000c:
            boolean r0 = r18.hasNext()
            if (r0 == 0) goto L_0x021c
            java.lang.Object r4 = r18.next()
            X.0at r4 = (X.C06120at) r4
            monitor-enter(r4)
            int r9 = r6.length()     // Catch:{ all -> 0x0219 }
            java.lang.String r0 = r4.A04     // Catch:{ all -> 0x0219 }
            int r7 = r0.length()     // Catch:{ all -> 0x0219 }
            r8 = 0
            r5 = 0
        L_0x0025:
            r1 = 0
            r14 = 1
            if (r5 >= r7) goto L_0x0053
            java.lang.String r0 = r4.A04     // Catch:{ all -> 0x0219 }
            char r3 = r0.charAt(r5)     // Catch:{ all -> 0x0219 }
            int r0 = r7 + -1
            r2 = 0
            if (r5 != r0) goto L_0x0035
            r2 = 1
        L_0x0035:
            r0 = 123(0x7b, float:1.72E-43)
            if (r3 == r0) goto L_0x0053
            r0 = 63
            if (r3 == r0) goto L_0x0053
            r0 = 37
            if (r3 == r0) goto L_0x0053
            if (r2 == 0) goto L_0x0048
            r0 = 47
            if (r3 != r0) goto L_0x0048
            goto L_0x0053
        L_0x0048:
            if (r5 >= r9) goto L_0x01fa
            char r0 = r6.charAt(r5)     // Catch:{ all -> 0x0219 }
            if (r3 != r0) goto L_0x01fa
            int r5 = r5 + 1
            goto L_0x0025
        L_0x0053:
            java.lang.String[] r0 = A01(r6)     // Catch:{ all -> 0x0219 }
            r10 = r0[r8]     // Catch:{ all -> 0x0219 }
            r17 = r0[r14]     // Catch:{ all -> 0x0219 }
            java.util.regex.Pattern r0 = r4.A02     // Catch:{ all -> 0x0219 }
            if (r0 != 0) goto L_0x0144
            monitor-enter(r4)     // Catch:{ all -> 0x0219 }
            java.util.HashMap r0 = X.AnonymousClass0TG.A04()     // Catch:{ all -> 0x0216 }
            r4.A01 = r0     // Catch:{ all -> 0x0216 }
            java.util.ArrayList r0 = X.C04300To.A00()     // Catch:{ all -> 0x0216 }
            r4.A00 = r0     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = r4.A04     // Catch:{ all -> 0x0216 }
            java.lang.String[] r0 = A01(r0)     // Catch:{ all -> 0x0216 }
            r12 = r0[r8]     // Catch:{ all -> 0x0216 }
            r11 = 1
            r16 = r0[r14]     // Catch:{ all -> 0x0216 }
            java.util.regex.Pattern r0 = X.C06110ar.A02     // Catch:{ all -> 0x0216 }
            java.util.regex.Matcher r5 = r0.matcher(r12)     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "/"
            boolean r0 = r12.endsWith(r0)     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x008e
            int r0 = r12.length()     // Catch:{ all -> 0x0216 }
            int r0 = r0 - r14
            java.lang.String r12 = r12.substring(r8, r0)     // Catch:{ all -> 0x0216 }
        L_0x008e:
            java.util.HashSet r9 = X.C25011Xz.A03()     // Catch:{ all -> 0x0216 }
        L_0x0092:
            boolean r0 = r5.find()     // Catch:{ all -> 0x0216 }
            r7 = 2
            if (r0 == 0) goto L_0x00ce
            java.lang.String r3 = r5.group(r8)     // Catch:{ all -> 0x0216 }
            java.lang.String r2 = r5.group(r11)     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "#"
            boolean r15 = r2.equals(r0)     // Catch:{ all -> 0x0216 }
            if (r15 == 0) goto L_0x00cb
            java.lang.Class<java.lang.Long> r14 = java.lang.Long.class
        L_0x00ab:
            java.lang.String r7 = r5.group(r7)     // Catch:{ all -> 0x0216 }
            java.util.List r2 = r4.A00     // Catch:{ all -> 0x0216 }
            X.0FE r0 = new X.0FE     // Catch:{ all -> 0x0216 }
            r0.<init>(r14, r7)     // Catch:{ all -> 0x0216 }
            r2.add(r0)     // Catch:{ all -> 0x0216 }
            boolean r0 = r9.add(r7)     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x01fe
            if (r15 == 0) goto L_0x00c4
            java.lang.String r0 = "(-?[0-9]+)"
            goto L_0x00c6
        L_0x00c4:
            java.lang.String r0 = "([^/]+)"
        L_0x00c6:
            java.lang.String r12 = r12.replace(r3, r0)     // Catch:{ all -> 0x0216 }
            goto L_0x0092
        L_0x00cb:
            java.lang.Class<java.lang.String> r14 = java.lang.String.class
            goto L_0x00ab
        L_0x00ce:
            java.lang.String r0 = "[/]?"
            java.lang.String r0 = X.AnonymousClass08S.A0J(r12, r0)     // Catch:{ all -> 0x0216 }
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r0)     // Catch:{ all -> 0x0216 }
            r4.A02 = r0     // Catch:{ all -> 0x0216 }
            java.util.Map r0 = A00(r16)     // Catch:{ all -> 0x0216 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x0216 }
            java.util.Iterator r14 = r0.iterator()     // Catch:{ all -> 0x0216 }
        L_0x00e6:
            boolean r0 = r14.hasNext()     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x0143
            java.lang.Object r3 = r14.next()     // Catch:{ all -> 0x0216 }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ all -> 0x0216 }
            java.util.regex.Pattern r2 = X.C06110ar.A01     // Catch:{ all -> 0x0216 }
            java.lang.Object r0 = r3.getValue()     // Catch:{ all -> 0x0216 }
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0     // Catch:{ all -> 0x0216 }
            java.util.regex.Matcher r12 = r2.matcher(r0)     // Catch:{ all -> 0x0216 }
            boolean r0 = r12.matches()     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x0206
            java.lang.Integer r5 = X.AnonymousClass07B.A00     // Catch:{ all -> 0x0216 }
            java.lang.String r2 = "#"
            java.lang.String r0 = r12.group(r11)     // Catch:{ all -> 0x0216 }
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x0134
            java.lang.Integer r5 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0216 }
        L_0x0114:
            java.lang.String r13 = r12.group(r7)     // Catch:{ all -> 0x0216 }
            r0 = 3
            java.lang.String r12 = r12.group(r0)     // Catch:{ all -> 0x0216 }
            java.lang.Object r3 = r3.getKey()     // Catch:{ all -> 0x0216 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0216 }
            boolean r0 = r9.add(r3)     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x020e
            java.util.Map r2 = r4.A01     // Catch:{ all -> 0x0216 }
            X.Evr r0 = new X.Evr     // Catch:{ all -> 0x0216 }
            r0.<init>(r13, r5, r12)     // Catch:{ all -> 0x0216 }
            r2.put(r3, r0)     // Catch:{ all -> 0x0216 }
            goto L_0x00e6
        L_0x0134:
            java.lang.String r2 = "!"
            java.lang.String r0 = r12.group(r11)     // Catch:{ all -> 0x0216 }
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x0216 }
            if (r0 == 0) goto L_0x0114
            java.lang.Integer r5 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x0216 }
            goto L_0x0114
        L_0x0143:
            monitor-exit(r4)     // Catch:{ all -> 0x0219 }
        L_0x0144:
            java.util.regex.Pattern r0 = r4.A02     // Catch:{ all -> 0x0219 }
            java.util.regex.Matcher r9 = r0.matcher(r10)     // Catch:{ all -> 0x0219 }
            boolean r0 = r9.matches()     // Catch:{ all -> 0x0219 }
            if (r0 == 0) goto L_0x01fa
            android.os.Bundle r7 = new android.os.Bundle     // Catch:{ all -> 0x0219 }
            r7.<init>()     // Catch:{ all -> 0x0219 }
        L_0x0155:
            java.util.List r0 = r4.A00     // Catch:{ all -> 0x0219 }
            int r0 = r0.size()     // Catch:{ all -> 0x0219 }
            if (r8 >= r0) goto L_0x018d
            java.util.List r0 = r4.A00     // Catch:{ all -> 0x0219 }
            java.lang.Object r3 = r0.get(r8)     // Catch:{ all -> 0x0219 }
            X.0FE r3 = (X.AnonymousClass0FE) r3     // Catch:{ all -> 0x0219 }
            java.lang.Object r2 = r3.A00     // Catch:{ all -> 0x0219 }
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r2 != r0) goto L_0x017d
            java.lang.Object r5 = r3.A01     // Catch:{ all -> 0x0219 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0219 }
            int r0 = r8 + 1
            java.lang.String r0 = r9.group(r0)     // Catch:{ all -> 0x0219 }
            long r2 = java.lang.Long.parseLong(r0)     // Catch:{ all -> 0x0219 }
            r7.putLong(r5, r2)     // Catch:{ all -> 0x0219 }
            goto L_0x018a
        L_0x017d:
            java.lang.Object r2 = r3.A01     // Catch:{ all -> 0x0219 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0219 }
            int r0 = r8 + 1
            java.lang.String r0 = r9.group(r0)     // Catch:{ all -> 0x0219 }
            r7.putString(r2, r0)     // Catch:{ all -> 0x0219 }
        L_0x018a:
            int r8 = r8 + 1
            goto L_0x0155
        L_0x018d:
            java.util.Map r8 = A00(r17)     // Catch:{ all -> 0x0219 }
            java.util.Map r0 = r4.A01     // Catch:{ all -> 0x0219 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x0219 }
            java.util.Iterator r9 = r0.iterator()     // Catch:{ all -> 0x0219 }
        L_0x019b:
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x0219 }
            if (r0 == 0) goto L_0x01f1
            java.lang.Object r0 = r9.next()     // Catch:{ all -> 0x0219 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x0219 }
            java.lang.Object r3 = r0.getKey()     // Catch:{ all -> 0x0219 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0219 }
            java.lang.Object r2 = r0.getValue()     // Catch:{ all -> 0x0219 }
            X.Evr r2 = (X.C30416Evr) r2     // Catch:{ all -> 0x0219 }
            java.lang.String r5 = r2.A02     // Catch:{ all -> 0x0219 }
            boolean r0 = r2.A03     // Catch:{ all -> 0x0219 }
            if (r0 == 0) goto L_0x01c0
            boolean r0 = r8.containsKey(r3)     // Catch:{ all -> 0x0219 }
            if (r0 != 0) goto L_0x01c0
            goto L_0x01fa
        L_0x01c0:
            boolean r0 = r8.containsKey(r3)     // Catch:{ all -> 0x0219 }
            if (r0 == 0) goto L_0x01da
            java.lang.Object r3 = r8.get(r3)     // Catch:{ all -> 0x0219 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0219 }
        L_0x01cc:
            java.lang.Integer r2 = r2.A00     // Catch:{ all -> 0x0219 }
            java.lang.Integer r0 = X.AnonymousClass07B.A01     // Catch:{ all -> 0x0219 }
            if (r2 != r0) goto L_0x01dd
            long r2 = java.lang.Long.parseLong(r3)     // Catch:{ all -> 0x0219 }
            r7.putLong(r5, r2)     // Catch:{ all -> 0x0219 }
            goto L_0x019b
        L_0x01da:
            java.lang.String r3 = r2.A01     // Catch:{ all -> 0x0219 }
            goto L_0x01cc
        L_0x01dd:
            java.lang.Integer r0 = X.AnonymousClass07B.A0C     // Catch:{ all -> 0x0219 }
            if (r2 != r0) goto L_0x01ed
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0219 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0219 }
            r7.putBoolean(r5, r0)     // Catch:{ all -> 0x0219 }
            goto L_0x019b
        L_0x01ed:
            r7.putString(r5, r3)     // Catch:{ all -> 0x0219 }
            goto L_0x019b
        L_0x01f1:
            X.2oH r1 = new X.2oH     // Catch:{ all -> 0x0219 }
            java.lang.Object r0 = r4.A03     // Catch:{ all -> 0x0219 }
            r1.<init>(r0, r7)     // Catch:{ all -> 0x0219 }
            monitor-exit(r4)
            goto L_0x01fb
        L_0x01fa:
            monitor-exit(r4)
        L_0x01fb:
            if (r1 == 0) goto L_0x000c
            return r1
        L_0x01fe:
            X.8s6 r1 = new X.8s6     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "Duplicate template key"
            r1.<init>(r0)     // Catch:{ all -> 0x0216 }
            goto L_0x0215
        L_0x0206:
            X.8s6 r1 = new X.8s6     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "Query parameter does not match templating syntax"
            r1.<init>(r0)     // Catch:{ all -> 0x0216 }
            goto L_0x0215
        L_0x020e:
            X.8s6 r1 = new X.8s6     // Catch:{ all -> 0x0216 }
            java.lang.String r0 = "Duplicate template key"
            r1.<init>(r0)     // Catch:{ all -> 0x0216 }
        L_0x0215:
            throw r1     // Catch:{ all -> 0x0216 }
        L_0x0216:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0219 }
            throw r0     // Catch:{ all -> 0x0219 }
        L_0x0219:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x021c:
            r0 = 0
            return r0
        L_0x021e:
            X.1w4 r1 = new X.1w4
            java.lang.String r0 = "Key may not be null"
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06110ar.A02(java.lang.String):X.2oH");
    }

    public void A03(String str, Object obj) {
        if (str != null) {
            this.A00.add(new C06120at(this, str, obj));
            return;
        }
        throw new C189818s6("Key template may not be null");
    }

    public static Map A00(String str) {
        HashMap A04 = AnonymousClass0TG.A04();
        Matcher matcher = A03.matcher(str);
        while (matcher.find()) {
            A04.put(Uri.decode(matcher.group(1)), Uri.decode(matcher.group(2)));
        }
        return A04;
    }
}
