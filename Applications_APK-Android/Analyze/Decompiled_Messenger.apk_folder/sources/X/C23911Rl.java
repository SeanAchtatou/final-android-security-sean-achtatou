package X;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.1Rl  reason: invalid class name and case insensitive filesystem */
public final class C23911Rl {
    public static final C23911Rl A03 = new C23911Rl();
    public final Executor A00;
    public final ExecutorService A01;
    public final ScheduledExecutorService A02;

    private C23911Rl() {
        boolean contains;
        ExecutorService A002;
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            contains = false;
        } else {
            contains = property.toLowerCase(Locale.US).contains("android");
        }
        if (!contains) {
            A002 = Executors.newCachedThreadPool();
        } else {
            A002 = C23921Rm.A00();
        }
        this.A01 = A002;
        this.A02 = Executors.newSingleThreadScheduledExecutor();
        this.A00 = new C23941Ro();
    }
}
