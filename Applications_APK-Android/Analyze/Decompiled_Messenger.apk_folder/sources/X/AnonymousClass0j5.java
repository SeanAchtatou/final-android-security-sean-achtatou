package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/* renamed from: X.0j5  reason: invalid class name */
public class AnonymousClass0j5 {
    public int A04(Uri uri, String str, String[] strArr) {
        if (this instanceof C28051eB) {
            return ((C25771aN) ((C28051eB) this).A00.A01.get()).A06().delete("properties", str, strArr);
        }
        throw new UnsupportedOperationException();
    }

    public Cursor A05(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String[] strArr3 = strArr;
        String str3 = str;
        String str4 = str2;
        String[] strArr4 = strArr2;
        if (!(this instanceof C28071eD)) {
            Uri uri2 = uri;
            if (!(this instanceof C11300mg)) {
                return A07(uri2, strArr3, str3, strArr4, str4, null);
            }
            return ((AnonymousClass0j5) ((C11300mg) this).A00.get()).A05(uri2, strArr3, str3, strArr4, str4);
        }
        C28071eD r1 = (C28071eD) this;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(C28071eD.A01(r1, strArr3, str3, str4));
        return sQLiteQueryBuilder.query(((C25771aN) r1.A01.get()).A06(), strArr3, str3, strArr4, null, null, str4);
    }

    public Uri A06(Uri uri, ContentValues contentValues) {
        if (!(this instanceof C28051eB)) {
            throw new UnsupportedOperationException();
        }
        SQLiteDatabase A06 = ((C25771aN) ((C28051eB) this).A00.A01.get()).A06();
        C007406x.A00(-150933117);
        A06.replaceOrThrow("properties", null, contentValues);
        C007406x.A00(548217722);
        return null;
    }

    public Cursor A07(Uri uri, String[] strArr, String str, String[] strArr2, String str2, String str3) {
        if (!(this instanceof C28051eB)) {
            throw new UnsupportedOperationException();
        }
        return ((C25771aN) ((C28051eB) this).A00.A01.get()).A06().query("properties", strArr, str, strArr2, null, null, str2, str3);
    }
}
