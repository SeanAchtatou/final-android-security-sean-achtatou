package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import androidx.fragment.app.Fragment;

/* renamed from: X.0qd  reason: invalid class name and case insensitive filesystem */
public final class C13090qd implements C13080qc {
    public final /* synthetic */ C12980qK A00;

    public C13090qd(C12980qK r1) {
        this.A00 = r1;
    }

    public void AMa(C13180qr r2) {
        this.A00.A01.AMa(r2);
    }

    public void AZu() {
        this.A00.A01.AZu();
    }

    public void AZx(Activity activity) {
        this.A00.A01.AZx(activity);
    }

    public Object AqZ(Class cls) {
        C12980qK r1 = this.A00;
        if (!cls.isInstance(r1)) {
            return r1.A01.AqZ(cls);
        }
        return r1;
    }

    public MenuInflater Atv() {
        return this.A00.A01.Atv();
    }

    public Object Aze(Object obj) {
        return this.A00.A01.Aze(obj);
    }

    public Resources B1A() {
        return this.A00.A01.B1A();
    }

    public C13060qW B4m() {
        return this.A00.B4m();
    }

    public View B8z(int i) {
        return this.A00.A01.B8z(i);
    }

    public Window B9w() {
        return this.A00.A01.B9w();
    }

    public boolean BB8(Throwable th) {
        return this.A00.A01.BB8(th);
    }

    public boolean BBy() {
        return this.A00.A01.BBy();
    }

    public boolean BLI(boolean z) {
        return this.A00.A01.BLI(z);
    }

    public void BNB(Bundle bundle) {
        this.A00.A08(bundle);
    }

    public void BNF(Intent intent) {
        this.A00.A0E(intent);
    }

    public void BNH(int i, int i2, Intent intent) {
        this.A00.A01.BNH(i, i2, intent);
    }

    public void BOX(Fragment fragment) {
        this.A00.A0G(fragment);
    }

    public void BPZ(Bundle bundle) {
        this.A00.A01.BPZ(bundle);
    }

    public boolean BUO(MenuItem menuItem) {
        return this.A00.A01.BUO(menuItem);
    }

    public Dialog BUk(int i) {
        return this.A00.A01.BUk(i);
    }

    public boolean BUt(Menu menu) {
        return this.A00.A01.BUt(menu);
    }

    public boolean BhL(MenuItem menuItem) {
        return this.A00.A01.BhL(menuItem);
    }

    public void BjD(Bundle bundle) {
        this.A00.A01.BjD(bundle);
    }

    public void BjI() {
        this.A00.A01.BjI();
    }

    public void BjR(int i, Dialog dialog) {
        this.A00.A01.BjR(i, dialog);
    }

    public boolean BjY(Menu menu) {
        return this.A00.A01.BjY(menu);
    }

    public void BmS() {
        this.A00.A01.BmS();
    }

    public void Bmx(Bundle bundle) {
        this.A00.A0F(bundle);
    }

    public void Btu() {
        this.A00.A01.Btu();
    }

    public void C0V(C27231cr r2) {
        this.A00.A01.C0V(r2);
    }

    public void C1L(C13180qr r2) {
        this.A00.A01.C1L(r2);
    }

    public void C7B(int i) {
        this.A00.A01.C7B(i);
    }

    public void C7C(View view) {
        this.A00.A01.C7C(view);
    }

    public void C8U(Intent intent) {
        this.A00.A01.C8U(intent);
    }

    public void CB0(Object obj, Object obj2) {
        this.A00.A01.CB0(obj, obj2);
    }

    public void CBQ(int i) {
        this.A00.A01.CBQ(i);
    }

    public void CGn(Intent intent) {
        this.A00.A01.CGn(intent);
    }

    public void CIW() {
        this.A00.CIW();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.A00.A01.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return this.A00.A01.dispatchTouchEvent(motionEvent);
    }

    public Intent getIntent() {
        return this.A00.A01.getIntent();
    }

    public void onActivityDestroy() {
        this.A00.A0A();
    }

    public void onAttachedToWindow() {
        this.A00.A01.onAttachedToWindow();
    }

    public void onBackPressed() {
        this.A00.A09();
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.A00.A01.onConfigurationChanged(configuration);
    }

    public void onContentChanged() {
        this.A00.A01.onContentChanged();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.A00.A01.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public View onCreatePanelView(int i) {
        return this.A00.A01.onCreatePanelView(i);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return this.A00.A01.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return this.A00.A01.onKeyUp(i, keyEvent);
    }

    public void onLowMemory() {
        this.A00.A01.onLowMemory();
    }

    public void onPause() {
        this.A00.A0B();
    }

    public void onResume() {
        this.A00.A0C();
    }

    public boolean onSearchRequested() {
        return this.A00.A01.onSearchRequested();
    }

    public void onStart() {
        this.A00.A0D();
    }

    public void onStop() {
        this.A00.A07();
    }

    public void onTrimMemory(int i) {
        this.A00.A01.onTrimMemory(i);
    }

    public void onWindowFocusChanged(boolean z) {
        this.A00.A01.onWindowFocusChanged(z);
    }

    public void startActivityForResult(Intent intent, int i) {
        this.A00.A01.startActivityForResult(intent, i);
    }
}
