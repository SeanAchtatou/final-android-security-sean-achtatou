package X;

import com.facebook.quicklog.QuickPerformanceLogger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.1Pq  reason: invalid class name and case insensitive filesystem */
public final class C23471Pq extends C23441Pn {
    private static volatile C23471Pq A02;
    public final AnonymousClass0Ud A00;
    public final QuickPerformanceLogger A01;

    public static final C23471Pq A00(AnonymousClass1XY r4) {
        if (A02 == null) {
            synchronized (C23471Pq.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A02, r4);
                if (A002 != null) {
                    try {
                        A02 = new C23471Pq(r4.getApplicationInjector());
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A02;
    }

    private C23471Pq(AnonymousClass1XY r2) {
        this.A00 = AnonymousClass0Ud.A00(r2);
        this.A01 = AnonymousClass0ZD.A03(r2);
    }
}
