package X;

import android.view.View;
import android.view.Window;

/* renamed from: X.1ul  reason: invalid class name and case insensitive filesystem */
public final class C37141ul {
    public static void A00(Window window, int i) {
        View decorView = window.getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(null);
        decorView.setSystemUiVisibility(i);
        decorView.setOnSystemUiVisibilityChangeListener(new C37151um(i, window));
    }
}
