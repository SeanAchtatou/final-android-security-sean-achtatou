package X;

/* renamed from: X.08r  reason: invalid class name and case insensitive filesystem */
public final class C010508r implements C010608s {
    public final /* synthetic */ AnonymousClass06H A00;

    public C010508r(AnonymousClass06H r1) {
        this.A00 = r1;
    }

    public void onBackground() {
        this.A00.callListenerBackground();
    }

    public void onForeground() {
        this.A00.callListenersForeground();
    }
}
