package X;

import android.os.MessageQueue;
import com.facebook.common.classmarkers.ClassMarkerLoader;
import com.facebook.common.dextricks.DalvikInternals;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.common.util.TriState;
import com.facebook.messaging.analytics.perf.MessagingInteractionStateManager;
import com.facebook.messaging.model.threadkey.ThreadKey;
import com.facebook.perf.startupstatemachine.StartupStateMachine;
import com.facebook.quicklog.QuickPerformanceLogger;
import com.facebook.redex.dynamicanalysis.DynamicAnalysis;
import com.facebook.redex.dynamicanalysis.DynamicAnalysisTraceManager;
import com.google.common.base.Joiner;
import io.card.payment.BuildConfig;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import javax.inject.Singleton;

@Singleton
/* renamed from: X.0fv  reason: invalid class name and case insensitive filesystem */
public final class C08770fv {
    public static final int[] A0T = {5505027, 5505028, 5505026};
    private static volatile C08770fv A0U;
    public int A00 = 0;
    public long A01 = 0;
    public AnonymousClass08W A02;
    public AnonymousClass0UN A03;
    public AnonymousClass17Y A04;
    public QuickPerformanceLogger A05;
    public Runnable A06;
    public String A07 = "not_preloaded";
    public Set A08 = new HashSet();
    public Set A09 = new HashSet();
    public boolean A0A;
    private int A0B = 0;
    private Runnable A0C = new AnonymousClass0lZ(this);
    public final MessageQueue.IdleHandler A0D = new C10750kn(this);
    public final C04480Uv A0E;
    public final AnonymousClass0WP A0F;
    public final AnonymousClass0Ud A0G;
    public final C08790fx A0H;
    public final AnonymousClass1Y6 A0I;
    public final AnonymousClass069 A0J;
    public final C08860g6 A0K;
    public final C10760ko A0L;
    public final C25501Zw A0M;
    public final StartupStateMachine A0N;
    public final Map A0O = new HashMap();
    public final AtomicInteger A0P = new AtomicInteger();
    public volatile boolean A0Q;
    public volatile boolean A0R;
    public volatile boolean A0S;

    public static final C08770fv A00(AnonymousClass1XY r5) {
        if (A0U == null) {
            synchronized (C08770fv.class) {
                AnonymousClass0WD A002 = AnonymousClass0WD.A00(A0U, r5);
                if (A002 != null) {
                    try {
                        AnonymousClass1XY applicationInjector = r5.getApplicationInjector();
                        A0U = new C08770fv(applicationInjector, AnonymousClass0ZD.A03(applicationInjector));
                        A002.A01();
                    } catch (Throwable th) {
                        A002.A01();
                        throw th;
                    }
                }
            }
        }
        return A0U;
    }

    private void A01(int i) {
        if (((C10960l9) AnonymousClass1XX.A03(AnonymousClass1Y3.BOk, this.A03)).A0B()) {
            this.A05.markerTag(i, "sms");
        }
    }

    public static void A02(C08770fv r8) {
        r8.A0L.A02(false);
        if (r8.A0M()) {
            ClassMarkerLoader.loadIsNotColdStartRunMarker();
            r8.A0M.A07(AnonymousClass07B.A01);
            r8.A0H.A08(5505027);
        } else if (A0A(r8)) {
            r8.A0M.A07(AnonymousClass07B.A0N);
            r8.A0H.A08(5505026);
        } else if (r8.A0N()) {
            r8.A0M.A07(AnonymousClass07B.A0C);
            r8.A0H.A08(5505028);
        }
        StartupStateMachine startupStateMachine = r8.A0N;
        synchronized (startupStateMachine) {
            startupStateMachine.A00 = 2;
        }
        r8.A05.markerEnd(5505027, 4);
        r8.A05.markerEnd(5505175, 4);
        StartupStateMachine startupStateMachine2 = r8.A0N;
        synchronized (startupStateMachine2) {
            startupStateMachine2.A02 = 2;
        }
        r8.A05.markerEnd(5505028, 4);
        StartupStateMachine startupStateMachine3 = r8.A0N;
        synchronized (startupStateMachine3) {
            startupStateMachine3.A03 = 2;
        }
        r8.A05.markerEnd(5505026, 4);
        C08860g6 r3 = r8.A0K;
        ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r3.A00)).AOI(r3.A05, "application_backgrounded");
        ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r3.A00)).AYS(r3.A05);
        r8.A0S = false;
        r8.A04 = null;
    }

    public static void A03(C08770fv r2) {
        if (!r2.A0A) {
            r2.A0I.C4C(r2.A0C);
        }
    }

    public static void A04(C08770fv r2, int i, String str) {
        if (r2.A05.isMarkerOn(5505032, i)) {
            r2.A05.markerTag(5505032, i, str);
        }
        if (r2.A05.isMarkerOn(5505138)) {
            r2.A05.markerTag(5505138, i, str);
        }
    }

    public static void A06(C08770fv r6, String str) {
        if (r6.A0S) {
            r6.A05.markerTag(5505028, "mainactivity_closed");
        }
        r6.A0S = false;
        if (r6.A0N()) {
            StartupStateMachine startupStateMachine = r6.A0N;
            synchronized (startupStateMachine) {
                startupStateMachine.A02 = 1;
            }
            r6.A05.markerTag(5505028, str);
            r6.A01(5505028);
            r6.A05.markerEnd(5505028, 2);
            r6.A0M.A08(AnonymousClass07B.A0C);
            ((C159747ai) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A2L, r6.A03)).A03(5505028);
            C08860g6 r3 = r6.A0K;
            ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r3.A00)).AOI(r3.A05, "luke_warm_start_complete");
            r6.A0H.A08(5505028);
            ((AnonymousClass9SK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AUW, r6.A03)).A01();
        }
        r6.A0Q = false;
        r6.A0L.A02(false);
    }

    public static void A09(C08770fv r5, short s) {
        r5.A0L.A02(false);
        r5.A0S = false;
        C08790fx.A05(r5.A0H, 5505027, s);
        DalvikInternals.onColdStartEnd();
        if (r5.A02 != null) {
            AnonymousClass08W r3 = r5.A02;
            QuickPerformanceLogger quickPerformanceLogger = (QuickPerformanceLogger) AnonymousClass1XX.A03(AnonymousClass1Y3.BBd, ((C84083yp) AnonymousClass1XX.A03(AnonymousClass1Y3.AID, r5.A03)).A00);
            quickPerformanceLogger.setAlwaysOnSampleRate(34603017, 100);
            quickPerformanceLogger.setAlwaysOnSampleRate(34603015, 100);
            quickPerformanceLogger.setAlwaysOnSampleRate(34603016, 100);
            quickPerformanceLogger.setAlwaysOnSampleRate(34603018, 100);
            quickPerformanceLogger.setAlwaysOnSampleRate(34603019, 100);
            quickPerformanceLogger.setAlwaysOnSampleRate(34603020, 100);
            quickPerformanceLogger.setAlwaysOnSampleRate(34603021, 100);
            C22859BId bId = new C22859BId(quickPerformanceLogger);
            synchronized (r3) {
                Iterator it = r3.A01.iterator();
                while (it.hasNext()) {
                    AnonymousClass08W.A00(bId, (C010308p) it.next());
                }
                r3.A01.clear();
                r3.A00 = bId;
            }
            r5.A02 = null;
        }
    }

    public static boolean A0A(C08770fv r2) {
        if (r2.A0N.A03() == 3) {
            return true;
        }
        return false;
    }

    public void A0B() {
        int i = this.A0B;
        if (i > 0) {
            int i2 = i - 1;
            this.A0B = i2;
            if (i2 == 0) {
                this.A05.markerEnd(5505060, 2);
            }
        }
    }

    public void A0C(int i, int i2, String str) {
        if (!this.A05.isMarkerOn(i, i2)) {
            this.A05.markerStart(i, i2);
            this.A05.markerAnnotate(i, i2, "starting_location", str);
        }
    }

    public void A0D(int i, int i2, short s) {
        this.A05.markerPoint(i, i2, Short.toString(s));
    }

    public void A0F(String str) {
        this.A05.markerPoint(5505027, str);
        this.A05.markerPoint(5505028, str);
        this.A05.markerPoint(5505026, str);
        this.A05.markerPoint(5505032, str);
        this.A05.markerPoint(5505138, str);
    }

    public void A0G(String str) {
        for (int i : A0T) {
            if (this.A05.isMarkerOn(i)) {
                this.A05.markerPoint(i, str);
            }
        }
    }

    public void A0H(String str) {
        MessagingInteractionStateManager messagingInteractionStateManager = (MessagingInteractionStateManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ApC, this.A03);
        int hashCode = str.hashCode();
        MessagingInteractionStateManager.A01(messagingInteractionStateManager, 5505025, hashCode);
        messagingInteractionStateManager.A01.markerStartWithCounter(5505025, hashCode);
        this.A05.markerStartWithCounter(5505076, hashCode);
        A03(this);
        this.A0O.put(str, C02240Ds.A01("send_message: %s", new Object[]{str}));
    }

    public void A0J(String str) {
        int i = this.A0B;
        this.A0B = i + 1;
        if (i == 0) {
            this.A05.markerStart(5505060);
        }
        this.A05.markerTag(5505060, str);
    }

    public void A0K(String str, String str2) {
        this.A05.markerTag(5505025, str.hashCode(), str2);
    }

    public void A0L(String str, short s) {
        this.A05.markerPoint(5505025, str.hashCode(), Short.toString(s));
    }

    public boolean A0M() {
        if (this.A0N.A01() == 3) {
            return true;
        }
        return false;
    }

    public boolean A0N() {
        if (this.A0N.A02() == 3) {
            return true;
        }
        return false;
    }

    private C08770fv(AnonymousClass1XY r4, QuickPerformanceLogger quickPerformanceLogger) {
        this.A03 = new AnonymousClass0UN(5, r4);
        this.A05 = AnonymousClass0ZD.A03(r4);
        this.A0I = AnonymousClass0UX.A07(r4);
        this.A0F = C25091Yh.A00(r4);
        this.A0G = AnonymousClass0Ud.A00(r4);
        this.A0L = C10760ko.A01(r4);
        this.A0E = C04490Ux.A0n(r4);
        this.A0M = C25501Zw.A00(r4);
        this.A0J = AnonymousClass067.A03(r4);
        C12150od.A00(r4);
        this.A0N = StartupStateMachine.A00(r4);
        this.A0H = C08790fx.A02(r4);
        this.A0K = C08860g6.A00(r4);
        this.A05 = quickPerformanceLogger;
        quickPerformanceLogger.setAlwaysOnSampleRate(5505060, AnonymousClass1Y3.A87);
        this.A05.setAlwaysOnSampleRate(5505025, 10);
        this.A05.setAlwaysOnSampleRate(5505049, 10);
        this.A05.setAlwaysOnSampleRate(5505047, 10);
    }

    public static void A05(C08770fv r4, int i, boolean z) {
        AnonymousClass17Y r0;
        ClassMarkerLoader.loadIsMessengerStartToInboxFinishMarker();
        r4.A05.markerTag(i, "dst_THREADLIST");
        r4.A05.markerAnnotate(i, "destination", "threadlist");
        if (r4.A05.isMarkerOn(i) && (r0 = r4.A04) != null) {
            r4.A05.markerAnnotate(i, "thread_list_types", ((C19911Ae) AnonymousClass1XX.A02(20, AnonymousClass1Y3.AOb, r0.A00.A0O)).A02(false));
        }
        if (z) {
            r4.A05.markerTag(i, "is_in_chat_heads");
            r4.A05.markerAnnotate(i, "source", "chat_heads");
        }
    }

    public static void A07(C08770fv r5, String str) {
        if (A0A(r5)) {
            StartupStateMachine startupStateMachine = r5.A0N;
            synchronized (startupStateMachine) {
                startupStateMachine.A03 = 1;
            }
            r5.A05.markerTag(5505026, str);
            r5.A01(5505026);
            r5.A05.markerEnd(5505026, 2);
            r5.A0M.A08(AnonymousClass07B.A0N);
            ((C159747ai) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A2L, r5.A03)).A03(5505026);
            C08860g6 r3 = r5.A0K;
            ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r3.A00)).AOI(r3.A05, "warm_start_complete");
            r5.A0H.A08(5505026);
            ((AnonymousClass9SK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AUW, r5.A03)).A01();
        }
        r5.A0L.A02(false);
    }

    public static void A08(C08770fv r10, short s) {
        boolean z;
        if (r10.A0M()) {
            if (s == 4 || r10.A0G.A0C() == TriState.YES || r10.A0G.A0G()) {
                ClassMarkerLoader.loadIsNotColdStartRunMarker();
            } else {
                ClassMarkerLoader.loadColdStartTTIRunMarker();
                String $const$string = TurboLoader.Locator.$const$string(AnonymousClass1Y3.A0n);
                synchronized (DynamicAnalysisTraceManager.class) {
                    boolean z2 = false;
                    if (DynamicAnalysis.sNumStaticallyInstrumented > 0) {
                        z2 = true;
                    }
                    if (z2) {
                        String str = DynamicAnalysisTraceManager.A00;
                        if (!str.equals($const$string)) {
                            C010708t.A0P("DYNA|TraceManager", "Trying to end tracing of a different interaction: expected: %s vs actual %s!", $const$string, str);
                        } else if (DynamicAnalysisTraceManager.A02.availablePermits() != 0) {
                            C010708t.A0P("DYNA|TraceManager", "Trying to end tracing for %s when tracing not started!", $const$string);
                        } else {
                            DynamicAnalysisTraceManager.A00 = BuildConfig.FLAVOR;
                            DynamicAnalysis.A00 = false;
                            int i = DynamicAnalysis.sNumStaticallyInstrumented;
                            if (i == 0) {
                                C010708t.A0J("DYNA", "Tracing has been stopped: App has not been instrumented");
                            } else {
                                C010708t.A0P("DYNA", "Tracing has been stopped: %d methods (%d shards interleaved) were instrumented; approx. %d methods data were collected", Integer.valueOf(i), Integer.valueOf(DynamicAnalysis.sMethodStatsArray.length), Integer.valueOf(DynamicAnalysis.A06.get()));
                            }
                            C010708t.A0J("DYNA|TraceManager", "Starting to dump the trace!");
                            new Thread(new C03660Oy($const$string)).start();
                            z = true;
                        }
                    }
                    z = false;
                }
                if (z) {
                    DynamicAnalysis.A01 = DynamicAnalysis.A06.incrementAndGet();
                    DynamicAnalysis.A07 = true;
                }
            }
            StartupStateMachine startupStateMachine = r10.A0N;
            synchronized (startupStateMachine) {
                startupStateMachine.A00 = 1;
            }
            r10.A05.dropAllInstancesOfMarker(5505190);
            r10.A05.markerTag(5505027, Joiner.on("+").join("activity", r10.A09, new Object[0]));
            r10.A05.markerAnnotate(5505027, "activity", Joiner.on(",").useForNull("null").join(r10.A09));
            r10.A05.markerTag(5505027, Joiner.on("+").join("action", r10.A08, new Object[0]));
            r10.A05.markerAnnotate(5505027, "action", Joiner.on(",").useForNull("null").join(r10.A08));
            r10.A01(5505027);
            r10.A05.markerEnd(5505027, s);
            r10.A05.markerEnd(5505175, s);
            r10.A0M.A08(AnonymousClass07B.A01);
            A09(r10, s);
            ((C159747ai) AnonymousClass1XX.A02(1, AnonymousClass1Y3.A2L, r10.A03)).A03(5505027);
            C08860g6 r3 = r10.A0K;
            ((C185414b) AnonymousClass1XX.A02(0, AnonymousClass1Y3.AQl, r3.A00)).AOI(r3.A05, "cold_start_complete");
            ((AnonymousClass9SK) AnonymousClass1XX.A02(3, AnonymousClass1Y3.AUW, r10.A03)).A01();
        }
    }

    public void A0E(ThreadKey threadKey, String str, String str2, int i) {
        C192917o r7;
        String str3;
        int hashCode = threadKey.A0J().hashCode();
        this.A05.markerStartWithCounter(5505032, hashCode);
        if (this.A0M.A05(AnonymousClass07B.A01) != 0) {
            this.A05.markerAnnotate(5505032, hashCode, "time_since_cold_startup", Long.toString(this.A0M.A05(AnonymousClass07B.A01)));
        }
        if (this.A0M.A05(AnonymousClass07B.A0C) != 0) {
            this.A05.markerAnnotate(5505032, hashCode, "time_since_lukewarm_startup", Long.toString(this.A0M.A05(AnonymousClass07B.A0C)));
        }
        if (this.A0M.A05(AnonymousClass07B.A0N) != 0) {
            this.A05.markerAnnotate(5505032, hashCode, "time_since_warm_startup", Long.toString(this.A0M.A05(AnonymousClass07B.A0N)));
        }
        C32781mK r1 = (C32781mK) AnonymousClass1XX.A02(2, AnonymousClass1Y3.AvI, this.A03);
        if (r1.A00 == null) {
            r7 = null;
        } else {
            r7 = r1.A04;
        }
        if (r7 == null) {
            r7 = r1.A01;
        }
        if (r7 != null) {
            QuickPerformanceLogger quickPerformanceLogger = this.A05;
            long j = r7.A03;
            quickPerformanceLogger.markerStartWithCounter(5505138, hashCode, j - (j - r7.A01));
            String valueOf = String.valueOf(r7.A03 - r7.A01);
            this.A05.markerAnnotate(5505032, hashCode, "touch_event_latency", valueOf);
            this.A05.markerAnnotate(5505138, hashCode, "touch_event_latency", valueOf);
        }
        if (threadKey.A0L()) {
            str3 = "group_thread";
        } else if (C12150od.A02(threadKey)) {
            str3 = "montage_thread";
        } else if (ThreadKey.A0E(threadKey)) {
            str3 = "sms_thread";
        } else {
            str3 = "one_on_one_thread";
        }
        this.A05.markerTag(5505032, hashCode, str3);
        if (r7 != null) {
            this.A05.markerTag(5505138, hashCode, str3);
        }
        this.A05.markerTag(5505032, hashCode, this.A07);
        this.A05.markerAnnotate(5505032, hashCode, "prefetch_status", str);
        this.A05.markerAnnotate(5505138, hashCode, "prefetch_status", str);
        this.A05.markerAnnotate(5505032, hashCode, "nav_source", str2);
        if (i >= 0) {
            this.A05.markerAnnotate(5505032, hashCode, "position_in_list", String.valueOf(i));
        }
        this.A05.markerAnnotate(5505032, hashCode, "thread_type", threadKey.A05.toString());
        if (this.A05.isMarkerOn(5505032, hashCode)) {
            ClassMarkerLoader.loadMessengerThreadListToThreadViewStartMarker();
            String str4 = this.A07;
            if (str4 == "preloaded") {
                ClassMarkerLoader.loadMessengerThreadListToThreadViewPreLoadedMarker();
            } else if (str4 == "prerendered") {
                ClassMarkerLoader.loadMessengerThreadListToThreadViewPreRenderedMarker();
            } else if (str4 == "not_preloaded") {
                ClassMarkerLoader.loadMessengerThreadListToThreadViewNotPreloadedMarker();
            }
        }
        this.A07 = "reused";
        A03(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void
     arg types: [int, int, int]
     candidates:
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, short, long):void
      com.facebook.quicklog.QuickPerformanceLogger.markerEnd(int, int, short):void */
    public void A0I(String str) {
        A02(this);
        int hashCode = str.hashCode();
        ((MessagingInteractionStateManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ApC, this.A03)).A02(5505034, hashCode, 4);
        this.A05.markerEnd(5505032, hashCode, (short) 4);
        if (this.A05.isMarkerOn(5505138)) {
            this.A05.markerEnd(5505138, hashCode, (short) 4);
        }
        ((MessagingInteractionStateManager) AnonymousClass1XX.A02(0, AnonymousClass1Y3.ApC, this.A03)).A02(5505025, 0, 4);
    }
}
