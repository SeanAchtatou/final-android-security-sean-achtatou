package X;

import com.facebook.acra.ACRA;
import com.facebook.common.dextricks.turboloader.TurboLoader;
import com.facebook.forker.Process;
import org.webrtc.audio.WebRtcAudioRecord;

/* renamed from: X.21J  reason: invalid class name */
public final class AnonymousClass21J {
    public static String A00(Integer num) {
        switch (num.intValue()) {
            case 1:
                return C99084oO.$const$string(AnonymousClass1Y3.A0w);
            case 2:
                return "PLATFORM_TAB";
            case 3:
                return "PLATFORM_TAB_CATEGORY";
            case 4:
                return "DISCOVER_TAB_M4";
            case 5:
                return "GROUPS_TAB";
            case ACRA.MULTI_SIGNAL_ANR_DETECTOR:
                return "WORKCHAT_DISCOVERY_TAB";
            case WebRtcAudioRecord.DEFAULT_AUDIO_SOURCE /*7*/:
                return "MORE_DRAWER_BROWSE";
            case 8:
                return "RECENT_THREAD_LIST";
            case Process.SIGKILL:
                return "BUSINESS_TAB";
            case AnonymousClass1Y3.A01:
                return "MESSAGE_REQUEST_LIST";
            case AnonymousClass1Y3.A02:
                return "FILTERED_REQUEST_LIST";
            default:
                return TurboLoader.Locator.$const$string(30);
        }
    }
}
