package X;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableEntry;
import java.util.Map;

/* renamed from: X.1Fe  reason: invalid class name and case insensitive filesystem */
public abstract class C21091Fe<K, V> extends C21101Ff<K, V> {
    public final Predicate A00;
    public final Map A01;

    public boolean containsKey(Object obj) {
        if (this.A01.containsKey(obj)) {
            if (!this.A00.apply(new ImmutableEntry(obj, this.A01.get(obj)))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public Object get(Object obj) {
        Object obj2 = this.A01.get(obj);
        if (obj2 == null || !this.A00.apply(new ImmutableEntry(obj, obj2))) {
            return null;
        }
        return obj2;
    }

    public Object put(Object obj, Object obj2) {
        Preconditions.checkArgument(this.A00.apply(new ImmutableEntry(obj, obj2)));
        return this.A01.put(obj, obj2);
    }

    public C21091Fe(Map map, Predicate predicate) {
        this.A01 = map;
        this.A00 = predicate;
    }

    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    public void putAll(Map map) {
        for (Map.Entry entry : map.entrySet()) {
            Preconditions.checkArgument(this.A00.apply(new ImmutableEntry(entry.getKey(), entry.getValue())));
        }
        this.A01.putAll(map);
    }

    public Object remove(Object obj) {
        if (containsKey(obj)) {
            return this.A01.remove(obj);
        }
        return null;
    }
}
