package X;

import com.facebook.mobileconfig.MobileConfigCxxChangeListener;
import com.facebook.mobileconfig.MobileConfigEmergencyPushChangeListener;
import com.facebook.tigon.iface.TigonServiceHolder;
import java.util.Map;

/* renamed from: X.0WV  reason: invalid class name */
public interface AnonymousClass0WV {
    void clearAlternativeUpdater();

    void clearOverrides();

    void deleteOldUserData(int i);

    String getConsistencyLoggingFlagsJSON();

    String getFrameworkStatus();

    C06190b2 getLatestHandle();

    C25851aV getNewOverridesTable();

    C25851aV getNewOverridesTableIfExists();

    boolean isConsistencyLoggingNeeded(AnonymousClass8ZO r1);

    boolean isFetchNeeded();

    boolean isTigonServiceSet();

    boolean isValid();

    void logConfigs(String str, AnonymousClass8ZO r2, Map map);

    void logExposure(String str, String str2, String str3);

    void logShadowResult(String str, String str2, String str3, String str4, String str5, String str6);

    void logStorageConsistency();

    boolean registerConfigChangeListener(MobileConfigCxxChangeListener mobileConfigCxxChangeListener);

    boolean setEpHandler(MobileConfigEmergencyPushChangeListener mobileConfigEmergencyPushChangeListener);

    boolean setSandboxURL(String str);

    void setTigonService(TigonServiceHolder tigonServiceHolder, boolean z);

    String syncFetchReason();

    boolean tryUpdateConfigsSynchronously(int i);

    boolean updateConfigs();

    boolean updateConfigsSynchronouslyWithDefaultUpdater(int i);

    boolean updateEmergencyPushConfigs();

    boolean updateEmergencyPushConfigsSynchronously(int i);
}
