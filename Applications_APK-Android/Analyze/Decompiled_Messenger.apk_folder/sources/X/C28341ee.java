package X;

import android.net.Uri;
import com.facebook.common.json.FbJsonDeserializer;
import com.facebook.common.json.UriDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.JacksonDeserializers$TokenBufferDeserializer;
import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.1ee  reason: invalid class name and case insensitive filesystem */
public final class C28341ee {
    private static final FbJsonDeserializer A00 = new FbJsonDeserializer();
    private static final ConcurrentMap A01;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fasterxml.jackson.databind.JsonDeserializer A00(java.lang.Class r3) {
        /*
            java.util.concurrent.ConcurrentMap r0 = X.C28341ee.A01
            java.lang.Object r1 = r0.get(r3)
            com.fasterxml.jackson.databind.JsonDeserializer r1 = (com.fasterxml.jackson.databind.JsonDeserializer) r1
            com.facebook.common.json.FbJsonDeserializer r0 = X.C28341ee.A00
            if (r1 != r0) goto L_0x000e
            r0 = 0
            return r0
        L_0x000e:
            if (r1 == 0) goto L_0x0011
            return r1
        L_0x0011:
            java.lang.Class<X.1fn> r0 = X.C29051fn.class
            boolean r0 = r0.isAssignableFrom(r3)
            if (r0 == 0) goto L_0x0065
            com.facebook.common.json.TypeModelBase64Deserializer r0 = new com.facebook.common.json.TypeModelBase64Deserializer
            r0.<init>()
        L_0x001e:
            if (r0 == 0) goto L_0x0032
            r1 = r0
            com.facebook.common.json.FbJsonDeserializer r1 = (com.facebook.common.json.FbJsonDeserializer) r1
            r1.init(r3)
        L_0x0026:
            java.util.concurrent.ConcurrentMap r2 = X.C28341ee.A01
            if (r0 == 0) goto L_0x002f
            r1 = r0
        L_0x002b:
            r2.putIfAbsent(r3, r1)
            return r0
        L_0x002f:
            com.facebook.common.json.FbJsonDeserializer r1 = X.C28341ee.A00
            goto L_0x002b
        L_0x0032:
            java.lang.String r2 = r3.getName()
            r1 = 36
            r0 = 95
            java.lang.String r1 = r2.replace(r1, r0)
            java.lang.String r0 = "Deserializer"
            java.lang.String r0 = r1.concat(r0)
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException | ExceptionInInitializerError | IllegalAccessException | InstantiationException | NoClassDefFoundError -> 0x004f }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ ClassNotFoundException | ExceptionInInitializerError | IllegalAccessException | InstantiationException | NoClassDefFoundError -> 0x004f }
            com.fasterxml.jackson.databind.JsonDeserializer r0 = (com.fasterxml.jackson.databind.JsonDeserializer) r0     // Catch:{ ClassNotFoundException | ExceptionInInitializerError | IllegalAccessException | InstantiationException | NoClassDefFoundError -> 0x004f }
            goto L_0x0050
        L_0x004f:
            r0 = 0
        L_0x0050:
            if (r0 != 0) goto L_0x0026
            java.lang.String r0 = "$Deserializer"
            java.lang.String r0 = r2.concat(r0)
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException | ExceptionInInitializerError | IllegalAccessException | InstantiationException | NoClassDefFoundError -> 0x0063 }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ ClassNotFoundException | ExceptionInInitializerError | IllegalAccessException | InstantiationException | NoClassDefFoundError -> 0x0063 }
            com.fasterxml.jackson.databind.JsonDeserializer r0 = (com.fasterxml.jackson.databind.JsonDeserializer) r0     // Catch:{ ClassNotFoundException | ExceptionInInitializerError | IllegalAccessException | InstantiationException | NoClassDefFoundError -> 0x0063 }
            goto L_0x0026
        L_0x0063:
            r0 = 0
            goto L_0x0026
        L_0x0065:
            java.lang.Class<X.0oo> r0 = X.C12200oo.class
            boolean r0 = r0.isAssignableFrom(r3)
            if (r0 == 0) goto L_0x007b
            java.lang.Class<X.3b1> r0 = X.C70653b1.class
            boolean r0 = r0.isAssignableFrom(r3)
            if (r0 != 0) goto L_0x007b
            com.facebook.common.json.TreeFragmentModelBase64Deserializer r0 = new com.facebook.common.json.TreeFragmentModelBase64Deserializer
            r0.<init>(r3)
            goto L_0x0026
        L_0x007b:
            java.lang.Class<X.1fo> r0 = X.C29061fo.class
            boolean r0 = r0.isAssignableFrom(r3)
            if (r0 == 0) goto L_0x0089
            com.facebook.common.json.FragmentModelDeserializer r0 = new com.facebook.common.json.FragmentModelDeserializer
            r0.<init>()
            goto L_0x001e
        L_0x0089:
            java.lang.Class<X.1fp> r0 = X.C29071fp.class
            boolean r0 = r0.isAssignableFrom(r3)
            if (r0 == 0) goto L_0x0097
            com.facebook.common.json.FBJsonSelfDeserializer r0 = new com.facebook.common.json.FBJsonSelfDeserializer
            r0.<init>()
            goto L_0x001e
        L_0x0097:
            r0 = 0
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C28341ee.A00(java.lang.Class):com.fasterxml.jackson.databind.JsonDeserializer");
    }

    static {
        Object obj;
        ConcurrentMap A07 = AnonymousClass0TG.A07();
        A01 = A07;
        Class<JsonNode> cls = JsonNode.class;
        if (cls == ObjectNode.class) {
            obj = JsonNodeDeserializer.ObjectDeserializer._instance;
        } else if (cls == ArrayNode.class) {
            obj = JsonNodeDeserializer.ArrayDeserializer._instance;
        } else {
            obj = JsonNodeDeserializer.instance;
        }
        A07.put(cls, obj);
        ConcurrentMap concurrentMap = A01;
        concurrentMap.put(String.class, StringDeserializer.instance);
        Class<Integer> cls2 = Integer.class;
        concurrentMap.put(cls2, C28351ef.find(cls2, cls2.getName()));
        Class<Long> cls3 = Long.class;
        concurrentMap.put(cls3, C28351ef.find(cls3, cls3.getName()));
        Class<Boolean> cls4 = Boolean.class;
        concurrentMap.put(cls4, C28351ef.find(cls4, cls4.getName()));
        Class<Float> cls5 = Float.class;
        concurrentMap.put(cls5, C28351ef.find(cls5, cls5.getName()));
        Class<Double> cls6 = Double.class;
        concurrentMap.put(cls6, C28351ef.find(cls6, cls6.getName()));
        concurrentMap.put(Uri.class, new UriDeserializer());
        concurrentMap.put(C11700no.class, JacksonDeserializers$TokenBufferDeserializer.instance);
        concurrentMap.put(Object.class, UntypedObjectDeserializer.instance);
    }
}
