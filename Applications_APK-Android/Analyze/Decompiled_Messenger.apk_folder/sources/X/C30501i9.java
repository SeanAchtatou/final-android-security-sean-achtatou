package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.facebook.acra.LogCatCollector;
import java.io.UnsupportedEncodingException;

/* renamed from: X.1i9  reason: invalid class name and case insensitive filesystem */
public abstract class C30501i9 {
    public final AnonymousClass0W6 A00;
    public final AnonymousClass0W6 A01;
    public final AnonymousClass0W6 A02;
    public final String A03;
    public final C04310Tq A04;

    public void A01(AnonymousClass19A r5) {
        SQLiteDatabase A012 = ((AnonymousClass183) this.A04.get()).A01();
        if (A012 != null) {
            A012.delete(this.A03, AnonymousClass08S.A0J(this.A00.A00, " = ?"), new String[]{r5.A05()});
        }
    }

    public void A02(AnonymousClass19A r2, boolean z) {
        String str;
        if (z) {
            str = "1";
        } else {
            str = "0";
        }
        A06(r2, str);
    }

    public void A03(AnonymousClass19A r6, byte[] bArr) {
        SQLiteDatabase A012 = ((AnonymousClass183) this.A04.get()).A01();
        if (A012 != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(this.A00.A00, r6.A05());
            contentValues.put(this.A02.A00, bArr);
            String str = this.A03;
            C007406x.A00(1501530031);
            A012.replaceOrThrow(str, null, contentValues);
            C007406x.A00(1203826641);
        }
    }

    public byte[] A04(AnonymousClass19A r12) {
        SQLiteDatabase A012 = ((AnonymousClass183) this.A04.get()).A01();
        if (A012 == null) {
            return null;
        }
        String[] strArr = {this.A02.A00};
        C06140av A032 = this.A00.A03(r12.A05());
        Cursor query = A012.query(this.A03, strArr, A032.A02(), A032.A04(), null, null, null);
        try {
            if (query.moveToNext()) {
                return this.A02.A07(query);
            }
            query.close();
            return null;
        } finally {
            query.close();
        }
    }

    public String A05(AnonymousClass19A r12) {
        if (!(this instanceof C30491i8)) {
            SQLiteDatabase A012 = ((AnonymousClass183) this.A04.get()).A01();
            if (A012 == null) {
                return null;
            }
            String[] strArr = {this.A02.A00};
            C06140av A032 = this.A00.A03(r12.A05());
            Cursor query = A012.query(this.A03, strArr, A032.A02(), A032.A04(), null, null, null);
            try {
                if (query.moveToNext()) {
                    return this.A02.A05(query);
                }
                query.close();
                return null;
            } finally {
                query.close();
            }
        } else {
            byte[] A042 = ((C30491i8) this).A04(r12);
            if (A042 == null) {
                return null;
            }
            try {
                return new String(A042, LogCatCollector.UTF_8_ENCODING);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Failed to decode key from keychain", e);
            }
        }
    }

    public void A06(AnonymousClass19A r6, String str) {
        if (!(this instanceof C30491i8)) {
            SQLiteDatabase A012 = ((AnonymousClass183) this.A04.get()).A01();
            if (A012 != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(this.A00.A00, r6.A05());
                contentValues.put(this.A02.A00, str);
                String str2 = this.A03;
                C007406x.A00(1199207578);
                A012.replaceOrThrow(str2, null, contentValues);
                C007406x.A00(9484497);
                return;
            }
            return;
        }
        ((C30491i8) this).A03(r6, str.getBytes());
    }

    public C30501i9(C04310Tq r1, String str, AnonymousClass0W6 r3, AnonymousClass0W6 r4, AnonymousClass0W6 r5) {
        this.A04 = r1;
        this.A03 = str;
        this.A00 = r3;
        this.A02 = r4;
        this.A01 = r5;
    }
}
