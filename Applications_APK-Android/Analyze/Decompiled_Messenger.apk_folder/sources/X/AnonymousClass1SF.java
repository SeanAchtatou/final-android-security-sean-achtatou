package X;

import com.facebook.common.dextricks.OptSvcAnalyticsStore;
import com.facebook.fbtrace.FbTraceNode;
import com.facebook.proxygen.TraceFieldType;
import java.io.ByteArrayInputStream;

/* renamed from: X.1SF  reason: invalid class name */
public final class AnonymousClass1SF implements C01390Aa {
    private final C34371pT A00;
    private final C04310Tq A01;

    public void BIu(Object obj, boolean z, String str) {
        String str2;
        C33631nt A002 = C37011uT.A00((FbTraceNode) obj);
        if (z) {
            str2 = "true";
        } else {
            str2 = "false";
        }
        A002.put(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, str2);
        if (str != null) {
            A002.put(TraceFieldType.ErrorCode, str);
        }
    }

    public Object BJK(String str, byte[] bArr) {
        if (!str.startsWith("/t_")) {
            return FbTraceNode.A03;
        }
        try {
            C37001uS A002 = C37001uS.A00(new C36291sp().Azf(new C36311sr(new ByteArrayInputStream(bArr))));
            String str2 = A002.traceInfo;
            if (str2 == null || str2.length() != 22) {
                return FbTraceNode.A03;
            }
            FbTraceNode A03 = this.A00.A03(str2);
            C33631nt A003 = C37011uT.A00(A03);
            A003.put("op", "mqtt_publish_received");
            A003.put("service", "receiver_mqtt_client");
            A003.put("appfg", this.A01.get());
            String A02 = C34371pT.A00(A03).A02();
            int length = A02.length();
            String str3 = A002.traceInfo;
            if (length != str3.length() && !A02.equals(str3)) {
                return FbTraceNode.A03;
            }
            byte[] A004 = new C36281so(new C36291sp()).A00(new C37001uS(A02));
            System.arraycopy(A004, 0, bArr, 0, A004.length);
            return A03;
        } catch (C70863bP unused) {
            return FbTraceNode.A03;
        }
    }

    public Object BJL(String str, byte[] bArr) {
        if (!str.startsWith("/t_")) {
            return FbTraceNode.A03;
        }
        try {
            C37001uS A002 = C37001uS.A00(new C36291sp().Azf(new C36311sr(new ByteArrayInputStream(bArr))));
            String str2 = A002.traceInfo;
            if (str2 == null || str2.length() != 22) {
                return FbTraceNode.A03;
            }
            FbTraceNode A03 = this.A00.A03(str2);
            C33631nt A003 = C37011uT.A00(A03);
            A003.put("op", "mqtt_publish_send");
            A003.put("service", "sender_mqtt_client");
            FbTraceNode A004 = C34371pT.A00(A03);
            String A02 = A004.A02();
            if (A02.length() != A002.traceInfo.length()) {
                return FbTraceNode.A03;
            }
            C37011uT.A00(A004).put("op", "proxygen_publish_send");
            byte[] A005 = new C36281so(new C36291sp()).A00(new C37001uS(A02));
            System.arraycopy(A005, 0, bArr, 0, A005.length);
            return A03;
        } catch (C70863bP unused) {
            return FbTraceNode.A03;
        }
    }

    public void BJQ(Object obj, boolean z, String str) {
        String str2;
        C33631nt A002 = C37011uT.A00((FbTraceNode) obj);
        if (z) {
            str2 = "true";
        } else {
            str2 = "false";
        }
        A002.put(OptSvcAnalyticsStore.LOGGING_KEY_DEX2OAT_SUCCESS, str2);
        if (str != null) {
            A002.put(TraceFieldType.ErrorCode, str);
        }
    }

    public AnonymousClass1SF(C34371pT r1, C04310Tq r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
